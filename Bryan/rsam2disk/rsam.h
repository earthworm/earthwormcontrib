/* rsam.h: rsam header file */

#ifndef _RSAM_H
#define _RSAM_H

#ifndef _TIME_H
	#include <time.h>
#endif

#define MAXPATHLEN	256

#define	RSAMLABLEN	8
#define STN_LEN  	5 /* maximum length of stn labels */
#define CHA_LEN  	4 /* maximum length of cha labels */
#define NET_LEN  	4 /* maximum length of net labels */


/* Stucture to hold station information */
typedef struct scn_struct {
	char		stn[STN_LEN];
	char		cha[CHA_LEN];
	char		net[NET_LEN];
} SCNstruct;

/* Structure to hold rsam header information */
typedef struct rsam_header {
	char		station[RSAMLABLEN];
	char		channel[RSAMLABLEN];
	char		network[RSAMLABLEN];
	int			year;
	int			no_samples_per_day;		/* number of rsam samples per day */
	long		length;					/* number of rsam samples per file */
	double 		delta; 					/* width of the rsam interval (secs) */
} RSAM_HEADER;

/* Structure to hold time period stuff associated with a data point 
** modified from Vidmar ew2rsam */
typedef struct time_period_struct
{
	double		time_period;			/* time period in seconds */
	double		rsam_value;				/* current rsam total */
	int			rsam_nsamp;				/* number of samples in current total */
	double		rsam_starttime;			/* start time of this bin */
} TSTRUCT;


/* Structure to hold rsam data */
typedef struct rsam_data {
	/* DC Offset stuff (from Vidmar ew2rsam) */
	double		DC_offset;					/* DC offset value */
	double		DC_curr_val;				/* Current DC total */
	int			DC_cur_nsamp;				/* Nunber of samples in current total */
	double		DC_starttime;				/* Averages for previous time slices */
	double		DC_array[DC_ARRAY_ENTRIES];	/* index of current position in array */
	int			DC_cur_pos;					/* index of starting position in array */
	int			DC_startup;					/* TRUE is starting up	*/

	/* Start time period stuff (from Vidamr ew2rsam) */
	TSTRUCT		TP[MAX_TIME_PERIODS];
} RSAM_DATA;

typedef struct rsam
{
	RSAM_HEADER	*rsam;						/* RSAM header information */
	RSAM_DATA	*rdata;						/* the RSAM data */
} RSAM;

#endif  /*_RSAM_H */
