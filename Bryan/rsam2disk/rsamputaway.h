/* rsamputaway.h: rsam header file */

#ifndef _RSAMPUTAWAY_H
#define _RSAMPUTAWAY_H

/* Function prototypes */
int rint_ew(double value, long *nearest_int);
int rsam_dir(char *dirname, TRACE_HEADER *thead, double *RsamPeriod,
			 int num_rsam_periods, char *fullPathName);
int rsam_filename(char *dirName, TRACE_HEADER *thead, char *fileName, struct tm *sampleTime);
int rsam_file_create(char *file, char *outArch, char *DataFormat, int *formatIndex, 
					 TRACE_HEADER *RsamHead, struct tm *sampleTime);
int rsam_store(char *file, char *outArch, char *DataFormat, long value, 
			   struct tm *sampleTime);


#endif  /*_RSAMPUTAWAY_H */
