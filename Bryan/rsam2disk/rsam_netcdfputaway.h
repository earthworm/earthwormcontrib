/* rsamputaway.h: rsam header file */

#ifndef _RSAM_NETCDFPUTAWAY_H
#define _RSAM_NETCDFPUTAWAY_H

/* Function prototypes */
int nc_rsam_store(char *fname, long rValue, struct tm *sampleTime);
int nc_rsam_create (char *filename, TRACE_HEADER *rhead, struct tm *rsamTime);

#endif  /*_RSAM_NETCDFPUTAWAY_H */
