/* rsam2disk.h: rsam header file */

#ifndef _RSAM2DISK_H
#define _RSAM2DISK_H


#define MAXTXT	150

#define STN_LEN  	5 /* maximum length of stn labels */
#define CHA_LEN  	4 /* maximum length of cha labels */
#define NET_LEN  	4 /* maximum length of net labels */


/* Stucture to hold station information */
typedef struct scn_struct {
	char		stn[STN_LEN];
	char		cha[CHA_LEN];
	char		net[NET_LEN];
} SCNstruct;



#endif  /*_RSAM2DISK_H */
