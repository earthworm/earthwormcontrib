/* rsam_asciiputaway.h: ascii format putaway header file for rsam data */

#ifndef _RSAM_ASCIIPUTAWAY_H
#define _RSAM_ASCIIPUTAWAY_H

/* Function prototypes */
int ascii_rsam_create (char *filename, TRACE_HEADER *rhead, struct tm *rsamTime);
int ascii_rsam_store(char *fname, long rValue, struct tm *sampleTime);

#endif  /*_RSAM_ASCIIPUTAWAY_H */
