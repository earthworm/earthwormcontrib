This code requires the NETCDF libaries to compile. The netcdf package can be downloaded from www.unidata.ucar.edu/software/netcdf/.

The code orignally supported ONLY netcdf format as it wrote files on a Windows PC which were then read by a plotting code writting on a Solaris machine.
Code was later added to support the writing of plain text files. There are now two flavors of this: 
ASCII_FORMAT - Data are written as long integers with bintime followed by the RSAM value. The file begins with a header line. Successive lines each contain one day of data
ASCII_XY_FORMAT - Each line contains a date, time, and one RSAM value.

Note that this code was written in the time of the dinosaurs and accepts ONLY SCN (i.e., TYPE_TRACEBUF, not TYPE_TRACEBUF2) format data.