/**************************************************************************
**  rsamputaway.c                                                        **
**                                                                       **
**	Putaway routines for rsam data. Currently, the only supported output **
**	format is netcdf.                                                    **
**                                                                       **
**	Carol Bryan                                                          **
**	Cascades Volcano Observatory                                         **
**	3/28/01                                                              **
**************************************************************************/


/* system includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

/* earthworm includes */
#include <earthworm.h>
#include <trace_buf.h>
#include <time_ew.h>
#include <rsam2disk.h>
#include <rsam_netcdfputaway.h>
#include <rsam_asciiputaway.h>
#include <rsam_asciixyputaway.h>

#define	NETCDF_FORMAT	0
#define	ASCII_FORMAT	1
#define ASCII_XY_FORMAT 2

/*************************************************************************
**		rint_ew()                                                       **
**		nearest integer function                                        **
**		Solaris rint rounds down for .5, so we'll do the same           **
*************************************************************************/
int rint_ew(double value, long *nearest_int) {
	
#if defined (_WINNT)

		double	fraction;
		double	integer;
		
		fraction = modf(value, &integer);

		if (fraction <= 5)
			*nearest_int = (long)floor(value);
		else
			*nearest_int = (long)ceil(value);

#elif defined  (_SOLARIS)

		*nearest_int = (long)rint(value);

#else

		logit ("", "rint_ew: Can't determine my platform. Compile with either _INTEL or __SOLARIS set; exitting. \n");
		return EW_FAILURE;

#endif
	
		return EW_SUCCESS;
}

/*************************************************************************
**       rsam_dir()                                                     **
**       figure out name of the directory where the file will go        **
*************************************************************************/
int rsam_dir(char *dirname, TRACE_HEADER *thead, double *RsamPeriod,
			 int num_rsam_periods, char *fullPathName) {
	
	int	    i;
	int     index;           /* the index of the RSAM period */
	char    delim;	
	long    dperiod;         /* period for the data          */
	double  *rperiod;        /* desired rsam periods         */
	char    cperiod[MAXTXT]; /* rsam period in a char string */
	
#if defined (_WINNT)

	delim = '\\';

#elif defined  (_SOLARIS)

	delim = '/';

#else

		logit ("", "rsam_dir: Can't determine my platform. Compile with either _INTEL or __SOLARIS set; exitting. \n");
		return EW_FAILURE;

#endif
	
	index = -1;
	/* Assume that the data period should be an integral value */
	rint_ew((double) 1.0 / thead->samprate, &dperiod);
	rperiod = RsamPeriod;
	for (i = 0; i < num_rsam_periods; i++) {
		/* We'll assume a match is data period is within +-0.1 sec
			of the desired value */
		if (((double)dperiod >= (rperiod[i] - 0.1)) && ((double)dperiod <= 
			rperiod[i] + 0.1)) {
			index = i;
			break;
		}
	}

	/* if we didn't get a valid period, something has gone haywire */
	if (index == -1) {
		logit("", "rsam_dir: <%s.%s.%s> data <period: %d> not of a desired period \n",
				thead->sta, thead->chan, thead->net, dperiod);
		return EW_FAILURE;
	}
	
	/* full path to output directory will be 
	   OutDir/RsamPeriod[i]/station.componet.network	
	
	   if period is in minutes, convert to minutes and append Min to 
	   directory name, else append Sec */
		if (((int)rperiod[index] % 60) == 0)
			sprintf(cperiod, "%dMin", (int)(rperiod[index]/60));
		else
			sprintf(cperiod, "%lfSec", rperiod[index]);

		sprintf(fullPathName, "%s%c%s%c%s_%s_%s",
			dirname, delim, cperiod, delim, thead->sta,
			thead->chan, thead->net);
		
		return EW_SUCCESS;
}

/*************************************************************************
**		rsam_filename()                                                 **
**		figure out name of the RSAM data file                           **
**		filename of of the form dirName/year_month_stn_comp_net         **
*************************************************************************/
int rsam_filename(char *dirName, TRACE_HEADER *thead, char *fileName,
				  struct tm *sampleTime) {

		time_t		nearestBinSec = -1;
		char		delim;

#if defined (_WINNT)

	delim = '\\';

#elif defined  (_SOLARIS)

	delim = '/';

#else

		logit ("", "rsam_dir: Can't determine my platform. Compile with either _INTEL or __SOLARIS set; exitting. \n");
		return EW_FAILURE;

#endif
		/* We'll use the starttime of the bin as the sample time;
			However, we'll also round the bintime to the nearest second*/	
		rint_ew(thead->starttime, &nearestBinSec);

		gmtime_ew(&nearestBinSec, sampleTime);
	
		sprintf(fileName, "%s%c%04d_%02d_%s_%s_%s.RSM", dirName, delim,
			sampleTime->tm_year + 1900,sampleTime->tm_mon + 1,
			thead->sta, thead->chan, thead->net);
		
		return EW_SUCCESS;
}

/*************************************************************************
**		rsam_create_file()                                              **
**		entry point to RSAM storage, branches depend on output format   **
*************************************************************************/
int rsam_file_create(char *file, char *outArch, char *DataFormat, 
					 int *formatIndex, TRACE_HEADER *RsamHead, 
					 struct tm *sampleTime)
{

	fprintf(stderr, "rsam_file_create: outArch; %s \n", outArch);

	if ((file == NULL) || (DataFormat == NULL)) {
		logit ("", "rsam_file_create: invalid parameters passed in.\n");
		return EW_FAILURE;
	}

    if (strcmp (DataFormat,"netcdf") == 0){
        *formatIndex = NETCDF_FORMAT;
		if (nc_rsam_create (file, RsamHead, sampleTime) != EW_SUCCESS){
			logit("", "rsam_file_create: Call to nc_rsam_create failed!\n" );
			return EW_FAILURE;
		}
    }
    else if (strcmp (DataFormat,"ascii") == 0){
        *formatIndex = ASCII_FORMAT;
		if (ascii_rsam_create (file, RsamHead, sampleTime) != EW_SUCCESS){
			logit("", "rsam_file_create: Call to ascii_rsam_create failed!\n" );
			return EW_FAILURE;
		}
    }
	 else if (strcmp (DataFormat,"ascii_xy") == 0){
        *formatIndex = ASCII_XY_FORMAT;
		if (ascii_xy_rsam_create (file, RsamHead, sampleTime) != EW_SUCCESS){
			logit("", "rsam_file_create: Call to ascii_rsam_create failed!\n" );
			return EW_FAILURE;
		}
    }
    else {
        logit("","rsam_file_create: undefined DataFormat: %s\n", DataFormat);
        return EW_FAILURE;
    }

	return EW_SUCCESS;
}

/*************************************************************************
**		rsam_store()                                                    **
**		finally we put away the data, branches depend on output format  **
*************************************************************************/
int rsam_store(char *file, char *outArch, char *DataFormat, 
	long value, struct tm *sampleTime)
{

	if ((file == NULL) || (sampleTime == NULL)) {
		logit ("", "rsam_store: Invalid parameters passed in!\n");
		return EW_FAILURE;
	}

    if (strcmp (DataFormat,"netcdf") == 0){
		if (nc_rsam_store(file, value, sampleTime) != EW_SUCCESS) {
			logit("", "Call to nc_rsam_store failed. \n");
			return EW_FAILURE;
		}
	}
    else if (strcmp (DataFormat,"ascii") == 0){
		if (ascii_rsam_store(file, value, sampleTime) != EW_SUCCESS) {
			logit("", "Call to ascii_rsam_store failed. \n");
			fprintf(stderr, "ascii_rsam_store: failed ! \n");
			return EW_FAILURE;
		}
	}
	  else if (strcmp (DataFormat,"ascii_xy") == 0){
		if (ascii_xy_rsam_store(file, value, sampleTime) != EW_SUCCESS) {
			logit("", "Call to ascii_rsam_store failed. \n");
			fprintf(stderr, "ascii_rsam_store: failed ! \n");
			return EW_FAILURE;
		}
	}
	else {
		logit ("", "rsam_store: undefined DataFormat: %s!\n", DataFormat);
		return EW_FAILURE;
	}

	return EW_SUCCESS;
}
	


