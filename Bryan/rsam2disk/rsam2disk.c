/*----------------------------------------------------------------------**
**	rsam2disk.c                                                         **
**                                                                      **
**	gets data from the wave_ring and writes monthly RSAM files          **
**	currently only supported output format is netCDF                    **																		**
**	Carol Bryan                                                         **
**	USGS/CVO                                                            **
**	27 March 2001                                                       **
**                                                                      **
**	modeled after programs by M. Chadwick and L. Vidmar                 **
**                                                                      **
** 11/2/2001 cjb: added function to write ASCII files                   **
**----------------------------------------------------------------------**

/* system includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#if defined _SPARC
	#include <unistd.h>			/* for getpid() */
#elif defined _INTEL
	#include <process.h>		/* for _getpid() */
#else
	logit("e", "%s: Must have either SPARC or INTEL defined during compilation.\n", argv[0]);
	return(EW_FAILURE);
#endif 

/* earthworm includes */
#include <earthworm.h>
#include <kom.h>
#include <swap.h>
#include <transport.h>				/* shared memory and transport stuff	*/
#include <trace_buf.h>				/* define TRACE_HEADER structure		*/
#include <mem_circ_queue.h>			/* for queuing trace buf messages		*/

/* local includes */
#include <rsam2disk.h>
#include <rsamputaway.h>

static 	SHM_INFO InRing;			/* shared memory region to use for i/o	*/
static	SHM_INFO OutRing;			/* shared memory region to use for i/o    */


#define   MAXLOGO	2
MSG_LOGO  GetLogo[MAXLOGO];			/* array for requesting module, type, and instid */
short	  nLogo;

/* Message Queue */
#define   QUEUE_SIZE	500			/* how many msgs we can queue			*/
QUEUE	  MsgQueue;	

/* Thread Stuff */
#define			THREAD_STACK	8192
static unsigned tidProcessor;		/* Processor thread id					*/
static unsigned tidStacker;			/* Thread moving messages from InRing to MsgQueue */
int				MessageStackerStatus = 0;	/* 0 => Stacker thread OK. <0 => dead	*/
int				ProcessorStatus = 0;		/* 0 => Processor thread OK. <0 => dead	*/

/* SCNs to include and exclude */
#define			SCN_INCREMENT 10	/* how many more to allocate when we run out */
SCNstruct		*IncludeSCN = NULL;	/* which SCNs to get 					*/
int				num_IncludeSCN = 0;	/* how many SCNs we have				*/
int				max_IncludeSCN = 0;	/* how many are allocated so far		*/
SCNstruct		*ExcludeSCN = NULL;	/* which SCNs to leave behind			*/
int				num_ExcludeSCN = 0;	/* how many SCNs we are exluding 		*/
int				max_ExcludeSCN = 0;	/* how many are allocated so far 		*/
int				MaxSCN = 0;			/* max number of SCNs to store			*/

int				num_rsam;			/* How many RSAMs have been allocated	*/

/* Number of time periods of RSAM data */
#define			MAX_TIME_PERIODS 5	/* same number as in ew2rsam			*/
int				num_periods = 0;	/* number of Rsam Periods to store		*/
double			RsamPeriod[MAX_TIME_PERIODS];

/* Things to read from the configuration file */
static char		InRingName[MAX_RING_STR]; /* name of input transport ring 	*/
static char     OutRingName[MAX_RING_STR]; /* name of ouput transport ring  */
static char		MyModuleName[20];	/* name of this module					*/
static int		LogSwitch;			/* 0 if no logfile is to be written		*/
static long		HeartBeatInterval;	/* seconds between heartbeats			*/
static char		OutDir[MAXTXT];		/* top level directory for rsam files	*/
static char		DataFormat[MAXTXT]; /* only netcdf is supported				*/
static char		OutputArch[MAXTXT];	/* intel or sparc					*/


/* and other useful bits */
static char		MyProgName[256];	/* name of this program					*/
static int		FormatIndex;		/* format for writing out the RSAM data	*/

/* Things to look up in the earthworm.h tables with getutil.c functions	*/
static long		InKey;				/* key of transport ring for i/o		*/
static long		OutKey;				/* key of transport ring for i/o		*/
static unsigned char InstId;		/* installation ID						*/
static unsigned char MyModuleId;	/* Module ID for this program			*/
static unsigned char TypeHeartBeat;
static unsigned char TypeWaveform;
static unsigned char TypeError;
static unsigned char InstWild;
static unsigned char ModWild;

/* Error messages used by this module */
#define ERR_MISSMSG	0			/* message missed in transport ring		*/
#define ERR_TOOBIG	1			/* retrieved msg too large for buffer	*/
#define ERR_NOTRACK	2			/* msg retrieved; tracking limit exceeded */
#define ERR_QUEUE	3			/* trouble with MsgQueue operation		*/

static char	errText[256];		/* string for error messages			*/

long MyPid;					/* Process ID to be sent with heartbeats */

/* Function prototypes */
thr_ret	MessageStacker(void *);
thr_ret Processor(void *);

/*----------------------------------------------------------------------**
**	rsam2disk_config													**
**																		**
**	processes command file using kom.c functions; exits if				**
**	errors are encountered												**
**----------------------------------------------------------------------*/
int rsam2disk_config(char *configfile) {
#define NUM_COMMANDS	12

	char	init[NUM_COMMANDS];	/* init flags; one byte for each
									required command 	*/
	int		nmiss;				/* number of required commands
									that were missed	*/
	char	*com;		
	char	*str;
	int		nfiles;
	int		success;
	int		i;

	/* Initialize one init flag for each required command 	*/
	for (i = 0; i < NUM_COMMANDS; i++) 
		init[i] = 0;

	nLogo = 0;
	num_periods = 0;
	num_IncludeSCN = 0;
	num_ExcludeSCN = 0;

	/* Open the main configuration file */
	nfiles = k_open(configfile);
	if (nfiles == 0) {
		fprintf(stderr, "%s: Error opening command file <%s> \n",
			MyProgName, configfile);
		return(EW_FAILURE);
	}	

	/* Process all command files	*/
	while (nfiles > 0) {
		while (k_rd()) {	/* read next line from active file	*/
			com = k_str();	/* get the first token from line	*/
			
			/* Ignore blank lines and comments */
			if (!com) continue;
			if (com[0] == '#') continue;

			/* Open a nested configuration file */
			if (com[0] == '@') {
				success = nfiles + 1;
				nfiles = k_open(&com[1]);
				if (nfiles != success) {
					fprintf(stderr, "%s Error opening command file <%s> \n",
						MyProgName, &com[1]);
					return(EW_FAILURE);
				}
				continue;
			}

			/* Process anything else as a command */
	/*0*/	if (k_its("MyModuleId")) {
				if ((str = k_str()) != NULL) {
					strcpy(MyModuleName, str);
					init[0] = 1;
				}
			}
	/*1*/	else if (k_its("InRingName")) {
				if ((str = k_str()) != NULL) {
					strcpy(InRingName, str);
					init[1] = 1;
				}
			}
	/*2*/	else if (k_its ("OutRingName")) {
				if ((str = k_str ()) != NULL){
					strcpy (OutRingName, str);
					init[2] = 1;
				}
			}
	/*3*/	else if (k_its("HeartBeatInterval")) {
				HeartBeatInterval = k_long();
				init[3] = 1;
			}
	/*4*/	else if (k_its("LogFile")) {
				LogSwitch = k_int();
				init[4] = 1;
			}
	/*5*/	else if (k_its("GetRsamValuesFrom")) {
				if (nLogo >= MAXLOGO) {
					fprintf(stderr, "%s: Too many <GetMsgLogo> commands in <%s>;"
						" max = %d \n", MyProgName, configfile, (int)MAXLOGO);
					return(EW_FAILURE);
				}
				if (str = k_str()) {
					if (GetInst(str, &GetLogo[nLogo].instid) != 0) {
						fprintf(stderr, "%s: Invalid installation name <%s> in"
							"<GetRsamValuesFrom> command \n", MyProgName, str);
						return(EW_FAILURE);	
					}
				}
				if (str = k_str()) {
					if (GetModId(str, &GetLogo[nLogo].mod) != 0) {
						fprintf(stderr, "%s: Invalid module name <%s> in"
							"<GetRsamValuesFrom> command \n", MyProgName, str);
						return(EW_FAILURE);	
					}
				}
				/* Always fetch waveform messages */
				if (GetType("TYPE_TRACEBUF", &GetLogo[nLogo].type) != 0) {
						fprintf(stderr, "%s: Invalid message type <%s> in"
							"<GetRsamValuesFrom> command \n", MyProgName, str);
						return(EW_FAILURE);	
				}
				nLogo++;
				init[5] = 1;
			}

			/* Enter SCNs to be included */
	/*6*/	else if (k_its("IncludeSCN")) {
				if (num_IncludeSCN >= max_IncludeSCN) {
					/* Need to allocate more */
					max_IncludeSCN = max_IncludeSCN + SCN_INCREMENT;
					if ((IncludeSCN = (SCNstruct *) realloc (IncludeSCN,
						(max_IncludeSCN * sizeof(SCNstruct)))) == NULL) {
						fprintf(stderr, "%s: Call to realloc failed. \n", MyProgName);
						return(EW_FAILURE);
					}
				}
				str = k_str();
				if ((str != NULL) && (strlen(str) < (size_t)STN_LEN))
					strcpy(IncludeSCN[num_IncludeSCN].stn, str);
				str = k_str();
				if ((str != NULL) && (strlen(str) < (size_t)CHA_LEN))
					strcpy(IncludeSCN[num_IncludeSCN].cha, str);
				str = k_str();
				if ((str != NULL) && (strlen(str) < (size_t)NET_LEN))
					strcpy(IncludeSCN[num_IncludeSCN].net, str);

				num_IncludeSCN++;
				init[6] = 1;
			}
			/* Enter SCN to be excluded */
	/*NR*/	else if (k_its("ExcludeSCN")) {
				if (num_ExcludeSCN >= max_ExcludeSCN) {
					/* Need to allocate more */
					max_ExcludeSCN = max_ExcludeSCN + SCN_INCREMENT;
					if ((ExcludeSCN = (SCNstruct *) realloc (ExcludeSCN,
						(max_ExcludeSCN * sizeof(SCNstruct)))) == NULL) {
						fprintf(stderr, "%s: Call to realloc failed. \n", MyProgName);
						return(EW_FAILURE);
					}
				}
				str = k_str();
				if ((str != NULL) && (strlen(str) < (size_t)STN_LEN))
					strcpy(ExcludeSCN[num_ExcludeSCN].stn, str);
				str = k_str();
				if ((str != NULL) && (strlen(str) < (size_t)CHA_LEN))
					strcpy(ExcludeSCN[num_ExcludeSCN].cha, str);
				str = k_str();
				if ((str != NULL) && (strlen(str) < (size_t)NET_LEN))
					strcpy(ExcludeSCN[num_ExcludeSCN].net, str);

				num_ExcludeSCN++;
			}
	/*7*/	else if (k_its("MaxSCN")) {
				MaxSCN = k_int();
				init[7] = 1;
			}
	/*8*/	else if (k_its ("RsamPeriod")) 
			{
				if (num_periods >= MAX_TIME_PERIODS)
				{
					fprintf (stderr, 
						"%s: Maximum number of <RsamPeriod> commands exceeded; Exiting!\n", MyProgName);
					return EW_FAILURE;
				}
				
				RsamPeriod[num_periods] = k_val ();
				num_periods++;
				
				init[8] = 1;
			}

	/*9*/	else if(k_its("DataFormat")) {
                if ((str = k_str()) != NULL) {  
                    strcpy(DataFormat, str);
					init[9] = 1;
				}
            }
  /*10*/    else if( k_its("OutDir") ) 
            {
                if ((str = k_str()) != NULL) {  
                    strcpy(OutDir, str);
					init[10] = 1;
				}
            }
  /*11*/	else if(k_its("OutputArch")) {
                if ((str = k_str()) != NULL)  
                    strcpy (OutputArch, str);

				/* check validity */
				if ((strcmp (OutputArch, "intel") != 0) &&
				    (strcmp (OutputArch, "sparc") != 0)) {
						fprintf (stderr, "%s: Invalid OutputArch %s\n", MyProgName, OutputArch);
						return EW_FAILURE;
				}
                init[11] = 1;
            }

			/* Unknown command */
			else {
				fprintf(stderr, "%s: <%s> Unknown command in <%s>. \n", 
					MyProgName, com, configfile);
				continue;
			}

			/* See if ther were any errors in processing the command file */
			if (k_err()) {
				fprintf(stderr, "%s: Bad <%s> command in <%s>. \n", MyProgName,
					com, configfile);
				return(EW_FAILURE);
			}
		}	/* end of while k_rd() */

		nfiles = k_close();
	}	/* end of while nfiles */

	/* Check init flags for missed commands */
	nmiss = 0;
	for (i = 0; i < NUM_COMMANDS; i++) 
		if (!init[i]) nmiss++;

	if (nmiss) {
		fprintf(stderr, "%s: ERROR, no ");
		if (!init[0]) fprintf(stderr, "<MyModuleId> "     );
		if (!init[1]) fprintf(stderr, "<InRingName> "     );	
		if (!init[2]) fprintf(stderr, "<OutRingName> "     );
		if (!init[3]) fprintf(stderr, "<HeartBeatInterval> "     );
		if (!init[4]) fprintf(stderr, "<LogFile> "     );
		if (!init[5]) fprintf(stderr, "<GetRsamValuesFrom> "     );
		if (!init[6]) fprintf(stderr, "<IncludeSCN> "     );
		if (!init[7]) fprintf(stderr, "<MaxSCN> "     );
		if (!init[8]) fprintf(stderr, "<RsamPeriod> "     );
		if (!init[9]) fprintf(stderr, "<DataFormat> "     );
		if (!init[10]) fprintf(stderr, "<OutDir> "     );
		if (!init[11]) fprintf(stderr, "<OutputArch> "     );

		fprintf(stderr, "command(s) in <%s>. \n", configfile);
		return(EW_FAILURE);
	}

	return(EW_SUCCESS);
}

/*----------------------------------------------------------------------**
**	rsam2disk_lookup													**
**																		**
**	Look up information in the earthworm.h tables						**
**----------------------------------------------------------------------*/
static int rsam2disk_lookup(void) {
		
	/* Lookup keys to shared memory region */
	if ((InKey = GetKey(InRingName)) == -1) {
		fprintf(stderr, "%s: Invalid ring name <%s>. \n", MyProgName, InRingName);
		return(EW_FAILURE);
	}
	if ((OutKey = GetKey(OutRingName)) == -1) {
		fprintf (stderr, "%s: Invalid ring name <%s>.\n", MyProgName, OutRingName);
		return EW_FAILURE;
	}


	/* Look up installations of interest */
	if (GetLocalInst(&InstId) != 0) {
		fprintf(stderr, "%s: Error getting local installation. \n", MyProgName);
		return(EW_FAILURE);
	}

	if (GetInst("INST_WILDCARD", &InstWild) != 0) {
		fprintf(stderr, "%s: Error getting wildcard installation ID. \n", MyProgName);
		return(EW_FAILURE);
	}

	/* Look up modules of interest */
	if (GetModId(MyModuleName, &MyModuleId) != 0) {
		fprintf(stderr, "%s: Invalid module name <%s>. \n", MyProgName, MyModuleName);
		return(EW_FAILURE);
	}
	if (GetModId("MOD_WILDCARD", &ModWild) != 0) {
		fprintf(stderr, "%s: Invalid module name <MOD_WILDCARD>. \n", MyProgName);
		return(EW_FAILURE);
	}

	/* Look up message types of interest */
	if (GetType("TYPE_HEARTBEAT", &TypeHeartBeat) != 0) {
		fprintf(stderr, "%s: Invalid message type <TYPE_HEARTBEAT>. \n", MyProgName);
		return(EW_FAILURE);
	}
	if (GetType("TYPE_ERROR", &TypeError) != 0) {
		fprintf(stderr, "%s: Invalid message type <TYPE_ERROR>. \n", MyProgName);
		return(EW_FAILURE);
	}
	if (GetType("TYPE_TRACEBUF", &TypeWaveform) != 0) {
		fprintf(stderr, "%s: Invalid message type <TYPE_TRACEBUF>. \n", MyProgName);
		return(EW_FAILURE);
	}

	return(EW_SUCCESS);
}

/*----------------------------------------------------------------------**
**	rsam2disk_status													**
**																		**
**	Builds a heartbeart or error message and puts it in the				**
**	shared memory. Writes errors to log file and to screen.				**
**																		**
**	from Vidmar, ew2rsam												**
**----------------------------------------------------------------------*/
static void rsam2disk_status(unsigned char type, short ierr, char *note) {

	MSG_LOGO	logo;
	char		msg[256];
	long		size;
	long		t;

	/* Build the message */
	logo.instid = InstId;
	logo.mod = MyModuleId;
	logo.type = type;

	time(&t);

	if (type == TypeHeartBeat) 
		sprintf(msg, "%ld %ld\n\0", t, MyPid);

	if (type == TypeError) {
		sprintf(msg, "%ld %hd %s\n\0", t, ierr, note);
		logit("et", "%s(%s): %s\n", MyProgName, MyModuleName, note);
	}

	size = strlen(msg); 	/* Don't include the null byte in the message */

	/* Write out the message to shared memory */
	/******************************************/
	if (tport_putmsg(&OutRing, &logo, size, msg) != PUT_OK) {
		if (type == TypeHeartBeat) 
			logit("et", "%s(%s): Error sending heartbeat. \n", MyProgName, MyModuleName);

		else if (type == TypeError)
			logit("et", "%s(%s): Error sending error: %d. \n", MyProgName, MyModuleName, ierr);
	}
}

/*----------------------------------------------------------------------**
**	matchSCN()															**
**																		**
**	Set retind to the index of the SCN matching stn.cha.net				**
**	found in the SCNlist array. Otherwise, set retind to -1.			**
**																		**
**	from Vidmar, ew2rsam												**
**----------------------------------------------------------------------*/
static int matchSCN(char *stn, char *cha, char *net, SCNstruct *Include, 
	int numInclude, SCNstruct *Exclude, int numExclude, int *retind) 
{
	int i, j;
	int stnWild, chaWild, netWild;
	int stnMatch, chaMatch, netMatch;
	
	stnMatch = chaMatch = netMatch = 0;

	if ((stn == NULL) || (cha == NULL) || (net == NULL) ||
		(Include == NULL) || (numInclude < 0)) {
		logit("et", "%s: Invalid parameters to check_scn_logo. \n");
		return(EW_FAILURE);
	}

	*retind = -1;

	/* Is SCN in the include list? */
	for (i = 0; i < numInclude; i++) {
		stnWild = chaWild = netWild = 0;
		stnMatch = chaMatch = netMatch = 0;

		/* Any wild cards? */
		if (strcmp(Include[i].stn, "*") == 0)
			stnWild = 1;
		if (strcmp(Include[i].cha, "*") == 0)
			chaWild = 1;
		if (strcmp(Include[i].net, "*") == 0)
			netWild = 1;

		/* Try to match explicitly */
		if (strcmp(stn, Include[i].stn) == 0)
			stnMatch = 1;
		if (strcmp(cha, Include[i].cha) == 0)
			chaMatch = 1;
		if (strcmp(net, Include[i].net) == 0)
			netMatch = 1;

		if ((stnWild == 1) || (stnMatch == 1))
			stnMatch = 1;
		if ((chaWild == 1) || (chaMatch == 1))
			chaMatch = 1;
		if ((netWild == 1) || (netMatch == 1))
			netMatch = 1;

		/* If stn, cha, and net match, set SCN was found in the 
		** Include list - now we have the check the Exclude list */
		if (stnMatch + chaMatch + netMatch == 3) {
			/* Is SCN in the exclude list? */
			for (j = 0; i < numExclude; j++) {
				stnWild = chaWild = netWild = 0;
				stnMatch = chaMatch = netMatch = 0;

				/* Any wild cards? */
				if (strcmp(Exclude[j].stn, "*") == 0)
					stnWild = 1;
				if (strcmp(Exclude[j].cha, "*") == 0)
					chaWild = 1;
				if (strcmp(Exclude[j].net, "*") == 0)
					netWild = 1;

				/* Try to match explicitly */
				if (strcmp(stn, Exclude[j].stn) == 0)
					stnMatch = 1;
				if (strcmp(cha, Exclude[j].cha) == 0)
					chaMatch = 1;
				if (strcmp(net, Exclude[j].net) == 0)
					netMatch = 1;
		
				if ((stnWild == 1) || (stnMatch == 1))
					stnMatch = 1;
				if ((chaWild == 1) || (chaMatch == 1))
					chaMatch = 1;
				if ((netWild == 1) || (netMatch == 1))
					netMatch = 1;


				/* If stn, cha, and net match, set SCN was found
				** in the Exclude list; Throw it back. Return -1 */
				if (stnMatch + chaMatch + netMatch == 3) {
					*retind = -1;
					return(EW_SUCCESS);
				}
			}	/* end of loop over Exclude list */

			/* SCN was not in the Exclude list; Return i */
			*retind = i;
			return(EW_SUCCESS);

		}	/* if SCN was found in the Include list */
	}
	
	return(EW_SUCCESS);
}
/*----------------------------------------------------------------------**
**	matchTP()										**
**												**
**	Set retind to the index of the matching rsam period			**
**	found in the RsamPeriod array. Otherwise, set retind to -1.		**
**												**
**----------------------------------------------------------------------*/
int matchTP(double samprate, int *retind) {
	int     i;
	long    data_period;

	if (samprate <= 0.0) {
		logit("et", "%s: Invalid sampling rate to check_time_period match. \n");
		return(EW_FAILURE);
	}

	*retind = -1;
	/* rounding is a bitch, so assume data period is integral */
	rint_ew((double) 1.0 / samprate, &data_period);

	for (i = 0; i < num_periods; i++) {
		/* We'll assume a match is data period is within +-0.1 sec
			of the desired value */
		if (((double)data_period >= (RsamPeriod[i] - 0.1)) && ((double)data_period <= 
			RsamPeriod[i] + 0.1)) {
			*retind = i;
			return EW_SUCCESS;
		}
	}
	return EW_SUCCESS;
}

/*----------------------------------------------------------------------**
**	MessageStacker														**
**																		**
**	Message Stacking Thread 											**
**	Move message from transport to memory queue.						**
**																		**	
**	after Vidmar, ew2rsam												**
**----------------------------------------------------------------------*/
thr_ret MessageStacker(void *dummy) {

	char			*msg;		/* "raw" retrieved message		*/
	int			res = -1;	/* return for SCN match			*/
	int			rep = -1;	/* return for time period match	*/
	long			recSize;	/* size of retrieved message	*/
	MSG_LOGO		recLogo;	/* logo of retrieved message	*/
	TRACE_HEADER	*WaveHeader;	
	int				ret;		/* return from enqueue			*/

	/* Allocate space for the input/output message */
	if ((msg = (char *) malloc(MAX_BYTES_PER_EQ)) == (char *)NULL) {
		logit("e", "%s: Error allocating msg. \n", MyProgName);
		MessageStackerStatus = -1; /* file a complaint to the main thread */
		KillSelfThread();	/* main thread will restart us */
	}

	WaveHeader = (TRACE_HEADER *)msg;

	/* Tell the main thread that we're OK */
	MessageStackerStatus = 0;
	
	/* Start service loop, picking up trigger messages */
	while(1) {
		/* Get a message from the transport ring */

		res = tport_getmsg(&InRing, GetLogo, nLogo, &recLogo, &recSize, 
			msg, MAX_BYTES_PER_EQ - 1);

		if (res == GET_NONE) {
			sleep_ew(100);
			continue;
		}	/* wait if no messages available */

		/* Check return code; report errors */
		if (res != GET_OK) {
			if (res == GET_TOOBIG) {
				sprintf(errText, "msg[%ld] i%d m%d t%d too long for target",
					recSize, (int)recLogo.instid, (int)recLogo.mod,
					(int)recLogo.type);
				rsam2disk_status(TypeError, ERR_TOOBIG, errText);
				continue;
			}
			else if (res == GET_MISS) {
				sprintf(errText, "missed msg(s) i%d m%d t%d in %s",
					(int)recLogo.instid, (int)recLogo.mod,
					(int)recLogo.type, InRingName);
				rsam2disk_status(TypeError, ERR_MISSMSG, errText);
			}
			else if (res == GET_NOTRACK) {
				sprintf(errText, "no tracking for logo i%d m%d t%d in %s",
					(int)recLogo.instid, (int)recLogo.mod,
					(int)recLogo.type, InRingName);
				rsam2disk_status(TypeError, ERR_NOTRACK, errText);
			}
		}

		/* If necessary, swap bytes in the wave message */
		if (WaveMsgMakeLocal(WaveHeader) < 0) {
			logit("et", "%s(%s): Unknown waveform type. \n",
				MyProgName, MyModuleName);
			continue;
		}

		/* ew2rsam adds extra letters (_R#) to channel name; 
			truncate this mess */
		if (strlen(WaveHeader->chan) > 3)
			WaveHeader->chan[3] = '\0';

		/* Check to see if msg's SCN code is desired */
		if (matchSCN(WaveHeader->sta, WaveHeader->chan, WaveHeader->net,
				IncludeSCN, num_IncludeSCN, ExcludeSCN, num_ExcludeSCN,
				&res) != EW_SUCCESS) {
			logit("et", "%s(%s): Call to matchSCN failed. \n", MyProgName, 
					MyModuleName);
			MessageStackerStatus = -1; /* file a complaint to the main thread */
			KillSelfThread();	/* main thread will restart us */;
		}

		/* If the message matched one of the desired SCNs, check to see if it
			matches one of the desired time periods */
		if (res >=0) {	
			if (matchTP(WaveHeader->samprate, &rep) != EW_SUCCESS) {
				logit("et", "%s(%s): Call to matchTP failed. \n", MyProgName, 
					MyModuleName);
				MessageStackerStatus = -1; /* file a complaint to the main thread */
				KillSelfThread();	/* main thread will restart us */;
			}		
		}

		/* If the message matched one of the desired SCNs and desired RsamPeriods, queue it */
		if ((res >= 0) && (rep >= 0)) {
			/* Queue retrieved message (res == GET_OK, GET_MISS, GET_NOTRACK) */
			RequestMutex();
			/* Put it into the queue */
			ret = enqueue(&MsgQueue, msg, recSize, recLogo);
			ReleaseMutex_ew();

			if (ret != 0) {
				if (ret == -2) {	/* Serious: quit */
					sprintf(errText, "Internal queue error. Terminating. \n");
					rsam2disk_status(TypeError, ERR_QUEUE, errText);
					MessageStackerStatus = -1; /* file a complaint to the main thread */
					KillSelfThread();	/* main thread will restart us */
				}
				if (ret == -1) {
					sprintf(errText, "Queue cannot allocate memory. Lost message. \n");
					rsam2disk_status(TypeError, ERR_QUEUE, errText);
					continue;
				}
				if (ret == -3) {
					sprintf(errText, "Queue full. Message lost. \n");
					rsam2disk_status(TypeError, ERR_QUEUE, errText);
					continue;
				}
			}	/* problem from enqueue */
		} /* if we have a desired message */
	} /* end of while */
}

/*----------------------------------------------------------------------**
**	Processor															**
**																		**
**	Message Processing Thread 											**
**																		**
**----------------------------------------------------------------------*/
thr_ret Processor(void *dummy) {
	
	int				ret;

	int				gotMsg;
	MSG_LOGO		recLogo;			/* logo of retrieved message	*/
	TRACE_HEADER	*RsamHeader;		/* ptr to Waveform data buffer	*/
	long			*RsamLong;			/* if waveform data are longs	*/
	short			*RsamShort;			/* if waveform data are shorts	*/
	char			*RsamBuf;			/* string to hold waveform msgs	*/
	long			RsamBufLen;			/* length of WaveBuf			*/
	struct tm		dataTime;			/* time of this RSAM sample		*/
	long			rsamValue = 0l;		/* value of this RSAM sample	*/

	char			fullDirName[MAX_DIR_LEN];	/* full path to output directory */
	char			filename[MAX_DIR_LEN];		/* full path to output file		 */
	struct stat 	buf;

	/* Allocate the Rsam trace buffer; recall that Rsam messages contain
		only one data point */
	RsamBufLen = sizeof(long) + sizeof(TRACE_HEADER);
	RsamBuf = (char *) malloc((size_t) RsamBufLen);

	if (RsamBuf == NULL) {
		logit("et", "%s(%s): Cannot allocate waveform buffer \n", 
			MyProgName, MyModuleName);
		ProcessorStatus = -1;
		KillSelfThread();
	}

	/* Pointer to header and data portions of Rsam message */
	RsamHeader = (TRACE_HEADER *)RsamBuf;
	RsamLong = (long *)(RsamBuf + sizeof(TRACE_HEADER));
	RsamShort = (short *)(RsamBuf + sizeof(TRACE_HEADER));

	while (1) {
		gotMsg = FALSE;
		while (gotMsg == FALSE) {
			RequestMutex();
			ret = dequeue(&MsgQueue, RsamBuf, &RsamBufLen, &recLogo);
			ReleaseMutex_ew();


			if (ret < 0) 
				sleep_ew(1000);	/*empty queue */
			else 
				gotMsg = TRUE;

		} /* while no messages are dequeued */


		/* sort out name of directory for this data if necessary */
		if (rsam_dir(OutDir, RsamHeader, RsamPeriod, num_periods, fullDirName) != EW_SUCCESS) {
			logit("et", "Processor continuing with next message");
			continue;
		}

		/* create the directory */
		if (RecursiveCreateDir(fullDirName) != EW_SUCCESS) {
			logit("et", "RecursiveCreateDir: could not create directory %s \n",
				fullDirName);
			continue;
		}

		
		memset(&dataTime, 0, (size_t)sizeof(struct tm));
		strcpy(filename, "");
		/* figure out the filename where the RSAM data will be stored */
		if (rsam_filename(fullDirName, RsamHeader, filename, &dataTime) != EW_SUCCESS) {
			logit("et", "Processor continuing with next message");
			continue;
		}


		/* does the RSAM file exist? if not, create the RSAM and initialize it */
		if (stat(filename, &buf) != 0) {
			if (errno == ENOENT) {
				if (rsam_file_create(filename, OutputArch, DataFormat, &FormatIndex, 
					RsamHeader, &dataTime) != EW_SUCCESS) {
					logit("et", "rsam_file_create: could not create RSAM file %s \n",
					filename);
					continue;
				}
			}
			else	{
				logit ("e", "Processor: Cannot stat %s - %s \n", 
							filename, strerror (errno));
				ProcessorStatus = -1;
				KillSelfThread();
			}
		}
		else

		/* get the RSAM value */
		if ((strcmp (RsamHeader->datatype, "i2") == 0) ||
					(strcmp (RsamHeader->datatype, "s2") == 0)){
			rsamValue = (long)(*RsamShort);
		}
		else if ((strcmp (RsamHeader->datatype, "i4") == 0) ||
				(strcmp (RsamHeader->datatype, "s4") == 0)) {
			rsamValue = (long)(*RsamLong);
		}
		else {
			logit("et", "%s(Processor): Not a valid data type. \n",
				MyProgName, RsamHeader->datatype);
			continue;
		}

		/* put the data away */
		fprintf(stderr, "storing data in file %s \n", filename);
		if (rsam_store(filename, OutputArch, DataFormat, rsamValue, 
				&dataTime) != EW_SUCCESS) {
			logit("et", "rsam_store: could not put data in RSAM file %s \n", filename);
			continue;
		}
		
		ProcessorStatus = 0;

	} /* while 1*/
}


/*----------------------------------------------------------------------**
**	rsam2disk.c														**
**----------------------------------------------------------------------*/
int main(int argc, char **argv) {
	
	long		timeNow;			/* current time						*/
	long 		timeLastHeartbeat;	/* time last heartbeat was sent		*/
	long 		recSize;			/* size of retrieved message		*/
	MSG_LOGO	recLogo;			/* logo of retrieved message		*/
	char		*flushmsg;

	/* Check command line arguments */
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <config_file> \n", argv[0]);
		return(EW_FAILURE);
	}

	/* Check to make sure that either SPARC or INTEL was defined during compilation */
#if defined _SPARC
	;
#elif defined _INTEL
	;
#else
	logit("e", "%s: Must have either SPARC or INTEL defined during compilation.\n", argv[0]);
	return(EW_FAILURE);
#endif

	/* Get Program Name */
	if (get_prog_name(argv[0], MyProgName) != EW_SUCCESS) {
		fprintf(stderr, "%s: Call to get_prog_name failed. \n", argv[0]);
		return(EW_FAILURE);
	}

	/* Read the configuration file */
	if (rsam2disk_config(argv[1]) != EW_SUCCESS) {
		fprintf(stderr, "%s: Call to %s_config failed. \n", MyProgName, MyProgName);
		return(EW_FAILURE);
	}	

	/* Look up stuff from earthworm.h tables */
	if (rsam2disk_lookup() != EW_SUCCESS) {
		fprintf(stderr, "%s(%s): Call to %s_lookup failed. \n",
				MyProgName, MyModuleName, MyProgName);
		return(EW_FAILURE);
	}

	/* Initialize name of log file and open it */
	logit_init(argv[1], (short)MyModuleId, 256, LogSwitch);
	logit("", "%s(%s): Read command file <%s> \n", MyProgName, MyModuleName, argv[1]);

	/* Get the process ID */
	if ((MyPid = (long) getpid()) == -1) {
		logit("e", "%s(%s): Call to getpid failed. Exiting. \n", MyProgName, MyModuleName);
		return(EW_FAILURE);
	}

	/* Attach to Input Shared Memory Ring */
	/*******************************************/
	tport_attach(&InRing, InKey);
	logit("", "%s(%s): Attached to public memory region %s: %d \n",
		MyProgName, MyModuleName, InRingName, InKey);

	/* Attach to Output shared memory ring */
	/*******************************************/
	tport_attach (&OutRing, OutKey);
	logit ("", "%s(%s): Attached to public memory region %s: %d\n", 
	          MyProgName, MyModuleName, OutRingName, OutKey);


	/* Issue a heartbeat during first pass through main loop */
	/*********************************************************/
	timeLastHeartbeat = time(&timeNow) - HeartBeatInterval - 1;

	/* Flush the incoming transport ring */
	/************************************/
	if ((flushmsg = (char *) malloc(MAX_BYTES_PER_EQ)) == NULL) {
		logit("e", "%s: Can't allocate flushmsg \n", MyProgName);
		return(EW_FAILURE);
	}

	while (tport_getmsg(&InRing, GetLogo, nLogo, &recLogo, &recSize,
		flushmsg, (MAX_BYTES_PER_EQ - 1)) != GET_NONE) 
		;

	/* Create MsgQueue mutex */
	CreateMutex_ew();

	/* Allocate the message queue */
	initqueue(&MsgQueue, QUEUE_SIZE, MAX_BYTES_PER_EQ);

	/* Start message stacking thread; Reads msgs from the InRing
	** and puts them into the Queue	*/
	if (StartThread(MessageStacker, (unsigned) THREAD_STACK, &tidStacker) == -1) {
		logit("e", "%s: Error starting MessageStacker thread.\n", MyProgName);
		tport_detach(&InRing);
		tport_detach (&OutRing);
		return(EW_FAILURE);
	}

	MessageStackerStatus = 0;

	/* Start decimator thread; Reads messages from the Queue 
	** and processes them */
	if (StartThread(Processor, (unsigned) THREAD_STACK, &tidProcessor) == -1) {
		logit("e", "%s: Error starting processor thread. \n", MyProgName);
		tport_detach(&InRing);
		tport_detach (&OutRing);
		return(EW_FAILURE);
	}

	ProcessorStatus = 0;
		
/*---------------------------- setup done; start main loop --------------------------*/

	while (tport_getflag(&InRing) != TERMINATE) {
		/* Send heartbeat */
		if (time(&timeNow) - timeLastHeartbeat >= HeartBeatInterval) {
			timeLastHeartbeat = timeNow;
			rsam2disk_status(TypeHeartBeat, 0, "");
		}

		/* Check on our threads */
		if (MessageStackerStatus < 0) {
			logit("et", "%s: MessageStacker thread died. \n", MyProgName);
			return(EW_FAILURE);
		}
		if (ProcessorStatus < 0) {
			logit("et", "%s: ProcessorStacker thread died. \n", MyProgName);
			return(EW_FAILURE);
		}


		sleep_ew(1000);
	} /* wait until TERMINATE is raised */

	/* Termination has been requested */
	tport_detach(&InRing);
	tport_detach (&OutRing);
	logit("t", "%s: Termination requested. \n", MyProgName);
	return(EW_SUCCESS);
}

