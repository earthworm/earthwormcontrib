/*************************************************************************
**	rsam_asciiputaway.c                                                 **
**                                                                      **
**	ascii  format putaway routines for rsam data.                       **
**                                                                      **
**	Carol Bryan                                                         **
**	Cascades Volcano Observatory                                        **
**	10/30/01                                                            **
*************************************************************************/

/* system includes */
#include <stdio.h>
#include <io.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <fcntl.h>  /* for open flags */

/* earthworm/glowworm includes */
#include <earthworm.h>
#include <trace_buf.h>
#include <time_ew.h>
#include <rsamputaway.h>
#include <rsam_asciiputaway.h>

/* package includes */
#include <netcdf.h>

/* rsam includes */
#include <rsam2disk.h>

#define ASCII_FILL_LONG -2147483647L
#define SEC_PER_DAY 86400	/* number of seconds in one day */
#define MAX_CHAR 100000

/*************************************************************************
**		ascii_rsam_create()                                             **
**		entry point to RSAM storage, ascii output format                **
**                                                                      **
**		Data values will be prefilled with a value of NC_LONG           **
/************************************************************************/
int ascii_rsam_create (char *filename, TRACE_HEADER *rhead, 
					struct tm *rsamTime) 
{
	FILE      *ofd;         /* file pointer for output              */
	int       daycount;     /* loop counter for day of month        */
	int       sampcount;    /* loop counter for sample in day       */
	int       nbins = 0;    /* number of RSAM values per day        */
	int	      ndays;        /* number of days of data in this file  */
	int       dpm[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	char      *month[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
						"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	char	  dateString[12]; /* string for writing date in to file */
	char      title[] = {"RSAM"};
	struct tm fileStart;    /* start of month                       */

	if ((ofd = fopen(filename, "a+")) != NULL) {
		/* determine number of days in this month                 */
		ndays = dpm[rsamTime->tm_mon];
		/* if it is February of a leap year, we need an extra day */
		if ((((rsamTime->tm_year + 1900) % 4) == 0) && (rsamTime->tm_mon == 1))
			ndays++;

		/* calculate number of samples per day to be written to this file */
		rint_ew((double)SEC_PER_DAY * rhead->samprate, &nbins);

		/* determine start time of file (first day of the month) */
		memcpy(&fileStart, rsamTime, sizeof(struct tm));
		fileStart.tm_mday = 1;
		fileStart.tm_hour = fileStart.tm_hour = fileStart.tm_sec = 0;		

		/*  write header information to file: tell the file this is RSAM data and write in station, 
		    channel, and network information as well as file start time (yyyymmdd hh:mm:ss)
		    and sampling rate */
		if (fprintf(ofd, "%5s %9s %9s %9s %04d%02d%02d %02d:%02d:%02d %10lf\n",
			title, rhead->sta, rhead->chan, rhead->net, fileStart.tm_year + 1900,
			fileStart.tm_mon + 1, fileStart.tm_mday, fileStart.tm_hour, fileStart.tm_min,
			fileStart.tm_sec, rhead->samprate) <= 0) {
			logit("", "ascii_rsam_create: unable to write header information to file %s\n", 
				filename);
			return EW_FAILURE;
		}
		
		/* initialize rsam values and corresponding bintime variable;
			store values as one line per day */
		for (daycount = 0; daycount < ndays; daycount++) {
			sprintf(dateString, "%04d-%3s-%02d", 
				fileStart.tm_year + 1900, month[fileStart.tm_mon], daycount + 1);
			fprintf(ofd, "%s", dateString);
			/* write one value each for rsam value and bintime */
			for (sampcount = 0; sampcount < nbins; sampcount++)
				fprintf(ofd, " %016ld %016ld", ASCII_FILL_LONG, ASCII_FILL_LONG);
			fprintf(ofd, "\n");
		}


		/* close 'er up */
		if (fclose(ofd)) {
			logit("", "fclose: file %s did not close cleanly \n", filename);
			return EW_FAILURE;
		}

	}
	else {
		/* assume the file already exists */
		return EW_SUCCESS;
	}

	return EW_SUCCESS;
}

/*************************************************************************
**		ascii_rsam_store()                                              **
**		put the data away in ascii format                               **
**                                                                      **
**		Data are written as long integers with bintime followed by the  **
**      RSAM value;  The file begins with a header line; successive     **
**      lines each contain one day of data                              **
**                                                                      **
**      MAX_CHAR needs to be at least max_no_samples_per_day * 2 *      **
**      (sizeof(long) + 1); 2*sizeof(long) for rsam value and bintime;  **
**      + 1 for blanks separating values; = 1440 (for 1-Min data)       **
**      *2(4*8 + 1) = 95040                                             **
*************************************************************************/
int ascii_rsam_store(char *fname, long rValue, struct tm *sampleTime) 
{


	FILE    *ofd;           /* file pointer for output              */
	char    hbuf[MAX_CHAR]; /* buffer for storing header string     */
	char    *buf;           /* pointer into buffer                  */
	int     narg =0;        /* number of arguments read from file   */
	int     year  = 0;      /* year from file header                */
	int     month = 0;      /* month from file header               */
	double  samprate = 0.0; /* data sampling rate                   */

	int	    ndays;          /* number of days of data in this file  */
	int     dpm[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	char    *cmonth[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
						"Jul", "Aug", "Sep", "Oct", "Nov", "Dec" }; 
	char	dateString[12]; /* string for writing date in to file   */

	int     nbins = 0;      /* number of RSAM values per day        */
	int     day_no = 0;		/* day of month for data                */
	int     day_index = 0;  /* index of sample for this day         */

	struct tm fileStart;    /* start of file                        */
	long    startTime;      /* start time of this file (sec since 1/1/1970) */
	long    bintime;        /* time of this sample (sec since 1/1/1970)     */
	long    storedTime;
	int     index = -1;     /* index of sample in this file         */
	long    storedValue;    /* fill value or previous value         */
	int     daycount;       /* loop counter for day                 */
	long    file_position;  /* location in file for this day's data */
	long    dataArray[2880]; /* max of 1440 data samples per day; one 
							    storage space each for RSAM value and
							    bintime                             */
	int     count, i;         /* loop counters                      */
	int     j;
	char    *token;  
	int      fh;

	if ((ofd = fopen(fname, "r+")) == NULL) {
		logit("", "ascii_rsam_store: unable to open file %s \n", fname);
		return EW_FAILURE;
	}
	fh = _fileno(ofd);

	/* initialize fileStart time */
	memset(&fileStart, 0, sizeof(struct tm));

	/* make sure we're at the start of the file */
	rewind(ofd);

	hbuf[0] = '\0';
	/* read start time from file */
	if (fgets(hbuf, MAX_CHAR, ofd) == NULL) {
		logit("", "ascii_rsam_store: unable to read file header \n");
		fclose(ofd);
		return EW_FAILURE;
	}

	if ((narg = sscanf(hbuf + 36, "%04d%02d%02d %02d:%02d:%02d %lf ", 
		&year, &month, &fileStart.tm_mday, &fileStart.tm_hour, &fileStart.tm_min, 
		&fileStart.tm_sec, &samprate)) != 7) {
		logit("", "ascii_rsam_store: unable to properly assign values from file header \n");
		fclose(ofd);
		return EW_FAILURE;
	}

	fileStart.tm_year = year - 1900;
	fileStart.tm_mon = month - 1;

	/* convert file start time to seconds since 1/1/70 */
	startTime = (long)timegm_ew(&fileStart);

	/* determine number of days in this month                 */
	ndays = dpm[fileStart.tm_mon];
	/* if it is February of a leap year, we need an extra day */
	if ((((fileStart.tm_year + 1900) % 4) == 0) && (fileStart.tm_mon == 1))
		ndays++;

	/* calculate number of samples per day in this file */
	rint_ew((double)SEC_PER_DAY * samprate, &nbins);

	/* figure out where to put this sample */
	bintime = (long)timegm_ew(sampleTime);
	/* if program stops and gets restarted, times may be off a bit, so put value
		in nearest slot */
	rint_ew(((double)bintime - startTime) * samprate, &index);
	day_no = (int)((float)index / (float)nbins) + 1;
	rint_ew ((double)(index % nbins), &day_index);

/*	fprintf(stderr, "ascii_rsam_store: bintime; %ld starttime: %ld index: %d day_no: %d day_index: %d\n", 
		bintime, startTime, index, day_no, day_index); */

	if (index < 0) {
		fclose(ofd);
		logit("", "ascii_rsam_store: Data point %ld before start of file %s (%lf sec)).\n", 
			bintime, fname, startTime);
		return EW_FAILURE;
	}
	else if (index >= nbins * ndays) {
		fclose(ofd);
		logit("", "ascii_rsam_store: Data point %ld after end of file %s (%lf sec)).\n", 
			bintime, fname, startTime);
		return EW_FAILURE;
	}
	else {
	/* move to start of line containing the data for this data point */
		for (daycount = 0; daycount < day_no - 1; daycount++) {
			if (fgets(hbuf, MAX_CHAR, ofd) == NULL) {
				logit("", "ascii_rsam_store: unable to read data line %d in file %s \n", 
					daycount +1, fname);
				fclose(ofd);
				return EW_FAILURE;
			}
		}

		/* current position in the file */
		file_position = ftell(ofd); 

		hbuf[0] = '\0';
		/* read the data line and confirm that it is the one we want */
		if (fgets(hbuf, MAX_CHAR, ofd) == NULL) {
			logit("", "ascii_rsam_store: unable to read data line in file %s \n", fname);
			fclose(ofd);
			return EW_FAILURE;
		}

		sprintf(dateString, "%04d-%3s-%02d", 
			fileStart.tm_year + 1900, cmonth[fileStart.tm_mon], day_no);
		if (strstr(hbuf, dateString) == NULL) {
			logit("", "requested data for day %d not available in file %s \n",
					dateString, fname);
			fclose(ofd);
			return EW_FAILURE;
		}

		/* initialize dataArray */
		for (i=0; i < 2880; i++);
			dataArray[i] = ASCII_FILL_LONG;

		/* get the data! Values are separated by a single blank */
		buf = hbuf + strlen(dateString); 
		token = strtok(buf, " ");
		count = -1;
		while (token) {
			count++;
			sscanf(token, "%ld", &dataArray[count]);
			/* get next token */
			token = strtok((char *)NULL, " ");
		}
			
		/* put the data in its place; recall that dataArray contains bintime followed by 
			RSAM value so bintime with day_index i goes at dataArray position 2*i and the
			corresponding RSAM value belongs in position 2*i + 1  */

		/* we may have previously put something in this bin */
		storedValue = ((dataArray[2*day_index + 1] == ASCII_FILL_LONG) ? 
				rValue : rValue + dataArray[2*day_index + 1]);
		dataArray[2*day_index + 1] = storedValue; 

/*
		fprintf(stderr, "storedValue: %ld day_index: %d dataArray: %ld previous: %ld\n",
			storedValue, day_index, dataArray[2*day_index + 1], dataArray[2*day_index - 1]);
*/		
		/* if there is a time already stored here, average them */
		if (dataArray[2*day_index] == ASCII_FILL_LONG)
			storedTime = bintime;
		else 
			rint_ew(0.5 * (double)(bintime + dataArray[2*day_index]), &storedTime);
		dataArray[2*day_index] = storedTime; 

/*		fprintf(stderr, "storedTime: %ld day_index: %d dataArray: %ld previous: %ld\n",
			storedTime, day_index, dataArray[2*day_index], dataArray[2*day_index -2]);
*/

		/* write new values back into the string */
		j = 0;
		hbuf[0] ='\0';
		j = sprintf(hbuf, "%s", dateString);
		for (i = 0; i < count + 1; i++)
			j += sprintf(hbuf + j, " %016ld", dataArray[i]);
		hbuf[j] = '\0';


		/* go to correct insertion point in file */
		rewind(ofd);
		fseek(ofd, file_position, SEEK_SET); 

		/* now write the data */
		if (fwrite(hbuf, strlen(hbuf), 1, ofd) != 1) {
/*		if (_write(fh, hbuf, strlen(hbuf)) == -1) { */
			logit("", "ascii_rsam_store: data not correctly written to file \n");
			fclose(ofd);
			return EW_FAILURE;
		}
		
	}

	if (fclose(ofd) != 0) {
		logit("", "ascii_rsam_store: file %s did not close cleanly while storing data \n", 
				fname);
		return EW_FAILURE;
	}

	return EW_SUCCESS;
}

