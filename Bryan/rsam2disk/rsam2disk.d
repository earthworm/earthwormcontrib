
#
#                     Configuration File for rsam2disk
#
MyModuleId		MOD_RSAM2DISK
InRingName		WAVE_RING	# Transport ring to find waveform data on,
OutRingName		HYPO_RING	# Transport ring to write output to,
HeartBeatInterval	15		# Heartbeat interval, in seconds,
LogFile		1		# 1 -> Keep log, 0 -> no log file

#
# Specify logos of the messages to grab from the InRing.
# messages asumed to be of type TYPE_TRACEBUF; therefore only
# module ID and installation ID need to be specified
#
GetRsamValuesFrom    INST_WILDCARD MOD_EW2RSAM  TYPE_WAVEFORM

#
# SCN Selection 
#
#  Use the following two configuration options to narrow down
#  the SCNs of the wave messages of interest (wildcard '*' 
#  characters are allowed for both options):
#
#   o IncludeSCN (required)
#   o ExcludeSCN (optional)
#
#
#  SCN which matches a line in the IncludeSCN section will be 
#  included, unless it also matches a line in the ExcludeSCN section
#
#  Examples:
#
#   1. Pick up only these SCNs
#
#       IncludeSCN HJG VHZ NC
#       IncludeSCN HJS VHZ NC
#       IncludeSCN KGM VHZ NC
#
#   2. Pick up all SCNs except for KGM VHZ NC
#
#       IncludeSCN * * * 
#       ExcludeSCN KGM VHZ NC
#
#   3. Pick up all SCNs except those with network code NC
#
#       IncludeSCN * * * 
#       ExcludeSCN * * NC

IncludeSCN HJG VHZ NC

#
# MaxSCN - max number of SCNs to track
#
MaxSCN		50

#
# RsamPeriod - time period, in seconds, for saving
# These must match periods calculated in ew2rsam
# It is suggested that one not write data of <60 sec to disk
#
RsamPeriod	60
RsamPeriod	600

# Select format of output data and the directory where it is written
# Currently only netCDF and ASCII formats are supported.
#
# NETCDF
#
DataFormat                  netcdf
OutDir                      "d:\data\netcdf"
#
# ASCII
#
# NB: ASCII format ouput is currently restricted to files with sampling
# rates of 1 MIN or greater
#
DataFormat                  ascii
OutDir                      "d:\data\ascii"
#
# Specify on what platform the output files will be used:
# intel or sparc - with this information, files will be written out
# in the correct byte order.
#
OutputArch                intel



