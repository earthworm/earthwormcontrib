/*************************************************************************
**	rsam_asciixyputaway.c                                               **
**                                                                      **
**	ascii  format putaway routines for rsam data.                       **
**                                                                      **
**	Carol Bryan                                                         **
**	Cascades Volcano Observatory                                        **
**	10/30/01                                                            **
*************************************************************************/

/* system includes */
#include <stdio.h>
#include <io.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <fcntl.h>  /* for open flags */

/* earthworm/glowworm includes */
#include <earthworm.h>
#include <trace_buf.h>
#include <time_ew.h>
#include <rsamputaway.h>
#include <rsam_asciixyputaway.h>

/* package includes */
#include <netcdf.h>

/* rsam includes */
#include <rsam2disk.h>

#define XY_FILL -9998
#define SEC_PER_DAY 86400	/* number of seconds in one day */
#define MAX_CHAR 1000

/*************************************************************************
**		ascii_xy_rsam_create()                                          **
**		entry point to RSAM storage, ascii_xy output format             **
**      this is a stripped down file, it contains no header info, i.e.  **
**      no station information                                          **
**                                                                      **
**		Data values will be prefilled with a value of NC_LONG           **
/************************************************************************/
int ascii_xy_rsam_create (char *filename, TRACE_HEADER *rhead, 
					struct tm *rsamTime) 
{
	FILE      *ofd;         /* file pointer for output              */
	int       daycount;     /* loop counter for day of month        */
	int       sampcount;    /* loop counter for sample in day       */
	int       nbins = 0;    /* number of RSAM values per day        */
	int	      ndays;        /* number of days of data in this file  */
	int       dpm[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	char      *month[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
						"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	char	  dateString[12]; /* string for writing date in to file */
	char      title[] = {"RSAM"};
	struct tm fileStart;    /* start of month                       */
	
	if ((ofd = fopen(filename, "a+")) != NULL) {
		/* determine number of days in this month                 */
		ndays = dpm[rsamTime->tm_mon];
		/* if it is February of a leap year, we need an extra day */
		if ((((rsamTime->tm_year + 1900) % 4) == 0) && (rsamTime->tm_mon == 1))
			ndays++;

		/* calculate number of samples per day to be written to this file */
		rint_ew((double)SEC_PER_DAY * rhead->samprate, &nbins);

		/* determine start time of file (first day of the month) */
		memcpy(&fileStart, rsamTime, sizeof(struct tm));
		fileStart.tm_mday = 1;
		fileStart.tm_hour = fileStart.tm_hour = fileStart.tm_sec = 0;		

			
		/* initialize time and rsam values to no data values;
			99:99:99 and XY_FILL respectively; store one value per line */
		for (daycount = 0; daycount < ndays; daycount++) {
			sprintf(dateString, "%02d/%02d/%04d", 
				fileStart.tm_mon + 1, daycount + 1, fileStart.tm_year + 1900);
			for (sampcount = 0; sampcount < nbins; sampcount++)
					fprintf(ofd, "%s 99:99:99 %+05d\n", dateString, XY_FILL);
		}

		/* close 'er up */
		if (fclose(ofd)) {
			logit("", "fclose: file %s did not close cleanly \n", filename);
			return EW_FAILURE;
		}

	}
	else {
		/* assume the file already exists */
		return EW_SUCCESS;
	}

	return EW_SUCCESS;
}

/*************************************************************************
**		ascii_xy_rsam_store()                                           **
**		put the data away in ascii format                               **
**                                                                      **
**		Data are written as long integers                               **
**      Each line contains a date, time, and one RSAM value             **
**                                                                      **
*************************************************************************/
int ascii_xy_rsam_store(char *fname, long rValue, struct tm *sampleTime) 
{


	FILE    *ofd;           /* file pointer for output              */
	char    hbuf[MAX_CHAR]; /* buffer for storing header string     */
	char    *buf;           /* pointer into buffer                  */
	int     narg =0;        /* number of arguments read from file   */
	int     year  = 0;      /* year from file header                */
	char    month[4];       /* month from file header               */
	double  samprate = 0.0; /* data sampling rate (samples per sec) */

	int	    ndays;          /* number of days of data in this file  */
	int     dpm[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	char    *cmonth[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
						"Jul", "Aug", "Sep", "Oct", "Nov", "Dec" }; 
	char	dateString[12]; /* string for writing date in to file   */

	int     nbins = 0;      /* number of RSAM values per day        */
	int     day_no = 0;		/* day of month for data                */
	int     day_index = 0;  /* index of sample for this day         */

	struct tm fileStart;    /* start of file                        */
	long    startTime;      /* start time of this file (sec since 1/1/1970) */
	long    bintime;        /* time of this sample (sec since 1/1/1970)     */
	long    storedTime;
	long	storedTime2;
	struct tm storedTimeStruct;
	struct tm *storedTimePtr = &storedTimeStruct;
	int     index = -1;     /* index of sample in this file         */
	long    storedValue;    /* fill value or previous value         */
	long    file_position;  /* location in file for this day's data */
	
	long	rsamValue = XY_FILL; /* data value              */
	char    timeString[9] = "99:99:99"; /* sample time as hh:mm:ss  */
	char    fileTime[9];
	int     count, i;         /* loop counters                      */
	int     j;
	int      fh;
	char	*token;


	if ((ofd = fopen(fname, "r+")) == NULL) {
		logit("", "ascii_rsam_store: unable to open file %s \n", fname);
		return EW_FAILURE;
	}
	fh = _fileno(ofd);

	/* initialize fileStart time */
	memset(&fileStart, 0, sizeof(struct tm));

	/* make sure we're at the start of the file */
	rewind(ofd);

	hbuf[0] = '\0';
	/* read start time from file */
	if (fgets(hbuf, MAX_CHAR, ofd) == NULL) {
		logit("", "ascii_xy_rsam_store: unable to read file header \n");
		fclose(ofd);
		return EW_FAILURE;
	}

	if ((narg = sscanf(hbuf, "%02d/%02d/%04d", &fileStart.tm_mon, &fileStart.tm_mday, &year)) != 3) {
		logit("", "ascii_xy_rsam_store: unable to properly assign values from file header \n");
		fclose(ofd);
		return EW_FAILURE;
	}

	/* change month to that expected by the time structure, i.e. 0-11 */
	fileStart.tm_mon--;

	/* initialize tm structure */
	fileStart.tm_year = year - 1900;
	fileStart.tm_yday = 0;
	fileStart.tm_mday = 1;
	fileStart.tm_hour = 0;
	fileStart.tm_min = 0;
	fileStart.tm_sec = 0;

	/* convert file start time to seconds since 1/1/70 */
	startTime = (long)timegm_ew(&fileStart);

	/* determine number of days in this month                 */
	ndays = dpm[fileStart.tm_mon];
	/* if it is February of a leap year, we need an extra day */
	if ((((fileStart.tm_year + 1900) % 4) == 0) && (fileStart.tm_mon == 1))
		ndays++;

	/* determine sampling rate of data in this file from the filename */
	strcpy(hbuf, fname);
	buf = hbuf;
	token = strtok(buf, "\\");
		while (token) {
			/* look for sampling rate (either denoted by Min or Sec) in this token */
			if (strstr(token, "Min") != NULL) {
				sscanf(token, "%lf", &samprate);
				samprate *= 60.0;
				break;
			}
			else if (strstr(token, "Sec") != NULL) {
				sscanf(token, "%lf", &samprate);
				break;
			}
			else {			
				/* get next token */
				token = strtok((char *)NULL, "\\");
			}

		}

	/* calculate number of samples per day in this file */
	rint_ew((double)SEC_PER_DAY / samprate, &nbins);


	/* figure out where to put this sample */
	bintime = (long)timegm_ew(sampleTime);
	/* if program stops and gets restarted, times may be off a bit, so put value
		in nearest slot */
	rint_ew(((double)bintime - startTime) / samprate, &index);
	day_no = (int)((float)index / (float)nbins) + 1;

	if (index < 0) {
		fclose(ofd);
		logit("", "ascii_rsam_store: Data point %ld before start of file %s (%lf sec)).\n", 
			bintime, fname, startTime);
		return EW_FAILURE;
	}
	else if (index >= nbins * ndays) {
		fclose(ofd);
		logit("", "ascii_rsam_store: Data point %ld after end of file %s (%lf sec)).\n", 
			bintime, fname, startTime);
		return EW_FAILURE;
	}
	else {
	/* move to start of line containing the data for this data point */
		for (count = 0; count < index; count++) {
			if (fgets(hbuf, MAX_CHAR, ofd) == NULL) {
				logit("", "ascii_xy_rsam_store: unable to read data line %d in file %s \n", 
					count + 1, fname);
				fclose(ofd);
				return EW_FAILURE;
			}
		}

		/* current position in the file */
		file_position = ftell(ofd); 

		hbuf[0] = '\0';
		/* read the data line and confirm that it is the one we want */
		if (fgets(hbuf, MAX_CHAR, ofd) == NULL) {
			logit("", "ascii_xy_rsam_store: unable to read data line in file %s \n", fname);
			fclose(ofd);
			return EW_FAILURE;
		}

		/* write date of sample into a string for ease of programming */
		sprintf(dateString, "%02d/%02d/%04d", 
			sampleTime->tm_mon + 1, day_no, sampleTime->tm_year + 1900);
		
		if (strstr(hbuf, dateString) == NULL) {
			logit("", "requested data for day %d not available in file %s \n",
					dateString, fname);
			fclose(ofd);
			return EW_FAILURE;
		}

		/* write sample time into a time string of the form hh:mm:ss */
		sprintf(timeString, "%02d:%02d:%02d", 
			sampleTime->tm_hour, sampleTime->tm_min, sampleTime->tm_sec); 


		/* get the data! */
		buf = hbuf + strlen(dateString) + 1;
		sscanf(buf, "%s %ld", fileTime, &rsamValue);
				
		/* put the data in its place */
		/* we may have previously put something in this bin */
		storedValue = (((int)rsamValue == XY_FILL) ? 
				rValue : rValue + rsamValue);
		rsamValue = storedValue; 

		/* if there is a time already stored here, average them */
		if (strcmp(fileTime, "99:99:99") == 0)
			storedTime = bintime;
		else {
			memset(&storedTimeStruct, 0, sizeof(struct tm));
			storedTimeStruct.tm_year = sampleTime->tm_year;
			storedTimeStruct.tm_mon = sampleTime->tm_mon;
			storedTimeStruct.tm_mday = sampleTime->tm_mday;

			if ((sscanf(hbuf, "%02d:%02d:%02d", &storedTimeStruct.tm_hour, 
					&storedTimeStruct.tm_min, &storedTimeStruct.tm_sec)) != 3) {
				logit("", "ascii_xy_rsam_store: unable to properly assign time value from file  \n");
				fclose(ofd);
				return EW_FAILURE;
			}

			storedTime2 = mktime(storedTimePtr);

			rint_ew(0.5 * (double)(bintime + storedTime2), &storedTime);

		}
					
		/* convert sampleTime to a struct tm value */
		sampleTime = gmtime(&storedTime);

		/* write time value to the timeString string */
		sprintf(timeString, "%02d:%02d:%02d",
			sampleTime->tm_hour, sampleTime->tm_min, sampleTime->tm_sec);

		/* write new values back into the string */
		j = 0;
		hbuf[0] ='\0';
		j = sprintf(hbuf, "%10s %8s %+05d", dateString, timeString, (int)rsamValue);
		hbuf[j] = '\0';


		/* go to correct insertion point in file */
		rewind(ofd);
		fseek(ofd, file_position, SEEK_SET);

		/* now write the data */
		if (fwrite(hbuf, strlen(hbuf), 1, ofd) != 1) {
			logit("", "ascii_rsam_store: data not correctly written to file \n");
			fclose(ofd);
			return EW_FAILURE;
		}
		
	}

	if (fclose(ofd) != 0) {
		logit("", "ascii_rsam_store: file %s did not close cleanly while storing data \n", 
				fname);
		return EW_FAILURE;
	}

	return EW_SUCCESS;
}

