/* rsam_asciixyputaway.h: ascii format putaway header file for rsam data */

#ifndef _RSAM_ASCIIXYPUTAWAY_H
#define _RSAM_ASCIIXYPUTAWAY_H

/* Function prototypes */
int ascii_xy_rsam_create (char *filename, TRACE_HEADER *rhead, struct tm *rsamTime);
int ascii_xy_rsam_store(char *fname, long rValue, struct tm *sampleTime);

#endif  /*_RSAM_ASCIIXYPUTAWAY_H */
