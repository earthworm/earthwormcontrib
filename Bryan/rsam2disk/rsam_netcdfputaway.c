/*************************************************************************
**	netcdfputaway.c	                                                    **
**                                                                      **
**	netcdf format putaway routines for rsam data.                       **
**                                                                      **
**	Carol Bryan                                                         **
**	Cascades Volcano Observatory                                        **
**	3/28/01                                                             **
*************************************************************************/

/* system includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

/* earthworm/glowworm includes */
#include <earthworm.h>
#include <trace_buf.h>
#include <time_ew.h>
#include <rsam2disk.h>
#include <rsamputaway.h>
#include <rsam_netcdfputaway.h>

/* package includes */
#include <netcdf.h>

#define SEC_PER_DAY 86400	/* number of seconds in one day */
/*************************************************************************
**		nc_rsam_create_file()                                           **
**		entry point to RSAM storage, branches depend on output format   **
**                                                                      **
**		Default behavior of netCDF nc_def_var is to prefill variable    **
**		array with an XDR representation of values of the appropriate   **
**		type.                                                           **
/************************************************************************/
int nc_rsam_create (char *filename, TRACE_HEADER *rhead, 
					struct tm *rsamTime) 
{
	int       ncid;         /* netCDF ID                            */
	int       bid;          /* bin ID                               */
	int       nbins = 0;    /* number of RSAM values per month      */
	int	      ndays;        /* number of days of data in this file  */
	int       dpm[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	char      title[] = {"RSAM"};
	double    epochTime;    /* time in sec since 1/1/1970           */
	double    samprate;     /* sampling rate                        */
	int       rsamValueId;  /* ID for the rsamValue variable        */	
	int	      bintimeId;    /* ID for the rsam value bintime        */
	struct tm fileStart;    /* start of month                       */

	if (nc_create(filename, NC_NOCLOBBER | NC_SHARE, &ncid) == NC_NOERR) {

		/* calculate number of samples to be written to this file */
		ndays = dpm[rsamTime->tm_mon];
		/* if it is February of a leap year, we need an extra day */
		if ((((rsamTime->tm_year + 1900) % 4) == 0) && (rsamTime->tm_mon == 1))
			ndays++;

		rint_ew((double)ndays * (double)SEC_PER_DAY * rhead->samprate, &nbins);

		/* define number of samples variable */
		if (nc_def_dim(ncid, "bins", nbins, &bid) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_def_dim: unable to create dimension <bins>");
			return EW_FAILURE;
		}

		/* tell the file this is RSAM data and write in station, channel, and
			network information */
		if (nc_put_att_text(ncid, NC_GLOBAL, "title", strlen(title), 
			title) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_put_att_text: unable to write title %s\n", title);
			return EW_FAILURE;
		}
		if (nc_put_att_text(ncid, NC_GLOBAL, "station", strlen(rhead->sta),
			rhead->sta) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_put_att_text: unable to write station %s\n", rhead->sta);
			return EW_FAILURE;
		}
		if (nc_put_att_text(ncid, NC_GLOBAL, "channel", strlen(rhead->chan),
			rhead->chan) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_put_att_text: unable to write channel %s\n", rhead->chan);
			return EW_FAILURE;
		}
		if (nc_put_att_text(ncid, NC_GLOBAL, "network", strlen(rhead->net),
			rhead->net) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_put_att_text: unable to write network %s\n", rhead->net);
			return EW_FAILURE;
		}

		/* write in start time of file (first day of the month) */
		memcpy(&fileStart, rsamTime, sizeof(struct tm));
		fileStart.tm_mday = 1;
		fileStart.tm_hour = fileStart.tm_min = fileStart.tm_sec = 0;
		epochTime = (double)timegm_ew(&fileStart);
		if (nc_put_att_double(ncid, NC_GLOBAL, "startBinTime", NC_DOUBLE, 1,
			&epochTime) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_put_att_text: unable to write starting bintime %lf\n", epochTime);
			return EW_FAILURE;
		}

		/* now annotate with sampling rate */
		samprate = rhead->samprate;
		if (nc_put_att_double(ncid, NC_GLOBAL, "samprate", NC_DOUBLE, 1, 
			&samprate) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_put_att_text: unable to write sampling rate %lf\n", rhead->samprate);
			return EW_FAILURE;
		}
	
		/* define the rsamValue variable */
		if (nc_def_var(ncid, "rsamValue", NC_LONG, 1, &bid, &rsamValueId) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_def_var: unable to define rsamValue variable \n");
			return EW_FAILURE;
		}

		/* define the rsam bintime variable */
		if (nc_def_var(ncid, "bintime", NC_LONG, 1, &bid, &bintimeId) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_def_var: unable to define bintime variable \n");
			return EW_FAILURE;
		}
	
		/* pau with definitions */
		if (nc_enddef(ncid) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_enddef: unable to end definitions \n");
			return EW_FAILURE;
		}

		/* close 'er up */
		if (nc_close(ncid) != NC_NOERR) {
			logit("", "nc_close: file %s did not close cleanly \n", filename);
			return EW_FAILURE;
		}

	}
	else {
		/* assume the file already exists */
		return EW_SUCCESS;
	}

	return EW_SUCCESS;
}


/*************************************************************************
**		nc_rsam_store()                                                 **
**		put the data away in nedCDF format                              **
**                                                                      **
**		Recall netCDF values are written as XDR representations so      **
**		one should not need to be concerned if data are to be written   **
**		to a machine with a different architecture than the machine     **
**		from which the data are written.                                **
/************************************************************************/
int nc_rsam_store(char *fname, long rValue, struct tm *sampleTime) 
{
	int     ncid;           /* netCDF file ID                       */
	int     bid;            /* bin id                               */
	int     binlength;      /* length of "bins" dimension           */
	double  startTime;      /* start time of this file (sec since 1/1/1970) */
	double  samprate;       /* sampling rate                        */
	long    bintime;        /* time of this sample (sec since 1/1/1970)     */
	long    ftime;          /* fill value or previous value         */
	long    storedTime;
	int     index = -1;     /* index of sample in this file         */
	int     rsamValueId;    /* ID for the rsamValue variable        */	
	int	    bintimeId;      /* ID for the rsam value bintime        */	
	long    rsamValue;		
	long    storedValue;    /* fill value or previous value         */


	if (nc_open(fname, NC_WRITE | NC_SHARE, &ncid) != NC_NOERR) {
		logit("", "nc_open: unable to open file %s \n", fname);
		return EW_FAILURE;
	}

	if (nc_inq_dimid(ncid, "bins", &bid) != NC_NOERR) {
		nc_close(ncid);
		logit("", "nc_inq_dimid: unable to get id of dimension <bins> \n");
		return EW_FAILURE;
	}

	if (nc_inq_dimlen(ncid, bid, &binlength) != NC_NOERR) {
		nc_close(ncid);
		logit("", "nc_inq_dimlen: unable to get length of dimension <bins> \n");
		return EW_FAILURE;
	}

	if (nc_get_att_double(ncid, NC_GLOBAL, "startBinTime", &startTime) != NC_NOERR) {
		nc_close(ncid);
		logit("", "nc_get_att_double: unable to get startBinTime \n");
		return EW_FAILURE;
	}
		
	if (nc_get_att_double(ncid, NC_GLOBAL, "samprate", &samprate) != NC_NOERR) {
		nc_close(ncid);
		logit("", "nc_get_att_double: unable to get sampling rate \n");
		return EW_FAILURE;
	}

	bintime = (long)timegm_ew(sampleTime);
	/* if program stops and gets restarted, times may be off a bit, so put value
		in nearest slot */
	rint_ew(((double)bintime - startTime) * samprate, &index);

	if (index < 0) {
		nc_close(ncid);
		logit("", "nc_rsam_store: Data point %ld before start of file %s (%lf sec)).\n", 
			bintime, fname, startTime);
		return EW_FAILURE;
	}
	else if (index > binlength) {
		nc_close(ncid);
		logit("", "nc_rsam_store: Data point %ld after end of file %s (%lf sec)).\n", 
			bintime, fname, startTime);
		return EW_FAILURE;
	}
	else {
		if (nc_inq_varid(ncid, "rsamValue", &rsamValueId) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_inq_varid: Could not get id for rsamValue.\n");
			return EW_FAILURE;
		}
		if (nc_get_var1_long(ncid, rsamValueId, &index, &rsamValue) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_get_var1_long: Could not %dth value of rsamValue.\n", index);
			return EW_FAILURE;
		}

		/* we may have previously put something in this bin */
		storedValue = ((rsamValue == FILL_LONG) ? rValue : rValue + rsamValue);
		if (nc_put_var1_long(ncid, rsamValueId, &index, &storedValue) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_put_var1_long: Could not write %dth value of rsamValue <%ld>.\n", 
				index, storedValue);
			return EW_FAILURE;
		}

		if (nc_inq_varid(ncid, "bintime", &bintimeId) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_inq_varid: Could not get id for bintime.\n");
			return EW_FAILURE;
		}
		if (nc_get_var1_long(ncid, bintimeId, &index, &ftime) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_get_var1_long: Could not %dth value of bintime.\n", index);
			return EW_FAILURE;
		}
		/* if there is a time already stored here, average them */
		if (ftime == FILL_LONG) 
			storedTime = bintime;
		else
			rint_ew(0.5 * (double)(bintime + ftime), &storedTime);

		if (nc_put_var1_long(ncid, bintimeId, &index, &storedTime) != NC_NOERR) {
			nc_close(ncid);
			logit("", "nc_put_var1_long: Could not write %dth value of bintime <%ld>.\n", 
				index, storedTime);
			return EW_FAILURE;
		}
	}

	if (nc_close(ncid) != NC_NOERR) {
			logit("", "nc_close: file %s did not close cleanly while storing data \n", 
					fname);
			return EW_FAILURE;
		}

	return EW_SUCCESS;
}









