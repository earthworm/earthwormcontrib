#include <iostream>
#include <math.h>
#include <string.h>
#include "v0_xml.h"

XERCES_CPP_NAMESPACE_USE

// internal call  to configure XML xerces modules
int v0_xml::init() {
    // Initialize the XML4C2 system
    try
    {
        XMLPlatformUtils::Initialize();
    }
 
    catch(const XMLException &toCatch)
    {
        cerr << "Error during Xerces-c Initialization.\n"
             << "  Exception message:"
             << StrX(toCatch.getMessage()).localForm() << endl;
        return 1;
    }
	
    return 0;
}

char * _stripTrailingSpaces(char c[]) {

    char *p;
    int l = strlen(c);
    p = &c[l-1];
    while (p != c && *p == ' ') {
	*p='\0';
	p--;
    }
    return c;
}

char * _stripLeadingSpaces(char c[]) {
	
    char *p;
    p = &c[0];
    while( *p != '\0') {
	if (*p == ' ') {
	    p++;
	} else {
	    break;
	}
    }
    return p;
}
// 
// get the top node of the document 
//
DOMNode * v0_xml::getStartingNode() {
    return v0_doc->getDocumentElement();
}

////////////////////////////////////////////////////////
// 
// return the text string associated with the element
//
XMLCh * _getTextOfElement(DOMNode *n) {

    XMLCh *value;
    if (!n) return NULL;
	
    value = NULL;
    for (DOMNode *child = n->getFirstChild(); child != 0 ; child=child->getNextSibling()) {
	if (child->getNodeType() == DOMNode::TEXT_NODE) {
	    value = (XMLCh *) child->getNodeValue();
	    break;
	}
    }
    return value;
}

////////////////////////////////////////////////////////
//
// return the element with the name requested
// 	recursively searches children
//
DOMNode * _findNode(const XMLCh *name, DOMNode *n) 
{
    DOMNode *child, *ret_node;

    char *search_name = XMLString::transcode(name);
    char *node_name = XMLString::transcode(n->getNodeName());
#ifdef DEBUG_DIFF
    cout << "DEBUG looking for <"<< search_name<< "> in node named <"<<node_name<<"> "<<endl;
#endif

    if (n->getNodeType() == DOMNode::ELEMENT_NODE) {
	if (XMLString::compareIString(n->getNodeName(), name) == 0) {
#ifdef DEBUG_DIFF
	    cout << "DEBUG found it in this node <" << search_name << ">"<<endl;
#endif
	    XMLString::release(&search_name);
	    XMLString::release(&node_name);
	    return n;
	}
	if (! n->hasChildNodes()) return NULL;
	// otherwise search the children nodes
	for (child = n->getFirstChild(); child != 0 ; child=child->getNextSibling()) {
	    if ( (ret_node = _findNode(name, child)) != NULL) {
		XMLString::release(&search_name);
		XMLString::release(&node_name);
		return ret_node;
	    }
	}
    }
    XMLString::release(&search_name);
    XMLString::release(&node_name);
    return NULL;
}


////////////////////////////////////////////////////////
// CONSTRUCTOR
// parse an XML file into a DOM object
v0_xml::v0_xml(char *file)
{

    if ( init() != 0 ) { 
	return;  
    }

    XercesDOMParser *parser = new XercesDOMParser;


    parser->setValidationScheme(XercesDOMParser::Val_Auto);
    parser->setDoNamespaces(false);		// do not use namespace processing
    parser->setDoSchema(false);		// do not do schema processing
    parser->setValidationSchemaFullChecking(false); // do not do schema validation
    parser->setCreateEntityReferenceNodes(false); 	// do not create entity reference nodes

    DOMTreeErrorReporter *errReporter = new DOMTreeErrorReporter();
    parser->setErrorHandler(errReporter);
    //
    //  Parse the XML file, catching any XML exceptions that might propogate
    //  out of it.
    //
    bool errorsOccured = false;
    try
    {
	parser->parse(file);
    }

    catch (const XMLException& e)
    {
	cerr << "An error occurred during parsing\n   Message: "
	     << StrX(e.getMessage()) << endl;
	errorsOccured = true;
    }

    catch (const DOMException& e)
    {
	const unsigned int maxChars = 2047;
	XMLCh errText[maxChars + 1];
	
	cerr << "\nDOM Error during parsing: '" << file << "'\n"
	     << "DOMException code is:  " << e.code << endl;
	
	if (DOMImplementation::loadDOMExceptionMsg(e.code, errText, maxChars))
	    cerr << "Message is: " << StrX(errText) << endl;
	
	errorsOccured = true;
    }

    if (errorsOccured || errReporter->getSawErrors()) {
	return;
    }

    // we have a valid XML document at this point, get the root element
    v0_doc = parser->getDocument();
}

////////////////////////////////////////////////////////
// CONSTRUCTOR
// build an DOM doc using the v0_cosmos object
v0_xml::v0_xml(v0_cosmos *vh)
{
    char tmpstr[50];
    int unknown_int;
    double unknown_real;

    unknown_int = vh->getUnknownIntVal();
    unknown_real = vh->getUnknownRealVal();

    if ( init() != 0 ) { 
	return;  
    }

    DOMImplementation* impl =  DOMImplementationRegistry::getDOMImplementation(X("Core"));
    v0_doc = impl->createDocument(
				  0,                    // root element namespace URI.
				  X("cosmos"),          // root element name
				  0);                   // document type object (DTD). 

    DOMElement* rootElem = v0_doc->getDocumentElement();

    /* record element */
    DOMElement* recordElem = v0_doc->createElement(X("record"));
    rootElem->appendChild(recordElem);
    recordElem->setAttribute(X("descriptor"), X("Raw"));
    recordElem->setAttribute(X("type"), X(vh->getParameterType()));

    /* record must have <owner> and <distributor>, <processor> and <network> is optional */
    DOMElement* operatorElem = v0_doc->createElement(X("owner"));
    recordElem->appendChild(operatorElem);
    DOMElement* codeElem = v0_doc->createElement(X("code"));
    operatorElem->appendChild(codeElem);
    codeElem->setAttribute(X("source"), X("IRIS"));
    {
	DOMText *codeDataVal;
	if (strcmp(vh->getOwnerNetworkCode(), "??") == 0) {
	    codeDataVal = v0_doc->createTextNode(X(vh->getNetworkCode()));
	} else {
	    codeDataVal = v0_doc->createTextNode(X(vh->getOwnerNetworkCode()));
	}
	codeElem->appendChild(codeDataVal);
    }

    DOMElement* networkElem = v0_doc->createElement(X("network"));
    recordElem->appendChild(networkElem);
    DOMElement* netcodeElem = v0_doc->createElement(X("code"));
    networkElem->appendChild(netcodeElem);
    netcodeElem->setAttribute(X("source"), X("IRIS"));
    DOMText* netcodeDataVal = v0_doc->createTextNode(X(vh->getNetworkCode()));
    netcodeElem->appendChild(netcodeDataVal);

    /* not clear how to do processor/distributor elements, am I them? */

    /* provide record/<format> containing cosmos version */
    DOMElement* formatElem = v0_doc->createElement(X("format"));
    sprintf(tmpstr, "%4.2f", (double) vh->getCOSMOSversion()/100.);
    formatElem->setAttribute(X("name"), X("COSMOS"));
    formatElem->setAttribute(X("version"), X(_stripLeadingSpaces(tmpstr)));
    recordElem->appendChild(formatElem);

    /* provide a comment from the V0 header */
    DOMElement* recCommentElem = v0_doc->createElement(X("comment"));
    DOMText*    commentDataVal = v0_doc->createTextNode(X(vh->getCommentLines()));
    recCommentElem->appendChild(commentDataVal);

    /* event element (trying to get this made optional) */

    /* station element */
    DOMElement* stationElem = v0_doc->createElement(X("station"));
    rootElem->appendChild(stationElem);
	
    DOMElement* addressElem = v0_doc->createElement(X("address"));
    stationElem->appendChild(addressElem);
    DOMText*    addressDataVal = v0_doc->createTextNode(X(_stripTrailingSpaces(vh->getStationDescription())));
    addressElem->appendChild(addressDataVal);

    DOMElement* coordElem = v0_doc->createElement(X("coordinates"));
    stationElem->appendChild(coordElem);
    DOMElement* latElem = v0_doc->createElement(X("latitude"));
    // sprintf(tmpstr, "%9.5f", vh->getStationLatitude());
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getStationLatitude());
    DOMText*    latDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    coordElem->appendChild(latElem);
    latElem->appendChild(latDataVal);
    DOMElement* lonElem = v0_doc->createElement(X("longitude"));
    coordElem->appendChild(lonElem);
    // sprintf(tmpstr, "%10.5f", vh->getStationLongitude());
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getStationLongitude());
    DOMText*    lonDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    lonElem->appendChild(lonDataVal);

    DOMElement * elevElem = v0_doc->createElement(X("elevation"));
    elevElem->setAttribute(X("units"), X("m"));
    stationElem->appendChild(elevElem);
    // sprintf(tmpstr, "%10.5f", vh->getStationElevation());
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getStationElevation());
    DOMText*    elevDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    elevElem->appendChild(elevDataVal);


    DOMElement * stanumElem = v0_doc->createElement(X("number"));	
    stationElem->appendChild(stanumElem);
    DOMText*    stanumDataVal = v0_doc->createTextNode(X(vh->getStationNumber()));
    stanumElem->appendChild(stanumDataVal);

    DOMElement * stanameElem = v0_doc->createElement(X("name"));	
    stationElem->appendChild(stanameElem);
    DOMText*    stanameDataVal = v0_doc->createTextNode(X(_stripTrailingSpaces(vh->getStationCode())));
    stanameElem->appendChild(stanameDataVal);

    // eventually get rid of address as it is redundent
    DOMElement* locationElem = v0_doc->createElement(X("location"));
    stationElem->appendChild(locationElem);
    DOMText*    locationDataVal = v0_doc->createTextNode(X(_stripTrailingSpaces(vh->getStationDescription())));
    locationElem->appendChild(locationDataVal);
    /* instrument element */
    /* example:

    <recorder type="Etna" manufacturer="Kinemetrics" serial_number="1225">
    <channel number="1" recorded="3" total="4"/>
    <digitizer_yStep units="microns"/>
    <azimuth>360</azimuth>
    <trigger_number>4</trigger_number>
    <least_significant_bit units="microvolts">0.29802</least_significant_bit>
    <full_scale_input units="volts">2.500000</full_scale_input>
    <number_of_bits>24</number_of_bits>
    <effective_number_of_bits>18</effective_number_of_bits>
    <filter type="anti-alias"/>
    </recorder>
    <sensor type="FBA-11" manufacturer="Kinemetrics">
    <natural_frequency>94.300000</natural_frequency>
    <damping_coefficient>0.640000</damping_coefficient>
    <sensitivity>0.625000</sensitivity>
    <full_scale_output units="volts">2.500000</full_scale_output>
    <full_scale_sensing_capability units="g">4.000000</full_scale_sensing_capability>
    <filter/>
    <gain>1.000000</gain>
    </sensor>
    <emplacement>
    <offset type="north" units="m"/>
    <offset type="east" units="m"/>
    <offset type="vertical" units="m"/>
    </emplacement>
    </instrument>

    */
    DOMElement* instElem = v0_doc->createElement(X("instrument"));
    rootElem->appendChild(instElem);


    // RECORDER
    DOMElement * recorderElem = v0_doc->createElement(X("recorder"));	
    instElem->appendChild(recorderElem);
    { 
	char *type, *mfg;
	int serial;

	type=vh->getDataloggerType();
	if ( (mfg=strchr(type, ',')) != NULL) {
	    *mfg='\0';
	    mfg++; // skip comma
	    mfg++; // skip space
	    recorderElem->setAttribute(X("manufacturer"), X(mfg));
	}
	recorderElem->setAttribute(X("type"), X(type));
	if ( (serial = vh->getDataloggerSerial()) != unknown_int) {
	    recorderElem->setAttribute(X("serial_number"), X(serial));
	}
    }
	
    DOMElement * chanElem = v0_doc->createElement(X("channel"));	
    recorderElem->appendChild(chanElem);
    chanElem->setAttribute(X("number"), X(vh->getChannelNumber()));
    chanElem->setAttribute(X("recorded"), X(vh->getDataloggerNumRecordedChannels()));
    chanElem->setAttribute(X("total"), X(vh->getDataloggerNumTotalChannels()));

    // NON-STANDARD - adding <code> element to channel
    DOMElement* chancodeElem = v0_doc->createElement(X("code"));
    chanElem->appendChild(chancodeElem);
    chancodeElem->setAttribute(X("source"), X("IRIS"));
    DOMText* chancodeDataVal = v0_doc->createTextNode(X(vh->getChannelCode()));
    chancodeElem->appendChild(chancodeDataVal);

    /* hardcoding for solid-state as this is for purely digital data */
    DOMElement * mediumElem = v0_doc->createElement(X("medium"));	
    recorderElem->appendChild(mediumElem);
    DOMText*    mediumDataVal = v0_doc->createTextNode(X("solid-state"));
    mediumElem->appendChild(mediumDataVal);

    /* NOT including azimuth or digitizer_yStep elements */

    /* hardcoding timing for GPS data as there are no others really being used in CISN */
    DOMElement * timeElem = v0_doc->createElement(X("timing"));	
    recorderElem->appendChild(timeElem);
    DOMText*    timeDataVal = v0_doc->createTextNode(X("GPS"));
    timeElem->appendChild(timeDataVal);
	
    DOMElement * lsbElem = v0_doc->createElement(X("least_significant_bit"));	
    recorderElem->appendChild(lsbElem);
    lsbElem->setAttribute(X("units"), X("microvolts"));
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getDataloggerDigitizerLSB());
    DOMText*    lsbDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    lsbElem->appendChild(lsbDataVal);

    DOMElement * fsElem = v0_doc->createElement(X("full_scale_input"));	
    recorderElem->appendChild(fsElem);
    fsElem->setAttribute(X("units"), X("volts"));
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getDataloggerDigitizerFullScale());
    DOMText*    fsDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    fsElem->appendChild(fsDataVal);

    DOMElement * nbElem = v0_doc->createElement(X("number_of_bits"));	
    recorderElem->appendChild(nbElem);
    DOMText*    nbDataVal = v0_doc->createTextNode(X(vh->getDataloggerDigitizerNumBits()));
    nbElem->appendChild(nbDataVal);
	
    // SENSOR
    DOMElement * sensorElem = v0_doc->createElement(X("sensor"));	
    instElem->appendChild(sensorElem);
    { 
	char *type, *mfg;
	int serial;

	type=vh->getSensorType();
	if ( (mfg=strchr(type, ',')) != NULL) {
	    *mfg='\0';
	    mfg++; // skip comma
	    mfg++; // skip space
	    sensorElem->setAttribute(X("manufacturer"), X(mfg));
	}
	sensorElem->setAttribute(X("type"), X(type));
	if ( (serial = vh->getSensorSerial()) != unknown_int) {
	    sensorElem->setAttribute(X("serial_number"), X(serial));
	}
    }
    DOMElement * orientElem = v0_doc->createElement(X("orientation"));	
    sensorElem->appendChild(orientElem);
    orientElem->setAttribute(X("degrees_from_north"), X(vh->getSensorAzimuth()));
    orientElem->setAttribute(X("degrees_from_vertical"), X(vh->getSensorDip()+90));

    DOMElement * nfElem = v0_doc->createElement(X("natural_frequency"));	
    sensorElem->appendChild(nfElem);
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getSensorNaturalFrequency());
    DOMText*    nfDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    nfElem->appendChild(nfDataVal);

    DOMElement * dampElem = v0_doc->createElement(X("damping_coefficient"));	
    sensorElem->appendChild(dampElem);
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getSensorDamping());
    DOMText*    dampDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    dampElem->appendChild(dampDataVal);

    DOMElement * sensElem = v0_doc->createElement(X("sensitivity"));	
    sensorElem->appendChild(sensElem);
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getSensorSensitivity());
    DOMText*    sensDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    sensElem->appendChild(sensDataVal);
    sensElem->setAttribute(X("units"), X("volts/g"));
	
    DOMElement * fsoElem = v0_doc->createElement(X("full_scale_output"));	
    sensorElem->appendChild(fsoElem);
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getSensorFullScaleV());
    DOMText*    fsoDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    fsoElem->appendChild(fsoDataVal);
    fsoElem->setAttribute(X("units"), X("volts"));

    DOMElement * fsgElem = v0_doc->createElement(X("full_scale_sensing_capability"));	
    sensorElem->appendChild(fsgElem);
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getSensorFullScaleG());
    DOMText*    fsgDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    fsgElem->appendChild(fsgDataVal);
    fsgElem->setAttribute(X("units"), X("g"));

    DOMElement * gainElem = v0_doc->createElement(X("gain"));	
    sensorElem->appendChild(gainElem);
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getSensorGain());
    DOMText*    gainDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    gainElem->appendChild(gainDataVal);

    /* PROCESSING ELEMENT */
    /* example: 

    <processing stage="preliminary" index="v0">
    <sample_interval units="msec">10.000000</sample_interval>
    <raw>
    <start date="2001-10-07" time="17:29:05" zone="UTC"/>
    <max units="counts">39664.0</max>
    <seconds_to_max>5.270000</seconds_to_max>
    <length units="sec">36.000000</length>
    </raw>
    </processing>

    */
    DOMElement* processElem = v0_doc->createElement(X("processing"));
    rootElem->appendChild(processElem);
    processElem->setAttribute(X("stage"), X("preliminary"));
    processElem->setAttribute(X("index"), X("V0"));

    DOMElement * siElem = v0_doc->createElement(X("sample_interval"));	
    processElem->appendChild(siElem);
    siElem->setAttribute(X("units"), X("sec"));
    sprintf(tmpstr, vh->getRealFormatPrintf(), (double) 1.0/vh->getSampleRate());
    DOMText*    siDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    siElem->appendChild(siDataVal);

    DOMElement * rawElem = v0_doc->createElement(X("raw"));	
    processElem->appendChild(rawElem);

    DOMElement * startElem = v0_doc->createElement(X("start"));	
    rawElem->appendChild(startElem);
    sprintf(tmpstr, "%4d-%02d-%02d", vh->getStartYear(), vh->getStartMonth(), vh->getStartMDay());
    startElem->setAttribute(X("date"), X(tmpstr));
    sprintf(tmpstr, "%02d:%02d:%02d.%04d", vh->getStartHour(), vh->getStartMin(), 
	    vh->getStartSecs(), vh->getStartMsecs());
    startElem->setAttribute(X("time"), X(tmpstr));
    startElem->setAttribute(X("zone"), X("UTC"));

    DOMElement * maxElem = v0_doc->createElement(X("max"));	
    rawElem->appendChild(maxElem);
    maxElem->setAttribute(X("units"), X("counts"));
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getDataMaxValue());
    DOMText*    maxDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    maxElem->appendChild(maxDataVal);
	
    DOMElement * tomElem = v0_doc->createElement(X("seconds_to_max"));	
    rawElem->appendChild(tomElem);
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getDataTimeOfMax());
    DOMText*    tomDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    tomElem->appendChild(tomDataVal);

    DOMElement * lenElem = v0_doc->createElement(X("length"));	
    rawElem->appendChild(lenElem);
    lenElem->setAttribute(X("units"), X("sec"));
    sprintf(tmpstr, vh->getRealFormatPrintf(), vh->getDataLength());
    DOMText*    lenDataVal = v0_doc->createTextNode(X(_stripLeadingSpaces(tmpstr)));
    lenElem->appendChild(lenDataVal);
    /* DATALESS ELEMENT */
    if (vh->isDataless()) {
	DOMElement* datalessElem = v0_doc->createElement(X("dataless"));
	rootElem->appendChild(datalessElem);
	DOMElement* dlStartElem = v0_doc->createElement(X("start_date"));
	datalessElem->appendChild(dlStartElem);
	DOMText*    dlStartDataVal = v0_doc->createTextNode(X(vh->getDatalessStart()));
	dlStartElem->appendChild(dlStartDataVal);
	DOMElement* dlEndElem = v0_doc->createElement(X("end_date"));
	datalessElem->appendChild(dlEndElem);
	DOMText*    dlEndDataVal = v0_doc->createTextNode(X(vh->getDatalessEnd()));
	dlEndElem->appendChild(dlEndDataVal);
    }
	
    /* DATA ELEMENT */ 
    // not filling this in as yet....why bother at this point
    DOMElement* dataElem = v0_doc->createElement(X("data"));
    rootElem->appendChild(dataElem);
}

////////////////////////////////////////////////////////
// DESTRUCTOR
v0_xml::~v0_xml()
{
    v0_doc->release();
    XMLPlatformUtils::Terminate();
}

////////////////////////////////////////////////////////
// does a coarse difference (are they string equivalent)
//  0 if false
//  1 if true
int _equalTextElements(DOMNode *n1, DOMNode *n2) {

    XMLCh *t1, *t2;
    int return_value=0;


    t1 = _getTextOfElement(n1);
    t2 = _getTextOfElement(n2);

    // check for empty but equal
    if (t1 == NULL && t2 == NULL) return 1;

    if (t1 != NULL && t2 != NULL) {
	return_value = XMLString::compareIString(t1, t2);
    } 
    if (return_value == 0) return 1;
    return 0;
}
////////////////////////////////////////////////////////
// are the elements values different numerically?
//
#define REAL_DIFF 0.001
bool _realDiff(char *value1, char *value2, char *nodeName, bool verbose) {
    double dvalue1, dvalue2, ddiff;
    int span;
    bool not_equal = true;

    if (value1 == NULL &&  value2 != NULL) return not_equal;
    if (value1 != NULL &&  value2 == NULL) return not_equal;

    if ( value1 !=NULL && (span = strspn(value1, "0123456789.-")) != 0) {
	// we have a number
	dvalue1 = atof(value1);
	dvalue2 = atof(value2);
	ddiff = fabs(dvalue1-dvalue2);
	if (ddiff < REAL_DIFF) {
	    not_equal = false;
	} else {
	    if (verbose) 
		cout << "'"<< nodeName<< "' value differs between v0 and XML file by "
		     << ddiff << " "
		     <<endl;
	}
    }
    return not_equal;
}
// diffAttributes(n1, n2) does a check of all attributes for a given node to
// see if they differ or exist
// returns true if they differ in anyway (missing attributes is a difference too)
bool _diffAttributes(DOMNode *n1, DOMNode *n2, bool verbose) {
	
    bool n1has, n2has;
    char *nodeName;

    bool return_value=false;
	
    n1has = n1->hasAttributes();
    n2has = n2->hasAttributes();
    if (!n1has && !n2has) return false;

    nodeName = XMLString::transcode(n1->getNodeName());
    if (n1has && !n2has) {
	if (verbose) cout << "Element <"<< nodeName<< "> has attributes in V0 file but not in XML file"<<endl;
	XMLString::release(&nodeName);
	return true;
    }
    if (!n1has && n2has) {
	if (verbose) cout << "Element <"<< nodeName<< "> has attributes in XML file but not in V0 file"<<endl;
	XMLString::release(&nodeName);
	return true;
    }
	
    // if we reach here, both elements have attributes
    DOMNamedNodeMap *n1Attributes = n1->getAttributes();
    DOMNamedNodeMap *n2Attributes = n2->getAttributes();

    // loop over n1 and search into n2 for comparable values
    int nSize = n1Attributes->getLength();
    for(int i=0;i<nSize;++i) {
	const XMLCh *xsv2;
	char *value2;

	DOMAttr *n1AttributeNode = (DOMAttr*) n1Attributes->item(i);
	// get attribute n1 name and value
	const XMLCh *xsn1 = n1AttributeNode->getName();
	char *name1 = XMLString::transcode(xsn1);
	const XMLCh *xsv1 = n1AttributeNode->getValue();
	char *value1 = XMLString::transcode(xsv1);

	// find the matching value in the other node map
	DOMAttr  *n2AttributeNode = (DOMAttr *) n2Attributes->getNamedItem(xsn1);
	if (n2AttributeNode == NULL) {
	    // the attribute named name is missing from node2
	    if (verbose) cout << "Element <" << nodeName << "> has attribute '"<< name1 
			      << "' in V0 file but not in XML file" << endl;
	} else {
	    // check the attribute value difference
	    xsv2 = n2AttributeNode->getValue();
	    value2 = XMLString::transcode(xsv2);
	    if (XMLString::compareIString(xsv1, xsv2) != 0) {
		// text is different, check numeric value diff
		if(_realDiff(value1, value2, name1, verbose)) {
		    if (verbose) {
			cout << "Element <" << nodeName << "> has attribute '"<< name1
			     << "' with value of '" << value1<<"' in V0 file " << endl;
			cout << "Element <" << nodeName << "> has attribute '"<< name1
			     << "' with value of '" << value2<<"' in XML file " << endl;
		    }
		    return_value = true;
		} 
	    } 
	    XMLString::release(&value2);
	}
	XMLString::release(&name1);
	XMLString::release(&value1);
    }

    XMLString::release(&nodeName);
    return return_value;
}


// diffElements(n1, n2) does a recursive difference of the elements
//
// returns false if all elements/children are equal
// returns true if all elements/children are NOT equal
//  if verbose is set to true, the differences are echoed to stdout

bool _diffElements(DOMNode *n1, DOMNode *n2, bool verbose) {

    char *nodeName;
    bool not_equal = false;

    nodeName = XMLString::transcode(n1->getNodeName());
    // first check to see if the text of the elements is equivalent
    if (!_equalTextElements(n1, n2)) {

	XMLCh *Xs1 = _getTextOfElement(n1);
	XMLCh *Xs2 = _getTextOfElement(n2);
	char *value1, *value2;

	not_equal = true;
	if (Xs1 != NULL)  value1 = XMLString::transcode(Xs1);
	if (Xs2 != NULL)  value2 = XMLString::transcode(Xs2);

	// now check to see if the values are numeric
	if (Xs1 != NULL && Xs2 != NULL) {
	    not_equal = _realDiff(value1, value2, nodeName, verbose);
	} else {
	    if (verbose) cout << "Element <"<< nodeName<< "> value differs between v0 and XML file."<<endl;
	}


	if (verbose && not_equal) {
	    if (Xs1 != NULL) {
		cout << "Element <"<< nodeName<< "> from v0 file text value='"<< value1 <<"'"<<endl;
	    } else {
		cout << "Element <"<< nodeName<< "> from v0 file text value=''"<<endl;
	    }
	    if (Xs2 != NULL) {
		cout << "Element <"<< nodeName<< "> from v0 XML file text value='"<< value2 <<"'"<<endl;
	    } else {
		cout << "Element <"<< nodeName<< "> from v0 XML file text value=''"<<endl;
	    }
	}
	if (Xs1 != NULL) XMLString::release(&value1);
	if (Xs2 != NULL) XMLString::release(&value2);
		
    }

    // stop right here if the text doesn't match up
    if (not_equal) {
	XMLString::release (&nodeName);
	return not_equal;
    }

    // now check the attributes to see if they match
    if ( (not_equal = _diffAttributes(n1, n2, verbose))) {
	XMLString::release (&nodeName);
	return not_equal;
    }


    // finally see if all the children's nodes match (recursively of course)....
    bool n1hasKids, n2hasKids;
    n1hasKids = n1->hasChildNodes();
    n2hasKids = n2->hasChildNodes();
    if (n1hasKids && !n2hasKids) {
	not_equal=true;
	if (verbose) cout << "Element <"<< nodeName<< "> from v0 file has children, but XML file does not."<<endl;
    }
    if (!n1hasKids && n2hasKids) {
	not_equal=true;
	if (verbose) cout << "Element <"<< nodeName<< "> from XML file has children, but V0 file does not."<<endl;
    }
    if (not_equal) {
	XMLString::release (&nodeName);
	return not_equal;
    }

    DOMNode *child1, *child2;
    for (child1 = n1->getFirstChild(); child1 != 0 ; child1=child1->getNextSibling()) {
	const XMLCh *xscn = child1->getNodeName();
	if (child1->getNodeType() != DOMNode::ELEMENT_NODE) {
	    // for now we only care about elements as children
	    continue;
	}
	// find the ELEMENT that is in node2
	if ( (child2 = _findNode(xscn, n2)) == NULL) {
	    // element exists in V0, but not in XML file
	    if (verbose) {
		char *childName = XMLString::transcode(child1->getNodeName());
		cout << "Element <"<< nodeName<< ">  has child element <"
		     << childName << "> in V0 file, but is missing from XML file."<<endl;
		XMLString::release(&childName);
	    }
	} else {
	    // recursively diff these two elements
	    bool tmp_return = _diffElements(child1, child2, verbose);
	    if (tmp_return) not_equal = true;
	}
    }

    XMLString::release (&nodeName);
    return not_equal;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
// _doFindAndDiffElements(n1, n2, name, verbose) - 
//	finds and differences the elements specified by name
//	returns true if the elements are different in any way or if they are missing from either
//		of the nodes
//	returns false if the named element is found in both nodes and they are identical
//
bool _doFindAndDiffElements (DOMNode *n1, DOMNode *n2, const char *elementName, bool verbose) {
    DOMNode *v0n, *v0refn;
    bool  ret_value = false;

    v0n = _findNode(X(elementName), n1);
    v0refn = _findNode(X(elementName), n2);

    if (v0n == NULL) {
	ret_value = true;
	if (verbose) cout << "Element <" << elementName << "> is missing from the V0 file" << endl;
    }
    if (v0refn == NULL) {
	ret_value = true;
	if (verbose) cout << "Element <" << elementName << "> is missing from the XML file" << endl;
    }
    if (ret_value) return ret_value;

    ret_value = _diffElements(v0n, v0refn, verbose);
	
    return ret_value;
	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
// difference between select elements of THIS v0_xml DOC and another one passed in as an argument
//	outputs to standard output the differences found for select elements/attributes if verbose is
//	set to true;
//	RETURNS: 0 if no difference within tolerances
//	         1 if there is a difference for any element
//	
//	
int v0_xml::diff(v0_xml *v0ref, bool verbose) {

    DOMNode *startingNodeV0, *startingNodeXML;
    int return_value=0;
    bool r1, r2, r3, r4;

    startingNodeV0 = v0_doc->getDocumentElement();
    startingNodeXML = v0ref->getStartingNode();

    if (verbose) cout <<"Differencing <record> element"<<endl;
    if (!(r1=_doFindAndDiffElements(startingNodeV0, startingNodeXML, "record",  verbose)) && verbose)
	cout << " No difference found"<<endl;

    if (verbose) cout <<"Differencing <station> element"<<endl;
    if (!(r2=_doFindAndDiffElements(startingNodeV0, startingNodeXML, "station", verbose)) && verbose)
	cout << " No difference found"<<endl;

    if (verbose) cout <<"Differencing <instrument> element"<<endl;
    if (!(r3=_doFindAndDiffElements(startingNodeV0, startingNodeXML, "instrument", verbose)) && verbose)
	cout << " No difference found"<<endl;

    if (verbose) cout <<"Differencing <processing>/<sample_interval> element"<<endl;
    if (!(r4=_doFindAndDiffElements(startingNodeV0, startingNodeXML, "sample_interval", verbose)) && verbose)
	cout << " No difference found"<<endl;

    if (r1 || r2 || r3 || r4) {
	return_value=1;
    } else {
	return_value=0;
    }

    return return_value;
}

void v0_xml::outputToFile(char *filename) {


    try {

	XMLCh*  gOutputEncoding = 0;   
	DOMWriter *serializer;
	XMLFormatTarget  *myFormTarget;
        XMLCh tempStr[100];
        XMLString::transcode("LS", tempStr, 99);

        DOMImplementation *impl = DOMImplementationRegistry::getDOMImplementation(tempStr);

	serializer = impl->createDOMWriter();


	serializer->setEncoding(gOutputEncoding);
	if (serializer->canSetFeature(XMLUni::fgDOMWRTFormatPrettyPrint, true)) {
            serializer->setFeature(XMLUni::fgDOMWRTFormatPrettyPrint, true);              
	}

	myFormTarget = new LocalFileFormatTarget(filename);

	const DOMNode *node=v0_doc->getDocumentElement();

	serializer->writeNode(myFormTarget, *node);
	delete serializer;
	delete myFormTarget;
    }
    catch (XMLException& e)
    {
	cerr << "An error occurred during creation of output transcoder. Msg is:"
	     << endl
	     << StrX(e.getMessage()).localForm() << endl;
    }
}

void v0_xml::printElementInfo(const char *n) {

    DOMNode *desiredNode, *startingNode;
	
    startingNode = v0_doc->getDocumentElement();
    if (!startingNode) {
	return;
    }
    desiredNode= _findNode(X(n), startingNode);
    if (desiredNode) {
	char *name = XMLString::transcode(desiredNode->getNodeName());
	cout << "Element name " << name<< " found"<<endl;
	XMLString::release(&name);

	if(desiredNode->hasAttributes()) {
	    // get all the attributes of the node
	    DOMNamedNodeMap *pAttributes = desiredNode->getAttributes();
	    int nSize = pAttributes->getLength();
	    cout <<"\tAttributes" << endl;
	    cout <<"\t----------" << endl;
	    for(int i=0;i<nSize;++i) {
		DOMAttr *pAttributeNode = (DOMAttr*) pAttributes->item(i);
		// get attribute name
		char *name = XMLString::transcode(pAttributeNode->getName());

		cout << "\t" << name << "=";
		XMLString::release(&name);

		// get attribute type
		name = XMLString::transcode(pAttributeNode->getValue());
		cout << name << endl;
		XMLString::release(&name);
	    }
	}
	// now find the text
	//                for (DOMNode *child = desiredNode->getFirstChild(); child != 0 ; child=child->getNextSibling()) {
	//                        if (child->getNodeType() == DOMNode::TEXT_NODE) {
	//				char *value = XMLString::transcode(child->getNodeValue());
	//				if (value) {
	//					cout << "Text value '" << value<< "'"<<endl;
	//               			XMLString::release(&value);
	//				}
	//                       }
	//              }
	XMLCh *Xs = _getTextOfElement(desiredNode);
	if (Xs) {
	    char *value = XMLString::transcode(Xs);
	    cout <<"Text value '" << value<< "'"<<endl;
	    XMLString::release(&value);
	}

    } else {
	cout << "Element " << n <<" not found " << endl;
    }
}
