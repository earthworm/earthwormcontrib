#ifndef FILELIST_H
#define FILELIST_H

#include <stdio.h>
#define FILEFILELIST 0
#define CMDLINEFILELIST 1

class FileList {
    int mode;
    FILE *fptr;
    int cmdline_index, start_flag;
    char **argv;
    int argc;
    char filename_buf[1024];

 public:
    FileList(int, int, char **);
    FileList(char*);
    char *getNextFile();
};

#endif
