.TH V02XML 1 "October 2011" "ISTI/Caltech/UCB" "ISTI MANUAL PAGES"
.SH NAME
v02xml \- convert a COSMOS V0 format file into XML header file
.SH SYNOPSIS
\fBv02xml [-v][-i \fIxml_input_to_check\fB][-x \fIxml_output_file\fB]\
[-N \fInetcode\fB][-S \fIstation_name\fB][-C \fIchannel_name\fB][-L \fIloc_code\fB][-F \fIfilelist\fB] \fIV0_file ...
.SH DESCRIPTION
\fBv02xml\fR converts COSMOS version 1.20 V0 format files (ASCII) into XML.
An XML file is created with the v0name.c.xml extension,
where v0name is the name of the v0 file and c is filled with the channel number for the file.
If no output xml file is specfied (using the -x option). If an .xml file is
desired for any V0 file, the -X option (new in v1.07) should be used. 
.LP
Introduced in version 1.03 is the ability to output and difference the XML
V0 header format recently defined by Mindy Squibb at COSMOS Virtual Data Center
at the University of California at Santa Barbara (UCSB). The XML format is a
work in progress and subject to revision. It has not been accepted as a standard
by the COSMOS group as yet. An XML file containing the header
information in the input V0 file will be output if the -x option is provided on the
command line. If the -i option is used, the V0 header and the XML file input will
be differenced and differences echoed to standard output.
.SH MULTIPLEXED V0
If multiplexed V0 files are provided, each xml file is prepended 
with the channel number. Multiplexed dataless files are allowed and will be
processed into individual .xml files automatically.
.SH OPTIONS
.TP
-H -h -?
Show the usage information.
.TP
-v
Spit verbose messages out about the differencing processing.
.TP
-i \fIxml_input_to_check\fR
This option provides for input a valid V0 XML file, created from an earlier run of v02xml,
as input for difference checking against the V0 header file. Differences are output
to standard output. The only elements in the XML file that are differenced are 
<record>, <station>, <instrument> and <processing>/<sample_interval>. 
This option can only be used against a single V0 file.
.TP
-x \fIxml_output_file\fR
This option produces an V0 XML file from the input V0 file. The output conforms to the
COSMOS XML format in progressive development at UCSB. This option can only be used against a
single V0 file.
.TP
-N \fInetcode\fR
Add the Network Code specified to the XML (2 char limit). If this is not
specified then the network is taken from the Text Header line 5 of the V0 header.
.TP
-S \fIstation_name\fR
Add the Station Name  specified to the XML (5 char limit). If this is
not given, the value is taken from the Station Code section of the V0 text header
and if that is blank, then the Station Number in the Integer block of
the V0 header (I8) is used.
.TP
-C \fIchannel_name\fR
Set the Channel Name to that specified in the option (3 char limit). If this is not
provided on the command line, then the name is matched to HN_ where the last char is
Z, N, or E depending upon the orientation found in the COSMOS V0 Integer Header (I54).
For appropriate channel names, please refer to the SEED manual Appendix A. For Caltech
data, all FBA data are named HL_. For CGS, all data from FBA's are named HN_.

For dataless V0 files, the \fIchannel_name\fR can be a comma separated list of
channel names.

.TP
-L \fIloc_code\fR
Set the location code to that specfied in the option (2 char limit). If no location
code is provided, the default value of [space][space] \"  \" is inserted.
.TP
-F \fIFileList_file\fR
This option tells the program to operate on the list of filenames provided in the
file given.
.SH DIAGNOSTICS
.SH NOTES
.SH SEE ALSO
.LP
XML Schema reference from Mindy Squibb
.SH REFERENCES
.LP
\fICOSMOS Strong Motion Data Format\fR Ver 1.20, August 15, 2001
.SH AUTHOR
Paul Friberg
