#
# Makefile for v02wws_ack
#
#

CFLAGS = -D_REENTRANT $(GLOBALFLAGS)

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

CFLAGS +=  -g

all: v02wws_ack


BINARIES = v02wws_ack.o v0_cosmos.o v0_multiplexor.o \
        $L/mem_circ_queue.o $L/chron3.o $L/kom.o \
	$L/getsysname_ew.o $L/getutil.o $L/logit_mt.o $L/transport.o \
	$L/sleep_ew.o $L/socket_ew.o $L/socket_ew_common.o $L/dirops_ew.o \
	$L/time_ew.o $L/threads_ew.o $L/sema_ew.o

v02wws_ack: $(BINARIES) 
	CC -o $B/v02wws_ack $(BINARIES) -lnsl -lsocket -mt -lposix4 -lthread -lm


%.o: %.C
	CC $(CFLAGS) -c  $(@F:.o=.C) -o $@ 

lint:
	lint v02wws_ack.C   $(GLOBALFLAGS)


# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/v02wws_ack* 

