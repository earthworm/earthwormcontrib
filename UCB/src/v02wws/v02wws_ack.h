#ifndef V02WWS_ACKH
#define V02WWS_ACKH
		
/* the constant below map into the v02wws.desc error file */
const int V02WWS_DEATH_EW_PUTMSG  = 3;

#define VER_NO "0.0.1 -  2011-10-18"
/* keep track of version notes here */

/*****************************************************************************
 *  constants                                                                *
 *****************************************************************************/

#define TRUE  1
#define FALSE  0
#define MAXCHANS  32    /* Max number of channels in a v0 file         */
#define FILE_NAM_LEN  500

/*****************************************************************************
 *  Define the structure for keeping track of a buffer of trace data.        *
 *****************************************************************************/

typedef struct _DATABUF {
    char sncl[20];     /* SNCL for this data channel                           */
    long *buf;         /* The raw trace data; native byte order                */
    long bufLen;       /* The length of the buffer                             */
    int bufptr;        /* The nominal time between sample points               */
    double starttime;  /* time of first sample in raw data buffer              */
    double endtime;    /* time of last sample in raw data buffer               */
} DATABUF;

#endif
