/* ====================================================================
 * 
 * Copyright (C) 2004 Instrumental Software Technologies, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code, or portions of this source code,
 *    must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. All advertising materials mentioning features or use of this
 *    software must display the following acknowledgment:
 *    "This product includes software developed by Instrumental
 *    Software Technologies, Inc. (http://www.isti.com)"
 *   
 * 4. If the software is provided with, or as part of a commercial
 *    product, or is used in other commercial software products the
 *    customer must be informed that "This product includes software
 *    developed by Instrumental Software Technologies, Inc. 
 *    (http://www.isti.com)"
 *   
 * 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
 *    must not be used to endorse or promote products derived from
 *    this software without prior written permission. For written
 *    permission, please contact "info@isti.com".
 *
 * 6. Products derived from this software may not be called "ISTI"
 *    nor may "ISTI" appear in their names without prior written
 *    permission of Instrumental Software Technologies, Inc.
 *
 * 7. Redistributions of any form whatsoever must retain the following
 *    acknowledgment:
 *    "This product includes software developed by Instrumental
 *    Software Technologies, Inc. (http://www.isti.com/)."
 *
 * THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
 * TECHNOLOGIES, INC. ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
 * INC.  OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ====================================================================
 
  A current version of the software can be found at 
  <A HREF="www.isti.com">http://www.isti.com</A>

  Bug reports and comments should be directed to 
  Instrumental Software Technologies, Inc. at info@isti.com

 ====================================================================*/

#ifndef V0_COSMOS_H
#define V0_COSMOS_H

#define V0_NUM_INTS 100
#define V0_NUM_REALS 100

#define COSMOS_VERSION 120
#include <stdio.h>
#include <iostream>

class v0_cosmos {

 private:
    bool    is_dataless;
    char *  file_name;
    char *  net_code;
    char *  sta_name;
    char *  chan_code;	/* seed format */
    char *  sensor_type;
    char    station_description[41];
    char *  comment_lines;
    char    real_format[28];
    char *  dataless_start;
    char *  dataless_end;
    int     sta_number;
    int     chan_num;
    int	    orientation;
    int	    integers[V0_NUM_INTS];
    int     number_samples;
    int     unknown_int;
    long *  data;
    double  reals[V0_NUM_REALS];
    double  unknown_real;

    void   clear();

 public:

    class format_exception {
    private:
	const char * err_string;
    public:
	format_exception(const char  *str) {
	    err_string = str;
	};
	~format_exception() {};
	void display() {
	    std::cerr << err_string << std::endl;
	};
    };

    v0_cosmos() throw (format_exception);
    v0_cosmos(char *filename) throw (format_exception);
    ~v0_cosmos();

    bool   parse(FILE *fptr) throw (format_exception);
    void   setFilename(char *filename);
    int    getStageIndex();
    int    getSampleRate();
    int    getNumSamples();
    char * getNetworkCode();
    char * getOwnerNetworkCode();
    void   setStationCode(char * sta);	// only setter method, maybe removed
    char * getStationCode();
    char * getStationDescription();
    char * getChannelCode();
    void   setChannelCode(char *chan)  throw (format_exception);
    double getStationLatitude();
    double getStationLongitude();
    double getStationElevation();
    int    getStationNumber();
    int    getStartYear();
    int    getStartYDay();
    int    getStartMonth();
    int    getStartMDay();
    int    getStartHour();
    int    getStartMin();
    int    getStartSecs();
    int    getStartMsecs();

    int    getCOSMOSversion();
    int    getUnknownIntVal();
    double getUnknownRealVal();
    char * getCommentLines();
    char * getRealFormatPrintf();
    char * getParameterType();

    int    getDataloggerSerial();
    char * getDataloggerType();
    int    getDataloggerNumTotalChannels();
    int    getDataloggerNumRecordedChannels();
    int    getDataloggerDigitizerNumBits();
    double getDataloggerDigitizerLSB();
    double getDataloggerDigitizerFullScale();
    int    getChannelNumber();

    char * getSensorType();
    int    getSensorSerial();
    double getSensorFullScaleG();
    double getSensorFullScaleV();
    double getSensorNaturalFrequency();
    double getSensorSensitivity();
    double getSensorDamping();
    double getSensorGain();
    int    getSensorAzimuth();
    int    getSensorDip();

    bool   isDataless();
    char * getDatalessStart();
    char * getDatalessEnd();

    long * getData();
    double   getDataMaxValue();		// returns counts value
    double   getDataTimeOfMax();	// returns seconds from start
    double   getDataLength();	// returns seconds
    void   switchDataPolarity();
};

#endif
