#ifndef V0_XML_H
#define V0_XML_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/XMLUni.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOMImplementationLS.hpp>
#include <xercesc/dom/DOMWriter.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

#include <stdio.h>
#include "v0_cosmos.h"
#include "StrX.h"

#include "DOMTreeErrorReporter.h"

XERCES_CPP_NAMESPACE_USE

class v0_xml {

 private:
    DOMDocument* v0_doc;

    int init();
    DOMNode * getStartingNode();
 public:
    v0_xml(v0_cosmos *);
    v0_xml(char * file);
    ~v0_xml();
    void outputToFile(char * );
    int  diff(v0_xml *, bool );

    void printElementInfo(const char *);
};


#endif
