/* ====================================================================
 * 
 * Copyright (C) 2004 Instrumental Software Technologies, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code, or portions of this source code,
 *    must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. All advertising materials mentioning features or use of this
 *    software must display the following acknowledgment:
 *    "This product includes software developed by Instrumental
 *    Software Technologies, Inc. (http://www.isti.com)"
 *   
 * 4. If the software is provided with, or as part of a commercial
 *    product, or is used in other commercial software products the
 *    customer must be informed that "This product includes software
 *    developed by Instrumental Software Technologies, Inc. 
 *    (http://www.isti.com)"
 *   
 * 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
 *    must not be used to endorse or promote products derived from
 *    this software without prior written permission. For written
 *    permission, please contact "info@isti.com".
 *
 * 6. Products derived from this software may not be called "ISTI"
 *    nor may "ISTI" appear in their names without prior written
 *    permission of Instrumental Software Technologies, Inc.
 *
 * 7. Redistributions of any form whatsoever must retain the following
 *    acknowledgment:
 *    "This product includes software developed by Instrumental
 *    Software Technologies, Inc. (http://www.isti.com/)."
 *
 * THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
 * TECHNOLOGIES, INC. ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
 * INC.  OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ====================================================================
 
  A current version of the software can be found at 
  <A HREF="www.isti.com">http://www.isti.com</A>

  Bug reports and comments should be directed to 
  Instrumental Software Technologies, Inc. at info@isti.com

 ====================================================================*/

#ifndef V0_MULTIPLEXOR_H
#define V0_MULTIPLEXOR_H

#include <iostream>
#include "v0_cosmos.h"

class v0_multiplexor {
	v0_cosmos *v0;
	FILE *fptr;
	char *file_name;
	bool v0FileStarted;

public:
	class file_io_exception {
	    public:
		file_io_exception(const char *s) {
			std::cerr<< "file IO exception: " << s <<std::endl;
		};
	       ~file_io_exception() {};
	};
	v0_multiplexor(char * filename) throw (file_io_exception);
	~v0_multiplexor();
	v0_cosmos * getNext();
};
	
#endif
