#ifndef lint
static char id[] = "@(#) $Id: datasock_import.c 241 2006-10-03 20:20:45Z paulf $";
#endif
/* 
 *   datasock_import.c:  Program to receive MiniSEED records from datalog
 *              via a socket, and put them into a transport ring.

This is NOT a true EW module in the sense that
a.  It does not exchange heartbeats with datalog (at this time), FIXED
b.  Input from datalog are raw MiniSEED records, not using EW encapsulation.

04/05/05 PNL 1.1    Now handles EW TRACEBUF2 packet (includes location code)
98/09/21 DSN 1.0.1  Fixed endtime calculation for EW tracebuf packet.
97/05/12 DSN 1.0    Initial coding


 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <fcntl.h>
#include <math.h>
#include <time.h> 
#include <sys/socket.h>
#include <netdb.h>

#ifdef _SOLARIS
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <wait.h>
#include <thread.h>
#endif

#include <errno.h>
#include <signal.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <imp_exp_gen.h>
#include <trace_buf.h>
 
#include <qlib2.h>

#define MAX_LOGO  20

/* Functions in this source file 
 *******************************/
void  config  ( char * );
void  lookup  ( void );
void  output_status  ( unsigned char, short, char * );
int   import_filter  ( char *, DATA_HDR *);

#ifdef _SOLARIS                      /* SOLARIS ONLY */
void *Heartbeat( void * );
void *MessageReceiver( void * );
#endif

int   WriteToSocket  ( int, char *, MSG_LOGO * );

extern int  errno;
static  SHM_INFO  Region;      /* shared memory region to use for i/o    */
static char	*cmdname;	    /* Program name.			*/
static char perrmsg[80];	/* prefix for system error messages.	*/

/* Things to read or derive from configuration file
 **************************************************/
static char    RingName[MAX_RING_STR]; /* name of transport ring for i/o    */
static char    MyModName[MAX_MOD_STR]; /* speak as this module name/id      */
static int     LogSwitch;           /* 0 if no logfile should be written */
static int     HeartBeatInt;        /* seconds between heartbeats (to local
					ring)  */
static long    MaxMsgSize;          /* max size for input/output msgs    */
static int     MyAliveInt;	    /* Seconds between sending alive message to foreign sender */
static char    MyAliveString[100];  /* Text of above alive message */

static char    SenderIpAdr[32];      /* Foreign sender's address, in dot notation */
static int     SenderPort;          /* Server's well-known port number   */
static int     SenderHeartRate;     /* Expect alive messages this often from foreign sender */
static char    SenderHeartText[100]; /* Text making up the sender's heart beat message */
static char    RemoteModName[MAX_MOD_STR];   /* Remote module name.    */
static char    RemoteNetName[MAX_INST_STR];   /* Remote network name.   */
static int     ImportEWTrace;	    /* Flag to Import EW TRACE into ring.	*/
static int     ImportMSTrace;	    /* Flag to import MS TRACE into ring.	*/
MSG_LOGO       PutAsLogo;           /* logo to be used for placing received messages into ring. */
				    /* May be superceeded in the message processing routine
				      "import_filter" */

/* Globals: timers, etc, used by both threads */
/**********************************************/
#define CONNECT_WAIT_DT 10      /* Seconds wait between connect attempts */
#define THEAD_STACK_SIZE 8192   /* Implies different things on different systems !! */
		/* on os2, the stack grows dynamically beyond this number if needed, but likey */
		/* at the expesene of performance. Solaris: not sure, but overflow might be fatal */
time_t LastServerBeat;          /* times of heartbeats from the server machine */
time_t MyLastInternalBeat;  	/* time of last heartbeat into the local Earthworm ring */
time_t MyLastSocketBeat;   	/* time of last heartbeat to server - via socket */
int HeartThreadStatus = 0;  		/* goes negative if server croaks. Set by heartbeat thread */
int MessageReceiverStatus =0;   /* status of message receiving thread: -1 means bad news */
char* MsgBuf;		        /* incoming message buffer; used by receiver thread */
int  Sd;                        /* Socket descriptor    */
unsigned  TidHeart;		/* thread id. was type thread_t on Solaris! */
unsigned  TidMsgRcv;		/* thread id. was type thread_t on Solaris! */
char Text[256];			/* quite crude - array for error text */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o     */
static unsigned char MyInstId;        /* local installation id             */
static unsigned char MyModId;       /* Module Id for this program        */
static unsigned char TypeHeartBeat; 
static unsigned char TypeError;
static unsigned char TypeTrace;
static unsigned char TypeMSTrace;
static unsigned char RemoteModId;	    /* Remote module id.    */
static unsigned char RemoteNetId;	    /* Remote network id.   */

static int terminate_proc = 0;

/* Error messages used by import 
 *********************************/
#define  ERR_SOCK_READ       0   /* error reading from socket  */
#define  ERR_TOOBIG          1   /* retreived msg too large for buffer     */
#define  ERR_TPORTPUT        2   /* error putting message into ring */
#define  ERR_SERVER_DEAD     3   /* server machine heartbeats not received on time */
#define  ERR_GARBAGE_IN     4   /* something other than STX after ETX */

#define	MIN_BLKSIZE 256		    /* Minimum SEED blocksize.	*/

/************************************************************************/
/*  finish_handler:							*/
/*	Signal handler -  sets terminate flag so we can exit cleanly.	*/
/************************************************************************/
void finish_handler(int sig)
{
    signal (sig,finish_handler);    /* Re-install handler (for SVR4)	*/
    terminate_proc = 1;
    return;
}

main( int argc, char **argv )
{
   long  nchar;			     /* counter for above buffer */
   int nr;			     /* number of characters read from socket */
   char chr;   			     /* character read from socket */
   int  on = 1;
   int  clientLen;
   u_long addr;			     /* binary form of sender ip address */
   struct hostent         *host;
   struct sockaddr_in insocket;
   time_t nowQuit;		     /* for deciding when to check for quitting */
   time_t lastQuit=0;
   time_t now;
   int state;
   /* to keep track of message assembly */
   char startCharacter=STX;	     /* frame start characer; from imp_exp_gen.h (ASCII STX) */
   char endCharacter=ETX;	     /* ditto frame end. (ASCII ETX) */
   int iret;			     /* misc. retrun values */
   int quit;
   int retryCount; /* to prevent flooding the log file */

   /* Check command line arguments 
   ******************************/
   cmdname = tail(argv[0]);
   if ( argc != 2 )
   	{
	fprintf( stderr, "Usage: %s <configfile>\n", cmdname );
	exit( 0 );
   	}
	   
   /* Read the configuration file(s)
   ********************************/
   config( argv[1] );

   /* Look up important info from earthworm.h tables
   ************************************************/
   lookup();

   /* Initialize name of log-file & open it 
   ***************************************/
   logit_init( cmdname, (short) MyModId, 256, LogSwitch );
   logit( "e" , "%s: Read command file <%s>\n", cmdname, argv[1] );

   /* Attach to Input/Output shared memory ring 
   *******************************************/
   terminate_proc = 0;
   signal (SIGINT,finish_handler);
   signal (SIGTERM,finish_handler);
   /* Set up a condition handler for SIGPIPE, since a write to a	*/
   /* close pipe/socket raises a alarm, not an error return code.	*/
   sigignore (SIGPIPE);
   tport_attach( &Region, RingKey );
   logit( "e", "%s: Attached to public memory region %s: %d\n", 
	  cmdname, RingName, RingKey );

   /* Allocate the message buffer
   ******************************/
    if ( MaxMsgSize < MIN_BLKSIZE ) {
	logit("e","%s: MaxMsgSize %d < MIN_BLKSIZE %d\n", cmdname, MaxMsgSize, MIN_BLKSIZE);
	exit( -1 );
    }
   if ( ( MsgBuf = (char *) malloc(MaxMsgSize) ) == (char *) NULL ) 
	{
	logit("e","%s: Cant allocate message buffer of %ld bytes\n",cmdname,MaxMsgSize);
	exit( -1 );
        }
 
   /* Initialize the socket system
   ******************************/
   SocketSysInit();

   /* fill in the hostent structure, given dot address as a char string
   ********************************************************************/
   if ((int)(addr = inet_addr(SenderIpAdr)) == -1)
      {
      logit( "e", "%s: inet_addr failed. Exiting.\n", cmdname );
      tport_detach( &Region );
      free(MsgBuf);
      exit( -1 );
      }

   host = gethostbyaddr((char *)&addr, sizeof (addr), AF_INET);
   if (host == NULL) 
     {
     logit( "e", "%s: gethostbyaddr failed for %s. Exiting.\n", cmdname, SenderIpAdr);
     tport_detach( &Region );
     free(MsgBuf);
     exit( -1 );
     }

   /* Stuff address and port into socket structure
   ********************************************/
   memset( (char *)&insocket, '\0', sizeof(insocket) );
   insocket.sin_family = AF_INET;
   insocket.sin_port   = htons( (short)SenderPort );
   memcpy( (char *)&insocket.sin_addr, host->h_addr_list[0], host->h_length );

   /* to prevent flooding the log file during long reconnect attempts */
   /********************************************************************/
   retryCount=0;  /* it may be reset elsewere */

				 /********************/
				      reconnect:
				 /********************/
   retryCount++; 
  
   /* Create a socket
   ****************/
   if ( ( Sd = socket( AF_INET, SOCK_STREAM, 0 ) ) == -1 )
      {
      sprintf (perrmsg, "%s: socket", cmdname);
      SocketPerror( perrmsg );
      logit( "et", "%s: Error opening socket.  Exiting\n", cmdname );
      SocketClose(Sd);
      free(MsgBuf);
      exit(0);
      }

   /* Try for a network connection - and keep trying forever !!
   ************************************************************/
   if(retryCount< 4)logit("et","%s: Trying to connect to %s on port %d\n",
			  cmdname,SenderIpAdr,SenderPort);
   if(retryCount==4)logit("et","%s: repetitions not logged\n",cmdname);
   if ( connect( Sd, (struct sockaddr *)&insocket, sizeof(insocket) ) == -1 )
      {

      /* Are we being told to quit */
      /*****************************/
      if ( tport_getflag( &Region ) == TERMINATE || terminate_proc )
	 {
         tport_detach( &Region );
         logit("et","%s: terminating on request\n",cmdname);
	 (void)KillThread(TidMsgRcv);
	 free(MsgBuf);
	 (void)KillThread(TidHeart);
         exit(0);	
	 }
      SocketClose( Sd );
      if(retryCount< 4)logit("et","%s: Failed to connect. Waiting\n",cmdname);
      if(retryCount==4)logit("et","%s: repetitions not logged\n",cmdname);
      sleep_ew(CONNECT_WAIT_DT*1000);
      goto reconnect;    /*** JUMP to reconnect ***/
      }
   logit("et","%s: Connected after %d seconds\n",cmdname,CONNECT_WAIT_DT*(retryCount-1));
   retryCount=0;
   state=0 ; /* which means we're initializing - no expectations */

   /* Start the heartbeat thread
   ****************************/
   HeartThreadStatus=0;  /* set it's status flag to ok */
   time(&MyLastInternalBeat); /* initialize our last heartbeat time */
   time(&MyLastSocketBeat);  /* initialize time of our heartbeat over socket */
   time(&LastServerBeat); /* initialize time of last heartbeat from serving machine */
   if ( StartThread( Heartbeat, (unsigned)THEAD_STACK_SIZE, &TidHeart ) == -1 )
      {
      logit( "e","%s: Error starting Heartbeat thread. Exiting.\n",cmdname );
      tport_detach( &Region );
      free(MsgBuf);
      exit( -1 );
      }

   /* Start the message receiver thread
   **********************************/
   MessageReceiverStatus =0; /* set it's status flag to ok */
   if ( StartThread( MessageReceiver, (unsigned)THEAD_STACK_SIZE, &TidMsgRcv ) == -1 )
      {
      logit( "e","%s: Error starting MessageReceiver thread. Exiting.\n",cmdname );
      tport_detach( &Region );
      free(MsgBuf);
      exit( -1 );
      }

   /* Working loop: check on server heartbeat status, check on receive thread health.
      check for shutdown requests. If things go wrong, kill all  threads and restart
   *********************************************************************************/
   quit=0; /* to restart or not to restart */
   while (1)
      {
      sleep_ew(1000); /* sleep one second. Remember, the receieve thread is awake */
      time(&now);

      /* How's the server's heart? */
      /*****************************/
      if (difftime(now,LastServerBeat) > (double)SenderHeartRate && SenderHeartRate !=0)
         {
	 output_status(TypeError,ERR_SERVER_DEAD," Server heartbeat lost. Restarting");
         quit=1; /*restart*/
	 }

      /* How's the receive thread feeling ? */
      /**************************************/
      if ( MessageReceiverStatus == -1)
	 {
	 logit("et","Ts: Receiver thread unhappy. Restarting\n",cmdname);
	 quit=1;
	 }

      /* How's the heartbeat thread feeling ? */
      /**************************************/
      if ( HeartThreadStatus == -1)
	 {
	 logit("et","%s: Heartbeat thread unhappy. Restarting\n",cmdname);
	 quit=1;
	 }

      /* Are we being told to quit */
      /*****************************/
      if ( tport_getflag( &Region ) == TERMINATE || terminate_proc )
	 {
         tport_detach( &Region );
         logit("et","%s: terminating on request\n",cmdname);
	 (void)KillThread(TidMsgRcv);
	 free(MsgBuf);
	 (void)KillThread(TidHeart);
         exit(0);	
	 }

      /* Any other shutdown conditions here */
      /**************************************/
      /* blah blah blah */

      /* restart preparations */
      /************************/
      if (quit == 1)
	 {
	 (void)KillThread(TidMsgRcv);
 	 (void)KillThread(TidHeart);
         SocketClose( Sd );
	 quit=0;
         goto reconnect;
         }

       }  /* end of working loop */
	 
	 	     
	
   }

/************************Messge Receiver Thread *********************
*          Listen for client heartbeats, and set global variable     *
*          showing time of last heartbeat. Main thread will take     *
*          it from there                                             *
**********************************************************************/
/*
Modified to read binary messages, alex 10/10/96: The scheme (I got it from Carl) is define
some sacred characters. Sacred characters are the start-of-message and end-of-message
framing characters, and an escape character. The sender's job is to cloak unfortunate bit
patters in the data which look like sacred characters by inserting before them an 'escape' 
character.  Our problem here is to recognize, and use, the 'real' start- and end-of- 
messge characters, and to 'decloak' any unfortunate look-alikes within the message body.
*/

#ifdef _SOLARIS
void * MessageReceiver( void *dummy )
#endif
{
    static char errText[256];
    int            nr;
    DATA_HDR      *hdr;
    BS            *bs = NULL;
    BLOCKETTE_HDR *bh = NULL;
    int goodpacket;
    int status;


   /* Tell the main thread we're ok 
   *******************************/
   MessageReceiverStatus=0;

    while (1) {
	goodpacket = 0;

	while(!goodpacket) {
	    nr = xread(Sd, (char *)MsgBuf, MIN_BLKSIZE);
	    if (nr == 0) {
		fprintf (stderr, "Found end of file on socket read: %d\n",nr);
		break;
	    }
	    if (nr < MIN_BLKSIZE) {
		fprintf (stderr, "Error reading from socket: expected %d got %d\n",
			 MIN_BLKSIZE, nr);
		goto suicide;
	    }
	    if ((hdr = decode_hdr_sdr ((SDR_HDR *)MsgBuf, MIN_BLKSIZE)) == NULL) {
		fprintf (stderr, "Error decoding SEED data hdr\n");
		break;
	    }
	    if (hdr->blksize > MaxMsgSize) {
		logit("et", "%s: MiniSEED Record size %d > MaxMsgSize %d; Exiting\n",
		      hdr->blksize, MaxMsgSize);
		exit (-1);
	    }
	    if (hdr->blksize > MIN_BLKSIZE) {
		nr = xread (Sd, (char *)MsgBuf + MIN_BLKSIZE, hdr->blksize-MIN_BLKSIZE);
		if (nr < hdr->blksize-MIN_BLKSIZE) {
		    fprintf (stderr, "Error reading SEED data\n");
		    break;
		}
	    }
	    goodpacket = 1;
	} /* End while !goodpacket */

	/* Process record.  */
	if (goodpacket) {
	    status = import_filter(MsgBuf,hdr); /* process the message via user-routine */
	    free_data_hdr (hdr);
	    if (status < 0) break;
	}
	else break;
    }

suicide:
   sprintf (perrmsg, "%s: recv()", cmdname);
   SocketPerror( perrmsg );
   sprintf(errText,"Bad socket read: %d\n",nr);
   logit("et",errText);
   MessageReceiverStatus=-1; /* file a complaint to the main thread */
   KillSelfThread(); /* main thread will restart us */
   logit("et","Fatal system error: Receiver thread could not KillSelf\n");
   exit(-1);

   }  /* end of MessageReceiver thread */



/***************************** Heartbeat **************************
 *           Send a heartbeat to the transport ring buffer        *
 *           Send a heartbeat to the server via socket            *
 *                 Check on our server's hearbeat                 *
 *           Slam socket shut if no Server heartbeat: that        *
 *            really shakes up the main thread                    *
 ******************************************************************/
#ifdef _SOLARIS
void *Heartbeat( void *dummy )
#endif
{
   int iret;
   MSG_LOGO 	reclogo;
   time_t now;

   /* once a second, do the rounds. If anything looks bad, set HeartThreadStatus to -1
      and go into a long sleep. The main thread should note that our status is -1,
      and launch into re-start procedure, which includes killing and restarting us. */
   while ( 1 )
      {
      sleep_ew(1000);
      time(&now);

      /* Beat our heart (into the local Earthworm) if it's time
       ********************************************************/
      if (difftime(now,MyLastInternalBeat) > (double)HeartBeatInt)
       	{	
        output_status( TypeHeartBeat, 0, "" );
	time(&MyLastInternalBeat);
        }

      /* Beat our heart (over the socket) to our server
      **************************************************/
      if (difftime(now,MyLastSocketBeat) > (double)MyAliveInt && MyAliveInt != 0)
	{
         reclogo.instid = MyInstId;
         reclogo.mod   = MyModId;
         reclogo.type  = TypeHeartBeat;
         if ( WriteToSocket( Sd, MyAliveString, &reclogo ) != 0 )
            {
            /* If we get an error, simply quit */
            sprintf( Text, "error sending alive msg to socket. Heart thread quitting\n" );
            output_status( TypeError, ERR_SOCK_READ, Text );
       	    logit("et",Text);
            HeartThreadStatus=-1;
	    KillSelfThread();  /* the main thread will resurect us */
	    logit("et","Fatal system error: Heart thread could not KillSelf\n");
	    exit(-1);
            }
	 MyLastSocketBeat=now;
          }	   
      }	
   }


/*****************************************************************************
 *  config() processes command file(s) using kom.c functions;                *
 *                    exits if any errors are encountered.	             *
 *****************************************************************************/
void config( char *configfile )
{
   int      ncommand;     /* # of required commands you expect to process   */ 
   char     init[20];     /* init flags, one byte for each required command */
   int      nmiss;        /* number of required commands that were missed   */
   char    *com;
   char    *str;
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command 
 *****************************************************/   
   ncommand = 15;
   for( i=0; i<ncommand; i++ )  init[i] = 0;

/* Open the main configuration file 
 **********************************/
   nfiles = k_open( configfile ); 
   if ( nfiles == 0 ) {
	fprintf( stderr,"%s: Error opening command file <%s>; exiting!\n", 
                 cmdname, configfile );
	exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {  
	    com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr, "%s: Error opening command file <%s>; exiting!\n",
                           cmdname, &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command 
         ************************************/
  /*0*/     if( k_its("MyModuleId") ) {
		str=k_str();
		if(str) strcpy(MyModName,str);
                init[0] = 1;
            }
  /*1*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("HeartBeatInt") ) {
                HeartBeatInt = k_int();
                init[2] = 1;
            }
  /*3*/	    else if(k_its("LogFile") ) {
		LogSwitch=k_int();
		init[3]=1;
	    }

         /* Maximum size (bytes) for incoming messages
          ********************************************/ 
  /*4*/     else if( k_its("MaxMsgSize") ) {
                MaxMsgSize = k_long();
                init[4] = 1;
	    }

	/* 5 Interval for alive messages to sending machine
	***************************************************/
	    else if( k_its("MyAliveInt") ) {
		MyAliveInt = k_int();
		init[5]=1;
		MyAliveInt = 0;		/*:: Hardwire to 0 for now. */
	    }

	/* 6 Text of alive message to sending machine
	*********************************************/
	else if( k_its("MyAliveString") ){
		str=k_str();
		if(str) strcpy(MyAliveString,str);
		init[6]=1;
	}

	/* 7 Sender's internet address, in dot notation
	***********************************************/
	else if(k_its("SenderIpAdr") ) {
		str=k_str();
		if(str) strcpy(SenderIpAdr,str);
		init[7]=1;
	}		

	/* 8 Sender's Port Number
	*************************/
	    else if( k_its("SenderPort") ) {
		SenderPort = k_int();
		init[8]=1;
	    }

	/* 9 Sender's Heart beat interval
	********************************/
	    else if( k_its("SenderHeartRate") ) {
		SenderHeartRate = k_int();
		init[9]=1;
		SenderHeartRate = 0;	/*:: Hardwire to 0 for now. */
	    }

	/* 10 Sender's heart beat text
	******************************/
	else if(k_its("SenderHeartText") ) {
		str=k_str();
		if(str) strcpy(SenderHeartText,str);
		init[10]=1;
	    }		

	/* 11 Import data in EW Trace format
	******************************/
	else if( k_its("ImportEWTrace") ) {
		ImportEWTrace= k_int();
		init[11]=1;
	    }

	/* 12 Import data in EW Trace format
	******************************/
	else if( k_its("ImportMSTrace") ) {
		ImportMSTrace= k_int();
		init[12]=1;
	    }

	/* 13 RemoteModId
	******************************/
	else if( k_its("RemoteModId") ) {
		str=k_str();
		if(str) strcpy(RemoteModName,str);
		init[13]=1;
	    }

	/* 14 RemoteNetId
	******************************/
	else if( k_its("RemoteNetId") ) {
		str=k_str();
		if(str) strcpy(RemoteNetName,str);
		init[14]=1;
	    }

        /* Unknown command
        *****************/ 
	    else {
                fprintf( stderr, "<%s> Unknown command in <%s>.\n", 
                         com, configfile );
                continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr, 
                       "Bad <%s> command in <%s>; exiting!\n",
                        com, configfile );
               exit( -1 );
            }
	}
	nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "%s: ERROR, no ", cmdname );
       if ( !init[0] )  fprintf( stderr, "<MyModuleId> "      );
       if ( !init[1] )  fprintf( stderr, "<RingName> "   );
       if ( !init[2] )  fprintf( stderr, "<HeartBeatInt> "     );
       if ( !init[3] )  fprintf( stderr, "<LogFile> " );
       if ( !init[4] )  fprintf( stderr, "<MaxMsgSize> "   );
       if ( !init[5] )  fprintf( stderr, "<MyAliveInt> "   );
       if ( !init[6] )  fprintf( stderr, "<MyAliveString> " );
       if ( !init[7] )  fprintf( stderr, "<SenderIpAdr> "    );
       if ( !init[8] )  fprintf( stderr, "<SenderPort> "   );
       if ( !init[9] )  fprintf( stderr, "<SenderHeartRate> "   );
       if ( !init[10] )  fprintf( stderr, "<SenderHeartText argh> "   );
       if ( !init[11] )  fprintf( stderr, "<ImportEWTrace> "   );
       if ( !init[12] )  fprintf( stderr, "<ImportMSTrace> "   );
       if ( !init[13] )  fprintf( stderr, "<RemoteModId> "   );
       if ( !init[14] )  fprintf( stderr, "<RemoteNetID> "   );
       fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }

   return;
}

/****************************************************************************
 *  lookup( )   Look up important info from earthworm.h tables       *
 ****************************************************************************/
void lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 ) {
	fprintf( stderr,
 	        "%s:  Invalid ring name <%s>; exiting!\n", cmdname, RingName);
	exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &MyInstId ) != 0 ) {
      fprintf( stderr, 
              "%s: error getting local installation id; exiting!\n", cmdname );
      exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetInst( RemoteNetName, &RemoteNetId ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid installation id <%s>; exiting!\n", cmdname, RemoteNetName );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr, 
	      "%s: Invalid module name <%s>; exiting!\n", cmdname, MyModName );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( RemoteModName, &RemoteModId ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid module name <%s>; exiting!\n", cmdname, RemoteModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n", cmdname );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_ERROR>; exiting!\n", cmdname );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF2", &TypeTrace ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_TRACEBUF2>; exiting!\n", cmdname );
      exit( -1 );
   }
   if ( GetType( "TYPE_MSTRACEBUF", &TypeMSTrace ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_MSTRACEBUF>; exiting!\n", cmdname );
      exit( -1 );
   }
   return;
} 

/*******************************************************************************
 * output_status() builds a heartbeat or error message & puts it into          *
 *                 shared memory.  Writes errors to log file & screen.         *
 *******************************************************************************/
void output_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char	       msg[256];
   long	       size;
   time_t        t;
 
/* Build the message
 *******************/ 
   logo.instid = MyInstId;
   logo.mod   = MyModId;
   logo.type  = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
	sprintf( msg, "%ld\n\0", t);
   }
   else if( type == TypeError )
   {
	sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
	logit( "et", "%s: %s\n", cmdname, note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */ 	

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","%s:  Error sending heartbeat.\n", cmdname );
	}
	else if( type == TypeError ) {
           logit("et","%s:  Error sending error:%d.\n", cmdname, ierr );
	}
   }

   return;
}

/************************** import_filter *************************
 *           Decide  what to do with this message                 *
 *                                                                *
 ******************************************************************
/*	This routine is handed each incoming message, and can do what it likes
with it. The intent is that here is where installation-specificd processing
is done, such as deciding whether we want this message (e.g. is the pick from
an intersting station?), changing its format, etc.
*/


int import_filter( char *msg, DATA_HDR *hdr )
{
    MSG_LOGO logo;
    int n;
    char *trace = NULL;
    TRACE2_HEADER *trace_header;
    int tracelen;
    int status = 0;

    /* Ignore packets with no data */
    if (hdr->sample_rate == 0 || hdr->sample_rate_mult == 0 || hdr->num_samples == 0) {
	return (status);
    }

    while (ImportMSTrace) {
	logo.instid = RemoteNetId;
	logo.mod = RemoteModId;
	logo.type = TypeMSTrace;
	/* Write the message to shared memory
	************************************/
	if( tport_putmsg( &Region, &logo, (long)(hdr->blksize), msg) != PUT_OK ) {
	    logit("et","%s:  Error sending message via transport:\n",cmdname );
	    status = -1;
	    break;
	}
	if (1) break;
    }

    while (ImportEWTrace) {
	logo.instid = RemoteNetId;
	logo.mod = RemoteModId;
	logo.type = TypeTrace;

	/* Unpack the MiniSEED record into an EW record.		   */
	/* This may break other earthworms, since we don't promise to stay */
	/* within the size limit of MAX_TRACEBUF_SIZ = 4096 bytes.         */
	tracelen = sizeof(TRACE2_HEADER) + sizeof(int)*hdr->num_samples;
	trace = malloc(tracelen);
	if (trace == NULL) {
	    logit("et", "%s: Error mallocing EW Trace record for %d samples.\n",
		  hdr->num_samples);
	    status = -1;
	    break;
	}

	n = ms_unpack (hdr, hdr->num_samples, msg, trace+sizeof(TRACE_HEADER));
	if (n != hdr->num_samples) {
	    logit("et", "%s: Error unpacking MS data, requested %d samples, got %d samples.\n",
		  cmdname, hdr->num_samples, n);
	    status = -1;
	    break;
	}
	    
	/* Fill in the trace header TRACE2_HEADER.	*/
	trace_header = (TRACE2_HEADER *)trace;
	memset((void *)trace_header, 0, sizeof(TRACE2_HEADER));
	trace_header->version[0] = TRACE2_VERSION0;
	trace_header->version[1] = TRACE2_VERSION1;
	trace_header->pinno = 0;
	trace_header->nsamp = hdr->num_samples;
	/* This breaks for traces that include a leapseconds.    */
	trace_header->starttime = (double)unix_time_from_int_time(hdr->begtime) +
	    ((double)(hdr->begtime.usec)/USECS_PER_SEC);
	trace_header->endtime = (double)unix_time_from_int_time(hdr->endtime) +
	    ((double)(hdr->endtime.usec)/USECS_PER_SEC);
	trace_header->samprate = sps_rate(hdr->sample_rate,hdr->sample_rate_mult);
	strcpy(trace_header->sta,trim(hdr->station_id));
	strcpy(trace_header->net,trim(hdr->network_id));
	strcpy(trace_header->chan,trim(hdr->channel_id));
	strcpy(trace_header->loc,trim(hdr->location_id));
	if (strlen(trace_header->loc) == 0)
	    strcpy(trace_header->loc, LOC_NULL_STRING);
	strcpy(trace_header->datatype,(my_wordorder == SEED_BIG_ENDIAN) ? "s4" : "i4");
	trace_header->quality[1] = (char)hdr->data_quality_flags;

	/* Write the message to shared memory
	************************************/
	if( tport_putmsg( &Region, &logo, (long)tracelen, trace) != PUT_OK ) {
	    logit("et","%s:  Error sending message via transport:\n",cmdname );
	    status = -1;
	    break;
	}
	if (1) break;
    }
    if (trace) free (trace);
    return (status);
}


/*************************** WriteToSocket ************************
 *    send a message logo and message to the socket               *
 *    returns  0 if there are no errors                           *
 *            -1 if any errors are detected                       *
 ******************************************************************/

int WriteToSocket( int ActiveSocket, char *msg, MSG_LOGO *logo )
{
   char asciilogo[10];       /* ascii version of outgoing logo */
   char startmsg = STX;       /* flag for beginning of message  */
   char endmsg   = ETX;       /* flag for end of message        */
   int  msglength;           /* total length of msg to be sent */
   int  nsend;               /* # characters to send at once   */
   int  i, rc;

/* Send "start of transmission" flag
 ***********************************/
   rc = send( ActiveSocket, &startmsg, 1, 0 );
   if( rc != 1 ) {
       sprintf (perrmsg, "%s: socket send", cmdname);
       SocketPerror( perrmsg );
       return( -1 );
   }

/* Send ascii representation of logo
 ***********************************/
   sprintf( asciilogo, "%3d%3d%3d",
           (int) logo->instid, (int) logo->mod, (int) logo->type );
   rc = send( ActiveSocket, asciilogo, 9, 0 );
   if( rc != 9 ) {
       sprintf (perrmsg, "%s: socket send", cmdname);
       SocketPerror( perrmsg );
       return( -1 );
   }
 
/* Debug print of message
*************************/
 /* printf("%s: sending under logo (%s):\n%s\n",cmdname,asciilogo,msg); */

/* Send message; break it into chunks if it's big!
 *************************************************/
/* Warning: Garbled data with nsend set to 32000. */
   nsend     = 1000;
   msglength = strlen(msg);
   for ( i=0; i<msglength; i+=nsend )
   {
       if ( nsend > msglength-i )  nsend = msglength-i;
                
       rc = send( ActiveSocket, &msg[i], nsend, 0 );
       if ( rc == -1 )
       {
	   sprintf (perrmsg, "%s: socket send", cmdname);
	   SocketPerror( perrmsg );
           return( -1 );
       }
       else if ( rc != nsend )
       {
           logit( "et",
                  "%s: error sending message to socket\n", cmdname );
           return( -1 );
       }
   }

/* Send "end of transmission" flag
 *********************************/
   rc = send( ActiveSocket, &endmsg, 1, 0 );
   if( rc != 1 ) {
       sprintf (perrmsg, "%s: socket send", cmdname);
       SocketPerror( perrmsg );
       return( -1 );
   }

   return( 0 );
}
