########################################################################
# $Id: datasock_import.d 241 2006-10-03 20:20:45Z paulf $
########################################################################
#
# program:	datasock_import
# invocation:	import_trace_local
# 
# Description:
# 
# 	This module will receive raw MiniSEED records from a socket, and import
# 	the data into Earthworm in TYPE_TRACEBUF2 and/or TYPE_MSTRACEBUF 
#	records. 
# 	It is designed to receive data from a remote datasock process.
# 
# 	This is NOT a true EW module in the sense that:
# 	    a.  It does not exchange heartbeats with datalog (at this time),
# 	    b.  Input from datalog are raw MiniSEED records, not using 
# 		EW encapsulation.
# 
# 	The module should currently be configured to supress outgoing alive
# 	messages and to ignore incoming alive messages since datasock does not
# 	generate or expect alive messages.

MyModuleId		MOD_DATASOCK_IMPORT_LOCAL	# my module name.
RingName		TRACE_RING	# import ring name.
HeartBeatInt		10		# heartbeat interval for ring.
LogFile			1		# create logfile ?
MaxMsgSize		512		# Max input messages size.
MyAliveInt		0		# 0 suppresses heartbeats.
MyAliveString		""		# no alive msg.
SenderIpAdr		127.0.0.1	# sender's ip address.
SenderPort		5002		# sender's ip port.
SenderHeartRate		0		# 0 supresses expecting hearbeats.
SenderHeartText 	""		# no remote alive msg.
ImportEWTrace		1		# import data in TYPE_TRACEBUF2 format.
ImportMSTrace		0		# import data in TYPE_MSTRACEBUF format.
RemoteModId		MOD_TRACE_ATHOS	# remote module id.
RemoteNetId		INST_UCB	# remote net id.
