/* $Id: bld_statchans.h 241 2006-10-03 20:20:45Z paulf $	*/
#include "statchan.h"

int bld_statchans (char *station_str,	/* station list string.		*/
		   char *channel_str,	/* channel list string.		*/
		   STATCHAN **pws,	/* ptr to STATCHAN structure.	*/
		   int *pnws);		/* ptr to # STATCHAN entries.	*/

int bld_statchan (char *station_str,	/* single station string.	*/
		  char *channel_str,	/* channel list string.		*/
		  STATCHAN **pws,	/* ptr to STATCHAN structure.	*/
		  int *pnws);		/* ptr to # STATCHAN entries.	*/
