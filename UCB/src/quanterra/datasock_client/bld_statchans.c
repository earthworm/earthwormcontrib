#ifndef lint
static char id[] = "@(#) $Id: bld_statchans.c 241 2006-10-03 20:20:45Z paulf $";
#endif
#include <stdio.h>
#include <string.h>
#include <memory.h>
#include <stdlib.h>

#include <qlib2.h>

#include "statchan.h"
#include "bld_statchans.h"

/************************************************************************/
/*  bld_statchans:							*/
/*	Add or augment a STATCHAN entry from a station_list and		*/
/*	channel_list							*/
/*	Inputs are:							*/
/*	    station_list (comma-delimited channel list).		*/
/*	    channel_list (comma-delimited channel list).		*/
/*	The station and channel may contain wildcard characters.	*/
/*	Station may contain "*" or "?" wildcard characters.		*/
/*	Channel may contain "?" wildcard characters.			*/
/*	Generate a cross-product of stations and channels.		*/
/*  Return 0 on success, -1 on error.					*/
/************************************************************************/
int bld_statchans (char *station_str,	/* station list string.		*/
		   char *channel_str,	/* channel list string.		*/
		   STATCHAN **pws,	/* ptr to statchan structure.	*/
		   int *pnws)		/* ptr to # statchan entries.	*/
{
    char *token, *str, *p;
    char **stations;
    int nstations = 0;
    int status;
    int i;

    stations = (char **)malloc(sizeof(char *));
    if (stations == NULL) return (-1);
    p = str = strdup (station_str);
    if (str == NULL) return (-1);

    /* Break the station list into individual station tokens.		*/
    while (token = strtok (p,",")) {
	stations = (char **)realloc(stations,(nstations+1)*sizeof(char *));
	stations[nstations++] = token;
	p = NULL;
    }

    /* Process the channel list for each station.			*/
    for (i=0; i<nstations; i++) {
	status = bld_statchan (stations[i], channel_str, 
				  pws, pnws);
	if (status != 0) break;
	p = NULL;
    }

    /* Free temp space and return status of channel parsing.		*/
    free(stations);
    free(str);
    return (status);
}

/************************************************************************/
/*  bld_statchan							*/
/*	Add or augment a STATCHAN entry from a station and channel_list.*/
/*	Inputs are:							*/
/*	    station							*/
/*	    channel_list (comma-delimited channel list).		*/
/*	The station and channel may contain wildcard characters.	*/
/*	Station may contain "*" or "?" wildcard characters.		*/
/*	Channel may contain "?" wildcard characters.			*/
/*  Returns 0 on success, -1 on error.					*/
/************************************************************************/
int bld_statchan (char *station_str,	/* single station string.	*/
		  char *channel_str,	/* channel list string.		*/
		  STATCHAN **pws,	/* ptr to statchan structure.	*/
		  int *pnws)		/* ptr to # statchan entries.	*/
{
    char *p;
    char *token;
    int is_duplicate = 0;
    int i;
    STATCHAN *ws = *pws;
    int nws = *pnws;
    char *station = NULL;
    char *channel = NULL;

    /* Check to see if this is a duplicate station name.		*/
    station = strdup(station_str);
    channel = strdup(channel_str);
    if (station == NULL || channel == NULL) {
	fprintf (stderr, "Error mallocing station or channel strings\n");
	if (station) free(station);
	if (channel) free(channel);
	return (-1);
    }
    uppercase (station);
    uppercase (channel);
    for (i=0; i<nws; i++) {
	if (strcmp(station, ws[i].station) == 0) break;
    }
    /* If station was not found, allocate space for a new station.	*/
    if (i >= nws) {
	ws = (nws == 0) ? 
	    (STATCHAN *)malloc((nws+1)*sizeof(STATCHAN)) :
	    (STATCHAN *)realloc(ws,(nws+1)*sizeof(STATCHAN));
	if (ws == NULL) {
	    fprintf (stderr, "Error mallocing station info\n");
	    free(station);
	    free(channel);
	    return (-1);
	}
	memset((void *)&ws[i], 0, sizeof(STATCHAN));
	i = nws++;
	strcpy(ws[i].station, station);
    }
    /* Append the new channels to the station request.			*/
    p = channel;
    while (token = strtok(p,",")) {
	ws[i].channel = (ws[i].nchannels == 0) ? 
	    (char **)malloc((ws[i].nchannels+1)*sizeof(char *)) :
	    (char **)realloc(ws[i].channel,
			     (ws[i].nchannels+1)*sizeof(char *));
	ws[i].channel[ws[i].nchannels] = strdup(token);
	++(ws[i].nchannels);
	p = NULL;
    }
    *pws = ws;
    *pnws = nws;
    free(station);
    free(channel);
    return (0);
}
