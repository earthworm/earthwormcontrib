#ifndef lint
static char id[] = "@(#) $Id: socket_subs.c 241 2006-10-03 20:20:45Z paulf $";
#endif
#include <stdio.h>
#include <stdlib.h>
#include <termio.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <math.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <qlib2.h>

#include "datasock_codes.h"
#include "channelv.h"

#define	FAILURE	-1
#define	SUCCESS	 0

/************************************************************************/
/*  Externals variables and functions.					*/
/************************************************************************/
extern FILE *info;		/* Default FILE for messages.		*/
extern char *cmdname;		/* Program name.			*/
extern int debug_option;	/* debug flag.				*/
extern int terminate_proc;	/* Terminate program flag.		*/

/*  Signal handler variables and functions.				*/
void finish_handler(int sig);
void terminate_program (int error);

const static int MIN_BLKSIZE = 128;
const static int MAX_BLKSIZE = 4096;

/************************************************************************/
/*  open_socket:							*/
/*	Open a socket on the specified machine and service name/number.	*/
/*	Returns:	 open file descriptor				*/
/*			negative value on error.			*/
/************************************************************************/
int open_socket(char *server, 	/* remote node name */
                char *service) 	/* remote service name */
{
    struct sockaddr_in server_sock;
    struct hostent *hp;
    struct servent *sp;
    unsigned long  ipadd;
    int s;

    /* Create an socket on which to make the connection.		*/
    /* This function call says we want our communications to take	*/
    /* place in the internet domain (AF_INET).				*/
    /* (The other possible domain is the Unix domain).			*/
    s = socket(AF_INET, SOCK_STREAM, 0);
    if (s < 0) {
	printf(" %% Error opening socket in client.\n");
	return(FAILURE);
    }
    /* Get the IP address of the remote client in the "hp" structure.	*/
    /* It must be available to this machine or the call files.		*/
    /* Info can come from hosts file, NIS, DNS, etc.			*/
    if (isalpha(server[0])) {
	hp = gethostbyname(server);	/* probe name server by name */
    } 
    else {
	ipadd = inet_addr(server);	/* convert IP to binary format */
	hp = gethostbyaddr((char*)&ipadd, sizeof(ipadd), AF_INET);	
					/*probe name server by address */
    }

    if (hp == NULL) {
	printf(" %% Remote host is unknown to name server.\n");
	return(FAILURE); 
    } 
    else {
	printf(" %% Remote host: %s\n", hp->h_name);
    }

    /* If specified as a name the service/port must be defined by name	*/
    /* on this system.							*/
    if (isalpha(service[0])) {
	sp = getservbyname(service, "tcp");	/* probe service by name */
	if (sp == NULL) {
	    printf(" %% Service not in local host table.\n");
	    return(-1);
	} 
	else {
	    server_sock.sin_port = sp->s_port;
	}
    } 
    else {
	server_sock.sin_port = htons(atoi(service)); /* convert ASCII to int. */
    }

    /* Create 'sockaddr_in' internet address structure for connect.	*/
    server_sock.sin_family = hp->h_addrtype;
    strncpy((char*)&server_sock.sin_addr, hp->h_addr, hp->h_length);

    /* Print IP address in xx.xx.xx.xx format */
    printf(" %% IP address: %s\n", inet_ntoa(server_sock.sin_addr));
    printf(" %% Service port # is %d. \n", ntohs(server_sock.sin_port));

    /* Connect to the socket address.	*/
    if (connect( s, (struct sockaddr *) &server_sock, sizeof(server_sock)) < 0) {
	printf(" %% Attempt to connect to remote host failed.\n ");
	return(FAILURE);
    }

    return(s);
}

/************************************************************************/
/*  get_packet:								*/
/*	Read a miniSEED packet from the socket, and return a DATA_HDR	*/
/*	structure for the packet.					*/
/*	Calling routine must free the DATA_HDR.				*/
/************************************************************************/
DATA_HDR* get_packet(int sd, char* indata)
{
    int            nr;
    DATA_HDR      *hdr;
    BS            *bs = NULL;
    BLOCKETTE_HDR *bh = NULL;
    int goodpacket;

    goodpacket = 0;

    while(!goodpacket) {
	nr = xread(sd, (char *)indata, MIN_BLKSIZE);
	if (nr == 0) {
            fprintf (stderr, "Found end of file on socket read: %d\n",nr);
            return((DATA_HDR *)FAILURE);
	}
	if (nr < MIN_BLKSIZE) {
            fprintf (stderr, "Error reading from socket: expected %d got %d\n",
		     MIN_BLKSIZE, nr);
            continue;
	}
	if ((hdr = decode_hdr_sdr ((SDR_HDR *)indata, MIN_BLKSIZE)) == NULL) {
	    fprintf (stderr, "Error decoding SEED data hdr\n");
	    continue;
	}
	if (hdr->blksize > MIN_BLKSIZE) {
	    nr = xread (sd, (char *)indata + MIN_BLKSIZE, hdr->blksize-MIN_BLKSIZE);
	    if (nr < hdr->blksize-MIN_BLKSIZE) {
                fprintf (stderr, "Error reading SEED data\n");
                continue;
	    }
	}
        
	goodpacket = 1;

	/* Fill in the number of data frames.   */
	if ((bs = find_blockette(hdr,1001)) &&
	    (bh = (BLOCKETTE_HDR *)(bs->pb))) {
	    hdr->num_data_frames = ((BLOCKETTE_1001 *)bh)->frame_count;
	    /* Explicitly set num_data_frames for SHEAR stations.   */
	    if (hdr->num_data_frames == 0 && 
		(hdr->sample_rate != 0 && hdr->sample_rate_mult != 0))
	      hdr->num_data_frames = (hdr->blksize - hdr->first_data) / sizeof(FRAME);
	}
	else {
	    hdr->num_data_frames =
	    (hdr->sample_rate == 0 || hdr->sample_rate_mult == 0) ? 0 :
	    (hdr->blksize - hdr->first_data) / sizeof(FRAME);
	}
    } /* End while !goodpacket */
    return (hdr);
}

/************************************************************************/
/*  read_from_socket:							*/
/*	Read data from a TCP/IP socket.					*/
/************************************************************************/
int read_from_socket (CHANNEL_INFO **channelv, int nchannel, 
		      char *host, char *service, char *passwd, int flag)
{
    int res;
    DATA_HDR *data_hdr;
    char request_str[256];
    char* cmap;		
    char seedrecord[512];
    int reading_packets = TRUE;
    int data[8192];
    int i, n;
    double samples_per_second;

    /* Set up a condition handler for SIGPIPE, since a write to a	*/
    /* close pipe/socket raises a alarm, not an error return code.	*/

    /* Open the socket connection */

    int socket_channel;
    while (! terminate_proc) {

	/* Allow signals to be handled with the default handler		*/
	/* while we setup the socket.					*/

	signal (SIGINT,SIG_DFL);
	signal (SIGTERM,SIG_DFL);
	signal (SIGPIPE,SIG_DFL);

	socket_channel = open_socket(host, service);
	if (socket_channel < 0) {
	    printf("Error on open_socket call.\n");
	    sleep(60);
	    continue;
	}
	if (flag & SOCKET_REQUEST_PASSWD) {
	    sprintf (request_str, "%d %s %s\n", 
		     DATASOCK_PASSWD_CODE, PASSWD_KEYWD, passwd);
	    if (debug_option & 1) {
		printf (request_str);
		fflush (stdout);
	    }
	    write (socket_channel, request_str, strlen(request_str));
	}
	if (flag & SOCKET_REQUEST_CHANNELS) {
	    for (i=0; i<nchannel && channelv != NULL; i++) {
		sprintf (request_str, "%d %s %s\n", DATASOCK_CHANNEL_CODE, 
			 channelv[i]->station, channelv[i]->channel);
		if (debug_option & 1) {
		    printf (request_str);
		    fflush (stdout);
		}
		write (socket_channel, request_str, strlen(request_str));
	    }
	}
	if (flag) {
	    sprintf (request_str, "%d %s\n", DATASOCK_EOT_CODE, "EOT");
	    if (debug_option & 1) {
		printf (request_str);
		fflush (stdout);
	    }
	    write (socket_channel, request_str, strlen(request_str)+1);
	}
	terminate_proc = 0;
	signal (SIGINT,finish_handler);
	signal (SIGTERM,finish_handler);
	signal (SIGPIPE,finish_handler);

	while(reading_packets && ! terminate_proc) {
	    data_hdr = get_packet(socket_channel,(char *)seedrecord);
	    if(data_hdr == NULL) {
		continue;
	    }
	    else if((int)data_hdr == FAILURE) {
		reading_packets = FALSE;
		continue;
	    }

	    /* Use data here...	*/
	    samples_per_second = sps_rate(data_hdr->sample_rate, data_hdr->sample_rate_mult);
	    fprintf (info, "%s.%s.%s time=%s n=%d rate=%.04lf hz delta=%.4lf\n",
		     data_hdr->station_id, 
		     data_hdr->network_id, 
		     data_hdr->channel_id,
		     time_to_str(data_hdr->begtime, MONTHS_FMT_1),
		     data_hdr->num_samples,
		     samples_per_second,
		     (double)1/samples_per_second);
	    if ((data_hdr->sample_rate != 0 && data_hdr->sample_rate_mult != 0))
		n = ms_unpack (data_hdr, data_hdr->num_samples, seedrecord,
			       data);
	    else n = 0;
	    if (n != data_hdr->num_samples) {
		fprintf (info, "packet has %d samples, unpacked %d\n",
			 data_hdr->num_samples, n);
	    }
	    if (debug_option & 2) {
		for (i=0; i<n; i++) {   
		    if (i%8 == 0 && i != 0) fprintf (info, "\n");
		    fprintf (info, "%10d", data[i]);
		    }
		fprintf (info, "\n");
	    }

	    free_data_hdr(data_hdr);
	}

	if (terminate_proc) break;
	close(socket_channel);
	sleep(60);
    }
    return (SUCCESS);
}
