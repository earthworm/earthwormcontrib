#ifndef lint
static char id[] = "@(#) $Id: datasock_client.c 241 2006-10-03 20:20:45Z paulf $";
#endif
/************************************************************************/
/*
Program:
    datasock_client

Author:
    Douglas Neuhauser, UC Berkeley Seismographic Station, 
	Adopted from a demo program by written by 
	    Woodrow H. Owens for Quanterra, Inc.
	    Copyright 1994 Quanterra, Inc.
	    Copyright (c) 1998 The Regents of the University of California.

Purpose:
    Sample datasock client program.

Modification History:
Date     who	Version	Modifications.
97/08/24 DSN	1.0	Initial coding.
98/05/08 DSN	1.1	Command line options.
*/
/************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>

#include <qlib2.h>

#include "datasock_codes.h"
#include "channelv.h"
#include "statchan.h"
#include "bld_statchans.h"

#define	VERSION	"1.1 (1998.128)"

char *syntax[] = {
"%s - Version " VERSION,
"%s [-S port] [-H hostname] [-p passwd]",
"	[-h] station_list channel_list",
"    where:",
"	-S port		Specify port number to connect to.",
"	-H hostname	Name of host to connect to.",
"	-p passwd	Password required from remote connection.",
"	-d n		Debug flag:",
"			1 = print datasock request.",
"			2 = output data values.",
"	-h		Help - prints syntax message.",
"	station_list	List of station names (comma-delimited).",
"	channel_list	List of channel names (comma-delimited).",
NULL };

/************************************************************************/
/*  Externals variables and functions.					*/
/************************************************************************/
FILE *info;			/* Default FILE for messages.		*/
char *cmdname;			/* Program name.			*/
int debug_option;		/* Debug flag.				*/
int terminate_proc;		/* Terminate program flag.		*/
int nchannel;			/* number of entries in channelv.	*/
CHANNEL_INFO **channelv = NULL;	/* list of channels.			*/

/*  Signal handler variables and functions.				*/
void finish_handler(int sig);
void terminate_program (int error);

/************************************************************************/
/*  main program.							*/
/************************************************************************/
main (int argc, char **argv)
{
    static int nreq;		/* # of stations in request list.	*/
    static STATCHAN *req;	/* station info from requests.		*/
    int status;
    int i, j;

    char *host = NULL;
    char *service = NULL;
    char *password = NULL;
    int flag = 0;

/*
    char *host = "bdsn.geo.berkeley.edu";
    char *service = "5020";
    char *password = "ms2web3D";
    int flag = SOCKET_REQUEST_PASSWD;
*/
/*
    char *host = "bdsn.geo.berkeley.edu";
    char *service = "5002";
    char *password = NULL;
    int flag = 0;
*/

    /* Variables needed for getopt. */
    extern char	*optarg;
    extern int	optind, opterr;
    int		c;

    cmdname = tail(argv[0]);
    info = stdout;

    while ( (c = getopt(argc,argv,"hS:H:p:d:")) != -1)
      switch (c) {
	case '?':
	case 'h':   print_syntax (cmdname,syntax,info); exit(0);
	case 'S':   service=optarg; break;
	case 'H':   host=optarg; break;
	case 'p':   password = optarg; flag |= SOCKET_REQUEST_PASSWD; break;
	case 'd':   debug_option = atoi(optarg); break;
      }

    /*	Skip over all options and their arguments.			*/
    argv = &(argv[optind]);
    argc -= optind;
    if (argc > 0) {
	status = bld_statchans (argv[0], argv[1], &req, &nreq);
	if (status != 0) {
	    fprintf (stderr, "Error parsing station and/or channel list\n");
	    exit(1);
	}
	/* Generate an explicit station channel list.			*/
	for (nchannel=0,i=0; i<nreq; i++) {
	    for (j=0; j<req[i].nchannels; j++) {
		channelv = (nchannel==0) ? 
		    (CHANNEL_INFO **) malloc((nchannel+2)*sizeof(CHANNEL_INFO **)) :
		    (CHANNEL_INFO **)realloc(channelv,(nchannel+2)*sizeof(CHANNEL_INFO **));
		if (channelv == NULL) {
		    fprintf (stderr, "Error allocating channelv array\n");
		    exit(1);

		}
		channelv[nchannel] = (CHANNEL_INFO *)calloc(1,sizeof(CHANNEL_INFO));
		if (channelv[nchannel] == NULL) {
		    fprintf (stderr, "Error allocating channelv space\n");
		    exit(1);
		}
		strcpy (channelv[nchannel]->station, req[i].station);
		strcpy (channelv[nchannel]->channel, req[i].channel[j]);
		channelv[++nchannel] = NULL;
	    }
	}
    }
    if (nchannel > 0) flag |= SOCKET_REQUEST_CHANNELS;

    while (! terminate_proc) {
	read_from_socket (channelv, nchannel, host, service, password, flag);
    }
    terminate_program (0);
}

/************************************************************************/
/*  finish_handler:							*/
/*	Signal handler to handle program termination cleanly.		*/
/************************************************************************/
void finish_handler(int sig)
{
    terminate_proc = 1;
    signal (sig,finish_handler);    /* Re-install handler (for SVR4)	*/
}

/************************************************************************/
/*  terminate_program                                                   */
/*      Terminate prog and return error code.  Clean up on the way out. */
/************************************************************************/
void terminate_program (int error)
{
    exit(error);
}
