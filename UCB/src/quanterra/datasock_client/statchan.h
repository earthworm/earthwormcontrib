/* $Id: statchan.h 241 2006-10-03 20:20:45Z paulf $	*/
/* Structure to hold station name and list of channels.			*/

#ifndef	__statchan_h
#define __statchan_h

typedef struct _statchan {	/* Structure for station channel info.	*/
	char station[8];	/* Station name.			*/
	int nchannels;		/* Number of channels.			*/
	char **channel;		/* Ptr to list of channel names.	*/
} STATCHAN;

#endif
