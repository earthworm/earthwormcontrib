/* $Id: channelv.h 241 2006-10-03 20:20:45Z paulf $	*/
/************************************************************************/
/*  Channel and selector info, used for sockets where we explictily	*/
/*  the stations and channels that we want.				*/
/************************************************************************/
typedef struct _channel_info {
    char station[8];
    char channel[4];
    char network[4];
    char location[4];
} CHANNEL_INFO;
