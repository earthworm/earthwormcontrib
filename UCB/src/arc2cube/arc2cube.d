# This is arc2cube's parameter file

#  Basic Earthworm setup:
#
MyModuleId        MOD_ARC2CUBE   # module id for this instance of arc2cube 
InRingName           HYPO_RING   # shared memory ring for input
LogFile              1           # 0 to turn off disk log file; 1 to turn it on
                                 # 2 to log to module log but not to stderr/stdout
HeartBeatInterval    30          # seconds between heartbeats

# List the message logos to grab from transport ring
#              Installation       Module          Message Types
GetEventsFrom  INST_WILDCARD    MOD_WILDCARD    # hyp2000arc - no choice.

# Set up temp and output directories for cube files.
TempDir    /home/qdds/temp
OutputDir  /home/qdds/polldir

NetCode    NC         # Two-letter network code

# Location Method code for the CUBE message. Thi si a single-letter code
# to identify the location method AND to indicate the review status.
# Upper-case letters indicate an unrevieweed solution;
# lower-case letters indicate soution has been reviewed by a human being.
# Solutions located by Hypoinverse are indicated with H or h. 
# Other standardized characters are for archaic location methods.
LocMethod  h

#
