#
#   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
#   CHECKED IT OUT USING THE COMMAND CHECKOUT.
#
#    $Id: makefile.sol 462 2009-05-13 22:22:24Z lombard $
#
#

CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
SRCS = arc2cube.c write_cube.c

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

ARC = arc2cube.o write_cube.o $L/chron3.o $L/logit.o $L/getutil.o \
     $L/read_arc.o $L/transport.o $L/kom.o $L/sleep_ew.o $L/time_ew.o \
     $L/dirops_ew.o

arc2cube: $(ARC)
	cc -o $(B)/arc2cube $(ARC) -lm -lposix4

.c.o:
	cc -c ${CFLAGS} $<

lint:
	lint arc2cube.c write_cube.c $(GLOBALFLAGS)

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/arc2cube*

depend:
	makedepend -fmakefile.sol -- $(CFLAGS) -- $(SRCS)

# DO NOT DELETE THIS LINE -- make depend depends on it.


