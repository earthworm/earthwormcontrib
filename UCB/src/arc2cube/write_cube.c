#ifndef lint
static char sccsid[] = "@(#) $Id: write_cube.c 474 2009-06-24 16:33:16Z lombard $";
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "earthworm.h"
#include "chron3.h"
#include "read_arc.h"
#include "write_cube.h"

#ifndef        MIN
#define        MIN(a,b)    ((a<b) ? a : b)
#endif
#define LINELEN 255
#define QDDS_VERSIONS	"0123456789ABCDEFGHJKLMNPQRSTUVWXYZZ"
#define PRINT_VERSION(i, l) ((i < sizeof(l)-1) ? l[i] : l[sizeof(l)-2])

/************************************************************************/
/*  roundoff:                                                           */
/*      Round a value to the closest integer.                           */
/*  return:                                                             */
/*      Closest integer value.                                          */
/*      x.5 rounds to sign(x)*(|x|+1)                                   */
/************************************************************************/
int roundoff (double      d)              /* double precision value to round.     */
{
    int sign, result;
    double ad;

    sign = (d > 0) ? 1 : -1;
    ad = fabs(d);
    result = sign * (int)(ad+.5);
    return (result);
}

/************************************************************************/
/*  menlocheck:								*/
/*	Return Menlo-Park check char from null terminated string.	*/
/************************************************************************/
char menlocheck( char *sz )
{
    unsigned sum;
    for( sum=0; *sz; sz++ )
    {
	if( sum&1 )
	    sum = (sum>>1) + 0x8000;
	else
	    sum >>= 1;
	sum += *sz;
	sum &= 0xFFFF;
    }
    return (char)(36 + sum%91);
}

/************************************************************************/
/* write_cube:								*/
/*	Write a Cube message to an open file.				*/
/*      Close the file before returning.                                */
/*      Supported CUBE formats: E, DE                                   */
/*      netcode is the two-character network code used in QDDS          */
/*      text (optional) is used for DE message to provide detail        */
/*	Return 0 on success, 1 on failure.				*/
/************************************************************************/
int write_cube(FILE *fp, char *format, struct Hsum *sumP, char *netcode, 
	       char LocMethod, char *text)
{
    char line[LINELEN+1];
    struct Greg g;
    long minute;
    int sex;
    char version;
    
    if (netcode == NULL || strlen(netcode) != 2) {
	logit("et", "write_qdds: netcode is missing or wrong length: mist be 2 chars\n");
	fclose(fp);
	return(1);
    }

    if (strncmp(format,CUBE_EVENT_FORMAT,1)==0) {
	/* E = Event messages. */
	minute = (long) (sumP->ot / 60.0);
	sex = (int)((sumP->ot - 60.0 * (double) minute) * 10.0);
	grg(minute,&g);
	version = PRINT_VERSION(sumP->version, QDDS_VERSIONS);

	sprintf (line, "%-2.2s%8d%-2.2s%c%04d%02d%02d%02d%02d%03d%7d%8d%4d%2d%3d%3d%4d%4d%4d%4d%2d%c  %2d%c%c",
		 format, sumP->qid%100000000, netcode, version,
		 g.year, g.month, g.day, g.hour, g.minute, sex,
		 roundoff(sumP->lat*10000), roundoff(sumP->lon*10000),
		 roundoff(sumP->z*10), roundoff(sumP->Mpref*10), 
		 sumP->nph - sumP->nphS, sumP->nph,
		 roundoff(sumP->dmin*10),
		 roundoff(sumP->rms*100),
		 roundoff(sumP->erh*10),
		 MIN(roundoff(sumP->erz*10),999),
		 roundoff(sumP->gap/3.6),
		 sumP->labelpref,
		 roundoff(sumP->mdmad*10),
		 LocMethod, '#');
	/* Add checksum in col 80. */
	line[79] = '\0';
	line[79] = menlocheck(line);
	fputs(line,fp);
    }
    else if (strncmp(format,CUBE_DELETE_FORMAT,2)==0) {
	/* DE = Delete Event messages. */
	if (text == NULL) text = "";
	fprintf (fp, "%-2.2s%8d%-2.2s%-.68s\n", format, 
		 sumP->qid%100000000,
		 netcode, text);
    }
    else {
	/* Unknown message format.  */
	logit("et", "write_qdds: Unknown message format '%s'\n", format);
	fclose(fp);
	return(1);		/* error status.	*/
    }
    fclose(fp);
    return(0);
}

	
