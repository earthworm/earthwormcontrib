/*
 *    $Id: arc2cube.c 474 2009-06-24 16:33:16Z lombard $
 *
 *
 *
 */

/*
 * arc2trig.c:
Takes a hypo arc message as input and produces a CUBE E-format file 
such as for submission to QDDS
*/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <chron3.h>
#include <earthworm.h>
#include <read_arc.h>
#include <kom.h>
#include <transport.h>
#include "write_cube.h"

#define MAX_STR		     255

/* Function prototypes */
int read_hyp (char *, char *, struct Hsum *);

/* Functions in this source file
 *******************************/
void   arc2cube_config  ( char * );
void   arc2cube_lookup  ( void );
void   arc2cube_status  ( unsigned char, short, char * );
int    arc2cube_hinvarc ( char*, MSG_LOGO );

static  SHM_INFO  InRegion;      /* shared memory region to use for input  */

#define   MAXLOGO   10
MSG_LOGO  GetLogo[MAXLOGO];    /* array for requesting module,type,instid */
short     nLogo;

static char ArcMsgBuf[MAX_BYTES_PER_EQ]; /* character string to hold event message
                                        MAX_BYTES_PER_EQ is from earthworm.h */


/* Things to read or derive from configuration file
 **************************************************/
static char    InRingName[MAX_RING_STR];       /* name of transport ring for input  */
static char    MyModName[MAX_MOD_STR];        /* speak as this module name/id      */
static int     LogSwitch;            /* 0 if no logfile should be written */
static long    HeartBeatInterval;    /* seconds between heartbeats        */
static char    TempDir[MAX_STR];     /* temporary directory to create files */
static char    OutputDir[MAX_STR];   /* destination directory   */
static char    NetCode[3];           /* two-letter network code */
static char    LocMethod;            /* single-letter location method code */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          InRingKey;      /* key of transport ring for input   */
static unsigned char InstId;         /* local installation id             */
static unsigned char MyModId;        /* Module Id for this program        */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeHyp2000Arc;

/* Error messages used by arc2cube
 *********************************/
#define  ERR_MISSMSG       0   /* message missed in transport ring       */
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer     */
#define  ERR_NOTRACK       2   /* msg retreived; tracking limit exceeded */
#define  ERR_FILEIO        3   /* error opening cube file                */
#define  ERR_DECODEARC     4   /* error reading input archive message    */
static char  Text[150];        /* string for log/error messages          */

struct Hsum Sum;               /* Hyp2000 summary data                   */
pid_t	MyPid; 	/** Out process id is sent with heartbeat **/


int main( int argc, char **argv )
{
   time_t      timeNow;          /* current time                  */
   time_t      timeLastBeat;     /* time last heartbeat was sent  */
   long      recsize;          /* size of retrieved message     */
   MSG_LOGO  reclogo;          /* logo of retrieved message     */
   int       res;


/* Check command line arguments
 ******************************/
   if ( argc != 2 )
   {
        fprintf( stderr, "Usage: arc2cube <configfile>\n" );
        exit( 0 );
   }

/* Initialize name of log-file & open it
 ***************************************/
   logit_init( argv[1], 0, 256, 1 );

/* Read the configuration file(s)
 ********************************/
   arc2cube_config( argv[1] );
   logit( "" , "%s: Read command file <%s>\n", argv[0], argv[1] );

/* Look up important info from earthworm.h tables
 ************************************************/
   arc2cube_lookup();

/* Reinitialize logit logging to the desired level
 *************************************************/
   logit_init( argv[1], 0, 256, LogSwitch );
   

/* Get our own process ID for restart purposes
 **********************************************/

   if ((MyPid = getpid ()) == -1)
   {
      logit ("e", "arc2cube: Call to getpid failed. Exiting.\n");
      exit (-1);
   }

/* Attach to Input shared memory ring
 ************************************/
   tport_attach( &InRegion, InRingKey );
   logit( "", "arc2cube: Attached to public memory region %s: %d\n",
          InRingName, InRingKey );

/* Force a heartbeat to be issued in first pass thru main loop
 *************************************************************/
   timeLastBeat = time(&timeNow) - HeartBeatInterval - 1;

/* Flush the incomming transport ring on startup
 **************************************************/

   while (tport_getmsg (&InRegion, GetLogo, nLogo,  &reclogo,
			&recsize, ArcMsgBuf, sizeof(ArcMsgBuf)- 1) != GET_NONE);

/*----------------------- setup done; start main loop -------------------------*/

   while(1)
   {
     /* send arc2cube's heartbeat
      ***************************/
        if  ( time(&timeNow) - timeLastBeat  >=  HeartBeatInterval )
        {
            timeLastBeat = timeNow;
            arc2cube_status( TypeHeartBeat, 0, "" );
        }

     /* Process all new messages
      **************************/
        do
        {
        /* see if a termination has been requested
         *****************************************/
           if ( tport_getflag( &InRegion ) == TERMINATE ||
                tport_getflag( &InRegion ) == MyPid )
           {
           /* detach from shared memory */
                tport_detach( &InRegion );
           /* write a termination msg to log file */
                logit( "t", "arc2cube: Termination requested; exiting!\n" );
                fflush( stdout );
                exit( 0 );
           }

        /* Get msg & check the return code from transport
         ************************************************/
           res = tport_getmsg( &InRegion, GetLogo, nLogo,
                               &reclogo, &recsize, ArcMsgBuf, sizeof(ArcMsgBuf)-1 );

           if( res == GET_NONE )          /* no more new messages     */
           {
                break;
           }
           else if( res == GET_TOOBIG )   /* next message was too big */
           {                              /* complain and try again   */
                sprintf(Text,
                        "Retrieved msg[%ld] (i%u m%u t%u) too big for ArcMsgBuf[%d]",
                        recsize, reclogo.instid, reclogo.mod, reclogo.type,
                        sizeof(ArcMsgBuf)-1 );
                arc2cube_status( TypeError, ERR_TOOBIG, Text );
                continue;
           }
           else if( res == GET_MISS )     /* got a msg, but missed some */
           {
                sprintf( Text,
                        "Missed msg(s)  i%u m%u t%u  %s.",
                         reclogo.instid, reclogo.mod, reclogo.type, InRingName );
                arc2cube_status( TypeError, ERR_MISSMSG, Text );
           }
           else if( res == GET_NOTRACK ) /* got a msg, but can't tell */
           {                             /* if any were missed        */
                sprintf( Text,
                         "Msg received (i%u m%u t%u); transport.h NTRACK_GET exceeded",
                          reclogo.instid, reclogo.mod, reclogo.type );
                arc2cube_status( TypeError, ERR_NOTRACK, Text );
           }

        /* Process the message
         *********************/
           ArcMsgBuf[recsize] = '\0';      /*null terminate the message*/

           if( reclogo.type == TypeHyp2000Arc )
           {
               int ret;
               ret   = arc2cube_hinvarc( ArcMsgBuf, reclogo );
               if(ret) arc2cube_status( TypeError, ERR_DECODEARC, "" );
           }

        } while( res != GET_NONE );  /*end of message-processing-loop */

        sleep_ew( 1000 );  /* no more messages; wait for new ones to arrive */

   }
/*-----------------------------end of main loop-------------------------------*/
}

/******************************************************************************
 *  arc2cube_config() processes command file(s) using kom.c functions;        *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
void arc2cube_config( char *configfile )
{
   int      ncommand;     /* # of required commands you expect to process   */
   char     init[10];     /* init flags, one byte for each required command */
   int      nmiss;        /* number of required commands that were missed   */
   char    *com;
   char    *str;
   int      nfiles;
   int      success;
   int      i;
   int      lastchar;
   
/* Set to zero one init flag for each required command
 *****************************************************/
   ncommand = 9;
   for( i=0; i<ncommand; i++ )  init[i] = 0;
   nLogo = 0;

/* Open the main configuration file
 **********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 ) {
        logit( "e",
                "arc2cube: Error opening command file <%s>; exiting!\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  logit( "e",
                          "arc2cube: Error opening command file <%s>; exiting!\n",
                           &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("InRingName") ) {
                str = k_str();
                if(str) strcpy( InRingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_long();
                init[3] = 1;
            }


         /* Enter installation & module to get event messages from
          ********************************************************/
  /*4*/     else if( k_its("GetEventsFrom") ) {
                if ( nLogo+1 >= MAXLOGO ) {
                    logit( "e",
                            "arc2cube: Too many <GetEventsFrom> commands in <%s>",
                             configfile );
                    logit( "e", "; max=%d; exiting!\n", (int) MAXLOGO/2 );
                    exit( -1 );
                }
                if( ( str=k_str() ) ) {
                   if( GetInst( str, &GetLogo[nLogo].instid ) != 0 ) {
                       logit( "e",
                               "arc2cube: Invalid installation name <%s>", str );
                       logit( "e", " in <GetEventsFrom> cmd; exiting!\n" );
                       exit( -1 );
                   }
                   GetLogo[nLogo+1].instid = GetLogo[nLogo].instid;
                }
                if( ( str=k_str() ) ) {
                   if( GetModId( str, &GetLogo[nLogo].mod ) != 0 ) {
                       logit( "e",
                               "arc2cube: Invalid module name <%s>", str );
                       logit( "e", " in <GetEventsFrom> cmd; exiting!\n" );
                       exit( -1 );
                   }
                   GetLogo[nLogo+1].mod = GetLogo[nLogo].mod;
                }
                if( GetType( "TYPE_HYP2000ARC", &GetLogo[nLogo].type ) != 0 ) {
                    logit( "e",
                               "arc2cube: Invalid message type <TYPE_HYP2000ARC>" );
                    logit( "e", "; exiting!\n" );
                    exit( -1 );
                }
                nLogo++;
                init[4] = 1;
            /*    printf("GetLogo[%d] inst:%d module:%d type:%d\n",
                        nLogo, (int) GetLogo[nLogo].instid,
                               (int) GetLogo[nLogo].mod,
                               (int) GetLogo[nLogo].type ); */  /*DEBUG*/
            }
  /*5*/     else if( k_its("TempDir") ) {
                str = k_str();
                if(str) strcpy( TempDir, str );
                init[5] = 1;
            }
  /*6*/     else if( k_its("OutputDir") ) {
                str = k_str();
                if(str) strcpy( OutputDir, str );
                init[6] = 1;
            }
  /*7*/     else if( k_its("NetCode") ) {
                str = k_str();
		if (str) {
		    if (strlen(str) == 2) {
			strcpy( NetCode, str );
			init[7] = 1;
		    } else {
			logit("e", "arc2cube: NetCode must be two letters");
			exit(-1);
		    }
		}
            }

  /*8*/     else if( k_its("LocMethod") ) {
                str = k_str();
                if(str) LocMethod = str[0];
                init[8] = 1;
            }

         /* Unknown command
          *****************/
            else {
                logit( "e", "arc2cube: <%s> Unknown command in <%s>.\n",
                         com, configfile );
                continue;
            }

        /* See if there were any errors processing the command
         *****************************************************/
            if( k_err() ) {
               logit( "e",
                       "arc2cube: Bad <%s> command in <%s>; exiting!\n",
                        com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       logit( "e", "arc2cube: ERROR, no " );
       if ( !init[0] )  logit( "e", "<LogFile> "           );
       if ( !init[1] )  logit( "e", "<MyModuleId> "        );
       if ( !init[2] )  logit( "e", "<InRingName> "        );
       if ( !init[3] )  logit( "e", "<HeartBeatInterval> " );
       if ( !init[4] )  logit( "e", "<GetEventsFrom> "     );
       if ( !init[5] )  logit( "e", "<TempDir>"            );
       if ( !init[6] )  logit( "e", "<OutputDir>"          );
       if ( !init[7] )  logit( "e", "<NetCode>"            );
       if ( !init[8] )  logit( "e", "<LocMethod>"          );
       logit( "e", "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }

#if defined(_OS2) || defined(_WINNT)
   lastchar = strlen(TempDir)-1;
   if( OutputDir[lastchar] != '\\' &&  TempDir[lastchar] != '/' )
      strcat( TempDir, "\\" );
   lastchar = strlen(OutputDir)-1;
   if( OutputDir[lastchar] != '\\' &&  OutputDir[lastchar] != '/' )
      strcat( OutputDir, "\\" );
#endif
#if defined(_SOLARIS) || defined(_LINUX)
   lastchar = strlen(TempDir)-1;
   if( TempDir[lastchar] != '/' ) strcat( TempDir, "/" );
   lastchar = strlen(OutputDir)-1;
   if( OutputDir[lastchar] != '/' ) strcat( OutputDir, "/" );
#endif
   

   return;
}

/******************************************************************************
 *  arc2cube_lookup( )   Look up important info from earthworm.h tables       *
 ******************************************************************************/
void arc2cube_lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( InRingKey = GetKey(InRingName) ) == -1 ) {
        fprintf( stderr,
                "arc2cube:  Invalid ring name <%s>; exiting!\n", InRingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr,
              "arc2cube: error getting local installation id; exiting!\n" );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr,
              "arc2cube: Invalid module name <%s>; exiting!\n", MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr,
              "arc2cube: Invalid message type <TYPE_HEARTBEAT>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr,
              "arc2cube: Invalid message type <TYPE_ERROR>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_HYP2000ARC", &TypeHyp2000Arc ) != 0 ) {
      fprintf( stderr,
              "arc2cube: Invalid message type <TYPE_HYP2000ARC>; exiting!\n" );
      exit( -1 );
   }

   return;
}

/******************************************************************************
 * arc2cube_status() builds a heartbeat or error message & puts it into       *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/
void arc2cube_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size;
   time_t        t;

/* Build the message
 *******************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
        sprintf( msg, "%ld %ld\n\0", (long) t, (long) MyPid );
   }
   else if( type == TypeError )
   {
        sprintf( msg, "%ld %hd %s\n\0", (long) t, ierr, note);
        logit( "et", "arc2cube: %s\n", note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &InRegion, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","arc2cube:  Error sending heartbeat.\n" );
        }
        else if( type == TypeError ) {
           logit("et","arc2cube:  Error sending error:%d.\n", ierr );
        }
   }

   return;
}

/*******************************************************************
 *  arc2cube_hinvarc( )  read a Hypoinverse archive message        *
 *                       and write a cube message to disk          *
 *******************************************************************/
int arc2cube_hinvarc( char* arcmsg, MSG_LOGO incoming_logo )
{
   char      line[MAX_STR];  /* to store lines from msg               */
   char      shdw[MAX_STR];  /* used only to keep read_hyp happy      */
   FILE     *fp;
   char      tempname[MAX_STR];
   char      finalname[MAX_STR];
   int       ret;

  /* Read summary line from arcmsg; process them
   ***********************************************************************/
   if ( sscanf( arcmsg, "%[^\n]", line ) != 1 )  return( -1 );
   /*logit( "e", "%s\n", line );*/  /*DEBUG*/

   /* Process the hypocenter card (1st line of msg) & its shadow
    ************************************************************/
   ret = read_hyp( line, shdw, &Sum );
   if (ret) return 1;
   
   /* Write cube message to file
    ***************************************/
   sprintf(tempname, "%s%d.evt", TempDir, Sum.qid);
   sprintf(finalname, "%s%d.evt", OutputDir, Sum.qid);
   if ((fp = fopen(tempname, "w")) == NULL) {
       arc2cube_status( TypeError, ERR_FILEIO, "Error creating cube file" );
       return(0);
   }

   if( write_cube(fp, CUBE_EVENT_FORMAT, &Sum, NetCode, LocMethod, "" ) != 0 )
   {
      arc2cube_status( TypeError, ERR_FILEIO, "Error writing cube file" );
      return(0);
   }

   if (rename_ew(tempname, finalname) != 0) {
       logit("et", "Error renaming %s to %s: %s\n", tempname, finalname,
	     strerror(errno));
       arc2cube_status( TypeError, ERR_FILEIO, "Error moving cube file" );
   }

   return(0);
}



