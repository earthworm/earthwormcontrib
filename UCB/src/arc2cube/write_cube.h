/*	$Id: write_cube.h 474 2009-06-24 16:33:16Z lombard $	*/

#ifndef WRITE_CUBE_H
#define WRITE_CUBE_H


/* Macros for various Cube message types */
#define CUBE_EVENT_FORMAT	"E"
#define CUBE_DELETE_FORMAT	"DE"

int write_cube(FILE *fp, char *format, struct Hsum *sumP, char *netcode, 
	       char LocaMethod, char *text);



#endif
