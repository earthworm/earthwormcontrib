/* 
 *   import_data.c:  Program to receive messages from far away via socket 
 *              and put them into a transport ring.

The intent is that there will be a specific import_xxx module for importing from
institution xxx. That is, there will be as many import modules as places to 
import from, each module having it's own name, and living in its own directory.
Each such module could have it's own "import_filter" routine, coded for the
specific job.

Upgraded to move binary messages. Alex 10/4/96. See comments in export_generic.c

Modified for Berkeley by Doug Neuhauser, UC Berkeley Seismo Lab
1.  Added option to insert heartbeat from remote server into ring.
2.  Added optional override of RemModId and RemInstId for remote heartbeat to ring.
3.  Non-blocking I/O on the socket, multi-char reads.
4.  Fixed problem in unquoting code.
 */

#ifdef _OS2
#define INCL_DOSPROCESS
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#include <types.h>
#include <nerrno.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <fcntl.h>
#include <math.h>
#include <time.h> 
#include <sys/socket.h>
#include <netdb.h>

#ifdef _SOLARIS
#include <unistd.h>
#include <fcntl.h>
#include <sys/filio.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <wait.h>
#include <thread.h>
#endif

#include <errno.h>
#include <signal.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <imp_exp_gen.h>
#include <trace_buf.h>
 
#include <qlib2.h>

#define MAX_LOGO  20

/* Functions in this source file 
 *******************************/
void  config  ( char * );
void  lookup  ( void );
void  output_status  ( unsigned char, unsigned char, unsigned char, short, char * );
void   import_filter  ( char *, int);

#ifdef _OS2                          /* OS2 ONLY */
void  Heartbeat( void * );
void  MessageReceiver( void * );
#endif
#ifdef _SOLARIS                      /* SOLARIS ONLY */
void *Heartbeat( void * );
void *MessageReceiver( void * );
#endif

int   WriteToSocket  ( int, char *, MSG_LOGO * );

extern int  errno;
static  SHM_INFO  Region;      /* shared memory region to use for i/o    */

/* Things to read or derive from configuration file
 **************************************************/
static char    RingName[32];        /* name of transport ring for i/o    */
static char    MyModName[32];       /* speak as this module name/id      */
static char    RemModName[20];       /* remote module name/id		    */
static char    RemInstName[20];	  /* remote institution name/id		    */
static int     LogSwitch;           /* 0 if no logfile should be written */
static int     HeartBeatInt;        /* seconds between heartbeats (to local
					ring)  */
static long    MaxMsgSize;          /* max size for input/output msgs    */
static int     MyAliveInt;	    /* Seconds between sending alive message to foreign sender */
static char    MyAliveString[100];  /* Text of above alive message */

static char    SenderIpAdr[32];      /* Foreign sender's address, in dot notation */
static int     SenderPort;          /* Server's well-known port number   */
static int     SenderHeartRate;     /* Expect alive messages this often from foreign sender */
static char    SenderHeartText[100]; /* Text making up the sender's heart beat message */
MSG_LOGO       PutAsLogo;           /* logo to be used for placing received messages into ring. */
				    /* May be superceeded in the message processing routine
				      "import_filter" */

/* Globals: timers, etc, used by both threads */
/**********************************************/
#define CONNECT_WAIT_DT 10      /* Seconds wait between connect attempts */
#define THEAD_STACK_SIZE 8192   /* Implies different things on different systems !! */
		/* on os2, the stack grows dynamically beyond this number if needed, but likey */
		/* at the expesene of performance. Solaris: not sure, but overflow might be fatal */
time_t LastServerBeat;          /* times of heartbeats from the server machine */
time_t MyLastInternalBeat;  	/* time of last heartbeat into the local Earthworm ring */
time_t MyLastSocketBeat;   	/* time of last heartbeat to server - via socket */
int HeartThreadStatus = 0;  		/* goes negative if server croaks. Set by heartbeat thread */
int MessageReceiverStatus =0;   /* status of message receiving thread: -1 means bad news */
char* MsgBuf;		        /* incoming message buffer; used by receiver thread */
int  Sd;                        /* Socket descriptor    */
unsigned  TidHeart;		/* thread id. was type thread_t on Solaris! */
unsigned  TidMsgRcv;		/* thread id. was type thread_t on Solaris! */
char Text[256];			/* quite crude - array for error text */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o     */
static unsigned char MyInstId;        /* local installation id             */
static unsigned char MyModId;       /* Module Id for this program        */
static unsigned char RemModId;      /* optional Remote Module Id		*/
static unsigned char RemInstId;	    /* optional - remote installation id.	*/
static unsigned char TypeHeartBeat; 
static unsigned char TypeError;
static unsigned char TypeTracebuf;
static unsigned char TypeMSTracebuf;
static unsigned char TypePick2;
static unsigned char TypeMHH;

/* Error messages used by import 
 *********************************/
#define  ERR_SOCK_READ       0   /* error reading from socket  */
#define  ERR_TOOBIG          1   /* retreived msg too large for buffer     */
#define  ERR_TPORTPUT        2   /* error putting message into ring */
#define  ERR_SERVER_DEAD     3   /* server machine heartbeats not received on time */
#define  ERR_GARBAGE_IN     4   /* something other than STX after ETX */

#define	MAXWARNINGS	100	/* max warnings about data format.	*/
#define	BIG_ENDIAN_TYPES	"st"
#define	LITTLE_ENDIAN_TYPES	"if"

static char *cmdname;		/* program name.			*/
static char perrmsg[80];	/* prefix for system error messages.	*/
static char *my_wordorder_types;	/* character to indicate wordorder.	*/
static char *foreign_wordorder_types;	/* character to indicate wordorder.	*/
static int terminate_proc = 0;

/************************************************************************/
/*  finish_handler:							*/
/*	Signal handler -  sets terminate flag so we can exit cleanly.	*/
/************************************************************************/
void finish_handler(int sig)
{
    signal (sig,finish_handler);    /* Re-install handler (for SVR4)	*/
    terminate_proc = 1;
    return;
}

main( int argc, char **argv )
{
   long  nchar;			     /* counter for above buffer */
   int nr;			     /* number of characters read from socket */
   char chr;   			     /* character read from socket */
   int  on = 1;
   int  clientLen;
   u_long addr;			     /* binary form of sender ip address */
   struct hostent         *host;
   struct sockaddr_in insocket;
   time_t nowQuit;		     /* for deciding when to check for quitting */
   time_t lastQuit=0;
   time_t now;
   int state;
   /* to keep track of message assembly */
   char startCharacter=STX;	     /* frame start characer; from imp_exp_gen.h (ASCII STX) */
   char endCharacter=ETX;	     /* ditto frame end. (ASCII ETX) */
   int iret;			     /* misc. retrun values */
   int quit;
   int retryCount; /* to prevent flooding the log file */
    int status;
    int noblock;

/* Catch broken socket signals
******************************/
#ifdef _SOLARIS
   (void)sigignore(SIGPIPE);
#endif
   terminate_proc = 0;
   signal (SIGINT,finish_handler);
   signal (SIGTERM,finish_handler);

   /* Check command line arguments 
   ******************************/
   cmdname = tail(argv[0]);
   get_my_wordorder();
   if (my_wordorder == SEED_BIG_ENDIAN) {
	my_wordorder_types = BIG_ENDIAN_TYPES;
	foreign_wordorder_types = LITTLE_ENDIAN_TYPES;
   }
   else {
	my_wordorder_types = LITTLE_ENDIAN_TYPES;
	foreign_wordorder_types = BIG_ENDIAN_TYPES;
   }
   if ( argc != 2 )
   	{
	fprintf( stderr, "Usage: %s <configfile>\n", cmdname );
	exit( 0 );
   	}
	   
   /* Read the configuration file(s)
   ********************************/
   config( argv[1] );

   /* Look up important info from earthworm.h tables
   ************************************************/
   lookup();

   /* Initialize name of log-file & open it 
   ***************************************/
   logit_init( cmdname, (short) MyModId, 256, LogSwitch );
   logit( "e" , "%s: Read command file <%s>\n", cmdname, argv[1] );

   /* Attach to Input/Output shared memory ring 
   *******************************************/
   tport_attach( &Region, RingKey );
   logit( "e", "%s: Attached to public memory region %s: %d\n", cmdname, 
	  RingName, RingKey );

   /* Allocate the message buffer
   ******************************/
   if ( ( MsgBuf = (char *) malloc(MaxMsgSize) ) == (char *) NULL ) 
	{
	logit("e","%s: Cant allocate message buffer of %ld bytes\n", cmdname,MaxMsgSize);
	exit( -1 );
        }
 
   /* Initialize the socket system
   ******************************/
   SocketSysInit();

   /* fill in the hostent structure, given dot address as a char string
   ********************************************************************/
   if ((int)(addr = inet_addr(SenderIpAdr)) == -1)
      {
      logit( "e", "%s: inet_addr failed. Exitting.\n", cmdname );
      tport_detach( &Region );
      free(MsgBuf);
      exit( -1 );
      }

   host = gethostbyaddr((char *)&addr, sizeof (addr), AF_INET);
   if (host == NULL) 
     {
     logit( "e", "%s: gethostbyaddr failed for %s. Exitting.\n", cmdname, SenderIpAdr);
     tport_detach( &Region );
     free(MsgBuf);
     exit( -1 );
     }

   /* Stuff address and port into socket structure
   ********************************************/
   memset( (char *)&insocket, '\0', sizeof(insocket) );
   insocket.sin_family = AF_INET;
   insocket.sin_port   = htons( (short)SenderPort );
   memcpy( (char *)&insocket.sin_addr, host->h_addr_list[0], host->h_length );

   /* to prevent flooding the log file during long reconnect attempts */
   /********************************************************************/
   retryCount=0;  /* it may be reset elsewere */

				 /********************/
				      reconnect:
				 /********************/
   retryCount++; 
  
   /* Create a socket
   ****************/
   if ( ( Sd = socket( AF_INET, SOCK_STREAM, 0 ) ) == -1 )
      {
      sprintf (perrmsg, "%s: socket", cmdname);
      SocketPerror(perrmsg);
      logit( "et", "%s: Error opening socket.  Exitting\n", cmdname );
      SocketClose(Sd);
      free(MsgBuf);
      exit(0);
      }

   /* Try for a network connection - and keep trying forever !!
   ************************************************************/
   if(retryCount< 4)logit("et","%s: Trying to connect to %s on port %d\n", cmdname,SenderIpAdr,SenderPort);
   if(retryCount==4)logit("et","%s: repetitions not logged\n", cmdname);
   if ( connect( Sd, (struct sockaddr *)&insocket, sizeof(insocket) ) == -1 )
      {
      /* Are we being told to quit */
      /*****************************/
      if ( tport_getflag( &Region ) == TERMINATE || terminate_proc )
	 {
         tport_detach( &Region );
         logit("et","%s: terminating on request\n", cmdname);
	 (void)KillThread(TidMsgRcv);
	 free(MsgBuf);
	 (void)KillThread(TidHeart);
         exit(0);	
	 }
      SocketClose( Sd );
      if(retryCount< 4)logit("et","%s: Failed to connect. Waiting\n", cmdname);
      if(retryCount==4)logit("et","%s: repetitions not logged\n", cmdname);
      sleep_ew(CONNECT_WAIT_DT*1000);
      goto reconnect;    /*** JUMP to reconnect ***/
      }
   logit("et","%s: Connected after %d seconds\n", cmdname,CONNECT_WAIT_DT*(retryCount-1));
   retryCount=0;
   state=0 ; /* which means we're initializing - no expectations */

    /* Set input file descriptor for non-blocking I/O.		*/
    noblock = 1;
    if ((status = ioctl (Sd, FIONBIO, &noblock)) != 0) { 
	logit ("et", "Error setting non-blocking I/O");
	 (void)KillThread(TidMsgRcv);
	 free(MsgBuf);
	 (void)KillThread(TidHeart);
         exit(0);	
    }
    /* Empty input buffer. */
    InitInputBuffer(Sd);

   /* Start the heartbeat thread
   ****************************/
   HeartThreadStatus=0;  /* set it's status flag to ok */
   time(&MyLastInternalBeat); /* initialize our last heartbeat time */
   time(&MyLastSocketBeat);  /* initialize time of our heartbeat over socket */
   time(&LastServerBeat); /* initialize time of last heartbeat from serving machine */
   if ( StartThread( Heartbeat, (unsigned)THEAD_STACK_SIZE, &TidHeart ) == -1 )
      {
      logit( "e","%s: Error starting Heartbeat thread. Exitting.\n", cmdname );
      tport_detach( &Region );
      free(MsgBuf);
      exit( -1 );
      }

   /* Start the message receiver thread
   **********************************/
   MessageReceiverStatus =0; /* set it's status flag to ok */
   if ( StartThread( MessageReceiver, (unsigned)THEAD_STACK_SIZE, &TidMsgRcv ) == -1 )
      {
      logit( "e","%s: Error starting MessageReceiver thread. Exitting.\n", cmdname );
      tport_detach( &Region );
      free(MsgBuf);
      exit( -1 );
      }

   /* Working loop: check on server heartbeat status, check on receive thread health.
      check for shutdown requests. If things go wrong, kill all  threads and restart
   *********************************************************************************/
   quit=0; /* to restart or not to restart */
   while (1)
      {
      sleep_ew(1000); /* sleep one second. Remember, the receieve thread is awake */
      time(&now);

      /* How's the server's heart? */
      /*****************************/
      if (difftime(now,LastServerBeat) > (double)SenderHeartRate && SenderHeartRate !=0)
         {
	 output_status(MyInstId,MyModId,TypeError,ERR_SERVER_DEAD," Server heartbeat lost. Restarting");
         quit=1; /*restart*/
	 }

      /* How's the receive thread feeling ? */
      /**************************************/
      if ( MessageReceiverStatus == -1)
	 {
	 logit("et","%s: Receiver thread unhappy. Restarting\n", cmdname);
	 quit=1;
	 }

      /* How's the heartbeat thread feeling ? */
      /**************************************/
      if ( HeartThreadStatus == -1)
	 {
	 logit("et","%s: Heartbeat thread unhappy. Restarting\n", cmdname);
	 quit=1;
	 }

      /* Are we being told to quit */
      /*****************************/
      if ( tport_getflag( &Region ) == TERMINATE || terminate_proc )
	 {
         tport_detach( &Region );
         logit("et","%s: terminating on request\n", cmdname);
	 (void)KillThread(TidMsgRcv);
	 free(MsgBuf);
	 (void)KillThread(TidHeart);
         exit(0);	
	 }

      /* Any other shutdown conditions here */
      /**************************************/
      /* blah blah blah */

      /* restart preparations */
      /************************/
      if (quit == 1)
	 {
	 (void)KillThread(TidMsgRcv);
 	 (void)KillThread(TidHeart);
         SocketClose( Sd );
	 quit=0;
         goto reconnect;
         }

       }  /* end of working loop */
	 
	 	     
	
   }

/************************Messge Receiver Thread *********************
*          Listen for client heartbeats, and set global variable     *
*          showing time of last heartbeat. Main thread will take     *
*          it from there                                             *
**********************************************************************/
/*
Modified to read binary messages, alex 10/10/96: The scheme (I got it from Carl) is define
some sacred characters. Sacred characters are the start-of-message and end-of-message
framing characters, and an escape character. The sender's job is to cloak unfortunate bit
patters in the data which look like sacred characters by inserting before them an 'escape' 
character.  Our problem here is to recognize, and use, the 'real' start- and end-of- 
messge characters, and to 'decloak' any unfortunate look-alikes within the message body.
*/

#ifdef _OS2
void   MessageReceiver( void *dummy )
#endif
#ifdef _SOLARIS
void * MessageReceiver( void *dummy )
#endif
{
   static int state=0;
   static char chr, lastChr;
   static int nr;
   static char errText[256];
   static long  nchar;			     /* counter for above buffer */
   static char startCharacter=STX;	     /* ASCII STX characer */
   static char endCharacter=ETX;	     /* ASCII ETX character */
   static char escape=ESC;		     /* our escape character */
   static int iret;			     /* misc. retrun values */
   static time_t now;
   char cInst[4], cMod[4], cType[4];
   MSG_LOGO logo;

   /* Tell the main thread we're ok 
   *******************************/
   MessageReceiverStatus=0;

   state=0; /* we're initializing */

   /* Read first character from socket
   ***********************************/
   
   /* NOTE -- we are guaranteed to drop at least one initial packet,	*/
   /* since we cannot successfully frame on the first packet.		*/

   nr=Recv(Sd,&chr,1,0); if (nr <= 0) goto suicide; /* get one character */
   nchar=0; /* next character position to load */

   /* Working loop: receive and process messages
    ********************************************/
   /* We are either  (0) initializing: searching for message start
      		     (1) expecting a message start: error if not
		     (2) assembling a message
      the variable "state' determines our mood */

   while(1) /* loop over bytes read from socket */
	{
	/* Initialization */
	/******************/
	if (state==0)   /* throw all away until we see a naked start character */
		{  
		/* Read from socket
		*******************/
		lastChr=chr;
		nr=Recv(Sd,&chr,1,0); if (nr <= 0) goto suicide; /* get one character */
		nchar=0; /* next character position to load */

		/* Do we have a real start character?
		*************************************/
		if ( lastChr!=escape && chr==startCharacter)
			{
			state=2;  /*from now on, assemble message */
			continue;
			}
		}

	/* Confirm message start */
  	/*************************/
	if (state==1)  /* the next char had better be a start character - naked, that is */
		{ 
		/* Read from socket
		*******************/
		lastChr=chr;
		nr=Recv(Sd,&chr,1,0); if (nr <= 0) goto suicide; /* get one character */

		/* Is it a naked start character?
		*********************************/
		if ( chr==startCharacter &&  lastChr != escape) /* This is it: message start!! */
			{
			nchar=0; /* start with firsts char position */
			state=2; /* go into assembly mode */
			continue;
			}
		else   /* we're eating garbage */
			{
			logit("et","unexpected character from client. Re-synching\n");
  			state=0; /* search for next start sequence */
			continue;
			}
		}

	/* In the throes of assembling a message */
	/****************************************/
	if (state==2)
		{ 
		/* Read from socket
		*******************/
		lastChr=chr;
		nr=Recv(Sd,&chr,1,0); if (nr <= 0) goto suicide; /* get one character */
		
		/* Is this the end?
		*******************/
		if (chr==endCharacter)   /* naked end character: end of the message is at hand */
			{ 
			/* We have a complete message */
			/*****************************/
			MsgBuf[nchar]=0; /* terminate as a string */
			if(strcmp(&MsgBuf[9],SenderHeartText)==0) /* Server's heartbeat */
				{
				/* printf("%s: Received heartbeat\n", cmdname); */
	   			time(&LastServerBeat); /* note time of heartbeat */
				/* Peel off the logo chacters */
				strncpy(cInst,MsgBuf,    3);  cInst[3]=0;  logo.instid =(unsigned char)atoi(cInst);
				strncpy(cMod ,&MsgBuf[3],3);  cMod[3] =0;  logo.mod   =(unsigned char)atoi(cMod);
				strncpy(cType,&MsgBuf[6],3);  cType[3]=0;  logo.type  =(unsigned char)atoi(cType);
				/* Allow config file info to override logo info for heartbeat. */
				if (RemInstId != 0) logo.instid = RemInstId;
				if (RemModId !=0) logo.mod = RemModId;
				output_status( logo.instid,logo.mod,TypeHeartBeat, 0, "" );
				state=1; /* reset for next message */
				MsgBuf[0]=' '; MsgBuf[9]=' ';
		   		}
			else
				{
				/* got a non-heartbeat message */
				import_filter(MsgBuf,nchar); /* process the message via user-routine */
				}
			state=1;
			continue;
			}
		else  
			{
			/* process the message byte we just read: we know it's not a naked end character*/
			/********************************************************************************/
			/* Escape sequence? */
			if (chr==escape)  
				{  /*  process the escape sequence */

				/* Read from socket
				*******************/
				lastChr=chr;
				nr=Recv(Sd,&chr,1,0); if (nr <= 0) goto suicide; /* get one more character */

				if( chr != startCharacter && chr != endCharacter && chr != escape)
					{   /* bad news: unknown escape sequence */
					logit("et","unknown escape sequence in message. Re-synching\n");
		  			state=0; /* search for next start sequence */
					continue;
					}
				else   /* it's ok: it's a legit escape sequence, and we save the escaped byte */
					{
		        		MsgBuf[nchar++]=chr; if(nchar>MaxMsgSize) goto freak; /*save character */
					continue;
					}
				}

			/*  Naked start character? */
			if (chr==startCharacter)
				{   /* bad news: unescaped start character */
				logit("et","unescaped start character in message. Re-synching\n");
	  			state=0; /* search for next start sequence */
				continue;
				}

			/* So it's not a naked start, escape, or a naked end: Hey, it's just a normal byte */
        		MsgBuf[nchar++]=chr; if(nchar>MaxMsgSize) goto freak; /*save character */
			continue;

			freak:  /* freakout: message won't fit */
				{
				logit("et","%s: receive buffer overflow after %ld bytes\n", cmdname,MaxMsgSize);
				state=0; /* initialize again */
				nchar=0;
				continue;
				}
			} /* end of not-an-endCharacter processing */
		} /* end of state==2 processing */
	}  /* end of loop over characters */

suicide:
   sprintf (perrmsg, "%s: recv()", cmdname);
   SocketPerror(perrmsg);
   sprintf(errText,"Bad socket read: %d\n",nr);
   logit("et",errText);
   MessageReceiverStatus=-1; /* file a complaint to the main thread */
   KillSelfThread(); /* main thread will restart us */
   logit("et","Fatal system error: Receiver thread could not KillSelf\n");
   exit(-1);

   }  /* end of MessageReceiver thread */



/***************************** Heartbeat **************************
 *           Send a heartbeat to the transport ring buffer        *
 *           Send a heartbeat to the server via socket            *
 *                 Check on our server's hearbeat                 *
 *           Slam socket shut if no Server heartbeat: that        *
 *            really shakes up the main thread                    *
 ******************************************************************/
#ifdef _OS2
void  Heartbeat( void *dummy )
#endif
#ifdef _SOLARIS
void *Heartbeat( void *dummy )
#endif
{
   int iret;
   MSG_LOGO 	reclogo;
   time_t now;

   /* once a second, do the rounds. If anything looks bad, set HeartThreadStatus to -1
      and go into a long sleep. The main thread should note that our status is -1,
      and launch into re-start procedure, which includes killing and restarting us. */
   while ( 1 )
      {
      sleep_ew(1000);
      time(&now);

      /* Beat our heart (into the local Earthworm) if it's time
       ********************************************************/
      if (difftime(now,MyLastInternalBeat) > (double)HeartBeatInt)
       	{	
        output_status( MyInstId,MyModId,TypeHeartBeat, 0, "" );
	time(&MyLastInternalBeat);
        }

      /* Beat our heart (over the socket) to our server
      **************************************************/
      if (difftime(now,MyLastSocketBeat) > (double)MyAliveInt && MyAliveInt != 0)
	{
         reclogo.instid = MyInstId;
         reclogo.mod   = MyModId;
         reclogo.type  = TypeHeartBeat;
         if ( WriteToSocket( Sd, MyAliveString, &reclogo ) != 0 )
            {
            /* If we get an error, simply quit */
            sprintf( Text, "error sending alive msg to socket. Heart thread quitting\n" );
            output_status( MyInstId,MyModId,TypeError, ERR_SOCK_READ, Text );
       	    logit("et",Text);
            HeartThreadStatus=-1;
	    KillSelfThread();  /* the main thread will resurect us */
	    logit("et","Fatal system error: Heart thread could not KillSelf\n");
	    exit(-1);
            }
	 MyLastSocketBeat=now;
          }	   
      }	
   }


/*****************************************************************************
 *  config() processes command file(s) using kom.c functions;                *
 *                    exits if any errors are encountered.	             *
 *****************************************************************************/
void config( char *configfile )
{
   int      ncommand;     /* # of required commands you expect to process   */ 
   char     init[20];     /* init flags, one byte for each required command */
   int      nmiss;        /* number of required commands that were missed   */
   char    *com;
   char    *str;
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command 
 *****************************************************/   
   ncommand = 11;
   for( i=0; i<ncommand; i++ )  init[i] = 0;

/* Open the main configuration file 
 **********************************/
   nfiles = k_open( configfile ); 
   if ( nfiles == 0 ) {
	fprintf( stderr,
                "%s: Error opening command file <%s>; exitting!\n", cmdname, 
                 configfile );
	exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {  
	    com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr, 
                          "%s: Error opening command file <%s>; exitting!\n", cmdname,
                           &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command 
         ************************************/
  /*0*/     if( k_its("MyModuleId") ) {
		str=k_str();
		if(str) strcpy(MyModName,str);
                init[0] = 1;
            }
  /*1*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("HeartBeatInt") ) {
                HeartBeatInt = k_int();
                init[2] = 1;
            }
  /*3*/	    else if(k_its("LogFile") ) {
		LogSwitch=k_int();
		init[3]=1;
	    }

         /* Maximum size (bytes) for incoming messages
          ********************************************/ 
  /*4*/     else if( k_its("MaxMsgSize") ) {
                MaxMsgSize = k_long();
                init[4] = 1;
	    }

	/* 5 Interval for alive messages to sending machine
	***************************************************/
	    else if( k_its("MyAliveInt") ) {
		MyAliveInt = k_int();
		init[5]=1;
	    }

	/* 6 Text of alive message to sending machine
	*********************************************/
	else if( k_its("MyAliveString") ){
		str=k_str();
		if(str) strcpy(MyAliveString,str);
		init[6]=1;
	}

	/* 7 Sender's internet address, in dot notation
	***********************************************/
	else if(k_its("SenderIpAdr") ) {
		str=k_str();
		if(str) strcpy(SenderIpAdr,str);
		init[7]=1;
	}		

	/* 8 Sender's Port Number
	*************************/
	    else if( k_its("SenderPort") ) {
		SenderPort = k_int();
		init[8]=1;
	    }

	/* 9 Sender's Heart beat interval
	********************************/
	    else if( k_its("SenderHeartRate") ) {
		SenderHeartRate = k_int();
		init[9]=1;
	    }

	/* 10 Sender's heart beat text
	******************************/
	else if(k_its("SenderHeartText") ) {
		str=k_str();
		if(str) strcpy(SenderHeartText,str);
		init[10]=1;
	    }		

        else if( k_its("RemModuleId") ) {
                str = k_str();
                if(str) strcpy( RemModName, str );
	}
  	else if( k_its("RemInstId") ) {
                str = k_str();
                if(str) strcpy( RemInstName, str );
	}

        /* Unknown command
        *****************/ 
	    else {
                fprintf( stderr, "<%s> Unknown command in <%s>.\n", 
                         com, configfile );
                continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr, 
                       "Bad <%s> command in <%s>; exitting!\n",
                        com, configfile );
               exit( -1 );
            }
	}
	nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "%s: ERROR, no ", cmdname );
       if ( !init[0] )  fprintf( stderr, "<MyModuleId> "      );
       if ( !init[1] )  fprintf( stderr, "<RingName> "   );
       if ( !init[2] )  fprintf( stderr, "<HeartBeatInt> "     );
       if ( !init[3] )  fprintf( stderr, "<LogFile> " );
       if ( !init[4] )  fprintf( stderr, "<MaxMsgSize> "   );
       if ( !init[5] )  fprintf( stderr, "<MyAliveInt> "   );
       if ( !init[6] )  fprintf( stderr, "<MyAliveString> " );
       if ( !init[7] )  fprintf( stderr, "<SenderIpAdr> "    );
       if ( !init[8] )  fprintf( stderr, "<SenderPort> "   );
       if ( !init[9] )  fprintf( stderr, "<SenderHeartRate> "   );
       if ( !init[10] )  fprintf( stderr, "<SenderHeartText argh> "   );
       fprintf( stderr, "command(s) in <%s>; exitting!\n", configfile );
       exit( -1 );
   }

   return;
}

/****************************************************************************
 *  lookup( )   Look up important info from earthworm.h tables       *
 ****************************************************************************/
void lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 ) {
	fprintf( stderr,
 	        "%s:  Invalid ring name <%s>; exitting!\n", cmdname, RingName);
	exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &MyInstId ) != 0 ) {
      fprintf( stderr, 
              "%s: error getting local installation id; exitting!\n", cmdname );
      exit( -1 );
   }
   if ( RemInstName[0] != 0 && GetInst( RemInstName, &RemInstId ) != 0 ) {
      fprintf( stderr,
              "%s: error getting installation id %s; exiting!\n", 
	      cmdname, RemInstName );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid module name <%s>; exitting!\n", cmdname, MyModName );
      exit( -1 );
   }
   if ( RemModName[0] != 0 && GetModId( RemModName, &RemModId ) != 0 ) {
      fprintf( stderr,
              "%s: Invalid module name <%s>; exiting!\n", cmdname, RemModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_HEARTBEAT>; exitting!\n", cmdname );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_ERROR>; exitting!\n", cmdname );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF", &TypeTracebuf ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_TRACEBUF>; exiting!\n", cmdname );
      exit( -1 );
   }
   if ( GetType( "TYPE_MSTRACEBUF", &TypeMSTracebuf ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_MSTRACEBUF>; exiting!\n", cmdname );
      exit( -1 );
   }
   if ( GetType( "TYPE_PICK2", &TypePick2 ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_PICK2>; exiting!\n", cmdname );
      exit( -1 );
   }
   if ( GetType( "TYPE_MHH", &TypeMHH ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_MHH>; exiting!\n", cmdname );
      exit( -1 );
   }
   return;
} 

/*******************************************************************************
 * output_status() builds a heartbeat or error message & puts it into          *
 *                 shared memory.  Writes errors to log file & screen.         *
 *******************************************************************************/
void output_status( unsigned char instid, unsigned char modid, unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char	       msg[256];
   long	       size;
   time_t        t;
 
/* Build the message
 *******************/ 
   logo.instid = instid;
   logo.mod = modid;
   logo.type  = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
	sprintf( msg, "%ld\n\0", t);
   }
   else if( type == TypeError )
   {
	sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
	logit( "et", "%s: %s\n", cmdname, note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */ 	

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","%s:  Error sending heartbeat.\n", cmdname );
	}
	else if( type == TypeError ) {
           logit("et","%s:  Error sending error:%d.\n", cmdname, ierr );
	}
   }

   return;
}

/************************** import_filter *************************
 *           Decide  what to do with this message                 *
 *                                                                *
 ******************************************************************
/*	This routine is handed each incoming message, and can do what it likes
with it. The intent is that here is where installation-specificd processing
is done, such as deciding whether we want this message (e.g. is the pick from
an intersting station?), changing its format, etc.
	The default action is to assume that the incoming message was generated
by the Earthworm standard "export_generic" module, which has a companion "export-filter"
routine. The default form of that routine attaches the logo at the start of the
message and exports it. So we peel it off, and drop it into our local transport
medium under that logo.
	We assume that the first 9 characters are three three-character fields
giving IstallationId, ModuleId, and MessageType.
*/


void import_filter( char *msg, int msgLen )
{
    char cInst[4], cMod[4], cType[4];
    MSG_LOGO logo;
    char *p;
    static int warnings = 0;

    /* We assume that this message was created by our bretheren "export_default",
       which attaches the logo as three groups of three characters at the front of
       the message */
    /* Peel off the logo chacters */
    strncpy(cInst,msg,    3);  cInst[3]=0;  logo.instid =(unsigned char)atoi(cInst);
    strncpy(cMod ,&msg[3],3);  cMod[3] =0;  logo.mod   =(unsigned char)atoi(cMod);
    strncpy(cType,&msg[6],3);  cType[3]=0;  logo.type  =(unsigned char)atoi(cType);
/*:: DEBUG 
    fprintf (stderr, "import_filter: instid=%d module=%d type=%d msglen=%d\n", 
	     logo.instid, logo.mod, logo.type, msgLen);
::*/

   /* Write the message to shared memory
   ************************************/
    if (logo.type == TypeTracebuf) {
	TRACE_HEADER *trace_header = (TRACE_HEADER *)&msg[9];
	if ((p=strchr(my_wordorder_types,trace_header->datatype[0]))!=NULL) {
	    /* Do nothing - data in my byte order.		    */
	}
	else if ((p=strchr(foreign_wordorder_types,trace_header->datatype[0]))!=NULL) {
	    /* Convert TRACEBUF data to out computer's byte order.  */
	    swab4(&trace_header->pinno);
	    swab4(&trace_header->nsamp);
	    swab8(&trace_header->starttime);
	    swab8(&trace_header->endtime);
	    swab8(&trace_header->samprate);
	    if (trace_header->datatype[1] == '4') {
		int *idata = (int *)msg+8+sizeof(TRACE_HEADER);
		int i;
		for (i=0; i<trace_header->nsamp; i++) {
		    swab4(idata+i);
		}
	    }
	    else if (trace_header->datatype[1] == '2') {
		short int *idata = (short int *)msg+8+sizeof(TRACE_HEADER);
		int i;
		for (i=0; i<trace_header->nsamp; i++) {
		    swab2(idata+i);
		}
	    }
	    else if (trace_header->datatype[1] == '8') {
		double *ddata = (double *)msg+8+sizeof(TRACE_HEADER);
		int i;
		for (i=0; i<trace_header->nsamp; i++) {
		    swab8(ddata+i);
		}
	    }
	    else {
		/* Unknown data length - discard record. */
		if (warnings++ < MAXWARNINGS) {
		    logit("et","%s: Unknown TRACEBUF format %s for logo "
			  "instid=%d mod=%d type=%d\n", cmdname, 
			  trace_header->datatype,
			  logo.instid, logo.mod, logo.type);
		}
		return;
	    }
	    trace_header->datatype[0] = my_wordorder_types[p-foreign_wordorder_types];
	}
	else {
	    /* Unknown format - discard record. */
	    if (warnings++ < MAXWARNINGS) {
		logit("et","%s: Unknown TRACEBUF format %s for logo "
		      "instid=%d mod=%d type=%d\n", cmdname, 
		      trace_header->datatype,
		      logo.instid, logo.mod, logo.type);
	    }
	    return;
	}
    }
   if( tport_putmsg( &Region, &logo, (long)(msgLen-9), &msg[9]) != PUT_OK )
        {
        logit("et","%s:  Error sending message via transport:\n", cmdname );
        return;
        }
   return;
}


/*************************** WriteToSocket ************************
 *    send a message logo and message to the socket               *
 *    returns  0 if there are no errors                           *
 *            -1 if any errors are detected                       *
 ******************************************************************/

int WriteToSocket( int ActiveSocket, char *msg, MSG_LOGO *logo )
{
   char asciilogo[10];       /* ascii version of outgoing logo */
   char startmsg = STX;       /* flag for beginning of message  */
   char endmsg   = ETX;       /* flag for end of message        */
   int  msglength;           /* total length of msg to be sent */
   int  nsend;               /* # characters to send at once   */
   int  i, rc;

/* Send "start of transmission" flag
 ***********************************/
   rc = send( ActiveSocket, &startmsg, 1, 0 );
   if( rc != 1 ) {
       sprintf (perrmsg, "%s: socket send", cmdname);
       SocketPerror(perrmsg);
       return( -1 );
   }

/* Send ascii representation of logo
 ***********************************/
   sprintf( asciilogo, "%3d%3d%3d",
           (int) logo->instid, (int) logo->mod, (int) logo->type );
   rc = send( ActiveSocket, asciilogo, 9, 0 );
   if( rc != 9 ) {
       sprintf (perrmsg, "%s: socket send", cmdname);
       SocketPerror(perrmsg);
       return( -1 );
   }
 
/* Debug print of message
*************************/
 /* printf("%s: sending under logo (%s):\n%s\n", cmdname,asciilogo,msg); */

/* Send message; break it into chunks if it's big!
 *************************************************/
/* Warning: Garbled data with nsend set to 32000. */
   nsend     = 1000;
   msglength = strlen(msg);
   for ( i=0; i<msglength; i+=nsend )
   {
       if ( nsend > msglength-i )  nsend = msglength-i;
                
       rc = send( ActiveSocket, &msg[i], nsend, 0 );
       if ( rc == -1 )
       {
	   sprintf (perrmsg, "%s: socket send", cmdname);
	   SocketPerror(perrmsg);
           return( -1 );
       }
       else if ( rc != nsend )
       {
           logit( "et",
                  "%s: error sending message to socket\n", cmdname );
           return( -1 );
       }
   }

/* Send "end of transmission" flag
 *********************************/
   rc = send( ActiveSocket, &endmsg, 1, 0 );
   if( rc != 1 ) {
       sprintf (perrmsg, "%s: socket send", cmdname);
       SocketPerror(perrmsg);
       return( -1 );
   }

   return( 0 );
}

/************************************************************************/
/*  Input character buffer for a single file descriptor.		*/
/************************************************************************/
#define	INBUFLEN   32768		/* Size of input buffer.	*/
static char InBuffer[INBUFLEN];		/* Input buffer.		*/
static int FreeCharIndex;		/* Index of free space in buffer*/
static int NextCharIndex;		/* Index of next char in buffer	*/
static int EOFSeen = 0;			/* EOF seen on socket.		*/
static int width = -1;			/* max # of file descriptors.	*/
static fd_set readfds;			/* Bitmap for read select.	*/

/************************************************************************/
/*  InitInputBuffer:							*/
/*	Empty the input buffer.						*/
/************************************************************************/
int InitInputBuffer (int Sd)
{
    FreeCharIndex = NextCharIndex = 0;
    EOFSeen = 0;
}

/************************************************************************/
/*  Recv:								*/
/*	Return buffered characters from the input file descriptor.	*/
/*	Modeled after recv() system call.				*/
/*	Last argument (flag) not used.					*/
/*  returns:								*/
/*	number of characters returned in buffer (> 0)			*/
/*	0 or EOF on error or socket closed.				*/
/************************************************************************/
int Recv(int Sd, char *buf, int bufsize, int flag)
{
    int status;
    int NChar;

    /* If EOF previously seen, return EOF.				*/
    if (EOFSeen) return (-1);

    /* If input buffer is empty, try to refill it.			*/
    if (NextCharIndex >= FreeCharIndex) {
	/* Input file descriptor has been set to non-blocking I/O.	*/
	/* Use select to wait until characters are available.		*/
	InitInputBuffer (Sd);
	if (width < 0) width = ulimit (4,0);
	FD_ZERO(&readfds);
	FD_SET(Sd,&readfds);
	status = 0;
	while ((status = select (width, &readfds, NULL, NULL, NULL)) == 0) ;
	if (status < 0) {
	    /* Error in select. */
	    fprintf (stderr, "Select returned %d\n", status);
	    close(Sd);
	    EOFSeen = 1;
	}
	/* Something is supposedly available on the socket.		*/
	/* Read as many characters as we can.				*/
	errno = 0;
	FreeCharIndex = read(Sd, InBuffer, INBUFLEN);
	if (FreeCharIndex <= 0) {
	    /* Error in read. */
	    fprintf (stderr, "Read returned %d\n", FreeCharIndex);
	    close (Sd);
	    EOFSeen = -1;
	}
    }
    
    /* If EOF previously seen, return EOF.				*/
    if (EOFSeen) return (-1);

    /* Return the maximum number of characters possible.		*/
    NChar = 0;
    while (NextCharIndex < FreeCharIndex && bufsize-- > 0) {
	buf[NChar++] = InBuffer[NextCharIndex++];
    }
    return (NChar);
}
