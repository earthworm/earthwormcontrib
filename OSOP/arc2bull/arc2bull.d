#            This is the configuration file for arc2bull
#
# All of the values need to have an "=" separating them unlike the config files
# in the rest of the EW systems
#
# Presence or lack of white space is not important
# Anything after a "#" is considered a comment
#
#  Do not use spaces in path or file names, use underscores
#  Do not use spaces in variable name
#  Do not use "-" dashes in variable or file names
#
#  i.e.  list_to_mail_to.txt
#  i.e   C:\earthworm\run\params\list_to_mail_to.txt
#  i.e.  /home/angel/earthquakes/mine/
#
#  Please DO end paths with a "\" or "/"
#  i.e   C:\earthworm\run\params\
#  i.e.  /home/angel/earthquakes/mine/
#
# Even though is is poor form, all of the values listed here are globals
# Most of the stuff here is ignored if not used.  So do not worry commenting
# out unused stuff.
#
# The important stuff is between the lines of #
#
################################################################################


log_path = C:\arc\logs\                 # Where to write the log file
arc_folder = C:\arc\                    # Where to look for .arc files
bull_file_folder = C:\bull\             # Where to write the Bulletin file
bull_file_name = bulletin.txt           # What the bulletin file is called


################################################################################
#
# What follows is FYI and showsthe first 80 columns of the of the first line
# of the .arc files.
#
# The column numbering below was cut and pasted from page 98 of the
# hypoinverse manual.  I have re-numbered the columns since Perl counts from 0 not 1
#
#
# First lines from two files as sent by Paul in the zipped file
#
# 200705150141454638 5580107 3400   33  0  5231  3  17 4658 44725328  94132     44 
# 200705160208240538 5642107 3339  137  0  7135  2   4 4231  52312 0  20113     17 
# 0         1         2         3         4         5         6         7         8
# 012345678901234567890123456789012345678901234567890123456789012345678901234567890
#
# Col. Len. Format Data (* revised from pre-Y2000 format)
# 
#
# 0 4 I4   Year. *
# 4 8 4I2  Month, day, hour and minute.
# 2 4 F4.2 Origin time seconds.
# 6 2 F2.0 Latitude (deg). First character must not be blank.
# 8 1 A1 S for south, blank otherwise.
# 9 4 F4.2 Latitude (min).
# 3 3 F3.0 Longitude (deg).
# 6 1 A1 E for east, blank otherwise.
# 7 4 F4.2 Longitude (min).
# 1 5 F5.2 Depth (km).
# 6 3 F3.2 Amplitude magnitude. *
# 9 3 I3   Number of P & S times with final weights greater than 0.1.
# 2 3 I3   Maximum azimuthal gap, degrees.
# 5 3 F3.0 Distance to nearest station (km).
# 8 4 F4.2 RMS travel time residual.
# 2 3 F3.0 Azimuth of largest principal error (deg E of N).
# 5 2 F2.0 Dip of largest principal error (deg).
# 7 4 F4.2 Size of largest principal error (km).
# 1 3 F3.0 Azimuth of intermediate principal error.
# 4 2 F2.0 Dip of intermediate principal error.
# 6 4 F4.2 Size of intermediate principal error (km).
# 0 3 F3.2 Coda duration magnitude. *






