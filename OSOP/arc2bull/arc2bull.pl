#!/usr/bin/perl

# Angel Rodriguez June 3, 2007
# Reads a configuration file
# Reads files from a folder where ew2file drops an ARC files
# Extracts the first line from those files
# parses the first line into a human readable format
# removes duplicate lines
# writes the collection of first lines into one bulletin style file.
# logs that the program was called

use strict;
use warnings;
my $write_to_the_log;
my $ini_file;
my $space = " ";
my @files = ();
my @arc_lines = ();
my %bull;


my $version = q($Revision: 335 $);
my $usage = "Usage: arc2bull <configfile>\n";

if ($ARGV[0]) {                
    $ini_file = shift;
}   else {print "\n$usage\n"; 
        exit 1;
    } 

if ($ini_file =~ m/^(\/|-|--)(\?|h|help)/i) {
  print $usage;
  print "\nI am version $version\n"; 
  print "\nFor more help or to make suggestion write angel\@volcanbaru.com\n";
  exit 1;
}

if ($ini_file =~ m/^(\/|-|--)(\?|v|ver|version)/i) {
    print "\nI am version $version\n";
    exit 1;
}

if (! -r $ini_file || ! -f $ini_file) {
    print <<"_";        # to be printed if .ini not readable as a file
    
Make sure that your configuration file is a plain text file that has DOS line
ending.  If you need help write angel\@volcanbaru.com
_

exit 1;
}
  
open INIFILE, "$ini_file";
while ($_ = <INIFILE>) {
  chomp;                                            # no newline
  s/#.*//;                                          # no comments
  s/^\s+//;                                         # no leading white space
  s/\s+$//;                                         # no trailing white space
  s/\s+//g;                                         # collapse internal spaces
  if (length($_)) {
     $_ =~ /\W/;
     $bull{$`} = $'; #load the hash with the values to the left and right of the match
  }
}
close INIFILE;

while ((my $key, my $value) = each(%bull)){
        my $path_sep = '/';
        (my $t = $value) =~ s/\\/$path_sep/g;
        $bull{$key} = $t;
}

my $time = scalar(localtime());
print my $logthis = "This is $version\n";
LOGIT($logthis);
print $logthis = "Starting up at $time with config file $ini_file\n";
LOGIT($logthis);
print $logthis = "Looking for the arc files in: $bull{arc_folder}\n";
LOGIT($logthis);
print $logthis = "Writing the bulletin to: $bull{bull_file_folder}$bull{bull_file_name}\n";
LOGIT($logthis);

  if (opendir SFILEDIR, $bull{arc_folder}) {
    @files = grep {$_ =~ m/\.arc/} readdir SFILEDIR;
    close SFILEDIR;
  }
  else {
    my $trouble = "Could not open directory $ bull{arc_folder}\n";
    printf STDERR $trouble;
    &logit($trouble);
  }

  
#          1         2         3         4         5         6         7         8         9        
#200704271640167114 3227 90 1148 4998  0  4332 72  74318216092 68404293 71   2464 -  056602777  1   0  13  0  9SNE WW    4    0  0   0  0     62322   0   0   0   0
#200609071351127336 1678 89 2971 1174  0 10163 19  11 2064 12116822  57331     43 
#01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789 

  
  foreach (@files) {
        open TEMP, $bull{arc_folder}."/".$_;
        my $line1 = <TEMP>;
    
        my $year = substr($line1,0,4);
        my $month = substr($line1,4,2);
        my $day = substr($line1,6,2);
        my $hour = substr($line1,8,2);
        my $minute = substr($line1,10,2);
        my $second = substr($line1,12,2);
    
        my $epi_lat = substr($line1,16,7);
        my $decimal = substr($epi_lat,3,4) / 6000.;
        if (length($decimal) >= 6) {
            $epi_lat = substr($epi_lat,0,3)+substr($decimal,0,6);
        } else {
            while (length($decimal) < 6){
                $decimal = "$decimal"."0";
            }
            $epi_lat = substr($epi_lat,0,2).substr($decimal,1,5);
        }
        if ( substr($line1,18,1) eq "S") {
            $epi_lat = "-".$epi_lat;
        }
    
        my $epi_long = substr($line1,23,8);
        $decimal = substr($epi_long,3,4) / 6000.;
        if (length($decimal) >= 6) {
            $epi_long = substr($epi_long,0,3)+substr($decimal,0,6);
        } else {
            while (length($decimal) < 6){
                $decimal = "$decimal"."0";
            }
            $epi_long = substr($epi_long,0,3).substr($decimal,1,5);
        }
        unless ( substr($line1,26,1) eq "E") {
            $epi_long = "-".$epi_long;
        }
        
        my $dep = sprintf("%5.2f", substr($line1,31,5)/100.);
        my $res = sprintf("%4.2f", substr($line1,48,4)/100.);
        my $azm = int substr($line1,42,3);
    
        my $eq_time = $year."-".$month."-".$day." ".$hour.":".$minute.":".$second;
    
        my $Mcoda = sprintf("%3.2f", substr($line1,70,3)/100.);
        my $line2 = "$eq_time Hypocenter $epi_lat $epi_long Dep $dep Mc $Mcoda Azm $azm Res $res \n";
    
        push @arc_lines, $line2;
  }
  
    undef my %saw;
    @arc_lines = grep(!$saw{$_}++, @arc_lines); 
  
  open WRITE_ARC, "+>$bull{bull_file_folder}$bull{bull_file_name}";
  foreach (@arc_lines) {
    print WRITE_ARC "$_";
  }
  close WRITE_ARC;
  my $event_num = scalar @arc_lines;
  $logthis = "Wrote $event_num hypocenters to $bull{bull_file_name}\n\n";
  LOGIT($logthis);
  

sub LOGIT {
    my $x;
    my $logname;
    $write_to_the_log = shift;
    my @time = localtime();
    my $year = $time[5]+1900;
    my $month = $time[4]+1;
    my $day = $time[3];
    if (($time[4]+1) < 10){
        $month = "0".($time[4]+1);
    } else {$month = $time[4]+1}
    if (($time[3]) < 10){
        $day = "0".$time[3];
    } else {$day = $time[3]}
    
    my $logtime = scalar (localtime());
    
    $logname = "arc2bull_$year$month$day\.log";
    if (-e "$bull{log_path}"."$logname") {
        $x = '>>'."$bull{log_path}"."$logname";
    } else {$x = '+>'."$bull{log_path}"."$logname";}
        
    open (TEMPLOG, $x);
        print TEMPLOG "$logtime$space$write_to_the_log";
    close TEMPLOG;                                      
    undef $write_to_the_log;
}
