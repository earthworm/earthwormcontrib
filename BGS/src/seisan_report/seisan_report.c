/*
 * seisan_report.c : Writes out a seisan S file
 *                   based on menlo_report.c.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>

/* Functions in this source file
 *******************************/
void  seisan_report_hinvarc ( char * );
void  seisan_report_config  ( char * );
void  seisan_report_lookup  ( void );
void  seisan_report_status  ( unsigned char, short, char * );
char *trim_leading_whitespace (char *string);
char *trim_trailing_whitespace (char *string);

static  SHM_INFO  Region;      /* shared memory region to use for i/o    */

#define   MAXLOGO   2
MSG_LOGO  GetLogo[MAXLOGO];    /* array for requesting module,type,instid */
short     nLogo;

#define BUF_SIZE MAX_BYTES_PER_EQ   /* define maximum size for event msg  */

#define SFILE_LINE_LEN  80       /* define line length for seisan S file */
#define MAX_PHASE_READINGS 100   /* maximum number of phase readings for an event */

/*structures to hold the details from an event,
  and from a phase reading */
struct event_details
{
  int year;
  int month;
  int day;
  int hour;
  int min;
  double secs;
  int lat_deg;
  double lat_min;
  int lat_south;
  double lat;
  int lon_deg;
  double lon_min;
  int lon_east;
  double lon;
  double depth;
  int n_stations;
  double RMS;
};
struct phase_details
{
  char station[6];
  char component[6];
  char phase;
  int hour;
  int min;
  double secs;
};


/* Things to read from configuration file
 ****************************************/
static char    RingName[MAX_RING_STR];        /* name of transport ring for i/o     */
static char    MyModName[MAX_MOD_STR];       /* speak as this module name/id       */
static int     LogSwitch;           /* 0 if no logfile should be written  */
static long    HeartBeatInterval;   /* seconds between heartbeats         */
static char    RemoteHost[30];      /* name of computer to copy files to  */
static char    RemoteUser[15];      /* username to use on remote machine  */
static char    RemotePasswd[15];    /* password of username on RemoteHost */
static char    TmpFile[20];         /* temporary remote file name         */
static char    RemoteDir[50];       /* remote directory to place files in */
static char    LocalDir[50];        /* local directory to write tmp files */
static char    ArcSuffix[6];        /* suffix to use for archive files    */
static char    SumSuffix[6];        /* suffix to use for summary files    */
static int     SendArc   = 1;       /* =0 if ArcSuffix="none" or "NONE"   */
static int     SendSum   = 1;       /* =0 if SumSuffix="none" or "NONE"   */
static int     KeepLocal = 0;       /* =1 if KeepLocalCopy command is in  */
                                    /*    configuration file              */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o      */
static unsigned char InstId;        /* local installation id              */
static unsigned char MyModId;       /* Module Id for this program         */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeHyp2000Arc;
static unsigned char TypeH71Sum2K;

/* Error messages used by seisan_report
 *************************************/
#define  ERR_MISSMSG       0   /* message missed in transport ring       */
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer     */
#define  ERR_NOTRACK       2   /* msg retreived; tracking limit exceeded */
#define  ERR_TOOMANY      3   /* too many phase readings in msg   */
#define  ERR_LOCALFILE     4   /* error creating/writing local temp file */
#define  ERR_RMFILE        5   /* error removing temporary file          */
static char  Text[150];        /* string for log/error messages          */

pid_t MyPid;	/** Hold our process ID to be sent with heartbeats **/


main( int argc, char **argv )
{
   static char  eqmsg[BUF_SIZE];  /* array to hold event message    */
   long         timeNow;          /* current time                   */
   long         timeLastBeat;     /* time last heartbeat was sent   */
   long         recsize;          /* size of retrieved message      */
   MSG_LOGO     reclogo;          /* logo of retrieved message      */
   int          res;


/* Initialize name of log-file & open it
 ***************************************/
   logit_init( argv[1], 0, 256, 1 );

/* Check command line arguments
 ******************************/
   if ( argc != 2 )
   {
        logit( "e" , "Usage: seisan_report <configfile>\n" );
        exit( 0 );
   }

/* Read the configuration file(s)
 ********************************/
   seisan_report_config( argv[1] );

/* Set logit to LogSwitch read from 
 * configfile.
 ***********************************/
   logit_init( argv[1], 0, 256, LogSwitch );
   logit( "" , "seisan_report: Read command file <%s>\n", argv[1] );
   
/* Look up important info from earthworm.h tables
 ************************************************/
   seisan_report_lookup();

/* Change working directory to that for writing tmp files
 ********************************************************/
   if ( chdir_ew( LocalDir ) == -1 )
   {
      fprintf( stderr, "%s: cannot change to directory <%s>\n",
               argv[0], LocalDir );
      fprintf( stderr, "%s: please correct <LocalDir> command in <%s>;",
               argv[0], argv[1] );
      fprintf( stderr, " exiting!\n" );
      exit( -1 );
   }

/* Get our process ID
 **********************/
   if ((MyPid = getpid ()) == -1)
   {
      logit ("e", "seisan_report: Call to getpid failed. Exiting.\n");
      return (EW_FAILURE);
   }

/* Attach to Input/Output shared memory ring
 *******************************************/
   tport_attach( &Region, RingKey );
   logit( "", "seisan_report: Attached to public memory region %s: %d\n",
          RingName, RingKey );

/* Force a heartbeat to be issued in first pass thru main loop
 *************************************************************/
   timeLastBeat = time(&timeNow) - HeartBeatInterval - 1;

/* Flush the incomming transport ring
 *************************************/
   while(tport_getmsg (&Region, GetLogo, nLogo,
		       &reclogo, &recsize, eqmsg, sizeof(eqmsg)-1) != GET_NONE);


/*----------------- setup done; start main loop ----------------------*/

   while( tport_getflag(&Region) != TERMINATE  && 
          tport_getflag(&Region) != MyPid         )
   {
     /* send seisan_report's heartbeat
      *******************************/
        if  ( time(&timeNow) - timeLastBeat  >=  HeartBeatInterval )
        {
            timeLastBeat = timeNow;
            seisan_report_status( TypeHeartBeat, 0, "" );
        }

     /* Process all new hypoinverse archive msgs and hypo71 summary msgs
      ******************************************************************/
        do
        {
        /* Get the next message from shared memory
         *****************************************/
           res = tport_getmsg( &Region, GetLogo, nLogo,
                               &reclogo, &recsize, eqmsg, sizeof(eqmsg)-1 );

        /* Check return code; report errors if necessary
         ***********************************************/
           if( res != GET_OK )
           {
              if( res == GET_NONE )
              {
                 break;
              }
              else if( res == GET_TOOBIG )
              {
                 sprintf( Text,
                         "Retrieved msg[%ld] (i%u m%u t%u) too big for eqmsg[%d]",
                          recsize, reclogo.instid, reclogo.mod, reclogo.type,
                          sizeof(eqmsg)-1 );
                 seisan_report_status( TypeError, ERR_TOOBIG, Text );
                 continue;
              }
              else if( res == GET_MISS )
              {
                 sprintf( Text,
                         "Missed msg(s)  i%u m%u t%u  %s.",
                          reclogo.instid, reclogo.mod, reclogo.type, RingName );
                 seisan_report_status( TypeError, ERR_MISSMSG, Text );
              }
              else if( res == GET_NOTRACK )
              {
                 sprintf( Text,
                         "Msg received (i%u m%u t%u); transport.h NTRACK_GET exceeded",
                          reclogo.instid, reclogo.mod, reclogo.type );
                 seisan_report_status( TypeError, ERR_NOTRACK, Text );
              }
           }

        /* Process new message (res==GET_OK,GET_MISS,GET_NOTRACK)
         ********************************************************/
           eqmsg[recsize] = '\0';   /*null terminate the message*/
           /*printf( "%s", eqmsg );*/   /*debug*/

           if( reclogo.type == TypeHyp2000Arc )
           {
              if( !SendArc ) continue;
              seisan_report_hinvarc( eqmsg );
           }
/* not currently implemented 
           else if( reclogo.type == TypeH71Sum2K )
           {
              if( !SendSum ) continue;
              menlo_report_h71sum( eqmsg ); 
           }
*/

        } while( res != GET_NONE );  /*end of message-processing-loop */

        sleep_ew( 1000 );  /* no more messages; wait for new ones to arrive */
   }
/*-----------------------------end of main loop-------------------------------*/

/* Termination has been requested
 ********************************/
/* detach from shared memory */
   tport_detach( &Region );
/* write a termination msg to log file */
   logit( "t", "seisan_report: Termination requested; exiting!\n" );
   exit( 0 );
}

/******************************************************************************
 *  seisan_report_hinvarc( )  process a Hypoinverse archive message            *
 ******************************************************************************/


void seisan_report_hinvarc( char *arcmsg )
{
   FILE *fp_s;
   char *ptr, temp_str[10], sfilename[30], temp_line[SFILE_LINE_LEN+2];
   int temp_int;
   struct event_details ev_details;
   struct phase_details p_details;
   char line1[SFILE_LINE_LEN+1];
   char lineI[SFILE_LINE_LEN+1];
   char line7[SFILE_LINE_LEN+1] = " STAT SP IPHASW D HRMM SECON CODA AMPLIT PERI AZIMU VELO SNR AR TRES W  DIS CAZ7";
   char *line4;
   time_t bin_time;
   struct tm *utc_time;


   /* set defaults for lat and lon */
   ev_details.lat_south = ev_details.lon_east = 1;

   /* split off the first line of the message and interpret it */
   ptr = strtok (arcmsg, "\n");
   strncpy (temp_str, ptr, 4);          temp_str[4] = '\0';
   ev_details.year = atoi(temp_str);
   strncpy (temp_str, ptr+4, 2);        temp_str[2] = '\0';
   ev_details.month = atoi(temp_str);
   strncpy (temp_str, ptr+6, 2);        temp_str[2] = '\0';
   ev_details.day = atoi(temp_str);
   strncpy (temp_str, ptr+8, 2);        temp_str[2] = '\0';
   ev_details.hour = atoi(temp_str);
   strncpy (temp_str, ptr+10, 2);       temp_str[2] = '\0';
   ev_details.min = atoi(temp_str);
   strncpy (temp_str, ptr+12, 4);       temp_str[4] = '\0';
   temp_int = atoi(temp_str);
   ev_details.secs = (double)(temp_int) / 100;
   strncpy (temp_str, ptr+16, 2);       temp_str[2] = '\0';
   ev_details.lat_deg = atoi(temp_str);
   if (*(ptr+18) == 'S') ev_details.lat_south = -1;
   strncpy (temp_str, ptr+19, 4);       temp_str[4] = '\0';
   temp_int = atoi(temp_str);
   ev_details.lat_min = (double)(temp_int) / 100;
   ev_details.lat = (((double)ev_details.lat_deg + (ev_details.lat_min / 60)) * ev_details.lat_south);
   strncpy (temp_str, ptr+24, 2);       temp_str[2] = '\0';
   ev_details.lon_deg = atoi(temp_str);
   if (*(ptr+26) == 'W') ev_details.lon_east = -1;
   strncpy (temp_str, ptr+27, 4);       temp_str[4] = '\0';
   temp_int = atoi(temp_str);
   ev_details.lon_min = (double)(temp_int) / 100;
   ev_details.lon = (((double)ev_details.lon_deg + (ev_details.lon_min / 60)) * ev_details.lon_east);
   strncpy (temp_str, ptr+32, 4);       temp_str[4] = '\0';
   temp_int = atoi(temp_str);
   ev_details.depth = (double)(temp_int) / 100;
   strncpy (temp_str, ptr+48, 4);       temp_str[4] = '\0';
   temp_int = atoi(temp_str);
   ev_details.RMS = (double)(temp_int) / 100; 

   /* now cycle through the other lines looking for the phase readings */
   ev_details.n_stations = 0;
   line4 = (char *)malloc ((SFILE_LINE_LEN+2) * MAX_PHASE_READINGS);
   sprintf (line4, "");
   for (ptr = strtok ((char *) 0, "\n"); ptr; ptr = strtok ((char *) 0, "\n"))
   {
      if (*ptr == '$') continue;
      else if (!isupper(*ptr)) continue;
      else 
      {
	ev_details.n_stations++;
	if (ev_details.n_stations > MAX_PHASE_READINGS)
	{
             sprintf( Text,
                     "Too many phase readings in message %s", arcmsg);
             seisan_report_status( TypeError, ERR_TOOMANY, Text );
	     return;
	}
	strncpy (temp_str, ptr, 5);       temp_str[5] = '\0';
	strcpy (p_details.station, trim_leading_whitespace(trim_trailing_whitespace(temp_str)));
        strncpy (temp_str, ptr+9, 3);     temp_str[3] = '\0';
	strcpy (p_details.component, trim_leading_whitespace(trim_trailing_whitespace(temp_str)));
	p_details.phase = *(ptr+13);
        strncpy (temp_str, ptr+25, 2);    temp_str[2] = '\0';
        p_details.hour = atoi(temp_str);
        strncpy (temp_str, ptr+27, 2);    temp_str[2] = '\0';
        p_details.min = atoi(temp_str);
        strncpy (temp_str, ptr+30, 4);    temp_str[4] = '\0';
        temp_int = atoi(temp_str);
        p_details.secs = (double)(temp_int) / 100;
        sprintf (temp_line, " %-5s%c%c  %c       %2d%2d %5.2f                                                    \n", 
	           p_details.station, p_details.component[0], p_details.component[2],
	           p_details.phase, p_details.hour, p_details.min, p_details.secs);
	strcat (line4, temp_line);
     }
   }

   /* now we can start to create the S file - filename first */
   sprintf (sfilename, "%02d-%02d%02d-%02dR.S%4d%02d", ev_details.day, ev_details.hour, ev_details.min,
                                                      (int)ev_details.secs, ev_details.year, ev_details.month);
   if( (fp_s = fopen (sfilename, "w")) == (FILE *) NULL )
   {
       sprintf( Text, "seisan_report: error opening file <%s>\n",
                sfilename );
       seisan_report_status( TypeError, ERR_LOCALFILE, Text );
       return;
   }
   
   /* create the line type 1 */
   sprintf (line1, " %4d %2d%2d %2d%2d %4.1f R %7.3f%8.3f%5.1f     %3d%4.1f                        1", 
              ev_details.year, ev_details.month, ev_details.day, 
              ev_details.hour, ev_details.min, ev_details.secs, 
	      ev_details.lat, ev_details.lon, ev_details.depth, ev_details.n_stations, ev_details.RMS);
   fprintf (fp_s, "%s\n", line1);
   
   /* create the ID line */
   time (&bin_time);
   utc_time = gmtime (&bin_time);
   sprintf (lineI, " ACTION:NEW %02d-%02d-%02d %02d:%02d OP:ew   STATUS:               ID:%4d%02d%02d%02d%02d%02d     I",
             utc_time->tm_year % 100, utc_time->tm_mon + 1, utc_time->tm_mday, utc_time->tm_hour, utc_time->tm_min,
              ev_details.year, ev_details.month, ev_details.day, 
              ev_details.hour, ev_details.min, (int)ev_details.secs);
   fprintf (fp_s, "%s\n", lineI);
   
   /* spit out a header and add the accumulated phase readings lines */
   fprintf (fp_s, "%s\n", line7);
   fprintf (fp_s, "%s\n", line4);
   
   /* tidy up */
   free (line4);
   fclose (fp_s);
   return;
}


/******************************************************************************
 *  seisan_report_config() processes command file(s) using kom.c functions;    *
 *                       exits if any errors are encountered.                 *
 ******************************************************************************/
void seisan_report_config( char *configfile )
{
   int      ncommand;     /* # of required commands you expect to process   */
   char     init[15];     /* init flags, one byte for each required command */
   int      nmiss;        /* number of required commands that were missed   */
   char    *com;
   char    *str;
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command
 *****************************************************/
   ncommand = 6;
   for( i=0; i<ncommand; i++ )  init[i] = 0;
   nLogo = 0;

/* Open the main configuration file
 **********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 ) {
        logit( "e" ,
                "seisan_report: Error opening command file <%s>; exiting!\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  logit( "e" ,
                          "seisan_report: Error opening command file <%s>; exiting!\n",
                           &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_long();
                init[3] = 1;
            }


         /* Enter installation & module to get event messages from
          ********************************************************/
  /*4*/     else if( k_its("GetEventsFrom") ) {
                if ( nLogo+1 >= MAXLOGO ) {
                    logit( "e" ,
                            "seisan_report: Too many <GetEventsFrom> commands in <%s>",
                             configfile );
                    logit( "e" , "; max=%d; exiting!\n", (int) MAXLOGO/2 );
                    exit( -1 );
                }
                if( ( str=k_str() ) ) {
                   if( GetInst( str, &GetLogo[nLogo].instid ) != 0 ) {
                       logit( "e" ,
                               "seisan_report: Invalid installation name <%s>", str );
                       logit( "e" , " in <GetEventsFrom> cmd; exiting!\n" );
                       exit( -1 );
                   }
                   GetLogo[nLogo+1].instid = GetLogo[nLogo].instid;
                }
                if( ( str=k_str() ) ) {
                   if( GetModId( str, &GetLogo[nLogo].mod ) != 0 ) {
                       logit( "e" ,
                               "seisan_report: Invalid module name <%s>", str );
                       logit( "e" , " in <GetEventsFrom> cmd; exiting!\n" );
                       exit( -1 );
                   }
                   GetLogo[nLogo+1].mod = GetLogo[nLogo].mod;
                }
                if( GetType( "TYPE_HYP2000ARC", &GetLogo[nLogo].type ) != 0 ) {
                    logit( "e" ,
                               "seisan_report: Invalid message type <TYPE_HYP2000ARC>" );
                    logit( "e" , "; exiting!\n" );
                    exit( -1 );
                }
                if( GetType( "TYPE_H71SUM2K", &GetLogo[nLogo+1].type ) != 0 ) {
                    logit( "e" ,
                               "seisan_report: Invalid message type <TYPE_H71SUM2K>" );
                    logit( "e" , "; exiting!\n" );
                    exit( -1 );
                }
                nLogo  += 2;
                init[4] = 1;
            /*    printf("GetLogo[%d] inst:%d module:%d type:%d\n",
                        nLogo, (int) GetLogo[nLogo].instid,
                               (int) GetLogo[nLogo].mod,
                               (int) GetLogo[nLogo].type ); */  /*DEBUG*/
            /*    printf("GetLogo[%d] inst:%d module:%d type:%d\n",
                        nLogo+1, (int) GetLogo[nLogo+1].instid,
                               (int) GetLogo[nLogo+1].mod,
                               (int) GetLogo[nLogo+1].type ); */  /*DEBUG*/
            }

         /* Enter name of local directory to write files to
          *****************************************************/
  /*5*/     else if( k_its("LocalDir") ) {
                str = k_str();
                if(str) strcpy( LocalDir, str );
                init[5] = 1;
            }

         /* Suffix to use for output archive files
          ****************************************/
  /*6*/     else if( k_its("ArcSuffix") ) {
                str = k_str();
                if(str) {
                   if( strlen(str) >= sizeof(ArcSuffix) ) {
                      logit( "e" ,
                              "seisan_report: ArcSuffix <%s> too long; ", str );
                      logit( "e" , "max length:%d; exiting!\n",
                              (int)(sizeof(ArcSuffix)-1) );
                      exit(-1);
                   }
                /* ArcSuffix shouldn't include the period */
                   if( str[0] == '.' ) strcpy( ArcSuffix, &str[1] );
                   else                strcpy( ArcSuffix, str     );
                /* Set SendArc flag based on ArcSuffix value */
                   if     (strncmp(ArcSuffix, "none", 4) == 0) SendArc=0;
                   else if(strncmp(ArcSuffix, "NONE", 4) == 0) SendArc=0;
                   else if(strncmp(ArcSuffix, "None", 4) == 0) SendArc=0;
                   else                                        SendArc=1;
                }
                init[6] = 1;
            }

        /* Unknown command
         *****************/
            else {
                logit( "e" , "seisan_report: <%s> Unknown command in <%s>.\n",
                         com, configfile );
                continue;
            }

        /* See if there were any errors processing the command
         *****************************************************/
            if( k_err() ) {
               logit( "e" ,
                       "seisan_report: Bad <%s> command in <%s>; exiting!\n",
                        com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       logit( "e" , "seisan_report: ERROR, no " );
       if ( !init[0] )  logit( "e" , "<LogFile> "           );
       if ( !init[1] )  logit( "e" , "<MyModuleId> "        );
       if ( !init[2] )  logit( "e" , "<RingName> "          );
       if ( !init[3] )  logit( "e" , "<HeartBeatInterval> " );
       if ( !init[4] )  logit( "e" , "<GetEventsFrom> "     );
       if ( !init[5] )  logit( "e" , "<LocalDir> "          );
       if ( !init[6] )  logit( "e" , "<ArcSuffix> "         );
       logit( "e" , "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }

   return;
}

/*******************************************************************************
 *  seisan_report_lookup( )   Look up important info from earthworm.h tables    *
 *******************************************************************************/
void seisan_report_lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 ) {
        fprintf( stderr,
                "seisan_report:  Invalid ring name <%s>; exiting!\n", RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr,
              "seisan_report: error getting local installation id; exiting!\n" );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr,
              "seisan_report: Invalid module name <%s>; exiting!\n", MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr,
              "seisan_report: Invalid message type <TYPE_HEARTBEAT>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr,
              "seisan_report: Invalid message type <TYPE_ERROR>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_HYP2000ARC", &TypeHyp2000Arc ) != 0 ) {
      fprintf( stderr,
              "seisan_report: Invalid message type <TYPE_HYP2000ARC>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_H71SUM2K", &TypeH71Sum2K ) != 0 ) {
      fprintf( stderr,
              "seisan_report: Invalid message type <TYPE_H71SUM2K>; exiting!\n" );
      exit( -1 );
   }
   return;
}

/*****************************************************************************
 * seisan_report_status() builds a heartbeat or error message & puts it into  *
 *                       shared memory.  Writes errors to log file & screen. *
 *****************************************************************************/
void seisan_report_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size;
   long        t;

/* Build the message
 *******************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
        sprintf( msg, "%ld %ld\n\0", t, MyPid);
   }
   else if( type == TypeError )
   {
        sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
        logit( "et", "seisan_report: %s\n", note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","seisan_report:  Error sending heartbeat.\n" );
        }
        else if( type == TypeError ) {
           logit("et","seisan_report:  Error sending error:%d.\n", ierr );
        }
   }

   return;
}


/*****************************************************************************
 * trim_leading_whitespace and trim_trailing_whitespace                      *
 *                       utility functions for tidying up strings.           *
 *****************************************************************************/
char *trim_leading_whitespace (char *string)
{
  while (*string && strchr (" \t", *string)) string ++;
  return string;
}

char *trim_trailing_whitespace (char *string)
{
  char *ptr;
  
  for (ptr = string + strlen (string) -1; ptr >= string; ptr --)
  {
    if (strchr (" \t", *ptr)) *ptr = '\0';
    else break;
  }
  return string;
}

