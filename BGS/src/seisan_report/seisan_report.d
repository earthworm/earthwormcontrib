
#
# This is seisan_report's parameter file

#  Basic Earthworm setup:
#
MyModuleId         MOD_REPORT     # module id for this instance of report 
RingName           PB_HYPO_RING      # ring to get input from
LogFile            1              # 0 to completely turn off disk log file
                                  # 2 to log to module log but not stderr/stdout
HeartBeatInterval  30             # seconds between heartbeats

# List the message logos to grab from transport ring
#              Installation       Module          Message Types
GetEventsFrom  INST_WILDCARD    MOD_EQPROC        # hyp2000arc

# Set suffixes for output filenames
# NOTE: If a suffix is "none", no file will   
#       be reported for that type of message
ArcSuffix     ".arc"        # suffix for archive (hinvarc message) files

# Local directory to write seisan files in
LocalDir      /users/earthwor/work  

