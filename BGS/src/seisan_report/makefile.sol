
#
#   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
#   CHECKED IT OUT USING THE COMMAND CHECKOUT.
#
#    $Id: makefile.sol 303 2007-03-14 17:05:43Z stefan $
#
#    Revision history:
#     $Log$
#     Revision 1.1  2007/03/14 17:04:50  stefan
#     contrib from david j scott of bgs
#
#     Revision 1.1.1.1  2005/07/14 19:57:51  paulf
#     Local ISTI CVS copy of EW v6.3
#
#     Revision 1.4  2002/11/03 05:08:33  lombard
#     Added missing CFLAGS definition,
#     Cleaned up compile target,
#
#     Revision 1.3  2001/05/04 16:43:17  bogaert
#     removed references to 'arcfile2ring', which is replaced by file2ring.
#
#     Revision 1.2  2000/08/08 17:19:17  lucky
#     Added lint directive
#
#     Revision 1.1  2000/02/14 18:56:41  lucky
#     Initial revision
#
#
#

CFLAGS = $(GLOBALFLAGS)

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib


SREPORT = seisan_report.o $L/logit.o $L/time_ew.o $L/kom.o  \
          $L/getutil.o $L/sleep_ew.o $L/transport.o $L/copyfile.o $L/dirops_ew.o
ARCF = arcfile2ring.o $L/kom.o $L/getutil.o $L/sleep_ew.o $L/transport.o

all: seisan_report arcfile2ring

seisan_report: $(SREPORT)
	cc -o $B/seisan_report $(SREPORT)  -lm -lposix4
arcfile2ring: $(ARCF)
	cc -o $B/arcfile2ring $(ARCF) -lposix4

.c.o:
	cc -c $(CFLAGS)  $<


lint:
	lint seisan_report.c $(GLOBALFLAGS)

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f  $B/seisan_report*
