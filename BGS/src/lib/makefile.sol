#
#   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
#   CHECKED IT OUT USING THE COMMAND CHECKOUT.
#
#    $Id: makefile.sol 284 2007-01-31 01:55:39Z stefan $
#
#    Revision history:
#     $Log$
#     Revision 1.1  2007/01/31 01:55:19  stefan
#     British Geological Survey contrib, thanks Simon Flower
#
#     Revision 1.3  2000/08/08 17:19:17  lucky
#     Added lint directive
#
#     Revision 1.2  2000/03/30 15:49:58  davidk
#     commented out -xCC flag that allows C++ comments.  Couldn't find any
#     C++ comments and this flag makes gcc choke.
#
#     Revision 1.1  2000/02/14 20:02:23  lucky
#     Initial revision
#

# list of objects for code that came from GEOLIB library
GEOLIB_OBJS = alloc.o chk_clk.o cmp_clk.o copy_file.o date_dy.o \
		dy_date.o dy_in_mn.o is_leap.o network.o \
		string.o subst_var.o chk_num.o diff_clk.o \
		add_clk.o

# list of objects for code that came from SDAS library
SDAS_OBJS = read_sdas_data.o data_utils.o

# list of objects created especially for Earthworm
EW_OBJS = utils.o utils_sol.o 

CFLAGS = -D_REENTRANT ${GLOBALFLAGS} -D_SPARC -D_SOLARIS

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

#CFLAGS += -xCC
CFLAGS += -I../zlib

OBJ = archman.o \
	utils.o \
	utils_sol.o \
	$(L)/threads_ew.o \
	$(L)/getutil.o $(L)/kom.o $(L)/logit_mt.o $(L)/socket_ew.o \
	$(L)/socket_ew_common.o $(L)/transport.o $(L)/sleep_ew.o \
	$(L)/time_ew.o $(L)/sema_ew.o $(L)/swap.o

# main dependencies
OBJS = $(GEOLIB_OBJS) $(SDAS_OBJS) $(EW_OBJS)

libbgs.a: $(OBJS)
	ar rv $@ $?

# Clean-up rules
clean:
	rm -f a.out core *.o
#	rm -f a.out core *.o *% *~

clean_bin:
	rm -f $B/archman*
