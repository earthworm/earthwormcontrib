 /**********************************************************************
  *                                                                    *
  *                         DiskMgr Program                            *
  *                                                                    *
  *  Report to the status manager if the disk is nearly full.          *
  *                                                                    *
  *  Note:  Changed 4/7/95 to run on Solaris 2.4 (from SunOS 4.1).     *
  *         The only changes required were to name of the system call  *
  *         for getting file-system statistics, to structure-type of   *
  *         its second argument, and to its required include file      *
  *                                                                    *
  *  Modified 7/96 to handle multiple disk partitions                  *
  *    Kent Lindquist, Geophysical Institute, University of Alaska     *
  **********************************************************************/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>

#define ERR_DISKFULL  0

#define NUM_PARTITIONS_MAX 5

/* Function prototypes
 *********************/
void diskmgr_config( char * );
int  diskmgr_status( unsigned char, short, char * );

/* Things to read (or derive) from configuration file
   **************************************************/
static long          RingKey;     /* key of transport ring to write to */
static unsigned char MyModId;     /* diskmgr's module id               */
static int           LogSwitch;   /* If 0, no logging should be done to disk */
static char	     DiskName[NUM_PARTITIONS_MAX][FILENAME_MAX];
				  /* Disk partitions to monitor */
unsigned long        Min_kbytes[NUM_PARTITIONS_MAX];  
				  /* complain when free disk space is  */
                                  /* below this number of kbytes       */
static int	     ndisks = 0;  /* Number of disk partitions to watch */

/* Things to look up in the earthworm.h tables with getutil.c functions
   ********************************************************************/
static unsigned char InstId;        /* local installation id  */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;

SHM_INFO Region1;

main( int argc, char **argv )
{
   static int heartBeatInterval = 5;
   unsigned   DiskAvail;
   char	      amount[100];
   char       note[70];
   int	      index;
   int        i;

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
        fprintf( stderr, "Usage: diskmgr <configfile>\n" );
        exit( 0 );
   }

/* Read configuration file
   ***********************/
   diskmgr_config( argv[1] );

/* Look up local installation id
   *****************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr,
              "diskmgr: error getting local installation id; exitting!\n" );
      exit( -1 );
   }

/* Look up message types from earthworm.h tables
   *********************************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr,
              "diskmgr: Invalid message type <TYPE_HEARTBEAT>; exitting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr,
              "diskmgr: Invalid message type <TYPE_ERROR>; exitting!\n" );
      exit( -1 );
   }

/* Initialize name of log-file & open it
   *************************************/
   logit_init( "diskmgr", (short) MyModId, 256, LogSwitch );
   logit( "" , "diskmgr: Read command file <%s>\n", argv[1] );

/* Get available disk space at program startup
   *******************************************/
   for( index = 0; index < ndisks; index++ )
   {
   if ( GetDiskAvail( DiskName[index], &DiskAvail ) == -1 )
   	{
      	    logit( "et",
            "diskmgr: Error getting file system statistics for %s; exiting!\n",
	    DiskName[index] );
      	    exit( 1 );
   	}

	if( (double) DiskAvail / 1000000. > 1. ) 
	{
	    sprintf( amount, "%6.2lf Gigabytes", (double) DiskAvail / 1000000. );
	}
	else if( (double) DiskAvail / 1000. > 1. )
	{
	    sprintf( amount, "%6.2lf Megabytes", (double) DiskAvail / 1000. );
	} 
	else
	{
	    sprintf( amount, "%6.2lf Kilobytes", (double) DiskAvail );
	}

   	logit( "et", 
	  "diskmgr: %s of space on disk %s available at startup.\n",
            amount, DiskName[index] );
   }

/* Attach to shared memory ring
   ****************************/
   tport_attach( &Region1, RingKey );

/* Loop until kill flag is set
   ***************************/
   while ( 1 )
   {

/* See if the disk is nearly full
   ******************************/
      for( index = 0; index < ndisks; index ++ )
      {
         if ( GetDiskAvail( DiskName[index], &DiskAvail ) == -1 )
            logit( "et",
	      "diskmgr:  Error getting file system statistics for %s.\n",
	      DiskName[index] );
   
         if ( DiskAvail < Min_kbytes[index] )
         {
	    if( (double) DiskAvail / 1000000. > 1. ) 
	    {
	        sprintf( amount, "%6.2lf Gigabytes", (double) DiskAvail / 1000000. );
	    }
	    else if( (double) DiskAvail / 1000. > 1. )
	    {
	        sprintf( amount, "%6.2lf Megabytes", (double) DiskAvail / 1000. );
	    } 
	    else
	    {
	        sprintf( amount, "%6.2lf Kilobytes", (double) DiskAvail );
	    }
            sprintf( note, "Disk %s nearly full; %s available.",
                     DiskName[index], amount );

            if ( diskmgr_status( TypeError, ERR_DISKFULL, note ) != PUT_OK )
                logit( "t", "diskmgr:  Error sending error message to ring.\n");
         }
      }

   /* Send heartbeat every heartBeatInterval seconds
      **********************************************/
      if ( diskmgr_status( TypeHeartBeat, 0, "" ) != PUT_OK )
      {
           logit( "t", "diskmgr:  Error sending heartbeat to ring.\n");
      }

   /* Check kill flag
      ***************/
      for ( i = 0; i < heartBeatInterval; i++ )
      {
         sleep_ew( 1000 );
         if ( tport_getflag( &Region1 ) == TERMINATE )
         {
                tport_detach( &Region1 );
                logit( "t", "diskmgr: Termination requested; exitting!\n" );
                exit( 0 );
         }
      }
   }
}


         /*************************************************
          *                 diskmgr_status                *
          *  Builds heartbeat or error msg and puts it in *
          *  shared memory.                               *
          *************************************************/
int diskmgr_status( unsigned char type,
                    short         ierr,
                    char         *note )
{
        MSG_LOGO    logo;
        char        msg[256];
        int         res;
        long        size;
        time_t      t;

        logo.instid = InstId;
        logo.mod    = MyModId;
        logo.type   = type;

        time( &t );

        if( type == TypeHeartBeat ) {
                sprintf ( msg, "%ld\n", t);
        }
        else if( type == TypeError ) {
                sprintf ( msg, "%ld %d %s\n", t, ierr, note);
        }

        size = strlen( msg );  /* don't include null byte in message */
        res  = tport_putmsg( &Region1, &logo, size, msg );

        return( res );
}


/***********************************************************************
 * diskmgr_config()  processes command file using kom.c functions      *
 *                      exits if any errors are encountered            *
 ***********************************************************************/
void diskmgr_config(char *configfile)
{
   int      ncommand;     /* # of required commands you expect to process   */
   char     init[10];     /* init flags, one byte for each required command */
   int      nmiss;        /* number of required commands that were missed   */
   char    *com;
   char    *str;
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command
 *****************************************************/
   ncommand = 4;
   for( i=0; i<ncommand; i++ )  init[i] = 0;

/* Open the main configuration file
 **********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 ) {
        fprintf( stderr,
                "diskmgr: Error opening command file <%s>; exitting!\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr,
                          "diskmgr: Error opening command file <%s>; exitting!\n",
                           &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command
         ************************************/
         /* Read module id for this program
          *********************************/
   /*0*/    if( k_its( "MyModuleId" ) ) {
                str = k_str();
                if (str) {
                   if ( GetModId( str, &MyModId ) < 0 ) {
                      fprintf( stderr,
                              "diskmgr: Invalid MyModuleId <%s> in <%s>",
                               str, configfile );
                      fprintf( stderr, "; exitting!\n" );
                      exit( -1 );
                   }
                }
                init[0] = 1;
            }

         /* Name of transport ring to write to
          ************************************/
   /*1*/    else if( k_its( "RingName" ) ) {
                str = k_str();
                if (str) {
                   if ( ( RingKey = GetKey(str) ) == -1 ) {
                      fprintf( stderr,
                              "diskmgr: Invalid RingName <%s> in <%s>",
                               str, configfile );
                      fprintf( stderr, "; exitting!\n" );
                      exit( -1 );
                   }
                }
                init[1] = 1;
            }
         /* Complain when free disk space drops lower than
          ************************************************/
   /*2*/    else if( k_its( "Disk_Min_kbytes" ) ) {
		str = k_str();
		if (str) {
			strcpy( DiskName[ndisks], str );
                	Min_kbytes[ndisks] = (unsigned long) k_long();
			ndisks++;
                	init[2] = 1;
		} 
            }
         /* Set Logfile switch
          ********************/
   /*3*/    else if( k_its( "LogFile" ) ) {
                LogSwitch = k_int();
                init[3] = 1;
            }
            else {
                fprintf(stderr, "diskmgr: <%s> unknown command in <%s>.\n",
                        com, configfile );
                continue;
            }

        /* See if there were any errors processing the command
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "diskmgr: Bad <%s> command in <%s>; \n",
                        com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "diskmgr: ERROR, no " );
       if ( !init[0] )  fprintf( stderr, "<MyModuleId> " );
       if ( !init[1] )  fprintf( stderr, "<RingName> "   );
       if ( !init[2] )  fprintf( stderr, "<Disk_Min_kbytes> " );
       if ( !init[3] )  fprintf( stderr, "<LogFile> "    );
       fprintf( stderr, "command(s) in <%s>; exitting!\n", configfile );
       exit( -1 );
   }

   return;
}
