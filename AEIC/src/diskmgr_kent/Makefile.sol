B = ../../bin
L = ../../lib

diskmgr: diskmgr.o $L/getavail.o $L/getutil.o $L/kom.o $L/logit.o \
         $L/sleep_ew.o $L/time_ew.o $L/transport.o 
	cc -o $B/diskmgr diskmgr.o $L/getavail.o $L/getutil.o $L/kom.o \
	      $L/logit.o $L/sleep_ew.o $L/time_ew.o $L/transport.o -lm -lposix4
