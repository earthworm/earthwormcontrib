#
#                  DiskMgr's Configuration File
#
MyModuleId    MOD_DISKMGR   # module id for this program,
RingName        HYPO_RING   # transport ring to write to,
LogFile                 0   # If 0, don't write logfile at all,
Disk_Min_kbytes .    5000   # Yell when free disk space is lower than
                            # this many kbytes
Disk_Min_kbytes /wf/seg 5000
Disk_Min_kbytes /wf/archive 5000
