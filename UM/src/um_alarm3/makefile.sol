CFLAGS = -D_REENTRANT $(GLOBALFLAGS)

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

EQALARM_EW = um_alarm3.o $L/logit_mt.o $L/getutil.o  $L/transport.o $L/kom.o \
		$L/sema_ew.o $L/sleep_ew.o $L/time_ew.o $L/read_arc.o $L/chron3.o

um_alarm3: $(EQALARM_EW)
	cc -o $(B)/um_alarm3 $(EQALARM_EW) -lm -mt -lposix4

