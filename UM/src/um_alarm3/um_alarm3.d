# 
# eqalarmMgr's configuration file
#
MyModuleId	MOD_UM_ALARM3	# module id for this instance of eqalarmMgr
RingName	HYPO_RING	# trasport ring for I/0
LogSwitch	1		# 0 to turn off logging to disk

# List the message logos to grab from transport ring
#		Installation	Module		Message Type
GetSumFrom	INST_MEMPHIS	MOD_EQPROC	TYPE_HYP2000ARC

# Read the magnitude threshold
#
MagThreshold	0.0

# uses echo message | /usr/ucb/mail -s "NMSZ earthquake" `cat EmailFile`
EmailFile  /export/home/seisadm/email.subscribers

# uses /bin/echo to write a message to a file named event1 in the specified
# directory on the specified host.   For example, this set of params yields
# /bin/ssh -l ew folkworm 'bin/echo <cube message> > EIDS/polldir/event1
#
# qdds (or cube format EIDS) commands
# use QDDSCommand (e.g. /usr/bin/ssh -l)
QDDSCommand "/bin/ssh -l"
# as this user
QDDSuser ew
# on this machine
QDDSmachine folkworm
# in this directory
QDDSpoll "EIDS/polldir"

