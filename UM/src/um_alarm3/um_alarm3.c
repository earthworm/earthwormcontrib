/* um_alarm2                         Mitch Withers 2/99

   20091095 replace hardwired rsh file transfer for qdds with a configurable
            command.  This lets us use ssh (or cat, or echo or whatever we
            want).  (withers)

   20091095 remove qpage because everything now uses qdds/eids (withers)

   separated qdds and cube messages types (qdds still uses cube, previous cube is now asap).
   Necessary to fill spaces with zeros to prevent paging software from modifying message.
   QDDS doesn't like this hence the need to now have to distinct messages.

   added ability to send cube messages to qdds system on folkworm 4/00

   added ability to send cube messages via page on gollum 4/00

   added ability to send human readable messages via email 4/00

   hacked eqalarm to read hyp2000arc messages and parse them into an
     alpha-numeric page message using the read_arc utilities.  Then
     make a system call to tpage.

 *       *** version 2.2 ***
 * eqalarm_ew.c: This is the simple, crude notification module created for v2.1
                 It is intended to provide the most minimal pager notification,
                 based on magnitude threshold only.

                 The idea is that the Bruce Julian "eqalarm" family will be
                 available 'outside' v2.1 proper to do the 'real' notification
                 functions for sophisticated installations.

                 This module serves to (1) provide v2.1 with an internal
                 minimalist notification capability for simple istallations
                 where there may not be other computers to run the 'real'
                 notification stuff, and (2) to serve as a pattern making
                 it easier for 'simple' installations to roll their own
                 notification schemes.

                 Alex Bittenbinder Jan 2 1996

  Wed Nov 11 17:11:03 MST 1998 lucky

	1) name of the config file passed to logit_init()
	2) incomming transport ring flushed at startup
	3) Process ID sent on with the heartbeat msgs for restart
	   purposes.

              ====== Y2K compliance ======

    Changed message type names to their y2k equivalents.
    Changed makeMessage() to accomodate the new TYPE_H71SUM2K 
    Changed isItBigDeal() to accomodate the new TYPE_H71SUM2K 

    Uses procMessage() instead of makeMessage and isItBigDeal

*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <read_arc.h>
#include <chron3.h>

static  SHM_INFO  InRegion;          /* public shared memory for receiving summary messages*/

/* Things to lookup in the earthworm.h table with getutil.c functions
 **********************************************************************/
static long          PublicKey;         /* key to pbulic memory region for i/o     */
static unsigned char InstId;            /* local installation id      */
static unsigned char MyModId;           /* our module id             */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeSumm;

/* Things to read from configuration file
 ****************************************/
static char MyModuleId[20];       /* module id for this module                */
static char RingName[20];         /* name of transport ring for i/o           */
static int  LogSwitch;            /* 0 if no logging should be done to disk   */

#define  MAXLOGO   1
MSG_LOGO  GetLogo[MAXLOGO];       /* array for requesting module,type,instid  */
short     nLogo;                  /* number of logos read from ring           */

double      MagThreshold;         /* Mag threshold above which we send a page */
char EmailFile[100];
char QDDSCommand[100];
char QDDSuser[100];
char QDDSmachine[100];
char QDDSpoll[100];

/* Variables for talking to statmgr
 **********************************/
time_t    timeNow;
time_t    timeLastBeat;             /* time last heartbeat was sent */
time_t    heartBeatInterval = 5;    /* seconds between heartbeats   */
char      Text[150];


pid_t MyPid;	/** Hold our process ID to be sent with heartbeats **/


/* Error words used by eqalarm_ew
 ********************************/
#define   ERR_MISSMSG           0
#define   ERR_TOOBIG            1
#define   ERR_NOTRACK           2
#define   ERR_INTERNAL          3


/* Functions in this source file
 ******************************/
void eqalarm_status( unsigned char, short, char *); /* sends heartbeats and errors into ring*/
void eqalarm_config( char * );          /* reads configuration (.d) file via Carl's routines */
void eqalarm_lookup ( void );           /* Goes from symbolic names to numeric values, via earthworm.h */

   /* formats a pretty message. args: hyp2000arc, MagThreshold.  */
int procMessage(char* , double , char*, char*, char*, char*, char* ); 
char  *make_datestr( double, char * );
void date17(double, char*);

/* **************************************************************************/

int main( int argc, char **argv )
{
  char        message[MAX_BYTES_PER_EQ];                 /* actual retrieved message   */
  long        msgSize;                     /* size of retrieved message  */
  MSG_LOGO    msgLogo;                     /* logo of retrieved message  */
  char        hyp2000arc[MAX_BYTES_PER_EQ];                 /* hypo71 summary card */
  int         res;
  int         err;
  char*       msgtext;


  /* Check command line arguments
   ******************************/
  if ( argc != 2 ) {
    fprintf( stderr, "Usage: um_alarm3 <configfile>\n" );
    return -1;
  }


  /* Read the configuration file(s)
  ********************************/
  eqalarm_config( argv[1] );


  /* Look up important info from earthworm.h tables
  ************************************************/
  eqalarm_lookup();

  /* DEBUG: dump variables to stdout
  **********************************/
	/*
        printf("um_alarm3: MyModuleId: %s \n",MyModuleId);
        printf("um_alarm3: RingName: %s \n",RingName);
        printf("um_alarm3: MagThreshold: %f \n",MagThreshold);
        printf("um_alarm3: GetLogo: %u %u %u\n",GetLogo[0].instid, GetLogo[0].mod, GetLogo[0].type);
        printf("um_alarm3: EQALARM nLogo: %d \n",nLogo);
	*/

  /* Initialize name of log-file & open it
  ***************************************/
  logit_init( argv[1], (short) MyModId, 256, LogSwitch );
  logit( "" , "um_alarm3: Read command file <%s>\n", argv[1] );

  /* Attach to public HYPO shared memory ring
  ******************************************/
  tport_attach( &InRegion, PublicKey );
  logit( "", "um_alarm3: Attached to public memory region <%s>: %ld.\n", RingName, InRegion.key );

  /* Get our process ID
  **********************/
  if ((MyPid = getpid ()) == -1) {
    logit ("e", "um_alarm3: Call to getpid failed. Exiting.\n");
    return (EW_FAILURE);
  }

  /* Send first heartbeat
   **********************/
  time(&timeLastBeat);
  eqalarm_status( TypeHeartBeat, 0, "" );

  /* Flush the incomming transport ring on startup
  **************************************************/
  while (tport_getmsg (&InRegion, GetLogo, nLogo,  &msgLogo,
                                &msgSize, message, sizeof(message)- 1) != GET_NONE);

  logit( "t", "um_alarm3: Started input buffer thread.\n" );


  /* ------------------------ start working loop -------------------------*/
  while(1) {
    do {

      /* see if a termination has been requested 
       *****************************************/
      if ( tport_getflag( &InRegion ) == TERMINATE ) {
         /* detach from shared memory regions*/
         tport_detach( &InRegion );
         logit("t", "um_alarm3: Terminateion requested; exiting.\n" );
         return 0;
      }

      /* send eqalarm_ew's heartbeat
       *****************************/
      if ( time(&timeNow) - timeLastBeat  >=  heartBeatInterval ) {
        timeLastBeat = timeNow;
        eqalarm_status( TypeHeartBeat, 0, "" );
      }

      /* Get and process the next hyposum message from shared memory 
       *************************************************************/
       res = tport_getmsg( &InRegion, GetLogo, nLogo, &msgLogo, &msgSize,
                           message, sizeof(message)-1 );
       switch(res) {
         case GET_NONE:
           break;

         case GET_TOOBIG:
           sprintf( Text, "Retrieved msg[%ld] (i%u m%u t%u) too big for message[%d]",
                           msgSize, msgLogo.instid, msgLogo.mod, msgLogo.type,
                           sizeof(message) );
           eqalarm_status( TypeError, ERR_TOOBIG, Text );
           break;

         case GET_MISS:
           sprintf( Text,"Missed msg(s)  i%u m%u t%u  region:%ld.",
                          msgLogo.instid, msgLogo.mod, msgLogo.type, InRegion.key);
           eqalarm_status( TypeError, ERR_MISSMSG, Text );

         case GET_NOTRACK:
           sprintf( Text,"Msg received (i%u m%u t%u); transport.h NTRACK_GET exceeded",
                          msgLogo.instid, msgLogo.mod, msgLogo.type );
           eqalarm_status( TypeError, ERR_NOTRACK, Text );

         case GET_OK:
           message[msgSize] = '\0';       /*null terminate the message*/
           /* copy message to hy71sum      */
           strcpy( hyp2000arc,message);

           /* See if its a big deal, and create a message to pagerfeeder if so
            *******************************************************************/
           if ( procMessage(hyp2000arc, MagThreshold, EmailFile, \
                QDDSCommand, QDDSuser, QDDSmachine, QDDSpoll )==EW_FAILURE) {
              logit("et","um_alarm3: failed to generate alarm\n");
           }
           break;
       }
    } while (res !=GET_NONE );      /* end of message processing loop */

    sleep_ew( 500 );       /* wait around for more summary lines   */

 }  /* end of while(1) */

        /*------------------------------end of working loop------------------------------*/
} /* end of main */

/****************************************************************************
 *      eqalarm_config() process command file using kom.c functions         *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void eqalarm_config( char* configfile )
{
   int  ncommand;       /* # of required commands you expect */
   char init[20];       /* init flags, one byte for each required command */
   int  nmiss;          /* number of required commands that were missed   */
   char *com;
   char *str;
   int  nfiles;
   int  success;
   int  i;


/* Set to zero one init flag for each required command
 *****************************************************/
   ncommand = 10;
   for( i=0; i<ncommand; i++ )  init[i] = 0;
   nLogo    = 0;

/* Open the main configuration file
 **********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 ) {
        fprintf( stderr,
                "um_alarm3: Error opening command file <%s>; exiting!\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr,
                          "um_alarm3: Error opening command file <%s>; exiting!\n",
                           &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command
         ************************************/
  /*0*/     if( k_its("LogSwitch") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModuleId , str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[2] = 1;
            }

        /* Retrieve installation and module to get messages from  */

  /*3*/     else if( k_its("GetSumFrom") ) {
                if ( nLogo >= MAXLOGO ) {
                    fprintf( stderr,
                            "um_alarm3: Too many <GetSumFrom> commands in <%s>",
                             configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAXLOGO );
                    exit( -1 );
                }
                if( ( str=k_str() ) ) {
                   if( GetInst( str, &GetLogo[nLogo].instid ) != 0 ) {
                       fprintf( stderr,
                               "um_alarm3: Invalid installation name <%s>", str );
                       fprintf( stderr, " in <GetSumFrom> cmd; exiting!\n" );
                       exit( -1 );
                   }
                }
                if( ( str=k_str() ) ) {
                   if( GetModId( str, &GetLogo[nLogo].mod ) != 0 ) {
                       fprintf( stderr,
                               "um_alarm3: Invalid module name <%s>", str );
                       fprintf( stderr, " in <GetSumFrom> cmd; exiting!\n" );
                       exit( -1 );
                   }
                }
                if( GetType( "TYPE_HYP2000ARC", &GetLogo[nLogo].type ) != 0 ) {
                    fprintf( stderr,
                               "um_alarm3: Invalid message type <TYPE_HYP2000ARC>" );
                    fprintf( stderr, "; exiting!\n" );
                    exit( -1 );
                }
		/*
                printf("um_alarm3: GetLogo[%d] inst:%d module:%d type:%d\n",
                        nLogo, (int) GetLogo[nLogo].instid,
                               (int) GetLogo[nLogo].mod,
                               (int) GetLogo[nLogo].type );
		*/
                nLogo++;
                init[3] = 1;
            }

        /* Get the magnitude threshold
         ****************************/
/*4*/   else if( k_its("MagThreshold") ) {
                MagThreshold=k_val();
                init[4] = 1;
        }

/*5*/   else if( k_its("EmailFile") ) {
                strcpy(EmailFile,k_str());
                init[5] = 1;
        }

/*6*/   else if( k_its("QDDSCommand") ) {
                strcpy(QDDSCommand,k_str());
                init[6] = 1;
        }

/*7*/   else if( k_its("QDDSuser") ) {
                strcpy(QDDSuser,k_str());
                init[7] = 1;
        }

/*8*/   else if( k_its("QDDSmachine") ) {
                strcpy(QDDSmachine,k_str());
                init[8] = 1;
        }

/*9*/   else if( k_its("QDDSpoll") ) {
                strcpy(QDDSpoll,k_str());
                init[9] = 1;
        }

        /* At this point we give up. Unknowd thing.
        *******************************************/
        else {
                fprintf(stderr, "um_alarm3: <%s> Unknown command in <%s>.\n",
                        com, configfile );
                continue;
        }

        /* See if there were any errors processing the command
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "um_alarm3: Bad <%s> command  in <%s>; exiting!\n",
                        com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "um_alarm3: ERROR, no " );
       if ( !init[0] )  fprintf( stderr, "<LogSwitch> "      );
       if ( !init[1] )  fprintf( stderr, "<MyModuleId> "   );
       if ( !init[2] )  fprintf( stderr, "<RingName> "     );
       if ( !init[3] )  fprintf( stderr, "<GetSumFrom> " );
       if ( !init[4] )  fprintf( stderr, "<MagThreshold> " );
       if ( !init[5] )  fprintf( stderr, "<EmailFile> " );
       if ( !init[6] )  fprintf( stderr, "<QDDSCommand> " );
       if ( !init[7] )  fprintf( stderr, "<QDDSuser> " );
       if ( !init[8] )  fprintf( stderr, "<QDDSmachine> " );
       if ( !init[9] )  fprintf( stderr, "<QDDSpoll> " );
       fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }

  return;
}


/*********************************************************************************/
/* eqalarm_status() builds a heartbeat or error msg & puts it into shared memory */
/*********************************************************************************/
void eqalarm_status( unsigned char type,  short ierr,  char *note )
{
        MSG_LOGO    logo;
        char        msg[256];
        long        size;
        time_t      t;

        logo.instid = InstId;
        logo.mod    = MyModId;
        logo.type   = type;

        time( &t );
        if( type == TypeHeartBeat ) {
                sprintf( msg, "%ld %ld\n", t, MyPid );
        }
        else if( type == TypeError ) {
                sprintf( msg, "%ld %hd %s\n", t, ierr, note );
                logit( "t", "um_alarm3:  %s\n", note );
        }

        size = strlen( msg );   /* don't include the null byte in the message */
        if( tport_putmsg( &InRegion, &logo, size, msg ) != PUT_OK )
        {
                if( type == TypeHeartBeat ) {
                    logit("et","um_alarm3:  Error sending heartbeat.\n" );
                }
                else if( type == TypeError ) {
                    logit("et","um_alarm3:  Error sending error:%d.\n", ierr );
                }
        }
        return;
}


/*********************************************************************************/
/*  eqalarm_lookup( ) Look up important info from earthworm.h tables             */
/*********************************************************************************/
void eqalarm_lookup( )
{
/* Look up keys to shared memory regions
   *************************************/
   if( (PublicKey = GetKey(RingName)) == -1 ) {
        fprintf( stderr,
                "um_alarm3: Invalid ring name <%s>; exiting!\n", RingName );
        exit( -1 );
   }

/* Look up installation Id
   ***********************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr,
              "um_alarm3: error getting local installation id; exiting!\n" );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModuleId, &MyModId ) != 0 ) {
      fprintf( stderr,
              "um_alarm3: Invalid module name <%s>; exiting!\n", MyModuleId );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr,
              "um_alarm3: Invalid message type <TYPE_HEARTBEAT>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr,
              "um_alarm3: Invalid message type <TYPE_ERROR>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_HYP2000ARC", &TypeSumm) != 0 ) {
      fprintf( stderr,
              "um_alarm3: Invalid message type <TYPE_HYP2000ARC>; exiting!\n" );
      exit( -1 );
   }
     return;
}


/***********************************************************************

  procMessage: hacked from makeMessage

***********************************************************************/

#include <sys/types.h>
#include "cubeE.h"

int procMessage(char* hyp2000arc, double Threshold, char* EmailFile, \
                char* QDDSCommand, char* QDDSuser, char* QDDSmachine, \
                char* QDDSpoll )
{
  #define PAGER_MSG_LEN 100
  #define MAX_STR 512
  #define DATESTR_LEN 10
  static char pagerText[PAGER_MSG_LEN];   /* Common max alpha pager message length */
  int index, i;
  char datestring[DATESTR_LEN];
  struct  Hsum Sum;
  struct  Hpck Pick;
  char      *in;            /* working pointer to archive message    */
  char      line[MAX_STR];  /* to store lines from msg               */ 
  char      shdw[MAX_STR];  /* to store shadow cards from msg        */ 
  int       msglen;         /* length of input archive message       */ 
  int       nline;          /* number of lines (not shadows) so far  */

  static char emailMsg[MAX_STR]; /* email command */
  static char cubeMsg[MAX_STR];  /* cube command */
  static char qddsMsg[MAX_STR];  /* qdds message */
  static char CubeE[82];         /* the actual cube format event message used by QDDS*/
  char a;
  u_int cksum;
  CUBEE cube;
  struct Greg greg;
  
   /* Initialize some stuff
   ***********************/
   nline  = 0;
   msglen = strlen( hyp2000arc );
   in     = hyp2000arc;

   while( in < hyp2000arc+msglen )
   {
      if ( sscanf( in, "%[^\n]", line ) != 1 )  return( -1 );
      in += strlen( line ) + 1;
      if ( sscanf( in, "%[^\n]", shdw ) != 1 )  return( -1 );
      in += strlen( shdw ) + 1;
      nline++;
      /* Process the hypocenter card (1st line of msg) & its shadow
      ************************************************************/
      if( nline == 1 ) {                /* hypocenter is 1st line in msg  */
         read_hyp( line, shdw, &Sum );
         continue;
      }
 
      /* Process all the phase cards & their shadows
      *********************************************/
      if( strlen(line) < (size_t) 75 )  /* found the terminator line      */
         break;
      read_phs( line, shdw, &Pick );    /* load info into Pick structure   */

   } /*end while over reading message*/

   /* Date and Time */
   make_datestr( Sum.ot, datestring );
   sprintf(pagerText,"%s Lat: %-3.2f Lon: %-4.2f Depth: %-4.1f Mag: %-2.1f Picks: %-3d Gap: %-3d, RMS: %-4.2, CERI",
                      datestring,
                      Sum.lat,            /* lat */
                      Sum.lon,            /* lon */
                      Sum.z,              /* depth */
                      Sum.Md,             /* duration magnitude */
                      Sum.nph,            /* number of picks */
                      Sum.gap,            /* max azimuthal gap */
                      Sum.rms);           /* rms travel time residual */

   /* make up the email message */
   sprintf(emailMsg,"printf \"%s\" | /usr/ucb/mail -s \"NMSZ earthquake\" `cat %s`",
                     pagerText,EmailFile);
   logit("","um_alarm3: making following system call:\n%s\n",emailMsg);
   system(emailMsg);

/* make up the cube text */
/* see ~withers/src/makecube for detailed explanation of cubeE format */

  strcpy(cube.Tp,"E \0");  /* message type always E */
  sprintf(cube.Eid,"%8d\0",Sum.qid); /* event id; careful ew4.0 has Sum.Eid as long */
                                                /*        ew4.1 has Sum.Eid as string */
  strcpy(cube.So,"NM\0");  /* network code always New Madrid */
  strcpy(cube.V,"A\0");    /* message version number always A */

  datime(Sum.ot,&greg); /* datime is from chron3 greg is of type struct Greg */
  cube.Year = greg.year;
  cube.Mo = greg.month;
  cube.Dy = greg.day;
  cube.Hr = greg.hour;
  cube.Mn = greg.minute;
  cube.Sec = (int)( 10.0 * greg.second);

  cube.Lat = (int)( 10000.0 * Sum.lat );
  cube.Long = (int)( 10000.0 * Sum.lon );
  cube.Dept = (int)( 10.0 * Sum.z );
  cube.Mg = (int)( 10.0 * Sum.Md );
  cube.Nst = Sum.nph;  /* assume number of stations are equal to number of phases (p-only) */
  cube.Nph = Sum.nph;
  cube.Dmin = Sum.dmin;
  cube.Rmss = (int)( 100.0 * Sum.rms );
  cube.Erho = (int)( 10.0 * Sum.erh );
  cube.Erzz = (int)( 10.0 * Sum.erz );
  cube.Gp = (int)( (float)Sum.gap / 3.6 );
  strcpy(cube.M,"D\0");  /* assume coda duration magnitude */
  cube.Nm = 0;  /* don't know number of coda's used */
  cube.Em = 0;  /* don't know mag error either */
  strcpy(cube.L,"H\0");  /* assume unchecked hypo location method */
  
  /* compute the chksum */
  sprintf(CubeE,"%-2.2s%8.8s%2.2s%1.1s%4d%2d%2d%2d%2d%3d%+7d%+8d%4d%2d%3d%3d%4d%4d%4d%4d%2d%1.1s%2d%2d%1.1s\0",
                  cube.Tp,
                  cube.Eid,
                  cube.So,
                  cube.V,
                  cube.Year,
                  cube.Mo,
                  cube.Dy,
                  cube.Hr,
                  cube.Mn,
                  cube.Sec,
                  cube.Lat,
                  cube.Long,
                  cube.Dept,
                  cube.Mg,
                  cube.Nst,
                  cube.Nph,
                  cube.Dmin,
                  cube.Rmss,
                  cube.Erho,
                  cube.Erzz,
                  cube.Gp,
                  cube.M,
                  cube.Nm,
                  cube.Em,
                  cube.L
           );
  cksum=0;
  for (i = 0; i < 79; i++) {
     if ( cksum&1 )
        cksum = (cksum>>1) + 0x8000;
     else
       cksum >>= 1;
        
     cksum += CubeE[i];
     cksum &= 0xFFFF;
  }

  a = 36 + cksum%91;
  sprintf(cube.C,"%1c\0",a);

  /* pack the message */
  sprintf(CubeE,"%-2.2s%8.8s%2.2s%1.1s%4d%2d%2d%2d%2d%3d%+7d%+8d%4d%2d%3d%3d%4d%4d%4d%4d%2d%1.1s%2d%2d%1.1s%1.1s\0",
                  cube.Tp,
                  cube.Eid,
                  cube.So,
                  cube.V,
                  cube.Year,
                  cube.Mo,
                  cube.Dy,
                  cube.Hr,
                  cube.Mn,
                  cube.Sec,
                  cube.Lat,
                  cube.Long,
                  cube.Dept,
                  cube.Mg,
                  cube.Nst,
                  cube.Nph,
                  cube.Dmin,
                  cube.Rmss,
                  cube.Erho,
                  cube.Erzz,
                  cube.Gp,
                  cube.M,
                  cube.Nm,
                  cube.Em,
                  cube.L,
                  cube.C
           );

  /* create the QDDS system call */

  sprintf(qddsMsg,"%s %s %s '/bin/echo \"%s\" > %s/event1'",
                  QDDSCommand, QDDSuser,QDDSmachine,CubeE,QDDSpoll);

  logit("","um_alarm3: making following system call:\n%s\n",qddsMsg);
  system(qddsMsg);

  return(EW_SUCCESS);
}
/*********************************************************************
   withers stole this from arc2trig

 * make_datestr()  takes a time in seconds since 1600 and converts   *
 *                 it into a character string in the form of:        *   
 *                   "19880123 12:34:12.21"                          *
 *                 It returns a pointer to the new character string  *
 *                                                                   *
 *    NOTE: this requires an output buffer >=21 characters long      *   
 *                                                                   *
 *  Y2K compliance:                                                  *
 *     date format changed to YYYYMMDD                               *
 *     date15() changed to date17()                                  *
 *                                                                   *
 *********************************************************************/

char *make_datestr( double t, char *datestr )
{
    char str17[18];   /* temporary date string */

/* Convert time to a pick-format character string */
    date17( t, str17 );
 
/* Convert a date character string in the form of:
   "19880123123412.21"        to one in the form of:
   "19880123 12:34:12.21"
    0123456789 123456789
   Requires a character string at least 21 characters long
*/
    strncpy( datestr, str17,    8 );    /*yyyymmdd*/
    datestr[8] = '\0';
    strcat ( datestr, " " );
    strncat( datestr, str17+8,  2 );    /*hr*/
    strcat ( datestr, ":" );
    strncat( datestr, str17+10,  2 );    /*min*/
    strcat ( datestr, ":" );
    strncat( datestr, str17+12, 5 );    /*seconds*/
 
    /*printf( "str17 <%s>  newstr<%s>\n", str17, datestr );*/ /*DEBUG*/
 
    return( datestr );
}

