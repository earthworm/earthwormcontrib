/***********************************************************************
 ewarc2xml                                          Mitch Withers 200706

 Read a Hypo arc message from the input ring and write a  Shakemap
 compliant xml event file to the output directory

 compile and run with earthworm

 ***********************************************************************/

#include <stdio.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include <swap.h>
#include <transport.h>
#include <mem_circ_queue.h>
#include <read_arc.h>
#include <rw_mag.h>
#include <chron3.h>
#include <time_ew.h>
#include "ewarc2xml.h"

static  SHM_INFO  InRegion;      /* shared memory region to use for i/o    */

#define   NUM_COMMANDS  8    /* how many required commands in the config file */
#define   MAX_STR 255

#define   MAXLOGO   5
MSG_LOGO  GetLogo[MAXLOGO];       /* array for requesting module,type,instid */
short     nLogo;

/* The message queue
  *******************/
#define QUEUE_SIZE              1000        /* How many msgs can we queue */
QUEUE   MsgQueue;                               /* from queue.h */

/* Thread stuff */
#define THREAD_STACK 8192
static unsigned tidProcessor;    /* Processor thread id */
static unsigned tidStacker;      /* Thread moving messages from InRing */
                                 /* to MsgQueue */
int MessageStackerStatus = 0;      /* 0=> Stacker thread ok. <0 => dead */
int ProcessorStatus = 0;           /* 0=> Processor thread ok. <0 => dead */

/* Things to read or derive from configuration file
 **************************************************/
static char    InRingName[MAX_RING_STR];   /* name of transport ring for i/o */
static char    MyModName[MAX_MOD_STR];     /* this module's given name */
static char    MyProgName[256];     /* this module's program name        */
static int     LogSwitch;           /* 0 if no logfile should be written */
static long    HeartBeatInterval;   /* seconds between heartbeats        */
static int     Debug = 0;           /* 1- print debug, 0- no debug */
static char    OutDirName[MAX_STR]; /* name of output xml directory */
static char    TmpDirName[MAX_STR]; /* name of temporary xml directory */
static char    QuakeAuthor[MAX_STR]; /* see the .d */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          InKey;         /* key of transport ring for i/o     */
static long          OutKey;        /* key of transport ring for i/o     */
static unsigned char InstId;        /* local installation id             */
static unsigned char MyModId;       /* Module Id for this program        */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeArc;
static unsigned char InstWild;
static unsigned char ModWild;

/* Error messages
 *********************************/
#define  ERR_MISSMSG       0   /* message missed in transport ring       */
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer     */
#define  ERR_NOTRACK       2   /* msg retreived; tracking limit exceeded */
#define  ERR_QUEUE         4   /* trouble with the MsgQueue operation */

static char  errText[MAX_STR];    /* string for log/error messages          */

pid_t MyPid;    /** Hold our process ID to be sent with heartbeats **/

/* Function Prototypes */
int read_hyp (char *, char *, struct Hsum *);

/* Functions in this source file
 *******************************/
static  int     ewarc2xml_config (char *);
static  int     ewarc2xml_lookup (void);
static  void    ewarc2xml_status (unsigned char, short, char *);
thr_ret                 MessageStacker (void *);
thr_ret                 Processor (void *);

int main (int argc, char **argv)
{
  time_t                  timeNow;       /* current time                  */
  time_t                  timeLastBeat;  /* time last heartbeat was sent  */
  long                  recsize;       /* size of retrieved message     */
  MSG_LOGO              reclogo;       /* logo of retrieved message     */
  char                  *flushmsg;
  int                   i, j, k;

  /* Check command line arguments
   ******************************/
  if (argc != 2)
  {
    fprintf (stderr, "Usage: ewarc2xml <configfile>\n");
    return EW_FAILURE;
  }

  /* To be used in logging functions
   *********************************/
  if (get_prog_name (argv[0], MyProgName) != EW_SUCCESS)
  {
    fprintf (stderr, "ewarc2xml: Call to get_prog_name failed.\n");
    return EW_FAILURE;
  }

  /* Initialize name of log-file & open it
   ***************************************/
  logit_init (argv[1], 0, 256, 1);

  /* Read the configuration file(s)
   ********************************/
  if (ewarc2xml_config(argv[1]) != EW_SUCCESS)
  {
    logit( "e", "ewarc2xml: Call to ewarc2xml_config failed \n");
    return EW_FAILURE;
  }
  logit ("" , "%s(%s): Read command file <%s>\n",
         MyProgName, MyModName, argv[1]);

  /* Look up important info from earthworm.h tables
   ************************************************/
  if (ewarc2xml_lookup() != EW_SUCCESS)
  {
    logit( "e", "%s(%s): Call to ewarc2xml_lookup failed \n",
             MyProgName, MyModName);
    return EW_FAILURE;
  }

 /* Reinitialize logit to desired logging level
   *********************************************/
  logit_init (argv[1], 0, 256, LogSwitch);

  /* Get our process ID
   **********************/
  if ((MyPid = getpid ()) == -1)
  {
    logit ("e", "%s(%s): Call to getpid failed. Exiting.\n",
           MyProgName, MyModName);
    return (EW_FAILURE);
  }

  /* Attach to shared memory ring
   *******************************************/
  tport_attach (&InRegion, InKey);
  logit ("", "%s(%s): Attached to public memory region %s: %d\n",
         MyProgName, MyModName, InRingName, InKey);

  /* Force a heartbeat to be issued in first pass thru main loop
   *************************************************************/
  timeLastBeat = time (&timeNow) - HeartBeatInterval - 1;

  /* Flush the incoming transport ring
   ***********************************/
  if ((flushmsg = (char *) malloc (MAX_BYTES_PER_EQ)) ==  NULL)
  {
    logit ("e", "ewarc2xml: can't allocate flushmsg; exiting.\n");
    return EW_FAILURE;
  }

  while (tport_getmsg (&InRegion, GetLogo, nLogo, &reclogo,
         &recsize, flushmsg, (MAX_BYTES_PER_EQ - 1)) != GET_NONE);

  /* Create MsgQueue mutex */
  CreateMutex_ew();

  /* Allocate the message Queue
   ********************************/
  initqueue (&MsgQueue, QUEUE_SIZE, MAX_BYTES_PER_EQ);

  /* Start message stacking thread which will read
   * messages from the InRing and put them into the Queue
   *******************************************************/
  if (StartThread (MessageStacker, (unsigned) THREAD_STACK, &tidStacker) == -1)
  {
    logit( "e", "ewarc2xml: Error starting MessageStacker thread.  Exiting.\n");
    tport_detach (&InRegion);
    return EW_FAILURE;
  }

  MessageStackerStatus = 0; /*assume the best*/

  /* Start processing thread which will read messages from
   * the Queue, process them and write them to the OutRing
   *******************************************************/
  if (StartThread (Processor, (unsigned) THREAD_STACK, &tidProcessor) == -1)
  {
    logit( "e", "ewarc2xml: Error starting Processor thread.  Exiting.\n");
    tport_detach (&InRegion);
    return EW_FAILURE;
  }

  ProcessorStatus = 0; /*assume the best*/

/*--------------------- setup done; start main loop -------------------------*/

  /* We don't do much here - just beat our heart
   * and check on our threads
   **********************************************/
  while (tport_getflag (&InRegion) != TERMINATE  &&
         tport_getflag (&InRegion) !=  MyPid )
  {

    /* send a heartbeat
    ***************************/
    if (time (&timeNow) - timeLastBeat  >=  HeartBeatInterval)
    {
      timeLastBeat = timeNow;
      ewarc2xml_status (TypeHeartBeat, 0, "");
    }

    /* Check on our threads */
    if (MessageStackerStatus < 0)
    {
      logit ("et", "ewarc2xml: MessageStacker thread died. Exiting\n");
      return EW_FAILURE;
    }

    if (ProcessorStatus < 0)
    {
      logit ("et", "ewarc2xml: Processor thread died. Exiting\n");
      return EW_FAILURE;
    }

    sleep_ew (1000);

  } /* wait until TERMINATE is raised  */

  /* Termination has been requested
   ********************************/
  tport_detach (&InRegion);
  logit ("t", "ewarc2xml: Termination requested; exiting!\n" );
  return EW_SUCCESS;

}

/******************************************************************************
 *  ewarc2xml_config() processes command file(s) using kom.c functions;       *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
static int ewarc2xml_config (char *configfile)
{
  char  init[NUM_COMMANDS]; /* init flags, one byte for each required command */
  int   nmiss;              /* number of required commands that were missed */
  char  *com;
  char  *str;
  int   nfiles;
  int   success;
  int   i;

    /* Set to zero one init flag for each required command
  *****************************************************/
  for (i = 0; i < NUM_COMMANDS; i++)
    init[i] = 0;

  nLogo = 0;

  nfiles = k_open (configfile);
  if (nfiles == 0)
  {
    logit("e", "ewarc2xml: Error opening command file <%s>; exiting!\n", configfile);
    return EW_FAILURE;
  }

  /* Process all command files
   ***************************/
  while (nfiles > 0)   /* While there are command files open */
  {
    while (k_rd ())        /* Read next line from active file  */
    { 
      com = k_str ();         /* Get the first token from line */

      /* Ignore blank lines & comments
       *******************************/
      if (!com)
        continue;

      if (com[0] == '#')
        continue;

      /* Open a nested configuration file
       **********************************/
      if (com[0] == '@')
      {
        success = nfiles + 1;
        nfiles  = k_open (&com[1]);
        if (nfiles != success)
        {
          logit("e", "ewarc2xml: Error opening command file <%s>; exiting!\n",
                   &com[1]);
          return EW_FAILURE;
        }
        continue;
      }

     /* Process anything else as a command
       ************************************/
/*0*/ if (k_its ("MyModuleId"))
      {
        if ((str = k_str ()) != NULL)
        {
          strcpy (MyModName, str);
          init[0] = 1;
        }
      }
/*1*/ else if (k_its ("InRing"))
      {
      if ((str = k_str ()) != NULL)
        {
          strcpy (InRingName, str);
          init[1] = 1;
        }
      }
/*2*/ else if (k_its ("HeartBeatInt"))
      {
        HeartBeatInterval = k_long ();
        init[2] = 1;
      }
/*3*/ else if (k_its ("LogFile"))
      {
        LogSwitch = k_int();
        init[3] = 1;
      }

/*NR*/else if (k_its ("Debug"))
      {
        Debug = 1;
      }
      /* Enter installation & module types to get
       *******************************************/
/*4*/ else if (k_its ("GetEventsFrom"))
      {
        if (nLogo >= MAXLOGO)
        {
          logit("e", "ewarc2xml: Too many <GetMsgLogo> commands in <%s>; "
                   "; max=%d; exiting!\n", configfile, (int) MAXLOGO);
          return EW_FAILURE;
        }
        if ((str = k_str()))
        {
          if (GetInst (str, &GetLogo[nLogo].instid) != 0)
          {
            logit("e", "ewarc2xml: Invalid installation name <%s> in "
                     "<GetEventsFrom> cmd; exiting!\n", str);
            return EW_FAILURE;
          }
        }
        if ((str = k_str()))
        {
          if (GetModId (str, &GetLogo[nLogo].mod) != 0)
          {
            logit("e", "ewarc2xml: Invalid module name <%s> in <GetEventsFrom> "
                     "cmd; exiting!\n", str);
            return EW_FAILURE;
          }
        }
        /* We'll always fetch arc messages */
        if (GetType ("TYPE_HYP2000ARC", &GetLogo[nLogo].type) != 0)
        {
          logit("e", "ewarc2xml: Invalid msgtype <%s> in <GetEventsFrom> "
                   "cmd; exiting!\n", str);
          return EW_FAILURE;
        }
        nLogo++;
        init[4] = 1;
      }

/*5*/ else if (k_its ("OutDir"))
      {
        if ((str = k_str ()) != NULL)
        {
          strcpy (OutDirName, str);
          init[5] = 1;
        }
      }
/*7*/ else if (k_its ("TmpDir"))
      {
        if ((str = k_str ()) != NULL)
        {
          strcpy (TmpDirName, str);
          init[6] = 1;
        }
      }
/*8*/ else if ( k_its("QuakeAuthor") ) {
        if( (str = k_str()) != NULL ) {
          strcpy(QuakeAuthor,str);
          if( QuakeAuthor == NULL) {
            logit( "e", "shakemapfeed: out of memory for <QuakeAuthor>\n");
            exit( -1 );
          }
          init[7] = 1;
        }
      }

      /* Unknown command
       *****************/
      else
      {
        logit("e", "ewarc2xml: <%s> Unknown command in <%s>.\n", com, configfile)
;
        continue;
      }

      /* See if there were any errors processing the command
       *****************************************************/
      if (k_err ())
      {
        logit("e", "ewarc2xml: Bad <%s> command in <%s>; exiting!\n", com, configfile);
        return EW_FAILURE;
      }

    } /** while k_rd() **/

    nfiles = k_close ();

  } /** while nfiles **/

  /* After all files are closed, check init flags for missed commands
   ******************************************************************/
  nmiss = 0;
  for (i = 0; i < NUM_COMMANDS; i++)
  {
    if (!init[i])
    {
      nmiss++;
    }
  }

  if (nmiss)
  {
    logit("e", "ewarc2xml: ERROR, no ");
    if (!init[0])  logit("e", "<MyModuleId> "    );
    if (!init[1])  logit("e", "<InRing> "        );
    if (!init[2])  logit("e", "<HeartBeatInt> "  );
    if (!init[3])  logit("e", "<LogFile> "       );
    if (!init[4])  logit("e", "<GetEventsFrom> " );
    if (!init[5])  logit("e", "<OutDir>        " );
    if (!init[6])  logit("e", "<TmpDir>        " );
    if (!init[7])  logit("e", "<QuakeAuthor>        " );

    logit("e", "command(s) in <%s>; exiting!\n", configfile);
    return EW_FAILURE;
  }

  return EW_SUCCESS;
}

/******************************************************************************
 *  ewarc2xml_lookup( )   Look up important info from earthworm.h tables      *
 ******************************************************************************/
static int ewarc2xml_lookup( void )
{

  /* Look up keys to shared memory regions
  *************************************/
  if ((InKey = GetKey (InRingName)) == -1)
  {
    logit( "e", "ewarc2xml:  Invalid ring name <%s>; exiting!\n", InRingName);
    return EW_FAILURE;
  }

  /* Look up installations of interest
  *********************************/
  if (GetLocalInst (&InstId) != 0)
  {
    logit( "e", "ewarc2xml: error getting local installation id; exiting!\n");
    return EW_FAILURE;
  }

  if (GetInst ("INST_WILDCARD", &InstWild ) != 0)
  {
    logit( "e", "ewarc2xml: error getting wildcard installation id; exiting!\n");
    return EW_FAILURE;
  }
  /* Look up modules of interest
  ******************************/
  if (GetModId (MyModName, &MyModId) != 0)
  {
    logit( "e", "ewarc2xml: Invalid module name <%s>; exiting!\n", MyModName);
    return EW_FAILURE;
  }

  if (GetModId ("MOD_WILDCARD", &ModWild) != 0)
  {
    logit( "e", "ewarc2xml: Invalid module name <MOD_WILDCARD>; exiting!\n");
    return EW_FAILURE;
  }
  /* Look up message types of interest
  *********************************/
  if (GetType ("TYPE_HEARTBEAT", &TypeHeartBeat) != 0)
  {
    logit( "e", "ewarc2xml: Invalid message type <TYPE_HEARTBEAT>; exiting!\n");
    return EW_FAILURE;
  }

  if (GetType ("TYPE_ERROR", &TypeError) != 0)
  {
    logit( "e", "ewarc2xml: Invalid message type <TYPE_ERROR>; exiting!\n");
    return EW_FAILURE;
  }

  if (GetType ("TYPE_HYP2000ARC", &TypeArc) != 0)
  {
    logit( "e", "ewarc2xml: Invalid message type <TYPE_HYP2000ARC>; exiting!\n");
    return EW_FAILURE;
  }

  return EW_SUCCESS;

}

/******************************************************************************
 * ewarc2xml_status() builds a heartbeat or error message & puts it into       *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/
static void ewarc2xml_status( unsigned char type, short ierr, char *note )
{
  MSG_LOGO    logo;
  char        msg[256];
  long        size;
  long        t;

  /* Build the message
  *******************/
  logo.instid = InstId;
  logo.mod    = MyModId;
  logo.type   = type;

  time (&t);

  if (type == TypeHeartBeat)
  {
    sprintf (msg, "%ld %ld\n\0", t, MyPid);
  }
  else if (type == TypeError)
  {
    sprintf (msg, "%ld %hd %s\n\0", t, ierr, note);
    logit ("et", "%s(%s): %s\n", MyProgName, MyModName, note);
  }

  size = strlen (msg);   /* don't include the null byte in the message */

  /* Write the message to shared memory
  ************************************/
  if (tport_putmsg (&InRegion, &logo, size, msg) != PUT_OK)
  {
    if (type == TypeHeartBeat)
    {
      logit ("et","%s(%s):  Error sending heartbeat.\n", MyProgName, MyModName);
    }
    else if (type == TypeError)
    {
      logit ("et","%s(%s):  Error sending error:%d.\n", MyProgName, MyModName, ierr);
    }
  }

}

/********************** Message Stacking Thread *******************
 *           Move messages from transport to memory queue         *
 ******************************************************************/
thr_ret MessageStacker (void *dummy)
{
  char          *msg;           /* "raw" retrieved message */
  int           res;
  long          recsize;        /* size of retrieved message */
  MSG_LOGO      reclogo;        /* logo of retrieved message */
  int           ret;

  /* Allocate space for input/output messages
   *******************************************/
  if ((msg = (char *) malloc (MAX_BYTES_PER_EQ)) == (char *) NULL)
  {
    logit ("e", "ewarc2xml: error allocating msg; exiting!\n");
    goto error;
  }

  /* Tell the main thread we're ok
   ********************************/
  MessageStackerStatus = 0;

  /* Start service loop, picking up messages
   *****************************************/
  while (1)
  {
    /* Get a message from transport ring, carefully checking error codes
     *******************************************************************/
    res = tport_getmsg (&InRegion, GetLogo, nLogo, &reclogo,
                        &recsize, msg, MAX_BYTES_PER_EQ-1);

    if (res == GET_NONE)  /* no messages for us now */
    {
      sleep_ew(100);
      continue;
    }

    if (res != GET_OK)    /* some kind of error code, speak */
    {
      if (res == GET_TOOBIG) /* msg too big for buffer */
      {
        sprintf (errText, "msg[%ld] i%d m%d t%d too long for target", recsize,
                           (int) reclogo.instid, (int) reclogo.mod, (int)reclogo.type);
        ewarc2xml_status (TypeError, ERR_TOOBIG, errText);
        continue;
      }
      else if (res == GET_MISS)  /* got msg, but may have missed some */
      {
        sprintf (errText, "missed msg(s) i%d m%d t%d in %s", (int) reclogo.instid,
                           (int) reclogo.mod, (int)reclogo.type, InRingName);
        ewarc2xml_status (TypeError, ERR_MISSMSG, errText);
      }
      else if (res == GET_NOTRACK) /* got msg, can't tell if any were missed */
      {
        sprintf (errText, "no tracking for logo i%d m%d t%d in %s", (int) reclogo.instid,
                           (int) reclogo.mod, (int)reclogo.type, InRingName);
        ewarc2xml_status (TypeError, ERR_NOTRACK, errText);
      }
    }

    /* Queue retrieved msg (res==GET_OK,GET_MISS,GET_NOTRACK)
    ********************************************************/
    RequestMutex ();
    ret = enqueue (&MsgQueue, msg, recsize, reclogo);
    ReleaseMutex_ew ();

    if (ret != 0)
    {
      if (ret == -2)  /* Serious: quit */
      {
        sprintf (errText, "internal queue error. Terminating.");
        ewarc2xml_status (TypeError, ERR_QUEUE, errText);
        goto error;
      }
      if (ret == -1)
      {
        sprintf (errText,"queue cannot allocate memory. Lost message.");
        ewarc2xml_status (TypeError, ERR_QUEUE, errText);
        continue;
      }
      if (ret == -3)
      {
        sprintf (errText, "Queue full. Message lost.");
        ewarc2xml_status (TypeError, ERR_QUEUE, errText);
        continue;
      }
    } /* problem from enqueue */

  } /* while (1) */

 /* we're quitting
   *****************/
  error:
    MessageStackerStatus = -1; /* file a complaint to the main thread */
    KillSelfThread (); /* main thread will restart us */

}

/********************** Message Processing Thread ****************
 *
 * This is where all the action is: grab a message from the
 * queue, determine the originating installation ID. Find
 * Extract the location of the event from the message and
 * write the hypoinverse arc message to OutRing.
 *
 ******************************************************************/
thr_ret Processor (void *dummy)
{

  int ret, i, j, k;
  long msgSize;
  MSG_LOGO reclogo;           /* logo of retrieved message */
  float lat, lon;
  struct Hsum Sum;            /* Hyp2000 summary data */
  struct Hpck Pick;          /* Hyp2000 pick structure */
  struct EVENT outeq;        /* struct to collect info for xml */
  static char arcMsg[MAX_BYTES_PER_EQ];
  char     *in;             /* working pointer to archive message    */
  char      line[MAX_STR];  /* to store lines from msg               */
  char      shdw[MAX_STR];  /* to store shadow cards from msg        */
  int       msglen;         /* length of input archive message       */
  int       nline;          /* number of lines (not shadows) so far  */
  unsigned char INST_WILDCARD;
  char fname[MAX_STR];      /* file of xml to create*/
  char tmpfile[MAX_STR];      /* path and file of temporary xml to create*/
  char outfile[MAX_STR];      /* path and file of final xml to create*/
  long id;
  FILE *outfid;
  struct tm otm;
  time_t ot;

  GetInst("INST_WILDCARD",&INST_WILDCARD);
  while (1)
  {
    /* Grab the next message
     ************************/
    RequestMutex ();
    ret = dequeue (&MsgQueue, arcMsg, &msgSize, &reclogo);
    ReleaseMutex_ew ();

    if (ret < 0)  /* empty queue */
    {
      sleep_ew (500);
    }
    else  /* for a message from queue */
    {

      /************************************************************
       * Only need to read the first line to get the event summary
       ***********************************************************/

      if ( sscanf( arcMsg, "%[^\n]", line ) != 1 )
      {
        logit( "et", "ewarc2xml: Error reading data from arcmsg\n" );
        ProcessorStatus = 1;
        KillSelfThread();
      }
      if ( read_hyp( line, shdw, &Sum ) != 0)
      {
        logit ("e", "ewarc2xml: Call to read_hyp failed.\n");
        ProcessorStatus = 1;
        KillSelfThread ();
      }
      if (Debug == 1)
      {
        logit( "e", "ewarc2xml got following EVENT MESSAGE:\n");
        logit( "e", "%s\n", line );
      }

      /* load event info */
      outeq.time      = Sum.ot - GSEC1970;
      outeq.lat       = Sum.lat;
      outeq.lon       = Sum.lon;
      outeq.depth     = Sum.z;
      outeq.nph       = Sum.nph;
      outeq.rms       = Sum.rms;
      outeq.id        = Sum.qid;
      sprintf( outeq.idstr, "%ld", Sum.qid );
      strcpy(  outeq.author, QuakeAuthor );
      if( Sum.wtpref ) {  /*use preferred magnitude */
        outeq.mag     = Sum.Mpref;
        outeq.magwt   = Sum.wtpref;
        sprintf( outeq.magtype, "M%c", Sum.labelpref );
      } else {            /* use coda magnitude */
        outeq.mag     = Sum.Md;
        outeq.magwt   = Sum.mdwt;
        sprintf( outeq.magtype, "M%c", Sum.mdtype );
      }
       
      id = Sum.qid;
      /* Write the "eventID_event.xml" file of event information */
      sprintf( fname,   "%ld_event.xml", id );
      sprintf( tmpfile, "%s/%s", TmpDirName,  fname );
      sprintf( outfile, "%s/%s", OutDirName, fname );
      outfid = fopen( tmpfile, "w");
      if ( outfid == 0L )
      {
        logit("et", "Build_SM_file: Unable to open ShakeMap file: %s\n",
                     tmpfile);
        return EW_FAILURE;
      }

      ot = (time_t)outeq.time;
      gmtime_ew( &ot, &otm );

      fprintf(outfid,
          "<?xml version=\"1.0\" encoding=\"US-ASCII\" standalone=\"yes\"?>\n");

      fprintf(outfid, "<!DOCTYPE earthquake [\n%s]>\n", EARTHQUAKE_DTD);
      fprintf(outfid, "<earthquake id=\"%ld\" lat=\"%.2f\" lon=\"%.2f\" mag=\"%.2f\" " "year=\"%d\" month=\"%d\" day=\"%d\" hour=\"%d\" minute=\"%d\" " "second=\"%d\" timezone=\"GMT\" depth=\"%.2f\" locstring=\"\" " "created=\"%ld\" />\n",
              id, outeq.lat, outeq.lon, outeq.mag, otm.tm_year+1900,
              otm.tm_mon+1, otm.tm_mday, otm.tm_hour, otm.tm_min, otm.tm_sec,
              outeq.depth, time((time_t *)0));
      fclose(outfid);

      /* Move completed file to output directory
       *****************************************/
      if( rename( tmpfile, outfile ) != 0 ) {
        logit("et","Build_SM_file: Error renaming ShakeMap file: %s to: %s\n",
                    tmpfile, outfile );
        return EW_FAILURE;
      }

    } /* if ret > 0 */
     ProcessorStatus = 0;
  } /* while 1 */ 
}

