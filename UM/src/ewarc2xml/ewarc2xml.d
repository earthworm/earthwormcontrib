
# configuration file for ewarc2xml

# read arc messages from, for example, eqproc and write a shakemap v3 xml
# file named event.xml

LogFile 1     # 0 means don't create a disc log file. 1=> do.
              # 2 means write to module log but not to stderr/stdout
MyModuleId MOD_EWARC2XML
InRing     HYPO_RING
HeartBeatInt 30

#
# Optional Debug switch - if this is uncommented
# lots of debug messages will be produced
#
Debug

# MsgLogo of original TYPE_HYP2000ARC
#   msg, written as a 9-char ascii string
#   msgtype-moduleid-installationid.
#   Used with binder's eventid to look up
#   the DBMS id of the event.

QuakeAuthor 014012006

# What message logos to listen to. Can be more than one, but no more
# than five.
# The type is hard coded to TYPE_HYP2000ARC
GetEventsFrom INST_WILDCARD MOD_WILDCARD

# put the xml in this directory
OutDir /gaia/home/mwithers/Projects/ewarc2xml/input

# staging area while writing event
TmpDir /gaia/home/mwithers/Projects/ewarc2xml/temp

