
struct EVENT {    /* An Event List */
  double  time;     /* time of Event (sec since 1970)         */
  float   lat;      /* latitude (degrees)                     */
  float   lon;      /* longitude (degrees)                    */
  float   depth;    /* hypocentral depth                      */
  float   mag;      /* magnitude, preferred or coda           */
  char    magtype[MAG_NAME_LEN+1];  /* type of magnitude      */
  float   magwt;    /* magnitude weight (~#stas used in calc) */
  int     nph;      /* number of phases used in the location  */
  float   rms;      /* rms residual of location               */
  long    id;       /* Event ID from binder                   */
  char    idstr[256];  /* Binder's eventid as a char string   */
  char    author[256]; /* Event author as a char string       */
};

#include "shake_dtd.h"
