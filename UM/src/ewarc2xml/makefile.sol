# makefile for ewarc2xml

CFLAGS = -D_REENTRANT -llibc ${GLOBALFLAGS}

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

OBJ = ewarc2xml.o \
      $(L)/logit_mt.o \
      $(L)/chron3.o \
      $(L)/getutil.o \
      $(L)/kom.o \
      $(L)/sleep_ew.o \
      $(L)/threads_ew.o \
      $(L)/time_ew.o \
      $(L)/transport.o \
      $(L)/mem_circ_queue.o \
      $(L)/sema_ew.o \
      $(L)/read_arc.o

ewarc2xml: $(OBJ); \
        cc -o $(B)/ewarc2xml $(OBJ) -lposix4 -lthread -lm

