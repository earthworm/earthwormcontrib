/*****************************************************************************
 *  nq_get_eq.h                                                              *
 *                                                                           *
 *  header file for nq_get_eq.c                                              *
 *****************************************************************************/

/*****************************************************************************
 *  defines                                                                  *
 *****************************************************************************/

#define MAXCHANNELS     1600  /* Maximum number of channels                  */

#define MAX_STADBS        20  /* Maximum number of Station database files    */

#define GDIRSZ           132  /* Size of string for GIF target directory     */
#define STALIST_SIZ      100  /* Size of string for station list file        */
#define MAXLOGO            2  /* Maximum number of Logos                     */

#define TIMEOUT          600  /* Time(sec) after event to wait for data.     */

#define MAX_STR          255  /* Size of strings for arkive records          */


/*****************************************************************************
 *  Define the structure for time records.                                   *
 *****************************************************************************/

typedef struct TStrct {   
    double  Time1600; /* Time (Sec since 1600/01/01 00:00:00.00)             */
    double  Time;     /* Time (Sec since 1970/01/01 00:00:00.00)             */
    int     Year;     /* Year                                                */
    int     Month;    /* Month                                               */
    int     Day;      /* Day                                                 */
    int     Hour;     /* Hour                                                */
    int     Min;      /* Minute                                              */
    double  Sec;      /* Second                                              */
} TStrct;

/*****************************************************************************
 *  Define the structure for Station information.                            *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/

typedef struct StaInfo {       /* A channel information structure            */
    char    Site[6];           /* Site                                       */
    char    Net[5];            /* Net                                        */
    char    Loc[5];            /* Loc                                        */
    int     sernum;            /* Serial Number                              */
    double  logA;              /* Number of data streams for this site       */
    double  Lat;               /* Latitude                                   */
    double  Lon;               /* Longitude                                  */
    double  Elev;              /* Elevation                                  */
    double  Dist;              /* Distance                                   */
    double  Stime;             /* Start time for plot                        */
} StaInfo;

/*****************************************************************************
 *  Define the structure for Channel information.                            *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/

typedef struct ChanInfo {      /* A channel information structure            */
    char    Site[6];           /* Site                                       */
    char    Comp[5];           /* Component                                  */
    char    Net[5];            /* Net                                        */
    char    Loc[5];            /* Loc                                        */
    double  Lat;               /* Latitude                                   */
    double  Lon;               /* Longitude                                  */
    double  Elev;              /* Elevation                                  */
    
} ChanInfo;

/*****************************************************************************
 *  Define the structure for the individual Global thread.                   *
 *  This is the private area the thread needs to keep track                  *
 *  of all those variables unique to itself.                                 *
 *****************************************************************************/

struct Global {
    int     Debug;             /*                                            */
    pid_t   pid;
    char    mod[20];
    int     SecsToWait;    /* Number of seconds to wait before starting plot */

    
    
    
    int     NSCN;              /* Number of SCNs we know about               */
    ChanInfo    Chan[MAXCHANNELS];
    
    int     Nsta;              /* Number of SCNs we know about               */
    StaInfo    Sta[MAXCHANNELS];
    int     NumIndexedSta;
    int     StaIndex[MAXCHANNELS];
    
/* Variables used during the generation of each plot
 ***************************************************/
    double  MaxDistance;       /* Maximum offset distance for traces in this plot. */
    
    double  MinLogA;           /* Minimum LogA for selection of sites  */

    int     nStaDB;            /* number of station databases we know about          */
    char    stationList[MAX_STADBS][STALIST_SIZ];
    
    char    SerNumList[STALIST_SIZ];
    
    char    indir[GDIRSZ];     /* Directory for manual-input arc files               */
    int     arcfileflg;        /* Flag set if we are accepting arc files             */
    
    char    cubedir[GDIRSZ];     /* Directory for manual-input arc files               */
    int     cubefileflg;        /* Flag set if we are accepting arc files             */
    
    char    GifDir[GDIRSZ];    /* Directory for temp files on local machine */
    char    OutDir[GDIRSZ];    /* Directory for output files on local machine */
    long    MaxChannels;       /*                                                    */
    double  MinSize;           /* Minimum size event to report                       */

    char    Site[5];           /*  0- 3 Site                              */
    char    Comp[5];           /* Component                               */
    char    Net[5];            /* Net                                     */
    char    Loc[5];            /* Location                                */
        
    double  EvtLat;            /* Event Latitude                          */
    double  EvtLon;            /* Event Longitude                         */
    double  EvtMag;            /* Event Magnitude                         */
    double  EvtTime;           /* Time of Event (Julian Sec)              */
        
};
typedef struct Global Global;

