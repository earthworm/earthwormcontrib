/***************************************************************************
 * This group of routines is started by the processor manager.             *
 * We're given a copy of the archive message for this event.               *
 *                                                                         *
 *   MainProcess - This is the process controller.                         *
 *       Read config file                                                  *
 *       Read Arkive msg and stuff info in ark structure.                  *
 *       Build table of data of interest                                   *
 *       Retrieve data traces                                              *
 *       Process traces                                                    *
 *       Plot                                                              *
 *       Publish                                                           *
 ***************************************************************************/

#include <platform.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <chron3.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include "mem_circ_queue.h" 
#include <swap.h>
#include <trace_buf.h>
#include <time_ew.h>
#include <decode.h>
/*
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
*/

#include "nq_get_eq.h"

/* Functions in this source file 
 *******************************/
int MainProcess(Global *But, char *net, char *eventid, double Evttime, double lat, double lon, double mag);
short Build_Table(Global *But, double mag, double lat, double lon, double Evttime);
void Sort_Ark (int, double *, int *);

void Decode_Time( double, TStrct *);
void Encode_Time( double *secs, TStrct *Time);
void GetMaxDist(double *dist, double loga, double mag);
double logA0(double dist);
void Get_Sta_Info(Global *);
void Get_SerNum_Info(Global *);
int Find_Sta_Info(Global *But);
short distaz (double lat1, double lon1, double lat2, double lon2, 
                double *rngkm, double *faz, double *baz);

double    sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's        */
                                     /* time 0 and 1970-01-01 00:00:00.0 GMT    */
int     Debug;

/********************************************************************
 *  MainProcess is responsible for shepherding one event through    *
 *  the seismogram plotting string.                                 *
 *                                                                  *
 ********************************************************************/

int MainProcess(Global *But, char *net, char *eventid, double Evttime, double lat, double lon, double mag)
{
    char    whoami[50], fname[100], tname[100];
    int     i, j, jj, jjj, k, error, ierr;
    
    /* Build the modname for error messages *
     ****************************************/
    sprintf(But->mod,"nq_get_eq");
    sprintf(whoami, "%s: %s: ", But->mod, "MainProcess");
    
    /* Let's see the message * 
     *************************/
    Debug = But->Debug;
    if (But->Debug >=2) {
        logit("et", "%s startup of event:\n%s %s %9.5f %10.5f %5.1f \n", whoami, net, eventid, lat, lon, mag);
//        return -1;
    }
    
    /* Go get all available information from the arkive msg. 
    ********************************************************/
    if(But->Debug) logit("et", "%s Got the cube msg for %s.\n", whoami, eventid);
    
    if(mag < But->MinSize) {
        if(But->Debug) logit("e", "%s Event %s%s too small (%5.2f)\n", whoami, net, eventid, mag);
        return 1;
    }
        
    Get_Sta_Info(But);
    
    Find_Sta_Info(But);
    
    Get_SerNum_Info(But);
    
    
    /* Build the To-Do list from all available information. 
     ******************************************************/
    error = Build_Table(But, mag, lat, lon, Evttime);
    if(error) {
        if(error>1) logit("e", "%s Error <%hd> building To Do list.\n", whoami, error);
        if(But->NumIndexedSta > 0) {
            if(But->Debug) logit("e", "        There are %d entries in table.\n", But->NumIndexedSta);
        } else {
            if(But->Debug) logit("e", "        There are no entries in table.\n");
            return 1;
        }
    }
    if(But->Debug) logit("e", "%s Got the Station request list.\n", whoami);
    /*  Process the traces by looping thru the To-Do list. 
    ******************************************************/
    sprintf(fname, "%s.txt", eventid);
    
     
    return 0;
}


/********************************************************************
 *    Build_Table is responsible for setting up the ToDo list.      *
 *    Input: station list with (lat,lon) of each                    *
 *           Event lat, lon, mag, EventTime                         *
 *                                                                  *
 ********************************************************************/

short Build_Table(Global *But, double mag, double lat, double lon, double Evttime)
{
    char    whoami[50];
    char    tstring[30], filename[100], tname[100];
    int     i, k, error, isec, ierr, jerr;
    double  offset[MAXCHANNELS];
    double  dist, dlat, dlon, faz, baz, t0, t1, Stime, rv;
    TStrct  Time1;  
	time_t  timenow;     
    FILE    *out;
    
    sprintf(whoami, "%s: %s: ", But->mod, "Build_Table");
    error = 0;
        
    GetMaxDist(&But->MaxDistance, But->MinLogA, mag);
	if(But->Debug) logit("e", "%s Max Distance = %f\n", whoami, But->MaxDistance);
    
    But->NumIndexedSta= 0;
    for(i=0;i<But->Nsta;i++)  But->StaIndex[i] = 0;
    
  /* Build the list of stations from the station list */
	for(i=0;i<But->Nsta;i++) {
		if( But->NumIndexedSta >= MAXCHANNELS ) {
			logit("e", 
				"%s Site table full; cannot load entire file.\n", whoami );
			logit("e", 
				"%s Use <maxsite> command to increase table size; exiting!\n", whoami );
			break;
		} 
		else {
			dlat = But->Sta[i].Lat;
			dlon = But->Sta[i].Lon;
			distaz(lat, lon, dlat, dlon, &dist, &faz, &baz);
			But->Sta[i].logA = mag - 3.0 - log10(dist/100.0) - 0.00301*(dist-100.0);
			if(dist < But->MaxDistance) {
				But->Sta[i].Dist = dist;
			
				rv = (dist > 150.0)? 8.0:6.0;
				Stime = Evttime + dist/rv;   /* Earliest time needed. */
				Stime -= 50.0;                     /* >40 sec pre-event     */
				Decode_Time(Stime, &Time1);
				isec = Time1.Sec;
				Time1.Sec = 10.0*(isec/10);
				Encode_Time( &Stime, &Time1);    /* StartTime modulo 10 */
				
				But->Sta[i].Dist  = dist;
				But->Sta[i].Stime = Stime;
				offset[But->NumIndexedSta] = dist;
				But->StaIndex[But->NumIndexedSta++] = i;
			}
		}
	}
    
    /* Go get the needed info *
     **************************/
    if(But->NumIndexedSta==0) error = 1;
    
    if(But->NumIndexedSta>1) Sort_Ark (But->NumIndexedSta, offset, &But->StaIndex[0]);
   
    if(But->Debug) {
    }
        if(But->NumIndexedSta > 0) {
			logit("e", "%s The following %d requested stations are available:\n Site:  Net:    S/N:      Dist:      logA:\n", 
				  whoami, But->NumIndexedSta);
			for(i=0;i<(But->NumIndexedSta);i++)  {
				k = But->StaIndex[i];
				logit("e", " %s    %s     %d      %f  %f \n", 
					But->Sta[k].Site, But->Sta[k].Net, But->Sta[k].sernum, But->Sta[k].Dist, But->Sta[k].logA);
			}
        } else {
			logit("e", "%s No stations are close enough.\n", whoami);
        }
        logit("e", "%s M: %f\n", whoami, mag);
    
    /* Build the current time string of form 20090520_123456 *
     *********************************************************/
    t0 = time(&timenow);
    Decode_Time( t0, &Time1);
    isec = Time1.Sec;
    sprintf(tstring, "%.4d%.02d%.02d_%.02d%.02d%.02d", Time1.Year, Time1.Month, Time1.Day, Time1.Hour, Time1.Min, isec);
    
	for(i=0;i<(But->NumIndexedSta);i++)  {
		k = But->StaIndex[i];
		/* Build the filename for each request. e.g. REQ_100169_20090520_123456.txt *
		 **************************************************************************/
		if(But->Sta[k].sernum > 100000) {
			sprintf(filename, "%sREQ_%d_%s.txt", But->GifDir, But->Sta[k].sernum, tstring);
			out = fopen(filename, "wb");
			if(out == 0L) {
				logit("e", "%s Unable to open Request File: %s\n", whoami, filename);    
			} else {
			
			/* Build the string for each file. e.g. GETEVT 2009-05-20 12:00:00 3600 *
			 ************************************************************************/
				Decode_Time( But->Sta[k].Stime, &Time1);
				isec = Time1.Sec;
				fprintf(out, "GETEVT %.4d-%.02d-%.02d %.02d:%.02d:%.02d 240\n", Time1.Year, Time1.Month, Time1.Day, Time1.Hour, Time1.Min, isec);
				fclose(out);
				sprintf(tname, "%sREQ_%d_%s.txt", But->OutDir, But->Sta[k].sernum, tstring);
				ierr = rename(filename, tname); 
				/* The following silliness is necessary to be Windows compatible */ 
				if( ierr != 0 ) {
					if(But->Debug) logit( "e", "Error moving file %s to %s; ierr = %d\n", filename, tname, ierr );
					if( remove( tname ) != 0 ) {
						logit("e","error deleting file %s\n", tname);
					} else  {
						if(But->Debug) logit("e","deleted file %s.\n", tname);
						jerr = rename( filename, tname );
						if( jerr != 0 ) {
							logit( "e", "error moving file %s to %s; ierr = %d\n", filename, tname, ierr );
						} else {
							if(But->Debug) logit("e","%s moved to %s\n", filename, tname );
						}
					}
				} else {
					if(But->Debug >= 2) logit("e","%s moved to %s\n", filename, tname );
				}
			}
        }
		
	}
    return error;
}

/*************************************************************************
 *   void Sort_Ark (n,dist,indx)                                         *
 *   --THIS is A VERSION OF THE HEAPSORT SUBROUTINE FROM THE NUMERICAL   *
 *     RECIPES BOOK. dist is REARRANGED IN ASCENDING ORDER &             *
 *     indx is PUT IN THE SAME ORDER AS dist.                            *
 *   long        n       !THE NUMBER OF VALUES OF dist TO BE SORTED      *
 *   FLOAT       dist(n) !SORT THIS ARRAY IN ASCENDING ORDER             *
 *   long        indx(n) !PASSIVELY REARRANGE THIS ARRAY TOO             *
 *************************************************************************/

void Sort_Ark (int n, double *dist, int *indx)
{
    long    indxh, ii, ir, i, j;
    double  disth;
    
    ii = n/2+1;    ir = n;
    
    while(1) {    
        if (ii > 1) {
            ii -= 1;
            disth      = dist[ii-1];   indxh      = indx[ii-1];
        }
        else {
            disth      = dist[ir-1];   indxh      = indx[ir-1];
            dist[ir-1] = dist[0];      indx[ir-1] = indx[0];
            ir -= 1;
            if(ir == 1) {
                dist[0] = disth;       indx[0]    = indxh;
                return;
            }
        }
        i = ii;   j = ii+ii;
        
        while (j <= ir) {
            if (j < ir && dist[j-1] < dist[j]) j += 1;
            
            if (disth < dist[j-1]) {
                dist[i-1] = dist[j-1]; indx[i-1] = indx[j-1];
                i = j;   j = j+j;
            }
            else    j = ir+1;
        }
        dist[i-1] = disth;   indx[i-1] = indxh;
    }
}


/**********************************************************************
 * Decode_Time : Decode time from seconds since 1970                  *
 *                                                                    *
 **********************************************************************/
void Decode_Time( double secs, TStrct *Time)
{
    struct Greg  g;
    long    minute;
    double  sex;

    Time->Time = secs;
    secs += sec1970;
    Time->Time1600 = secs;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    Time->Year  = g.year;
    Time->Month = g.month;
    Time->Day   = g.day;
    Time->Hour  = g.hour;
    Time->Min   = g.minute;
    Time->Sec   = sex;
}


/**********************************************************************
 * Encode_Time : Encode time to seconds since 1970                    *
 *                                                                    *
 **********************************************************************/
void Encode_Time( double *secs, TStrct *Time)
{
    struct Greg    g;

    g.year   = Time->Year;
    g.month  = Time->Month;
    g.day    = Time->Day;
    g.hour   = Time->Hour;
    g.minute = Time->Min;
    *secs    = 60.0 * (double) julmin(&g) + Time->Sec - sec1970;
}


/********************************************************************
 * GetMaxDist                                                       *
 *   Returns the distance at which we expect to see an amplitude    *
 *   of loga for an event with magnitude mag.                       *
 *  Bakun&Joyner, 1984, BSSA, 74, 1827-1843.                        *
 *  (for Central California)                                        *
 *  -logA = 1.000*log(r/100) + 0.00301*(r-100) + 3.0;  0<= r <=400  * 
 *          r = sqrt(dist^2+depth^2)                                *
 *                                                                  *
 *  logA  = log10(Amp);                                             *
 *  Ml100 = logA - (-logA0);                                        *
 *                                                                  *
 ********************************************************************/

void GetMaxDist(double *dist, double loga, double mag)
{
    char    whoami[90];
    double  rm, rmin, rmax, eta, fl, fu, fm, fac, logA0min, logA0max;
    
    sprintf(whoami, " %s: ", "GetMaxDist");

	rmin = 0.0;
	rmax = 1000.0;
	eta  = 0.1;
	fac  = mag - loga;
	logA0min = logA0(rmin);
	logA0max = logA0(rmax);
	fl = logA0min - fac;
	fu = logA0max - fac;
	if(fl*fu > 0.0) {
		logit("e", "%s Error setting up iteration.\n mag: %f loga: %f fac: %f logA0min: %f logA0max: %f  \n", 
		       whoami, mag, loga, fac, logA0min, logA0max);
		return;
	}
	
	do{
		rm = (rmin+rmax)/2.0;
		fm = logA0(rm) - fac;
		if(fm*fl > 0.0) {
			rmin = rm;
			fl = fm;
		} else {
			rmax = rm;
			fu = rm;
		}
	} while(fabs(fm) > eta);
	*dist = rm;
}


/********************************************************************
 * logA0                                                            *
 *   Solves the attenuation function at distance dist with an       *
 *   amplitude of loga for an event with magnitude mag.             *
 ********************************************************************/

double logA0(double dist)
{
    double  logA;
    
/*  logA = log10(dist/100.0) + 0.00301*(dist-100.0) + 3.0;  */
    logA = log10(dist) + 0.00301*dist + 0.699;
        
    return(logA);
}


/****************************************************************************
 *  Get_Sta_Info(Global *But);                                              *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Sta_Info(Global *But)
{
    char    whoami[50], *com, *str;
    int     i, j, k, nfiles, success, type, sensor, units, idum;
    double  dlat, mlat, dlon, mlon, gain, sens, ssens;

    sprintf(whoami, "%s: %s: ", But->mod, "Get_Sta_Info");
    
	if(But->Debug) logit("e", "%s %d database files.\n", whoami, But->nStaDB );
    But->NSCN = 0;
    for(k=0;k<But->nStaDB;k++) {
            /* Open the main station file
             ****************************/
        nfiles = k_open( But->stationList[k] );
	if(But->Debug) logit("e", "%s nfiles = %d for file %s.\n", whoami, nfiles, But->stationList[k] );
        if(nfiles == 0) {
            logit("e", "%s Error opening command file <%s>; exiting!\n", whoami, But->stationList[k] );
            exit( -1 );
        }

            /* Process all command files
             ***************************/
        while(nfiles > 0) {  /* While there are command files open */
            while(k_rd())  {      /* Read next line from active file  */
                com = k_str();         /* Get the first token from line */

                    /* Ignore blank lines & comments
                     *******************************/
                if( !com )           continue;
                if( com[0] == '#' )  continue;

                    /* Open a nested configuration file
                     **********************************/
                if( com[0] == '@' ) {
                    success = nfiles+1;
                    nfiles  = k_open(&com[1]);
                    if ( nfiles != success ) {
                        logit("e", "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                        exit( -1 );
                    }
                    continue;
                }

                /* Process anything else as a channel descriptor
                 ***********************************************/

                if( But->NSCN >= MAXCHANNELS ) {
                    logit("e", "%s Too many channel entries in <%s>", 
                             whoami, But->stationList[k] );
                    logit("e", "; max=%d; exiting!\n", (int) MAXCHANNELS );
                    exit( -1 );
                }
                j = But->NSCN;
                
                    /* S C N */
                strncpy( But->Chan[j].Site, com,  6);
                str = k_str();
                if(str) strncpy( But->Chan[j].Net,  str,  2);
                str = k_str();
                if(str) strncpy( But->Chan[j].Comp, str, 3);
                str = k_str();
                if(str) strncpy( But->Chan[j].Loc, str, 3);
                for(i=0;i<6;i++) if(But->Chan[j].Site[i]==' ') But->Chan[j].Site[i] = 0;
                for(i=0;i<2;i++) if(But->Chan[j].Net[i] ==' ') But->Chan[j].Net[i]  = 0;
                for(i=0;i<3;i++) if(But->Chan[j].Comp[i]==' ') But->Chan[j].Comp[i] = 0;
                for(i=0;i<2;i++) if(But->Chan[j].Loc[i] ==' ') But->Chan[j].Loc[i]  = 0;
                But->Chan[j].Comp[3] = But->Chan[j].Net[2] = But->Chan[j].Site[5] = But->Chan[j].Loc[2] = 0;

                    /* Lat Lon Elev */
                But->Chan[j].Lat  = k_val();
                But->Chan[j].Lon  = k_val();
                But->Chan[j].Elev = k_val();
                                
                k_int();
                k_val();
                k_val();
                k_int();
                k_int();
                k_val();
                k_val();
                k_int();
                        
                               
                if (k_err()) {
                    logit("e", "%s Error decoding line in station file\n%s\n  exiting!\n", whoami, k_get() );
                    exit( -1 );
                }
                k_str();
                    
                k_str();
                    
                But->NSCN++;
            }
            nfiles = k_close();
        }
    }
	if(But->Debug) logit("e", "%s %d channels in local database. nfiles = %d\n", whoami, But->NSCN, nfiles );
}

/****************************************************************************
 *  Get_SerNum_Info(Global *But);                                           *
 *  Retrieve the information about serial numbers of network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_SerNum_Info(Global *But)
{
    char    whoami[50], *com, *str, string[100];
    char    Site[6], Net[6], Loc[6];
    int     sernum, numSN, nfiles;
    int     i, j, k;
    FILE    *in;

    sprintf(whoami, "%s: %s: ", But->mod, "Get_SerNum_Info");
    
    numSN = 0;
            /* Open the main station file
             ****************************/
	in = fopen(But->SerNumList, "rw" );
	if(in == 0L) {
		logit("e", "%s Unable to open S/N File: %s\n", whoami, But->SerNumList);    
	} else {

		while(fgets(string, 100, in) ) {      /* Read next line from active file  */
			
				/* Ignore comments
				 *******************************/
			if( !string )           continue;
			if( string[0] == '#' )  continue;
	
			/* Process anything else as a channel descriptor
			 ***********************************************/
	
			if( numSN >= MAXCHANNELS ) {
				logit("e", "%s Too many channel entries in <%s>", 
						 whoami, But->SerNumList );
				logit("e", "; max=%d; exiting!\n", (int) MAXCHANNELS );
				exit( -1 );
			}
			
				/* S C N */
			sscanf(string, "%d %d %s %s %s", &j, &sernum, Site, Net, Loc);
				
			for(i=0;i<6;i++) if(Site[i]==' ') Site[i] = 0;
			for(i=0;i<2;i++) if(Net[i] ==' ') Net[i]  = 0;
			for(i=0;i<2;i++) if(Loc[i] ==' ') Loc[i]  = 0;
						   
			for(j=0;j<But->Nsta;j++) {
				if(strcmp(Site, But->Sta[j].Site )==0 && 
				   strcmp(Net,  But->Sta[j].Net  )==0 &&
				   strcmp(Loc,  But->Sta[j].Loc  )==0) {
				   But->Sta[j].sernum = sernum;
				}
			}
				
			numSN++;
		}
		fclose(in);
	}
    
	if(But->Debug) logit("e", "%s %d channels in local database.\n", whoami, numSN );
}


void xGet_SerNum_Info(Global *But)
{
    char    whoami[50], *com, *str, string[100];
    char    Site[6], Net[6], Loc[6];
    int     sernum, numSN, nfiles;
    int     i, j, k;

    sprintf(whoami, "%s: %s: ", But->mod, "Get_SerNum_Info");
    
    numSN = 0;
            /* Open the main station file
             ****************************/
	strcpy(string, But->SerNumList );
	nfiles = k_open( string );
	if(But->Debug) logit("e", "%s nfiles = %d for file %s.\n", whoami, nfiles, string );
	if(nfiles == 0) {
		logit("e", "%s Error opening command file <%s>; exiting!\n", whoami, But->SerNumList );
	/*	exit( -1 );    */
	}

	i = 0;
	while(k_rd())  {      /* Read next line from active file  */
		com = k_str();         /* Get the first token from line */
		if(++i > 1000) break;
		
			/* Ignore blank lines & comments
			 *******************************/
		if( !com )           continue;
		if( com[0] == '#' )  continue;

		/* Process anything else as a channel descriptor
		 ***********************************************/

		if( numSN >= MAXCHANNELS ) {
			logit("e", "%s Too many channel entries in <%s>", 
					 whoami, But->SerNumList );
			logit("e", "; max=%d; exiting!\n", (int) MAXCHANNELS );
			exit( -1 );
		}
		
			/* S C N */
		sernum  = k_int();
		str = k_str();
		strncpy( Site, str,  6);
		str = k_str();
		if(str) strncpy( Net,  str,  2);
		str = k_str();
		if(str) strncpy( Loc,  str,  2);
		for(i=0;i<6;i++) if(Site[i]==' ') Site[i] = 0;
		for(i=0;i<2;i++) if(Net[i] ==' ') Net[i]  = 0;
		for(i=0;i<2;i++) if(Loc[i] ==' ') Loc[i]  = 0;
					   
		if (k_err()) {
			logit("e", "%s Error decoding line in station file\n%s\n  exiting!\n", whoami, k_get() );
			exit( -1 );
		}
	    for(j=0;j<But->Nsta;j++) {
	        if(strcmp(Site, But->Sta[j].Site )==0 && 
	           strcmp(Net,  But->Sta[j].Net ) ==0 &&
	           strcmp(Loc,  But->Sta[j].Loc ) ==0) {
			   But->Sta[j].sernum = sernum;
	        }
	    }
			
		numSN++;
	}
	nfiles = k_close();
    
	if(But->Debug) logit("e", "%s %d stations and S/Ns in local database.\n", whoami, numSN );
}


/*************************************************************************
 *  Find_Sta_Info(Global *But);                                          *
 *  Retrieve all the information available about station i               *
 *  and put it into an internal structure for reference.                 *
 *  This should eventually be a call to the database; for now we must    *
 *  supply an ascii file with all the info.                              *
 *************************************************************************/
int Find_Sta_Info(Global *But)
{
    char    whoami[90];
    int     i, j, jj;

    sprintf(whoami, "%s: %s: ", But->mod, "Find_Sta_Info");
    
    if(But->Debug) logit("e", "%s There are  %d channels listed. \n", whoami, But->NSCN); 
    But->Nsta = 0;
    for(i=0;i<But->NSCN;i++) {
	    jj = But->Nsta;
	    for(j=0;j<But->Nsta;j++) {
	        if(strcmp(But->Chan[i].Site, But->Sta[j].Site )==0 && 
	           strcmp(But->Chan[i].Net,  But->Sta[j].Net )==0  &&
	           strcmp(But->Chan[i].Loc,  But->Sta[j].Loc )==0) {
			    jj = j;
	        }
	    }
        if(jj == But->Nsta) {
	        strcpy(But->Sta[jj].Site,     But->Chan[i].Site);
	        strcpy(But->Sta[jj].Net,      But->Chan[i].Net);
	        strcpy(But->Sta[jj].Loc,      But->Chan[i].Loc);
	        But->Sta[jj].Lat  = But->Chan[i].Lat;
	        But->Sta[jj].Lon  = But->Chan[i].Lon;
	        But->Sta[jj].Elev = But->Chan[i].Elev;
	        But->Nsta += 1;
        }
    }
    
    if(But->Debug) {
		logit("e", "%s There are  %d stations. \n", whoami, But->Nsta); 
	    for(j=0;j<But->Nsta;j++) {
			logit("e", "%s %s %s \n", But->Sta[j].Site, But->Sta[j].Net, But->Sta[j].Loc ); 
	    }
    }
    
    return 0;
}


/************************************************************************

    Subroutine distaz (lat1,lon1,lat2,lon2,RNGKM,FAZ,BAZ)
    
c--  COMPUTES RANGE AND AZIMUTHS (FORWARD AND BACK) BETWEEN TWO POINTS.
c--  OPERATOR CHOOSES BETWEEN 3 FIRST ORDER ELLIPSOIDAL MODELS OF THE
c--  EARTH AS DEFINED BY THE MAJOR RADIUS AND FLATTENING.
c--  THE PROGRAM UTILIZES THE SODANO AND ROBINSON (1963) DIRECT SOLUTION
c--  OF GEODESICS (ARMY MAP SERVICE, TECH REP #7, SECTION IV).
c--  (TERMS ARE GIVEN TO ORDER ECCENTRICITY TO THE FOURTH POWER.)
c--  ACCURACY FOR VERY LONG GEODESICS:
c--          DISTANCE < +/-  1 METER
c--          AZIMUTH  < +/- .01 SEC
c
    Ellipsoid            Major Radius    Minor Radius    Flattening
1  Fischer 1960            6378166.0        6356784.28      298.30
2  Clarke1866              6378206.4        6356583.8       294.98
3  S. Am 1967              6378160.0        6356774.72      298.25
4  Hayford Intl 1910       6378388.0        6356911.94613   297.00
5  WGS 1972                6378135.0        6356750.519915  298.26
6  Bessel 1841             6377397.155      6356078.96284   299.1528
7  Everest 1830            6377276.3452     6356075.4133    300.8017
8  Airy                    6377563.396      6356256.81      299.325
9  Hough 1960              6378270.0        6356794.343479  297.00
10 Fischer 1968            6378150.0        6356768.337303  298.30
11 Clarke1880              6378249.145      6356514.86955   293.465
12 Fischer 1960            6378155.0        6356773.32      298.30
13 Intl Astr Union         6378160.0        6378160.0       298.25
14 Krasovsky               6378245.0        6356863.0188    298.30
15 WGS 1984                6378137.0        6356752.31      298.257223563
16 Aust Natl               6378160.0        6356774.719     298.25
17 GRS80                   6378137.0        63567552.31414  298.2572
18 Helmert                 6378200.0        6356818.17      298.30
19 Mod. Airy               6377341.89       6356036.143     299.325
20 Mod. Everest            6377304.063      6356103.039     300.8017
21 Mercury 1960            6378166.0        6356784.283666  298.30
22 S.E. Asia               6378155.0        6356773.3205    298.2305
23 Sphere                  6370997.0        6370997.0         0.0
24 Walbeck                 6376896.0        6355834.8467    302.78
25 WGS 1966                6378145.0        6356759.769356  298.25

************************************************************************/

short distaz (double lat1, double lon1, double lat2, double lon2, 
                double *rngkm, double *faz, double *baz)
{
    double    pi, rd, f, fsq;
    double    a3, a5, a6, a10, a16, a17, a18, a19, a20;
    double    a21, a22, a22t1, a22t2, a22t3, a23, a24, a25, a27, a28, a30;
    double    a31, a32, a33, a34, a35, a36, a38, a39, a40;
    double    a41, a43;
    double    a50, a51, a52, a54, a55, a57, a58, a62;
    double    dlon, alph[2];
    double    p11, p12, p13, p14, p15, p16, p17, p18;
    double    p21, p22, p23, p24, p25, p26, p27, p28, pd1, pd2;
    double    ang45;
    short     k;
    double    RAD, MRAD, FINV;
    
    RAD  = 6378206.4;
    MRAD = 6356583.8;
    FINV = 294.98;
    ang45 = atan(1.0);
    
    pi = 4.0*atan(1.0);
    rd = pi/180.0;

    a5 = RAD;
    a3 = FINV;
    
    if(a3==0.0) {
        a6 = MRAD;                    /*    minor radius of ellipsoid    */
        f = fsq = a10 = 0.0;
    }
    else {
        a6 = (a3-1.0)*a5/a3;          /*    minor radius of ellipsoid    */
        f = 1.0/a3;                   /*    flattening    */
        fsq = 1.0/(a3*a3);
        a10 = (a5*a5-a6*a6)/(a5*a5);  /*    eccentricity squared    */
    }
    
    /*    Following definitions are from Sodano algorithm for long geodesics    */
    a50 = 1.0 + f + fsq;
    a51 = f + fsq;
    a52 = fsq/ 2.0;
    a54 = fsq/16.0;
    a55 = fsq/ 8.0;
    a57 = fsq* 1.25;
    a58 = fsq/ 4.0;
    
/*     THIS IS THE CALCULATION LOOP. */
    
    p11 = lat1*rd;
    p12 = lon1*rd;
    p21 = lat2*rd;
    p22 = lon2*rd;
    *rngkm = *faz = *baz = 0.0;
    if( (lat1==lat2) && (lon1==lon2))    return(0);
    
    /*    Make sure points are not exactly on the equator    */
    if(p11 == 0.0) p11 = 0.000001;
    if(p21 == 0.0) p21 = 0.000001;
    
    /*    Make sure points are not exactly on the same meridian    */
    if(p12 == p22)             p22 += 0.0000000001;
    if(fabs(p12-p22) == pi)    p22 += 0.0000000001;
    
    /*    Correct latitudes for flattening    */
    p13 = sin(p11);
    p14 = cos(p11);
    p15 = p13/p14;
    p18 = p15*(1.0-f);
    a62 = atan(p18);
    p16 = sin(a62);
    p17 = cos(a62);
    
    p23 = sin(p21);
    p24 = cos(p21);
    p25 = p23/p24;
    p28 = p25*(1.0-f);
    a62 = atan(p28);
    p26 = sin(a62);
    p27 = cos(a62);
    
    dlon = p22-p12;
    a16 = fabs(dlon);
    
    /*    Difference in longitude to minimum (<pi)    */
    if(a16 >= pi) a16 = 2.0*pi - a16;

    /*    Compute range (a35)    */
    if(a16==0.0)     {a17 = 0.0;         a18 = 1.0;}
    else             {a17 = sin(a16);    a18 = cos(a16);}
    a19 = p16*p26;
    a20 = p17*p27;
    a21 = a19 + a20*a18;
    
    a40 = a41 = a43 = 0.0;
    
    for(k=0; k<2; k++) {
        a22t1 = (a17*p27)*(a17*p27);
        a22t2 = p26*p17 - p16*p27*a18;
        a22   = sqrt(a22t1 + (a22t2*a22t2));
        if(a22 == 0.0) return(0);
        a22t3 = a22*a21;
        a23 = (a20*a17)/a22;
        a24 = 1.0 - a23*a23;
        a25 = asin(a22);
        if(a21 < 0.0) a25 = pi-a25;
        a27 = (a25*a25)/a22;
        a28 = a21*a27;
        if(k==0) {
            a30 = (a50*a25) + a19*(a51*a22-a52*a27);
            a31 = 0.5*a24*(fsq*a28 - a51*(a25 + a22t3));
            a32 = a19*a19*a52*a22t3;
            a33 = a24*a24*(a54*(a25 + a22t3) - a52*a28 - a55*a22t3*(a21*a21));
            a34 = a19*a24*a52*(a27 + a22t3*a21);
            a35 = (a30 + a31 - a32 + a33 + a34)*a6;
            *rngkm = a35/1000.0;
        }

    /*    Compute azimuths    */
        a36 = (a51*a25) - a19*(a52*a22 + fsq*a27) + a24*(a58*a22t3 - a57*a25 + fsq*a28);
        a38 = a36*a23 + a16;
        a39 = sin(a38);
        a40 = cos(a38);

        if(a39*p27 == 0.0) a43 = 0.0;
        else {
          a41 = (p26*p17 - a40*p16*p27)/(a39*p27);
          if(a41 == 0.0)    a43 = pi/2.0;
          else                a43 = atan(1.0/a41);
        }

        alph[k] = a43;
        if((dlon <= -pi) || ((dlon >= 0.0) && (dlon < pi))) {
          if(a41 >= 0.0)       alph[k] = alph[k]-pi;
        }
        else {
          if(a41 >= 0.0)       alph[k] = pi - alph[k];
          else                 alph[k] = 2.0*pi - alph[k];
        }
        if(alph[k] >= pi)      alph[k] = alph[k] - pi;
        if(alph[k] <  pi)      alph[k] = alph[k] + pi;
        if(alph[k] <  0.0)     alph[k] = alph[k] + 2.0*pi;
        if(alph[k] >= 2.0*pi)  alph[k] = alph[k] - 2.0*pi;
        alph[k] = alph[k]/rd;
        pd1 = p16;
        pd2 = p17;
        p16 = p26;
        p17 = p27;
        p26 = pd1;
        p27 = pd2;
        dlon = -dlon;
    }

    *faz = alph[0];
    *baz = alph[1];
    while (*faz >= 360.0) *faz = *faz - 360.0;
    while (*baz >= 360.0) *baz = *baz - 360.0;
    
    return(0);
}



