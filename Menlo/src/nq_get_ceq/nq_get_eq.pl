#!/usr/local/bin/perl

########################################################################
# This PerlScript manages the LastEQ system for creating custom
# plots of the most recent event recorded by a station for the web.  
# Virtually everything needed to customize the installation is
# contained in the next several lines.
#
#                                  3/7/2008   Jim Luetgert
########################################################################

$logo    = "/waveforms/smusgs.gif";
$altlogo = "USGS";
$netname = "Northern California Seismic Network";
$self    = "/cgi-bin/lasteq.pl";
$datadir = "/waveforms/lasteq";
$gifdir  = "/home/html/ncsn/waveforms/lasteq";

$pagecolor   = "#ffffcc";          # Colors of various page elements
$pagecolor   = "#eeeeff";          # Colors of various page elements
$bannercolor = "#669966";
$bannertxtcolor = "#ffffff";
$recenteqslist = "http://quake.wr.usgs.gov/recenteqs/Quakes/quakes0.html";
$recenteqs     = "http://quake.wr.usgs.gov/recenteqs/Quakes/nc";

# Common links for page headers
$mouseout = "onMouseOut=\"window.status=''; return true;\"";
$tag0 = " ";
$tag1 = "<br>";
$tag2 = " <A HREF=\"$self\" onMouseOver=\"window.status='Select From Available Data'; return true;\"    $mouseout> Select a Station </A>  |  ";
$onclick = "onClick=\"var new_window = window.open('$recenteqslist','listwindow', 'width=400,height=400,resizable,scrollbars'); new_window.focus();\"";
$tag3 = " <A HREF=\"#\" onMouseOver=\"window.status='Show List of Recent Earthquakes'; return true;\"    $mouseout $onclick> List of Recent Earthquakes </A>   ";

# Get the lists of available sites
# Of Form: JSF_NC.gif

    $list = `ls $gifdir *.gif`;
    @list = split (' ',$list);
    @site = ();
    $numsites = 0;
    
    foreach $item (@list) {
        ($sitename, $suffix) = split (/\./, $item);
        if($suffix eq "gif") {
            $flag = 1;
            foreach $name (@site) {
                if($sitename eq $name) { $flag = 0; }
            }
            if($flag) {  @site = (@site,$sitename); $numsites = $numsites + 1; }
        }
    }

# Check the query string for further instructions.

    $query_string = $ENV{'QUERY_STRING'};

	if ( ! defined($query_string) || $query_string eq "") {
		&select_sta;
	}
	else {
		($querytype,$qvalue) = split ('&',$query_string);
		if($querytype eq 3) { &show_single_panel;  }
	}

exit;


########################################################################
# Display a trace snippet for one site
# querytype = 3
# $qname  is the name of site 
########################################################################
sub show_single_panel {

    $sizeof_line = $ENV{'CONTENT_LENGTH'};
    read (STDIN, $line, $sizeof_line);
    ($field_name, $qname) = split (/=/, $line);

    ($sta,$net) = split ('_',$qname);
	$banner1 = "$netname";
	$banner2 = "$sta $net";

    $currentitem = "$gifdir/$qname.txt";
    $dothtml = ".html";
    if(open(TXTIN, $currentitem)) {
    	while($event = <TXTIN>) {
    		chop $event;
    		$thiseq = "$recenteqs$event$dothtml";
			$onclick = "onClick=\"var new_window = window.open('$thiseq','listwindow', 'width=800,height=710,resizable,scrollbars'); new_window.focus();\"";
			$tag1 = " <A HREF=\"#\" onMouseOver=\"window.status='Show Info About this Earthquake'; return true;\"    $mouseout $onclick> Info About this Earthquake </A> |  ";
    	}
    	close TXTIN;
    }
    
    &Build_Banner;
 
    $currentitem = "$qname.gif";
   
    print STDOUT "<center><td>";
    print STDOUT "<A name=\"anchor1\"   \n>";
    print STDOUT "<IMG SRC=\"$datadir/$currentitem\" >  \n";
    print STDOUT "</a></td></center> \n ";
    
    print STDOUT "  </body> </html> \n";

return;

}

########################################################################
# Select station to display
# querytype = 5
########################################################################
sub select_sta {

    $banner1 = $netname;
    $banner2 = "Last EQ at a Station";

    $tag1 = " ";
    $tag2 = " ";

    &Build_Banner;
    
print STDOUT <<EOT;
    <p><font size="3"><b>Select a Station, then Submit it for a view of the most recent event.
     <form method="post" action="$self?3&NULL" name="chan_form">
       <select name="chan_sel">
EOT

    if($numsites > 0) {
        foreach $item (@site) {
            print STDOUT " <option value=\"$item\">$item</option> \n";
        }
        print STDOUT " </select> <p> <br> \n";
    }
    else {
        print STDOUT " </select> <p> <br> \n";
        print "No Data Available","<br>";
    }
    
    print STDOUT " <input type=submit value=\"Submit Station\"> \n";
    print STDOUT " </form> </center> \n";
    print STDOUT " </BODY></HTML> \n";

return;
}

########################################################################
# Build the generic banner for a page.
########################################################################
sub Build_Banner {

print STDOUT <<EOT;
Content-type: text/html

    <HTML>
    <HEAD><TITLE>
    $netname - Last EQ
    </TITLE></HEAD>
    <BODY BGCOLOR=$pagecolor TEXT=#000000 vlink=purple link=blue>

    <img align=left src="$logo" alt="$altlogo"> <br>

    <dl></dl>

    <table width="75%" border="1" align="center" bgcolor=$bannercolor bordercolordark="#000000">
      <tr bgcolor=$bannercolor> 
        <td height="47"> 
          <center>
            <font face="Times New Roman, Times, serif" size="6" color=$bannertxtcolor>
                $banner1
            </font> </center>
        </td>
      </tr>
      <tr bgcolor=$bannercolor> 
        <td height="34"> 
          <center>
            <font face="Arial"><font size="4" color=$bannertxtcolor face="Times New Roman, Times, serif">
                $banner2
            </font></font> </center>
        </td>
      </tr>
    </table>

    <center><h4>
    $tag0 $tag1 $tag2 $tag3
    </center>

    <P> <HR> <P>
EOT

return;
}

########################################################################
