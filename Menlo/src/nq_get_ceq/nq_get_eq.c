/*************************************************************************
 *      nq_get_eq.c:
 * Takes cube messages from the ring, buffers them in an in-memory fifo; 
 * pulls them out, and starts data request processing for each message.
 *
 * Main:
 * Starts the Heartbeat thread. 
 * Pulls messages files.  
 * It calls processing routines to do all the seismology.
 * 
 * Jim 02/11/10
**************************************************************************/

#include <platform.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include <decode.h>
#include <kom.h>
#include <transport.h>
#include "mem_circ_queue.h" 

#include "nq_get_eq.h" 

#define MAXN              30  /* Maximum number of networks to consider  */

/* Functions in this source file
 *******************************/
thr_ret Heartbeat( void * );
void config_me(Global *But,  char * ); /* reads configuration (.d) file via Carl's routines  */
void ewmod_status( unsigned char, short, char *); /* sends heartbeats and errors into ring   */
void lookup_ew ( void );     /* Goes from symbolic names to numeric values, via earthworm.h  */

void GetMaxDist(double *dist, double loga, double mag);

/* Shared memory
 ***************/
static  SHM_INFO  InRegion;              /* public shared memory for receiving arkive messages */

/* Things to lookup in the earthworm.h table with getutil.c functions
 ********************************************************************/
static long          InRingKey;         /* key of transport ring for input   */
static long          OutRingKey;        /* key of transport ring for output  */
static unsigned char InstId;            /* local installation id             */
static unsigned char MyModId;           /* our module id                     */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;

/* Things to read from configuration file
 ****************************************/
static char InRingName[20];             /* name of transport ring for i/o          */
static char OutRingName[20];            /* name of transport ring for output       */
static char MyModuleId[20];             /* module id for this module               */
static int  LogSwitch;                  /* 0 if no logging should be done to disk  */
static time_t HeartBeatInterval = 5;    /* seconds between heartbeats              */
static int  Debugger;                   /* debug flag                              */
Global      Btlr;                       /* Private area for the threads            */

pid_t MyPid;              /* Our own pid, sent with heartbeat for restart purposes */

/* Variables for talking to statmgr
 **********************************/
char      Text[150];

/* Error words used by nq_get_eq
 *****************************/
#define   ERR_MISSMSG       0
#define   ERR_TOOBIG        1
#define   ERR_NOTRACK       2
#define   ERR_INTERNAL      3
#define   ERR_QUEUE         4

/* Server thread stuff
**********************/
#define THREAD_STACK  8096
#define THREAD_STACK_SIZE 8192

int MainProcess(Global *But, char *net, char *eventid, double Evttime, double lat, double lon, double mag);
void ProcessCUBEFile( void);
void Encode_Time( double *secs, TStrct *Time);

/* Other globals
 ***************/
char   progname[] = "nq_get_eq";
char  Netcode[MAXN][20];      /* Network to watch for messages         */
int   Nnets;                  /* Number of networks to accept          */

time_t MyLastInternalBeat;      /* time of last heartbeat into the local Earthworm ring    */
unsigned  TidHeart;             /* thread id. was type thread_t on Solaris! */

/**********************************************************************************
 *  main( int argc, char **argv )                                                 *
 **********************************************************************************/

main( int argc, char **argv )
{
    char        eventid[20], fname[100], string[500], subname[] = "Main";
    int         i, j, resA, waitA, ret;
    long        recsizeA;   /* size of retrieved message */
    MSG_LOGO    reclogoA;   /* logo of retrieved message */
    double      magt, Dist;
    float       mag, lat, lon;
    FILE        *fp;

        /* Check command line arguments
         ******************************/
    if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", progname );
        exit( 0 );
    }
    Debugger = 0;

    /* Get our own Pid for restart purposes
    ***************************************/
    Btlr.pid = MyPid = getpid();
    if( MyPid == -1 ) {
      fprintf( stderr,"%s: Cannot get pid. Exiting.\n", progname);
      return -1;
    }

        /* Read the configuration file(s)
         ********************************/
    config_me(&Btlr, argv[1] );

        /* Look up important info from earthworm.h tables
         ************************************************/
    lookup_ew();

        /* Initialize name of log-file & open it
         ***************************************/
    logit_init( argv[1], (short) MyModId, 512, LogSwitch );
    logit( "" , "%s: %s: Read command file <%s>\n",  progname, subname, argv[1] );
    
        /* DEBUG: dump variables to stdout
        **********************************/
    if(LogSwitch) {
        logit("","%s: %s: MyModuleId:  %s \n",   progname, subname, MyModuleId);
        logit("","%s: %s: InRingName:  %s \n",   progname, subname, InRingName);
    }

        /* Create a Mutex to control access to queue
        ********************************************/
    CreateMutex_ew();
    if(Debugger) logit("","%s: %s: Created Mutex\n",   progname, subname);


        /* Attach to public HYPO shared memory ring
         ******************************************/
    tport_attach( &InRegion, InRingKey );
    if (LogSwitch) logit( "", "%s: %s: Attached to public memory region <%s>: %ld.\n", 
                              progname, subname, InRingName, InRegion.key );
#ifdef _SOLARIS                    /* SOLARIS ONLY:                         */
    if (Debugger) logit( "e", "%s: %s: Attached to public memory region <%s> key: %ld mid: %ld sid: %ld.\n", 
                              progname, subname, InRingName, InRegion.key, InRegion.mid, InRegion.sid );
    if (Debugger) logit( "e", "%s: %s: nbytes: %ld keymax: %ld keyin: %ld keyold: %ld flag: %d.\n", 
                              progname, subname, InRegion.addr->nbytes, 
                              InRegion.addr->keymax, InRegion.addr->keyin, 
                              InRegion.addr->keyold, InRegion.addr->flag );
#endif                             /*                                       */

        /* ------------- See if there is a cube file directory --------------*/
    if  ( Btlr.cubefileflg ) {
    /* Change to the directory with the input files
     ***********************************************/
        if( chdir_ew( Btlr.cubedir ) == -1 ) {
            logit( "e", "%s: %s: CubeInDir directory <%s> not found; "
                     "exiting!\n", progname, subname, Btlr.cubedir );
            exit(-1);
        }
        if(Debugger)logit("et","%s: %s: changed to directory <%s>\n", progname, subname,Btlr.cubedir);
    }

   /* Start the heartbeat thread
   ****************************/
    time(&MyLastInternalBeat); /* initialize our last heartbeat time */
                          
    if ( StartThread( Heartbeat, (unsigned)THREAD_STACK_SIZE, &TidHeart ) == -1 ) {
        logit( "et","%s: %s: Error starting Heartbeat thread. Exiting.\n", progname, subname );
        tport_detach( &InRegion );
        return -1;
    }

	/* sleep for 2 seconds to allow heart to beat so statmgr gets it.  */
	
	sleep_ew(2000);


    if(Debugger) {
    }
        logit("e", "%s: %s: Magnitude-Distance table for LogA = %f.\n",  progname, subname, Btlr.MinLogA);
	    for(i=0;i<10;i++) {
	    	magt = (double)i/2.0;
		    GetMaxDist(&Dist, Btlr.MinLogA, magt);
	        logit("e", "%f  %f\n",  magt, Dist );
	    }
		if(Nnets==0) logit( "e", "%s: %s: Accept events any network.\n",  progname, subname );
		for(j=0;j<Nnets;j++) logit( "e", "%s: %s: Accept events from network: %s.\n",  progname, subname, Netcode[j] );

        /* ------------------------ start working loop -------------------------*/
    waitA = 0;
    while(1) {
                       
        /* -------------- see if a termination has been requested ----------*/
		if ( tport_getflag( &InRegion ) == TERMINATE ||
			 tport_getflag( &InRegion ) == MyPid ) {      /* detach from shared memory regions*/
			sleep_ew( 500 );       /* wait around while butlers finish */
			tport_detach( &InRegion );
			logit("et", "%s: %s: Termination requested; exiting.\n",  progname, subname );
			fflush(stdout);
			exit( 0 );
		}

		ProcessCUBEFile();

        sleep_ew( 500 );       /* wait around for more summary lines   */
    }
}


/***************************** Heartbeat **************************
 *           Send a heartbeat to the transport ring buffer        *
 ******************************************************************/

thr_ret Heartbeat( void *dummy )
{
    time_t now;

   /* once a second, do the rounds.  */
    while ( 1 ) {
        sleep_ew(1000);
        time(&now);

        /* Beat our heart (into the local Earthworm) if it's time
        ********************************************************/
        if (difftime(now,MyLastInternalBeat) > (double)HeartBeatInterval) {
            ewmod_status( TypeHeartBeat, 0, "" );
            time(&MyLastInternalBeat);
        }
    }
}


/***************************** ProcessCUBEFile **********************************
 * Checks the CUBE input directory for CUBE files.  While there are files, we   *
 * read them, pass them on for processing and delete them.                      *
 ********************************************************************************/
void ProcessCUBEFile( void)
{
    char    whoami[50], fname[100], tname[100], *com, *str, subname[] = "ProcessCUBEFile";
    char    net[5], eventid[100], date[25], time[25];
    int     i, year, mon, day, hour, min, sec, nfiles, reject;
    long    ret;
    TStrct  Time;  
    double  Evttime, lat, lon, mag;

    sprintf(whoami, " %s: %s: ", "nq_get_eq", subname);
    /* ------------- See if there is an CUBE file to process --------------*/
    if  ( Btlr.cubefileflg ) {
    /* Change to the directory with the input files
     ***********************************************/
        if( chdir_ew( Btlr.cubedir ) == -1 ) {
            logit( "e", "%s: %s: GetFromDir directory <%s> not found; "
                     "exiting!\n", progname, subname, Btlr.cubedir );
            exit(-1);
        }
//        if(Debugger)logit("et","%s changed to directory <%s>\n", whoami, Btlr.cubedir);
        do {
            ret = GetFileName( fname );
        
            if( ret != 1 ) {  /* Files found; Process */
				if( strcmp(fname,"core")==0 ) {
					if( remove( fname ) != 0) {
						logit("et", "Error: Cannot delete core file <%s>; exiting!", fname );
						break;
					}
					continue;
				}
                logit("et","\n");
                logit("e","%s got file name <%s>\n", whoami, fname);
                sleep_ew( 1*1000 );
					/* Open the next file
					 **********************************/
				nfiles = k_open( fname );
				if(nfiles == 0) {
					logit("e", "%s Error opening command file <%s>; exiting!\n", whoami, fname );
					exit( -1 );
				}
				while(nfiles > 0) {  /* While there are command files open */
					while(k_rd())  {      /* Read next line from active file  */
						com = k_str();         /* Get the first token from line */
			
							/* Ignore blank lines & comments
							 *******************************/
						if( !com )           continue;
						if( com[0] == '#' )  continue;
					
						memcpy( net, com, 2 );
						net[2] = '\0';
			
						str = k_str();
						if(str) strcpy( eventid , str );
			
						str = k_str();
						if(str) {
							strcpy( date , str );
							memcpy( str, date, 4 );
							str[4] = '\0';
							if ( sscanf( str, "%d", &Time.Year ) < 1 ) Time.Year = 0;
							memcpy( str, date+5, 2 );
							str[2] = '\0';
							if ( sscanf( str, "%d", &Time.Month ) < 1 ) Time.Month = 0;
							memcpy( str, date+8, 2 );
							str[2] = '\0';
							if ( sscanf( str, "%d", &Time.Day ) < 1 ) Time.Day = 0;
							
						}
			
						str = k_str();
						if(str) {
							strcpy( time , str );
							memcpy( str, time, 2 );
							str[2] = '\0';
							if ( sscanf( str, "%d", &Time.Hour ) < 1 ) Time.Hour = 0;
							memcpy( str, time+3, 2 );
							str[2] = '\0';
							if ( sscanf( str, "%d", &Time.Min ) < 1 ) Time.Min = 0;
							memcpy( str, time+6, 2 );
							str[2] = '\0';
							if ( sscanf( str, "%d", &sec ) < 1 ) sec = 0;
							Time.Sec   = sec;
						}
						
						lat = k_val();
						
						lon = k_val();
						
						mag = k_val();
                
					}
					nfiles = k_close();
				}
				logit("e", "%s %s%s %.4d-%.02d-%.02d %.02d:%.02d:%f %9.5f %10.5f %5.1f \n", whoami, net, eventid, 
									Time.Year, Time.Month, Time.Day, Time.Hour, Time.Min, Time.Sec, lat, lon, mag);
				Encode_Time( &Evttime, &Time);   
		
		/* If the cube file is not from requested net.  Ignore it.
		   *********************************************************/
				reject = 0;
				if ( strncmp( Netcode[0], "*", 1 ) != 0 ) {
					reject = 1;
					for(i=0;i<Nnets;i++) {
						if ( strncmp( net, Netcode[i], 2 ) == 0 ) {
							reject = 0;
						}
					}
				}
				if(reject) {
					logit( "e", "\nEvent file rejected: %s\n", fname );
				} else {
					MainProcess(&Btlr, net, eventid, Evttime, lat, lon, mag);  /*  Insert processing code here */
				}
				
				sprintf(tname, "%ssave/%s", Btlr.cubedir, fname);
				ret = rename(fname, tname); 
				/* The following silliness is necessary to be Windows compatible */ 
				if( ret != 0 ) {
					if(Btlr.Debug) logit( "e", "Error moving file %s to %s; ret = %d\n", fname, tname, ret );
					if( remove( tname ) != 0 ) {
						logit("e","error deleting file %s\n", tname);
					} else  {
						if(Btlr.Debug) logit("e","deleted file %s.\n", tname);
						ret = rename( fname, tname );
						if( ret != 0 ) {
							logit( "e", "error moving file %s to %s; ret = %d\n", fname, tname, ret );
						} else {
							if(Btlr.Debug) logit("e","%s moved to %s\n", fname, tname );
						}
					}
				} else {
					if(Btlr.Debug >= 2) logit("e","%s moved to %s\n", fname, tname );
				}
            }
        } while (ret != 1);
    }
}

/****************************************************************************
 *      config_me() process command file using kom.c functions              *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void config_me(Global *But,   char* configfile )
{
    char    whoami[50], *com, *str;
    char    init[20];       /* init flags, one byte for each required command */
    int     ncommand;       /* # of required commands you expect */
    int     nmiss;          /* number of required commands that were missed   */
    int     i, j, n, nfiles, success;

    sprintf(whoami, "%s: %s: ", progname, "config_me");
        /* Set to zero one init flag for each required command
         *****************************************************/
    ncommand = 8;
    for(i=0; i<ncommand; i++ )  init[i] = 0;
    
    But->MinLogA     =  -0.5;

    But->MinSize     =   0.0;
    But->Debug       = 0;
    But->nStaDB      = 0;
    But->cubefileflg = 0;
	strcpy( Netcode[0], "*" );
	Nnets = 0;
    
        /* Open the main configuration file
         **********************************/
    nfiles = k_open( configfile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, configfile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

                /* Process anything else as a command
                 ************************************/
  /*0*/
            if( k_its("LogSwitch") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/
            else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModuleId , str );
                init[1] = 1;
            }
  /*2*/
            else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( InRingName, str );
                init[2] = 1;
            }

  /*3*/
            else if( k_its("HeartBeatInt") ) {
                HeartBeatInterval = k_long();
                init[3] = 1;
            }

                /* get station list path/name
                *****************************/
/*4*/
            else if( k_its("StationList") ) {
                if ( But->nStaDB >= MAX_STADBS ) {
                    fprintf( stderr, "%s Too many <StationList> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_STADBS );
                    return;
                }
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->stationList[But->nStaDB] , str );
                But->nStaDB++;
                init[4] = 1;
            }

                /* get serial number list path/name
                *****************************/
/*5*/
            else if( k_its("SerNumList") ) {
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->SerNumList , str );
                init[5] = 1;
            }

        /* get Gif directory path/name
        *****************************/
/*6*/
            else if( k_its("GifDir") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. Gif directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                j = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[j-1] != '/' ) strcat(str, "/");
                if(str) strcpy( But->GifDir , str );
                init[6] = 1;
            }

        /* get output directory path/name
        *****************************/
/*7*/
            else if( k_its("OutDir") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. Gif directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                j = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[j-1] != '/' ) strcat(str, "/");
                if(str) strcpy( But->OutDir , str );
                init[7] = 1;
            }

               /* optional commands */


        /* get Cube file input directory path/name
        ****************************************/
            else if( k_its("CubeDir") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf(stderr,"%s Fatal error. Cube directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    exit( -1 );
                }
                j = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[j-1] != '/' ) strcat(str, "/");
                if(str) strcpy( But->cubedir , str );
                But->cubefileflg = 1;
            }
            
			else if( k_its("Network") ) {
                str = k_str();
                if(Nnets<MAXN) {
					if(str) strcpy( Netcode[Nnets++], str );
                }
            }

            else if( k_its("MinSize") ) {  /*optional command*/
                But->MinSize = k_val();
                if(But->MinSize>10.0) But->MinSize = 10.0; 
                if(But->MinSize< 0.0) But->MinSize =  0.0; 
            }

            else if( k_its("MinLogA") ) {  /*optional command*/
			    But->MinLogA = k_val();
                if(But->MinLogA>10.0) But->MinLogA = 10.0; 
                if(But->MinLogA<-3.0) But->MinLogA = -3.0; 
            }

            else if( k_its("Debug") ) {  /*optional command*/
                But->Debug = k_int();
                if( k_err() ) But->Debug = 99;
                Debugger = But->Debug;
            }
            

                /* At this point we give up. Unknown thing.
                *******************************************/
            else {
                fprintf(stderr, "%s <%s> Unknown command in <%s>.\n",
                         whoami, com, configfile );
                continue;
            }

                /* See if there were any errors processing the command
                 *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "%s Bad <%s> command  in <%s>; exiting!\n",
                             whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
    }

        /* After all files are closed, check init flags for missed commands
         ******************************************************************/
    nmiss = 0;
    for(i=0;i<ncommand;i++)  if( !init[i] ) nmiss++;
    if ( nmiss ) {
        fprintf( stderr, "%s ERROR, no ",  whoami);
        if ( !init[0] )  fprintf( stderr, "<LogSwitch> "       );
        if ( !init[1] )  fprintf( stderr, "<MyModuleId> "      );
        if ( !init[2] )  fprintf( stderr, "<RingName> "        );
        if ( !init[3] )  fprintf( stderr, "<HeartBeatInt> "    );
        if ( !init[4] )  fprintf( stderr, "<GetSumFrom> "      );
        if ( !init[5] )  fprintf( stderr, "<StationList> "     );
        if ( !init[6] )  fprintf( stderr, "<SerNumList> "      );
        if ( !init[7] )  fprintf( stderr, "<GifDir> "          );
        if ( !init[8] )  fprintf( stderr, "<OutDir> "          );
        fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
        exit( -1 );
    }
    if(But->Debug) {
    	fprintf( stderr, "%s %d database files.\n", whoami, But->nStaDB );
    	for(i=0;i<But->nStaDB;i++) fprintf( stderr, "%s %d \n", whoami, But->stationList[i] );
    }
}


/**********************************************************************************
 * ewmod_status() builds a heartbeat or error msg & puts it into shared memory    *
 **********************************************************************************/
void ewmod_status( unsigned char type,  short ierr,  char *note )
{
    char        subname[] = "ewmod_status";
    MSG_LOGO    logo;
    char        msg[512];
    long        size;
    time_t      t;

	/* Build the message
	 *******************/
    logo.instid = InstId;
    logo.mod    = MyModId;
    logo.type   = type;

    time( &t );
    if( type == TypeHeartBeat ) {
        sprintf( msg, "%ld %ld\n\0", t,MyPid);
    } else
    if( type == TypeError ) {
        sprintf( msg, "%ld %hd %s\n", t, ierr, note );
        logit( "t", "%s: %s:  %s\n",  progname, subname, note );
    }

    size = strlen( msg );   /* don't include the null byte in the message */

	/* Write the message to shared memory
	 ************************************/
    if( tport_putmsg( &InRegion, &logo, size, msg ) != PUT_OK ) {
        if( type == TypeHeartBeat ) {
            logit("et","%s: %s:  Error sending heartbeat.\n",  progname, subname );
        } else
        if( type == TypeError ) {
            logit("et","%s: %s:  Error sending error:%d.\n",  progname, subname, ierr );
        }
    }
}


/****************************************************************************
 *  lookup_ew( ) Look up important info from earthworm.h tables           *
 ****************************************************************************/
void lookup_ew( )
{
    char        subname[] = "lookup_ew";
    
        /* Look up keys to shared memory regions
         ***************************************/
    if( (InRingKey = GetKey(InRingName)) == -1 ) {
        fprintf( stderr,
                "%s: %s: Invalid ring name <%s>; exiting!\n",  progname, subname, InRingName );
        exit( -1 );
    }

        /* Look up installation Id
         *************************/
    if ( GetLocalInst( &InstId ) != 0 ) {
        fprintf( stderr,
                "%s: %s: error getting local installation id; exiting!\n",  progname, subname );
        exit( -1 );
    }

        /* Look up modules of interest
         *****************************/
    if ( GetModId( MyModuleId, &MyModId ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid module name <%s>; exiting!\n",  progname, subname, MyModuleId );
        exit( -1 );
    }

        /* Look up message types of interest
         ***********************************/
    if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n",  progname, subname );
        exit( -1 );
    }
    if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid message type <TYPE_ERROR>; exiting!\n",  progname, subname );
        exit( -1 );
    }
}



