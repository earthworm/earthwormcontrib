#
# This is the nq_get_eq parameter file. This module gets "trigger" messages
# from the hypo_rings, and initiates threads to make record sections.

#  Basic Earthworm setup:
#
LogSwitch     1              # 0 to completely turn off disk log file
MyModuleId    MOD_NQGETEQ     # module id for this instance of nq_get_eq 

RingName      HYPO_RING      # ring to get input from
HeartBeatInt  5              # seconds between heartbeats

# List the message logos to grab from transport ring
#           Installation    Module      Message Type
GetSumFrom  INST_MENLO      MOD_EQPROC  TYPE_HYP2000ARC

#    others
MaxMsgSize     60000    # length of largest message we'll ever handle - in bytes
MaxMessages      100    # limit of number of message to buffer

# Directory for manual input of arc files.
ArcDir   /home/picker/gifs/nq_get_eq/arc_indir/

# Directory for manual input of cube files.
CubeDir   /home/picker/gifs/nq_get_eq/cube_indir/

 StationList     /home/earthworm/run/params/netquake.db1

 SerNumList     /home/earthworm/run/params/nq_nc_sernums.txt
GifDir   /home/picker/gifs/nq_get_eq/  
OutDir   /home/picker/sendfiles/netquakes/  

# We accept a command "Passes" which issues a heartbeat-type logit
# to let us know nq_get_eq is alive (debugging)
 Passes   8000

# We accept a command "NoFlush" which prevents the ring from being
# flushed on restart.
#NoFlush
#
 

    # *** Optional Commands ***
# MinLogA   0.0
 SecsToWait 300
 MaxDist    300  # Max distance of traces to plot; default=100km
 MinSize    0.0  # Minimum magnitude to save in list; default=0.0
 MaxDays      5  # Max number of days to list; default=7
 
# We accept a command "Debug" which turns on a bunch of log messages
 
  Debug 1

