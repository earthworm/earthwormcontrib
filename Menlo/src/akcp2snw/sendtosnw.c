#include <socket_ew.h>

/**********************************************************
   SendSensorReadingToSNW

   Send a string to the SNW agent over a socket connnection.
 **********************************************************/

int SendSensorReadingToSnw( char snwStr[],
                            char snwserver[],
                            unsigned short snwport,
                            int snwtimeout )
{
   int    sd;
   struct sockaddr_in sin;
   struct hostent *he;

/* Open a socket for sending to SNW
   ********************************/
   sd = socket_ew( AF_INET, SOCK_STREAM, 0 );
   if ( sd == -1 )
   {
      logit( "et", "Error opening socket.\n" );
      return -1;
   }

/* Get SNW agent host information from hosts file or DNS
   *****************************************************/
   memset( (char *)&sin, '\0', sizeof(sin) );
   sin.sin_family = AF_INET;
   sin.sin_port   = htons(snwport);
   he = gethostbyname( snwserver );
   if ( he == NULL )
   {
      logit( "et", "Error. gethostbyname cannot retrieve host information.\n" );
      SocketClose( sd );
      return -1;
   }

/* Connect to SNW agent
   ********************/
   memcpy( (char *)&sin.sin_addr, he->h_addr_list[0], he->h_length );
   if ( connect_ew( sd, (struct sockaddr FAR*)&sin, sizeof(sin), snwtimeout ) == -1 )
   {
      SocketPerror( "Can't connect to SNW agent." );
      SocketClose( sd );
      return -1;
   }

/* Send sensor measurement to SNW agent
   ************************************/
   if ( sendall( sd, snwStr, strlen(snwStr), 0 ) == SOCKET_ERROR )
   {
      SocketPerror( "Can't send sensor measurement to SNW agent." );
      SocketClose( sd );
      return -1;
   }
   SocketClose( sd );
   return 0;
}
