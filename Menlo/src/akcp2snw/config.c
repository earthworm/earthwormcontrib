#include <stdio.h>
#include <stdlib.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include "akcp2snw.h"


/* Global variables
   ****************/
long           RingKey;               // Transport ring key
unsigned char  MyModId;               // This program's module id
int            LogFlag;               // If 0, no disk logging
int            CheckInterval;         // Check SNMP agents this often
int            HeartBeatInterval;     // Heartbeats to statmgr
char           SnwServer[80];         // Machine where the SNW agent is running
unsigned short SnwPort;               // Port number of SNW agent on SNW server
SPR            *spr = NULL;
int            nSPR = 0;              // Number of SPR lines in config file



       /***************************************************
        *                   GetConfig()                   *
        *  Read the configuration file.                   *
        *  Exit if any errors are encountered.            *
        ***************************************************/

void GetConfig( char *configfile )
{
#define NCOMMAND 8

   char  init[NCOMMAND];        // Flags, one for each command
   int   nmiss;                 // Number of commands that were missed
   char  *com;
   char  *str;
   int   nfiles;
   int   success;
   int   i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < NCOMMAND; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
        printf( "akcp2snw: Error opening command file <%s>. Exiting.\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )      // While there are command files open
   {
        while ( k_rd() )     // Read next line from active file
        {
           com = k_str();    // Get the first token from line

/* Ignore blank lines & comments
   *****************************/
           if ( !com )          continue;
           if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
           if ( com[0] == '@' )
           {
              success = nfiles + 1;
              nfiles  = k_open( &com[1] );
              if ( nfiles != success )
              {
                 printf( "akcp2snw: Error opening command file <%s>. Exiting.\n",
                     &com[1] );
                 exit( -1 );
              }
              continue;
           }

           if ( k_its( "MyModuleId" ) )
           {
              str = k_str();
              if ( str )
              {
                 if ( GetModId( str, &MyModId ) < 0 )
                 {
                    printf( "akcp2snw: Invalid MyModuleId <%s> in <%s>",
                             str, configfile );
                    printf( ". Exiting.\n" );
                    exit( -1 );
                 }
              }
              init[0] = 1;
           }

           else if( k_its( "RingName" ) )
           {
              str = k_str();
              if (str)
              {
                 if ( ( RingKey = GetKey(str) ) == -1 )
                 {
                    printf( "akcp2snw: Invalid RingName <%s> in <%s>",
                             str, configfile );
                    printf( ". Exiting.\n" );
                    exit( -1 );
                 }
              }
              init[1] = 1;
           }

           else if( k_its( "LogFlag" ) )
           {
              LogFlag = k_int();
              init[2] = 1;
           }

           else if( k_its( "HeartBeatInterval" ) )
           {
              HeartBeatInterval = k_int();
              init[3] = 1;
           }

           else if( k_its( "CheckInterval" ) )
           {
              CheckInterval = k_int();
              init[4] = 1;
           }

           else if( k_its( "SnwServer" ) )
           {
              str = k_str();
              if (str) strcpy( SnwServer, str );
              init[5] = 1;
           }

           else if( k_its( "SnwPort" ) )
           {
              SnwPort = k_int();
              init[6] = 1;
           }

           else if( k_its( "SPR" ) )
           {
              SPR *anotherSPR;
              nSPR++;
              anotherSPR = (SPR *) realloc( spr, nSPR * sizeof(SPR) );
              if ( anotherSPR != NULL )
              {
                 spr = anotherSPR;
                 str = k_str();
                 if (str) strcpy( spr[nSPR-1].netSta, str );       // eg "NC-PBHRY"
                 str = k_str();
                 if (str) strcpy( spr[nSPR-1].sensorprobe, str );  // eg "buckhillvoltage"
                 spr[nSPR-1].sprobeport = k_int();                 // eg 0, 1, 2, ...
                 str = k_str();
                 if (str) strcpy( spr[nSPR-1].sensortype, str );   // eg "dcvoltage"
                 str = k_str();
                 if (str) strcpy( spr[nSPR-1].snwLabel, str );     // eg "Battery Voltage (nom 24V)"
                 spr[nSPR-1].prevReadSensor = READSENSOR;          // Assume we were able to read sensor last time
                 init[7] = 1;
              }
              else
              {
                 free( spr );
                 printf( "Error reallocating memory.\n" );
                 exit( -1 );
              }
           }

           else
           {
              printf( "akcp2snw: <%s> unknown command in <%s>.\n",
                       com, configfile );
              continue;
           }

/* See if there were any errors processing the command
   ***************************************************/
           if ( k_err() )
           {
              printf( "akcp2snw: Bad <%s> command in <%s>; \n",
                       com, configfile );
              exit( -1 );
           }
        }
        nfiles = k_close();
   }

/* Check flags for missed commands
   *******************************/
   nmiss = 0;
   for ( i = 0; i < NCOMMAND; i++ )
      if( !init[i] ) nmiss++;

   if ( nmiss )
   {
      printf( "akcp2snw: ERROR, no " );
      if ( !init[0] ) printf( "<MyModuleId> "        );
      if ( !init[1] ) printf( "<RingName> "          );
      if ( !init[2] ) printf( "<LogFlag> "           );
      if ( !init[3] ) printf( "<HeartBeatInterval> " );
      if ( !init[4] ) printf( "<CheckInterval> "     );
      if ( !init[5] ) printf( "<SnwServer> "         );
      if ( !init[6] ) printf( "<SnwPort> "           );
      if ( !init[7] ) printf( "<SPR> "               );
      printf( "command(s) in <%s>. Exiting.\n", configfile );
      exit( -1 );
   }
   return;
}


       /***************************************************
        *                   LogConfig()                   *
        *     Log the configuration file parameters.      *
        ***************************************************/

void LogConfig( void )
{
   int i;

   logit( "e", "MyModId:           %u\n",  MyModId );
   logit( "e", "RingKey:           %ld\n", RingKey );
   logit( "e", "HeartBeatInterval: %d seconds\n", HeartBeatInterval );
   logit( "e", "LogFlag:           %d\n",  LogFlag );
   logit( "e", "CheckInterval:     %d seconds\n", CheckInterval );
   logit( "e", "SnwServer:         %s\n",  SnwServer );
   logit( "e", "SnwPort:           %d\n",  SnwPort );
   logit( "e", "\n" );
   logit( "e", "                            S-Probe  Sensor\n" );
   logit( "e", "  Net-Sta   SNMP device      Port     Type         SNW Label\n" );
   logit( "e", "  -------   -----------     -------  ------        ---------\n" );

   for ( i = 0; i - nSPR; i++ )
      logit( "e", " %-8s  %-20s  %d  %-11s  \"%s\"\n",
              spr[i].netSta, spr[i].sensorprobe,
              spr[i].sprobeport, spr[i].sensortype,
              spr[i].snwLabel );
   logit( "e", "\n" );
   return;
}
