
/* akcp2snw.c

   This is an Earthworm program to read sensor values from
   AKCP SensorProbes and send them to SeisnetWatch.
   Values are obtained using the net-snmp API.
   Alarms are sent via Earthworm if sensor values are out of range.

   To get voltage or humidity from a SensorProbe from a command prompt:
   snmpget -mSPAGENT-MIB -v1 -cpublic -Ov <sensorprobe> sensorProbeHumidityPercent.<sprobeport>
   snmpget -mSPAGENT-MIB -v1 -cpublic -Ov <sensorprobe> sensorProbeHumidityStatus.<sprobeport>

   To get temperature from a SensorProbe from a command prompt:
   snmpget -mSPAGENT-MIB -v1 -cpublic -Ov <sensorprobe> sensorProbeTempDegree.<sprobeport>
   snmpget -mSPAGENT-MIB -v1 -cpublic -Ov <sensorprobe> sensorProbeTempStatus.<sprobeport>
   ********************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include "akcp2snw.h"

/* From the configuration file
   ***************************/
extern unsigned char  MyModId;               // This program's module id
extern long           RingKey;               // Transport ring key
extern int            LogFlag;
extern int            HeartBeatInterval;     // Heartbeats to statmgr
extern int            CheckInterval;         // Check SNMP devices this often, in seconds
extern char           SnwServer[];           // Machine where the SNW agent is running
extern unsigned short SnwPort;               // Port number of SNW agent
extern SPR            *spr;                  // One reading from a sensor
extern int            nSPR;                  // Number of SPR lines in config file

/* Function prototypes
   *******************/
void GetConfig( char *configfile );
void LogConfig( void );
int  ReportStatus( unsigned char type, short ierr, char *note );
int  GetSensorProbeValue( SPR s, int *value, int *dcStatus );
int  SendSensorReadingToSnw( char snwStr[], char SnwServer[], unsigned short SnwPort, int SnwTimeout );

/* Global variables shared with status.c
   *************************************/
pid_t         myPid;            // For restarts by startstop
char          InstId;           // Local installation id
unsigned char TypeHeartBeat;
unsigned char TypeError;
SHM_INFO      Region;


           /****************************************
            *       Main program starts here       *
            ****************************************/

int main( int argc, char *argv[] )
{

/* Check command line arguments and get configuration file
   *******************************************************/
   if ( argc != 2 )
   {
        printf( "Usage: akcp2snw <configfile>\n" );
        return 0;
   }
   GetConfig( argv[1] );

/* Look up installation id
   ***********************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "%s: Error getting installation id. Exiting.\n", argv[0] );
      return -1;
   }

/* Look up message types from earthworm.h tables
   *********************************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "%s: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n", argv[0] );
      return -1;
   }

   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "%s: Invalid message type <TYPE_ERROR>. Exiting.\n", argv[0] );
      return -1;
   }

/* Log the configuration file
   **************************/
   logit_init( argv[1], (short)MyModId, 256, LogFlag );
   LogConfig();

/* Get process ID for heartbeat messages
   *************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "%s: Cannot get pid. Exiting.\n", argv[0] );
      return -1;
   }

/* Attach to shared memory ring
   ****************************/
   tport_attach( &Region, RingKey );

/* Initialize the socket system for sending to SNW
   ***********************************************/
   SocketSysInit();

/* Loop until kill flag is set
   ***************************/
   while ( 1 )
   {
      static time_t tHeart = 0;       // When the last heartbeat was sent
      static time_t tCheck = 0;       // When the sensors were last checked
      time_t now = time(0);           // Current time
      int i;

/* Check kill flag
   ***************/
      if ( tport_getflag( &Region ) == TERMINATE ||
           tport_getflag( &Region ) == myPid )
      {
         tport_detach( &Region );
         logit( "et", "Termination requested. Exiting.\n" );
         return 0;
      }

/* Send heartbeat every HeartBeatInterval seconds
   **********************************************/
      if ( (now - tHeart) >= (time_t)HeartBeatInterval )
      {
         if ( ReportStatus( TypeHeartBeat, 0, "" ) != PUT_OK )
            logit( "et", "%s: Error sending heartbeat to ring.\n", argv[0] );
         tHeart = now;
      }

/* Read all AKCP sensors every CheckInterval seconds
   *************************************************/
      if ( (now - tCheck) < (time_t)CheckInterval )
      {
         sleep_ew( 1000 );
         continue;
      }
      tCheck = now;

      logit( "e", "\n" );
      for ( i = 0; i < nSPR; i++ )
      {
         char      note[120];
         const int nparam = 2;          // Number of params to write to SNW
         const int snwTimeout = -1;     // Connection timeout in msec; if -1, do not time out
         char      snwStr[120];         // Entire string to send to SNW
         int       val;                 // Measurement from the SensorProbe
         int       stat;                // Status of the SensorProbe measurement, as listed
                                        //    in the dcStatus array defined below.

/* dcStatus of DC volts, humidity, and temperature devices
   *******************************************************/
         char *dcStatus[] = {"", "No Status", "Sensor Normal", "High Warning", "High Critical",
                            "Low Warning", "Low Critical", "Sensor Error"};

/* dcStatus of AC volts and other relay devices
   ********************************************/
         char *acStatus[] = {"", "No Status", "Sensor Normal", "", "Critical",
                            "", "", "Sensor Error"};

/* Get a SensorProbe measurement and dcStatus of the measurement.
   Send an alarm if we can't read from the SensorProbe.
   *************************************************************/
         if ( GetSensorProbeValue( spr[i], &val, &stat ) != COMPLETED_OK )
         {
            if ( spr[i].prevReadSensor == READSENSOR )
            {
               sprintf( note, "%s: Error reading %s from port %d", spr[i].sensorprobe, spr[i].sensortype,
                        spr[i].sprobeport );
               logit( "et", "%s\n", note );
               if ( ReportStatus( TypeError, ERR_READSENSOR, note ) != PUT_OK )
                  logit( "et", "%s: Error sending error message to ring.\n", argv[0] );
            }
            spr[i].prevReadSensor = NOREADSENSOR;
            continue;
         }

/* We got a successful reading from the SensorProbe.
   Send an alarm-cleared message if we couldn't read from the SensorProbe before.
   *****************************************************************************/
         if ( spr[i].prevReadSensor == NOREADSENSOR )
         {
            sprintf( note, "Now able to read %s from %s port %d", spr[i].sensorprobe, spr[i].sensortype,
                     spr[i].sprobeport );
            logit( "et", "%s\n", note );
            if ( ReportStatus( TypeError, ERR_READSENSOR, note ) != PUT_OK )
               logit( "et", "%s: Error sending error message to ring.\n", argv[0] );
         }
         spr[i].prevReadSensor = READSENSOR;

/* Log the SensorProbe measurement and prepare a string to send to SeisnetWatch
   ****************************************************************************/
         if ( strcmp(spr[i].sensortype, "dcvoltage") == 0 )
         {
            double dcVoltage = 0.1 * val;    // Tenths of a volt
            logit( "et", "%s %s=%.1lf (%s)\n", spr[i].netSta, spr[i].snwLabel, dcVoltage, dcStatus[stat] );
            sprintf( snwStr, "%s:%d:%s=%.1lf", spr[i].netSta, nparam, spr[i].snwLabel, dcVoltage );
         }
         else if ( strcmp(spr[i].sensortype, "temperature") == 0 )
         {
            double tempF = 0.1 * val;        // Tenths of a degree
            logit( "et", "%s %s=%.1lf (%s)\n", spr[i].netSta, spr[i].snwLabel, tempF, dcStatus[stat] );
            sprintf( snwStr, "%s:%d:%s=%.1lf", spr[i].netSta, nparam, spr[i].snwLabel, tempF );
         }
         else if ( strcmp(spr[i].sensortype, "humidity") == 0 )
         {
            int humidity = val;        // Percent
            logit( "et", "%s %s=%d (%s)\n", spr[i].netSta, spr[i].snwLabel, humidity, dcStatus[stat] );
            sprintf( snwStr, "%s:%d:%s=%d", spr[i].netSta, nparam, spr[i].snwLabel, humidity );
         }

/* AC voltage sensors record only the presence or absense of AC power.
   If acVoltage = 2, AC power is on.  If acVoltage = 4, AC power is off.
   ********************************************************************/
         else if ( strcmp(spr[i].sensortype, "acvoltage") == 0 )
         {
            int  acVoltage = val;
            logit( "et", "%s %s=%d (%s)\n", spr[i].netSta, spr[i].snwLabel, acVoltage, acStatus[val] );
            sprintf( snwStr, "%s:%d:%s=%d", spr[i].netSta, nparam, spr[i].snwLabel, acVoltage );
         }
         strcat( snwStr, ";UsageLevel=3\n" );

/* Send string to SNW
   ******************/
         if ( SendSensorReadingToSnw( snwStr, SnwServer, SnwPort, snwTimeout ) != 0 )
            logit( "et", "Error sending sensor reading to SeisnetWatch\n" );
      }
   }
}

