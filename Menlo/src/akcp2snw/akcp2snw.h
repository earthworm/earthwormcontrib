
// akcp2snw.h
// **********

/* Contains one measurement from an AKCP SensorProbe.
   *************************************************/
typedef struct
{
   char netSta[10];       // Network-station code used by SNW
   char sensorprobe[60];  // Network name; must be in DNS or hosts file
   int  sprobeport;       // RJ45 jack number on the SensorProbe, starting with 0
   char sensortype[15];   // dcvoltage, acvoltage, temperature, or humidity
   char snwLabel[40];     // Label sent to SNW
   int  prevReadSensor;   // Was the sensor was readable the last time we tried?
} SPR;

#define NOREADSENSOR 0
#define READSENSOR   1
#define COMPLETED_OK       0

/* Return values from getsnmp function call
   ****************************************/
#define COMPLETED_OK       0
#define CANT_OPEN_SESSION -1
#define ERROR_IN_PACKET   -2
#define SNMP_TIMEOUT      -3
#define UNKNOWN_ERROR     -4

/* Errors reported by Earthworm
   ****************************/
#define ERR_READSENSOR 0
#define ERR_ALARM      1
