#define WIN32
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <string.h>
#include "akcp2snw.h"


/* Get an integer sensor reading and a status value from a SensorProbe.:
   *******************************************************************/

int GetSensorProbeValue( SPR s, int *returnVal, int *returnStatus )
{
   netsnmp_session session, *ss;   // Contains info about who we're talking to
   netsnmp_pdu *pdu;               // Fill with info we send to the remote host
   netsnmp_pdu *response;          // Sent back from the remote host to us
   netsnmp_variable_list *vars;    // Variables we want to access via SNMP

   oid    anOID[MAX_OID_LEN];      // The location of information we want to retrieve
   size_t anOID_len;
   int    status;
   int    return_code;
   char   mib[80];
   char   *peer = s.sensorprobe;

/* Initialize the SNMP library
   ***************************/
   init_snmp( "GetSnmp" );

/* Initialize a "session" that defines who we're going to
   talk to and what authentication we will be using
   ******************************************************/
   snmp_sess_init( &session );                   // Set up defaults
   session.peername      = strdup( peer );       // Memory allocated here
   session.version       = SNMP_VERSION_1;
   session.community     = "public";
   session.community_len = strlen( session.community );

/* Open the "session" (conversation with SNMP server)
   **************************************************/
   SOCK_STARTUP;
   ss = snmp_open( &session );                   // Allocates session "ss"

   if ( !ss )
   {
      snmp_sess_perror( "GetSnmp", &session );
      return_code = CANT_OPEN_SESSION;
      goto ERROR_OPENING_SESSION;
   }

/* Create the PDU packet for our request
   *************************************/
   pdu = snmp_pdu_create( SNMP_MSG_GET );        // Does this allocate "pdu" ???

/* Add requests for parameters of interest.
   Add a null request after each parameter request.
   get_node will fail if the MIB is not found or
   if the parameter isn't part of the MIB.
   ***********************************************/
   anOID_len = MAX_OID_LEN;
   if ( strcmp(s.sensortype, "temperature") == 0 )
//    sprintf( mib, ".1.3.6.1.4.1.3854.1.2.2.1.16.1.3.%d", s.sprobeport );    // Whole degrees
      sprintf( mib, ".1.3.6.1.4.1.3854.1.2.2.1.16.1.14.%d", s.sprobeport );   // Tenth's of a degree
   else if ( strcmp(s.sensortype, "dcvoltage") == 0 )
      sprintf( mib, ".1.3.6.1.4.1.3854.1.2.2.1.17.1.3.%d", s.sprobeport );
   else if ( strcmp(s.sensortype, "humidity") == 0 )
      sprintf( mib, ".1.3.6.1.4.1.3854.1.2.2.1.17.1.3.%d", s.sprobeport );
   else if ( strcmp(s.sensortype, "acvoltage") == 0 )
      sprintf( mib, ".1.3.6.1.4.1.3854.1.2.2.1.18.1.3.%d",  s.sprobeport );
   else
   {
      logit( "et", "Invalid sensor type: %s\n", s.sensortype );
      logit( "et", "Exiting.\n" );
      exit( 0 );
   }
   if ( !read_objid( mib, anOID, &anOID_len ) )
   {
      logit( "et", "%s: read_objid failed for %s\n", peer, mib );
      logit( "et", "Exiting.\n" );
      exit( 0 );
   }
   snmp_add_null_var( pdu, anOID, anOID_len );

   anOID_len = MAX_OID_LEN;

   if ( strcmp(s.sensortype, "temperature") == 0 )
      sprintf( mib, ".1.3.6.1.4.1.3854.1.2.2.1.16.1.4.%d", s.sprobeport );
   else if ( strcmp(s.sensortype, "dcvoltage") == 0 )
      sprintf( mib, ".1.3.6.1.4.1.3854.1.2.2.1.17.1.4.%d", s.sprobeport );
   else if ( strcmp(s.sensortype, "humidity") == 0 )
      sprintf( mib, ".1.3.6.1.4.1.3854.1.2.2.1.17.1.4.%d", s.sprobeport );
   else if ( strcmp(s.sensortype, "acvoltage") == 0 )
      sprintf( mib, ".1.3.6.1.4.1.3854.1.2.2.1.18.1.4.%d", s.sprobeport );
   if ( !read_objid( mib, anOID, &anOID_len ) )
   {
      logit( "et", "%s: read_objid failed for %s\n", peer, mib );
      logit( "et", "Exiting.\n" );
      exit( 0 );
   }
   snmp_add_null_var( pdu, anOID, anOID_len );

/* Send request to SNMP server and wait for response
   *************************************************/
   status = snmp_synch_response( ss, pdu, &response );

/* Process the response
   ********************/
   if ( status == STAT_SUCCESS && response->errstat == SNMP_ERR_NOERROR )
   {

/* Success. Get the result variables.
   *********************************/
//    for ( vars = response->variables; vars; vars = vars->next_variable )
//       print_variable( vars->name, vars->name_length, vars );

      vars = response->variables;
      *returnVal = *vars->val.integer;          // Output
      if ( vars->type != ASN_INTEGER )
      {
         logit( "et", "Error. returnVal is not an integer. Exiting.\n" );
         exit( 0 );
      }

      vars = vars->next_variable;
      *returnStatus = *vars->val.integer;       // Output
      if ( vars->type != ASN_INTEGER )
      {
         logit( "et", "Error. returnStatus is not an integer. Exiting.\n" );
         exit( 0 );
      }
      return_code = COMPLETED_OK;
   }

/* Request failed. Print what went wrong.
   *************************************/
   else
   {
      if ( status == STAT_SUCCESS )
      {
         logit( "et", "%s: Error in packet\n", peer );
         logit( "et", "%s: %s\n", peer, snmp_errstring(response->errstat) );
         return_code = ERROR_IN_PACKET;
      }
      else if ( status == STAT_TIMEOUT )
      {
         logit( "et", "%s: Timeout: No response from %s.\n", peer, session.peername );
         return_code = SNMP_TIMEOUT;
      }
      else
      {
         snmp_sess_perror( "GetSnmp", ss );
         return_code = UNKNOWN_ERROR;
      }
   }

/* Close session and return
   ************************/
   if ( response )
      snmp_free_pdu( response );
   snmp_close( ss );                // Frees "ss", I think

ERROR_OPENING_SESSION:
   free( session.peername );        // Allocated by strdup above
   SOCK_CLEANUP;
   return return_code;
}
