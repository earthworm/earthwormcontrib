#
#                akcp2snw's Configuration File
#
MyModuleId   MOD_AKCP2SNW   # Module id for this program.

RingName        STAT_RING   # Earthworm transport ring to connect to.

HeartBeatInterval      30   # Interval between heartbeats sent to statmgr,
                            #   in seconds.

LogFlag                 1   # If 0, turn off disk logging
                            # If 1, log to disk
                            # If 2, disable logging to stderr/stdout

CheckInterval         900   # Get snmp values this often, in seconds.

SnwServer        campbell   # Machine where the SNW agent is running.
                            # The name must be in the hosts file or DNS (No IPs)

SnwPort              6666   # Port number of SNW agent on the SNW server.


# These are the measurements to be obtained from AKCP SensorProbes.

#                               S-Probe  Sensor
#    Net-Sta   SensorProbe       Port     Type        SNW Label
#    -------   -----------      -------  ------       ---------
SPR  NC-CBRRY  bollingervoltage     0   dcvoltage   "Battery Voltage (nom 12V)"
SPR  NC-CBRRY  bollingervoltage     1   acvoltage   "AC Voltage Status"
SPR  NC-HBLRY  hollistervoltage     0   dcvoltage   "Battery Voltage (nom 12V)"
SPR  NC-HBLRY  hollistervoltage     1   acvoltage   "AC Voltage Status"
SPR  NC-PBHRY  buckhillvoltage      0   dcvoltage   "Battery Voltage (nom 24V)"
SPR  NC-PHORY  sensorprobe-hog      0   temperature "Outside Temperature (Deg F)"
SPR  NC-PHORY  sensorprobe-hog      1   temperature "Room Temperature (Deg F)"
SPR  NC-PHORY  sensorprobe-hog      1   humidity    "Room Humidity (percent)"
SPR  NC-PWHRY  sensorprobe-williams 0   temperature "Room Temperature (Deg F)"
