/*****************************************************************************
 *  nq_get_eq.h                                                              *
 *                                                                           *
 *  header file for nq_get_eq.c                                              *
 *****************************************************************************/

/*****************************************************************************
 *  defines                                                                  *
 *****************************************************************************/

#define MAXCHANNELS     1600  /* Maximum number of channels                  */

#define MAX_STADBS        20  /* Maximum number of Station database files    */

#define GDIRSZ           132  /* Size of string for GIF target directory     */
#define STALIST_SIZ      100  /* Size of string for station list file        */
#define MAXLOGO            2  /* Maximum number of Logos                     */

#define TIMEOUT          600  /* Time(sec) after event to wait for data.     */

#define MAX_STR          255  /* Size of strings for arkive records          */


/*****************************************************************************
 *  Define the structure for time records.                                   *
 *****************************************************************************/

typedef struct TStrct {   
    double  Time1600; /* Time (Sec since 1600/01/01 00:00:00.00)             */
    double  Time;     /* Time (Sec since 1970/01/01 00:00:00.00)             */
    int     Year;     /* Year                                                */
    int     Month;    /* Month                                               */
    int     Day;      /* Day                                                 */
    int     Hour;     /* Hour                                                */
    int     Min;      /* Minute                                              */
    double  Sec;      /* Second                                              */
} TStrct;

/*****************************************************************************
 *  Define the structure for the Arkive.                                     *
 *  This is an abbreviated structure.                                        *
 ******************************************************************************/

typedef struct Arkive {   /* An Arkive List */
    char    ArkivMsg[MAX_BYTES_PER_EQ];
    double  EvntTime;     /* Time of Event (Julian Sec)                                    */
    short   EvntYear;     /*   0-  1   0-  3 Event Year                                    */
    short   EvntMonth;    /*   2-  3   4-  5 Event Month                                   */
    short   EvntDay;      /*   4-  5   6-  7 Event Day                                     */
    short   EvntHour;     /*   6-  7   8-  9 Event Hour                                    */
    short   EvntMin;      /*   8-  9  10- 11 Event Minute                                  */
    float   EvntSec;      /*  10- 13  12- 15 Event Second*100                              */
    float   EvntLat;      /*  14- 20  16- 22 Geog coord of Event                           */
    float   EvntLon;      /*  21- 28  23- 30 Geog coord of Event                           */
    float   EvntDepth;    /*  29- 33  31- 35 Depth of Event*100                            */
    float   Smag;         /*  34- 35  36- 38 Mag from max S amplitude from NCSN stations   */
    long    NumPhs;       /*  36- 38  39- 41 # phases used in solution (final weights > 0.1) */
    long    Gap;          /*  39- 41  42- 44 Maximum azimuthal gap                         */
    float   Dmin;         /*  42- 44  45- 47 Minimum distance to site                      */
    float   rms;          /*  45- 48  48- 51 RMS travel time residual*100                  */
    float   mag;          /*  67- 68  70- 72 Coda duration mag                             */
    float   erh;          /*  80- 83  85- 88 Horizontal error*100 (km)                     */
    float   erz;          /*  84- 87  89- 92 Vertical error*100 (km)                       */
    char    ExMagType;    /*     114     122 "External" magnitude label or type code       */
    float   ExMag;        /* 115-117 123-125 "External" magnitude                          */
    float   ExMagWgt;     /* 118-120 126-128 Total of the "external" mag weights (~ number of readings) */
    char    AltMagType;   /*     121     129 Alt amplitude magnitude label or type code    */
    float   AltMag;       /* 122-124 130-132 Alt amplitude magnitude                       */
    float   AltMagWgt;    /* 125-127 133-135 Total of the alt amplitude mag weights (~ number of readings) */
    char    EvntIDS[10];  /* 128-137 136-145 Event ID                                      */
    long    EvntID;       /* Event ID                                                      */
    char    PreMagType;   /*     138     146 Preferred magnitude label code chosen from those available */
    float   PreMag;       /* 139-141 147-149 Preferred magnitude                           */
    float   PreMagWgt;    /* 142-144 150-153 Total of the preferred mag weights (~ number of readings) */
    char    AltDmagType;  /*     145     154 Alt coda dur. magnitude label or type code    */
    float   AltDmag;      /* 146-148 155-157 Alt coda dur. magnitude                       */
    float   AltDmagWgt;   /* 149-151 158-161 Total of the alternate coda duration magnitude weights (~ number of readings) */

    long    MaxChannels;  /*  */
    long    index[MAXCHANNELS];
} Arkive;

/*****************************************************************************
 *  Define the structure for Station information.                            *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/

typedef struct StaInfo {       /* A channel information structure            */
    char    Site[6];           /* Site                                       */
    char    Net[5];            /* Net                                        */
    int     sernum;            /* Serial Number                              */
    double  logA;              /* Number of data streams for this site       */
    double  Lat;               /* Latitude                                   */
    double  Lon;               /* Longitude                                  */
    double  Elev;              /* Elevation                                  */
    double  Dist;              /* Distance                                   */
    double  Stime;             /* Start time for plot                        */
} StaInfo;

/*****************************************************************************
 *  Define the structure for Channel information.                            *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/

typedef struct ChanInfo {      /* A channel information structure            */
    char    Site[6];           /* Site                                       */
    char    Comp[5];           /* Component                                  */
    char    Net[5];            /* Net                                        */
    char    Loc[5];            /* Loc                                        */
    double  Lat;               /* Latitude                                   */
    double  Lon;               /* Longitude                                  */
    double  Elev;              /* Elevation                                  */
    
} ChanInfo;

/*****************************************************************************
 *  Define the structure for the individual Global thread.                   *
 *  This is the private area the thread needs to keep track                  *
 *  of all those variables unique to itself.                                 *
 *****************************************************************************/

struct Global {
    int     Debug;             /*                                            */
    pid_t   pid;
    char    mod[20];
    int     SecsToWait;    /* Number of seconds to wait before starting plot */

    
    
    
    int     NSCN;              /* Number of SCNs we know about               */
    ChanInfo    Chan[MAXCHANNELS];
    
    int     Nsta;              /* Number of SCNs we know about               */
    StaInfo    Sta[MAXCHANNELS];
    int     NumIndexedSta;
    int     StaIndex[MAXCHANNELS];
    
/* Variables used during the generation of each plot
 ***************************************************/
    double  MaxDistance;       /* Maximum offset distance for traces in this plot. */
    
    double  MinLogA;           /* Minimum LogA for selection of sites  */

    int     nStaDB;            /* number of station databases we know about          */
    char    stationList[MAX_STADBS][STALIST_SIZ];
    
    char    SerNumList[STALIST_SIZ];
    
    char    indir[GDIRSZ];     /* Directory for manual-input arc files               */
    int     arcfileflg;        /* Flag set if we are accepting arc files             */
    
    char    cubedir[GDIRSZ];     /* Directory for manual-input arc files               */
    int     cubefileflg;        /* Flag set if we are accepting arc files             */
    
    char    GifDir[GDIRSZ];    /* Directory for temp files on local machine */
    char    OutDir[GDIRSZ];    /* Directory for output files on local machine */
    long    MaxChannels;       /*                                                    */
    long    MaxDist;           /* Maximum offset distance for traces to plot.        */
    double  MinSize;           /* Minimum size event to report                       */
    long    MaxDays;           /* Maximum number of days in event list.              */

    char    Site[5];           /*  0- 3 Site                              */
    char    Comp[5];           /* Component                               */
    char    Net[5];            /* Net                                     */
    char    Loc[5];            /* Location                                */
        
    double  EvtLat;            /* Event Latitude                          */
    double  EvtLon;            /* Event Longitude                         */
    double  EvtMag;            /* Event Magnitude                         */
    double  EvtTime;           /* Time of Event (Julian Sec)              */
        
    Arkive  Ark;               /* Current arkive structure                */
};
typedef struct Global Global;

