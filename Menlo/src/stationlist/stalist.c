/**********************************************************************
  stalist.c

  Get the menu from wave_serverV and put info
  into a file to be sent to the webservers.
  Also, check the start and end times for consistency.
  Patterned after getmenu.
  
                           4/28/2001, 6/28/2002  Jim Luetgert
 **********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <platform.h> /* includes system-dependent socket header files */
#include <chron3.h>
#include <kom.h>
#include <earthworm.h>
#include <ws_clientII.h>

#include "stalist.h"

#ifndef INADDR_NONE
#define INADDR_NONE  0xffffffff  /* should be in <netinet/in.h> */
#endif

#define MAXNETS     20  /* Maximum number of networks              */

/* Function prototypes
   *******************/
void Decode_Time( double, TStrct *);
void Encode_Time( double *, TStrct *);
void date22( double, char *);
void hpsort(int n, ChanInfo ra[]);
void Get_Sta_Info(Global *But);
void LogWsErr( char [], int );
void config_me(Global *But, char* configfile );

/* Globals to set from configuration file
   **************************************/
char     MenuList[100];
int      numserv;                              /* Number of wave servers */
int      wsLoc[MAX_WAVESERVERS];
char     wsIp[MAX_WAVESERVERS][MAX_ADRLEN];
char     wsPort[MAX_WAVESERVERS][MAX_ADRLEN];  /* Available wave servers */
char     wsComment[MAX_WAVESERVERS][MAX_ADRLEN];  /* Available wave servers */
Global   But;
int      Debug;               /* non-zero -> debug logging            */

/* Constants
   *********/
const long   wsTimeout = 30000;                /* milliSeconds to wait for reply */

/* Time conversion factor for moving between
   Carl Johnson's seconds-since-1600 routines (chron3.c)
   and Solaris' and OS/2's seconds-since-1970 routines
   *****************************************************/
static char   Str1970[18] = "19700101000000.00";
static double Sec1970;
static char   Prog[10] = "stalist";


/**********************************************************************
  main

 **********************************************************************/
int main( int argc, char *argv[] )
{
    int     i, j, jj, k, kk, nm, dupe, rc, first_error, ierr, jerr;
    char    time1[25], time2[25];
    char    line[80], stime[80], etime[80], SCNnam[20], date[30];
    char    tname[175], fname[175], whoami[50], site[5];
    char    quot = '"';
    char    comptype[100][5], network[MAXNETS][5], string[120];
    int     ncomptype, ncomp[100], nnetwork, nnet[MAXNETS], array[100][MAXNETS];
    char    Inst_name[50][40], Sens_name[50][40];
    int     Inst_type[50], ninsttype, ninst[50], Sens_type[50], nsenstype, nsens[50];
    time_t  current_time;
    double  atime, tankStarttime, tankEndtime;
    TStrct  Time1;  
    FILE   *out;
    ChanInfo    MenuChan[MAXCHANNELS];
    WS_MENU_QUEUE_REC queue;

    sprintf(whoami, " %s:", Prog);
    queue.head = (WS_MENU) NULL;
    queue.tail = (WS_MENU) NULL;

  /* Calculate timebase conversion constant
  **************************************/
    Sec1970 = julsec17( Str1970 );

  /* Check command line arguments
  ****************************/
    if ( argc != 2 ) {
        printf( "Usage: %s config.d\n", Prog );
        return -1;
    }

    logit_init( Prog, (short) 0, 256, 0 );


  /* Zero the wave server arrays
  ***************************/
    for ( i = 0; i < MAX_WAVESERVERS; i++ ) {
        memset( wsIp[i], 0, MAX_ADRLEN );
        memset( wsPort[i], 0, MAX_ADRLEN );
        memset( wsComment[i], 0, MAX_ADRLEN );
    }

    /* Read the configuration file(s)
     ********************************/
    config_me(&But, argv[1] );

  /* Initialize the socket system
  ****************************/
    SocketSysInit();
    
    setWsClient_ewDebug(0);
    if(Debug) setWsClient_ewDebug(1);

  /* Build the current wave server menus
  ***********************************/
    for (i=0; i<numserv; i++ ) {
        rc = wsAppendMenu( wsIp[i], wsPort[i], &queue, wsTimeout );
        if ( rc != WS_ERR_NONE ) {
            LogWsErr( "wsAppendMenu", rc );
            logit("e", "%s Unable to Append %s:%s (%s) \n", whoami, wsIp[i], wsPort[i], wsComment[i]);
        }
    }

    if (queue.head == NULL ) {
      logit("e", "%s nothing in servers\n", whoami);
      exit( 0 );
    }
    
    Get_Sta_Info(&But);
    
    if(Debug) logit("e", "%s numserv: %d But.NSCN: %d \n", whoami, numserv, But.NSCN);
    
  /* Check for time problems
  ***************************/
    for(j=0; j<numserv; j++) {
        WS_PSCNL scnp;

        rc = wsGetServerPSCNL( wsIp[j], wsPort[j], &scnp, &queue );
        if ( rc == WS_ERR_EMPTY_MENU ) continue;
        if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) continue;

        first_error = 1;
		wsLoc[j] = (strlen(scnp->loc))? 1:0;
        while ( 1 ) {
	        tankStarttime = scnp->tankStarttime;
	        tankEndtime   = scnp->tankEndtime;
	        date22 (tankStarttime, time1);
	        date22 (tankEndtime,   time2);
	        if(wsLoc[j])
	        	sprintf(SCNnam, "%s_%s_%s_%s", scnp->sta, scnp->chan, scnp->net, scnp->loc);
	        else
		        sprintf(SCNnam, "%s_%s_%s", scnp->sta, scnp->chan , scnp->net);
	        if((tankEndtime - time(&current_time)) > 3600) {
	            if(first_error) {
		            logit("e", "Problem Channels for waveserver %s:%s (%s) \n", wsIp[j], wsPort[j], wsComment[j]);
		            first_error = 0;
	            }
	            logit("e", "Channel %s has EndTime (%s) in the future. \n", SCNnam, time2);
	        }
	        if(tankStarttime > tankEndtime) {
	            if(first_error) {
		            logit("e", "Problem Channels for waveserver %s:%s (%s) \n", wsIp[j], wsPort[j], wsComment[j]);
		            first_error = 0;
	            }
	            logit("e", "Channel %s has EndTime (%s) before StartTime (%s). \n", SCNnam, time2, time1);
	        }
	        if((time(&current_time) - tankEndtime) > 3600) {
	            if(first_error) {
		            logit("e", "Problem Channels for waveserver %s:%s (%s) \n", wsIp[j], wsPort[j], wsComment[j]);
		            first_error = 0;
	            }
	            logit("e", "Channel %s has EndTime: %s. Probably a dead channel. \n", SCNnam, time2);
	        }
        

            if ( scnp->next == NULL )
                break;
            else
                scnp = scnp->next;
        }
    }
    
    nm = 0;

  /* Print contents of all tanks
  ***************************/
    for(j=0; j<numserv; j++) {
        WS_PSCNL scnp;

        rc = wsGetServerPSCNL( wsIp[j], wsPort[j], &scnp, &queue );
        if ( rc == WS_ERR_EMPTY_MENU ) continue;
        if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) continue;

		wsLoc[j] = (strlen(scnp->loc))? 1:0;
        while ( 1 ) {
            date17( scnp->tankStarttime+Sec1970, line );
            for ( i = 0; i < 8; i++ ) stime[i] = line[i];
            stime[8] =  '_';
            for ( i = 8; i < 12; i++ ) stime[i+1] = line[i];
            stime[13] =  '_';
            for ( i = 12; i < 17; i++ ) stime[i+2] = line[i];
            stime[19] =  0;

            date17( scnp->tankEndtime+Sec1970, line );
            for ( i = 0; i < 8; i++ ) etime[i] = line[i];
            etime[8] =  '_';
            for ( i = 8; i < 12; i++ ) etime[i+1] = line[i];
            etime[13] =  '_';
            for ( i = 12; i < 17; i++ ) etime[i+2] = line[i];
            etime[19] =  0;

            if(strcmp(scnp->chan,  "T")   !=0  && 
               strcmp(scnp->chan,  "---" )!=0) {
                MenuChan[nm].Lat = MenuChan[nm].Lon = MenuChan[nm].Elev = 0.0;
                strcpy(MenuChan[nm].Site, scnp->sta);
                strcpy(MenuChan[nm].Comp, scnp->chan);
                strcpy(MenuChan[nm].Net,  scnp->net);
                sprintf(MenuChan[nm].SCNnam, "%s_%s_%s", scnp->sta, scnp->chan , scnp->net);
                strcpy(MenuChan[nm].SiteName, "");
                strcpy(MenuChan[nm].Loc,  "  ");
    
                for(jj=0;jj<But.NSCN;jj++) {
                    if(strcmp(But.Chan[jj].Site, scnp->sta )==0 && 
                       strcmp(But.Chan[jj].Comp, scnp->chan)==0 && 
                       strcmp(But.Chan[jj].Net,  scnp->net )==0) {
                        
                        if(wsLoc[j]==0)  {
    
    if(Debug) logit("e", "%s Non-loc code WS %s:%s (%s) \n", whoami, wsIp[j], wsPort[j], wsComment[j]);
    
	                        sprintf(SCNnam, "%s_%s_%s", scnp->sta, scnp->chan , scnp->net);
	                        strcpy(MenuChan[nm].SiteName, But.Chan[jj].SiteName);
	                        strcpy(MenuChan[nm].Loc,  But.Chan[jj].Loc);
	                  /*      if(strcmp(MenuChan[nm].Loc, "--") ==0) strcpy(MenuChan[nm].Loc,  "  "); */
	                        strcpy(MenuChan[nm].SCNLnam, But.Chan[jj].SCNLnam);
	                        MenuChan[nm].Lat  = But.Chan[jj].Lat;
	                        MenuChan[nm].Lon  = But.Chan[jj].Lon;
	                        MenuChan[nm].Elev  = But.Chan[jj].Elev;
	                        MenuChan[nm].Inst_type  = But.Chan[jj].Inst_type;
	                        MenuChan[nm].Sens_type  = But.Chan[jj].Sens_type;
	                        MenuChan[nm].Sens_gain  = But.Chan[jj].Sens_gain;
	                        MenuChan[nm].Sens_unit  = But.Chan[jj].Sens_unit;
	                        MenuChan[nm].sensitivity  = But.Chan[jj].sensitivity;
	                        MenuChan[nm].ShkQual    = But.Chan[jj].ShkQual;
	                        break;
                        }
                        
                        else if(strcmp(But.Chan[jj].Loc,  scnp->loc )==0 )  {
    
    if(Debug) logit("e", "%s %s %s %s \n", scnp->sta, scnp->chan , scnp->net, scnp->loc);
    
	                        sprintf(SCNnam, "%s_%s_%s_%s", scnp->sta, scnp->chan , scnp->net, scnp->loc);
	                        strcpy(MenuChan[nm].SiteName, But.Chan[jj].SiteName);
	                        strcpy(MenuChan[nm].Loc,  But.Chan[jj].Loc);
	                  /*      if(strcmp(MenuChan[nm].Loc, "--") ==0) strcpy(MenuChan[nm].Loc,  "  "); */
	                        strcpy(MenuChan[nm].SCNLnam, But.Chan[jj].SCNLnam);
	                        MenuChan[nm].Lat  = But.Chan[jj].Lat;
	                        MenuChan[nm].Lon  = But.Chan[jj].Lon;
	                        MenuChan[nm].Elev  = But.Chan[jj].Elev;
	                        MenuChan[nm].Inst_type  = But.Chan[jj].Inst_type;
	                        MenuChan[nm].Sens_type  = But.Chan[jj].Sens_type;
	                        MenuChan[nm].Sens_gain  = But.Chan[jj].Sens_gain;
	                        MenuChan[nm].Sens_unit  = But.Chan[jj].Sens_unit;
	                        MenuChan[nm].sensitivity = But.Chan[jj].sensitivity;
	                        MenuChan[nm].ShkQual     = But.Chan[jj].ShkQual;
                nm++;
	                        break;
                        }
                    }
                }
            }

            if ( scnp->next == NULL )
                break;
            else
                scnp = scnp->next;
        }
    }
    
    if(Debug) logit("et", "  %d channels. \n", nm); 
    hpsort(nm, &MenuChan[-1]);
    
    i = 0;
    do {
        for(j=i+1;j<nm;j++) {
            if(strcmp(MenuChan[i].SCNLnam, MenuChan[j].SCNLnam )==0) {
                for(k=j;k<nm-1;k++) MenuChan[k] = MenuChan[k+1]; 
                nm--;
                j--;
            }
        }
        i++;
    } while(i<nm);
    
    logit("et", "  %d channels. \n", nm); 
        
    sprintf(fname, "%s%s", But.GifDir, MenuList);

    for(i=0;i<But.nltargets;i++) {
	    out = fopen(fname, "wb");
        if(out == 0L) {
            logit("e", "%s Unable to open MenuList File: %s\n", whoami, fname);    
        } else {
		    fprintf(out, "SITE  COMP NET LOC  LATITUDE   LONGITUDE  ELEV    DESCRIPTION \n"); 
		    fprintf(out, "                                          (M) \n \n"); 

		    for(j=0;j<nm;j++) {
		   /*   fprintf(out, "%-5s  %-3s  %-2s  %f %f %4.0f    %s \n", */
		        fprintf(out, "%-5s  %-3s  %-2s  %-2s  %8.2f %11.2f %4.0f    %s \n", 
		        MenuChan[j].Site, MenuChan[j].Comp, MenuChan[j].Net, MenuChan[j].Loc, 
		        MenuChan[j].Lat,  MenuChan[j].Lon,  MenuChan[j].Elev, MenuChan[j].SiteName); 
		    }
		    
		    atime = time(&current_time);
		    Decode_Time( atime, &Time1);
		    Encode_Time( &Time1.Time, &Time1);
		    date22 (Time1.Time, date);
		    
		    fprintf(out, "\n This table last updated %.11s  \n", date);
		/*  fprintf(out, "\n This table last updated %.16s UTC \n", date);   */
		    
		    fclose(out);
            sprintf(tname,  "%s%s", But.loctarget[i], MenuList );
            ierr = rename( fname, tname );
                /* The following silliness is necessary to be Windows compatible */ 
            if( ierr != 0 ) {
                if(Debug) logit( "e", "error moving file %s to %s\n", fname, tname );
                if( remove( tname ) != 0 ) {
                    logit("e","error deleting file %s\n", tname);
                } else  {
                    if(Debug) logit("e","deleted file %s.\n", tname);
                    jerr = rename( fname, tname );
                    if( jerr != 0 ) {
                        logit( "e", "error moving file %s to %s; ierr = %d\n", fname, tname, ierr );
                    } else {
                        if(Debug) logit("e","%s moved to %s\n", fname, tname );
                    }
                }
            } else {
                logit("e","%s moved to %s\n", fname, But.loctarget[i] );
            }
	    }
	}

  /* Now, audit the database
   *************************/
    for(j=0;j<nm;j++) {
        if(MenuChan[j].Lat==0.0 && MenuChan[j].Lon==0.0 && MenuChan[j].Elev==0.0) {
            logit("e", "Served Channel %s not in the database. \n", MenuChan[j].SCNLnam);
        }
    }

    for(i=0;i<But.NSCN;i++) {
        dupe = 0;
        for(j=0;j<nm;j++) {
            if(strcmp(But.Chan[i].SCNLnam, MenuChan[j].SCNLnam )==0) dupe = 1;
        }
        if(!dupe) logit("e", "Database entry %s not being served. \n", But.Chan[i].SCNLnam);
    }
    
    ncomptype = nnetwork = 0;
	for(i=0;i<100;i++) strcpy(comptype[i], "XXX");
	for(i=0;i<100;i++) ncomp[i] = 0;
	for(i=0;i<MAXNETS;i++) nnet[i] = 0;
	for(i=0;i<100;i++) {
		for(j=0;j<MAXNETS;j++) array[i][j] = 0;
	}

    /* Break out component types by network. */
    for(j=0;j<nm;j++) {
    	jj = -1;
    	for(i=0;i<ncomptype;i++) {
            if(strcmp(comptype[i], MenuChan[j].Comp )==0) jj = i;
    	}
    	if(jj == -1) {
            jj = i = ncomptype++;
            strcpy(comptype[i], MenuChan[j].Comp);
    	}
    	ncomp[jj]++;
    	kk = -1;
    	for(i=0;i<nnetwork;i++) {
            if(strcmp(network[i], MenuChan[j].Net )==0) kk = i;
    	}
    	if(kk == -1) {
            kk = i = nnetwork++;
            strcpy(network[i], MenuChan[j].Net);
    	}
    	nnet[kk]++;
    	array[jj][kk]++;
    }
logit("e", "\n  %d networks. \n", nnetwork); 
logit("e", "  %d component types. \n\n", ncomptype); 
    
    logit("e", "         ");
    for(i=0;i<nnetwork;i++) logit("e","%s   ", network[i]);
    logit("e", "Total \n");
    for(j=0;j<ncomptype;j++) {
	    logit("e", " %s   ", comptype[j]); 
	    for(i=0;i<nnetwork;i++) {
		    array[ncomptype][i] +=  array[j][i];
		    logit("e", " %3d ", array[j][i]); 
	    }
	    logit("e", " %3d  \n", ncomp[j]);
    }
    logit("e", " %s ", "total"); 
    for(i=0;i<nnetwork;i++) logit("e", " %3d ", array[ncomptype][i]);
    logit("e", "%4d  \n", nm);
        
    /* Break out sensor types by network. */
    nsenstype = 0;
	for(i=0;i<50;i++) nsens[i] = 0;
	for(i=0;i<MAXNETS;i++) nnet[i] = 0;
	for(i=0;i<100;i++) {
		for(j=0;j<MAXNETS;j++) array[i][j] = 0;
	}

    for(j=0;j<nm;j++) {
    	jj = -1;
    	for(i=0;i<nsenstype;i++) {
            if(Sens_type[i] == MenuChan[j].Sens_type ) jj = i;
    	}
    	if(jj == -1) {
            jj = i = nsenstype++;
            Sens_type[i] = MenuChan[j].Sens_type;
    	}
    	/*
        if(MenuChan[j].Sens_type < 1 || MenuChan[j].Sens_type > 18) 
        	logit("e", "    %s %f %f %f %d %f  %d %f  %d  %f %d\n", 
        		MenuChan[j].SCNnam, MenuChan[j].Lat, 
        		MenuChan[j].Lon, MenuChan[j].Elev, MenuChan[j].Inst_type, 
        		MenuChan[j].Inst_gain, MenuChan[j].Sens_type, MenuChan[j].Sens_gain, 
        		MenuChan[j].Sens_unit, MenuChan[j].SiteCorr, MenuChan[j].ShkQual);
    	*/
    	nsens[jj]++;
    	kk = -1;
    	for(i=0;i<nnetwork;i++) {
            if(strcmp(network[i], MenuChan[j].Net )==0) kk = i;
    	}
    	if(kk == -1) {
            kk = i = nnetwork++;
            strcpy(network[i], MenuChan[j].Net);
    	}
    	nnet[kk]++;
    	array[jj][kk]++;
    } 
logit("e", "\n  %d networks. \n", nnetwork); 
logit("e", "  %d sensor types. \n", nsenstype); 
    strcpy(Sens_name[0], "Unknown           ");
    strcpy(Sens_name[1], "L4C               ");
    strcpy(Sens_name[2], "FBA-23            ");
    strcpy(Sens_name[3], "Guralp CMG40T-1   ");
    strcpy(Sens_name[4], "HS1               ");
    strcpy(Sens_name[5], "Wilcoxon          ");
    strcpy(Sens_name[6], "Dilatometer       ");
    strcpy(Sens_name[7], "STS-1             ");
    strcpy(Sens_name[8], "STS-2             ");
    strcpy(Sens_name[9], "Episensor         ");
    strcpy(Sens_name[10], "S-13              ");
    strcpy(Sens_name[11], "Guralp CMG3ESP    ");
    strcpy(Sens_name[12], "Reftek 130        ");
    strcpy(Sens_name[13], "Ranger SS-1       ");
    strcpy(Sens_name[14], "Teledyne SD212    ");
    strcpy(Sens_name[15], "Benioff           ");
    strcpy(Sens_name[16], "Sprengnether S6000");
    strcpy(Sens_name[18], "SH-1/SV-1         ");
    strcpy(Sens_name[18], "Oyo GS-11         ");
    strcpy(Sens_name[19], "L22               ");
    strcpy(Sens_name[20], "HS10              ");
    strcpy(Sens_name[21], "Benioff           ");
    strcpy(Sens_name[22], "Guralp CMG40T-2   ");
    strcpy(Sens_name[23], "Time-code         ");
    strcpy(Sens_name[24], "SL210/220         ");
    strcpy(Sens_name[25], "FBA-11            ");
    strcpy(Sens_name[26], "HS1-LT            ");
    strcpy(Sens_name[27], "GS-20DM           ");
    strcpy(Sens_name[28], "S-13.pge          ");
    strcpy(Sens_name[29], "SM-6              ");
    strcpy(Sens_name[30], "Guralp CMG40T-3   ");
    strcpy(Sens_name[31], "Guralp SAFOD      ");
    strcpy(Sens_name[32], "GeoSig MEMS       ");
    logit("e", "                     ");
    for(i=0;i<nnetwork;i++) logit("e","%s   ", network[i]);
    logit("e", "Total \n");
    for(j=0;j<nsenstype;j++) {
	    logit("e", " %s", Sens_name[Sens_type[j]]); 
	    for(i=0;i<nnetwork;i++) {
		    array[nsenstype][i] +=  array[j][i];
		    logit("e", " %3d ", array[j][i]); 
	    }
	    logit("e", "    %3d  \n", nsens[j]);
    }
    logit("e", " %s             ", "total"); 
    for(i=0;i<nnetwork;i++) logit("e", " %3d ", array[nsenstype][i]);
    logit("e", "   %4d  \n", nm);
    
    /* Save only one record per physical datalogger. */
    i = 0;
    do {
        for(j=i+1;j<nm;j++) {
            if(strcmp(MenuChan[i].Site, MenuChan[j].Site )==0 && 
               strcmp(MenuChan[i].Net,  MenuChan[j].Net  )==0 &&
               MenuChan[i].Inst_type == MenuChan[j].Inst_type  ) {
                for(k=j;k<nm-1;k++) MenuChan[k] = MenuChan[k+1];
                nm--;
                j--;
            }
        }
        i++;
    } while(i<nm);
    
    logit("e", " \n  %d Stations. \n", nm); 
    
    /* Break out datalogger types by network. */
    ninsttype = nnetwork = 0;
	for(i=0;i<100;i++) ninst[i] = 0;
	for(i=0;i<MAXNETS;i++) nnet[i] = 0;
	for(i=0;i<100;i++) {
		for(j=0;j<MAXNETS;j++) array[i][j] = 0;
	}

    for(j=0;j<nm;j++) {
    	jj = -1;
    	for(i=0;i<ninsttype;i++) {
            if(Inst_type[i] == MenuChan[j].Inst_type ) jj = i;
    	}
    	if(jj == -1) {
            jj = i = ninsttype++;
            Inst_type[i] = MenuChan[j].Inst_type;
    	}
    	ninst[jj]++;
    	kk = -1;
    	for(i=0;i<nnetwork;i++) {
            if(strcmp(network[i], MenuChan[j].Net )==0) kk = i;
    	}
    	if(kk == -1) {
            kk = i = nnetwork++;
            strcpy(network[i], MenuChan[j].Net);
    	}
    	nnet[kk]++;
    	array[jj][kk]++;
    	/*
        if(MenuChan[j].Inst_type == 4) 
        	logit("e", "    %s %s %s %f %f %f %d %f  %d %f  %d  %f %d\n", 
        		MenuChan[j].SCNnam, MenuChan[j].Site, MenuChan[j].Net, MenuChan[j].Lat, 
        		MenuChan[j].Lon, MenuChan[j].Elev, MenuChan[j].Inst_type, 
        		MenuChan[j].Inst_gain, MenuChan[j].Sens_type, MenuChan[j].Sens_gain, 
        		MenuChan[j].Sens_unit, MenuChan[j].SiteCorr, MenuChan[j].ShkQual);
    	*/
    }
logit("e", "  %d networks. \n", nnetwork); 
logit("e", "  %d datalogger types. \n", ninsttype); 
    strcpy(Inst_name[0], "Unknown        ");
    strcpy(Inst_name[1], "PG&E Analog    ");
    strcpy(Inst_name[1], "Analog         ");
    strcpy(Inst_name[2], "DST            ");
    strcpy(Inst_name[3], "RefTek         ");
    strcpy(Inst_name[4], "Nanometrics    ");
    strcpy(Inst_name[5], "Quanterra      ");
    strcpy(Inst_name[6], "K2             ");
    strcpy(Inst_name[7], "Anadig HRD24   ");
    strcpy(Inst_name[8], "NN digital     ");
    strcpy(Inst_name[9], "Reftek 130     ");
    strcpy(Inst_name[10], "Anadig Trident ");
    strcpy(Inst_name[11], "Anadig Lynx    ");
    strcpy(Inst_name[12], "Nanometrics    ");
    strcpy(Inst_name[13], "Nanometrics    ");
    strcpy(Inst_name[14], "Quanterra Q4120");
    strcpy(Inst_name[15], "Quanterra Q730 ");
    strcpy(Inst_name[16], "RefTek 72A-08  ");
    strcpy(Inst_name[17], "Quanterra Q80  ");
    strcpy(Inst_name[18], "GERI           ");
    strcpy(Inst_name[19], "Taurus         ");
    strcpy(Inst_name[20], "Etna           ");
    strcpy(Inst_name[21], "MW             ");
    strcpy(Inst_name[22], "Guralp SAFOD   ");
    strcpy(Inst_name[23], "GSR-IA18       ");
    strcpy(Inst_name[24], "Makalu         ");
    strcpy(Inst_name[25], "Basalt         ");
    strcpy(Inst_name[26], "Unknown        ");
    strcpy(Inst_name[27], "Unknown        ");
    strcpy(Inst_name[28], "Unknown        ");
    strcpy(Inst_name[29], "Unknown        ");
    strcpy(Inst_name[30], "Unknown        ");
    logit("e", "                  ");
    for(i=0;i<nnetwork;i++) logit("e","%s   ", network[i]);
    logit("e", "Total \n");
    for(j=0;j<ninsttype;j++) {
	    logit("e", " %s", Inst_name[Inst_type[j]]); 
	    for(i=0;i<nnetwork;i++) {
		    array[ninsttype][i] +=  array[j][i];
		    logit("e", " %3d ", array[j][i]); 
	    }
	    logit("e", " %3d  \n", ninst[j]);
    }
    logit("e", " %s      ", "total    "); 
    for(i=0;i<nnetwork;i++) logit("e", " %3d ", array[ninsttype][i]);
    logit("e", " %3d  \n", nm);
        
    

  /* Release the linked list created by wsAppendMenu
   *************************************************/
    wsKillMenu( &queue );

    return 0;
}


/**********************************************************************
 * Decode_Time : Decode time from seconds since 1970                  *
 *                                                                    *
 **********************************************************************/
void Decode_Time( double secs, TStrct *Time)
{
    struct Greg  g;
    long    minute;
    double  sex;

    Time->Time = secs;
    secs += Sec1970;
    Time->Time1600 = secs;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    Time->Year  = g.year;
    Time->Month = g.month;
    Time->Day   = g.day;
    Time->Hour  = g.hour;
    Time->Min   = g.minute;
    Time->Sec   = sex;
}


/**********************************************************************
 * Encode_Time : Encode time to seconds since 1970                    *
 *                                                                    *
 **********************************************************************/
void Encode_Time( double *secs, TStrct *Time)
{
    struct Greg    g;

    g.year   = Time->Year;
    g.month  = Time->Month;
    g.day    = Time->Day;
    g.hour   = Time->Hour;
    g.minute = Time->Min;
    *secs    = 60.0 * (double) julmin(&g) + Time->Sec - Sec1970;
}


/**********************************************************************
 * date22 : Calculate 22 char date in the form Jan23,1988 12:34 12.21 *
 *          from the julian seconds.  Remember to leave space for the *
 *          string termination (NUL).                                 *
 **********************************************************************/
void date22( double secs, char *c22)
{
    char *cmo[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    struct Greg  g;
    long    minute;
    double  sex;

    secs += Sec1970;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    sprintf(c22, "%3s%2d,%4d %.2d:%.2d:%05.2f",
            cmo[g.month-1], g.day, g.year, g.hour, g.minute, sex);
}


/*******************************************************************************
 *    hpsort performs a sifting sort in place of the n members of the          *
 *    array ra.  This is from Recipes in C, so it is assumed that the array    *
 *    is 1-based as in fortran rather than 0-based as in C!!                   *
 *******************************************************************************/

void hpsort(int n, ChanInfo ra[])
{
    int   i, j, k, ir;
    ChanInfo rra;
    
    if(n<2) return;
    k  = (n >> 1)+1;
    ir = n;
        
    for(;;) {
        if(k > 1) rra = ra[--k];
        else {
            rra = ra[ir];
            ra[ir] = ra[1];
            if(--ir == 1) {
                ra[1] = rra;
                break;
            }
        }
        i = k; j = k+k;
        while(j<=ir) {
            if(j<ir && strcmp(ra[j].Site, ra[j+1].Site)<0) j++;
            if(strcmp(rra.Site, ra[j].Site)<0) {    
                ra[i] = ra[j];
                i = j;
                j <<= 1;
            } else break;
        }
        ra[i] = rra;
    }
}


/****************************************************************************
 *  Get_Sta_Info(Global *But);                                              *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Sta_Info(Global *But)
{
    char    whoami[50], *com, *str;
    int     i, j, k, nfiles, success, type, sensor, units;
    double  dlat, mlat, dlon, mlon, gain, sens, ssens;

    sprintf(whoami, "%s: %s: ", Prog, "Get_Sta_Info");
    
    But->NSCN = 0;
    for(k=0;k<But->nStaDB;k++) {
            /* Open the main station file
             ****************************/
        nfiles = k_open( But->stationList[k] );
        if(nfiles == 0) {
            fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, But->stationList[k] );
            exit( -1 );
        }

            /* Process all command files
             ***************************/
        while(nfiles > 0) {  /* While there are command files open */
            while(k_rd())  {      /* Read next line from active file  */
                com = k_str();         /* Get the first token from line */

                    /* Ignore blank lines & comments
                     *******************************/
                if( !com )           continue;
                if( com[0] == '#' )  continue;

                    /* Open a nested configuration file
                     **********************************/
                if( com[0] == '@' ) {
                    success = nfiles+1;
                    nfiles  = k_open(&com[1]);
                    if ( nfiles != success ) {
                        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                        exit( -1 );
                    }
                    continue;
                }

                /* Process anything else as a channel descriptor
                 ***********************************************/

                if( But->NSCN >= MAXCHANNELS ) {
                    fprintf(stderr, "%s Too many channel entries in <%s>", 
                             whoami, But->stationList[k] );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAXCHANNELS );
                    exit( -1 );
                }
                j = But->NSCN;
                
                    /* S C N */
                strncpy( But->Chan[j].Site, com,  6);
                str = k_str();
                if(str) strncpy( But->Chan[j].Net,  str,  2);
                str = k_str();
                if(str) strncpy( But->Chan[j].Comp, str, 3);
                str = k_str();
                if(str) strncpy( But->Chan[j].Loc, str, 3);
                for(i=0;i<6;i++) if(But->Chan[j].Site[i]==' ') But->Chan[j].Site[i] = 0;
                for(i=0;i<2;i++) if(But->Chan[j].Net[i]==' ')  But->Chan[j].Net[i]  = 0;
                for(i=0;i<3;i++) if(But->Chan[j].Comp[i]==' ') But->Chan[j].Comp[i] = 0;
                for(i=0;i<2;i++) if(But->Chan[j].Loc[i]==' ')  But->Chan[j].Loc[i]  = 0;
                But->Chan[j].Comp[3] = But->Chan[j].Net[2] = But->Chan[j].Site[5] = But->Chan[j].Loc[2] = 0;
                sprintf(But->Chan[j].SCNnam, "%s_%s_%s", 
                        But->Chan[j].Site, But->Chan[j].Comp , But->Chan[j].Net);
           /*     if(strcmp(But->Chan[j].Loc, "--") ==0) strcpy(But->Chan[j].Loc,  "  ");  */
                sprintf(But->Chan[j].SCNLnam, "%s_%s_%s_%s", 
                        But->Chan[j].Site, But->Chan[j].Comp, But->Chan[j].Net, But->Chan[j].Loc);


                    /* Lat Lon Elev */
                But->Chan[j].Lat  = k_val();
                But->Chan[j].Lon  = k_val();
                But->Chan[j].Elev = k_val();
                
                But->Chan[j].Inst_type   = k_int();
                But->Chan[j].Inst_gain   = k_val();
                gain   = k_val();
                But->Chan[j].Sens_type = k_int();
                But->Chan[j].Sens_unit  = k_int();
                But->Chan[j].Sens_gain  = k_val();
                if(But->Chan[j].Sens_unit == 3) But->Chan[j].Sens_gain /= 981.0;
                But->Chan[j].SiteCorr = k_val();
                But->Chan[j].ShkQual  = k_int();
                        
               
                But->Chan[j].sensitivity = (1000000.0*But->Chan[j].Sens_gain/But->Chan[j].Inst_gain)*gain*But->Chan[j].SiteCorr;    /*    sensitivity counts/units        */
                
                if (k_err()) {
                    fprintf( stderr, "%s Error decoding line in station file\n%s\n  exiting!\n", whoami, k_get() );
                    exit( -1 );
                }
         /*>Comment<*/
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].SiteName, str );
                    
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].Descript, str );
                    
                But->NSCN++;
            }
            nfiles = k_close();
        }
    }
}

/****************************************************************************
 *      LogWsErr prints waveserver errors.                                  *
 ****************************************************************************/

void LogWsErr( char fun[], int rc )
{
  switch ( rc ) {
    case WS_ERR_INPUT:
      printf( "%s: Bad input parameters.\n", fun );
      break;

    case WS_ERR_EMPTY_MENU:
      printf( "%s: Empty menu.\n", fun );
      break;

    case WS_ERR_SERVER_NOT_IN_MENU:
      printf( "%s: Empty menu.\n", fun );
      break;

    case WS_ERR_SCNL_NOT_IN_MENU:
      printf( "%s: SCN not in menu.\n", fun );
      break;

    case WS_ERR_BUFFER_OVERFLOW:
      printf( "%s: Buffer overflow.\n", fun );
      break;

    case WS_ERR_MEMORY:
      printf( "%s: Out of memory.\n", fun );
      break;

    case WS_ERR_BROKEN_CONNECTION:
      printf( "%s: The connection broke.\n", fun );
      break;

    case WS_ERR_SOCKET:
      printf( "%s: Could not get a connection.\n", fun );
      break;

    case WS_ERR_NO_CONNECTION:
      printf( "%s: Could not get a connection.\n", fun );
      break;

    default:
      printf( "%s: unknown ws_client error: %d.\n", fun, rc );
      break;
    }

  return;
}

/****************************************************************************
 *      config_me() process command file using kom.c functions              *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void config_me(Global *But, char* configfile )
{
    char    whoami[50], *com, *str;
    char    init[20];
    int     i, n, nfiles, ncommand, nmiss, success;

    sprintf(whoami, " %s: %s: ", Prog, "config_me");
        /* Set one init flag to zero for each required command
         *****************************************************/
    ncommand = 4;
    for(i=0; i<ncommand; i++ )  init[i] = 0;
    numserv = 0;
    Debug = 0;
    But->nStaDB = 0;
    But->nltargets = 0;
    
        /* Open the main configuration file
         **********************************/
    nfiles = k_open( configfile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, configfile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

                /* Process anything else as a command
                 ************************************/
            
         /* wave server addresses and port numbers to get trace snippets from
          *******************************************************************/
/*0*/          
            else if( k_its("WaveServer") ) {
                if ( numserv >= MAX_WAVESERVERS ) {
                    fprintf( stderr, "%s Too many <WaveServer> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_WAVESERVERS );
                    return;
                }
                if( (long)(str=k_str()) != 0 )  strcpy(wsIp[numserv],str);
                if( (long)(str=k_str()) != 0 )  strcpy(wsPort[numserv],str);
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy(wsComment[numserv],str);
                numserv++;
                init[0]=1;
            }

                /* get menu list path/name
                *****************************/
/*1*/
            else if( k_its("MenuList") ) {
                str = k_str();
                if( (int)strlen(str) >= 100) {
                    fprintf( stderr, "%s Fatal error. Menu list name %s greater than %d char.\n", 
                            whoami, str, 100);
                    exit(-1);
                }
                if(str) strcpy( MenuList , str );
                init[1] = 1;
            }

                /* get station list path/name
                *****************************/
/*2*/
            else if( k_its("StationList") ) {
                if ( But->nStaDB >= MAX_STADBS ) {
                    fprintf( stderr, "%s Too many <StationList> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_STADBS );
                    return;
                }
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->stationList[But->nStaDB] , str );
                But->nStaDB++;
                 init[2] = 1;   
            }

        /* get local directory path/name
        *****************************/
/*3*/
            else if( k_its("GifDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. Gif directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                if(str) strcpy( But->GifDir , str );
                init[3] = 1;
            }

         /* get the local target directory(s)
        ************************************/
            else if( k_its("LocalTarget") ) {
                if ( But->nltargets >= MAX_TARGETS ) {
                    logit("e", "%s Too many <LocalTarget> commands in <%s>", 
                             whoami, configfile );
                    logit("e", "; max=%d; exiting!\n", (int) MAX_TARGETS );
                    return;
                }
                if( (long)(str=k_str()) != 0 )  {
                    n = strlen(str);   /* Make sure directory name has proper ending! */
                    if( str[n-1] != '/' ) strcat(str, "/");
                    strcpy(But->loctarget[But->nltargets], str);
                }
                But->nltargets += 1;
            }
            else if( k_its("Debug") ) {
                Debug = 1;
            }


                /* At this point we give up. Unknown thing.
                *******************************************/
            else {
                fprintf(stderr, "%s <%s> Unknown command in <%s>.\n",
                         whoami, com, configfile );
                continue;
            }

                /* See if there were any errors processing the command
                 *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "%s Bad <%s> command  in <%s>; exiting!\n",
                             whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

        /* After all files are closed, check init flags for missed commands
         ******************************************************************/
    nmiss = 0;
    for(i=0;i<ncommand;i++)  if( !init[i] ) nmiss++;
    if ( nmiss ) {
        fprintf( stderr, "%s ERROR, no ", whoami);
        if ( !init[0] )  fprintf( stderr, "<WaveServer> "   );
        if ( !init[1] )  fprintf( stderr, "<MenuList> "     );
        if ( !init[2] )  fprintf( stderr, "<StationList> "  );
        if ( !init[3] )  fprintf( stderr, "<GifDir> "       );
        fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
        exit( -1 );
    }
}


