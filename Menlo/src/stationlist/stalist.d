########################################################################
# This is the stalist parameter file. 
# This module creates a list of currently served channels.
#
#                                   4/28/2001, 6/25/02    Jim Luetgert
########################################################################

# List of wave servers (ip port comment) to contact to retrieve trace data.

@/home/earthworm/run/params/all_waveservers.d
#  WaveServer     130.118.43.34   16010     " wsv3 other"

# The file with all the station information.
 StationList     /home/earthworm/run/params/calsta.db1

# Directory in which to store the temporary info file.
 GifDir   /home/picker/

# Directory in which to put info file for output.
 LocalTarget   /home/picker/sendfiler/stalist/
 LocalTarget   /home/picker/sendfiles/stalist/

# The file with all the waveserver stations.
 MenuList     Station_List

