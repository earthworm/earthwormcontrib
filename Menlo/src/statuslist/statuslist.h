/* Include file for statuslist */
/**************************************************************************
 *  defines                                                               *
 **************************************************************************/

#define MAXCHANNELS     3600  /* Maximum number of channels              */
#define MAX_ADRLEN        50

#define MAX_STADBS        20  /* Maximum number of Station database files */
#define MAX_WAVESERVERS   60  /* Maximum number of Waveservers            */
#define MAX_TARGETS       10  /* largest number of targets                */

#define GDIRSZ           132  /* Size of string for GIF target directory  */
#define STALIST_SIZ      100  /* Size of string for station list file     */

/**************************************************************************
 *  Define the structure for time records.                                *
 **************************************************************************/

typedef struct TStrct {   
    double  Time1600; /* Time (Sec since 1600/01/01 00:00:00.00)          */
    double  Time;     /* Time (Sec since 1970/01/01 00:00:00.00)          */
    int     Year;     /* Year                                             */
    int     Month;    /* Month                                            */
    int     Day;      /* Day                                              */
    int     Hour;     /* Hour                                             */
    int     Min;      /* Minute                                           */
    double  Sec;      /* Second                                           */
} TStrct;

/*********************************************************************
    Define the structure for Channel information.
    This is an abbreviated structure.
*********************************************************************/

typedef struct ChanInfo {  /* A channel information structure         */
    char    Site[6];           /* Site                                    */
    char    Comp[5];           /* Component                               */
    char    Net[5];            /* Net                                     */
    char    Loc[5];            /* Loc                                     */
    char    SCN[15];           /* SCN                                     */
    char    SCNtxt[17];        /* S C N                                   */
    char    SCNnam[17];        /* S_C_N                                   */
    char    SiteName[50];      /* Common Name of Site                     */
    char    Descript[100];     /* Common Name of Site                     */
    double  Lat;               /* Latitude                                */
    double  Lon;               /* Longitude                               */
    double  Elev;              /* Elevation                               */
    
    int		Inst_type;         /* Type of instrument                      */
    double  Inst_gain;         /* Gain of instrument (microv/count)       */
    int		Sens_type;         /* Type of sensor                          */
    double  Sens_gain;         /* Gain of sensor (volts/unit)             */
    int		Sens_unit;         /* Sensor units d=1; v=2; a=3              */
    double  SiteCorr;          /* Site correction factor.                 */
    double  sensitivity;       /* Channel sensitivity  counts/units       */
    int		ShkQual;           /* Station (Chan) type                     */
    
    int     priority;          /* 0->OK 1->suspect 2->bad                 */
    int     errtype[10];       /*                                         */
} ChanInfo;

/*********************************************************************
    Define the structure for FlatLine Channel information.
    This is an abbreviated structure.
*********************************************************************/

typedef struct FlatInfo {  /* A channel information structure         */
    char    Site[6];           /* Site                                    */
    char    Comp[5];           /* Component                               */
    char    Net[5];            /* Net                                     */
    char    Loc[5];            /* Loc                                     */
    
    int		diff;              /* Max - Min                               */
    double  StdDev;            /* Standard Deviation                      */
    double  Rat;               /* diff/StdDev                             */
    
    int     priority;          /* 0->OK 1->suspect 2->bad                 */
    int     errtype[10];       /*                                         */
} FlatInfo;

/**************************************************************************
 *  Define the structure for the individual Butler thread.                *
 *  This is the private area the thread needs to keep track               *
 *  of all those variables unique to itself.                              *
 **************************************************************************/

struct Butler {
    int     nStaDB;          /* number of station databases we know about   */
    char    stationList[MAX_STADBS][STALIST_SIZ];
    int     nFlat;           /* number of FlatLine outputs we know about    */
    char    FlatList[MAX_STADBS][STALIST_SIZ];
    char    MenuList[STALIST_SIZ];
    pid_t   pid;
    int     status;
    WS_MENU_QUEUE_REC menu_queue[MAX_WAVESERVERS];
    
    int		nflatlines;       /* Number of SCNs we know about               */
    FlatInfo	Flat[MAXCHANNELS];
    
    int		NSCN;             /* Number of SCNs we know about               */
    ChanInfo	Chan[MAXCHANNELS];

    int     nltargets;         /* Number of local target directories        */
    char    loctarget[MAX_TARGETS][100];/* Target in form-> /directory/     */
    
    char    GifDir[GDIRSZ];    /* Directory for storage of .gif & .html on local machine */
    int     LoBound;           /* Lower bound for flagging                   */
    int     MidBound;          /* Middle bound for flagging                   */
    int     HiBound;           /* Upper bound for flagging                   */
    double  RatBound;
    double  StdBound;
};
typedef struct Butler Butler;

