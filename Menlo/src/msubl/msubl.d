#
# This is the msubl parameter file. This module gets "trigger" messages
# from the hypo_rings, and initiates threads to make record sections.

#  Basic Earthworm setup:
#
LogSwitch	 1              # 0 to completely turn off disk log file
MyModuleId	 MOD_MSUBL       # module id for this instance of report 

RingName	 HYPO_RING      # ring to get input from
HeartBeatInt     5          # seconds between heartbeats

# List the message logos to grab from transport ring
#           Installation    Module      Message Type
 GetSumFrom  INST_WILDCARD   MOD_EQPROC  TYPE_HYP2000ARC

#	others
MaxMsgSize     500	# length of largest message we'll ever handle - in bytes
MaxMessages   1000	# limit of number of message to buffer

# We accept a command "Passes" which issues a heartbeat-type logit
# to let us know msubl is alive (debugging)
 Passes   800

# We accept a command "NoFlush" which prevents the ring from being
# flushed on restart.
  NoFlush
#
# A list of wave servers is supplied.
# We interrogate all wave servers as to what they have, and do the best we can
# to get as many traces as possible.

wsTimeout 60 # time limit (secs) for any one interaction with a wave server.

# List of wave servers (ip port) to contact to retrieve trace data.
  WaveServer     130.118.43.4    16024      "wsv2 - Reftek"
  WaveServer     130.118.43.4    16025     # wsv2 nano
  WaveServer     130.118.43.4    16026     # wsv2 dst
  WaveServer     130.118.43.4    16027     # wsv2 ucb
  WaveServer     130.118.43.4    16031     # wsv2 ad1
  WaveServer     130.118.43.4    16032     # wsv2 ad2
  WaveServer     130.118.43.4    16033     # wsv2 ad3
  WaveServer     130.118.43.4    16034     # wsv2 ad4
  WaveServer     130.118.43.4    16035     # wsv2 ad5
  WaveServer     130.118.43.4    16036     # wsv2 ad6
  WaveServer     130.118.43.4    16037     # wsv2 ad7
  WaveServer     130.118.43.4    16038     # wsv2 ad8
  WaveServer     130.118.43.4    16039     # wsv2 ad9

  WaveServer     130.118.43.3    16024      "wsv1 - Reftek"
  WaveServer     130.118.43.3    16025     # wsv1 nano
  WaveServer     130.118.43.3    16026     # wsv1 dst
  WaveServer     130.118.43.3    16027     # wsv1 ucb
  WaveServer     130.118.43.3    16031     # wsv1 ad1
  WaveServer     130.118.43.3    16032     # wsv1 ad2
  WaveServer     130.118.43.3    16033     # wsv1 ad3
  WaveServer     130.118.43.3    16034     # wsv1 ad4
  WaveServer     130.118.43.3    16035     # wsv1 ad5
  WaveServer     130.118.43.3    16036     # wsv1 ad6
  WaveServer     130.118.43.3    16037     # wsv1 ad7
  WaveServer     130.118.43.3    16038     # wsv1 ad8
  WaveServer     130.118.43.3    16039     # wsv1 ad9

 MlStations     /home/earthworm/run/params/digital.msubl


# Directory in which to store the temporary .dig files.
TmpDir   /home/luetgert/gifs/msubl/ 

# Directory in which to store the final .dig files.
OutDir   /home/luetgert/gifs/msubl/ 

# Data type selection.
#Component  VHZ
#Component  ADZ
#Component  EP1

    # *** Optional Commands ***
# Which traces will we display? (optional)
 Use_all_Sites     # Use all available channels from waveserver(s)  
#Use_picked_Sites  # Use picked channels available from waveserver(s) 
 
 MaxDist    250  # Max distance of traces to plot; default=100km
 MinSize    3.0  # Minimum magnitude to save in list; default=0.0
 RetryCount   2  # Number of attempts to get a trace from server; default=10

# We accept a command "Debug" which turns on a bunch of log messages
  Debug
# WSDebug

