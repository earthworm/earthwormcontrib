/*********************************************************************
    msubl.h
    
    header file for msubl.c
*********************************************************************/

/*********************************************************************
    defines
*********************************************************************/

#define MAXCHANNELS     1200  /* Maximum number of channels              */

#define TIMEOUT         1200  /* Time(sec) after event to wait for data. */
#define MAXTRACELTH    33000  /* Maximum trace length for data.          */

#define MAX_STR          255  /* Size of strings for arkive records      */
#define MAX_WAVESERVERS   30  /* Maximum number of Waveservers           */
#define MAX_ADRLEN        20  /* Size of waveserver address arrays       */
#define MAX_COMPS         20  /* Number of acceptable component classes  */
#define MAX_PAGERS        30  /* Maximum number of Pager addresses       */

#define MAXLOGO            2  /* Maximum number of Logos                 */
#define MAX_SERV_THRDS     1  /* Maximum number of processor threads     */
#define MAX_TARGETS        5  /* Maximum number of targets               */

#define GDIRSZ           132  /* Size of string for target directorys    */
#define STALIST_SIZ      100  /* Size of string for station list file    */

#define SWAP(a,b) temp=(a);(a)=(b);(b)=temp
#define SIGN(a,b) (b)*abs((a))


typedef struct cdouble{double r,i;} cdouble;

cdouble Cadd(cdouble a, cdouble b);
cdouble Csub(cdouble a, cdouble b);
cdouble Cmul(cdouble a, cdouble b);
cdouble Complex(double re, double im);
cdouble Conjg(cdouble z);
cdouble Cdiv(cdouble a, cdouble b);
double Cabs(cdouble z);
cdouble RCmul(double x, cdouble a);
cdouble Cexp(cdouble z);


/*********************************************************************
    Define the structure for Pole/Zero information.
*********************************************************************/

typedef struct Resp {  /* A PoleZero information structure */
    int     iunit;         /* Units                            */
    int     np;            /* Number of Poles                  */
    int     nz;            /* Number of Zeros                  */
    cdouble Pole[30];      /* The complex poles                */
    cdouble Zero[30];      /* The complex zeros                */
    double    a0;            /* normalization                    */
    double    sensitivity;   /* digital sensitivity * gain       */
} Resp;

/*********************************************************************
    Define the structure for Channel information.
    This is an abbreviated structure.
*********************************************************************/

typedef struct ChanInfo {  /* A channel information structure         */
    char    Site[6];       /* Site                                    */
    char    Comp[5];       /* Component                               */
    char    Net[5];        /* Net                                     */
    char    SCN[15];       /* SCN                                     */
    char    SCNtxt[17];    /* S C N                                   */
    char    SCNnam[17];    /* S_C_N                                   */
    char    SiteName[50];  /* Common Name of Site                     */
    double  Lat;           /* Latitude                                */
    double  Lon;           /* Longitude                               */
    double  Elev;          /* Elevation                               */
    
    int        Inst_type;     /* Type of instrument                      */
    int        Sens_type;     /* Type of sensor                          */
    int        Sens_unit;     /* Sensor units d=1; v=2; a=3              */
    double  Gain;          /* digital counts*Gain = cgs units         */
    double  sensitivity;   /* Channel sensitivity  counts/units       */
} ChanInfo;

/*********************************************************************
    Define the structure for Arkive Phase cards.
    This is an abbreviated structure.
*********************************************************************/

typedef struct ArkInfo {  /* A phase card structure */
    char    Site[6];      /*  0- 3  0- 4 Site                                */
    char    Comp[5];      /* 95-97  9-11 Component                           */
    char    Net[5];       /* 98-99  5- 6 Net                                 */
    char    Prem[5];      /*  4- 5 13-14 P remark                            */
    char    fm;           /*     6    15 P first motion                      */
    int     Pwgt;         /*     7    16 weight                              */
    double  PPick;        /*  9-18+19-23 17-29+29-33 P pick time             */
    double  SPick;        /*  9-18+31-35 17-29+41-45 S pick time             */
    char    Srem[5];      /* 36-37 46-47 S remark                            */
    float   SttResid;     /* 40-43 50-53 S travel-time residual              */
    float   PttResid;     /* 24-27 34-37 P travel-time residual              */
    float   Pdelay;       /* 50-53 66-69 P delay time                        */
    float   Sdelay;       /* 54-57 70-73 S delay time                        */
    float   Dist;         /* 58-61 74-77 Epicentral distance (km)            */
    float   Emerg;        /* 62-64 78-80 Emergence angle at source           */
    float   CodaDur;      /* 71-74 87-90 Coda duration (sec)                 */
    float   Azim;         /* 75-77 91-93 Azimuth to station in deg. E of N.  */
    char    DataSrc;      /* 91    108                                       */
    char    StaList;      /* Flag (*) to indicate info came from StaList     */
    
    int        Inst_type;    /* Type of instrument                               */
    int        Sens_type;    /* Type of sensor                                   */
    int        Sens_unit;    /* Sensor units disp=1; vel=2; acc=3                */
    double  Gain;         /* digital counts*Gain = cgs units                  */
    double  sensitivity;  /* Channel sensitivity  counts/units                */
    float   Duration;     /* Seconds to acquire                              */
    double  Stime;        /* Requested start time                            */
    double  Amp;          /* Raw Maximum Amplitude                           */
    double  WA_Amp;       /* Wood-Anderson Amplitude                         */
    double  xmag;         /* Wood-Anderson Amplitude                         */
    double  Ml100;        /* Ml from Wood-Anderson Amplitude                 */
    
    double  Max;          /*                   */
    double  Min;          /*                   */
    int        nMax;         /*                   */
    int        nMin;         /*                   */
    int        reject;       /*                   */
} ArkInfo;

/*********************************************************************
    Define the structure for the Arkive.
    This is an abbreviated structure.
*********************************************************************/

typedef struct Arkive {   /* An Arkive List */
    char    ArkivMsg[MAX_BYTES_PER_EQ];
    long    InList;       /* Number of archive records in list                             */
    double  EvntTime;     /* Time of Event (Julian Sec)                                    */
    short   EvntYear;     /*   0-  1   0-  3 Event Year                                    */
    short   EvntMonth;    /*   2-  3   4-  5 Event Month                                   */
    short   EvntDay;      /*   4-  5   6-  7 Event Day                                     */
    short   EvntHour;     /*   6-  7   8-  9 Event Hour                                    */
    short   EvntMin;      /*   8-  9  10- 11 Event Minute                                  */
    float   EvntSec;      /*  10- 13  12- 15 Event Second*100                              */
    float   EvntLat;      /*  14- 20  16- 22 Geog coord of Event                           */
    float   EvntLon;      /*  21- 28  23- 30 Geog coord of Event                           */
    float   EvntDepth;    /*  29- 33  31- 35 Depth of Event*100                            */
    float   Smag;         /*  34- 35  36- 38 Mag from max S amplitude from NCSN stations   */
    long    NumPhs;       /*  36- 38  39- 41 # phases used in solution (final weights > 0.1) */
    long    Gap;          /*  39- 41  42- 44 Maximum azimuthal gap                         */
    float   Dmin;         /*  42- 44  45- 47 Minimum distance to site                      */
    float   rms;          /*  45- 48  48- 51 RMS travel time residual*100                  */
    float   mag;          /*  67- 68  70- 72 Coda duration mag                             */
    float   erh;          /*  80- 83  85- 88 Horizontal error*100 (km)                     */
    float   erz;          /*  84- 87  89- 92 Vertical error*100 (km)                       */
    char    ExMagType;    /*     114     122 "External" magnitude label or type code       */
    float   ExMag;        /* 115-117 123-125 "External" magnitude                          */
    float   ExMagWgt;     /* 118-120 126-128 Total of the "external" mag weights (~ number of readings) */
    char    AltMagType;   /*     121     129 Alt amplitude magnitude label or type code    */
    float   AltMag;       /* 122-124 130-132 Alt amplitude magnitude                       */
    float   AltMagWgt;    /* 125-127 133-135 Total of the alt amplitude mag weights (~ number of readings) */
    char    EvntIDS[10];  /* 128-137 136-145 Event ID                                      */
    long    EvntID;       /* Event ID                                                      */
    char    PreMagType;   /*     138     146 Preferred magnitude label code chosen from those available */
    float   PreMag;       /* 139-141 147-149 Preferred magnitude                           */
    float   PreMagWgt;    /* 142-144 150-153 Total of the preferred mag weights (~ number of readings) */
    char    AltDmagType;  /*     145     154 Alt coda dur. magnitude label or type code    */
    float   AltDmag;      /* 146-148 155-157 Alt coda dur. magnitude                       */
    float   AltDmagWgt;   /* 149-151 158-161 Total of the alternate coda duration magnitude weights (~ number of readings) */

    int     MaxChannels;  /*  */
    long    Current;      /* Index of Currently active channel */
    long    index[MAXCHANNELS];
    ArkInfo A[MAXCHANNELS];
    double  Pa;           /* Intercept of the PPick Vapp       */
    double  Pb;           /* PPick slope                       */
    double  VpApp;        /* PPick Vapp                        */
    double  Ml;           /* Ml                                */
    int     Ml_sta;       /* # of Mls used in solution         */
    double  Ml_Med;       /* Median Ml                         */
} Arkive;

/*********************************************************************
    Define the structure for the individual Butler thread.
    This is the private area the thread needs to keep track
    of all those variables unique to itself.
*********************************************************************/

struct Butler {
    int     Debug;               /*                                                    */
    int     WSDebug;             /*                                                    */
    int     myThrdId;
    int     ButStatus;           /* -1 = idle; 0 = initiating; 1 = running             */
    int     readyflag;           /* ready for data                                     */
    int     got_a_menu;
    char    TraceBuf[MAXTRACELTH*10]; /* This should work for 24-bit digitizers        */
    char    mod[20];
    long    samp_sec;            /* samples/sec for current trace                      */
    long    Npts;                /* Number of points in trace                          */
    double  Mean;                /* Mean value of the last data gulp                   */
    
    double  Secs_Screen;         /* Maximum Number of secs of data to retrieve         */
    double  Pre_Event;           /* Maximum Number of secs before event to start plot  */

    int     nentries;            /* Number of menu entries for this SCN                */
    double  TStime[MAX_WAVESERVERS*2]; /* Tank start for this entry                    */
    double  TEtime[MAX_WAVESERVERS*2]; /* Tank end for this entry                      */
    int     index[MAX_WAVESERVERS*2];  /* WaveServer for this entry                    */
    int     Current_Plot;        /* number of plot being worked on                     */
    
    int     inmenu[MAX_WAVESERVERS];
    WS_MENU_QUEUE_REC menu_queue[MAX_WAVESERVERS];
    
    int        NSCN;                /* Number of SCNs we know about                       */
    ChanInfo    Chan[MAXCHANNELS];
/* Globals to set from configuration file
 ****************************************/
    long    wsTimeout;           /* seconds to wait for reply from ws                  */
                                 /* list of available waveServers, from config. file   */
    char    wsIp[MAX_WAVESERVERS][MAX_ADRLEN];
    char    wsPort[MAX_WAVESERVERS][MAX_ADRLEN];
    char    wsComment[MAX_WAVESERVERS][50];
    int     nServer;             /* number of wave servers we know about               */
    char    TmpDir[GDIRSZ];      /* Directory for temporary files on local machine     */
    char    OutDir[GDIRSZ];      /* Directory on local machine for files to be flung   */
    int     ncomp;               /* Number of Acceptable component types               */
    char    Comp[MAX_COMPS][5];  /* Acceptable component types                         */
    char    MlStations[STALIST_SIZ];
    char    person[MAX_PAGERS][50];
    int     nPager;              /* number of defined pager recipients                 */
    pid_t   pid;
    int     status;
    long    MaxDistance;         /* Maximum offset distance for traces in this plot.   */
    
    short   LocalTime;           /* Offset of local time from GMT e.g. -7 = PST        */
    char    LocalTimeID[4];      /* Local time ID e.g. PST                             */
    short   Use_all_Sites;       /* Flag                                               */
    int     MaxChannels;         /*                                                    */
    long    MaxDist;             /* Maximum offset distance for traces to plot.        */
    long    RetryCount;          /* Retry count for waveserver errors.                 */
    double  MinSize;             /* Minimum size event to report                       */

    long    InList;              /* Number of event records in list                    */
};
typedef struct Butler Butler;


