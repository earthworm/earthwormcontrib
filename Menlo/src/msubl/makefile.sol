CFLAGS = -D_REENTRANT $(GLOBALFLAGS)

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

BINARIES = msubl.o processorThread.o convolv.o \
		$L/logit_mt.o $L/getutil.o  $L/transport.o $L/kom.o \
		$L/sleep_ew.o $L/time_ew.o $L/threads_ew.o $L/sema_ew.o\
		$L/mem_circ_queue.o  $L/rw_strongmotion.o  \
		$L/getavail.o $L/getsysname_ew.o $L/chron3.o\
		$L/parse_trig.o $L/pipe.o $L/remote_copy.o \
		$L/socket_ew.o $L/socket_ew_common.o $L/ws_clientII.o

msubl: $(BINARIES)
	cc -o $(B)/msubl $(BINARIES) -lsocket -lnsl -lm -mt -lposix4 -lthread -lc

.c.o:
	$(CC) $(CFLAGS) -g $(CPPFLAGS) -c  $(OUTPUT_OPTION) $<
