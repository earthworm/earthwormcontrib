/***************************************************************************
 * This thread is started by the processor thread manager. We're given     *
 * an integer which identifies who we are.                                 *
 *                                                                         *
 *   Jeeves - This is the process controller.                              *
 *       Read config file                                                  *
 *       Read Arkive msg and stuff info in ark structure.                  *
 *       Build table of data of interest                                   *
 *       Retrieve data traces                                              *
 *       Process traces                                                    *
 *       Plot                                                              *
 *       Publish                                                           *
 ***************************************************************************/

#include <platform.h>
#include <errno.h>
#include "math.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <decode.h>
#include <kom.h>
#include <chron3.h>
#include <time.h>
#include <time_ew.h>
#include <transport.h>

#include <ws_clientII.h>

#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

#include "gd.h"
#include "gdfontt.h"   /*  6pt      */
#include "gdfonts.h"   /*  7pt      */
#include "gdfontmb.h"  /*  9pt Bold */
#include "gdfontl.h"   /* 13pt      */
#include "gdfontg.h"   /* 10pt Bold */

#include "msubl.h"

/* Functions in this source file 
 *******************************/
int  MainProcess(Butler *, Arkive *Ark);
void BitBuster(Butler *But, double *Data, Arkive *Ark);
void Encode_Time( double *secs, Arkive *Ark);
void Sort_Servers (Butler *, double StartTime);
int  RequestWave(Butler *, int, double *, char *, char *, char *, double, double);
int  Get_Ark(Arkive *);
void read_hyp(Arkive *, char *);
void read_phs(ArkInfo *, char *, char *);
int  Build_Table(Butler *, Arkive *, long *);
int  Chan_Info (Butler *, Arkive *, int);
int  No_Dups (Arkive *, char *sta, char *comp, char *net);
int  Build_Menu (Butler *);
int  In_Menu (Butler *, char *sta, char *comp, char *net, double *, double *);
int  In_Menu_list (Butler *, char *Site, char *Comp, char *Net);
void Sort_Ark (long, double *, long *);

void Build_SM_file(Butler *, Arkive *);
void date22( double, char *);
int  MinMax(long, short, double *, double *, double *, double *, double *);
void ML_Get_Sta_Info(Butler *);
int  distaz (double lat1, double lon1, double lat2, double lon2, 
                double *rngkm, double *faz, double *baz);
                
                
/* Functions elsewhere 
 *********************/
void pager( char *note ); /* sends notes into ring   */
  
extern  Butler      Btlr;             /* Private area for the threads          */

char    module[20];           /* Global area for the module name   */
double  Data[MAXTRACELTH*2], PltData[MAXTRACELTH*2];  /* Trace arrays */

/********************************************************************
 *  MainProcess is responsible for shepherding one event through    *
 *  the MsubL processing.                                           *
 *                                                                  *
 ********************************************************************/

int MainProcess(Butler *But, Arkive *Ark)
{
    char        whoami[50], Text[200], errtxt[200];
    double      hold;  
    time_t      current_time, start_time, end_time;
    long        j, TooLate, next, ret, InListToDo, We_Need[MAXCHANNELS];
    int         i, jj, jjj, k, error, successful, Traces_done;
    double      Amplitude, Middle, Scale, Min, Max;
    Resp        PZ;
    
    start_time = time(&current_time);
    
    ML_Get_Sta_Info(But);
    
    /* Build the modname for error messages *
     ****************************************/
    sprintf(But->mod,"msubl.%.2d", But->myThrdId);
    sprintf(module,"msubl.%.2d", But->myThrdId);
    sprintf(whoami, " %s: %s: ", But->mod, "MainProcess");
    
    /* Let's see the message * 
     *************************/
    if (But->Debug ==1) {
        logit("e", "%s Begin trace processing.\n", whoami);
        logit("et", "%s startup of thread %d on event:\n%.80s...\n", 
                whoami, But->myThrdId, Ark->ArkivMsg);
    }
    
    /* Go get all available information from the arkive msg. 
    ********************************************************/
    Ark->MaxChannels = But->MaxChannels;
    error = Get_Ark(Ark);
    if(error) {
        logit("e", "%s Error <%hd> reading arkive msg.\n", whoami, error);
        logit("e", "        There are %d entries in table.\n", Ark->InList);
        if(Ark->InList < 1) {
            logit("e", "exiting!\n");
            return 1;
        }
    }
    if(But->Debug) logit("e", "%s Got the arkive msg.\n", whoami);
    
    if(Ark->mag < But->MinSize) {
        if(But->Debug) logit("e", "%s Event %s too small (%5.2f)\n", whoami, Ark->EvntIDS, Ark->mag);
        return 1;
    }
    
    if(Ark->InList < 5) {
        if(But->Debug) logit("e", "%s Event %s picked by too few stations. (%5.2f)\n", whoami, Ark->EvntIDS, Ark->InList);
        return 1;
    }
    
    /* Set up the plotting parameters for each plot. 
     ************************************************/
    But->Secs_Screen = 60.0; /* Maximum Number of secs of data to retrieve        */
    But->Pre_Event   =  0.0; /* Maximum Number of secs before event to start plot */

    RequestMutex();
    Build_Menu(But);  /*  Build the current wave server menus */
    ReleaseMutex_ew();
    
    if(!But->got_a_menu) {
        logit("e", "%s No Menu from %d servers! Just quit.\n", whoami, But->nServer);
        return 1;
    }
    /* Build the To-Do list from all available information. 
     *******************************************************/
    error = Build_Table(But, Ark, &InListToDo);
    if(error) {
        logit("e", "%s Error <%hd> building ToDo list.\n", whoami, error);
        logit("e", "        There are %d entries in table.\n", InListToDo);
        if(InListToDo < 1) {
            logit("e", "exiting!\n");
            RequestMutex();
            for(j=0;j<But->nServer;j++) wsKillMenu(&(But->menu_queue[j]));
            ReleaseMutex_ew();
            return 1;
        }
    }
    if(But->Debug) logit("e", "%s Got the ToDo list.\n", whoami);
    for(i=0;i<InListToDo;i++) We_Need[i] = 1;
    for(i=InListToDo;i<Ark->MaxChannels;i++) We_Need[i] = 0;
    /*  Process the traces by looping thru the To-Do list. 
    ******************************************************/
 
    next = 0;  /* First Initialize a bunch of stuff. */
    time(&current_time);
    TooLate = current_time + TIMEOUT;
    
    Traces_done = 0;
        /*  Go get the traces.
        **********************/
    while(current_time < TooLate && next < InListToDo) {
        if(We_Need[next]) {
            k = Ark->Current = Ark->index[next];
            if(In_Menu_list(But, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net )) {  
                successful = 0;
                Sort_Servers (But, Ark->A[k].Stime);
                for(jj=0;jj<But->nentries;jj++) {
                    jjj = But->index[jj];
                
                    Ark->A[k].Duration = But->Secs_Screen + 5.0 + Ark->A[k].Dist/6;
                    if(Ark->A[k].Stime + Ark->A[k].Duration > But->TEtime[jjj]) 
                        Ark->A[k].Duration = But->TEtime[jjj] - Ark->A[k].Stime;
                
                    RequestMutex();
                    successful = RequestWave(But, jjj, Data, 
                        Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, 
                        Ark->A[k].Stime, Ark->A[k].Duration);
                    ReleaseMutex_ew();
                    if(successful == 1) {   /*    Plot this trace to memory. */
                        break;
                    }
                    else if(successful == 2) {
                        if(But->Debug) 
                            logit("e", "%s RequestWave error 2.\n", whoami);
                        continue;
                    }
                    else if(successful == 3) {   /* Gap in data */
                        if(But->Debug) 
                            logit("e", "%s RequestWave error 3.\n", whoami);
                        continue;
                    }
                }
                if(successful == 1) {   /*    Process this trace.   */
                            /*   Special code for Menlo.   */
                    if(strcmp(Ark->A[k].Site, "CBR")==0 ||
                       strcmp(Ark->A[k].Site, "NLH")==0 || 
                       strcmp(Ark->A[k].Site, "CPI")==0 || 
                       strcmp(Ark->A[k].Site, "JCH")==0 ) BitBuster(But, Data, Ark);

                    if(But->Debug) 
                        logit("e", "\n%s %s %s %s.\n", 
                            whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net);

                    Traces_done   += 1;
                    
                    MinMax(But->Npts, 1, Data, &Amplitude, &Middle, &Min, &Max);
                    Ark->A[k].Min  = Min;  Ark->A[k].Max = Max;
                    Ark->A[k].nMin = Ark->A[k].nMax = Ark->A[k].reject = 0;
                    if(Amplitude < 15) {
                        Ark->A[k].reject = 1; 
                        if(But->Debug) 
                            logit("e", "\n%s %s %s %s rejected for low amplitude (%f).\n", 
                                whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, Amplitude);
                    } 
                    for(j=0;j<But->Npts;j++) {
                        if(Data[j] <= Min+70) Ark->A[k].nMin += 1;
                        if(Data[j] >= Max-70) Ark->A[k].nMax += 1;
                    }
                    if(Ark->A[k].Inst_type == 1) {
                        if((Ark->A[k].nMin >= 6 || Ark->A[k].nMax >= 6) && 
                           (Ark->A[k].Comp[0]=='V') && 
                           (Ark->A[k].Min < -1600 || Ark->A[k].Max > 1600)) {
                            Ark->A[k].reject = 2;
                            if(But->Debug) 
                                logit("e", "\n%s %s %s %s rejected for clipping.\n", 
                                    whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net);
                        }
                    }
                    
                    for(j=0;j<But->Npts;j++) PltData[j] = Data[j];
                    est_wood(But, PltData, Ark);
                    
                    We_Need[next] = 0;
                }
            }  
        }
        next += 1;
        if(next >= InListToDo) break;
        time(&current_time);
    }
    if(But->Debug && current_time >= TooLate) 
        logit("e", "\n%s Ran out of time.\n", whoami);
    
        /*  Finish up the calculations.
        *******************************/
    if(Traces_done > 0) {   
        Get_Average_Ml(Ark);
        Get_Median_Ml(Ark);
        
        if((Ark->Ml_Med >= But->MinSize-0.5 || 
            Ark->Ml     >= But->MinSize-0.5) && 
            abs(Ark->Ml-Ark->mag) < 1.0) {
                Build_SM_file(But, Ark);
        }
        
        logit("et", "%s Event: %s Md: %5.2f Ml: %5.2f Ml_med: %5.2f\n", 
            whoami, Ark->EvntIDS, Ark->mag, Ark->Ml, Ark->Ml_Med); 
    
        sprintf(Text,"Event: %s      Md:%5.2f Ml:%5.2f rms:%4.2f Mls:%d Vapp:%5.2f", 
            Ark->EvntIDS, Ark->mag, Ark->Ml, Ark->rms, Ark->Ml_sta, Ark->VpApp); 
        logit("et", "%s %s\n", whoami, Text); 
    
        if((Ark->Ml_Med >= But->MinSize || 
            Ark->Ml     >= But->MinSize) && 
            Ark->Ml_sta >= 3 &&
            abs(Ark->Ml-Ark->mag) < 1.0) {
            for(i=0;i<But->nPager;i++) 
             pager1(Text, &But->person[i][0]); 
        }
   
        end_time = time(&current_time) - start_time;
        logit("et", "%s Processing time: %d\n", whoami, end_time); 
    
        Encode_Time(&hold, Ark);
        end_time = time(&current_time) - hold;
        logit("et", "%s Time since event: %d\n", whoami, end_time); 
        
    }
    
    RequestMutex();
    for(j=0;j<But->nServer;j++) wsKillMenu(&(But->menu_queue[j]));
    ReleaseMutex_ew();
    
    return 0;
}


/*******************************************************************************
 *    BitBuster corrects erroneous bits which show up on some DST channels     *
 *                  Menlo only                                                 *
 *******************************************************************************/

void BitBuster(Butler *But, double *Data, Arkive *Ark)
{
    char    SCNtxt[20];
    double    d;
    int        i, j, k, mask, sign, ii, i1, i2, width, sample, noise;
    int        a, b, c;

    i = But->Current_Plot;
    noise = 15;
    for(j=0;j<But->Npts;j++) {
        d = Data[j];
        width = 6;
        i1 = j-width;  i2 = j+width;
        if(i1<0) i1 = 0;
        if(i2>But->Npts) i2 = But->Npts;
        sign = Data[j]<0.0? -1:1;
        mask = noise;
        for(ii=i1;ii<i2;ii++) {
            sample = (int)fabs(Data[ii]);
            if(ii != j && sample != 919191) mask = mask | sample;
        }
        sample = mask >> 1;
        while(sample>1) {
            mask = mask | sample;
            sample = sample >> 1;
        }
        mask = mask << 1;
        mask = mask | noise;
            
        sample = (int)fabs(Data[j]);
        if(sample != 919191) {
            Data[j] = sign*(sample & mask);
            if(Data[j] != d) {
                a = b = sample & (mask ^ 0xFFFF);
                ii = 0;
                for(i1=0;i1<32;i1++) {if((a&1) == 1) ii++; a=a>>1;}
                if(But->Debug) {
                    k = Ark->Current;
                      sprintf(SCNtxt, "%s %s %s", Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net);
                    logit("e", "%s %s. %x %7.0f %7.0f %7.0f %7.0f %5x %3d bit(s) at pt: %6d of %6d\n", 
                        "BitBuster:", SCNtxt, mask, 
                        Data[j-1], Data[j], Data[j+1], d, b, ii, j, But->Npts);
                }   
            }
        }
    }
}


/**********************************************************************
 * Encode_Time : Encode time to seconds since 1970                    *
 *                                                                    *
 **********************************************************************/
void Encode_Time( double *secs, Arkive *Ark)
{
    struct Greg    g;
    double  sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's     */
                                       /* time 0 and 1970-01-01 00:00:00.0 GMT */

    g.year   = Ark->EvntYear;
    g.month  = Ark->EvntMonth;
    g.day    = Ark->EvntDay;
    g.hour   = Ark->EvntHour;
    g.minute = Ark->EvntMin;
    *secs    = 60.0 * (double) julmin(&g) + Ark->EvntSec - sec1970;
}


/*************************************************************************
 *   Sort_Servers                                                        *
 *      From the table of waveservers containing data for the current    *
 *      SCN, the table is re-sorted to provide an intelligent order of   *
 *      search for the data.                                             *
 *                                                                       *
 *      The strategy is to start by dividing the possible waveservers    *
 *      into those which contain the requested StartTime and those which *
 *      don't.  Those which do are retained in the order specified in    *
 *      the config file allowing us to specify a preference for certain  *
 *      waveservers.  Those waveservers which do not contain the         *
 *      requested StartTime are sorted such that the possible data       *
 *      retrieved is maximized.                                          *
 *************************************************************************/

void Sort_Servers (Butler *But, double StartTime)
{
    char    whoami[50], c22[25];
    double  tdiff[MAX_WAVESERVERS*2];
    int     j, k, jj, kk, hold, index[MAX_WAVESERVERS*2];
    
    sprintf(whoami, " %s: %s: ", But->mod, "Sort_Servers");
        /* Throw out servers with data too old. */
    j = 0;
    while(j<But->nentries) {
        k = But->index[j];
        if(StartTime > But->TEtime[k]) {
            if(But->Debug) {
                date22( StartTime, c22);
                logit("e","%s %d %d  %s", whoami, j, k, c22);
                    logit("e", " %s %s <%s>\n", 
                          But->wsIp[k], But->wsPort[k], But->wsComment[k]);
                date22( But->TEtime[k], c22);
                logit("e","ends at: %s rejected.\n", c22);
            }
            But->inmenu[k] = 0;    
            But->nentries -= 1;
            for(jj=j;jj<But->nentries;jj++) {
                But->index[jj] = But->index[jj+1];
            }
        } else j++;
    }
    if(But->nentries <= 1) return;  /* nothing to sort */
            
    /* Calculate time differences between StartTime needed and tankStartTime */
    /* And copy positive values to the top of the list in the order given    */
    jj = 0;
    for(j=0;j<But->nentries;j++) {
        k = index[j] = But->index[j];
        tdiff[k] = StartTime - But->TStime[k];
        if(tdiff[k]>=0) {
            But->index[jj++] = index[j];
            tdiff[k] = -65000000; /* two years should be enough of a flag */
        }
    }
    
    /* Sort the index list copy in descending order */
    j = 0;
    do {
        k = index[j];
        for(jj=j+1;jj<But->nentries;jj++) {
            kk = index[jj];
            if(tdiff[kk]>tdiff[k]) {
                hold = index[j];
                index[j] = index[jj];
                index[jj] = hold;
            }
            k = index[j];
        }
        j += 1;
    } while(j < But->nentries);
    
    /* Then transfer the negatives */
    for(j=jj,k=0;j<But->nentries;j++,k++) {
        But->index[j] = index[k];
    }
}    


/********************************************************************
 *  RequestWave                                                     *
 *                                                                  *
 *   k - waveserver index                                           *
 ********************************************************************/
int RequestWave(Butler *But, int k, double *Data, 
            char *Site, char *Comp, char *Net, double Stime, double Duration)
{
    char     *token, server[wsADRLEN*3], whoami[50], SCNtxt[17];
    float    samprate, sample;
    double   temp, temp1;
    int      i, j, nsamp, nbytes, io, retry;
    int      success, ret, WSDebug = 0;          
    TRACE_REQ   request;
    WS_MENU  menu = NULL;
    WS_PSCN  pscn = NULL;
    
    sprintf(whoami, " %s: %s: ", But->mod, "RequestWave");
    WSDebug = But->WSDebug;
    success = retry = 0;
gettrace:
    strcpy(request.sta,  Site);
    strcpy(request.chan, Comp);
    strcpy(request.net,  Net );
    request.waitSec = 0;
    request.pinno = 0;
    request.reqStarttime = Stime;
    request.reqEndtime   = request.reqStarttime + Duration;
    request.partial = 1;
    request.pBuf    = But->TraceBuf;
    request.bufLen  = MAXTRACELTH*7;
    request.timeout = But->wsTimeout;
    request.fill    = 919191;
    sprintf(SCNtxt, "%s %s %s", Site, Comp, Net);
    
    /*    Put out WaveServer request here and wait for response */
    /* Get the trace
     ***************/
    /* rummage through all the servers we've been told about */
    if ( (wsSearchSCN( &request, &menu, &pscn, &But->menu_queue[k]  )) == WS_ERR_NONE ) {
      strcpy(server, menu->addr);
      strcat(server, "  ");
      strcat(server, menu->port);
    } else strcpy(server, "unknown");
 
    if(WSDebug) {     
        logit("e","\n%s Issuing request to wsGetTraceAscii: server: %s\n", whoami, server);
        logit("e","    %s %f %f %d\n", SCNtxt,
             Stime, request.reqEndtime, request.timeout);
        logit("e"," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
    }
    
    io = wsGetTraceAscii(&request, &But->menu_queue[k], But->wsTimeout);
    
    if (io == WS_ERR_NONE ) {
        if(WSDebug) {
            logit("e"," %s server: %s trace %s: went ok first time. Got %ld bytes\n",
                whoami, server, SCNtxt, request.actLen); 
            logit("e","%s server: %s Return from wsGetTraceAscii: %d\n", whoami, server, io);
            logit("e","        actStarttime=%lf, actEndtime=%lf, actLen=%ld, samprate=%lf\n",
                  request.actStarttime, request.actEndtime, request.actLen, request.samprate);
        }
    }
    else {
        switch(io) {
        case WS_ERR_EMPTY_MENU:
        case WS_ERR_SCN_NOT_IN_MENU:
        case WS_ERR_BUFFER_OVERFLOW:
            if (io == WS_ERR_EMPTY_MENU ) 
                logit("e"," %s server: %s No menu found.  We might as well quit.\n", whoami, server); 
            if (io == WS_ERR_SCN_NOT_IN_MENU ) 
                logit("e"," %s server: %s Trace %s not in menu\n", whoami, server, SCNtxt);
            if (io == WS_ERR_BUFFER_OVERFLOW ) 
                logit("e"," %s server: %s Trace %s overflowed buffer. Fatal.\n", whoami, server, SCNtxt); 
            success = 2;                /*   We might as well quit */
            return success;
        
        case WS_ERR_PARSE:
        case WS_ERR_TIMEOUT:
        case WS_ERR_BROKEN_CONNECTION:
        case WS_ERR_NO_CONNECTION:
            sleep_ew(500);
            retry += 1;
            if (io == WS_ERR_PARSE )
                logit("e"," %s server: %s Trace %s: Couldn't parse server's reply. Try again.\n", 
                        whoami, server, SCNtxt); 
            if (io == WS_ERR_TIMEOUT )
                logit("e"," %s server: %s Trace %s: Timeout to wave server. Try again.\n", whoami, server, SCNtxt); 
            if (io == WS_ERR_BROKEN_CONNECTION ) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Broken connection to wave server. Try again.\n", 
                whoami, server, SCNtxt); 
            }
            if (io == WS_ERR_NO_CONNECTION ) {
                if(WSDebug || retry>1) 
                    logit("e"," %s server: %s Trace %s: No connection to wave server.\n", whoami, server, SCNtxt); 
                if(WSDebug) logit("e"," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
            }
            ret = wsAttachServer( menu, But->wsTimeout );
            if(ret == WS_ERR_NONE && retry < But->RetryCount) goto gettrace;
            if(WSDebug) {   
                switch(ret) {
                case WS_ERR_NO_CONNECTION:
                    logit("e"," %s server: %s: No connection to wave server.\n", whoami, server); 
                    break;
                case WS_ERR_SOCKET:
                    logit("e"," %s server: %s: Socket error.\n", whoami, server); 
                    break;
                case WS_ERR_INPUT:
                    logit("e"," %s server: %s: Menu missing.\n", whoami, server); 
                    break;
                default:
                    logit("e"," %s server: %s: wsAttachServer error %d.\n", whoami, server, ret); 
                }
            }
            return 2;                /*   We might as well quit */
        
        case WS_WRN_FLAGGED:
            if((int)strlen(&request.retFlag)>0) {
                if(WSDebug) logit("e","%s server: %s Trace %s: return flag from wsGetTraceAscii: <%c>\n %.80s\n", 
                        whoami, server, SCNtxt, request.retFlag, But->TraceBuf);
                if(request.retFlag == 'L') return 3;                /*   We might as well quit */
                if(request.retFlag == 'R') return 3;                /*   We might as well quit */
                if(request.retFlag == 'G') return 3;                /*   We might as well quit */
            }
            logit("e"," %s server: %s Trace %s: No trace available. Wave server returned %c\n", 
                whoami, server, SCNtxt, request.retFlag); 
            return 2;                /*   We might as well quit */
        
        default:
            logit( "et","%s server: %s Failed.  io = %d\n", whoami, server, io );
        }
    }

    if((int)strlen(&request.retFlag)>0) {
        if(WSDebug) logit("e","%s server: %s Trace %s: return flag from wsGetTraceAscii: <%c>\n %.80s\n", 
                whoami, server, SCNtxt, request.retFlag, But->TraceBuf);
        if(request.retFlag == 'L') return 3;                /*   We might as well quit */
        if(request.retFlag == 'R') return 3;                /*   We might as well quit */
        if(request.retFlag == 'G') return 3;                /*   We might as well quit */
    }

    nbytes = request.actLen;
    But->samp_sec = request.samprate<=0? 100:request.samprate;
    samprate = request.samprate;
    
    nsamp = Duration*samprate;
    if(nsamp > MAXTRACELTH) nsamp = MAXTRACELTH;
    strtok(But->TraceBuf, " ,");
    j = 0;
    token = strtok(0L, " ,");
    
    But->Mean = 0.0;
    nsamp = 0;
    while(token!=0L && j<MAXTRACELTH) {
        if(strcmp(token, "919191")==0) {
    /*        Data[j++] = 0;    */
        } else {
            sscanf( token, "%f", &sample);
            Data[j] = sample;
            But->Mean += Data[j];
            j += 1;
            nsamp += 1;
        }
        token = strtok(0L, " ,");
    }
    
    But->Mean = But->Mean/nsamp;
    But->Npts = j;
    temp = j/samprate + 1.0;
    temp1 = request.reqEndtime - request.reqStarttime;
    if(temp < (Duration-10)) {
        sleep_ew(1000);
        retry += 1;
        if(retry < But->RetryCount) goto gettrace;
        if (WSDebug) logit("e","%s server: %s Trace %s incomplete after %d trys: %lf  %f %f\n",
            whoami, server, SCNtxt, But->RetryCount, Duration, temp, temp1);
    }   
    if (io == WS_ERR_NONE ) success = 1;

    return success;
}

/********************************************************************
 *    Get_Ark is responsible for reading the archive msg.           *
 *                                                                  *
 ********************************************************************/

int Get_Ark(Arkive *Ark)
{
    char    *in;            /* working pointer to archive message    */
    char    line[MAX_STR];  /* to store lines from msg               */
    char    shdw[MAX_STR];  /* to store shadow cards from msg        */
    int     msglen;         /* length of input archive message       */
    int     nline;          /* number of lines (not shadows) so far  */
    int     i;
    
  /* Initialize some stuff
   ***********************/
    nline  = 0;
    msglen = strlen( Ark->ArkivMsg );
    in     = Ark->ArkivMsg;
    
    /*    initialize the ArkList    
    ***************************/
    Ark->InList = 0;
    for(i=0;i<Ark->MaxChannels;i++) {
        Ark->A[i].PPick = Ark->A[i].SPick = 0.0;
        Ark->A[i].StaList = ' ';
        strcpy(Ark->A[i].Site, " ");
        strcpy(Ark->A[i].Comp, " ");
        strcpy(Ark->A[i].Net, " ");
    }
    
   /* Read one data line and its shadow at a time from arkive; process them
   ***********************************************************************/
    i = 0;
    while( in < Ark->ArkivMsg + msglen ) {
        if ( sscanf( in, "%[^\n]", line ) != 1 )  return( -1 );
        in += strlen( line ) + 1;
        if ( sscanf( in, "%[^\n]", shdw ) != 1 )  return( -1 );
        in += strlen( shdw ) + 1;
        nline++;

        /* Process the hypocenter card (1st line of msg) & its shadow
        ************************************************************/
        if( nline == 1 ) {                      /* hypocenter is first line in msg     */
            read_hyp(Ark, line );
            continue;
        }

        /* Process all the phase cards & their shadows
        *********************************************/
        if( (int)strlen(line) < (size_t)75 ) break;       /* found the terminator line           */
        read_phs( &(Ark->A[i]), line, shdw);  /*  load phase info into Ark structure  */
        i += 1;
        if(i >= Ark->MaxChannels) break;
    
         Ark->A[i].SPick = (Ark->A[i].SPick==0.0)? 
                           Ark->EvntTime + Ark->A[i].Dist/3.36 :
                           Ark->A[i].SPick;         /* SPick */
    
         Ark->A[i].SPick = Ark->EvntTime + Ark->A[i].Dist/3.36;         /* SPick */
    
    }
    Ark->InList = i; 
    return(0);
}


/**************************************************************
 * read_hyp() reads the hypocenter line from an archive msg   *
 **************************************************************/
/*------------------------------------------------------------------------
 Sample hypoinverse archive summary line and its shadow.  The summary line
 may be up to 188 characters long.  Its full format is described in 
 documentation (shadow.doc) by Fred Klein.
           10        20        30        40        50        60        70        80        90        100       110       120       130       140       
0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
9701151845332336  148120 3703  605 0 19104  5  37 8971 15222513 13518SLA  57 * 0 132 14424  0125  0 27PMMTWWWD 26X                 1007557D175125Z114  8
$1
  ------------------------------------------------------------------------*/

void read_hyp(Arkive *Ark,  char *hyp)
{
    char   datestr[20], subname[] = "read_hyp";
    int    i, j, io;
    float  deg, min;

    Ark->EvntYear  =  DECODE( hyp+0,  4, atof );         /* year of origin time     */
    Ark->EvntMonth =  DECODE( hyp+4,  2, atof );         /* month of origin time    */
    Ark->EvntDay   =  DECODE( hyp+6,  2, atof );         /* day of origin time      */
    Ark->EvntHour  =  DECODE( hyp+8,  2, atof );         /* hour of origin time     */
    Ark->EvntMin   =  DECODE( hyp+10, 2, atof );         /* minute of origin time   */
    Ark->EvntSec   = (DECODE( hyp+12, 4, atof ))/100.0;  /* seconds of origin time  */
    strncpy( datestr, hyp, 12 );
    datestr[12] = '\0';
    strcat ( datestr, "00.00" );                        /* origin time without seconds */
    if(epochsec17(&(Ark->EvntTime), datestr) ) {
       logit("", "%s: Error converting origin time: %s\n", subname, datestr );
    }
    Ark->EvntTime += (double)Ark->EvntSec;                /* Time of Event (Julian Sec) */

    deg =  DECODE( hyp+16, 2, atof );
    min = (DECODE( hyp+19, 4, atof ))/100.;
    Ark->EvntLat = deg + min/60.;                       /* Latitude of Event */
    if( hyp[18]=='S' || hyp[18]=='s' ) Ark->EvntLat = -Ark->EvntLat;

    deg =  DECODE( hyp+23, 3, atof );
    min = (DECODE( hyp+27, 4, atof ))/100.;
    Ark->EvntLon = deg + min/60.;                       /* Longitude of Event */
    if( hyp[26]=='W' || hyp[26]=='w' || hyp[26]==' ' ) Ark->EvntLon = -Ark->EvntLon;

    Ark->EvntDepth = (DECODE( hyp+31, 5, atof ))/100.;  /* Depth of Event            */
    Ark->Smag      = (DECODE( hyp+36, 3, atof ))/100.;  /* Magnitude from maximum S amplitude from NCSN stations */
    Ark->NumPhs    =  DECODE( hyp+39, 3, atoi );        /* # phases used in solution */
    Ark->Gap       =  DECODE( hyp+42, 3, atoi );        /* Maximum azimuthal gap     */
    Ark->Dmin      =  DECODE( hyp+45, 3, atoi );        /* Minimum distance to site  */
    Ark->rms       = (DECODE( hyp+48, 4, atof ))/100.;  /* RMS travel time residual  */

    Ark->erh       = (DECODE( hyp+85, 4, atof ))/100.;  /* Magnitude of horizontal error             */
    Ark->erz       = (DECODE( hyp+89, 4, atof ))/100.;  /* Magnitude of vertical error               */

    Ark->mag       = (DECODE( hyp+70, 3, atof ))/100.;  /* Coda duration mag     */  
 
    Ark->ExMagType = hyp[122];                          /* "External" mag label or type code    */  
    Ark->ExMag     = (DECODE( hyp+123, 3, atof ))/100.; /* "External" magnitude                 */  
    Ark->AltMagType = hyp[129];                         /* Alt amplitude mag label or type code */  
    Ark->AltMag    = (DECODE( hyp+130, 3, atof ))/100.; /* Alternate amplitude mag              */  
    Ark->PreMagType = hyp[146];                         /* Alt amplitude mag label or type code */  
    Ark->PreMag    = (DECODE( hyp+147, 3, atof ))/100.; /* Alternate amplitude mag              */  
    if(Ark->PreMag != 0.0) Ark->mag = Ark->PreMag;
    
    strcpy(Ark->EvntIDS, "No_ID");
    for(i=136,j=0;i<146;i++) if(hyp[i]!=' ') {j = i;break;}
    if(j) strncpy(Ark->EvntIDS, &hyp[j], 146-j);
    Ark->EvntIDS[146-j] = 0;
    io = sscanf(Ark->EvntIDS, "%d", & Ark->EvntID);
    if(io != 1) {
        logit("e", "%s: io:%d EvntID: <%s> interpreted as: %d\n", 
            subname, io, Ark->EvntIDS, Ark->EvntID);
    }
}


/***************************************************************
 * read_phs() reads a phase line & its shadow from archive msg *
 ***************************************************************/
/*------------------------------------------------------------------------
Sample HYPOINVERSE station archive card (P-arrival) and a shadow card.
   (phase card is 100 chars; shadow is up to 103 chars):
0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1
PPC  PD0V97 1151845 3500 -21129    0   0   0     0 -27   0  8611400  0    916616 0 185   0 WD  VHZNC

0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1
$   4 3.29 1.80 3.45 1.97 0.08 PSN0    9 PHP2 1334  1 427  3 328  5 108  7  62  9  49  0   0

--------------------------------------------------------------------------*/
void read_phs(ArkInfo *A, char *phs, char *shdw)
{
    char   datestr[20], subname[] = "read_phs";
    float  sec;
    int    i;

    A->StaList = '*';
    strncpy( A->Site, phs,    5  );                     /* Site */
    A->Site[5] = '\0';
    for(i=0; i<5; i++){      /* get rid of trailing blanks in A->Site */
        if(A->Site[i]==' ') {
            A->Site[i]='\0';
            break;
        }
    }
    strncpy( A->Comp, phs+9, 3);                        /* Component */
    A->Comp[3] = '\0';
    strncpy( A->Net,  phs+5, 2);                        /* Net */
    A->Net[2]  = '\0';

    strncpy( A->Prem, phs+13, 3);                       /* P remark (&first motion) */
    A->Prem[3]  =  '\0';
    A->PttResid = (DECODE( phs+34,  4, atof ))/100.0;   /* P travel-time residual */
    A->Pdelay   = (DECODE( phs+66,  4, atof ))/100.0;   /* P travel-time delay    */

    strncpy( A->Srem, phs+46, 2 );                      /* S remark               */
    A->Srem[2]  =  '\0';
    A->SttResid = (DECODE( phs+50,  4, atof ))/100.0;   /* S travel-time residual */
    A->Sdelay   = (DECODE( phs+70,  4, atof ))/100.0;   /* S travel-time delay    */

    strncpy( datestr,  phs+17,  12 );                   /* arrival time without seconds */
    datestr[12]='\0';
    strcat ( datestr, "00.00" );
    if( phs[31] == '.' ) sec =  DECODE( phs+29, 5, atof );
    else                 sec = (DECODE( phs+29, 5, atof ))/100.0;
    if(epochsec17(&(A->SPick), datestr ) ) {
      printf( "%s: Error converting arrival time: %s\n", subname, datestr );
    }
    A->PPick = A->SPick;
    A->PPick = (sec==0.0)? 0.0:A->PPick+sec; /* PPick */
    
    if( phs[43] == '.' ) sec =  DECODE( phs+41, 5, atof );
    else                 sec = (DECODE( phs+41, 5, atof ))/100.0;
    A->SPick = (sec==0.0)? 0.0:A->SPick+sec; /* SPick */
    
    A->Dist      = (DECODE( phs+74,  4, atof ))/10.0;   /* Epicentral distance (km)             */
    A->Emerg     =  DECODE( phs+78,  3, atof );         /* Emergence angle at source            */
    
    A->CodaDur   =  DECODE( phs+87,  4, atof );         /* Coda duration (sec)                  */
    A->Azim      =  DECODE( phs+91,  3, atof );         /* Azimuth to station in deg. E of N.   */
    A->DataSrc   =  phs[108];
    A->fm        =  phs[15];                            /* First motion                         */
    A->Pwgt      =  DECODE( phs+16,  1, atof );         /* Coda duration (sec)                  */
    
    i  =  DECODE( shdw+35, 5, atol );                   /* Coda duration time (sec)             */
}


/********************************************************************
 *    Build_Table is responsible for setting up the ToDo list.      *
 *                                                                  *
 ********************************************************************/

int Build_Table(Butler *But, Arkive *Ark, long *InListToDo)
{
    char    whoami[50], site[6], net[3], comp[4], line[256];
    FILE    *file;
    double  sx, sy, ss, sxoss, st2, t, offset[MAXCHANNELS];
    double  dist, lat, lon, elev, dlat, dlon, faz, baz, t0, t1;
    short   i, j, k, kk, error, flag, stime_flag;
    
    sprintf(whoami, " %s: %s: ", But->mod, "Build_Table");
    stime_flag = error = *InListToDo = 0;
    But->MaxDistance = But->MaxDist;
    if(Ark->mag < 1.0) But->MaxDistance = 150.0;
    if(Ark->mag < 2.0) But->MaxDistance = 200.0;
    if(Ark->mag < 3.0) But->MaxDistance = 250.0;
    if(But->MaxDistance > But->MaxDist) But->MaxDistance = But->MaxDist;
    
    But->MaxDistance = But->MaxDist;
    
    sx = sy = st2 = 0.0;
    ss = Ark->InList;
    Ark->Pa = Ark->Pb = 0.0;
    for(i=0;i<Ark->InList;i++) {
        sx += Ark->A[i].Dist;
        sy += Ark->A[i].PPick - Ark->EvntTime;
    }
    sxoss = sx/ss;
    for(i=0;i<Ark->InList;i++) {
        t = Ark->A[i].Dist - sxoss;
        st2 += t*t;
        Ark->Pb += t*(Ark->A[i].PPick - Ark->EvntTime);
    }
    Ark->Pb /= st2;
    Ark->VpApp = 1.0/Ark->Pb;
    Ark->Pa = (sy - sx*Ark->Pb)/ss;
    
    if(But->Use_all_Sites) {  /* Fill out the Ark list with unpicked entries from the station list */
        if(But->Debug) logit("e", "%s Using all sites.\n", whoami);
        
        lat = Ark->EvntLat;    lon = Ark->EvntLon;
        for(i=0;i<But->NSCN;i++) {
            if( Ark->InList >= MAXCHANNELS ) {
                fprintf( stderr, "%s %s %s ", whoami, 
                    "Site table full; cannot load entire file\n", 
                    "Use <maxsite> command to increase table size; exiting!\n");
                break;
            } 
            else {
                distaz(lat, lon, But->Chan[i].Lat, But->Chan[i].Lon, &dist, &faz, &baz);
                if(dist < But->MaxDistance) {
                    flag = But->ncomp>0?  0:1;
                    for(kk=0;kk<But->ncomp;kk++) {
                        if(strcmp(But->Chan[i].Comp, But->Comp[kk]) == 0) flag = 1; 
                    }
                    
                    if(flag && No_Dups(Ark, But->Chan[i].Site, But->Chan[i].Comp, But->Chan[i].Net)) {    
                        j = Ark->InList;
                        strcpy(Ark->A[j].Site, But->Chan[i].Site);
                        strcpy(Ark->A[j].Comp, But->Chan[i].Comp);
                        strcpy(Ark->A[j].Net,  But->Chan[i].Net);
                        strcpy(Ark->A[j].Prem,  " ");
                        strcpy(Ark->A[j].Srem,  " ");
                        Ark->A[j].Dist = dist;
                        Ark->A[j].PPick = Ark->EvntTime + dist/Ark->VpApp + Ark->Pa;
                        Ark->A[j].SPick = Ark->EvntTime + dist/3.36;
                        Ark->A[j].PttResid = 0.0;
                        Ark->A[j].SttResid = 0.0;
                        Ark->A[j].Dist     = dist;
                        Ark->A[j].Emerg    = 0.0;
                        Ark->A[j].CodaDur  = 0.0;
                        Ark->A[j].Azim     = faz;
                        Ark->InList += 1;
                    } 
                }
            }
        }
    }
    
    /* Keep only those which have been blessed *
     *******************************************/
    for(i=0;i<Ark->InList;i++) {
        Ark->A[i].reject = 3;    /*    we later clear this if trace found and not clipped    */
        Ark->A[i].Ml100  = 0.0;    
        j = Chan_Info(But, Ark, i);
        if(j>=0) {
            Ark->A[i].Inst_type    = But->Chan[j].Inst_type;
            Ark->A[i].Gain         = But->Chan[j].Gain;
            Ark->A[i].Sens_type    = But->Chan[j].Sens_type;
            Ark->A[i].Sens_unit    = But->Chan[j].Sens_unit;
            Ark->A[i].sensitivity  = But->Chan[j].sensitivity;
        }
        else {
            Ark->A[i].Dist = 30000.0;
        }
    }
    
    /* Set up the arrays for selecting and sorting *
     ***********************************************/
    for(i=0;i<Ark->InList;i++) {
        offset[i] = (Ark->A[i].Dist>0.0 && Ark->A[i].Dist< But->MaxDistance)? Ark->A[i].Dist:30000.0; 
        Ark->index[i] = i;
        Ark->A[i].SPick = Ark->A[i].PPick + Ark->A[i].Dist*(0.900/Ark->VpApp);
    }
    
    /* Go get the needed info *
    ***************************/
    if(But->Debug) logit("e", "%s Get the %d requested traces.\n", whoami, Ark->InList);
    j = 0;
    for(i=0;i<Ark->InList;i++)  {
        if(offset[i] < But->MaxDistance) {
            flag = But->ncomp>0?  0:1;
            for(kk=0;kk<But->ncomp;kk++) {
                if(strcmp(Ark->A[i].Comp, But->Comp[kk]) == 0) flag = 1; 
            }
                            
            if(flag && In_Menu(But, Ark->A[i].Site, Ark->A[i].Comp, Ark->A[i].Net, &t0, &t1)) {    
                Ark->A[i].Stime = Ark->EvntTime - But->Pre_Event;   /* Earliest time needed. */
                if(Ark->A[i].Stime > t0 && Ark->A[i].Stime < t1) {
                    Ark->A[i].Duration = But->Secs_Screen + 5.0 + Ark->A[i].Dist/6;   /* Time Reduced/6             */
                    if(Ark->A[i].Stime + Ark->A[i].Duration > t1) 
                        Ark->A[i].Duration = t1 - Ark->A[i].Stime;
                    
                    j += 1;
                    *InListToDo = j;
                }
            } else offset[i] = 30000.0;
        } else offset[i] = 30000.0;
    }
    if(*InListToDo==0) error = 1;
    Sort_Ark (Ark->InList, offset, &Ark->index[0]);
    
    if(But->Debug) {
        logit("e", "%s The following %d traces are available:\n Site:  Net:  Comp:   Dist:      Picked:\n", 
              whoami, *InListToDo);
        for(i=0;i<(*InListToDo);i++)  {
            k = Ark->index[i];
            logit("e", " %s    %s    %s     %f  %c \n", 
                Ark->A[k].Site, Ark->A[k].Net, Ark->A[k].Comp, 
                Ark->A[k].Dist, Ark->A[k].StaList);
        }
        logit("e", "%s %s Md: %f\n", whoami, Ark->EvntIDS, Ark->mag);
    }
    return error;
}

/*************************************************************************
 *   short Chan_Info (sta,comp,net)                                        *
 *      Determines if the scn is in the Arkive list                      *
 *************************************************************************/

int Chan_Info (Butler *But, Arkive *Ark, int k)
{
    int    i;
      
    for(i=0;i<But->NSCN;i++) {
       if(strcmp(But->Chan[i].Site, Ark->A[k].Site)==0 && 
          strcmp(But->Chan[i].Comp, Ark->A[k].Comp)==0 && 
          strcmp(But->Chan[i].Net,  Ark->A[k].Net )==0) {
          sprintf(But->Chan[i].SCNtxt, "%s %s %s", But->Chan[i].Site, But->Chan[i].Comp, But->Chan[i].Net);
          return i;
       }
    }
    return (-1);
}


/*************************************************************************
 *      No_Dups (sta,comp,net)                                           *
 *      Determines if the scn is in the Arkive list                      *
 *************************************************************************/

int No_Dups (Arkive *Ark, char *sta, char *comp, char *net)
{
    int    i;
      
    for(i=0;i<Ark->InList;i++) {
       if(strcmp(Ark->A[i].Site, sta )==0 && 
          strcmp(Ark->A[i].Comp, comp)==0 && 
          strcmp(Ark->A[i].Net,  net )==0) {
              return 0;
       }
    }
    return 1;
}


/*************************************************************************
 *   short Build_Menu ()                                                 *
 *      Builds the waveserver's menu                                     *
 *************************************************************************/

int Build_Menu (Butler *But)
{
    char    whoami[50], server[100];
    int     j, retry, ret, rc;
    WS_PSCN scnp;
    WS_MENU menu; 
      
    /* Build the current wave server menus *
     ***************************************/

    sprintf(whoami, " %s: %s: ", But->mod, "Build_Menu");
    But->got_a_menu = 0;
    
    for(j=0;j<But->nServer;j++) But->index[j]  = j;
    
    for (j=0;j< But->nServer; j++) {
        retry = 0;
        But->inmenu[j] = 0;
        sprintf(server, " %s:%s <%s>", But->wsIp[j], But->wsPort[j], But->wsComment[j]);
        if ( But->wsIp[j][0] == 0 ) continue;
    Append:
        
        ret = wsAppendMenu(But->wsIp[j], But->wsPort[j], &But->menu_queue[j], But->wsTimeout);
        
        if (ret == WS_ERR_NO_CONNECTION) { 
            if(But->Debug) 
                logit("e","%s Could not get a connection to %s to get menu.\n", whoami, server);
        }
        else if (ret == WS_ERR_SOCKET) 
            logit("e","%s Could not create a socket for %s\n", whoami, server);
        else if (ret == WS_ERR_BROKEN_CONNECTION) {
            logit("e","%s Connection to %s broke during menu\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_TIMEOUT) {
            logit("e","%s Connection to %s timed out during menu.\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_MEMORY) 
            logit("e","%s Waveserver %s out of memory.\n", whoami, server);
        else if (ret == WS_ERR_INPUT) {
            logit("e","%s Connection to %s input error\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_PARSE) 
            logit("e","%s Parser failed for %s\n", whoami, server);
        else if (ret == WS_ERR_BUFFER_OVERFLOW) 
            logit("e","%s Buffer overflowed for %s\n", whoami, server);
        else if (ret == WS_ERR_EMPTY_MENU) 
            logit("e","%s Unexpected empty menu from %s\n", whoami, server);
        else if (ret == WS_ERR_NONE) {
            But->inmenu[j] = But->got_a_menu = 1;
        }
        else logit("e","%s Connection to %s returns error: %d\n", 
                   whoami, server, ret);
    }
    /* Let's make sure that servers in our server list have really connected.
       **********************************************************************/  
    for(j=0;j<But->nServer;j++) {
        if ( But->inmenu[j] ) {
            rc = wsGetServerPSCN( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
            if ( rc == WS_ERR_EMPTY_MENU ) {
                logit("e","%s Empty menu.\n", whoami);
                But->inmenu[j] = 0;
            }
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                logit("e","%s %s not in menu.\n", whoami, But->wsIp[j]);
                But->inmenu[j] = But->wsIp[j][0] = 0;
            }
        /* Then, detach 'em and let RequestWave attach only the ones it needs.
           **********************************************************************/  
            menu = But->menu_queue[j].head;
            if ( menu->sock > 0 ) wsDetachServer( menu );
        }
    }
    return 0;
}


/*************************************************************************
 *   short In_Menu (sta,comp,net)                                        *
 *      Determines if the scn is in the waveserver's menu                *
 *************************************************************************/

int In_Menu (Butler *But, char *sta, char *comp, char *net, 
                double *tankStarttime, double *tankEndtime)
{
    char    whoami[50];
    int     i, rc;
    WS_PSCN scnp;
      
    sprintf(whoami, " %s: %s: ", But->mod, "In_Menu");
    for(i=0;i<But->nServer;i++) {
        if(But->inmenu[i]) {
            rc = wsGetServerPSCN( But->wsIp[i], But->wsPort[i], &scnp, &But->menu_queue[i]);    
            if ( rc == WS_ERR_EMPTY_MENU )         continue;
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                if(But->Debug) logit("e","%s  %s:%s not in menu.\n", whoami, But->wsIp[i], But->wsPort[i]);
                But->inmenu[i] = 0;
                continue;
            }

            while ( 1 ) {
               if(strcmp(scnp->sta,  sta )==0 && 
                  strcmp(scnp->chan, comp)==0 && 
                  strcmp(scnp->net,  net )==0) {
                      *tankStarttime = scnp->tankStarttime;
                      *tankEndtime   = scnp->tankEndtime;
                      return 1;
               }
                if(comp[0] == 'T' && scnp->chan[0] == 'T') {
                    fprintf(stderr,"%s A Time channel.  <%s> <%s> <%s> :: <%s> <%s> <%s>\n", 
                        whoami, sta, comp, net, scnp->sta, scnp->chan, scnp->net );
                }
               if ( scnp->next == NULL )
                  break;
               else
                  scnp = scnp->next;
            }
        }
    }
    return 0;
}


/*************************************************************************
 *   In_Menu_list                                                        *
 *      Determines if the scn is in the waveservers' menu.               *
 *      If there, the tank starttime and endtime are returned.           *
 *      Also, the Server IP# and port are returned.                      *
 *************************************************************************/

int In_Menu_list (Butler *But, char *Site, char *Comp, char *Net)
{
    char    whoami[50], server[100], SCNtxt[25];
    long    j, rc;
    WS_PSCN scnp;
    
    sprintf(whoami, " %s: %s: ", But->mod, "In_Menu_list");
    sprintf(SCNtxt, " %s %s %s", Site, Comp, Net);
    But->nentries = 0;
    for(j=0;j<But->nServer;j++) {
        if(But->inmenu[j]) {
            sprintf(server, " %s:%s <%s>", But->wsIp[j], But->wsPort[j], But->wsComment[j]);
            rc = wsGetServerPSCN( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
            if ( rc == WS_ERR_EMPTY_MENU ) {
                if(But->Debug) logit("e","%s Empty menu for %s \n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                if(But->Debug) logit("e","%s  %s not in menu.\n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }

            while ( 1 ) {
               if(strcmp(scnp->sta,  Site)==0 && 
                  strcmp(scnp->chan, Comp)==0 && 
                  strcmp(scnp->net,  Net )==0) {
                  But->TStime[j] = scnp->tankStarttime;
                  But->TEtime[j] = scnp->tankEndtime;
                  But->index[But->nentries]  = j;
                  But->nentries += 1;
               }
               if ( scnp->next == NULL )
                  break;
               else
                  scnp = scnp->next;
            }
        }
    }
    if(But->nentries>0) return 1;
    return 0;
}


/*************************************************************************
 *   void Sort_Ark (n,dist,indx)                                         *
 *   --THIS is A VERSION OF THE HEAPSORT SUBROUTINE FROM THE NUMERICAL   *
 *     RECIPES BOOK. dist is REARRANGED IN ASCENDING ORDER &             *
 *     indx is PUT IN THE SAME ORDER AS dist.                            *
 *   long        n       !THE NUMBER OF VALUES OF dist TO BE SORTED      *
 *   double      dist(n) !SORT THIS ARRAY IN ASCENDING ORDER             *
 *   long        indx(n) !PASSIVELY REARRANGE THIS ARRAY TOO             *
 *************************************************************************/

void Sort_Ark (long n, double *dist, long *indx)
{
    long    indxh, ii, ir, i, j;
    double  disth;
    
    ii = n/2+1;    ir = n;
    
    while(1) {    
        if (ii > 1) {
            ii -= 1;
            disth      = dist[ii-1];   indxh      = indx[ii-1];
        }
        else {
            disth      = dist[ir-1];   indxh      = indx[ir-1];
            dist[ir-1] = dist[0];      indx[ir-1] = indx[0];
            ir -= 1;
            if(ir == 1) {
                dist[0] = disth;       indx[0]    = indxh;
                return;
            }
        }
        i = ii;   j = ii+ii;
        
        while (j <= ir) {
            if (j < ir && dist[j-1] < dist[j]) j += 1;
            
            if (disth < dist[j-1]) {
                dist[i-1] = dist[j-1]; indx[i-1] = indx[j-1];
                i = j;   j = j+j;
            }
            else    j = ir+1;
        }
        dist[i-1] = disth;   indx[i-1] = indxh;
    }
}


/*********************************************************************
 *   Build_SM_file()                                                 *
 *********************************************************************/

void Build_SM_file(Butler *But, Arkive *Ark)
{
    char    whoami[50], LinkNamea[50], LinkFNamea[50], FlingName[120];
    int     i, j, k, minute, yeargmt;
    double  lat, lon, sex, secs, sec1970 = 11676096000.00;  
    FILE    *out;
    struct Greg  g;
    
    sprintf(whoami, " %s: %s: ", But->mod, "Build_SM_file");
    
/* Do the Ml file
 *****************/    
    sex = Ark->EvntTime + sec1970;
    minute = (long) (sex / 60.0);
    grg(minute, &g);
    yeargmt = g.year;
    lat = Ark->EvntLat;
    lon = Ark->EvntLon;
    if(But->Debug)  logit("e", "%s Writing Ml File\n", whoami);   
    sprintf( LinkNamea,  "%s.%s", Ark->EvntIDS, "dig");      /* #####.dig */
    sprintf( LinkFNamea, "%s%s.0%s",        But->TmpDir, But->mod, ".dig");
    out = fopen(LinkFNamea, "wb");
    if(out == 0L) {
        logit("e", "%s Unable to write Ml File: %s\n", whoami, LinkFNamea);    
    } else {
        fprintf(out, "------------------------------------------------------------\n");
        fprintf(out, "                   MsubL for Event Number : %s    \n", Ark->EvntIDS);
        fprintf(out, "------------------------------------------------------------\n");
        fprintf(out, "Summary of Results :                                      \n\n");
        fprintf(out, "Origin Time      :  %.2d/%.2d/%.2d     %.2d:%.2d:%05.2f UTC \n",
                      yeargmt, Ark->EvntMonth, Ark->EvntDay, Ark->EvntHour,  Ark->EvntMin, Ark->EvntSec);
        fprintf(out, "Location         : Lat : %6.2f  Long : %7.2f  Depth : %6.2f  \n", lat, lon, Ark->EvntDepth);
        fprintf(out, "Magnitude        : Md :  %5.2f Ml :  %5.2f Ml(median) :  %5.2f \n", Ark->mag, Ark->Ml, Ark->Ml_Med);
        fprintf(out, "Components in Ml :   %d   Magnitude RMS : %5.2f  \n\n", Ark->NumPhs, Ark->rms);
        fprintf(out, "------------------------------------------------------------\n");
        fprintf(out, "Digital Seismic Network Results (in cgs) :    \n\n");
        fprintf(out, "Sta  Comp :   WA_Amp   Ml100\n");
        
        for(k=0;k<Ark->InList;k++) {
            j = Ark->index[k];
            if(Ark->A[j].reject==0) {
                fprintf(out, "%s  %s : %9.2f %7.2f\n", 
                Ark->A[j].Site, Ark->A[j].Comp,  Ark->A[j].WA_Amp, Ark->A[j].Ml100);
            } 
            if(But->Debug) {
                logit("e", "%s %s  %s : %9.2f %7.2f %d\n", 
                whoami, Ark->A[j].Site, Ark->A[j].Comp,  
                Ark->A[j].WA_Amp, Ark->A[j].Ml100, Ark->A[j].reject);
            }
        }
        
        fclose(out);

        if((int)strlen(But->OutDir) > 0) {
            sprintf(FlingName, "%s%s", But->OutDir, LinkNamea);
        } else {
            sprintf(FlingName, "%s", LinkNamea);
        }
        rename(LinkFNamea, FlingName);
    }
}


/**********************************************************************
 * date22 : Calculate 22 char date in the form Jan23,1988 12:34 12.21 *
 *          from the julian seconds.  Remember to leave space for the *
 *          string termination (NUL).                                 *
 **********************************************************************/
void date22( double secs, char *c22)
{
    char *cmo[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    struct Greg  g;
    long    minute;
    double  sex;
    double  sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's     */
                                       /* time 0 and 1970-01-01 00:00:00.0 GMT */

    secs += sec1970;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    sprintf(c22, "%3s%2d,%4d %.2d:%.2d:%05.2f",
            cmo[g.month-1], g.day, g.year, g.hour, g.minute, sex);
}


/*************************************************************************
 *   MinMax determines the maximum deflection.                           *
 *                                                                       *
 *************************************************************************/
int MinMax(long Npts, short decimation, double *Data, 
           double *Amp, double *Mid, double *Min, double *Max)
{
    double   min, max;
    int      i;
    
    min = max = Data[0];
    for(i=0;i<Npts;i+=decimation) {
        if(Data[i] != 919191) {
            if(Data[i]<min) min = Data[i];
            if(Data[i]>max) max = Data[i];
        }
    }
    *Amp = max - min;
    *Mid = min + *Amp/2.0;
    *Min = min;
    *Max = max;
    if(*Amp==0.0) return 1;
    return 0;
}


/*************************************************************************
 *  ML_Get_Sta_Info(Butler *But);                                        *
 *  Retrieve all the information available about the network stations    *
 *  and put it into an internal structure for reference.                 *
 *  This should eventually be a call to the database; for now we must    *
 *  supply an ascii file with all the info.                              *
 *NLH   NC KADE  38  7.3689 122  8.9528  1800.0   N 1 1.00               *
 *          1         2         3         4         5         6         7*
 *01234567890123456789012345678901234567890123456789012345678901234567890*
 *************************************************************************/
void ML_Get_Sta_Info(Butler *But)
{
    char    whoami[50];
    char     comp9, ns, ew, line[256];
    short    i, j, n, dlat, dlon, elevation, type, sensor, units;
    float    mlat, mlon, gain, sens, ssens, sitecorr;
    FILE    *file;

    sprintf(whoami, " %s: %s: ", But->mod, "ML_Get_Sta_Info");
    
    if( (file = fopen( But->MlStations, "r" )) == (FILE *) NULL ) {
        fprintf(stderr,
            "%s Cannot open site file <%s>; exiting!\n", whoami, But->MlStations);
    } else {
        But->NSCN = 0;
        while( fgets( line, sizeof(line), file ) != (char *) NULL ) {
            if( But->NSCN >= MAXCHANNELS ) {
                fprintf( stderr,
                    "%s Site table full; cannot load entire file <%s>\n", whoami, But->MlStations );
                fprintf( stderr,
                    "%s Use <maxsite> command to increase table size; exiting!\n", whoami );
                break;
            } 
            else {
                if(line[0] != '#' && (int)strlen(line) > 20) {
                    /* decode each line of the file */
                    j = But->NSCN;
                    
                    /* S C N */
                    strncpy( But->Chan[j].Site, &line[0],  6);
                    for(i=0;i<6;i++) if(But->Chan[j].Site[i]==' ') But->Chan[j].Site[i] = 0;
                    strncpy( But->Chan[j].Net,  &line[6],  2);
                    for(i=0;i<2;i++) if(But->Chan[j].Net[i]==' ')  But->Chan[j].Net[i]  = 0;
                    strncpy( But->Chan[j].Comp, &line[10], 3);
                    for(i=0;i<3;i++) if(But->Chan[j].Comp[i]==' ') But->Chan[j].Comp[i] = 0;
                    But->Chan[j].Comp[3] = But->Chan[j].Net[2] = But->Chan[j].Site[5] = 0;

                        /* use one-letter component if there is no 3-letter component given */
                    comp9 = line[9];
                    if ( !strcmp(But->Chan[j].Comp, "   ") ) sprintf( But->Chan[j].Comp, "%c  ", comp9 );

                    /* Lat Lon Elev */
                    n = sscanf( &line[15], "%hd %f", &dlat, &mlat);
                    if (n < 2) logit("e", "ML_Get_Sta_Info: Error decoding line in station file\n%s\n", line );
                    ns = line[25];
                    n = sscanf( &line[26], "%hd %f",  &dlon, &mlon);
                    if (n < 2) logit("e", "ML_Get_Sta_Info: Error decoding line in station file\n%s\n", line );
                    ew = line[37];
                    n = sscanf( &line[38], "%hd", &elevation );
                    if (n < 1) logit("e", "ML_Get_Sta_Info: Error decoding line in station file\n%s\n", line );

                        /* convert to decimal degrees */
                    if ( dlat < 0 ) dlat = -dlat;
                    if ( dlon < 0 ) dlon = -dlon;
                    But->Chan[j].Lat = (double) dlat + (mlat/60.0);
                    But->Chan[j].Lon = (double) dlon + (mlon/60.0);
                        /* make south-latitudes and west-longitudes negative */
                    if ( ns=='s' || ns=='S' )               But->Chan[j].Lat = -But->Chan[j].Lat;
                    if ( ew=='w' || ew=='W' || ew==' ' )    But->Chan[j].Lon = -But->Chan[j].Lon;
                    But->Chan[j].Elev = (double) elevation/1000.0;
                    
                    n = sscanf( &line[50], "%hd %f %f %hd %hd %f %f", 
                    &type, &sens, &gain, &sensor, &units, &ssens, &sitecorr);
                    if (n < 2) logit("e", "ML_Get_Sta_Info: Error decoding line in station file\n%s\n", line );
                    
                    if(units == 3) ssens /= 981.0;
                    But->Chan[j].Inst_type = type;
                    But->Chan[j].Gain = gain;
                    But->Chan[j].Sens_type = sensor;
                    But->Chan[j].Sens_unit = units;
                   
                    But->Chan[j].sensitivity = (1000000.0*ssens/sens)/gain*sitecorr;    /*    sensitivity counts/units        */
                    
                    But->NSCN++;
    
                }
            }
        }
        fclose(file);
    }
}


/************************************************************************

    Subroutine distaz (lat1,lon1,lat2,lon2,RNGKM,FAZ,BAZ)
    
c--  COMPUTES RANGE AND AZIMUTHS (FORWARD AND BACK) BETWEEN TWO POINTS.
c--  OPERATOR CHOOSES BETWEEN 3 FIRST ORDER ELLIPSOIDAL MODELS OF THE
c--  EARTH AS DEFINED BY THE MAJOR RADIUS AND FLATTENING.
c--  THE PROGRAM UTILIZES THE SODANO AND ROBINSON (1963) DIRECT SOLUTION
c--  OF GEODESICS (ARMY MAP SERVICE, TECH REP #7, SECTION IV).
c--  (TERMS ARE GIVEN TO ORDER ECCENTRICITY TO THE FOURTH POWER.)
c--  ACCURACY FOR VERY LONG GEODESICS:
c--          DISTANCE < +/-  1 METER
c--          AZIMUTH  < +/- .01 SEC
c
    Ellipsoid            Major Radius    Minor Radius    Flattening
1  Fischer 1960            6378166.0        6356784.28      298.30
2  Clarke1866              6378206.4        6356583.8       294.98
3  S. Am 1967              6378160.0        6356774.72      298.25
4  Hayford Intl 1910       6378388.0        6356911.94613   297.00
5  WGS 1972                6378135.0        6356750.519915  298.26
6  Bessel 1841             6377397.155      6356078.96284   299.1528
7  Everest 1830            6377276.3452     6356075.4133    300.8017
8  Airy                    6377563.396      6356256.81      299.325
9  Hough 1960              6378270.0        6356794.343479  297.00
10 Fischer 1968            6378150.0        6356768.337303  298.30
11 Clarke1880              6378249.145      6356514.86955   293.465
12 Fischer 1960            6378155.0        6356773.32      298.30
13 Intl Astr Union         6378160.0        6378160.0       298.25
14 Krasovsky               6378245.0        6356863.0188    298.30
15 WGS 1984                6378137.0        6356752.31      298.257223563
16 Aust Natl               6378160.0        6356774.719     298.25
17 GRS80                   6378137.0        63567552.31414  298.2572
18 Helmert                 6378200.0        6356818.17      298.30
19 Mod. Airy               6377341.89       6356036.143     299.325
20 Mod. Everest            6377304.063      6356103.039     300.8017
21 Mercury 1960            6378166.0        6356784.283666  298.30
22 S.E. Asia               6378155.0        6356773.3205    298.2305
23 Sphere                  6370997.0        6370997.0         0.0
24 Walbeck                 6376896.0        6355834.8467    302.78
25 WGS 1966                6378145.0        6356759.769356  298.25

************************************************************************/

int distaz (double lat1, double lon1, double lat2, double lon2, 
                double *rngkm, double *faz, double *baz)
{
    double    pi, rd, f, fsq;
    double    a3, a5, a6, a10, a16, a17, a18, a19, a20;
    double    a21, a22, a22t1, a22t2, a22t3, a23, a24, a25, a27, a28, a30;
    double    a31, a32, a33, a34, a35, a36, a38, a39, a40;
    double    a41, a43;
    double    a50, a51, a52, a54, a55, a57, a58, a62;
    double    dlon, alph[2];
    double    p11, p12, p13, p14, p15, p16, p17, p18;
    double    p21, p22, p23, p24, p25, p26, p27, p28, pd1, pd2;
    double    ang45;
    short    k;
    double    RAD, MRAD, FINV;
    
    RAD  = 6378206.4;
    MRAD = 6356583.8;
    FINV = 294.98;
    ang45 = atan(1.0);
    
    pi = 4.0*atan(1.0);
    rd = pi/180.0;

    a5 = RAD;
    a3 = FINV;
    
    if(a3==0.0) {
        a6 = MRAD;                    /*    minor radius of ellipsoid    */
        f = fsq = a10 = 0.0;
    }
    else {
        a6 = (a3-1.0)*a5/a3;          /*    minor radius of ellipsoid    */
        f = 1.0/a3;                   /*    flattening    */
        fsq = 1.0/(a3*a3);
        a10 = (a5*a5-a6*a6)/(a5*a5);  /*    eccentricity squared    */
    }
    
    /*    Following definitions are from Sodano algorithm for long geodesics    */
    a50 = 1.0 + f + fsq;
    a51 = f + fsq;
    a52 = fsq/ 2.0;
    a54 = fsq/16.0;
    a55 = fsq/ 8.0;
    a57 = fsq* 1.25;
    a58 = fsq/ 4.0;
    
/*     THIS IS THE CALCULATION LOOP. */
    
 POINT3:
    
    p11 = lat1*rd;
    p12 = lon1*rd;
    p21 = lat2*rd;
    p22 = lon2*rd;
    *rngkm = *faz = *baz = 0.0;
    if( (lat1==lat2) && (lon1==lon2))    return(0);
    
    /*    Make sure points are not exactly on the equator    */
    if(p11 == 0.0) p11 = 0.000001;
    if(p21 == 0.0) p21 = 0.000001;
    
    /*    Make sure points are not exactly on the same meridian    */
    if(p12 == p22)             p22 += 0.0000000001;
    if(fabs(p12-p22) == pi)    p22 += 0.0000000001;
    
    /*    Correct latitudes for flattening    */
    p13 = sin(p11);
    p14 = cos(p11);
    p15 = p13/p14;
    p18 = p15*(1.0-f);
    a62 = atan(p18);
    p16 = sin(a62);
    p17 = cos(a62);
    
    p23 = sin(p21);
    p24 = cos(p21);
    p25 = p23/p24;
    p28 = p25*(1.0-f);
    a62 = atan(p28);
    p26 = sin(a62);
    p27 = cos(a62);
    
    dlon = p22-p12;
    a16 = fabs(dlon);
    
    /*    Difference in longitude to minimum (<pi)    */
    if(a16 >= pi) a16 = 2.0*pi - a16;

    /*    Compute range (a35)    */
    if(a16==0.0)     {a17 = 0.0;         a18 = 1.0;}
    else             {a17 = sin(a16);    a18 = cos(a16);}
    a19 = p16*p26;
    a20 = p17*p27;
    a21 = a19 + a20*a18;
    
    a40 = a41 = a43 = 0.0;
    
    for(k=0; k<2; k++) {
        a22t1 = (a17*p27)*(a17*p27);
        a22t2 = p26*p17 - p16*p27*a18;
        a22   = sqrt(a22t1 + (a22t2*a22t2));
        if(a22 == 0.0) return(0);
        a22t3 = a22*a21;
        a23 = (a20*a17)/a22;
        a24 = 1.0 - a23*a23;
        a25 = asin(a22);
        if(a21 < 0.0) a25 = pi-a25;
        a27 = (a25*a25)/a22;
        a28 = a21*a27;
        if(k==0) {
            a30 = (a50*a25) + a19*(a51*a22-a52*a27);
            a31 = 0.5*a24*(fsq*a28 - a51*(a25 + a22t3));
            a32 = a19*a19*a52*a22t3;
            a33 = a24*a24*(a54*(a25 + a22t3) - a52*a28 - a55*a22t3*(a21*a21));
            a34 = a19*a24*a52*(a27 + a22t3*a21);
            a35 = (a30 + a31 - a32 + a33 + a34)*a6;
            *rngkm = a35/1000.0;
        }

    /*    Compute azimuths    */
        a36 = (a51*a25) - a19*(a52*a22 + fsq*a27) + a24*(a58*a22t3 - a57*a25 + fsq*a28);
        a38 = a36*a23 + a16;
        a39 = sin(a38);
        a40 = cos(a38);

        if(a39*p27 == 0.0) a43 = 0.0;
        else {
          a41 = (p26*p17 - a40*p16*p27)/(a39*p27);
          if(a41 == 0.0)    a43 = pi/2.0;
          else                a43 = atan(1.0/a41);
        }

        alph[k] = a43;
        if((dlon <= -pi) || ((dlon >= 0.0) && (dlon < pi))) {
          if(a41 >= 0.0)       alph[k] = alph[k]-pi;
        }
        else {
          if(a41 >= 0.0)       alph[k] = pi - alph[k];
          else                 alph[k] = 2.0*pi - alph[k];
        }
        if(alph[k] >= pi)      alph[k] = alph[k] - pi;
        if(alph[k] <  pi)      alph[k] = alph[k] + pi;
        if(alph[k] <  0.0)     alph[k] = alph[k] + 2.0*pi;
        if(alph[k] >= 2.0*pi)  alph[k] = alph[k] - 2.0*pi;
        alph[k] = alph[k]/rd;
        pd1 = p16;
        pd2 = p17;
        p16 = p26;
        p17 = p27;
        p26 = pd1;
        p27 = pd2;
        dlon = -dlon;
    }

    *faz = alph[0];
    *baz = alph[1];
    while (*faz >= 360.0) *faz = *faz - 360.0;
    while (*baz >= 360.0) *baz = *baz - 360.0;
    
    return(0);
}



