#
#                     Make file for html2txt
#                         Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = html2txt.o

html2txt: $O
	cc -o $B/html2txt $O -lm -lsocket -lnsl -lposix4

clean:
	/bin/rm -f html2txt *.o

clean_bin:
	/bin/rm -f html2txt
