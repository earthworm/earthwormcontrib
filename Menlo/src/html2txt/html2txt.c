/* THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU CHECKED IT OUT!
 *
 *  $Id: html2txt.c 608 2011-03-23 23:14:36Z dietz $
 * 
 *  Revision history:
 *   $Log$
 *   Revision 1.2  2007/09/25 17:49:34  dietz
 *   added RCS keywords to track changes
 *
 */

   /********************************************************************
    *                           html2txt.c                             *
    *                                                                  *
    *  Command-line program to strip off html tags and write a simple  *
    *  text file. For certain hard-coded tag values, will write        *
    *  pre-defined text to the output file.  Will write a space in     *
    *  place of all HTML entities (&xxxx;)                             *
    *                                                                  *
    *  Lynn Dietz  Sept 21, 2007                                       *
    ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Possible States of reading the HTML file
 ******************************************/
#define INSIDE_TAG   1
#define OUTSIDE_TAG  2
#define AFTER_ESCAPE 3
#define AFTER_AMP    4

/* Decimal values of ascii characters to watch for
 *************************************************/
#define LF          10  /* line feed '\n'  */
#define CR          13  /* carriage return */

/* HTML tags (case-insensitive) which produce special output
 ***********************************************************/
#define MAX_TAG      2             /* #char to compare in html tags   */
#define NUM_TAGACT   6             /* current number of newline tags  */

char *TagValue[NUM_TAGACT] =
                       { "TR",     /* table row       -> newline      */
                         "tr",     /* table row       -> newline      */
                         "BR",     /* break           -> newline      */
                         "br",     /* break           -> newline      */
                         "TD",     /* table delimiter -> space or tab */
                         "td" };   /* table delimiter -> space or tab */

char *TagAction[NUM_TAGACT] =
                       { "\n",     /* TR */
                         "\n",     /* tr */
                         "\n",     /* BR */
                         "\n",     /* br */
                         " " ,     /* TD */
                         " "  };   /* td */

int main( int argc, char *argv[] )
{
   FILE  *ifp;             /* file to read html from                   */
   FILE  *ofp;             /* file to write txt to                     */
   char   tag[MAX_TAG+1];  /* first MAX_TAG chars of html tag NTS      */
   char   c;               /* single character from file               */
   int    nctag;           /* number of chars currently stashed in tag */
   int    state;           /* current state of processing              */
   int    afterwhite = 0;  /* flag for skipping multiple whitespaces   */
   int    firstafteramp;   /* flag for reading after an ampersand      */
   int    numeric;         /* flag for reading html entities           */
   int    ita;

/* Check arguments
 *****************/
   if( argc != 3 )
   {
      fprintf( stderr, "Usage: html2txt <input_html_file> <output_txt_file>\n" );
      return( 1 );
   }

/* Open the files
 ****************/
   ifp = fopen( argv[1], "rb" );
   if( ifp == NULL )
   {
     fprintf( stderr, "html2txt: Cannot open input file <%s>\n", argv[1] );
     return( 1 );
   }

   ofp = fopen( argv[2], "wb" );
   if( ofp == NULL )
   {
     fprintf( stderr, "html2txt: Cannot open output <%s>\n",  argv[2] );
     return( 1 );
   }
   state      = OUTSIDE_TAG;  /* start on assumption we're not in a tag */
   afterwhite = 0;            /* we haven't written any whitespace */

/* Read thru file a character at a time
 ***************************************/
   while( fread( &c, sizeof(char), 1, ifp ) == sizeof(char) ) 
   {
   /* Process characters outside of html tags 
    *****************************************/
      if( state == OUTSIDE_TAG ) 
      {
         if( c == '<' )                  /* found beginning of tag */
         {
            state = INSIDE_TAG; 
            nctag = 0;
            continue; 
         }
         else if( c == '\\' )            /* found escape char; skip it */
         {
            state = AFTER_ESCAPE;
            continue;
         }
         else if( c == '&' )             /* found ampersand; skip it */
         {                               
            state = AFTER_AMP;
            firstafteramp = 1;     
            numeric       = 0;       
            continue;
         }
         else if( c == ';' )             /* found semicolon outside of tag; make it a line feed */
         {                               
            c = '\n';
         }
         else if( c == ' '  ||           /* found space,          */
                  c == '\t' ||           /* found tab,            */
                  c == LF   ||           /* found line feed '\n'  */
                  c == CR      )         /* found carriage return */
         {
            if( afterwhite ) continue;   /* ignore multiples      */
            c = ' ';                     /* replace first one with a space */
            afterwhite = 1;
         }   
         else
         {
            afterwhite = 0;
         }
         fwrite( &c, sizeof(char), 1, ofp );
      }

   /* Output a single char following an escape 
    ******************************************/
      else if( state == AFTER_ESCAPE ) 
      {   
         fwrite( &c, sizeof(char), 1, ofp );
         state = OUTSIDE_TAG;
      }

   /* Process chars inside of HTML entities (after '&')
    ***************************************************/
      else if( state == AFTER_AMP ) 
      {   
         if( firstafteramp ) 
         {
            firstafteramp = 0;
            if( c == '#' ) 
            {
               numeric = 1;   /* numeric html entity */
               continue;      /* go get next char */
            }
         }
         if( c == ';' )       /* found end of this html entity */
         {
            state = OUTSIDE_TAG;
            fwrite( " ", sizeof(char), strlen(" "), ofp ); /* output space instead of entity */
         }        
         else if( numeric && ( c<'0' || c>'9' ) )        /* found end of numeric html entity */
         { 
            state = OUTSIDE_TAG;
            fwrite( " ", sizeof(char), strlen(" "), ofp ); /* output space instead of entity */
            if( c == '<' ) state = INSIDE_TAG;             /* ignore if it's start of tag    */
            else fwrite( &c, sizeof(char), 1, ofp );       /* otherwise, write the character */
         }
      }

   /* Process characters inside of html tags 
    ****************************************/
      else if( state == INSIDE_TAG ) 
      {
         if( c == '>' )                  /* found the end of the tag */
         {
            tag[nctag] = 0;              /* null terminate tag */
         /* check if this tag requires special output 
          *******************************************/
            for( ita=0; ita<NUM_TAGACT; ita++ )  
            {                            
               if( strncmp( tag, TagValue[ita], 
                                strlen(TagValue[ita]) ) == 0 ) 
               {
                  fwrite( TagAction[ita], sizeof(char), strlen(TagAction[ita]), ofp );
                  break;
               }
            }
            state = OUTSIDE_TAG;
            continue;
         }
         else if( nctag == MAX_TAG )     /* already stored what we need; */
         {
            continue;                    /* skip this char in tag        */
         }   
         tag[nctag] = c;                 /* store first MAX_TAG chars of tag */
         nctag++;
      }
   } /* end while reading single char */

   fwrite( "\n", sizeof(char), strlen("\n"), ofp );  /* end file with newline */

   fclose( ifp );
   fclose( ofp );
   return( 0 );
}

