/******************************************************************************
 *  k2aud.c                                                                   *
 *  Read a K2-format header data file (from the NCSN system),                 *
 *  Reports changes.                                                          *
 *  Note: This is slightly different than the k2aud.c used by k2faudit.       *
 *  It is specific to K2s (no Mt Whitneys) cause thats all we have continuous *                                                                            *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include <chron3.h>
#include "sm_file2ew.h"

#include "nkwhdrs_jhl.h"              /* Kinemetrics header definitions */

#ifdef _INTEL
/*  macro returns byte-swapped version of given 16-bit unsigned integer */
#define BYTESWAP_UINT16(var) \
               ((unsigned short)(((unsigned short)(var)>>(unsigned char)8) + \
               ((unsigned short)(var)<<(unsigned char)8)))

/*  macro returns byte-swapped version of given 32-bit unsigned integer */
#define BYTESWAP_UINT32(var) \
                 ((unsigned long)(((unsigned long)(var)>>(unsigned char)24) + \
        (((unsigned long)(var)>>(unsigned char)8)&(unsigned long)0x0000FF00)+ \
        (((unsigned long)(var)<<(unsigned char)8)&(unsigned long)0x00FF0000)+ \
                                 ((unsigned long)(var)<<(unsigned char)24)))
#endif

#ifdef _SPARC
#define BYTESWAP_UINT16(var) (var)
#define BYTESWAP_UINT32(var) (var)
#endif

#define PACKET_MAX_SIZE  3003      /* 2992 + 8 + 3, same as QT */
#define BMSG_LEN 15000

extern double	tolerance;           /* Lat/long tolerance in degrees     */

/*****************************************************************************
 *  Define the structure for time records.                                   *
 *****************************************************************************/

typedef struct TStrct {   
    double  Time1600; /* Time (Sec since 1600/01/01 00:00:00.00)             */
    double  Time;     /* Time (Sec since 1970/01/01 00:00:00.00)             */
    int     Year;     /* Year                                                */
    int     Month;    /* Month                                               */
    int     Day;      /* Day                                                 */
    int     Hour;     /* Hour                                                */
    int     Min;      /* Minute                                              */
    double  Sec;      /* Second                                              */
} TStrct;

/* Functions in this source file 
 *******************************/
int nsmp2ew( FILE *fp, char *fname, char *, char * );
void Decode_Time( double, TStrct *);
int read_head( FILE *fp );
int Bcheck_head( char* );
int list_head( char*, int );
int EMail( char person[], char *subject, char *msg );

void logit( char *, char *, ... );   /* logit.c      sys-independent  */

/* Globals from sm_file2ew.c
 ***************************/
extern CHANNELNAME ChannelName[MAXCHAN]; /* table for box/chan to SCNL conversion */
extern int         NchanNames;           /* number of channel names in table      */
/*extern char        ArchiveDir[NAM_LEN];   directory to store known headers      */
extern int         Debug;
extern int     nPager;
extern char    person[MAX_PAGERS][50];
extern char    manager[50];

static int   print_head = 0;
K2_HEADER    head, ohead;
double   sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's        */
                                    /* time 0 and 1970-01-01 00:00:00.0 GMT    */

/******************************************************************************
 * nsmp2ew()  Read and process the .evt file.                                 *
 ******************************************************************************/
int nsmp2ew( FILE *fp, char *fname, char *ArchiveDir, char *HeaderDir )
{
    char     whoami[50], serial[10], fnew[90], aname[150], msg[200], Bmsg[BMSG_LEN];
    FILE     *fx;      
    int      ii, unknown, stat;
    double   Time;
    time_t   current_time;
    TStrct   Time1;    

/* Initialize variables 
 **********************/
    sprintf(whoami, " %s: %s: ", "k2f2audit", "nsmp2ew");
    memset( &ohead,  0, sizeof(K2_HEADER) );
    memset( &head,   0, sizeof(K2_HEADER) );

/* Close/reopen file in binary read
 **********************************/
    fclose( fp );
    fp = fopen( fname, "rb" );
    if( fp == NULL )                 return 0;
    if(strstr(fname, "k2hdr.") == 0) return 0;

/* First, just read the header & first frame to get some basic info. 
 ******************************************************************/
    rewind(fp);
    
    stat = read_head(fp);
    
    if(head.roParms.stream.flags != 0) {
        logit("e", "%s %s not data. flags = %d \n", 
                    whoami, fname, head.roParms.stream.flags);
    }
    
    sprintf(serial, "%d", head.rwParms.misc.serialNumber);
    unknown = 1;
    for(ii=0;ii<NchanNames;ii++) {
        if(strcmp(serial, ChannelName[ii].box) == 0) unknown = 0;
    }
    if(unknown) {
        sprintf(Bmsg, "K2 site %s (%s) unknown to database. (%s) \n", 
        		head.rwParms.misc.stnID, head.rwParms.misc.comment, fname);
        logit("e", "%s %s  \n", whoami, Bmsg);
		list_head(Bmsg, 1);
        sprintf(msg, "K2 site %s unknown to database.", head.rwParms.misc.stnID);
		EMail( manager, msg, Bmsg );
    }
    
    sprintf(aname, "%s%s.%d", ArchiveDir, head.rwParms.misc.stnID, head.rwParms.misc.serialNumber);
    if(Debug) logit("e", "%s archive file: %s   \n", whoami, aname);
    fx = fopen( aname, "rb" );
    if( fx == NULL ) {
    	fx = fopen( aname, "w" );
		if( fx == NULL ) {
			logit( "e", "%s: trouble opening archive file: %s \n", whoami, aname ); 
		} else {
	    	fwrite(&head, 1, sizeof(K2_HEADER), fx);
	    	fclose(fx);
			logit( "e", "%s: Created new archive file: %s \n", whoami, aname ); 
	        sprintf(Bmsg, "First data from K2 site %s (%s). (%s) \nNew archive file created.\n", 
	        	head.rwParms.misc.stnID, head.rwParms.misc.comment, fname);
	        logit("e", "%s %s  \n", whoami, Bmsg);
		    
			list_head(Bmsg, 1);
	        sprintf(msg, "First data from K2 site %s", head.rwParms.misc.stnID);
			
			EMail( manager, msg, Bmsg );
			for(ii=0;ii<nPager;ii++) EMail( person[ii], msg, Bmsg );
    	}
/*		return( 1 );   */
    } else {
    	fread(&ohead, 1, sizeof(K2_HEADER), fx);
    	fclose( fx );

	    sprintf(Bmsg, "The header for the instrument at %s (%s) is different than the last time it reported.\n", 
	    		head.rwParms.misc.stnID, head.rwParms.misc.comment);
	    
	    ii = Bcheck_head(Bmsg);
	    if(ii) {
			sprintf(msg, "Changes for K2 site %s", head.rwParms.misc.stnID);
			
			EMail( manager, msg, Bmsg );
			for(ii=0;ii<nPager;ii++) EMail( person[ii], msg, Bmsg );
	    }
	    
    	fx = fopen( aname, "w" );
		if( fx == NULL ) {
			logit( "e", "%s: trouble opening archive file: %s \n",
			      whoami, aname ); 
		} else {
	    	fwrite(&head, 1, sizeof(K2_HEADER), fx);
	    	fclose(fx);
    	}
    }
        
    sprintf(aname, "%s%s.%d.txt", HeaderDir, head.rwParms.misc.stnID, head.rwParms.misc.serialNumber);
    sprintf(Bmsg, "The header for the instrument at %s (%s) as last reported.\n", 
    		head.rwParms.misc.stnID, head.rwParms.misc.comment);
    Time = time(&current_time);
    Decode_Time( Time, &Time1);
    sprintf(msg, "Header written: %.2d/%.2d/%4d at %.2d:%.2d.\n\n", 
                  Time1.Month, Time1.Day, Time1.Year, Time1.Hour, Time1.Min);
    strcat(Bmsg, msg);
    
	list_head(Bmsg, Debug);
	fx = fopen( aname, "w" );
	if( fx == NULL ) {
		logit( "e", "%s: trouble opening header file: %s \n",
		      whoami, aname ); 
	} else {
    	fwrite(Bmsg, 1, strlen(Bmsg), fx);
    	fclose(fx);
    	if(Debug) logit( "e", "%s: Created new text header file: %s \n", whoami, aname );
	}
        
    return( 1 );
}



/**********************************************************************
 * Decode_Time : Decode time from seconds since 1970                  *
 *                                                                    *
 **********************************************************************/
void Decode_Time( double secs, TStrct *Time)
{
    struct Greg  g;
    long    minute;
    double  sex;

    Time->Time = secs;
    secs += sec1970;
    Time->Time1600 = secs;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    Time->Year  = g.year;
    Time->Month = g.month;
    Time->Day   = g.day;
    Time->Hour  = g.hour;
    Time->Min   = g.minute;
    Time->Sec   = sex;
}


/******************************************************************************
 * read_head(fp)  Read the file header, swap bytes if necessary, and print.   *
 ******************************************************************************/
int read_head( FILE *fp )
{
   long       la;
   int        i, nchans, stat;
   
/* Read in the file header.
   If a K2, there will be 2040 bytes.
 ************************************/
    /*
    stat = fread(&head, MAX_K2_CHANNELS, 1, fp);
    */
    nchans = MAX_K2_CHANNELS;
    stat = fread(&head, 1, 8, fp);
    stat = fread(&head.roParms.misc,       1, sizeof(struct MISC_RO_PARMS)+sizeof(struct TIMING_RO_PARMS), fp);
    stat = fread(&head.roParms.channel[0], 1, sizeof(struct CHANNEL_RO_PARMS)*nchans, fp);
    stat = fread(&head.roParms.stream,     1, sizeof(struct STREAM_RO_PARMS), fp);
    
    stat = fread(&head.rwParms.misc,       1, sizeof(struct MISC_RW_PARMS)+sizeof(struct TIMING_RW_PARMS), fp);
    stat = fread(&head.rwParms.channel[0], 1, sizeof(struct CHANNEL_RW_PARMS)*nchans, fp);
    
    stat = fread(&head.rwParms.stream,     1, sizeof(struct STREAM_K2_RW_PARMS), fp);
    
    stat = fread(&head.rwParms.modem,      1, sizeof(struct MODEM_RW_PARMS), fp);
    
    head.roParms.headerVersion = BYTESWAP_UINT16(head.roParms.headerVersion);
    head.roParms.headerBytes   = BYTESWAP_UINT16(head.roParms.headerBytes);
    
    if(Debug && print_head)
    logit("e", "HEADER: %c%c%c %d %hu %hu \n", 
            head.roParms.id[0], head.roParms.id[1], head.roParms.id[2], 
       (int)head.roParms.instrumentCode, 
            head.roParms.headerVersion, 
            head.roParms.headerBytes);
    
    head.roParms.misc.installedChan  = BYTESWAP_UINT16(head.roParms.misc.installedChan);
    head.roParms.misc.maxChannels    = BYTESWAP_UINT16(head.roParms.misc.maxChannels);
    head.roParms.misc.sysBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.sysBlkVersion);
    head.roParms.misc.bootBlkVersion = BYTESWAP_UINT16(head.roParms.misc.bootBlkVersion);
    head.roParms.misc.appBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.appBlkVersion);
    head.roParms.misc.dspBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.dspBlkVersion);
    head.roParms.misc.batteryVoltage = BYTESWAP_UINT16(head.roParms.misc.batteryVoltage);
    head.roParms.misc.crc            = BYTESWAP_UINT16(head.roParms.misc.crc);
    head.roParms.misc.flags          = BYTESWAP_UINT16(head.roParms.misc.flags);
    head.roParms.misc.temperature    = BYTESWAP_UINT16(head.roParms.misc.temperature);
    nchans = head.roParms.misc.maxChannels;
    
    if(Debug && print_head)
    logit("e", "MISC_RO_PARMS: %d %d %d %hu %hu %hu %hu %hu %hu %d %hu %hu %d \n", 
        (int)head.roParms.misc.a2dBits, 
        (int)head.roParms.misc.sampleBytes, 
        (int)head.roParms.misc.restartSource, 
            head.roParms.misc.installedChan,
            head.roParms.misc.maxChannels,
            head.roParms.misc.sysBlkVersion,
            head.roParms.misc.bootBlkVersion,
            head.roParms.misc.appBlkVersion,
            head.roParms.misc.dspBlkVersion,
            head.roParms.misc.batteryVoltage,
            head.roParms.misc.crc,
            head.roParms.misc.flags,
            head.roParms.misc.temperature );
    
    head.roParms.timing.gpsLockFailCount  = BYTESWAP_UINT16(head.roParms.timing.gpsLockFailCount);
    head.roParms.timing.gpsUpdateRTCCount = BYTESWAP_UINT16(head.roParms.timing.gpsUpdateRTCCount);
    head.roParms.timing.acqDelay          = BYTESWAP_UINT16(head.roParms.timing.acqDelay);
    head.roParms.timing.gpsLatitude       = BYTESWAP_UINT16(head.roParms.timing.gpsLatitude);
    head.roParms.timing.gpsLongitude      = BYTESWAP_UINT16(head.roParms.timing.gpsLongitude);
    head.roParms.timing.gpsAltitude       = BYTESWAP_UINT16(head.roParms.timing.gpsAltitude);
    head.roParms.timing.dacCount          = BYTESWAP_UINT16(head.roParms.timing.dacCount);
    head.roParms.timing.gpsLastDrift[0]   = BYTESWAP_UINT16(head.roParms.timing.gpsLastDrift[0]);
    head.roParms.timing.gpsLastDrift[1]   = BYTESWAP_UINT16(head.roParms.timing.gpsLastDrift[1]);
    
    head.roParms.timing.gpsLastTurnOnTime[0] = BYTESWAP_UINT32(head.roParms.timing.gpsLastTurnOnTime[0]);
    head.roParms.timing.gpsLastTurnOnTime[1] = BYTESWAP_UINT32(head.roParms.timing.gpsLastTurnOnTime[1]);
    head.roParms.timing.gpsLastUpdateTime[0] = BYTESWAP_UINT32(head.roParms.timing.gpsLastUpdateTime[0]);
    head.roParms.timing.gpsLastUpdateTime[1] = BYTESWAP_UINT32(head.roParms.timing.gpsLastUpdateTime[1]);
    head.roParms.timing.gpsLastLockTime[0]   = BYTESWAP_UINT32(head.roParms.timing.gpsLastLockTime[0]);
    head.roParms.timing.gpsLastLockTime[1]   = BYTESWAP_UINT32(head.roParms.timing.gpsLastLockTime[1]);
    
    if(Debug && print_head)
    logit("e", "TIMING_RO_PARMS: %d %d %d %hu %hu %d %d %d %d %hu %d %d %lu %lu %lu %lu %lu %lu \n", 
        (int)head.roParms.timing.clockSource, 
        (int)head.roParms.timing.gpsStatus, 
        (int)head.roParms.timing.gpsSOH, 
            head.roParms.timing.gpsLockFailCount, 
            head.roParms.timing.gpsUpdateRTCCount, 
            head.roParms.timing.acqDelay, 
            head.roParms.timing.gpsLatitude, 
            head.roParms.timing.gpsLongitude, 
            head.roParms.timing.gpsAltitude, 
            head.roParms.timing.dacCount,
            head.roParms.timing.gpsLastDrift[0], 
            head.roParms.timing.gpsLastDrift[1], 
            
            head.roParms.timing.gpsLastTurnOnTime[0], 
            head.roParms.timing.gpsLastTurnOnTime[1], 
            head.roParms.timing.gpsLastUpdateTime[0], 
            head.roParms.timing.gpsLastUpdateTime[1], 
            head.roParms.timing.gpsLastLockTime[0], 
            head.roParms.timing.gpsLastLockTime[1] );
           
    
    
    for(i=0;i<nchans;i++) {
        head.roParms.channel[i].maxPeak       = BYTESWAP_UINT32(head.roParms.channel[i].maxPeak);
        head.roParms.channel[i].maxPeakOffset = BYTESWAP_UINT32(head.roParms.channel[i].maxPeakOffset);
        head.roParms.channel[i].minPeak       = BYTESWAP_UINT32(head.roParms.channel[i].minPeak);
        head.roParms.channel[i].minPeakOffset = BYTESWAP_UINT32(head.roParms.channel[i].minPeakOffset);
        head.roParms.channel[i].mean          = BYTESWAP_UINT32(head.roParms.channel[i].mean);
        head.roParms.channel[i].aqOffset      = BYTESWAP_UINT32(head.roParms.channel[i].aqOffset);
        
        if(Debug && print_head)
        logit("e", "CHANNEL_RO_PARMS: %d %lu %d %lu %d %d \n", 
            head.roParms.channel[i].maxPeak,  
            head.roParms.channel[i].maxPeakOffset,  
            head.roParms.channel[i].minPeak,  
            head.roParms.channel[i].minPeakOffset,  
            head.roParms.channel[i].mean,  
            head.roParms.channel[i].aqOffset );
        
    }
    
    
    head.roParms.stream.startTime       = BYTESWAP_UINT32(head.roParms.stream.startTime);
    head.roParms.stream.triggerTime     = BYTESWAP_UINT32(head.roParms.stream.triggerTime);
    head.roParms.stream.duration        = BYTESWAP_UINT32(head.roParms.stream.duration);
    head.roParms.stream.errors          = BYTESWAP_UINT16(head.roParms.stream.errors);
    head.roParms.stream.flags           = BYTESWAP_UINT16(head.roParms.stream.flags);
    head.roParms.stream.nscans          = BYTESWAP_UINT32(head.roParms.stream.nscans);
    head.roParms.stream.startTimeMsec   = BYTESWAP_UINT16(head.roParms.stream.startTimeMsec);
    head.roParms.stream.triggerTimeMsec = BYTESWAP_UINT16(head.roParms.stream.triggerTimeMsec);
    
    if(Debug && print_head)
    logit("e", "STREAM_RO_PARMS: %d %d %d %d %d %d %d %d \n", 
            head.roParms.stream.startTime,  
            head.roParms.stream.triggerTime,  
            head.roParms.stream.duration,  
            head.roParms.stream.errors,  
            head.roParms.stream.flags,  
            head.roParms.stream.startTimeMsec,  
            head.roParms.stream.triggerTimeMsec,  
            head.roParms.stream.nscans  );
    
    head.rwParms.misc.serialNumber   = BYTESWAP_UINT16(head.rwParms.misc.serialNumber);
    head.rwParms.misc.nchannels      = BYTESWAP_UINT16(head.rwParms.misc.nchannels);
    head.rwParms.misc.elevation      = BYTESWAP_UINT16(head.rwParms.misc.elevation);
                                  la = BYTESWAP_UINT32(*(long *)(&head.rwParms.misc.latitude));
    head.rwParms.misc.latitude       = *(float *)(&(la));
                                  la = BYTESWAP_UINT32(*(long *)(&head.rwParms.misc.longitude));
    head.rwParms.misc.longitude      = *(float *)(&la);
    head.rwParms.misc.cutler_bitmap  = BYTESWAP_UINT32(head.rwParms.misc.cutler_bitmap);
    head.rwParms.misc.channel_bitmap = BYTESWAP_UINT32(head.rwParms.misc.channel_bitmap);
    
    if(Debug && print_head)
    logit("e", "MISC_RW_PARMS: %hu %hu %.5s %.33s %d %f %f %d %d %d %d %d %lo %d %.17s %d %d\n", 
            head.rwParms.misc.serialNumber,
            head.rwParms.misc.nchannels,
            head.rwParms.misc.stnID,  
            head.rwParms.misc.comment, 
            head.rwParms.misc.elevation,
            head.rwParms.misc.latitude, 
            head.rwParms.misc.longitude,
            
        (int)head.rwParms.misc.cutlerCode, 
        (int)head.rwParms.misc.minBatteryVoltage, 
        (int)head.rwParms.misc.cutler_decimation, 
        (int)head.rwParms.misc.cutler_irig_type, 
            head.rwParms.misc.cutler_bitmap,
            head.rwParms.misc.channel_bitmap,
        (int)head.rwParms.misc.cutler_protocol, 
            head.rwParms.misc.siteID, 
        (int)head.rwParms.misc.externalTrigger, 
        (int)head.rwParms.misc.networkFlag );
    
    head.rwParms.timing.localOffset = BYTESWAP_UINT16(head.rwParms.timing.localOffset);
    
    if(Debug && print_head)
    logit("e", "TIMING_RW_PARMS: %d %d %hu \n", 
        (int)head.rwParms.timing.gpsTurnOnInterval,  
        (int)head.rwParms.timing.gpsMaxTurnOnTime, 
            head.rwParms.timing.localOffset  );
    
    for(i=0;i<nchans;i++) {
        head.rwParms.channel[i].north      = BYTESWAP_UINT16(head.rwParms.channel[i].north);
        head.rwParms.channel[i].east       = BYTESWAP_UINT16(head.rwParms.channel[i].east);
        head.rwParms.channel[i].up         = BYTESWAP_UINT16(head.rwParms.channel[i].up);
        head.rwParms.channel[i].altitude   = BYTESWAP_UINT16(head.rwParms.channel[i].altitude);
        head.rwParms.channel[i].azimuth    = BYTESWAP_UINT16(head.rwParms.channel[i].azimuth);
        head.rwParms.channel[i].sensorType = BYTESWAP_UINT16(head.rwParms.channel[i].sensorType);
        head.rwParms.channel[i].sensorSerialNumber    = BYTESWAP_UINT16(head.rwParms.channel[i].sensorSerialNumber);
        head.rwParms.channel[i].sensorSerialNumberExt = BYTESWAP_UINT16(head.rwParms.channel[i].sensorSerialNumberExt);
        head.rwParms.channel[i].gain                  = BYTESWAP_UINT16(head.rwParms.channel[i].gain);
        head.rwParms.channel[i].StaLtaRatio           = BYTESWAP_UINT16(head.rwParms.channel[i].StaLtaRatio);
        
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].fullscale));
        head.rwParms.channel[i].fullscale             = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].sensitivity));
        head.rwParms.channel[i].sensitivity           = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].damping));
        head.rwParms.channel[i].damping               = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].naturalFrequency));
        head.rwParms.channel[i].naturalFrequency      = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].triggerThreshold));
        head.rwParms.channel[i].triggerThreshold      = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].detriggerThreshold));
        head.rwParms.channel[i].detriggerThreshold    = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].alarmTriggerThreshold));
        head.rwParms.channel[i].alarmTriggerThreshold = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].calCoil));
        head.rwParms.channel[i].calCoil               = *(float *)(&la);
        if(head.rwParms.channel[i].range != ' ' && (head.rwParms.channel[i].range < '0' || head.rwParms.channel[i].range > '9') )
        	head.rwParms.channel[i].range = ' ';
        if(head.rwParms.channel[i].sensorgain != ' ' && (head.rwParms.channel[i].sensorgain < '0' || head.rwParms.channel[i].sensorgain > '9') )
        	head.rwParms.channel[i].sensorgain = ' '; 
        
        if(Debug && print_head)
        logit("e", "CHANNEL_RW_PARMS: %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f %f %f %f %f %f %f %f %c %c %s %s \n", 
            head.rwParms.channel[i].id,  
            head.rwParms.channel[i].sensorSerialNumberExt,  
            head.rwParms.channel[i].north,  
            head.rwParms.channel[i].east,  
            head.rwParms.channel[i].up,  
            head.rwParms.channel[i].altitude,
            head.rwParms.channel[i].azimuth,
            head.rwParms.channel[i].sensorType,
            head.rwParms.channel[i].sensorSerialNumber,
            head.rwParms.channel[i].gain,
        (int)head.rwParms.channel[i].triggerType,  
        (int)head.rwParms.channel[i].iirTriggerFilter,  
        (int)head.rwParms.channel[i].StaSeconds,  
        (int)head.rwParms.channel[i].LtaSeconds,  
            head.rwParms.channel[i].StaLtaRatio,
        (int)head.rwParms.channel[i].StaLtaPercent,  
            head.rwParms.channel[i].fullscale,
            head.rwParms.channel[i].sensitivity,
            head.rwParms.channel[i].damping,
            head.rwParms.channel[i].naturalFrequency,
            head.rwParms.channel[i].triggerThreshold,
            head.rwParms.channel[i].detriggerThreshold,
            head.rwParms.channel[i].alarmTriggerThreshold,
            head.rwParms.channel[i].calCoil,
            head.rwParms.channel[i].range,
            head.rwParms.channel[i].sensorgain,
            head.rwParms.channel[i].networkcode,
            head.rwParms.channel[i].locationcode
        );
        
    }
    
    
    head.rwParms.stream.eventNumber = BYTESWAP_UINT16(head.rwParms.stream.eventNumber);
    head.rwParms.stream.sps         = BYTESWAP_UINT16(head.rwParms.stream.sps);
    head.rwParms.stream.apw         = BYTESWAP_UINT16(head.rwParms.stream.apw);
    head.rwParms.stream.preEvent    = BYTESWAP_UINT16(head.rwParms.stream.preEvent);
    head.rwParms.stream.postEvent   = BYTESWAP_UINT16(head.rwParms.stream.postEvent);
    head.rwParms.stream.minRunTime  = BYTESWAP_UINT16(head.rwParms.stream.minRunTime);
    head.rwParms.stream.Timeout     = BYTESWAP_UINT16(head.rwParms.stream.Timeout);
    head.rwParms.stream.TxBlkSize   = BYTESWAP_UINT16(head.rwParms.stream.TxBlkSize);
    head.rwParms.stream.BufferSize  = BYTESWAP_UINT16(head.rwParms.stream.BufferSize);
    head.rwParms.stream.SampleRate  = BYTESWAP_UINT16(head.rwParms.stream.SampleRate);
    head.rwParms.stream.TxChanMap   = BYTESWAP_UINT32(head.rwParms.stream.TxChanMap);
    head.rwParms.stream.VotesToTrigger   = BYTESWAP_UINT16(head.rwParms.stream.VotesToTrigger);
    head.rwParms.stream.VotesToDetrigger = BYTESWAP_UINT16(head.rwParms.stream.VotesToDetrigger);
    
    
    if(Debug && print_head)
    logit("e", "STREAM_K2_RW_PARMS: |%d| |%d| |%d| %d %d %d %d %d %d %d %d %d %d %d %d %d %d %lu\n", 
        (int)head.rwParms.stream.filterFlag,  
        (int)head.rwParms.stream.primaryStorage,  
        (int)head.rwParms.stream.secondaryStorage,  
            head.rwParms.stream.eventNumber,  
            head.rwParms.stream.sps,  
            head.rwParms.stream.apw,  
            head.rwParms.stream.preEvent,  
            head.rwParms.stream.postEvent,  
            head.rwParms.stream.minRunTime,  
            head.rwParms.stream.VotesToTrigger,
            head.rwParms.stream.VotesToDetrigger,
        (int)head.rwParms.stream.FilterType,  
        (int)head.rwParms.stream.DataFmt,  
            head.rwParms.stream.Timeout,
            head.rwParms.stream.TxBlkSize,
            head.rwParms.stream.BufferSize,
            head.rwParms.stream.SampleRate,
            head.rwParms.stream.TxChanMap
              );
            
    return stat;
}


/******************************************************************************
 * Bcheck_head(fp)  Check the file header against the historic header.        *
 ******************************************************************************/
int Bcheck_head( char *msg )
{
	char	string[150];
	double	elev_err = 10.0;
	int     i, nchans, stat;
   
	stat = 0;
    /**/
    if(Debug) logit("e", "HEADER: %c%c%c %d %hu %hu \n", 
            head.roParms.id[0], head.roParms.id[1], head.roParms.id[2], 
       (int)head.roParms.instrumentCode, 
            head.roParms.headerVersion, 
            head.roParms.headerBytes);
    
    if(ohead.roParms.instrumentCode != head.roParms.instrumentCode) {
	    stat = 1;
	    sprintf(string, "HEADER: instrumentCode %d has changed to: %d  \n", 
	            ohead.roParms.instrumentCode, head.roParms.instrumentCode );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.headerVersion != head.roParms.headerVersion) {
	    stat = 1;
	    sprintf(string, "HEADER: headerVersion %hu has changed to: %hu  \n", 
	            ohead.roParms.headerVersion, head.roParms.headerVersion );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.headerBytes != head.roParms.headerBytes) {
	    stat = 1;
	    sprintf(string, "HEADER: headerBytes %hu has changed to: %hu  \n", 
	            ohead.roParms.headerBytes, head.roParms.headerBytes );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    nchans = head.roParms.misc.maxChannels;
    
    /*
    logit("e", "MISC_RO_PARMS: %d %d %d %hu %hu %hu %hu %hu %hu %d %hu %hu %d \n", 
        (int)head.roParms.misc.a2dBits, 
        (int)head.roParms.misc.sampleBytes, 
        (int)head.roParms.misc.restartSource, 
            head.roParms.misc.installedChan,
            head.roParms.misc.maxChannels,
            head.roParms.misc.sysBlkVersion,
            head.roParms.misc.bootBlkVersion,
            head.roParms.misc.appBlkVersion,
            head.roParms.misc.dspBlkVersion,
            head.roParms.misc.batteryVoltage,
            head.roParms.misc.crc,
            head.roParms.misc.flags,
            head.roParms.misc.temperature );
    */
    
    if(ohead.roParms.misc.a2dBits != head.roParms.misc.a2dBits) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: a2dBits %d has changed to: %d  \n", 
	            ohead.roParms.misc.a2dBits, head.roParms.misc.a2dBits );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.sampleBytes != head.roParms.misc.sampleBytes) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: sampleBytes %d has changed to: %d  \n", 
	            ohead.roParms.misc.sampleBytes, head.roParms.misc.sampleBytes );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.restartSource != head.roParms.misc.restartSource) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: restartSource %d has changed to: %d  \n", 
	            ohead.roParms.misc.restartSource, head.roParms.misc.restartSource );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.installedChan != head.roParms.misc.installedChan) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: installedChan %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.installedChan, head.roParms.misc.installedChan );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.maxChannels != head.roParms.misc.maxChannels) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: maxChannels %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.maxChannels, head.roParms.misc.maxChannels );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.sysBlkVersion != head.roParms.misc.sysBlkVersion) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: sysBlkVersion %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.sysBlkVersion, head.roParms.misc.sysBlkVersion );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.bootBlkVersion != head.roParms.misc.bootBlkVersion) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: bootBlkVersion %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.bootBlkVersion, head.roParms.misc.bootBlkVersion );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.appBlkVersion != head.roParms.misc.appBlkVersion) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: appBlkVersion %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.appBlkVersion, head.roParms.misc.appBlkVersion );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.dspBlkVersion != head.roParms.misc.dspBlkVersion) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: dspBlkVersion %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.dspBlkVersion, head.roParms.misc.dspBlkVersion );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
            /*
    if(ohead.roParms.misc.batteryVoltage != head.roParms.misc.batteryVoltage) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: batteryVoltage %d has changed to: %d  \n", 
	            ohead.roParms.misc.batteryVoltage, head.roParms.misc.batteryVoltage );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
            */
    if(ohead.roParms.misc.crc != head.roParms.misc.crc) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: crc %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.crc, head.roParms.misc.crc );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.flags != head.roParms.misc.flags) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: flags %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.flags, head.roParms.misc.flags );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    
    /*
    logit("e", "TIMING_RO_PARMS: %d %d %d %hu %hu %d %d %d %d %hu %d %d %lu %lu %lu %lu %lu %lu \n", 
        (int)head.roParms.timing.clockSource, 
        (int)head.roParms.timing.gpsStatus, 
        (int)head.roParms.timing.gpsSOH, 
            head.roParms.timing.gpsLockFailCount, 
            head.roParms.timing.gpsUpdateRTCCount, 
            head.roParms.timing.acqDelay, 
            head.roParms.timing.gpsLatitude, 
            head.roParms.timing.gpsLongitude, 
            head.roParms.timing.gpsAltitude, 
            head.roParms.timing.dacCount,
            head.roParms.timing.gpsLastDrift[0], 
            head.roParms.timing.gpsLastDrift[1], 
            
            head.roParms.timing.gpsLastTurnOnTime[0], 
            head.roParms.timing.gpsLastTurnOnTime[1], 
            head.roParms.timing.gpsLastUpdateTime[0], 
            head.roParms.timing.gpsLastUpdateTime[1], 
            head.roParms.timing.gpsLastLockTime[0], 
            head.roParms.timing.gpsLastLockTime[1] );
    */
    
    if(ohead.roParms.timing.clockSource != head.roParms.timing.clockSource) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: clockSource %d has changed to: %d  \n", 
	            ohead.roParms.timing.clockSource, head.roParms.timing.clockSource );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    /*
    if(ohead.roParms.timing.gpsStatus != head.roParms.timing.gpsStatus) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: gpsStatus %d has changed to: %d  \n", 
	            ohead.roParms.timing.gpsStatus, head.roParms.timing.gpsStatus );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.timing.gpsSOH != head.roParms.timing.gpsSOH) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: gpsSOH %d has changed to: %d  \n", 
	            ohead.roParms.timing.gpsSOH, head.roParms.timing.gpsSOH );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    */
    if(ohead.roParms.timing.acqDelay != head.roParms.timing.acqDelay) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: acqDelay %d has changed to: %d  \n", 
	            ohead.roParms.timing.acqDelay, head.roParms.timing.acqDelay );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    /*
    if(ohead.roParms.timing.gpsLatitude != head.roParms.timing.gpsLatitude) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: gpsLatitude %d has changed to: %d  \n", 
	            ohead.roParms.timing.gpsLatitude, head.roParms.timing.gpsLatitude );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.timing.gpsLongitude != head.roParms.timing.gpsLongitude) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: gpsLongitude %d has changed to: %d  \n", 
	            ohead.roParms.timing.gpsLongitude, head.roParms.timing.gpsLongitude );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.timing.gpsAltitude != head.roParms.timing.gpsAltitude) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: gpsAltitude %d has changed to: %d  \n", 
		            ohead.roParms.timing.gpsAltitude, head.roParms.timing.gpsAltitude ); } 
    
    if(ohead.roParms.timing.dacCount != head.roParms.timing.dacCount) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: dacCount %d has changed to: %d  \n", 
	            ohead.roParms.timing.dacCount, head.roParms.timing.dacCount );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    */
    
    for(i=0;i<nchans;i++) {
        /*
        logit("e", "CHANNEL_RO_PARMS: %d %lu %d %lu %d %d \n", 
            head.roParms.channel[i].maxPeak,  
            head.roParms.channel[i].maxPeakOffset,  
            head.roParms.channel[i].minPeak,  
            head.roParms.channel[i].minPeakOffset,  
            head.roParms.channel[i].mean,  
            head.roParms.channel[i].aqOffset );
        */
    }
    
        
    /*
    logit("e", "STREAM_RO_PARMS: %d %d %d %d %d %d %d %d \n", 
            head.roParms.stream.startTime,  
            head.roParms.stream.triggerTime,  
            head.roParms.stream.duration,  
            head.roParms.stream.errors,  
            head.roParms.stream.flags,  
            head.roParms.stream.startTimeMsec,  
            head.roParms.stream.triggerTimeMsec,  
            head.roParms.stream.nscans  );
    */
    /*
    if(ohead.roParms.stream.startTime != head.roParms.stream.startTime) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: startTime %d has changed to: %d  \n", 
	            ohead.roParms.stream.startTime, head.roParms.stream.startTime );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.triggerTime != head.roParms.stream.triggerTime) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: triggerTime %d has changed to: %d  \n", 
	            ohead.roParms.stream.triggerTime, head.roParms.stream.triggerTime );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.duration != head.roParms.stream.duration) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: duration %d has changed to: %d  \n", 
	            ohead.roParms.stream.duration, head.roParms.stream.duration );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.errors != head.roParms.stream.errors) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: errors %d has changed to: %d  \n", 
	            ohead.roParms.stream.errors, head.roParms.stream.errors );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.flags != head.roParms.stream.flags) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: flags %d has changed to: %d  \n", 
	            ohead.roParms.stream.flags, head.roParms.stream.flags );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.startTimeMsec != head.roParms.stream.startTimeMsec) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: startTimeMsec %d has changed to: %d  \n", 
	            ohead.roParms.stream.startTimeMsec, head.roParms.stream.startTimeMsec );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.triggerTimeMsec != head.roParms.stream.triggerTimeMsec) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: triggerTimeMsec %d has changed to: %d  \n", 
	            ohead.roParms.stream.triggerTimeMsec, head.roParms.stream.triggerTimeMsec );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.nscans != head.roParms.stream.nscans) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: nscans %d has changed to: %d  \n", 
	            ohead.roParms.stream.nscans, head.roParms.stream.nscans );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    */
    /*
    logit("e", "MISC_RW_PARMS: %hu %hu %.5s %.33s %d %f %f %d %d %d %d %d %lo %d %.17s %d %d\n", 
            head.rwParms.misc.serialNumber,
            head.rwParms.misc.nchannels,
            head.rwParms.misc.stnID,  
            head.rwParms.misc.comment, 
            head.rwParms.misc.elevation,
            head.rwParms.misc.latitude, 
            head.rwParms.misc.longitude,
            
        (int)head.rwParms.misc.cutlerCode, 
        (int)head.rwParms.misc.minBatteryVoltage, 
        (int)head.rwParms.misc.cutler_decimation, 
        (int)head.rwParms.misc.cutler_irig_type, 
            head.rwParms.misc.cutler_bitmap,
            head.rwParms.misc.channel_bitmap,
        (int)head.rwParms.misc.cutler_protocol, 
            head.rwParms.misc.siteID, 
        (int)head.rwParms.misc.externalTrigger, 
        (int)head.rwParms.misc.networkFlag );
    */
    if(ohead.rwParms.misc.serialNumber != head.rwParms.misc.serialNumber) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: serialNumber %hu has changed to: %hu  \n", 
	            ohead.rwParms.misc.serialNumber, head.rwParms.misc.serialNumber );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.nchannels != head.rwParms.misc.nchannels) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: nchannels %hu has changed to: %hu  \n", 
	            ohead.rwParms.misc.nchannels, head.rwParms.misc.nchannels );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
	if(strcmp(ohead.rwParms.misc.stnID, head.rwParms.misc.stnID) != 0) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: stnID %s has changed to: %s  \n", 
	            ohead.rwParms.misc.stnID, head.rwParms.misc.stnID );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(abs(ohead.rwParms.misc.elevation - head.rwParms.misc.elevation) > elev_err) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: elevation %d has changed to: %d  \n", 
	            ohead.rwParms.misc.elevation, head.rwParms.misc.elevation );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(fabs(ohead.rwParms.misc.latitude - head.rwParms.misc.latitude) > tolerance) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: latitude %f has changed to: %f  \n", 
	            ohead.rwParms.misc.latitude, head.rwParms.misc.latitude );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(fabs(ohead.rwParms.misc.longitude - head.rwParms.misc.longitude) > tolerance) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: longitude %f has changed to: %f  \n", 
	            ohead.rwParms.misc.longitude, head.rwParms.misc.longitude );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.cutlerCode != head.rwParms.misc.cutlerCode) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: cutlerCode %d has changed to: %d  \n", 
	            ohead.rwParms.misc.cutlerCode, head.rwParms.misc.cutlerCode );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.minBatteryVoltage != head.rwParms.misc.minBatteryVoltage) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: minBatteryVoltage %d has changed to: %d  \n", 
	            ohead.rwParms.misc.minBatteryVoltage, head.rwParms.misc.minBatteryVoltage );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.cutler_decimation != head.rwParms.misc.cutler_decimation) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: cutler_decimation %d has changed to: %d  \n", 
	            ohead.rwParms.misc.cutler_decimation, head.rwParms.misc.cutler_decimation );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.cutler_irig_type != head.rwParms.misc.cutler_irig_type) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: cutler_irig_type %d has changed to: %d  \n", 
	            ohead.rwParms.misc.cutler_irig_type, head.rwParms.misc.cutler_irig_type );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.cutler_bitmap != head.rwParms.misc.cutler_bitmap) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: cutler_bitmap %d has changed to: %d  \n", 
	            ohead.rwParms.misc.cutler_bitmap, head.rwParms.misc.cutler_bitmap );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.channel_bitmap != head.rwParms.misc.channel_bitmap) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: channel_bitmap %lo has changed to: %lo  \n", 
	            ohead.rwParms.misc.channel_bitmap, head.rwParms.misc.channel_bitmap );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.cutler_protocol != head.rwParms.misc.cutler_protocol) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: cutler_protocol %d has changed to: %d  \n", 
	            ohead.rwParms.misc.cutler_protocol, head.rwParms.misc.cutler_protocol );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
	if(strcmp(ohead.rwParms.misc.siteID, head.rwParms.misc.siteID) != 0) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: siteID %s has changed to: %s  \n", 
	            ohead.rwParms.misc.siteID, head.rwParms.misc.siteID );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.externalTrigger != head.rwParms.misc.externalTrigger) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: externalTrigger %d has changed to: %d  \n", 
	            ohead.rwParms.misc.externalTrigger, head.rwParms.misc.externalTrigger );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.networkFlag != head.rwParms.misc.networkFlag) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: networkFlag %d has changed to: %d  \n", 
	            ohead.rwParms.misc.networkFlag, head.rwParms.misc.networkFlag );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    /*
    logit("e", "TIMING_RW_PARMS: %d %d %hu \n", 
        (int)head.rwParms.timing.gpsTurnOnInterval,  
        (int)head.rwParms.timing.gpsMaxTurnOnTime, 
            head.rwParms.timing.localOffset  );
    	*/
    
    if(ohead.rwParms.timing.gpsTurnOnInterval != head.rwParms.timing.gpsTurnOnInterval) {
	    stat = 1;
	    sprintf(string, "TIMING_RW_PARMS: gpsTurnOnInterval %d has changed to: %d  \n", 
	            ohead.rwParms.timing.gpsTurnOnInterval, head.rwParms.timing.gpsTurnOnInterval );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}

    if(ohead.rwParms.timing.gpsMaxTurnOnTime != head.rwParms.timing.gpsMaxTurnOnTime) {
	    stat = 1;
	    sprintf(string, "TIMING_RW_PARMS: gpsMaxTurnOnTime %d has changed to: %d  \n", 
	            ohead.rwParms.timing.gpsMaxTurnOnTime, head.rwParms.timing.gpsMaxTurnOnTime );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}

    if(ohead.rwParms.timing.localOffset != head.rwParms.timing.localOffset) {
	    stat = 1;
	    sprintf(string, "TIMING_RW_PARMS: localOffset %hu has changed to: %hu  \n", 
	            ohead.rwParms.timing.localOffset, head.rwParms.timing.localOffset );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    for(i=0;i<nchans;i++) {
        /*
        logit("e", "CHANNEL_RW_PARMS: %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f %f %f %f %f %f %f %f %c %c %s %s \n", 
            head.rwParms.channel[i].id,  
            head.rwParms.channel[i].sensorSerialNumberExt,  
            head.rwParms.channel[i].north,  
            head.rwParms.channel[i].east,  
            head.rwParms.channel[i].up,  
            head.rwParms.channel[i].altitude,
            head.rwParms.channel[i].azimuth,
            head.rwParms.channel[i].sensorType,
            head.rwParms.channel[i].sensorSerialNumber,
            head.rwParms.channel[i].gain,
        (int)head.rwParms.channel[i].triggerType,  
        (int)head.rwParms.channel[i].iirTriggerFilter,  
        (int)head.rwParms.channel[i].StaSeconds,  
        (int)head.rwParms.channel[i].LtaSeconds,  
            head.rwParms.channel[i].StaLtaRatio,
        (int)head.rwParms.channel[i].StaLtaPercent,
          
            head.rwParms.channel[i].fullscale,
            head.rwParms.channel[i].sensitivity,
            head.rwParms.channel[i].damping,
            head.rwParms.channel[i].naturalFrequency,
            head.rwParms.channel[i].triggerThreshold,
            head.rwParms.channel[i].detriggerThreshold,
            head.rwParms.channel[i].alarmTriggerThreshold
            head.rwParms.channel[i].calCoil,
            head.rwParms.channel[i].range,
            head.rwParms.channel[i].sensorgain,
            head.rwParms.channel[i].networkcode,
            head.rwParms.channel[i].locationcode
        );
    	*/
    
	    if(strcmp(ohead.rwParms.channel[i].id, head.rwParms.channel[i].id) != 0) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: id %s has changed to: %s  \n", 
		            i, ohead.rwParms.channel[i].id, head.rwParms.channel[i].id );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
  
	    if(strcmp(ohead.rwParms.channel[i].networkcode, head.rwParms.channel[i].networkcode) != 0) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: networkcode %s has changed to: %s  \n", 
		            i, ohead.rwParms.channel[i].networkcode, head.rwParms.channel[i].networkcode );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(strcmp(ohead.rwParms.channel[i].locationcode, head.rwParms.channel[i].locationcode) != 0) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: locationcode %s has changed to: %s  \n", 
		            i, ohead.rwParms.channel[i].locationcode, head.rwParms.channel[i].locationcode );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].sensorSerialNumberExt != head.rwParms.channel[i].sensorSerialNumberExt) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: sensorSerialNumberExt %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].sensorSerialNumberExt, head.rwParms.channel[i].sensorSerialNumberExt );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].north != head.rwParms.channel[i].north) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: north %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].north, head.rwParms.channel[i].north );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].east != head.rwParms.channel[i].east) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: east %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].east, head.rwParms.channel[i].east );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].up != head.rwParms.channel[i].up) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: up %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].up, head.rwParms.channel[i].up );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].altitude != head.rwParms.channel[i].altitude) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: altitude %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].altitude, head.rwParms.channel[i].altitude );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].azimuth != head.rwParms.channel[i].azimuth) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: azimuth %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].azimuth, head.rwParms.channel[i].azimuth );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].sensorType != head.rwParms.channel[i].sensorType) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: sensorType %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].sensorType, head.rwParms.channel[i].sensorType );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].sensorSerialNumber != head.rwParms.channel[i].sensorSerialNumber) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: sensorSerialNumber %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].sensorSerialNumber, head.rwParms.channel[i].sensorSerialNumber );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].gain != head.rwParms.channel[i].gain) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: gain %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].gain, head.rwParms.channel[i].gain );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].triggerType != head.rwParms.channel[i].triggerType) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: triggerType %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].triggerType, head.rwParms.channel[i].triggerType );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].iirTriggerFilter != head.rwParms.channel[i].iirTriggerFilter) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: iirTriggerFilter %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].iirTriggerFilter, head.rwParms.channel[i].iirTriggerFilter );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].StaSeconds != head.rwParms.channel[i].StaSeconds) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: StaSeconds %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].StaSeconds, head.rwParms.channel[i].StaSeconds );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].LtaSeconds != head.rwParms.channel[i].LtaSeconds) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: LtaSeconds %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].LtaSeconds, head.rwParms.channel[i].LtaSeconds );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].StaLtaRatio != head.rwParms.channel[i].StaLtaRatio) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: StaLtaRatio %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].StaLtaRatio, head.rwParms.channel[i].StaLtaRatio );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].StaLtaPercent != head.rwParms.channel[i].StaLtaPercent) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: StaLtaPercent %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].StaLtaPercent, head.rwParms.channel[i].StaLtaPercent );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].fullscale != head.rwParms.channel[i].fullscale) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: fullscale %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].fullscale, head.rwParms.channel[i].fullscale );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].sensitivity != head.rwParms.channel[i].sensitivity) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: sensitivity %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].sensitivity, head.rwParms.channel[i].sensitivity );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].damping != head.rwParms.channel[i].damping) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: damping %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].damping, head.rwParms.channel[i].damping );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
	    
	    if(ohead.rwParms.channel[i].naturalFrequency != head.rwParms.channel[i].naturalFrequency) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: naturalFrequency %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].naturalFrequency, head.rwParms.channel[i].naturalFrequency );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].triggerThreshold != head.rwParms.channel[i].triggerThreshold) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: triggerThreshold %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].triggerThreshold, head.rwParms.channel[i].triggerThreshold );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].detriggerThreshold != head.rwParms.channel[i].detriggerThreshold) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: detriggerThreshold %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].detriggerThreshold, head.rwParms.channel[i].detriggerThreshold );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].alarmTriggerThreshold != head.rwParms.channel[i].alarmTriggerThreshold) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: alarmTriggerThreshold %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].alarmTriggerThreshold, head.rwParms.channel[i].alarmTriggerThreshold );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].calCoil != head.rwParms.channel[i].calCoil) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: calCoil %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].calCoil, head.rwParms.channel[i].calCoil );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
        
    }
    
    
    /*
    logit("e", "STREAM_K2_RW_PARMS: |%d| |%d| |%d| %d %d %d %d %d %d %d %d %d %d %d %d %d %d %lu\n", 
        (int)head.rwParms.stream.filterFlag,  
        (int)head.rwParms.stream.primaryStorage,  
        (int)head.rwParms.stream.secondaryStorage,  
            head.rwParms.stream.eventNumber,  
            head.rwParms.stream.sps,  
            head.rwParms.stream.apw,  
            head.rwParms.stream.preEvent,  
            head.rwParms.stream.postEvent,  
            head.rwParms.stream.minRunTime,  
            head.rwParms.stream.VotesToTrigger,
            head.rwParms.stream.VotesToDetrigger,
        (int)head.rwParms.stream.FilterType,  
        (int)head.rwParms.stream.DataFmt,  
            head.rwParms.stream.Timeout,
            head.rwParms.stream.TxBlkSize,
            head.rwParms.stream.BufferSize,
            head.rwParms.stream.SampleRate,
            head.rwParms.stream.TxChanMap );
		*/
    
    if(ohead.rwParms.stream.filterFlag != head.rwParms.stream.filterFlag) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: filterFlag %d has changed to: %d  \n", 
	            ohead.rwParms.stream.filterFlag, head.rwParms.stream.filterFlag );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.primaryStorage != head.rwParms.stream.primaryStorage) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: primaryStorage %d has changed to: %d  \n", 
	            ohead.rwParms.stream.primaryStorage, head.rwParms.stream.primaryStorage );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.secondaryStorage != head.rwParms.stream.secondaryStorage) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: secondaryStorage %d has changed to: %d  \n", 
	            ohead.rwParms.stream.secondaryStorage, head.rwParms.stream.secondaryStorage );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.eventNumber != head.rwParms.stream.eventNumber) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: eventNumber %d has changed to: %d  \n", 
	            ohead.rwParms.stream.eventNumber, head.rwParms.stream.eventNumber );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.sps != head.rwParms.stream.sps) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: sps %d has changed to: %d  \n", 
	            ohead.rwParms.stream.sps, head.rwParms.stream.sps );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.apw != head.rwParms.stream.apw) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: apw %d has changed to: %d  \n", 
	            ohead.rwParms.stream.apw, head.rwParms.stream.apw );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.preEvent != head.rwParms.stream.preEvent) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: preEvent %d has changed to: %d  \n", 
	            ohead.rwParms.stream.preEvent, head.rwParms.stream.preEvent );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.postEvent != head.rwParms.stream.postEvent) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: postEvent %d has changed to: %d  \n", 
	            ohead.rwParms.stream.postEvent, head.rwParms.stream.postEvent );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.minRunTime != head.rwParms.stream.minRunTime) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: minRunTime %d has changed to: %d  \n", 
	            ohead.rwParms.stream.minRunTime, head.rwParms.stream.minRunTime );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.VotesToTrigger != head.rwParms.stream.VotesToTrigger) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: VotesToTrigger %d has changed to: %d  \n", 
	            ohead.rwParms.stream.VotesToTrigger, head.rwParms.stream.VotesToTrigger );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.VotesToDetrigger != head.rwParms.stream.VotesToDetrigger) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: VotesToDetrigger %d has changed to: %d  \n", 
	            ohead.rwParms.stream.VotesToDetrigger, head.rwParms.stream.VotesToDetrigger );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.FilterType != head.rwParms.stream.FilterType) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: FilterType %d has changed to: %d  \n", 
	            ohead.rwParms.stream.FilterType, head.rwParms.stream.FilterType );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.DataFmt != head.rwParms.stream.DataFmt) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: DataFmt %d has changed to: %d  \n", 
	            ohead.rwParms.stream.DataFmt, head.rwParms.stream.DataFmt );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.Timeout != head.rwParms.stream.Timeout) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: Timeout %d has changed to: %d  \n", 
	            ohead.rwParms.stream.Timeout, head.rwParms.stream.Timeout );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.TxBlkSize != head.rwParms.stream.TxBlkSize) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: TxBlkSize %d has changed to: %d  \n", 
	            ohead.rwParms.stream.TxBlkSize, head.rwParms.stream.TxBlkSize );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.BufferSize != head.rwParms.stream.BufferSize) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: BufferSize %d has changed to: %d  \n", 
	            ohead.rwParms.stream.BufferSize, head.rwParms.stream.BufferSize );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.SampleRate != head.rwParms.stream.SampleRate) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: SampleRate %d has changed to: %d  \n", 
	            ohead.rwParms.stream.SampleRate, head.rwParms.stream.SampleRate );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.TxChanMap != head.rwParms.stream.TxChanMap) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: TxChanMap %lu has changed to: %lu  \n", 
	            ohead.rwParms.stream.TxChanMap, head.rwParms.stream.TxChanMap );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    return stat;
}


/******************************************************************************
 * list_head(fp)  Check the file header against the historic header.         *
 ******************************************************************************/
int list_head( char *msg, int log )
{
   char		string[200];
   int      i, nchans, stat;
   
/* Read in the file header.
   If a K2, there will be 2040 bytes.
 ************************************/
    /*
    stat = fread(&head, 1, 8, fp);
    stat = fread(&head.roParms.misc,       1, sizeof(struct MISC_RO_PARMS)+sizeof(struct TIMING_RO_PARMS), fp);
    stat = fread(&head.roParms.channel[0], 1, sizeof(struct CHANNEL_RO_PARMS)*nchans, fp);
    stat = fread(&head.roParms.stream,     1, sizeof(struct STREAM_RO_PARMS), fp);
    
    stat = fread(&head.rwParms.misc,       1, sizeof(struct MISC_RW_PARMS)+sizeof(struct TIMING_RW_PARMS), fp);
    stat = fread(&head.rwParms.channel[0], 1, sizeof(struct CHANNEL_RW_PARMS)*nchans, fp);
    stat = fread(&head.rwParms.stream,     1, sizeof(struct STREAM_K2_RW_PARMS), fp);
    stat = fread(&head.rwParms.modem,      1, sizeof(struct MODEM_RW_PARMS), fp);
    */
    
    
    sprintf(string, "HEADER: ID: %c%c%c instrumentCode: %d headerVersion: %hu headerBytes: %hu \n", 
            head.roParms.id[0], head.roParms.id[1], head.roParms.id[2], 
       (int)head.roParms.instrumentCode, 
            head.roParms.headerVersion, 
            head.roParms.headerBytes);
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
    nchans = head.roParms.misc.maxChannels;
    
    sprintf(string, "MISC_RO_PARMS: a2dBits: %d sampleBytes: %d restartSource: %d installedChan: %hu maxChannels: %hu sysBlkVersion: %hu \n", 
        (int)head.roParms.misc.a2dBits, 
        (int)head.roParms.misc.sampleBytes, 
        (int)head.roParms.misc.restartSource, 
            head.roParms.misc.installedChan,
            head.roParms.misc.maxChannels,
            head.roParms.misc.sysBlkVersion);
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
    sprintf(string, "             : bootBlkVersion: %hu appBlkVersion: %hu dspBlkVersion: %hu batteryVoltage: %d crc: %hu flags: %hu temperature: %d \n", 
            head.roParms.misc.bootBlkVersion,
            head.roParms.misc.appBlkVersion,
            head.roParms.misc.dspBlkVersion,
            head.roParms.misc.batteryVoltage,
            head.roParms.misc.crc,
            head.roParms.misc.flags,
            head.roParms.misc.temperature );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
    sprintf(string, "TIMING_RO_PARMS: clockSource: %d gpsStatus: %d gpsSOH: %d gpsLockFailCount: %hu gpsUpdateRTCCount: %hu acqDelay: %d \n", 
        (int)head.roParms.timing.clockSource, 
        (int)head.roParms.timing.gpsStatus, 
        (int)head.roParms.timing.gpsSOH, 
            head.roParms.timing.gpsLockFailCount, 
            head.roParms.timing.gpsUpdateRTCCount, 
            head.roParms.timing.acqDelay );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
           
    sprintf(string, "               : gpsLatitude: %d gpsLongitude: %d gpsAltitude: %d dacCount: %hu \n", 
            head.roParms.timing.gpsLatitude, 
            head.roParms.timing.gpsLongitude, 
            head.roParms.timing.gpsAltitude, 
            head.roParms.timing.dacCount );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
           
    sprintf(string, "               : gpsLastDrift: %d %d gpsLastTurnOnTime: %lu %lu \n", 
            head.roParms.timing.gpsLastDrift[0], 
            head.roParms.timing.gpsLastDrift[1], 
            head.roParms.timing.gpsLastTurnOnTime[0], 
            head.roParms.timing.gpsLastTurnOnTime[1] );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
           
    sprintf(string, "               : gpsLastUpdateTime: %lu %lu gpsLastLockTime: %lu %lu \n", 
            head.roParms.timing.gpsLastUpdateTime[0], 
            head.roParms.timing.gpsLastUpdateTime[1], 
            head.roParms.timing.gpsLastLockTime[0], 
            head.roParms.timing.gpsLastLockTime[1] );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
         
    sprintf(string, "STREAM_RO_PARMS: startTime: %d triggerTime: %d duration: %d errs: %d flags: %d \n", 
            head.roParms.stream.startTime,  
            head.roParms.stream.triggerTime,  
            head.roParms.stream.duration,  
            head.roParms.stream.errors,  
            head.roParms.stream.flags );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
    sprintf(string, "               : startTimeMsec: %d triggerTimeMsec: %d nscans: %d \n", 
            head.roParms.stream.startTimeMsec,  
            head.roParms.stream.triggerTimeMsec,  
            head.roParms.stream.nscans  );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
          
    
    sprintf(string, "CHANNEL_RO_PARMS:  nchans: %d \n", nchans); 
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    for(i=0;i<nchans;i++) {
        sprintf(string, "   Chan: %2d maxPeak: %11d maxPeakOffset: %5lu minPeak: %11d minPeakOffset: %5lu mean: %d aqOffset: %d \n", 
            i,
            head.roParms.channel[i].maxPeak,  
            head.roParms.channel[i].maxPeakOffset,  
            head.roParms.channel[i].minPeak,  
            head.roParms.channel[i].minPeakOffset,  
            head.roParms.channel[i].mean,  
            head.roParms.channel[i].aqOffset );
	    if(log) logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
        
    }
    
    
    sprintf(string, "CHANNEL_RW_PARMS:  I \n"); 
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    for(i=0;i<nchans;i++) {
        sprintf(string, "   Chan: %2d id: %s %2s %5s %2s north: %d east: %d up: %d altitude: %3d azimuth: %3d sensorType: %2d sensorSerialNumber: %5d %d \n", 
            i,
            head.rwParms.misc.stnID,
            head.rwParms.channel[i].networkcode,  /* "NC",*/
            head.rwParms.channel[i].id,  
            head.rwParms.channel[i].locationcode,  /* "--",*/
            head.rwParms.channel[i].north,  
            head.rwParms.channel[i].east,  
            head.rwParms.channel[i].up,  
            head.rwParms.channel[i].altitude,
            head.rwParms.channel[i].azimuth,
            head.rwParms.channel[i].sensorType,
            head.rwParms.channel[i].sensorSerialNumber,
            head.rwParms.channel[i].sensorSerialNumberExt
		);
	    if(log) logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
        
    sprintf(string, "CHANNEL_RW_PARMS:  II \n"); 
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    for(i=0;i<nchans;i++) {
        sprintf(string, "   Chan: %2d gain: %d triggerType: %d iirTriggerFilter: %d StaSeconds: %d LtaSeconds: %d StaLtaRatio: %d StaLtaPercent: %d \n", 
            i,
            head.rwParms.channel[i].gain,
        (int)head.rwParms.channel[i].triggerType,  
        (int)head.rwParms.channel[i].iirTriggerFilter,  
        (int)head.rwParms.channel[i].StaSeconds,  
        (int)head.rwParms.channel[i].LtaSeconds,  
            head.rwParms.channel[i].StaLtaRatio,
        (int)head.rwParms.channel[i].StaLtaPercent );
	    if(log) logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    }
    sprintf(string, "CHANNEL_RW_PARMS:  III \n"); 
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    for(i=0;i<nchans;i++) {
        sprintf(string, "   Chan: %2d fullscale: %f sensitivity: %f damping: %f naturalFrequency: %9.6f \n", 
            i,
            head.rwParms.channel[i].fullscale,
            head.rwParms.channel[i].sensitivity,
            head.rwParms.channel[i].damping,
            head.rwParms.channel[i].naturalFrequency );
	    if(log) logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    }
    sprintf(string, "CHANNEL_RW_PARMS:  IV \n"); 
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    for(i=0;i<nchans;i++) {
        sprintf(string, "   Chan: %2d triggerThreshold: %f detriggerThreshold: %f alarmTriggerThreshold: %f calCoil: %f range: %c sensorgain: %c \n", 
            i,
            head.rwParms.channel[i].triggerThreshold,
            head.rwParms.channel[i].detriggerThreshold,
            head.rwParms.channel[i].alarmTriggerThreshold,
            head.rwParms.channel[i].calCoil, 
            head.rwParms.channel[i].range, 
            head.rwParms.channel[i].sensorgain );
	    if(log) logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    }
    
    
    sprintf(string, "MISC_RW_PARMS: serialNumber: %hu nchannels: %hu stnID: %.5s \n", 
            head.rwParms.misc.serialNumber,
            head.rwParms.misc.nchannels,
            head.rwParms.misc.stnID );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
    sprintf(string, "             : comment: %.33s elevation: %d latitude: %f longitude: %f \n", 
            head.rwParms.misc.comment, 
            head.rwParms.misc.elevation,
            head.rwParms.misc.latitude, 
            head.rwParms.misc.longitude );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
    sprintf(string, "             : cutlerCode: %d minBatteryVoltage: %d cutler_decimation: %d cutler_irig_type: %d cutler_bitmap: %d \n", 
        (int)head.rwParms.misc.cutlerCode, 
        (int)head.rwParms.misc.minBatteryVoltage, 
        (int)head.rwParms.misc.cutler_decimation, 
        (int)head.rwParms.misc.cutler_irig_type, 
            head.rwParms.misc.cutler_bitmap );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
    sprintf(string, "             : channel_bitmap: %lo cutler_protocol: %d siteID: %.17s externalTrigger: %d networkFlag: %d\n", 
            head.rwParms.misc.channel_bitmap,
        (int)head.rwParms.misc.cutler_protocol, 
            head.rwParms.misc.siteID, 
        (int)head.rwParms.misc.externalTrigger, 
        (int)head.rwParms.misc.networkFlag );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
    sprintf(string, "TIMING_RW_PARMS: gpsTurnOnInterval: %d gpsMaxTurnOnTime: %d localOffset: %hu \n", 
        (int)head.rwParms.timing.gpsTurnOnInterval,  
        (int)head.rwParms.timing.gpsMaxTurnOnTime, 
            head.rwParms.timing.localOffset  );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
    sprintf(string, "STREAM_K2_RW_PARMS: filterFlag: %d primaryStorage: %d secondaryStorage: %d eventNumber: %d sps: %d apw: %d \n", 
        (int)head.rwParms.stream.filterFlag,  
        (int)head.rwParms.stream.primaryStorage,  
        (int)head.rwParms.stream.secondaryStorage,  
            head.rwParms.stream.eventNumber,  
            head.rwParms.stream.sps,  
            head.rwParms.stream.apw );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	
    sprintf(string, "                  : preEvent: %d postEvent: %d minRunTime: %d VotesToTrigger: %d VotesToDetrigger: %d FilterType: %d \n", 
            head.rwParms.stream.preEvent,  
            head.rwParms.stream.postEvent,  
            head.rwParms.stream.minRunTime,  
            head.rwParms.stream.VotesToTrigger,
            head.rwParms.stream.VotesToDetrigger,
        (int)head.rwParms.stream.FilterType );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
    sprintf(string, "                  : DataFmt: %d Timeout: %d TxBlkSize: %d BufferSize: %d SampleRate: %d TxChanMap: %lu\n", 
        (int)head.rwParms.stream.DataFmt,  
            head.rwParms.stream.Timeout,
            head.rwParms.stream.TxBlkSize,
            head.rwParms.stream.BufferSize,
            head.rwParms.stream.SampleRate,
            head.rwParms.stream.TxChanMap );
    if(log) logit("e", "%s", string);
    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
            
    return stat;
}


/******************************************************************************
 * EMail ()
 *   recipient
 *   subject of the message
 *   message to send
 *
 ******************************************************************************/
int EMail( char person[], char *subject, char *msg )
{
   FILE *f;
   char cmnd[100];          /* Command line */
   char *msg_ptr, mailProg[100];	
   int  i, max_char;

   strcpy (mailProg, "/usr/ucb/Mail");

	/* Mail message without a subject */
	if (subject == NULL) {
		(void) sprintf( cmnd, "%s %s", mailProg, person );
	}
	/* Mail message with the given subject */
	else {
		(void) sprintf( cmnd, "%s -s \"%s\" %s", mailProg, subject, person );
	}

	if ( ( f = popen( cmnd, "w" ) ) == NULL ) {
		fprintf( stderr, "sendmail: Can't run command \"%s\"\n", cmnd );
		return( -1 );
	}

	/*  Body of the message  */
	msg_ptr = msg;
	max_char = BMSG_LEN;
	while ( *msg_ptr && max_char--) putc( *msg_ptr++, f );

	pclose( f );
   
   return( 0 );
}

