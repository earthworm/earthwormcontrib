
/* Define Strong Motion file types
 *********************************/
#define MAXCHAN     3000      /*largest number of channels we can handle */
#define MAX_PAGERS    30      /* Maximum number of Pager addresses       */
#define SM_VENDOR_LEN 49
#define SM_STA_LEN     6
#define SM_COMP_LEN    8
#define SM_NET_LEN     8
#define SM_LOC_LEN     5
#define SM_MAX_CHAN   18      /* max # channels in a TYPE_SM message     */
#define NAM_LEN 	 100      /* length of full directory name           */
#define BUFLEN     65000      /* define maximum size for an event msg    */

/* Structure Definitions
 ***********************/
typedef struct _CHANNELNAME_ {
   char	box[SM_VENDOR_LEN+1]; /* Installation-assigned box name              */
   int	chan;		          /* Channel number on this box                  */	
   char sta[SM_STA_LEN+1];    /* NTS: Site code as per IRIS SEED             */ 
   char comp[SM_COMP_LEN+1];  /* NTS: Channel/Component code as per IRIS SEED*/ 
   char net[SM_NET_LEN+1];    /* NTS: Network code as per IRIS SEED          */
   char loc[SM_LOC_LEN+1];    /* NTS: Location code as per IRIS SEED         */
} CHANNELNAME;

/* Function prototypes
 *********************/
int nsmp2ew( FILE *fp, char *fname, char *, char * );

