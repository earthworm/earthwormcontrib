#
# getqdds.d  -  Configuration file for the getqdds program
#
MyModName         MOD_GETQDDS       # Module name for this instance of getqdds.
                                    #   The specified name must appear in the
                                    #   earthworm.d configuration file.
RingName          HYPO_RING         # Triglist, heartbeat, and error messages
                                    #   are sent to this ring.
LogFile           1                 # Set to 0 to disable logging to disk.
HeartBeatInterval 15                # Interval between heartbeats sent to
                                    #   statmgr, in seconds.
PreEvent          30                # Start saving this many seconds before
                                    #   event time.
EventLength       1800              # Save this many seconds of data
#
InDir             /home/kohler/QDDS/outputdir
                                    # qdds files are read from this directory.
#
SaveDir           /home/kohler/QDDS/savedir
                                    # Acceptable QDDS event are saved here.
#
EventIdFname      /home/earthworm/run/params/event_id.getqdds
                                    # Name of file containing the event id
                                    #   to be assigned to the next qdds event
#
HerrinFname       /home/earthworm/run/params/herrin.table
                                    # File containing Direct P travel times
                                    # from the Herrin tables
#
# A maximum of 25 CompCode lines are allowed.
# They will all appear in the triglist message of each event,
# eg "*  VHZ  *"
#
CompCode          VHZ
CompCode          VDZ
CompCode          VDN
CompCode          VDE
CompCode          BHZ
CompCode          BHN
CompCode          BHE

