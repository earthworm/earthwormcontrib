
#include <stdio.h>

     /*****************************************************************
      *                           GetEventId()                        *
      *                                                               *
      *  Read the event id of the next event from the event id file.  *
      *                                                               *
      *  Returns  0 if all went well.                                 *
      *          -1 if the event id file could not be opened.         *
      *          -2 if the event id could not be read from the file.  *
      *****************************************************************/

int GetEventId( char fname[], int *event_id )
{
   int  rc;
   FILE *fp = fopen( fname, "r" );

   if ( fp == NULL ) return -1;
   rc = (fscanf( fp, "%d", event_id ) == 1) ? 0 : -2;
   fclose( fp );
   return rc;
}


     /*****************************************************************
      *                           PutEventId()                        *
      *                                                               *
      *  Write the event id of the next event to the event id file.   *
      *                                                               *
      *  Returns  0 if all went well.                                 *
      *          -1 if the event id file could not be opened.         *
      *          -2 if the event id could not be written.             *
      *****************************************************************/

int PutEventId( char fname[], int event_id )
{
   int  rc;
   FILE *fp = fopen( fname, "r+" );

   if ( fp == NULL ) return -1;
   rc = (fprintf( fp, "%d", event_id ) < 0) ? -2 : 0;
   fclose( fp );
   return rc;
}
