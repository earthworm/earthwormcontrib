
#include <stdio.h>
#include <math.h>

static double tt[201][21];
static double delval[201];
static double depval[21];


   /********************************************************************
    *                             herrin()                             *
    *                                                                  *
    *  Reads Herrin travel-time tables and sets up depval and delval.  *
    *  Returns: 0 if all is ok; < 0 for abnormal return.               *
    ********************************************************************/

int herrin( const char fname[] )
{
   int    i, j;
   FILE   *fp = fopen( fname, "r" );

   if ( fp == NULL ) return -1;     /* Error opening file */

/* Read table values.
   Convert minutes and seconds to seconds.
   **************************************/
   for ( i = 0; i < 201; i++ )
   {
      for ( j = 0; j < 21; j++ )
      {
         double xmin, sec;
         int    rc = fscanf( fp, " %lf %lf", &xmin, &sec );
         if ( (rc == EOF) || (rc < 2) )
         {
            fclose( fp );
            return -2;
         }
         tt[i][j] = 60.*xmin + sec;
      }
   }
   fclose( fp );

/* Fill in delval array.
   delval goes from  0 to 100 degrees.
   **********************************/
   delval[0] = 0.;
   for ( i = 1; i < 201; i++ )
      delval[i] = delval[i-1] + 0.5;

/* Fill in depval array.
   depval goes from 0 to 800 km.
   ****************************/
   depval[0] =  0.;
   depval[1] = 15.;
   depval[2] = 40.;
   depval[3] = 50.;
   for ( j = 4; j < 8; j++ )
      depval[j] = depval[j-1] + 25.;
   for ( j = 8; j < 21; j++ )
      depval[j] = depval[j-1] + 50.;
   return 0;
}


  /***********************************************************************
   *                              interp()                               *
   *                                                                     *
   *  Found original listing ("Index of Programs and Subroutines of the  *
   *  N.C.E.R. Computer Program Library", by M.S. Hamilton, D.A.         *
   *  Lombardero, W.H.K. Lee, and S.W. Stewart, published by National    *
   *  Center for Earthquake Research, U.S. Geological Survey, Menlo      *
   *  Park, CA, January, 1970).  Compared to listing, found and          *
   *  corrected bug in computation of derivative for cubic               *
   *  interpolation.  --J.R. Evans, 01/26/88.                            *
   ***********************************************************************/

void interp( double x, double x1, double x2, double x3, double x4,
             double f1, double f2, double f3, double f4, double *f,
             int cubic, double *fp, int deriv )
{
   double f12 = (f2 - f1)/(x2 - x1);   /* Argh... what if x1=x2 ??? */
   double f123;
   double f1234;

   *f = f1 + (x - x1)*f12;

   if ( cubic )
   {
      double f23   = (f3  - f2) /(x3 - x2);
      double f34   = (f4  - f3) /(x4 - x3);
      double f234  = (f34 - f23)/(x4 - x2);

      f123  = (f23  - f12) /(x3 - x1);
      f1234 = (f234 - f123)/(x4 - x1);
      *f += (x - x1)*(x - x2)*(f123 + (x - x3)*f1234);
   }

   if ( deriv )
   {
      *fp = f12;
      if ( cubic )
         *fp += (2.*x - x1 - x2)*f123 + (3.*x*x - 2.*x*(x1 + x2 + x3) +
                x1*x2 + x1*x3 + x2*x3)*f1234;
   }
   return;
}


    /*************************************************************
     *                          locate()                         *
     *                                                           *
     *  Compute index numbers, using delta (deg) and dep (km).   *
     *  Returns 0 if all ok, < 0 if there is a problem.          *
     *************************************************************/

int locate( double del, double dep, int *ii, int *jj )
{
   int i, j;

   if ( del <   0. ) return -1;
   if ( del > 100. ) return -2;
   if ( dep <   0. ) return -3;
   if ( dep > 800. ) return -4;

   for ( i = 0; i < 200; i++ )
      if ( del < delval[i+1] ) break;
   if ( i > 199 ) i = 199;
   *ii = i;

   for ( j = 0; j < 20; j++ )
      if ( dep < depval[j+1] ) break;
   if ( j > 19 ) j = 19;
   *jj = j;
   return 0;
}


    /******************************************************************
     *                            pkpint()                            *
     *                                                                *
     *    Table and interpolation for PKIKP Herrin (1968) table.      *
     *    Distance interpolation is hyperbolic (and derivative of     *
     *    the interpolating curve is returned as dtdd--slowness).     *
     *    Table values are corrected for depth before interpolation   *
     *    (depth correction is a linear interpolation                 *
     *    by depth and distance of Herrin depth correction table      *
     *    for PKIKP)   J.R.Evans  08/22/79                            *
     ******************************************************************/

int pkpint( double delta, double depth, double *tt, double *dtdd )
{
   const double tabl[] =
      {33.0, 34.9, 36.8, 38.7, 40.7, 42.6, 44.5, 46.4, 48.3, 50.2, 52.1,
      54.0, 55.9, 57.8, 59.8, 1.7, 3.7, 5.6, 7.5, 9.4, 11.3, 13.2, 15.1,
      17.0, 18.9, 20.8, 22.7, 24.6, 26.5, 28.3, 30.1, 31.9, 33.7, 35.4,
      37.1, 38.8, 40.5, 42.1, 43.7, 45.3, 46.8, 48.3, 49.8, 51.2, 52.6,
      53.9, 55.3, 56.6, 57.8, 58.9, 0.0, 1.0, 2.0, 3.0, 4.0, 4.9, 5.7,
      6.5, 7.2, 7.8, 8.4, 8.9, 9.3, 9.7, 10.1, 10.4, 10.6, 10.8, 10.9,
      11.0, 11.0};
   const double dcorr[] =
      {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 6.2,
      6.2, 6.2, 6.2, 6.2, 6.2, 7.4, 7.4, 7.4, 7.4, 7.4, 7.4, 13.5, 13.5,
      13.6, 13.6, 13.6, 13.6, 19.6, 19.6, 19.6, 19.7, 19.7, 19.8, 25.6,
      25.6, 25.7, 25.7, 25.8, 25.8, 31.4, 31.5, 31.6, 31.7, 31.7, 31.8,
      37.2, 37.3, 37.4, 37.5, 37.6, 37.6, 42.8, 42.9, 43.0, 43.1, 43.2,
      43.3, 48.3, 48.4, 48.5, 48.7, 48.8, 48.8, 53.6, 53.7, 53.9, 54.0,
      54.2, 54.2, 58.8, 58.9, 59.1, 59.3, 59.4, 59.5, 63.8, 63.9, 64.1,
      64.4, 64.5, 64.6, 68.7, 68.8, 69.1, 69.3, 69.5, 69.6, 73.4, 73.6,
      73.8, 74.1, 74.3, 74.4, 78.1, 78.2, 78.5, 78.8, 79.0, 79.1, 82.6,
      82.7, 83.0, 83.4, 83.6, 83.7, 87.0, 87.2, 87.5, 87.9, 88.2, 88.3};

   int    ii, jj;
   int    i1, i2, i3, i4;
   double d1, d2, dl1, dl2;
   double dt1, dt2, dc, t0;
   double a2b2, a2;
   double t1, t2;

/* Check validity of parameters
   ****************************/
   if ( depth >= 800. ) return -1;
   if ( delta <  110. ) return -2;
   if ( delta >  180. ) return -2;

/* Find depth range
   ****************/
   if ( depth >= 50. )
   {
      d1 = depth - fmod( depth, 50. );
      d2 = d1 + 50.;
      ii = (int)(d1/50. + 2.);
   }

   else if ( depth >= 40. )
      { d1 = 40.; d2 = 50.; ii = 2; }

   else if ( depth >= 15. )
      { d1 = 15.; d2 = 40.; ii = 1; }

   else
      { d1 =  0.; d2 = 15.; ii = 0; }

   ii *= 6;

/* Find distance range
   *******************/
   if ( delta >= 140. )
   {
      jj = (int)((delta - 140.)/10.) + 2;
      if ( jj == 6 ) jj = 5;
      dl1 = 140. + 10.*(double)(jj - 2);
      dl2 = dl1 + 10.;
   }
   else
   {
      jj  = 1;
      dl1 = 110.;
      dl2 = 140.;
   }
   if ( (jj > 5) || (jj < 1) ) return -3;   /* Delta out of range */

   i1 = ii + jj - 1;           /* Indexes into the dcorr array */
   i2 = ii + jj + 5;
   i3 = ii + jj;
   i4 = ii + jj + 6;

/* Linear interpolation of depth and delta for depth correction
   ************************************************************/
   dt1 = (depth - d1)*(dcorr[i2] - dcorr[i1])/(d2 - d1) + dcorr[i1];
   dt2 = (depth - d1)*(dcorr[i4] - dcorr[i3])/(d2 - d1) + dcorr[i3];
   dc  = (delta - dl1)*(dt2 - dt1)/(dl2 - dl1) + dt1;

/* Find coefficients for hyperbolic interpolation of main table.
   t0 is presumed asymptote intersection with 180 degrees.
   ************************************************************/
   t0 = 1306.90 - dc;
   ii = (int)(delta - 110.);

/* Extract table times, depth correct, and add in 18, 19, or 20
   minutes as appropriate.  Slowness is not well corrected for depth
   since only the depth correction at "delta" is used.  Slowness
   will be correct at depth=0 and times are correct at all depths.
   *****************************************************************/
   t1 = tabl[ii]   - dc + 1080.;
   t2 = tabl[ii+1] - dc + 1080.;

   if ( ii > 14 ) t1 += 60.;
   if ( ii > 13 ) t2 += 60.;
   if ( ii > 49 ) t1 += 60.;
   if ( ii > 48 ) t2 += 60.;

   dl1 = 109. + (double)ii;
   dl2 = dl1 + 1.;

   a2b2 = ((t1 - t0)*(t1 - t0) - (t2 - t0)*(t2 - t0))/
          ((dl1 - 180.)*(dl1 - 180.) - (dl2 - 180.)*(dl2 - 180.));

   a2 = (t1 - t0)*(t1 - t0) - a2b2*(dl1 - 180.)*(dl1 - 180.);

/* Interpolate
   ***********/
   *tt   = t0 - sqrt(a2 + a2b2*(delta - 180.)*(delta - 180.));
   *dtdd = a2b2*(180. - delta)/(t0 - *tt);
   return 0;
}


    /*******************************************************************
     *                             ttddel()                            *
     *                                                                 *
     *  Calculates the travel time "t" and the derivative of "t" with  *
     *  respect to distance ("dtddel").                                *
     *  Arguments:                                                     *
     *        del    Distance (degrees)                                *
     *        dep    Depth (km)                                        *
     *        t      Travel time stored here (seconds)                 *
     *        dtddel Derivative stored here (seconds/degree)           *
     *******************************************************************/

int ttddel( double del, double dep, double *t, double *dtddel )
{
   double f1, f2, f3, f4, fp;
   int    i, j;
   int    cubic = (del < 2.) ? 1: 0;

   if ( locate( del, dep, &i, &j ) < 0 ) return -1;

   if ( i > 197 ) i = 197;
   if ( j >  17 ) j =  17;

   interp( dep, depval[j], depval[j+1], depval[j+2], depval[j+3], tt[i][j],
           tt[i][j+1], tt[i][j+2], tt[i][j+3], &f1, cubic, &fp, 0 );

   interp( dep, depval[j], depval[j+1], depval[j+2], depval[j+3], tt[i+1][j],
           tt[i+1][j+1], tt[i+1][j+2], tt[i+1][j+3], &f2, cubic, &fp, 0 );

   interp( dep, depval[j], depval[j+1], depval[j+2], depval[j+3], tt[i+2][j],
           tt[i+2][j+1], tt[i+2][j+2], tt[i+2][j+3], &f3, cubic, &fp, 0 );

   interp( dep, depval[j], depval[j+1], depval[j+2], depval[j+3], tt[i+3][j],
           tt[i+3][j+1], tt[i+3][j+2], tt[i+3][j+3], &f4, cubic, &fp, 0 );

   interp( del, delval[i], delval[i+1], delval[i+2], delval[i+3], f1, f2,
           f3, f4, t, cubic, dtddel, 1 );
   return 0;
}

