
/*   getqdds.h    */
 
#define ERR_LINETOOBIG     0  /* Line in qdds file is too big for buffer */
#define ERR_QDDSDECODE     1  /* Error decoding qdds event line */
#define ERR_EVENTIDREADERR 2  /* Can't read event id from file */
#define ERR_EVENTIDWRITERR 3  /* Can't write event id to file */
#define ERR_PUTMSG         4  /* tport_putmsg() error */

#define MAXC             100  /* Size of buffer to contain line from qdds file */
#define TRIGBUFSIZE     4000  /* Size of triglist message buffer */
#define MAXCOMP           25  /* Maximum number of component codes */

