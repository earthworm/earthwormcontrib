#
#                     Make file for getqdds
#                         Solaris Version
#
#  The posix4 library is required for nanaosleep.
#
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = getqdds.o config.o event_id.o dstaz.o extract.o herrin.o \
    $L/sleep_ew.o $L/logit.o $L/time_ew.o $L/dirops_ew.o $L/kom.o \
    $L/getutil.o $L/transport.o

getqdds: $O
	cc -o $B/getqdds $O -lm -lposix4
