/*******************************
  adsendxs.h
 *******************************/

typedef struct
{
   char sta[6];
   char comp[4];
   char net[3];
   char loc[3];
   int  pin;
   char daqChan[16];
}
SCNL;

#define MAXCHAN  32          // Maximum number of channels to digitize
#define DLE 0x10
#define ETX 0x03
#define MAXGPSPACKETSIZE 100

// GetGpsTime() return values
#define CRITICAL_ERROR              -1
#define GPS_TIME_UNAVAILABLE         0
#define UNLOCKED_GPS_TIME_AVAILABLE  1
#define LOCKED_GPS_TIME_AVAILABLE    2

// antennaOpen values
#define FALSE    0
#define TRUE     1
#define UNKNOWN -1

// Statmgr error codes
#define NI_ERROR      1
#define GPS_ERROR     2
#define NOPULSE_ERROR 3
