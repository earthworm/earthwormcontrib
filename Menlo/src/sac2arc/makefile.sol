#   THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU HAVE
#   CHECKED IT OUT USING THE COMMAND CHECKOUT.
#
#    $Id: makefile.sol 103 2005-02-02 17:27:43Z dietz $
#
#    Revision history:
#     $Log$
#     Revision 1.1  2005/02/02 17:27:43  dietz
#     Simplified from sac2hypo to be more self-contained.
#
#

CFLAGS = ${GLOBALFLAGS} 

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

OBJ = sac2arc.o $(L)/chron3.o $(L)/time_ew.o 

sac2arc: $(OBJ)
	cc -o $(B)/sac2arc $(OBJ) -lnsl -lposix4 -lm

lint:
	lint sac2arc.c $(GLOBALFLAGS)

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/sac2arc*

