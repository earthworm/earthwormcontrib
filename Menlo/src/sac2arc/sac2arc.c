/*
 *   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT USING THE COMMAND CHECKOUT.
 *
 *    $Id: sac2arc.c 103 2005-02-02 17:27:43Z dietz $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.1  2005/02/02 17:27:43  dietz
 *     Simplified from sac2hypo to be more self-contained.
 *
 */

/*  sac2arc.c
 *  Greatly simplified version of sac2hypo which reads SAC headers
 *  from files listed in 'saclist' and writes a sparsely populated
 *  hypoinverse archive file (no shadows) containing picks and codas.
 *  This simple archive file can be used as input to hypoinverse to
 *  locate the event.  LDD 1/31/2005
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <earthworm.h>
#include <sachead.h>
#include <chron3.h>

#define MAXNAME 64

/* Function prototypes
 *********************/
double getSACreftime( struct SAChead * );
size_t SAC_Ktostr( char *s1, int len1, char *s2 );
void   blankpad( char *str, int length );

/* Define max lengths of various lines in the Hypoinverse archive format
   This length does not include a newline or Null character.
 ************************************************************************/
#define ARC_MAX_LEN   200
#define ARC_HYP_LEN   164
#define ARC_PHS_LEN   120
#define ARC_TRM_LEN    72

int main( int argc, char **argv )
{
   char          *sacfilelist = "saclist";
   char          *outfilename = "hypo.arc.in";
   char           infile[MAXNAME+1];
   char           line[ARC_MAX_LEN+1];
   FILE          *flist;
   FILE          *fin;         
   FILE          *fout;
   long           eventid;
   struct SAChead sachd;           
   int            status = EW_SUCCESS;
   int            nsac   = 0;
   int            i;
  
/* Open filename list 
   ******************/
   if ((flist = fopen (sacfilelist, "r")) == NULL)
   {
      fprintf (stderr, "Cannot open file: %s, exiting!\n", sacfilelist );
      return EW_FAILURE;
   }

/* Read the eventid from the 1st line of the file list
   *****************************************************/
   fgets (line, ARC_MAX_LEN, flist);
   if (sscanf (line, "EVENTID:%ld", &eventid ) != 1) 
   {
      fprintf (stderr, "Cannot read eventid from: %s in file %s; exiting!\n", 
               line, sacfilelist );
      fclose (flist);
      return EW_FAILURE;
   }

/* Open output file 
   ****************/
   fout = fopen( outfilename, "w" );
   if( fout == (FILE *)NULL )
   {
      fprintf (stderr, "Cannot open output file: %s, exiting!\n", outfilename );
      return EW_FAILURE;
   }

/* Loop over all SAC files in list
   *******************************/
   while (fgets (line, ARC_MAX_LEN, flist) != NULL)
   {
      double tref;      /* Reference time in SAC header   */
      char datestr[24]; /* working space for date strings */

      if (sscanf (line, "%s", infile) != 1) 
      {
         fprintf (stderr, 
                  "Cannot read sacfilename from file: %s, exiting!\n", 
                  sacfilelist);
         status = EW_FAILURE;
         break;
      }

   /* open this SAC file
      ******************/
      if ((fin = fopen (infile, "rb")) == NULL)
      {
         fprintf (stderr, "Cannot open file: %s, exiting!\n", infile);
         status = EW_FAILURE;
         break;
      }
	
   /* Read SAC header from file; close file; get reference time
      *********************************************************/
      if( fread( &sachd, sizeof(char), (size_t)SACHEADERSIZE, fin ) 
          != (size_t) SACHEADERSIZE )
      {
         fprintf (stderr, "Error reading SAC header from file: %s, exiting!\n", 
                  infile);
         status = EW_FAILURE;
         break;
      }
      fclose (fin);

      if(( tref = getSACreftime( &sachd ) ) < 0 )
      {
         fprintf (stderr, "Call to getSACreftime failed.\n");
         status = EW_FAILURE;
         break;
      }

   /* If first SAC file, write archive summary line
      *********************************************/
      if( nsac == 0 )
      {
          double ot = tref;  /* start with reference time as Origin Time */
          if( sachd.o  != (float)SACUNDEF ) ot += (double) sachd.o;
          date17( ot, datestr );
          memset( line, ' ', ARC_MAX_LEN );
          line[ARC_MAX_LEN] = 0;

          strncpy( line,     datestr,   14 );
          strncpy( line+14,  datestr+15, 2 );  /* skip the decimal point */
          sprintf( line+136, "%10ld", eventid );
          blankpad( line, ARC_HYP_LEN );
          fprintf( fout, "%s\n", line );
      }
 
   /* Write out archive phase line(s)
      ******************************/
      {
         char   sta[6];
         char   cmp[4];
         char   net[3];
         char   loc[3];
         char   Pdesc[5];
         char   Sdesc[5];
         double Parr = 0.0;
         double Sarr = 0.0;
         int    coda = 0;
 
         if( sachd.a  != (float)SACUNDEF ) Parr = tref + sachd.a;
         if( sachd.t0 != (float)SACUNDEF ) Sarr = tref + sachd.t0;
         if( sachd.f  != (float)SACUNDEF ) coda = (int)(sachd.f - sachd.a + 0.5);
         SAC_Ktostr( sta,   6, sachd.kstnm  );
         SAC_Ktostr( cmp,   4, sachd.kcmpnm );
         SAC_Ktostr( net,   3, sachd.knetwk );
         SAC_Ktostr( loc,   3, sachd.khole  );
         SAC_Ktostr( Pdesc, 5, sachd.ka     );
         SAC_Ktostr( Sdesc, 5, sachd.kt0    );

         if( Parr )
         {
            memset( line, ' ', ARC_MAX_LEN );
            line[ARC_MAX_LEN] = 0;
            date17( Parr, datestr );
            strncpy( line,     sta,      5 );
            strncpy( line+5,   net,      2 );
            strncpy( line+9,   cmp,      3 );
            strncpy( line+13,  Pdesc,    4 );
            strncpy( line+17,  datestr, 17 );
            if( coda ) sprintf( line+87, "%4d",  coda );
            strncpy( line+111, loc,      2 );
            blankpad( line, ARC_PHS_LEN );
            fprintf( fout, "%s\n", line );
         }
     
         if( Sarr )
         {
            memset( line, ' ', ARC_MAX_LEN );
            line[ARC_MAX_LEN] = 0;
            date17( Sarr, datestr );
            strncpy( line,     sta,        5 );
            strncpy( line+5,   net,        2 );
            strncpy( line+9,   cmp,        3 );
            strncpy( line+17,  datestr,   12 );  /* yyyymmddhhmm */
            strncpy( line+41,  datestr+12, 5 );  /* ss.hh */
            strncpy( line+46,  Sdesc,      4 );
            strncpy( line+111, loc,        2 );
            blankpad( line, ARC_PHS_LEN );
            fprintf( fout, "%s\n", line );
         }        
      }
      nsac++;

   } /* while loop over files in the sacfilelist */
  
/* Write terminator line 
 ***********************/
   if( status == EW_SUCCESS ) 
   {
      memset( line, ' ', ARC_MAX_LEN );
      line[ARC_MAX_LEN] = 0;
      sprintf( line+62, "%10ld", eventid );
      blankpad( line, ARC_TRM_LEN );
      fprintf( fout, "%s\n", line );
      fprintf( stderr, "Phase data from %d SAC files written to "
                       "Hypoinverse archive file: %s\n", 
               nsac, outfilename );
   }

/* close files
 *************/
   fclose (flist);
   fclose (fout);
   
   return (status);
}

 
/******************************************************
 * getSACreftime() reads the reference time in a SAC  *
 *   header and returns it in double seconds since    *
 *   1600 (Gregorian calendar)                        *
 ******************************************************/
double getSACreftime( struct SAChead *psac )
{
   struct Greg g;
   double      jsecs;
 
/* Find julian seconds from 1600 to Jan 1 of the correct year
 ************************************************************/
   g.year   = (int) psac->nzyear;
   g.month  = 1;
   g.day    = 1;
   g.hour   = (int) psac->nzhour;
   g.minute = (int) psac->nzmin;
   jsecs    = 60.0 * (double) julmin(&g);

/* Then add the number of seconds for the day-of-year */
   jsecs   += 86400.*(psac->nzjday-1);

/* And finally add the seconds and milliseconds */
   jsecs   += (double) psac->nzsec + (double) psac->nzmsec/1000.;

   return( jsecs );                              
}

/****************************************************************
 * SAC_Ktostr() convert a SAC header K-field (which is filled   *
 *   with white-space) into a null-terminated character string  *
 *   Returns the length of the new string.                      *
 ****************************************************************/
size_t SAC_Ktostr( char *s1, int len1, char *s2 )
{
   int i;
   char tmp[K_LEN+1];
 
/* NULL-fill the target
 **********************/
   for( i=0; i<len1; i++ ) s1[i]='\0';
 
/* Is the K-field NULL ?
 ***********************/
   strncpy( tmp, s2, K_LEN );
   tmp[K_LEN] = '\0';
   if( strcmp( tmp, SACSTRUNDEF ) == 0 )  return( 0 );
 
/* Null terminate after last non-space character
 ***********************************************/
   for( i=K_LEN-1; i>=0; i-- )     /* start at the end of the string */
   {
     if( tmp[i]!=' ' &&  tmp[i]!='\0' )  /* find last non-space char */
     {
        tmp[i+1]='\0';   /* null-terminate after last non-space char */
        break;
     }
   }
   if( i<0 ) return( 0 );  /* K-field was empty */
    
   strncpy( s1, tmp, len1-1 );
   return( strlen( s1 ) );
}
 
/*******************************************
 * Replace Nulls with blanks up in a string.
 * Null terminate string at length 
 ******************************************/
void blankpad( char *str, int length )
{ 
   int i;
   for( i=0; i<length; i++ ) {
      if( str[i] == 0 ) str[i] = ' ';
   }
   str[length] = 0;
   return;
}

