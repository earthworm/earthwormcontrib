# This is runwws's Parameter File

# Basic Earthworm setup
# ---------------------
 MyModuleId   MOD_RUNWWS    # Module id for this instance of runwws
 RingName     HYPO_RING     # Ring to get input from
 HeartbeatInt 30            # Seconds between heartbeats to statmgr
 LogFile      1             # 0 = turn off disk log file;
                            # 1 = turn on disk log
                            # 2 = write disk log but not to stderr/stdout

# PipeTo: Send output to a child program started with the following command.
# Do not use the --noinput option, because it causes the child process to
# ignore requests to shut down politely.
# -------------------------------------------------------------------------
# PipeTo "java -Xrs -cp wws-lib/winston.jar gov.usgs.winston.in.ImportEW imp-wws.d"
PipeTo "java -cp wws-lib/winston.jar gov.usgs.winston.server.WWS wws.d"


# ChildWaitTime: After asking the WWS java process to exit, runwws waits
# ChildWaitTime milliseconds for the child process to die.  If doesn't
# die in this amount of time, runwws forcibly terminates the child process.
# ------------------------------------------------------------------------
ChildWaitTime 5000
