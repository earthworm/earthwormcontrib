
CFLAGS = ${GLOBALFLAGS}

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib


RUNWWS = runwws.o $L/logit.o $L/time_ew.o $L/kom.o \
        $L/chron3.o $L/getutil.o $L/pipe.o \
        $L/sleep_ew.o $L/transport.o


runwws: $(RUNWWS)
	cc -o $B/runwws  $(RUNWWS)  -lm -lposix4


lint:
	lint runwws.c $(GLOBALFLAGS)

clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/runwws*
