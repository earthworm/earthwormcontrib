#
# This is the spectrogram parameter file. This module gets data gulps
# from the waveserver(s), and computes spectrograms

#  Basic Earthworm setup:
#
LogSwitch     1           # 0 to completely turn off disk log file
MyModuleId    MOD_SGRAM   # module id for this instance of report 
RingName      HYPO_RING   # ring to get input from
HeartBeatInt  15          # seconds between heartbeats

wsTimeout 40 # time limit (secs) for any one interaction with a wave server.

# List of wave servers (ip port comment) to contact to retrieve trace data.
 WaveServer 130.118.43.11  16022      "wsv  - image1"
 WaveServer 130.118.43.12  16022      "wsv  - image2"

# Filename prefix on target computer.  This is useful for identifying
# files for automated deletion via crontab.
 Prefix nc

# Directory in which to store the temporary .gif, .link and .html files.
 GifDir   /home/picker/gifs/sgram/ 

# Directory in which to store the .gif files for output
 LocalTarget   /home/picker/sendfiler/sgram1/

# The file with all the station information. (old format)
# StationList   /home/earthworm/run/params/calsta.db

# The file with all the station information. (new format)
 StationList1   /home/earthworm/run/params/calsta.db1

# Plot Parameters - sorry it's so complex, but that's the price of versatility
        # The following is designed such that each SCN creates it's own
        # spectrogram display; one per day of data.
# S  Site
# C  Component
# N  Network
# 04 Hours/Plot      Number of hours in each GIF image.
# 05 Plot Previous   On startup, retrieve and plot n previous hours from tank.
# 06 Local Time Diff UTC - Local.  e.g. -7 -> PDT; -8 -> PST
# 07 Local Time Zone Three character time zone name.
# 08 Show UTC        UTC will be shown on one of the time axes.
# 09 Use Local       The day page will be local day rather than UTC day.
# 10 XSize           Size of plot in inches. Setting this > 100 will imply pixels
# 11 Pixels/Line     Vertical pixels per line of trace displayed
# 12 Minutes/Line    Number of minutes per line of trace displayed
# 13 Seconds/Gulp    Number of seconds per fft
# 14 Freq Max        Maximum frequency
# 15 Freq Mute       Hz to mute at low end
# 16 nbw             Display scaling. 1:linear, 2:log 3:log(1st diff) [neg for greyscale]
# 17 amax            Clipping amplitude applied to spectrum [0 for none]
# 18 scale           Scale factor for wiggle-line display.
# 19 Comment         A comment for the top of the display.
#                                      
#     S    C   N  04 05 06  07  08 09 10   11  12 13 14 15    16  17        18  Comment
#                   

 Plot CBR  VDZ NC 24 1  -8  PST 1  1  350  1   1  60 10 0.2   2    2000    10.0 "Bollinger Canyon"
 Plot KCPB BHZ NC 24 1  -8  PST 1  1  350  1   1  60 10 0.2   2   150000    2.0 "Cahto Peak"
 Plot KHMB HHZ NC 24 1  -8  PST 1  1  350  1   1  60 10 0.2   2   150000    2.0 "Horse Mountain"
 Plot KMPB BHZ NC 24 1  -8  PST 1  1  350  1   1  60 10 0.2   2   150000    2.0 "Mount Pierce"
 Plot KRP  HHZ NC 24 1  -8  PST 1  1  350  1   1  60 10 0.2   2   150000    2.0 "Rodgers"
 Plot LDB  SHZ NC 24 1  -8  PST 1  1  350  1   1  60 10 0.2   2   150000    2.0 "Digger Butte"
 Plot MEM  VHZ NC 24 1  -8  PST 1  1  350  1   1  60 10 0.2   2   200000    2.0 "East Mammoth"
#                                      
#     S    C   N  L  04 05 06  07  08 09 10   11  12 13 14 15    16  17        18  Comment
#                   

 SCNL BBG  VHZ NC -- 24 1  -8  PST 1  1  350  1   1  60 10 0.2   2   150000    2.0 "Big Mountain"

    # *** Optional Commands ***

 UpdateInt    15      # Number of minutes between updates; default=2
 RetryCount    1      # Number of attempts to get a trace from server; default=10
 Logo    smusgs.gif   # Name of logo in GifDir to be plotted on each image

# We accept a command "SaveDrifts" which logs maximum values to GIF directory.
# SaveDrifts

Make_HTML
Days2Save

# We accept a command "BuildOnRestart" to totally rebuild images on restart.
# BuildOnRestart   

# We accept a command "DaysAgo" to build images for previous days.  
# This is an off-line process; sgram will exit when the images are done.
# DaysAgo 2

# We accept a command "UseDST" to automatically switch the local time to 
# daylight savings time on the appropriate dates.
 UseDST
 
#StandAlone

 PlotDown  
#PlotUp
# We accept a command "Debug" which turns on a bunch of log messages
# Debug
# WSDebug
