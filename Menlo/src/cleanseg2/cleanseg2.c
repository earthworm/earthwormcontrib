
    /*******************************************************************
     *                            cleanseg2                            *
     *                                                                 *
     *  Program to erase old directories containing SEG2 files which   *
     *  were created by organizefile.  The directory names contain     *
     *  the year, month, day, and hour.  Cleanseg2 is used in the      *
     *  SAFOD tape backup computers, safod-tp1 and safod-tp2.          *
     *  This program runs on Solaris only.                             *
     *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <earthworm.h>
#include <time_ew.h>

#define CMD_BUFLEN 256


int main( int argc, char **argv )
{
   int    rc;
   char   *topLevelDir; 
   double nhour;
   DIR    *dp;
   struct dirent *dentp;
   time_t now;

/* Get parameters from command line
   ********************************/
   if ( argc < 3 )
   {
      printf( "Usage: cleanseg2 <dir> <nhour>\n" );
      printf( "  dir   = Top-level directory containing SEG2 files and directories\n" );
      printf( "          The first 11 characters of the filename contain the ymd-h\n" );
      printf( "  nhour = Number of hours.  Files and directories whose names are\n" );
      printf( "          older than nhour hours are deleted.\n" );
      printf( "          nhour may be fractional, eg 3.25\n" );
      return 0;
   }
   topLevelDir = argv[1];
   rc = sscanf( argv[2], "%lf", &nhour );
   if ( rc < 1 )
   {
      printf( "Error decoding nhour.\n" );
      printf( "The string value is: %s\n", argv[2] );
      return 0;
   }

/* Open the top-level directory
   ****************************/
   dp = opendir( topLevelDir );
   if ( dp == NULL )
   {
      printf( "Error opening top-level directory: %s\n", topLevelDir );
      printf( "Exiting.\n" );
      return 0;
   }

/* Get current time, according to system clock
   *******************************************/
   time( &now );

/* Get directory names from the top-level directory
   ************************************************/
   while (  dentp = readdir( dp ) )
   {
      struct stat buf;
      struct tm   stm;
      time_t timeseg2;
      int    year, mon, day, hour;
      double agehour;
      char   syscmd[CMD_BUFLEN];

      stat( dentp->d_name, &buf );

      if ( !S_ISDIR(buf.st_mode) )        /* Ignore non-directories */
         continue;

/* Ignore non-date-based directories
   *********************************/
      if( dentp->d_name[8] == '-' )       /* it's a seg2fnc-type file name */
      {
        rc = sscanf( dentp->d_name, "%4d%2d%2d-%2d", &year, &mon, &day, &hour );
        if ( rc < 4 ) continue;
      }
      else if( dentp->d_name[0] == 'w' )  /* it's a ew2cta-type file name */
      {
        rc = sscanf( dentp->d_name, "wu%4d%2d%2d%2d", &year, &mon, &day, &hour );
        if ( rc < 4 ) continue;
      }
      else continue;                      /* don't know what format it is! */

/* Ignore directories containing recent data
   *****************************************/
      stm.tm_year = year - 1900;
      stm.tm_mon  = mon - 1;
      stm.tm_mday = day;
      stm.tm_hour = hour;
      stm.tm_min  = 0;
      stm.tm_sec  = 0;
      timeseg2 = timegm_ew( &stm );
      agehour  = difftime( now, timeseg2 ) / 3600.0;
      if ( agehour < nhour ) continue;

/* Erase the SEG2 directory containing one hour of data.
   There is no error handling here.  Probably, none is needed.
   **********************************************************/
      snprintf( syscmd, CMD_BUFLEN, "/bin/rm -r %s/%s", topLevelDir, dentp->d_name );
      system( syscmd );
   }

   closedir( dp );
   return 0;
}
