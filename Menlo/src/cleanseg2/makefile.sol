#
#                    Make file for cleanseg2
#                        Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib


OBJS = cleanseg2.o

cleanseg2: $(OBJS)
	cc -o $B/cleanseg2 $(OBJS) $L/time_ew.o -mt -lm -lposix4 -lthread -lc


# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/ls_cta*
