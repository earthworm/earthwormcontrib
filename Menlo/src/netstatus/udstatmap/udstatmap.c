/*************************************************************************
 *      udstatmap.c:                                                         *
 * On a time schedule (scheduler or crontab), reads station location     *
 * info from a file and plots sites onto GIF basemaps for use on the web.*
 * These files are then transferred to webserver(s).                     *
 *                                                                       *
 *                                                                       *
 *                                                                       *
 * Jim Luetgert    07/23/02                                              *
 *************************************************************************/

#include <platform.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <kom.h>

#include "gd.h"
#include "gdfontt.h"   /*  6pt      */
#include "gdfonts.h"   /*  7pt      */
#include "gdfontmb.h"  /*  9pt Bold */
#include "gdfontl.h"   /* 13pt      */
#include "gdfontg.h"   /* 10pt Bold */

#include "udstatmap.h"

/* Functions in this source file
 *******************************/
void add_data_to_base_map (Global *But, MapInfo *Map, char *BaseMapFile, char *OutputMapFile);
void initial_tgd_stuff (char *GifFile);
void plot_data_on_index_map(Global *But, MapInfo *Map);
void get_data_clr(int, long *clr);
void get_data_symbol(int halfhtpix, int xpix, int ypix, int *xlft, int *xrgt, int *ytop, int *ybot); 
void get_label_loc(int hour, int xpix, int ypix, int *xname, int *yname);
void Pallette(gdImagePtr GIF, long color[]);
void update_html_on_index_map(Global *But, MapInfo *Map, char *outfile);
void Get_Sta_Info(Global *But);
void config_me(Global *But, char *); 

/* Things to read from configuration file
 ****************************************/
static int  Debug = 1;               /* debug flag                      */

/* Other globals
 ***************/
Global    But;
char      module[50];

gdImagePtr im_in, im_out;
long       gcolor[MAXCOLORS];        /* GIF colors                            */

/*************************************************************************
 *  main( int argc, char **argv )                                        *
 *************************************************************************/

main( int argc, char **argv )
{
    int   i, j, k;
    char  whoami[50], temp[150], infile[150], outfile[150], mapfile[150], scnfile[150], htmfile[150], s[80];
    char  string[500];
    FILE    *in;
    FILE    *out, *hlist;
    time_t  now;
    struct tm *date;
    
    /* Check command line arguments
     ******************************/
    if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", "udstatmap");
        exit( 0 );
    }
    
    strcpy(module, argv[1]);
    for(i=0;i<(int)strlen(module);i++) if(module[i]=='.') module[i] = 0;

    sprintf(whoami, " %s: %s: ", module, "main");

    /* Read the configuration file(s)
     ********************************/
    config_me(&But, argv[1] );
    
    time(&now);
    date = localtime(&now);
    strftime(s,80,"%c",date);
    fprintf(stdout, "%s\n", s);

    if(Debug) fprintf( stdout, "Getting station info. \n");
    Get_Sta_Info(&But);
    if(Debug) {
        fprintf( stdout, "Station info: \n");
        for(i=0;i<But.NSCN;i++) {
            fprintf( stdout, "%s %s %s %s %f %f %d  ", 
            But.Chan[i].Site, But.Chan[i].Comp, But.Chan[i].Net, But.Chan[i].Loc, 
            But.Chan[i].Lat,  But.Chan[i].Lon,  But.Chan[i].priority);
            for(k=0;k<10;k++) fprintf( stdout, " %d ",  But.Chan[i].errtype[k]);
            fprintf( stdout, "\n"); 
        }
    }
    
    for(i=0;i<But.nltargets;i++) {
        if(Debug) fprintf( stdout, "Building files for number %d of %d directories. \n", i+1, But.nltargets);
    /* This is the index map of stations.
    ***********************************************************/
        But.BigMap = 1;
        sprintf(infile,  "%s%s", But.BaseMapDir, But.Index.BaseMap);
        sprintf(outfile, "%s%s", But.loctarget[i],  But.Index.BaseMap);
        if(Debug) fprintf( stdout, "Adding data to %s. -> %s \n", infile, outfile);
        add_data_to_base_map(&But, &(But.Index), infile, outfile);

    /* These are the 2-deg maps of stations with links and buttons.
    ***************************************************************/
        But.BigMap = 0;
        for(j=0;j<But.Nmaps;j++) {
            sprintf(infile,  "%s%s", But.BaseMapDir, But.Map[j].BaseMap);
            sprintf(outfile, "%s%s", But.loctarget[i],  But.Map[j].BaseMap);
            if(Debug) fprintf( stdout, "Adding data to %s. -> %s \n", infile, outfile);
            add_data_to_base_map(&But, &(But.Map[j]), infile, outfile);

            sprintf(mapfile, "%s%s", But.GifDir, "select.map");
            if(Debug) fprintf( stdout, "Making html. \n");
            update_html_on_index_map(&But, &(But.Map[j]), mapfile);
            
            strcpy(temp, But.Map[j].BaseMap);
            for(k=0;k<strlen(temp);k++) {
                if(temp[k] == '.') {temp[k] = 0; break;}
            }
            sprintf(htmfile, "%s%s.html", But.loctarget[i], temp);
            sprintf(infile,  "%s%s", But.BaseMapDir, "indexa.html");
            sprintf(temp,  "%s%s", But.BaseMapDir, "indexb.html");
            if(Debug) fprintf( stdout, "Building file -> %s from %s %s %s\n", htmfile, infile, mapfile, temp);
            
            out = fopen(htmfile, "wb");
            if(out == 0L) {
                fprintf( stdout, "%s Unable to open listb.html File: %s\n", whoami, htmfile);    
            } else {
                in  = fopen(infile, "r");
                if(in == 0L) {
                    fprintf( stdout, "%s Unable to open indexa.html File: %s\n", whoami, infile);    
                } else {
                    while(fgets(string, 480, in)!=0L) {
                        fprintf(out, "%s", string);
                    }
                    fclose(in);
                }
                in  = fopen(mapfile, "r");
                if(in == 0L) {
                    fprintf( stdout, "%s Unable to open select.map File: %s\n", whoami, mapfile);    
                } else {
                    while(fgets(string, 480, in)!=0L) {
                        fprintf(out, "%s", string);
                    }
                    fclose(in);
                }
                in  = fopen(temp, "r");
                if(in == 0L) {
                    fprintf( stdout, "%s Unable to open indexb.html File: %s\n", whoami, temp);    
                } else {
                    while(fgets(string, 480, in)!=0L) {
                        fprintf(out, "%s", string);
                    }
                    fclose(in);
                }
                fclose(out);
            
            }
            
        }
        /* Build the list of channels requiring heli-plots.
        ***************************************************/
        /* Reads the array of data and makes list of problem channels. */

        sprintf(scnfile, "%s%s", But.GifDir, "heli_chans");
        hlist = fopen(scnfile, "wb");
        if(hlist == 0L) {
            fprintf( stdout, "%s Unable to write File: %s\n", whoami, scnfile); 
        } else {
            /* Loop Through Data
             *******************/
            for(i=0;i<But.NSCN;i++) {
                if(But.Chan[i].priority > 0 && But.Chan[i].errtype[0] == 0) {
                    fprintf(hlist, "SCN %s %s %s\n", But.Chan[i].Site, But.Chan[i].Comp, But.Chan[i].Net);
                }
            }
            fclose(hlist);
        }
        
    }
    
}


/**************************************************************************
 *  add_data_to_base_map - plots data on station_map .gif basemap         *
 **************************************************************************/
void add_data_to_base_map (Global *But, MapInfo *Map, char *BaseMapFile, char *OutputMapFile)
{
    FILE    *out;

    initial_tgd_stuff(BaseMapFile);

/* Send data location info to command file.
 ******************************************/
    plot_data_on_index_map(But, Map);

    /* Make the GIF file. *
     **********************/        
    out = fopen(OutputMapFile, "wb");
    if(out == 0L) {
        fprintf( stdout, "%s: %s:  Unable to write GIF File: %s\n", 
                module, "add_data_to_base_map", OutputMapFile); 
        exit(-1);
    } else {
        gdImageGif(im_out, out);
        fclose(out);
    }
    gdImageDestroy(im_out);
}


/**************************************************************************
 *  Initialize the Gif Image stuff.                                       *
 **************************************************************************/
void initial_tgd_stuff (char *GifFile)
{
    FILE    *in;

    in = fopen(GifFile, "rb");
    if(in) {
        im_out = gdImageCreateFromGif(in);
        fclose(in);
        if(im_out) {
            Pallette(im_out, gcolor);
        }
    }
}

/**************************************************************************
 *  Reads the array of data and plots them in colors.                     *
 **************************************************************************/
void plot_data_on_index_map(Global *But, MapInfo *Map) 
{
    int     i, xpix, ypix, xname, yname, xlft, xrgt, ytop, ybot, hour, pixsize;
    long    black, clr;

/*# Loop Through Data
  # ----------------- */
    for(i=0;i<But->NSCN;i++) {
        if(But->Chan[i].Lat > Map->botindexdeg && But->Chan[i].Lat < Map->topindexdeg && 
           But->Chan[i].Lon > Map->lftindexdeg && But->Chan[i].Lon < Map->rgtindexdeg) {
/*        # Find pixel location of site on index map. */
            xpix = (int)(Map->lftpix_ind + Map->xpixperdeg*(But->Chan[i].Lon - Map->lftindexdeg) + 0.5);
            ypix = (int)(Map->toppix_ind + Map->ypixperdeg*(Map->topindexdeg - But->Chan[i].Lat) + 0.5);

/*        # Assign color based on comp value. */
            get_data_clr(But->Chan[i].priority, &clr);

/*        # Scale and locate square plot symbol. */
            pixsize = Map->halfhtpix;
            if(But->BigMap && But->Chan[i].priority > 0) pixsize = pixsize*2;
            get_data_symbol(pixsize, xpix, ypix, &xlft, &xrgt, &ytop, &ybot);

            black = gcolor[BLACK];
            gdImageFilledRectangle(im_out, xlft, ytop, xrgt, ybot,  clr);
            gdImageRectangle(im_out, xlft, ytop, xrgt, ybot,  black);

/*        # Locate label.    */
            if(But->BigMap == 0) {
                if(But->Chan[i].priority > 0) {
                    hour = 2;
                    get_label_loc(hour, xpix, ypix, &xname, &yname);

                    gdImageString(im_out, gdFontMediumBold, xname, yname, But->Chan[i].Site, black);
                }
            }
        }
    }
    if(But->BigMap) {
        black = gcolor[BLACK];
        xpix = (int)(Map->lftpix_ind + Map->xpixperdeg*(-124.5 - Map->lftindexdeg) + 0.5);
        ypix = (int)(Map->toppix_ind + Map->ypixperdeg*(Map->topindexdeg - 35.0) + 0.5);
        pixsize = Map->halfhtpix*2;
        get_data_symbol(pixsize, xpix, ypix, &xlft, &xrgt, &ytop, &ybot);
    	get_data_clr(10, &clr);
        gdImageFilledRectangle(im_out, xlft, ytop, xrgt, ybot,  clr);
        gdImageRectangle(im_out, xlft, ytop, xrgt, ybot,  black);
        gdImageString(im_out, gdFontSmall, xpix+pixsize*2, ypix-pixsize, "No data to waveservers", black);
        
        ypix += pixsize*2*2;
        get_data_symbol(pixsize, xpix, ypix, &xlft, &xrgt, &ytop, &ybot);
    	get_data_clr(9, &clr);
        gdImageFilledRectangle(im_out, xlft, ytop, xrgt, ybot,  clr);
        gdImageRectangle(im_out, xlft, ytop, xrgt, ybot,  black);
        gdImageString(im_out, gdFontSmall, xpix+pixsize*2, ypix-pixsize, "Flat line", black);
        
        ypix += pixsize*2*2;
        get_data_symbol(pixsize, xpix, ypix, &xlft, &xrgt, &ytop, &ybot);
    	get_data_clr(7, &clr);
        gdImageFilledRectangle(im_out, xlft, ytop, xrgt, ybot,  clr);
        gdImageRectangle(im_out, xlft, ytop, xrgt, ybot,  black);
        gdImageString(im_out, gdFontSmall, xpix+pixsize*2, ypix-pixsize, "Probable Flat line", black);
        
        ypix += pixsize*2*2;
        get_data_symbol(pixsize, xpix, ypix, &xlft, &xrgt, &ytop, &ybot);
    	get_data_clr(8, &clr);
        gdImageFilledRectangle(im_out, xlft, ytop, xrgt, ybot,  clr);
        gdImageRectangle(im_out, xlft, ytop, xrgt, ybot,  black);
        gdImageString(im_out, gdFontSmall, xpix+pixsize*2, ypix-pixsize, "Possible data problem", black);
    }
}

/**************************************************************************
 *  Get the color for the site symbol based on comp code.                 *
 **************************************************************************/
void get_data_clr(int priority, long *clr) 
{
       if     (priority==0) {*clr = gcolor[WHITE];}
       else if(priority==1) {*clr = gcolor[YELLOW];}
       else if(priority==2) {*clr = gcolor[GREEN];} 
       else if(priority==5)  {*clr = gcolor[GREY];} 
  /*     else if(priority==6)  {*clr = gcolor[BLACK];} */
       else if(priority==6)  {*clr = gcolor[TURQ];} 
       else if(priority==7)  {*clr = gcolor[BLUE];} 
       else if(priority==8)  {*clr = gcolor[TURQ];}
       else if(priority==9)  {*clr = gcolor[PURPLE];}
       else if(priority==10) {*clr = gcolor[RED];}
       else                 {*clr = gcolor[WHITE];}
}


/**************************************************************************
 *  Get the box bounds for the site symbol based on global halfhtpix.     *
 **************************************************************************/
void get_data_symbol(int halfhtpix, int xpix, int ypix, int *xlft, int *xrgt, int *ytop, int *ybot) 
{
       *xlft = xpix - halfhtpix;
       *xrgt = xpix + halfhtpix;
       *ytop = ypix - halfhtpix;
       *ybot = ypix + halfhtpix;
}

/**************************************************************************
 *  Get the proper location for the site label.                           *
 **************************************************************************/
void get_label_loc(int hour, int xpix, int ypix, int *xname, int *yname) 
{
    int  xl, yl, xoff, yoff;
    
/*    # Locate center of label from center of symbol in pixels.  */

    if(hour <=  0) {xl =   0; yl = -10;}
    if(hour ==  1) {xl =   9; yl =  -8;}
    if(hour ==  2) {xl =  18; yl =  -5;}
    if(hour ==  3) {xl =  18; yl =   0;}
    if(hour ==  4) {xl =  18; yl =   5;}
    if(hour ==  5) {xl =   9; yl =   8;}
    if(hour ==  6) {xl =   0; yl =  10;}
    if(hour ==  7) {xl =  -9; yl =   8;}
    if(hour ==  8) {xl = -18; yl =   5;}
    if(hour ==  9) {xl = -18; yl =   0;}
    if(hour == 10) {xl = -18; yl =  -5;}
    if(hour == 11) {xl =  -9; yl =  -8;}
    if(hour >= 12) {xl =   0; yl = -10;}

    xoff = -13;
    yoff =  -5;
    *xname = xpix + xl + xoff;
    *yname = ypix + yl + yoff;

}


/********************************************************************
 *    update_html_on_index_map constructs the html (select.map)     *
 *    for mapping the points on the map to other links.             *
 *                                                                  *
 ********************************************************************/

void update_html_on_index_map(Global *But, MapInfo *Map, char *outfile)
 {
    char    whoami[50], linkfile[50], mapfile[150], htmlfile[150], giffile[150], winstatus[300];
    int     i, j, k, pixwidth_ind0, pixheight_ind0, xpix, ypix;
    double  xpixperdeg, ypixperdeg;
    FILE    *in;
    FILE    *out, *html;
    gdImagePtr    im_in;

    sprintf(whoami, " %s: %s: ", module, "update_html_on_index_map");

/*   # Open the index template map to get its dimensions.  */
    sprintf(mapfile,  "%s%s", But->BaseMapDir, Map->BaseMap);
fprintf( stdout, "%s Open File: %s\n", whoami, mapfile); 
    in = fopen(mapfile, "rb");
    if(in) {
        im_in = gdImageCreateFromGif(in);
        fclose(in);
        pixwidth_ind0 = im_in->sx;
        pixheight_ind0 = im_in->sy;
        gdImageDestroy(im_in);
    }

/*    # Open server side imagemap .map file for data points.   */
fprintf( stdout, "%s Open File: %s\n", whoami, outfile); 
    out = fopen(outfile, "wb");
    if(out == 0L) {
        fprintf( stdout, "%s Unable to write File: %s\n", whoami, outfile); 
        exit(-1);
    }
    
    sprintf(mapfile,  "%s%s", But->MapDirOnWeb, Map->BaseMap);
    fprintf(out, " <center> \n");
    fprintf(out, "     <img src=%s ", mapfile);
    fprintf(out, " width=%d height=%d  ", pixwidth_ind0, pixheight_ind0);
    fprintf(out, " border=\"2\" usemap=\"#Click Station Map\"><BR> \n");
    fprintf(out, " </center> \n");
    fprintf(out, "  <p><map name=\"Click Station Map\">  \n");
    
/*# Reads the array of data and plots them in colors. */

/*# Loop Through Data backwards
  # ----------------- */
    for(i=But->NSCN-1;i>=0;i--) {
/*        # Find pixel location of site on index map. */
        if(But->Chan[i].Lat > Map->botindexdeg && But->Chan[i].Lat < Map->topindexdeg && 
           But->Chan[i].Lon > Map->lftindexdeg && But->Chan[i].Lon < Map->rgtindexdeg) {
            xpix = (int)(Map->lftpix_ind + Map->xpixperdeg*(But->Chan[i].Lon - Map->lftindexdeg) + 0.5);
            ypix = (int)(Map->toppix_ind + Map->ypixperdeg*(Map->topindexdeg - But->Chan[i].Lat) + 0.5);
/*
fprintf( stdout, "%s %s %s %s %f %f %d  ", 
But->Chan[i].Site, But->Chan[i].Comp, But->Chan[i].Net, But->Chan[i].Loc, 
But->Chan[i].Lat,  But->Chan[i].Lon,  But->Chan[i].priority);
for(k=0;k<10;k++) fprintf( stdout, " %d ",  But->Chan[i].errtype[k]);
fprintf( stdout, "\n"); 
*/
            sprintf(linkfile, "yy.%s_%s.html", But->Chan[i].Site, But->Chan[i].Net);
            /*
            if(But->Chan[i].priority > 0 && But->Chan[i].errtype[0] == 0) {
                sprintf(linkfile, "xx.%s_%s_%s_%s.html", But->Chan[i].Site, But->Chan[i].Comp, But->Chan[i].Net, But->Chan[i].Loc);
            }*/
            sprintf(winstatus, "Station %s", But->Chan[i].Site);
            if(But->Chan[i].errtype[1] > 0 || But->Chan[i].errtype[2] > 0) {
                sprintf(winstatus, "Station %s - Bad times in the waveservers", But->Chan[i].Site);
            }
            if(But->Chan[i].errtype[0] > 0) {
                sprintf(winstatus, "Station %s - No data to the waveservers", But->Chan[i].Site);
                sprintf(linkfile, "#");
            }
            if(But->Chan[i].errtype[4] > 0 || But->Chan[i].errtype[5] > 0) {
                sprintf(winstatus, "Station %s - Might be No data (Flat line)", But->Chan[i].Site);
            }
            if(But->Chan[i].errtype[6] > 0) {
                sprintf(winstatus, "Station %s - Possible problem", But->Chan[i].Site);
            }
            if(But->Chan[i].errtype[3] > 0) {
                sprintf(winstatus, "Station %s - No data (Flat line)", But->Chan[i].Site);
            }
            fprintf(out, " <area shape=\"rect\" coords=\"%d,%d,%d,%d\" href=\"%s\" \n",
                    xpix-Map->halfhtpix, ypix-Map->halfhtpix, xpix+Map->halfhtpix, ypix+Map->halfhtpix, linkfile);
            fprintf(out, " onMouseOver=\"window.status='%s'; return true;\" \n", winstatus);
            fprintf(out, " onMouseOut=\"window.status=''; return true;\"  ");
            fprintf(out, "  title=\"%s\"> \n \n", winstatus);
        }
    }
    fprintf(out, " </map> \n");
    
    fprintf(out, " <hr> \n");
    
/*# Loop Through Data
  # ----------------- */
    for(i=0;i<But->NSCN;i++) {
/*        # Find pixel location of site on index map. */
        if(But->Chan[i].Lat > Map->botindexdeg && But->Chan[i].Lat < Map->topindexdeg && 
           But->Chan[i].Lon > Map->lftindexdeg && But->Chan[i].Lon < Map->rgtindexdeg) {

            sprintf(winstatus, "Channel %s_%s_%s_%s", But->Chan[i].Site, But->Chan[i].Comp, But->Chan[i].Net, But->Chan[i].Loc);
/*
fprintf( stdout, "%s %f %f %d  ", winstatus, But->Chan[i].Lat,  But->Chan[i].Lon,  But->Chan[i].priority);
for(k=0;k<10;k++) fprintf( stdout, " %d ",  But->Chan[i].errtype[k]);
fprintf( stdout, "\n"); 
*/
            if(But->Chan[i].errtype[1] > 0 || But->Chan[i].errtype[2] > 0) {
                sprintf(winstatus, "Channel %s_%s_%s_%s - Bad times in the waveservers", But->Chan[i].Site, But->Chan[i].Comp, But->Chan[i].Net, But->Chan[i].Loc);
            }
            if(But->Chan[i].errtype[0] > 0) {
                sprintf(winstatus, "Channel %s_%s_%s_%s - No data to the waveservers", But->Chan[i].Site, But->Chan[i].Comp, But->Chan[i].Net, But->Chan[i].Loc);
            }
            if(But->Chan[i].errtype[4] > 0 || But->Chan[i].errtype[5] > 0) {
                sprintf(winstatus, "Channel %s_%s_%s_%s - Might be No data (Flat line)", But->Chan[i].Site, But->Chan[i].Comp, But->Chan[i].Net, But->Chan[i].Loc);
            }
            if(But->Chan[i].errtype[6] > 0) {
                sprintf(winstatus, "Channel %s_%s_%s_%s - Possible problem", But->Chan[i].Site, But->Chan[i].Comp, But->Chan[i].Net, But->Chan[i].Loc);
            }
            if(But->Chan[i].errtype[3] > 0) {
                sprintf(winstatus, "Channel %s_%s_%s_%s - No data (Flat line)", But->Chan[i].Site, But->Chan[i].Comp, But->Chan[i].Net, But->Chan[i].Loc);
            }
            sprintf(htmlfile, "yy.%s_%s.html", But->Chan[i].Site, But->Chan[i].Net);
            sprintf(giffile, "%syy.%s_%s.gif", But->MapDirOnWeb, But->Chan[i].Site, But->Chan[i].Net);
            if(But->Chan[i].priority > 0) {
	            if(But->Chan[i].errtype[0] == 0) {
	            }
	            else {
	                fprintf(out, "<a href=\"#\"> %s </a><br>\n", winstatus);
	            }
            }
            for(j=0;j<But->nltargets;j++) {
                sprintf(linkfile, "%ssave/%s", But->loctarget[j], htmlfile);
                html = fopen(linkfile, "rb");
                if(html == 0L) {
 	                sprintf(linkfile, "%s%s", But->loctarget[j], htmlfile);
	                html = fopen(linkfile, "wb");
	                if(html == 0L) {
	                    fprintf( stdout, "%s Unable to write File: %s\n", whoami, linkfile); 
	                } else {
	                    fprintf(html, "<html>\n <head><title>\n%s\n</title></head>\n", linkfile);
	                    fprintf(html, "<body BGCOLOR=#ffffff TEXT=#000000 vlink=purple link=blue >\n");
	                    fprintf(html, "<p>\n <center><td>\n");
	                    fprintf(html, "<img SRC=\"%s\" \n </center><td>\n", giffile);
		                fprintf(html, "<a href=\"%scalmap.html\"> Return to Main Map </a><br>\n", But->MapDirOnWeb);
	                    fprintf(html, "<p>\n </body></html>\n");
	                    fclose(html);
	                }
                } else {
                    fclose(html);
                }
            }
       }
    }
    fprintf(out, " <hr> \n");
     
    fclose(out);
}

/*******************************************************************************
 *    Pallette defines the pallete to be used for plotting.                    *
 *     PALCOLORS colors are defined.                                           *
 *                                                                             *
 *******************************************************************************/

void Pallette(gdImagePtr GIF, long color[])
{
    color[WHITE]  = gdImageColorAllocate(GIF, 255, 255, 255);
    color[BLACK]  = gdImageColorAllocate(GIF, 0,     0,   0);
    color[RED]    = gdImageColorAllocate(GIF, 255,   0,   0);
    color[BLUE]   = gdImageColorAllocate(GIF, 0,     0, 255);
    color[GREEN]  = gdImageColorAllocate(GIF, 0,   105,   0);
    color[GREY]   = gdImageColorAllocate(GIF, 125, 125, 125);
    color[YELLOW] = gdImageColorAllocate(GIF, 125, 125,   0);
    color[TURQ]   = gdImageColorAllocate(GIF, 0,   255, 255);
    color[PURPLE] = gdImageColorAllocate(GIF, 200,   0, 200);    
    
    gdImageColorTransparent(GIF, -1);
}

/****************************************************************************
 *  Get_Sta_Info( );                                                        *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Sta_Info(Global *But)
{
    char    whoami[50], *com, *str, ns, ew;
    int     i, j, k, nfiles;

    sprintf(whoami, "%s: %s: ", module, "Get_Sta_Info");
    
    ns = 'N';
    ew = 'W';
    But->NSCN = 0;
    
        /* Open the main station file
         ****************************/
    nfiles = k_open( But->StationList );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, But->StationList );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

            /* Process anything else as a channel descriptor
             ***********************************************/

            if( But->NSCN >= MAXCHANNELS ) {
                fprintf(stderr, "%s Too many channel entries in <%s>", whoami, But->StationList );
                fprintf(stderr, "; max=%d; exiting!\n", (int) MAXCHANNELS );
                exit( -1 );
            }
            j = But->NSCN;
            
                /* S C N */
            strncpy( But->Chan[j].Site, com,  6);
            str = k_str();
            if(str) strncpy( But->Chan[j].Comp, str, 3);
            str = k_str();
            if(str) strncpy( But->Chan[j].Net,  str,  2);
            str = k_str();
            if(str) strncpy( But->Chan[j].Loc,  str,  2);
            for(i=0;i<6;i++) if(But->Chan[j].Site[i]==' ') But->Chan[j].Site[i] = 0;
            for(i=0;i<2;i++) if(But->Chan[j].Net[i]==' ')  But->Chan[j].Net[i]  = 0;
            for(i=0;i<3;i++) if(But->Chan[j].Comp[i]==' ') But->Chan[j].Comp[i] = 0;
            But->Chan[j].Comp[3] = But->Chan[j].Net[2] = But->Chan[j].Site[5] = 0;


                /* Lat Lon priority */
            But->Chan[j].Lat = k_val();
            But->Chan[j].Lon = k_val();
          /*   But->Chan[j].Lon = -But->Chan[j].Lon; */
            But->Chan[j].priority = k_int();
            
            for(k=0;k<10;k++) {
                But->Chan[j].errtype[k]   = k_int();
            }
           
            But->NSCN++;
        }
        nfiles = k_close();
    }
}


/****************************************************************************
 *      config_me() process command file using kom.c functions              *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void config_me(Global *But, char* configfile )
{
    char    whoami[50], *com, *str;
    char    init[20];       /* init flags, one byte for each required command */
    int     ncommand;       /* # of required commands you expect              */
    int     nmiss;          /* number of required commands that were missed   */
    int     i, n, nfiles, success;

    sprintf(whoami, " %s: %s: ", module, "config_me");
        /* Set one init flag to zero for each required command
         *****************************************************/
    ncommand = 7;
    for(i=0; i<ncommand; i++ )  init[i] = 0;
    But->nltargets = 0;
    But->Nmaps = 0;
    Debug = 0;

        /* Open the main configuration file
         **********************************/
    nfiles = k_open( configfile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, configfile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

                /* Process anything else as a command
                 ************************************/

        /* get the local target directory(s)
        ************************************/
/*0*/
            if( k_its("LocalTarget") ) {
                if ( But->nltargets >= MAX_TARGETS ) {
                    fprintf( stderr, "%s Too many <LocalTarget> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_TARGETS );
                    return;
                }
                if( (long)(str=k_str()) != 0 )  {
                    n = strlen(str);   /* Make sure directory name has proper ending! */
                    if( str[n-1] != '/' ) strcat(str, "/");
                    strcpy(But->loctarget[But->nltargets], str);
                }
                But->nltargets += 1;
                init[0] = 1;
            }

                /* get station list path/name
                *****************************/
/*1*/
            else if( k_its("StationList") ) {
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->StationList , str );
                init[1] = 1;
            }

        /* get basemap directory path/name
        *****************************/
/*2*/
            else if( k_its("BaseMapDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. BaseMap directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                if(str) strcpy( But->BaseMapDir , str );
                init[2] = 1;
            }

        /* get temp file directory path/name
        *****************************/
/*3*/
            else if( k_its("GifDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. GifDir directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                if(str) strcpy( But->GifDir , str );
                init[3] = 1;
            }

        /* get IndexMap file name and data
        *****************************/
/*4*/
            else if( k_its("IndexMap") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. IndexMap file name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                if(str) strcpy( But->Index.BaseMap , str );
                But->Index.lftindexdeg = k_val();
                But->Index.rgtindexdeg = k_val();
                But->Index.botindexdeg = k_val();
                But->Index.topindexdeg = k_val();
                But->Index.lftpix_ind = k_int();
                But->Index.rgtpix_ind = k_int();
                But->Index.botpix_ind = k_int();
                But->Index.toppix_ind = k_int();
                But->Index.halfhtpix = k_int();
            /*# Define degrees to pixel scale factors: */
                But->Index.xpixperdeg = (But->Index.rgtpix_ind  - But->Index.lftpix_ind)/
                                        (But->Index.rgtindexdeg - But->Index.lftindexdeg);
                But->Index.ypixperdeg = (But->Index.botpix_ind  - But->Index.toppix_ind)/
                                        (But->Index.topindexdeg - But->Index.botindexdeg);
                init[4] = 1;
            }

        /* get 2-deg file names and data
        *****************************/
/*5*/
            else if( k_its("TwoDeg") ) {
                if ( But->Nmaps >= MAXMAPS ) {
                    fprintf( stderr, "%s Too many <TwoDeg> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAXMAPS );
                    return;
                }
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. TwoDeg file name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                if(str) strcpy( But->Map[But->Nmaps].BaseMap , str );
                But->Map[But->Nmaps].lftindexdeg = k_val();
                But->Map[But->Nmaps].rgtindexdeg = k_val();
                But->Map[But->Nmaps].botindexdeg = k_val();
                But->Map[But->Nmaps].topindexdeg = k_val();
                But->Map[But->Nmaps].lftpix_ind = k_int();
                But->Map[But->Nmaps].rgtpix_ind = k_int();
                But->Map[But->Nmaps].botpix_ind = k_int();
                But->Map[But->Nmaps].toppix_ind = k_int();
                But->Map[But->Nmaps].halfhtpix = k_int();
            /*# Define degrees to pixel scale factors: */
                But->Map[But->Nmaps].xpixperdeg = (But->Map[But->Nmaps].rgtpix_ind  - But->Map[But->Nmaps].lftpix_ind)/
                                                  (But->Map[But->Nmaps].rgtindexdeg - But->Map[But->Nmaps].lftindexdeg);
                But->Map[But->Nmaps].ypixperdeg = (But->Map[But->Nmaps].botpix_ind  - But->Map[But->Nmaps].toppix_ind)/
                                                  (But->Map[But->Nmaps].topindexdeg - But->Map[But->Nmaps].botindexdeg);
                But->Nmaps += 1;
                init[5] = 1;
            }

        /* get MapDirOnWeb file path/name
        *****************************/
/*6*/
            else if( k_its("MapDirOnWeb") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. MapDirOnWeb file name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                if(str) strcpy( But->MapDirOnWeb , str );
                init[6] = 1;
            }


            else if( k_its("Debug") )          Debug = 1;   /* optional commands */


                /* At this point we give up. Unknown thing.
                *******************************************/
            else {
                fprintf(stderr, "%s <%s> Unknown command in <%s>.\n",
                         whoami, com, configfile );
                continue;
            }

                /* See if there were any errors processing the command
                 *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "%s Bad <%s> command  in <%s>; exiting!\n",
                             whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

        /* After all files are closed, check init flags for missed commands
         ******************************************************************/
    nmiss = 0;
    for(i=0;i<ncommand;i++)  if( !init[i] ) nmiss++;
    if ( nmiss ) {
        fprintf( stderr, "%s ERROR, no ", whoami);
        if ( !init[0] )  fprintf( stderr, "<LocalTarget> "    );
        if ( !init[1] )  fprintf( stderr, "<StationList> "   );
        if ( !init[2] )  fprintf( stderr, "<GifDir> "     );
        if ( !init[3] )  fprintf( stderr, "<BaseMapDir> "     );
        if ( !init[4] )  fprintf( stderr, "<IndexMap> "     );
        if ( !init[5] )  fprintf( stderr, "<TwoDeg> "     );
        if ( !init[6] )  fprintf( stderr, "<MapDirOnWeb> "   );
        fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
        exit( -1 );
    }
 }


