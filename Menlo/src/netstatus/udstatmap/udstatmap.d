#
# This is the udstatmap parameter file. This module gets site location
# information, and creates index maps and html.
 

# Directory in which to store the temp files.
  GifDir   /home/picker/gifs/nssetup/

# Directory in which to store the files for output
  LocalTarget   /home/picker/sendfiler/netstat/  

# The file with all the station information.
 StationList     /home/picker/gifs/nssetup/Station_List

# Directory in which to store the .gif files for output
  BaseMapDir   /home/picker/gifs/nssetup/basemaps

# index base map                   degrees                 pixel bounds
#                           left   right   bot  top  left  right  bot  top  sym
  IndexMap	 netstatca.gif  -125.0  -116.5   33   43   6    546    643   6   2

# 2-deg maps                       degrees                 pixel bounds
#                           left   right   bot  top  left  right  bot  top  sym
  TwoDeg	 42-124.gif     -125   -123    41   43   55    593    551  13   3
  TwoDeg	 42-123.gif     -124   -122    41   43   55    593    551  13   3
  TwoDeg	 42-122.gif     -123   -121    41   43   55    593    551  13   3

  TwoDeg	 41-124.gif     -125   -123    40   42   55    593    551  13   3
  TwoDeg	 41-123.gif     -124   -122    40   42   55    593    551  13   3
  TwoDeg	 41-122.gif     -123   -121    40   42   55    593    551  13   3

  TwoDeg	 40-124.gif     -125   -123    39   41   55    593    551  13   3
  TwoDeg	 40-123.gif     -124   -122    39   41   55    593    551  13   3
  TwoDeg	 40-122.gif     -123   -121    39   41   55    593    551  13   3
  TwoDeg	 40-121.gif     -122   -120    39   41   55    593    551  13   3
  TwoDeg	 40-120.gif     -121   -119    39   41   55    593    551  13   3

  TwoDeg	 39-123.gif     -124   -122    38   40   55    593    551  13   3
  TwoDeg	 39-122.gif     -123   -121    38   40   55    593    551  13   3
  TwoDeg	 39-121.gif     -122   -120    38   40   55    593    551  13   3
  TwoDeg	 39-120.gif     -121   -119    38   40   55    593    551  13   3

  TwoDeg	 38-123.gif     -124   -122    37   39   55    593    551  13   3
  TwoDeg	 38-122.gif     -123   -121    37   39   55    593    551  13   3
  TwoDeg	 38-121.gif     -122   -120    37   39   55    593    551  13   3
  TwoDeg	 38-120.gif     -121   -119    37   39   55    593    551  13   3
  TwoDeg	 38-119.gif     -120   -118    37   39   55    593    551  13   3
  TwoDeg	 38-118.gif     -119   -117    37   39   55    593    551  13   3

  TwoDeg	 37-122.gif     -123   -121    36   38   55    593    551  13   3
  TwoDeg	 37-121.gif     -122   -120    36   38   55    593    551  13   3
  TwoDeg	 37-120.gif     -121   -119    36   38   55    593    551  13   3
  TwoDeg	 37-119.gif     -120   -118    36   38   55    593    551  13   3
  TwoDeg	 37-118.gif     -119   -117    36   38   55    593    551  13   3

  TwoDeg	 36-121.gif     -122   -120    35   37   55    593    551  13   3
  TwoDeg	 36-120.gif     -121   -119    35   37   55    593    551  13   3
  TwoDeg	 36-119.gif     -120   -118    35   37   55    593    551  13   3
  TwoDeg	 36-118.gif     -119   -117    35   37   55    593    551  13   3

  TwoDeg	 35-121.gif     -122   -120    34   36   55    593    551  13   3
  TwoDeg	 35-120.gif     -121   -119    34   36   55    593    551  13   3
  TwoDeg	 35-119.gif     -120   -118    34   36   55    593    551  13   3
  TwoDeg	 35-117.gif     -118   -116    34   36   55    593    551  13   3

  TwoDeg	 34-117.gif     -118   -116    33   35   55    593    551  13   3

# map path on webserver
  MapDirOnWeb	/waveforms/net_status/

# We accept a command "Debug" which turns on a bunch of log messages
  Debug
