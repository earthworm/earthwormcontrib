CFLAGS = -D_REENTRANT $(GLOBALFLAGS)

B = ${EW_HOME}/${EW_VERSION}/bin
L = ${EW_HOME}/${EW_VERSION}/lib

BINARIES = udstatmap.o  $L/kom.o \
		$L/gd.o $L/gdfontt.o $L/gdfonts.o $L/gdfontmb.o \
		$L/gdfontl.o $L/gdfontg.o 

udstatmap: $(BINARIES)
	cc -o $(B)/udstatmap $(BINARIES) -lsocket -lnsl -lm -mt -lposix4 -lthread -lc

.c.o:
	$(CC) $(CFLAGS) -g $(CPPFLAGS) -c  $(OUTPUT_OPTION) $<
