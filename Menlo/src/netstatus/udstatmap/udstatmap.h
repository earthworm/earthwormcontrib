/* Include file for udstatmap */
/**************************************************************************
 *  defines                                                               *
 **************************************************************************/

#define MAXCHANNELS     1200  /* Maximum number of channels               */
#define MAXCOLORS         20  /* Number of colors defined                 */
#define MAXMAPS           50  /* Maximum number of 2-deg maps                 */
#define MAX_TARGETS       10  /* largest number of targets                */
#define GDIRSZ           132  /* Size of string for GIF target directory  */
#define STALIST_SIZ      100  /* Size of string for station list file     */

#define WHITE  0
#define BLACK  1
#define RED    2
#define BLUE   3
#define GREEN  4
#define GREY   5
#define YELLOW 6
#define TURQ   7
#define PURPLE 8

/*********************************************************************
    Define the structure for Channel information.
 *********************************************************************/
typedef struct ChanInfo {      /* A channel information structure         */
    char    Site[6];           /* Site                                    */
    char    Comp[5];           /* Component                               */
    char    Net[5];            /* Net                                     */
    char    Loc[5];            /* Loc                                     */
    double  Lat;               /* Latitude                                */
    double  Lon;               /* Longitude                               */
    
    int     priority;          /* 0->OK 1->suspect 2->bad                 */
    int     errtype[10];       /*                                         */
} ChanInfo;

/*********************************************************************
    Define the structure for Map information.
 *********************************************************************/
typedef struct MapInfo {         /* A channel information structure       */
    char    BaseMap[GDIRSZ];     /* input ${Maps0}/${IndexBase}.gif       */
    char    OutputMap[GDIRSZ];   /* output ${Maps0}/${IndexBase}.gif      */
	int     lftpix_ind, toppix_ind, rgtpix_ind, botpix_ind;
	double  lftindexdeg, topindexdeg, rgtindexdeg, botindexdeg;
	int     halfhtpix;           /* Half the symbol size                  */
    double  xpixperdeg, ypixperdeg;
} MapInfo;


/**************************************************************************
 *  Define the structure for the Global data.                             *
 **************************************************************************/

struct Global {
    int      Debug;
    
    char     StationList[GDIRSZ];    /* File with SCN info */
    int		 NSCN;             /* Number of SCNs we know about               */
    ChanInfo Chan[MAXCHANNELS];

    MapInfo	 Index;
    int	     Nmaps;             /* Number of 2-deg sheets we want          */
    MapInfo  Map[MAXMAPS];

    int      nltargets;         /* Number of local target directories        */
    char     loctarget[MAX_TARGETS][GDIRSZ];/* Target in form-> /directory/     */
    
    int      BigMap;
    char     GifDir[GDIRSZ];    /* Directory for storage of temp files on local machine */
    char     BaseMapDir[GDIRSZ];    /* Directory for storage of Base Maps on local machine */
    char     MapDirOnWeb[GDIRSZ];    /* Directory for storage of Base Maps on webserver */
};
typedef struct Global Global;

