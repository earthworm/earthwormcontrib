
  /*****************************************************************
   *                           gaplist2.c                          *
   *                                                               *
   *  Program to search for gaps in tracebuf messages and print    *
   *  tables.                                                      *
   *****************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <transport.h>
#include <time_ew.h>
#include <earthworm.h>
#include <trace_buf.h>
#include <chron3.h>
#include <kom.h>
#include <swap.h>
#include <trheadconv.h>
#include "gaplist2.h"

/* Windows provides a non-ANSI version of snprintf,
   name _snprintf.  The Windows version has a
   different return value than ANSI snprintf.
   ***********************************************/
#ifdef _WINNT
#define snprintf _snprintf
#endif

/* Function prototypes
   *******************/
void GetConfig( char *, GPARM * );
void LogConfig( GPARM * );
void GetEwd( void );

/* Global variables
   ****************/
extern int           nScnl;           /* # of scnl's to monitor */
extern LSCNL         *lScnl;          /* Array of scnl parameters and labels */
static char          *TraceBuf;       /* The tracebuf buffer */
static TRACE_HEADER  *TraceHead;      /* The tracebuf header */
static TRACE2_HEADER *Trace2Head;     /* The tracebuf2 header */
static GPARM         Gparm;           /* Configuration file parameters */
pid_t                myPid;           /* for restarts by startstop */
static SHM_INFO      region;          /* Shared memory region */
static MSG_LOGO      errlogo;         /* Logo of outgoing errors */

/* Values set by GetEwd(), in config.c
   ***********************************/
unsigned char        inst_wildcard;   /* From earthworm.d */
unsigned char        inst_local;      /* From earthworm.d */
unsigned char        type_tracebuf;   /* From earthworm.d */
unsigned char        type_tracebuf2;  /* From earthworm.d */
unsigned char        type_tracecomp;  /* From earthworm.d */
unsigned char        type_trace2comp; /* From earthworm.d */
unsigned char        type_heartbeat;  /* From earthworm.d */
unsigned char        type_error;      /* From earthworm.d */
unsigned char        mod_wildcard;    /* From earthworm.d */
unsigned char        mod_gaplist2;    /* From earthworm.d */


/* Function prototypes
   *******************/
void AddNode( int i, int nsamp, double starttime, double samprate );
void RemoveOldNodes( int i, double MaxAge );
int  GetLatency( int i, double *aveLatency, double *maxLatency,
                 double *pdataInList );


         /*************************************************
          *        The main program starts here.          *
          *************************************************/

int main( int argc, char *argv[] )
{
   MSG_LOGO logo;
   MSG_LOGO hrtlogo;           /* Logo of outgoing heartbeats */
   long     gotsize;
   int      res;
   time_t   prevPrintTime;
   time_t   prevHeartTime;
   unsigned char seq;

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      printf( "Usage: gaplist2 <configfile>\n" );
      return -1;
   }

/* Open log file
   *************/
   logit_init( argv[1], 0, 256, 1 );

/* Read things from earthworm*d files.
   GetEwd() exits on error.
   **********************************/
   GetEwd();

/* Get parameters from the configuration files.
   GetConfig() exits on error.
   *******************************************/
   GetConfig( argv[1], &Gparm );

/* Get the module id for this process
   **********************************/
   if ( GetModId( Gparm.MyModName, &mod_gaplist2 ) != 0 )
   {
      logit("", "gaplist2: Error getting %s. Exiting.\n", Gparm.MyModName );
      return -1;
   }

/* Specify logos of incoming waveforms, outgoing heartbeats, errors
   ****************************************************************/
   if ( Gparm.nGetLogo == 0 )
   {
      Gparm.nGetLogo = 2;
      Gparm.GetLogo  = (MSG_LOGO *) calloc( Gparm.nGetLogo, sizeof(MSG_LOGO) );

      if ( Gparm.GetLogo == NULL )
      {
         logit( "e", "gaplist2: Error allocating space for GetLogo. Exiting\n" );
         return -1;
      }
      Gparm.GetLogo[0].instid = inst_wildcard;
      Gparm.GetLogo[0].mod    = mod_wildcard;
      Gparm.GetLogo[0].type   = type_tracebuf2;

      Gparm.GetLogo[1].instid = inst_wildcard;
      Gparm.GetLogo[1].mod    = mod_wildcard;
      Gparm.GetLogo[1].type   = type_tracebuf;
   }

   hrtlogo.instid = inst_local;
   hrtlogo.type   = type_heartbeat;
   hrtlogo.mod    = mod_gaplist2;

   errlogo.instid = inst_local;
   errlogo.type   = type_error;
   errlogo.mod    = mod_gaplist2;

/* Log the configuration parameters
   ********************************/
   LogConfig( &Gparm );

/* Get process ID for heartbeat messages
   *************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
     logit( "e","gaplist2: Cannot get pid. Exiting.\n" );
     free( Gparm.GetLogo );
     return -1;
   }

/* Allocate the trace buffer
   *************************/
   TraceBuf = (char *) malloc( MAX_TRACEBUF_SIZ );
   if ( TraceBuf == NULL )
   {
      logit( "t", "Error allocating trace buffer.  Exiting.\n" );
      free( Gparm.GetLogo );
      return -1;
   }
   TraceHead  = (TRACE_HEADER *)TraceBuf;
   Trace2Head = (TRACE2_HEADER *)TraceBuf;

/* Attach to transport ring
   ************************/
   tport_attach( &region, Gparm.InKey );

/* Flush transport ring on startup
   *******************************/
   while ( tport_copyfrom( &region, Gparm.GetLogo, Gparm.nGetLogo,
                           &logo, &gotsize,
                           TraceBuf, MAX_TRACEBUF_SIZ, &seq ) != GET_NONE );

/* Get the time we start reading messages
   **************************************/
   time( &prevPrintTime );
   time( &prevHeartTime );

/* See if termination flag has been set
   ************************************/
   while ( 1 )
   {
      time_t now;          /* Current time - integer */

      sleep_ew( 100 );

      if ( tport_getflag( &region ) == TERMINATE ||
           tport_getflag( &region ) == myPid )
         break;

/* Send a heartbeat to the transport ring
   **************************************/
      time( &now );
      if ( Gparm.HeartbeatInt > 0 )
      {
         if ( (now - prevHeartTime) >= Gparm.HeartbeatInt )
         {
            int  lineLen;
            char line[40];

            prevHeartTime = now;

            snprintf( line, sizeof(line), "%ld %ld\n", now, myPid );
            line[sizeof(line)-1] = 0;
            lineLen = strlen( line );

            if ( tport_putmsg( &region, &hrtlogo, lineLen, line ) !=
                 PUT_OK )
            {
               logit( "et", "gaplist2: Error sending heartbeat. Exiting." );
               free( Gparm.GetLogo );
               return -1;
            }
         }
      }

/* Log the gap table
   *****************/
      if ( (now - prevPrintTime) >= Gparm.TablePrintInt )
      {
         int i;                        /* Index value for logging nodes */
         prevPrintTime = now;

         logit( "e", "\n" );
         logit( "e", "                  Ave     Max            Data   Percent\n" );
         logit( "e", "     SCNL       Latency Latency  nNodes  (sec)   Data\n" );
         logit( "e", "     ----       ------- -------  ------  -----  -------\n" );

         for ( i = 0; i < nScnl; i++ )
         {
            int nNodes;
            double MaxAge = 3600.0;
            double aveLatency;
            double maxLatency;
            double dataInList;
            double percentData;

            if ( lScnl[i].isLabel )
            {
               logit( "e", "%s\n", lScnl[i].label );
               continue;
            }

/* Clean out old nodes from tails of linked lists
   **********************************************/
            RemoveOldNodes( i, MaxAge );

/* Get latency and percent data for this scnl
   ******************************************/
            nNodes = GetLatency( i, &aveLatency, &maxLatency, &dataInList );
            percentData = 100.0 * dataInList / MaxAge;

            logit( "e", "%5s",      lScnl[i].scnl.sta );
            logit( "e", " %s",      lScnl[i].scnl.chan );
            logit( "e", " %s",      lScnl[i].scnl.net );
            logit( "e", " %s",      lScnl[i].scnl.loc );
            logit( "e", " %6.1lf",  aveLatency );
            logit( "e", "  %6.1lf", maxLatency );
            logit( "e", "   %4d",   nNodes );
            logit( "e", "  %7.2lf", dataInList );
            logit( "e", "  %5.2lf", percentData );
            logit( "e", "\n" );
         }
      }

/* Get all available tracebuf and tracebuf2 messages
   *************************************************/
      while ( 1 )
      {
         int i;            /* lScnl index */

         res = tport_copyfrom( &region, Gparm.GetLogo, Gparm.nGetLogo,
                               &logo, &gotsize,
                               TraceBuf, MAX_TRACEBUF_SIZ, &seq );

         if ( res == GET_NONE )
            break;

         if ( res == GET_TOOBIG )
         {
            logit( "et", "gaplist2: Retrieved message is too big (%d)\n", gotsize );
            break;
         }

         if ( res == GET_NOTRACK )
            logit( "et", "gaplist2: NTRACK_GET exceeded.\n" );

         if ( res == GET_MISS_LAPPED )
            logit( "et", "gaplist2: GET_MISS_LAPPED error.\n" );

         if ( res == GET_MISS_SEQGAP );     /* Do nothing */

/* If necessary, swap bytes in tracebuf message.
   Beware dangling else statement. First set of {} is required.
   ***********************************************************/
         if ( logo.type == type_tracebuf ||
              logo.type == type_tracecomp   )
         {
            if ( WaveMsgMakeLocal( TraceHead ) < 0 )
            {
               logit( "et", "gaplist2: WaveMsgMakeLocal() error.\n" );
               continue;
            }
         }
         else
         {
            if ( WaveMsg2MakeLocal( Trace2Head ) < 0 )
            {
               logit( "et", "gaplist2: WaveMsg2MakeLocal() error.\n" );
               continue;
            }
         }

/* Convert TYPE_TRACEBUF messages to TYPE_TRACEBUF2
   Convert TYPE_TRACE_COMP_UA msgs to TYPE_TRACE2_COMP_UA
   ******************************************************/
         if ( logo.type == type_tracebuf ||
              logo.type == type_tracecomp   )
            Trace2Head = TrHeadConv( TraceHead );

/* Get index (i) of this lScnl
   ***************************/
         for ( i = 0; i < nScnl; i++ )
         {
            if ( strcmp(lScnl[i].scnl.sta,  Trace2Head->sta ) == 0 &&
                 strcmp(lScnl[i].scnl.chan, Trace2Head->chan) == 0 &&
                 strcmp(lScnl[i].scnl.net,  Trace2Head->net ) == 0 &&
                 strcmp(lScnl[i].scnl.loc,  Trace2Head->loc ) == 0 )
               break;
         }
         if ( i == nScnl ) continue;    /* scnl not in list */

/* Add SCNL to head of linked list
   *******************************/
         AddNode( i, Trace2Head->nsamp, Trace2Head->starttime,
                  Trace2Head->samprate );
      }
   }

/* Shut down program
   *****************/
   logit( "t", "Termination flag detected. Program stopping.\n" );
   tport_detach( &region );
   free( Gparm.GetLogo );
   return 0;
}


/************************************************************
 *                        AddNode()                         *
 *                                                          *
 *  Add SCNL to head of linked list.                        *
 ************************************************************/

void AddNode( int i, int nsamp, double starttime, double samprate )
{
   NODE   *newnode = malloc( sizeof(NODE) );
   double tnow;                       /* High resolution time */

   newnode->prev      = lScnl[i].head;
   newnode->next      = NULL;
   newnode->nsamp     = nsamp;
   newnode->starttime = starttime;
   newnode->samprate  = samprate;
   hrtime_ew( &tnow );                /* Get current time */
   newnode->toa       = tnow;

   if ( lScnl[i].head == NULL )
      lScnl[i].tail = newnode;        /* Do not move tail pointer */
   else
      lScnl[i].head->next = newnode;  /* Create link to new node */

   lScnl[i].head = newnode;
   return;
}


/************************************************************
 *                     RemoveOldNodes()                     *
 *                                                          *
 *  Remove old nodes from the linked list.                  *
 ************************************************************/

void RemoveOldNodes( int i, double MaxAge )
{
   time_t now;
   NODE   *current = lScnl[i].tail;
   NODE   *oldNode;

   if ( lScnl[i].head == NULL ) return;
   time( &now );

   while ( current != NULL )
   {
      double endtime = current->starttime + (current->nsamp / current->samprate);
      double age = now - endtime;

      if ( age < MaxAge ) break;
      oldNode = current;
      current = current->next;
      free( oldNode );
      lScnl[i].tail = current;

      if ( current != NULL )
         current->prev = NULL;
      else
         lScnl[i].head = NULL;
   }
   return;
}


/************************************************************
 *                       GetLatency()                       *
 *                                                          *
 *  Computer average latency for all nodes in linked list.  *
 *  Returns number of nodes found                           *
 ************************************************************/

int GetLatency( int i, double *pLatency, double *pmaxLatency,
                double *pdataInList )
{
   NODE   *current   = lScnl[i].tail;
   int    count      = 0;
   double aveLatency = 0.0;
   double maxLatency = 0.0;
   double dataInList = 0.0;

   while ( current != NULL )
   {
      double latency = current->toa - current->starttime -
                      (current->nsamp - 1) / current->samprate;
      double trbLength = current->nsamp / current->samprate;

//    logit( "e", "starttime: %.3lf",  current->starttime );
//    logit( "e", "  latency: %.2lf",  latency );
//    logit( "e", "  nsamp: %d",       current->nsamp );
//    logit( "e", "  samprate: %.3lf", current->samprate );
//    logit( "e", "\n" );
      count++;
      aveLatency += latency;
      if ( maxLatency < latency ) maxLatency = latency;
      dataInList += trbLength;
      current = current->next;
   }
   aveLatency = (count > 0) ? aveLatency/count : 0;
   *pLatency    = aveLatency;
   *pmaxLatency = maxLatency;
   *pdataInList = dataInList;
   return count;
}
