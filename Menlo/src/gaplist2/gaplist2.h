
/******************************************************************
 *                        File gaplist2.h                         *
 ******************************************************************/

#include <time.h>
#include <earthworm.h>
#include <trace_buf.h>

#define LABEL_LEN 32              /* Must be a multiple of 4 */

/* Structure definitions
   *********************/
typedef struct
{
   char   sta[TRACE2_STA_LEN];    /* Station name */
   char   chan[TRACE2_CHAN_LEN];  /* Component */
   char   net[TRACE2_NET_LEN];    /* Network */
   char   loc[TRACE2_LOC_LEN];    /* Location code */
} SCNL;

typedef struct
{
   char   InRing[MAX_RING_STR];   /* Name of ring containing tracebuf messages */
   char   MyModName[MAX_MOD_STR]; /* Module name */
   long   InKey;                  /* Key to ring where waveforms live */
   int    HeartbeatInt;           /* Heartbeat interval in seconds */
   int    TablePrintInt;          /* Interval in sec at which to print table on screen */
   int    PtoInterval;            /* Compute percent time on over this interval (sec) */
   short     nGetLogo;            /* Number of logos in GetLogo   */
   MSG_LOGO *GetLogo;             /* Logos of requested waveforms */
} GPARM;

typedef struct node
{
   int    nsamp;                  /* Number of samples in tracebuf message */
   double starttime;              /* Time of beginning of tracebuf message */
   double samprate;               /* Sample rate (nominal) */
   struct node *prev;             /* Pointer to previous node */
   struct node *next;             /* Pointer to next node */
   double toa;                    /* Time of arrival of tracebuf message */
} NODE;

typedef struct
{
   int    isLabel;                /* 1 if label; 0 if scnl */
   char   label[LABEL_LEN];       /* Label line */
   SCNL   scnl;
   NODE   *head;                  /* Pointer to head of linked list */
   NODE   *tail;                  /* Pointer to tail of linked list */
} LSCNL;

