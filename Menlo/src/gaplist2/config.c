
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>
#include <trace_buf.h>
#include "gaplist2.h"

/* Values set by GetEwd(), below
   *****************************/
extern unsigned char inst_wildcard;
extern unsigned char inst_local;
extern unsigned char type_tracebuf;
extern unsigned char type_tracebuf2;
extern unsigned char type_tracecomp;
extern unsigned char type_trace2comp;
extern unsigned char type_heartbeat;
extern unsigned char type_error;
extern unsigned char mod_wildcard;
extern unsigned char mod_gaplist2;

#define ncommand 5          /* Number of commands in the config file */

int   nScnl = 0;            /* Number of scnl's to monitor, including label lines */
LSCNL *lScnl = NULL;        /* Array of scnl structures */


   /*****************************************************************
    *                          GetConfig()                          *
    *         Processes command file using kom.c functions.         *
    *****************************************************************/

void GetConfig( char *config_file, GPARM *Gparm )
{
   char init[ncommand];     /* Flags, one for each command */
   int  nmiss;              /* Number of commands that were missed */
   int  nfiles;
   int  i;
   int  scnlSize = 0;       /* Num bytes in scnl array */

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ ) init[i] = 0;

/* Initialize Configuration parameters
   ***********************************/
   Gparm->nGetLogo = 0;
   Gparm->GetLogo  = NULL;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( config_file );
   if ( nfiles == 0 )
   {
      logit( "e", "gaplist2: Error opening configuration file <%s> Exiting.\n",
              config_file );
      exit( -1 );
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          /* Get the first token from line */

         if ( !com ) continue;             /* Ignore blank lines */
         if ( com[0] == '#' ) continue;    /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               logit( "e", "gaplist2: Error opening command file <%s>. Exiting.\n",
                       &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "InRing" ) )
         {
            if ( str = k_str() )
            {
               strncpy( Gparm->InRing, str, MAX_RING_STR );
               Gparm->InRing[MAX_RING_STR-1] = '\0';

               if( (Gparm->InKey = GetKey(str)) == -1 )
               {
                  logit( "e", "gaplist2: Invalid InRing name <%s>. Exiting.\n", str );
                  exit( -1 );
               }
            }
            init[0] = 1;
         }

         else if ( k_its( "HeartbeatInt" ) )
         {
            Gparm->HeartbeatInt = k_int();
            init[1] = 1;
         }

         else if ( k_its( "TablePrintInt" ) )
         {
            Gparm->TablePrintInt = k_int();
            init[2] = 1;
         }

         else if ( k_its( "MyModuleId" ) )
         {
            Gparm->MyModName[0] = '\0';
            if ( str=k_str() )
            {
               strncpy( Gparm->MyModName, str, MAX_MOD_STR );
               Gparm->MyModName[MAX_MOD_STR-1] = '\0';
            }
            init[3] = 1;
         }

         else if ( k_its( "PtoInterval" ) )
         {
            Gparm->PtoInterval = k_int();
            init[4] = 1;
         }

 /*opt*/ else if ( k_its( "GetLogo" ) )
         {
            MSG_LOGO *tlogo = NULL;
            int       nlogo = Gparm->nGetLogo;
            tlogo = (MSG_LOGO *)realloc( Gparm->GetLogo,
                                         (nlogo+1)*sizeof(MSG_LOGO) );
            if( tlogo == NULL )
            {
               logit( "e", "gaplist2: GetLogo: error reallocing %d bytes;"
                      " exiting!\n", (nlogo+1)*sizeof(MSG_LOGO) );
               exit( -1 );
            }
            Gparm->GetLogo = tlogo;

            if( str=k_str() )       /* read instid */
            {
               if( GetInst( str, &(Gparm->GetLogo[nlogo].instid) ) != 0 )
               {
                  logit( "e", "gaplist2: Invalid installation name <%s>"
                         " in <GetLogo> cmd; exiting!\n", str );
                  exit( -1 );
               }
               if( str=k_str() )    /* read module id */
               {
                  if( GetModId( str, &(Gparm->GetLogo[nlogo].mod) ) != 0 )
                  {
                     logit( "e", "gaplist2: Invalid module name <%s>"
                            " in <GetLogo> cmd; exiting!\n", str );
                     exit( -1 );

                  }
                  if( str=k_str() ) /* read message type */
                  {
                     if( strcmp(str,"TYPE_TRACEBUF")      !=0 &&
                         strcmp(str,"TYPE_TRACEBUF2")     !=0 &&
                         strcmp(str,"TYPE_TRACE_COMP_UA") !=0 &&
                         strcmp(str,"TYPE_TRACE2_COMP_UA")!=0    )
                     {
                        logit( "e","gaplist2: Invalid message type <%s> in <GetLogo>"
                               " cmd; must be TYPE_TRACEBUF, TYPE_TRACEBUF2,"
                               " TYPE_TRACE_COMP_UA, or TYPE_TRACE2_COMP_UA;"
                               " exiting!\n", str );
                        exit( -1 );
                     }
                     if( GetType( str, &(Gparm->GetLogo[nlogo].type) ) != 0 ) {
                        logit( "e", "gaplist2: Invalid message type <%s>"
                               " in <GetLogo> cmd; exiting!\n", str );
                        exit( -1 );
                     }
                     Gparm->nGetLogo++;
                  } /* end if msgtype */
               } /* end if modid */
            } /* end if instid */
         } /* end GetLogo cmd */


         else if ( k_its( "Scnl" ) )
         {
            LSCNL *scnlPtr;
            scnlSize += sizeof( LSCNL );
            scnlPtr = (LSCNL *)realloc( lScnl, scnlSize );
            if ( scnlPtr ==  NULL )
            {
               printf( "gaplist2: realloc() error in getconfig(). Exiting.\n" );
               exit( -1 );
            }
            lScnl = scnlPtr;
            lScnl[nScnl].isLabel = 0;              /* Not a label */
            strncpy( lScnl[nScnl].scnl.sta,  k_str(), TRACE2_STA_LEN );
            strncpy( lScnl[nScnl].scnl.chan, k_str(), TRACE2_CHAN_LEN );
            strncpy( lScnl[nScnl].scnl.net,  k_str(), TRACE2_NET_LEN );
            strncpy( lScnl[nScnl].scnl.loc,  k_str(), TRACE2_LOC_LEN );
            lScnl[nScnl].scnl.sta[TRACE2_STA_LEN-1]   = '\0';
            lScnl[nScnl].scnl.chan[TRACE2_CHAN_LEN-1] = '\0';
            lScnl[nScnl].scnl.net[TRACE2_NET_LEN-1]   = '\0';
            lScnl[nScnl].scnl.loc[TRACE2_LOC_LEN-1]   = '\0';
            lScnl[nScnl].head = NULL;
            lScnl[nScnl].tail = NULL;
            nScnl++;
         }

         else if ( k_its( "Label" ) )
         {
            LSCNL *scnlPtr;
            scnlSize += sizeof( LSCNL );
            scnlPtr = (LSCNL *)realloc( lScnl, scnlSize );
            if ( scnlPtr ==  NULL )
            {
               printf( "gaplist2: realloc() error in getconfig(). Exiting.\n" );
               exit( -1 );
            }
            lScnl = scnlPtr;
            lScnl[nScnl].isLabel = 1;
            strncpy( lScnl[nScnl].label, k_str(), LABEL_LEN );
            lScnl[nScnl].label[LABEL_LEN-1] = '\0';    /* Null-terminate */
            lScnl[nScnl].scnl.sta[0]  = '\0';
            lScnl[nScnl].scnl.chan[0] = '\0';
            lScnl[nScnl].scnl.net[0]  = '\0';
            lScnl[nScnl].scnl.loc[0]  = '\0';
            lScnl[nScnl].head = NULL;
            lScnl[nScnl].tail = NULL;
            nScnl++;
         }

/* An unknown parameter was encountered
   ************************************/
         else
         {
            logit( "e", "gaplist2: <%s> unknown parameter in <%s>\n",
                    com, config_file );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            logit( "e", "gaplist2: Bad <%s> command in <%s>. Exiting.\n", com,
                    config_file );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss > 0 )
   {
      logit( "e", "gaplist2: ERROR, no " );
      if ( !init[0] ) logit( "e", "<InRing> " );
      if ( !init[1] ) logit( "e", "<HeartbeatInt> " );
      if ( !init[2] ) logit( "e", "<TablePrintInt> " );
      if ( !init[3] ) logit( "e", "<MyModuleId> " );
      if ( !init[4] ) logit( "e", "<PtoInterval> " );
      logit( "e", "command(s) in <%s>. Exiting.\n", config_file );
      exit( -1 );
   }
   return;
}


   /**************************************************************
    *                         LogConfig()                        *
    *              Log the configuration parameters              *
    **************************************************************/

void LogConfig( GPARM *Gparm )
{
   int i;

   logit( "", "\n" );
   logit( "", "InRing:               %s\n", Gparm->InRing );
   logit( "", "MyModName:            %s\n", Gparm->MyModName );
   logit( "", "HeartbeatInt:        %6d\n", Gparm->HeartbeatInt );
   logit( "", "TablePrintInt:       %6d\n", Gparm->TablePrintInt );
   logit( "", "PtoInterval:         %6d\n", Gparm->PtoInterval );
   logit( "", "nGetLogo:            %6d\n", Gparm->nGetLogo );
   for( i=0; i<Gparm->nGetLogo; i++ )
   {
      logit( "", "GetLogo[%d]:   i%u m%u t%u\n", i,
            Gparm->GetLogo[i].instid, Gparm->GetLogo[i].mod,
            Gparm->GetLogo[i].type );
   }
   logit( "", "nScnl:               %6d\n", nScnl );
   for ( i = 0; i < nScnl; i++ )
   {
      if ( lScnl[i].isLabel )
      {
         logit( "", "Label    %s\n", lScnl[i].label );
      }
      else
      {
         logit( "", "Scnl     %5s %3s %2s %2s\n",
               lScnl[i].scnl.sta, lScnl[i].scnl.chan,
               lScnl[i].scnl.net, lScnl[i].scnl.loc );
      }
   }
   logit( "", "\n" );
   return;
}


             /********************************************
              *                 GetEwd()                 *
              *    Read things from earthworm*d files    *
              ********************************************/
void GetEwd( void )
{
   if ( GetInst( "INST_WILDCARD", &inst_wildcard ) != 0 )
   {
      logit( "et", "gaplist2: Error getting INST_WILDCARD. Exiting.\n" );
      exit( -1 );
   }
   if ( GetLocalInst( &inst_local ) != 0 )
   {
      logit( "et", "gaplist2: Error getting MyInstId.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF", &type_tracebuf ) != 0 )
   {
      logit( "et", "gaplist2: Error getting <TYPE_TRACEBUF>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF2", &type_tracebuf2 ) != 0 )
   {
      logit( "et", "gaplist2: Error getting <TYPE_TRACEBUF2>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACE_COMP_UA", &type_tracecomp ) != 0 )
   {
      logit( "et", "gaplist2: Error getting <TYPE_TRACE_COMP_UA>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACE2_COMP_UA", &type_trace2comp ) != 0 )
   {
      logit( "et", "gaplist2: Error getting <TYPE_TRACE2_COMP_UA>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_HEARTBEAT", &type_heartbeat ) != 0 )
   {
      logit( "et", "gaplist2: Error getting <TYPE_HEARTBEAT>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &type_error ) != 0 )
   {
      logit( "et", "gaplist2: Error getting <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetModId( "MOD_WILDCARD", &mod_wildcard ) != 0 )
   {
      logit( "et", "gaplist2: Error getting MOD_WILDCARD. Exiting.\n" );
      exit( -1 );
   }
   return;
}

