#
# This is the eqstat parameter file. This module gets "trigger" messages
# from the hypo_rings, and initiates threads to make record sections.

#  Basic Earthworm setup:
#
LogSwitch     1              # 0 to completely turn off disk log file
MyModuleId    MOD_EQSTAT     # module id for this instance of eqstat 

RingName      HYPO_RING      # ring to get input from
HeartBeatInt  5              # seconds between heartbeats

# List the message logos to grab from transport ring
#           Installation    Module      Message Type
GetSumFrom  INST_MENLO      MOD_EQPROC  TYPE_HYP2000ARC

#    others
MaxMsgSize     60000    # length of largest message we'll ever handle - in bytes
MaxMessages      100    # limit of number of message to buffer
wsTimeout 10 # time limit (secs) for any one interaction with a wave server.

# List of wave servers (ip port) to contact to retrieve trace data.

@/home/earthworm/run/params/all_waveservers.d

# Directory in which to store the temporary amplitude files.
GifDir   /home/picker/gifs/eqstat/amp_files 

# Directory for manual input of arc files.
ArcDir   /home/picker/gifs/eqstat/arc_indir/

# Directory in which to store the .gif files for output.
LocalTarget   /home/picker/sendfiler/eqstat/ 
# LocalTarget   /home/picker/sendfiles/eqstat/ 

 StationList     /home/earthworm/run/params/calsta.db1

 StationCorr     /home/earthworm/run/params/StaCorr.db

# We accept a command "Passes" which issues a heartbeat-type logit
# to let us know eqstat is alive (debugging)
 Passes   8000

# We accept a command "NoFlush" which prevents the ring from being
# flushed on restart.
#NoFlush
#
# LocalTime      -7  PDT  # Time difference to GMT, Time zone
  LocalTime      -8  PST  # Time difference to GMT, Time zone
 
  UseDST

# Plot Parameters - sorry it's so complex, but that's the price of versatility
# XSize           Overall size of plot in inches
# YSize           Setting these > 100 will imply pixels
#                                      
#              X     Y        
#              Size  Size                              
 SetPlotParams 550   150         


    # *** Optional Commands ***
# MinLogA   0.0
 SecsToWait 300
 MaxDist    300  # Max distance of traces to plot; default=100km
 MinSize    0.0  # Minimum magnitude to save in list; default=0.0
 RetryCount   2  # Number of attempts to get a trace from server; default=10
 MaxDays      5  # Max number of days to list; default=7
 
# We accept a command "Debug" which turns on a bunch of log messages
 
  Debug 1
# WSDebug

