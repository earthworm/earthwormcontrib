
                            /*********************
                             *    parsetrig.c    *
                             *********************/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>      /* For malloc() */
#include <chron3.h>
#include <earthworm.h>
#include <parse_trig.h>
#include "evtc.h"


     /***************************************************************
      *                       ParseTrigbuf()                        *
      *                                                             *
      *           Function to decode one trigger line.              *
      *                                                             *
      *  Returns EVTC_FAILURE or EVTC_SUCCESS                       *
      ***************************************************************/

int ParseTrigbuf( char *trigbuf, char **nxtLine, TRIGGER *trig, unsigned *eventId )
{
   char    token[5];
   int     seconds;
   SNIPPET snip;
   int     first = (trigbuf == *nxtLine) ? 1 : 0;

   if ( parseSnippet( trigbuf , &snip , nxtLine ) == EW_FAILURE )
      return EVTC_FAILURE;

   strcpy( trig->sta, snip.sta );
   strcpy( trig->cmp, snip.chan );
   strcpy( trig->net, snip.net );
   strcpy( trig->loc, snip.loc );

   strncpy( token, snip.startYYYYMMDD, 4 );
   token[4] = '\0';
   trig->ssYear = atoi( token );

   strncpy( token, snip.startYYYYMMDD+4, 2 );
   token[2] = '\0';
   trig->ssMonth = atoi( token );

   strncpy( token, snip.startYYYYMMDD+6, 2 );
   token[2] = '\0';
   trig->ssDay = atoi( token );

/* WARNING: snip.startHHMMSS is apparently unterminated!
            Looks like a bug in parsetrig.
   ****************************************************/
/* printf( "snip.startHHMMSS: %s\n", snip.startHHMMSS ); */

   strncpy( token, snip.startHHMMSS, 2 );
   token[2] = '\0';
   trig->ssHour = atoi( token );

   strncpy( token, snip.startHHMMSS+3, 2 );
   token[2] = '\0';
   trig->ssMinute = atoi( token );

   strncpy( token, snip.startHHMMSS+6, 2 );
   token[2] = '\0';
   seconds = atoi( token );
   trig->ssSecond = seconds + snip.starttime - floor(snip.starttime);

   trig->stime    = snip.starttime;
   trig->duration = snip.duration;

/* snip.eventId is only set the first time parseSnippet is called
   **************************************************************/
   if ( first && sscanf( snip.eventId, "%u", eventId ) < 1 )
   {
      logit( "et", "ParseTrigbuf: snip.eventId is non-integer: %s\n", snip.eventId );
      return EVTC_FAILURE;
   }

   return EVTC_SUCCESS;
}

