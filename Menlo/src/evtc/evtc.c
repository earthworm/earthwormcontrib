
    /***********************************************************************
     *                                evtc.c                               *
     *                                                                     *
     *                          Event Coordinator.                         *
     ***********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include <time_ew.h>
#include "evtc.h"
#include "trig_queue.h"
#include "arc_queue.h"

/* Function declarations
   *********************/
void GetConfig( char * );
void LogConfig( void );
void Lookup( void );
void SendStatus( SHM_INFO *, pid_t, unsigned char, short, char * );

/* Parameters obtained from configuration file
   *******************************************/
extern char     RingName[20];        /* Name of transport ring for i/o */
extern long     HeartBeatInterval;   /* Seconds between heartbeats */
extern short    nLogo;               /* Number of logo types to get from ring */
extern MSG_LOGO GetLogo[MAXLOGO];    /* Array for module,type,instid */
extern int      MaxMsgSize;          /* Max size of triglist and arc msgs */
extern int      TrigDequeue;         /* Triggers are dequeued after this many sec */
extern int      ArcDequeue;          /* Arc msgs are dequeued after this many sec */
extern int      LogArc;              /* If 1, log outgoing arc msgs; if 0, don't */

/* Get these parameters from the earthworm.h file
   **********************************************/
extern long          RingKey;        /* Key of transport ring for i/o */
extern unsigned char TypeHeartBeat;
extern unsigned char TypeError;
extern unsigned char TypeTriglist;
extern unsigned char TypeArcMsg;


int main( int argc, char **argv )
{
   long       timeLastBeat;     /* Time last heartbeat was sent */
   char       *msgbuf;          /* Buffer to hold triglist and arc msgs */
   char       *configFileName;
   MSG_LOGO   reclogo;          /* Logo of retrieved message */
   long       recsize;          /* Size of retrieved message */
   const int  LogFile = 1;      /* If 1, write a log file */
   SHM_INFO   Region;           /* Shared memory region to use for i/o */
   pid_t      myPid;            /* Process id of this process */
   TRIG_QUEUE tq = {0,0};       /* Trigger queue; initialize to empty */
   ARC_QUEUE  aq = {0,0};       /* Arc queue; initialize to empty */

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      printf( "Usage: evtc <configfile>\n" );
      return -1;
   }
   configFileName = argv[1];

/* Open an existing log file or create a new one
   *********************************************/
   logit_init( configFileName, 0, 256, LogFile );

/* Read the configuration file.
   GetConfig exits if there is an error in the file.
   ************************************************/
   GetConfig( configFileName );

/* Look up stuff in the earthworm.h tables.
   Lookup exits if it encounters an error.
   ***************************************/
   Lookup();

/* Get my own pid, for restart purposes
   ************************************/
#ifdef _SOLARIS
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "evtc: Can't get my pid. Exiting.\n" );
      return -1;
   }
#endif

#ifdef _WINNT
   myPid = _getpid();
#endif

/* Log the configuration file parameters
   *************************************/
   LogConfig();

/* Allocate the message buffer.  This buffer must be big
   enough to contain the biggest triglist or arc message.
   *****************************************************/
   msgbuf = (char *) malloc( (size_t)MaxMsgSize );
   if ( msgbuf == NULL )
   {
      logit( "e", "evtc: Can't allocate the message buffer. Exiting.\n" );
      return -1;
   }

/* Attach to shared memory ring.
   We will get triglist messages from this ring,
   and write status messages to this ring.
   *****************************************/
   tport_attach( &Region, RingKey );

/* Flush the transport ring
   ************************/
   while ( tport_getmsg( &Region, GetLogo, nLogo, &reclogo, &recsize, msgbuf,
                         MaxMsgSize ) != GET_NONE );

/* Send a heartbeat in first pass thru main loop
   *********************************************/
   timeLastBeat = time(0) - HeartBeatInterval - 1;

/* Main loop
   *********/
   while ( 1 )
   {
      time_t now = time(0);                         /* Current time */

/* Beat the heart
   **************/
      if ( HeartBeatInterval > 0 )
         if  ( (now - timeLastBeat) >= HeartBeatInterval )
         {
             timeLastBeat = now;
             SendStatus( &Region, myPid, TypeHeartBeat, 0, "" );
         }

/* See if termination has been requested
   *************************************/
      if ( tport_getflag( &Region ) == TERMINATE  ||
           tport_getflag( &Region ) == myPid )
         break;

/* Get new triglist and arc messages from shared memory
   ****************************************************/
      while ( 1 )
      {
         char errMsg[100];       /* Messages to statmgr */

         int res = tport_getmsg( &Region, GetLogo, nLogo, &reclogo, &recsize,
                                 msgbuf, MaxMsgSize );

         if ( res == GET_NONE )                      /* No more new messages */
            break;
         else if ( res == GET_TOOBIG )               /* Next message was too big */
         {
            sprintf( errMsg, "Retrieved msg[%ld] (i%u m%u t%u) too big for buffer[%d]",
                    recsize, reclogo.instid, reclogo.mod, reclogo.type, MaxMsgSize );
            SendStatus( &Region, myPid, TypeError, ERR_TOOBIG, errMsg );
            continue;                                /* Dump the message */
         }
         else if ( res == GET_MISS )                 /* Got a msg, but missed some */
         {
            sprintf( errMsg, "Missed message(s)  i%u m%u t%u  %s.",
                     reclogo.instid, reclogo.mod, reclogo.type, RingName );
            SendStatus( &Region, myPid, TypeError, ERR_MISSMSG, errMsg );
         }
         else if ( res == GET_NOTRACK )              /* Got a message, but can't */
         {                                           /* tell if any were missed */
            sprintf( errMsg, "Message received (i%u m%u t%u); NTRACK_GET exceeded",
                      reclogo.instid, reclogo.mod, reclogo.type );
            SendStatus( &Region, myPid, TypeError, ERR_NOTRACK, errMsg );
         }

/* Null-terminate the message.  If the message
   fills the buffer, discard the last character.
   ********************************************/
         if ( recsize < MaxMsgSize )
            msgbuf[recsize]   = '\0';
         else
            msgbuf[recsize-1] = '\0';

/* We received a triglist message
   ******************************/
         if ( reclogo.type == TypeTriglist )
         {
            TRIG_DATA td;               /* Structure to hold trigger data */
            td.tRecd = now;             /* Timestamp the trigger msg */
            td.nArc  = 0;               /* No arc msg OTs in this trigger */

/* Decode trig msg and log a few parameters
   ****************************************/
            if ( DecodeTrig( msgbuf, &td ) == EVTC_FAILURE )
            {
               sprintf( errMsg, "Can't decode triglist msg[%ld] (i%u m%u t%u)",
                       recsize, reclogo.instid, reclogo.mod, reclogo.type );
               SendStatus( &Region, myPid, TypeError, ERR_TRIGDECODE, errMsg );
               continue;                                /* Dump the message */
            }
            logit( "t", "Trig msg trcv:%u id:%8u bt:%10u dur:%d\n",
                   td.tRecd, td.eventId, td.begintime, td.duration );

/* Put the new trigger in the trigger queue
   ****************************************/
            if ( EnqueueTrig( &tq, td ) == EVTC_FAILURE )
            {
               sprintf( errMsg, "EnqueueTrig error.  Event id: %d", td.eventId );
               SendStatus( &Region, myPid, TypeError, ERR_MALLOC, errMsg );
               continue;                                /* Dump the message */
            }
         }           /* End of trig msg loop */

/* We received an arc message
   **************************/
         if ( reclogo.type == TypeArcMsg )
         {
            ARC_DATA ad;                /* Structure to hold arc data */
            ad.tRecd = now;             /* Timestamp the arc msg */
 
/* Decode arc msg and log a few parameters.
   Store a few parameters in ad struct.
   ***************************************/
            if ( DecodeArc( msgbuf, &ad ) == EVTC_FAILURE )
            {
               sprintf( errMsg, "Can't decode arc msg[%ld] (i%u m%u t%u)",
                       recsize, reclogo.instid, reclogo.mod, reclogo.type );
               SendStatus( &Region, myPid, TypeError, ERR_ARCDECODE, errMsg );
               continue;                                /* Dump the message */
            }
            logit( "t", "Arc msg  trcv:%u id:%8u ot:%10u\n",
                    ad.tRecd, ad.eventId, ad.ot );

/* Store the whole arc msg as text in the ad struct
   ************************************************/
            ad.msg = (char *) malloc( recsize );
            if ( ad.msg == NULL )
            {
               sprintf( errMsg, "malloc error.  Event id: %d", ad.eventId );
               SendStatus( &Region, myPid, TypeError, ERR_MALLOC, errMsg );
               continue;                                /* Dump the message */
            }
            memcpy( ad.msg, msgbuf, recsize );
            ad.msgSize = recsize;
 
/* Enqueue the ad struct in the arc queue
   **************************************/
            if ( EnqueueArc( &aq, ad ) == EVTC_FAILURE )
            {
               sprintf( errMsg, "EnqueueArc error.  Event id: %d", ad.eventId );
               SendStatus( &Region, myPid, TypeError, ERR_MALLOC, errMsg );
               free( ad.msg );
               continue;                                /* Dump the message */
            }
         }                   /* End of arc msg loop */
      }                      /* End of loop that processes new arcs/trigs */

/* Discard any triggers older than TrigDequeue seconds
   ***************************************************/
      while ( !IsTqEmpty( tq ) )
      {
         TRIG_DATA td = ValFrontTrig( tq );

         if ( (now - td.tRecd) < TrigDequeue ) break;
         DequeueTrig( &tq, &td );
         logit( "t", "Trig msg dequeued id:%u  nArc:%d\n", td.eventId, td.nArc );
      }

/* Are any arc msgs older than ArcDequeue seconds?
   If so, dequeue the arc msg.
   ***********************************************/
      while ( !IsAqEmpty( aq ) )
      {
         int      nTrig;
         ARC_DATA ad = ValFrontArc( aq );

         if ( (now - ad.tRecd) < ArcDequeue ) break;
         DequeueArc( &aq, &ad );

/* Is the OT of the arc msg within any triggers?
   *********************************************/
         nTrig = CountTrig( tq, ad.ot );
         logit( "t", "Arc msg dequeued id:%u  nTrig:%d\n", ad.eventId, nTrig );

/* If the OT isn't contained within any triggers, send the
   arc message to the output shared memory ring.  From there,
   it can be picked up by the arc2trig program and converted
   to a triglist message.
   *********************************************************/
         if ( nTrig == 0 )
         {
            extern unsigned char InstId;      /* Local installlation id */
            extern unsigned char MyModId;     /* Mod id for this instance of evtc */
            extern unsigned char TypeArcMsg;  /* HYP2000ARC message type */

            char errMsg[100];         /* Messages to statmgr */
            int      i;
            MSG_LOGO putLogo;         /* Logo of outgoing arc msg */

            if ( LogArc )
            {
               logit( "", "****************************************" );
               logit( "", "****************************************\n" ); 
               for ( i = 0; i < ad.msgSize; i++ )
                  logit( "", "%c", ad.msg[i] );
               logit( "", "****************************************" ); 
               logit( "", "****************************************\n" ); 
            }

            putLogo.instid = InstId;
            putLogo.mod    = MyModId;
            putLogo.type   = TypeArcMsg;

            if ( tport_putmsg( &Region, &putLogo, ad.msgSize, ad.msg ) == PUT_OK )
               logit( "t","evtc: Arc message sent to transport ring. id:%d\n", ad.eventId );
            else
            {
               sprintf( errMsg, "Arc message can't be sent to transport ring. id:%d\n",
                        ad.eventId );
               SendStatus( &Region, myPid, TypeError, ERR_PUTARC, errMsg );
            }
         }
         free( ad.msg );     /* Deallocate the arc message buffer */
      }
      sleep_ew( 1000 );      /* Wait for new messages to arrive */
   }

/* The termination flag was set.  It's time to exit.
   ************************************************/
   tport_detach( &Region );
   logit( "t", "Termination requested. Exiting.\n" );
   return 0;
}

