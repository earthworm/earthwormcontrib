
#include <time.h>

typedef struct
{
   time_t   tRecd;        /* Time we received the arc msg */
   unsigned eventId;      /* Id number assigned by carltrig */
   time_t   ot;           /* Origin time in secs since midnight 1/1/70 */
   char     *msg;         /* The full arc msg */
   int      msgSize;      /* Number of chars in arc msg */
} ARC_DATA;

struct arc_linked_list
{
   ARC_DATA  d;
   struct arc_linked_list *next;
};

typedef struct arc_linked_list ARC_ELEMENT;
typedef ARC_ELEMENT *ARC_LINK;

typedef struct arc_queue
{
   ARC_LINK front;
   ARC_LINK rear;
} ARC_QUEUE;


/* Function prototypes
   *******************/
int      IsAqEmpty( ARC_QUEUE );
ARC_DATA ValFrontArc( ARC_QUEUE );
int      DequeueArc( ARC_QUEUE *, ARC_DATA * );
int      EnqueueArc( ARC_QUEUE *, ARC_DATA );
int      DecodeArc( char *, ARC_DATA * ); 

