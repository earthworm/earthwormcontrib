
        /***************************************************************
         *                         config.c                            *
         *                                                             *
         *  Functions to read and log the configuration files.         *
         ***************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <transport.h>
#include <kom.h>
#include "evtc.h"

/* Functions in this source file
   *****************************/
void GetConfig( char * );
void LogConfig( void );
void Lookup( void );

/* Share these parameters with other source files
   **********************************************/
char     RingName[20];           /* Name of transport ring for i/o */
char     MyModName[20];          /* Speak as this module name/id */
long     HeartBeatInterval;      /* Seconds between heartbeats */
short    nLogo = 0;              /* Number of logo types to get from ring */
MSG_LOGO GetLogo[MAXLOGO];       /* Array for module,type,instid */
int      MaxMsgSize;             /* Max size of triglist and arc msgs */
int      TrigDequeue;            /* Triggers are dequeued after this many sec */
int      ArcDequeue;             /* Arc msgs are dequeued after this many sec */
int      LogArc;                 /* If 1, log outgoing arc msgs; if 0, don't */

/* Look these up in the earthworm.h tables with getutil functions
   **************************************************************/
long          RingKey;           /* Key of transport ring for i/o */
unsigned char InstId;            /* Local installation id */
unsigned char MyModId;           /* Module Id for this program */
unsigned char TypeHeartBeat;
unsigned char TypeError;
unsigned char TypeTriglist;
unsigned char TypeArcMsg;


  /**********************************************************************
   *  GetConfig() processes command file(s) using kom.c functions;      *
   *                    exits if any errors are encountered.            *
   **********************************************************************/

#define NCOMMAND 8

void GetConfig( char *configfile )
{
   const    ncommand = NCOMMAND;    /* Process this many required commands */
   char     init[NCOMMAND];         /* Init flags, one for each command */
   int      nmiss;                  /* Number of missing commands */
   char     *com;
   char     *str;
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
        logit( "", "evtc: Error opening command file <%s>. Exiting.\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         com = k_str();         /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )           continue;
         if ( com[0] == '#' )  continue;

/* Open a nested configuration file
   ********************************/
         if( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               logit( "", "evtc: Error opening command file <%s>. Exiting.\n",
                        &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         else if ( k_its("MyModuleId") )
         {
            str = k_str();
            if (str) strcpy( MyModName, str );
            init[0] = 1;
         }
         else if ( k_its("RingName") )
         {
            str = k_str();
            if (str) strcpy( RingName, str );
            init[1] = 1;
         }
         else if ( k_its("HeartBeatInterval") )
         {
            HeartBeatInterval = k_long();
            init[2] = 1;
         }
         else if ( k_its("GetLogo") )
         {
            if ( nLogo+1 > MAXLOGO )
            {
               logit( "", "evtc: Too many <GetLogo> commands in <%s>",
                        configfile );
               logit( "", "; max=%d. Exiting.\n", (int) MAXLOGO );
               exit( -1 );
            }
            if ( ( str=k_str() ) )
            {
               if ( GetInst( str, &GetLogo[nLogo].instid ) != 0 )
               {
                  logit( "", "evtc: Invalid installation name <%s>", str );
                  logit( "", " in <GetLogo> cmd. Exiting.\n" );
                  exit( -1 );
               }
            }
            if ( ( str=k_str() ) )
            {
               if ( GetModId( str, &GetLogo[nLogo].mod ) != 0 )
               {
                  logit( "", "evtc: Invalid module name <%s>", str );
                  logit( "", " in <GetLogo> cmd. Exiting.\n" );
                  exit( -1 );
               }
            }
            if ( ( str=k_str() ) )
            {
               if ( GetType( str, &GetLogo[nLogo].type ) != 0 )
               {
                  logit( "", "evtc: Invalid message type <%s>", str );
                  logit( "", " in <GetLogo> cmd. Exiting.\n" );
                  exit( -1 );
               }
            }
            nLogo++;
            init[3] = 1;
         }
         else if ( k_its("MaxMsgSize") )
         {
            MaxMsgSize = k_int();
            init[4] = 1;
         }
         else if ( k_its("TrigDequeue") )
         {
            TrigDequeue = k_int();
            init[5] = 1;
         }
         else if ( k_its("ArcDequeue") )
         {
            ArcDequeue = k_int();
            init[6] = 1;
         }
         else if ( k_its("LogArc") )
         {
            LogArc = k_int();
            init[7] = 1;
         }

/* Unknown command
   ***************/
         else
         {
            logit( "", "evtc: <%s> Unknown command in <%s>.\n",
                     com, configfile );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            logit( "", "evtc: Bad <%s> command in <%s>. Exiting.\n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
       logit( "", "evtc: ERROR, no " );
       if ( !init[0] ) logit( "", "<MyModuleId> "        );
       if ( !init[1] ) logit( "", "<RingName> "          );
       if ( !init[2] ) logit( "", "<HeartBeatInterval> " );
       if ( !init[3] ) logit( "", "<GetLogo> "           );
       if ( !init[4] ) logit( "", "<MaxMsgSize> "        );
       if ( !init[5] ) logit( "", "<TrigDequeue> "       );
       if ( !init[6] ) logit( "", "<ArcDequeue> "        );
       if ( !init[7] ) logit( "", "<LogArc> "            );
       logit( "", "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }
   return;
}


/***************************************************************************
 *  LogConfig()   Print the configuration file parameters in the log file. *
 ***************************************************************************/

void LogConfig( void )
{
   int i;

   logit( "", "\n" );
   logit( "", "MyModuleId:        %s\n", MyModName );
   logit( "", "RingName:          %s\n", RingName );
   logit( "", "HeartBeatInterval: %d\n", HeartBeatInterval );
   logit( "", "MaxMsgSize:        %d\n", MaxMsgSize );
   logit( "", "TrigDequeue:       %d\n", TrigDequeue );
   logit( "", "ArcDequeue:        %d\n", ArcDequeue );
   logit( "", "LogArc:            %d\n", LogArc );

   for ( i = 0; i < nLogo; i++ )
      logit( "", "GetLogo:      instid:%u  mod:%u  type:%u\n",
             GetLogo[i].instid, GetLogo[i].mod, GetLogo[i].type );
   logit( "", "\n" );
   return;
}


     /*****************************************************************
      *  Lookup()   Look up important info from earthworm.h tables    *
      *****************************************************************/

void Lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if ( ( RingKey = GetKey(RingName) ) == -1 )
   {
        logit( "", "evtc:  Invalid ring name <%s>. Exiting.\n", RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      logit( "", "evtc: Error getting local installation id. Exiting.\n" );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 )
   {
      logit( "", "evtc: Invalid module name <%s>. Exiting.\n", MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      logit( "", "evtc: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      logit( "", "evtc: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRIGLIST_SCNL", &TypeTriglist ) != 0 )
   {
      logit( "", "evtc: Invalid message type <TYPE_TRIGLIST_SCNL>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_HYP2000ARC", &TypeArcMsg ) != 0 )
   {
      logit( "", "evtc: Invalid message type <TYPE_HYP2000ARC>. Exiting.\n" );
      exit( -1 );
   }
   return;
}
