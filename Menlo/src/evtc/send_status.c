
#include <stdio.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>


/*************************************************************************
 * SendStatus() builds a heartbeat or error message & puts it into       *
 *              shared memory.  Writes errors to log file & screen.      *
 *************************************************************************/

void SendStatus( SHM_INFO *Region, pid_t myPid, unsigned char type, short ierr,
                 char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   long     t;

   extern unsigned char InstId;         /* Local installation id */
   extern unsigned char MyModId;        /* Module Id for this program */
   extern unsigned char TypeHeartBeat;
   extern unsigned char TypeError;

/* Build the message
   *****************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if ( type == TypeHeartBeat )
      sprintf( msg, "%ld %d\n", t, myPid );
   else if ( type == TypeError )
   {
      sprintf( msg, "%ld %hd %s\n", t, ierr, note );
      logit( "et", "%s\n", note );
   }
   else return;

   size = strlen( msg );

/* Write the message to shared memory
   **********************************/
   if ( tport_putmsg( Region, &logo, size, msg ) != PUT_OK )
   {
      if ( type == TypeHeartBeat )
         logit( "et","evtc: Error sending heartbeat.\n" );
      if ( type == TypeError )
         logit( "et","evtc: Error sending error:%d.\n", ierr );
   }
   return;
}

