
#include <time.h>

typedef struct
{
   time_t   tRecd;        /* Time we received the trig msg */
   unsigned eventId;      /* Id number assigned by carltrig */
   time_t   begintime;    /* Seconds since midnight 1/1/70 */
   int      duration;     /* Seconds */
   int      nArc;         /* Number of arc msg OTs in this triger */
} TRIG_DATA;

struct trig_linked_list
{
   TRIG_DATA  d;
   struct trig_linked_list *next;
};

typedef struct trig_linked_list TRIG_ELEMENT;
typedef TRIG_ELEMENT *TRIG_LINK;

typedef struct trig_queue
{
   TRIG_LINK front;
   TRIG_LINK rear;
} TRIG_QUEUE;


/* Function prototypes
   *******************/
int       IsTqEmpty( TRIG_QUEUE );
TRIG_DATA ValFrontTrig( TRIG_QUEUE );
int       DequeueTrig( TRIG_QUEUE *, TRIG_DATA * );
int       EnqueueTrig( TRIG_QUEUE *, TRIG_DATA );
int       CountTrig( TRIG_QUEUE, time_t );
int       DecodeTrig( char *, TRIG_DATA * );

