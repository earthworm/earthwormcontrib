#
#
#     This is the configuration file for the Event Coordinator (evtc).
#
MyModuleId         MOD_EVTC       # Module name for this instance of evtc.
                                  #   The specified name must appear in the
                                  #   earthworm.d configuration file.
RingName           HYPO_RING      # Triglist and arc messages are obtained from
                                  #   this ring.  Error and status messages are
                                  #   sent to this ring.
HeartBeatInterval  30             # Interval (seconds) between heartbeats sent
                                  #   to statmgr.  If 0, don't send heartbeats.
MaxMsgSize         100000         # Max size of triglist and arc messages.
TrigDequeue        1200           # Triggers are dequeued after this many seconds
ArcDequeue         600            # Arc msgs are dequeued after this many seconds
LogArc             1              # If 1, log outgoing arc msgs; if 0, don't.
#
# GetLogo: Get triglist and arc messages with these logos.
#          At least one GetLogo should have message type TYPE_TRIGLIST_SCNL, and
#          one should have message type TYPE_HYP2000ARC.
#          Up to 10 GetLogo lines are allowed.
#
#            Inst Id      Module Id      Message Type
#            -------      ---------      ------------
GetLogo     INST_MENLO  MOD_CARLSUBTRIG TYPE_TRIGLIST_SCNL
GetLogo     INST_MENLO  MOD_EQPROC      TYPE_HYP2000ARC
