
/*   evtc.h    */

#include <time.h>
#include <trace_buf.h>

#define ERR_MISSMSG     0   /* Message missed in transport ring */
#define ERR_TOOBIG      1   /* Retrieved message too large for buffer */
#define ERR_NOTRACK     2   /* Message retrieved; tracking limit exceeded */
#define ERR_MALLOC      3   /* malloc() error */
#define ERR_TRIGDECODE  4   /* Error decoding the triglist msg */
#define ERR_ARCDECODE   5   /* Error decoding the arc msg */
#define ERR_PUTARC      6   /* Can't send arc msg to transport ring */

#define EVTC_FAILURE    1
#define EVTC_SUCCESS    0

#define MAXLOGO        10   /* Maximum number of triglist and arc logos to get */

typedef struct
{
   char   sta[TRACE2_STA_LEN];   /* Station   */
   char   cmp[TRACE2_CHAN_LEN];  /* Component */
   char   net[TRACE2_NET_LEN];   /* Network   */
   char   loc[TRACE2_LOC_LEN];   /* Location   */
   int    ssYear;           /* Year of start save time (last two digits) */
   int    ssMonth;          /* Month of start save time (January = 1) */
   int    ssDay;            /* Day of start save time */
   int    ssHour;           /* Hour of start save time */
   int    ssMinute;         /* Minute of start save time */
   double ssSecond;         /* Second of start save time */
   double stime;            /* Start save time in sec since Jan 1, 1970 */
   int    duration;         /* Duration in seconds */
} TRIGGER;

