
#include <stdio.h>
#include <stdlib.h>        /* for malloc() */
#include <string.h>
#include <time.h>
#include <time_ew.h>
#include <earthworm.h>
#include "evtc.h"
#include "arc_queue.h"


int IsAqEmpty( ARC_QUEUE q )
{
   return (q.front == NULL);
}

ARC_DATA ValFrontArc( ARC_QUEUE q )
{
   return ( q.front->d );
}

int DequeueArc( ARC_QUEUE *q, ARC_DATA *x )
{
   ARC_LINK temp = q->front;

   if ( !IsAqEmpty(*q) )
   {
      *x = temp->d;
      q->front = temp->next;
      free( temp );
   }
   else
      printf( "Arc queue empty.\n" );
   return 0;
}
 
 
int EnqueueArc( ARC_QUEUE *q, ARC_DATA x )
{
   ARC_LINK temp;
 
   temp = (ARC_LINK) malloc( sizeof(ARC_ELEMENT) );
   if ( temp == NULL )                               /* malloc error */
      return EVTC_FAILURE;

   temp->d = x;
   temp->next = NULL;
   if ( IsAqEmpty(*q) )
      q->front = q->rear = temp;
   else
   {
      q->rear->next = temp;
      q->rear = temp;
   }
   return EVTC_SUCCESS;
}   


  /*******************************************************************
   *                            DecodeArc()                          *
   *                                                                 *
   *  arcbuf = The arc message                                       *
   *  Returns: 0 if all is ok; -1 if an error occured.               *
   *******************************************************************/

int DecodeArc( char *arcbuf, ARC_DATA *ad )
{
   int       year, month, day, hour, minute, second, hunsec;
   int       md;           /* Duration magnitude */
   time_t    ot;           /* Origin time */
   struct tm stm;
   unsigned  eventId;

   if ( sscanf( arcbuf, "%4d", &year ) < 1 )
   {
      logit( "e", "Error decoding year from arc message.\n" );
      return EVTC_FAILURE;
   }
   if ( sscanf( arcbuf+4, "%2d", &month ) < 1 )
   {
      logit( "e", "Error decoding month from arc message.\n" );
      return EVTC_FAILURE;
   }
   if ( sscanf( arcbuf+6, "%2d", &day ) < 1 )
   {
      logit( "e", "Error decoding day from arc message.\n" );
      return EVTC_FAILURE;
   }
   if ( sscanf( arcbuf+8, "%2d", &hour ) < 1 )
   {
      logit( "e", "Error decoding hour from arc message.\n" );
      return EVTC_FAILURE;
   }
   if ( sscanf( arcbuf+10, "%2d", &minute ) < 1 )
   {
      logit( "e", "Error decoding minute from arc message.\n" );
      return EVTC_FAILURE;
   }
   if ( sscanf( arcbuf+12, "%2d", &second ) < 1 )
   {
      logit( "e", "Error decoding second from arc message.\n" );
      return EVTC_FAILURE;
   }
   if ( sscanf( arcbuf+14, "%2d", &hunsec ) < 1 )
   {
      logit( "e", "Error decoding hundredths of a second from arc message.\n" );
      return EVTC_FAILURE;
   }
   if ( sscanf( arcbuf+70, "%3d", &md ) < 1 )
   {
      logit( "e", "Error decoding duration magnitude from arc message.\n" );
      return EVTC_FAILURE;
   }
   if ( sscanf( arcbuf+136, "%10u", &eventId ) < 1 )
   {
      logit( "e", "Error decoding event id from arc message.\n" );
      return EVTC_FAILURE;
   }

/* logit( "e", "Origin time: %d %d/%d %02d:%02d:%02d.%02d\n", year, month, day,
          hour, minute, second, hunsec ); */

/* Convert origin time to seconds since midnight 1/1/70
   ****************************************************/
   stm.tm_year  = year - 1900;
   stm.tm_mon   = month - 1;
   stm.tm_mday  = day;
   stm.tm_hour  = hour;
   stm.tm_min   = minute;
   stm.tm_sec   = second;
   stm.tm_isdst = 0;
   ot = timegm_ew( &stm );
 
/* Store arc parameters in ARC_DATA structure
   ******************************************/
   ad->eventId = eventId;
   ad->ot      = ot;
   return EVTC_SUCCESS;
}
