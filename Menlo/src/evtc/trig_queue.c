
#include <stdio.h>
#include <stdlib.h>        /* for malloc() */
#include <string.h>
#include <earthworm.h>
#include "evtc.h"
#include "trig_queue.h"

/* Function prototype */
int ParseTrigbuf( char *, char **, TRIGGER *, unsigned * );


int IsTqEmpty( TRIG_QUEUE q )
{
   return (q.front == NULL);
}


TRIG_DATA ValFrontTrig( TRIG_QUEUE q )
{
   return ( q.front->d );
}


int DequeueTrig( TRIG_QUEUE *q, TRIG_DATA *x )
{
   TRIG_LINK temp = q->front;

   if ( IsTqEmpty(*q) ) return EVTC_FAILURE;

   *x = temp->d;
   q->front = temp->next;
   free( temp );
   return EVTC_SUCCESS;
}


int EnqueueTrig( TRIG_QUEUE *q, TRIG_DATA x )
{
   TRIG_LINK temp;

   temp = (TRIG_LINK) malloc( sizeof(TRIG_ELEMENT) );
   if ( temp == NULL )
      return EVTC_FAILURE;

   temp->d = x;
   temp->next = NULL;
   if ( IsTqEmpty(*q) )
      q->front = q->rear = temp;
   else
   {
      q->rear->next = temp;
      q->rear = temp;
   }
   return EVTC_SUCCESS;
}


   /***********************************************************
    *                       CountTrig()                       *
    *                                                         *
    *  How many trig messages contain the OT of a particular  *
    *  arc message?                                           *
    ***********************************************************/

int CountTrig( TRIG_QUEUE tq, time_t ot )
{
   int       nTrig = 0;
   TRIG_LINK link;

   for ( link = tq.front; link != NULL; link = link->next )
   {
      time_t begintime = link->d.begintime;
      time_t endtime   = begintime + link->d.duration;
      if ( ot >= begintime && ot <= endtime )
      {
         nTrig++;
         link->d.nArc++;
      }
   }
   return nTrig;
}


  /*******************************************************************
   *                            DecodeTrig()                         *
   *                                                                 *
   *  trigbuf = The trig message                                     *
   *  Returns: 0 if all is ok; -1 if an error occured.               *   
   *******************************************************************/

int DecodeTrig( char *trigbuf, TRIG_DATA *td )
{
   char     *nxtLine;
   TRIGGER  trig;
   unsigned eventId;

/* Get event id and first trigger from trig message
   ************************************************/
   nxtLine = trigbuf;
   if ( ParseTrigbuf( trigbuf , &nxtLine, &trig, &eventId ) != EVTC_SUCCESS )
   {
      return EVTC_FAILURE;             /* Error parsing trig msg */
   }

/* Store trigger in TRIG_DATA structure
   ************************************/
   td->eventId   = eventId;
   td->begintime = (time_t) trig.stime;
   td->duration  = trig.duration;
   return EVTC_SUCCESS;
}

