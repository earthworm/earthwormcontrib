#
#                        Make file for evtc
#                         Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = evtc.o config.o arc_queue.o trig_queue.o send_status.o parsetrig.o \
    $L/logit.o $L/kom.o $L/getutil.o $L/sleep_ew.o $L/time_ew.o \
    $L/chron3.o $L/transport.o $L/parse_trig.o

evtc: $O
	cc -o $B/evtc $O -lm -lposix4

.c.o:
	cc -c ${CFLAGS} $<
 
lint:
	lint evtc.c $(GLOBALFLAGS)
 
# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/evtc*
