/* Include file for udstatlist */
/**************************************************************************
 *  defines                                                               *
 **************************************************************************/

#define MAXCHANNELS     1500  /* Maximum number of channels               */
#define MAX_TARGETS       10  /* largest number of targets                */
#define GDIRSZ           132  /* Size of string for GIF target directory  */
#define STALIST_SIZ      100  /* Size of string for station list file     */

/*********************************************************************
    Define the structure for Channel information.
 *********************************************************************/
typedef struct ChanInfo {      /* A channel information structure         */
    char    Site[6];           /* Site                                    */
    char    Comp[5];           /* Component                               */
    char    Net[5];            /* Net                                     */
    char    Loc[5];            /* Loc                                     */
    double  Lat;               /* Latitude                                */
    double  Lon;               /* Longitude                               */
    
    int     priority;          /* 0->OK 1->suspect 2->bad                 */
    int     errtype[10];       /*                                         */
} ChanInfo;

/**************************************************************************
 *  Define the structure for the Global data.                             *
 **************************************************************************/

struct Global {
    int      Debug;
    
    char     StationList[GDIRSZ];    /* File with SCN info */
    int		 NSCN;             /* Number of SCNs we know about               */
    ChanInfo Chan[MAXCHANNELS];

    int      nltargets;         /* Number of local target directories        */
    char     loctarget[MAX_TARGETS][GDIRSZ];/* Target in form-> /directory/     */
    
};
typedef struct Global Global;

