CFLAGS = -D_REENTRANT $(GLOBALFLAGS)

B = ${EW_HOME}/${EW_VERSION}/bin
L = ${EW_HOME}/${EW_VERSION}/lib

BINARIES = udstatlist.o  $L/kom.o \
		$L/gd.o $L/gdfontt.o $L/gdfonts.o $L/gdfontmb.o \
		$L/gdfontl.o $L/gdfontg.o 

udstatlist: $(BINARIES)
	cc -o $(B)/udstatlist $(BINARIES) -lsocket -lnsl -lm -mt -lposix4 -lthread -lc

.c.o:
	$(CC) $(CFLAGS) -g $(CPPFLAGS) -c  $(OUTPUT_OPTION) $<
