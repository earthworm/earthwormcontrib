/*******************************************************************************
 *      udstatlist.c:                                                          *
 * On a time schedule (scheduler or crontab), reads station status             *
 * info from a file and parses it for snwclient to send to SNWCollectionAgent. *
 *                                                                             *
 *                                                                             *
 *                                                                             *
 * Jim Luetgert    02/16/06                                                    *
 *******************************************************************************/

#include <platform.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <kom.h>

#include "udstatlist.h"

/* Functions in this source file
 *******************************/
void Get_Sta_Info(Global *But);
void config_me(Global *But, char *); 

/* Things to read from configuration file
 ****************************************/
static int  Debug = 1;               /* debug flag                      */

/* Other globals
 ***************/
Global    But;
char      module[50];

/*************************************************************************
 *  main( int argc, char **argv )                                        *
 *************************************************************************/

main( int argc, char **argv )
{
    int   i, j, k, yn;
    char  whoami[50], scnfile[150];
    char  string[500];
    FILE    *hlist;
    
    /* Check command line arguments
     ******************************/
    if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", "udstatlist");
        exit( 0 );
    }
    
    strcpy(module, argv[1]);
    for(i=0;i<(int)strlen(module);i++) if(module[i]=='.') module[i] = 0;

    sprintf(whoami, " %s: %s: ", module, "main");

    /* Read the configuration file(s)
     ********************************/
    config_me(&But, argv[1] );
    
    if(Debug) fprintf( stdout, "Getting station info. \n");
    Get_Sta_Info(&But);
    if(Debug) {
        fprintf( stdout, "Station info: \n");
        for(i=0;i<But.NSCN;i++) {
            fprintf( stdout, "%s %s %s %s %f %f %d  ", 
            But.Chan[i].Site, But.Chan[i].Comp, But.Chan[i].Net, But.Chan[i].Loc, 
            But.Chan[i].Lat,  But.Chan[i].Lon,  But.Chan[i].priority);
            for(k=0;k<10;k++) fprintf( stdout, " %d ",  But.Chan[i].errtype[k]);
            fprintf( stdout, "\n"); 
        }
    }
    
    for(j=0;j<But.nltargets;j++) {
        if(Debug) fprintf( stdout, "Building files for number %d of %d directories. \n", j+1, But.nltargets);

        /* Build the list of channels requiring heli-plots.
	    ***************************************************/
		/* Reads the array of data and makes list of problem channels. */

	    sprintf(scnfile, "%s%s", But.loctarget[j], "snw_data");
	    hlist = fopen(scnfile, "wb");
	    if(hlist == 0L) {
	        fprintf( stdout, "%s Unable to write File: %s\n", whoami, scnfile); 
	    } else {
			/* Loop Through Data
			 *******************/
		    for(i=0;i<But.NSCN;i++) {
		     /* yn = (But.Chan[i].priority < 10)? 1:0;
				fprintf(hlist, "%s-%s:2:Data Quality for Chan %s=%d;Data to WaveServers for Chan %s=%d\n", 
				But.Chan[i].Net, But.Chan[i].Site, But.Chan[i].Comp, But.Chan[i].priority, But.Chan[i].Comp, yn);  */
				fprintf(hlist, "%s-%s:1:Data Quality for Chan %s=%d\n", 
					But.Chan[i].Net, But.Chan[i].Site, But.Chan[i].Comp, But.Chan[i].priority);
		        
		    }
		    fclose(hlist);
	    }
	    
    }
    
}


/****************************************************************************
 *  Get_Sta_Info( );                                                        *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Sta_Info(Global *But)
{
    char    whoami[50], *com, *str, ns, ew;
    int     i, j, k, nfiles;

    sprintf(whoami, "%s: %s: ", module, "Get_Sta_Info");
    
    ns = 'N';
    ew = 'W';
    But->NSCN = 0;
    
        /* Open the main station file
         ****************************/
    nfiles = k_open( But->StationList );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, But->StationList );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;
            if( strcmp(com, "This") == 0 )  continue;

            /* Process anything else as a channel descriptor
             ***********************************************/

            if( But->NSCN >= MAXCHANNELS ) {
                fprintf(stderr, "%s Too many channel entries in <%s>", whoami, But->StationList );
                fprintf(stderr, "; max=%d; exiting!\n", (int) MAXCHANNELS );
                exit( -1 );
            }
            j = But->NSCN;
            
                /* S C N */
            strncpy( But->Chan[j].Site, com,  6);
            str = k_str();
            if(str) strncpy( But->Chan[j].Comp, str, 3);
            str = k_str();
            if(str) strncpy( But->Chan[j].Net,  str,  2);
            str = k_str();
            if(str) strncpy( But->Chan[j].Loc,  str,  2);
            for(i=0;i<6;i++) if(But->Chan[j].Site[i]==' ') But->Chan[j].Site[i] = 0;
            for(i=0;i<2;i++) if(But->Chan[j].Net[i]==' ')  But->Chan[j].Net[i]  = 0;
            for(i=0;i<3;i++) if(But->Chan[j].Comp[i]==' ') But->Chan[j].Comp[i] = 0;
            But->Chan[j].Comp[3] = But->Chan[j].Net[2] = But->Chan[j].Site[5] = 0;


                /* Lat Lon priority */
            But->Chan[j].Lat = k_val();
            But->Chan[j].Lon = k_val();
          /*   But->Chan[j].Lon = -But->Chan[j].Lon; */
            But->Chan[j].priority = k_int();
            
            for(k=0;k<10;k++) {
	            But->Chan[j].errtype[k]   = k_int();
            }
           
            But->NSCN++;
        }
        nfiles = k_close();
    }
}


/****************************************************************************
 *      config_me() process command file using kom.c functions              *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void config_me(Global *But, char* configfile )
{
    char    whoami[50], *com, *str;
    char    init[20];       /* init flags, one byte for each required command */
    int     ncommand;       /* # of required commands you expect              */
    int     nmiss;          /* number of required commands that were missed   */
    int     i, n, nfiles, success;

    sprintf(whoami, " %s: %s: ", module, "config_me");
        /* Set one init flag to zero for each required command
         *****************************************************/
    ncommand = 2;
    for(i=0; i<ncommand; i++ )  init[i] = 0;
    But->nltargets = 0;
    Debug = 0;

        /* Open the main configuration file
         **********************************/
    nfiles = k_open( configfile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, configfile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

                /* Process anything else as a command
                 ************************************/

        /* get the local target directory(s)
        ************************************/
/*0*/
            if( k_its("LocalTarget") ) {
                if ( But->nltargets >= MAX_TARGETS ) {
                    fprintf( stderr, "%s Too many <LocalTarget> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_TARGETS );
                    return;
                }
                if( (long)(str=k_str()) != 0 )  {
                    n = strlen(str);   /* Make sure directory name has proper ending! */
                    if( str[n-1] != '/' ) strcat(str, "/");
                    strcpy(But->loctarget[But->nltargets], str);
                }
                But->nltargets += 1;
                init[0] = 1;
            }

                /* get station list path/name
                *****************************/
/*1*/
            else if( k_its("StationList") ) {
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->StationList , str );
                init[1] = 1;
            }


            else if( k_its("Debug") )          Debug = 1;   /* optional commands */


                /* At this point we give up. Unknown thing.
                *******************************************/
            else {
                fprintf(stderr, "%s <%s> Unknown command in <%s>.\n",
                         whoami, com, configfile );
                continue;
            }

                /* See if there were any errors processing the command
                 *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "%s Bad <%s> command  in <%s>; exiting!\n",
                             whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

        /* After all files are closed, check init flags for missed commands
         ******************************************************************/
    nmiss = 0;
    for(i=0;i<ncommand;i++)  if( !init[i] ) nmiss++;
    if ( nmiss ) {
        fprintf( stderr, "%s ERROR, no ", whoami);
        if ( !init[0] )  fprintf( stderr, "<LocalTarget> "    );
        if ( !init[1] )  fprintf( stderr, "<StationList> "   );
        fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
        exit( -1 );
    }
 }


