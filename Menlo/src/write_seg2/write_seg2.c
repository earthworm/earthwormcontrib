/*
                                  write_seg2.c

   This program writes SEG2 files from the GERI system at SAFOD to a
   set of archive tapes.

   The program invokes the UNIX "tar" command to write the disk files
   to tape.

   This program runs on Solaris only.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <earthworm.h>
#include <unistd.h>
#include <kom.h>
#include <transport.h>
#include <time_ew.h>
#include "write_seg2.h"
#include "tapeio.h"

extern int errno;

#define SEG2FNC_NAMES 1    
#define EW2CTA_NAMES  2


/* Function prototypes
   *******************/
void GetConfig( char * );
void LogConfig( void );
void LookupMore( void );
void LookupMsgType( void );
void SendStatus( unsigned char, short, char * );
void GetTarFileName( char TarFileName[] );
int  ddTrace( char [] );

/* Global variables
   ****************/
static SHM_INFO Region;              /* Shared memory region to use for i/o */
static pid_t    myPid;               /* Process id of this process */
static char     Text[150];           /* String for log/error messages */

/* Get these from the config file
   ******************************/
static char RingName[20];            /* Name of transport ring for i/o */
static char MyModName[20];           /* Speak as this module name/id */
static long HeartBeatInterval;       /* Seconds between heartbeats */
       char TapeName[80];            /* Name of tape device */
       char ChangerName[80];         /* Name of tape changer */
       char TarFileDir[MAX_FNL];     /* Where to find tar files */
       char LogFileDir[MAX_FNL];     /* Where to find tape log files */
       int  LogTape;                 /* If 1, log to tape log files */
       int  BlockingFactor;          /* In 512-byte tape blocks */
static int  FileNameFormat;          /* which program created file names */ 

/* Look these up in the earthworm.h tables with getutil functions
   **************************************************************/
static long          RingKey;        /* Transport ring key */
static unsigned char InstId;         /* Local installation id */
static unsigned char MyModId;        /* Module Id for this program */
static unsigned char TypeHeartBeat;
       unsigned char TypeError;      /* Share with ddTrace() */


int main( int argc, char **argv )
{
   char   *configFile;
   time_t t_heart_prev;           /* Time last heartbeat was sent */
   time_t t_test_prev;            /* Time we last looked for SEG2 files */
   int    testInterval = 5;       /* Look for tar files this often */
   extern MTXSTATUS mtxStatus;    /* Declared in tapeio.c */

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      printf( "Usage: write_seg2 <configfile>\n" );
      return 0;
   }
   configFile = argv[1];

/* Look up message types from earthworm.h tables
   *********************************************/
   LookupMsgType();

/* Read the configuration file
   ***************************/
   GetConfig( configFile );

/* Look up more info from earthworm.h tables
   *****************************************/
   LookupMore();

/* Initialize name of log-file & open it
   *************************************/
   logit_init( "write_seg2", (short) MyModId, 256, 1 );
   logit( "" , "Read command file <%s>\n", argv[1] );

/* Get the pid of this process for restart purposes
   ************************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Log the configuration file
   **************************/
   LogConfig();

/* Change the working directory to TarFileDir
   ******************************************/
   if ( chdir_ew(TarFileDir) == -1 )
   {
      logit( "et", "chdir_ew() error: %s\n", strerror(errno) );
      logit( "et", "Can't change working directory to: %s\n", TarFileDir );
      logit( "et", "Exiting.\n" );
      return -1;
   }

/* Attach to shared memory ring
   ****************************/
   tport_attach( &Region, RingKey );
   logit( "", "Attached to public memory region: %s\n", RingName );

/* Make sure a tape is loaded in the drive
   ***************************************/
   if ( GetMtxStatus( ChangerName ) < 0 )
   {
      logit( "et", "write_seg2: Can't get mtx status. Exiting.\n" );
      return -1;
   }
   if ( strcmp( mtxStatus.drive[0].status, "Empty") == 0 )
   {
      logit( "et", "write_seg2: Error. Tape drive is empty. Exiting.\n" );
      return -1;
   }
   if ( strcmp( mtxStatus.drive[0].status, "Unknown") == 0 )
   {
      logit( "et", "write_seg2: Error. Tape drive status is unknown. Exiting.\n" );
      return -1;
   }
   if ( strcmp( mtxStatus.drive[0].status, "Full") != 0 )
   {
      logit( "et", "write_seg2: Invalid tape drive status. Exiting.\n" );
      return -1;
   }
   if ( strcmp( mtxStatus.drive[0].tapeLabel, "None") == 0 )
   {
      logit( "et", "At startup, tape drive contains a tape without a barcode label.\n" );
   }
   else
   {
      char message[MSGLEN];
      sprintf( message, "At startup, tape drive contains a tape with barcode label: %s\n",
               mtxStatus.drive[0].tapeLabel );
      logit( "et", message );
      if ( LogTape == 1 )
         WriteTapeLog( message, mtxStatus.drive[0].tapeLabel );
   }

/* Initialize heartbeat and SEG2-file-test timers
   **********************************************/
   time( &t_heart_prev );
   t_heart_prev -= HeartBeatInterval;
   time( &t_test_prev );
   t_test_prev -= testInterval;

/* Loop until kill flag is set
   ***************************/
   while ( tport_getflag( &Region ) != TERMINATE &&
           tport_getflag( &Region ) != myPid )
   {
      time_t t_heart;
      time_t t_test;

/* Send a heartbeat to statmgr
   ***************************/
      time( &t_heart );
      if  ( (t_heart - t_heart_prev) > HeartBeatInterval )
      {
         t_heart_prev = t_heart;
         SendStatus( TypeHeartBeat, 0, "" );
      }

/* See if any SEG2 tar files have showed up.
   If so, get the name of the earliest tar file.
   ********************************************/
      time( &t_test );
      if ( t_test - t_test_prev > testInterval )
      {
         int ntar;
         char TarFileName[MAX_FNL];

         GetTarFileName( TarFileName );
         if ( strlen(TarFileName) > 0 )
         {

/* Send the tar file to tape.
   If the tar-to-tape fails, just try again later.
   **********************************************/
            ddTrace( TarFileName );
         }
         t_test_prev = t_test;
      }
      sleep_ew( 1000 );
   }

/* Log which tape is in the tape drive.
   Then, exit program.
   ***********************************/
   if ( GetMtxStatus( ChangerName ) < 0 )
   {
      logit( "et", "write_seg2: Can't get mtx status. Exiting.\n" );
      return -1;
   }
   if ( strcmp( mtxStatus.drive[0].status, "Empty") == 0 )
   {
      logit( "et", "write_seg2: Error. Tape drive is empty. Exiting.\n" );
      return -1;
   }
   if ( strcmp( mtxStatus.drive[0].status, "Unknown") == 0 )
   {
      logit( "et", "write_seg2: Error. Tape drive status is unknown. Exiting.\n" );
      return -1;
   }
   if ( strcmp( mtxStatus.drive[0].status, "Full") != 0 )
   {
      logit( "et", "write_seg2: Invalid tape drive status. Exiting.\n" );
      return -1;
   }
   if ( strcmp( mtxStatus.drive[0].tapeLabel, "None") == 0 )
   {
      logit( "et", "At shutdown, tape drive contains a tape without a barcode label.\n" );
   }
   else
   {
      char message[MSGLEN];
      sprintf( message, "At shutdown, tape drive contains a tape with barcode label: %s\n",
               mtxStatus.drive[0].tapeLabel );
      logit( "et", message );
      if ( LogTape == 1 )
         WriteTapeLog( message, mtxStatus.drive[0].tapeLabel );
   }
   tport_detach( &Region );
   logit( "et", "Termination requested. Exiting.\n" );
   return 0;
}


     /********************************************************
      *                  GetTarFileName()                    *
      * See if any SEG2 tar files have showed up.            *
      * If so, get the name of the earliest tar file.        *
      ********************************************************/

void GetTarFileName( char TarFileName[] )
{
   DIR    *dp;
   struct dirent *dentp;
   int    ntarfile = 0;
   char   earlyfile[20];

/* Open the directory containing tar files
   ***************************************/
   dp = opendir( TarFileDir );
   if ( dp == NULL )
   {
      printf( "Error opening tar file directory: %s\n", TarFileDir );
      printf( "Exiting.\n" );
      exit( 0 );
   }

   if     ( FileNameFormat == SEG2FNC_NAMES ) strcpy( earlyfile, "30000101-00.tar"  );
   else if( FileNameFormat == EW2CTA_NAMES  ) strcpy( earlyfile, "wu3000010100.tar" );

/* Get name of earliest tar file
   *****************************/
   while (  dentp = readdir( dp ) )
   {
      struct stat buf;
      char   fullPath[MAX_FNL];
      int    rc;
      int    year, mon, day, hour;

/* Ignore non-regular files (eg directories)
   *****************************************/
      sprintf( fullPath, "%s/%s", TarFileDir, dentp->d_name );
      stat( fullPath, &buf );
      if ( !S_ISREG(buf.st_mode) ) continue;

/* Ignore non-date-based files. The file must contain a valid date.
   ***************************************************************/
      if( FileNameFormat == SEG2FNC_NAMES )      /* 15 chars: YYYYMMDD-HH.tar */
      {
        if ( strlen(dentp->d_name) != 15 ) continue;
        rc = sscanf( dentp->d_name, "%4d%2d%2d-%2d.tar", &year, &mon, &day, &hour );
        if ( rc < 4 ) continue;
        if ( dentp->d_name[8] != '-' ) continue;
        if ( strcmp(&dentp->d_name[11], ".tar") != 0 ) continue;
      }
      else if( FileNameFormat == EW2CTA_NAMES )  /* 16 chars: wuYYYYMMDDHH.tar */
      {
        if ( strlen(dentp->d_name) != 16 ) continue;
        rc = sscanf( dentp->d_name, "wu%4d%2d%2d%2d.tar", &year, &mon, &day, &hour );
        if ( rc < 4 ) continue;
        if ( strcmp(&dentp->d_name[12], ".tar") != 0 ) continue;
      } 
      else continue;

      if ( (year < 2000) || (year > 3000) ) continue;
      if ( ( mon < 1)    || ( mon > 12)   ) continue;
      if ( ( day < 1)    || ( day > 31)   ) continue;
      if ( (hour < 0)    || (hour > 23)   ) continue;
      ntarfile++;

/* Is this the earliest file?  If so, save file name
   *************************************************/
      if ( strcmp(dentp->d_name, earlyfile) < 0 )
         strcpy( earlyfile, dentp->d_name );
   }
   closedir( dp );

   TarFileName[0] = '\0';
   if ( ntarfile > 0 )
      strcpy( TarFileName, earlyfile );
   return;
}


  /************************************************************************
   *  GetConfig() processes command file(s) using kom.c functions;        *
   *                    exits if any errors are encountered.              *
   ************************************************************************/
#define NCOMMAND 10

void GetConfig( char *configfile )
{
   const int ncommand = NCOMMAND;  /* Process this many required commands */
   char  init[NCOMMAND];           /* Init flags, one for each command */
   int   nmiss;                    /* Number of missing commands */
   char  *com;
   char  *str;
   int   nfiles;
   int   success;
   int   i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      printf( "Error opening command file <%s>. Exiting.\n", configfile );
      exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )             /* Read next line from active file  */
      {
         com = k_str();            /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )          continue;
         if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
         if ( com[0] == '@' )
         {
            success = nfiles+1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "Can't open command file <%s>. Exiting.\n", &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         else if ( k_its("MyModuleId") )
         {
             str = k_str();
             if (str) strcpy( MyModName, str );
             init[0] = 1;
         }
         else if ( k_its("RingName") )
         {
             str = k_str();
             if (str) strcpy( RingName, str );
             init[1] = 1;
         }
         else if ( k_its("HeartBeatInterval") )
         {
             HeartBeatInterval = k_long();
             init[2] = 1;
         }
         else if ( k_its("TapeName") )
         {
             str = k_str();
             if (str) strcpy( TapeName, str );
             init[3] = 1;
         }
         else if ( k_its("ChangerName") )
         {
             str = k_str();
             if (str) strcpy( ChangerName, str );
             init[4] = 1;
         }
         else if ( k_its("TarFileDir") )
         {
             str = k_str();
             if (str)
             {
                strncpy( TarFileDir, str, MAX_FNL );
                init[5] = 1;
             }
         }
         else if ( k_its("LogFileDir") )
         {
             str = k_str();
             if (str)
             {
                strncpy( LogFileDir, str, MAX_FNL );
                init[6] = 1;
             }
         }
         else if ( k_its("LogTape") )
         {
             LogTape = k_long();
             init[7] = 1;
         }
         else if ( k_its("BlockingFactor") )
         {
             BlockingFactor = k_int();
             init[8] = 1;
         }
         else if( k_its( "FileNameFormat" ) )
         {
             FileNameFormat = k_int();
             if     ( FileNameFormat == SEG2FNC_NAMES ) init[9] = 1;
             else if( FileNameFormat == EW2CTA_NAMES  ) init[9] = 1;
             else {
               logit( "e", "write_seg2: Invalid <FileNameFormat> value %d in <%s>; \n",
                      FileNameFormat, configfile );
               exit( -1 );
             }
         }


/* Unknown command
   ***************/
         else
         {
             printf( "<%s> Unknown command in <%s>.\n", com, configfile );
             continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "Bad <%s> command in <%s>. Exiting.\n", com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
       printf( "ERROR, no " );
       if ( !init[0] )  printf( "<MyModuleId> "        );
       if ( !init[1] )  printf( "<RingName> "          );
       if ( !init[2] )  printf( "<HeartBeatInterval> " );
       if ( !init[3] )  printf( "<TapeName> "          );
       if ( !init[4] )  printf( "<ChangerName> "       );
       if ( !init[5] )  printf( "<TarFileDir> "        );
       if ( !init[6] )  printf( "<LogFileDir> "        );
       if ( !init[7] )  printf( "<LogTape> "           );
       if ( !init[8] )  printf( "<BlockingFactor> "    );
       if ( !init[9] )  printf( "<FileNameFormat> "    );
       printf( "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }
   return;
}


  /************************************************************************
   *  LogConfig() prints the config parameters in the log file.           *
   ************************************************************************/

void LogConfig( void )
{
   int i;

   logit( "", "\nConfig File Parameters:\n" );
   logit( "", "   MyModuleId:        %s\n", MyModName );
   logit( "", "   RingName:          %s\n", RingName );
   logit( "", "   HeartBeatInterval: %d\n", HeartBeatInterval );
   logit( "", "   TapeName:          %s\n", TapeName );
   logit( "", "   ChangerName:       %s\n", ChangerName );
   logit( "", "   TarFileDir:        %s\n", TarFileDir );
   logit( "", "   LogFileDir:        %s\n", LogFileDir );
   logit( "", "   LogTape:           %d\n", LogTape );
   logit( "", "   BlockingFactor:    %d\n", BlockingFactor );
   logit( "", "   FileNameFormat:    %d\n", FileNameFormat );
   return;
}


  /**********************************************************************
   *  LookupMsgType( )   Look up message types from earthworm.h tables  *
   **********************************************************************/

void LookupMsgType( void )
{
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "Invalid message type <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   return;
}


  /**********************************************************************
   *  LookupMore( )   Look up more from earthworm.h tables              *
   **********************************************************************/

void LookupMore( void )
{
   if ( ( RingKey = GetKey(RingName) ) == -1 )
   {
        printf( " Invalid ring name <%s>. Exiting.\n", RingName);
        exit( -1 );
   }
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "Error geting local installation id. Exiting.\n" );
      exit( -1 );
   }
   if ( GetModId( MyModName, &MyModId ) != 0 )
   {
      printf( "Invalid module name <%s>; Exiting.\n", MyModName );
      exit( -1 );
   }
   return;
}


/******************************************************************************
 * SendStatus() builds a heartbeat or error message & puts it into            *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/

void SendStatus( unsigned char type, short ierr, char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   long     t;

/* Build the message
   *****************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if ( type == TypeHeartBeat )
      sprintf( msg, "%ld %d\n", t, myPid );
   else if ( type == TypeError )
   {
      sprintf( msg, "%ld %hd %s\n", t, ierr, note );
      logit( "et", "%s\n", note );
   }
   else
      return;

   size = strlen( msg );           /* Don't include null byte in message */

/* Write the message to shared memory
   **********************************/
   if ( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if ( type == TypeHeartBeat )
           logit("et","Error sending heartbeat.\n" );
        else
           if ( type == TypeError )
              logit("et","Error sending error:%d.\n", ierr );
   }
   return;
}


    /****************************************************************
     * WriteTapeLog() creates and writes to individual log files    *
     * for each tape.                                               *
     *                                                              *
     * Returns  0 if no errors detected,                            *
     *         -1 if no tape label,                                 *
     *         -2 if the log file cannot be opened,                 *
     *         -3 if an error occured while writing to the log file *
     ****************************************************************/
#define DATE_LEN 10   /* Length of date field */

int WriteTapeLog( char *message, char *tapeLabel )
{
   FILE *fp;
   char fname[MAX_FNL];
   char msg[256];

   if ( strcmp( tapeLabel, "None" ) == 0 ) return -1;

/* Open tape log file
   ******************/
   strcpy( fname, LogFileDir );
   strcat( fname, "/" );
   strcat( fname, tapeLabel );
   strcat( fname, ".log" );

   fp = fopen( fname, "a" );
   if ( fp == NULL ) return -2;

/* Put current time at front of message
   ************************************/
   {
      time_t now;
      struct tm res;

      time( &now );
      gmtime_ew( &now, &res );
      sprintf( msg, "%4d%02d%02d_UTC_%02d:%02d:%02d %s", (res.tm_year + TM_YEAR_CORR),
               (res.tm_mon + 1), res.tm_mday, res.tm_hour, res.tm_min, res.tm_sec,
               message );
   }

/* Write message to tape log file
   ******************************/
   if ( fputs( msg, fp ) == EOF )
   {
      fclose( fp );
      return -3;
   }
   fclose( fp );
   return 0;
}
