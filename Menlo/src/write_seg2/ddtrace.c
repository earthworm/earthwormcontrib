
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <thread.h>
#include <earthworm.h>
#include "write_seg2.h"
#include "tapeio.h"

extern int errno;

/* Declared in write_seg2.c
   ************************/
extern char TapeName[];           /* Name of tape drive */
extern char ChangerName[];        /* Name of tape changer */
extern char TarFileDir[];         /* Directory containing tar files */
extern unsigned char TypeError;   /* Message type for error */
extern int  LogTape;
extern int  BlockingFactor;       /* In 512-byte tape blocks */


    /****************************************************************
     *                           ddTrace()                          *
     *                                                              *
     *  Invoked by write_seg2.                                      *
     *  This function writes a single tar file to tape.             *
     *  Returns 0 if all is ok, -1 if something failed.             *
     ****************************************************************/

int ddTrace( char TarFileName[] )
{
   int  tarStatus;
   int  mtStatus;
   int  rc;
   char errText[80];

/* Change the working directory to TarFileDir
   ******************************************/
   if ( chdir_ew(TarFileDir) == -1 )
   {
      logit( "et", "chdir_ew() error: %s\n", strerror(errno) );
      logit( "et", "Can't change working directory to: %s\n", TarFileDir );
      return -1;
   }

/* Tar the tar file to tape
   ************************/
   rc = WriteTarFile( TarFileName, TapeName, BlockingFactor, &tarStatus );
   if ( rc < 0 )
   {
      logit( "et", "WriteTarFile() error: %d\n", rc );
      sprintf( errText, "Tar error. rc: %d", rc );
      SendStatus( TypeError, ERR_TAR, errText );
      return -1;
   }

/* Tar to tape succeeded.  Log the event.
   *************************************/
   if ( tarStatus == 0 )
   {
      char message[MSGLEN];
      char tapeLabel[LABLEN];

      if ( GetTapeLabel( ChangerName, tapeLabel ) == 0 )
      {
         if ( strcmp( tapeLabel, "None" ) == 0 )
            sprintf( message, "File %s written to a tape without a barcode label.\n",
                     TarFileName );
         else
            sprintf( message, "File %s written to tape with barcode label: %s\n",
                     TarFileName, tapeLabel );
      }
      else
      {
         sprintf( message, "File %s written to tape.  Can't read barcode label.\n",
                  TarFileName );
      }
      logit( "et", "%s", message );
      if ( LogTape == 1 )
         WriteTapeLog( message, tapeLabel );

/* Erase disk file and return
   **************************/
      if ( remove( TarFileName ) == -1 )
         logit( "et", "Error deleting file: %s\n", TarFileName );
      return 0;
   }

/* Tar failed, possibly because we reached the end of tape.
   (which will produce an exit status of "2")
   *******************************************************/
   logit( "et", "WriteTarFile() failed. Status: %d\n", tarStatus );

/* Load the next tape into the autoloader
   **************************************/
   logit( "et", "Loading next tape from autoloader.\n" );
   rc = LoadNextTape( ChangerName );

/* LoadNextTape() failed, possibly because we have already
   loaded the last tape.
   *******************************************************/
   if ( rc < 0 )
   {
      logit( "et", "LoadNextTape(): " );
      if ( rc == -1 ) logit( "e", "Can't get mtx status." );
      if ( rc == -2 ) logit( "e", "No tape in tape drive." );
      if ( rc == -3 ) logit( "e", "No empty slots in autoloader." );
      if ( rc == -4 ) logit( "e", "No more tapes available in autoloader." );
      if ( rc == -5 ) logit( "e", "mtx error on tape load or unload." );
      if ( rc == -7 ) logit( "e", "Blocking factor too large." );
      logit( "e", "\n" );
      sprintf( errText, "LoadNextTape() error. rc: %d", rc );
      SendStatus( TypeError, ERR_OFFLINE, errText );
      return -1;
   }

/* We successfully changed tapes
   *****************************/
   logit( "et", "LoadNextTape() succeeded.\n" );
   sprintf( errText, "Autoloader tape changed." );
   SendStatus( TypeError, MSG_OFFLINE, errText );

/* Wait until the tape is loaded into the tape drive.
   GetMtStatus() will block until tape is loaded.
   *************************************************/
   rc = GetMtStatus( TapeName, &mtStatus );
   if ( rc < 0 )
      logit( "et", "GetMtStatus() error. rc = &d\n", rc );
   else
      if ( mtStatus == 1 )
         logit( "et", "mt command unrecognized or mt unable to open tape drive.\n" );
      else if ( mtStatus == 2 )
         logit( "et", "An mt operation failed.\n" );

/* The next tape loaded ok.  ddTrace() should succeed the
   next time it is called, unless the tape drive is bad.
   ******************************************************/
   return -1;
}
