
/* Error messages used by write_seg2
   *********************************/
#define  ERR_TAR        1           /* Tar error */
#define  ERR_OFFLINE    2           /* Error taking tape offline */
#define  MSG_OFFLINE    3           /* Autoloader tape changed */

#define  MAX_FNL      256           /* Max length of file name */
#define  MSGLEN       256

void SendStatus( unsigned char, short, char * );
int  WriteTapeLog( char *message, char *tapeLabel );

