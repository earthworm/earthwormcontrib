#
# This is write_seg2's parameter file
#
MyModuleId        MOD_WRITE_SEG2 # Module id for this instance of write_seg2
RingName          HYPO_RING      # Shared memory ring for input/output
HeartBeatInterval 30             # Seconds between heartbeats
TapeName          /dev/rmt/0n    # Should end in "n" (no rewind)
ChangerName       /dev/changer   # Device name of tape changer
LogTape           1              # If 1, log to tape log files in
                                 #    directory LogFileDir

# BlockingFactor specifies the number of 512-byte tape blocks to be
# included in each tar write operation.  Warning: The default blocking
# factor for tar is 20.  If you write a tape with a blocking factor
# greater than 20, you will need to read the tape back with a blocking
# factor greater than or equal to its original blocking factor.
# Otherwise, you will get a read error.  For more information, see the
# man page for tar.
BlockingFactor    126

# TarFileDir contains tar files to be written to tape
TarFileDir        /home/earthworm/run/seg2tar

# LogFileDir contains a log file for each tape
LogFileDir        /home/earthworm/run/tapelogs

# What is the hourly directory name format?
FileNameFormat  1        # 1 = seg2fnc: format YYYYMMDD-HH
                         # 2 =  ew2cta: format wuYYYYMMDDHH

# THE END
