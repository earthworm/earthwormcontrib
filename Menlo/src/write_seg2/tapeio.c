
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <earthworm.h>
#include "write_seg2.h"
#include "tapeio.h"

/* Length of string to contain blocking factor */
#define LENBFSTR 20

static int mtxInit = 0;
MTXSTATUS  mtxStatus;

extern int LogTape;     /* If 1, log to tape log files */


   /*****************************************************************
    *                         GetMtStatus()                         *
    *                                                               *
    *               Get the status of the tape drive.               *
    *                                                               *
    *  Returns  0 = no error                                        *
    *          -1 = fork error                                      *
    *          -2 = execl error                                     *
    *          -3 = waitpid error                                   *
    *          -4 = mt terminated by signal                         *
    *          -5 = mt stopped by signal                            *
    *          -6 = unknown mt exit status                          *
    *****************************************************************/

int GetMtStatus( char *tapename,       /* Name of tape device */
                 int  *mtrc )          /* mt return code */
{
   pid_t pid;
   int   waitStatus;

/* Fork a child process to run the mt command
   ******************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                              /* In child process */
         execlp( "mt", "mt", "-f", tapename, "status", NULL );
         return -2;

      default:
         break;
   }

/* Wait for the mt command to complete
   ***********************************/
   if ( waitpid( pid, &waitStatus, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( waitStatus ) )          /* mt exited */
   {
      *mtrc = WEXITSTATUS( waitStatus );
      return 0;
   }
   else if ( WIFSIGNALED( waitStatus ) )   /* mt terminated by signal */
   {
      *mtrc = WTERMSIG( waitStatus );
      return -4;
   }
   else if ( WIFSTOPPED( waitStatus ) )    /* mt stopped by signal */
   {
      *mtrc = WSTOPSIG( waitStatus );
      return -5;
   }
   return -6;                              /* Unknown exit status */
}


   /*****************************************************************
    *                         WriteTarFile()                        *
    *                                                               *
    *        Copies a file to tape using the tar command.           *
    *                                                               *
    *  Returns  0 = no error                                        *
    *          -1 = fork error                                      *
    *          -2 = execl error                                     *
    *          -3 = waitpid error                                   *
    *          -4 = tar terminated by signal                        *
    *          -5 = tar stopped by signal                           *
    *          -6 = unknown tar exit status                         *
    *          -7 = blocking factor too large                       *
    *****************************************************************/


int WriteTarFile( char *fname,          /* Name of file to write */
                  char *tapename,       /* Name of tape device */
                  int  blockingFactor,  /* Argument to tar command */
                  int  *mtrc )          /* mt return code */
{
   pid_t pid;
   int   waitStatus;
   char  bfstr[LENBFSTR];     /* String to contain blocking factor */

/* Encode the blocking factor.
   snprintf() returns either the number of bytes written, or, if the
   buffer is too small, the number of bytes that would have been written.
   *********************************************************************/
   if ( snprintf(bfstr, LENBFSTR, "%d", blockingFactor) >= LENBFSTR )
      return -7;

/* Fork a child process to run the tar command.
   The "e" option means: exit immediately on any
   write error, with a positive status code.
   *********************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                              /* In child process */
         execlp( "/bin/tar", "/bin/tar", "cefb", tapename, bfstr, fname, NULL );
         return -2;

      default:
         break;
   }

/* Wait for the tar command to complete
   ************************************/
   if ( waitpid( pid, &waitStatus, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( waitStatus ) )          /* tar exited */
   {
      *mtrc = WEXITSTATUS( waitStatus );
      return 0;
   }
   else if ( WIFSIGNALED( waitStatus ) )   /* tar terminated by signal */
   {
      *mtrc = WTERMSIG( waitStatus );
      return -4;
   }
   else if ( WIFSTOPPED( waitStatus ) )    /* tar stopped by signal */
   {
      *mtrc = WSTOPSIG( waitStatus );
      return -5;
   }
   return -6;                              /* Unknown exit status */
}


   /*****************************************************************
    *                        InitMtxStatus()                        *
    *                                                               *
    *              Initialize the mtx status structure.             *
    *                                                               *
    *  Returns  0 = no error                                        *
    *          -1 = popen() failed                                  *
    *          -2 = error getting first line from pipe              *
    *          -3 = error initializing drive info                   *
    *          -4 = error initializing slot info                    *
    *****************************************************************/

int InitMtxStatus( char *changer )           /* Name of tape changer */
{
   FILE *fp;
   int  i;
   char *ptr;
   char s[SLEN];
   char command[SLEN];
   int  ndrive;
   int  nslot;

/* Create a read-only pipe to the output of the mtx command
   ********************************************************/
   snprintf( command, SLEN, "mtx -f %s status", changer );
   fp = popen( command, "r" );
   if ( fp == NULL )
   {
      printf( "popen() failed.\n" );
      return -1;
   }

/* Get first line from pipe
   ************************/
   if ( fgets( s, SLEN, fp ) == NULL ) return -2;
   s[SLEN-1] = 0;

/* Decode ndrive.  Allocate space for drive info.
   *********************************************/
   ptr = strchr( s, ':' );
   if ( ptr++ == NULL ) return -3;
   if ( sscanf( ptr, "%d", &ndrive ) < 1 ) return -3;
   mtxStatus.ndrive = ndrive;
   mtxStatus.drive  = (DRIVE *)calloc( ndrive, sizeof(DRIVE) );
   if ( mtxStatus.drive == NULL ) return -3;
   for ( i = 0; i < ndrive; i++ )
      strcpy( mtxStatus.drive[i].status, "Unknown" );

/* Decode nslot.  Allocate space for slot info.
   *******************************************/
   ptr = strchr( s, ',' );
   if ( ptr++ == NULL ) return -4;
   if ( sscanf( ptr, "%d", &nslot ) < 1 ) return -4;
   mtxStatus.nslot = nslot;
   mtxStatus.slot  = (SLOT *)calloc( nslot, sizeof(SLOT) );
   if ( mtxStatus.slot == NULL ) return -4;
   for ( i = 0; i < nslot; i++ )
      strcpy( mtxStatus.slot[i].status, "Unknown" );

   pclose( fp );    /* Close the pipe */
   return 0;
}


   /*****************************************************************
    *                        GetMtxStatus()                         *
    *                                                               *
    *               Get the status of the autoloader.               *
    *                                                               *
    *  Returns  0 = no error                                        *
    *          -1 = popen() failed                                  *
    *          -2 = error getting first line from pipe              *
    *          -3 = error getting drive status                      *
    *          -4 = error getting slot status                       *
    *          -5 = InitMtxStatus() error                           *
    *****************************************************************/

int GetMtxStatus( char *changer )         /* Name of tape changer */
{
   int  i;
   FILE *fp;
   char *token;
   char s[SLEN];
   char command[SLEN];

/* Initialize the mtx status structure
   ***********************************/
   if ( !mtxInit )
   {
      int rc = InitMtxStatus( &changer[0] );
      if ( rc < 0 )
      {
         printf( "InitMtxStatus() error: %d\n", rc );
         return -5;
      }
      mtxInit = 1;
   }

/* Create a read-only pipe to the output of the mtx command
   ********************************************************/
   snprintf( command, SLEN, "mtx -f %s status", changer );
   fp = popen( command, "r" );
   if ( fp == NULL )
   {
      printf( "popen() failed.\n" );
      return -1;
   }

/* Read first line from pipe.  Ignore contents.
   *******************************************/
   if ( fgets( s, SLEN, fp ) == NULL ) return -2;

/* Read drive status and tape label
   ********************************/
   for ( i = 0; i < mtxStatus.ndrive; i++ )
   {
      do     /* Ignore short lines */
      {
         if ( fgets( s, SLEN, fp ) == NULL ) return -3;
         s[SLEN-1] = 0;
      } while (strlen(s) < 10);

      token = strtok( s, ":" );
      if ( token == NULL ) return -3;
      token = strtok( NULL, "\n " );
      if ( token == NULL ) return -3;
      strncpy( mtxStatus.drive[i].status, token, STATLEN );
      if ( strcmp(token,"Full") == 0 )
      {
         strcpy( mtxStatus.drive[i].tapeLabel, "None" );
         token = strtok( NULL, "=" );
         if ( token != NULL )
         {
            token = strtok( NULL, "\n " );
            if ( token != NULL )
            {
               strncpy( mtxStatus.drive[i].tapeLabel, token, LABLEN );
               mtxStatus.drive[i].tapeLabel[LABLEN-1] = 0;
            }
         }
      }
   }

/* Read slot status and tape label
   *******************************/
   for ( i = 0; i < mtxStatus.nslot; i++ )
   {
      do     /* Ignore short lines */
      {
         if ( fgets( s, SLEN, fp ) == NULL ) return -4;
         s[SLEN-1] = 0;
      } while (strlen(s) < 10);

      token = strtok( s, ":" );
      if ( token == NULL ) return -4;
      token = strtok( NULL, "\n " );
      if ( token == NULL ) return -4;
      strncpy( mtxStatus.slot[i].status, token, STATLEN );
      if ( strcmp(token,"Full") == 0 )
      {
         strcpy( mtxStatus.slot[i].tapeLabel, "None" );
         token = strtok( NULL, "=" );
         if ( token != NULL )
         {
            token = strtok( NULL, "\n " );
            if ( token != NULL )
            {
               strncpy( mtxStatus.slot[i].tapeLabel, token, LABLEN );
               mtxStatus.slot[i].tapeLabel[LABLEN-1] = 0;
            }
         }
      }
   }
   pclose( fp );    /* Close the pipe */
   return 0;
}


   /*****************************************************************
    *                          SpawnMtx()                           *
    *                                                               *
    *            Spawn an mtx process with one argument.            *
    *                                                               *
    *  Returns  0 = no error                                        *
    *          -1 = fork error                                      *
    *          -2 = execl error                                     *
    *          -3 = waitpid error                                   *
    *          -4 = mtx terminated by signal                        *
    *          -5 = mtx stopped by signal                           *
    *          -6 = unknown mtx exit status                         *
    *****************************************************************/

int SpawnMtx( char *changer,        /* Name of tape changer */
              char command[],       /* Load or unload */
              int  mtxArg,          /* Argument to mtx command */
              int  *mtxrc )         /* mtx return code */
{
   pid_t pid;
   int   waitStatus;
   char  slot[20];

/* Convert slot number to text string
   **********************************/
   snprintf( slot, 20, "%d", mtxArg );

/* Fork a child process to run the mtx command
   *******************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                              /* In child process */
         execlp( "mtx", "mtx", "-f", changer, command, slot, NULL );
         return -2;

      default:
         break;
   }

/* Wait for the mtx command to complete
   ************************************/
   if ( waitpid( pid, &waitStatus, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( waitStatus ) )          /* mtx exited */
   {
      *mtxrc = WEXITSTATUS( waitStatus );
      return 0;
   }
   else if ( WIFSIGNALED( waitStatus ) )   /* mtx terminated by signal */
   {
      *mtxrc = WTERMSIG( waitStatus );
      return -4;
   }
   else if ( WIFSTOPPED( waitStatus ) )    /* mtx stopped by signal */
   {
      *mtxrc = WSTOPSIG( waitStatus );
      return -5;
   }
   return -6;                              /* Unknown exit status */
}


   /*****************************************************************
    *                         MtxLoadTape()                         *
    *                                                               *
    *               Load a tape from the autoloader.                *
    *                                                               *
    *  Returns  0 = no error                                        *
    *          -1 = fork error                                      *
    *          -2 = execl error                                     *
    *          -3 = waitpid error                                   *
    *          -4 = mtx terminated by signal                        *
    *          -5 = mtx stopped by signal                           *
    *          -6 = unknown mtx exit status                         *
    *****************************************************************/

int MtxLoadTape( char *changer,        /* Name of tape changer */
                 int  tapeToLoad,      /* Slot number */
                 int  *mtxrc )         /* mtx return code */
{
   return SpawnMtx( changer, "load", tapeToLoad, mtxrc );
}


   /*****************************************************************
    *                        MtxUnloadTape()                        *
    *                                                               *
    *              Unload a tape from the autoloader.               *
    *                                                               *
    *  Returns  0 = no error                                        *
    *          -1 = fork error                                      *
    *          -2 = execl error                                     *
    *          -3 = waitpid error                                   *
    *          -4 = mtx terminated by signal                        *
    *          -5 = mtx stopped by signal                           *
    *          -6 = unknown mtx exit status                         *
    *****************************************************************/

int MtxUnloadTape( char *changer,        /* Name of tape changer */
                   int  tapeToUnload,    /* Slot number */
                   int  *mtxrc )         /* mtx return code */
{
   return SpawnMtx( changer, "unload", tapeToUnload, mtxrc );
}


   /*******************************************************************
    *                         LoadNextTape()                          *
    *                                                                 *
    *  Load the next tape in the autoloader, using mtx.               *
    *  We assume the autoloader contains only one tape drive.         *
    *                                                                 *
    *  Returns  0 = No error.                                         *
    *          -1 = Can't get mtx status                              *
    *          -2 = No tape in tape drive                             *
    *          -3 = No empty slots in autoloader                      *
    *          -4 = No more tapes available in autoloader             *
    *          -5 = mtx error on tape load or unload (see log file)   *
    *******************************************************************/

int LoadNextTape( char *changer )         /* Name of tape changer */
{ 
   char command[SLEN];
   char message[MSGLEN];
   int  rc;
   int  mtxrc;
   int  i;
   int  fes;           /* First empty slot */
   int  nswt;          /* Next slot with a tape */

/* Make sure the tape drive contains a tape
   ****************************************/
   rc = GetMtxStatus( changer );
   if ( rc < 0 ) return -1;
   if ( mtxStatus.ndrive < 1 ) return -1;
   if ( strcmp( mtxStatus.drive[0].status, "Full" ) != 0 )
      return -2;

/* Get slot number of first empty slot
   ***********************************/
   for ( i = 0; i < mtxStatus.nslot; i++ )
   {
      if ( strcmp(mtxStatus.slot[i].status, "Empty") == 0 )
         break;
   }
   if ( i >= mtxStatus.nslot ) return -3;  /* No empty slots */
   fes = i + 1;                            /* First empty slot */

/* Unload tape into first empty slot
   *********************************/
   rc = MtxUnloadTape( changer, fes, &mtxrc );
   if ( rc != 0 )
      printf( "MtxUnloadTape() rc: %d\n", rc );
   if ( mtxrc != 0 )
      printf( "MtxUnloadTape() mtxrc: %d\n", mtxrc );
   if ( (rc != 0) || (mtxrc != 0) ) return -5;

/* Get label of unloaded tape
   **************************/
   rc = GetMtxStatus( changer );
   if ( rc < 0 ) return -1;
   sprintf( message, "Tape unloaded from tape drive to slot %d.  Label: %s\n",
            fes, mtxStatus.slot[i].tapeLabel );
   logit( "et", message );
   if ( LogTape == 1 )
      WriteTapeLog( message, mtxStatus.slot[i].tapeLabel );

/* Get slot number of next tape to load
   ************************************/
   for ( i = fes; i < mtxStatus.nslot; i++ )
   {
      if ( strcmp(mtxStatus.slot[i].status, "Full") == 0 )
         break;
   }
   if ( i >= mtxStatus.nslot ) return -4;  /* No more empty tapes */
   nswt = i + 1;                           /* Next slot with a tape */

/* Load tape from next full slot into tape drive
   *********************************************/
   rc = MtxLoadTape( changer, nswt, &mtxrc );
   if ( rc != 0 )
      printf( "MtxLoadTape() rc: %d\n", rc );
   if ( mtxrc != 0 )
      printf( "MtxLoadTape() mtxrc: %d\n", mtxrc );
   if ( (rc != 0) || (mtxrc != 0) ) return -5;

/* Get label of tape just loaded into tape drive
   *********************************************/
   rc = GetMtxStatus( changer );
   if ( rc < 0 ) return -1;
   sprintf( message, "Tape loaded into tape drive from slot %d.  Label: %s\n",
            nswt, mtxStatus.drive[0].tapeLabel );
   logit( "et", message );
   if ( LogTape == 1 )
      WriteTapeLog( message, mtxStatus.slot[i].tapeLabel );
   return 0;
}


   /*******************************************************************
    *                         GetTapeLabel()                          *
    *                                                                 *
    *  Get the label of whichever tape is loaded in the tape drive.   *
    *  We assume the autoloader contains only one tape drive.         *
    *                                                                 *
    *  Returns  0 = No error.                                         *
    *          -1 = Can't get mtx status                              *
    *          -2 = No tape in tape drive                             *
    *******************************************************************/

int GetTapeLabel( char *changer,          /* Name of tape changer */
                  char *tapeLabel )
{ 
   int rc;

/* Make sure the tape drive contains a tape
   ****************************************/
   rc = GetMtxStatus( changer );
   if ( rc < 0 ) return -1;
   if ( mtxStatus.ndrive < 1 ) return -1;
   if ( strcmp( mtxStatus.drive[0].status, "Full" ) != 0 )
      return -2;

/* Return the tape label
   *********************/
   strcpy( tapeLabel, mtxStatus.drive[0].tapeLabel );
   return 0;
}


#undef ENABLE_MAIN
#ifdef ENABLE_MAIN

  /****************************************************************
   *  The following program exercises the functions in this file  *
   ****************************************************************/
int main( int argc, char *argv[] )
{
   int  i;
   int  rc;
   char changer[] = "/dev/changer";

   rc = LoadNextTape( &changer[0] );
   if ( rc == -1 )
   {
      printf( "LoadNextTape() error: Can't get mtx status.\n" );
      return -1;
   }
   if ( rc == -2 )
   {
      printf( "LoadNextTape() error: No tape in tape drive.\n" );
      return -1;
   }
   if ( rc == -3 )
   {
      printf( "LoadNextTape() error: No empty slots in autoloader.\n" );
      return -1;
   }
   if ( rc == -4 )
   {
      printf( "LoadNextTape() error: No more tapes available in autoloader.\n" );
      return -1;
   }
   if ( rc == -5 )
   {
      printf( "LoadNextTape() error: mtx error on tape load or unload.\n" );
      return -1;
   }
   printf( "LoadNextTape() succeeded.\n" );

/* rc = GetMtxStatus( &changer[0], &mtxStatus );
   if ( rc < 0 )
   {
      printf( "GetMtxStatus() error: %d\n", rc );
      return -1;
   }

   putchar( '\n' );
   for ( i = 0; i < mtxStatus.ndrive; i++ )
      printf( "Drive %d status: %s\n", i, mtxStatus.drive[i].status );
   putchar( '\n' );
   for ( i = 0; i < mtxStatus.nslot; i++ )
   {
      printf( "Slot %d status: %s", i, mtxStatus.slot[i].status );
      if ( strcmp(mtxStatus.slot[i].status, "Full") == 0 )
         printf( "  Label: %s", mtxStatus.slot[i].tapeLabel );
      putchar( '\n' );
   } */

   return 0;
}
#endif
