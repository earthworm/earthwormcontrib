#
#                    Make file for write_seg2
#                        Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib


WRITETAPE = write_seg2.o ddtrace.o tapeio.o \
        $L/logit.o $L/kom.o $L/getutil.o $L/sleep_ew.o \
        $L/time_ew.o $L/transport.o $L/dirops_ew.o

write_seg2: $(WRITETAPE)
	cc -o $B/write_seg2 $(WRITETAPE) -lm -lposix4 -lc

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/write_seg2*

