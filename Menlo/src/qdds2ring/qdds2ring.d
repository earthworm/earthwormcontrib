#
# qdds2ring.d  -  Configuration file for the qdds2ring program
#
MyModName         MOD_QDDS2RING     # Module name for this instance of qdds2ring.
                                    #   The specified name must appear in the
                                    #   earthworm.d configuration file.
RingName          HYPO_RING         # Cubic, heartbeat, and error messages
                                    #   are sent to this ring.
LogFile           1                 # Set to 0 to disable logging to disk.
HeartBeatInterval 15                # Interval between heartbeats sent to
                                    #   statmgr, in seconds.
#
InDir             /home/picker/QDDS/outputdir
                                    # qdds files are read from this directory.
#
SaveDir           /home/picker/QDDS/savedir
                                    # Acceptable QDDS event are saved here.

#Debug                              # Optional debug flag

#Network US                         # Optional network to accept.
                                    # May specify up to 20.
                                    # ALL accepted if none specified.
