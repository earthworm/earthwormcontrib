/* THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT.
 *
 *    $Id: qdds2ring.h 331 2007-04-25 16:18:32Z luetgert $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.3  2007/04/25 16:18:32  luetgert
 *     Increased maximum number of networks.
 *     .
 *
 *     Revision 1.2  2007/01/16 22:01:40  dietz
 *     got rid of Windows-esque end-of-lines
 *
 */

/*   getqdds.h    */
 
#define ERR_LINETOOBIG     0  /* Line in qdds file is too big for buffer */
#define ERR_QDDSDECODE     1  /* Error decoding qdds event line */
#define ERR_EVENTIDREADERR 2  /* Can't read event id from file */
#define ERR_EVENTIDWRITERR 3  /* Can't write event id to file */
#define ERR_PUTMSG         4  /* tport_putmsg() error */

#define MAXC             100  /* Size of buffer to contain line from qdds file */
#define MAXN              30  /* Maximum number of networks to consider  */
#define TRIGBUFSIZE     4000  /* Size of triglist message buffer */

