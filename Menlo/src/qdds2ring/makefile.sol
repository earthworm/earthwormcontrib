#
#                     Make file for qdds2ring
#                         Solaris Version
#
#  The posix4 library is required for nanaosleep.
#
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = qdds2ring.o \
    $L/sleep_ew.o $L/logit.o $L/time_ew.o $L/dirops_ew.o $L/kom.o \
    $L/getutil.o $L/transport.o

qdds2ring: $O
	cc -o $B/qdds2ring $O -lm -lposix4

# THE END
