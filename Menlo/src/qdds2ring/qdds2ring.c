/* THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT.
 *
 *    $Id: qdds2ring.c 331 2007-04-25 16:18:32Z luetgert $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.4  2007/04/25 16:18:32  luetgert
 *     Increased maximum number of networks.
 *     .
 *
 *     Revision 1.3  2007/01/16 22:01:40  dietz
 *     got rid of Windows-esque end-of-lines
 *
 */

  /*********************************************************************
   *                              qdds2ring                            *
   *                                                                   *
   *  This program grabs a CUBE format file from qdds and transfers    *
   *  it to the ring.  Modeled after Will Kohler's getqdds module.     *
   *                                                                   *
   *                                     Jim Luetgert 07/31/2000       *
   *********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include <math.h>
#include <time_ew.h>
#include <kom.h>
#include "qdds2ring.h"

/* For getpid */
#ifdef _SOLARIS
#include <unistd.h>
#endif
#ifdef _WINNT
#include <process.h>
#endif

/* Function declarations
   *********************/
void SendStatus( unsigned char, short, char * );
int  MsgConvert( char *, char *, int );
void config_me( char * );
void Lookup( void );
void LogConfig( void );

/* Global variables
   ****************/
static SHM_INFO region;       /* Shared memory region to use for i/o   */
static pid_t    myPid;        /* Process id of this process            */
static int      Debug;        /* debug flag                            */

/* Read these from the configuration file
   **************************************/
int   LogFile;                /* Flag value, 0 - 1                     */
char  MyModName[20];          /* Speak as this module name/id          */
char  RingName[20];           /* Name of transport ring for i/o        */
long  HeartBeatInterval;      /* Seconds between heartbeats            */
char  InDir[80];              /* Directory containing files to be sent */
char  SaveDir[80];            /* Save acceptable QDDS events here      */
char  Netcode[MAXN][20];      /* Network to watch for messages         */
int   Nnets;                  /* Number of networks to accept          */

/* Look these up in the earthworm.h tables with getutil functions
   **************************************************************/
long          RingKey;        /* Key of transport ring for i/o         */
unsigned char InstId;         /* Local installation id                 */
unsigned char MyModId;        /* Module Id for this program            */
unsigned char TypeHeartBeat;
unsigned char TypeError;
unsigned char TypeCubic;

#define NCOMMAND 6

int main( int argc, char *argv[] )
{
	long timeNow;              /* Current time                 */
	long timeLastBeat;         /* Time last heartbeat was sent */
	char text[100];            /* To contain status messages   */
	int  j;
	int  rc;
	int  event_id;

	char defaultConfig[] = "qdds2ring.d";
	char *configFileName = (argc > 1 ) ? argv[1] : &defaultConfig[0];

/* Read the configuration file
   ***************************/
	config_me( configFileName );
     
/* Look up important info in the earthworm.h tables
   ************************************************/
	Lookup();

/* Set up logging
   **************/
	logit_init( argv[0], (short)MyModId, TRIGBUFSIZE, LogFile );

/* Get my own pid for restart purposes
   ***********************************/
#ifdef _SOLARIS
	myPid = getpid();
	if ( myPid == -1 ) {
		logit( "e", "qdds2ring: Can't get my pid. Exiting.\n" );
		return -1;
	}
#endif

#ifdef _WINNT
	myPid = _getpid();
#endif

/* Log the configuration file parameters
   *************************************/
	LogConfig();

/* Attach to Input/Output shared memory ring
   *****************************************/
	tport_attach( &region, RingKey );

/* Change working directory to "InDir"
   **********************************/
	if ( chdir_ew( InDir ) == -1 ) {
		logit( "e", "Error. Can't change working directory to %s\n Exiting.",
		   InDir );
		return -1;
	}
	if(Debug) {
		if(Nnets==0) logit( "e", "qdds2ring: Accept events any network.\n" );
		for(j=0;j<Nnets;j++) logit( "e", "qdds2ring: Accept events from network: %s.\n", Netcode[j] );
	}

/* Force a heartbeat to be issued in first pass thru main loop
   ***********************************************************/
	timeLastBeat = time(&timeNow) - HeartBeatInterval - 1;

/* Get the name of a file from qdds.
   If rc==1, the InDir directory is empty.
   **************************************/
	while ( 1 ) {
		int      rc;
		int      i;
		int      reject;
		int      linelen;
		int      nullfound = 0;
		FILE     *fp;
		char     fname[100];
		char     line[MAXC];
		char     command[200];
		MSG_LOGO logo;

/* Beat the heart
   **************/
		if  ( (time(&timeNow) - timeLastBeat) >= HeartBeatInterval ) {  
			timeLastBeat = timeNow;
			SendStatus( TypeHeartBeat, 0, "" );
		}  
 
/* See if termination has been requested
   *************************************/
		if ( tport_getflag( &region ) == TERMINATE )
			break;

/* Get the name of a qdds file to process
   **************************************/
		rc = GetFileName( fname );

		if ( rc == 1 ) {      /* Sleep a while and try again */
			sleep_ew( 500 );
			continue;
		}

/* Open the file for reading only
   ******************************/
		fp = fopen( fname, "rb" );
		if ( fp == NULL ) {
			sleep_ew( 1000 );
			continue;
		}

/* Get a line from the file and strip off the newline character
   ************************************************************/
		line[0] = '\0';
		fgets( line, MAXC, fp );
		fclose( fp );

		for ( i = 0; i < MAXC; i++ ) {
			if ( line[i] == '\0' ) {
				nullfound = 1;
				if ( line[i-1] == '\n' ) {
					line[i-1] = '\0';
					break;
				}
			}
		}

/* The line is too big for our buffer. Pitch the file.
   **************************************************/
		if ( !nullfound ) {
			sprintf( text, "Line in qdds file %s is too long (> %d chars)\n",
			    fname, MAXC-1 );
			SendStatus( TypeError, ERR_LINETOOBIG, text );
			if ( remove( fname ) == -1 )
				logit( "et", "Error erasing file %s\n", fname );
			continue;
		}

/* The line is too short to be of interest.  Pitch the file.
   ********************************************************/
		linelen = i;
		if ( linelen < 28 ) {
			if ( remove( fname ) == -1 )
				logit( "et", "Error erasing file %s\n", fname );
			continue;
		}

/* The file is not an event file.  Pitch it.
   ****************************************/
		if ( strncmp( line, "E ", 2 ) != 0 ) {
			if ( remove( fname ) == -1 )
				logit( "et", "Error erasing file %s\n", fname );
			continue;
		}

/* The file event file is not from requested net.  Pitch it.
   *********************************************************/
		reject = 0;
		if ( strncmp( Netcode[0], "*", 1 ) != 0 ) {
			reject = 1;
			for(i=0;i<Nnets;i++) {
				if ( strncmp( line+10, Netcode[i], 2 ) == 0 ) {
					reject = 0;
				}
			}
		}
		if(reject) {
			if(Debug) {
				logit( "", "\n" );
				logit( "et", "\nEvent file rejected: %s\n%s\n", fname, line );
			}
			if ( remove( fname ) == -1 )
						logit( "et", "Error erasing file %s\n", fname );
			continue;
		}

/* This is a requested event. Log it.
   **********************************/
		logit( "", "\n" );
		logit( "et", "Event file found: %s\n", fname );
		logit( "", "%s\n", line );

/* Send the cubic message to the transport ring
   **********************************************/
		logo.instid = InstId;
		logo.mod    = MyModId;
		logo.type   = TypeCubic;

		if ( tport_putmsg( &region, &logo, strlen(line), line ) != PUT_OK ) {
			sprintf( text, "Error sending cubic message to transport ring.\n" );
			SendStatus( TypeError, ERR_PUTMSG, text );
		}
		if(Debug)
		    logit( "et", "qdds2ring: Cubic msg sent to transport ring \n %s \n", line );

/* Save the file for possible later use
   ************************************/
#ifdef _SOLARIS
		sprintf( command, "/usr/bin/cp -f %s %s", fname, SaveDir );
#endif
#ifdef _WINNT
		sprintf( command, "copy %s %s", fname, SaveDir );
#endif
		if ( system( command ) == -1 ) {
			logit( "et", "Error copying file %s to %s\n", fname, SaveDir );
		/*  sleep_ew( 300000 );  */
			sleep_ew(  10000 );
		}
		if(Debug)
		    logit( "et", "qdds2ring: Executing copy command: %s \n", command );

		if ( remove( fname ) == -1 ) {
			logit( "et", "Error erasing file %s\n", fname );
		/*  sleep_ew( 300000 );  */
			sleep_ew(  10000 );
		}
	}
	return 0;
}


/*************************************************************************
 * SendStatus() builds a heartbeat or error message & puts it into       *
 *              shared memory.  Writes errors to log file & screen.      *
 *************************************************************************/

void SendStatus( unsigned char type, short ierr, char *note )
{
	MSG_LOGO logo;
	char     msg[256];
	long     size;
	long     t;

/* Build the message
   *****************/
	logo.instid = InstId;
	logo.mod    = MyModId;
	logo.type   = type;

	time( &t );

	if ( type == TypeHeartBeat )
		sprintf( msg, "%ld %d\n", t, myPid );
	else if ( type == TypeError ) {
		sprintf( msg, "%ld %hd %s\n", t, ierr, note );
		logit( "et", "qdds2ring: %s\n", note );
	}
	else return;

	size = strlen( msg );

/* Write the message to shared memory
   **********************************/
	if ( tport_putmsg( &region, &logo, size, msg ) != PUT_OK ) {
		if ( type == TypeHeartBeat )
			logit( "et", "qdds2ring: Error sending heartbeat.\n" );
		if ( type == TypeError )
			logit( "et", "qdds2ring: Error sending error:%d.\n", ierr );
	}
	return;
}


/****************************************************************************
 *      config_me() process command file using kom.c functions            *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void config_me( char *configfile )
{
	int      ncommand = NCOMMAND;    /* Process this many required commands */
	char     init[NCOMMAND];         /* Init flags, one for each command */
	int      nmiss;                  /* Number of missing commands */
	char     *com;
	char     *str;
	int      nfiles;
	int      success;
	int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
	for ( i = 0; i < ncommand; i++ ) init[i] = 0;
    Debug = 0;
   strcpy( Netcode[0], "*" );
   Nnets = 0;
    
/* Open the main configuration file
   ********************************/
	nfiles = k_open( configfile );
	if ( nfiles == 0 ) {
		printf( "qdds2ring: Error opening command file <%s>. Exiting.\n", configfile );
		exit( -1 );
	}
 
/* Process all command files
   *************************/
	while ( nfiles > 0 ) {           /* While there are command files open */
		while ( k_rd() ) {          /* Read next line from active file  */
			com = k_str();         /* Get the first token from line */
 
/* Ignore blank lines & comments
   *****************************/
			if ( !com )           continue;
			if ( com[0] == '#' )  continue;
 
/* Open a nested configuration file
   ********************************/
			if( com[0] == '@' ) {
				success = nfiles + 1;
				nfiles  = k_open( &com[1] );
				if ( nfiles != success ) {
					printf( "qdds2ring: Error opening command file <%s>. Exiting.\n", &com[1] );
					exit( -1 );
				}
				continue;
			}   
 
/* Process anything else as a command
   **********************************/
			if ( k_its("LogFile") ) {
				LogFile = k_int();
				init[0] = 1;
			}
			else if ( k_its("MyModName") ) {
				str = k_str();
				if (str) strcpy( MyModName, str );
				init[1] = 1;
			}
			else if ( k_its("RingName") ) {
				str = k_str();
				if (str) strcpy( RingName, str );
				init[2] = 1;
			}
			else if ( k_its("HeartBeatInterval") ) {
				HeartBeatInterval = k_long();
				init[3] = 1;
			}
			else if ( k_its("InDir") ) {
				str = k_str();
				if (str) strcpy( (char *)InDir, str );
				init[4] = 1;
			}
			else if ( k_its("SaveDir") ) {
				str = k_str();
				if (str) strcpy( (char *)SaveDir, str );
				init[5] = 1;
			}
 

            else if( k_its("Debug") ) {  /*optional command*/
                Debug = 1;
            }

  /*optional*/
			else if( k_its("Network") ) {
                str = k_str();
                if(Nnets<MAXN) {
					if(str) strcpy( Netcode[Nnets++], str );
                }
            }


/* Unknown command
   ***************/
			else {
				printf( "qdds2ring: <%s> Unknown command in <%s>.\n", com, configfile );
				continue;
			}
 
/* See if there were any errors processing the command
   ***************************************************/
			if ( k_err() ) {
				printf( "qdds2ring: Bad <%s> command in <%s>. Exiting.\n", com, configfile );
				exit( -1 );
			}
		}
		nfiles = k_close();
	}
 
/* After all files are closed, check init flags for missed commands
   ****************************************************************/
	nmiss = 0;
	for ( i = 0; i < ncommand; i++ )
		if ( !init[i] ) nmiss++;

	if ( nmiss ) {
		printf( "qdds2ring: ERROR, no " );
		if ( !init[0]  ) printf( "<LogFile> "           );
		if ( !init[1]  ) printf( "<MyModName> "         );
		if ( !init[2]  ) printf( "<RingName> "          );
		if ( !init[3]  ) printf( "<HeartBeatInterval> " );
		if ( !init[4]  ) printf( "<InDir> "             );
		if ( !init[5]  ) printf( "<SaveDir> "           );
		printf( "command(s) in <%s>. Exiting.\n", configfile );
		exit( -1 );
	}
	return;
}
 
 
     /*****************************************************************
      *  Lookup()   Look up important info from earthworm.h tables    *
      *****************************************************************/
 
void Lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
	if ( ( RingKey = GetKey(RingName) ) == -1 ) {
		printf( "qdds2ring:  Invalid ring name <%s>. Exiting.\n", RingName);
		exit( -1 );
	}
 
/* Look up installations of interest
   *********************************/
	if ( GetLocalInst( &InstId ) != 0 ) {
		printf( "qdds2ring: Error getting local installation id. Exiting.\n" );
		exit( -1 );
	}
 
/* Look up modules of interest
   ***************************/
	if ( GetModId( MyModName, &MyModId ) != 0 ) {
		printf( "qdds2ring: Invalid module name <%s>. Exiting.\n", MyModName );
		exit( -1 );
	}
 
/* Look up message types of interest
   *********************************/
	if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
		printf( "qdds2ring: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );      exit( -1 );
	}
	if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
		printf( "qdds2ring: Invalid message type <TYPE_ERROR>. Exiting.\n" );
		exit( -1 );
	}
	if ( GetType( "TYPE_CUBIC", &TypeCubic ) != 0 ) {
		printf( "qdds2ring: Invalid message type <TYPE_TRIGLIST2K>. Exiting.\n" );
		exit( -1 );
	}
}


     /******************************************************************
      *                           LogConfig()                          *
      *                                                                *
      *         Log the configuration file parameters.                 *
      ******************************************************************/

void LogConfig( void )
{
	int i;

	logit( "", "MyModName:         %s\n", MyModName );
	logit( "", "MyModId:           %u\n", MyModId );
	logit( "", "LogFile:           %d\n", LogFile );
	logit( "", "RingName:          %s\n", RingName );
	logit( "", "HeartBeatInterval: %d\n", HeartBeatInterval );
	logit( "", "InDir:             %s\n", InDir );
	logit( "", "SaveDir:           %s\n", SaveDir );

	return;
}

