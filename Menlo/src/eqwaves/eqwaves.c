/*************************************************************************
 *      eqwaves.c:
 * Takes arkive messages, buffers them in an in-memory fifo; 
 * pulls them out, and starts record section processing for each message.
 *
 * Main:
 * Starts the ProcessorMgr thread. Sets up the in-memory queue buffer. 
 * Loops reading hyosum messages, and stuffs them into the queue.
 * 
 * ProcessorMgr:
 * Pulls messages from the queue.  
 * It gets a pointer to some structure, containing the arkive record.
 * It calls processing routines to do all the seismology.
 * Jim Luetgert 12/17/97
**************************************************************************/
 
#include <platform.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include <decode.h>
#include <kom.h>
#include <transport.h>
#include "mem_circ_queue.h" 
#include <ws_clientII.h>
#include "gd.h"

#include "eqwaves.h" 

/* Functions in this source file
 *******************************/
thr_ret Heartbeat( void * );
thr_ret ProcessorMgr( void*);
void config_me(Global *But,  char * ); /* reads configuration (.d) file via Carl's routines  */
void ewmod_status( unsigned char, short, char *); /* sends heartbeats and errors into ring   */
void lookup_ew ( void );     /* Goes from symbolic names to numeric values, via earthworm.h  */

/* Shared memory
 ***************/
static  SHM_INFO  InRegion;              /* public shared memory for receiving arkive messages */

/* Things to lookup in the earthworm.h table with getutil.c functions
 ********************************************************************/
static long          InRingKey;         /* key of transport ring for input   */
static unsigned char InstId;            /* local installation id             */
static unsigned char MyModId;           /* our module id                     */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;

#define  MAXBUFSIZE   100000
/* Things to read from configuration file
 ****************************************/
static char InRingName[20];             /* name of transport ring for i/o          */
static char OutRingName[20];            /* name of transport ring for output       */
static char MyModuleId[20];             /* module id for this module               */
static int  LogSwitch;                  /* 0 if no logging should be done to disk  */
//static long MaxMsgSize = DB_MAX_BYTES_PER_EQ; /* max size for input/output msgs       */
static long MaxMsgSize = MAXBUFSIZE;    /* max size for input/output msgs       */
static int  MaxMessages;                /* max messages in output circular buffer  */
static time_t HeartBeatInterval = 5;    /* seconds between heartbeats              */
static int  Debugger;                   /* debug flag                              */
static int  Flush;                      /* Flush flag                              */
static int  howlong = -1;
Global      Btlr;                       /* Private area for the threads            */
Arkive      Ark;

#define  MAXLOGO   2
static MSG_LOGO  GetLogo[MAXLOGO];      /* array for requesting module,type,instid */
short     nLogo;                        /* number of logos read from ring          */

pid_t MyPid;              /* Our own pid, sent with heartbeat for restart purposes */

/* Variables for talking to statmgr
 **********************************/
char      Text[150];

/* Error words used by eqwaves
 *****************************/
#define   ERR_MISSMSG       0
#define   ERR_TOOBIG        1
#define   ERR_NOTRACK       2
#define   ERR_INTERNAL      3
#define   ERR_QUEUE         4

/* The in-memory fifo buffer 
****************************/
QUEUE OutQueue;               /* from queue.h, queue.c; sets up linked list via malloc and free */
static long num_in_Q;         /* Number of msgs currently in the queue */ 
//static char OutMsg[DB_MAX_BYTES_PER_EQ]; 
//static char ArcMsgBufA[DB_MAX_BYTES_PER_EQ]; /* character string to hold event message */ 
//                                          /* DB_MAX_BYTES_PER_EQ is from earthworm.h */
static char OutMsg[MAXBUFSIZE]; 
static char ArcMsgBufA[MAXBUFSIZE]; /* character string to hold event message */ 
                                          
/* Server thread stuff
**********************/
static unsigned int tidProcessorMgr;              /* id of dispatcher of socket-serving threads */
#define THREAD_STACK  8096
#define THREAD_STACK_SIZE 8192

int MainProcess(Global *, Arkive *Ark);
void ProcessArcFile( void);

/* Other globals
 ***************/
char   progname[] = "eqwaves";

time_t MyLastInternalBeat;      /* time of last heartbeat into the local Earthworm ring    */
unsigned  TidHeart;             /* thread id. was type thread_t on Solaris! */

/**********************************************************************************
 *  main( int argc, char **argv )                                                 *
 **********************************************************************************/

int main( int argc, char **argv )
{
    char        eventid[20], fname[100], string[500], subname[] = "Main";
    int         i, j, resA, waitA, ret;
    long        recsizeA;   /* size of retrieved message */
    MSG_LOGO    reclogoA;   /* logo of retrieved message */
    float       mag;
    FILE        *fp;

        /* Check command line arguments
         ******************************/
    if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", progname );
        exit( 0 );
    }
    Debugger = num_in_Q = 0;
    Flush = 1;

    /* Get our own Pid for restart purposes
    ***************************************/
    MyPid = getpid();
    if( MyPid == -1 ) {
      fprintf( stderr,"%s: Cannot get pid. Exiting.\n", progname);
      return -1;
    }

        /* Read the configuration file(s)
         ********************************/
    config_me(&Btlr, argv[1] );

        /* Look up important info from earthworm.h tables
         ************************************************/
    lookup_ew();

        /* Initialize name of log-file & open it
         ***************************************/
    logit_init( argv[1], (short) MyModId, 512, LogSwitch );
    logit( "" , "%s: %s: Read command file <%s>\n",  progname, subname, argv[1] );
    
        /* DEBUG: dump variables to stdout
        **********************************/
    if(LogSwitch) {
        logit("","%s: %s: MyModuleId:  %s \n",   progname, subname, MyModuleId);
        logit("","%s: %s: InRingName:  %s \n",   progname, subname, InRingName);
        logit("","%s: %s: GetLogo: %u %u %u\n",  progname, subname, GetLogo[0].instid, GetLogo[0].mod, GetLogo[0].type);
        logit("","%s: %s: MaxMsgSizeo: %d \n",   progname, subname, MaxMsgSize);
        logit("","%s: %s: MaxMessages: %d \n",   progname, subname, MaxMessages);
    }

        /* Create a Mutex to control access to queue
        ********************************************/
    CreateMutex_ew();
    if(Debugger) logit("","%s: %s: Created Mutex\n",   progname, subname);

        /* Allocate space for  messages
        *********************************/
    if(LogSwitch) {
        logit("","%s: %s: ArcMsgSize: %d \n",  progname, subname, sizeof(ArcMsgBufA));
        logit("","%s: %s: OutMsgSize: %d \n",  progname, subname, sizeof(OutMsg));
    }

        /* Initialize the message queue
        *******************************/
    initqueue ( &OutQueue, (unsigned long)MaxMessages, (unsigned long)MaxMsgSize);   

        /* Start the Dispatcher thread which pulls 
         * messages from fifo, and starts processing threads
         ***************************************************/
    if ( StartThread( ProcessorMgr, THREAD_STACK, &tidProcessorMgr ) == -1 ) {
        logit( "e", "%s: %s: Error starting ProcessorMgr thread. Exiting.\n",  progname, subname );
        return( -1 );
    }
    logit("","%s: %s: ProcessorMgr started.\n",  progname, subname);

        /* Attach to public HYPO shared memory ring
         ******************************************/
    tport_attach( &InRegion, InRingKey );
    if (LogSwitch) logit( "", "%s: %s: Attached to public memory region <%s>: %ld.\n", 
                              progname, subname, InRingName, InRegion.key );
#ifdef _SOLARIS                    /* SOLARIS ONLY:                         */
    if (Debugger) logit( "e", "%s: %s: Attached to public memory region <%s> key: %ld mid: %ld sid: %ld.\n", 
                              progname, subname, InRingName, InRegion.key, InRegion.mid, InRegion.sid );
    if (Debugger) logit( "e", "%s: %s: nbytes: %ld keymax: %ld keyin: %ld keyold: %ld flag: %d.\n", 
                              progname, subname, InRegion.addr->nbytes, 
                              InRegion.addr->keymax, InRegion.addr->keyin, 
                              InRegion.addr->keyold, InRegion.addr->flag );
#endif                             /*                                       */

        /* ------------- See if there is an arc file to process --------------*/
    if  ( Btlr.arcfileflg ) {
    /* Change to the directory with the input files
     ***********************************************/
        if( chdir_ew( Btlr.indir ) == -1 ) {
            logit( "e", "%s: %s: GetFromDir directory <%s> not found; "
                     "exiting!\n", progname, subname, Btlr.indir );
            exit(-1);
        }
        if(Debugger)logit("et","%s: %s: changed to directory <%s>\n", progname, subname,Btlr.indir);
    }

   /* Start the heartbeat thread
   ****************************/
    time(&MyLastInternalBeat); /* initialize our last heartbeat time */
                          
    if ( StartThread( Heartbeat, (unsigned)THREAD_STACK_SIZE, &TidHeart ) == -1 ) {
        logit( "et","%s: %s: Error starting Heartbeat thread. Exiting.\n", progname, subname );
        tport_detach( &InRegion );
        return -1;
    }

	/* sleep for 2 seconds to allow heart to beat so statmgr gets it.  */
	
	sleep_ew(2000);

        /* ------------------------ flush the ring -------------------------*/
        /* -------------- to prevent re-processing of old events -----------*/
        /* ------------------------ after a restart ------------------------*/
    if(Flush) {
        do {
            resA = tport_getmsg( &InRegion, GetLogo, nLogo, 
                                &reclogoA, &recsizeA, ArcMsgBufA, sizeof(ArcMsgBufA)-1 );
        
        } while(resA != GET_NONE);
    }

        /* ------------------------ start working loop -------------------------*/
    waitA = 0;
    while(1) {
        do {    /* ------ start of message processing loop: take all messages, 
                            'till transport says "no more" ---*/
                       
        /* -------------- see if a termination has been requested ----------*/
            if ( tport_getflag( &InRegion ) == TERMINATE ||
                 tport_getflag( &InRegion ) == MyPid ) {      /* detach from shared memory regions*/
                sleep_ew( 500 );       /* wait around while butlers finish */
                tport_detach( &InRegion );
                logit("et", "%s: %s: Termination requested; exiting.\n",  progname, subname );
                fflush(stdout);
                exit( 0 );
            }

                        /* Get and process the next arkive message from shared memory */
                        /***************************************************************/
            resA = tport_getmsg( &InRegion, GetLogo, nLogo, 
                                &reclogoA, &recsizeA, ArcMsgBufA, sizeof(ArcMsgBufA)-1 );
            switch(resA) {
                int ret;

                case GET_NONE:          /* no more new messages     */
                    if (LogSwitch && waitA>=howlong && howlong>0) {
                       logit("et","%s: %s: No new messages from %s in %ld passes.\n",  
                                  progname, subname, InRingName, waitA);
                       waitA = 0;
                    }
                    waitA += 1;
                    break;

                case GET_TOOBIG:   /* next message was too big. Complain and try again */
                    sprintf( Text, "Retrieved msg[%ld] (i%u m%u t%u) too big for ArcMsgBufA[%d]",
                             recsizeA, reclogoA.instid, reclogoA.mod, reclogoA.type, sizeof(ArcMsgBufA)-1 );
                    ewmod_status( TypeError, ERR_TOOBIG, Text );
                    break;

                case GET_MISS:     /* got a msg, but missed some */
                    sprintf( Text,"Missed msg(s)  i%u m%u t%u  region:%ld.",
                            reclogoA.instid, reclogoA.mod, reclogoA.type, InRegion.key);
                    ewmod_status( TypeError, ERR_MISSMSG, Text );
                    goto PutInQueue;

                case GET_NOTRACK: /* got a msg, but can't tell if any were missed  */
                    sprintf( Text,"Msg received (i%u m%u t%u); transport.h NTRACK_GET exceeded",
                            reclogoA.instid, reclogoA.mod, reclogoA.type );
                    ewmod_status( TypeError, ERR_NOTRACK, Text );
                    goto PutInQueue;

                case GET_OK:
                /* Stuff the message into the fifo
                **********************************/
  PutInQueue:              
                    waitA = 0;
                        if(Debugger) {
                             logit("et", "%s: %s: Retrieved msg[%ld] (i%u m%u t%u)\n      ArcMsgBufA: %.200s\n",
                                progname, subname, recsizeA, reclogoA.instid, reclogoA.mod, reclogoA.type, ArcMsgBufA );
                        }
                        mag = (float)((DECODE( ArcMsgBufA+67, 2, atof ))/10.0);
                        mag = (float)((DECODE( ArcMsgBufA+70, 2, atof ))/10.0);
    
                        strcpy(eventid, "No_ID");
                        for(i=136,j=0;i<146;i++) if(ArcMsgBufA[i]!=' ') {j = i;break;}
                        if(j) strncpy(eventid, &ArcMsgBufA[j], 146-j);
                        eventid[146-j] = 0;
                        if(mag < Btlr.MinSize) {
                            if(Btlr.Debug) logit("e", "%s: %s: Event %s too small (%5.2f)\n", progname, subname, eventid, mag);
                            continue;
                        }
    
                        if((num_in_Q < 20 || mag > 2.5) &&
                           (num_in_Q < 10 || mag > 2.0)) {
                            if(num_in_Q <  5 || mag > 1.0) {
                                RequestMutex();
                                ret=enqueue( &OutQueue, ArcMsgBufA, recsizeA, reclogoA ); /* put it into the queue */
                                if(Debugger > 1) 
                                    logit("et","%s: %s: Retrieved and Queued message.\n",  progname, subname); /*debug */
                                num_in_Q += 1;
                                ReleaseMutex_ew();
                                if ( ret!= 0 ) {       
                                    if (ret==-2) { /* Serious: quit */
                                        sprintf(Text,"internal queue error %d. Terminating/n", ret);
                                                    ewmod_status( TypeError, ERR_QUEUE, Text );
                                        exit(-1);
                                    }
                                    if (ret==-1) sprintf(Text,"queue cant allocate memory. Lost message\n");
                                    if (ret==-3) sprintf(Text,"Circular queue lapped. Message lost\n");
                                    ewmod_status( TypeError, ERR_QUEUE, Text );
                                    continue;
                                }
                            }
                        }
                    
                    break;
            }
        } while (resA !=GET_NONE );      /* ------- end of message processing loop for A ------------- */

        sleep_ew( 500 );       /* wait around for more summary lines   */

    }
}



/***************************** Heartbeat **************************
 *           Send a heartbeat to the transport ring buffer        *
 ******************************************************************/

thr_ret Heartbeat( void *dummy )
{
    time_t now;

   /* once a second, do the rounds.  */
    while ( 1 ) {
        sleep_ew(1000);
        time(&now);

        /* Beat our heart (into the local Earthworm) if it's time
        ********************************************************/
        if (difftime(now,MyLastInternalBeat) > (double)HeartBeatInterval) {
            ewmod_status( TypeHeartBeat, 0, "" );
            time(&MyLastInternalBeat);
        }
    }
}


/************************** ProcessorMgr Thread *********************************
 * Waits for for a hypo message to appear in the fifo buffer. When it           *
 * does, starts a processing thread, gives it a pointer to station list         *
 * and hypo message, and wishes them luck. Keeps track of active threads,       * 
 * and does not start too many threads.                                         *
 ********************************************************************************/

thr_ret OldProcessorMgr( void *dummy )
{
    char        subname[] = "ProcessorMgr";
    long        ret, recsize, num_processed;
    MSG_LOGO    reclogo;                /* logo of retrieved message  */

    if (LogSwitch) logit("", "%s: %s: Beginning ProcessorMgr.\n",  progname, subname);

    num_processed = 0;      
    while (1) {   /* Pull a message from the queue *
                   *********************************/
        RequestMutex();
        ret=dequeue( &OutQueue, OutMsg, &recsize, &reclogo);
        ReleaseMutex_ew();
        if(ret == 0 && Debugger > 1) { /*  0 means got a msg */
            printf("%s: %s: Processor Thread Retrieved msg[%ld] (i%u m%u t%u) OutMsg: %.100s\n",
                     progname, subname, recsize, reclogo.instid, reclogo.mod, reclogo.type, OutMsg );
        }
        if(ret < 0 ) {  /* -1 means empty queue sleep a while */
            sleep_ew(100);
            continue;
        }
        num_in_Q -= 1;
        if( recsize >= MaxMsgSize) {
            logit("et","%s: %s: Fatal error. Retrieved message is too long: %.200s\n",  progname, subname, OutMsg);
            recsize = MaxMsgSize;
        }
        OutMsg[recsize] = 0;        /* Make sure the message is null terminated */
   
           /* Start the Processing *
            ************************/
        sleep_ew(Btlr.SecsToWait*1000);
        if(num_in_Q>0 && LogSwitch) {
            logit("e","%s: %s: Starting Processing. There are %ld Msgs in Queue. \n",  progname, subname,num_in_Q);
        }
        
        strcpy( Ark.ArkivMsg, OutMsg );
        MainProcess(&Btlr, &Ark);  /*  Insert processing code here */
    }    
}

/************************** ProcessorMgr Thread *********************************
 * Waits for for a hypo message to appear in the fifo buffer. When it           *
 * does, starts a processing thread, gives it a pointer to station list         *
 * and hypo message, and wishes them luck. Keeps track of active threads,       * 
 * and does not start too many threads.                                         *
 ********************************************************************************/

thr_ret ProcessorMgr( void *dummy )
{
    char        fname[100], string[500], subname[] = "ProcessorMgr";
    long        ret, recsize, num_processed;
    MSG_LOGO    reclogo;                /* logo of retrieved message  */
    FILE        *fp;

    if (LogSwitch) logit("", "%s: %s: Beginning ProcessorMgr.\n",  progname, subname);

    if  ( Btlr.arcfileflg ) {
    /* Change to the directory with the input files
     ***********************************************/
        if( chdir_ew( Btlr.indir ) == -1 ) {
            logit( "e", "%s: %s: GetFromDir directory <%s> not found; "
                     "exiting!\n", progname, subname, Btlr.indir );
            exit(-1);
        }
        if(Debugger)logit("et","%s: %s: changed to directory <%s>\n", progname, subname,Btlr.indir);
    }

    num_processed = 0;      
    while (1) {   /* Pull a message from the queue *
                   *********************************/
        RequestMutex();
        ret=dequeue( &OutQueue, OutMsg, &recsize, &reclogo);
        ReleaseMutex_ew();
        if(ret == 0 && Debugger > 1) { /*  0 means got a msg */
            printf("%s: %s: Processor Thread Retrieved msg[%ld] (i%u m%u t%u) OutMsg: %.100s\n",
                     progname, subname, recsize, reclogo.instid, reclogo.mod, reclogo.type, OutMsg );
        }
        if(ret < 0 ) {  /* -1 means empty queue sleep a while */
            sleep_ew(100);
            ProcessArcFile();
            continue;
        }
        num_in_Q -= 1;
        if( recsize >= MaxMsgSize) {
            logit("et","%s: %s: Fatal error. Retrieved message is too long: %.200s\n",  progname, subname, OutMsg);
            recsize = MaxMsgSize;
        }
        OutMsg[recsize] = 0;        /* Make sure the message is null terminated */
   
           /* Start the Processing *
            ************************/
        sleep_ew(Btlr.SecsToWait*1000);
        if(num_in_Q>0 && LogSwitch) {
            logit("e","%s: %s: Starting Processing. There are %ld Msgs in Queue. \n",  progname, subname,num_in_Q);
        }
        
        strcpy( Ark.ArkivMsg, OutMsg );
        MainProcess(&Btlr, &Ark);  /*  Insert processing code here */

        /* ------------- See if there is an arc file to process --------------*/
        ProcessArcFile();
    }    
}

/***************************** ProcessArcFile ***********************************
 * Checks the arc input directory for arc files.  While there are files, we     *
 * read them, pass them on for processing and delete them.                      *
 ********************************************************************************/
void ProcessArcFile( void)
{
    char        fname[100], string[500], subname[] = "ProcessArcFile";
    long        ret, recsize;
    FILE        *fp;

    /* ------------- See if there is an arc file to process --------------*/
    if  ( Btlr.arcfileflg ) {
        do {
            ret = GetFileName( fname );
        
            if( ret != 1 ) {  /* Files found; Process */
                if(Debugger)logit("et","%s: %s: got file name <%s>\n",progname, subname,fname);
                sleep_ew( 1*1000 );
                fp = fopen( fname, "rb" );
                if ( fp != NULL ) {
                    if(strstr(fname, ".arc") != 0) {
                        OutMsg[0] = 0L;
                        while(fgets(string, 480, fp)!=0L){
                            strcat(OutMsg, string);
                        }
                        recsize = strlen(OutMsg);
                        if(Debugger) {
                             printf("%s: %s: Retrieved msg[%ld] \n      ArcMsg: %.200s\n",
                                progname, subname, recsize, OutMsg );
                        }
        
                        strcpy( Ark.ArkivMsg, OutMsg );
                        MainProcess(&Btlr, &Ark);  /*  Insert processing code here */

                    }
                    fclose( fp );
                }
                if( remove( fname ) != 0 ) {
                    logit("et","%s: %s: error deleting file: %s\n", progname, subname, fname);
                } else  {
                    if(Debugger)logit("e","%s: %s: Removed %s \n \n", progname, subname, fname );
                }
            }
        } while (ret != 1);
    }
}

/****************************************************************************
 *      config_me() process command file using kom.c functions              *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void config_me(Global *But,   char* configfile )
{
    char    whoami[50], *com, *str;
    char    init[20];       /* init flags, one byte for each required command */
    int     ncommand;       /* # of required commands you expect */
    int     nmiss;          /* number of required commands that were missed   */
    int     i, j, n, nfiles, success;

    sprintf(whoami, "%s: %s: ", progname, "config_me");
        /* Set to zero one init flag for each required command
         *****************************************************/
    nLogo    = 0;
    ncommand = 15;
    for(i=0; i<ncommand; i++ )  init[i] = 0;
    
    But->LocalTime = -7;
    strcpy(But->LocalTimeID, "PST");
    But->Use_all_Sites = 0;
    But->MaxChannels = MAXCHANNELS;
    But->MaxList    = 100;
    But->MaxDays    =   7;
    But->RedSize    =   3.0;

    But->MinSize    =   0.0;
    But->MaxDist    = 100;
    But->wsTimeout  = WSTIMEOUT*1000;
    But->RetryCount =  10;
    But->Debug      = 0;
    But->WSDebug    = 0;
    But->ncomp      = 0;
    But->nStaDB     = 0;
    But->nServer    = 0;
    But->arcfileflg = 0;
    But->nltargets  = 0;
    But->nPlots     = 0;
    But->UseDST     = 0;
    But->SecsToWait = 0;
    But->Use_QDDS   = 0;
    
        /* Open the main configuration file
         **********************************/
    nfiles = k_open( configfile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, configfile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

                /* Process anything else as a command
                 ************************************/
  /*0*/
            if( k_its("LogSwitch") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/
            else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModuleId , str );
                init[1] = 1;
            }
  /*2*/
            else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( InRingName, str );
                init[2] = 1;
            }

  /*3*/
            else if( k_its("HeartBeatInt") ) {
                HeartBeatInterval = k_long();
                init[3] = 1;
            }

                /* Retrieve installation and module to get messages from  */

  /*4*/
            else if( k_its("GetSumFrom") ) {
                if ( nLogo >= MAXLOGO ) {
                    fprintf(stderr, "%s Too many <GetSumFrom> commands in <%s>", whoami, configfile );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAXLOGO );
                    exit( -1 );
                }
                if( ( str=k_str() ) ) {
                    if( GetInst( str, &GetLogo[nLogo].instid ) != 0 ) {
                        fprintf( stderr, "%s Invalid installation name <%s>", whoami, str );
                        fprintf( stderr, " in <GetSumFrom> cmd; exiting!\n" );
                        exit( -1 );
                    }
                }
                if( ( str=k_str() ) ) {
                    if( GetModId( str, &GetLogo[nLogo].mod ) != 0 ) {
                        fprintf( stderr, "%s Invalid module name <%s>", whoami, str );
                        fprintf( stderr, " in <GetSumFrom> cmd; exiting!\n" );
                        exit( -1 );
                    }
                }
                if( ( str=k_str() ) ) {
                    if( GetType( str, &GetLogo[nLogo].type ) != 0 ) {
                        fprintf( stderr, "%s Invalid message type <%s>", whoami, str );
                        fprintf( stderr, "; exiting!\n" );
                        exit( -1 );
                }
                    }
                nLogo++;
                init[4] = 1;
            }

                /* get memory fifo buffer parameters
                ************************************/
  /*5*/
            else if( k_its("MaxMsgSize") ) {
                MaxMsgSize = k_int();
                init[5] = 1;
            }

  /*6*/
            else if( k_its("MaxMessages") ) {
                MaxMessages = k_int();
                init[6] = 1;
            }

/*7*/
            else if( k_its("wsTimeout") ) {
                But->wsTimeout = k_int()*1000; 
                init[7] = 1;
            }

         /* wave server addresses and port numbers to get trace snippets from
          *******************************************************************/
/*8*/
            else if( k_its("WaveServer") ) {
                if ( But->nServer >= MAX_WAVESERVERS ) {
                    fprintf(stderr, "%s Too many <WaveServer> commands in <%s>", 
                             whoami, configfile );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAX_WAVESERVERS );
                    exit( -1 );
                }
                if( (long)(str=k_str()) != 0 )  strcpy(But->wsIp[But->nServer],str);
                if( (long)(str=k_str()) != 0 )  strcpy(But->wsPort[But->nServer],str);
                if( (long)(str=k_str()) != 0 )  strcpy(But->wsComment[But->nServer],str);
                But->nServer++;
                init[8]=1;
                init[9] = 1;
            }


        /* get Gif directory path/name
        *****************************/
/*10*/
            else if( k_its("GifDir") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf(stderr,"%s Fatal error. Gif directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    exit( -1 );
                }
                j = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[j-1] != '/' ) strcat(str, "/");
                if(str) strcpy( But->GifDir , str );
                init[10] = 1;
            }

          /* get plot parameters
        ************************************/
/*11*/
            else if( k_its("SetPlotParams") ) {
                if ( But->nPlots >= MAXPLOTS ) {
                    fprintf(stderr, "%s Too many <SetPlotParams> commands in <%s>", 
                             whoami, configfile );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAXPLOTS );
                    exit( -1 );
                }
                j = But->nPlots;
                But->plt[j].done = 0;
                But->plt[j].xsize = k_val();
                if(But->plt[j].xsize >= 100.0) {            /* Overall size of plot in pixels */
                    But->plt[j].xpix = But->plt[j].xsize;
                    But->plt[j].xsize = (But->plt[j].xpix - 4.0)/72.0;
                } else {
                    But->plt[j].xpix = 72.0*But->plt[j].xsize + 4;
                }
                
                But->plt[j].ysize = k_val();
                if(But->plt[j].ysize >= 100.0) {            /* Overall size of plot in pixels */
                    But->plt[j].ypix = But->plt[j].ysize;
                    But->plt[j].ysize = (But->plt[j].ypix - 8.0)/72.0;
                } else {
                    But->plt[j].ypix = 72.0*But->plt[j].ysize + 8;
                }
                
                But->plt[j].ntrace = k_int();            /* # of traces to display */
                if(But->plt[j].ntrace < 1 || But->plt[j].ntrace > 100) {
                    But->plt[j].ntrace = 15;
                }
                
                But->plt[j].SecPerScreenReq = k_val();    /* # of seconds to display */
                
                But->plt[j].pre_event = k_val();         /* # of seconds before event time to start trace */
                
                But->plt[j].Format = k_int();            /* format of plot */
                if(But->plt[j].Format < 1 || But->plt[j].Format > 5) {
                    But->plt[j].Format = 1;
                }
                
                But->plt[j].trace_label = k_int();        /* trace label? */
                if(But->plt[j].trace_label < 0 || But->plt[j].trace_label > 1) {
                    But->plt[j].trace_label = 1;
                }
                
                But->plt[j].plot_enable = k_int();        /* plot enabled? */
                if(But->plt[j].plot_enable < 0 || But->plt[j].plot_enable > 1) {
                    But->plt[j].plot_enable = 1;
                }
                
                But->nPlots++;
                init[11] = 1;
            }

          /* get the component types
        ****************************/
/*12*/
            else if( k_its("Component") ) {
                if ( But->ncomp >= MAX_COMPS ) {
                    fprintf(stderr, "%s Too many <Component> commands in <%s>", 
                             whoami, configfile );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAX_COMPS );
                    exit( -1 );
                }
                if( (long)(str=k_str()) != 0 )  {
                    strcpy(But->Comp[But->ncomp], str);
                }
                But->ncomp += 1;
                init[12] = 1;
            }


                /* get station list path/name
                *****************************/
/*13*/
            else if( k_its("StationList") ) {
                if ( But->nStaDB >= MAX_STADBS ) {
                    fprintf( stderr, "%s Too many <StationList> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_STADBS );
                    return;
                }
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->stationList[But->nStaDB] , str );
                But->nStaDB++;
                init[13] = 1;
            }


        /* get Prefix for deletable files on the target
        *****************************/
/*14*/
            else if( k_its("Prefix") ) {
                str = k_str();
                if( (int)strlen(str) >= PRFXSZ) {
                    fprintf( stderr, "%s Fatal error. File prefix %s greater than %d char.\n",
                        whoami, str, PRFXSZ);
                    exit( -1 );
                }
                if(str) strcpy(But->Prefix, str );
                init[14] = 1;
            }



               /* optional commands */


        /* get QDDS directory path/name
        *****************************/
/*xx*/
            else if( k_its("QDDSDir") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf(stderr,"%s Fatal error. QDDSDir directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    exit( -1 );
                }
                j = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[j-1] != '/' ) strcat(str, "/");
                if(str) strcpy( But->QDDSDir , str );
                But->Use_QDDS = 1;
            }


        /* get QDDS directory path/name
        *****************************/
/*xx*/
            else if( k_its("QDDSLink") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf(stderr,"%s Fatal error. QDDSLink directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    exit( -1 );
                }
                j = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[j-1] != '/' ) strcat(str, "/");
                if(str) strcpy( But->QDDSLink , str );
                But->Use_QDDS = 1;
            }


        /* get QDDS directory path/name
        *****************************/
/*xx*/
            else if( k_its("QDDSLogo") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf(stderr,"%s Fatal error. QDDSLogo directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    exit( -1 );
                }
                if(str) strcpy( But->QDDSLogo , str );
                But->Use_QDDS = 1;
            }


        /* get QDDS directory path/name
        *****************************/
/*xx*/
            else if( k_its("QDDSLabel") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf(stderr,"%s Fatal error. QDDSLabel directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    exit( -1 );
                }
                if(str) strcpy( But->QDDSLabel , str );
                But->Use_QDDS = 1;
            }


        /* get Arcfile input directory path/name
        ****************************************/
            else if( k_its("ArcDir") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf(stderr,"%s Fatal error. Arc directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    exit( -1 );
                }
                j = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[j-1] != '/' ) strcat(str, "/");
                if(str) strcpy( But->indir , str );
                But->arcfileflg = 1;
            }
            
        /* get the local target directory(s)
        ************************************/
            else if( k_its("LocalTarget") ) {
                if ( But->nltargets >= MAX_TARGETS ) {
                    logit("e", "%s Too many <LocalTarget> commands in <%s>", 
                             whoami, configfile );
                    logit("e", "; max=%d; exiting!\n", (int) MAX_TARGETS );
                    return;
                }
                if( (long)(str=k_str()) != 0 )  {
                    n = strlen(str);   /* Make sure directory name has proper ending! */
                    if( str[n-1] != '/' ) strcat(str, "/");
                    strcpy(But->loctarget[But->nltargets], str);
                }
                But->nltargets += 1;
            }

            else if( k_its("LocalTime") ) {
                But->LocalTime = k_int();
                str = k_str();
                strncpy(But->LocalTimeID, str, (size_t)3);
                But->LocalTimeID[3] = '\0';
            }

            else if( k_its("Use_all_Sites") )    But->Use_all_Sites = 1;
            else if( k_its("Use_picked_Sites") ) But->Use_all_Sites = 0;

            else if( k_its("MaxSite") ) {  /*optional command*/
                But->MaxChannels = k_int();
                if(But->MaxChannels>MAXCHANNELS) But->MaxChannels = MAXCHANNELS; /* Need to do this until all
                                                                          MaxChannel arrays are dynamic */
            }

            else if( k_its("MaxDist") ) {  /*optional command*/
                But->MaxDist = k_int();
                if(But->MaxDist>1000) But->MaxDist = 1000; 
                if(But->MaxDist<  10) But->MaxDist =   10; 
            }

            else if( k_its("MaxList") ) {  /*optional command*/
                But->MaxList = k_int();
                if(But->MaxList>1000) But->MaxList = 1000; 
                if(But->MaxList<  10) But->MaxList =   10; 
            }

            else if( k_its("MaxDays") ) {  /*optional command*/
                But->MaxDays = k_int();
                if(But->MaxDays>30) But->MaxDays = 30; 
                if(But->MaxDays< 1) But->MaxDays =  1; 
            }

            else if( k_its("RetryCount") ) {  /*optional command*/
                But->RetryCount = k_int();
                if(But->RetryCount>100) But->RetryCount = 100; 
                if(But->RetryCount<  1) But->RetryCount =   1; 
            }

            else if( k_its("RedSize") ) {  /*optional command*/
                But->RedSize = k_val();
                if(But->RedSize>10.0) But->RedSize = 10.0; 
                if(But->RedSize< 1.0) But->RedSize =  1.0; 
            }

            else if( k_its("MinSize") ) {  /*optional command*/
                But->MinSize = k_val();
                if(But->MinSize>10.0) But->MinSize = 10.0; 
                if(But->MinSize< 0.0) But->MinSize =  0.0; 
            }

            else if( k_its("Passes") ) {
                howlong = k_int();
            }

            else if( k_its("SecsToWait") ) {  /*optional command*/
                But->SecsToWait = k_int();
                if(But->SecsToWait>1000) But->SecsToWait = 1000; 
                if(But->SecsToWait<   0) But->SecsToWait =    0; 
            }

            else if( k_its("UseDST") )         But->UseDST = 1;       /* optional commands */

            else if( k_its("Debug") ) {  /*optional command*/
                But->Debug = k_int();
                if( k_err() ) But->Debug = 99;
                Debugger = But->Debug;
            }
            
            else if( k_its("WSDebug") ) But->WSDebug = 1;   /* optional commands */

            else if( k_its("NoFlush") ) {  /*optional command*/
                Flush = 0;
            }


                /* At this point we give up. Unknown thing.
                *******************************************/
            else {
                fprintf(stderr, "%s <%s> Unknown command in <%s>.\n",
                         whoami, com, configfile );
                continue;
            }

                /* See if there were any errors processing the command
                 *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "%s Bad <%s> command  in <%s>; exiting!\n",
                             whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
    }

        /* After all files are closed, check init flags for missed commands
         ******************************************************************/
    nmiss = 0;
    for(i=0;i<ncommand;i++)  if( !init[i] ) nmiss++;
    if ( nmiss ) {
        fprintf( stderr, "%s ERROR, no ",  whoami);
        if ( !init[0] )  fprintf( stderr, "<LogSwitch> "       );
        if ( !init[1] )  fprintf( stderr, "<MyModuleId> "      );
        if ( !init[2] )  fprintf( stderr, "<RingName> "        );
        if ( !init[3] )  fprintf( stderr, "<HeartBeatInt> "    );
        if ( !init[4] )  fprintf( stderr, "<GetSumFrom> "      );
        if ( !init[5] )  fprintf( stderr, "<MaxMsgSize> "      );
        if ( !init[6] )  fprintf( stderr, "<MaxMessages> "     );
        if ( !init[7] )  fprintf( stderr, "<wsTimeout> "       );
        if ( !init[8] )  fprintf( stderr, "<WaveServer> "      );
        if ( !init[9] )  fprintf( stderr, "<Target> "          );
        if ( !init[10])  fprintf( stderr, "<GifDir> "          );
        if ( !init[11])  fprintf( stderr, "<SetPlotParams> "   );
        if ( !init[12])  fprintf( stderr, "<Component> "       );
        if ( !init[13])  fprintf( stderr, "<StationList> "     );
        if ( !init[14])  fprintf( stderr, "<Prefix> "          );
        fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
        exit( -1 );
    }
}


/**********************************************************************************
 * ewmod_status() builds a heartbeat or error msg & puts it into shared memory    *
 **********************************************************************************/
void ewmod_status( unsigned char type,  short ierr,  char *note )
{
    char        subname[] = "ewmod_status";
    MSG_LOGO    logo;
    char        msg[512];
    long        size;
    time_t      t;

    logo.instid = InstId;
    logo.mod    = MyModId;
    logo.type   = type;

    time( &t );
    if( type == TypeHeartBeat ) {
        sprintf( msg, "%ld %ld\n\0", t,MyPid);
    } else
    if( type == TypeError ) {
        sprintf( msg, "%ld %hd %s\n", t, ierr, note );
        logit( "t", "%s: %s:  %s\n",  progname, subname, note );
    }

    size = strlen( msg );   /* don't include the null byte in the message */
    if( tport_putmsg( &InRegion, &logo, size, msg ) != PUT_OK ) {
        if( type == TypeHeartBeat ) {
            logit("et","%s: %s:  Error sending heartbeat.\n",  progname, subname );
        } else
        if( type == TypeError ) {
            logit("et","%s: %s:  Error sending error:%d.\n",  progname, subname, ierr );
        }
    }
}


/****************************************************************************
 *  lookup_ew( ) Look up important info from earthworm.h tables           *
 ****************************************************************************/
void lookup_ew( )
{
    char        subname[] = "lookup_ew";
    
        /* Look up keys to shared memory regions
         ***************************************/
    if( (InRingKey = GetKey(InRingName)) == -1 ) {
        fprintf( stderr,
                "%s: %s: Invalid ring name <%s>; exiting!\n",  progname, subname, InRingName );
        exit( -1 );
    }

        /* Look up installation Id
         *************************/
    if ( GetLocalInst( &InstId ) != 0 ) {
        fprintf( stderr,
                "%s: %s: error getting local installation id; exiting!\n",  progname, subname );
        exit( -1 );
    }

        /* Look up modules of interest
         *****************************/
    if ( GetModId( MyModuleId, &MyModId ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid module name <%s>; exiting!\n",  progname, subname, MyModuleId );
        exit( -1 );
    }

        /* Look up message types of interest
         ***********************************/
    if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n",  progname, subname );
        exit( -1 );
    }
    if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid message type <TYPE_ERROR>; exiting!\n",  progname, subname );
        exit( -1 );
    }
}


