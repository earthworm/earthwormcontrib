#
# This is the eqwaves parameter file. This module gets "trigger" messages
# from the hypo_rings, and initiates threads to make record sections.

#  Basic Earthworm setup:
#
LogSwitch     1              # 0 to completely turn off disk log file
MyModuleId    MOD_EQWV_A     # module id for this instance of eqwaves 

RingName      HYPO_RING      # ring to get input from
HeartBeatInt  5              # seconds between heartbeats

# List the message logos to grab from transport ring
#           Installation    Module      Message Type
GetSumFrom  INST_MENLO      MOD_EQPROC  TYPE_HYP2000ARC

#    others
MaxMsgSize     60000    # length of largest message we'll ever handle - in bytes
MaxMessages      100    # limit of number of message to buffer
wsTimeout 10 # time limit (secs) for any one interaction with a wave server.

# List of wave servers (ip port) to contact to retrieve trace data.

@all_waveservers.d

# Directory in which to store the temporary .gif, .link and .html files.
GifDir   /home/picker/gifs/wavesall/ 

# Directory in which to store the qdds add-on files.
QDDSDir    /home/picker/sendfiles/qdds/ 
QDDSLink   http://quake.wr.usgs.gov/waveforms/wavesall/ 
QDDSLogo   Waveform_nc 
QDDSLabel  "Northern Califonia Waveforms" 

# Directory for manual input of arc files.
ArcDir   /home/picker/gifs/wavesall/arc_indir/

# Directory in which to store the .gif files for output.
LocalTarget   /home/picker/sendfiler/eqwaves/ 
LocalTarget   /home/picker/sendfiles/eqwaves/ 

 StationList     /home/earthworm/run/params/calsta.db

# We accept a command "Passes" which issues a heartbeat-type logit
# to let us know eqwaves is alive (debugging)
 Passes   8000

# We accept a command "NoFlush" which prevents the ring from being
# flushed on restart.
#NoFlush
#
# LocalTime      -7  PDT  # Time difference to GMT, Time zone
  LocalTime      -8  PST  # Time difference to GMT, Time zone
 
  UseDST

# Filename prefix on target computer.  This is useful for identifying
# files for automated deletion via crontab.
 Prefix nc

# Plot Parameters - sorry it's so complex, but that's the price of versatility
        # The following is designed such that the first two plots are 
        # displayed side-by-side on the web page. All subsequent plots
        # follow in queue down the page.
# XSize           Overall size of plot in inches
# YSize           Setting these > 100 will imply pixels
# NTrace          Number of traces
# TotalSecs      -1 => codalen+10+PreEvent
# SecsPreEvent   Seconds to plot before event time
# PlotForm       Format of plot. 1 - aligned on event time (default)
#                                2 - aligned on first arrivals
#                                3 - reduced at 6 km/s
#                                4 - reduced at 8 km/s
#                                5 - reduced at 8 km/s; true distance
# TraceLabel      Include trace labels to left of traces 
# PlotEnable      Actually do the plot (only really needed for 1 and/or 2)
#                                      
#              X     Y     Number  Total   Secs     Format  Trace   Plot    
#              Size  Size  Traces  Secs   PreEvent          Label   Enable                            
 SetPlotParams 550   550     15    -1       10        2       1       1     
#SetPlotParams 280   550     15     5        2        2       0       1     
 SetPlotParams 550   550     15     5        1        2       1       1     
 SetPlotParams 9.0  10.0     30    30        2        4       1       1     
#SetPlotParams 10.0 15.0    180    15        2        5       1       1     

# Data type selection.
 Component  VHZ
 Component  VDZ
#Component  HNZ
 Component  EP1
 Component  HHZ
 Component  BHZ
 Component  EHZ
 Component  SHZ
 Component  SLZ
 Component  EH1    #SAFOD pilot hole

    # *** Optional Commands ***
# Which traces will we display? (optional)
#Use_all_Sites     # Use all available channels from waveserver(s)  
#Use_picked_Sites  # Use picked channels available from waveserver(s) 
 
 MaxDist    300  # Max distance of traces to plot; default=100km
 RedSize    3.0  # Minimum magnitude to display as red in list; default=3.0
 MinSize    0.0  # Minimum magnitude to save in list; default=0.0
 RetryCount   2  # Number of attempts to get a trace from server; default=10
 MaxList    200  # Max number of events to list; default=100
 MaxDays      5  # Max number of days to list; default=7
 
# We accept a command "Debug" which turns on a bunch of log messages
 
  Debug 1
# WSDebug

