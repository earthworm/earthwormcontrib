/***************************************************************************
 * This group of routines is started by the processor manager.             *
 * We're given a copy of the archive message for this event.               *
 *                                                                         *
 *   MainProcess - This is the process controller.                         *
 *       Read config file                                                  *
 *       Read Arkive msg and stuff info in ark structure.                  *
 *       Build table of data of interest                                   *
 *       Retrieve data traces                                              *
 *       Process traces                                                    *
 *       Plot                                                              *
 *       Publish                                                           *
 ***************************************************************************/

#include <platform.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <chron3.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include "mem_circ_queue.h" 
#include <swap.h>
#include <trace_buf.h>
#include <time_ew.h>
#include <decode.h>
/*
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
*/
#include <ws_clientII.h>
#include "gd.h"
#include "gdfontt.h"   /*  6pt      */
#include "gdfonts.h"   /*  7pt      */
#include "gdfontmb.h"  /*  9pt Bold */
#include "gdfontl.h"   /* 13pt      */
#include "gdfontg.h"   /* 10pt Bold */

#include "eqwaves.h"

/* Structure for keeping track of buffer of trace data */
DATABUF  pTrace;
GAP *pGap;

/* Functions in this source file 
 *******************************/
int MainProcess(Global *, Arkive *Ark);
void Sort_Servers (Global *, double StartTime);
short Build_Axes(Global *, Arkive *, PltPar *);
void Pallette(int i, gdImagePtr GIF, long color[]);
short Plot_Trace(Global *, double *, PltPar *, double, ArkInfo *);
void End_Plot(Global *, Arkive *);
void Copy2Local(Global *But, char *FileName);
short Get_Ark(Arkive *);
void read_hyp(Arkive *, char *);
void read_hypy2k(Arkive *, char *);
void read_phs(ArkInfo *, char *, char *);
void read_phsy2k(ArkInfo *, char *, char *);
short Build_Table(Global *, Arkive *, long *);
short No_Dups (Arkive *Arkp, char *sta, char *comp, char *net, char *loc);
void Sort_Ark (long, double *, long *);

void Make_Time( double *secs, char *string);
short Build_Menu (Global *);
int In_Menu (Global *, char *sta, char *comp, char *net, char *Loc, double *, double *);
int In_Menu_list (Global *, char *Site, char *Comp, char *Net, char *Loc);
int RequestWave(Global *, int, double *, char *, char *, char *, char *, double, double);
int WSReqBin(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt);
int WSReqBin2(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt);
int IsDST(int, int, int);
void Decode_Time( double, TStrct *);
void date22( double, char *);
void date11( double, char *);
int MinMax(long, long, int, double *, double *, double *);
int ixp(double);
int iyp(double);
int iyq(double, double);
void Get_Sta_Info(Global *);
int Put_Sta_Info(Global *But, ArkInfo *A);
short distaz (double lat1, double lon1, double lat2, double lon2, 
                double *rngkm, double *faz, double *baz);

double    XLabel;                    /* Width of label space                    */
double    XLMargin;                  /* Margin to left of axes                  */
double    XRMargin;                  /* Margin to right of axes                 */
double    YBMargin = 1.0;            /* Margin at bottom of axes                */
double    YTMargin = 0.7;            /* Margin at top of axes                   */
double    sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's        */
                                     /* time 0 and 1970-01-01 00:00:00.0 GMT    */
int     Debug;
double  Data[MAXTRACELTH];

/********************************************************************
 *  MainProcess is responsible for shepherding one event through    *
 *  the seismogram plotting string.                                 *
 *                                                                  *
 ********************************************************************/

int MainProcess(Global *But, Arkive *Ark)
{
    char    whoami[50], fname[100], tname[100];
    double  big_screen_secs;
    time_t  current_time;
    long    TooLate, next, InListToDo, We_Need[MAXCHANNELS];
    int     i, j, jj, jjj, k, error, ierr, successful, Traces_done, Plots_done;
    FILE    *out;
    
    /* Build the modname for error messages *
     ****************************************/
    sprintf(But->mod,"eqwaves");
    sprintf(whoami, " %s: %s: ", But->mod, "MainProcess");
    
    XLabel    = 1.2;  /* Width of label space     */
    XLabel    = 1.4;  /* Width of label space     */
    XLabel    = 1.7;  /* Width of label space     */
    XLMargin  = 0.2;  /* Margin to left of axes   */
    YBMargin  = 0.2;  /* Margin at bottom of axes */
    YBMargin  = 0.4;  /* Margin at bottom of axes */
    XRMargin  = 0.7;  /* Margin to right of axes  */
    YTMargin  = 0.7;  /* Margin at top of axes    */

    /* Clear out the gap list */
    pTrace.nGaps   = 0;
    
    /* Let's see the message * 
     *************************/
    Debug = But->Debug;
    if (But->Debug >=2) {
        logit("et", "%s begin event:\n%.80s...\n", whoami, Ark->ArkivMsg);
    }
    
    /* Go get all available information from the arkive msg. 
    ********************************************************/
    Ark->MaxChannels = But->MaxChannels;
    error = Get_Ark(Ark);
    if(error) {
        logit("e", "%s Error <%hd> reading arkive msg.\n", whoami, error);
        logit("e", "        There are %d entries in table.\n", Ark->InList);
        if(Ark->InList < 1) {
            logit("e", "exiting!\n");
            return 1;
        }
    }
    if(But->Debug) logit("et", "%s Got the arkive msg for %s.\n", whoami, Ark->EvntIDS);
    
    if(Ark->mag < But->MinSize) {
        if(But->Debug) logit("e", "%s Event %s too small (%5.2f)\n", whoami, Ark->EvntIDS, Ark->mag);
        return 1;
    }
    
    Get_Sta_Info(But);
    
    /* Set up the plotting parameters for each plot. 
     ************************************************/
    But->Secs_Screen = 60.0; /* Maximum Number of secs of data to retrieve        */
    But->Pre_Event   =  0.0; /* Maximum Number of secs before event to start plot */
    for(i=0;i<But->nPlots;i++) {
        But->plt[i].sec_per_screen = But->plt[i].SecPerScreenReq;
        if(But->plt[i].sec_per_screen < 1.0) {
                    /* READ (ARKIVA(1), '(T68, F2.1)') XMAG
                       TAU = 10.0**((XMAG - FMA)/FMB)
                       DATA FMA, FMB/-.87, 2.0/      */
            big_screen_secs = pow(10.0, (Ark->mag + 0.87)/2.0);
            big_screen_secs = 10.0*(ceil(big_screen_secs/10.0)) + But->plt[i].pre_event;
            if(big_screen_secs <  30.0) big_screen_secs =  30.0;
            if(big_screen_secs > 300.0) big_screen_secs = 300.0;
            But->plt[i].sec_per_screen = big_screen_secs;
        }
        if(But->plt[i].sec_per_screen > But->Secs_Screen) But->Secs_Screen = But->plt[i].sec_per_screen;
        if(i==1) {    /* This is potentially the companion plot for the first one. */
            But->plt[i].ntrace = But->plt[0].ntrace;
            But->plt[i].ysize  = But->plt[0].ysize;
        }
        But->plt[i].ktrace  = 0;
        But->plt[i].xoffset =  But->plt[i].trace_label? XLMargin + XLabel:XLMargin;
        But->plt[i].axexmax =  But->plt[i].xsize - But->plt[i].xoffset - XRMargin;
        But->plt[i].axeymax =  But->plt[i].ysize - YBMargin - YTMargin;
        But->plt[i].tsize   = (But->plt[i].axeymax-0.4)/But->plt[i].ntrace; /* height of one trace [Data] */
        if(But->plt[i].tsize < 0.2) {
            But->plt[i].ntrace = (long)((But->plt[i].axeymax-0.4)/0.2);
            But->plt[i].tsize  = (But->plt[i].axeymax-0.4)/But->plt[i].ntrace; 
        }
        if(But->plt[i].Format == 5) But->plt[i].tsize   = 0.2;         /* Time Reduced/8; true distance */
        
        But->plt[i].ycenter =  But->plt[i].axeymax - But->plt[i].tsize/2.0;
        if(But->plt[i].pre_event > But->Pre_Event) But->Pre_Event = But->plt[i].pre_event;
    }

    Build_Menu(But);  /*  Build the current wave server menus */
    
    if(!But->got_a_menu) {
        logit("e", "%s No Menu from %d servers! Just quit.\n", whoami, But->nServer);
        return 1;
    }
    /* Build the To-Do list from all available information. 
     ******************************************************/
    error = Build_Table(But, Ark, &InListToDo);
    if(error) {
        if(error>1) logit("e", "%s Error <%hd> building To Do list.\n", whoami, error);
        if(InListToDo > 0) {
            logit("e", "        There are %d entries in table.\n", InListToDo);
        } else {
            logit("e", "        There are no entries in table.\n");
            for(j=0;j<But->nServer;j++) wsKillMenu(&(But->menu_queue[j]));
            return 1;
        }
    }
    if(But->Debug) logit("e", "%s Got the To Do list.\n", whoami);
    for(i=0;i<InListToDo;i++) We_Need[i] = 1;
    for(i=InListToDo;i<Ark->MaxChannels;i++) We_Need[i] = 0;
    /*  Process the traces by looping thru the To-Do list. 
    ******************************************************/
 
    next = 0;  /* First Initialize a bunch of stuff. */
    time(&current_time);
    TooLate = current_time + TIMEOUT;
    
        /*  Build the axes for all requested plots.
        *******************************************/
    for(i=0;i<But->nPlots;i++) {
        Build_Axes(But, Ark, &(But->plt[i]));
    }

    for(i=0;i<But->nPlots;i++) But->plt[i].done = 0;
    Traces_done = 0;
    
        /*  Go get the traces.
        **********************/
    if(But->Debug) logit("e", "%s Processing traces.\n", whoami);
    while(current_time < TooLate && next < InListToDo) {
        if(We_Need[next]) {
            k = Ark->index[next];
            if(In_Menu_list(But, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, Ark->A[k].Loc )) {  
                successful = 0;
                Sort_Servers (But, Ark->A[k].Stime);
                for(jj=0;jj<But->nentries;jj++) {
                    jjj = But->index[jj];
                
                    Ark->A[k].Duration = But->Secs_Screen + 5.0 + Ark->A[k].Dist/6;
                    if(Ark->A[k].Stime + Ark->A[k].Duration > But->TEtime[jjj]) 
                        Ark->A[k].Duration = But->TEtime[jjj] - Ark->A[k].Stime;
                
                    successful = RequestWave(But, jjj, Data, 
                        Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, Ark->A[k].Loc, 
                        Ark->A[k].Stime, Ark->A[k].Duration);

                    if(successful == 1) {   /*    Plot this trace to memory. */
                        break;
                    }
                    else if(successful == 2) {
                        if(But->Debug) 
                            logit("e", "%s RequestWave exception 2. %s %s %s %s\n", 
                                whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, Ark->A[k].Loc);
                        continue;
                    }
                    else if(successful == 3) {   /* Gap in data */
                        if(But->Debug) 
                            logit("e", "%s RequestWave exception 3. %s %s %s %s\n", 
                                whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, Ark->A[k].Loc);
                        continue;
                    }
                    else if(successful == 4) {   /* Too many small gaps in data */
                        if(But->Debug) 
                            logit("e", "%s RequestWave exception 4. %s %s %s %s\n", 
                                whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, Ark->A[k].Loc);
                        continue;
                    }
                }
                    
                if(successful == 1) {   /*    Plot this trace to memory.    
                                         ******************************/
                    Plots_done = 0;
                    Traces_done   += 1;
                    for(i=0;i<But->nPlots;i++) {
                        if(But->plt[i].done) Plots_done   += 1;
                        else  {
                            if(Plot_Trace(But, Data, &(But->plt[i]), Ark->EvntTime, &(Ark->A[k]))) {
                                if(But->Debug) logit("e", "%s Plot %d done: %d %d %d\n", 
                                             whoami, i, next, Traces_done, InListToDo);
                            
                            }
                        }
                    }
                    if(Plots_done == But->nPlots) {
                        if(But->Debug) logit("e", "%s All plots done: %d %d %d %d\n", 
                                             whoami, next, Traces_done, InListToDo);
                        break;
                    }
                    We_Need[next] = 0;
                }
            
            }  
        }
        next += 1;
        if(next >= InListToDo) break;
        time(&current_time);
    }
    if(But->Debug) logit("e", "%s Traces processed: %d %d %d %d\n", 
                   whoami, next, Traces_done, InListToDo, (current_time - TooLate));
    
        /*  Save the GIF files for each requested plot.
        ***********************************************/
    if(Traces_done > 0) {    /* Make the GIF files. */
        logit("et", "%s Writing GIF File(s): %s%s.rsec##.gif  Md: %f\n", 
                whoami, But->Prefix, Ark->EvntIDS, Ark->mag);
        for(i=0;i<But->nPlots;i++) {
            sprintf(But->plt[i].GifName,  "%s%s.rsec%.1d%s", But->Prefix, Ark->EvntIDS, i, ".gif");      /* nc#####.rseci */
            for(j=0;j<But->nltargets;j++) {
                sprintf( tname, "%s%s.%.1d%s", But->GifDir, But->mod, i, ".gif");
                out = fopen(tname, "wb");
                if(out == 0L) {
                    logit("e", "%s Unable to write GIF File: %s\n", whoami, tname); 
                    continue;   
                } else {
                    gdImageGif(But->plt[i].GifImage, out);
                    fclose(out);
                    sprintf(fname,  "%s%s", But->loctarget[j], But->plt[i].GifName );
                    ierr = rename(tname, fname); 
                }
            }
            gdImageDestroy(But->plt[i].GifImage);
        }
        
        End_Plot(But, Ark);
        sleep_ew(200);
    }
    for(j=0;j<But->nServer;j++) wsKillMenu(&(But->menu_queue[j]));

    /* Clear out the gap list */
    pTrace.nGaps = 0;
    
    return 0;
}


/*************************************************************************
 *   Sort_Servers                                                        *
 *      From the table of waveservers containing data for the current    *
 *      SCN, the table is re-sorted to provide an intelligent order of   *
 *      search for the data.                                             *
 *                                                                       *
 *      The strategy is to start by dividing the possible waveservers    *
 *      into those which contain the requested StartTime and those which *
 *      don't.  Those which do are retained in the order specified in    *
 *      the config file allowing us to specify a preference for certain  *
 *      waveservers.  Those waveservers which do not contain the         *
 *      requested StartTime are sorted such that the possible data       *
 *      retrieved is maximized.                                          *
 *************************************************************************/

void Sort_Servers (Global *But, double StartTime)
{
    char    whoami[50], c22[25];
    double  tdiff[MAX_WAVESERVERS*2];
    int     j, k, jj, last_jj, kk, hold, index[MAX_WAVESERVERS*2];
    
    sprintf(whoami, " %s: %s: ", But->mod, "Sort_Servers");
        /* Throw out servers with data too old.
    j = 0;
    while(j<But->nentries) {
        k = But->index[j];
        if(StartTime > But->TEtime[k]) {
            if(But->Debug) {
                date22( StartTime, c22);
                logit("e","%s %d %d  %s", whoami, j, k, c22);
                    logit("e", " %s %s <%s>\n", 
                          But->wsIp[k], But->wsPort[k], But->wsComment[k]);
                date22( But->TEtime[k], c22);
                logit("e","ends at: %s rejected.\n", c22);
            }
            But->inmenu[k] = 0;    
            But->nentries -= 1;
            for(jj=j;jj<But->nentries;jj++) {
                But->index[jj] = But->index[jj+1];
            }
        } else j++;
    } */
    if(But->nentries <= 1) return;  /* nothing to sort */
            
    /* Calculate time differences between StartTime needed and tankStartTime */
    /* And copy positive values to the top of the list in the order given    */
    jj = 0;
    for(j=0;j<But->nentries;j++) {
        k = index[j] = But->index[j];
        tdiff[k] = StartTime - But->TStime[k];
        if(tdiff[k]>=0) {
            But->index[jj++] = index[j];
            tdiff[k] = -65000000; /* two years should be enough of a flag */
        }
    }
    last_jj = jj;
    
    /* Sort the index list copy in descending order */
    j = 0;
    do {
        k = index[j];
        for(jj=j+1;jj<But->nentries;jj++) {
            kk = index[jj];
            if(tdiff[kk]>tdiff[k]) {
                hold = index[j];
                index[j] = index[jj];
                index[jj] = hold;
            }
            k = index[j];
        }
        j += 1;
    } while(j < But->nentries);
    
    /* Then transfer the negatives */
    for(j=last_jj,k=0;j<But->nentries;j++,k++) {
        But->index[j] = index[k];
    }
}    


/********************************************************************
 *    Build_Axes constructs the record section axes by drawing the  *
 *    GIF image in memory.                                          *
 *                                                                  *
 ********************************************************************/

short Build_Axes(Global *But, Arkive *Ark, PltPar *plt)
{
    char    whoami[50], c22[30], d22[30], cstr[150], string[150], LocalTimeID[4];
    double  xl, ymax, xx, frac, tsize, cm_sec, t_start, d_start, LocalTimeOffset;
    int     nsec, isec, timeclr, labelspace, ticksspace, timelabel;
    int     x[6], y[6], labelflag, ix, iy, i, j, kk, strlth;
    int     inx[]={0,1,2,2,2,3,4,4,4,4,5}, iny[]={2,3,2,1,0,1,1,0,2,3,3};
    long    black;
    TStrct  t0;    

    sprintf(whoami, " %s: %s: ", But->mod, "Build_Axes");
    plt->GifImage = gdImageCreate((int)plt->xpix, (int)plt->ypix);
    if(plt->GifImage==0) {
        logit("e", "%s Not enough memory! Reduce size of image or increase memory.\n\n", 
              whoami);
        return 1;
    }
    if(plt->GifImage->sx != plt->xpix) {
        logit("e", "%s Not enough memory for entire image! Reduce size of image or increase memory.\n\n", 
             whoami);
        return 1;
    }
    Pallette(0, plt->GifImage, plt->gcolor);

/* Plot the frame *
 ******************/
    ymax = plt->axeymax;
    xl = plt->trace_label? plt->xoffset- XLabel:plt->xoffset;
    xl = XLMargin;
    black = plt->gcolor[BLACK];
    x[0] = ixp(xl);
    x[1] = ixp(plt->xoffset);
    x[2] = ixp(plt->axexmax + plt->xoffset);
    y[0] = iyq(plt->ysize, 0.0);
    y[1] = iyq(plt->ysize, ymax);
    gdImageRectangle( plt->GifImage, x[0], y[1], x[2],  y[0],  black);
    gdImageLine(      plt->GifImage, x[1], y[0], x[1],  y[1],  black);
    if(plt->Format < 5) {                    
        for(j=1;j<=plt->ntrace;j++) {
            y[2] = iyq(plt->ysize, (ymax - j*plt->tsize));
            gdImageLine(  plt->GifImage, x[0], y[2], x[1], y[2], black);
        }
    }
    
/* Make the time ticks * 
 ***********************/
    cm_sec = plt->axexmax / plt->sec_per_screen;
    if(plt->Format == 1) {                    /* Aligned on Event Time      */
        t_start = Ark->EvntTime - plt->pre_event;
        d_start = Ark->EvntHour*3600 + Ark->EvntMin*60 + Ark->EvntSec - plt->pre_event;
    }   
    else {                    
        t_start = d_start =  - plt->pre_event;
    }
    if(d_start < 0.0) d_start += 86400; 

    frac = (1.0 - (modf(d_start, &xx)));     /* fraction of sec        */
    nsec = (int)xx + 1;                           /* 1st whole secs on plot */

    timelabel  = 1;
    labelspace = ( 10*cm_sec < 0.7)? 60:10;
    labelspace = ( 10*cm_sec > 7.0)?  1:labelspace;
    labelflag = 0;
    ticksspace = (100*cm_sec < 2.8)?  0: 1;
    ticksspace = (100*cm_sec < 3.2)?  0: 1;
    ticksspace = ( 72*cm_sec < 4.0)?  0: 1;
    for(j=0;j<plt->sec_per_screen;j++) {
        x[0] = ixp(j*cm_sec + frac * cm_sec + plt->xoffset);
        isec = nsec + j;
        if ((div(isec, 60)).rem == 0)      tsize = 0.3;    /* 60 sec ticks */
        else if ((div(isec, 10)).rem == 0) tsize = 0.2;    /* 10 sec ticks */
        else                               tsize = 0.1;    /*  1 sec ticks */
        if ((div(isec, labelspace)).rem == 0) {            /* 10 sec ticks */
            iy = iyq(plt->ysize, -0.1);
            date11 (d_start + frac + j, string);
            strlth = strlen(string);
            if(strlth > 3) labelflag += 1;
            string[strlth] = '\0';
            ix = x[0] - 5*strlth/2;
            timeclr = ((div(isec, 60)).rem == 0)? plt->gcolor[RED]:black;
      /*    gdImageString(plt->GifImage, gdFontTiny, ix, iy, string, timeclr);  */
            gdImageString(plt->GifImage, gdFontSmall, ix, iy, string, timeclr);
            timelabel = 0;
            gdImageLine(plt->GifImage, x[0], y[0], x[0], y[1], plt->gcolor[GREY]); /* make the tick */
        }
        if(tsize != 0.1 || ticksspace) {
            y[2] = iyq(plt->ysize, tsize);
            gdImageLine(plt->GifImage, x[0], y[0], x[0], y[2], black); /* make the tick */
        }
    }
    if(timelabel) {
        ix = ixp(frac * cm_sec + plt->xoffset)-2;
        iy = iyq(plt->ysize, -0.1);
        date11 (d_start + frac, string);
            strlth = strlen(string);
        string[strlth] = '\0';
        ix = ixp(frac * cm_sec + plt->xoffset) - 5*strlth/2;
    /*  gdImageString(plt->GifImage, gdFontTiny, ix, iy, string, black);  */
        gdImageString(plt->GifImage, gdFontSmall, ix, iy, string, black);
    }
    gdImageRectangle( plt->GifImage, x[0], y[1], x[2],  y[0],  black);
    gdImageLine(      plt->GifImage, x[1], y[0], x[1],  y[1],  black);
    
    iy = iyq(plt->ysize, -0.25);
    if(labelflag < 1) {
        ix = (ixp(plt->axexmax + plt->xoffset) + ixp(plt->xoffset))/2 - 5*4;
        gdImageString(plt->GifImage, gdFontSmall, ix, iy, "Seconds", black);
    }
    else {
        ix = (ixp(plt->axexmax + plt->xoffset) + ixp(plt->xoffset))/2 - 5*8;
        gdImageString(plt->GifImage, gdFontSmall, ix, iy, "Minutes:Seconds", black);
    }
    
    if(plt->Format == 5) {                    /* Time Reduced/8; true distance */
        ymax = plt->axeymax;
        for(j=0;j<But->MaxDist;j+=10) {  /*   Put in the distance axis tics and labels   */
            if ((div(j, 100)).rem == 0)      tsize = 0.3;    /* 100 km ticks */
            else if ((div(j, 10)).rem == 0)  tsize = 0.2;    /*  10 km ticks */
            else                             tsize = 0.1;    /*   1 km ticks */
            x[0] = ixp(plt->xoffset);
            x[1] = ixp(plt->xoffset - tsize);
            iy = iyq(plt->ysize,  ymax - j*(ymax/But->MaxDist));
            gdImageLine(plt->GifImage, x[0], iy, x[1], iy, black); /* make the tick */
            sprintf(string, "%d", j);
            gdImageString(plt->GifImage, gdFontTiny, x[0] - 20, iy+5, string, plt->gcolor[RED]);
        }
        
    }

/* Write label with the Event ID, event time * 
 *********************************************/
    ix = ixp(xl+0.1);
    iy = iyq(plt->ysize,  (ymax + 0.5));
    sprintf(cstr, "Event: %ld  ", Ark->EvntID) ;
    gdImageString(plt->GifImage, gdFontMediumBold, ix, iy, cstr, black);    

    strcpy(LocalTimeID, But->LocalTimeID);
    LocalTimeOffset = But->LocalTime;
    Decode_Time(Ark->EvntTime + But->LocalTime, &t0);
    if(IsDST(t0.Year,t0.Month,t0.Day) && But->UseDST) {
        LocalTimeOffset = LocalTimeOffset + 1;
        LocalTimeID[1] = 'D';
    }
    date22 (Ark->EvntTime, c22);
    date22 (Ark->EvntTime + LocalTimeOffset*3600, d22);
    iy = iyq(plt->ysize,  (ymax + 0.25));
    sprintf(cstr, "%.22s UTC (%.11s %3s)", c22, &d22[11], LocalTimeID) ;
    gdImageString(plt->GifImage, gdFontMediumBold, ix, iy, cstr, black);    

/* Initial it *
 **************/
    if(plt->trace_label) {
        i = 2;
        j = (int)(plt->ysize*72.0) - 5;
        for(kk=0;kk<11;kk++) gdImageSetPixel(plt->GifImage, i+inx[kk], j+iny[kk], black);

/* Legend it *
 *************/
        i = ixp(xl)+2;   
        j = iyq(plt->ysize, 0.0) - 5;
        gdImageRectangle      (plt->GifImage, i,   j,   i+5, j-5, plt->gcolor[BLACK]); 
        gdImageFilledRectangle(plt->GifImage, i+1, j-4, i+4, j-1, plt->gcolor[BLUE]); 
        gdImageString(plt->GifImage, gdFontTiny, i+8, j-7, "Coda", plt->gcolor[BLACK]);
        
        j -= 7;
        gdImageRectangle      (plt->GifImage, i,   j,   i+5, j-5, plt->gcolor[BLACK]); 
        gdImageFilledRectangle(plt->GifImage, i+1, j-4, i+4, j-1, plt->gcolor[GREEN]); 
        gdImageString(plt->GifImage, gdFontTiny, i+8, j-7, "P-Predicted", plt->gcolor[BLACK]);

        j -= 7;
        gdImageRectangle      (plt->GifImage, i,   j,   i+5, j-5, plt->gcolor[BLACK]); 
        gdImageFilledRectangle(plt->GifImage, i+1, j-4, i+4, j-1, plt->gcolor[RED]); 
        gdImageString(plt->GifImage, gdFontTiny, i+8, j-7, "P-Pick", plt->gcolor[BLACK]);

/* Label it *
 ************/
        j = iyq(plt->ysize,  0.1) - 1;
        i = ixp(plt->xoffset)-30;
        if(plt->Format == 3) strcpy(cstr, "T-X/6");   /* Time Reduced/6 */
        if(plt->Format == 4) strcpy(cstr, "T-X/8");   /* Time Reduced/8 */
        if(plt->Format == 5) strcpy(cstr, "T-X/8");   /* Time Reduced/8 */
        if(plt->Format == 3 || plt->Format == 4 || plt->Format == 5) 
            gdImageString(plt->GifImage, gdFontTiny, i, j, cstr, plt->gcolor[GREEN]);
    }  
    return 0;
}


/*******************************************************************************
 *    Pallette defines the pallete to be used for plotting.                    *
 *     PALCOLORS colors are defined.                                           *
 *                                                                             *
 *******************************************************************************/

void Pallette(int ColorFlag, gdImagePtr GIF, long color[])
{
    color[WHITE]  = gdImageColorAllocate(GIF, 255, 255, 255);
    color[BLACK]  = gdImageColorAllocate(GIF, 0,     0,   0);
    color[RED]    = gdImageColorAllocate(GIF, 255,   0,   0);
    color[BLUE]   = gdImageColorAllocate(GIF, 0,     0, 255);
    color[GREEN]  = gdImageColorAllocate(GIF, 0,   105,   0);
    color[GREY]   = gdImageColorAllocate(GIF, 125, 125, 125);
    color[YELLOW] = gdImageColorAllocate(GIF, 125, 125,   0);
    color[TURQ]   = gdImageColorAllocate(GIF, 0,   255, 255);
    color[PURPLE] = gdImageColorAllocate(GIF, 200,   0, 200);    
    
    gdImageColorTransparent(GIF, -1);
}

/*******************************************************************************
 *    Plot_Trace plots an individual trace (Data) having the index <thisone>   *
 *    within the ToDoList and stuffs it into the GIF image in memory.          *
 *                                                                             *
 *******************************************************************************/

short Plot_Trace(Global *But, double *Data, PltPar *plt, double EventTime, ArkInfo *A)
{
    char    whoami[50], cstr[150], units[20], plotloc[3];
    double  x, y, xp, scale, xinc, dt_start, cm_sec, Amp1, Amp2, Amplitude, AmpReal, Middle, ymax;
    double  offset, delay, dist, ycenter;
    int     ix, iy, iy1, iy2, lastx, lasty, decimation, acquired, gap0, gap1, samp_pix;
    long    i, j, pick_clr, npts, start, stat;

    sprintf(whoami, "%s: %s: ", But->mod, "Plot_Trace");
    
    if(plt->ycenter < 0.2 + plt->tsize/2.0 && plt->Format != 5) {
        plt->done = 1;
        return 1;
    }
    
    npts     = But->Npts;
    offset   = But->Pre_Event - plt->pre_event;
    dist     = A->Dist;
    dt_start = 0.0;          /* plt->Format == 1 Aligned on Event Time    */

    if(plt->Format == 2) {   /* Aligned on First Arrivals    */
        dt_start = A->PPick - EventTime;
    }
    if(plt->Format == 3) {   /* Time Reduced/6    */
        dt_start = dist/6.0;
    }
    if(plt->Format == 4) {   /* Time Reduced/8    */
        dt_start = dist/8.0;
    }
    if(plt->Format == 5) {   /* Time Reduced/8    */
        dt_start = dist/8.0;
    }
    dt_start += offset;
    start = (long)(dt_start*But->samp_sec);
    if(start < 0) start = 0;
    if(start > npts) start = npts;
    
    cm_sec     = plt->axexmax / plt->sec_per_screen;
    decimation = 2;
    samp_pix   = (int)(But->samp_sec / cm_sec / 72.0);
    decimation = (samp_pix/4 < 1)? 1:samp_pix/4;       /*  decimation */
    xinc = decimation * cm_sec / But->samp_sec;
    stat = MinMax(npts, start, 1, Data, &Amp2, &Middle);
    stat = MinMax(npts, start, decimation, Data, &Amplitude, &Middle);
    
    Amp1 = (Amplitude < 50)? 50:Amplitude;
    scale = (stat==0)? 0.95*plt->tsize/Amp1:0.0;
    ycenter = plt->ycenter;
    if(plt->Format == 5) {     /* Calculate ycenter: Time Reduced/8; true distance */
        ymax = plt->axeymax;
        ycenter = ymax - dist*(ymax/But->MaxDist);
        if(dist > But->MaxDist) {
            if(But->Debug) 
                logit("e", "PlotTrace: MaxDist exceeded for %s %s: %f %f\n", 
                   A->Site, A->Comp, dist, But->MaxDist);
            plt->done = 1;
            return 1;
        }
    }

/* label STATION NAME, dist, maxamp and azim *
 *********************************************/
    AmpReal = Amplitude/A->sensitivity;
    AmpReal = Amp2/A->sensitivity;
    strcpy(units, "counts");
    if(A->Sens_unit == 2) strcpy(units, "cm/s");
    if(A->Sens_unit == 3) {
        strcpy(units, "cm/s/s");
        strcpy(units, "%g");
        AmpReal *= 100.0/980;
    }
    if(plt->trace_label) {
        ix = ixp(XLMargin + 0.05);
        y  = ycenter + 0.15;
        if(plt->tsize < 0.4) y -= 0.05;
        if(plt->tsize < 0.3) y -= 0.05;
        iy = iyq(plt->ysize,  (y));
        
        if(strcmp("--",  A->Loc)==0) strcpy(plotloc, "  ");
        else                         strcpy(plotloc, A->Loc);
        if(plt->Format != 5) 
            sprintf(cstr, "%-4s %2s %3s %2s %4.0f km", A->Site, A->Net,  A->Comp, plotloc, dist);
        else 
            sprintf(cstr, "%-4s %2s %3s %2s", A->Site, A->Net, A->Comp, plotloc);
        gdImageString(plt->GifImage, gdFontTiny, ix, iy, cstr, plt->gcolor[BLACK]);

        if(plt->Format != 5) {                    /* Time Reduced/8; true distance */
            if(plt->tsize >= 0.3) {
                sprintf(cstr, "AmpMax: %.0f", Amp2);
                sprintf(cstr, "Max: %f %s", AmpReal, units);
                iy = iyq(plt->ysize,  (y - 0.1));
                gdImageString(plt->GifImage, gdFontTiny, ix, iy, cstr, plt->gcolor[BLACK]);
            }

            if(plt->tsize >= 0.4) {
                sprintf(cstr, "Azimuth: %.0f", A->Azim);
                iy = iyq(plt->ysize,  (y - 0.2));
                gdImageString(plt->GifImage, gdFontTiny, ix, iy, cstr, plt->gcolor[BLACK]);
            }
        }
    }

/* Plot the picks * 
 ******************/
    iy1 = iyq(plt->ysize,  ycenter + plt->tsize/2.0);
    iy2 = iyq(plt->ysize,  ycenter - plt->tsize/2.0);
    delay = But->Pre_Event - dt_start - EventTime;
    
    if(A->StaList == '*') {
        if(A->PPick != 0.0) {
            xp = (A->PPick + delay) * cm_sec;
            if(xp < plt->axexmax) {
                ix = ixp(xp + plt->xoffset);
                pick_clr = plt->gcolor[RED];
                gdImageLine(plt->GifImage, ix, iy1, ix, iy2, pick_clr);
                ix = ixp(xp + plt->xoffset - 0.3);
                gdImageString(plt->GifImage, gdFontTiny, ix, iy2-8, A->Prem, pick_clr);
            }
            
            if(A->PttResid != 0.0) {
                x = xp - A->PttResid*cm_sec;
                if(x < plt->axexmax) {
                    ix = ixp(x + plt->xoffset);
                    gdImageLine(plt->GifImage, ix, iy1+3, ix, iy2-3, plt->gcolor[GREEN]);
                }
            }

            if(A->CodaDur != 0.0) {
                x = xp + A->CodaDur*cm_sec;
                if(x < plt->axexmax) {
                    ix = ixp(x + plt->xoffset);
                    gdImageLine(plt->GifImage, ix, iy1+3, ix, iy2-3, plt->gcolor[BLUE]);
                }
            }
        }
    }

/* Plot the trace *
 ******************/
    x = 0.0;
    i = 0;
    lastx = ixp(x + plt->xoffset); 
    lasty = iyq(plt->ysize,  ycenter + Data[start]*scale - Middle*scale);  
    if(Amplitude>0.0) {  
        pGap = pTrace.gapList;
        acquired = 0;
        for(j=start;j<npts;j+=decimation) {
            x += xinc;
            if (x > plt->axexmax) {break;}
            gap0 = (i >= pTrace.nGaps)? pTrace.nRaw:pGap->firstSamp;
            gap1 = (i >= pTrace.nGaps)? pTrace.nRaw:pGap->lastSamp;
            if(j > gap1) {
            if(But->Debug) 
                logit("e", "PlotTrace: Gap detected: %d %d\n", gap0, gap1);
                pGap = pGap->next;
                if(pGap == (GAP *)NULL) i = pTrace.nGaps;
                i += 1;
                gap0 = (i >= pTrace.nGaps)? pTrace.nRaw:pGap->firstSamp;
                gap1 = (i >= pTrace.nGaps)? pTrace.nRaw:pGap->lastSamp;
            }
            if(j < gap0) {
                ix = ixp(x + plt->xoffset); 
                iy = iyq(plt->ysize,  ycenter + Data[j]*scale - Middle*scale);
                if(acquired) {
                    gdImageLine(plt->GifImage, ix, iy, lastx, lasty, plt->gcolor[BLACK]);
                }
                lastx = ix;  lasty = iy;
                acquired = 1;
            }
            else {
                acquired = 0;  
            }
        } 
    } else {
        lasty = iyq(plt->ysize,  ycenter);
        ix = ixp(plt->xoffset);
        gdImageString(plt->GifImage, gdFontSmall, ix+2, lasty-3, " No Data", plt->gcolor[RED]);
    }   

/* finish up.  Move down for next trace.
   *************************************/
    plt->ycenter -= plt->tsize;            /* increment y position  */
    plt->ktrace  += 1;                     /* increment trace count */
        
    return 0;
}


/*********************************************************************
 *   End_Plot()                                                      *
 *********************************************************************/

void End_Plot(Global *But, Arkive *Ark)
{
    char    whoami[50], LocalTimeID[4], temp[150];
    char    LinkNamea[150], LinkFNamea[150], LinkNameb[150], LinkFNameb[150], LinkNamec[150];
    char    NS, EW, string[500], IndexF[150], ListA[150], ListB[150];
    int     i, MaxEvents, minute;
    short   yeargmt, year, month, day, hour;
    double  lat, lon, sex, secs, nowsecs, LocalTimeOffset, sec1970 = 11676096000.00;  
    FILE    *in, *out;
    struct Greg  g;
    TStrct  t0;    
    
    sprintf(whoami, " %s: %s: ", But->mod, "End_Plot");
/* Do the main link file
 ***********************/    
    if(But->Debug)  logit("e", "%s Writing Main Link File\n", whoami);   
    sprintf( LinkNamea,  "%s%s.rsec0%s", But->Prefix, Ark->EvntIDS, ".html");      /* nc#####.rseci */
    sprintf( LinkNamec,  "%s%s/", But->Prefix, Ark->EvntIDS);      /* nc##### */
    sprintf( LinkFNamea, "%s%s",         But->GifDir, LinkNamea);
    
    out = fopen(LinkFNamea, "wb");
    if(out == 0L) {
        logit("e", "%s Unable to write Link File: %s\n", whoami, LinkFNamea);    
    } else {
        fprintf(out, "<html><head> ");
        fprintf(out, "<title ALIGN= middle> WAVEFORMS FOR EVENT ID=%s </title>\n", Ark->EvntIDS);
        sprintf( temp, "%s%s", But->GifDir, "indexc.html");
        
        in  = fopen(temp, "r");
        if(in == 0L) {
            fprintf(out, "<BODY BGCOLOR=#EEEEEE TEXT=#333333>\n");
            fprintf(out, "<h1><A NAME=\"top\">");
            fprintf(out, "<CENTER><font color=\"#bb0000\"> ");
            fprintf(out, "Waveforms from the California Integrated Seismic Network ");
            fprintf(out, "</font></CENTER></A></h1>");
            fprintf(out, "</head>\n");
            fprintf(out, "<P><hr><h4> <a href=\"FAQ.html\" >Frequently Asked Questions</a> about this display.</h4><hr>\n");
            fprintf(out, "<P><A NAME=\"grm\">");
        } else {
            while(fgets(string, 480, in)!=0L) {
                fprintf(out, "%s", string);
            }
            fclose(in);
        }
        
        for(i=0;i<But->nPlots;i++) {
            if(But->plt[i].plot_enable) {
                fprintf(out, "<P><A NAME=\"grm%d\">",i);
                fprintf(out, "<IMG ALIGN=MIDDLE  SRC=\"%s\" ALT\"Seismogram GIF\">", But->plt[i].GifName);
                fprintf(out, " <br></p>\n");
                fprintf(out, "THIS PLOT SHOWS:\n");
                fprintf(out, "<ul> <li>Closest %ld channels with phase picks\n", But->plt[i].ktrace);
                fprintf(out, "     <li>%5.2f Seconds of time\n", But->plt[i].sec_per_screen);
                if(But->plt[i].Format == 1) {
                    fprintf(out, "     <li>Traces are displayed in true time.\n");
                    fprintf(out, "     <li>Event origin time is %5.2f sec after beginning of plot\n", But->plt[i].pre_event);
                }
                if(But->plt[i].Format == 2) {
                    fprintf(out, "     <li>Traces are aligned with first arrivals at 0 sec.\n");
                    fprintf(out, "     <li>First arrival is at 0 sec., %5.2f sec after beginning of plot\n", But->plt[i].pre_event);
                }
                if(But->plt[i].Format == 3) {
                    fprintf(out, "     <li>Traces are displayed in time reduced by distance/6 km/s.\n");
                    fprintf(out, "     <li>Event origin time is %5.2f sec after beginning of plot\n", But->plt[i].pre_event);
                }
                if(But->plt[i].Format == 4) {
                    fprintf(out, "     <li>Traces are displayed in time reduced by distance/8 km/s.\n");
                    fprintf(out, "     <li>Event origin time is %5.2f sec after beginning of plot\n", But->plt[i].pre_event);
                }
                if(But->plt[i].Format == 5) {
                    fprintf(out, "     <li>Traces are displayed in time reduced by distance/8 km/s.\n");
                    fprintf(out, "     <li>Event origin time is %5.2f sec after beginning of plot\n", But->plt[i].pre_event);
                }
                fprintf(out, "</ul></A></P>\n<hr>\n");
            }
        }
    
        fprintf(out, "</html>\n");
        fclose(out);
    
        Copy2Local(But, LinkNamea);
    }
    
/* Do the abbreviated link file
 ******************************/        
     if(But->Debug)  logit("e", "%s Writing abbreviated Link File\n", whoami);   
    sprintf( LinkNameb,  "%s%s.rsec1%s", But->Prefix, Ark->EvntIDS, ".html");      /* nc#####.rseci */
    sprintf( LinkFNameb, "%s%s",        But->GifDir, LinkNameb);
    
    out = fopen(LinkFNameb, "wb");
    if(out == 0L) {
        logit("e", "%s Unable to write Link File: %s\n", whoami, LinkFNameb);    
    } else {
        fprintf(out, "<html><head> ");
        fprintf(out, "<title ALIGN= middle> CISN WAVEFORMS FOR EVENT ID=%s </title>\n", Ark->EvntIDS);
        fprintf(out, "<BODY BGCOLOR=#EEEEEE TEXT=#333333>\n");
        fprintf(out, "<h1><A NAME=\"top\">");
        
        fprintf(out, "<CENTER><font color=\"#bb0000\"> ");
        fprintf(out, "Waveforms from the California Integrated Seismic Network ");
        fprintf(out, "</font></CENTER></A></h1>");
        fprintf(out, "</head>\n");
        fprintf(out, "<P><hr><h4>Seismograms of closest stations (sorted by distance)</h4><hr>\n");
        fprintf(out, "<P><A NAME=\"grm\">");
        
        for(i=0;i<2;i++) {
            if(But->plt[i].plot_enable) {
                fprintf(out, "<P><A NAME=\"grm%d\">",i);
                fprintf(out, "<IMG ALIGN=MIDDLE  SRC=\"%s\" ALT\"Seismogram GIF\">", But->plt[i].GifName);
                fprintf(out, " <br></p>\n");
                fprintf(out, "THIS PLOT SHOWS:\n");
                fprintf(out, "<ul> <li>Closest %ld sites with phase picks\n", But->plt[i].ktrace);
                fprintf(out, "     <li>%5.2f Seconds of time\n", But->plt[i].sec_per_screen);
                if(But->plt[i].Format == 1) {
                    fprintf(out, "     <li>Traces are displayed in true time.\n");
                    fprintf(out, "     <li>Event origin time is %5.2f sec after beginning of plot\n", But->plt[i].pre_event);
                }
                if(But->plt[i].Format == 2) {
                    fprintf(out, "     <li>Traces are aligned with first arrivals at 0 sec.\n");
                    fprintf(out, "     <li>First arrival is at 0 sec., %5.2f sec after beginning of plot\n", But->plt[i].pre_event);
                }
                if(But->plt[i].Format == 3) {
                    fprintf(out, "     <li>Traces are displayed in time reduced by distance/6 km/s.\n");
                    fprintf(out, "     <li>Event origin time is %5.2f sec after beginning of plot\n", But->plt[i].pre_event);
                }
                if(But->plt[i].Format == 4) {
                    fprintf(out, "     <li>Traces are displayed in time reduced by distance/8 km/s.\n");
                    fprintf(out, "     <li>Event origin time is %5.2f sec after beginning of plot\n", But->plt[i].pre_event);
                }
                if(But->plt[i].Format == 5) {
                    fprintf(out, "     <li>Traces are displayed in time reduced by distance/8 km/s.\n");
                    fprintf(out, "     <li>Event origin time is %5.2f sec after beginning of plot\n", But->plt[i].pre_event);
                }
                fprintf(out, "</ul></A></P>\n<hr>\n");
            }
        }
    
        fprintf(out, "</html>\n");
        fclose(out);
    
        Copy2Local(But, LinkNameb);
    }
        
/* Copy the last lista.html file (IndexF) to ListB       *
 * with the insertion of the current event along the way *
 *********************************************************/    

    strcpy(LocalTimeID, But->LocalTimeID);
    LocalTimeOffset = But->LocalTime;
    Decode_Time(Ark->EvntTime + But->LocalTime, &t0);
    if(IsDST(t0.Year,t0.Month,t0.Day) && But->UseDST) {
        LocalTimeOffset = LocalTimeOffset + 1;
        LocalTimeID[1] = 'D';
    }
    sex = Ark->EvntTime + sec1970;
    minute = (long) (sex / 60.0);
    grg(minute, &g);
    yeargmt = g.year;
    minute += (int)LocalTimeOffset*60;
    grg(minute, &g);
    year  = g.year;
    month = g.month;
    day   = g.day;
    hour  = g.hour;
    lat = Ark->EvntLat>=0.0? Ark->EvntLat:-Ark->EvntLat;
    lon = Ark->EvntLon>=0.0? Ark->EvntLon:-Ark->EvntLon;
    NS  = Ark->EvntLat>=0.0? 'N':'S';
    EW  = Ark->EvntLon>=0.0? 'E':'W';
     if(But->Debug)  logit("e", "%s ListA to ListB\n", whoami);   
    sprintf( LinkNamea,  "%s%s.rsec0%s", But->Prefix, Ark->EvntIDS, ".html");      /* nc#####.rseci */
    sprintf( LinkNameb,  "%s%s.rsec1%s", But->Prefix, Ark->EvntIDS, ".html");      /* nc#####.rseci */
    sprintf( ListA,  "%s%s", But->GifDir, "lista.html");   /* event list             */
    sprintf( ListB,  "%s%s", But->GifDir, "listb.html");   /* copy of the event list */
    in  = fopen(ListA, "r");
    out = fopen(ListB, "wb");
    if(out == 0L) {
        logit("e", "%s Unable to open listb.html File: %s\n", whoami, ListB);    
    } else {
        if(Ark->mag >= But->RedSize) {
            fprintf(out, "|<a href=\"%s\" target=\"Event Window\">%s</a>", 
                          LinkNamea, Ark->EvntIDS);
            fprintf(out, "|<font color=red>%4.1f</font>", 
                          Ark->mag);
            fprintf(out, "|<font color=red>%.2d:%.2d:%05.2f %.2d/%.2d/%.2d</font>", 
                          Ark->EvntHour,  Ark->EvntMin, Ark->EvntSec, 
                          Ark->EvntMonth, Ark->EvntDay, yeargmt);
            fprintf(out, "|<font color=red>%.2d:%.2d:%05.2f %.2d/%.2d/%.2d</font>", 
                          hour, Ark->EvntMin, Ark->EvntSec, 
                          month, day, year);
            fprintf(out, "|<font color=red>%5.2f%c</font>|<font color=red>%6.2f%c</font>", 
                          lat, NS, lon, EW);
            fprintf(out, "|<font color=red>%6.2f </font>|", 
                          Ark->EvntDepth);
            fprintf(out, "|<a href=\"%s\" target=\"Event Window\">%s</a>", 
                          LinkNameb, "*");
        } else {
            fprintf(out, "<font color=red>|</font><a href=\"%s\" target=\"Event Window\">%s</a>", 
                          LinkNamea, Ark->EvntIDS);
            fprintf(out, "<font color=red>|</font>%4.1f", 
                          Ark->mag);
            fprintf(out, "<font color=red>|</font>%.2d:%.2d:%05.2f %.2d/%.2d/%.2d", 
                          Ark->EvntHour,  Ark->EvntMin, Ark->EvntSec,
                          Ark->EvntMonth, Ark->EvntDay, yeargmt);
            fprintf(out, "<font color=red>|</font>%.2d:%.2d:%05.2f %.2d/%.2d/%.2d", 
                          hour, Ark->EvntMin, Ark->EvntSec,
                          month, day, year);
            fprintf(out, "<font color=red>|</font>%5.2f%c<font color=red>|</font>%6.2f%c<font color=red>|</font>%6.2f <font color=red>|</font>", 
                          lat, NS, lon, EW, Ark->EvntDepth);
            fprintf(out, "<font color=red>|</font><a href=\"%s\" target=\"Event Window\">%s</a>", 
                          LinkNameb, "*");
        }
        fprintf(out, "\n<font color=red>------------------------------------------------------------------------------------</font>\n");
        
        if(in == 0L) {
            logit("e", "%s Unable to open lista.html File: %s\n", whoami, ListA);    
        } else {
            while(fgets(string, 480, in)!=0L) {
                fprintf(out, "%s", string);
            }
            fclose(in);
        }
        fclose(out);
    }
        
    /* Then copy it back so we have a record. 
     ****************************************/    
     nowsecs = secs = Ark->EvntTime - But->MaxDays*86400;
     if(But->Debug)  logit("e", "%s ListB to ListA,  MaxList: %d %d %f\n", whoami, But->MaxList, But->MaxDays, nowsecs);   
    in  = fopen(ListB, "r");
    if(in == 0L) {
        logit("e", "%s Unable to open listb.html File: %s\n", whoami, ListB);    
    } else {
        out = fopen(ListA, "wb");
        if(out == 0L) {
                logit("e", "%s Unable to open lista.html File: %s\n", whoami, ListA); 
                fclose(in);
            }   
        else {
            MaxEvents = 0;
            while(fgets(string, 480, in)!=0L && MaxEvents < But->MaxList) {
                if(strstr(string,"href=")) {
                    Make_Time( &secs, string);
                    MaxEvents += 1;
                    fprintf(out, "%s", string);
                } else {
                    fprintf(out, "%s", string);
                    if(secs < nowsecs) break;
                }
            }
            fclose(out);
        }
        fclose(in);
    }
    
/* Build the index.html file (IndexF)             *
 * with the insertion of the events along the way *
 **************************************************/    

     if(But->Debug)  logit("e", "%s Building index file\n", whoami);   
    sprintf( IndexF, "%s%s", But->GifDir, "index.html");   /* current copy of the full index.html file */
    out = fopen(IndexF, "wb");
    if(out == 0L) {
        logit("e", "%s Unable to open index.html File: %s\n", whoami, IndexF);    
    } else {
        sprintf( temp, "%s%s", But->GifDir, "indexa.html");
        in  = fopen(temp, "r");
        if(in == 0L) {
            fprintf(out, "<HTML><HEAD><TITLE>\nRecent Earthquakes\n</TITLE></HEAD>\n");
            fprintf(out, "<BODY BGCOLOR=#EEEEEE TEXT=#333333 vlink=purple>\n");
            fprintf(out, "<A NAME=\"top\"></A><CENTER>\n");
            fprintf(out, "<H2><IMG SRC=\"smusgs.gif\" WIDTH=58 HEIGHT=35 ALT=\"Logo\" ALIGN=\"middle\">\n");
            fprintf(out, "<FONT COLOR=red>Recent Earthquakes</FONT></H2><br>\n");
            fprintf(out, "<P>Latest Event.</p></CENTER>\n");
        } else {
            while(fgets(string, 480, in)!=0L) {
                fprintf(out, "%s", string);
            }
            fclose(in);
        }
        
        fprintf(out, "<P><A NAME=\"grm\"><IMG ALIGN=MIDDLE  SRC=\"%s\" ALT\"Seismogram GIF\"> </p>\n", But->plt[0].GifName);
        fprintf(out, "\n\n");
        
        fprintf(out, "<STRONG><P><font color=red>\n");
        fprintf(out, "             Here are the earthquakes available for viewing, most recent at top ...<br>\n");
        fprintf(out, "</font></STRONG>\n");
        
        fprintf(out, "<pre>");
        fprintf(out, "<font color=red>");
        fprintf(out, "------------------------------------------------------------------------------------\n");
        fprintf(out, "| EVENT  | MD |    UNIVERSAL TIME    |   LOCAL TIME (%3s)   | LAT  | LONG  | DEPTH |\n", LocalTimeID);
        fprintf(out, "|        |    |  h:m:s     mm/dd/yy  |  h:m:s     mm/dd/yy  | deg  | deg   |  km   |\n");
        fprintf(out, "------------------------------------------------------------------------------------</font>\n");

        in  = fopen(ListA, "r");
        if(in == 0L) {
            logit("e", "%s Unable to open lista.html File: %s\n", whoami, ListA);    
        } else {
            while(fgets(string, 480, in)!=0L) {
                fprintf(out, "%s", string);
            }
            fclose(in);
        }
        fprintf(out, "</pre>\n");
        
        sprintf( temp, "%s%s", But->GifDir, "indexb.html");
        in  = fopen(temp, "r");
        if(in == 0L) {
            fprintf(out, "<P><HR><font color=red></font>\n");
            fprintf(out, "<P><A HREF=\"#top\">Top of this page</A>\n");
            fprintf(out, "</BODY></HTML>\n");
        } else {
            while(fgets(string, 480, in)!=0L) {
                fprintf(out, "%s", string);
            }
            fclose(in);
        }
        fclose(out);
        
        Copy2Local(But, "index.html");
    }
    if(But->Use_QDDS) {
	/* Build the QDDS add-on file         *
	 **************************************/    

	    sprintf( IndexF, "%snc%s.01.%s.add", But->QDDSDir, Ark->EvntIDS, But->QDDSLogo);   /* current copy of the full index.html file */
		if(But->Debug)  logit("e", "%s Building QDDS addon file %s\n", whoami, IndexF);   
	    out = fopen(IndexF, "wb");
	    if(out == 0L) {
	        logit("e", "%s Unable to open add-on File: %s\n", whoami, IndexF);    
	    } else {
	/*        fprintf(out, "LI%.8sNC01  %s   %s%s  %s\n", 
	        		Ark->EvntIDS, But->QDDSLogo, But->QDDSLink, LinkNamea, But->QDDSLabel);    */
	        
	        fprintf(out, "LI%.8sNC01  %s   %s%s  %s\n", 
	        		Ark->EvntIDS, But->QDDSLogo, But->QDDSLink, LinkNamec, But->QDDSLabel);
	        
	        fclose(out);
	    }
    }

}


/*********************************************************************
 *   Copy2Local()                                                    *
 *    Copies a text file to local directory(s).                      *
 *********************************************************************/
void Copy2Local(Global *But, char *FileName)
{
    char    aname[175], fname[175], tname[175], trash[175], whoami[50], string[500];
    int     j, ierr;
    FILE    *in, *out;
    
    sprintf(whoami, " %s: %s: ", But->mod, "Copy2Local");
    if(But->Debug) logit("e", "%s Processing: %s\n", whoami, FileName);
    sprintf(tname,  "%s%s.temp", But->GifDir, FileName);
    sprintf(aname,  "%s%s",      But->GifDir, FileName);
    sprintf(trash,  "%s%s",      But->GifDir, "lastfile");
    
    for(j=0;j<But->nltargets;j++) {
        in  = fopen(aname, "r");
        if(in == 0L) {
            logit("e", "%s Unable to open File: %s\n", whoami, aname);
            return;    
        } else {
            out = fopen(tname, "wb");
            if(out == 0L) {
                logit("e", "%s Unable to open File: %s\n", whoami, tname);    
            } else {
                while(fgets(string, 480, in)!=0L) {
                    fprintf(out, "%s", string);
                }
                fclose(out);
                sprintf(fname,  "%s%s", But->loctarget[j], FileName );
                if(But->Debug) logit("e", "%s Writing: %s\n", whoami, fname);
                ierr = rename(tname, fname); 
            }
            fclose(in);
        }
    }
    ierr = rename(aname, trash);
}


/********************************************************************
 *    Get_Ark is responsible for reading the archive msg.           *
 *                                                                  *
 ********************************************************************/

short xGet_Ark(Arkive *Ark)
{
    char    *in, temp[5];   /* working pointer to archive message    */
    char    line[MAX_STR];  /* to store lines from msg               */
    char    shdw[MAX_STR];  /* to store shadow cards from msg        */
    int     msglen;         /* length of input archive message       */
    int     nline;          /* number of lines (not shadows) so far  */
    int     i, y2k;
    
  /* Initialize some stuff
   ***********************/
    nline  = 0;
    y2k    = 0;
    msglen = strlen( Ark->ArkivMsg );
    in     = Ark->ArkivMsg;
    strncpy( temp, in, 2 );
    temp[2] = 0;
    sscanf(temp, "%d", &i);
    if(i==19 || i==20) y2k = 1;
    
    /* Initialize the ArkList    
    *************************/
    Ark->InList = 0;
    for(i=0;i<Ark->MaxChannels;i++) {
        strcpy(Ark->A[i].Site, " ");
        strcpy(Ark->A[i].Comp, " ");
        strcpy(Ark->A[i].Net,  " ");
        strcpy(Ark->A[i].Loc,  " ");
        Ark->A[i].PPick    = Ark->A[i].SPick = 0.0;
        Ark->A[i].StaList = ' ';
    }
    
   /* Read one data line and its shadow at a time from arkive; process them
    ***********************************************************************/
    i = 0;
    while( in < Ark->ArkivMsg + msglen ) {
        if ( sscanf( in, "%[^\n]", line ) != 1 )  return( -1 );
        in += strlen( line ) + 1;
        if ( sscanf( in, "%[^\n]", shdw ) != 1 )  return( -1 );
        in += strlen( shdw ) + 1;
        nline++;

        /* Process the hypocenter card (1st line of msg) & its shadow
         ************************************************************/
        if( nline == 1 ) {                      /* hypocenter is first line in msg     */
            if(y2k)  read_hypy2k(Ark, line );
            else     read_hyp(   Ark, line );
            continue;
        }

        /* Process all the phase cards & their shadows
         *********************************************/
        if( (int)strlen(line) < (size_t)75 ) break;       /* found the terminator line           */
        if(y2k) read_phsy2k( &(Ark->A[i]), line, shdw);   /* load phase info into Ark structure  */
        else    read_phs(    &(Ark->A[i]), line, shdw);   /* load phase info into Ark structure  */
        i += 1;
        if(i >= Ark->MaxChannels) break;
    }
    Ark->InList = i; 
    return(0);
}


/********************************************************************
 *    Get_Ark is responsible for reading the archive msg.           *
 *                                                                  *
 ********************************************************************/

short Get_Ark(Arkive *Ark)
{
    char    *in, temp[5];   /* working pointer to archive message    */
    char    line[MAX_STR];  /* to store lines from msg               */
    int     msglen;         /* length of input archive message       */
    int     nline;          /* number of lines (not shadows) so far  */
    int     i, y2k;
    
  /* Initialize some stuff
   ***********************/
    nline  = 0;
    y2k    = 0;
    msglen = strlen( Ark->ArkivMsg );
    in     = Ark->ArkivMsg;
    strncpy( temp, in, 2 );
    temp[2] = 0;
    sscanf(temp, "%d", &i);
    if(i==19 || i==20) y2k = 1;
    
    /* Initialize the ArkList    
    *************************/
    Ark->InList = 0;
    for(i=0;i<Ark->MaxChannels;i++) {
        strcpy(Ark->A[i].Site, " ");
        strcpy(Ark->A[i].Comp, " ");
        strcpy(Ark->A[i].Net,  " ");
        strcpy(Ark->A[i].Loc,  " ");
        Ark->A[i].PPick    = Ark->A[i].SPick = 0.0;
        Ark->A[i].StaList = ' ';
    }
    
   /* Read one data line at a time from arkive and process it.
    * Ignore the shadows (if any).
    **********************************************************/
    i = 0;
    while( in < Ark->ArkivMsg + msglen ) {
        if ( sscanf( in, "%[^\n]", line ) != 1 )  return( -1 );
        in += strlen( line ) + 1;
        nline++;
        
        if(line[0] != '$') {
        /* Process the hypocenter card (1st line of msg).
         *************************************************/
			if( nline == 1 ) {                      /* hypocenter is first line in msg     */
				if(y2k)  read_hypy2k(Ark, line );
				else     read_hyp(   Ark, line );
			}
			else {
        /* Process all the phase cards.
         *******************************/
				if( (int)strlen(line) < (size_t)75 ) break;       /* found the terminator line           */
				if(y2k) read_phsy2k( &(Ark->A[i]), line, line);   /* load phase info into Ark structure  */
				else    read_phs(    &(Ark->A[i]), line, line);   /* load phase info into Ark structure  */
				i += 1;
				if(i >= Ark->MaxChannels) break;
			}
        }
    }
    /* Sanity Check  */
    if(Ark->EvntLat == 0.0 || Ark->EvntLon == 0.0) {
		Ark->InList = 0; 
		return(1);
    }
    Ark->InList = i; 
    return(0);
}


/**************************************************************
 * read_hyp() reads the hypocenter line from an archive msg   *
 **************************************************************/
/*------------------------------------------------------------------------
 Sample hypoinverse archive summary line and its shadow.  The summary line
 may be up to 188 characters long.  Its full format is described in 
 documentation (shadow.doc) by Fred Klein.
           10        20        30        40        50        60        70        80        90        100       110       120       130       140       
0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
9701151845332336  148120 3703  605 0 19104  5  37 8971 15222513 13518SLA  57 * 0 132 14424  0125  0 27PMMTWWWD 26X                 1007557D175125Z114  8
$1
  ------------------------------------------------------------------------*/

void read_hyp(Arkive *Ark,  char *hyp)
{
    char   temp[16], datestr[20], subname[] = "read_hyp";
    int    i, j, io;
    float  deg, min;

    Ark->EvntYear  =  (short)DECODE( hyp+0,  2, atof );         /* year of origin time     */
    Ark->EvntMonth =  (short)DECODE( hyp+2,  2, atof );         /* month of origin time    */
    Ark->EvntDay   =  (short)DECODE( hyp+4,  2, atof );         /* day of origin time      */
    Ark->EvntHour  =  (short)DECODE( hyp+6,  2, atof );         /* hour of origin time     */
    Ark->EvntMin   =  (short)DECODE( hyp+8,  2, atof );         /* minute of origin time   */
    Ark->EvntSec   = (float)((DECODE( hyp+10, 4, atof ))/100.0);  /* seconds of origin time  */
    strncpy( temp, hyp, 10 );
    temp[10] = '\0';
    sprintf(datestr, "19%s00.00", temp);                /* origin time without seconds */
    if(epochsec17(&(Ark->EvntTime), datestr) ) {
       logit("", "%s: Error converting origin time: %s\n", subname, datestr );
    }
    Ark->EvntTime += (double)Ark->EvntSec;              /* Time of Event (Julian Sec) */

    deg =  (float)DECODE( hyp+14, 2, atof );
    min = (float)((DECODE( hyp+17, 4, atof ))/100.);
    Ark->EvntLat = (float)(deg + min/60.);                       /* Latitude of Event */
    if( hyp[16]=='S' || hyp[16]=='s' ) Ark->EvntLat = -Ark->EvntLat;

    deg =  (float)DECODE( hyp+21, 3, atof );
    min = (float)((DECODE( hyp+25, 4, atof ))/100.);
    Ark->EvntLon = (float)(deg + min/60.);                       /* Longitude of Event */
    if( hyp[24]=='W' || hyp[24]=='w' || hyp[24]==' ' ) Ark->EvntLon = -Ark->EvntLon;

    Ark->EvntDepth = (float)((DECODE( hyp+29, 5, atof ))/100.);  /* Depth of Event            */
    Ark->Smag      = (float)((DECODE( hyp+34, 2, atof ))/10.);   /* Magnitude from maximum S amplitude from NCSN stations */
    Ark->NumPhs    =  DECODE( hyp+36, 3, atoi );        /* # phases used in solution */
    Ark->Gap       =  DECODE( hyp+39, 3, atoi );        /* Maximum azimuthal gap     */
    Ark->Dmin      =  (float)DECODE( hyp+42, 3, atoi );        /* Minimum distance to site  */
    Ark->rms       = (float)((DECODE( hyp+45, 4, atof ))/100.);  /* RMS travel time residual  */

    Ark->erh       = (float)((DECODE( hyp+80, 4, atof ))/100.);  /* Magnitude of horizontal error        */
    Ark->erz       = (float)((DECODE( hyp+84, 4, atof ))/100.);  /* Magnitude of vertical error          */

    Ark->mag       = (float)((DECODE( hyp+67, 2, atof ))/10.);   /* Coda duration mag                    */
 
    Ark->ExMagType = hyp[114];                          /* "External" mag label or type code    */  
    Ark->ExMag     = (float)((DECODE( hyp+115, 3, atof ))/100.); /* "External" magnitude                 */  
    Ark->AltMagType = hyp[121];                         /* Alt amplitude mag label or type code */  
    Ark->AltMag    = (float)((DECODE( hyp+122, 3, atof ))/100.); /* Alternate amplitude mag              */  
    Ark->PreMagType = hyp[138];                         /* Alt amplitude mag label or type code */  
    Ark->PreMag    = (float)((DECODE( hyp+139, 3, atof ))/100.); /* Alternate amplitude mag              */  
    if(Ark->PreMag != 0.0) Ark->mag = Ark->PreMag;
    
    strcpy(Ark->EvntIDS, "No_ID");
    for(i=128,j=0;i<138;i++) if(hyp[i]!=' ') {j = i;break;}
    if(j) strncpy(Ark->EvntIDS, &hyp[j], 138-j);
    Ark->EvntIDS[138-j] = 0;
    io = sscanf(Ark->EvntIDS, "%d", & Ark->EvntID);
    if(io != 1) {
        logit("e", "%s: io:%d EvntID: <%s> interpreted as: %d\n", 
            subname, io, Ark->EvntIDS, Ark->EvntID);
    }
}


/**************************************************************
 * read_hypy2k() reads the hypocenter line from an archive msg   *
 **************************************************************/
/*------------------------------------------------------------------------
 Sample hypoinverse archive summary line and its shadow.  The summary line
 may be up to 188 characters long.  Its full format is described in 
 documentation (shadow.doc) by Fred Klein.
           10        20        30        40        50        60        70        80        90        100       110       120       130       140       
0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
9701151845332336  148120 3703  605 0 19104  5  37 8971 15222513 13518SLA  57 * 0 132 14424  0125  0 27PMMTWWWD 26X                 1007557D175125Z114  8
$1
  ------------------------------------------------------------------------*/

void read_hypy2k(Arkive *Ark,  char *hyp)
{
    char   datestr[20], subname[] = "read_hypy2k";
    int    i, j, io;
    float  deg, min;

    Ark->EvntYear  =  (short)DECODE( hyp+0,  4, atof );         /* year of origin time     */
    Ark->EvntMonth =  (short)DECODE( hyp+4,  2, atof );         /* month of origin time    */
    Ark->EvntDay   =  (short)DECODE( hyp+6,  2, atof );         /* day of origin time      */
    Ark->EvntHour  =  (short)DECODE( hyp+8,  2, atof );         /* hour of origin time     */
    Ark->EvntMin   =  (short)DECODE( hyp+10, 2, atof );         /* minute of origin time   */
    Ark->EvntSec   = (float)((DECODE( hyp+12, 4, atof ))/100.0);  /* seconds of origin time  */
    strncpy( datestr, hyp, 12 );
    datestr[12] = '\0';
    strcat ( datestr, "00.00" );                         /* origin time without seconds */
    if(epochsec17(&(Ark->EvntTime), datestr) ) {
       logit("", "%s: Error converting origin time: %s\n", subname, datestr );
    }
    Ark->EvntTime += (double)Ark->EvntSec;               /* Time of Event (Julian Sec)  */

    deg = (float)DECODE( hyp+16, 2, atof );
    min = (float)((DECODE( hyp+19, 4, atof ))/100.);
    Ark->EvntLat = (float)(deg + min/60.);                       /* Latitude of Event            */
    if( hyp[18]=='S' || hyp[18]=='s' ) Ark->EvntLat = -Ark->EvntLat;

    deg =  (float)DECODE( hyp+23, 3, atof );
    min = (float)((DECODE( hyp+27, 4, atof ))/100.);
    Ark->EvntLon = (float)(deg + min/60.);                       /* Longitude of Event           */
    if( hyp[26]=='W' || hyp[26]=='w' || hyp[26]==' ' ) Ark->EvntLon = -Ark->EvntLon;

    Ark->EvntDepth = (float)((DECODE( hyp+31, 5, atof ))/100.);  /* Depth of Event               */
    Ark->Smag      = (float)((DECODE( hyp+36, 3, atof ))/100.);  /* Magnitude from maximum S amplitude from NCSN stations */
    Ark->NumPhs    =  DECODE( hyp+39, 3, atoi );        /* # phases used in solution    */
    Ark->Gap       =  DECODE( hyp+42, 3, atoi );        /* Maximum azimuthal gap        */
    Ark->Dmin      =  (float)DECODE( hyp+45, 3, atoi );        /* Minimum distance to site     */
    Ark->rms       = (float)((DECODE( hyp+48, 4, atof ))/100.);  /* RMS travel time residual     */

    Ark->erh       = (float)((DECODE( hyp+85, 4, atof ))/100.);  /* Magnitude of horizontal error        */
    Ark->erz       = (float)((DECODE( hyp+89, 4, atof ))/100.);  /* Magnitude of vertical error          */

    Ark->mag       = (float)((DECODE( hyp+70, 3, atof ))/100.);  /* Coda duration mag                    */  
 
    Ark->ExMagType = hyp[122];                          /* "External" mag label or type code    */  
    Ark->ExMag     = (float)((DECODE( hyp+123, 3, atof ))/100.); /* "External" magnitude                 */  
    Ark->AltMagType = hyp[129];                         /* Alt amplitude mag label or type code */  
    Ark->AltMag    = (float)((DECODE( hyp+130, 3, atof ))/100.); /* Alternate amplitude mag              */  
    Ark->PreMagType = hyp[146];                         /* Alt amplitude mag label or type code */  
    Ark->PreMag    = (float)((DECODE( hyp+147, 3, atof ))/100.); /* Alternate amplitude mag              */  
    if(Ark->PreMag != 0.0) Ark->mag = Ark->PreMag;
    
    strcpy(Ark->EvntIDS, "No_ID");
    for(i=136,j=0;i<146;i++) if(hyp[i]!=' ') {j = i;break;}
    if(j) strncpy(Ark->EvntIDS, &hyp[j], 146-j);
    Ark->EvntIDS[146-j] = 0;
    io = sscanf(Ark->EvntIDS, "%d", & Ark->EvntID);
    if(io != 1) {
        logit("e", "%s: io:%d EvntID: <%s> interpreted as: %d\n", 
            subname, io, Ark->EvntIDS, Ark->EvntID);
    }
}


/***************************************************************
 * read_phs() reads a phase line & its shadow from archive msg *
 ***************************************************************/
/*------------------------------------------------------------------------
Sample HYPOINVERSE station archive card (P-arrival) and a shadow card.
   (phase card is 100 chars; shadow is up to 103 chars):
0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1
PPC  PD0V97 1151845 3500 -21129    0   0   0     0 -27   0  8611400  0    916616 0 185   0 WD  VHZNC

0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1
$   4 3.29 1.80 3.45 1.97 0.08 PSN0    9 PHP2 1334  1 427  3 328  5 108  7  62  9  49  0   0

--------------------------------------------------------------------------*/
void read_phs(ArkInfo *A, char *phs, char *shdw)
{
    char   temp[16], datestr[20], subname[] = "read_phs";
    float  sec;
    int    i;

    A->StaList = '*';
    strncpy( A->Site, phs,    4  );                     /* Site */
    strncat( A->Site, phs+94, 1  );
    A->Site[5] = '\0';
    for(i=0; i<5; i++){      /* get rid of trailing blanks in A->Site */
        if(A->Site[i]==' ') {
            A->Site[i]='\0';
            break;
        }
    }
    strncpy( A->Comp, phs+95, 3);                       /* Component */
    A->Comp[3] = '\0';
    strncpy( A->Net,  phs+98, 2);                       /* Net       */
    A->Net[2]  = '\0';

    strncpy( A->Prem, phs+ 4, 3);                       /* P remark               */
    A->Prem[3]  =  '\0';
    A->PttResid = (float)((DECODE( phs+24,  4, atof ))/100.0);   /* P travel-time residual */
    A->Pdelay   = (float)((DECODE( phs+50,  4, atof ))/100.0);   /* P travel-time delay    */

    strncpy( A->Srem, phs+35, 2 );                      /* S remark               */
    A->Srem[2]  =  '\0';
    A->SttResid = (float)((DECODE( phs+40,  4, atof ))/100.0);   /* S travel-time residual */
    A->Sdelay   = (float)((DECODE( phs+54,  4, atof ))/100.0);   /* S travel-time delay    */

    strncpy( temp,  phs+9,  10 );                               
    temp[10]='\0';
    sprintf(datestr, "19%s00.00", temp);                /* arrival time without seconds */
    if( phs[21] == '.' ) sec =  (float)DECODE( phs+19, 5, atof );
    else                 sec = (float)((DECODE( phs+19, 5, atof ))/100.0);
    if(epochsec17(&(A->SPick), datestr ) ) {
      printf( "%s: Error converting arrival time: %s\n", subname, datestr );
    }
    A->PPick = A->SPick;
    A->PPick = (sec==0.0)? 0.0:A->PPick+sec;            /* PPick */
    
    if( phs[33] == '.' ) sec =  (float)DECODE( phs+31, 5, atof );
    else                 sec = (float)((DECODE( phs+31, 5, atof ))/100.0);
    A->SPick = (sec==0.0)? 0.0:A->SPick+sec;            /* SPick */
    
    A->Dist      = (float)((DECODE( phs+58,  4, atof ))/10.0);   /* Epicentral distance (km)             */
    A->Emerg     =  (float)DECODE( phs+62,  3, atof );         /* Emergence angle at source            */
    
    A->CodaDur   =  (float)DECODE( phs+71,  4, atof );         /* Coda duration (sec)                  */
    A->Azim      =  (float)DECODE( phs+75,  3, atof );         /* Azimuth to station in deg. E of N.   */
    A->DataSrc   =  phs[91];
    A->fm        =  phs[6];                             /* First motion                         */
    A->Pwgt      =  (int)DECODE( phs+7,  1, atof );          /* Coda duration (sec)                  */
    if(A->Pwgt >= 5) A->StaList = ' ';    /* Don't plot deleted picks                  */
    
/*    i  =  DECODE( shdw+35, 5, atol );  We ignore the shadow!  */                 /* Coda duration time (sec)             */
}


/******************************************************************
 * read_phsy2k() reads a phase line & its shadow from archive msg *
 ******************************************************************/
/*------------------------------------------------------------------------
Sample HYPOINVERSE station archive card (P-arrival) and a shadow card.
   (phase card is 100 chars; shadow is up to 103 chars):
0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1
KCT  NC 4HHZ  PD0200302230607 2607  -5253    0   0   0      0 0  0  15   0 110 9100  0   60251300  0 977   0WD 
LHV  NN VSHZ  PD0200404071938 1850   3170    0   0   0      0 0  0  15   0 11110900  0   43281268  0 964   0WD    0

0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1
$   6 4.85 1.80 4.62 1.46 0.15 NSR0  -33 PHP014802 131096 15 814 19 351 23 293 31 409 33 258

--------------------------------------------------------------------------*/
void read_phsy2k(ArkInfo *A, char *phs, char *shdw)
{
    char   datestr[20], subname[] = "read_phsy2k";
    float  sec;
    int    i;

    A->StaList = '*';
    strncpy( A->Site, phs,    5  );                     /* Site */
    A->Site[5] = '\0';
    for(i=0; i<5; i++){      /* get rid of trailing blanks in A->Site */
        if(A->Site[i]==' ') {
            A->Site[i]='\0';
            break;
        }
    }
    strncpy( A->Comp, phs+9, 3);                        /* Component */
    A->Comp[3] = '\0';
    strncpy( A->Net,  phs+5, 2);                        /* Net */
    A->Net[2]  = '\0';

    strncpy( A->Loc, "--", 2);
    strncpy( A->Loc,  phs+111, 2);                      /* Loc */
    A->Loc[2]  = '\0';

    strncpy( A->Prem, phs+13, 3);                       /* P remark (&first motion) */
    A->Prem[3]  =  '\0';
    A->PttResid = (float)((DECODE( phs+34,  4, atof ))/100.0);   /* P travel-time residual */
    A->Pdelay   = (float)((DECODE( phs+66,  4, atof ))/100.0);   /* P travel-time delay    */

    strncpy( A->Srem, phs+46, 2 );                      /* S remark               */
    A->Srem[2]  =  '\0';
    A->SttResid = (float)((DECODE( phs+50,  4, atof ))/100.0);   /* S travel-time residual */
    A->Sdelay   = (float)((DECODE( phs+70,  4, atof ))/100.0);   /* S travel-time delay    */

    strncpy( datestr,  phs+17,  12 );                   /* arrival time without seconds */
    datestr[12]='\0';
    strcat ( datestr, "00.00" );
    if( phs[31] == '.' ) sec =  (float)DECODE( phs+29, 5, atof );
    else                 sec = (float)((DECODE( phs+29, 5, atof ))/100.0);
    if(epochsec17(&(A->SPick), datestr ) ) {
      printf( "%s: Error converting arrival time: %s\n", subname, datestr );
    }
    A->PPick = A->SPick;
    A->PPick = (sec==0.0)? 0.0:A->PPick+sec;            /* PPick */
    
    if( phs[43] == '.' ) sec =  (float)DECODE( phs+41, 5, atof );
    else                 sec = (float)((DECODE( phs+41, 5, atof ))/100.0);
    A->SPick = (sec==0.0)? 0.0:A->SPick+sec;            /* SPick */
    
    A->Dist      = (float)((DECODE( phs+74,  4, atof ))/10.0);   /* Epicentral distance (km)             */
    A->Emerg     =  (float)DECODE( phs+78,  3, atof );         /* Emergence angle at source            */
    
    A->CodaDur   =  (float)DECODE( phs+87,  4, atof );         /* Coda duration (sec)                  */
    A->Azim      =  (float)DECODE( phs+91,  3, atof );         /* Azimuth to station in deg. E of N.   */
    A->DataSrc   =  phs[108];
    A->fm        =  phs[15];                            /* First motion                         */
    A->Pwgt      =  (int)DECODE( phs+16,  1, atof );    /* Coda duration (sec)                  */
    if(A->Pwgt >= 5) A->StaList = ' ';    /* Don't plot deleted picks                  */
    
/*    i  =  DECODE( shdw+35, 5, atol );  We ignore the shadow!  */                 /* Coda duration time (sec)             */
}


/********************************************************************
 *    Build_Table is responsible for setting up the ToDo list.      *
 *                                                                  *
 ********************************************************************/

short Build_Table(Global *But, Arkive *Ark, long *InListToDo)
{
    char    whoami[50];
    double  offset[MAXCHANNELS];
    int     i, j, k, kk, error, flag, stime_flag;
    double  dist, lat, lon, elev, dlat, dlon, faz, baz, t0, t1;
    
    sprintf(whoami, " %s: %s: ", But->mod, "Build_Table");
    *InListToDo = 0;
    stime_flag = error = 0;
    But->MaxDistance = But->MaxDist;
    
    /* The following may need to be changed for low-attenuation regions */
    if(Ark->mag < 1.0) But->MaxDistance = 150;
    if(Ark->mag < 2.0) But->MaxDistance = 200;
    if(Ark->mag < 3.0) But->MaxDistance = 250;
    
    for(i=0;i<Ark->InList;i++) {
        Put_Sta_Info(But, &Ark->A[i]);
    }
    
    if(But->MaxDistance > But->MaxDist) But->MaxDistance = But->MaxDist;
    if(But->Use_all_Sites || Ark->mag<2.0 || Ark->InList<5) {  /* Fill out the Ark list with unpicked entries from the station list */
        if(But->Debug) logit("e", "%s Using all sites.\n", whoami);
        lat = Ark->EvntLat;    lon = Ark->EvntLon;
        for(i=0;i<But->NSCN;i++) {
            if( Ark->InList >= MAXCHANNELS ) {
                logit("e", 
                    "%s Site table full; cannot load entire file.\n", whoami );
                logit("e", 
                    "%s Use <maxsite> command to increase table size; exiting!\n", whoami );
                break;
            } 
            else {
                dlat = But->Chan[i].Lat;
                dlon = But->Chan[i].Lon;
                elev = But->Chan[i].Elev;
                distaz(lat, lon, dlat, dlon, &dist, &faz, &baz);
                if(dist < But->MaxDistance) {
                    flag = 0;
                    for(kk=0;kk<But->ncomp;kk++) {
                        if(strcmp(But->Chan[i].Comp, But->Comp[kk]) == 0) flag = 1; 
                    }
                    
                    if(flag) {
	                    if(No_Dups(Ark, But->Chan[i].Site, But->Chan[i].Comp, But->Chan[i].Net, But->Chan[i].Loc)) {    
	                        j = Ark->InList;
	                        strcpy(Ark->A[j].Site, But->Chan[i].Site);
	                        strcpy(Ark->A[j].Comp, But->Chan[i].Comp);
	                        strcpy(Ark->A[j].Net,  But->Chan[i].Net);
	                        strcpy(Ark->A[j].Loc,  But->Chan[i].Loc);
	                        strcpy(Ark->A[j].Prem,  " ");
	                        strcpy(Ark->A[j].Srem,  " ");
	                        Ark->A[j].Dist = (float)dist;
	                        Ark->A[j].sensitivity = But->Chan[i].sensitivity;
	                        Ark->A[j].Sens_unit = But->Chan[i].Sens_unit;
	                        Ark->A[j].PPick = Ark->EvntTime + dist/6.00;
	                        Ark->A[j].SPick = Ark->EvntTime + dist/3.46;
	                        Ark->A[j].PttResid = 0.0;
	                        Ark->A[j].SttResid = 0.0;
	                        Ark->A[j].Emerg = 0.0;
	                        Ark->A[j].CodaDur = 0.0;
	                        Ark->A[j].Azim = (float)faz;
	                        Ark->InList += 1;
	                    } 
                    } 
                }
            }
        }
    }
    
    /* Set up the arrays for selecting and sorting *
     ***********************************************/
    for(i=0;i<Ark->InList;i++) {
        offset[i] = (Ark->A[i].Dist>0.0 && Ark->A[i].Dist< But->MaxDistance)? Ark->A[i].Dist:30000.0; 
        Ark->index[i] = i;
    }
    
    /* Go get the needed info *
     **************************/
    if(But->Debug > 1) logit("e", "%s Get the %d requested traces.\n", whoami, Ark->InList);
    j = 0;
    for(i=0;i<Ark->InList;i++)  {
        if( j >= Ark->MaxChannels ) {    /* Make sure internal site table has room left */
           logit("e", "%s Site table full <%d>; cannot load entire arkive file\n", whoami, j);
           logit("e", "        Use <MaxSite> command to increase table size; exiting!\n");
           return 2;
        }

        if(offset[i] < But->MaxDistance) {
            flag = 0;
            for(kk=0;kk<But->ncomp;kk++) {
                if(strcmp(Ark->A[i].Comp, But->Comp[kk]) == 0) flag = 1; 
            }
                            
           /*
            if(flag && In_Menu_list(But, Ark->A[i].Site, Ark->A[i].Comp, Ark->A[i].Net, Ark->A[i].Loc )) {  
                Ark->A[i].Stime = Ark->EvntTime - But->Pre_Event;   
                Sort_Servers (But, Ark->A[i].Stime);
                if(But->nentries > 0) {
                    Ark->A[i].Duration = But->Secs_Screen + 5.0 + Ark->A[i].Dist/6;
                    if(Ark->A[i].Stime + Ark->A[i].Duration > But->TEtime[But->index[0]]) 
                        Ark->A[i].Duration = But->TEtime[But->index[0]] - Ark->A[i].Stime;
                    j += 1;
                    *InListToDo = j;
                }
            }
            */
            if(flag && In_Menu(But, Ark->A[i].Site, Ark->A[i].Comp, Ark->A[i].Net, Ark->A[i].Loc, &t0, &t1)) {    
                Ark->A[i].Stime = Ark->EvntTime - But->Pre_Event;   /* Earliest time needed. */
                if(Ark->A[i].Stime > t0 && Ark->A[i].Stime < t1) {
                    Ark->A[i].Duration = (float)(But->Secs_Screen + 5.0 + Ark->A[i].Dist/6);   /* Time Reduced/6             */
                    if(Ark->A[i].Stime + Ark->A[i].Duration > t1) 
                        Ark->A[i].Duration = (float)(t1 - Ark->A[i].Stime);
                    
                    j += 1;
                    *InListToDo = j;
                }
            } else 
                offset[i] = 30000.0;
        } else 
            offset[i] = 30000.0;
    }
    if(*InListToDo==0) return 1;
    Sort_Ark (Ark->InList, offset, &Ark->index[0]);
    
    if(But->Debug) {
        logit("e", "%s The following %d of %d requested traces are available:\n Site:  Net:  Comp:  Loc:   Dist:   Picked:\n", 
              whoami, *InListToDo, Ark->InList);
        for(i=0;i<(*InListToDo);i++)  {
            k = Ark->index[i];
            logit("e", " %4s    %2s    %3s    %2s %8.3f       %c \n", 
                Ark->A[k].Site, Ark->A[k].Net, Ark->A[k].Comp, Ark->A[k].Loc, 
                Ark->A[k].Dist, Ark->A[k].StaList);
        }
        logit("e", "%s Md: %f\n", whoami, Ark->mag);
    }
    return error;
}

/*************************************************************************
 *   short No_Dups (sta,comp,net,loc)                                    *
 *      Determines if the scn is in the Arkive list                      *
 *************************************************************************/

short No_Dups (Arkive *Arkp, char *sta, char *comp, char *net, char *loc)
{
    int    i;
      
    for(i=0;i<Arkp->InList;i++) {
       if(strcmp(Arkp->A[i].Site, sta )==0 && 
          strcmp(Arkp->A[i].Comp, comp)==0 && 
          strcmp(Arkp->A[i].Net,  net )==0 &&
          strcmp(Arkp->A[i].Loc,  loc )==0) {
          return 0;
       }
    }
    return 1;
}


/*************************************************************************
 *   void Sort_Ark (n,dist,indx)                                         *
 *   --THIS is A VERSION OF THE HEAPSORT SUBROUTINE FROM THE NUMERICAL   *
 *     RECIPES BOOK. dist is REARRANGED IN ASCENDING ORDER &             *
 *     indx is PUT IN THE SAME ORDER AS dist.                            *
 *   long        n       !THE NUMBER OF VALUES OF dist TO BE SORTED      *
 *   FLOAT       dist(n) !SORT THIS ARRAY IN ASCENDING ORDER             *
 *   long        indx(n) !PASSIVELY REARRANGE THIS ARRAY TOO             *
 *************************************************************************/

void Sort_Ark (long n, double *dist, long *indx)
{
    long    indxh, ii, ir, i, j;
    double  disth;
    
    ii = n/2+1;    ir = n;
    
    while(1) {    
        if (ii > 1) {
            ii -= 1;
            disth      = dist[ii-1];   indxh      = indx[ii-1];
        }
        else {
            disth      = dist[ir-1];   indxh      = indx[ir-1];
            dist[ir-1] = dist[0];      indx[ir-1] = indx[0];
            ir -= 1;
            if(ir == 1) {
                dist[0] = disth;       indx[0]    = indxh;
                return;
            }
        }
        i = ii;   j = ii+ii;
        
        while (j <= ir) {
            if (j < ir && dist[j-1] < dist[j]) j += 1;
            
            if (disth < dist[j-1]) {
                dist[i-1] = dist[j-1]; indx[i-1] = indx[j-1];
                i = j;   j = j+j;
            }
            else    j = ir+1;
        }
        dist[i-1] = disth;   indx[i-1] = indxh;
    }
}


/**********************************************************************
 * Make_Time : Encode time from string of form:                       *
 *     HH:MM:SS.SS MM/DD/YYYY to seconds since 1970                   *
 *                                                                    *
 **********************************************************************/
void Make_Time( double *secs, char *string)
{
    struct Greg    g;
    char   *token, str[10];
    double sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's     */
                                      /* time 0 and 1970-01-01 00:00:00.0 GMT */
    
    token = strstr(string, ":");
    strncpy(str, token-2, (size_t)2);
    str[2] = 0;
    sscanf( str, "%d", &g.hour);
    strncpy(str, token+1, (size_t)2);
    sscanf( str, "%d", &g.minute);
    strncpy(str, token+4, (size_t)5);
    str[5] = 0;
    sscanf( str, "%f", &g.second);
    
    token = strstr(token, "/");
    strncpy(str, token-2, (size_t)2);
    str[2] = 0;
    sscanf( str, "%d", &g.month);
    strncpy(str, token+1, (size_t)2);
    sscanf( str, "%d", &g.day);
    strncpy(str, token+4, (size_t)4);
    str[4] = 0;
    sscanf( str, "%d", &g.year);
    
    *secs    = 60.0 * (double) julmin(&g) - sec1970;
}


/*************************************************************************
 *   short Build_Menu ()                                                 *
 *      Builds the waveserver's menu                                     *
 *************************************************************************/

short Build_Menu (Global *But)
{
    char    whoami[50], server[100];
    int     j, retry, ret, rc;
    WS_PSCNL scnp;
    WS_MENU menu; 
      
    /* Build the current wave server menus *
     ***************************************/

    sprintf(whoami, " %s: %s: ", But->mod, "Build_Menu");
    But->got_a_menu = 0;
    
    for(j=0;j<But->nServer;j++) But->index[j]  = j;
    
    for (j=0;j< But->nServer; j++) {
        retry = 0;
        But->inmenu[j] = 0;
        sprintf(server, " %s:%s <%s>", But->wsIp[j], But->wsPort[j], But->wsComment[j]);
        if ( But->wsIp[j][0] == 0 ) continue;
    Append:
        
        ret = wsAppendMenu(But->wsIp[j], But->wsPort[j], &But->menu_queue[j], But->wsTimeout);
        
        if (ret == WS_ERR_NO_CONNECTION) { 
            if(But->Debug) 
                logit("e","%s Could not get a connection to %s to get menu.\n", whoami, server);
        }
        else if (ret == WS_ERR_SOCKET) 
            logit("e","%s Could not create a socket for %s\n", whoami, server);
        else if (ret == WS_ERR_BROKEN_CONNECTION) {
            logit("e","%s Connection to %s broke during menu\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_TIMEOUT) {
            logit("e","%s Connection to %s timed out during menu.\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_MEMORY) 
            logit("e","%s Waveserver %s out of memory.\n", whoami, server);
        else if (ret == WS_ERR_INPUT) {
            logit("e","%s Connection to %s input error\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_PARSE) 
            logit("e","%s Parser failed for %s\n", whoami, server);
        else if (ret == WS_ERR_BUFFER_OVERFLOW) 
            logit("e","%s Buffer overflowed for %s\n", whoami, server);
        else if (ret == WS_ERR_EMPTY_MENU) 
            logit("e","%s Unexpected empty menu from %s\n", whoami, server);
        else if (ret == WS_ERR_NONE) {
            But->inmenu[j] = But->got_a_menu = 1;
        }
        else logit("e","%s Connection to %s returns error: %d\n", 
                   whoami, server, ret);
    }
    /* Let's make sure that servers in our server list have really connected.
       **********************************************************************/  
    for(j=0;j<But->nServer;j++) {
        if ( But->inmenu[j] ) {
            rc = wsGetServerPSCNL( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
            if ( rc == WS_ERR_EMPTY_MENU ) {
                logit("e","%s Empty menu.\n", whoami);
                But->inmenu[j] = 0;
            }
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                logit("e","%s %s not in menu.\n", whoami, But->wsIp[j]);
                But->inmenu[j] = But->wsIp[j][0] = 0;
            }
        /* Then, detach 'em and let RequestWave attach only the ones it needs.
           **********************************************************************/  
            menu = But->menu_queue[j].head;
            if ( menu->sock > 0 ) wsDetachServer( menu );
        }
    }
    return 0;
}


/*************************************************************************
 *   short In_Menu (sta,comp,net,loc)                                    *
 *      Determines if the scnl is in the waveserver's menu               *
 *************************************************************************/

int In_Menu (Global *But, char *sta, char *comp, char *net, char *Loc, 
                double *tankStarttime, double *tankEndtime)
{
    char    whoami[50];
    int     i, rc;
    WS_PSCNL scnp;
      
    sprintf(whoami, " %s: %s: ", But->mod, "In_Menu");
    for(i=0;i<But->nServer;i++) {
        if(But->inmenu[i]) {
            rc = wsGetServerPSCNL( But->wsIp[i], But->wsPort[i], &scnp, &But->menu_queue[i]);    
            if ( rc == WS_ERR_EMPTY_MENU )         continue;
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                if(But->Debug) logit("e","%s  %s:%s not in menu.\n", whoami, But->wsIp[i], But->wsPort[i]);
                But->inmenu[i] = 0;
                continue;
            }

			But->wsLoc[i] = (strlen(scnp->loc))? 1:0;
            while ( 1 ) {
               if(strcmp(scnp->sta,  sta )==0 && 
                  strcmp(scnp->chan, comp)==0 && 
                  strcmp(scnp->net,  net )==0) {
					if(strcmp(scnp->loc,  Loc )==0 || But->wsLoc[i]==0)  {  
						*tankStarttime = scnp->tankStarttime;
						*tankEndtime   = scnp->tankEndtime;
						return 1;
					}
				}
                if(comp[0] == 'T' && scnp->chan[0] == 'T') {
                    logit("e", "%s A Time channel.  <%s> <%s> <%s> :: <%s> <%s> <%s>\n", 
                        whoami, sta, comp, net, scnp->sta, scnp->chan, scnp->net );
                }
               if ( scnp->next == NULL )
                  break;
               else
                  scnp = scnp->next;
            }
        }
    }
    return 0;
}


/*************************************************************************
 *   In_Menu_list                                                        *
 *      Determines if the scnl is in the waveservers' menu.              *
 *      If there, the tank starttime and endtime are returned.           *
 *      Also, the Server IP# and port are returned.                      *
 *************************************************************************/

int In_Menu_list (Global *But, char *Site, char *Comp, char *Net, char *Loc)
{
    char    whoami[50], server[100], sport[30], SCNtxt[25];
    long    j, rc;
    WS_PSCNL scnp;
    
    sprintf(whoami, " %s: %s: ", But->mod, "In_Menu_list");
    sprintf(SCNtxt, " %s %s %s %s", Site, Comp, Net, Loc);
    But->nentries = 0;
    for(j=0;j<But->nServer;j++) {
        if(But->inmenu[j]) {
            sprintf(sport, " %s:%s", But->wsIp[j], But->wsPort[j]);
            sprintf(server, " %-21s <%s>", sport, But->wsComment[j]);
            rc = wsGetServerPSCNL( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
            if ( rc == WS_ERR_EMPTY_MENU ) {
                if(But->Debug) logit("e","%s Empty menu for %s \n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                if(But->Debug) logit("e","%s  %s not in menu.\n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }

			But->wsLoc[j] = (strlen(scnp->loc))? 1:0;
            while ( 1 ) {
               if(strcmp(scnp->sta,  Site)==0 && 
                  strcmp(scnp->chan, Comp)==0 && 
                  strcmp(scnp->net,  Net )==0) {
					if(strcmp(scnp->loc,  Loc )==0 || But->wsLoc[j]==0)  {  
						But->TStime[j] = scnp->tankStarttime;
						But->TEtime[j] = scnp->tankEndtime;
						But->index[But->nentries]  = j;
				/*  	if(But->Debug) 
							logit("e","%s  %s Found %s %s %s %s in %s.\n", 
									whoami, server, Site, Comp, Net, Loc, But->wsComment[j]); */
						But->nentries += 1;
					}
               }
               if ( scnp->next == NULL )
                  break;
               else
                  scnp = scnp->next;
            }
        }
    }
    if(But->nentries>0) return 1;
    return 0;
}


/********************************************************************
 *  RequestWave                                                     *
 *   This is the binary version                                     *
 *   k - waveserver index                                           *
 ********************************************************************/
int RequestWave(Global *But, int k, double *Data,  
            char *Site, char *Comp, char *Net, char *Loc, double Stime, double Duration)
{
    char     whoami[50], SCNtxt[17];
    int      i, ret, iEnd, npoints, gap0;
    double   mean;
    TRACE_REQ   request;
    GAP *pGap;
    
    sprintf(whoami, " %s: %s: ", But->mod, "RequestWave");
    strcpy(request.sta,  Site);
    strcpy(request.chan, Comp);
    strcpy(request.net,  Net );
    strcpy(request.loc,  Loc );   
    request.waitSec = 0;
    request.pinno = 0;
    request.reqStarttime = Stime;
    request.reqEndtime   = request.reqStarttime + Duration;
    request.partial = 1;
    request.pBuf    = But->TraceBuf;
    request.bufLen  = MAXTRACELTH*9;
    request.timeout = But->wsTimeout;
    request.fill    = 919191;
    sprintf(SCNtxt, "%s %s %s %s", Site, Comp, Net, Loc);

    /* Clear out the gap list */
    if(pTrace.nGaps != 0) {
        while ( (pGap = pTrace.gapList) != (GAP *)NULL) {
            pTrace.gapList = pGap->next;
            free(pGap);
        }
    }
    pTrace.nGaps = 0;

    if(But->wsLoc[k]==0) {
	    ret = WSReqBin(But, k, &request, &pTrace, SCNtxt);
    } else {
	    ret = WSReqBin2(But, k, &request, &pTrace, SCNtxt);
    }
    
  /* Find mean value of non-gap data */
    pGap = pTrace.gapList;
    i = npoints = 0L;
    mean    = 0.0;
  /*
   * Loop over all the data, skipping any gaps. Note that a `gap' will not be declared
   * at the end of the data, so the counter `i' will always get to pTrace.nRaw.
   */
    do {
        iEnd = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->firstSamp - 1;
        if (pGap != (GAP *)NULL) { /* Test for gap within peak-search window */
            gap0 = pGap->lastSamp - pGap->firstSamp + 1;
            if(Debug) logit("t", "trace from <%s> has %d point gap in window at %d\n", SCNtxt, gap0, pGap->firstSamp);
        }
        for (; i < iEnd; i++) {
            mean += pTrace.rawData[i];
            npoints++;
        }
        if (pGap != (GAP *)NULL) {     /* Move the counter over this gap */    
            i = pGap->lastSamp + 1;
            pGap = pGap->next;
        }
    } while (i < pTrace.nRaw );
  
    mean /= (double)npoints;
  
  /* Now remove the mean, and set points inside gaps to zero */
    i = 0;
    do {
        iEnd = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->firstSamp - 1;
        for (; i < iEnd; i++) pTrace.rawData[i] -= mean;

        if (pGap != (GAP *)NULL) {    /* Fill in the gap with zeros */    

            for ( ;i < pGap->lastSamp + 1; i++) pTrace.rawData[i] = 0.0;

            pGap = pGap->next;
        }
    } while (i < pTrace.nRaw );

    But->Npts = pTrace.nRaw;
    if(But->Npts>MAXTRACELTH) {
        logit("e","%s Trace: %s Too many points: %d Trimmed to %d.\n", whoami, SCNtxt, But->Npts, MAXTRACELTH);
        But->Npts = MAXTRACELTH;
    }
    for(i=0;i<But->Npts;i++) Data[i] = pTrace.rawData[i];
    But->Mean = 0.0;
    if(pTrace.nGaps > 500) ret = 4;

    return ret;
}

/********************************************************************
 *  WSReqBin                                                        *
 *                                                                  *
 *   k - waveserver index                                           *
 ********************************************************************/
int WSReqBin(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt)
{
    char     server[wsADRLEN*3], whoami[50];
    int      i, kk, io, retry;
    int      isamp, nsamp, gap0, success, ret, WSDebug = 0;          
    long     iEnd, npoints;
    WS_MENU  menu = NULL;
    WS_PSCNL  pscn = NULL;
    double   mean, traceEnd, samprate;
    long    *longPtr;
    short   *shortPtr;
    
    TRACE_HEADER *pTH;
    TRACE_HEADER *pTH4;
    char tbuf[MAX_TRACEBUF_SIZ];
    GAP *pGap, *newGap;
    
    sprintf(whoami, " %s: %s: ", But->mod, "WSReqBin");
    WSDebug = But->WSDebug;
    success = retry = 0;
    
gettrace:
    menu = NULL;
    /*    Put out WaveServer request here and wait for response */
    /* Get the trace
     ***************/
    /* rummage through all the servers we've been told about */
    if ( (wsSearchSCN( request, &menu, &pscn, &But->menu_queue[k]  )) == WS_ERR_NONE ) {
        strcpy(server, menu->addr);
        strcat(server, "  ");
        strcat(server, menu->port);
    } else {
        strcpy(server, "unknown");
    }
    
/* initialize the global trace buffer, freeing old GAP structures. */
    pTrace->nRaw  = 0L;
    pTrace->delta     = 0.0;
    pTrace->starttime = 0.0;
    pTrace->endtime   = 0.0;
    pTrace->nGaps = 0;

    /* Clear out the gap list */
    pTrace->nGaps = 0;
    while ( (pGap = pTrace->gapList) != (GAP *)NULL) {
        pTrace->gapList = pGap->next;
        free(pGap);
    }
 
    if(WSDebug) {     
        logit("e","\n%s Issuing request to wsGetTraceBin: server: %s Socket: %d.\n", 
             whoami, server, menu->sock);
        logit("e","    %s %f %f %d\n", SCNtxt,
             request->reqStarttime, request->reqEndtime, request->timeout);
    }

    io = wsGetTraceBin(request, &But->menu_queue[k], But->wsTimeout);
    
    if (io == WS_ERR_NONE ) {
        if(WSDebug) {
            logit("e"," %s server: %s trace %s: went ok first time. Got %ld bytes\n",
                whoami, server, SCNtxt, request->actLen); 
            logit("e","        actStarttime=%lf, actEndtime=%lf, actLen=%ld, samprate=%lf\n",
                  request->actStarttime, request->actEndtime, request->actLen, request->samprate);
        }
    }
    else {
        switch(io) {
        case WS_ERR_EMPTY_MENU:
        case WS_ERR_SCNL_NOT_IN_MENU:
        case WS_ERR_BUFFER_OVERFLOW:
            if (io == WS_ERR_EMPTY_MENU ) 
                logit("e"," %s server: %s No menu found.  We might as well quit.\n", whoami, server); 
            if (io == WS_ERR_SCNL_NOT_IN_MENU ) 
                logit("e"," %s server: %s Trace %s not in menu\n", whoami, server, SCNtxt);
            if (io == WS_ERR_BUFFER_OVERFLOW ) 
                logit("e"," %s server: %s Trace %s overflowed buffer. Fatal.\n", whoami, server, SCNtxt); 
            return 2;                /*   We might as well quit */
        
        case WS_ERR_PARSE:
        case WS_ERR_TIMEOUT:
        case WS_ERR_BROKEN_CONNECTION:
        case WS_ERR_NO_CONNECTION:
            sleep_ew(500);
            retry += 1;
            if (io == WS_ERR_PARSE )
                logit("e"," %s server: %s Trace %s: Couldn't parse server's reply. Try again.\n", 
                        whoami, server, SCNtxt); 
            if (io == WS_ERR_TIMEOUT )
                logit("e"," %s server: %s Trace %s: Timeout to wave server. Try again.\n", whoami, server, SCNtxt); 
            if (io == WS_ERR_BROKEN_CONNECTION ) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Broken connection to wave server. Try again.\n", 
                whoami, server, SCNtxt); 
            }
            if (io == WS_ERR_NO_CONNECTION ) {
                if(WSDebug || retry>1) 
                    logit("e"," %s server: %s Trace %s: No connection to wave server.\n", whoami, server, SCNtxt); 
                if(WSDebug) logit("e"," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
            }
            ret = wsAttachServer( menu, But->wsTimeout );
            if(ret == WS_ERR_NONE && retry < But->RetryCount) goto gettrace;
            if(WSDebug) {   
                switch(ret) {
                case WS_ERR_NO_CONNECTION:
                    logit("e"," %s server: %s: No connection to wave server.\n", whoami, server); 
                    break;
                case WS_ERR_SOCKET:
                    logit("e"," %s server: %s: Socket error.\n", whoami, server); 
                    break;
                case WS_ERR_INPUT:
                    logit("e"," %s server: %s: Menu missing.\n", whoami, server); 
                    break;
                default:
                    logit("e"," %s server: %s: wsAttachServer error %d.\n", whoami, server, ret); 
                }
            }
            return 2;                /*   We might as well quit */
        
        case WS_WRN_FLAGGED:
            if((int)strlen(&request->retFlag)>0) {
                if(WSDebug)  
                logit("e","%s server: %s Trace %s: return flag from wsGetTraceBin: <%c>\n %.50s\n", 
                        whoami, server, SCNtxt, request->retFlag, But->TraceBuf);
            }
            if(WSDebug) logit("e"," %s server: %s Trace %s: No trace available. Wave server returned %c\n", 
                whoami, server, SCNtxt, request->retFlag); 
            
            return 2;              /*   break;  We might as well quit */
        
        default:
            logit( "et","%s server: %s Failed.  io = %d\n", whoami, server, io );
        }
    }
    
    /* Transfer trace data from TRACE_BUF packets into Trace buffer */
    traceEnd = (request->actEndtime < request->reqEndtime) ?  request->actEndtime :
                                                              request->reqEndtime;
    pTH  = (TRACE_HEADER *)request->pBuf;
    pTH4 = (TRACE_HEADER *)tbuf;
    /*
    * Swap to local byte-order. Note that we will be calling this function
    * twice for the first TRACE_BUF packet; this is OK as the second call
    * will have no effect.
    */

    memcpy( pTH4, pTH, sizeof(TRACE_HEADER) );
    if(WSDebug) logit( "e","%s server: %s Make Local\n", whoami, server, io );
    if (WaveMsgMakeLocal(pTH4) == -1) {
        logit("et", "%s server: %s %s.%s.%s unknown datatype <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
        return 5;
    }
    if (WaveMsgMakeLocal(pTH4) == -2) {
        logit("et", "%s server: %s %s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
    /*  return 5;  */
    }

    if(WSDebug) logit("e"," %s server: %s Trace %s: Data has samprate %g.\n", 
                     whoami, server, SCNtxt, pTH4->samprate); 
    if (pTH4->samprate < 0.1) {
        logit("et", "%s server: %s %s.%s.%s (%s) has zero samplerate (%g); skipping trace\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, SCNtxt, pTH4->samprate);
        return 5;
    }
    But->samp_sec = pTH4->samprate<=0? 100L:(long)pTH4->samprate;

    pTrace->delta = 1.0/But->samp_sec;
    samprate = But->samp_sec;   /* Save rate of first packet to compare with later packets */
    pTrace->starttime = request->reqStarttime;
    /* Set Trace endtime so it can be used to test for gap at start of data */
    pTrace->endtime = ( (pTH4->starttime < request->reqStarttime) ?
                        pTH4->starttime : request->reqStarttime) - 0.5*pTrace->delta ;
    if(WSDebug) logit("e"," pTH->starttime: %f request->reqStarttime: %f delta: %f endtime: %f.\n", 
                     pTH4->starttime, request->reqStarttime, pTrace->delta, pTrace->endtime); 

  /* Look at all the retrieved TRACE_BUF packets 
   * Note that we must copy each tracebuf from the big character buffer
   * to the TRACE_BUF structure pTH4.  This is because of the occasionally 
   * seen case of a channel putting an odd number of i2 samples into
   * its tracebufs!  */
    kk = 0;
    while( pTH < (TRACE_HEADER *)(request->pBuf + request->actLen) ) {
        memcpy( pTH4, pTH, sizeof(TRACE_HEADER) );
         /* Swap bytes to local order */
	    if (WaveMsgMakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsgMakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
	    /*  return 5;  */
        }
    
        nsamp = pTH4->nsamp;
        memcpy( pTH4, pTH, sizeof(TRACE_HEADER) + nsamp*4 );
         /* Swap bytes to local order */
	    if (WaveMsgMakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsgMakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
	    /*  return 5;  */
        }
    
        if ( fabs(pTH4->samprate - samprate) > 1.0) {
            logit("et", "%s <%s.%s.%s samplerate change: %f - %f; discarding trace\n",
                whoami, pTH4->sta, pTH4->chan, pTH4->net, samprate, pTH4->samprate);
            return 5;
        }
    
    /* Check for gap */
        if (pTrace->endtime + 1.5 * pTrace->delta < pTH4->starttime) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Gap detected.\n", 
                        whoami, server, SCNtxt); 
            if ( (newGap = (GAP *)calloc(1, sizeof(GAP))) == (GAP *)NULL) {
                logit("et", "getTraceFromWS: out of memory for GAP struct\n");
                return -1;
            }
            newGap->starttime = pTrace->endtime + pTrace->delta;
            newGap->gapLen = pTH4->starttime - newGap->starttime;
            newGap->firstSamp = pTrace->nRaw;
            newGap->lastSamp  = pTrace->nRaw + (long)( (newGap->gapLen * samprate) - 0.5);
            if(WSDebug) logit("e"," starttime: %f gaplen: %f firstSamp: %d lastSamp: %d.\n", 
                newGap->starttime, newGap->gapLen, newGap->firstSamp, newGap->lastSamp); 
            /* Put GAP struct on list, earliest gap first */
            if (pTrace->gapList == (GAP *)NULL)
                pTrace->gapList = newGap;
            else
                pGap->next = newGap;
            pGap = newGap;  /* leave pGap pointing at the last GAP on the list */
            pTrace->nGaps++;

            /* Advance the Trace pointers past the gap; maybe gap will get filled */
            pTrace->nRaw = newGap->lastSamp + 1;
            pTrace->endtime += newGap->gapLen;
        }
    
        isamp = (pTrace->starttime > pTH4->starttime)?
                (long)( 0.5 + (pTrace->starttime - pTH4->starttime) * samprate):0;

        if (request->reqEndtime < pTH4->endtime) {
            nsamp = pTH4->nsamp - (long)( 0.5 * (pTH4->endtime - request->reqEndtime) * samprate);
            pTrace->endtime = request->reqEndtime;
        } 
        else {
            nsamp = pTH4->nsamp;
            pTrace->endtime = pTH4->endtime;
        }

    /* Assume trace data is integer valued here, long or short */    
        if (pTH4->datatype[1] == '4') {
            longPtr=(long*) ((char*)pTH4 + sizeof(TRACE_HEADER) + isamp * 4);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *longPtr;
                longPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF message */
            pTH = (TRACE_HEADER *)((char *)pTH + sizeof(TRACE_HEADER) + pTH4->nsamp * 4);
        }
        else {   /* pTH->datatype[1] == 2, we assume */
            shortPtr=(short*) ((char*)pTH4 + sizeof(TRACE_HEADER) + isamp * 2);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *shortPtr;
                shortPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF packets */
            pTH = (TRACE_HEADER *)((char *)pTH + sizeof(TRACE_HEADER) + pTH4->nsamp * 2);
        }
    }  /* End of loop over TRACE_BUF packets */
  

    if (io == WS_ERR_NONE ) success = 1;

    return success;
}


/********************************************************************
 *  WSReqBin2                                                       *
 *                                                                  *
 *  Retrieves binary data from location code enabled waveservers.   *
 *   k - waveserver index                                           *
 ********************************************************************/
int WSReqBin2(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt)
{
    char     server[wsADRLEN*3], whoami[50];
    int      i, kk, io, retry;
    int      isamp, nsamp, gap0, success, ret, WSDebug = 0;          
    long     iEnd, npoints;
    WS_MENU  menu = NULL;
    WS_PSCNL  pscn = NULL;
    double   mean, traceEnd, samprate;
    long    *longPtr;
    short   *shortPtr;
    
    TRACE2_HEADER *pTH;
    TRACE2_HEADER *pTH4;
    char tbuf[MAX_TRACEBUF_SIZ];
    GAP *pGap, *newGap;
    
    sprintf(whoami, " %s: %s: ", But->mod, "WSReqBin2");
    WSDebug = But->WSDebug;
    success = retry = 0;
    
gettrace:
    menu = NULL;
    /*    Put out WaveServer request here and wait for response */
    /* Get the trace
     ***************/
    /* rummage through all the servers we've been told about */
    if ( (wsSearchSCNL( request, &menu, &pscn, &But->menu_queue[k]  )) == WS_ERR_NONE ) {
        strcpy(server, menu->addr);
        strcat(server, "  ");
        strcat(server, menu->port);
    } else {
        strcpy(server, "unknown");
    }
    
/* initialize the global trace buffer, freeing old GAP structures. */
    pTrace->nRaw  = 0L;
    pTrace->delta     = 0.0;
    pTrace->starttime = 0.0;
    pTrace->endtime   = 0.0;
    pTrace->nGaps = 0;

    /* Clear out the gap list */
    pTrace->nGaps = 0;
    while ( (pGap = pTrace->gapList) != (GAP *)NULL) {
        pTrace->gapList = pGap->next;
        free(pGap);
    }
 
    if(WSDebug) {     
        logit("e","\n%s Issuing request to wsGetTraceBinL: server: %s Socket: %d.\n", 
             whoami, server, menu->sock);
        logit("e","    %s %f %f %d\n", SCNtxt,
             request->reqStarttime, request->reqEndtime, request->timeout);
    }

    io = wsGetTraceBinL(request, &But->menu_queue[k], But->wsTimeout);
    
    if (io == WS_ERR_NONE ) {
        if(WSDebug) {
            logit("e"," %s server: %s trace %s: went ok first time. Got %ld bytes\n",
                whoami, server, SCNtxt, request->actLen); 
            logit("e","        actStarttime=%lf, actEndtime=%lf, actLen=%ld, samprate=%lf\n",
                  request->actStarttime, request->actEndtime, request->actLen, request->samprate);
        }
    }
    else {
        switch(io) {
        case WS_ERR_EMPTY_MENU:
        case WS_ERR_SCNL_NOT_IN_MENU:
        case WS_ERR_BUFFER_OVERFLOW:
            if (io == WS_ERR_EMPTY_MENU ) 
                logit("e"," %s server: %s No menu found.  We might as well quit.\n", whoami, server); 
            if (io == WS_ERR_SCNL_NOT_IN_MENU ) 
                logit("e"," %s server: %s Trace %s not in menu\n", whoami, server, SCNtxt);
            if (io == WS_ERR_BUFFER_OVERFLOW ) 
                logit("e"," %s server: %s Trace %s overflowed buffer. Fatal.\n", whoami, server, SCNtxt); 
            return 2;                /*   We might as well quit */
        
        case WS_ERR_PARSE:
        case WS_ERR_TIMEOUT:
        case WS_ERR_BROKEN_CONNECTION:
        case WS_ERR_NO_CONNECTION:
            sleep_ew(500);
            retry += 1;
            if (io == WS_ERR_PARSE )
                logit("e"," %s server: %s Trace %s: Couldn't parse server's reply. Try again.\n", 
                        whoami, server, SCNtxt); 
            if (io == WS_ERR_TIMEOUT )
                logit("e"," %s server: %s Trace %s: Timeout to wave server. Try again.\n", whoami, server, SCNtxt); 
            if (io == WS_ERR_BROKEN_CONNECTION ) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Broken connection to wave server. Try again.\n", 
                whoami, server, SCNtxt); 
            }
            if (io == WS_ERR_NO_CONNECTION ) {
                if(WSDebug || retry>1) 
                    logit("e"," %s server: %s Trace %s: No connection to wave server.\n", whoami, server, SCNtxt); 
                if(WSDebug) logit("e"," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
            }
            ret = wsAttachServer( menu, But->wsTimeout );
            if(ret == WS_ERR_NONE && retry < But->RetryCount) goto gettrace;
            if(WSDebug) {   
                switch(ret) {
                case WS_ERR_NO_CONNECTION:
                    logit("e"," %s server: %s: No connection to wave server.\n", whoami, server); 
                    break;
                case WS_ERR_SOCKET:
                    logit("e"," %s server: %s: Socket error.\n", whoami, server); 
                    break;
                case WS_ERR_INPUT:
                    logit("e"," %s server: %s: Menu missing.\n", whoami, server); 
                    break;
                default:
                    logit("e"," %s server: %s: wsAttachServer error %d.\n", whoami, server, ret); 
                }
            }
            return 2;                /*   We might as well quit */
        
        case WS_WRN_FLAGGED:
            if((int)strlen(&request->retFlag)>0) {
                if(WSDebug)  
                logit("e","%s server: %s Trace %s: return flag from wsGetTraceBin: <%c>\n %.50s\n", 
                        whoami, server, SCNtxt, request->retFlag, But->TraceBuf);
            }
            if(WSDebug) logit("e"," %s server: %s Trace %s: No trace available. Wave server returned %c\n", 
                whoami, server, SCNtxt, request->retFlag); 
            
            return 2;            /*       We might as well quit */
        
        default:
            logit( "et","%s server: %s Failed.  io = %d\n", whoami, server, io );
        }
    }
    
    /* Transfer trace data from TRACE_BUF packets into Trace buffer */
    traceEnd = (request->actEndtime < request->reqEndtime) ?  request->actEndtime :
                                                              request->reqEndtime;
    pTH  = (TRACE2_HEADER *)request->pBuf;
    pTH4 = (TRACE2_HEADER *)tbuf;
    /*
    * Swap to local byte-order. Note that we will be calling this function
    * twice for the first TRACE_BUF packet; this is OK as the second call
    * will have no effect.
    */

    memcpy( pTH4, pTH, sizeof(TRACE2_HEADER) );
    if(WSDebug) logit( "e","%s server: %s Make Local\n", whoami, server, io );
    if (WaveMsg2MakeLocal(pTH4) == -1) {
        logit("et", "%s server: %s %s.%s.%s.%s unknown datatype <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
        return 5;
    }
    if (WaveMsg2MakeLocal(pTH4) == -2) {
        logit("et", "%s server: %s %s.%s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
    /*  return 5;  */
    }

    if(WSDebug) logit("e"," %s server: %s Trace %s: Data has samprate %g.\n", 
                     whoami, server, SCNtxt, pTH4->samprate); 
    if (pTH4->samprate < 0.1) {
        logit("et", "%s server: %s %s.%s.%s.%s (%s) has zero samplerate (%g); skipping trace\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, SCNtxt, pTH4->samprate);
        return 5;
    }
    But->samp_sec = pTH4->samprate<=0? 100L:(long)pTH4->samprate;

    pTrace->delta = 1.0/But->samp_sec;
    samprate = But->samp_sec;   /* Save rate of first packet to compare with later packets */
    pTrace->starttime = request->reqStarttime;
    /* Set Trace endtime so it can be used to test for gap at start of data */
    pTrace->endtime = ( (pTH4->starttime < request->reqStarttime) ?
                        pTH4->starttime : request->reqStarttime) - 0.5*pTrace->delta ;
    if(WSDebug) logit("e"," pTH->starttime: %f request->reqStarttime: %f delta: %f endtime: %f.\n", 
                     pTH4->starttime, request->reqStarttime, pTrace->delta, pTrace->endtime); 

  /* Look at all the retrieved TRACE_BUF packets 
   * Note that we must copy each tracebuf from the big character buffer
   * to the TRACE_BUF structure pTH4.  This is because of the occasionally 
   * seen case of a channel putting an odd number of i2 samples into
   * its tracebufs!  */
    kk = 0;
    while( pTH < (TRACE2_HEADER *)(request->pBuf + request->actLen) ) {
        memcpy( pTH4, pTH, sizeof(TRACE2_HEADER) );
         /* Swap bytes to local order */
	    if (WaveMsg2MakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsg2MakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	    /*  return 5;  */
        }
    
        nsamp = pTH4->nsamp;
        memcpy( pTH4, pTH, sizeof(TRACE2_HEADER) + nsamp*4 );
         /* Swap bytes to local order */
	    if (WaveMsg2MakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsg2MakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	    /*  return 5;  */
        }
    
        if ( fabs(pTH4->samprate - samprate) > 1.0) {
            logit("et", "%s <%s.%s.%s.%s samplerate change: %f - %f; discarding trace\n",
                whoami, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, samprate, pTH4->samprate);
            return 5;
        }
    
    /* Check for gap */
        if (pTrace->endtime + 1.5 * pTrace->delta < pTH4->starttime) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Gap detected.\n", 
                        whoami, server, SCNtxt); 
            if ( (newGap = (GAP *)calloc(1, sizeof(GAP))) == (GAP *)NULL) {
                logit("et", "getTraceFromWS: out of memory for GAP struct\n");
                return -1;
            }
            newGap->starttime = pTrace->endtime + pTrace->delta;
            newGap->gapLen = pTH4->starttime - newGap->starttime;
            newGap->firstSamp = pTrace->nRaw;
            newGap->lastSamp  = pTrace->nRaw + (long)( (newGap->gapLen * samprate) - 0.5);
            if(WSDebug) logit("e"," starttime: %f gaplen: %f firstSamp: %d lastSamp: %d.\n", 
                newGap->starttime, newGap->gapLen, newGap->firstSamp, newGap->lastSamp); 
            /* Put GAP struct on list, earliest gap first */
            if (pTrace->gapList == (GAP *)NULL)
                pTrace->gapList = newGap;
            else
                pGap->next = newGap;
            pGap = newGap;  /* leave pGap pointing at the last GAP on the list */
            pTrace->nGaps++;

            /* Advance the Trace pointers past the gap; maybe gap will get filled */
            pTrace->nRaw = newGap->lastSamp + 1;
            pTrace->endtime += newGap->gapLen;
        }
    
        isamp = (pTrace->starttime > pTH4->starttime)?
                (long)( 0.5 + (pTrace->starttime - pTH4->starttime) * samprate):0;

        if (request->reqEndtime < pTH4->endtime) {
            nsamp = pTH4->nsamp - (long)( 0.5 * (pTH4->endtime - request->reqEndtime) * samprate);
            pTrace->endtime = request->reqEndtime;
        } 
        else {
            nsamp = pTH4->nsamp;
            pTrace->endtime = pTH4->endtime;
        }

    /* Assume trace data is integer valued here, long or short */    
        if (pTH4->datatype[1] == '4') {
            longPtr=(long*) ((char*)pTH4 + sizeof(TRACE2_HEADER) + isamp * 4);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *longPtr;
                longPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF message */
            pTH = (TRACE2_HEADER *)((char *)pTH + sizeof(TRACE2_HEADER) + pTH4->nsamp * 4);
        }
        else {   /* pTH->datatype[1] == 2, we assume */
            shortPtr=(short*) ((char*)pTH4 + sizeof(TRACE2_HEADER) + isamp * 2);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *shortPtr;
                shortPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF packets */
            pTH = (TRACE2_HEADER *)((char *)pTH + sizeof(TRACE2_HEADER) + pTH4->nsamp * 2);
        }
    }  /* End of loop over TRACE_BUF packets */
  

    if (io == WS_ERR_NONE ) success = 1;

    return success;
}


/**********************************************************************
 * IsDST : Determine if we are using daylight savings time.           *
 *         This is a valid function for US and Canada thru 31/12/2099 *
 *                                                                    *
 *         Modified 02/16/07 to reflect political changes. JHL        *
 *         Fixed 03/05/08 to run correctly. JHL                       *
 *                                                                    *
 **********************************************************************/
int IsDST(int year, int month, int day)
{
    int     i, leapyr, day1, day2, num, jd, jd1, jd2, jd3, jd4;
    int     dpm[] = {31,28,31,30,31,30,31,31,30,31,30,31};
    
    leapyr = 0;
    if((year-4*(year/4))==0 && (year-100*(year/100))!=0) leapyr = 1;
    if((year-400*(year/400))==0) leapyr = 1;
    
    num = ((year-1900)*5)/4 + 5;
    num = num - 7*(num/7);       /* day # of 1 March                  */
    	day1 = num;
    	if(day1>7) day1 = day1 - 7;
    	jd3 = 59 + day1 + leapyr;    /* Julian day of 2nd Sunday in March */
    day1 = num + 2;
    if(day1>7) day1 = day1 - 7;  /* day # (-1) of 1 April             */
    day1 = 8 - day1;             /* date of 1st Sunday in April       */
    jd1 = 90 + day1 + leapyr;    /* Julian day of 1st Sunday in April */
    day2 = num + 3;
    if(day2>7) day2 = day2 - 7;  /* day # (-1) of 1 Oct               */
    day2 = 8 - day2;             /* date of 1st Sunday in Oct         */
    while(day2<=31) day2 += 7;   /* date of 1st Sunday in Nov         */
    	jd4 = 273 + leapyr + day2;   /* Julian day of 1st Sunday in Nov  */
    day2 -= 7;                   /* date of last Sunday in Oct        */
    jd2 = 273 + day2 + leapyr;   /* Julian day of last Sunday in Oct  */
    
    jd = day;
    for(i=0;i<month-1;i++) jd += dpm[i];
    if(month>2) jd += leapyr;
    
    if(jd>=jd1 && jd<jd2) return 1;
    if(jd>=jd3 && jd<jd4) return 1;
    
    return 0;
}


/**********************************************************************
 * Decode_Time : Decode time from seconds since 1970                  *
 *                                                                    *
 **********************************************************************/
void Decode_Time( double secs, TStrct *Time)
{
    struct Greg  g;
    long    minute;
    double  sex;

    Time->Time = secs;
    secs += sec1970;
    Time->Time1600 = secs;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    Time->Year  = g.year;
    Time->Month = g.month;
    Time->Day   = g.day;
    Time->Hour  = g.hour;
    Time->Min   = g.minute;
    Time->Sec   = sex;
}


/**********************************************************************
 * date22 : Calculate 22 char date in the form Jan23,1988 12:34 12.21 *
 *          from the julian seconds.  Remember to leave space for the *
 *          string termination (NUL).                                 *
 **********************************************************************/
void date22( double secs, char *c22)
{
    char *cmo[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    struct Greg  g;
    long    minute;
    double  sex;

    secs += sec1970;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    sprintf(c22, "%3s%2d,%4d %.2d:%.2d:%05.2f",
            cmo[g.month-1], g.day, g.year, g.hour, g.minute, sex);
}


/***********************************************************************
 * date11 : Calculate 11 char h:m:s time in the form 12:34:12.21       *
 *          from the daily seconds.  Remember to leave space for the   *
 *          string termination (NUL).                                  *
 *          This is a special version used for labelling the time axis *
 ***********************************************************************/
void date11( double secs, char *c11)
{
    long    hour, minute, wholesex;
    double  sex, fracsex;

    if(secs < 86400.0) secs = secs - 86400; /* make negative times negative */
    minute = (long) (secs / 60.0);
    sex    = secs - 60.0 * minute;
    wholesex = (long) sex;
    fracsex  = sex - wholesex;
    hour   = (long) (minute / 60.0);
    minute = minute - 60 * hour;
    while(hour>=24) hour -= 24;
    
    if(hour != 0) {
        sprintf(c11, "%d:%.2d:%05.2f", hour, minute, sex);
        if(fracsex >= 0.01) sprintf(c11, "%.2d:%.2d:%05.2f", hour, minute, sex);
        else sprintf(c11, "%.2d:%.2d:%.2d", hour, minute, wholesex);
    }
    else if(minute != 0) {
        sprintf(c11, "%d:%05.2f", minute, sex);
        if(fracsex >= 0.01) sprintf(c11, "%.2d:%05.2f", minute, sex);
        else sprintf(c11, "%.2d:%.2d", minute, wholesex);
    }
    else if(sex >= 10.0) {
        if(fracsex >= 0.01) sprintf(c11, "%05.2f", sex);
        else sprintf(c11, "%.2d", wholesex);
    }
    else {
        if(fracsex >= 0.01) sprintf(c11, "%4.2f", sex);
        else sprintf(c11, "%.1d", wholesex);
    }
}


/*************************************************************************
 *   MinMax determines the maximum deflection.                           *
 *                                                                       *
 *************************************************************************/
int MinMax(long Npts, long start, int decimation, double *Data, double *Amp, double *Mid)
{
    double   min, max, bignum = 1.0e20;
    int      i, j, k, acquired, gap0, gap1;
    
    i = k = 0;
    pGap = pTrace.gapList;
    acquired = 0;
    for(j=start;j<Npts;j+=decimation) {
        gap0 = (i >= pTrace.nGaps)? pTrace.nRaw:pGap->firstSamp;
        gap1 = (i >= pTrace.nGaps)? pTrace.nRaw:pGap->lastSamp;
        if(j > gap1) {
            pGap = pGap->next;
            if(pGap == (GAP *)NULL) i = pTrace.nGaps;
            i += 1;
            gap0 = (i >= pTrace.nGaps)? pTrace.nRaw:pGap->firstSamp;
            gap1 = (i >= pTrace.nGaps)? pTrace.nRaw:pGap->lastSamp;
        }
        if(j < gap0) {
            if(k++ == 0) {
                min = max = Data[j];
            }
            if(Data[j]<-bignum) Data[j] = -bignum;
            if(Data[j]>+bignum) Data[j] =  bignum;
            if(Data[j]<min) min = Data[j];
            if(Data[j]>max) max = Data[j];
            acquired = 1;
        }
        else {
            acquired = 0;  
        }
    }
    *Amp = max-min;
    *Mid = min + *Amp/2.0;
    if(*Amp > 1.0e19) return 2;
    if(*Amp==0.0) return 1;
    return 0;
}


/*************************************************************************
 *   ixp calculates the x pixel location.                                *
 *                                                                       *
 *************************************************************************/
int ixp(double a)
{
    int    i = (int)(a*72.0);
    return i;
}


/*************************************************************************
 *   iyp calculates the y pixel location.                                *
 *                                                                       *
 *************************************************************************/
int iyp(double a)
{
    int    i = (int)((a - YBMargin)*72.0) + 4;
    return i;
}

/*************************************************************************
 *   iyq calculates the y pixel location.                                *
 *                                                                       *
 *************************************************************************/
int iyq(double ysize, double a)
{
    double   val;
    int      i;
    
    val = (ysize - a - YBMargin); /* y increases up from bottom */
    
    if(val > ysize) val = ysize;
    if(val < 0.0)   val = 0.0;
    i = (int)(val*72.0) + 4;
    return i;
}

/****************************************************************************
 *  Get_Sta_Info(Global *But);                                              *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Sta_Info(Global *But)
{
    char    whoami[50], *com, *str;
    int     i, j, k, nfiles, success, type, sensor, units;
    double  dlat, mlat, dlon, mlon, gain, sens, ssens;

    sprintf(whoami, "%s: %s: ", But->mod, "Get_Sta_Info");
    
    But->NSCN = 0;
    for(k=0;k<But->nStaDB;k++) {
            /* Open the main station file
             ****************************/
        nfiles = k_open( But->stationList[k] );
        if(nfiles == 0) {
            logit("e", "%s Error opening command file <%s>; exiting!\n", whoami, But->stationList[k] );
            exit( -1 );
        }

            /* Process all command files
             ***************************/
        while(nfiles > 0) {  /* While there are command files open */
            while(k_rd())  {      /* Read next line from active file  */
                com = k_str();         /* Get the first token from line */

                    /* Ignore blank lines & comments
                     *******************************/
                if( !com )           continue;
                if( com[0] == '#' )  continue;

                    /* Open a nested configuration file
                     **********************************/
                if( com[0] == '@' ) {
                    success = nfiles+1;
                    nfiles  = k_open(&com[1]);
                    if ( nfiles != success ) {
                        logit("e", "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                        exit( -1 );
                    }
                    continue;
                }

                /* Process anything else as a channel descriptor
                 ***********************************************/

                if( But->NSCN >= MAXCHANNELS ) {
                    logit("e", "%s Too many channel entries in <%s>", 
                             whoami, But->stationList[k] );
                    logit("e", "; max=%d; exiting!\n", (int) MAXCHANNELS );
                    exit( -1 );
                }
                j = But->NSCN;
                
                    /* S C N */
                strncpy( But->Chan[j].Site, com,  6);
                str = k_str();
                if(str) strncpy( But->Chan[j].Net,  str,  2);
                str = k_str();
                if(str) strncpy( But->Chan[j].Comp, str, 3);
                str = k_str();
                if(str) strncpy( But->Chan[j].Loc, str, 3);
                for(i=0;i<6;i++) if(But->Chan[j].Site[i]==' ') But->Chan[j].Site[i] = 0;
                for(i=0;i<2;i++) if(But->Chan[j].Net[i]==' ')  But->Chan[j].Net[i]  = 0;
                for(i=0;i<3;i++) if(But->Chan[j].Comp[i]==' ') But->Chan[j].Comp[i] = 0;
                for(i=0;i<2;i++) if(But->Chan[j].Loc[i]==' ')  But->Chan[j].Loc[i]  = 0;
                But->Chan[j].Comp[3] = But->Chan[j].Net[2] = But->Chan[j].Site[5] = But->Chan[j].Loc[2] = 0;
                sprintf(But->Chan[j].SCNnam, "%s_%s_%s_%s", 
                        But->Chan[j].Site, But->Chan[j].Comp, But->Chan[j].Net, But->Chan[j].Loc);


                    /* Lat Lon Elev */
                But->Chan[j].Lat  = k_val();
                But->Chan[j].Lon  = k_val();
                But->Chan[j].Elev = k_val();
                                
                But->Chan[j].Inst_type  = k_int();
                But->Chan[j].Inst_gain  = k_val();
                But->Chan[j].GainFudge  = k_val();
                But->Chan[j].Sens_type  = k_int();
                But->Chan[j].Sens_unit  = k_int();
                But->Chan[j].Sens_gain  = k_val();
                if(But->Chan[j].Sens_unit == 3) But->Chan[j].Sens_gain /= 981.0;
                But->Chan[j].SiteCorr = k_val();
                But->Chan[j].ShkQual  = k_int();
                        
               
                But->Chan[j].sensitivity = (1000000.0*But->Chan[j].Sens_gain/But->Chan[j].Inst_gain)*But->Chan[j].GainFudge;    /*    sensitivity counts/units        */
                
                if (k_err()) {
                    logit("e", "%s Error decoding line in station file\n%s\n  exiting!\n", whoami, k_get() );
                    exit( -1 );
                }
         /*>Comment<*/
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].SiteName, str );
                    
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].Descript, str );
                    
                But->NSCN++;
            }
            nfiles = k_close();
        }
    }
}


/*************************************************************************
 *  Put_Sta_Info(Global *But);                                           *
 *  Retrieve all the information available about station i               *
 *  and put it into an internal structure for reference.                 *
 *  This should eventually be a call to the database; for now we must    *
 *  supply an ascii file with all the info.                              *
 *************************************************************************/
int Put_Sta_Info(Global *But, ArkInfo *A)
{
    char       whoami[50];
    int        j;

    sprintf(whoami, " %s: %s: ", But->mod, "Put_Sta_Info");
    
    for(j=0;j<But->NSCN;j++) {
        if(strcmp(But->Chan[j].Site, A->Site )==0 && 
           strcmp(But->Chan[j].Comp, A->Comp)==0 && 
           strcmp(But->Chan[j].Net,  A->Net )==0) {
            strcpy(A->Loc,  But->Chan[j].Loc);
            A->sensitivity  = But->Chan[j].sensitivity;
            A->Sens_unit  = But->Chan[j].Sens_unit;
            return 1;
        }
    }
    
    A->sensitivity  = 1000000.0;    /* Default: 1 volt/unit & 1 microvolt/count */
    A->Sens_unit  = 2;
    return 0;
}


/************************************************************************

    Subroutine distaz (lat1,lon1,lat2,lon2,RNGKM,FAZ,BAZ)
    
c--  COMPUTES RANGE AND AZIMUTHS (FORWARD AND BACK) BETWEEN TWO POINTS.
c--  OPERATOR CHOOSES BETWEEN 3 FIRST ORDER ELLIPSOIDAL MODELS OF THE
c--  EARTH AS DEFINED BY THE MAJOR RADIUS AND FLATTENING.
c--  THE PROGRAM UTILIZES THE SODANO AND ROBINSON (1963) DIRECT SOLUTION
c--  OF GEODESICS (ARMY MAP SERVICE, TECH REP #7, SECTION IV).
c--  (TERMS ARE GIVEN TO ORDER ECCENTRICITY TO THE FOURTH POWER.)
c--  ACCURACY FOR VERY LONG GEODESICS:
c--          DISTANCE < +/-  1 METER
c--          AZIMUTH  < +/- .01 SEC
c
    Ellipsoid            Major Radius    Minor Radius    Flattening
1  Fischer 1960            6378166.0        6356784.28      298.30
2  Clarke1866              6378206.4        6356583.8       294.98
3  S. Am 1967              6378160.0        6356774.72      298.25
4  Hayford Intl 1910       6378388.0        6356911.94613   297.00
5  WGS 1972                6378135.0        6356750.519915  298.26
6  Bessel 1841             6377397.155      6356078.96284   299.1528
7  Everest 1830            6377276.3452     6356075.4133    300.8017
8  Airy                    6377563.396      6356256.81      299.325
9  Hough 1960              6378270.0        6356794.343479  297.00
10 Fischer 1968            6378150.0        6356768.337303  298.30
11 Clarke1880              6378249.145      6356514.86955   293.465
12 Fischer 1960            6378155.0        6356773.32      298.30
13 Intl Astr Union         6378160.0        6378160.0       298.25
14 Krasovsky               6378245.0        6356863.0188    298.30
15 WGS 1984                6378137.0        6356752.31      298.257223563
16 Aust Natl               6378160.0        6356774.719     298.25
17 GRS80                   6378137.0        63567552.31414  298.2572
18 Helmert                 6378200.0        6356818.17      298.30
19 Mod. Airy               6377341.89       6356036.143     299.325
20 Mod. Everest            6377304.063      6356103.039     300.8017
21 Mercury 1960            6378166.0        6356784.283666  298.30
22 S.E. Asia               6378155.0        6356773.3205    298.2305
23 Sphere                  6370997.0        6370997.0         0.0
24 Walbeck                 6376896.0        6355834.8467    302.78
25 WGS 1966                6378145.0        6356759.769356  298.25

************************************************************************/

short distaz (double lat1, double lon1, double lat2, double lon2, 
                double *rngkm, double *faz, double *baz)
{
    double    pi, rd, f, fsq;
    double    a3, a5, a6, a10, a16, a17, a18, a19, a20;
    double    a21, a22, a22t1, a22t2, a22t3, a23, a24, a25, a27, a28, a30;
    double    a31, a32, a33, a34, a35, a36, a38, a39, a40;
    double    a41, a43;
    double    a50, a51, a52, a54, a55, a57, a58, a62;
    double    dlon, alph[2];
    double    p11, p12, p13, p14, p15, p16, p17, p18;
    double    p21, p22, p23, p24, p25, p26, p27, p28, pd1, pd2;
    double    ang45;
    short    k;
    double    RAD, MRAD, FINV;
    
    RAD  = 6378206.4;
    MRAD = 6356583.8;
    FINV = 294.98;
    ang45 = atan(1.0);
    
    pi = 4.0*atan(1.0);
    rd = pi/180.0;

    a5 = RAD;
    a3 = FINV;
    
    if(a3==0.0) {
        a6 = MRAD;                    /*    minor radius of ellipsoid    */
        f = fsq = a10 = 0.0;
    }
    else {
        a6 = (a3-1.0)*a5/a3;          /*    minor radius of ellipsoid    */
        f = 1.0/a3;                   /*    flattening    */
        fsq = 1.0/(a3*a3);
        a10 = (a5*a5-a6*a6)/(a5*a5);  /*    eccentricity squared    */
    }
    
    /*    Following definitions are from Sodano algorithm for long geodesics    */
    a50 = 1.0 + f + fsq;
    a51 = f + fsq;
    a52 = fsq/ 2.0;
    a54 = fsq/16.0;
    a55 = fsq/ 8.0;
    a57 = fsq* 1.25;
    a58 = fsq/ 4.0;
    
/*     THIS IS THE CALCULATION LOOP. */
    
    p11 = lat1*rd;
    p12 = lon1*rd;
    p21 = lat2*rd;
    p22 = lon2*rd;
    *rngkm = *faz = *baz = 0.0;
    if( (lat1==lat2) && (lon1==lon2))    return(0);
    
    /*    Make sure points are not exactly on the equator    */
    if(p11 == 0.0) p11 = 0.000001;
    if(p21 == 0.0) p21 = 0.000001;
    
    /*    Make sure points are not exactly on the same meridian    */
    if(p12 == p22)             p22 += 0.0000000001;
    if(fabs(p12-p22) == pi)    p22 += 0.0000000001;
    
    /*    Correct latitudes for flattening    */
    p13 = sin(p11);
    p14 = cos(p11);
    p15 = p13/p14;
    p18 = p15*(1.0-f);
    a62 = atan(p18);
    p16 = sin(a62);
    p17 = cos(a62);
    
    p23 = sin(p21);
    p24 = cos(p21);
    p25 = p23/p24;
    p28 = p25*(1.0-f);
    a62 = atan(p28);
    p26 = sin(a62);
    p27 = cos(a62);
    
    dlon = p22-p12;
    a16 = fabs(dlon);
    
    /*    Difference in longitude to minimum (<pi)    */
    if(a16 >= pi) a16 = 2.0*pi - a16;

    /*    Compute range (a35)    */
    if(a16==0.0)     {a17 = 0.0;         a18 = 1.0;}
    else             {a17 = sin(a16);    a18 = cos(a16);}
    a19 = p16*p26;
    a20 = p17*p27;
    a21 = a19 + a20*a18;
    
    a40 = a41 = a43 = 0.0;
    
    for(k=0; k<2; k++) {
        a22t1 = (a17*p27)*(a17*p27);
        a22t2 = p26*p17 - p16*p27*a18;
        a22   = sqrt(a22t1 + (a22t2*a22t2));
        if(a22 == 0.0) return(0);
        a22t3 = a22*a21;
        a23 = (a20*a17)/a22;
        a24 = 1.0 - a23*a23;
        a25 = asin(a22);
        if(a21 < 0.0) a25 = pi-a25;
        a27 = (a25*a25)/a22;
        a28 = a21*a27;
        if(k==0) {
            a30 = (a50*a25) + a19*(a51*a22-a52*a27);
            a31 = 0.5*a24*(fsq*a28 - a51*(a25 + a22t3));
            a32 = a19*a19*a52*a22t3;
            a33 = a24*a24*(a54*(a25 + a22t3) - a52*a28 - a55*a22t3*(a21*a21));
            a34 = a19*a24*a52*(a27 + a22t3*a21);
            a35 = (a30 + a31 - a32 + a33 + a34)*a6;
            *rngkm = a35/1000.0;
        }

    /*    Compute azimuths    */
        a36 = (a51*a25) - a19*(a52*a22 + fsq*a27) + a24*(a58*a22t3 - a57*a25 + fsq*a28);
        a38 = a36*a23 + a16;
        a39 = sin(a38);
        a40 = cos(a38);

        if(a39*p27 == 0.0) a43 = 0.0;
        else {
          a41 = (p26*p17 - a40*p16*p27)/(a39*p27);
          if(a41 == 0.0)    a43 = pi/2.0;
          else                a43 = atan(1.0/a41);
        }

        alph[k] = a43;
        if((dlon <= -pi) || ((dlon >= 0.0) && (dlon < pi))) {
          if(a41 >= 0.0)       alph[k] = alph[k]-pi;
        }
        else {
          if(a41 >= 0.0)       alph[k] = pi - alph[k];
          else                 alph[k] = 2.0*pi - alph[k];
        }
        if(alph[k] >= pi)      alph[k] = alph[k] - pi;
        if(alph[k] <  pi)      alph[k] = alph[k] + pi;
        if(alph[k] <  0.0)     alph[k] = alph[k] + 2.0*pi;
        if(alph[k] >= 2.0*pi)  alph[k] = alph[k] - 2.0*pi;
        alph[k] = alph[k]/rd;
        pd1 = p16;
        pd2 = p17;
        p16 = p26;
        p17 = p27;
        p26 = pd1;
        p27 = pd2;
        dlon = -dlon;
    }

    *faz = alph[0];
    *baz = alph[1];
    while (*faz >= 360.0) *faz = *faz - 360.0;
    while (*baz >= 360.0) *baz = *baz - 360.0;
    
    return(0);
}


