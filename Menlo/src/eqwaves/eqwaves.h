/*****************************************************************************
 *  eqwaves.h                                                                *
 *                                                                           *
 *  header file for eqwaves.c                                                *
 *****************************************************************************/

/*****************************************************************************
 *  defines                                                                  *
 *****************************************************************************/

#define MAXCHANNELS     3600  /* Maximum number of channels                  */
#define MAXSAMPRATE      500  /* Maximum # of samples/sec.                   */
#define MAXMINUTES         5  /* Maximum # of minutes of trace.              */
#define MAXTRACELTH MAXMINUTES*60*MAXSAMPRATE /* Max. data trace length      */
#define MAXTRACEBUF MAXTRACELTH*10  /* This should work for 24-bit data      */

#define WSTIMEOUT          5  /* Number of seconds 'til waveserver times out */

#define MAXCOLORS         20  /* Number of colors defined                    */

#define MAXPLOTS           8  /* Maximum number of Plots/Event               */
#define MAX_STADBS        20  /* Maximum number of Station database files    */
#define MAX_WAVESERVERS   90  /* Maximum number of Waveservers               */
#define MAX_ADRLEN        20  /* Size of waveserver address arrays           */
#define MAX_TARGETS       10  /* Maximum number of targets                   */

#define PRFXSZ            20  /* Size of string for GIF file prefix          */
#define GDIRSZ           132  /* Size of string for GIF target directory     */
#define STALIST_SIZ      100  /* Size of string for station list file        */
#define MAXLOGO            2  /* Maximum number of Logos                     */

#define TIMEOUT          600  /* Time(sec) after event to wait for data.     */
#define MAX_COMPS         30  /* Number of acceptable component classes      */

#define MAX_STR          255  /* Size of strings for arkive records          */
#define MAX_SERV_THRDS     1  /* Maximum number of processor threads         */
#define MAX_GAPS        1000  /* Maximum number of gaps per data request.    */


/*****************************************************************************
 *  Define the structure for time records.                                   *
 *****************************************************************************/

typedef struct TStrct {   
    double  Time1600; /* Time (Sec since 1600/01/01 00:00:00.00)             */
    double  Time;     /* Time (Sec since 1970/01/01 00:00:00.00)             */
    int     Year;     /* Year                                                */
    int     Month;    /* Month                                               */
    int     Day;      /* Day                                                 */
    int     Hour;     /* Hour                                                */
    int     Min;      /* Minute                                              */
    double  Sec;      /* Second                                              */
} TStrct;

/*****************************************************************************
 *  Define the structure for specifying gaps in the data.                    *
 * Note: if a gap would be declared at end of data, the data must be         *
 * truncated instead of adding another GAP structure. A gap may be           *
 * declared at the start of the data, however.                               *
 *****************************************************************************/

typedef struct _GAP *PGAP;
typedef struct _GAP {
  double starttime;  /* time of first sample in the gap                      */
  double gapLen;     /* time from first gap sample to first sample after gap */
  long firstSamp;    /* index of first gap sample in data buffer             */
  long lastSamp;     /* index of last gap sample in data buffer              */
  PGAP next;         /* The next gap structure in the list                   */
} GAP;

/*****************************************************************************
 *  Define the structure for keeping track of buffer of trace data.          *
 *****************************************************************************/

typedef struct _DATABUF {
  double rawData[MAXTRACELTH*5];   /* The raw trace data; native byte order  */
  double delta;      /* The nominal time between sample points               */
  double starttime;  /* time of first sample in raw data buffer              */
  double endtime;    /* time of last sample in raw data buffer               */
  long nRaw;         /* number of samples in raw data buffer, including gaps */
  long lenRaw;       /* length to the rawData array                          */
  GAP *gapList;      /* linked list of gaps in raw data                      */
  int nGaps;         /* number of gaps found in raw data                     */
} DATABUF;

/*****************************************************************************
 *  Define the structure for Arkive Phase cards.                             *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/

typedef struct ArkInfo {  /* A phase card structure */
    char    Site[6];      /*  0- 3  0- 4 Site                                */
    char    Comp[5];      /* 95-97  9-11 Component                           */
    char    Net[5];       /* 98-99  5- 6 Net                                 */
    char    Loc[5];       /* Loc                                             */
    char    Prem[5];      /*  4- 5 13-14 P remark                            */
    char    fm;           /*     6    15 P first motion                      */
    int     Pwgt;         /*     7    16 weight                              */
    double  PPick;        /*  9-18+19-23 17-29+29-33 P pick time             */
    double  SPick;        /*  9-18+31-35 17-29+41-45 S pick time             */
    char    Srem[5];      /* 36-37 46-47 S remark                            */
    float   SttResid;     /* 40-43 50-53 S travel-time residual              */
    float   PttResid;     /* 24-27 34-37 P travel-time residual              */
    float   Pdelay;       /* 50-53 66-69 P delay time                        */
    float   Sdelay;       /* 54-57 70-73 S delay time                        */
    float   Dist;         /* 58-61 74-77 Epicentral distance (km)            */
    float   Emerg;        /* 62-64 78-80 Emergence angle at source           */
    float   CodaDur;      /* 71-74 87-90 Coda duration (sec)                 */
    float   Azim;         /* 75-77 91-93 Azimuth to station in deg. E of N.  */
    char    DataSrc;      /* 91    108                                       */
    char    StaList;      /* Flag (*) to indicate info came from StaList     */
    
    int     Ignore;       /* Flag to ignore this phase                       */
    double  Duration;     /* Seconds to acquire                              */
    double  Stime;        /* Requested start time                            */
    double  sensitivity;  /* Channel sensitivity  counts/units               */
    int     Sens_unit;    /* Sensor units d=1; v=2; a=3                      */
} ArkInfo;

/*****************************************************************************
 *  Define the structure for the Arkive.                                     *
 *  This is an abbreviated structure.                                        *
 ******************************************************************************/

typedef struct Arkive {   /* An Arkive List */
    char    ArkivMsg[MAX_BYTES_PER_EQ];
    long    InList;       /* Number of archive records in list                             */
    double  EvntTime;     /* Time of Event (Julian Sec)                                    */
    short   EvntYear;     /*   0-  1   0-  3 Event Year                                    */
    short   EvntMonth;    /*   2-  3   4-  5 Event Month                                   */
    short   EvntDay;      /*   4-  5   6-  7 Event Day                                     */
    short   EvntHour;     /*   6-  7   8-  9 Event Hour                                    */
    short   EvntMin;      /*   8-  9  10- 11 Event Minute                                  */
    float   EvntSec;      /*  10- 13  12- 15 Event Second*100                              */
    float   EvntLat;      /*  14- 20  16- 22 Geog coord of Event                           */
    float   EvntLon;      /*  21- 28  23- 30 Geog coord of Event                           */
    float   EvntDepth;    /*  29- 33  31- 35 Depth of Event*100                            */
    float   Smag;         /*  34- 35  36- 38 Mag from max S amplitude from NCSN stations   */
    long    NumPhs;       /*  36- 38  39- 41 # phases used in solution (final weights > 0.1) */
    long    Gap;          /*  39- 41  42- 44 Maximum azimuthal gap                         */
    float   Dmin;         /*  42- 44  45- 47 Minimum distance to site                      */
    float   rms;          /*  45- 48  48- 51 RMS travel time residual*100                  */
    float   mag;          /*  67- 68  70- 72 Coda duration mag                             */
    float   erh;          /*  80- 83  85- 88 Horizontal error*100 (km)                     */
    float   erz;          /*  84- 87  89- 92 Vertical error*100 (km)                       */
    char    ExMagType;    /*     114     122 "External" magnitude label or type code       */
    float   ExMag;        /* 115-117 123-125 "External" magnitude                          */
    float   ExMagWgt;     /* 118-120 126-128 Total of the "external" mag weights (~ number of readings) */
    char    AltMagType;   /*     121     129 Alt amplitude magnitude label or type code    */
    float   AltMag;       /* 122-124 130-132 Alt amplitude magnitude                       */
    float   AltMagWgt;    /* 125-127 133-135 Total of the alt amplitude mag weights (~ number of readings) */
    char    EvntIDS[10];  /* 128-137 136-145 Event ID                                      */
    long    EvntID;       /* Event ID                                                      */
    char    PreMagType;   /*     138     146 Preferred magnitude label code chosen from those available */
    float   PreMag;       /* 139-141 147-149 Preferred magnitude                           */
    float   PreMagWgt;    /* 142-144 150-153 Total of the preferred mag weights (~ number of readings) */
    char    AltDmagType;  /*     145     154 Alt coda dur. magnitude label or type code    */
    float   AltDmag;      /* 146-148 155-157 Alt coda dur. magnitude                       */
    float   AltDmagWgt;   /* 149-151 158-161 Total of the alternate coda duration magnitude weights (~ number of readings) */

    long    MaxChannels;  /*  */
    long    index[MAXCHANNELS];
    ArkInfo A[MAXCHANNELS];
} Arkive;

/*****************************************************************************
 *  Define the structure for Channel information.                            *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/

typedef struct ChanInfo {      /* A channel information structure            */
    char    Site[6];           /* Site                                       */
    char    Comp[5];           /* Component                                  */
    char    Net[5];            /* Net                                        */
    char    Loc[5];            /* Loc                                        */
    char    SCNtxt[25];        /* S C N                                      */
    char    SCNnam[25];        /* S_C_N                                      */
    char    SiteName[50];      /* Common Name of Site                        */
    char    Descript[200];     /* Common Name of Site                        */
    double  Lat;               /* Latitude                                   */
    double  Lon;               /* Longitude                                  */
    double  Elev;              /* Elevation                                  */
    
    int     Inst_type;         /* Type of instrument                         */
    double  Inst_gain;         /* Gain of instrument (microv/count)          */
    int     Sens_type;         /* Type of sensor                             */
    double  Sens_gain;         /* Gain of sensor (volts/unit)                */
    int     Sens_unit;         /* Sensor units d=1; v=2; a=3                 */
    double  GainFudge;         /* Additional gain factor.                    */
    double  SiteCorr;          /* Site correction factor.                    */
    double  sensitivity;       /* Channel sensitivity  counts/units          */
    int		ShkQual;           /* Station (Chan) type                        */
    
    double  Scale;             /* Scale factor [Data] (in)                   */
    double  Scaler;            /* Scale factor [Data] (in)                   */
} ChanInfo;

/*****************************************************************************
 *  Define the structure for the Plotting parameters.                        *
 *****************************************************************************/
#define WHITE  0
#define BLACK  1
#define RED    2
#define BLUE   3
#define GREEN  4
#define GREY   5
#define YELLOW 6
#define TURQ   7
#define PURPLE 8

struct PltPar {
    double  xsize, ysize;    /* Size of plot (inches)                        */
    double  xpix, ypix;      /* Size of plot (pixels)                        */
    long    ntrace;          /* # of traces to plot in this screen           */
    double  SecPerScreenReq; /* seconds /screen (x-axis) autoscale           */
    double  pre_event;       /* seconds before event time                    */
    long    Format;          /* Form of plot. 
                                     1 - aligned on event time 
                                     2 - aligned on first arrivals           
                                     3 - reduced at 6 km/s           
                                     4 - reduced at 8 km/s                        
                                     5 - reduced at 8 km/s;true distance     */
    long    trace_label;     /* Flag for trace labels                        */
    long    plot_enable;     /* Flag for making this plot                    */
    
    double  sec_per_screen;  /* seconds /screen (x-axis) autoscale           */
    double  xoffset;         /* Offset of data space from (0,0) (in)         */
    double  axexmax;         /* max axe x position [Data] (in)               */
    double  axeymax;         /* max axe y position [Data] (in)               */
    double  tsize;           /* trace size [Data] (in)                       */
    double  ycenter;         /* center of y-axis                             */
    long    ktrace;          /* traces plotted this screen                   */
    long    done;            /* flag to signal end of plot                   */

    long    gcolor[MAXCOLORS]; /* GIF colors                                 */
    char    GifName[50];
    char    GifTName[50];
    gdImagePtr    GifImage;
};
typedef struct PltPar PltPar;


/*****************************************************************************
 *  Define the structure for the individual Global thread.                   *
 *  This is the private area the thread needs to keep track                  *
 *  of all those variables unique to itself.                                 *
 *****************************************************************************/

struct Global {
    int     Debug;             /*                                            */
    int     WSDebug;           /*                                            */
    int     myThrdId;
    int     ButStatus;         /* -1 = idle; 0 = initiating; 1 = running     */
    int     readyflag;         /* ready for data                             */
    int     got_a_menu;
    int     nPlots;            /* number of plots actually needed            */
    int     Current_Plot;      /* number of plot being worked on             */
    
    int     ncomp;              /* Number of Acceptable component types      */
    char    Comp[MAX_COMPS][5]; /* Acceptable component types                */
    
    char    TraceBuf[MAXTRACEBUF]; /* This should work for 24-bit digitizers */
    char    mod[20];
    
    int     nltargets;         /* Number of local target directories         */
    char    loctarget[MAX_TARGETS][100];/* Target in form-> /directory/      */
    
    int     Use_QDDS;          /* Flag that QDDS add-on files are being used */
    char    QDDSDir[GDIRSZ];   /* Directory for QDDS addon files             */
    char    QDDSLogo[GDIRSZ];  /* Logo for QDDS addon files                  */
    char    QDDSLink[GDIRSZ];  /* Directory on server for add-on link        */
    char    QDDSLabel[GDIRSZ]; /* Label for QDDS addon files                 */
   
    char    GifDir[GDIRSZ];    /* Directory for .gif & .html on local machine */
    char    Prefix[PRFXSZ];    /* Prefix for .gif & .html files on target machine */
    PltPar  plt[MAXPLOTS];     /* plotting parameters                        */
    int     logo;              /* =1 if logo                                 */
    int     logox, logoy;      /* Dimensions of logo                         */
    char    logoname[GDIRSZ+50]; /* Name of the logo GIF                     */
    int     status;
    pid_t   pid;
    WS_MENU_QUEUE_REC menu_queue[MAX_WAVESERVERS];
    
    int     SecsToWait;    /* Number of seconds to wait before starting plot */
    
    int     NSCN;              /* Number of SCNs we know about               */
    ChanInfo    Chan[MAXCHANNELS];
/* Globals to set from configuration file
 ****************************************/
    long    wsTimeout;         /* seconds to wait for reply from ws          */
    int     nServer;           /* number of wave servers we know about       */
    long    RetryCount;        /* Retry count for waveserver errors.         */
                        /* list of available waveServers, from config. file  */
    int     inmenu[MAX_WAVESERVERS];
    int     wsLoc[MAX_WAVESERVERS];
    char    wsIp[MAX_WAVESERVERS][MAX_ADRLEN];
    char    wsPort[MAX_WAVESERVERS][MAX_ADRLEN];
    char    wsComment[MAX_WAVESERVERS][50];

/* Variables used during the generation of each plot
 ***************************************************/
    long    samp_sec;          /* samples/sec                                */
    long    Npts;              /* Number of points in trace                  */
    double  Mean;              /* Mean value of the last data gulp           */
    long    MaxDistance;       /* Maximum offset distance for traces in this plot.   */
    
    double  Secs_Screen;       /* Maximum Number of secs of data to retrieve */
    double  Pre_Event;         /* Maximum Number of secs before event to start plot  */

    int     nentries;          /* Number of menu entries for this SCN        */
    double  TStime[MAX_WAVESERVERS*2]; /* Tank start for this entry          */
    double  TEtime[MAX_WAVESERVERS*2]; /* Tank end for this entry            */
    int     index[MAX_WAVESERVERS*2];  /* WaveServer for this entry          */
    
    int     nStaDB;              /* number of station databases we know about          */
    char    stationList[MAX_STADBS][STALIST_SIZ];
    
    char    indir[GDIRSZ];       /* Directory for manual-input arc files               */
    int     arcfileflg;          /* Flag set if we are accepting arc files             */
    int     UseDST;              /* Daylight Savings Time used when needed             */
    short   LocalTime;           /* Offset of local time from GMT e.g. -7 = PST        */
    char    LocalTimeID[4];      /* Local time ID e.g. PST                             */
    short   Use_all_Sites;       /* Flag                                               */
    long    MaxChannels;         /*                                                    */
    long    MaxDist;             /* Maximum offset distance for traces to plot.        */
    double  MinSize;             /* Minimum size event to report                       */
    long    MaxList;             /* Maximum number of entries in event list.           */
    long    MaxDays;             /* Maximum number of days in event list.              */
    double  RedSize;             /* Event size to switch to red in list                */
};
typedef struct Global Global;
