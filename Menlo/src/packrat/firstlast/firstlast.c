
    /*******************************************************************
     *                            firstlast                            *
     *                                                                 *
     *  Program to find the times of the earliest and latest data in   *
     *  a tracebuf file.                                               *
     *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "trace_buf.h"
#include "time_ew.h"

#define FNLEN 80       // Max length of file names

void PrintGmtime( double, int );
int  GetFirstLast( char *tbfname, double *starttime, double *endtime );


int main( int argc, char *argv[] )
{
   char   *fnames;           // Name of file containing tracebuf file names
   char   syscmd[80];
   char   tbfname[FNLEN];
   char   outfname[] = "firstlast.out";
   FILE   *tempfp;
   FILE   *outfp;
   double starttime;
   double endtime;

/* Open the output file which will contain the first
   and last times of all tracebuf files.
   Any old versions of output file will be overwritten.
   ***************************************************/
   outfp = fopen( outfname, "w" );
   if ( outfp == NULL )
   {
      printf( "Error opening output file: %s\n", outfname );
      printf( "Exiting.\n" );
      return -1;
   }

/* Get names of tracebuf files and write
   them to a temporary file in /var/tmp
   *************************************/
   fnames = tmpnam( NULL );
   strcpy( syscmd, "find . -print | cut -d/ -f2 | grep tb$ | sort > " );
// strcpy( syscmd, "find . -print | grep tb | grep _ | sort > " );
// strcpy( syscmd, "/bin/ls -1 *.tb > " );
   strcat( syscmd, fnames );
   system( syscmd );

/* Open the temporary file
   ***********************/
   tempfp = fopen( fnames, "r" );
   if ( tempfp == NULL )
   {
      printf( "Error opening temporary file %s\n", fnames );
      return -1;
   }

/* Get time of first and last data in each tracebuf file
   *****************************************************/
   while ( fgets(tbfname, FNLEN, tempfp) != NULL )
   {
      int stl = strlen( tbfname );

      tbfname[stl-1] = '\0';
      fprintf( outfp, "%s", tbfname );
      if ( GetFirstLast( tbfname, &starttime, &endtime ) != 0 ) return 0;
      fprintf( outfp, "  %.2lf", starttime );
      fprintf( outfp, "  %.2lf", endtime );
      fprintf( outfp, "\n" );
   }
   fclose( tempfp );
   fclose( outfp );
   remove( fnames );   // Remove file containing tracebuf file names

/* Run gaplist program.  Gaplist reads file firstlast.out,
   and creates a log file named gaplist.log.  Gaplist.log
   shows gaps and overlaps between files.
   ******************************************************/
   system( "gaplist" );
   return 0;
}


int GetFirstLast( char *tbfname, double *starttime, double *endtime )
{
   FILE   *tbfp;
   int    filesize;
   int    seekbyte;
   int    nread;
   int    msgsize;
   int    nmsg;
// char   pbuf[23];
   TRACE_HEADER trh;
   struct stat fileinfo;

/* Get file size
   *************/
   if ( stat( tbfname, &fileinfo ) == -1 )
   {
      printf( "stat() error on tracebuf file: %s\n", tbfname );
      return -1;
   }
   filesize = fileinfo.st_size;

/* Open the tracebuf file
   **********************/
   tbfp = fopen( tbfname, "r" );
   if ( tbfp == NULL )
   {
      printf( "Error opening tracebuf file %s\n", tbfname );
      return -1;
   }

/* Get first tracebuf message
   **************************/
   nread = fread( &trh, sizeof(TRACE_HEADER), 1, tbfp );
   if ( nread < 1 )
   {
      printf( "fread() error on first tracebuf msg in file: %s\n", tbfname );
      fclose( tbfp );
      return -1;
   }
// printf( "%4d",     trh.pinno );
// printf( "  %-5s",  trh.sta );
// printf( "  %-3s",  trh.chan );
// printf( "   %-2s", trh.net );
// printf( "  %.0lf", trh.samprate );
// printf( "  %3d",   trh.nsamp );
// datestr23( trh.starttime, pbuf, 23 );
// printf( "  %s", pbuf );
// datestr23( trh.endtime, pbuf, 23 );
// printf( "  %s", pbuf );
// printf( "\n" );
   *starttime = trh.starttime;     // Return to calling program

/* Get last tracebuf message
   *************************/
   msgsize  = sizeof(TRACE_HEADER) + (trh.nsamp * sizeof(short));
   nmsg     = filesize / msgsize;
   seekbyte = msgsize * (nmsg - 1);

   if ( fseek( tbfp, seekbyte, SEEK_SET ) == -1 )
   {
      printf( "fseek() error on last tracebuf msg in file: %s\n", tbfname );
      fclose( tbfp );
      return -1;
   }
   nread = fread( &trh, sizeof(TRACE_HEADER), 1, tbfp );
   if ( nread < 1 )
   {
      printf( "fread() error on last tracebuf msg in file: %s\n", tbfname );
      fclose( tbfp );
      return -1;
   }
// printf( "%4d",     trh.pinno );
// printf( "  %-5s",  trh.sta );
// printf( "  %-3s",  trh.chan );
// printf( "   %-2s", trh.net );
// printf( "  %.0lf", trh.samprate );
// printf( "  %3d",   trh.nsamp );
// datestr23( trh.starttime, pbuf, 23 );
// printf( "  %s", pbuf );
// datestr23( trh.endtime, pbuf, 23 );
// printf( "  %s", pbuf );
// printf( "\n" );
   *endtime = trh.endtime;        // Return to calling program
   fclose( tbfp );
   return 0;
}
