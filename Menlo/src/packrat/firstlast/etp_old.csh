#!/bin/csh

echo -n "Current directory: "
pwd
echo "Directories with firstlast.out files:"
ls ../*/firstlast.out* | tail
echo -n "Enter name of previous directory: "
set prevdir = $<
set firstlast = "../"${prevdir}"/firstlast.out"
echo -n This value written to file endtimeprev:
tail -1 $firstlast | cut -c42- | tee endtimeprev
