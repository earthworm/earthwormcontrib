#!/bin/csh

set firstlast = `ls ../*/firstlast.out | tail -1`
echo -n "Using this firstlast file: "
echo $firstlast
echo
echo -n "Current directory: "
pwd
echo
echo -n This value written to file endtimeprev:
tail -1 $firstlast | cut -c42- | tee endtimeprev
