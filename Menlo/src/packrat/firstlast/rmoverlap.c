
    /*******************************************************************
     *                            rmoverlap                            *
     *                                                                 *
     *  Program to remove overlapping tracebuf files.                  *
     *  Duplicate data are permanently removed from hard drive.        *
     *                                                                 *
     *  Usage:  rmoverlap                                              *
     *******************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "trace_buf.h"

/* Function prototypes
   *******************/
int RemoveTotalOverlap( char *fname );
int RemovePartialOverlap( char *tbfname, double endtimeprev );
void logit_init( char *logName );
void logit( char *format, ... );
void LogCurrentTime( void );


int main( int argc, char *argv[] )
{
   FILE   *fp;
   char   line[80];
   char   firstlastoutname[] = "firstlast.out";
   double endtimeprev = -1.0;
   char   logfile[80] = "rmoverlap.log";

/* Open log file
   *************/
   logit_init( logfile );
   logit( "Program start time: " );
   LogCurrentTime();
 
/* Get value of endtimeprev from file endtimeprev.
   The program won't run without getting endtimeprev.
   *************************************************/
   fp = fopen( "endtimeprev", "r" );
   if ( fp == NULL )
      logit( "Error opening file endtimeprev.\n" );
   else
   {
      if ( fscanf( fp, "%lf", &endtimeprev ) < 1 )
         logit( "Error reading endtimeprev from file.\n" );
      fclose( fp );
   }
   if ( endtimeprev < 0.0 )
   {
      logit( "ERROR: Can't get endtimeprev from file, so we can't\n" );
      logit( "detect gaps at the beginning of the first file.\n" );
      logit( "Exiting.\n" );
      return 0;
   }
   logit( "endtimeprev: %.2lf\n", endtimeprev );

/* Open input file (output file from program firstlast)
   ****************************************************/
   fp = fopen( firstlastoutname, "r" );
   if ( fp == NULL )
   {
      logit( "Error opening input file: %s\n", firstlastoutname );
      logit( "Exiting.\n" );
      return 0;
   }

/* Get one line at a time from input file
   **************************************/
   while ( fgets( line, 80, fp ) != NULL )
   {
      char   fname[80];
      double starttime;
      double endtime;
      double gap;

      if ( sscanf( line, "%s%lf%lf", fname, &starttime, &endtime ) < 3 )
      {
         logit( "Error decoding line:\n%s\n", line );
         return 0;
      }

      gap = starttime - endtimeprev;

/* Total overlap.  Delete the entire current file.
   **********************************************/
      if ( endtime <= endtimeprev )
      {
         logit( "%s", fname );
         logit( "  total overlap %.2lf seconds\n", gap );
         if ( RemoveTotalOverlap( fname ) == 0 )
            logit( "File %s deleted.\n", fname );
         else
         {
            logit( "Error deleting file %s. Exiting.\n", fname );
            return 0;
         }
      }

/* Partial overlap.  Delete beginning part of current file
   that overlaps the previous file.
   *******************************************************/
      else if ( gap <= 0.0 )
      {
         logit( "%s", fname );
         logit( "  partial overlap %.2lf seconds\n", gap );
         if ( RemovePartialOverlap( fname, endtimeprev ) == 0 )
            logit( "Beginning of file %s deleted.\n", fname );
         else
         {
            logit( "RemovePartialOverlap() error. Exiting.\n" );
            return 0;
         }
         endtimeprev = endtime;
      }

/* No overlap detected
   *******************/
      else
         endtimeprev = endtime;
   }
   fclose( fp );
   logit( "Exiting rmoverlap\n" );
   return 0;
}


int RemovePartialOverlap( char *tbfname, double endtimeprev )
{
// int   pn = 1;      // Pin number to dump.  -1 = dump all pins.
   FILE  *tbfp;
   FILE  *tempfp;
   TRACE_HEADER trh;
   char  tbuf[MAX_TRACEBUF_SIZ];
   char  tempfname[80];

/* Open original tracebuf file
   ***************************/
   tbfp = fopen( tbfname, "r" );
   if ( tbfp == NULL )
   {
      logit( "Error opening file %s\n", tbfname );
      return -1;
   }

/* Open temporary tracebuf file
   ****************************/
   strcpy( tempfname, tbfname );
   strcat( tempfname, ".temp" );
   tempfp = fopen( tempfname, "w" );
   if ( tempfp == NULL )
   {
      logit( "Error opening file %s\n", tempfname );
      return -1;
   }

/* Get one tracebuf message at a time
   **********************************/
   while ( 1 )
   {
      int nread;
      int nwritten;

      nread = fread( &trh, sizeof(TRACE_HEADER), 1, tbfp );
      if ( nread < 1 ) break;

//    if ( (pn == -1) || (pn == trh.pinno) )
//    {
//       logit( "%4d",     trh.pinno );
//       logit( "  %-5s",  trh.sta );
//       logit( "  %-3s",  trh.chan );
//       logit( "   %-2s", trh.net );
//       logit( "  %.0lf", trh.samprate );
//       logit( "  %3d",   trh.nsamp );
//       logit( "  %.2lf", trh.starttime );
//       logit( "  %.2lf", trh.endtime );
//       if ( trh.starttime > (endtimeprev + 0.5/trh.samprate) )
//          logit( "  Keep" );
//       else
//          logit( "  Discard" );
//       logit( "\n" );
//    }

      nread = fread( &tbuf[0], sizeof(short), trh.nsamp, tbfp );
      if ( nread < trh.nsamp )
      {
         logit( "fread() error on tracebuf file: %s\n", tbfname );
         logit( "Exiting.\n" );
         exit( 0 );
      }

/* Write this tracebuf message to temporary file
   *********************************************/
      if ( trh.starttime > (endtimeprev + 0.5/trh.samprate) )
      {
         nwritten = fwrite( &trh, sizeof(TRACE_HEADER), 1, tempfp );
         if ( nwritten < 1 )
         {
            logit( "fwrite() error on temporary file: %s\n", tempfname );
            logit( "Exiting.\n" );
            exit( 0 );
         }

         nwritten = fwrite( &tbuf[0], sizeof(short), trh.nsamp, tempfp );
         if ( nwritten < trh.nsamp )
         {
            logit( "fwrite() error on temporary file: %s\n", tempfname );
            logit( "Exiting.\n" );
            exit( 0 );
         }
      }
   }
   fclose( tbfp );
   fclose( tempfp );

/* Replace original tracebuf file with the temporary file
   ******************************************************/
   if ( rename(tempfname,tbfname) == -1 )
   {
      logit( "Error renaming %s to %s\n", tempfname, tbfname );
      logit( "Exiting.\n" );
      exit( 0 );
   }
   return 0;
}


int RemoveTotalOverlap( char *fname )
{
   char ans[40];

   do {
      printf( "Deleting file %s.  Are you sure (y or n)? ", fname );
      gets( ans );
   } while (ans[0]!='y' && ans[0]!='n');
   if ( ans[0] == 'n' )
   {
      printf( "Exiting.\n" );
      exit( 0 );
   }
   return remove( fname );
}
