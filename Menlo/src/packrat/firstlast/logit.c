#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/varargs.h>

static FILE *fp;



     /*************************************************************
      *                          logit.c                          *
      *                                                           *
      *            Functions for maintaining log files.           *
      *                                                           *
      *  First, call logit_init.  Then, call logit.               *
      *                                                           *
      *  The program must link to time_ew.o.                      *
      *  In Solaris, the program must also link to the posix      *
      *  library (-lposix4)                                       *
      *                                                           *
      *************************************************************/



   /*************************************************************************
    *                             logit_init                                *
    *                                                                       *
    *      Call this function once before the other logit routines.         *
    *                                                                       *
    *   logName: Name of the log file                                       *
    *************************************************************************/

void logit_init( char *logName )
{
   fp = fopen( logName, "a" );
   if ( fp == NULL )
   {
      fprintf( stderr, "Error opening log file: %s\n", logName );
      fprintf( stderr, "Exiting.\n" );
      exit( 0 );
   }
   return;
}



   /*****************************************************************
    *                            logit                              *
    *                                                               *
    *          Function to log a message to stdout and              *
    *          to a disk file.                                      *
    *                                                               *
    *  The calling sequence is identical to printf.                 *
    *****************************************************************/

void logit( char *format, ... )
{
   char   buf[256];
   auto va_list ap;


   va_start( ap, format );
   vprintf( format, ap );
   vfprintf( fp, format, ap );
   fflush( fp );
   va_end( ap );
}


void LogCurrentTime( void )
{
   time_t ltime;

   time( &ltime );                   // Get current time
   logit( "%s", ctime(&ltime) );
   return;
}

/*
int main( int argc, char *argv[] )
{
   logit_init( "hello.log" );
   logit( "Hello world\n" );
   return 0;
}
*/
