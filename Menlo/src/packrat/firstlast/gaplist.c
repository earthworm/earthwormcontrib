
    /*******************************************************************
     *                             gaplist                             *
     *                                                                 *
     *  Program to find gaps and overlaps in the output from the       *
     *  firstlast program.                                             *
     *                                                                 *
     *  Usage:    gaplist                                              *
     *******************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

/* Function prototypes
   *******************/
void logit_init( char *logName );
void logit( char *format, ... );
void LogCurrentTime( void );


int main( int argc, char *argv[] )
{
   FILE   *fp;
   char   line[80];
   char   firstlastoutname[] = "firstlast.out";
   double endtimeprev = -1.0;
   double endtimeprevfile = -1.0;
   int    tapenumprev = 0;
   char   logfile[80] = "gaplist.log";

/* Open log file
   *************/
   logit_init( logfile );
   logit( "Program start time: " );
   LogCurrentTime();

/* Get value of endtimeprev from file endtimeprev.
   The program won't run without getting endtimeprev.
   *************************************************/
   fp = fopen( "endtimeprev", "r" );
   if ( fp == NULL )
      logit( "Error opening file endtimeprev.\n" );
   else
   {
      if ( fscanf( fp, "%lf", &endtimeprev ) < 1 )
         logit( "Error reading endtimeprev from file.\n" );
      fclose( fp );
   }
   if ( endtimeprev < 0.0 )
   {
      logit( "WARNING: Can't get endtimeprev from file, so we can't\n" );
      logit( "         detect gaps at beginning of first file.\n" );
   }
 
/* Open input file (output file from program firstlast)
   ****************************************************/
   fp = fopen( firstlastoutname, "r" );
   if ( fp == NULL )
   {
      logit( "Error opening input file: %s\n", firstlastoutname );
      logit( "Exiting.\n" );
      return 0;
   }

/* Get one line at a time from input file
   **************************************/
   while ( fgets( line, 80, fp ) != NULL )
   {
      char   fname[80];
      char   tapestr[5];
      int    tapenum;
      double starttime;
      double endtime;
      double gap;
      double gapfile;

/* Decode the line
   ***************/
      if ( sscanf( line, "%s%lf%lf", fname, &starttime, &endtime ) < 3 )
      {
         logit( "Error decoding line:\n%s\n", line );
         return 0;
      }
      memcpy( tapestr, fname, 4 );
      tapestr[4] = '\0';
      tapenum = atoi( tapestr );

/* Set endtimeprev if it wasn't obtained from file
   ***********************************************/
      if ( endtimeprev < 0.0 )
         endtimeprev = endtimeprevfile = starttime - 0.01;

      gap     = starttime - endtimeprev;
      gapfile = starttime - endtimeprevfile;

/* Total overlap.  The entire current file will be deleted by rmoverlap.
   ********************************************************************/
      logit( "%s  %.2lf  %.2lf", fname, starttime, endtime );
      if ( endtime <= endtimeprev )
         logit( "  total overlap %.2lf seconds\n", gap );

/* Partial overlap.  Delete beginning part of current file
   that overlaps the previous file.
   *******************************************************/
      else if ( gap <= 0.0 )
      {
         logit( "  partial overlap %.2lf seconds\n", gap );
         endtimeprev = endtime;
      }

/* Gap detected.  Data between the previous and current files were lost.
   ********************************************************************/
      else if ( gap > 0.015 )
      {
         logit( "  gap %.2lf seconds\n", gap );
         endtimeprev = endtime;
         if ( gap > 60.0 )
            logit( "WARNING: Large gap detected.\n" );
      }

/* No gap detected
   ***************/
      else
      {
         logit( "\n" );
         endtimeprev = endtime;
      }

/* File end time must be greater than start time
   *********************************************/
      if ( endtime < starttime )
         logit( "ERROR: File end time is less than file start time.\n" );
      

/* Time must not decrease from one file to the next on the same tape
   *****************************************************************/
      if ( tapenum == tapenumprev )
      {
         if ( gapfile < -0.005 )
            logit( "WARNING: Time decreases between files on the same tape.\n" );
      }
      tapenumprev     = tapenum;
      endtimeprevfile = endtime;
   }
   fclose( fp );
   logit( "Exiting gaplist\n" );
   return 0;
}
