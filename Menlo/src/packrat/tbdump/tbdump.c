
    /*******************************************************************
     *                              tbdump                             *
     *                                                                 *
     *  Program to dump the headers of a tracebuf file.                *
     *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "trace_buf.h"
#include "time_ew.h"


void PrintGmtime( double, int );


int main( int argc, char *argv[] )
{
   int   pn = -1;      // Pin number to dump.  -1 = dump all pins.
   int   seekbyte = 0;
   char  *tbfname;
   FILE  *tbfp;
   TRACE_HEADER trh;

/* Get command line arguments
   **************************/
   if ( (argc < 2) | (argc > 3) )
   {
      printf( "Usage: tbdump [filename] [pin]\n" );
      printf( "The pin argument is optional.\n" );
      printf( "If not specified, all pins are printed.\n" );
      return 0;
   }
   tbfname = argv[1];
   if ( argc > 2 ) pn = atoi( argv[2] );

/* Open the tracebuf file
   **********************/
   tbfp = fopen( tbfname, "r" );
   if ( tbfp == NULL )
   {
      printf( "Error opening tracebuf file %s\n", tbfname );
      return -1;
   }

/* Get one tracebuf message at a time
   **********************************/
   printf( "\n" );
   printf( "  pin  sta  chan net srate nsamp       starttime               endtime\n" );
   printf( "  ---  ---  ---- --- ----- -----       ---------               -------\n" );

   while ( 1 )
   {
      int nread;

      if ( fseek( tbfp, seekbyte, SEEK_SET ) == -1 )
      {
         printf( "fseek() error on tracebuf file: %s\n", tbfname );
         printf( "Exiting.\n" );
         break;
      }
      nread = fread( &trh, sizeof(TRACE_HEADER), 1, tbfp );
      if ( nread < 1 )
      {
         printf( "fread() error on tracebuf file: %s\n", tbfname );
         printf( "Exiting.\n" );
         break;
      }

      if ( (pn == -1) || (pn == trh.pinno) )
      {
         char pbuf[23];
         int  c;

         printf( "%4d",     trh.pinno );
         printf( "  %-5s",  trh.sta );
         printf( "  %-3s",  trh.chan );
         printf( "   %-2s", trh.net );
         printf( "  %.0lf", trh.samprate );
         printf( "  %3d",   trh.nsamp );
         datestr23( trh.starttime, pbuf, 23 );
         printf( "  %s", pbuf );
         datestr23( trh.endtime, pbuf, 23 );
         printf( "  %s", pbuf );
         printf( "\n" );

// A hex dump of SCN follows:
#ifdef JUNK
         printf( "sta:" );
         for ( c = 0; c < 7; c++ )
            printf( " %02x", trh.sta[c] );
         printf( "\n" );
         printf( "chan:" );
         for ( c = 0; c < 9; c++ )
            printf( " %02x", trh.chan[c] );
         printf( "\n" );
         printf( "net:" );
         for ( c = 0; c < 9; c++ )
            printf( " %02x", trh.net[c] );
         printf( "\n" );
#endif
      }
      seekbyte += sizeof(TRACE_HEADER) + (trh.nsamp * sizeof(short));
   }
   return 0;
}
