
    /*******************************************************************
     *                               lstb                              *
     *                                                                 *
     *  Program to list files from a continuous tape archive (cta)     *
     *  This is a Solaris-only program.                                *
     *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <time_ew.h>

void PositionTape( char tapeName[], int *n2list );
int TarTv( char *, int * );


int main( int argc, char **argv )
{
   int    rc;
   int    status;
   char   *tapeName;
   char   defaultTapeName[] = "/dev/rmt/2cn";
   int    n2list;
   int    nfiles = 0;

/* Get the name of the tape device
   *******************************/
   tapeName = (argc < 2) ? &defaultTapeName[0] : argv[1];

/* Position tape
   *************/
   PositionTape( tapeName, &n2list );

/* Use tar tv to list the contents of the tape.
   tar exit code 3 = tape read error
   *******************************************/
   while ( 1 )
   {
      rc = TarTv( tapeName, &status );

      if ( rc < 0 )                        /* Couldn't invoke tar */
      {  
         fprintf( stderr, "Couldn't invoke tar.  TarTv rc = %d\n", rc );
         break;
      }  

      if ( status > 0 )                    /* A tar error occurred */
      {
         fprintf( stderr, "Tar exit status: %d\n", status );
         break;
      }
      nfiles++;
      if ( nfiles >= n2list ) break;

/* If file kill-lstb exists, don't read any more tapes
   ***************************************************/
      if ( remove( "kill-lstb" ) == 0 )    // Check again here
      {
         fprintf( stderr, "Found file kill-lstb. Exiting.\n" );
         break;
      }
   }
   return 0;
}



void PositionTape( char tapedev[], int *n2list )
{
   char response[20];
   char command[40];
   char rs;
   int  rc;

   fprintf( stderr, "Enter 'r' to list files from the beginning of the tape, or\n" );
   fprintf( stderr, "enter 's' to rewind and skip files before listing files, or\n" );
   fprintf( stderr, "enter 'e' to list the last 10 files on the tape, or\n" );
   fprintf( stderr, "enter control-C to stop the program.\n" );
   do
   {
      fprintf( stderr, "Enter 'r' or 's' or 'e' or control-C: " );
      gets( response );
      rs = tolower( response[0] );
   } while( rs != 'r' && rs != 's' && rs != 'e' );

   if ( rs == 'r' )
   {
      sprintf( command, "mt -f %s rewind", tapedev );
      system( command );
   }
   if ( rs == 's' )
   {
      int nskip;
      fprintf( stderr, "How many files to skip after rewinding? " );
      gets( response );
      nskip = atoi( response );
      sprintf( command, "mt -f %s asf %d", tapedev, nskip );
      system( command );
   }
   if ( rs == 'e' )
   {
      sprintf( command, "mt -f %s eom", tapedev );
      system( command );
      sprintf( command, "mt -f %s bsf 11", tapedev );
      system( command );
      *n2list = 10;
   }

/* Get number of files to list
   ***************************/
   if ( rs == 'r' || rs == 's' )
   {
      do
      {
         fprintf( stderr, "Enter number of files to list, or\n" );
         fprintf( stderr, "enter 0 to list all files.\n" );
         gets( response );
         rc = sscanf( response, "%d", n2list );
      } while ( rc == 0 );
      if ( *n2list == 0 ) *n2list = 99999;
   }
   return;
}

