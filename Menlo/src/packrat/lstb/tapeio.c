
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

int errno;


   /***********************************************************************
    *                              TarTv()                                *
    *                                                                     *
    *         Use tar tv to list the contents of a tar tape.              *
    *                                                                     *
    *  Returns  0 = no error occurred                                     *
    *          -1 = fork error                                            *
    *          -2 = execl error                                           *
    *          -3 = waitpid error                                         *
    *          -4 = tar terminated by signal                              *
    *          -5 = tar stopped by signal                                 *
    *          -6 = unknown tar exit status                               *
    ***********************************************************************/

int TarTv( char *tapename,      /* Name of tape device */
           int  *stat )         /* Status code */
{
   pid_t pid;
   int   status;
   int   blocking_factor = 126;
   char  bfactor[40];

   sprintf( bfactor, "%d", blocking_factor );

/* Fork a child process to run the tar command
   *******************************************/
   switch( pid = fork() )
   {
      case -1:
         return -1;

      case 0:                          /* In child process */
         execlp( "tar", "tar", "tvfb", tapename, bfactor, NULL );
         return -2;

      default:
         break;
   }

/* Wait for the tar command to complete
   ************************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( status ) )          /* tar exited */
   {
      *stat = WEXITSTATUS( status );
      return 0;
   }
   else if ( WIFSIGNALED( status ) )   /* tar terminated by signal */
   {
      *stat = WTERMSIG( status );
      return -4;
   }
   else if ( WIFSTOPPED( status ) )    /* tar stopped by signal */
   {
      *stat = WSTOPSIG( status );
      return -5;
   }
   return -6;                         /* Unknown exit status */
}
