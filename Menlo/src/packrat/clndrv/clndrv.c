#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mtio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stropts.h>
#include <stdio.h>

int main( int argc, char *argv[] )
{
   char device[] = "/dev/rmt/1";
   int  fd;                        // File descriptor
   struct mtget mtg;

/* Open tape drive
   ***************/
   fd = open( device, O_RDONLY );
   if ( fd == -1 )
   {
      printf( "Error opening device: %s\n", device );
      return 0;
   }
   printf( "Device opened: %s\n", device );

/* Get status structure
   ********************/
   if ( ioctl(fd, MTIOCGET, &mtg) == -1 )
   {
      printf( "ioctl error\n" );
      return 0;
   }

/* Print status
   ************/
   printf( "mt_type:  %hd\n", mtg.mt_type );
   printf( "mt_dsreg: %0x hex\n", mtg.mt_dsreg );
   printf( "mt_erreg: %0x hex\n", mtg.mt_erreg );
   printf( "mt_resid: %0x hex\n", mtg.mt_resid );
   printf( "mt_flags: %0x hex\n", mtg.mt_flags );
   printf( "mt_fileno: %d\n", mtg.mt_fileno );
   printf( "mt_blkno:  %d\n", mtg.mt_blkno );
   printf( "mt_bf:    %hd\n", mtg.mt_bf );

/* Close tape drive
   ****************/
   close( fd );
   return 0;
}
