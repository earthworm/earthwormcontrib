
/*
       tapeio.h - Header file for tapeio.c
*/

#ifndef TAPEIO_H
#define TAPEIO_H

#include <sys/mtio.h>

/* Function prototypes
   *******************/
int WriteTarFile( char *fname, char *tapename, int blocksize, int *stat );

#endif
