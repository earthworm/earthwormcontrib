
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <unistd.h>
#include "tapeio.h"

   /***********************************************************************
    *                            WriteTarFile()                           *
    *                                                                     *
    *         Copies a file to tape using the Unix tar command.           *
    *                                                                     *
    *  Returns  0 = no error                                              *
    *          -1 = fork error                                            *
    *          -2 = execl error                                           *
    *          -3 = waitpid error                                         *
    *          -4 = tar terminated by signal                              *
    *          -5 = tar stopped by signal                                 *
    *          -6 = unknown tar exit status                               *
    ***********************************************************************/

int WriteTarFile( char *fname,           // Name of file to write
                  char *tapename,        // Name of tape device
                  int  blocking_factor,  // Block factor of tar tape
                  int  *stat )           // Status code
{
   pid_t pid;
   int   status;
   char  bfactor[40];

   snprintf( bfactor, 40, "%d", blocking_factor );

/* Fork a child process to run the tar command.
   The "e" option means: exit immediately on any
   write error, with a positive status code.
   *********************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                          /* In child process */
         execlp( "tar", "tar", "cefb", tapename, bfactor, fname, NULL );
         return -2;

      default:
         break;
   }

/* Wait for the tar command to complete
   ************************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( status ) )          /* tar exited */
   {
      *stat = WEXITSTATUS( status );
      return 0;
   }
   else if ( WIFSIGNALED( status ) )   /* tar terminated by signal */
   {
      *stat = WTERMSIG( status );
      return -4;
   }
   else if ( WIFSTOPPED( status ) )    /* tar stopped by signal */
   {
      *stat = WSTOPSIG( status );
      return -5;
   }
   return -6;                         /* Unknown exit status */
}
