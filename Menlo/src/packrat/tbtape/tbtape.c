
  /********************************************************************
   *                               tbtape                             *
   *                                                                  *
   *  Program to copy tracebuf files to tape, using tar.              *
   *  Each tracebuf file is deleted, after copying it to tape.        *
   ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <math.h>
#include <ctype.h>
#include <unistd.h>
#include "tapeio.h"

#define FNLEN 80       // Max length of file names

void FirstLast( char tapedev[], int blocking_factor );
char AppendOverwrite( char tapedev[] );
void logit_init( char *logName );
void logit( char *format, ... );
void LogCurrentTime( void );


int main( int argc, char *argv[] )
{
   int    blocking_factor = 126;
   char   tapedev[] = "/dev/rmt/2cn";     // c = Use hardware compression
   char   oas;               // Overwrite, append, or skip
   int    i;
   char   tbfname[FNLEN];    // Earthworm tracebuf file name
   int    nfile = 0;         // Number of tracebuf files
   char   *fnames;           // Name of file containing tracebuf file names
   char   syscmd[80];
   FILE   *fp;
   char   logfile[80] = "tbtape.log";

/* Open log file
   *************/
   logit_init( logfile );
   logit( "Program start time: " );
   LogCurrentTime();

/* Get names of tracebuf files and write
   them to a temporary file in /var/tmp.
   tb$ refers to lines that end in "tb".
   *************************************/
   fnames = tmpnam( NULL );

   strcpy( syscmd, "find . -print | cut -d/ -f2 | grep tb$ | sort > " );
// strcpy( syscmd, "find . -print | grep tb | grep _ | sort > " );
// strcpy( syscmd, "/bin/ls -1 *.tb > " );
// strcpy( syscmd, "/bin/ls -1 *.tb | sort -t _ -k 2 > " );
   strcat( syscmd, fnames );
   system( syscmd );

/* Get number of tracebuf files to be written to tape
   **************************************************/
   fp = fopen( fnames, "r" );
   if ( fp == NULL )
   {
      logit( "Error opening temporary file %s\n", fnames );
      return -1;
   }
   while ( fgets(tbfname, FNLEN, fp) != NULL )
      nfile++;
   rewind( fp );

/* List the first and last tar files on the tape
   *********************************************/
   FirstLast( tapedev, blocking_factor );

/* Append or overwrite tape?
   *************************/
   oas = AppendOverwrite( tapedev );

/* Get tracebuf file names from the temporary file.
   Write each tracebuf file to tape.
   ***********************************************/
   for ( i = 0; i < nfile; i++ )
   {
      int rc;
      int tarStatus;            // Status code
      int len;

      if ( fgets(tbfname, FNLEN, fp) == NULL )
         break;

      len = strlen( tbfname );
      tbfname[len-1] = NULL;

      if ( oas == 'a' ) logit( "Appending" );
      else logit( "Writing" );
      logit( " file %d of %d to tape: %s\n", i+1, nfile, tbfname );

      rc = WriteTarFile( tbfname, tapedev, blocking_factor, &tarStatus );
      if ( rc < 0 )
      {
         logit( "WriteTarFile() error. rc: %d\n", rc );
         return -1;
      }
      if ( tarStatus != 0 )
      {
         logit( "WriteTarFile() error. tarStatus: %d\n", tarStatus );
         return -1;
      }
      logit( "File %d of %d ", i+1, nfile );
      if ( oas == 'a' ) logit( "appended to tape\n" );
      else logit( "written to tape\n" );

/* Delete the tracebuf file
   ************************/
      if ( remove( tbfname ) == 0 )
      {
         logit( "Deleted file %s\n", tbfname );
      }
      else
      {
         logit( "Error deleting file %s\n", tbfname );
         logit( "Exiting.\n" );
         return -1;
      }


/* If file kill-tbtape exists, don't write any more tracebuf files
   ***************************************************************/
      if ( remove( "kill-tbtape" ) == 0 )
      {
         logit( "Found file kill-tbtape. Exiting.\n" );
         break;
      }
   }
   fclose( fp );

/* Delete the temporary file
   *************************/
   if ( remove(fnames) == -1 )
   {
      logit( "Error removing temporary file %s\n", fnames );
      return -1;
   }
   logit( "Program exit time: " );
   LogCurrentTime();
   return 0;
}


void FirstLast( char tapedev[], int blocking_factor )
{
   char response[20];
   char command[40];
   char yn;

// printf( "\nPlease verify the first and last files on the tape.\n" );
   printf( "\nPlease verify the last file on the tape.\n" );
// printf( "If the block size printed below is zero, the tape is empty.\n" );
// printf( "First tape file:\n" );
// sprintf( command, "mt -f %s rewind", tapedev );
// system( command );
// sprintf( command, "tar tvfb %s %d", tapedev, blocking_factor );
// system( command );

   printf( "Last tape file:\n" );
   sprintf( command, "mt -f %s eom", tapedev );
   system( command );
   sprintf( command, "mt -f %s bsf 2", tapedev );
   system( command );
   sprintf( command, "tar tvfb %s %d", tapedev, blocking_factor );
   system( command );
   return;
}


   /****************************************************
    *                AppendOverwrite()                 *
    *                                                  *
    *  Returns oas parameter.                          *
    ****************************************************/

char AppendOverwrite( char tapedev[] )
{
   char response[20];
   char command[40];
   char oas;

   printf( "\nEnter 'o' to overwrite existing files on the tape, or\n" );
   printf( "enter 'a' to append data to the end of the tape, or\n" );
   printf( "enter 's' to rewind and skip files before writing to tape, or\n" );
   printf( "enter control-C to stop the program.\n" );
   printf( "If you know the tape is empty, enter 'o'.\n" );
   do
   {
      printf( "Enter 'o' or 'a' or 's' or control-C: " );
      gets( response );
      oas = tolower( response[0] );
   } while( oas != 'o' && oas != 'a' && oas != 's' );

   if ( oas == 'o' )
   {
      sprintf( command, "mt -f %s rewind", tapedev );
      printf( "Rewinding tape\n" );
   }

   if ( oas == 'a' )
   {
      sprintf( command, "mt -f %s eom", tapedev );
      printf( "Advancing tape to end-of-media (eom)\n" );
   }

   if ( oas == 's' )
   {
      int nskip;
      printf( "How many files to skip after rewinding? " );
      gets( response );
      nskip = atoi( response );
      sprintf( command, "mt -f %s asf %d", tapedev, nskip );
      printf( "Rewinding and skipping %d files\n", nskip );
   }
   system( command );
   return oas;
}
