	PROGRAM TISK

C	24-JAN-1991 
C	Douglas D. Given
C	U.S.G.S., O.E.V.E.
C	525 So. Wilson Ave.
C	Pasadena, CA  91106
C
c	Read 4mm DAT tape and create an .EVT file
c	User specifies a start date/time (GMT) and a length (seconds)
c	and we do the rest...
c
c	A SQUIRREL tape has a variable number of files on it, the file length 
c	is set in the SQUIRREL.INI file and is usually tens of minutes/file.
c	The first block of each file is a dump of the WEB header area
c	  The variable 'w.lsli_end' contains the expected last RTC of the file
c	The following blocks are data buffers, one/tape-block
c	The last block in the file is the WEB again with 'w.lsli_end' updated.
c	The last file will invariably be truncated and the 'w.lsli_end' value
c	  in the first WEB block will be incorrect, the WEB block at the end
c	  of the file will have the correct value (or you can get it from the
c	  last data block).
c
c ITS ABOUT TIME
c	Things can be confusing because there are three time bases in the data:
c	1) LRTC - (Real Time Clock) An RTC is just a sample point. RTC's are 
c	   counted from the time the on-line system start collecting data so 
c	   each sample has a unique sequential number in the time series.
c	   Knowing this number, the sample rate, and the actual time of the
c	   first (or any) sample, you can figure out the true time of a sample.
c	2) LBUF - the buffer number of a packet of samples. Buffers are counted
c	   from system startup or reset. This is not a true time base because 
c	   you must translate to RTC's (using 'w.kSPB' =>'Samples-Per-Buffer')
c	3) DT - real world time is kept in Julian seconds. The actual time of
c	   sample 'w.ltirig' is 'w.dtirig' (as long as IRIGE decoded OK). This
c	   is our link to real time. LMIN is sometimes use to hold the Julian
c	   minute. It is just DT/60.0
c
c	NOTE: the terms 'block', 'record', and 'buffer' may be used 
c	      interchangebly in comments and variable names

	implicit real*8 (d)
	integer*4 irank /50/
c ... mag tape stuff
	include '($SSDEF)'		!tape status definitions
	include 'lib$glue:mt_tape.inc'
	record /mt_iosb/ kchan
	integer*4 mt_lun
	integer*4 old_rtc
	integer*4 nwords_dio

	INTEGER*4  LRTC_START, LRTC_STOP, LB_START, LB_STOP, LB_DUR
	INTEGER*4  ISKIP_FILE
	COMMON/RTC_BUF/ LRTC_START, LRTC_STOP, LB_START, LB_STOP, LB_DUR,
	1	ISKIP_FILE

c ... tape position and event information, 'mt.', 'ev.' variables, 'ibuff' array
	include 'TISK.INC'

	character*40 cfile	!KOM subroutine work space

C	character*1	cyn		!yes/no response 
	character*32	cdev
	character*7	cstate
c .. new stuff aww
	character*22	c22
	character*12	cnode
	character*10	ctape
	character*4	ctdev
	integer*4	nnode
	integer*4	tape_n
	
c ... intialize some values

	parameter lun_in  = 5		!input unit
	parameter lun_out = 6		!error/status output unit
	parameter lun_evt = 15		!unit number for .EVT file output

	mt_lun = lun_out

	bytes_per_tape = 5.E09

	cstate = ' '
	call kommcrc(ires)
	if (ires .gt. 0) then
	  call komstrc(6, cstate, ires)
	  if (ires .lt. 1) then
	    write(lun_out, *) 'posting state must be =< 6c.'
	    stop
	  else
	    cstate = cstate(:ires) // char(0)
	  end if
	else
	  cstate = 'REFALL' // char(0)
	  ires = 6
	end if
	write(lun_out, *) 'Ids will be posted for state:', cstate(:ires)

c ..... Terminal interaction
c ... ask for tape device to read
  100	continue

	write (lun_out, 7100) 
 7100	format (' Tape device name (e.g. mub0:): ', $)

	call komrdc(lun_in, ires)		!get a line
	ier = 100
	if (ires .eq. -10) goto 920		!^Z,  *EXIT*
	if (ires .eq. 0) then			!use default
	  CALL TRANSLATE_STRING('TISK$DEVICE', cdev, 'LMN$FILE_DEVICE', NC_DEV)
	  IF (NC_DEV .LE. 0) STOP 'NO TAPE DEVICE DEFINED (TISK$DEVICE).'
	else if (ires .lt. 0) then		!error
	  goto 100
	else
 	  call komstrc(31, cdev, ires)		!get device name
	  ier = 102
	  if (ires .lt. 0) then
	    goto 100				!error
	  else if (ires .gt. 0) then
	    NC_DEV = ires
	    call upper_case(cdev)		!force to uppercase
	    if (cdev(NC_DEV:NC_DEV) .ne. ':') then	!append ":" if not present
	      NC_DEV = NC_DEV + 1
	      cdev(NC_DEV:NC_DEV) = ':'
	    end if
	  end if				!else, no more devices in list
	end if

	cdev = cdev(:nc_dev) // char(0)

C ... ask tape length
 105	continue
	write (lun_out, 7200) 
 7200	format (' Tape length in gigabytes:', $)
	ier = 110
	call komrdc(lun_in, ires)		!get a line
	if (ires .gt. 0) then
	  ans = 0.
 	  call komvalc(ans, ires)		!get answer
	  ier = 112
	  if (ans .gt. 0.) then
	      bytes_per_tape = ans * 10. ** 9.
	  else if (ans .le. 0. .or. ires .le. 0) then
	    goto 105				!error
	  end if
	else if (ires .eq. -10) then
	  goto 920		!^Z,  *EXIT*
 	else if (ires .le. 0) then
	  goto 105
	end if

c ... mount the tape
 110	continue

	call mtmnt (kchan, cdev, mt_lun, ires)
	ier = 110
	if (ires .lt. 1) goto 8160

	write (lun_out, 7110) cdev(:nc_dev), kchan.chnl
 7110	format (' Device ', a<NC_DEV>, ' mounted, assigned channel ', i4)

c ... read the current tape file "header", return time, etc.
c	return info in "mt." structure/common

	call read_head (lun_out, kchan, ires)
	ier = 112
	if (ires .lt. 1) goto 910
	mt.dt_bot = mt.dt_bof 		!set beginning of tape time
	mt.kfile = 1

c ... dump some status stuff

	write (lun_out, *) '       ------- Some WEB information -------'
	write (lun_out, *) '       WEB_ID                  = ', w.web_id
	write (lun_out, *) '       # of pins               = ', w.npin
	write (lun_out, *) '       Digitizing rate         = ', w.krate
	write (lun_out, *) '       Samples / buffer        = ', w.kspb
	write (lun_out, *) '       Words / data buffer     = ', w.nwdio
	write (lun_out, *) '       Words / WEB parm        = ', w.nwpar
	write (lun_out, *) '       Current/last CUSPID     = ', w.l_cid
	write (lun_out, 7000) (w.c_net(j), j=1,4), (w.c_dev(k), k=1,4)
 7000	format ('        Acquiring network       = ', 4a,/,
	1	'        Acquiring device        = ', 4a)
c .. new stuff aww
	write (lun_out, *) '       Tape file #             = ', mt.kfile
	CNODE = ' '
	CALL MVC(12, W.C_T_DUB, %REF(CNODE))		! PACKRAT TAPE HOST
	READ(UNIT=CNODE,FMT='(6x,I6)') NNODE
	WRITE (LUN_OUT, *) '       Tape device node        = ',  NNODE

	CTDEV = ' '
	CALL MVC(4, W.C_T_DEV, %REF(CTDEV))		! DEVICE USED BY PACKRAT
	WRITE (LUN_OUT, *) '       Tape device             = ', CTDEV

	CTAPE = ' '
	CALL MVC(10, W.C_T_TAPE, %REF(CTAPE))		! PACKRAT TAPE #
	READ(UNIT=CTAPE, FMT='(I10)') TAPE_N
	WRITE (LUN_OUT, *) '       Tape number             = ',  TAPE_N

	CALL DATE22 (W.D_T_T0, %REF(C22))		! TIME TAPE STARTED
	WRITE (LUN_OUT, 7002) C22
 7002	FORMAT ('        Tape written at time    = ', a22)

c ... guess at tape end
	nwords_dio = w.nwdio
	dsec_per_tape = dble((bytes_per_tape/float(nwords_dio * 2))
	1	 * float(w.kspb) * w.stirig)

	mt.dt_eot = mt.dt_bot + dsec_per_tape

	call date22 (mt.dt_bot, %ref(mt.c_bot))
	write (lun_out, 7112) mt.c_bot, mt.ctime
 7112	format (/,' Tape begins at           : ', a22, 1x, a6)

c	call date22 (mt.dt_eot, %ref(mt.c_eot))
c	write (lun_out, 7114) mt.c_eot, mt.ctime
c 7114	format ( ' Tape ends approx. : ', a22, 1x, a6, ' if 60m')

	call date22 (mt.dt_eot, %ref(mt.c_eot))
	write (lun_out, 7115) mt.c_eot, mt.ctime
 7115	format ( ' Estimated end at approx. : ', a22, 1x, a6, 
	1	 ' (if 90m tape)')

	write (lun_out, 7116) mt.file_len, mt.file_len/60.0 
 7116	format ( ' Length of each file      : ', f7.1, 
	1  ' seconds (', f6.2, ' minutes)')

c ... get event parameters

  120	continue

	write (lun_out, *) ' '				!skip a line
	write (lun_out, *) 
	1 ' Enter event beginning date and time ', 
	1 ' (specify all fields, no defaults except seconds)'
	write (lun_out, *) 
	1 ' Ex: ', mt.c_bot
	write (lun_out, *)
	1 '   : YEAR MON DY HRMN SEC (free format)'

	write (lun_out, 7120) 
 7120	format ($, '    : ')

	call read_datime (lun_in, lun_out, ev.dt_start0, ires)
	ier = 120
	if (ires .eq. -10) goto 920		!^Z,  *EXIT*
	if (ires .eq. -1)  goto 120		!bad format
	if (ires .eq.  0)  goto 120		!bad format
	if (ires .lt.  0)  goto 120		!bad error

	lmin = ires			!Julian minute

	if (ev.dt_start0 .lt. mt.dt_bot) then 	!check that time is valid
	  ier = 121
	  write (lun_out, *) 
	1   ' ** ERROR: Event starts before beginning of this tape.'
	  write (lun_out, 7112) mt.c_bot, mt.ctime

	  goto 120

	else if (ev.dt_start0 .gt. mt.dt_eot) then
	  ier = 122
	  write (lun_out, *) 
	1 ' ** WARNING: Event probably starts after end of this tape.'
	  write (lun_out, 7115) mt.c_eot, mt.ctime
	  write (lun_out, *) '  We''ll give it a shot anyway...'

	end if

c ... get event duration
  130	continue

	write (lun_out, 7130)
 7130	format ($, ' Event length in seconds [60.]: ')

	call komrdc(lun_in, ires)
	ier = 130
	if (ires .eq. 0) then
	  ev.duration0 = 60.
	else if (ires .eq. -10) then
	  goto 920		!^Z,  *EXIT*
	else if (ires .lt. 0) then
	  goto 130
	else
	  call komvalc(ev.duration0, ires)
	  ier = 135
	  if (ires .lt. 1) goto 130
	  iskip_file = 0
	  call komintc(iskip_file, ires)
	  ier = 136
	  if (ires .lt. 1) goto 130
	  write(lun_out, *) 'skip file specified:', iskip_file
	end if

	ev.dt_stop0 = ev.dt_start0 + ev.duration0	!event stop time

	if (ev.dt_stop0 .gt. mt.dt_eot) then	!check
	  ier = 136
	  write (lun_out, *) 
	1' ** INFORMATION: I think event will continue on next tape.'
	  write (lun_out, *) 
	1'                 Be prepared to load it when asked.'
	end if

c ... figure start and stop buffer numbers for the requested event
c	Start/stop times will land in the middle of a buffer, therefore, we
c	must 'buffer align': shift start time back to nearest buffer boundary
c	and push stop time forward. Each shift will never be greater than the
c	time of one buffer. But total could be almost 2 buffers long.
c	EXAMPLE: if buffers are 100 samples long and you ask for the time 
c	period corrisponding to samples #100 and #101, we must retrieve 200 
c	samples (buffers 1 and 2) to get the two samples you want. We can only
c	get whole buffers.
c	The first RTC = 0 (not 1), therefore, the way to calculate the FIRST
c	RTC in any buffer is:
c
c		LRTC = W.KSPB * (LBUF - 1)
c	also	LBUF = (LRTC/W.KSPB) + 1	!to calc. buffer from RTC
c
c	Where W.KSPB is the number of Samples Per Buffer.
c	EXAMPLE: if there are 50 samples/buffer:
c		BUFFER #	FIRST RTC	LAST RTC
c		    1		  0		   49
c		    2		 50	 	   99
c		    3		100		  149
c		  300	      15000		15049

	lrtc_start =((ev.dt_start0 - mt.dt_base) * w.krate) + mt.lt_base
	lrtc_stop  =((ev.dt_stop0  - mt.dt_base) * w.krate) + mt.lt_base
	lb_start   = (lrtc_start/w.kspb) + 1
	lb_stop    = (lrtc_stop/w.kspb) + 1 
	lb_dur     = lb_stop - lb_start + 1	!total buffers we'll get
	lrtc_start = (lb_start - 1) * w.kspb
	lrtc_stop  = (lb_stop * w.kspb) - 1 

c ... estimate .EVT file size
	lsize = ((lb_dur * nwords_dio) + maxreg)/256		!size in blocks

	write (lun_out, *) ' The .EVT file will require', lsize,
	1	' disk blocks.'

c ... reset start/stop time and duration to reflect whole buffers above
	ev.dt_start = DT_OF_LRTC(lrtc_start)
	ev.dt_stop = DT_OF_LRTC(lrtc_stop)
	ev.duration = ev.dt_stop - ev.dt_start

c ... read a CUSPID number
  140	continue

	write (lun_out, 7140)
 7140	format ($, ' CUSP ID # [<rtn> = assigned automatically]: ')

	call komrdc(lun_in, ires)
	ier = 140
	if (ires .eq. -10) goto 920		!^Z,  *EXIT*
	if (ires .eq. 0) then			!<rtn>
	  call nxtseq ('EVENT', ev.lid, ires)
	  ier = 142
	  if (ires .lt. 0) goto 910
	else if (ires .lt. 0) then
	  goto 140
	else
	  call komintc(ev.lid, ires)		!read ID
	  ier = 144
	  if (ires .lt. 1) goto 140		!error
	end if

c ... show results of interrogation

	call date22 (ev.dt_start0, %ref(ev.c_start0))
	call date22 (ev.dt_stop0,  %ref(ev.c_stop0))

	call date22 (ev.dt_start, %ref(ev.c_start))
	call date22 (ev.dt_stop,  %ref(ev.c_stop))

	write (lun_out, *) ' '
	write (lun_out, *) ' ------ EVENT SUMMARY ------'
	write (lun_out, *) 
	1 ' Note: Event has been aligned with whole data buffers.'
	write (lun_out, *) 
	1 '       Data buffers are', mt.secpb, ' seconds long.'

	write (lun_out, 7150) ev.lid, ev.c_start, ev.c_stop, 
	1		    ev.duration, cdev(:nc_dev)
 7150	format (/' CUSP ID number   = ', i10,/,
	1	 ' Event start time = ', a22, /,
	1	 ' Event stop  time = ', a22, /,
	1	 ' Event duration   = ', f8.2,' seconds',/,  
	1	 ' Tape device      = ', a<NC_DEV>,/)

c ... find the correct tape record where event begins

	write (lun_out, *) '--- Positioning tape to proper place ---'
	call find_block (lun_out, kchan, ires)
	ier = 240
	if (ires .lt. 1) goto 910
	write (lun_out, *) '--- Positioning of tape complete ---'

c ... build the .EVT file name

	cfile = 'CUSP$OUT:T' // CHAR(0)
	call conintc(cfile, ev.lid, ires)
	cfile = cfile(:ires) // '.EVT' // CHAR(0)

c ... open the .EVT file (as a "NEW" file)
	write (lun_out, *) '--- Opening .EVT file ---'
	l_blocks = 0
	call rms_openc (2, lun_evt, cfile, l_blocks, ires)
	ier = 300
	if (ires .lt. 0) goto 8300

c ... set some values in WEB so this will look like a proper event to REFORM
c	REFORM does some consistancy checks with these so they must be set.
c	Create pseudo event #1 in t. structure

	ndex = 1
	w.l_cid 	= ev.lid		!CUSP ID of "last" event
	w.kscarab	= ndex			!index in t. arrays
	t.lcid(ndex)	= ev.lid		!CUSP ID of this event
	t.ltron(ndex)	= lrtc_start 		!RTC of event start
	t.ltroff(ndex)	= lrtc_stop		!event end
	w.lsca_end	= lrtc_stop	!RTC of event end for scarab
	w.lsli_end	= lrtc_stop	!RTC of event end for sling
	if (w.l_cid .ne. w.ksling) then
	  write (lun_out, *) 'Warning - .evt # D.N.E. internal file_id',
	1	w.l_cid, ':', w.ksling
	end if
c ... write out the WEB region

	l_blocks = 0
	call rms_put (lun_evt, maxreg, w.web_id, l_block, ires)
	ier = 320
	if (ires .lt. 0) goto 910

	l_block = l_block + ires	!inc. pointer into .EVT file

c ... write out the first data buffer 
c	(it was read in CALL FIND_BLOCK and still resides in IBUFF array)
	nwords_dio = w.nwdio
	call rms_put (lun_evt, nwords_dio, ibuff, l_block, ires)
	ier = 330
	if (ires .lt. 0) goto 910

	l_block = l_block + ires
	lb_last = mt.lbuf
	kbuf = 1			!buffer output counter

c ... loop to write out data buffers ..............................

	write (lun_out, *) '--- Beginning data dump loop ---'
	loop_end = lb_stop - 1
	do while (lb_last .le. loop_end)

c ... read a tape block; could be data, WEB, EOF, EOT: check and handle IOSB(1) returned by MT routines
	  nwords_dio = w.nwdio
	  call mtrd (kchan, nwords_dio, ibuff, mt_lun, ires)
	  ier = 410
	  if (ires .eq. maxreg-1) then
	    call lib$movc3 (ires, %ref(ibuff), %ref(w.web_id)) !move buffer to WEB
	    write (lun_out, *) ' Jumping file boundary...  (Ummmph!)'
	    call mtspf (kchan, 1, mt_lun, ires)  !skip to head of next file
	    ier = 420
	    if (ires .lt. 0) goto 910
	    if (kchan.iosb(1) .eq. SS$_ENDOFVOLUME .or.
	1	kchan.iosb(1) .eq. SS$_ENDOFTAPE) then
	        call next_tape (lun_out, cdev(:nc_dev), kchan, ires)		!need new tape
	        if (ires .eq. 0) then
		  goto 800		!user asked to terminate tape
		else if (ires .lt. 0) then
		  ier = 421
	          goto 910
		end if
	    else
	      mt.kfile = mt.kfile + 1
	      call read_head (lun_out, kchan, ires)
	      ier = 422
	      if (ires .lt. 0) goto 910
	    end if

c ... if EOF (this should never happen, should hit WEB first)
	  else if (kchan.iosb(1) .eq. SS$_ENDOFFILE) then	!jump over file boundary
	    write (lun_out, *) ' Jumping file boundary...  (Ummmph!)'
C	    call mtspf (kchan, 1, mt_lun, ires)  !skip to head of next file
C	    ier = 430
C	    if (ires .ne. 1) goto 910
	    mt.kfile = mt.kfile + 1
	    call read_head (lun_out, kchan, ires)
	    ier = 431
	    if (ires .lt. 0) goto 910

c ... EOT (End-of-tape)
	  else if (kchan.iosb(1) .eq. SS$_ENDOFVOLUME .or.
	1	   kchan.iosb(1) .eq. SS$_ENDOFTAPE) 	then	!need new tape
	    call next_tape (lun_out, cdev(:nc_dev), kchan, ires)
	    if (ires .eq. 0) goto 800		!user asked to terminate tape
	    ier = 440
	    if (ires .lt. 0) goto 910

c ...	its a DATA buffer
	  else
	    mt.lbuf = ibuff.h.lbuf			!update current buffer info.
	    mt.lrtc = ibuff.h.lrtc
	    mt.krec = mt.krec + 1
	  end if

	  if (ibuff.h.lbuf .ne. lb_last + 1) then	! check for skipped buffers/resets
	    ier = 450
	    write(lun_out,*) ' !! Error -Core may have reset during event!!'
	    write(lun_out,*) ' Time tear - previous buff rtc:', old_rtc,
	1	' current rtc:', mt.lrtc
	    write (lun_out, *) 
	1	' Jumped from ', lb_last, ' to ', mt.lbuf
	    write (lun_out, *) 
	1	' *** ERROR: ', mt.lbuf-lb_last-1, ' buffer(s) skipped.'
	    goto 800		! 910
	  else
	    old_rtc = mt.lrtc
	  end if

c	... Buffer OK, write it out
	  nwords_dio = w.nwdio	  	
 	  call rms_put (lun_evt, nwords_dio, ibuff, l_block, ires)
	  ier = 460
	  if (ires .lt. 0) goto 910

	  l_block = l_block + ires		!inc. output pointer
	  kbuf = kbuf + 1			!inc. buffer counter
	  lb_last = ibuff.h.lbuf		!update last buffer output #

C	  if (mod(kbuf, 10) .eq. 0) then	!reassure every 10 buffers
C	    write (lun_out, *) kbuf, ' buffers output.'
C	  end if

	end do

c ... end of loop to write out data buffers ..............................

c ... finish up an event
 800	continue

c ... revise WEB values to reflect true event end

	lrtc_stop = (lb_last * w.kspb) - 1	!RTC of last point/buffer read

	ndex = 1
	w.l_cid 	= ev.lid		!CUSP ID of "last" event
	w.kscarab	= ndex			!index in t. arrays
	t.lcid(ndex)	= ev.lid		!CUSP ID of this event
	t.ltron(ndex)	= lrtc_start 		!RTC of event start
	t.ltroff(ndex)	= lrtc_stop		!event end
	w.lsca_end	= t.ltroff(ndex)	!RTC of start for scarab
	w.lsli_end	= t.ltroff(ndex)	!RTC of start for sling

c ... rewrite the WEB region
	l_block = 0
	call rms_put (lun_evt, maxreg, w.web_id, l_block, ires)
	ier = 820
	if (ires .lt. 0) goto 910

	write (lun_out, 7800) ev.lid, ev.c_start0, ev.c_stop0, 
	1		ev.duration0
 7800	format ( 
	1	 ' Original event request for CUSP ID ',i10,/,
	1	 '    Event start time = ', a22, /,
	1	 '    Event stop  time = ', a22, /,
	1	 '    Event duration   = ', f8.2,' seconds')

c 	... revise event parameters to reflect true event size
	ev.duration = kbuf*mt.secpb
	ev.dt_stop = DT_OF_LBUF(lb_last)
	1	 + DFLOAT(w.kspb-1)/DFLOAT(w.krate)	! AWW 3/27/93
	call date22 (ev.dt_stop, %ref(ev.c_stop))

	write (lun_out, 7376) ev.lid, ev.c_start, ev.c_stop, 
	1		ev.duration, kbuf
 7376	format ( 
	1	 ' Actual event resulting for CUSP ID ',i10,/,
	1	 '    Event start time = ', a22, /,
	1	 '    Event stop  time = ', a22, /,
	1	 '    Event duration   = ', f8.2,' seconds',/,
	1	 '    Buffers written  = ', i5)

	if (lb_last .ne. lb_stop) then
	  write (lun_out, *)
	1	'**** WARNING: event is not quite what you asked for.'
	  write (lun_out, *)
	1	'     lb_last=',lb_last , ' lb_stop =', lb_stop
	end if

c ... close the file

 890	continue

	close (lun_evt)			!close .EVT

	if (cstate .ne. ' ') then
	  call postc(cstate, ev.lid, irank, ires)
	  if (ires .lt. 0) then
	    write(lun_out, *) 'error - cannot post event id:', ev.lid
	  end if
	end if

	call mtdismo (cdev(:nc_dev), kchan.chnl, lun_out, ires)	!dismount /nounload

	write (lun_out, *) ' *** Don''t forget to remove your tape ***'

	call exit

c ... Non-fatal ERRORs
 8160	continue			!tape mount error

	if (ires .eq. -25) then		!error on mount

	  write (lun_out, 7816) cdev(:nc_dev), ires
 7816	  format ( '*** Error on mount ', a<NC_DEV>, 4x, i6)
	else if (ires .eq. -30) then	!error on assign
	  write (lun_out, 7817)  cdev(:nc_dev), ires
 7817	  format ( '*** Error on assign ', a<NC_DEV>, 4x, i6)
	  call mtdismo (cdev(:nc_dev), kchan.chnl, lun_out, ires)	!dismount /nounload
	end if

	goto 100

c	... couldn't open .EVT file
 8300	continue

	write (lun_out, 7830) cfile
 7830	format ('*** Could not open ', a)

	goto 910

c ... Fatal ERROR path
  910	continue

	write(lun_out, 7910) ier,	ires
 7910	format(' -- TISK -- FATAL ERROR : ier = ', 
	1	i6, ' ires = ', i6)

	WRITE(LUN_OUT,*) 'IOSB STATUS OF LAST MT CALL:'
	IRES = KCHAN.IOSB(1)
	CALL GETMSG(LUN_OUT, IRES)


c ... EXIT path
 920	continue

c ... dismount/nounload the tape
	if (kchan.chnl .ne. 0) then
	  call mtdismo (cdev(:nc_dev), kchan.chnl, lun_out, ires)
	end if
	call exit

	end

c ----------------------------------------------------------------------
	SUBROUTINE READ_DATIME (LUN_IN, LUN_OUT, DT, IRES)
C	Read in a date/time in CUSPish format, return Julian second
c	Only default allowed is seconds=0.0
c	IRES < -1	IER of serious data error
c	IRES = -1	illegal response
c	IRES = -10	^Z, exit
c	IRES = 0	no data on line
c	IRES > 0	LMIN of date/time
	implicit	none
	integer*4 lun_in
	integer*4 lun_out
	real*8 dt
	integer*4 ires
	integer*4 ier
	logical*4 it_is
C	implicit real*8 (d)
	integer*4 kyr,kmo,kda,khr,kmn
	common/GRG/ kyr, kmo, kda, khr, kmn	!date conversion array

C	character*3 cmo(12)
C	data cmo/ 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 
C	1	  'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'/
	character*20 cstr
	character*20 cpoint
	integer*4 ipos
	integer*4 lmin, khrmn
	real*4 sec

c ... suck in the whole input line
	call komrdc(lun_in, ires)	
	ier = 120
	if (ires .eq. -10) then			!^Z,  *EXIT*
	  return
	else if (ires .eq. 0) then		!no response, ask again
	  ires = 0
	  return
	else if (ires .lt. 0) then 		!error
	  goto 910
	end if

c ... parse it a token at a time
	call komintc(kyr, ires)			!read year
	ier = 122
	ipos = 1
	if (ires .eq. 0) goto 810		!incomplete date
	if (ires .lt. 0) goto 910
	if (kyr .lt. 100) kyr = kyr + 1900

	call komstrc(16, cstr, ires)		!read month
	ier = 124
	ipos = 6
	if (ires .eq. 0) goto 810		!incomplete date
	if (ires .lt. 0) goto 910

	kmo = 0
	if (it_is( 'JAN')) kmo = 1
	if (it_is( 'FEB')) kmo = 2
	if (it_is( 'MAR')) kmo = 3
	if (it_is( 'APR')) kmo = 4
	if (it_is( 'MAY')) kmo = 5
	if (it_is( 'JUN')) kmo = 6
	if (it_is( 'JUL')) kmo = 7
	if (it_is( 'AUG')) kmo = 8
	if (it_is( 'SEP')) kmo = 9
	if (it_is( 'OCT')) kmo = 10
	if (it_is( 'NOV')) kmo = 11
	if (it_is( 'DEC')) kmo = 12
	if (kmo .lt. 1) goto 810

	call komintc(kda, ires)			!read day
	ier = 126
	ipos = 10
	if (ires .eq. 0) goto 810		!incomplete date
	if (ires .lt. 0) goto 910

	if (ires .lt.  1) goto 810
	if (kda  .lt.  1) goto 810
	if (kda  .gt. 31) goto 810

	call komintc(khrmn, ires)
	ier = 127
	ipos = 13
	if (ires .eq. 0) goto 810		!incomplete date
	if (ires .lt. 0) goto 910

	khr = khrmn / 100
	kmn = khrmn - 100 * khr
	if(khr .lt. 0 .or. khr .gt. 23) goto 810
	if(kmn .lt. 0 .or. kmn .gt. 59) goto 810

	call komvalc(sec, ires)
	ier = 128
	ipos = 18
	if (ires .eq. 0) sec = 0.0		! default to 0.0 sec
	if (ires .lt. 0) goto 910

C .. convert time to Julian minute

	call grgmin (kyr, lmin)

	dt = lmin * 60.0d0 + sec

	ires = lmin

	return

c ... Bad response
  810	continue

	write (lun_out, *) 
	1	' ** ERROR in time: incomplete or bad format.'
	call komprtc(lun_out, ires)		!regurg last input line

c ... point at the bad field
	cpoint = '                    '
	cpoint(ipos:ipos) = '^'
	write (lun_out, *) cpoint
	ires = -1

	return

c ... Fatal ERROR path
  910	continue

	write(lun_out, 9100) ier,	ires
 9100	format(' -- READ_DATIME-- FATAL ERROR : ier = ', 
	1	i6, ' ires = ', i6)

	ires = -ier
	return

	end

c ----------------------------------------------------------------------
	SUBROUTINE READ_HEAD (IUNIT, KCHAN, IRES)
c	Read the first two records of a tape file and return info. through
c	the TISK.INC
c	Note that the block sizes requested are defined by the parameters
c	maxreg for the WEB block and w.nwdio for data blocks. These are defined
c	in TISK.INC based on parameters defined in RT$LIB:WEB.INC (which 
C	is included in TISK.INC).

	implicit real*8 (d)

c ... tape position information
	include 'TISK.INC'

	include '($SSDEF)'		!tape status definitions
	include 'lib$glue:mt_tape.inc'
	record /mt_iosb/ kchan
	integer*4 mt_lun
	integer*4 nwords_dio
c .. new stuff aww
	character*22	c22
	character*12	cnode
	character*10	ctape
	character*4	ctdev
	integer*4	nnode
	integer*4	tape_n

	mt_lun = iunit

c ... read the first WEB record, map it to WEB structure
	call mtrd (kchan, maxreg-1, w.web_id, mt_lun, ires)
	if (ires .ne. maxreg-1) then		!words read not equal WEB size
	  write (iunit, *) ' *** READ_HEAD ERROR: reading WEB block.'
	  write (iunit, *) ' maxreg = ', maxreg, '  WORDS = ', IRES
	  write (iunit, *) ' nwpar   = ', w.nwpar
	  write (iunit, *) ' nwdio   = ', w.nwdio
	  write (iunit, *) ' dtirig  = ', w.dtirig
	  write (iunit, *) ' ltirig  = ', w.ltirig
	  write (iunit, *) ' dtser   = ', w.dtser
	  write (iunit, *) ' ltser   = ', w.ltser
	  write (iunit, *) ' web_id  = ', w.web_id
	  write (iunit, *) ' npin    = ', w.npin
	  write (iunit, *) ' krate   = ', w.krate
	  write (iunit, *) ' kspb    = ', w.kspb
	  write (iunit, *) ' l_cid   = ', w.l_cid
	  write (iunit, 7000) (w.c_net(j), j=1,4), (w.c_dev(k), k=1,4)
 7000	  format ('        Acquiring network       = ', 4a,/,
	1	  '        Acquiring device        = ', 4a)
c .. new stuff aww
	  CALL MVC(12, W.C_T_DUB, %REF(CNODE))		! PACKRAT TAPE HOST
	  READ(UNIT=CNODE,FMT='(6x,I6)') NNODE
	  WRITE (iunit, *) '       Tape device node        = ', NNODE

	  CALL MVC(4, W.C_T_DEV, %REF(CTDEV))		! DEVICE USED BY PACKRAT
	  WRITE (iunit, *) '       Tape device             = ', CTDEV

	  CALL MVC(10, W.C_T_TAPE, %REF(CTAPE))		! PACKRAT TAPE #
	  READ(UNIT=CTAPE, FMT='(I10)') TAPE_N
	  WRITE (iunit, *) '       Tape number             = ', TAPE_N

	  CALL DATE22 (W.D_T_T0, %REF(C22))		! TIME TAPE STARTED
	  WRITE (iunit, 7002) C22
 7002	  FORMAT ('        Tape written at time    = ', a22)

	  ier = 205
	  goto 910
	end if

c ... get and remember time base information 

	if (w.dtirig .gt. 0.0) then		!IRIGE time base
	  mt.dt_base = w.dtirig 		! Julian sec of last decode
	  mt.lt_base = w.ltirig
	  mt.ctime = 'IRIGE '
	else if (w.dtser .gt. 0.0) then		!series time base (2nd choice)
	  mt.dt_base = w.dtser 			! Julian sec of last decode
	  mt.lt_base = w.ltser
	  mt.ctime = 'SYSTEM'
	end if

	mt.secps = 1.0/real(w.krate)			!sec/sample

c ... read the first data record
	nwords_dio = w.nwdio
	call mtrd (kchan, nwords_dio, ibuff, mt_lun, ires)
	if (ires .ne. nwords_dio) then		!words read not equal WEB size
	  write (iunit, *) ' *** READ_HEAD ERROR: reading DATA block.'
	  write (iunit, *) ' w.nwdio = ', w.nwdio, '  WORDS = ', ires
c	  ier = 215
c	  goto 910
	end if

	mt.lbuf = ibuff.h.lbuf		!buffer number from the header
	mt.lrtc = ibuff.h.lrtc		!RTC of the buffer
c ... calc time of this sample based on the LRTC
	mt.dt_bof = DT_OF_LRTC(mt.lrtc)

c ... calculate time of end-of-file (assuming 'w.lsli_end' contains the RTC of
c	the last buffer in the file) This may not be true for a truncated file
	rtc_file    = w.lsli_end - mt.lrtc		!file len in samples
	if (w.stirig .eq. 0.) w.stirig = 0.01
	mt.file_len = rtc_file * w.stirig		!file len in seconds
	mt.dt_eof   = DT_OF_LRTC(w.lsli_end)

c ... some constants for later use
	mt.secpb = mt.secps * w.kspb		!seconds/buffer
	mt.kbpf  = mt.file_len / mt.secpb	!buffers/file

	mt.krec   = 3			!2 read, so we are pointed at 3rd

	ires = 1
	return

c ... Fatal ERROR path
  910	continue

	write(iunit, 7910) ier,	ires
 7910	format(' -- READ_HEAD -- FATAL ERROR : ier = ', 
	1	i6, ' ires = ', i6)

	WRITE(IUNIT, *) 'IOSB STATUS OF LAST MT CALL:'
	IRES = KCHAN.IOSB(1)
	CALL GETMSG(IUNIT, IRES)

	ires = -ier
	return

	end

c ----------------------------------------------------------------------
	SUBROUTINE FIND_BLOCK (IUNIT, KCHAN, IRES)
C	Given a buffer number "LBUF" find the tape block where that buffer is 
c	located
c	Assume we are at the head of a file and READ_HEAD has been called.
c	Assume tape hasn't moved since the last call to MTRD (...IBUF...)
c	IMPORTANT PROGRAMMING NOTE:
c	The action of reading a block (MTRD or READ_HEAD) moves the tape to
c	the next block. Therefore, the block you are current pointing at is
c	the one PAST the last one read. Also, WEB blocks, EOF markers,  etc
c	also count as a block.
c	If the block you want is past the middle of the file it would be faster
c	to skip to the EOF and backup to the block you want. I'll have to do
c	that some day when I have time.

	implicit real*8 (d)

c ... tape position information
	include 'TISK.INC'

	include '($SSDEF)'		!tape status definitions
	include 'lib$glue:mt_tape.inc'
	record /mt_iosb/ kchan
	integer*4 mt_lun
	integer*4 iskip
	integer*4 nwords_dio

	INTEGER*4  LRTC_START, LRTC_STOP, LB_START, LB_STOP, LB_DUR
	INTEGER*4  ISKIP_FILE
	COMMON/RTC_BUF/ LRTC_START, LRTC_STOP, LB_START, LB_STOP, LB_DUR,
	1	ISKIP_FILE

	mt_lun = iunit
	iskip = 0		! reposition attempts

c ... calculate file and buffer on tape where event should begin
c	(-1 because we are really pointed at block after mt.lbuf)

	ib_skip = LB_START - mt.lbuf -1		!buffers to skip

	if_skip = int(ib_skip/real(mt.kbpf))	!files to skip

c ... skip files
 100	continue

	if (iskip_file .ne. 0) then
	  if_skip = iskip_file
	  write(iunit, *) 'skip file specified:', if_skip
	  iskip_file = 0
	end if	  

	if (if_skip .ne. 0) then
	  write (iunit, *) ' Skipping ', if_skip, ' files...'
	  call mtspf (kchan, if_skip, mt_lun, ires)	!skip to ifile
	  ier = 240
	  if (ires .ne. abs(if_skip)) then
	    if (kchan.iosb(1) .eq. SS$_ENDOFVOLUME .OR.
	1	kchan.iosb(1) .eq. SS$_ENDOFTAPE ) then
		  goto 800
	    else
	      goto 910		!error
	    end if
	  end if

	  mt.kfile = mt.kfile + ires		!inc. file counter

c ... read the new file "header", check it

	  call read_head (iunit, kchan, ires)
	  ier = 250
	  if (ires .lt. 1) goto 910

	end if

c	write (iunit, *) ' File #  = ', mt.kfile
c	write (iunit, *) ' Block # = ', mt.krec
c	write (iunit, *) ' LRTC # = ', lrtc
c	write (iunit, *) ' LBUF #  = ', mt.lbuf
c	write (iunit, *) ' LBLK #  = ', lb_start, lb_stop

	call date22 (mt.dt_bof, %ref(mt.c_bof))
	write (iunit, 7250) mt.c_bof, mt.ctime
 7250	format (' File start time: ', a22, 1x, a6)

	call date22 (mt.dt_eof, %ref(mt.c_eof))
	write (iunit, 7260) mt.c_eof, mt.ctime
 7260	format (' File stop  time: ', a22, 1x, a6)

c ... OK, we should be in the right file, recalculate buffers given new data 
c	from  WEB in this file

	lrtc_start =((ev.dt_start0 - mt.dt_base) * w.krate) + mt.lt_base
	lrtc_stop  =((ev.dt_stop0  - mt.dt_base) * w.krate) + mt.lt_base
	lb_start   = (lrtc_start/w.kspb) + 1
	lb_stop    = (lrtc_stop/w.kspb) + 1	! AWW 3.27.93
	lb_dur     = lb_stop - lb_start + 1	!total buffers we'll get
	lrtc_start = (lb_start - 1) * w.kspb
	lrtc_stop  = (lb_stop * w.kspb) - 1 

  200	continue

	ib_skip = LB_START - mt.lbuf -1		!buffers to skip
	if_skip = int(ib_skip/real(mt.kbpf))	!files to skip
	iskip = iskip + 1
	if (iskip .gt. 10) then
	  write (iunit, *) 'Cannot find file/block after 10 skips',
	1	' ... assume missing data on tape.'
	  ires = 0
	  return
	end if
	if (if_skip .ne. 0) then
	  write (iunit, *) ' File:', mt.kfile, ' lb:', mt.lbuf,
	1	 'lb_start:', LB_START 
	  goto 100	!NOT as expected,skip files again
	end if
	write (iunit, *) ' Skipping ', ib_skip, ' buffers...'

	call mtspb (kchan, ib_skip, mt_lun, lrtn, ires)
	ier = 270
	mt.krec = mt.krec + sign(lrtn, ib_skip)	! MTSPB returns lrtn=ABS(buffers skipped)
	if (ABS(ib_skip) .ne. lrtn) then
	  if (kchan.iosb(1) .eq. SS$_ENDOFFILE) then			!Hit EOF
	    call mtspf (kchan, 1, mt_lun, ires)	!skip to next file
	    ier = 275
	    if (ires .lt. 1) then
	      if (kchan.iosb(1) .eq. SS$_ENDOFVOLUME .OR.
	1	kchan.iosb(1) .eq. SS$_ENDOFTAPE) then
	        goto 800			!hit EOT or EOV
	      else
	        goto 910
	      end if
	    end if
	    call read_head (iunit, kchan, ires)
	    ier = 276
	    if (ires .lt. 1) goto 910
	  else if (kchan.iosb(1) .eq. SS$_ENDOFVOLUME .OR.
	1	kchan.iosb(1) .eq. SS$_ENDOFTAPE) then
	    goto 800			!hit EOT or EOV
	  else
	    goto 910
	  end if
	else				!read the data buffer
	  nwords_dio = w.nwdio
	  call mtrd (kchan, nwords_dio, ibuff, mt_lun, ires)
	  ier = 280
	  if (ires .ne. nwords_dio) then		!words read not equal WEB size
	    write (iunit, *) ' *** FIND_BLOCK ERROR: reading DATA block.'
	    write (iunit, *) ' w.nwdio = ', w.nwdio, '  WORDS = ', ires
	    ier = 285
	    goto 910
	  end if
	end if

c ... get data from buffer header
	mt.lbuf = ibuff.h.lbuf
	mt.lrtc = ibuff.h.lrtc
	if (mt.lbuf .ne. LB_START) goto 200		!try again

	ires = mt.lbuf
	return

c ... ERROR paths
  800	continue

	write (iunit, *) ' *** FIND_BLOCK ERROR: hit EOT.'
	write (iunit, *) ' Number of files skipped:', kchan.iosb(2)
	ires = -ier
	return

c ... Fatal ERROR path
  910	continue

	write(iunit, 7910) ier,	ires
 7910	format(' -- FIND_BLOCK -- FATAL ERROR : ier = ', 
	1	i6, ' ires = ', i6)

	WRITE(IUNIT, *) 'IOSB STATUS OF LAST MT CALL:'
	IRES = KCHAN.IOSB(1)
	CALL GETMSG(IUNIT, IRES)

	ires = -ier
	return

	end

c ----------------------------------------------------------------------
	SUBROUTINE NEXT_TAPE (LUN_OUT, CDEV, KCHAN, IRES)
C	dispatch current tape, start on next tape
c	IRES = 0	terminate the event
c	IRES < 0	ERROR
c	IRES > 0	OK

	implicit real*8 (d)


c ... mag tape stuff
	include '($SSDEF)'		!tape status definitions
	include 'TISK.INC'
	include 'LIB$GLUE:MT_TAPE.INC'
	record /mt_iosb/ kchan
	integer*4 mt_lun

	CHARACTER*(*) CDEV
	CHARACTER*32  CTAPE
	character*1	cyn		!yes/no response 

	mt_lun = lun_out

	call mtrw (kchan, 'REWIND', mt_lun, ires)		!rewind tape
	ier = 100
	if (ires .lt. 1) goto 910

	call mtdismo (cdev, kchan.chnl, lun_out, ires) !dismount tape, unass. kchan
	ier = 110
	if (ires .lt. 1) goto 910

	write (lun_out, *) '**** NEED NEXT TAPE IN SEQUENCE...' 
	write (lun_out, 7110) CDEV
 7110	format('     Please put in on ', a)

C ... tape ready loop (keep on asking and it shall be given)
	cyn = ' '
	do while (cyn .ne. 'Y' .and. cyn .ne. 'N')

	  write (lun_out, 7120) 
 7120	  format 
	1   (' Type ''Y'' when ready or ''N'' to terminate [Y]: ',$)

	  read *, cyn

	  if (cyn .eq. ' ') cyn = 'Y'		!default

	  call upper_case(cyn)

	end do

	if (cyn .eq. 'N') then
	  ires = 0
	  return					!TTFN
	end if

c ... tape must be ready	      
	ctape = cdev // char(0)
	call mtmnt (kchan, ctape, mt_lun, ires)	!mount the next tape
	ier = 130
	if (ires .lt. 1) goto 910

	call read_head (lun_out, kchan, ires)
	ier = 135
	if (ires .lt. 0) goto 910

	mt.dt_bot = mt.dt_bof 		!set beginning of tape time
	mt.kfile = 1

	ires = 1

	return

c ... Fatal ERROR path
  910	continue

	write(LUN_OUT, 7910) ier,	ires
 7910	format(' -- NEXT_TAPE -- FATAL ERROR : ier = ', 
	1	i6, ' ires = ', i6)

	WRITE(LUN_OUT, *) 'IOSB STATUS OF LAST MT CALL:'
	IRES = KCHAN.IOSB(1)
	CALL GETMSG(LUN_OUT, IRES)

	ires = -ier

	end

c ----------------------------------------------------------------------
	SUBROUTINE MTDISMO (CDEV, CHNL, IUNIT, IRES)
c	dismount/nounload tape, deassign channel
	character*(*) cdev
	integer*4 sys$dismou
	include '($DMTDEF)'

c .. dismount /nounload
	ifunc = DMT$M_NOUNLOAD .or. DMT$M_ABORT
	lres = sys$dismou (cdev, %VAL(ifunc))
	ier = 10
	if (.not. lres) goto 910

c .. deassign channel
C NOTE: This returns lres=0 (SYSTEM$_FACILITY) which is technically an error
c	but it seems to deassign the channel OK, so we ignore it

	lres = sys$dassgn (%VAL(chnl))
	ier = 20
	if (lres .eq. 0 .or. lres .eq. 1) then
	  ires = 1
	  return
	end if

c ... error path
 910	continue
	write(iunit, 7910) ier,	lres
 7910	format(' -- MTDISMO -- FATAL ERROR : ier = ', 
	1	i6, ' lres = ', i6)
	call getmsg (iunit, lres)
	ires = -ier

	end

c ----------------------------------------------------------------------
	REAL*8 FUNCTION DT_OF_LRTC (LSAMPLE)
c	convert RTC to DT (Julian seconds)
	implicit real*8 (d)
	include 'TISK.INC'
	integer lsample

	dt_of_lrtc = mt.dt_base + (lsample - mt.lt_base) * mt.secps

	end

c ----------------------------------------------------------------------
	REAL*8 FUNCTION DT_OF_LBUF (LBUFFER)
c	convert Buffer to DT (Julian seconds)
	implicit real*8 (d)
	include 'TISK.INC'
	integer lbuffer

	lrtc_first = ((lbuffer - 1) * w.kspb)	! RTC of 1st sample AWW 3.27.93
	dt_of_lbuf = mt.dt_base + (lrtc_first - mt.lt_base) * mt.secps

	end

