/*
 *   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT USING THE COMMAND CHECKOUT.
 *
 *    $Id: chron.h 452 2009-03-03 20:04:20Z kohler $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.1  2009/03/03 19:54:11  kohler
 *     *** empty log message ***
 *
 *     Revision 1.1  2000/02/14 16:00:43  lucky
 *     Initial revision
 *
 *
 */

struct Greg {
	int year;
	int month;
	int day;
	int hour;
	int minute;
};
