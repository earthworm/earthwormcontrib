
#include <sunmath.h>


void VaxF2Ieee( unsigned char datasrc[], float *datadest )
{
   fp_exception_field_type excep;
   int rounding;
   int nconvert = 1;
   convert_external_t src, dest;

   src.arch = convert_external_vax;
   src.type = convert_external_float;
   src.size = 4;
   dest.arch = convert_external_pc;
   dest.type = convert_external_float;
   dest.size = 4;
   rounding = convert_external_common;

   excep = convert_external( (char *)datasrc, src, (char *)datadest,
                              dest, rounding, nconvert );
   return;
}


void VaxD2Ieee( unsigned char datasrc[], double *datadest )
{
   fp_exception_field_type excep;
   int rounding;
   int nconvert = 1;
   convert_external_t src, dest;

   src.arch = convert_external_vax;
   src.type = convert_external_float;
   src.size = 8;
   dest.arch = convert_external_pc;
   dest.type = convert_external_float;
   dest.size = 8;
   rounding = convert_external_common;

   excep = convert_external( (char *)datasrc, src, (char *)datadest,
                              dest, rounding, nconvert );
   return;
}
