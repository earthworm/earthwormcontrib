#ifdef JUNK
// ... put here to make sure it is first
	INCLUDE 'WEB$INCLUDE'

// ... MAP TO ONE DATA BUFFER STRUCTURE (see RT$LIB:DIO_STRUCT.INC)
	RECORD /DIO/ IBUFF

// ... make the buffer large enough to take anything
// MAXIMUM TRANSFER SIZE IS 65535 BYTES
// PARAMETER NW_IBUFF = 65535/2	// BUFFER SIZE IN WORDS I*2 (used elsewhere)
// PARAMETER MAXWD_WEB = 256 * ((MAXREG + 255)/256)	// WHOLE PAGES
// ... DDG - WEB GREW TO > ONE TAPE I/O, added this param.
// PARAMETER MAXWD_WEB_MT = MIN(MAXWD_WEB, NW_IBUFF)	// WHOLE PAGES
// PARAMETER MAXWD_DIO = 256 * ((MAXDIO + 255)/256)	// WHOLE PAGES
// INTEGER*4 IBUFF(NW_IBUFF/2)	// SIZE of array is in longwords I*4

// COMMON/GREST/ IBUFF		// GREST page aligns the array
// EQUIVALENCE (IBUFF(1), WEB_VERSION)	// I*4 -> REAL*4
#endif

/* Mag tape information structure
   ******************************/
typedef struct mt {
   int    kfile;        // Current file number
   int    krec;         // Current record number
   double dt_base;      // Time base
   int    lt_base;      // RTC of the decode time
   float  secps;        // Sec/sample (=w.stirig)
   char   ctime[6];     // Time base is: 'IRIGE ' or 'SYSTEM' 
   double dt_bot;       // Time of beginning-of-tape
   double dt_eot;       // Time of end-of-tape
   double dt_bof;       // Time of beginning-of-file
   double dt_eof;       // Time of end-of-file
   char   c_bot[22];    // Char. beginning-of-tape
   char   c_eot[22];    // Char. end-of-tape
   char   c_bof[22];    // Char. beginning-of-file
   char   c_eof[22];    // Char. end-of-file
   int    kbpf;         // Buffers/tape file
   float  secpb;        // Seconds/buffer
   float  file_len;     // File length in seconds
   int    lbuf;         // Current buffer pointed to on tape
   int    lrtc;         // RTC of current buffer
} MT;

/* Event data
   **********/
typedef struct ev {
   double dt_start0;    // Event start time (asked for)
   double dt_stop0;     // Event stop time (asked for)
   float  duration0;    // Event duration (seconds)(asked for)
   char   c_start0[22]; // Char. start
   char   c_stop0[22];  // Char. stop
   double dt_start;     // Event start time (buffer aligned)
   double dt_stop;      // Event stop time (buffer aligned)
   float  duration;     // Event duration (seconds)(buffer aligned)
   char   c_start[22];  // Char. start
   char   c_stop[22];   // Char. stop
   int    lid;
} EV;
