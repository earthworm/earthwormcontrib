
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include "web.h"

void VaxF2Ieee( unsigned char datasrc[], float *datadest );
void VaxD2Ieee( unsigned char datasrc[], double *datadest );


void InitStrings( WEB w, PIN *p, SUB *s, short **wdb )
{
   int i;

   for ( i = 0; i < w.mpin; i++ )
   {
      p->csite[i] = malloc( 6 );
      p->cinst[i] = malloc( 4 );
      p->coper[i] = malloc( 6 );
   }

   for ( i = 0; i < MAXSUB; i++ )
      s->csub[i] = malloc( 32 );

   *wdb = calloc( w.nwdio, sizeof(short) );
   if ( *wdb == NULL )
   {
      printf( "calloc error in InitStrings[]. Exiting.\n" );
      exit( 0 );
   }
   return;
}


void CleanString( char str[], int nch )
{
   int i;
   int foundend = 0;

   for ( i = nch-1; i >= 0; i-- )
   {
      char c = str[i];

      if ( foundend )
         str[i] = isprint(c) ? c : ' ';
      else
      {
         if ( isprint(c) && !isspace(c) )
         {
            foundend = 1;
            str[i] = c;
         }
         else
            str[i] = NULL;
      }
   }
   str[nch-1] = NULL;
   return;
}


void ReadWeb( FILE *fp, WEB *w )
{
   char  *webbuf;
   int   i;
   int   nread;               /* Number of items read */
   char  *wp;
   unsigned char bb[8];

   webbuf = calloc( 2*MAXPAR, sizeof(char) );
   if ( webbuf == NULL )
   {
      printf( "calloc() error in ReadWeb(). Exiting.\n" );
      exit( -1 );
   }

   fseek( fp, 0L, SEEK_SET );

   nread = fread( webbuf, sizeof(char), 2*MAXPAR, fp );
   if ( nread < 2*MAXPAR )
   {
      printf( "Can't read web buffer in ReadWeb(). Exiting.\n" );
      exit( -1 );
   }

/* Copy parameters from the web buffer to the web structure
   ********************************************************/
   wp = webbuf;
   memcpy( bb,                wp, sizeof(float) );   wp += 4;
   VaxF2Ieee( bb, &w->web_id );
   memcpy( &w->nwpar,         wp, sizeof(long) );    wp += 4;
   memcpy( &w->mpin,          wp, sizeof(long) );    wp += 4;
   memcpy( &w->npin,          wp, sizeof(long) );    wp += 4;
   memcpy( &w->msub,          wp, sizeof(long) );    wp += 4;
   memcpy( &w->nsub,          wp, sizeof(long) );    wp += 4;
   memcpy( &w->mweb,          wp, sizeof(long) );    wp += 4;
   memcpy( &w->nweb,          wp, sizeof(long) );    wp += 4;
   memcpy( &w->kstop,         wp, sizeof(long) );    wp += 4;
   memcpy( &w->krate,         wp, sizeof(long) );    wp += 4;
   memcpy( &w->kfact,         wp, sizeof(long) );    wp += 4;
   memcpy( &w->lpacer,        wp, sizeof(long) );    wp += 4;
   memcpy( &w->kchlo,         wp, sizeof(long) );    wp += 4;
   memcpy( &w->kchhi,         wp, sizeof(long) );    wp += 4;
   memcpy( &w->kspb,          wp, sizeof(long) );    wp += 4;
   memcpy( &w->kframe,        wp, sizeof(long) );    wp += 4;
   memcpy( &w->nbuf,          wp, sizeof(long) );    wp += 4;
   memcpy( &w->ireftek,       wp, sizeof(long) );    wp += 4;
   memcpy( &w->nwdata,        wp, sizeof(short) );   wp += 2;
   memcpy( &w->nwdma,         wp, sizeof(short) );   wp += 2;
   memcpy( &w->nwdio,         wp, sizeof(short) );   wp += 2;
   memcpy( &w->nwnul,         wp, sizeof(short) );   wp += 2;
   memcpy( &w->lbuf,          wp, sizeof(long) );    wp += 4;
   memcpy( &w->l_err,         wp, sizeof(long) );    wp += 4;
   memcpy( &w->lb_next,       wp, sizeof(long) );    wp += 4;
   memcpy( &w->lb_last,       wp, sizeof(long) );    wp += 4;
   memcpy( &w->ksca_stop,     wp, sizeof(long) );    wp += 4;
   memcpy( &w->ksli_stop,     wp, sizeof(long) );    wp += 4;
   memcpy( &w->ldmax,         wp, sizeof(long) );    wp += 4;
   memcpy( &w->ktrgin,        wp, sizeof(long) );    wp += 4;
   memcpy( &w->lidout,        wp, sizeof(long) );    wp += 4;
   memcpy( &w->l_cid,         wp, sizeof(long) );    wp += 4;
   memcpy( &w->kscarab,       wp, sizeof(long) );    wp += 4;
   memcpy( &w->ksling,        wp, sizeof(long) );    wp += 4;
   memcpy( &w->lsca_end,      wp, sizeof(long) );    wp += 4;
   memcpy( &w->lsli_end,      wp, sizeof(long) );    wp += 4;
   memcpy( &w->kspi_on,       wp, sizeof(long) );    wp += 4;
   memcpy( &w->kspi_stop,     wp, sizeof(long) );    wp += 4;
   memcpy( &w->ndec,          wp, sizeof(long) );    wp += 4;
   memcpy( bb,                wp, sizeof(float) );   wp += 4;
   VaxF2Ieee( bb, &w->tstw );
   memcpy( bb,                wp, sizeof(float) );   wp += 4;
   VaxF2Ieee( bb, &w->tltw );
   memcpy( bb,                wp, sizeof(float) );   wp += 4;
   VaxF2Ieee( bb, &w->quiet );
   memcpy( bb,                wp, sizeof(float) );   wp += 4;
   VaxF2Ieee( bb, &w->sense );
   memcpy( bb,                wp, sizeof(float) );   wp += 4;
   VaxF2Ieee( bb, &w->tenon );
   memcpy( bb,                wp, sizeof(float) );   wp += 4;
   VaxF2Ieee( bb, &w->tenoff );
   memcpy( bb,                wp, sizeof(float) );   wp += 4;
   VaxF2Ieee( bb, &w->tprevt );
   memcpy( bb,                wp, sizeof(float) );   wp += 4;
   VaxF2Ieee( bb, &w->tpost );
   memcpy( &w->irig_stop,     wp, sizeof(long) );    wp += 4;
   memcpy( &w->kirige,        wp, sizeof(long) );    wp += 4;
   memcpy( &w->kyear,         wp, sizeof(long) );    wp += 4;
   memcpy( &w->ltirig,        wp, sizeof(long) );    wp += 4;
   memcpy( bb,                wp, sizeof(float) );   wp += 4;
   VaxF2Ieee( bb, &w->stirig );
   memcpy( bb,                wp, sizeof(double) );  wp += 8;
   VaxD2Ieee( bb, &w->dtirig );
   memcpy( &w->subalarm,      wp, sizeof(long) );    wp += 4;
   memcpy( &w->lerrtc,        wp, sizeof(long) );    wp += 4;
   memcpy( &w->n_err,         wp, sizeof(long) );    wp += 4;
   memcpy( w->kbad,           wp, 4*sizeof(long) );  wp += 16;
   memcpy( &w->ltsys,         wp, sizeof(long) );    wp += 4;
   memcpy( &w->ltmin,         wp, sizeof(long) );    wp += 4;
   memcpy( bb,                wp, sizeof(double) );  wp += 8;
   VaxD2Ieee( bb, &w->dtsys );
   memcpy( &w->ltser,         wp, sizeof(long) );    wp += 4;
   memcpy( bb,                wp, sizeof(double) );  wp += 8;
   VaxD2Ieee( bb, &w->dtser );
   memcpy( bb,                wp, sizeof(double) );  wp += 8;
   VaxD2Ieee( bb, &w->dtstop );
   memcpy( &w->ltstop,        wp, sizeof(long) );    wp += 4;
   memcpy( w->c_net,          wp, 4*sizeof(char) );  wp += 4;
   CleanString( w->c_net, 4 );
   memcpy( w->c_dev,          wp, 4*sizeof(char) );  wp += 4;
   CleanString( w->c_dev, 4 );
   memcpy( w->c_t_net,        wp, 4*sizeof(char) );  wp += 4;
   CleanString( w->c_t_net, 4 );
   memcpy( w->c_t_dev,        wp, 4*sizeof(char) );  wp += 4;
   CleanString( w->c_t_dev, 4 );
   memcpy( w->c_t_dub,        wp, 12*sizeof(char) ); wp += 12;
   CleanString( w->c_t_dub, 12 );
   memcpy( w->c_t_tape,       wp, 10*sizeof(char) ); wp += 10;
   CleanString( w->c_t_tape, 10 );
   memcpy( bb,                wp, sizeof(double) );  wp += 8;
   VaxD2Ieee( bb, &w->d_t_t0 );
   memcpy( bb,                wp, sizeof(float) );   wp += 4;
   VaxF2Ieee( bb, &w->r_t_dur );
   memcpy( bb,                wp, sizeof(float) );   wp += 4;
   VaxF2Ieee( bb, &w->r_t_rate );
   memcpy( w->c_t_track,      wp, 32*sizeof(char) ); wp += 32;
   CleanString( w->c_t_track, 32 );
   memcpy( w->c_t_stalst,     wp, 30*sizeof(char) ); wp += 30;
   CleanString( w->c_t_stalst, 30 );
   free( webbuf );
   return;
}


void ReadPin( FILE *fp, WEB w, PIN *p )
{
   int   i, j;
   char  *pinbuf;
   int   nread;               /* Number of items read */
   char  *pp;
   unsigned char bb[4];
   int   bufsize = 23 * w.mpin * sizeof(short);
   char  csite[MAXPIN][6];
   char  cinst[MAXPIN][4];
   char  coper[MAXPIN][6];

   pinbuf = malloc( bufsize );
   if ( pinbuf == NULL )
   {
      printf( "malloc() error in ReadPin(). Exiting.\n" );
      exit( -1 );
   }

   fseek( fp, 2*MAXPAR, SEEK_SET );

   nread = fread( pinbuf, sizeof(char), bufsize, fp );
   if ( nread < bufsize )
   {
      printf( "Can't read pin buffer in ReadPin(). Exiting.\n" );
      exit( -1 );
   }

/* Copy parameters from the pin buffer to the pin structure
   ********************************************************/
   pp = pinbuf;
   memcpy( csite, pp, 6 * w.mpin );               pp += 6 * w.mpin;
   for ( i = 0; i < 6; i++ )
      for ( j = 0; j < w.mpin; j++ )
      {
         char c = csite[j][i];
         p->csite[j][i] = (c>='A' && c<='Z') ? c : NULL;
      }
   memcpy( cinst, pp, 4 * w.mpin );               pp += 4 * w.mpin;
   for ( i = 0; i < 6; i++ )
      for ( j = 0; j < w.mpin; j++ )
      {
         char c = cinst[j][i];
         p->cinst[j][i] = (c>='A' && c<='Z') ? c : NULL;
      }
   memcpy( coper, pp, 6 * w.mpin );               pp += 6 * w.mpin;
   for ( i = 0; i < 6; i++ )
      for ( j = 0; j < w.mpin; j++ )
      {
         char c = coper[j][i];
         p->coper[j][i] = (c>='A' && c<='Z') ? c : NULL;
      }
   if ( fabsf(w.web_id - 2.01) < 0.0001 ) return;
   memcpy( &p->kpin,  pp, w.mpin*sizeof(short) ); pp += 2*w.mpin;
   memcpy( &p->kslm,  pp, w.mpin*sizeof(short) ); pp += 2*w.mpin;
   memcpy( &p->kvote, pp, w.mpin*sizeof(short) ); pp += 2*w.mpin;
   memcpy( &p->ktrg,  pp, w.mpin*sizeof(short) ); pp += 2*w.mpin;
   memcpy( &p->kdly,  pp, w.mpin*sizeof(short) ); pp += 2*w.mpin;
   for ( i = 0; i < w.mpin; i++ )
   {
      memcpy( bb, pp, sizeof(float) ); pp += 4;
      VaxF2Ieee( bb, (float *)&p->bias[i] );
   }
   for ( i = 0; i < w.mpin; i++ )
   {
      memcpy( bb, pp, sizeof(float) ); pp += 4;
      VaxF2Ieee( bb, (float *)&p->snr_on[i] );
   }
   for ( i = 0; i < w.mpin; i++ )
   {
      memcpy( bb, pp, sizeof(float) ); pp += 4;
      VaxF2Ieee( bb, (float *)&p->snr_off[i] );
   }
   for ( i = 0; i < w.mpin; i++ )
   {
      memcpy( bb, pp, sizeof(float) ); pp += 4;
      VaxF2Ieee( bb, (float *)&p->rb[i] );
   }
   for ( i = 0; i < w.mpin; i++ )
   {
      memcpy( bb, pp, sizeof(float) ); pp += 4;
      VaxF2Ieee( bb, (float *)&p->rbb[i] );
   }
   free( pinbuf );
   return;
}


void ReadSub( FILE *fp, SUB *s )
{
   int   i, j;
   char  *subbuf;
   int   nread;               /* Number of items read */
   char  *sp;
   int   bufsize = 17 * MAXSUB * sizeof(short);
   char  csub[MAXSUB][32];

   subbuf = malloc( bufsize );
   if ( subbuf == NULL )
   {
      printf( "malloc() error in ReadSub(). Exiting.\n" );
      exit( -1 );
   }

   fseek( fp, 2*(MAXPAR+23*MAXPIN), SEEK_SET );

   nread = fread( subbuf, sizeof(char), bufsize, fp );
   if ( nread < bufsize )
   {
      printf( "Can't read subnet buffer in ReadSub(). Exiting.\n" );
      exit( -1 );
   }

/* Copy parameters from the subnet buffer to the subnet structure
   **************************************************************/
   sp = subbuf;
   memcpy( csub, sp, 32*MAXSUB );                sp += 32*MAXSUB;
   for ( j = 0; j < MAXSUB; j++ )
   {
      for ( i = 0; i < 31; i++ )
      {
         char c = csub[j][i];
         s->csub[j][i] = isprint(c) ? c : NULL;
      }
      s->csub[j][31] = NULL;
   }
   memcpy( &s->kthr, sp, MAXSUB*sizeof(short) ); sp += 2*MAXSUB;
   free( subbuf );
   return;
}


void ReadNode( FILE *fp, NODE *n )
{
   char  *nodebuf;
   int   nread;               /* Number of items read */
   char  *np;
   int   bufsize = 2 * MAXWEB * sizeof(short);

   nodebuf = malloc( bufsize );
   if ( nodebuf == NULL )
   {
      printf( "malloc() error in ReadNode(). Exiting.\n" );
      exit( -1 );
   }

   fseek( fp, 2*(MAXPAR+23*MAXPIN+17*MAXSUB), SEEK_SET );

   nread = fread( nodebuf, sizeof(char), bufsize, fp );
   if ( nread < bufsize )
   {
      printf( "Can't read subnet buffer in ReadNode(). Exiting.\n" );
      exit( -1 );
   }

/* Copy parameters from the node buffer to the node structure
   **********************************************************/
   np = nodebuf;
   memcpy( &n->ksub, np, MAXWEB*sizeof(short) ); np += 2*MAXWEB;
   memcpy( &n->kchn, np, MAXWEB*sizeof(short) ); np += 2*MAXWEB;
   free( nodebuf );
   return;
}


int ReadWaveBlock( FILE *fp, short *wdb, int nsamp )
{
   char  *buf;
   int   nread;               /* Number of items read */
   char  *p;
   int   bufsize = nsamp * sizeof(short);

   buf = malloc( bufsize );
   if ( buf == NULL )
   {
      printf( "malloc() error in ReadWaveBlock(). Exiting.\n" );
      exit( -1 );
   }

   nread = fread( buf, sizeof(char), bufsize, fp );

/* Check for read error.  Probably read off end of file.
   ****************************************************/
   if ( nread < bufsize )
   {
      free( buf );
      return -1;
   }

   p = buf;
   memcpy( wdb, buf, nsamp*sizeof(short) ); p += nsamp * sizeof(short);
   free( buf );
   return 0;      // No error encountered
}
