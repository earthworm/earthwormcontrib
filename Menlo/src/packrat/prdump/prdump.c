
    /*******************************************************************
     *                              prdump                             *
     *                                                                 *
     *  Program to dump the headers of a packrat file that has been    *
     *  copied from tape using dd.                                     *
     *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "web.h"
#include "chron3.h"
#include "irige.h"

void ReadWeb( FILE *fp, WEB *w );
void PrintWeb( WEB w );
void InitStrings( WEB w, PIN *p, SUB *s, short **wdb );
void ReadPin( FILE *fp, WEB w, PIN *p );
void PrintPin( WEB w, PIN p );
void ReadSub( FILE *fp, SUB *s );
void PrintSub( WEB w, SUB s );
void ReadNode( FILE *fp, NODE *n );
void PrintNode( WEB w, NODE n );
void PrintBlockHeader( BLOCK *b, WEB w );
int  ReadWaveBlock( FILE *fp, short *wdb, int nsamp );
void PrintWave( WEB w, short *wd );
int  Irige( struct TIME_BUFF *, long, int, int, short *, int, int );
void PrintGmtime( double, int );


int main( int argc, char *argv[] )
{
   const int wdbh_size = 1024;   // Waveform data block header size
   const int web_size  = 65534;  // Web header block size
   int   seekbyte;
   char  *fname;
   FILE  *fp;
   WEB   w;               // Web structure (scalar parameters)
   PIN   p;               // Pin arrays structure
   SUB   s;               // Subnet arrays structure
   NODE  n;               // Trigger node membership structure
   short *wdb;            // Waveform data block, allocated in InitStrings[]
   BLOCK *wdbh;           // Waveform data block header structure
   short *wd;             // Array containing mux'd waveform data
   int   IrigeStatus;     // Initially, there is no status
   struct TIME_BUFF Tbuf; // Irige()'s read-write structure

/* Get command line arguments
   **************************/
   if ( argc != 2 )
   {
      printf( "Usage: prdump [filename]\n" );
      return 0;
   }
   fname = argv[1];

/* Open the disk file
   ******************/
   fp = fopen( fname, "r" );
   if ( fp == NULL )
   {
      printf( "Error opening file %s\n", fname );
      return -1;
   }

/* Read and print web structure, stored in
   the first web_size bytes of the disk file.
   *****************************************/
   ReadWeb( fp, &w );
   PrintWeb( w );

/* Do we know this format version?
   *******************************/
   if ( (fabsf(w.web_id - 2.01) > 0.0001) &&
        (fabsf(w.web_id - 2.05) > 0.0001) &&
        (fabsf(w.web_id - 2.60) > 0.0001) )
   {
     printf( "ERROR. This packrat version not recognized.\n" );
     printf( "web_id: %f\n", w.web_id );
     return -1;
   }

/* Allocate arrays in the pin and subnet structures.
   Arrays sizes are contained in web structure.
   Set addresses of the waveform data block header
   and the waveform data array.
   ************************************************/
   InitStrings( w, &p, &s, &wdb );
   wdbh = (BLOCK *)wdb;
   wd   = &wdb[wdbh_size];

/* Get the pin structure
   *********************/
   ReadPin( fp, w, &p );
   PrintPin( w, p );

/* The subnet structure
   ********************/
// ReadSub( fp, &s );
// PrintSub( w, s );

/* The node structure (subnet membership)
   **************************************/
// ReadNode( fp, &n );
// PrintNode( w, n );

/* Get one waveform block at a time, equivalent
   to a physical block (QIO) on the tape.
   ********************************************/
   printf( "\nBlock Headers:\n" );
   printf( "   lbuf    lrtc        Block start    IRIGE status\n" );
   printf( "   ----    ----        -----------    ------------\n" );

   seekbyte = web_size;     // Skip the file header (web) block
   while ( 1 )
   {
      fseek( fp, seekbyte, SEEK_SET );
      if ( ReadWaveBlock( fp, wdb, w.nwdio ) == -1 ) break;
      PrintBlockHeader( wdbh, w );

/* Decode IRIGE channel.
   Compare decoded time to time in header block.
   ********************************************/
      IrigeStatus = Irige( &Tbuf, wdbh->lrtc, w.npin, w.kspb, wd,
                           w.kirige, w.kyear );
      printf( " " );
      if      ( IrigeStatus == TIME_NOSYNC )
         printf( " Lost sync  " );
      else if ( IrigeStatus == TIME_FLAT )
         printf( " Flat       " );
      else if ( IrigeStatus == TIME_NOISE )
         printf( " Noisy      " );
      else if ( IrigeStatus == TIME_WAIT )
         printf( " Searching  " );
      else if ( IrigeStatus == TIME_OK )
         printf( " Locked-on  " );

      if ( IrigeStatus != TIME_WAIT )
         PrintGmtime( Tbuf.t - 11676096000., 3 );

      printf( "\n" );
//    PrintWave( w, wd );
      seekbyte += 2 * w.nwdio;
   }

   return 0;
}


void PrintString( char str[] )
{
   if ( strlen(str) > 0 )
      printf( "%s\n", str );
   else
      printf( "<null>\n" );
   return;
}


void PrintWeb( WEB w )
{
   char c18[20];
   int i;

   printf( "\nGlobal Parameters:\n" );
   printf( "web_id     %f\n",   w.web_id );
   printf( "nwpar        %6ld\n", w.nwpar );
   printf( "mpin         %6ld\n", w.mpin );
   printf( "npin         %6ld\n", w.npin );
   printf( "msub         %6ld\n", w.msub );
   printf( "nsub         %6ld\n", w.nsub );
   printf( "mweb         %6ld\n", w.mweb );
   printf( "nweb         %6ld\n", w.nweb );
   printf( "kstop        %6ld\n", w.kstop );
   printf( "krate        %6ld\n", w.krate );
   printf( "kfact        %6ld\n", w.kfact );
   printf( "lpacer       %6ld\n", w.lpacer );
   printf( "kchlo        %6ld\n", w.kchlo );
   printf( "kchhi        %6ld\n", w.kchhi );
   printf( "kspb         %6ld\n", w.kspb );
   printf( "kframe       %6ld\n", w.kframe );
   printf( "nbuf         %6ld\n", w.nbuf );
   printf( "ireftek      %6ld\n", w.ireftek );
   printf( "nwdata       %6hd\n", w.nwdata );
   printf( "nwdma        %6hd\n", w.nwdma );
   printf( "nwdio        %6hd\n", w.nwdio );
   printf( "nwnul        %6hd\n", w.nwnul );
   printf( "lbuf         %6ld\n", w.lbuf );
   printf( "l_err        %6ld\n", w.l_err );
   printf( "lb_next      %6ld\n", w.lb_next );
   printf( "lb_last      %6ld\n", w.lb_last );
   printf( "ksca_stop    %6ld\n", w.ksca_stop );
   printf( "ksli_stop    %6ld\n", w.ksli_stop );
   printf( "ldmax        %6ld\n", w.ldmax );
   printf( "ktrgin       %6ld\n", w.ktrgin );
   printf( "lidout       %6ld\n", w.lidout );
   printf( "l_cid        %6ld\n", w.l_cid );
   printf( "kscarab      %6ld\n", w.kscarab );
   printf( "ksling       %6ld\n", w.ksling );
   printf( "lsca_end   %6ld\n", w.lsca_end );
   printf( "lsli_end   %6ld\n", w.lsli_end );
   printf( "kspi_on      %6ld\n", w.kspi_on );
   printf( "kspi_stop    %6ld\n", w.kspi_stop );
   printf( "ndec         %6ld\n", w.ndec );
   printf( "tstw       %6f\n",  w.tstw );
   printf( "tltw       %6f\n",  w.tltw );
   printf( "quiet      %6f\n",  w.quiet );
   printf( "sense      %6f\n",  w.sense );
   printf( "tenon      %6f\n",  w.tenon );
   printf( "tenoff     %6f\n",  w.tenoff );
   printf( "tprevt     %6f\n",  w.tprevt );
   printf( "tpost      %6f\n",  w.tpost );
   printf( "irig_stop    %6ld\n", w.irig_stop );
   printf( "kirige       %6ld\n", w.kirige );
   printf( "kyear        %6ld\n", w.kyear );
   printf( "ltirig       %6ld\n", w.ltirig );
   printf( "stirig     %f\n",   w.stirig );
   printf( "dtirig     %lf",    w.dtirig );
   date18( w.dtirig, c18 );
   printf( "  %s\n", c18 );
   printf( "subalarm     %6ld\n", w.subalarm );
   printf( "lerrtc       %6ld\n", w.lerrtc );
   printf( "n_err        %6ld\n", w.n_err );

   printf( "kbad[4]    " );
   for ( i = 0; i < 4; i++ ) printf( "  %ld", w.kbad[i] );
   putchar( '\n' );

   printf( "ltsys        %6ld\n", w.ltsys );
   printf( "ltmin        %6ld\n", w.ltmin );
   printf( "dtsys      %lf",  w.dtsys );
   date18( w.dtsys, c18 );
   printf( "  %s\n", c18 );
   printf( "ltser        %6ld\n", w.ltser );
   printf( "dtser      %lf",  w.dtser );
   date18( w.dtser, c18 );
   printf( "  %s\n", c18 );
   printf( "dtstop     %lf\n",  w.dtstop );
   printf( "ltser        %6ld\n", w.ltser );
   printf( "ltstop       %6ld\n", w.ltstop );
   printf( "c_net      " ); PrintString( w.c_net );
   printf( "c_dev      " ); PrintString( w.c_dev );
   printf( "c_t_net    " ); PrintString( w.c_t_net );
   printf( "c_t_dev    " ); PrintString( w.c_t_dev );
   printf( "c_t_dub    " ); PrintString( w.c_t_dub );
   printf( "c_t_tape   " ); PrintString( w.c_t_tape );
   printf( "d_t_t0     %lf",  w.d_t_t0 );
   date18( w.d_t_t0, c18 );
   printf( "  %s\n", c18 );
   printf( "r_t_dur    %f\n",   w.r_t_dur );
   printf( "r_t_rate   %f\n",   w.r_t_rate );
   printf( "c_t_track  " ); PrintString( w.c_t_track );
   printf( "c_t_stalst " ); PrintString( w.c_t_stalst );
   return;
}


void PrintPin( WEB w, PIN p )
{
   int i;

   printf( "\nPin Arrays:\n" );
   if ( fabsf(w.web_id - 2.01) < 0.0001 )
   {

/* Print complete pin list here,
   including pins with bogus SCNs
   ******************************/
      printf( "pin site inst\n" );
      printf( "--- ---- ----\n" );

      for ( i = 0; i < w.npin; i++ )
      {
         printf( "%3d",   i );
         printf( " %-4s", p.csite[i] );
         printf( " %-3s", p.cinst[i] );
         putchar( '\n' );
      }
   }

   if ( fabsf(w.web_id - 2.05) < 0.0001 ||
        fabsf(w.web_id - 2.60) < 0.0001 )
   {
      printf( "site inst oper kpin kslm kvote ktrg kdly" );
      printf( "  bias  snr_on snr_off   rb      rbb\n" );
      printf( "---- ---- ---- ---- ---- ----- ---- ----" );
      printf( "  ----  ------ -------   --      ---\n" );

      for ( i = 0; i < w.npin; i++ )
      {
         if ( strcmp(p.csite[i], "XXX") == 0 ) continue;
         printf( "%-4s",      p.csite[i] );
         printf( " %-3s",     p.cinst[i] );
         printf( " %-2s",     p.coper[i] );
         printf( "  %4hd",    p.kpin[i] );
         printf( "   %2hd",   p.kslm[i] );
         printf( "   %2hd",   p.kvote[i] );
         printf( "   %2hd",   p.ktrg[i] );
         printf( "   %2hd",   p.kdly[i] );
         printf( " %8.3f",    p.bias[i] );
         printf( " %6.3f",    p.snr_on[i] );
         printf( " %6.3f",    p.snr_off[i] );
         printf( " %8.3f",    p.rb[i] );
         printf( " %8.3f",    p.rbb[i] );
         putchar( '\n' );
      }
   }
   return;
}


void PrintSub( WEB w, SUB s )
{
   int i;

   printf( "\nSubnet Arrays:\n" );
   printf( "subnet      csub                         kthr\n" );
   printf( "------      ----                         ----\n" );

   for ( i = 0; i < w.nsub; i++ )
   {
      printf( "   %2d",  i );
      printf( "   %s",   s.csub[i] );
      printf( "   %2hd", s.kthr[i] );
      putchar( '\n' );
   }
   return;
}


void PrintNode( WEB w, NODE n )
{
   int i;

   printf( "\nNode Arrays (trigger node membership):\n" );
   printf( "index   ksub   kchn\n" );
   printf( "-----   ----   ----\n" );

   for ( i = 0; i < w.nweb; i++ )
   {
      printf( "   %4d",  i );
      printf( "   %2hd", n.ksub[i] );
      printf( "   %3hd", n.kchn[i] );
      putchar( '\n' );
   }
   return;
}


void PrintBlockHeader( BLOCK *wdbh, WEB w )
{
/* Block start time (bst) is calculated from
   time of last IRIGE decode (dtirig).
   *****************************************/
   char c18[20];
   double bst = w.dtirig + (double)w.stirig * (wdbh->lrtc - w.ltirig);

   printf( "  %6d", wdbh->lbuf );
   printf( "  %6d", wdbh->lrtc );
   date18( bst, c18 );
   printf( "  %s",   c18 );
   return;
}


void PrintWave( WEB w, short *wd )
{
   int i;

   for ( i = 0; i < w.kspb; i++ )
   {
      int j;
      printf( "%2d", i );
      for ( j = 0; j < 10; j++ )
      {
         int k = (w.npin * i) + j;
         printf( "%7hd", wd[k] );
      }
      putchar( '\n' );
   }
   return;
}
