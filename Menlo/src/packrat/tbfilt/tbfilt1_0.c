
    /*******************************************************************
     *                             tbfilt                              *
     *                                                                 *
     *  Program to remove dummy channels from tracbuf files and        *
     *  remap SCN values using calsta2000.loc
     *                                                                 *
     *  File names are obtained from stdin.                            *
     *  Usage: ls *.tb | tbfilt                                        *
     *******************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include "trace_buf.h"
#include "time_ew.h"

#define MAXINT 2147483647
#define MAXPIN 1000

/* If FILTERFILE = 0, this program will list SCNs that are
   missing from calsta2000.loc, but these SCNs will not be
   removed from the tracebuf files.
   *******************************************************/
#define FILTERFILE 1

typedef struct {
   int  found;
   char sta[6];
   char chan[4];
   char net[3];
   long starttime;
} PIN;

typedef struct {
   int maxpinno;
   PIN ipin[MAXPIN];   // Input SCN
   PIN opin[MAXPIN];   // Output SCN
} PINLIST;

int  GetPinList( char tbfname[], PINLIST *pl );
int  FilterOneFile( char tbfname[], PINLIST *pl );
void SaveTimeCode( PINLIST *pl );


int main( int argc, char *argv[] )
{
   int     i;
   char    tbfname[80];
   PINLIST pl;

   while ( gets( tbfname ) != NULL )
   {

/* Get SCN mapping from the calsta2000.loc file
   ********************************************/
      printf( "\nGetting SCN mapping for tracebuf file %s\n", tbfname );
      if ( GetPinList( tbfname, &pl ) == -1 )
      {
         printf( "Exiting.\n" );
         return 0;
      }
      printf( "SCN mapping completed.\n" );

/* Save all time code channels, regardless of whether
   they are in calsta2000.loc
   **************************************************/
      SaveTimeCode( &pl );

/* Print lists of pins that we are going to discard
   from the tracebuf file.
   ************************************************/
      printf( "\nThese channels were not found in calsta2000.loc:\n" );
      for ( i = 0; i <= pl.maxpinno; i++ )
      {
         if ( pl.ipin[i].found == 0 ) continue;
         if ( pl.opin[i].found == 0 )
         {
            printf( "pin %5d", i );
            printf( " %-5s", pl.ipin[i].sta );
            printf( " %3s",  pl.ipin[i].chan );
            printf( " %2s",  pl.ipin[i].net );
            printf( "\n" );
         }
      }

      printf( "\nThese channels were found in calsta2000.loc,\n" );
      printf( "but the instrument was not in operation when\n" );
      printf( "the channels were digitized:\n" );
      for ( i = 0; i <= pl.maxpinno; i++ )
      {
         if ( pl.ipin[i].found == 0 ) continue;
         if ( pl.opin[i].found == 1 )
         {
            printf( "pin %5d", i );
            printf( " %-5s", pl.ipin[i].sta );
            printf( " %3s",  pl.ipin[i].chan );
            printf( " %2s",  pl.ipin[i].net );
            printf( "\n" );
         }
      }

/* Discard unwanted pins from the tracebuf file.
   If pl.opin[i].found =2, we want to save data from the pin.
   *********************************************************/
      if ( FILTERFILE )
      {
         printf( "\nFiltering file %s\n", tbfname );
         if ( FilterOneFile( tbfname, &pl ) == -1 )
         {
            printf( "Exiting.\n" );
            return 0;
         }
         printf( "File filtered: %s\n", tbfname );
      }
   }
   return 0;
}


  /*************************************************
   *                 FilterOneFile()               *
   *  Filter the tracebuf file using SCN mapping   *
   *************************************************/

int FilterOneFile( char tbfname[], PINLIST *pl )
{
   FILE  *tbfp;
   FILE  *tempfp;
   TRACE_HEADER trh;
   char  tbuf[MAX_TRACEBUF_SIZ];
   char  tempfname[80];

/* Open the original tracebuf file
   *******************************/
   tbfp = fopen( tbfname, "r" );
   if ( tbfp == NULL )
   {
      printf( "Error opening tracebuf file %s\n", tbfname );
      return -1;
   }

/* Open a temporary tracebuf file that will contain
   all the tracebuf messages we want to keep.
   ************************************************/
   strcpy( tempfname, tbfname );
   strcat( tempfname, ".temp" );
   tempfp = fopen( tempfname, "w" );
   if ( tempfp == NULL )
   {
      printf( "Error opening temporary file %s\n", tempfname );
      return -1;
   }

/* Get one tracebuf message at a time
   **********************************/
   while ( 1 )
   {
      int nread;
      int nwritten;
      int pin;

      nread = fread( &trh, sizeof(TRACE_HEADER), 1, tbfp );
      if ( nread < 1 ) break;

//    printf( "%4d",     trh.pinno );
//    printf( "  %-5s",  trh.sta );
//    printf( "  %-3s",  trh.chan );
//    printf( "   %-2s", trh.net );
//    printf( "  %.0lf", trh.samprate );
//    printf( "  %3d",   trh.nsamp );
//    printf( "  %.2lf", trh.starttime );
//    printf( "  %.2lf", trh.endtime );
//    printf( "\n" );

      nread = fread( &tbuf[0], sizeof(short), trh.nsamp, tbfp );
      if ( nread < trh.nsamp )
      {
         printf( "fread() error on tracebuf file: %s\n", tbfname );
         return -1;
      }

/* Discard this tracebuf message if its SCN
   was not found in calsta2000.loc
   ****************************************/
      pin = trh.pinno;
      if ( pl->opin[pin].found != 2 ) continue;

/* Set the SCNs in the tracebuf file to the
   values found in calsta2000.loc
   ****************************************/
      strcpy( trh.sta,  pl->opin[pin].sta );
      strcpy( trh.chan, pl->opin[pin].chan );
      strcpy( trh.net,  pl->opin[pin].net );

/* Write this tracebuf message to the temporary file
   *************************************************/
      nwritten = fwrite( &trh, sizeof(TRACE_HEADER), 1, tempfp );
      if ( nwritten < 1 )
      {
         printf( "fwrite() error on temporary file: %s\n", tempfname );
         return -1;
      }

      nwritten = fwrite( &tbuf[0], sizeof(short), trh.nsamp, tempfp );
      if ( nwritten < trh.nsamp )
      {
         printf( "fwrite() error on temporary file: %s\n", tempfname );
         return -1;
      }
   }
   fclose( tbfp );
   fclose( tempfp );

/* Replace original tracebuf file with the temporary file
   ******************************************************/
   if ( rename(tempfname,tbfname) == -1 )
   {
      printf( "Error renaming %s to %s\n", tempfname, tbfname );
      return -1;
   }
   return 0;
}


   /**************************************************
    *                  GetPinList()                  *
    *  Get SCN mapping from the calsta2000.loc file  *
    **************************************************/

int GetPinList( char tbfname[], PINLIST *pl )
{
   int          i;
   FILE         *tbfp;
   FILE         *csfp;
   TRACE_HEADER trh;
   long         offset = 0;
   int          pinprev = -99999;
   char         csfname[] = "/home/picker/packrat/src/pr2tb/calsta2000.loc";
   char         line[160];

/* Initialize input and output pin lists
   *************************************/
   for ( i = 0; i < MAXPIN; i++ )
   {
      pl->ipin[i].found = 0;
      pl->opin[i].found = 0;
   }

/* Open original tracebuf file
   ***************************/
   tbfp = fopen( tbfname, "r" );
   if ( tbfp == NULL )
   {
      printf( "Error opening tracebuf file %s\n", tbfname );
      return -1;
   }

/* Get SCNs, time, and pin numbers from the tracebuf file
   ******************************************************/
   while ( 1 )
   {
      int  nread;
      int  nwritten;
      long starttime;
      struct tm *stm;

      if ( fseek(tbfp, offset, SEEK_SET) == -1 )
      {
         printf( "fseek error\n" );
         fclose( tbfp );
         return -1;
      }

      nread = fread( &trh, sizeof(TRACE_HEADER), 1, tbfp );
      if ( nread < 1 ) break;

      if ( trh.pinno < 0 || trh.pinno >= MAXPIN )
      {
         printf( "Invalid pinno found: %d\n", trh.pinno );
         fclose( tbfp );
         return -1;
      }
      if ( trh.pinno <= pinprev ) break;
      pl->ipin[trh.pinno].found = 1;
      strcpy( pl->ipin[trh.pinno].sta,  trh.sta );
      strcpy( pl->ipin[trh.pinno].chan, trh.chan );
      strcpy( pl->ipin[trh.pinno].net,  trh.net );

      pl->ipin[trh.pinno].starttime = (long)trh.starttime;

      offset += sizeof(TRACE_HEADER) + (trh.nsamp * sizeof(short));
      pinprev = trh.pinno;
   }
   fclose( tbfp );

/* Get the biggest pin number we found in the tracebuf file
   ********************************************************/
   pl->maxpinno = -1;
   for ( i = 0; i < MAXPIN; i++ )
      if ( pl->ipin[i].found == 1 )
         pl->maxpinno = i;
   if ( pl->maxpinno == -1 )
   {
      printf( "No SCNs found in tracebuf file\n" );
      return -1;
   }

/* Reset the network code for station FRI from BK to WR
   ****************************************************/
   for ( i = 0; i < pl->maxpinno; i++ )
      if ( strcmp(pl->ipin[i].sta, "FRI") == 0 &&
           strcmp(pl->ipin[i].net, "BK")  == 0 )
         strcpy( pl->ipin[i].net,  "WR" );

/* Open the calsta2000.loc file and read one line at a time
   ********************************************************/
   csfp = fopen( csfname, "r" );
   if ( csfp == NULL )
   {
      printf( "Error opening calsta2000 file %s\n", csfname );
      return -1;
   }

   while ( fgets( line, 160, csfp ) != NULL )
   {
      int  j;
      char sta[6];
      char net[3];
      char chan[4];
      char ratn[5];
      char ratc[2];
      char str[5];
      int  syear;
      int  smonth;
      int  sday;
      int  eyear;
      int  emonth;
      int  eday;
      struct tm stm;
      time_t stime;
      time_t etime;

/* Decode SCN and rational name code from calsta2000.loc
   *****************************************************/
      for ( j = 0 ; j < 5; j++ )
         sta[j] = (line[j] != ' ') ? line[j] : '\0';
      sta[5] = '\0';
      for ( j = 0 ; j < 2; j++ )
         net[j] = (line[j+5] != ' ') ? line[j+5] : '\0';
      net[2] = '\0';
      for ( j = 0 ; j < 3; j++ )
         chan[j] = (line[j+8] != ' ') ? line[j+8] : '\0';
      chan[3] = '\0';
      for ( j = 0 ; j < 4; j++ )
         ratn[j] = (line[j+12] != ' ') ? line[j+12] : '\0';
      ratn[4] = '\0';
      for ( j = 0 ; j < 1; j++ )
         ratc[j] = (line[j+17] != ' ') ? line[j+17] : '\0';
      ratc[1] = '\0';

/* Decode instrument start and end dates from calsta2000.loc
   *********************************************************/
      for ( j = 0 ; j < 4; j++ )
         str[j] = (line[j+89] != ' ') ? line[j+89] : '\0';
      str[4] = '\0';
      syear = atoi( str );
      for ( j = 0 ; j < 2; j++ )
         str[j] = (line[j+93] != ' ') ? line[j+93] : '\0';
      str[2] = '\0';
      smonth = atoi( str );
      for ( j = 0 ; j < 2; j++ )
         str[j] = (line[j+95] != ' ') ? line[j+95] : '\0';
      str[2] = '\0';
      sday = atoi( str );
      for ( j = 0 ; j < 4; j++ )
         str[j] = (line[j+98] != ' ') ? line[j+98] : '\0';
      str[4] = '\0';
      eyear = atoi( str );
      for ( j = 0 ; j < 2; j++ )
         str[j] = (line[j+102] != ' ') ? line[j+102] : '\0';
      str[2] = '\0';
      emonth = atoi( str );
      for ( j = 0 ; j < 2; j++ )
         str[j] = (line[j+104] != ' ') ? line[j+104] : '\0';
      str[2] = '\0';
      eday = atoi( str );

/* Convert instrument start and end dates to seconds since 1/1/1970
   ****************************************************************/
      stm.tm_year = syear - 1900;
      stm.tm_mon  = smonth -1;
      stm.tm_mday = sday;
      stm.tm_hour = 0;
      stm.tm_min  = 0;
      stm.tm_sec  = 0;
      stime = timegm_ew( &stm );
      if ( eyear == 3000 )
         etime = MAXINT;
      else
      {
         stm.tm_year = eyear - 1900;
         stm.tm_mon  = emonth -1;
         stm.tm_mday = eday;
         stm.tm_hour = 23;
         stm.tm_min  = 59;
         stm.tm_sec  = 59;
         etime = timegm_ew( &stm );
      }

/* Set the SCN values in the output pin list.
   Then, mark the pin as found.
   If found=1, the SCN was found in calsta2000.loc
   If found=2, the instrument was in operation,
   according to the calsta2000.file.
   ***********************************************/
      for ( j = 0; j <= pl->maxpinno; j++ )
      {
         int isscn;
         int israt;
         time_t starttime = (time_t)pl->ipin[j].starttime;

         if ( pl->ipin[j].found == 0 ) continue;
         if ( pl->opin[j].found == 2 ) continue;

/* See if the SCN or rational name from calsta2000.loc
   is in the pin list.  Also make sure the instrument
   was in operation, according to the calsta2000.loc file.
   ******************************************************/
         isscn = strcmp( sta,  pl->ipin[j].sta  ) == 0 &&
                 strcmp( chan, pl->ipin[j].chan ) == 0 &&
                 strcmp( net,  pl->ipin[j].net  ) == 0;
         israt = strcmp( ratn, pl->ipin[j].sta  ) == 0 &&
                 strcmp( ratc, pl->ipin[j].chan ) == 0;

         if ( isscn || israt )
         {
            pl->opin[j].found = 1;
            if ( starttime >= stime &&
                 starttime <= etime )
            {
               strcpy( pl->opin[j].sta,  sta );
               strcpy( pl->opin[j].chan, chan );
               strcpy( pl->opin[j].net,  net );
               pl->opin[j].found = 2;
            }
         }
      }
   }
   fclose( tbfp );
   return 0;
}


   /*********************************************
    *              SaveTimeCode()               *
    *                                           *
    * Save all time code channels, regardless   *
    * of whether they are in calsta2000.loc     *
    *********************************************/

void SaveTimeCode( PINLIST *pl )
{
   int i;

   for ( i = 0; i < pl->maxpinno; i++ )
   {
      char ista[6];
      char ichan[4];
      char inet[3];

      if ( pl->ipin[i].found == 0 ) continue;

      strcpy( ista,  pl->ipin[i].sta );
      strcpy( ichan, pl->ipin[i].chan );
      strcpy( inet,  pl->ipin[i].net );

      if ( strcmp(ista,"WWVB") == 0 ||
           strcmp(ista,"IRG" ) == 0 ||
           strcmp(ista,"IRG1") == 0 ||
           strcmp(ista,"IRG2") == 0 ||
           strcmp(ista,"IRG3") == 0 ||
           strcmp(ista,"IRGE") == 0 ||
           strcmp(ista,"IRIG") == 0 )
      {
         strcpy( pl->opin[i].sta,  ista );
         strcpy( pl->opin[i].chan, "T" );
         strcpy( pl->opin[i].net,  "NC" );
         pl->opin[i].found = 2;
      }
   }
   return;
}
