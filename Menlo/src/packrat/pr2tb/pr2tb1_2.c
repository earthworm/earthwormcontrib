
  /*************************************************************************
   *                                  pr2tb                                *
   *                                                                       *
   *  Program to convert packrat files to Earthworm TRACEBUF format.       *
   *  Warning: This is a one-way conversion.  There's no way to go from    *
   *  tracebuf format back to packrat format.                              *
   *                                                                       *
   *  Usage:                                                               *
   *  ls *.pr | pr2tb -d -r    or    ls <filename> | pr2tb -d -r           *
   *  -d : Each packrat file will be deleted after conversion (optional).  *
   *  -r : Rename SCNs on each tracebuf file to modern format (optional).  *
   *************************************************************************/

/*
Version 1.1  9/3/2008
New mapping of time-code channels:
IRG T pin 1 --> IRG1 NC T
IRG T pin 2 --> IRG2 NC T
IRG T pin 3 --> IRG3 NC T

Version 1.2  9/12/2008
Checks if time is decreasing in the packrat file.
This could happen if lrtc in the data block header gets messed up.
If so, the packrat file is not converted.  It gets renamed to xxx.bad.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <math.h>
#include <unistd.h>
#include "web.h"
#include "chron3.h"
#include "trace_buf.h"

#define FNLEN 80       // Max length of file names

int  ConvertFile( char prfname[], char tbfname[] );
int  GetIrigeTime( char prfname[], double *irigeTime, int *lrtcTime );
void PrintGmtime( double, int );
void RenameTracebufFile( char prfname[], char tempname[], char tbfname[],
                         double fst );
void ReadWeb( FILE *prfp, WEB *w );
void InitStrings( WEB w, PIN *p );
void FreeStrings( WEB w, PIN *p );
void ReadPin( FILE *prfp, PIN *p );
int  ReadWaveBlock( FILE *prfp, short *wdb, int nsamp );


/* The main program starts here
 ******************************/

int main( int argc, char *argv[] )
{
   const  char pr2tbVersion[] = "1.2";
   int    i;
   char   prfname[FNLEN];          // Packrat file name
   char   tbfname[FNLEN];          // Earthworm tracebuf file name
   int    dpr = 0;                 // If 1, delete packrat files
   int    rscn = 0;                // If 1, rename tracebuf SCNs

/* Print the pr2tb version number
   ******************************/
   printf( "pr2tb version: %s\n", pr2tbVersion );

/* Get command line arguments
   **************************/
   for ( i = 0; i < argc; i++ )
   {
      if ( strcmp(argv[i], "-d") == 0 ) dpr  = 1;
      if ( strcmp(argv[i], "-r") == 0 ) rscn = 1;
   }

/* Get names of packrat files from standard input.
   Skip all other files.
   **********************************************/
   while ( gets( prfname ) != NULL )
   {
      char *suffix;          // Pointer to file name suffix
      int  stl;              // String length
      int  cfrc;             // ConvertFile() return code

      stl = strlen( prfname );
      if ( stl < 3 ) continue;
      suffix = &prfname[stl-3];
      if ( strcmp(suffix, ".pr") != 0 ) continue;

/* Convert one packrat file to tracebuf format.
   ConvertFile() returns the name of the tracebuf file.
   Note that if IRIGE time signal can't be decoded,
   ConvertFile() returns an error, and the packrat file
   is not converted.
   ****************************************************/
      printf( "Converting packrat file %s to tracebuf format\n", prfname );
      cfrc = ConvertFile( prfname, tbfname );

/* ConvertFile() succeeded.  Delete the packrat file.
   *************************************************/
      if ( cfrc == 0 )
      {
         printf( "Finished converting file %s to %s\n", prfname, tbfname );

         if ( dpr == 1 )                   // Delete packrat file
         {
            if ( remove( prfname ) == -1 )
            {
               printf( "Error deleting packrat file: %s\n", prfname );
               printf( "Exiting.\n" );
               return -1;
            }
            printf( "Finished deleting packrat file %s\n", prfname );
         }

/* Run tbfilt on the newly created tracebuf file.
   tbfilt converts the SCN of each tracebuf message
   to modern format.
   ************************************************/
         if ( rscn == 1 )
         {
            FILE *fp_pipe;
            int  rc_pclose;

            fp_pipe = popen( "tbfilt", "w" );
            if ( fp_pipe == NULL )
            {
               printf( "Error opening pipe to tbfilt\n" );
               return -1;
            }
            fprintf( fp_pipe, "%s\n", tbfname );
            rc_pclose = pclose( fp_pipe );
            if ( rc_pclose != 0 )
            {
               printf( "tbfilt error detected. rc_pclose: %d\n", rc_pclose );
               return -1;
            }
         }
      }

/* ConvertFile() failed.  Rename the packrat file.
   **********************************************/
      else
      {
         char newname[FNLEN];   // New packrat file name

         printf( "Error converting packrat file %s to tracebuf format\n", prfname );
         strcpy( newname, prfname );
         strcat( newname, ".bad" );

         if ( rename( prfname, newname ) == -1 )
         {
            printf( "ERROR renaming file %s to %s\n", prfname, newname );
            printf( "Exiting.\n" );
            return -1;
         }
         printf( "Packrat file %s renamed to %s\n", prfname, newname );
      }
   }
   return 0;
}


int ConvertFile( char prfname[],    // Packrat file name
                 char tbfname[] )   // Name of new tracebuf file
{
   const int wdbh_size = 1024;   // Waveform data block header size
   const int web_size  = 65534;  // Web header block size
   const int nsamp     = 100;    // Number of samples per tracebuf message

   int    seekbyte;
   int    traceBufSize;      // Size of one tracebuf message
   int    pin;               // Pin number
   double fst;               // Packrat file start time
   double irigeTime;         // Time from IRIGE decode
   int    lrtcTime;          // lrtc value of irigeTime
   FILE   *prfp;             // File pointer to packrat file
   FILE   *tbfp;             // File pointer to tracebuf file
   WEB    w;                 // Packrat web structure (scalar parameters)
   PIN    p;                 // Packrat pin array structure
   short  *wdb;              // Packrat waveform block
   BLOCK  *wdbh;             // Packrat waveform block header structure
   short  *wd;               // Array containing mux'd waveform data
   short  *traceBuf;         // Where tracebuf messages are assembled
   TRACE_HEADER *traceHead;  // Where the tracebuf header is stored (SCN format)
   int    first = 1;         // 1 for the first packrat block
   int    lrtcprev;          // Previous value of sample count
   char   *ptr;
   char   tempname[FNLEN];   // Name of temporary file

/* Get packrat file start time by decoding digitized IRIGE signal.
   For this to work, kyear and kirige must be set correctly in the
   packrat web header block, and the IRIGE signal must be valid.
   Packrat tapes nominally record IRIGE on three pins (1, 2, and 3).
   The preferred IRIGE signal is on pin number kirige.
   Note that if IRIGE is not available, the data for a whole packrat
   file are not converted to tracebuf format.
   ****************************************************************/
   if ( GetIrigeTime(prfname, &irigeTime, &lrtcTime) != 0 )
   {
      printf( "GetIrigeTime() error\n" );
      return -1;
   }
// printf( "Time from decoded IRIGE: " );
// PrintGmtime( irigeTime - 11676096000., 2 );   // Two decimal places
// printf( "  lrtc: %d\n", lrtcTime );

/* Open the packrat disk file
   **************************/
   prfp = fopen( prfname, "r" );
   if ( prfp == NULL )
   {
      printf( "Error opening packrat file: %s\n", prfname );
      printf( "Exiting.\n" );
      exit( 0 );
   }

/* Create a temporary file to contain tracebuf data
   ************************************************/
   ptr = tempnam( ".", "tmp" );    // Create name of temporary file
   if ( ptr == NULL )
   {
      printf( "tempnam() error. Exiting.\n" );
      exit( 0 );
   }
   strncpy( tempname, ptr, FNLEN );
   free( ptr );

   tbfp = fopen( tempname, "w" );
   if ( tbfp == NULL )
   {
      printf( "Error opening file: %s Exiting.\n", tempname );
      exit( 0 );
   }

/* Read packrat web structure, stored in
   first web_size bytes of disk file.
   *************************************/
   ReadWeb( prfp, &w );

   if ( w.krate != 100 )
   {
      printf( "ERROR.  Bad krate (nominal sample rate): %d\n", w.krate );
      printf( "Exiting.\n" );
      exit( 0 );
   }

/* Allocate buffer to contain Earthworm tracebuf messages
   ******************************************************/
   traceBufSize = sizeof(TRACE_HEADER) + (nsamp * sizeof(short));
   traceBuf     = (short *) malloc( w.npin * traceBufSize );
   if ( traceBuf == NULL )
   {
      printf( "ERROR: Cannot allocate earthworm trace buffer.\n" );
      printf( "Exiting.\n" );
      exit( 0 );
   }

/* Allocate arrays in the pin structure.
   Arrays sizes are contained in web structure.
   *******************************************/
   InitStrings( w, &p );

/* Allocate packrat waveform data block.  Set addresses
   of waveform data block header and waveform data array.
   *****************************************************/
   wdb = (short *) calloc( w.nwdio, sizeof(short) );
   if ( wdb == NULL )
   {
      printf( "ERROR: Cannot allocate packrat waveform data block.\n" );
      printf( "Exiting.\n" );
      exit( 0 );
   }
   wdbh = (BLOCK *)wdb;
   wd   = &wdb[wdbh_size];

/* Get pin structure from packrat tape
   ***********************************/
   ReadPin( prfp, &p );

/* Fill in some of the tracebuf headers
   ************************************/
   for ( pin = 0; pin < w.npin; pin++ )
   {
      traceHead = (TRACE_HEADER *) &traceBuf[pin*traceBufSize/2];
      traceHead->pinno      = pin;
      traceHead->samprate   = (double)w.krate;
      traceHead->nsamp      = w.kspb;
      traceHead->quality[0] = '\0';
      traceHead->quality[1] = '\0';

/* Fill in the trace header site name.
   Rename some of the Irige channels.
   **********************************/
      if (      (strcmp(p.csite[pin],"IRG") == 0) && (pin == 1) )
         strcpy( traceHead->sta, "IRG1" );
      else if ( (strcmp(p.csite[pin],"IRG") == 0) && (pin == 2) )
         strcpy( traceHead->sta, "IRG2" );
      else if ( (strcmp(p.csite[pin],"IRG") == 0) && (pin == 3) )
         strcpy( traceHead->sta, "IRG3" );
      else
         strcpy( traceHead->sta, p.csite[pin] );

/* Fill in the component/channel code and data type
   ************************************************/
      strcpy( traceHead->chan, p.cinst[pin] );
      strcpy( traceHead->datatype, "i2" );

/* Fill in the network code.
   Set missing network codes for time channels to NC.
   Set all other missing network codes to --.
   *************************************************/
      strcpy( traceHead->net,  p.coper[pin] );
      if ( strlen(p.coper[pin]) == 0 )
      {
         if ( strcmp(p.csite[pin],"WWVB") == 0 ||
              strcmp(p.csite[pin],"IRG" ) == 0 ||
              strcmp(p.csite[pin],"IRG1") == 0 ||
              strcmp(p.csite[pin],"IRG2") == 0 ||
              strcmp(p.csite[pin],"IRG3") == 0 ||
              strcmp(p.csite[pin],"IRGE") == 0 ||
              strcmp(p.csite[pin],"IRIG") == 0 )
            strcpy( traceHead->net, "NC" );
         else
            strcpy( traceHead->net, "--" );
      }
   }

/* Get one waveform block at a time from packrat tape,
   equivalent to a physical block (QIO) on the tape.
   **************************************************/
   seekbyte = web_size;   // Skip the packrat file header (web) block
   while ( 1 )
   {
      double bst_irige;   // Packrat block start time, from decoded irige
      int i;              // Scan number within packrat block
      int nchar;          // Number of characters in tracebuf message
      double starttime;   // Tracebuf message start time
      double endtime;     // Tracebuf message end time

      fseek( prfp, seekbyte, SEEK_SET );
      if ( ReadWaveBlock( prfp, wdb, w.nwdio ) == -1 ) break;
      bst_irige = irigeTime + (wdbh->lrtc - lrtcTime) / (double)w.krate;
      starttime = bst_irige - 11676096000.;
      endtime   = starttime + (double)(w.kspb - 1) / (double)w.krate;

/* Make sure sample number is increasing in the packrat file.
   If it isn't, give up converting this file.
   The packrat file will be renamed xxxx.bad
   *********************************************************/
      if ( first )
      {
         fst   = bst_irige;     // Set file start time
         first = 0;
      }
      else
      {
         if ( wdbh->lrtc <= lrtcprev )
         {
            printf( "ERROR.  lrtc decreasing within packrat file.\n" );
            fclose( prfp );
            fclose( tbfp );
            remove( tempname );
            free( traceBuf );
            FreeStrings( w, &p );
            free( wdb );
            return -1;
         }
      }
      lrtcprev = wdbh->lrtc;   // Save sample count

/* Loop through all pins
   *********************/
      for ( pin = 0; pin < w.npin; pin++ )
      {
         int nwritten;
         int irc = w.kirige;      // Irig channel from web block

/* Get rid of dummy channels and channels with short station names.
   csite = Station name code; cinst = Component code
   ***************************************************************/
         if ( strcmp(p.csite[pin],"XXX") == 0 ) continue;
         if ( strcmp(p.csite[pin],"P"  ) == 0 ) continue;
         if ( strcmp(p.csite[pin],"PG" ) == 0 ) continue;
         if ( strcmp(p.cinst[pin],"XXX") == 0 ) continue;
         if ( strlen(p.csite[pin])        < 3 ) continue;

/* Set start and end times for this message
   ****************************************/
         traceHead = (TRACE_HEADER *) &traceBuf[pin*traceBufSize/2];
         traceHead->starttime = starttime;
         traceHead->endtime   = endtime;

/* Copy packrat data samples to tracebuf message
   *********************************************/
         for ( i = 0; i < w.kspb; i++ )
         {
            int j = pin*traceBufSize/2 + i + sizeof(TRACE_HEADER) / 2;
            int k = (w.npin * i) + pin;
            traceBuf[j] = wd[k];
         }

/* Write to tracebuf file
   **********************/
         nchar = sizeof(TRACE_HEADER) + (traceHead->nsamp * sizeof(short));
         nwritten = fwrite( &traceBuf[pin*traceBufSize/2], 1, nchar, tbfp );
         if ( nwritten < 1 )
         {
            printf( "ERROR writing temporary tracebuf file: %s\n", tempname );
            printf( "Exiting.\n" );
            exit( 0 );
         }
      }
      seekbyte += 2 * w.nwdio;
   }

/* Close files and deallocate memory
   *********************************/
   fclose( prfp );
   fclose( tbfp );
   free( traceBuf );
   FreeStrings( w, &p );
   free( wdb );

/* Rename the tracebuf file using the packrat
   file name and the file start time
   ******************************************/
   RenameTracebufFile( prfname, tempname, tbfname, fst );
   return 0;
}


void RenameTracebufFile( char prfname[], char tempname[], char tbfname[],
                         double fst )
{
   double fstround;        // Rounded file start time
   char   c18[20];
   int    stl;             // String length
   char   datestr[9];      // Date, as a string
   char   timestr[7];      // Time, as a string

/* Build name of tracebuf file
   ***************************/
   strncpy( tbfname, prfname, FNLEN );
   stl = strlen( tbfname ) - 3;        // Discard the last 3 characters
   if ( stl < 0 ) stl = 0;
   tbfname[stl] = NULL;
   if ( stl > 0 ) strcat( tbfname, "_" );
   fstround = floor( fst + 0.5 );
   date18( fstround, c18 );
   memcpy( datestr, &c18[0], 8 );
   datestr[8] = NULL;
   memcpy( timestr, &c18[8], 6 );
   timestr[6] = NULL;
   strcat( tbfname, datestr );
   strcat( tbfname, "_" );
   strcat( tbfname, timestr );
   strcat( tbfname, ".tb" );

/* Rename the temporary file to the new tracebuf file name
   *******************************************************/
   if ( rename( tempname, tbfname ) == -1 )
   {
      printf( "ERROR renaming file %s to %s\n", tempname, tbfname );
      printf( "Exiting.\n" );
      exit( 0 );
   }
   return;
}
