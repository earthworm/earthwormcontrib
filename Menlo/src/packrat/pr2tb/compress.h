
/*
       compress.h - Header file for compress.c
*/

#ifndef COMPRESS_H
#define COMPRESS_H

/* Function prototypes
   *******************/
int CompressFile( char *, int * );

#endif
