
    /*******************************************************************
     *                             irigetime                           *
     *                                                                 *
     *  Get irige time and the corresponding value of lrtc from a      *
     *  packrat file.  kirige and kyear must be set correctly in the   *
     *  web header block, and the irige signal must be valid.          *
     *  If time is decoded ok, GetIrigeTime() returns 0.               *
     *  Otherwise, GetIrigeTime() returns -1.                          *
     *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "web.h"
#include "chron3.h"
#include "irige.h"

void ReadWeb( FILE *fp, WEB *w );
void PrintWeb( WEB w );
void InitAllStrings( WEB w, PIN *p, short **wdb );
void FreeAllStrings( WEB w, PIN *p, short **wdb );
int  ReadWaveBlock( FILE *fp, short *wdb, int nsamp );
void PrintWave( WEB w, short *wd );
int  Irige( struct TIME_BUFF *, long, int, int, short *, int, int );
void PrintGmtime( double, int );


int GetIrigeTime( char prfname[], double *irigeTime, int *lrtcTime )
{
   const int wdbh_size = 1024;   // Waveform data block header size
   const int web_size  = 65534;  // Web header block size
   int   seekbyte;
   FILE  *fp;
   WEB   w;               // Web structure (scalar parameters)
   PIN   p;               // Pin arrays structure
   NODE  n;               // Trigger node membership structure
   short *wdb;            // Waveform data block, allocated in InitAllStrings[]
   BLOCK *wdbh;           // Waveform data block header structure
   short *wd;             // Array containing mux'd waveform data
   int   IrigeStatus;     // Initially, there is no status
   int   lockAttained = 0;
   int   rc = 0;
   struct TIME_BUFF Tbuf; // Irige()'s read-write structure

/* Open the disk file
   ******************/
   fp = fopen( prfname, "r" );
   if ( fp == NULL )
   {
      printf( "Error opening packrat file %s\n", prfname );
      return -1;
   }

/* Read and print web structure, stored in
   the first web_size bytes of the disk file.
   *****************************************/
   ReadWeb( fp, &w );
// PrintWeb( w );

/* Do we know this format version?
   *******************************/
   if ( fabsf(w.web_id - 2.60) > 0.0001 )
   {
     printf( "ERROR. This packrat version not recognized.\n" );
     printf( "web_id: %f\n", w.web_id );
     return -1;
   }

/* Allocate arrays in the pin structure.
   Arrays sizes are contained in web structure.
   Set addresses of the waveform data block header
   and the waveform data array.
   ***********************************************/
   InitAllStrings( w, &p, &wdb );
   wdbh = (BLOCK *)wdb;
   wd   = &wdb[wdbh_size];

/* Get one waveform block at a time, equivalent
   to a physical block (QIO) on the tape.
   ********************************************/
// printf( "\nBlock Headers:\n" );
// printf( "   lbuf    lrtc        IRIGE status\n" );
// printf( "   ----    ----        ------------\n" );

   seekbyte = web_size;     // Skip the file header (web) block

   while ( 1 )
   {
      fseek( fp, seekbyte, SEEK_SET );
      if ( ReadWaveBlock( fp, wdb, w.nwdio ) == -1 ) break;

/* Decode IRIGE channel.
   Compare decoded time to time in header block.
   Possible values of IrigeStatus:
   TIME_NOSYNC, TIME_FLAT, TIME_NOISE, TIME_WAIT, TIME_OK
   ******************************************************/
      IrigeStatus = Irige( &Tbuf, wdbh->lrtc, w.npin, w.kspb, wd,
                           w.kirige, w.kyear );
      if ( IrigeStatus == TIME_OK )
      {
         lockAttained = 1;
//       PrintGmtime( Tbuf.t - 11676096000., 2 );
//       printf( "  lrtc: %6d\n", wdbh->lrtc );
         break;
      }
//    PrintWave( w, wd );
      seekbyte += 2 * w.nwdio;
   }

   if ( lockAttained )
   {
      *irigeTime = Tbuf.t;
      *lrtcTime  = wdbh->lrtc;
      rc = 0;
   }
   else
   {
      printf( "ERROR.  Can't lock on IRIGE time.\n" );
      rc = -1;
   }

/* Close files and deallocate memory
   *********************************/
   fclose( fp );
   FreeAllStrings( w, &p, &wdb );
   free( wdb );
   return rc;
}


void PrintString( char str[] )
{
   if ( strlen(str) > 0 )
      printf( "%s\n", str );
   else
      printf( "<null>\n" );
   return;
}


void PrintWeb( WEB w )
{
   char c18[20];
   int i;

   printf( "\nGlobal Parameters:\n" );
   printf( "web_id     %f\n",   w.web_id );
   printf( "nwpar        %6ld\n", w.nwpar );
   printf( "mpin         %6ld\n", w.mpin );
   printf( "npin         %6ld\n", w.npin );
   printf( "msub         %6ld\n", w.msub );
   printf( "nsub         %6ld\n", w.nsub );
   printf( "mweb         %6ld\n", w.mweb );
   printf( "nweb         %6ld\n", w.nweb );
   printf( "kstop        %6ld\n", w.kstop );
   printf( "krate        %6ld\n", w.krate );
   printf( "kfact        %6ld\n", w.kfact );
   printf( "lpacer       %6ld\n", w.lpacer );
   printf( "kchlo        %6ld\n", w.kchlo );
   printf( "kchhi        %6ld\n", w.kchhi );
   printf( "kspb         %6ld\n", w.kspb );
   printf( "kframe       %6ld\n", w.kframe );
   printf( "nbuf         %6ld\n", w.nbuf );
   printf( "ireftek      %6ld\n", w.ireftek );
   printf( "nwdata       %6hd\n", w.nwdata );
   printf( "nwdma        %6hd\n", w.nwdma );
   printf( "nwdio        %6hd\n", w.nwdio );
   printf( "nwnul        %6hd\n", w.nwnul );
   printf( "lbuf         %6ld\n", w.lbuf );
   printf( "l_err        %6ld\n", w.l_err );
   printf( "lb_next      %6ld\n", w.lb_next );
   printf( "lb_last      %6ld\n", w.lb_last );
   printf( "ksca_stop    %6ld\n", w.ksca_stop );
   printf( "ksli_stop    %6ld\n", w.ksli_stop );
   printf( "ldmax        %6ld\n", w.ldmax );
   printf( "ktrgin       %6ld\n", w.ktrgin );
   printf( "lidout       %6ld\n", w.lidout );
   printf( "l_cid        %6ld\n", w.l_cid );
   printf( "kscarab      %6ld\n", w.kscarab );
   printf( "ksling       %6ld\n", w.ksling );
   printf( "lsca_end   %6ld\n", w.lsca_end );
   printf( "lsli_end   %6ld\n", w.lsli_end );
   printf( "kspi_on      %6ld\n", w.kspi_on );
   printf( "kspi_stop    %6ld\n", w.kspi_stop );
   printf( "ndec         %6ld\n", w.ndec );
   printf( "tstw       %6f\n",  w.tstw );
   printf( "tltw       %6f\n",  w.tltw );
   printf( "quiet      %6f\n",  w.quiet );
   printf( "sense      %6f\n",  w.sense );
   printf( "tenon      %6f\n",  w.tenon );
   printf( "tenoff     %6f\n",  w.tenoff );
   printf( "tprevt     %6f\n",  w.tprevt );
   printf( "tpost      %6f\n",  w.tpost );
   printf( "irig_stop    %6ld\n", w.irig_stop );
   printf( "kirige       %6ld\n", w.kirige );
   printf( "kyear        %6ld\n", w.kyear );
   printf( "ltirig       %6ld\n", w.ltirig );
   printf( "stirig     %f\n",   w.stirig );
   printf( "dtirig     %lf",    w.dtirig );
   date18( w.dtirig, c18 );
   printf( "  %s\n", c18 );
   printf( "subalarm     %6ld\n", w.subalarm );
   printf( "lerrtc       %6ld\n", w.lerrtc );
   printf( "n_err        %6ld\n", w.n_err );

   printf( "kbad[4]    " );
   for ( i = 0; i < 4; i++ ) printf( "  %ld", w.kbad[i] );
   putchar( '\n' );

   printf( "ltsys        %6ld\n", w.ltsys );
   printf( "ltmin        %6ld\n", w.ltmin );
   printf( "dtsys      %lf",  w.dtsys );
   date18( w.dtsys, c18 );
   printf( "  %s\n", c18 );
   printf( "ltser        %6ld\n", w.ltser );
   printf( "dtser      %lf",  w.dtser );
   date18( w.dtser, c18 );
   printf( "  %s\n", c18 );
   printf( "dtstop     %lf\n",  w.dtstop );
   printf( "ltser        %6ld\n", w.ltser );
   printf( "ltstop       %6ld\n", w.ltstop );
   printf( "c_net      " ); PrintString( w.c_net );
   printf( "c_dev      " ); PrintString( w.c_dev );
   printf( "c_t_net    " ); PrintString( w.c_t_net );
   printf( "c_t_dev    " ); PrintString( w.c_t_dev );
   printf( "c_t_dub    " ); PrintString( w.c_t_dub );
   printf( "c_t_tape   " ); PrintString( w.c_t_tape );
   printf( "d_t_t0     %lf",  w.d_t_t0 );
   date18( w.d_t_t0, c18 );
   printf( "  %s\n", c18 );
   printf( "r_t_dur    %f\n",   w.r_t_dur );
   printf( "r_t_rate   %f\n",   w.r_t_rate );
   printf( "c_t_track  " ); PrintString( w.c_t_track );
   printf( "c_t_stalst " ); PrintString( w.c_t_stalst );
   return;
}


void PrintWave( WEB w, short *wd )
{
   int i;

   for ( i = 0; i < w.kspb; i++ )
   {
      int j;
      printf( "%2d", i );
      for ( j = 0; j < 10; j++ )
      {
         int k = (w.npin * i) + j;
         printf( "%7hd", wd[k] );
      }
      putchar( '\n' );
   }
   return;
}


void InitAllStrings( WEB w, PIN *p, short **wdb )
{
   int i;

   for ( i = 0; i < w.mpin; i++ )
   {
      p->csite[i] = malloc( 6 );
      p->cinst[i] = malloc( 4 );
      p->coper[i] = malloc( 6 );
   }

   *wdb = calloc( w.nwdio, sizeof(short) );
   if ( *wdb == NULL )
   {
      printf( "calloc error in InitAllStrings[]. Exiting.\n" );
      exit( 0 );
   }
   return;
}


void FreeAllStrings( WEB w, PIN *p, short **wdb )
{
   int i;

   for ( i = 0; i < w.mpin; i++ )
   {
      free( p->csite[i] );
      free( p->cinst[i] );
      free( p->coper[i] );
   }
   free( *wdb );
   return;
}
