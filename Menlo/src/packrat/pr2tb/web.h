
    /*******************************************************************
     *                              web.h                              *
     *                                                                 *
     *  The web and pin structures.                                    *
     *******************************************************************/

#define MAXPAR  512       /* Maximum i*2 words allowed in parameter area */
#define MAXPIN  1024      /* Pin arrays */
#define MAXSUB  200       /* Subnet arrays */
#define MAXWEB  2000      /* Pin-subnet arrays (nodes) */
#define MAXTRG  20        /* Event arrays */
#define PADDING 256

#define MAXREG  (MAXPAR + 23*MAXPIN + 17*MAXSUB + 2*MAXWEB + 7*MAXTRG + PADDING)



typedef struct web {
   float  web_id;     // Version of web for this data set
   long   nwpar;      // Words in parameter area
   long   mpin;       // Maximum number of pins allowed
   long   npin;       // Number of active pins
   long   msub;       // Maximum number of subnets
   long   nsub;       // Current subnets
   long   mweb;       // Maximum web node membership
   long   nweb;       // Current web node membership
                      // 32 bytes, 16 words

   long   kstop;      // Core/system stop flag. stop=1, idle=10
   long   krate;      // Nominal digitization rate
   long   kfact;      // Acceleration factor
   long   lpacer;     // Tustin hardware clock frequency
   long   kchlo;      // Starting channel
   long   kchhi;      // Ending channel
   long   kspb;       // Scans per buffer
   long   kframe;     // Number of words in a major frame
   long   nbuf;       // Number of data buffers in memory
   long   ireftek;    // Number of words in Reftek header & trailer
   short  nwdata;     // Words of data
   short  nwdma;      // Words per dma io
   short  nwdio;      // Words per disk qio
   short  nwnul;      // Padding to make disk io an
                      //   integral # of disk blocks
   long   lbuf;       // Core: Most recently completed buffer
                      //   (2 qios are ahead of it)
                      //   (1 buf = 1 qio from DR11W)
   long   l_err;      // Total number of i/o errors detected by core
   long   lb_next;    // Buffer that scarab is working with
   long   lb_last;    // Last buffer sling has written
   long   ksca_stop;  // Scarab control flag. stop=1, idle=10
   long   ksli_stop;  // Sling  control flag. stop=1, idle=10
   long   ldmax;      // # of data buffers, cutoff for reform
   long   ktrgin;     // Pointer to last event (ltron, ltroff, lcid)
   long   lidout;     // Pointer to last cuspid made by bicuspid
   long   l_cid;      // CUSP id current sling/scarab event
   long   kscarab;    // Last event out to disk (ltron, ltroff, lcid)
   long   ksling;     // Last event out to tape (ltron, ltroff, lcid)
   long   lsca_end;   // RTC set by scarab at the true end of an event
   long   lsli_end;   // RTC set by sling  at the true end of an event
                      // 104 bytes   52 words

   long   kspi_on;    // Spider event in progress
   long   kspi_stop;  // Spider stop flag, 1 => stop, -1 -> hot start
   long   ndec;       // Trigger decimation
   float  tstw;       // Short term window (seconds)
   float  tltw;       // Long term window (seconds)
   float  quiet;      // Quiet station constant
   float  sense;      // Network sensitivity
   float  tenon;      // Onset tenacity (spike supression)
   float  tenoff;     // Termination tenacity
   float  tprevt;     // Pre-event memory
   float  tpost;      // Post-event memory
                      // 44 bytes  22 words

   long   irig_stop;  // Irige control flag. stop=1, idle=10
   long   kirige;     // Irig channel
   long   kyear;      // Year of interest -- for irig decoder
   long   ltirig;     // RTC at last decode
   float  stirig;     // Digitization interval
   double dtirig;     // Time at last decode
                      // 28 bytes   14 words

   long   subalarm;   // Number of concurrently triggered
                      // Subnets for alarm, 04Aug92 -RSD
   long   lerrtc;     // RTC of error buffer
   long   n_err;      // Number of logged i/o errors
   long   kbad[4];    // IOSB for last DR11W i/o error
                      // 28 bytes   14 words

   long   ltsys;      // LRTC at core start (system time)
   long   ltmin;      // Julian minute at series start
   double dtsys;      // Gregorian sec at core start
   long   ltser;      // RTC for dtser
   double dtser;      // Gregorian systime that core reset lbuf
   double dtstop;     // CPU time at core stop, julian seconds
   long   ltstop;     // RTC for dtstop
   char   c_net[4];   // Network acquiring the original data
   char   c_dev[4];   // Device used to acquire the original data
                      // 48 bytes   24 words

   char   c_t_net[4];      // Network digitizing the analog tape
   char   c_t_dev[4];      // device used by c_t_net
   char   c_t_dub[12];     // Dub tape label	-PJJ 091186
   char   c_t_tape[10];    // Tape id ie a,b,etc or 5 day id
   double d_t_t0;          // Requested start time
   float  r_t_dur;         // Requested duration
   float  r_t_rate;        // Requested dig rate
   char   c_t_track[32];   // Tracks digitized 10 -> 32 -PJJ 091186
   char   c_t_stalst[30];  // Name of station master list
                      // 108 bytes   54 words

} WEB;                // 392 bytes   Total=196 words


typedef struct pin {           // Pin arrays
   char   *csite[MAXPIN];      // Site name
   char   *cinst[MAXPIN];      // Instrument type 28Jun88 -RSD
   char   *coper[MAXPIN];      // Owner if not owned by recording net
// char   *sta[MAXPIN];        // SEED station name
// char   *comp[MAXPIN];       // SEED component code
// char   *net[MAXPIN];        // SEED network code
// char   *loc[MAXPIN];        // SEED location code
   short  kpin[MAXPIN];        // ADC connection
   short  kslm[MAXPIN];        // Scan list memory array
   short  kvote[MAXPIN];       // Pin voting table, 1 -> vote,
                               //                   0 -> don't
   short  ktrg[MAXPIN];        // Current trigger condition
   short  kdly[MAXPIN];        // State transition delay
   float  bias[MAXPIN];        // Bias
   float  snr_on[MAXPIN];      // Signal/noise ratio for a
                               //   trigger to start
   float  snr_off[MAXPIN];     // SNR for a pin level
                               //   trigger to end
   float  rb[MAXPIN];          // STW - Short term window
   float  rbb[MAXPIN];         // LTW - Long term average
} PIN;


typedef struct sub {           // Subnet arrays
   char   *csub[MAXSUB];       // Subnet titles
   short  kthr[MAXSUB];        // Subnet trigger levels
} SUB;


typedef struct node {          // Trigger node membership (one entry
                               //     for each subnet-station pair)
   short  ksub[MAXWEB];        // Subnet index
   short  kchn[MAXWEB];        // ADC channel
} NODE;


typedef struct trig {          // Event trigger fifo
   short  ktrtyp[MAXTRG];      // Trigger type
   long   ltron[MAXTRG];       // Trigger on  rtc
   long   ltroff[MAXTRG];      // Trigger off rtc
   long   lcid[MAXTRG];        // Cuspid for triggers
} TRIG;

typedef struct block {         // Data block header info
   long   lbuf;                // Buffer number of a packet of samples
   long   lrtc;                // Sample point number
} BLOCK;
