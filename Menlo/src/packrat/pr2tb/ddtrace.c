
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <thread.h>
#include <earthworm.h>
#include "write_cta.h"
#include "tapeio.h"

/* Declared in write_cta.c
   ***********************/
extern char *spooldir;            /* Name of spool directory */
extern char *tapename;            /* Tape device name */
extern char *changer;             /* Name of tape changer or "none" */
extern int  term_flag;            /* 1 if transport ring term flag set */
extern int  pauseWrite;           /* 1 if tape writing is paused */
extern int  TapeWriteIntSec;      /* Tape write interval in seconds */
extern int  compress;             /* If 1, compress */
extern char *class;               /* RT or TS */ 
extern int  priority;             /* 0-59 for RT */ 
extern unsigned char typeError;   /* Message type for error */


      /****************************************************************
       *                        GetFileName()                         *
       *                                                              *
       *  Get the name of a trace file with the earliest time.        *
       *  Trace files begin with "wu".                                *
       *                                                              *
       *  Returns fname, which contains the file name.                *
       *                                                              *
       *  Returns  0 if success                                       *
       *          -1 if no trace files were found                     *
       ****************************************************************/

int GetFileName( char fname[] )
{
   DIR    *dirp;
   struct dirent *dent;
   char   earliest[80];
   int    gotOne = 0;

   if ( (dirp = opendir(".")) == NULL )
   {
      logit( "e", "write_cta: opendir failed.\n" );
      return -1;
   }

   while ( dent = readdir( dirp ) )
   {
      char *d_name = dent->d_name;

      if ( strncmp( d_name, "wu", 2 ) == 0 )
      {
         if ( gotOne )
         {
            if ( strcmp( d_name, earliest ) < 0 )
               strcpy( earliest, d_name );
         }
         else
         {
            strcpy( earliest, d_name );
            gotOne = 1;
         }
      }
   }
   closedir( dirp );

   if ( gotOne )
   {
      strcpy( fname, earliest );
      return 0;                     /* Found a trace file */
   }
   else
      return -1;                    /* Couldn't find a trace file */
}



    /********************************************************************
     *                             ddTrace()                            *
     *                                                                  *
     *  Thread function invoked by write_cta.                           *
     *  This thread reads a disk file created by main() and writes the  *
     *  contents to tape.                                               *
     *                                                                  *
     *  The main thread will set term_flag when it's time to shut down. *
     *  ddTrace() will then finish writing all available disk files to  *
     *  tape and exit.                                                  *
     ********************************************************************/

thr_ret ddTrace( void *thr_arg )
{
   int numWritten = 0;    /* Number of tar files written to current tape */

/* Yield the cpu to the main thread, so the main thread
   doesn't drop too many tracebuf messages from transport.
   ******************************************************/
   thr_yield();

/* Change the working directory to spooldir
   ****************************************/
   if ( chdir( spooldir ) == -1 )
   {
      logit( "e", "write_cta: chdir(spooldir) failed.\n" );
      exit( -1 );
   }

/* Process one disk file each time through the loop
   ************************************************/
   while ( 1 )
   {
      int    rc;
      int    status;
      char   fname[80];
      int    msgWritten = 0;
      char   errText[80];
      int    isCompressed;
      int    fnameLen;

/* Wait for a disk file to show up.  Stay in this loop
   if the pauseWrite flag is set.  If we are stuck in this
   loop and the terminate flag is set, exit immediately.
   *******************************************************/
      while ( pauseWrite || (GetFileName(fname) == -1) )
      {
         if ( pauseWrite )
         {
            putchar( (char)7 );       /* Beeep */
            fflush( stdout );
            if ( !msgWritten )
            {
               puts( "write_cta: It is now safe to change tapes." );
               puts( "write_cta: Beeping will continue until \"cta resume\" is typed" );
               msgWritten = 1;
            }
         }
         if ( term_flag ) exit( 0 );
         sleep_ew( 1000 );
      }

/* See if the file is compressed
   *****************************/
      fnameLen = strlen( fname );
      isCompressed = ( (fnameLen>1) && !strcmp(&fname[fnameLen-2],".Z") ) ? 1: 0;

/* If we aren't compressing, erase any compressed files in
   spool directory.  This probably won't happen too often.
   *******************************************************/
      if ( !compress && isCompressed )
      {
         if ( remove( fname ) == -1 )
            logit( "et", "write_cta: Error deleting file: %s\n", fname );
         if ( term_flag ) exit( 0 );
         continue;
      }

/* Compress the file, if desired.  If the file has
   already been compressed, don't try to compress it again.
   *******************************************************/
      if ( compress && !isCompressed )
      {
         logit( "et", "write_cta: Compressing %s\n", fname );
         rc = CompressFile( fname, class, priority, &status );

         if ( rc < 0 )                        /* Couldn't invoke compress */
         {
            logit( "et", "write_cta: CompressFile error: %d\n", rc );
            if ( rc == -4 )
               logit( "et", "write_cta: compress terminated by a signal\n" );
            sprintf( errText, "Compress error. rc: %d", rc );
            SendStatus( typeError, ERR_COMPRESS, errText );
            if ( term_flag ) exit( 0 );
            sleep_ew( 60000 );
            continue;
         }
         else
         {
            if ( status != 0 )
            {
               logit( "et", "write_cta: CompressFile failed. Status: %d\n",
                      status );
               sprintf( errText, "Compress failed. Status: %d", status );
               SendStatus( typeError, ERR_COMPRESS, errText );
               if ( term_flag ) exit( 0 );
               sleep_ew( 60000 );
               continue;
            }
         }
         logit( "et", "write_cta: %s compressed.\n", fname );
         strcat( fname, ".Z" );
      }

/* Write the disk file to tape
   ***************************/
      logit( "et", "write_cta: Writing %s to tape.\n", fname );

      rc = WriteTarFile( fname, tapename, class, priority, &status );

/* Can't invoke tar.  Sleep a while and try again.
   **********************************************/
      if ( rc < 0 )
      {
         logit( "et", "write_cta: WriteTarFile() error: %d\n", rc );
         sprintf( errText, "Tar error. rc: %d", rc );
         SendStatus( typeError, ERR_TAR, errText );
         if ( term_flag ) exit( 0 );
         sleep_ew( 60000 );
         continue;
      }

/* Tar failed, possibly because we reached the end of tape.
   (which will produce an exit status of "2")
   *******************************************************/
      if ( status != 0 )
      {
         logit( "et", "write_cta: WriteTarFile() failed. Status: %d\n",
                status );
         numWritten = 0;             /* Reset counter of files written to tape */

/* Load the next tape into the autoloader
   **************************************/
         logit( "et", "write_cta: Taking tape offline.\n" );
         rc = LoadNextTape( tapename, changer, &status );

/* LoadNextTape() failed, possibly because we have already
   loaded the last tape.  We can no longer write to tape, but
   will continue writing disk files until the disk fills up.
   **********************************************************/
         if ( rc < 0 )
         {
            logit( "et", "LoadNextTape(): " );
            if ( rc ==  -1 ) logit( "e", "mt fork error." );
            if ( rc ==  -2 ) logit( "e", "mt command not in search path." );
            if ( rc ==  -3 ) logit( "e", "mt waitpid error." );
            if ( rc ==  -4 ) logit( "e", "mt error. No tape loaded or drive offline. status: %d", status );
            if ( rc ==  -5 ) logit( "e", "mt terminated by signal." );
            if ( rc ==  -6 ) logit( "e", "mt stopped by signal." );
            if ( rc ==  -7 ) logit( "e", "Unknown mt exit status." );
            if ( rc ==  -8 ) logit( "e", "mtx fork error." );
            if ( rc ==  -9 ) logit( "e", "mtx command not in search path." );
            if ( rc == -10 ) logit( "e", "mtx waitpid error." );
            if ( rc == -11 ) logit( "e", "mtx error. Can't load next tape. status: %d", status );
            if ( rc == -12 ) logit( "e", "mtx terminated by signal." );
            if ( rc == -13 ) logit( "e", "mtx stopped by signal." );
            if ( rc == -14 ) logit( "e", "Unknown mtx exit status." );
            logit( "e", "\n" );
            sprintf( errText, "LoadNextTape() error. rc: %d", rc );
            SendStatus( typeError, ERR_OFFLINE, errText );
            if ( term_flag ) exit( 0 );
            sleep_ew( 60000 );
            continue;
         }

/* LoadNextTape succeeded, so we successfully changed tapes.
   Try writing the same tar file to the new tape.
   ********************************************************/
         logit( "et", "write_cta: LoadNextTape() succeeded.\n" );
         sprintf( errText, "Autoloader tape changed.", status );
         SendStatus( typeError, MSG_OFFLINE, errText );

/* Get status of tape drive in Peak autoloader unit.
   GetMtStatus() will block until tape is loaded in drive.
   ******************************************************/
         if ( strcmp( changer, "none" ) != 0 )
         {
            char mtClass[] = "TS";
            int  mtPrio = 0;
            int  status;

            logit( "et", "write_cta: Getting mt status.\n" );
            rc = GetMtStatus( tapename, mtClass, mtPrio, &status );
            if ( rc < 0 )
               logit( "et", "write_cta: GetMtStatus() error. rc = &d\n", rc );
            else
               if ( status == 1 )
                  logit( "et", "mt command unrecognized or mt unable to open tape drive.\n" );
               else if ( status == 2 )
                  logit( "et", "An mt operation failed.\n" );
            continue;
         }
      }

/* Tar succeeded.  Log time elapsed
   since we started writing this tape.
   **********************************/
      logit( "et", "write_cta: File %s written to tape.\n", fname );
      numWritten++;
      {
         int secsWritten  = numWritten * TapeWriteIntSec;
         int daysWritten  = secsWritten / 86400;
         int hoursWritten = (secsWritten - 86400*daysWritten) / 3600;
         int minsWritten  = (secsWritten - 86400*daysWritten - 3600*hoursWritten) / 60;
         logit( "et", "write_cta: Elapsed time (dy,hr,mn): %d %02d:%02d\n",
                daysWritten, hoursWritten, minsWritten );
      }

/* Erase disk file
   ***************/
      if ( remove( fname ) == -1 )
         logit( "et", "write_cta: Error deleting file: %s\n", fname );
   }
}
