#include <stdio.h>
#include <string.h>

#define MAXRECIPIENT 10

int SendMail( char recipient[][60], int nrecipient, char *mailProg, 
              char *subject, char *msg, char *msgPrefix, 
              char *msgSuffix, char *mailServer, char *from );

  
int prmail( char *subject, char *msg )
{
   char mailProg[]   = "/usr/ucb/Mail";
   char mailServer[] = "gscamnlh01.wr.usgs.gov";
   int  nrecipient = 1;
   char recipient[MAXRECIPIENT][60];

   strcpy( recipient[0], "kohler@usgs.gov" );

   return SendMail( recipient, nrecipient, mailProg, subject, msg,
                    "", "", mailServer, "" );
}
