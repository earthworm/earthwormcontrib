
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


     /*************************************************************
      *                         MtRewind()                        *
      *                                                           *
      *  Rewind the tape.                                         *
      *                                                           *
      *  Returns -1 if error.                                     *
      *           0 if all is ok                                  *
      *************************************************************/

int MtRewind( char *tapeDev,
              int  *stat )         // Status code returned by mt
{
   pid_t pid;
   int   status;

/* Fork a child process to run the mt command
   ******************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                          /* In child process */
         execlp( "mt", "mt", "-f", tapeDev, "rewind", NULL );
         return -2;

      default:
         break;
   }

/* Wait for the mt command to complete
   ***********************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( status ) )          /* mt exited */
   {
      *stat = WEXITSTATUS( status );
      return 0;
   }
   else if ( WIFSIGNALED( status ) )   /* mt terminated by signal */
   {
      *stat = WTERMSIG( status );
      return -4;
   }
   else if ( WIFSTOPPED( status ) )    /* mt stopped by signal */
   {
      *stat = WSTOPSIG( status );
      return -5;
   }
   return -6;                         /* Unknown waitpid exit status */
}


     /*************************************************************
      *                         MtOffline()                       *
      *                                                           *
      *  Rewind the tape and eject it from the tape drive.        *
      *                                                           *
      *  Returns -1 if error.                                     *
      *           0 if all is ok                                  *
      *************************************************************/

int MtOffline( char *tapeDev,
              int  *stat )         // Status code returned by mt
{
   pid_t pid;
   int   status;

/* Fork a child process to run the mt command
   ******************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                          /* In child process */
         execlp( "mt", "mt", "-f", tapeDev, "offline", NULL );
         return -2;

      default:
         break;
   }

/* Wait for the mt command to complete
   ***********************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( status ) )          /* mt exited */
   {
      *stat = WEXITSTATUS( status );
      return 0;
   }
   else if ( WIFSIGNALED( status ) )   /* mt terminated by signal */
   {
      *stat = WTERMSIG( status );
      return -4;
   }
   else if ( WIFSTOPPED( status ) )    /* mt stopped by signal */
   {
      *stat = WSTOPSIG( status );
      return -5;
   }
   return -6;                         /* Unknown waitpid exit status */
}


     /*************************************************************
      *                        MtxControl()                       *
      *                                                           *
      *  Load or unload tape from library slot.                   *
      *                                                           *
      *  Returns -1 if error.                                     *
      *           0 if all is ok                                  *
      *************************************************************/

int MtxControl( char libDev[],       // Library device name
                char cmd[],          // Load or unload
                int  slot,           // Slot number
                int  dte,            // Data transfer element (drive)
                int  *stat )         // Status code returned by dd
{
   char  slotstr[40];
   char  dtestr[40];
   pid_t pid;
   int   status;

   sprintf( slotstr, "%d", slot );
   sprintf( dtestr, "%d", dte );

/* Fork a child process to run the mtx command
   *******************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                          /* In child process */
         execlp( "mtx", "mtx", "-f", libDev, cmd, slotstr, dtestr, NULL );
         return -2;

      default:
         break;
   }

/* Wait for the mtx command to complete
   ************************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( status ) )          /* mtx exited */
   {
      *stat = WEXITSTATUS( status );
      return 0;
   }
   else if ( WIFSIGNALED( status ) )   /* mtx terminated by signal */
   {
      *stat = WTERMSIG( status );
      return -4;
   }
   else if ( WIFSTOPPED( status ) )    /* mtx stopped by signal */
   {
      *stat = WSTOPSIG( status );
      return -5;
   }
   return -6;                         /* Unknown waitpid exit status */
}


     /*************************************************************
      *                           MtFs()                          *
      *                                                           *
      *  Skip format or backwards nskip files.                    *
      *                                                           *
      *  Returns -1 if error.                                     *
      *           0 if all is ok                                  *
      *************************************************************/

int MtFs( char *tapeDev, int nskip, int *stat )
{
   char  forback[40];
   char  skip[40];
   pid_t pid;
   int   status;

   if ( nskip == 0 )
   {
      *stat = 0;
      return 0;
   }

   if ( nskip > 0 )
   {
      strcpy( forback, "fsf" );
      sprintf( skip, "%d", nskip );
   }
   else
   {
      strcpy( forback, "nbsf" );
      sprintf( skip, "%d", -nskip );
   }

/* Fork a child process to run the mt command
   ******************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                          /* In child process */
         execlp( "mt", "mt", "-f", tapeDev, forback, skip, NULL );
         return -2;

      default:
         break;
   }

/* Wait for the mt command to complete
   ***********************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( status ) )          /* mt exited */
   {
      *stat = WEXITSTATUS( status );
      return 0;
   }
   else if ( WIFSIGNALED( status ) )   /* mt terminated by signal */
   {
      *stat = WTERMSIG( status );
      return -4;
   }
   else if ( WIFSTOPPED( status ) )    /* mt stopped by signal */
   {
      *stat = WSTOPSIG( status );
      return -5;
   }
   return -6;                         /* Unknown waitpid exit status */
}


int Mtdd( char *tapeDev,       // eg /dev/rmt/0
          char outfile[],      // Name of output file
          int ibs,             // Tape input block size
          int *stat )          // Status code returned by dd
{
   char  ifile[40];
   char  ofile[40];
   char  iblsz[40];
   pid_t pid;
   int   status;

   sprintf( ifile, "if=%s", tapeDev );
   sprintf( ofile, "of=%s", outfile );
   sprintf( iblsz, "ibs=%d", ibs );

/* Fork a child process to run the dd command.
   The "e" option means: exit immediately on any
   write error, with a positive status code.
   *********************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                          /* In child process */
         execlp( "dd", "dd", ifile, ofile, iblsz, NULL );
         return -2;

      default:
         break;
   }

/* Wait for the dd command to complete
   ***********************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( status ) )          /* dd exited */
   {
      *stat = WEXITSTATUS( status );
      return 0;
   }
   else if ( WIFSIGNALED( status ) )   /* dd terminated by signal */
   {
      *stat = WTERMSIG( status );
      return -4;
   }
   else if ( WIFSTOPPED( status ) )    /* dd stopped by signal */
   {
      *stat = WSTOPSIG( status );
      return -5;
   }
   return -6;                         /* Unknown waitpid exit status */
}
