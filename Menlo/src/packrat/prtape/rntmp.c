#include <stdio.h>
#include <stdlib.h>
#include "web.h"
#include "chron3.h"

void ReadWeb( FILE *prfp, WEB *w );
int  ReadWaveBlock( FILE *prfp, short *wdb, int nsamp );
void PrintBlockHeader( BLOCK *b, double bst );


int GetFileStartTime( char *prfname, double *fst )
{
   extern FILE *fplog;     // Declared in prtape.c
   const  int web_size = 65534;  // Web header block size
   int    seekbyte;
   double bst;             // Block start time
   WEB    w;               // Packrat web structure (scalar parameters)
   FILE   *prfp;           // File pointer to packrat file
   short  *wdb;            // Packrat waveform block
   BLOCK  *wdbh;           // Packrat waveform block header structure

/* Open the packrat file
   *********************/
   prfp = fopen( prfname, "r" );
   if ( prfp == NULL )
   {
      printf( "Error opening packrat file: %s\n", prfname );
      fprintf( fplog, "Error opening packrat file: %s\n", prfname );
      return -1;
   }

/* Read packrat web structure
   **************************/
   ReadWeb( prfp, &w );

/* Allocate packrat waveform data block.  Set addresses
   of waveform data block header and waveform data array.
   *****************************************************/
   wdb = (short *) calloc( w.nwdio, sizeof(short) );
   if ( wdb == NULL )
   {
      printf( "ERROR: Cannot allocate packrat waveform data block.\n" );
      fprintf( fplog, "ERROR: Cannot allocate packrat waveform data block.\n" );
      return -1;
   }
   wdbh = (BLOCK *)wdb;

/* Read first waveform block
   *************************/
   seekbyte = web_size;   // Skip the packrat file header (web) block
   fseek( prfp, seekbyte, SEEK_SET );
   if ( ReadWaveBlock( prfp, wdb, w.nwdio ) == -1 )
   {
      printf( "ReadWaveBlock() error.\n", prfname );
      fprintf( fplog, "ReadWaveBlock() error.\n", prfname );
      return -1;
   }
   bst = w.dtirig + (double)w.stirig * (wdbh->lrtc - w.ltirig);
// PrintBlockHeader( wdbh, bst );
   fclose( prfp );
   *fst = bst;
   return 0;
}


void PrintBlockHeader( BLOCK *wdbh, double bst )
{
   char c18[20];

   printf( "  %6d", wdbh->lbuf );
   printf( "  %6d", wdbh->lrtc );
   date18( bst, c18 );
   printf( "  File start time: %s\n", c18 );
   return;
}

