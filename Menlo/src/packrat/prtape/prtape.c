
    /*******************************************************************
     *                              prtape                             *
     *                                                                 *
     *  Program to read files from a packrat/squirrel tape.            *
     *  This is a Solaris-only program.                                *
     *                                                                 *
     *  If control-c is pressed while reading tapes, the program will  *
     *  finish reading the current file, return the current tape to    *
     *  its slot in the autoloader, and then exit.                     *
     *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>     // for stat()
#include <sys/stat.h>      // for stat()
#include "time_ew.h"
#include "chron3.h"

#define  MAXSLOT 100

/* Function prototypes
   *******************/
int  MtRewind( char *tapeDev, int *stat );
int  MtOffline( char *tapeDev, int *stat );
int  MtFs( char *tapeDev, int nskip, int *stat );
int  Mtdd( char *tapeDev, char prfname[], int ibs, int *stat );
void GetFileLen( char prfname[], int *fileLen );
void RenamePackratFile( char prfname[], int tape, int filenum );
void GetConfig( char *configfile );
int  ReadTape( int slotnum, int tape, int firstfile, int lastfile );
int  MtxControl( char libDev[], char cmd[], int slot, int dte,
                 int *stat );
void logit_init( char *logName );
void logit( char *format, ... );
void LogCurrentTime( void );
int  CleanTapeHeads( int slotnum, int filenum );
int  prmail( char *subject, char *msg );

/* Global variables, set from prtape config file
   *********************************************/
static char libDev[80]  = "/dev/changer";
static char tapeDev[80] = "/dev/rmt/0n";
static char logfile[80] = "prtape.log";
static int  dte = 0;                  // Data transfer element
static int  ibs = 65534;              // Tape input block size
static int  pr2tb = 0;                // If 1, convert packrat to tb
static int  nslot = 0;                // Number of slots used
static int  slot[MAXSLOT];            // Library slot number (1-21)
static int  tape[MAXSLOT];            // Tape number, written on label
static int  firstfile[MAXSLOT];       // First file to read
static int  lastfile[MAXSLOT];        // Last file to read
static int  rc_readtape[MAXSLOT];     // Return code of ReadTape()
static char subject[80];              // mail message
static char msg[160];                 // mail message


int main( int argc, char *argv[] )
{
   int    rc = 0;               // Return code of this program
   int    i;
   char   *configfile;
   time_t ltime;                // Current time
   char   default_config[] = "prtape.d";

/* Get name of config file
   ***********************/
   configfile = (argc > 1) ? argv[1] : default_config;

/* Open and read the prtape configuration file.
   If any parameters are not in the config file, use default values.
   If the config file doesn't exist or if the format is bad,
   GetConfig() will exit.
   ****************************************************************/
   GetConfig( configfile );

/* Initialize arrays
   *****************/
   for ( i = 0; i < MAXSLOT; i++ )
      rc_readtape[i] = 9;            // Tape not read

/* Open log file
   *************/
   logit_init( logfile );

/* Log config file parameters
   **************************/
   logit( "Program start time: " );
   LogCurrentTime();
   logit( "Configuration file: %s\n", configfile );
   logit( "Tape library device name: %s\n", libDev );
   logit( "Tape drive device name: %s\n", tapeDev );
   logit( "Log file name: %s\n", logfile );
   logit( "Tape input block size: %d\n", ibs );
   logit( "Data transfer element: %d\n", dte );
   if ( pr2tb == 1 )
      logit( "Each packrat file will be converted to tb format.\n" );
   else
      logit( "Packrat files will not be converted to tb format.\n" );
   logit( "\nCopy the following tape slots/tapes/files:\n" );
   for ( i = 0; i < nslot; i++ )
   {
      logit( "Slot %2d   Tape %4d", slot[i], tape[i] );
      if ( (firstfile[i] != 1) || (lastfile[i] != 999) )
      {
         logit( "   FirstFile %2d", firstfile[i] );
         logit( "   LastFile %2d",  lastfile[i] );
      }
      logit( "\n" );
   }

/* We aren't using a tape library.
   Read tape currently loaded in the tape drive.
   When finished, the tape isn't rewound.
   ********************************************/
   if ( strcmp(libDev, "none") == 0 )
   {
      rc_readtape[0] = ReadTape( slot[0], tape[0], firstfile[0], lastfile[0] );
      if ( rc_readtape[0] == -1 )
      {
         logit( "ReadTape() error. Exiting.\n" );
         rc = -1;         // Error return
      }
      exit( 0 );
   }

/* We are using a tape library
   ***************************/
   for ( i = 0; i < nslot; i++ )
   {
      int  mtxstatus;
      int  j;
      char tapetype[20];
      const int ntry_unload = 10;

/* Is this tape a cleaning tape or a packrat tape?
   **********************************************/
      if ( tape[i] == 0 )
         strcpy( tapetype, "cleaning tape" );
      else
         sprintf( tapetype, "tape %d", tape[i] );

/* Load tape
   *********/
      logit( "\nLoading %s from library slot %d into dte %d\n",
         tapetype, slot[i], dte );
      if ( MtxControl( libDev, "load", slot[i], dte, &mtxstatus ) < 0 )
      {
         logit( "MtxControl() load error.\n" );
         exit( -1 );
      }
      if ( mtxstatus != 0 )
      {
         logit( "ERROR loading tape. mtxstatus: %d\n", mtxstatus );
         if ( mtxstatus == 1 )
            logit( "Make sure library is under SCSI control.\n" );
         rc = -1;
         break;
      }
      logit( "Sleeping 20 seconds to ensure load is complete\n" );
      sleep( 20 );

/* This is a cleaning tape. Cleaning tapes can be
   installed in any slot in the autoloader.
   They have a tape number of 0 in the prtape.d file.
   *************************************************/
      if ( tape[i] == 0 )
      {
         logit( "Waiting 20 seconds for cleaning to complete\n" );
         sleep( 20 );
      }

/* This is a data tape.
   Read it using the ReadTape() function.
   If rc_readtape =  0, no errors were detected.
   If rc_readtape = -1, errors were detected.  Try the next tape.
   If rc_readtape = -2, unload tape and exit program.
   *************************************************************/
      else
      {
         int mt_offline_status;
         int k;
         const int ntry_eject = 30;

         rc_readtape[i] = ReadTape( slot[i], tape[i], firstfile[i], lastfile[i] );
         if ( rc_readtape[i] == -1 )
            logit( "ReadTape() error.\n" );

/* Eject the tape cartridge from the drive.
   If this fails, wait a while and try again.
   If still no luck, exit the program.
   *****************************************/
         for ( k = 0; k < ntry_eject; k++ )
         {
            logit( "Ejecting tape cartridge from dte %d\n", dte );
            MtOffline( tapeDev, &mt_offline_status );
            if ( mt_offline_status == 0 ) goto EJECT_SUCCEEDED;
            logit( "ERROR ejecting tape cartridge from dte %d\n", dte );
            logit( "mt_offline_status: %d\n", mt_offline_status );
            if ( remove( "kill-prtape" ) == 0 )
               { logit( "Found file kill-prtape. Exiting.\n" ); break; }
            if ( k < (ntry_eject-1) ) sleep( 120 );
         }
         logit( "Error ejecting tape after %d tries. Exiting.\n", ntry_eject );
         break;
EJECT_SUCCEEDED:
         ;       // Empty statement
      }

/* Unload tape.  If this fails, wait a while and
   try again.  The autoloader may be resetting itself.
   **************************************************/
      for ( j = 0; j < ntry_unload; j++ )
      {
         logit( "Unloading %s from dte %d into library slot %d\n",
            tapetype, dte, slot[i] );
         if ( MtxControl( libDev, "unload", slot[i], dte, &mtxstatus ) < 0 )
            { logit( "MtxControl() unload error.\n" ); break; }
         if ( mtxstatus == 0 ) goto UNLOAD_SUCCEEDED;
         logit( "ERROR unloading tape. mtxstatus: %d\n", mtxstatus );
         logit( "Make sure library is under SCSI control.\n" );
         if ( remove( "kill-prtape" ) == 0 )
            { logit( "Found file kill-prtape. Exiting.\n" ); break; }
         if ( j < (ntry_unload-1) ) sleep( 60 );
      }
      logit( "Error unloading tape after %d tries. Exiting.\n", ntry_unload );
      break;
UNLOAD_SUCCEEDED:

/* Exit if a kill file is detected
   *******************************/
      if ( rc_readtape[i] == -2 ) break;    // Kill-file detected by ReadTape()
      if ( remove( "kill-prtape" ) == 0 )   // Check again for kill file
      {
         logit( "Found file kill-prtape. Exiting.\n" );
         break;
      }
   }

/* Log some status messages and exit
   *********************************/
   logit( "\nRead status of packrat tapes:\n" );
   for ( i = 0; i < nslot; i++ )
   {
      if ( tape[i] == 0 ) continue;        // Skip cleaning tapes
      logit( "Slot %2d   Tape %4d   ", slot[i], tape[i] );
      if ( rc_readtape[i] ==  9 ) logit( "Tape not read." );
      if ( rc_readtape[i] ==  0 ) logit( "Tape read ok." );
      if ( rc_readtape[i] == -1 ) logit( "Read errors detected." );
      if ( rc_readtape[i] == -2 ) logit( "Program killed while reading tape." );
      logit( "\n" );
   }
   logit( "Program exit time: " );
   LogCurrentTime();
   return rc;
}


 /***************************************************************
  *                          ReadTape()                         *
  *                                                             *
  *  Read all files from one packrat tape.                      *
  *  The tape must already be loaded into the tape drive.       *
  *  The tape is rewound before reading, but not after.         *
  *  ReadTape() returns  0 if no error was detected.            *
  *                     -1 if an error was detected.            *
  *                     -2 if we want to terminate the program. *
  ***************************************************************/

int ReadTape( int slotnum, int tape, int firstfile, int lastfile )
{
   int j;
   int nskip;
   int filenum;
   int stat;
   int mt_fsf_status;
   int wtmtrc;
   const int ntry_rewind = 2;
   const int ntry_dd     = 1;

/* Rewind the tape.  If a rewind error occurs,
   sleep 60 seconds and try again.
   ******************************************/
   for ( j = 0; j < ntry_rewind; j++ )
   {
      MtRewind( tapeDev, &stat );
      if ( stat == 0 ) goto REWIND_SUCCEEDED;
      logit( "MtRewind() error.\n" );
      if ( j < (ntry_rewind-1) ) sleep( 60 );
   }
   logit( "Rewind failed after %d attempts.\n", ntry_rewind );
   return -1;
REWIND_SUCCEEDED:

/* Skip to the first tape file to be copied to disk
   ************************************************/
   nskip = firstfile - 1;
   if ( nskip < 0 )
   {
      logit( "Skipping backward %d tape file", -nskip );
      if ( nskip < -1 ) logit( "s" );
      logit( "\n" );
   }

   if ( nskip > 0 )
   {
      logit( "Skipping forward %d tape file", nskip );
      if ( nskip > 1 ) logit( "s" );
      logit( "\n" );
   }
   wtmtrc = MtFs( tapeDev, nskip, &mt_fsf_status );
   if ( wtmtrc < 0 )       // This should never happen
   {
      logit( "MtFs() error.\n" );
      return -1;
   }
   if ( mt_fsf_status != 0 )
   {
      logit( "ERROR detected while skipping files.\n" );
      logit( "mt_fsf_status: %d\n", mt_fsf_status );
      return -1;
   }

/* Loop through all files on packrat tape
   **************************************/
   for ( filenum = firstfile; filenum <= lastfile; filenum++ )
   {
      char *ptr;
      char prfname[80];
      int wtddrc, ddstatus;
      int fileLen;

/* Create name of temporary file
   *****************************/
      ptr = tempnam( ".", "tmp" );
      if ( ptr == NULL )
      {
         logit( "tempnam() error.\n" );
         return -1;
      }
      strncpy( prfname, ptr, 80 );
      free( ptr );

/* Read tape file using dd.
   If a dd error was detected, try again.
   *************************************/
      for ( j = 0; j < ntry_dd; j++ )
      {

/* After the first try, clean the tape heads
   *****************************************/
         if ( j > 0 )
            if ( CleanTapeHeads(slotnum, filenum) != 0 ) return -1;

/* Read file using dd.
   dd returns 0 if no errors were detected.
   dd returns 2 if a read error was detected.
   *****************************************/
         logit( "Copying tape %d file %d from %s\n", tape, filenum, tapeDev );
         wtddrc = Mtdd( tapeDev, prfname, ibs, &ddstatus );
         if ( wtddrc < 0 )       // This should never happen
         {
            logit( "Mtdd() error.\n" );
            return -1;
         }
         if ( ddstatus == 0 ) goto DD_SUCCEEDED;
         logit( "Error %d detected by dd\n", ddstatus );
         unlink( prfname );
      }
      logit( "dd failed %d times.\n", ntry_dd );
      logit( "Sleeping 60 seconds.\n" );
      sleep( 60 );
      return -1;
DD_SUCCEEDED:

/* If a zero-length disk file was created, we read a logical
   end-of-tape (EOT).  Delete the zero-length file.
   *********************************************************/
      GetFileLen( prfname, &fileLen );
      if ( fileLen == 0 )
      {
         logit( "Logical end-of-tape encountered\n" );
         unlink( prfname );
         return 0;
      }

/* Log some stuff
   **************/
      logit( "Finished copying file %d", filenum );
      logit( " from tape cartridge in library slot %d\n", slotnum );

/* Rename the packrat file with format tttt_ff.pr where
   tttt is the tape number and ff is the file number
   ****************************************************/
      RenamePackratFile( prfname, tape, filenum );
      logit( "Renamed temporary file to %s\n", prfname );

/* Convert the packrat file to tracebuf format
   *******************************************/
      if ( pr2tb == 1 )
      {
         FILE *fp_pipe;
         int  rc_pclose;

         fp_pipe = popen( "pr2tb -d -r | tee -a pr2tb.log", "w" );
         if ( fp_pipe == NULL )
         {
            logit( "Error opening pipe to pr2tb\n" );
            return -1;
         }
         fprintf( fp_pipe, "%s\n", prfname );
         rc_pclose = pclose( fp_pipe );
         if ( rc_pclose != 0 )
         {
            logit( "pr2tb error detected. rc_pclose: %d\n", rc_pclose );
            return -1;
         }
      }

/* If file kill-prtape exists, delete it.
   Don't read any more files from this tape.
   ****************************************/
      if ( remove( "kill-prtape" ) == 0 )
      {
         logit( "Found file kill-prtape. Exiting.\n" );
         return -2;
      }
   }
   return 0;
}


void GetFileLen( char prfname[], int *fileLen )
{
   struct stat buf;

   stat( prfname, &buf );
   *fileLen = buf.st_size;
   return;
}


void RenamePackratFile( char prfname[], int tape, int filenum )
{
   char   fnameNew[80];

   sprintf( fnameNew, "%04df%02d.pr", tape, filenum );
   if ( rename( prfname, fnameNew ) == -1 )
   {
      logit( "ERROR renaming file %s to %s\n", prfname, fnameNew );
      logit( "Exiting.\n" );
      exit( 0 );
   }
   strcpy( prfname, fnameNew );
   return;
}


void GetConfig( char *configfile )
{
   int  i;
   char line[80];
   char *p;
   int  len;
   FILE *fp;

/* Initialize arrays
   *****************/
   for ( i = 0; i < MAXSLOT; i++ )
   {
      firstfile[i] =   1;
      lastfile[i]  = 999;
   }

   fp = fopen( configfile, "r" );
   if ( fp == NULL )
   {
      printf( "ERROR open config file: %s\n", configfile );
      exit( -1 );
   }

   while ( fgets(line, 80, fp) != NULL ) 
   {
      p = strtok( line, " " );

      if ( strcmp(p, "LibDev") == 0 )
      {
         p = strtok( NULL, " " );
         if ( p == NULL ) continue;
         strcpy( libDev, p );
         len = strlen( libDev ) - 1;
         if ( libDev[len] == '\n' ) libDev[len] = NULL;
         continue;
      }
      if ( strcmp(p, "TapeDrive") == 0 )
      {
         p = strtok( NULL, " " );
         if ( p == NULL ) continue;
         strcpy( tapeDev, p );
         len = strlen( tapeDev ) - 1;
         if ( tapeDev[len] == '\n' ) tapeDev[len] = NULL;
         p = strtok( NULL, " " );
         if ( p == NULL ) continue;
         if ( sscanf(p, "%d", &dte) < 1 )
         {
            printf( "ERROR decoding dte from config file.\n" );
            exit( -1 );
         }
         continue;
      }
      if ( strcmp(p, "LogFile") == 0 )
      {
         p = strtok( NULL, " " );
         if ( p == NULL ) continue;
         strcpy( logfile, p );
         len = strlen( logfile ) - 1;
         if ( logfile[len] == '\n' ) logfile[len] = NULL;
         continue;
      }
      if ( strcmp(p, "ibs") == 0 )
      {
         p = strtok( NULL, " " );
         if ( p == NULL ) continue;
         if ( sscanf(p, "%d", &ibs) < 1 )
         {
            printf( "ERROR decoding ibs from config file.\n" );
            exit( -1 );
         }
         continue;
      }
      if ( strcmp(p, "Pr2tb") == 0 )
      {
         p = strtok( NULL, " " );
         if ( p == NULL ) continue;
         if ( sscanf(p, "%d", &pr2tb) < 1 )
         {
            printf( "ERROR decoding Pr2tb from config file.\n" );
            exit( -1 );
         }
         continue;
      }
      if ( strcmp(p, "Slot") == 0 )
      {
         int nsl = nslot;

         if ( nslot == MAXSLOT )
         {
            printf( "ERROR. Too many Slot commands in config file.\n" );
            printf( "Max number of Slot commands is %d\n", MAXSLOT );
            exit( -1 );
         }
         p = strtok( NULL, " " );
         if ( p == NULL ) continue;
         if ( sscanf(p, "%d", &slot[nslot]) < 1 )
         {
            printf( "ERROR decoding slot[%d] from config file.\n",
                    nslot );
            exit( -1 );
         }
         if ( (slot[nslot] < 1) || (slot[nslot] > MAXSLOT) )
         {
            printf( "ERROR. Invalid slot number: %d\n", slot[nslot] );
            exit( -1 );
         }
         p = strtok( NULL, " " );
         if ( p == NULL ) continue;
         if ( sscanf(p, "%d", &tape[nslot]) < 1 )
         {
            printf( "ERROR decoding tape[%d] from config file.\n",
                    nslot );
            exit( -1 );
         }
         nslot++;
         p = strtok( NULL, " " );
         if ( p == NULL ) continue;
         if ( sscanf(p, "%d", &firstfile[nsl]) < 1 )
         {
            printf( "ERROR decoding firstfile[%d] from config file.\n",
                    nsl );
            exit( -1 );
         }
         p = strtok( NULL, " " );
         if ( p == NULL ) continue;
         if ( sscanf(p, "%d", &lastfile[nsl]) < 1 )
         {
            printf( "ERROR decoding lastfile[%d] from config file.\n",
                    nsl );
            exit( -1 );
         }
         continue;
      }
   }
   fclose( fp );
   return;
}


/**************************************************************
 *                      CleanTapeHeads()                      *
 *  Currently, this function just rewinds the tape and finds  *
 *  the current file.  No tape head cleaning is performed.    *
 **************************************************************/

int CleanTapeHeads( int slotnum, int filenum )
{
   int nskip;
   int stat;
   int mt_fsf_status;
   int wtmtrc;

   logit( "Rewinding tape\n" );
   MtRewind( tapeDev, &stat );
   if ( stat != 0 )
   {
      logit( "MtRewind() error.\n" );
      return -1;
   }
   nskip = filenum - 1;
   if ( nskip > 0 )
   {
      logit( "Skipping forward %d tape files\n", nskip );
      wtmtrc = MtFs( tapeDev, nskip, &mt_fsf_status );
      if ( wtmtrc < 0 )       // This should never happen
      {
         logit( "MtFs() error.\n" );
         return -1;
      }
      if ( mt_fsf_status != 0 )
      {
         logit( "ERROR detected while skipping files.\n" );
         logit( "mt_fsf_status: %d\n", mt_fsf_status );
         return -1;
      }
   }
   return 0;
}
