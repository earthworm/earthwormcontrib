#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

static int ctrlc = 0;


void handler( int signo )
{
   write( 1, "Hi, I'm the signal handler!\n", 29 );
   ctrlc = 1;
}


int main( int argc, char *argv[] )
{
   int i;
   sigset_t sigs;
   void (*ohand)(int);

   sigemptyset( &sigs );                   // Initialize to all 0's
   sigaddset( &sigs, SIGINT );             // Add ctrl-c to signal set

   ohand = sigset( SIGINT, handler );      // Install the handler
   if ( ohand == SIG_ERR )
   {
      printf( "sigset() error" );
      return 0;
   }

   for ( i = 0; i < 5; i++ )
   {
      printf( "Entering loop %d\n", i );
      sigprocmask( SIG_BLOCK, &sigs, NULL );    // Block ctrl-C
      sleep( 10 );
      printf( "Leaving loop %d\n", i );
      sigprocmask( SIG_UNBLOCK, &sigs, NULL );  // Unblock ctrl-C
      if ( ctrlc == 1 ) break;
   }
   printf( "Exiting!\n" );
   return 0;
}
