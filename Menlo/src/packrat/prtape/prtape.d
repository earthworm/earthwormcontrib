#
# This is prtape's parameter file
# All parameters are optional (they have default values).
#
# LogFile: Name of log file.
# Default value is prtape.log.
LogFile    prtape.log
#
# ibs: Tape input block size.
# Default value is 65534.
ibs        65534
#
# LibDev: Library device name.
# If a library is not in use, set libDev to "none".
# Default value is /dev/changer
LibDev     /dev/changer
#
# TapeDrive: Two parameters, tapeDev and dte.
# tapeDev is the tape device name, which should end in "n".
# Default value of tapeDev is /dev/rmt/0n.
# dte is the data transfer element, as display by the mtx
# status command.  dte is only used if we are using a tape
# library.  Default value of dte is 0.
TapeDrive  /dev/rmt/0n  0
#
# Pr2tb: If 1, convert each packrat file to tracebuf format
# and then delete the packrat file.
# Default value of Pr2tb is 0.
Pr2tb       1
#
# Slot: Four parameters, slot #, tape #, firstfile and lastfile.
# Use one Slot command for each slot you want to read from,
# up to a maximum of 21 slots.
# Tapes will be read in the order specified below.
#
# firstfile is the file number of first file to read from tape.
# Set firstfile to 1 to read from beginning of tape.
# lastfile is the file number of last file to read from tape.
# Set lastfile to 999 to read to end of tape.
# If firstfile and lastfile are not specified, 1-999 is assumed.
#
# If a slot contains a cleaning tape, set the tape # to 0.
# Usually, cleaning tapes are stored in slot 1, although they
# can be stored in any slot.
#
#   slot tape first last
#    num  num file  file
#   ---- ---- ----- ----
#Slot  2 1004   12  999
#Slot  3 1005    2  999
#Slot  4 1006    3  999
#Slot  5 1007    2  999
#Slot  6 1008
#Slot  7 1009
#Slot  8 1010   23  999
 Slot  1    0
 Slot  9 1011   25  999
 Slot 10 1012
 Slot 11 1013
 Slot 12 1014
 Slot  1    0
 Slot 13 1015
 Slot 14 1016
 Slot 15 1017
 Slot 16 1018
