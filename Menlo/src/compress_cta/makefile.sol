#
#                  Make file for compress_cta
#                        Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib
I = $(EW_HOME)/$(EW_VERSION)/include


COMPRESSCTA = compress_cta.o getfname.o compressfile.o \
              setprio.o \
              $L/logit.o $L/kom.o $L/getutil.o \
              $L/sleep_ew.o $L/time_ew.o $L/transport.o

compress_cta: $(COMPRESSCTA)
	cc -o $B/compress_cta $(COMPRESSCTA) -lm -lposix4 -lc

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/compress_cta*

lint:
	lint -I $I -Nlevel=4 compressfile.c

