
  /********************************************************************
   *                          compress_cta.c                          *
   ********************************************************************/
/*
   This program compresses cta files, and moves them to another
   directory.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <unistd.h>
#include <kom.h>
#include <transport.h>
#include "compress_cta.h"

/* Function prototypes
   *******************/
void GetConfig( char * );
void LogConfig( void );
void LookupMore( void );
void LookupMsgType( void );
int  CompressFile( char *fname, char *compressClass,
                   int  compressPrio, int  *stat );

/* Global variables
   ****************/
static SHM_INFO Region;              /* Shared memory region to use for i/o */
static pid_t    myPid;               /* Process id of this process */

/* Get these from the config file
   ******************************/
static char     RingName[20];        /* Name of transport ring for i/o */
static char     MyModName[20];       /* Speak as this module name/id */
static long     HeartBeatInt;        /* Seconds between heartbeats */
static char     InDir[100];          /* Name of input directory */
static char     OutDir[100];         /* Name of output directory */
static char     CompressClass[20];   /* Class for compress process */
static int      CompressPrio;        /* Priority for compress process */

/* Look these up in the earthworm.h tables with getutil functions
   **************************************************************/
static long          RingKey;        /* Transport ring key */
static unsigned char InstId;         /* Local installation id */
static unsigned char MyModId;        /* Module Id for this program */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;


int main( int argc, char **argv )
{
   long timeNow;           /* Current time */
   long timeLastBeat;      /* Time last heartbeat was sent */
   char *configFile;

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      printf( "Usage: compress_cta <configfile>\n" );
      return 0;
   }
   configFile = argv[1];

/* Look up message types from earthworm.h tables
   *********************************************/
   LookupMsgType();

/* Read the configuration file
   ***************************/
   GetConfig( configFile );

/* Look up more info from earthworm.h tables
   *****************************************/
   LookupMore();

/* Initialize name of log-file & open it
   *************************************/
   logit_init( "compress_cta", (short) MyModId, 256, 1 );
   logit( "" , "Read command file <%s>\n", argv[1] );

/* Get the pid of this process for restart purposes
   ************************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "compress_cta: Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Log the configuration file
   **************************/
   LogConfig();

/* Change working directory to OutDir,
   just to make sure OutDir exists.
   **********************************/
   if ( chdir( OutDir ) == -1 )
   {
      logit( "e", "compress_cta: Can't cwd to %s\n", OutDir );
      logit( "e", "compress_cta: Exiting.\n" );
      return -1;
   }

/* Change working directory to InDir
   *********************************/
   if ( chdir( InDir ) == -1 )
   {
      logit( "e", "compress_cta: Can't cwd to %s\n", InDir );
      logit( "e", "compress_cta: Exiting.\n" );
      return -1;
   }

/* Attach to shared memory ring
   ****************************/
   tport_attach( &Region, RingKey );
   logit( "", "Attached to public memory region: %s\n", RingName );

/* Force a heartbeat to be issued in first pass thru main loop
   ***********************************************************/
   timeLastBeat = time(&timeNow) - HeartBeatInt - 1;

/* Main loop
   *********/
   while ( tport_getflag( &Region ) != TERMINATE &&
           tport_getflag( &Region ) != myPid )
   {
      char ctafile[80];       /* Name of cta file */

/* Send a heartbeat to statmgr
   ***************************/
      if  ( (time(&timeNow) - timeLastBeat) >= HeartBeatInt )
      {
         timeLastBeat = timeNow;
         SendStatus( TypeHeartBeat, 0, "" );
      }

/* See if a cta file has arrived.
   If so, compress it.
   *****************************/
      if ( GetFileName( ctafile ) == 0 )
      {
         int  rc, stat;
         char errText[80];
         char inName[100];
         char outName[100];

         logit( "et", "compress_cta: Compressing file %s\n", ctafile );

         rc = CompressFile( ctafile, CompressClass, CompressPrio, &stat );
         if ( rc < 0 )                        /* Couldn't invoke compress */
         {
            logit( "et", "compress_cta: CompressFile error: %d\n", rc );
            if ( rc == -4 )
               logit( "et", "compress_cta: compress terminated by a signal\n" );
            sprintf( errText, "Compress error. rc: %d", rc );
            SendStatus( TypeError, ERR_COMPRESS, errText );
            sleep_ew( 60000 );
            continue;
         }
         else
         {
            if ( stat != 0 )
            {
               logit( "et", "compress_cta: CompressFile failed. Status: %d\n",
                      stat );
               sprintf( errText, "Compress failed. Status: %d", stat );
               SendStatus( TypeError, ERR_COMPRESS, errText );
               sleep_ew( 60000 );
               continue;
            }
         }
         logit( "et", "compress_cta: File %s compressed\n", ctafile );

/* Move the compressed file to OutDir
   **********************************/
         snprintf( inName,  100, "%s.Z",    ctafile );
         snprintf( outName, 100, "%s/%s.Z", OutDir, ctafile );

         if ( rename( inName, outName ) == -1 )
            logit( "et", "compress_cta: Error moving file %s to directory %s\n",
                   inName, OutDir );
/*       else
            logit( "et", "compress_cta: File %s moved to directory %s\n",
                   inName, OutDir ); */
      }

      sleep_ew( 1000 );       /* Wait for new cta files to arrive */
   }

/* Get ready to exit
   *****************/
   tport_detach( &Region );
   logit( "et", "compress_cta: Exiting.\n" );
   return 0;
}


  /************************************************************************
   *  GetConfig() processes command file(s) using kom.c functions;        *
   *                    exits if any errors are encountered.              *
   ************************************************************************/

#define NCOMMAND 7

void GetConfig( char *configfile )
{
   const int ncommand = NCOMMAND;    /* Process this many required commands */
   char      init[NCOMMAND];         /* Init flags, one for each command */
   int       nmiss;                  /* Number of missing commands */
   char      *com;
   char      *str;
   int       nfiles;
   int       success;
   int       i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      printf( "compress_cta: Error opening command file <%s>. Exiting.\n",
               configfile );
      exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )             /* Read next line from active file  */
      {
         com = k_str();            /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )          continue;
         if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
         if ( com[0] == '@' )
         {
            success = nfiles+1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "compress_cta: Can't open command file <%s>. Exiting.\n",
                        &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         else if ( k_its("MyModuleId") )
         {
             str = k_str();
             if (str) strcpy( MyModName, str );
             init[0] = 1;
         }
         else if ( k_its("RingName") )
         {
             str = k_str();
             if (str) strcpy( RingName, str );
             init[1] = 1;
         }
         else if ( k_its("HeartBeatInt") )
         {
             HeartBeatInt = k_long();
             init[2] = 1;
         }
         else if ( k_its("InDir") )
         {
             str = k_str();
             if (str) strcpy( InDir, str );
             init[3] = 1;
         }
         else if ( k_its("OutDir") )
         {
             str = k_str();
             if (str) strcpy( OutDir, str );
             init[4] = 1;
         }
         else if ( k_its("CompressClass") )
         {
             str = k_str();
             if (str) strcpy( CompressClass, str );
             init[5] = 1;
         }
         else if ( k_its("CompressPrio") )
         {
             CompressPrio = k_int();
             init[6] = 1;
         }

/* Unknown command
   ***************/
         else
         {
             printf( "compress_cta: <%s> Unknown command in <%s>.\n",
                      com, configfile );
             continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "compress_cta: Bad <%s> command in <%s>. Exiting.\n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
       printf( "compress_cta: ERROR, no " );
       if ( !init[0] )  printf( "<MyModuleId> "    );
       if ( !init[1] )  printf( "<RingName> "      );
       if ( !init[2] )  printf( "<HeartBeatInt> "  );
       if ( !init[3] )  printf( "<InDir> "         );
       if ( !init[4] )  printf( "<OutDir> "        );
       if ( !init[5] )  printf( "<CompressClass> " );
       if ( !init[6] )  printf( "<CompressPrio> "  );
       printf( "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }
   return;
}


  /************************************************************************
   *  LogConfig() prints the config parameters in the log file.           *
   ************************************************************************/

void LogConfig( void )
{
   logit( "", "\nConfig File Parameters:\n" );
   logit( "", "   MyModuleId:    %s\n", MyModName );
   logit( "", "   RingName:      %s\n", RingName );
   logit( "", "   HeartBeatInt:  %d\n", HeartBeatInt );
   logit( "", "   InDir:         %s\n", InDir );
   logit( "", "   OutDir:        %s\n", OutDir );
   logit( "", "   CompressClass: %s\n", CompressClass );
   logit( "", "   CompressPrio:  %d\n", CompressPrio );
   return;
}


  /**********************************************************************
   *  LookupMsgType( )   Look up message types from earthworm.h tables  *
   **********************************************************************/

void LookupMsgType( void )
{
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "compress_cta: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "compress_cta: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   return;
}


  /**********************************************************************
   *  LookupMore( )   Look up more from earthworm.h tables              *
   **********************************************************************/

void LookupMore( void )
{
   if ( ( RingKey = GetKey(RingName) ) == -1 )
   {
        printf( "compress_cta:  Invalid ring name <%s>. Exiting.\n", RingName);
        exit( -1 );
   }
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "compress_cta: Error geting local installation id. Exiting.\n" );
      exit( -1 );
   }
   if ( GetModId( MyModName, &MyModId ) != 0 )
   {
      printf( "compress_cta: Invalid module name <%s>; Exiting.\n", MyModName );
      exit( -1 );
   }
   return;
}


/******************************************************************************
 * SendStatus() builds a heartbeat or error message & puts it into            *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/

void SendStatus( unsigned char type, short ierr, char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   long     t;

/* Build the message
   *****************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if ( type == TypeHeartBeat )
      sprintf( msg, "%ld %d\n", t, myPid );
   else if ( type == TypeError )
   {
      sprintf( msg, "%ld %hd %s\n", t, ierr, note );
      logit( "et", "compress_cta: %s\n", note );
   }
   else
      return;

   size = strlen( msg );           /* Don't include null byte in message */

/* Write the message to shared memory
   **********************************/
   if ( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if ( type == TypeHeartBeat )
           logit("et","compress_cta:  Error sending heartbeat.\n" );
        else
           if ( type == TypeError )
              logit("et","compress_cta:  Error sending error:%d.\n", ierr );
   }
   return;
}
