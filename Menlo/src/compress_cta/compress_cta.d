#
# This is compress_cta's parameter file
#
MyModuleId    MOD_COMPRESSCTA # Module id for this instance of compress_cta
RingName      WAVE_RING       # Shared memory ring for input/output
HeartBeatInt  30              # Seconds between heartbeats
CompressClass TS              # Class for compress process
CompressPrio  0               # Priority for compress process

# Uncompressed cta files are read here
InDir  /home/earthworm/run-will/ew2cta

# Compressed cta files are written here
OutDir /home/earthworm/run-will/compress_cta
