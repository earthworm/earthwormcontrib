
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <wait.h>
#include <unistd.h>

void SetPriority( pid_t, char *, int );


   /***********************************************************************
    *                            CompressFile()                           *
    *                                                                     *
    *         Compress a file using the Unix compress command.            *
    *                                                                     *
    *  Returns  0 = no error                                              *
    *          -1 = fork error                                            *
    *          -2 = execl error                                           *
    *          -3 = waitpid error                                         *
    *          -4 = compress terminated by signal                         *
    *          -5 = compress stopped by signal                            *
    *          -6 = unknown compress exit status                          *
    ***********************************************************************/

int CompressFile( char *fname,           /* Name of file to compress */
                  char *compressClass,   /* RT or TS */
                  int  compressPrio,     /* Process priority */
                  int  *stat )           /* Status code */
{
   pid_t pid;
   int   status;

/* Fork a child process to run the compress command.
   fork() forks all the threads in a process.
   fork1() forks only the calling thread.
   ************************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                          /* In child process */
         execlp( "compress", "compress", "-fv", fname, NULL );
         return -2;

      default:
         break;
   }

/* Set the class and priority of the compress process
   **************************************************/
   SetPriority( pid, compressClass, compressPrio );

/* Wait for the compress command to complete
   *****************************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( status ) )          /* compress exited */
   {
      *stat = WEXITSTATUS( status );
      return 0;
   }
   else if ( WIFSIGNALED( status ) )   /* compress terminated by signal */
   {
      *stat = WTERMSIG( status );
      return -4;
   }
   else if ( WIFSTOPPED( status ) )    /* compress stopped by signal */
   {
      *stat = WSTOPSIG( status );
      return -5;
   }
   return -6;                         /* Unknown exit status */
}
