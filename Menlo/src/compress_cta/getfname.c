
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <earthworm.h>


      /****************************************************************
       *                        GetFileName()                         *
       *                                                              *
       *  Get the name of a trace file with the earliest time.        *
       *  Trace files begin with "wu".                                *
       *                                                              *
       *  Returns fname, which contains the file name.                *
       *                                                              *
       *  Returns  0 if success                                       *
       *          -1 if no trace files were found                     *
       ****************************************************************/

int GetFileName( char fname[] )
{
   DIR    *dirp;
   struct dirent *dent;
   char   earliest[80];
   int    gotOne = 0;

   if ( (dirp = opendir(".")) == NULL )
   {
      logit( "e", "compress_cta: opendir failed.\n" );
      return -1;
   }

/* File name must begin with wu and not end with .Z
   ************************************************/
   while ( dent = readdir( dirp ) )
   {
      char *d_name = dent->d_name;

      if ( strncmp( d_name, "wu", 2 ) == 0 )
      {
         char *ptr = d_name + strlen(d_name) - 2;

         if ( strcmp( ptr, ".Z" ) != 0 )
         {
            if ( gotOne )
            {
               if ( strcmp( d_name, earliest ) < 0 )
                  strcpy( earliest, d_name );
            }
            else
            {
               strcpy( earliest, d_name );
               gotOne = 1;
            }
         }
      }
   }
   closedir( dirp );

   if ( gotOne )
   {
      strcpy( fname, earliest );
      return 0;                     /* Found a cta file */
   }
   else
      return -1;                    /* Couldn't find a cta file */
}
