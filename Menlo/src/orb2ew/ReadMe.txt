Yeah, I know, these aren't even EarthWorm modules.  They are Antelope 
modules that allow Antelopes and Earthworms to trade tracebufs. They are 
designed so that two different networks could exchange data in near-real-time 
even though one is principally an Antelope shop and the other is an Earthworm 
shop. The original routine, orb2eworm, written by Kent Lindquist, required one 
side or the other to run both Antelope and Earthworm. Transfer of translated data 
between sites could then be accomplished by either export/import or orb2orb.  To 
make life a little easier, Kent morphed orb2eworm into orb2ew and ew2orb.  
These new routines enfolded the Earthworm export/import functions into 
Antelope routines so that they could run on an Antelope system and 
communicate with an external Earthworm without having to run an internal 
Earthworm.  One could have alternatively created Earthworm modules that 
communicated directly with a remote Antelope's orb except that BRTT's orb 
communication routines are proprietary.  Kent's versions of these routines can be 
found at the Antelope User's Group website, http://www.indiana.edu/~aug/.  More 
specifically, at http://quakeinfo.geology.indiana.edu/cgi-
bin/cvsweb.cgi/contrib/bin/rt/ew2orb/.  Those routines were written before 
Earthworm recognized and used location codes.  These modifications 
accommodate location codes.

The executables are compiled and linked for Solaris 9.

March, 2005   Jim Luetgert

