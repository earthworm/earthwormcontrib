@echo off	
rem _______________________________________________________
rem |
rem |  backup-params.cmd
rem | 
rem |  Creates a zip file of the EW_PARAMS directory and  
rem |  copies it to backup space on campbell as ppicker.
rem |  You will probably need to type ppicker's password
rem |  to complete the file transfer.  But that's better 
rem |  than having to type all the commands too!
rem |
rem |  Set env variable THISHOST to be the name of this 
rem |  computer - in lower case!
rem | 
rem |  Requires these non-DOS commands:
rem |   c:\local\bin\date.exe  unix-style date command
rem |   pscp.exe               PuTTY's ssh-like scp command;
rem |                          must be in PATH
rem |
rem |  NOTE: % is the escape character for CMD scripts!
rem |______________________________________________________

setlocal
set THISHOST=hakone
cd %EW_PARAMS%
cd ..

echo Zipping up files in %EW_PARAMS%  ...

c:\local\bin\date.exe "+set ZIPFILE=%THISHOST%.params-20%%y%%m%%d.zip" > setzipfile.cmd
call setzipfile.cmd
rm   setzipfile.cmd

zip -9 -r %ZIPFILE% params

echo Copying %ZIPFILE% to campbell ...

pscp %ZIPFILE% ppicker@campbell:/ebird/backup/%THISHOST%/%ZIPFILE%
rm   %ZIPFILE%

endlocal
