#!/bin/perl
#
# a perlscript that tars a params directory 
# and copies it to campbell's backup space.

#  Run this script (or let crontab do it)

$homedir = "/home/earthworm/run";
$prog    = "backup-params.pl";
$machine = "ew04";

system("date '+%Y %m %d' >$homedir/date");
if(open(DATE, "$homedir/date")) {
	while(<DATE>) { $date = $_; }
	chomp($date);
	close(DATE);
}
system("rm $homedir/date");
@datepart = split (' ',$date);

$tarfile = "$machine.params-$datepart[0]$datepart[1]$datepart[2].tar";

system("rm $homedir/params/core");

chdir $homedir;
system("tar cvf $homedir/$tarfile params");

system("compress $homedir/$tarfile ");

system("scp $homedir/$tarfile.Z ppicker\@campbell:/ebird/backup/$machine");

system("rm $homedir/$tarfile.Z");



