#
# This is the helicont parameter file. This module gets data gulps
# from the waveserver(s), and creates helicorder displays.
 

#  Basic Earthworm setup:
#
LogSwitch       1                  # 0 to completely turn off disk log file
MyModuleId      MOD_HELICNT        # module id for this instance of report 
RingName        HYPO_RING          # ring to get input from
HeartBeatInt    15                 # seconds between heartbeats

wsTimeout 60 # time limit (secs) for any one interaction with a wave server.

# List of wave servers (ip port comment) to contact to retrieve trace data.
 WaveServer 130.118.43.11  16022      "wsv  - image1"
 WaveServer 130.118.43.12  16022      "wsv  - image2"
 WaveServer 128.32.149.134 16022      "wsv  - BK"

  @all_waveservers.d

# Directory in which to store the temporary .gif, .link and .html files.
GifDir   /home/earthworm/run-jim/gifs/helicont/  

# Directory in which to store the .gif files for output
 LocalTarget   /home/earthworm/run-jim/sendfiles/helicont/

# The file with all the station information.
# StationList     /home/earthworm/run/params/calsta.db

 StationList1     /home/earthworm/run-jim/params/calsta.db1  # new format

# Plot Display Parameters - 
		# The following is designed such that each SCN creates it's own
		# helicorder display; one per panel of data.
		
# 01 HoursPerPlot    Total number of hours per gif image
# 02 Minutes/Line    Number of minutes per line of trace
# 03 Plot Previous   On startup, retrieve and plot at least n previous hours from tank.
#                           (not used)
# 04 XSize           Overall size of plot in inches
# 05 YSize           Setting these > 100 will imply pixels
# 06 Show UTC        UTC will be shown on one of the time axes.
# 07 Use Local       The day page will be local day rather than UTC day.
#                                      
#        01  02 03 04  05 06 07 

 Display 24  30  0 1000  600  1  1  
# Display 24  15  0 10  20  1  1  
# Display  8  15  4 10 480  1  1  


#LabelDC       # Label each trace line with its dc offset.

# Channel Parameters - sorry it's so complex, but that's the price of versatility
		# The following is designed such that each SCN creates it's own
		# helicorder display; one per panel of data.
# S                  Site
# C                  Component
# N                  Network
# 04 Local Time Diff UTC - Local.  e.g. -7 -> PDT; -8 -> PST
# 05 Local Time Zone Three character time zone name.
# 06 Plot Previous   On startup, retrieve and plot n previous hours from tank.
#                           (not used)
# 07 Scale Factor    Scale factor to dress up image.
# 08 Mean Removal    Mean of 1st minute of each line will be removed.
# 09 Filter          Apply a generic lo-cut filter.
#                                      
#     S    C   N  04  05   06   07  08   09

 SCN  HJS  VHZ NC -8  PST   0  5.0   1    0   "John Smith Road"

#  Specify channels with location codes.
#  Wildcard (**) is accepted if you don't know the location code.
#                                     
#     S    C   N   L  04  05   06   07  08   09
 SCNL PMM  VHZ NC --  -8  PST   0  1.0   1    0   "Middle Mountain"
 SCNL JSB  VDZ NC **  -8  PST   0  1.0   1    0   "San Bruno Mountain"
 SCNL NHF  VDZ NC --  -8  PST   1  0.5   1    0   "Hamilton Field"
 SCNL YBH  HHZ BK 00  -8  PST   1  0.5   1    0   "YBH.00"
 SCNL YBH  HHZ BK 10  -8  PST   1  0.5   1    0   "YBH.10"

    # *** Optional Commands ***

# Filename prefix on target computer.  This is useful for identifying
# files for automated deletion via crontab.
Prefix nc

 UpdateInt     5  # Number of minutes between updates; default=2
 RetryCount    1  # Number of attempts to get a trace from server; default=2
 Logo    smusgs.gif   # Name of logo in GifDir to be plotted on each image

# Clip  200      # Clip the traces at N pixels.

# Gulp  300       # Specify number of seconds of data to acquire at a time.

# Normally, data is decimated before plotting to be roughly compatible with a
# 72 dpi pixel density.  The decimation factor is computed online based 
# upon the sample rate and plot scaling. This may be factor of 50-200. 
# If a true imitation of a helicorder plot is required, in which the
# "pen" overwrites itself, you can force the decimation to a smaller value.

# Decimate 1

  Minlines    2    # Minimum number of lines to leave empty at end of plot.
  
  PlotUp           # Plot from bottom to top.
# PlotDown         # Plot from top to bottom.

# If this flag is set, local time is automatically changed to/from Daylight
# Savings Time on the appropriate days for North America.
# If you use this feature, make sure that you have specified standard 
# time in SCN above.

  UseDST

# This option allows helicont to be run as a standalone module.
# Shared memory is not accessed and heartbeats are not sent or received.

# StandAlone      # Run helicont outside of earthworm.

# We accept a command "Debug" which turns on a bunch of log messages
# Debug
# WSDebug
