#!/usr/local/bin/perl

########################################################################
# This is the perl script which creates and drives the helicorder pages.
# There are 4 basic displays, each with its own subroutine.  Two of these
# have versions with and without javascript.  We make the assumption that
# browsers with version numbers >= 4 have javascript.
# 
#                                  Jim Luetgert   8/2000
########################################################################

# Define a bunch of global variables for consistency and ease of modification.

require "helicom.pl";

$lastmod = "March 26, 2002";
$infodir = "$other/recent/helicorders";
$quake   = "http://quake.wr.usgs.gov";
$self  = "/cgi-bin/helicont.pl";
$webdir = "/waveforms/helicont";
$datadir = "$other/waveforms/helicont";
$gifdir = "$rootdir"."$webdir";
$stationmap  = "$datadir/Station_map.gif";
$selectormap = "$gifdir/index_map.gif";
$select_list = "$gifdir/select.map";
$selectormap = "$datadir/index_map.gif";
$smwid  = "395";                   # Width and height of selector map
$smhgt  = "396";
$gwid  = "150";					# Width and height of thumbnails
$ghgt  = "300";
$gbwid  = "850";                   # Width and height of drum recorder plots
$gbhgt  = "1613";
$numpergroup = 4;
#$pagecolor = "#ffffcc";			# Colors of various page elements
#$bannercolor = "#669966";

# Common links for page headers
$mouseout = "\nonMouseOut=\"window.status=''; return true;\"";
$tag5 = "<br> \n ";
$tag6 = " <A HREF=\"$self\" onMouseOver=\"window.status='Select Different Data'; return true;\"    $mouseout> Data Selector </A> ||  \n ";
$onclick = "onClick=\"var new_window = window.open('$stationmap','mapwindow', 'width=400,height=400,resizable'); new_window.focus();\"";
$tag7 = " <A HREF=\"#\" onMouseOver=\"window.status='View Station Map'; return true;\"    $mouseout $onclick> Station Map </A> ||  \n ";
$onclick = "onClick=\"var new_window = window.open('$recenteqslist','listwindow', 'width=400,height=400,resizable,scrollbars'); new_window.focus();\"";
$tag8 = " <A HREF=\"#\" onMouseOver=\"window.status='Show List of Recent Earthquakes'; return true;\"    $mouseout $onclick> List of Recent Earthquakes </A> || \n ";
$onclick = "onClick=\"var new_window = window.open('$recenteqsmap','recentmapwindow', 'width=400,height=400,resizable,scrollbars'); new_window.focus();\"";
$tag9 = " <A HREF=\"#\" onMouseOver=\"window.status='View Recent Earthquake Map'; return true;\"    $mouseout $onclick> Index Map of Recent Earthquakes  || </A> \n ";
$tag10 = " <A HREF=\"$infodir/index.html\" onMouseOver=\"window.status='Description of the Drum Recorder Data'; return true;\"    $mouseout > About the Drum Recorders </A> <br> \n ";

# This is the default comment list sent by heli1 on startup
# Of form : JSF_VDZ_NC. At the Stanford dish.
if(open(NAMES, "$gifdir/znamelist.dat")) {
 	while(<NAMES>) {
		chomp;
		($sitename = $_) =~ s/([^.]*)\.([^.]*).*/$1/;
		($siteid = $_) =~ s/([^.]*)\.([^.]*).*/$2/;
		$longname{$sitename} = $siteid;
 	}
	close(NAMES);
}

# This is an optional custom comment list placed by the user.
if(open(NAMES, "$gifdir/zznamelist.dat")) {
 	while(<NAMES>) {
		chomp;
		($sitename = $_) =~ s/([^.]*)\.([^.]*).*/$1/;
		($siteid = $_) =~ s/([^.]*)\.([^.]*).*/$2/;
		$longname{$sitename} = $siteid;
 	}
	close(NAMES);
}

# Get some of the basic file information needed by all subroutines.
# Get the lists of available sites and datetimes
# Of Form: nc.JSF_VDZ_NC_00.2000051500.gif

	$list = `ls $gifdir *.gif`;
	@list = split (' ',$list);
	@reverselist = reverse(@list);
	@site = ();
	$numsites = 0;
	@dates = ();
	$numdates = 0;
	foreach $item (@list) {
		($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
		($filedate = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3/;
		($suffix = $item) =~ s/([^.]*)\.([^.]*)\.([^.]*)\.([^.]*).*/$4/;
		if($suffix eq "gif") {
			$flag = 1;
			foreach $name (@site) {
				if($sitename eq $name) { $flag = 0; }
			}
		    if($flag) { @site = (@site,$sitename); $numsites = $numsites + 1; }
			$flag = 1;
			foreach $idate (@dates) {
				if($filedate eq $idate) { $flag = 0; }
			}
		    if($flag) { @dates = (@dates,$filedate);  $numdates = $numdates + 1; }
		}
	}
	@dates = sort(@dates);
	@reversedates = reverse(@dates);

# Find the client's browser version so we know whether or not
# to send Javascript.
    $JavaOK = 1;       # Assume we can use javascript for time
    $ua = $ENV{'HTTP_USER_AGENT'};
    ($bv,$rest) = split ('\(',$ua);
    if($bv =~ /\//) {
        ($browser,$versionpt) = split ('/',$bv);
    }
    else {
        $versionpt = 3;
    }
    if($rest =~ /MSIE/) {$JavaOK = 0; }

    $version = $versionpt;
    if($versionpt =~ /\s/) {
        ($version,$rest) = split ('\s',$versionpt);
    }
#    $version = 3;
    if($version < 4) {$JavaOK = 0; }

# Check the query string for further instructions.
#
	$qname = "NO DATA AVAILABLE";
	$query_string = $ENV{'QUERY_STRING'};

    if ( ! defined($query_string) || $query_string eq "") {
       if($version >= 4) { &make_map; }
       else              { &make_index; }
    }
	else {
		$group = "$query_string";
		($querytype,$qvalue,$qdate) = split ('&',$query_string);
		
		$i = 0;
		$j = 0;
		foreach $name (@site) {
			$i = $i + 1;
			if($name =~ $qvalue) {
				$qname = $name;
				$qvalue = $i;
				$j = 1;
				last;
			}
		}
		if($j == 0 && $qvalue != 0) {
			$querytype = 5;
		}
	
   if($querytype eq 1) { &make_index; }
   if($querytype eq 2) { &show_date_thumbs; }
   if($querytype eq 3) { &show_single_panel;  }
   if($querytype eq 4) { &make_index; }
       if($querytype eq 5) { 
           if($version >= 4) { &make_map; }
           else              { &make_index; }
       }
}

exit;


########################################################################
# Construct and display the index of all available helicorders
# querytype = 4  (or really old browser)
########################################################################
sub make_index {

	$banner1 = "Welcome to the NCSN Drum Recorder Display";
	$banner2 = "Continuously Scrolling Real-time Views of Selected Seismograms";

	$tag1 = " ";
	$tag2 = " ";
	$tag3 = " ";
	$tag4 = " ";
	$tag5 = " ";
    $tag8 = " <A HREF=\"./heliLV.pl\" onMouseOver=\"window.status='View Seismograms from Long Valley, CA'; return true;\"    $mouseout> Long Valley Seismograms </A> || \n ";

    $title = "NCSN - Drum Recorder Index";

    &pre_Banner;
    
    print STDOUT "</HEAD> \n ";

    &Build_Banner;
    
    if($version < 4) {
        print STDOUT "<center><H2><P> \n";
        print STDOUT "Please update your browser to experience the full capabilities of these pages. <br>\n";
        print STDOUT "<A HREF=\"http://home.netscape.com/computing/download/index.html\" onMouseOver=\"window.status='Download new browser from Netscape'; return true;\"    $mouseout> Netscape </A> \n";
        print STDOUT "&nbsp &nbsp \n";
        print STDOUT "<A HREF=\"http://microsoft.com\" onMouseOver=\"window.status='Download new browser from Microsoft'; return true;\"    $mouseout> Explorer </A> \n";
        print STDOUT "</H2><HR></center> \n";
    }
    
    print STDOUT "<P><h4>\n";
        print STDOUT "The records shown below are recorded in real-time in Menlo Park, Ca. \n";
        print STDOUT "They are each updated on this page every five minutes. \n";
        print STDOUT "Each panel represents 24 hours of data from one station. \n";
        print STDOUT "The file sizes are approximately 100-200 kbytes.\n";
    print STDOUT "<br></h4>\n";
    	
	$i = 0;
	foreach $name (@site) {
		$i = $i + 1;
		$lname = $longname{$name};
		($sta,$comp,$net) = split ('_',$name);
		$inq = "1&$name&0";
		$wstatus = "View Thumbnails of $sta $comp $net for All Available Days";
		print STDOUT "<P><HR><P><h4>     ";
		print STDOUT "  $sta $comp $net \n";
		if($lname) {print STDOUT " ( $lname )  ";}
		foreach $item (@reverselist) {
			($suffix = $item) =~ s/([^.]*)\.([^.]*)\.([^.]*)\.([^.]*).*/$4/;
			if($suffix eq "gif") {
				($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
				if($sitename eq $name) {
					($datetime = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3_$4/;
					($date,$hr) = split ('_',$datetime);
					($datepart = $date) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
					($year,$mon,$day) = split ('_',$datepart);
					$inq = "3&$name&0";
					$wstatus = "View Data for $sta $comp $net";
					print STDOUT "  | <A HREF=\"$self\?$inq\"  \n";
					print STDOUT "  onMouseOver=\"window.status='$wstatus'; return true;\" \n";
					print STDOUT "  onMouseOut=\"window.status=''; return true;\"> \n";
					print STDOUT " <B>View It</B> </A> | \n";
				}
			}
		}
	}
	
	print STDOUT "<P><h4><HR>Thumbnail previews of all stations are available:</h4>";
	foreach $item (@reversedates) {
		($datepart = $item) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
		($year,$mon,$day) = split ('_',$datepart);
		$inq = "2&0&$item";
		$wstatus = "View Thumbnails of All Stations";
		print STDOUT " | <A HREF=\"$self\?$inq\"  \n";
		print STDOUT "  onMouseOver=\"window.status='$wstatus'; return true;\" \n";
		print STDOUT "  onMouseOut=\"window.status=''; return true;\"> \n";
		print STDOUT "  <B>HERE</B></A> | \n";
	}

    &post_content;

return;

}

########################################################################
# Display thumbnails of spectrograms for all sites
# querytype = 2
########################################################################
sub show_date_thumbs {

    $banner1 = "Northern California Seismic Network";
	$banner2 = "Drum Recorder Displays";
    $this = "20020202";
	
# Tags to go elsewhere
	
    $tag1 = " ";
    $tag2 = " ";
    $tag3 = " ";
    $tag4 = " ";

    $title = "Northern California Seismic Network - Drum Recorders";

    &pre_Banner;
    
    print STDOUT "</HEAD> \n ";

    &Build_Banner;
    
    print STDOUT "<P><h4>\n";
        print STDOUT "The records shown below are recorded in real-time in Menlo Park, Ca. \n";
        print STDOUT "They are each updated on this page every five minutes. \n";
        print STDOUT "Each panel represents 24 hours of data from one station. \n";
        print STDOUT "The file sizes are approximately 100-200 kbytes.\n";
    print STDOUT "<br></h4>\n";
    
    print STDOUT "<center><P><h4><FONT COLOR=red>\n";
        print STDOUT "Click on a seismogram for full size display.\n";
    print STDOUT "</FONT></h4></center>\n";

	$count = 0;
	foreach $item (@list) {
		($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
		($filedate = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3/;
		($suffix = $item)   =~ s/([^.]*)\.([^.]*)\.([^.]*)\.([^.]*).*/$4/;
		if($suffix eq "gif") {
			if($filedate eq $this) {
				$displayitem[$count] = $item; 
				$displayname[$count] = $sitename; 
				$count = $count + 1;
			}
		}
	}

    print STDOUT "<HR><center>";
	print STDOUT " <table width=\"75\%\" bordercolorlight=$pagecolor bordercolordark=$pagecolor>\n";
	$j = $count/4;
	for($jj=0; $jj<$j; $jj++) {
		$kk = $count - $jj*4;
		if($kk > 4) {
			$kk = 4;
		}
		print STDOUT "<tr>\n";
		for($i=0; $i<$kk;$i++) {
			$ii = $jj*4 + $i;
			($sta,$comp,$net) = split ('_',$displayname[$ii]);
			print STDOUT "  <td><center>\n <B>$sta $comp $net</B> \n </center></td> \n";
		}
		print STDOUT "</tr>\n";
		print STDOUT "<tr>\n";
		for($i=0; $i<$kk;$i++) {
			$ii = $jj*4 + $i;
			$iii = $ii + 1;
			($sta,$comp,$net) = split ('_',$displayname[$ii]);
			$inq = "3&$displayname[$ii]&0";
			$wstatus = "$sta $comp $net";
			print STDOUT "<td><center>\n <A HREF=\"$self\?$inq\" \n ";
			print STDOUT "  onMouseOver=\"window.status='$wstatus'; return true;\" \n";
			print STDOUT "  onMouseOut=\"window.status=''; return true;\"> \n";
            print STDOUT "  <IMG SRC=\"$datadir/$displayitem[$ii]\" WIDTH=$gwid HEIGHT=$ghgt></A></center></td>\n ";
        }
        print STDOUT "</tr>\n";
        print STDOUT "<tr>\n";
        print STDOUT " <td>&nbsp;</td>\n <td>&nbsp;</td>\n <td>&nbsp;</td>\n <td>&nbsp;</td>\n";
        print STDOUT "</tr>\n";
    }
    print STDOUT "</table>\n";
    print STDOUT "</td></center> \n ";

    &post_content;
    
return;

}

########################################################################
# Display a Drum Recorder for one day
# querytype = 3
# $qvalue  is index to array of site names
########################################################################
sub show_single_panel {

# Verify that this data is available.
    
    $i = 0;
    $count = 0;
    foreach $name (@site) {
        $i = $i + 1;
        if($i eq $qvalue) { 
            foreach $item (@reverselist) {
                ($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
                if($sitename eq $name) {
                    ($filedate = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3/;
                    ($datetime = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3_$4/;
                    ($date,$hr) = split ('_',$datetime);
                
                    ($datepart = $date) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
                    $displayitem[$count] = $item; 
                    $displaydate[$count] = $datepart; 
                    $count = $count + 1;
                    
                        $currentitem = $item;
                        $currentname = $name;
                    
                }
            }
            last; 
        }
    }
    $lname = $longname{$qname};
    ($sta,$comp,$net) = split ('_',$qname);
	$channel = "$sta $comp $net";
    
    if($lname) {
		$banner1 = $channel;
        $banner2 = " ( $lname )  ";
    } else {
        $banner1 = "Northern California Seismic Network";
		$banner2 = $channel;
    }

# Tags to go elsewhere
    
    $tag1 = " ";
    $tag2 = " ";

    $psite = $qvalue - 1;
    if($psite < 1) { $psite = $numsites; }
    $pname = $site[$psite-1];
    ($sta,$comp,$net) = split ('_',$pname);
    $inq  = "3&$pname&0";
    $tag3 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='Data for $sta $comp $net'; return true;\" $mouseout>Previous Site ($sta $comp $net)</A> || ";
    
    $nsite = $qvalue + 1;
    if($nsite > $numsites) { $nsite = 1; }
    $nname = $site[$nsite-1];
    ($sta,$comp,$net) = split ('_',$nname);
    $inq  = "3&$nname&0";
    $tag4 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='Data for $sta $comp $net'; return true;\" $mouseout>Next Site ($sta $comp $net)</A>  ";

    $title = "Northern California Seismic Network - Drum Recorders";
	
    &pre_Banner;
    
        print STDOUT "<META http-equiv=\"Refresh\" content=\"300; URL=$self\?$query_string\"> ";
    
    
    print STDOUT "</HEAD> \n ";

    &Build_Banner;
    
    if($currentitem) {	    
	    print STDOUT "<center>";
	    print STDOUT "<A name=\"anchor1\"   \n>";
	    print STDOUT "<IMG SRC=\"$datadir/$currentitem\" WIDTH=$gbwid HEIGHT=$gbhgt>  \n";
	    print STDOUT "</a>";

	    print STDOUT "</center> \n ";
    } else {
	    print STDOUT "<center> <h2>";
	    print STDOUT "No data available for $channel!";
	    print STDOUT "</td></center> \n ";
    }

    &post_content;

return;

}

########################################################################
# Display map for selection of helicorders
# querytype = 5
########################################################################
sub make_map {

	$banner1 = "Welcome to the NCSN Drum Recorder Display";
	$banner2 = "Continuously Scrolling Real-time Views of Selected Seismograms";
    $banner1 = "Northern California Drum Recorders";
    $banner2 = "Data Selector";
    $banner1 = "Data Selector";
    $banner2 = "";
    
    $tag5 = " ";
    $tag6 = " <A HREF=\"/cgi-bin/heliLV.pl\" onMouseOver=\"window.status='View Seismograms from Long Valley, CA'; return true;\"    $mouseout> Long Valley Seismograms </A> || ";

    $title = "NCSN - Drum Recorder Data Selector";
   
    &pre_Banner;

print STDOUT <<EOT;
    <SCRIPT language="JavaScript">

        <!--

        function buildLink(stationid)
        {
            var the_sel_site = stationid;
            
        // Asking for all stations
            if (the_sel_site == 99) {
                    var newlink = "$self?2&0";
                window.location = newlink; 
            }
        // Asking for selected station
            else {
                var newlink = "$self?3&" + the_sel_site + "&0";
                window.location = newlink;
            }
        }

        function statusBar(stationid)
        {
            window.status =  stationid;
        }

        function newstatusBar(stationid)
        {
            window.status =  stationid  + " " + window.event.x;
        }


        if(!window.event && window.captureEvents) {
        // set up event capturing for mouse events (add or subtract as desired)
            window.captureEvents(Event.MOUSEOVER|Event.MOUSEOUT|Event.CLICK|Event.DBLCLICK);
        // set window event handlers (add or subtract as desired)
            window.onmouseover = WM_getCursorHandler;
            window.onmouseout = WM_getCursorHandler;
            window.onclick = WM_getCursorHandler;
            window.ondblclick = WM_getCursorHandler;
        // create an object to store the event properties 
            window.event = new Object;
        }

        function WM_getCursorHandler(e) {
        // set event properties to global vars (add or subtract as desired)
            window.event.clientX = e.pageX;
            window.event.clientY = e.pageY;
            window.event.x = e.layerX;
            window.event.y = e.layerY;
            window.event.screenX = e.screenX;
            window.event.screenY = e.screenY;
        // route the event back to the intended function
            if ( routeEvent(e) == false ) {
                return false;
            } else {
                return true;
            }
        }

        //-->

    </SCRIPT>
    </HEAD>

EOT

    &Build_Banner;


print STDOUT <<EOT;

    <center><p><font size="4"><b>Select a station.
    </center>

<!-- Now open $select_list -->
EOT

	if(open(FROM, "$select_list")) {
		while(<FROM>) {
			chomp;
			print STDOUT "$_\n";
		}
		close FROM;
	} else {
		print STDOUT " \n <!-- Couldn't open $select_list -->\n\n";
	}

    &post_content;

return;
}

########################################################################
# Build the html that directly wraps the content
########################################################################
sub pre_Banner {

print STDOUT <<EOT;
Content-type: text/html

    <HTML>
    <HEAD><TITLE>
    $title
    </TITLE>
    <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <META NAME="KeyWords" CONTENT="earthquake, earthquakes, united states, san francisco, california, 
    nevada, epicenter, fault, wave, seismicity, seismic, foreshock, aftershock, tremor, magnitude, 
    magnitude scale, intensity, intensity scale, seismogram, seismograph, seismologist, seismology, 
    geophysics, geologist, subduction, tsnuamis, hypocenter, quake, quakes, helicorder, drum recorder, 
    webicorder, network">

    <!-- Load external style sheet for this site --> 
    <LINK rel="stylesheet" type="text/css" href="/all-other.css">
EOT
return;

}

########################################################################
# Build the generic banner for a page.
########################################################################
sub Build_Banner {

print STDOUT <<EOT;

    <BODY bgcolor=$pagecolor TEXT=#000000 topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" 
       marginwidth="0" marginheight="0" link=$link vlink=$vlink alink=$alink>

<!-- New USGS/EHZ Banner -->


<TABLE width="600" border="0" cellspacing="0" cellpadding="2">
  <TR> 
    <TD> 
      <TABLE border="0" cellspacing="0" cellpadding="0">
        <TR>
          <TD><A href="http://www.usgs.gov/"><IMG src="/images/banner2-usgs_sml.gif" width="600" height="38" border="0"></A></TD>
        </TR>
        <TR>
          <TD><A href="/"><IMG src="/images/banner2-ehz_sml.gif" width="600" height="25" border="0"></A></TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>


<!-- USGS/EHZ Banner

    <TABLE width="600" border="0" cellspacing="0" cellpadding="2">
      <TR> 
        <TD>
          <TABLE border="0" cellspacing="0" cellpadding="0">
            <TR> 
              <TD><A href="http://www.usgs.gov/"><IMG src="/images/banner-usgs.gif" width="480" height="73" border="0" alt="USGS Banner"></A></TD>
            </TR>
            <TR>
              <TD><A href="http://earthquake.usgs.gov/"><IMG src="/images/banner-ezh.gif" width="480" height="22" border="0" alt="Earthquake Team Banner"></A></TD>
            </TR>
          </TABLE>
        </TD>
      </TR>
    </TABLE> -->

<!-- Begin Primary Horizontal Navbar -->

<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
  <TR> 
    <TD width="1%">&nbsp;</TD>
    <TD width="99%">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TR> 
          <TD width="1%" background="/images/blugrdnt.gif">&nbsp;</TD>
          <TD width="99%">
            <TABLE border="0" cellspacing="0" cellpadding="3">
              <TR> 
                <TD bgcolor="#003399"> 
                  <DIV align="center">
                  <A href="/recent/index.html" class="nav-selected">
                  <FONT color="#CCCCFF">
                  Latest Quake Info</FONT></A></DIV>
                </TD>
                <TD bgcolor="#003399" background="/images/tab-right.gif">&nbsp;</TD>
                <TD bgcolor="#000000"> 
                  <DIV align="center">
                  <A href="/info/basics.html" class="navbar">
                  <FONT color="#CCCCCC"> 
                  General Quake Info</FONT></A></DIV>
                </TD>
                <TD bgcolor="#000000" background="/images/tabs.gif">&nbsp;</TD>
                <TD bgcolor="#000000"> 
                  <DIV align="center">
                  <A href="/prepare/hazards.html" class="navbar">
                  <FONT color="#CCCCCC"> 
                   Hazards &amp; Preparedness</FONT></A></DIV>
                </TD>
                <TD bgcolor="#000000" background="/images/tabs.gif">&nbsp;</TD>
                <TD bgcolor="#000000"> 
                  <DIV align="center">
                  <A href="/research/index.html" class="navbar">
                  <FONT color="#CCCCCC"> 
                   Earthquake Research</FONT></A></DIV>
                </TD>
                <TD bgcolor="#000000" background="/images/tabs.gif">&nbsp;</TD>
                <TD bgcolor="#000000"> 
                  <DIV align="center">
                  <A href="/features/index.html" class="navbar">
                  <FONT color="#CCCCCC"> 
                   Special Features</FONT></A></DIV>
                </TD>
                <TD bgcolor="#000000" background="/images/tabs.gif">&nbsp;</TD>
                <TD bgcolor="#000000"> 
                  <DIV align="center">
                  <A href="/resources/index.html" class="navbar">
                  <FONT color="#CCCCCC"> 
                   Additional Resources</FONT></A></DIV>
                </TD>
                <TD bgcolor="#000000" background="/images/tabs.gif">&nbsp;</TD>
                <TD bgcolor="#000000"> 
                  <DIV align="center">
                  <A href="/site/search.html" class="navbar">
                  <FONT color="#CCCCCC">
                  Search</FONT></A></DIV>
                </TD>
                <TD background="/images/tab-end.gif">&nbsp;</TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#003399">
        <TR> 
          <TD width="1%" background="/images/blugrdnt.gif">&nbsp;</TD>
          <TD width="99%"> 
            <TABLE border="0" cellspacing="0" cellpadding="3" width="100%">
              <TR> 
                <TD class="locationbar">
                  <FONT color="#FFFFFF"><I>You are here: </I></FONT>
                  <A href="/index.html" class="loc-links"><FONT color="#CCCCFF">
                     Home</FONT></A>
                  <IMG src="/images/arrow-loc.gif" width="10" height="10" alt="&gt;">
                  <A href="/recent/index.html" class="loc-links"><FONT color="#CCCCFF">
                     Latest Quake Info</FONT></A>
                  <IMG src="/images/arrow-loc.gif" width="10" height="10" alt="&gt;">
                  <A href="/recent/helicorders.html" class="loc-links"><FONT color="#CCCCFF">
                     Real-time Seismogram Displays</FONT></A>
                  <IMG src="/images/arrow-loc.gif" width="10" height="10" alt="&gt;">
                  <FONT color="#FFFFFF">Northern California Seismic Network</FONT>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000" height="1">
        <TR> 
          <TD width="1%" background="/images/blugrdnt.gif">
          <IMG src="/images/clear.gif" width=1 height=1></TD>
          <TD width="99%" bgcolor="#003366">
          <IMG src="/images/clear.gif" width=1 height=1></TD>
        </TR>
        <TR> 
          <TD width="1%" background="/images/blkgrdnt.gif">
          <IMG src="/images/clear.gif" width=1 height=1></TD>
          <TD width="99%" bgcolor="#00001A">
          <IMG src="/images/clear.gif" width=1 height=1></TD>
        </TR>
      </TABLE>
    </TD>
  </TR>
</TABLE>

<!-- Put in local banner and links -->

EOT

    &pre_content;

    print STDOUT "<table width=\"75%\" border=\"1\" align=\"center\" bgcolor=$bannercolor bordercolordark=\"#000000\"> \n";
      if($banner1) {
      print STDOUT "<tr bgcolor=$bannercolor> \n";
        print STDOUT "<td height=\"47\"> \n";
          print STDOUT "<center> \n";
            print STDOUT "<font face=\"Times New Roman, Times, serif\" size=\"6\" color=$bannertxtcolor> \n";
              print STDOUT "$banner1 \n";
            print STDOUT "</font> </center> \n";
        print STDOUT "</td> \n";
      print STDOUT "</tr> \n";
      }
      if($banner2) {
      print STDOUT "<tr bgcolor=$bannercolor> \n";
        print STDOUT "<td height=\"34\"> \n";
          print STDOUT "<center> \n";
            print STDOUT "<font face=\"Times New Roman, Times, serif\" size=\"4\" color=$bannertxtcolor> \n";
              print STDOUT "$banner2 \n";
            print STDOUT "</font> </center> \n";
        print STDOUT "</td> \n";
      print STDOUT "</tr> \n";
      }
    print STDOUT "</table> \n";
        
    print STDOUT "<center><h4> \n";
    print STDOUT "$tag1 \n $tag2 \n $tag3 \n $tag4 \n $tag5 \n $tag6 \n $tag7 \n $tag8 \n $tag9 \n $tag10 \n";
    print STDOUT "</h4></center> \n";
    print STDOUT "<P> <HR> <P> \n";

return;

}

########################################################################
# Build the html that directly wraps the content
########################################################################
sub pre_content {

print STDOUT <<EOT;

<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
  <TR>
    <TD width="2%">
      <IMG src="/images/clear.gif" width="20" height="10" alt="spacer">
    </TD>
    <TD valign="top" width="100%" bgcolor="#FFFFFF">


<!-- Begin Page Content here --> 


      <TABLE width="100%" border="0" cellspacing="0" cellpadding="12">
        <TR>
          <TD class="bodytext">
          
EOT
return;

}

########################################################################
# Build the html that finishes wrapping the content and does the footer
########################################################################
sub post_content {

print STDOUT <<EOT;

</TABLE>
          </TD>
        </TR>
      </TABLE>


<!-- End Page Content -->

<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
  <TR>
    <TD width="2%">
      <IMG src="/images/clear.gif" width="20" height="10" alt="spacer">
    </TD>
    <TD valign="top" width="100%" bgcolor="#FFFFFF">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="12">
        <TR align="center" bgcolor="#FFFFFF"> 
    <TD>
    <hr>
    <center><h4> $tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8 $tag9 $tag10 </h4></center>
    </TD>
        </TR>
      </TABLE>
        </TR>
      </TABLE>

    </TD>
  </TR>
  <TR>
    <TD></TD>
    <TD bgcolor="#000000">


<!-- Begin Secondary Horizontal Navbar -->


      <TABLE width="100%" border="0" cellspacing="0" cellpadding="2">
        <TR align="center" bgcolor="#000000"> 
          <TD class="navbar2">
            <A href="/index.html" class="navbar2"><FONT color="#CCCCCC">
              Homepage</FONT></A>
            <B> &nbsp;|&nbsp;</B> 
            <NOBR><A href="/site/index.html" class="navbar2"><FONT color="#CCCCCC">
              Site Index</FONT></A></NOBR>
            <B> &nbsp;|&nbsp;</B> 
            <NOBR><A href="/program/contact.html" class="navbar2"><FONT color="#CCCCCC">
              Contact Us</FONT></A></NOBR>
            <B> &nbsp;|&nbsp;</B> 
            <NOBR><A href="/program/index.html" class="navbar2"><FONT color="#CCCCCC">
              About Us</FONT></A></NOBR>
            <B> &nbsp;|&nbsp;</B> 
            <NOBR><A href="/recent/news-current.html" class="navbar2"><FONT color="#CCCCCC">
              USGS Earthquake News Releases</FONT></A></NOBR>
          </TD>
        </TR>
      </TABLE>


<!-- End Secondary Horizontal Navbar -->


    </TD>
  </TR>


<!-- Footer -->

<TABLE width="100%" border="0" cellspacing="0" cellpadding="8">
  <TR> 
    <TD class="footer">
      U.S. Geological Survey, Earthquake Hazards Program<BR>
      URL http://quake.wr.usgs.gov/cgi-bin/helicorder.pl<BR>
      Contact:<A href="mailto:webmaster\@ehznorth.wr.usgs.gov">webmaster\@quake.wr.usgs.gov</A><BR>
      Last modification: <!-- #BeginDate format:Am1 -->$lastmod<!-- #EndDate --> 
    </TD>
  </TR>
</TABLE>

</BODY>
</HTML>
          
EOT
return;

}


########################################################################
