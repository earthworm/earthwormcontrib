
/*
 * decimate.c
*/

#include <string.h>
#include <trace_buf.h>
#include "cuspfeeder.h"


  /*************************************************************************
   *                             DecimateMsg()                             *
   *                                                                       *
   *  Decimate one tracebuf message to a target frequency.                 *
   *                                                                       *
   *  Accepts:                                                             *
   *     firstSamp = First sample of message to save.                      *
   *     di        = Decimation interval.                                  *
   *                                                                       *
   *  Returns:                                                             *
   *     firstSamp = First sample of next message to save, if no gap.      *
   *     oldLen is the message length in bytes, before decimation.         *
   *     newLen is the message length in bytes, after decimation.          *
   *                                                                       *
   *  Function return value:                                               *
   *     -1 if bytesPerSamp is invalid (not 2 or 4)                        *
   *      0 if all went ok.                                                *
   *************************************************************************/

int DecimateMsg( char *msg, int di, int *firstSamp, int *oldLen, int *newLen )
{
   TRACE_HEADER *th = (TRACE_HEADER *)msg;
   int   i;
   int   j = 0;
   short *d2 = (short *)(msg + sizeof(TRACE_HEADER));
   long  *d4 = (long  *)(msg + sizeof(TRACE_HEADER));
   int   fs = *firstSamp;
   int   bytesPerSamp = th->datatype[1] - 48;

/* Check for valid bytesPerSamp
   ****************************/
   if ( (bytesPerSamp != 2 ) && (bytesPerSamp != 4) )
   {
      logit( "", "Invalid bytesPerSamp: %d\n", bytesPerSamp );
      return -1;
   }

/* Calculate number of bytes in original message
   *********************************************/
   *oldLen = sizeof(TRACE_HEADER) + (bytesPerSamp * th->nsamp);

/* No useful samples in this message
   *********************************/
   if ( fs >= th->nsamp )
   {
      *firstSamp -= th->nsamp;
      *newLen     = 0;
      return 0;
   }

/* Decimate the message
   ********************/
   for ( i = fs; i < th->nsamp; i += di )
   {
      if ( bytesPerSamp == 2 )
         d2[j] = d2[i];
      else
         d4[j] = d4[i];
      j++;
   }

/* Calculate the message header and return values
   **********************************************/
   th->starttime += fs / th->samprate;
   *firstSamp     = i - th->nsamp;
   th->nsamp      = j;
   *newLen        = sizeof(TRACE_HEADER) + (bytesPerSamp * th->nsamp);
   th->samprate  /= (double)di;
   th->endtime    = th->starttime + ((j - 1) / th->samprate);
   return 0;
}


  /*************************************************************************
   *                            DecimateTrace()                            *
   *                                                                       *
   *  Decimate a sequence of tracebuf messages to a target frequency.      *
   *  The tracebuf messages are guaranteed to have the same SCN, but       *
   *  there may be gaps between the messages.  The tracebuf array is       *
   *  modified in place.                                                   *
   *                                                                       *
   *  Accepts:                                                             *
   *     dsmp     = Pointer to decimation work buffer                      *
   *     tracebuf = Array containing sequence of messages for one SCN,     *
   *                before decimation.                                     *
   *     actLen   = Combined length of tracebuf messages, in bytes,        *
   *                before decimation.                                     *
   *                                                                       *
   *  Returns:                                                             *
   *     tracebuf = Array containing sequence of messages for one SCN,     *
   *                after decimation.                                      *
   *     actLen   = Combined length of tracebuf messages, in bytes,        *
   *                after decimation.                                      *
   *                                                                       *
   *  Function return value:                                               *
   *     -1 on error                                                       *
   *      0 otherwise                                                      *
   *************************************************************************/

int DecimateTrace( double *dsmp, char *tracebuf, long *actLen )
{
   TRACE_HEADER *th = (TRACE_HEADER *)tracebuf;
   int    i;
   int    di = 1;                   /* Decimation interval */
   extern int  nDecInt;             /* Number of known decimation intervals */
   extern DECINT DecInt[MAX_DEC];   /* Array of decimation intervals */
   char   outMsg[MAX_TRACEBUF_SIZ]; /* Decimated trace buffer */

#ifdef JUNK
   int inLen     = sizeof(TRACE_HEADER);
   int outLen    = 0;
   int firstSamp = 0;              /* First sample to save in message */
   int first     = 1;              /* 1 if first msg for this trace */
                                   /*   or if there is a gap */
   int workLen   = 0;
   double endtime;                 /* Time of last sample in epoch seconds */
   char   *oldptr = tracebuf;      /* Pointer to undecimated messages */
   char   *newptr = tracebuf;      /* Pointer to decimated messages */
#endif

/* See if data with this sample rate should be decimated.
   If data sample rate is within 1% of configured sample
   rate, use the specified decimation interval.
   *****************************************************/
   for ( i = 0; i < nDecInt; i ++ )
   {
      double fr = (th->samprate - DecInt[i].freq) / DecInt[i].freq;
      double absfr = (fr < 0.0) ? -fr : fr;

      if ( absfr < 0.01 )
      {
         di = DecInt[i].decRate;
         break;
      }
   }
   if ( di < 2 ) return 0;                 /* No decimation */

/* Initialize pointers into the input trace buffer
   ***********************************************/

/* Get "filter length" samples from the input trace buffer
   *******************************************************/

/* Loop through all messages with this SCN
   ***************************************/
/* while ( inLen < *actLen )
   {
      int oldMsgLen;
      int newMsgLen;

      if ( !first )
      {
         double gap_samp = th->samprate * (th->starttime - endtime);
         int    nLost    = (int)(gap_samp + 0.5) - 1; */

/* If an overlap is found in the undecimated data, skip the channel
   ****************************************************************/
/*       if ( nLost < 0 )
         {
            logit( "e", "cuspfeeder: Error. Data overlap. SCN: %s %s %s",
                   th->sta, th->chan, th->net );
            logit( "e", " Skipping channel.\n" );
            return -1;
         } */

/* Readjust firstSamp to compensate for a gap in the data.
   (index of first sample to save in current message)
   ******************************************************/
/*       firstSamp -= nLost;
         if ( firstSamp < 0 )
            firstSamp = 0;
      }
      endtime = th->endtime; */

/* Decimate one tracebuf message
   *****************************/
/*    if ( DecimateMsg( oldptr, di, &firstSamp, &oldMsgLen, &newMsgLen ) == -1 )
         return -1; */

/* Remove extra space from the trace data
   **************************************/
/*    memmove( newptr, oldptr, newMsgLen );

      oldptr += oldMsgLen;
      inLen  += oldMsgLen;
      newptr += newMsgLen;
      outLen += newMsgLen;

      first = 0;
   }
   *actLen = outLen; */

   return 0;
}
