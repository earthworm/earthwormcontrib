
        /***************************************************************
         *                         config.c                            *
         *                                                             *
         *  Functions to read and log the configuration files.         *
         ***************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <transport.h>
#include <kom.h>
#include "cuspfeeder.h"

/* Functions in this source file
   *****************************/
void GetConfig( char * );
void LogConfig( void );
void Lookup( void );

/* Share these with the sender thread
   **********************************/
volatile char LocalTriggerDir[MAX_STRLEN]; /* For temporary storage of trigger files */
volatile char LocalWaveformDir[MAX_STRLEN];/* For local storage of waveform files */
volatile char RemoteIpAddress[MAX_STRLEN]; /* IP address of system to receive trigger */
volatile int  WellKnownPort;          /* The well-known port number */
volatile int  RetryInterval;          /* If send fails, retry this often (msec) */
volatile int  nWsv = 0;               /* Number of waveservers we know about */
volatile WSV  Wsv[MAX_WSV];           /* Waveserver IP address and port numbers */
volatile long wsTimeout;              /* Milliseconds to wait for reply from ws */
volatile int  SendAllComponents;      /* If nonzero, send all components for each station */
volatile int  MaxWait;                /* Max time in sec to wait for waveforms */
volatile int  ChunkDelay;             /* Set larger than maximum message length */
volatile int  nDec = 0;               /* Number of known decimation intervals */
volatile DECSTR Dec[MAX_DEC];         /* Array of decimation intervals */
volatile int  TraceBufferSize;        /* Size of trace buffer in bytes */
volatile int  WaveformDest=DEST_CUSPONLY;  /* Where to ship final waveform file */

char     RingName[20];           /* Name of transport ring for i/o */
char     MyModName[20];          /* Speak as this module name/id */
long     HeartBeatInterval;      /* Seconds between heartbeats */
short    nLogo = 0;              /* Number of logo types to get from ring */
MSG_LOGO GetLogo[MAX_LOGO];      /* Array for module,type,instid */

/* Look these up in the earthworm.h tables with getutil functions
   **************************************************************/
long          RingKey;           /* Key of transport ring for i/o */
unsigned char InstId;            /* Local installation id */
unsigned char MyModId;           /* Module Id for this program */
unsigned char TypeHeartBeat;
unsigned char TypeError;
unsigned char TypeTriglist;


/****************************************************************************
 *  GetConfig() processes command file(s) using kom.c functions;            *
 *                    exits if any errors are encountered.                  *
 ****************************************************************************/

#define NCOMMAND 14

void GetConfig( char *configfile )
{
   const    ncommand = NCOMMAND;    /* Process this many required commands */
   char     init[NCOMMAND];         /* Init flags, one for each command */
   int      nmiss;                  /* Number of missing commands */
   char     *com;
   char     *str;
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ ) init[i] = 0;
   LocalWaveformDir[0] = '\0';

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
        logit( "e", "cuspfeeder: Error opening command file <%s>. Exiting.\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         com = k_str();         /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )           continue;
         if ( com[0] == '#' )  continue;

/* Open a nested configuration file
   ********************************/
         if( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               logit( "e", "cuspfeeder: Error opening command file <%s>. Exiting.\n",
                        &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         else if ( k_its("MyModuleId") )
         {
            str = k_str();
            if (str) strcpy( MyModName, str );
            init[0] = 1;
         }
         else if ( k_its("RingName") )
         {
            str = k_str();
            if (str) strcpy( RingName, str );
            init[1] = 1;
         }
         else if ( k_its("HeartBeatInterval") )
         {
            HeartBeatInterval = k_long();
            init[2] = 1;
         }
         else if ( k_its("GetTrigsFrom") )
         {
            if ( nLogo+1 > MAX_LOGO )
            {
               logit( "e", "cuspfeeder: Too many <GetTrigsFrom> commands in <%s>",
                        configfile );
               logit( "e", "; max=%d. Exiting.\n", (int) MAX_LOGO );
               exit( -1 );
            }
            if ( ( str=k_str() ) )
            {
               if ( GetInst( str, &GetLogo[nLogo].instid ) != 0 )
               {
                  logit( "e", "cuspfeeder: Invalid installation name <%s>", str );
                  logit( "e", " in <GetTrigsFrom> cmd. Exiting.\n" );
                  exit( -1 );
               }
            }
            if ( ( str=k_str() ) )
            {
               if ( GetModId( str, &GetLogo[nLogo].mod ) != 0 )
               {
                  logit( "e", "cuspfeeder: Invalid module name <%s>", str );
                  logit( "e", " in <GetTrigsFrom> cmd. Exiting.\n" );
                  exit( -1 );
               }
            }
            if ( GetType( "TYPE_TRIGLIST2K", &GetLogo[nLogo].type ) != 0 )
            {
               logit( "e", "cuspfeeder: Invalid message type <TYPE_TRIGLIST2K>" );
               logit( "e", ". Exiting.\n" );
               exit( -1 );
            }
            nLogo++;
            init[3] = 1;
         }
         else if ( k_its("LocalTriggerDir") )
         {
            str = k_str();
            if (str && strlen(str)<MAX_STRLEN) strcpy( (char *)LocalTriggerDir, str );
            else {
               logit( "e", "cuspfeeder: Bad LocalTriggerDir command (maxlen=%d)."
                          " Exiting.\n", MAX_STRLEN-1 );
               exit( -1 );
            }
            init[4] = 1;
         }
         else if ( k_its("RemoteIpAddress") )
         {
            str = k_str();
            if (str && strlen(str)<MAX_STRLEN) strcpy( (char *)RemoteIpAddress, str );
            else {
               logit( "e", "cuspfeeder: Bad RemoteIpAddress command (maxlen=%d)."
                          " Exiting.\n", MAX_STRLEN-1 );
               exit( -1 );
            }
            init[5] = 1;
         }
         else if ( k_its("WellKnownPort") )
         {
            WellKnownPort = k_int();
            init[6] = 1;
         }
         else if ( k_its("RetryInterval") )
         {
            RetryInterval = k_int();
            init[7] = 1;
         }
         else if ( k_its("Waveserver") )
         {
            if ( nWsv+1 > MAX_WSV )
            {
               logit( "e", "cuspfeeder: Too many <Waveserver> commands in <%s>", configfile );
               logit( "e", "; max=%d. Exiting.\n", (int) MAX_WSV );
               exit( -1 );
            }
            str = k_str();
            if ( str )
               strcpy( (char *)Wsv[nWsv].ip, str );
            else
            {
               logit( "e", "cuspfeeder: Can't get a waveserver IP address. Exiting.\n" );
               exit( -1 );
            }
            str = k_str();
            if ( str )
               strcpy( (char *)Wsv[nWsv].port, str );
            else
            {
               logit( "e", "cuspfeeder: Can't get a waveserver port number. Exiting.\n" );
               exit( -1 );
            }
            nWsv++;
            init[8] = 1;
         }
         else if ( k_its("wsTimeout") )
         {
            wsTimeout = k_long();
            init[9] = 1;
         }
         else if ( k_its("TraceBufferSize") )
         {
            TraceBufferSize = k_int();
            init[10] = 1;
         }
         else if ( k_its("SendAllComponents") )
         {
            SendAllComponents = k_int();
            init[11] = 1;
         }
         else if ( k_its("MaxWait") )
         {
            MaxWait = k_int();
            init[12] = 1;
         }
         else if ( k_its("ChunkDelay") )
         {
            ChunkDelay = k_int();
            init[13] = 1;
         }
         else if ( k_its("Decimate") )    /* Optional decimation */
         {
            if ( nDec == MAX_DEC )
            {
               logit( "e", "Too many Decimate commands in config file. Exiting.\n" );
               exit( -1 );
            }
            Dec[nDec].freq       = k_val();
            Dec[nDec].decRate    = k_int();
            Dec[nDec].passRipple = k_val();
            Dec[nDec].stopRipple = k_val();
            Dec[nDec].passAllow  = k_val();
            Dec[nDec].msgSize    = k_int();
            str = k_str();
            if (str) strcpy( (char *)&Dec[nDec].inComp[0], str );
            str = k_str();
            if (str) strcpy( (char *)&Dec[nDec].outComp[0], str );
            if ( strlen((char *)&Dec[nDec].inComp[0]) != strlen((char *)&Dec[nDec].outComp[0]) )
            {
               logit( "e", "InComp/OutComp length mismatch. Exiting.\n" );
               exit( -1 );
            }
            nDec++;
         }
         else if ( k_its("WaveformDestination") )  /* Optional cmd */
         {
            WaveformDest = k_int();
            if( WaveformDest < DEST_CUSPONLY  || 
                WaveformDest > DEST_LOCALONLY   )
            {
               logit( "e", "Invalid WaveformDestination value %d; Exiting.\n",
                      WaveformDest );
               exit( -1 );
            }
         }
         else if ( k_its("LocalWaveformDir") )   /* Optional cmd */
         {
            str = k_str();
            if( str && strlen(str)<MAX_STRLEN )
            {
               strcpy( (char *)LocalWaveformDir, str );
               if( RecursiveCreateDir( (char *)LocalWaveformDir ) == EW_FAILURE ) 
               {
                  logit( "e", "cuspfeeder: Error creating LocalWaveformDir %s."
                             " Exiting.\n", LocalWaveformDir );
                  exit( -1 );
               }
            }
            else 
            {
               logit( "e", "cuspfeeder: Bad LocalWaveformDir command (maxlen=%d)."
                          " Exiting.\n", MAX_STRLEN-1 );
               exit( -1 );
            }
         }

/* Unknown command
   ***************/
         else
         {
            logit( "e", "cuspfeeder: <%s> Unknown command in <%s>.\n",
                     com, configfile );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            logit( "e", "cuspfeeder: Bad <%s> command in <%s>. Exiting.\n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
       logit( "e", "cuspfeeder: ERROR, no " );
       if ( !init[0]  ) logit( "e", "<MyModuleId> "          );
       if ( !init[1]  ) logit( "e", "<RingName> "            );
       if ( !init[2]  ) logit( "e", "<HeartBeatInterval> "   );
       if ( !init[3]  ) logit( "e", "<GetTrigsFrom> "        );
       if ( !init[4]  ) logit( "e", "<LocalTriggerDir> "     );
       if ( !init[5]  ) logit( "e", "<RemoteIpAddress> "     );
       if ( !init[6]  ) logit( "e", "<WellKnownPort> "       );
       if ( !init[7]  ) logit( "e", "<RetryInterval> "       );
       if ( !init[8]  ) logit( "e", "<Waveserver> "          );
       if ( !init[9]  ) logit( "e", "<wsTimeout> "           );
       if ( !init[10] ) logit( "e", "<TraceBufferSize> "     );
       if ( !init[11] ) logit( "e", "<SendAllComponents> "   );
       if ( !init[12] ) logit( "e", "<MaxWait> "             );
       if ( !init[13] ) logit( "e", "<ChunkDelay> "          );
       logit( "e", "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }

/* Check for "required optional" command
 ***************************************/
   if ( WaveformDest != DEST_CUSPONLY ) 
   {
       if( strlen((char *)LocalWaveformDir) == 0 ) 
       {
          logit( "e", "cuspfeeder: ERROR, no <LocalWaveformDir> command "
                     "in <%s>. Exiting.\n", configfile );
          exit( -1 );
       }
   }
 
   return;
}


/***************************************************************************
 *  LogConfig()   Print the configuration file parameters in the log file. *
 ***************************************************************************/

void LogConfig( void )
{
   int i;

   logit( "", "\n" );
   logit( "", "MyModuleId:        %s\n", MyModName );
   logit( "", "RingName:          %s\n", RingName );
   logit( "", "HeartBeatInterval: %d\n", HeartBeatInterval );
   logit( "", "LocalTriggerDir:   %s\n", LocalTriggerDir );
   logit( "", "WaveformDest:      %d "
              "(0=CUSP only; 1=CUSP and local dir; 2=local dir only)\n", 
               WaveformDest );
   if( WaveformDest != DEST_CUSPONLY )    
      logit( "", "LocalWaveformDir:  %s\n", LocalWaveformDir );
   logit( "", "RemoteIpAddress:   %s\n", RemoteIpAddress );
   logit( "", "WellKnownPort:     %d\n", WellKnownPort );
   logit( "", "RetryInterval:     %d\n", RetryInterval );
   logit( "", "wsTimeout:         %d\n", wsTimeout );
   logit( "", "TraceBufferSize:   %d\n", TraceBufferSize );
   logit( "", "SendAllComponents: %d\n", SendAllComponents );
   logit( "", "MaxWait:           %d\n", MaxWait );
   logit( "", "ChunkDelay:        %d\n", ChunkDelay );

   for ( i = 0; i < nLogo; i++ )
      logit( "", "GetTrigsFrom:      instid:%u  mod:%u\n",
             GetLogo[i].instid, GetLogo[i].mod );

   for ( i = 0; i < nWsv; i++ )
      logit( "", "Waveserver:        %s %s\n", Wsv[i].ip, Wsv[i].port );

   for ( i = 0; i < nDec; i++ )
   {
      logit( "", "Decimate:          %5.1lf   %3d  %8.5lf  %8.5lf  %8.5lf  %3d",
             Dec[i].freq, Dec[i].decRate, Dec[i].passRipple,
             Dec[i].stopRipple, Dec[i].passAllow, Dec[i].msgSize );
      logit( "", "  %3s  %3s\n", Dec[i].inComp, Dec[i].outComp );
   }

   logit( "", "\n" );
   return;
}


     /*****************************************************************
      *  Lookup()   Look up important info from earthworm.h tables    *
      *****************************************************************/

void Lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if ( ( RingKey = GetKey(RingName) ) == -1 )
   {
        logit( "e", "cuspfeeder:  Invalid ring name <%s>. Exiting.\n", RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      logit( "e", "cuspfeeder: Error getting local installation id. Exiting.\n" );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 )
   {
      logit( "e", "cuspfeeder: Invalid module name <%s>. Exiting.\n", MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      logit( "e", "cuspfeeder: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      logit( "e", "cuspfeeder: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRIGLIST2K", &TypeTriglist ) != 0 )
   {
      logit( "e", "cuspfeeder: Invalid message type <TYPE_TRIGLIST2K>. Exiting.\n" );
      exit( -1 );
   }
   return;
}
