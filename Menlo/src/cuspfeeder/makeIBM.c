
/*
 * makeIBM.c
*/

#include <string.h>
#include <swap.h>
#include <trace_buf.h>
#include <earthworm.h>


   /**************************** TraceMakeIBM ******************************
   *                                                                       *
   *  Converts a series of tracebuf messages into IBM byte order,          *
   *  if they aren't already.                                              *
   *                                                                       *
   *  Returns -1 if unknown data type, 0 otherwise                         *
   *************************************************************************/

int TraceMakeIBM( char *tracebuf, long tracelen )
{
   char *tout, *tin;      /* working pointers into trace buffer */
   TracePacket  wpkt;     /* working waveform packet */
   char *wdata;           /* pointer to beginning of data in tracepacket */
   long  nsamp;           /* number of samples in a single tracebuf msg */
   int   byte_per_sample; /* taken from tracebuf header */
   char  byte_order;      /* taken from tracebuf header */
   long  datalen;         /* total length (bytes) of samps in a tracebuf msg */
   int   i;

   tin = tout = tracebuf;
   wdata = wpkt.msg + sizeof( TRACE_HEADER );

/* Work thru entire trace buffer
 *******************************/
   while( tout < tracebuf+tracelen )
   {

   /* Read one header & see what sort of data the message carries
      We assume that all messages for a trace are in the same
      byte order, so if the first is Intel, we return immediately.
      ************************************************************/
      memcpy( wpkt.msg, tout, sizeof( TRACE_HEADER ) );
      tout += sizeof( TRACE_HEADER );

      if ( strcmp(wpkt.trh.datatype, "i2") == 0 )
         return 0;

      else if ( strcmp(wpkt.trh.datatype, "i4") == 0 )
         return 0;

      else if ( strcmp(wpkt.trh.datatype, "s2") == 0 )
         byte_per_sample = 2;

      else if ( strcmp(wpkt.trh.datatype, "s4") == 0 )
         byte_per_sample = 4;

      else
         return -1;  /* Unknown data type */

   /* We've established that the data is in Sun byte order.
      Get ready to swap whole trace, one tracebuf message at a time.
    ****************************************************************/
      nsamp      = wpkt.trh.nsamp;
      byte_order = wpkt.trh.datatype[0];
    #ifdef _INTEL
      if( byte_order == 's' ) SwapLong( &nsamp );
    #endif

   /* Swap the header fields
    ************************/
      SwapInt( &(wpkt.trh.pinno) );
      SwapInt( &(wpkt.trh.nsamp) );
      SwapDouble( &(wpkt.trh.starttime) );
      SwapDouble( &(wpkt.trh.endtime) );
      SwapDouble( &(wpkt.trh.samprate) );

   /* Copy data samples into working buffer
    ***************************************/
      datalen = byte_per_sample * nsamp;
      if( datalen + sizeof(TRACE_HEADER) > MAX_TRACEBUF_SIZ )
      {
         logit( "e",
               "TraceMakeIBM: msg[%d] overflows working buffer[%d]\n",
                datalen+sizeof(TRACE_HEADER), MAX_TRACEBUF_SIZ );
         return -1;
      }
      memcpy( wdata, tout, datalen );
      tout += datalen;

   /* Swap the data
    ***************/
      if ( byte_per_sample == 2 )
      {
         short *shortPtr = (short *) wdata;

         for ( i = 0; i < nsamp; i++ )
            SwapShort( &shortPtr[i] );

         strcpy( wpkt.trh.datatype, "i2" );
      }

      else if ( byte_per_sample == 4 )
      {
         long *longPtr  = (long  *) wdata;

         for ( i = 0; i < nsamp; i++ )
            SwapLong( &(longPtr[i]) );

         strcpy( wpkt.trh.datatype, "i4" );
      }

   /* Copy swapped tracebuf message back into buffer
    ************************************************/
      memcpy( tin, wpkt.msg, sizeof(TRACE_HEADER)+datalen );
      tin += sizeof(TRACE_HEADER)+datalen;
   }

   return 0;
}
