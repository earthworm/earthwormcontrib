
  /*******************************************************************************
   *                                 cuspfeeder.c                                *
   *                                                                             *
   *                  This program feeds event waveforms to Cusp.                *
   *                                                                             *
   *  The main thread gets trigger messages from a transport ring and writes     *
   *  them to disk files.  A second thread reads the trigger messages from disk  *
   *  and grabs the appropriate waveform data from a waveserver.  The waveform   *
   *  data is then sent to Cusp via a socket connection.  The getevent program,  *
   *  which lives on a VMS system, gets data from this program.                  *
   *  Cuspfeeder is a standard Earthworm module and which runs on both Solaris   *
   *  and Windows NT.                                                            *
   *                                                                             *
   *  WMK 7/1/98                                                                 *
   *******************************************************************************/

/* Decimation option added.  The user specifies a target sampling rate,
   and the program chooses a decimation factor which results in approximately
   the target sampling rate.  No anti-aliasing filters are applied before
   decimation.  WMK 1/24/00

   Cuspfeeder now decimates using the same algorithm as Pete Lombards decimate
   program.  Feature added after 1/24/00.  WMK

   Event id problem: If cuspfeeder receives a triglist message with a non-integer
   event id, it will make a note in its log file, and it will skip the triglist
   message.  Currently, Menlo uses only integer event id's, but someone may slip
   us a non-integer event id in the future.  WMK 5/2/00

   Program now uses realloc() to handle any number of channels.  Improved logging.
   Made many other minor changes.  WMK 12/11/01

   Added new algorithm to rename component codes of decimated tracebuf messages.
   This feature is used only for decimated teleseismic data on heli1.  WMK 2/3/03
 */
   

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include <time_ew.h>
#include "cuspfeeder.h"

#define MAXLOGO 2

/* Function declarations
   *********************/
void GetConfig( char * );
void LogConfig( void );
void Lookup( void );
void SendStatus( unsigned char, short, char * );
void WriteTriglistMsg( char [], int );
thr_ret SenderThread( void * );

/* Global variables
   ****************/
static SHM_INFO Region;              /* Shared memory region to use for i/o */
static pid_t    myPid;               /* Process id of this process */

/* Things to read or derive from configuration file
   ************************************************/
extern char  RingName[20];           /* Name of transport ring for i/o */
extern char  MyModName[20];          /* Speak as this module name/id */
extern long  HeartBeatInterval;      /* Seconds between heartbeats */
extern char  LocalTriggerDir[80];    /* For temporary storage of trigger files */
extern short nLogo;                  /* Number of logo types to get from ring */
extern MSG_LOGO GetLogo[MAXLOGO];    /* Array for module,type,instid */

/* Look these up in the earthworm.h tables with getutil functions
   **************************************************************/
extern long          RingKey;        /* Key of transport ring for i/o */
extern unsigned char InstId;         /* Local installation id */
extern unsigned char MyModId;        /* Module Id for this program */
extern unsigned char TypeHeartBeat;
extern unsigned char TypeError;
extern unsigned char TypeTriglist;

/* Share these with the thread function
   ************************************/
volatile char RemoteHost[40];        /* Name of the remote host */
volatile char RemoteUserId[40];      /* User name on remote host */


int main( int argc, char **argv )
{
   long      timeNow;           /* Current time */
   long      timeLastBeat;      /* Time last heartbeat was sent */
   int       res;
   char      *trigbuf;          /* Buffer to hold triglist msg */
   char      *configFileName;
   unsigned  thread_id;         /* Id number of server thread */
   MSG_LOGO  reclogo;           /* Logo of retrieved message */
   long      recsize;           /* Size of retrieved message */
   const int LogFile = 1;       /* If 1, write a log file */

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      printf( "Usage: cuspfeeder <configfile>\n" );
      return -1;
   }
   configFileName = argv[1];

/* Open an existing log file or create a new one
   *********************************************/
   logit_init( configFileName, 0, 256, LogFile );

/* Read the configuration file
   ***************************/
   GetConfig( configFileName );

/* Look up stuff in the earthworm.h tables
   ***************************************/
   Lookup();

/* Get my own pid, for restart purposes
   ************************************/
#ifdef _SOLARIS
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "cuspfeeder: Can't get my pid. Exiting.\n" );
      return -1;
   }
#endif

#ifdef _WINNT
   myPid = _getpid();
#endif


/* Log the configuration file parameters
   *************************************/
   LogConfig();

/* Allocate the triglist message buffer.
   Max size of carltrig triglist messages is MAX_BYTES_PER_EQ.
   **********************************************************/
   trigbuf = (char *) malloc( (size_t)MAX_BYTES_PER_EQ );
   if ( trigbuf == NULL )
   {
      logit( "e", "cuspfeeder: Can't allocate triglist message buffer. Exiting.\n" );
      return -1;
   }

/* Attach to shared memory ring.
   We will get triglist messages from this ring,
   and write status messages to this ring.
   *****************************************/
   tport_attach( &Region, RingKey );
   logit( "", "cuspfeeder: Attached to shared memory region %s: %d\n",
          RingName, RingKey );

/* Change the working directory to LocalTriggerDir
   ***********************************************/
   if ( chdir_ew( LocalTriggerDir ) < 0 )
   {
      logit( "et", "cuspfeeder: chdir_ew error.\n" );
      return -1;
   }

/* Start a new thread which will send trigger files
   to a remote system using a socket connection
   ************************************************/
   if ( StartThread( SenderThread, (unsigned)0, &thread_id ) < 0 )
   {
      logit( "et", "cuspfeeder: Can't start server thread.\n" );
      return -1;
   }

/* Flush the transport ring
   ************************/
   while ( tport_getmsg( &Region, GetLogo, nLogo, &reclogo, &recsize, trigbuf,
                         MAX_BYTES_PER_EQ ) != GET_NONE );

/* Force a heartbeat to be issued in first pass thru main loop
   ***********************************************************/
   timeLastBeat = time(&timeNow) - HeartBeatInterval - 1;

/* Main loop
   *********/
   while ( 1 )
   {

/* Beat the heart
   **************/
      if  ( (time(&timeNow) - timeLastBeat) >= HeartBeatInterval )
      {
          timeLastBeat = timeNow;
          SendStatus( TypeHeartBeat, 0, "" );
      }

/* See if termination has been requested
   *************************************/
      if ( tport_getflag( &Region ) == TERMINATE  ||
           tport_getflag( &Region ) == myPid )
         break;

/* Process all new messages before sleeping again
   **********************************************/
      do
      {
         char Text[100];                               /* String for log/error messages */

/* Get a triglist message and check the getmsg() return code
   *********************************************************/
         res = tport_getmsg( &Region, GetLogo, nLogo, &reclogo, &recsize,
                             trigbuf, MAX_BYTES_PER_EQ );

         if ( res == GET_NONE )                        /* No more new messages */
            break;
         else if ( res == GET_TOOBIG )                 /* Next message was too big */
         {
            sprintf( Text, "Retrieved triglist msg[%ld] (i%u m%u t%u) too big for buffer[%d]",
                    recsize, reclogo.instid, reclogo.mod, reclogo.type,
                    MAX_BYTES_PER_EQ );
            SendStatus( TypeError, ERR_TOOBIG, Text );
            continue;
         }
         else if ( res == GET_MISS )                   /* Got a msg, but missed some */
         {
            sprintf( Text, "Missed triglist message(s)  i%u m%u t%u  %s.",
                     reclogo.instid, reclogo.mod, reclogo.type, RingName );
            SendStatus( TypeError, ERR_MISSMSG, Text );
         }
         else if ( res == GET_NOTRACK )                /* Got a message, but can't */
         {                                             /* tell if any were missed */
            sprintf( Text, "Triglist message received (i%u m%u t%u); NTRACK_GET exceeded",
                      reclogo.instid, reclogo.mod, reclogo.type );
            SendStatus( TypeError, ERR_NOTRACK, Text );
         }

/* Write the triglist message to disk
   **********************************/
         WriteTriglistMsg( trigbuf, (int)recsize );

      } while ( res != GET_NONE );              /* End of message-processing loop */

      sleep_ew( 250 );                          /* Wait for new messages to arrive */
   }

/* Terminate program
   *****************/
   tport_detach( &Region );
   return 0;
}


/*************************************************************************
 * SendStatus() builds a heartbeat or error message & puts it into       *
 *              shared memory.  Writes errors to log file & screen.      *
 *************************************************************************/

void SendStatus( unsigned char type, short ierr, char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   long     t;

/* Build the message
   *****************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if ( type == TypeHeartBeat )
      sprintf( msg, "%ld %d\n", t, myPid );
   else if ( type == TypeError )
   {
      sprintf( msg, "%ld %hd %s\n", t, ierr, note );
      logit( "et", "%s\n", note );
   }
   else return;

   size = strlen( msg );

/* Write the message to shared memory
   **********************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
      if( type == TypeHeartBeat )
         logit("et","cuspfeeder: Error sending heartbeat.\n" );
      if( type == TypeError )
         logit("et","cuspfeeder: Error sending error:%d.\n", ierr );
   }
   return;
}


/***************************************************************************
 *  WriteTriglistMsg()  Write a triglist message to a disk file.           *
 *                      The server thread will read this file and send     *
 *                      it to a remote system.                             *
 ***************************************************************************/

void WriteTriglistMsg( char trigbuf[], int recsize )
{
   char       fname[100];
   FILE       *fp;
   char       *eventidptr;
   const char event_token[] = "EVENT ID:";
   unsigned   event_id;
   int        len;

/* Get the event id from the triglist message
   ******************************************/
   eventidptr = strstr( trigbuf, event_token );
   if ( eventidptr == NULL )
   {
      logit( "et", "cuspfeeder: No event id in trigger message.\n" );
      return;
   }
   eventidptr += strlen( event_token );
   if ( sscanf( eventidptr, "%u", &event_id ) < 1 )
   {
      logit( "et", "cuspfeeder: Error decoding the event id.\n" );
      return;
   }

/* Write the triglist message to a new file.
   File name format is triglist.eventid
   eg /home/earthworm/run/log/triglist.000001
   ******************************************/
   fp = fopen( "temp", "wb" );
   if ( fp  == NULL )
   {
      logit( "et", "cuspfeeder: Error opening new triglist file %s\n", fname );
      return;
   }

   if ( fwrite( trigbuf, sizeof(char), recsize, fp ) < (size_t)recsize )
   {
      logit( "et", "cuspfeeder: Error writing triglist message to file %s\n", fname );
      fclose( fp );
      return;
   }
   fclose( fp );

/* Rename the temporary file to it's real name
   *******************************************/
   strcpy( fname, "triglist." );
   len = strlen( fname );
   sprintf( fname+len, "%06u", event_id );
   rename( "temp", fname );
   return;
}
