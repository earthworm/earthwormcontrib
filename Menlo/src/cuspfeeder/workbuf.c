/*
   workbuf.c
*/

#include <stdio.h>
#include <math.h>
#include <string.h>      /* memmove() */
#include <trace_buf.h>
#include "cuspfeeder.h"


int AppendMsgWb( char *msg, WORKBUF *wb )
{
   TRACE_HEADER *th = (TRACE_HEADER *)msg;
   int    nfree = wb->size - wb->nwork;
   int    i;
   short  *d2 = (short *)(msg + sizeof(TRACE_HEADER));
   long   *d4 = (long  *)(msg + sizeof(TRACE_HEADER));
   int    bytesPerSamp = th->datatype[1] - 48;
   int    nwork = wb->nwork;
   int    nsamp;
   double endtime;

/* To ensure proper byte alignment, copy headers to local variables
   ****************************************************************/
   memcpy( &endtime, &th->endtime, sizeof(double) );
   memcpy( &nsamp,   &th->nsamp,   sizeof(int)    );

/* Not enough space in work buffer
   *******************************/
   if ( nsamp > (wb->size - wb->nwork) )
      return -1;

/* Copy samples from message buffer to work buffer.
   Convert to doubles for filtering.
   Copy data to local variable to ensure byte alignment.
   ****************************************************/
   if ( bytesPerSamp == 2 )
      for ( i = 0; i < nsamp; i++ )
      {
         short i2;
         memcpy( &i2, &d2[i], sizeof(short) );
         wb->dsmp[nwork++] = (double)i2;
      }
   else
      for ( i = 0; i < nsamp; i++ )
      {
         long i4;
         memcpy( &i4, &d4[i], sizeof(long) );
         wb->dsmp[nwork++] = (double)i4;
      }

/* Update work buffer sample count and time of last sample
   *******************************************************/
   wb->nwork += nsamp;
   wb->twork = endtime;
   return 0;
}


void LogWb( WORKBUF *wb )
{
   int i;

   printf( "Work buffer...\n" );
   printf( "size: %d\n", wb->size );
   printf( "nwork: %d\n", wb->nwork );
   printf( "twork: %.3lf\n", wb->twork );

   for ( i = 0; i < wb->nwork; i++ )
      printf( " %.1lf", wb->dsmp[i] );
   printf( "\n" );
}


     /************************************************************
      *                      FiltWbSamp()                        *
      *                                                          *
      *  Filter the first samples in the work buffer.            *
      *  Returns samp  = The filtered sample.                    *
      *          tsamp = Time of filtered sample, corrected for  *
      *                  filter delay.                           *
      ************************************************************/

void FiltWbSamp( WORKBUF *wb, DECSTR *decPtr, double *samp, double *tsamp )
{
   int i;
   int length = decPtr->length;
   int nDrop  = decPtr->decRate;
   int nLeft  = wb->nwork - nDrop;
   int half_l = length / 2;
   int nodd   = length - (2 * half_l);

   double sum = 0.0;
   double *s1 = &(wb->dsmp[0]);
   double *s2 = &(wb->dsmp[nDrop]);

/* Apply FIR filter to first samples in work buffer
   ************************************************/
   for ( i = 0; i < half_l; i++ )
      sum += decPtr->coef[i] * (wb->dsmp[i] + wb->dsmp[length - i - 1]);

   if ( nodd == 1 )
      sum += decPtr->coef[half_l] * wb->dsmp[half_l];

/* Remove unneeded unfiltered samples from work buffer
   ***************************************************/
   memmove( s1, s2, (nLeft * sizeof(double)) );
   wb->nwork -= nDrop;

/* Return the filtered sample
   **************************/
   *samp = sum;

/* Calculate time of filtered sample
   *********************************/
   *tsamp = wb->twork - (wb->nwork - 0.5 * (decPtr->length + 1)) /
            decPtr->freq;
   return;
}
