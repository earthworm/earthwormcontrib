/*
 * setdec.c: Set up the decimation filter
 *
 *  Bug fix.  SetDec() now returns if ifl<2.  Before, remeznp() would exit
 *  if (ifl==1).  WMK  3/1/01
 */

/*      Function: SetDec                                                */
/*                                                                      */
/*      Inputs:         Pointer to the Network structure                */
/*                      Decimation rate                                 */
/*                                                                      */
/*      Outputs:        Filled in FILTER array                          */
/*                                                                      */
/*      Returns:        0 on success                                    */
/*                      -1 on failure                                   */
/*                                                                      */

#include <stdio.h>
#include <earthworm.h>        /* logit */
#include "cuspfeeder.h"

/* Function prototypes
   *******************/
int remeznp( int, double *, double *, double *, double *, int *, double ** );
int Zeroes( double *, double **, double **, int );


int SetDec( int ifl )
{
   extern DECSTR Dec[MAX_DEC];     /* Array of decimation intervals */
   int    i;
   int    order;
   int    length;                  /* Number of coefficients in filter */
   double freqSample = 1.0;        /* input relative sample rate  */
   double freqPass;                /* pass-band upper freq limit             */
   double freqStop;                /* stop-band lower freq limit             */
   double freq_edges[4];           /* Array of edges of frequency bands      */
   double levels[4];               /* One level for each band edge           */
   double devs[2];                 /* Allowed ripple for each band           */
   double weights[2];              /* Weights for each band                  */
   double max_dev;
   double finalNyquist;            /* Decimate's final Nyquist frequency     */
   double *coeffs;                 /* Pointer to coefficients                */
   double *wr, *wi;                /* pointers for complex zeroes            */

/* Handle the case of no decimation
   ********************************/
   if ( Dec[ifl].decRate < 2 )
   {
      logit( "e", "Invalid decimation rate: %d\n", Dec[ifl].decRate );
      return -1;
   }

/* Set up the filter.
   Ref: Digital Signal Processing, Proakis and Manolakis (1996)
   Set the upper limit of pass-band frequency to little less
   than the Nyquist frequency (1/2 the final sample rate).
   ************************************************************/
   finalNyquist  = 0.5 * freqSample / (double)Dec[ifl].decRate;
   freqPass      = Dec[ifl].passAllow * finalNyquist;
   freq_edges[0] = 0.0;
   levels[0]     = 1.0;
   levels[1]     = 1.0;
   levels[2]     = 0.0;
   levels[3]     = 0.0;
   freq_edges[3] = 0.5;

/* Set the allowed ripple
   **********************/
   devs[0] = Dec[ifl].passRipple;
   devs[1] = Dec[ifl].stopRipple;

/* Set the highest weight for the smallest deviation
   *************************************************/
   max_dev = ( devs[0] < devs[1] ) ? devs[0] : devs[1];
   for ( i = 0; i < 2; i++ )
      weights[i] = max_dev / devs[i];

/* Stop-band frequency is sample rate - final Nyquist;
   This provides minimum aliasing while minimizing filter order
   ************************************************************/
   freqStop = freqSample / (double)Dec[ifl].decRate - finalNyquist;
/* logit( "", "Remez frequencies: pass: %e, stop: %e, sample: %e\n",
        freqPass, freqStop, freqSample ); */

/* Frequencies in remezlp must be referenced to the sample frequency
   *****************************************************************/
   freq_edges[1] = freqPass / freqSample;
   freq_edges[2] = freqStop / freqSample;
   if ( remeznp(2, freq_edges, levels, devs, weights, &length, &coeffs)
       != EW_SUCCESS )
   {
     logit( "e", "cuspfeeder: Call to remeznp() failed. Exiting.\n" );
     return -1;
   }

   Dec[ifl].length = length;
   Dec[ifl].coef   = coeffs;

/* Calculate filter zeros
   **********************/
   order = length - 1;

   if ( Zeroes(coeffs, &wr, &wi, order) != EW_SUCCESS )
   {
     logit( "e", "cuspfeeder: Failed to calculate filter zeros. Exiting.\n" );
     return -1;
   }

/* Log some information
   ********************/
   logit("","\nDecimation rate: %d\n", Dec[ifl].decRate );

   logit("", "Time lag introduced by filter: %.1f times the data sample period.\n",
         order / 2.0);
   logit( "", "Filter length: %d\n", length );

   logit("", "Filter coefficients:\n" );

   for ( i = 0; i < (length+1)/2;i++ )
     logit("", "\t%4d % 10.8e\n", i+1, coeffs[i] );

   for ( i = length/2 - 1; i > -1; i-- )
     logit( "", "\t%4d % 10.8e\n", length - i, coeffs[i] );

/* logit( "","\nZeroes for FIR filter of order %ld:\n", order );
   for ( i = 0; i < order; i++ )
      logit( "", "\t%4d % 10.8e % 10.8e\n", i+1, wr[i], wi[i] ); */

   logit( "", "\n" );

   free( wr );
   free( wi );

/* Set the output frequency
   ************************/
   freqSample /= (double)Dec[ifl].decRate;

   return 0;
}
