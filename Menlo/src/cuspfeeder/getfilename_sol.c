
       /***************************************************************
        *                     getfilename_sol.c                       *
        *                                                             *
        *  Function to get the name of a trigger file in the current  *
        *  directory.                                                 *
        *                                                             *
        *  Returns 0 if all ok                                        *
        *          1 if no trigger files are found                    *
        ***************************************************************/

#include <stdio.h>
#include <dirent.h>
#include "cuspfeeder.h"

/* Declared volatile in cuspfeeder.c
   *********************************/
extern char LocalTriggerDir[80];


int GetFname( char fname[] )
{
   DIR           *dp;
   struct dirent *dentp;

   dp = opendir( LocalTriggerDir );
   if ( dp == NULL )
   {
      logit( "et", "getfilename: Can't open the trigger directory.\n" );
      return CUSPFEEDER_FAILURE;
   }

   while ( dentp = readdir(dp) )
   {
      if ( strncmp( dentp->d_name, "triglist.", 9 ) == 0 )
      {
         strcpy( fname, dentp->d_name );
         closedir( dp );
         return CUSPFEEDER_SUCCESS;
      }
   }
   closedir( dp );
   return CUSPFEEDER_FAILURE;
}
