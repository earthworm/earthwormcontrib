
/*   cuspfeeder.h    */

#define ERR_MISSMSG         0  /* Message missed in transport ring */
#define ERR_TOOBIG          1  /* Retrieved message too large for buffer */
#define ERR_NOTRACK         2  /* Message retrieved; tracking limit exceeded */
#define ERR_NOCONNECT       3  /* Can't connect to Cusp */
#define ERR_NOREALLOC       4  /* realloc failure */
#define ERR_EVENT_TOO_BIG   5  /* Event too large for tracebuf */

#define CUSPFEEDER_FAILURE  1
#define CUSPFEEDER_SUCCESS  0

#define DEST_CUSPONLY       0  /* waveform file to CUSP only */ 
#define DEST_CUSPANDLOCAL   1  /* waveform file to CUSP and local dir */
#define DEST_LOCALONLY      2  /* waveform file to local dir only */

#define GOT_IT              1  /* Status flags for trigger channels */
#define HAVENT_GOTTEN_IT    0

#define BAD_CONNECTION     -1  /* Returned by RequestWaves() */
#define NOT_AVAILABLE      -2
#define BUFFER_OVERFLOW    -3

#define TRUE                1
#define FALSE               0

#define MAX_WSV            50  /* Maximum number of Waveservers */
#define MAX_DEC            20  /* Maximum known sampling rates/component codes to decimate */
#define MAX_ADRLEN         20  /* Size of waveserver address arrays */
#define MAX_LOGO            2  /* Maximum number of triglist logos */
#define MAX_STRLEN         80  /* Maximum length of strings */

typedef struct
{
   char   sta[6];              /* Station */
   char   cmp[4];              /* Component */
   char   net[3];              /* Network */
} SCN;

typedef struct
{
   char   sta[6];              /* Station */
   char   cmp[4];              /* Component */
   char   net[3];              /* Network */
   int    ssYear;              /* Year of start save time (last two digits) */
   int    ssMonth;             /* Month of start save time (January = 1) */
   int    ssDay;               /* Day of start save time */
   int    ssHour;              /* Hour of start save time */
   int    ssMinute;            /* Minute of start save time */
   double ssSecond;            /* Second of start save time */
   double stime;               /* Start save time in sec since Jan 1, 1970 */
   int    duration;            /* Duration in seconds */
   int    status;              /* GOT_IT or HAVENT_GOTTEN_IT */
} TRIGGER;


typedef struct                 /* Structure containing info about each waveserver */
{
   char ip[MAX_ADRLEN];
   char port[MAX_ADRLEN];
} WSV;

typedef struct                 /* Structure containing decimation parameters */
{
   double freq;                /* Frequency before decimation, in samp/sec */
   int    decRate;             /* Decimation rate */
   double passRipple;          /* Filter design parameter */
   double stopRipple;          /* Filter design parameter */
   double passAllow;           /* Fraction of Nyquist freq for pass band (<1.0) */
   int    msgSize;             /* Max size of decimated tracebuf msgs, in samples */
   int    length;              /* Length of symmetric FIR filter */
   double *coef;               /* Array of filter coefficients */
   char   inComp[4];           /* Input component code */
   char   outComp[4];          /* Output component code */
} DECSTR;

typedef struct                 /* Workbuf structure */
{
   double *dsmp;               /* Array of data samples */
   int    size;                /* Workbuf capacity in samples */
   int    nwork;               /* Number of samples in workbuf */
   double twork;               /* Time of last sample in workbuf */
} WORKBUF;

