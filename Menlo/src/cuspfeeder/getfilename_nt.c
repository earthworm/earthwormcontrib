
       /***************************************************************
        *                      getfilename_nt.c                       *
        *                                                             *
        *  Function to get the name of a trigger file in the current  *
        *  directory.                                                 *
        *                                                             *
        *  Returns 0 if all ok                                        *
        *          1 if no trigger files are found                    *
        ***************************************************************/

#include <stdio.h>
#include <windows.h>
#include "cuspfeeder.h"


int GetFname( char fname[] )
{
   HANDLE          fileHandle;
   const char      fileMask[] = "triglist.*";
   WIN32_FIND_DATA findData;

   fileHandle = FindFirstFile( fileMask, &findData );
   if ( fileHandle  == INVALID_HANDLE_VALUE )
      return CUSPFEEDER_FAILURE;

   strcpy( fname, findData.cFileName );
   FindClose( fileHandle );
   return CUSPFEEDER_SUCCESS;
}

