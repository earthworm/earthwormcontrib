
   /*************************************************************
    *                       sendwave_sol.c                      *
    *************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <earthworm.h>
#include "cuspfeeder.h"

#define BUFLEN 1000

static char buf[BUFLEN];

/* Declared elsewhere
   ******************/
extern int  errno;                     /* Contains socket error numbers */
extern char RemoteIpAddress[80];       /* IP address of system to receive triggers */
extern int  WellKnownPort;             /* The well-known port number */

static int  sd;                        /* Socket descriptor */


      /************************************************************
       *                    GetCuspConnection()                   *
       *       Get a connection to the remote Cusp system.        *
       ************************************************************/

int GetCuspConnection( void )
{
   struct sockaddr_in server;         /* Server socket address structure */
   const int optVal = 1;

/* Get a new socket descriptor
   ***************************/
   sd = socket( AF_INET, SOCK_STREAM, 0 );
   if ( sd == -1 )
   {
      logit( "et", "cuspfeeder: socket() error. errno: %d\n", errno );
      return CUSPFEEDER_FAILURE;
   }

/* Allow reuse of socket addresses
   *******************************/
   if ( setsockopt( sd, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
                    sizeof(int) ) == -1 )
   {
      logit( "et", "cuspfeeder: setsockopt() error. errno: %d\n", errno );
      SocketClose( sd );
      return CUSPFEEDER_FAILURE;
   }

/* Fill in socket address structure
   ********************************/
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family      = AF_INET;
   server.sin_port        = htons((unsigned short)WellKnownPort);
   server.sin_addr.s_addr = inet_addr( RemoteIpAddress );

/* Connect to the server
   *********************/
   logit( "et", "Attempting to connect to Cusp: %s port %d\n",
          RemoteIpAddress, WellKnownPort );
   if ( connect( sd, (struct sockaddr *)&server, sizeof(server) ) == -1 )
   {
      logit( "et", "cuspfeeder: connect() error.\n" );
      SocketClose( sd );
      return CUSPFEEDER_FAILURE;
   }
   return CUSPFEEDER_SUCCESS;
}


     /************************************************************
      *                     SendEventToCusp()                    *
      *                                                          *
      *  Send BUFLEN bytes at a time to the Cusp system.         *
      ************************************************************/

int SendEventToCusp( FILE *fp, unsigned *byteCount, unsigned *event_id )
{
   int i;
   int bytesInFile = *byteCount - sizeof(unsigned);
   int nbuf        = bytesInFile / BUFLEN;
   int nleft       = bytesInFile % BUFLEN;

   unsigned byteCountIBM = *byteCount;
   unsigned eventIdIBM   = *event_id;

/* Swap bytes if this is a SPARC or Ultra system
   *********************************************/
#ifdef _SPARC
   SwapInt( (int *)&byteCountIBM );
   SwapInt( (int *)&eventIdIBM );
#endif

/* Send the number of bytes to follow
   **********************************/
   if ( send( sd, (char *)&byteCountIBM, sizeof(unsigned), NULL ) == -1 )
   {
      logit( "et", "cuspfeeder: Error sending file size. errno: %d\n", errno );
      SocketClose( sd );
      return CUSPFEEDER_FAILURE;
   }

/* Send the event id
   *****************/
   if ( send( sd, (char *)&eventIdIBM, sizeof(unsigned), NULL ) == -1 )
   {
      logit( "et", "cuspfeeder: Error sending event id. errno: %d\n", errno );
      SocketClose( sd );
      return CUSPFEEDER_FAILURE;
   }

/* Rewind the temporary event file
   *******************************/
   rewind( fp );

/* Read the event file, BUFLEN bytes at a time, and
   send the bytes via the socket connection
   ************************************************/
   for ( i = 0; i < nbuf; i++ )
   {
      if ( fread( buf, sizeof(char), BUFLEN, fp ) < BUFLEN )
      {
         logit( "et", "Error reading temporary event file.\n" );
         return CUSPFEEDER_FAILURE;
      }

      if ( send( sd, buf, BUFLEN, NULL ) == -1 )
      {
         logit( "et", "cuspfeeder: send() error. errno: %d\n", errno );
         SocketClose( sd );
         return CUSPFEEDER_FAILURE;
      }
   }

/* Send any leftover bytes
   ***********************/
   if ( fread( buf, sizeof(char), nleft, fp ) < (size_t)nleft )
   {
      logit( "et", "Error reading temporary event file.\n" );
      SocketClose( sd );
      return CUSPFEEDER_FAILURE;
   }

   if ( send( sd, buf, nleft, NULL ) == -1 )
   {
      logit( "et", "cuspfeeder: send() error. errno: %d\n", errno );
      SocketClose( sd );
      return CUSPFEEDER_FAILURE;
   }

/* Wait until Cusp sends back an ACK (acknowledge) character
   *********************************************************/
   {
      const char ack = (char)6;     /* Ascii ACK character */
      char ch;                      /* Place to put the received character */

      if ( recv( sd, &ch, sizeof(char), NULL ) == -1 )
      {
         logit( "et", "cuspfeeder: Error receiving ACK character from Cusp\n" );
         SocketClose( sd );
         return CUSPFEEDER_FAILURE;
      }
      if ( ch != ack )              /* This probably won't happen */
      {
         logit( "et", "cuspfeeder: Error. Received %d instead of ACK from Cusp\n",
                (int)ch );
         SocketClose( sd );
         return CUSPFEEDER_FAILURE;
      }
   }

/* Life is wonderful
   *****************/
   SocketClose( sd );
   return CUSPFEEDER_SUCCESS;
}
