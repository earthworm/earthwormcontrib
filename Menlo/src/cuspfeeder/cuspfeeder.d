#
#     This is cuspfeeder's parameter file (cuspfeeder.d)
#
MyModuleId         MOD_CUSPFEEDER          # Module name for this instance of cuspfeeder.
                                           #   The specified name must appear in the
                                           #   earthworm.d configuration file.
RingName           HYPO_RING               # Triglist messages are obtained from this
                                           #   ring.  Error and status messages are sent
                                           #   to this ring.
HeartBeatInterval  15                      # Interval between heartbeats sent to statmgr,
                                           #   in seconds.
LocalTriggerDir  /home/earthworm/run/trig  # For temporary storage of trigger files.
                                           #   Create this directory before running
                                           #   cuspfeeder.
WaveformDestination 0                      # Optional command; accepted arguments:
                                           #   0=remote CUSP machine only (default)
                                           #   1=remote CUSP machine and local directory
                                           #   2=local directory only
LocalWaveformDir  /home/earthworm/run/wf/  # Local directory for saving waveform files;
                                           #   required only if WaveformDestination=1,2
RemoteIpAddress    130.118.43.16           # IP address of CUSP system to receive event
                                           #   files.  pinole=130.118.43.16
WellKnownPort      17002                   # Well-known port number for socket connection
                                           #   to the CUSP system.
RetryInterval      60                      # If send to CUSP fails, retry this often (sec).
wsTimeout          50000                   # Msec to wait for reply from waveserver.
GetTrigsFrom    INST_MENLO MOD_GETQDDS     # Get triglist messages with this instid, modid
TraceBufferSize    2000000                 # In bytes.  Must be big enough to contain the
                                           #   biggest requested time interval for one SCN.
SendAllComponents  0                       # If nonzero, send waveforms for all channels
                                           #   at a station, not just the triggered channel.
                                           #   If zero, send only the triggered channel.
MaxWait            300                     # Maximum time in seconds to wait for waveforms
                                           #   to show up on waveserver.
ChunkDelay         30                      # Chunk delay (sec).  Set this number larger
                                           #   than the largest message sent to Cusp.
#
# If the component code of an incoming tracebuf message matches "InComp" (below),
# then decimate the message and change the component code to "OutComp".
# Question mark (?) characters in InComp and OutComp are single-character wild cards.
# If a tracebuf message doesn't have a sample rate or component code specified below,
# it will not be decimated.  Up to 20 "Decimate" lines may be specified.
#
#          Sample  Decim   Pass    Stop   Pass   Msg   In    Out
#           Rate   Rate   Ripple  Ripple  Allow  Size  Comp  Comp
#          ------  -----  ------  ------  -----  ----  ----  ----
Decimate   100.0     5    0.005   0.0031   0.8   100   E??   S??
Decimate   100.0     5    0.005   0.0031   0.8   100   H??   B??
Decimate   100.0     5    0.005   0.0031   0.8   100   VD?   SH?
Decimate   100.0     5    0.005   0.0031   0.8   100   V??   S??
Decimate   100.0     5    0.005   0.0031   0.8   100   AD?   BN?
Decimate   100.0     5    0.005   0.0031   0.8   100   AS?   BN?
Decimate   100.0     5    0.005   0.0031   0.8   100   DLI   BV1
Decimate   200.0    10    0.005   0.0031   0.8   100   E??   S??
Decimate   200.0    10    0.005   0.0031   0.8   100   H??   B??
#
# Get waveforms from the following waveservers.
# From 1 to 50 waveservers may be specified.
#
#               IP Address      Port
Waveserver     130.118.43.34   16031     # wsv3 ad1
Waveserver     130.118.43.34   16032     # wsv3 ad2
Waveserver     130.118.43.34   16033     # wsv3 ad3
Waveserver     130.118.43.34   16034     # wsv3 ad4
Waveserver     130.118.43.34   16035     # wsv3 ad5
Waveserver     130.118.43.34   16036     # wsv3 ad6
Waveserver     130.118.43.34   16037     # wsv3 ad7
# Waveserver   130.118.43.34   16022     # wsv3 reftek
Waveserver     130.118.43.34   16024     # wsv3 nano2
Waveserver     130.118.43.34   16025     # wsv3 nano1
Waveserver     130.118.43.34   16026     # wsv3 dst
Waveserver     130.118.43.34   16028     # wsv3 cit
Waveserver     130.118.43.34   16029     # wsv3 k2nc
Waveserver     130.118.43.34   16030     # wsv3 k2np
#
Waveserver     130.118.43.4    16031     # wsv2 ad1
Waveserver     130.118.43.4    16032     # wsv2 ad2
Waveserver     130.118.43.4    16033     # wsv2 ad3
Waveserver     130.118.43.4    16034     # wsv2 ad4
Waveserver     130.118.43.4    16035     # wsv2 ad5
Waveserver     130.118.43.4    16036     # wsv2 ad6
Waveserver     130.118.43.4    16037     # wsv2 ad7
# Waveserver   130.118.43.4    16022     # wsv2 reftek
Waveserver     130.118.43.4    16024     # wsv2 nano2
Waveserver     130.118.43.4    16025     # wsv2 nano1
Waveserver     130.118.43.4    16026     # wsv2 dst
Waveserver     130.118.43.4    16028     # wsv2 cit
Waveserver     130.118.43.4    16029     # wsv2 k2nc
Waveserver     130.118.43.4    16030     # wsv2 k2np
#
