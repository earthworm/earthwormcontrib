
/*
 * filtdecim.c
 *
 *  Bug fix.  The variable "newlen" in function FiltDecim() is now initialized to 0.
 *  Before, if the program couldn't calculate any decimated samples, newlen would
 *  remain undefined.  WMK  3/1/01
*/

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <trace_buf.h>
#include <earthworm.h>
#include "cuspfeeder.h"

extern int  nDec;               /* Number of known decimation intervals */
extern DECSTR Dec[MAX_DEC];     /* Array of decimation intervals */

char *GetFirstMsg( char *, long * );
char *GetMsg( long *, int * );
int  AppendMsgWb( char *, WORKBUF * );
void FiltWbSamp( WORKBUF *, DECSTR *, double *, double * );
void LogWb( WORKBUF * );
void InitInMsg( char * );
void InitOutMsg( char * );
void PutMsg( char *, long * );

#define NINT(x) ((x) < 0.0) ? ((x) - 0.5) : ((x) + 0.5)


  /*************************************************************************
   *                              FiltDecim()                              *
   *                                                                       *
   *  Decimate a sequence of tracebuf messages to a target frequency.      *
   *  The tracebuf messages are guaranteed to have the same SCN, but       *
   *  there may be gaps between the messages.  The tracebuf array is       *
   *  modified in place.                                                   *
   *                                                                       *
   *  Accepts:                                                             *
   *     tracebuf = Array containing sequence of messages for one SCN,     *
   *                before decimation.                                     *
   *     actlen   = Combined length of tracebuf messages, in bytes,        *
   *                before decimation.                                     *
   *                                                                       *
   *  Returns:                                                             *
   *     tracebuf = Array containing sequence of messages for one SCN,     *
   *                after decimation.                                      *
   *     actlen   = Combined length of tracebuf messages, in bytes,        *
   *                after decimation.                                      *
   *                                                                       *
   *  Function return value:                                               *
   *     -1 on error                                                       *
   *      0 otherwise                                                      *
   *************************************************************************/

int FiltDecim( char *tracebuf, long *actlen )
{
   char outMsg[MAX_TRACEBUF_SIZ];      /* Decimated trace buffer */

   TRACE_HEADER *outHdr = (TRACE_HEADER *)outMsg;
   TRACE_HEADER *inHdr  = (TRACE_HEADER *)tracebuf;
   short *d2 = (short *)(outMsg + sizeof(TRACE_HEADER));
   long  *d4 = (long  *)(outMsg + sizeof(TRACE_HEADER));

   int     bytesPerSamp;
   int     i, k;
   int     decMatch = 0;       /* Default is no match */
   char    *inMsg;
   int     chanLen = strlen( inHdr->chan );
   WORKBUF wb;
   double  samp;
   double  tsamp;
   long    newlen = 0;

/* Decide whether or not to decimate this tracebuf message.
   We will decimate if the component code of the tracebuf
   message matches inComp in the array of decimation
   structures, and if the data sample rate is within 1% of
   the configured sample rate.
   *******************************************************/
   for ( i = 0; i < nDec; i ++ )
   {
      int j;
      int compMatch = 1;                      /* Default is match */

      if ( Dec[i].decRate < 2 ) continue;     /* No decimation */

/* Skip this Dec entry if the sample rates don't match
   ***************************************************/
      if ( fabs( (inHdr->samprate - Dec[i].freq) / Dec[i].freq ) > 0.01 )
         continue;

/* Skip this Dec entry if it has a different length
   ************************************************/
      if ( strlen( Dec[i].inComp ) != (unsigned)chanLen ) continue;

/* Skip this Dec entry if any character in the component codes
   doesn't match.  inComp may contain wildcard (?) characters.
   ***********************************************************/
      for ( j = 0; j < chanLen; j++ )
      {
         char inCompChar = Dec[i].inComp[j];
         if ( (inCompChar != '?') && (inCompChar != inHdr->chan[j]) )
            { compMatch = 0; break; }        /* No comp code match */
      }

/* If the sample rate and comp code both match,
   don't look at any more Dec entries.
   *******************************************/
      if ( compMatch ) { decMatch = 1; break; }
   }
   if ( !decMatch ) return 0;                /* No decimation for this tracebuf msg */

/* Create a work buffer for the decimation filter.
   "i" is the Dec index, from above.
   **********************************************/
   wb.size  = Dec[i].length + (MAX_TRACEBUF_SIZ / 2);
   wb.nwork = 0;
   wb.twork = 0.0;

   wb.dsmp = (double *)calloc( wb.size, sizeof(double) );
   if ( wb.dsmp == NULL )
   {
      logit( "e", "cuspfeeder: Can't allocate decimation work buffer. Exiting.\n" );
      exit( -1 );
   }

/* Initialize the input pointer so getmsg() will
   return the first undecimated tracebuf message.
   *********************************************/
   InitInMsg( tracebuf );

/* Initialize the output pointer.  Decimated messages
   will overwrite the original, undecimated messages.
   **************************************************/
   InitOutMsg( tracebuf );

/* Copy trace header from first message to output buffer.
   Reset nsamp and sample rate.
   *****************************************************/
   memcpy( outMsg, tracebuf, sizeof(TRACE_HEADER) );
   outHdr->nsamp     = 0;
   outHdr->samprate /= Dec[i].decRate;
   bytesPerSamp = outHdr->datatype[1] - 48;

/* Set component code of outgoing tracebuf message to outComp.
   If a character of outComp is the wildcard (?) character,
   don't change that character in the tracebuf header.
   **********************************************************/
   for ( k = 0; k < chanLen; k++ )
   {
      char outCompChar = Dec[i].outComp[k];
      if ( outCompChar != '?' ) outHdr->chan[k] = outCompChar;
   }

/* Loop through messages obtained from tracebuf
   ********************************************/
   while ( 1 )
   {
      int gapflag;

/* Get next message from tracebuf
   ******************************/
      inMsg = GetMsg( actlen, &gapflag );
      if ( inMsg == NULL ) break;

/* If a gap occurred, flush the work buffer,
   and write any partial messages out to tracebuf
   **********************************************/
      if ( gapflag )
      {
         wb.nwork = 0;
         if ( outHdr->nsamp > 0 )
         {
/*          printf( "%3d  %.3lf  %.3lf\n", outHdr->nsamp, outHdr->starttime,
                    outHdr->endtime ); */
            PutMsg( outMsg, &newlen );
            outHdr->nsamp = 0;
         }
      }

/* Append samples to work buffer
   *****************************/
      if ( AppendMsgWb( inMsg, &wb ) == -1 )
      {
         logit( "e", "cuspfeeder: Can't append samples to workbuf. Exiting.\n" );
         exit( -1 );
      }

/* Enough samples to filter?
   ************************/
      while ( wb.nwork >= Dec[i].length )
      {
         FiltWbSamp( &wb, &Dec[i], &samp, &tsamp );

/* Append filtered sample to output message
   ****************************************/
         if ( bytesPerSamp == 2 )
            d2[outHdr->nsamp] = (short)NINT( samp );
         else if (bytesPerSamp == 4 )
            d4[outHdr->nsamp] = (long) NINT( samp );

/*       printf( "%2d", outHdr->nsamp );
         printf( "  %3d", d2[outHdr->nsamp] );
         printf( "  %.3lf\n", tsamp ); */

         outHdr->nsamp++;

/* Starting a new output tracebuf message
   **************************************/
         if ( outHdr->nsamp == 1 )
            outHdr->starttime = tsamp;

/* Current message end time is tsamp
   *********************************/
         outHdr->endtime = tsamp;

/* Write output message to tracebuf
   ********************************/
         if ( outHdr->nsamp == Dec[i].msgSize )
         {
/*          printf( "%3d  %.3lf  %.3lf\n", outHdr->nsamp, outHdr->starttime,
                    outHdr->endtime ); */
            PutMsg( outMsg, &newlen );
            outHdr->nsamp = 0;
         }
      }
   }

/* Write out the last, partially complete message to tracebuf
   **********************************************************/
   if ( outHdr->nsamp > 0 )
   {
/*    printf( "%3d  %.3lf  %.3lf\n", outHdr->nsamp, outHdr->starttime,
              outHdr->endtime ); */
      PutMsg( outMsg, &newlen );
   }

/* All went well.
   Return the cumulative length of decimated data.
   **********************************************/
   *actlen = newlen;
   free( wb.dsmp );
   return 0;
}
