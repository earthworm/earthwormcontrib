
              /********************************************************
               *                    sender_thread.c                   *
               ********************************************************/

#include <stdio.h>
#include <stdlib.h>                    /* calloc(), realloc() */
#include <malloc.h>                    /* for realloc() */
#include <string.h>
#include <earthworm.h>
#include <ws_clientII.h>
#include <swap.h>
#include "cuspfeeder.h"

/* Declared volatile in config.c
   *****************************/
extern char LocalTriggerDir[MAX_STRLEN]; /* For storage of temporary trigger files */
extern char LocalWaveformDir[MAX_STRLEN];/* For local storage of waveform files */
extern char RemoteIpAddress[MAX_STRLEN]; /* IP address of system to receive trigger */
extern int  WellKnownPort;         /* The well-known port number */
extern int  RetryInterval;         /* If send fails, retry this often (msec) */
extern unsigned char TypeError;
extern int  TraceBufferSize;       /* Size of trace buffer in bytes */
extern int  nWsv;                  /* Number of waveservers we know about */
extern WSV  Wsv[MAX_WSV];          /* Waveserver IP address and port numbers */
extern int  SendAllComponents;     /* If nonzero, send all components for each station */
extern int  nDec;                  /* Number of known decimation intervals */
extern DECSTR Dec[MAX_DEC];        /* Array of decimation intervals */
extern int  WaveformDest;          /* Where to ship final waveform file */

/* Function declarations
   *********************/
int  GetTrigList( char *, TRIGGER **, int *, unsigned * );
int  Build_Menu( WS_MENU_QUEUE_REC *, char *, char * );
int  RequestWave( WS_MENU_QUEUE_REC *, WS_PSCN, TRIGGER *, char [], long * );
void RequestDummy( TRIGGER *, char [], long * );
int  GetCuspConnection( void );
int  SendEventToCusp( FILE *, unsigned *, unsigned * );
void SendStatus( unsigned char, short, char * );
int  TraceMakeIBM( char *, long );
int  GetPropDelay( char *, char *, SCN *, int, TRIGGER *, int, double * );
int  TraceMakeLocal( char *, long );
int  SetDec( int );
int  FiltDecim( char *, long * );


      /********************************************************************
       *                           SenderThread()                         *
       *               Thread function invoked by cuspfeeder.             *
       ********************************************************************/

thr_ret SenderThread( void *junk )
{
   char    trigFileName[80];
   char    *tracebuf;
   TRIGGER *trigs = NULL;          /* List of triggers */
   SCN     *scns  = NULL;          /* List of SCNs obtained */
   int     i;
   int     maxFiltLen = 0;
   int     maxActLen  = 0;         /* Length in bytes of biggest trace */

/* Allocate the buffer to contain waveforms
   ****************************************/
   tracebuf = (char *) malloc( (size_t)TraceBufferSize );
   if ( tracebuf == NULL )
   {
      logit( "e", "SenderThread: Can't allocate trace buffer. Exiting.\n" );
      exit( -1 );
   }

/* Set up and log the decimation filters
   *************************************/
   for ( i = 0; i < nDec; i++ )
   {
      if ( SetDec( i ) == -1 )
      {
         logit( "e", "SenderThread: Call to SetDec() failed. Exiting.\n" );
         exit( -1 );
      }
   }

/* Change the working directory to LocalTriggerDir
   ***********************************************/
   if ( chdir_ew( LocalTriggerDir ) < 0 )
   {
      logit( "et", "SenderThread: chdir_ew error.\n" );
      exit( -1 );
   }
   if ( strlen( LocalWaveformDir ) == 0 ) 
   {
      strcpy( LocalWaveformDir, LocalTriggerDir );
   }

/* Initialize the socket system.  SocketSysInit() is just
   a stub in Solaris, but it does something in Windows NT.
   ******************************************************/
   SocketSysInit();

/* Wait for trigger files to show up.
   When one appears, read its contents into the trigger list.
   *********************************************************/
   while ( 1 )
   {
      int      nTrigger;
      unsigned event_id;

      while ( GetTrigList( trigFileName, &trigs, &nTrigger, &event_id )
              == CUSPFEEDER_SUCCESS )
      {
         char     saveFileName[MAX_STRLEN*2];
         char     eventFileName[MAX_STRLEN*2];
         FILE     *fp;
         int      i;
         int      j;                             /* Loop index */
         long     actLen;                        /* Actual length of trace data */
         unsigned byteCount = sizeof(unsigned);  /* Send this many bytes to Cusp */
         int      nScn = 0;                      /* This many SCNs have been obtained */

         logit( "e", "\n" );
         logit( "et", "Processing event %u\n", event_id );

/* Generate event waveform file name 
   *********************************/
         strcpy( eventFileName, LocalWaveformDir );
         strcat( eventFileName, "/event.tmp" );

/* Open a temporary event file on disk
   ***********************************/
         while ( 1 )
         {
            fp = fopen( eventFileName, "wb+" );
            if ( fp != NULL ) break;
            logit( "e", "Can't create temporary event file.  Will try again in 60 sec.\n" );
            sleep_ew( 60000 );
         }

/* Search all available waveservers for the desired channels
   *********************************************************/
         for ( i = 0; i < nWsv; i++ )
         {
            WS_MENU_QUEUE_REC queue;                /* Points to the list of menus */
            WS_PSCN           scnp;                 /* Points to an individual menu */
            double            propDelay;

            logit( "e", "Cuspfeeder connecting to waveserver: %-15s port %5s.\n",
                   Wsv[i].ip, Wsv[i].port );

/* If the desired traces haven't showed up yet, sleep for a while.
   GetPropDelay() estimates the delay needed to allow data from the
   end of the desired time window will show up at the waveservers.
   An additional time of ChunkDelay allows for the fact that tracebuf
   data arrives in chunks (messages) rather than continuously.
   ******************************************************************/
            if ( GetPropDelay( Wsv[i].ip, Wsv[i].port, scns, nScn, trigs, nTrigger,
                               &propDelay ) == CUSPFEEDER_FAILURE )
               continue;

            if ( propDelay > 0.0 )
            {
               extern int ChunkDelay;
               double totalDelay = propDelay + ChunkDelay;
               logit( "e", "Waiting %.2lf sec for the traces to show up...\n", totalDelay );
               sleep_ew( (int)(1000. * totalDelay) );
            }

/* Build_Menu() calls wsAppendMenu() for the specified waveserver
   **************************************************************/
            queue.head = queue.tail = NULL;
            if ( Build_Menu( &queue, Wsv[i].ip, Wsv[i].port ) == CUSPFEEDER_FAILURE )
               goto GET_NEXT_WAVESERVER;

/* Get a pointer to the pscn list for the waveserver of interest
   *************************************************************/
            if ( wsGetServerPSCN( Wsv[i].ip, Wsv[i].port, &scnp, &queue ) ==
                 WS_ERR_EMPTY_MENU )
               goto GET_NEXT_WAVESERVER;
 
/* Traverse the waveserver-menu linked-list to find out
   which SCNs are being served
   ****************************************************/
            while ( 1 )
            {
               int status;
               int wantIt = 0;    /* Non-zero if we want this SCN */

/* Have we already gotten this SCN from a waveserver?
   If so, don't get it again.
   *************************************************/
               for ( j = 0; j < nScn; j++ )
                  if ( strcmp( scnp->sta,  scns[j].sta ) == 0 &&
                       strcmp( scnp->chan, scns[j].cmp ) == 0 &&
                       strcmp( scnp->net,  scns[j].net ) == 0 )
                     goto GET_NEXT_MENU_ENTRY;

/* Do we want this SCN?  We want it if the waveserver SCN matches
   an SCN in the trigger list.  Wildcards are allowed in trigger
   list SCNs.  The index of the matching trigger (j) is used below.
   ***************************************************************/
               for ( j = 0; j < nTrigger; j++ )
               {
                  int sta_ok = strcmp( trigs[j].sta, "*"        ) == 0 ||
                               strcmp( trigs[j].sta, scnp->sta  ) == 0;
                  int cmp_ok = strcmp( trigs[j].cmp, "*"        ) == 0 ||
                               strcmp( trigs[j].cmp, scnp->chan ) == 0;
                  int net_ok = strcmp( trigs[j].net, "*"        ) == 0 ||
                               strcmp( trigs[j].net, scnp->net  ) == 0;

                  if ( wantIt = sta_ok && cmp_ok && net_ok )
                     break;
               }

/* We want this SCN.  Is there room in the scns array?
   If not, realloc the scns array.
   **************************************************/
               if ( wantIt )
               {
                     static int MaxScn = 0;           /* Size of scns array */

                     if ( nScn >= MaxScn )
                     {
                        const int MaxScnIncr = 50;
                        int newSize = (MaxScn+MaxScnIncr) * sizeof(SCN);
                        SCN *newPtr = (SCN *) realloc( scns, newSize );

                        if ( newPtr == NULL )
                        {
                           char errtxt[] = "Can't realloc the scns[] array\n";
                           SendStatus( TypeError, ERR_NOREALLOC, errtxt );
                           logit( "et", "%s", errtxt );
                           break;                /* Done with this waveserver */
                        }
                        scns = newPtr;
                        MaxScn += MaxScnIncr;
/*                      logit( "t", "New value of MaxScn:  %d\n", MaxScn ); */ /*DEBUG*/
                     }

/* Request this channel from waveserver
   ************************************/
                  logit( "e", "Requesting %-4s %s %s  %04d%02d%02d %02d:%02d:%5.2lf %3d sec. ",
                          scnp->sta, scnp->chan, scnp->net,
                          trigs[j].ssYear, trigs[j].ssMonth, trigs[j].ssDay,
                          trigs[j].ssHour, trigs[j].ssMinute, trigs[j].ssSecond,
                          trigs[j].duration );

                  status = RequestWave( &queue, scnp, &trigs[j], tracebuf, &actLen );

/* If we got the channel, convert data samples to IBM format.
   This is the native format of DEC VAX computers.
   Then, write the tracebuf message to the temporary event file.
   ************************************************************/
                  if ( status == GOT_IT )             /* Got the requested trace */
                  {
                     logit( "e", "Got it. actLen: %6d", actLen );

                     if ( nDec > 0 )                  /* Decimate trace */
                     {
                        if ( TraceMakeLocal( tracebuf, actLen ) < 0 )
                        {
                           logit( "e", "\nTraceMakeLocal() error.\n" );
                           goto GET_NEXT_MENU_ENTRY;
                        }
                        FiltDecim( tracebuf, &actLen );
                        logit( "e", " decLen: %d", actLen );
                     }
                     logit( "e", "\n" );
                
                     TraceMakeIBM( tracebuf, actLen );

                     if ( fwrite( tracebuf, sizeof(char), actLen, fp ) < (size_t)actLen )
                        logit( "e", "Error writing temporary event file.\n" );
                     byteCount += (unsigned)actLen;

/* Enter the SCN in the list of received SCNs
   ******************************************/
                     strcpy( scns[nScn].sta, scnp->sta  );
                     strcpy( scns[nScn].cmp, scnp->chan );
                     strcpy( scns[nScn].net, scnp->net  );
                     nScn++;

/* Flag the trigger.  If a trigger never gets flagged, we will
   log the fact after all waveservers have been checked.
   ***********************************************************/
                     trigs[j].status = GOT_IT;

/* If maxActLen increased, log the new value
   *****************************************/
                     if ( actLen > maxActLen )
                     {
                        maxActLen = actLen;
                        logit( "t", "New value of maxActLen: %d\n", maxActLen );
                     }
                  }

/* The trace buffer is too small.
   This is serious.  Send a message to statmgr.
   *******************************************/
                  else if ( status == BUFFER_OVERFLOW )
                  {
                     char errtxt[] = "Buffer overflow. Increase TraceBufferSize in cuspfeeder.d";

                     SendStatus( TypeError, ERR_EVENT_TOO_BIG, errtxt );
                     logit( "e", "Trace buffer overflow.  Skipping to next waveserver.\n" );
                     goto GET_NEXT_WAVESERVER;
                  }

                  else if ( status == NOT_AVAILABLE )
                     logit( "e", "Not available.\n" );

                  else if ( status == BAD_CONNECTION )
                     logit( "e", "Bad connection.\n" );

                  else
                     logit( "e", "\nUnknown value returned by RequestWave: %d\n",
                            status );
               }                       /* End of loop over wanted channels */

/* Are we finished with this waveserver?
   If not, get the next SCN from its menu.
   **************************************/
GET_NEXT_MENU_ENTRY:
               if ( scnp->next == NULL ) break;
               scnp = scnp->next;
            }                          /* End of loop over waveserver menu entries */

GET_NEXT_WAVESERVER:
            wsKillMenu( &queue );      /* Close connection and kill menu queue */
         }                             /* End of loop over waveservers */

/* Log any SCNs in the trigger list that we
   couldn't get from the waveservers
   ****************************************/
         for ( j = 0; j < nTrigger; j++ )
            if ( trigs[j].status != GOT_IT )
            {
               if ( ( strcmp(trigs[j].sta, "*") != 0 ) &&
                    ( strcmp(trigs[j].cmp, "*") != 0 ) &&
                    ( strcmp(trigs[j].net, "*") != 0 ) )
                  logit( "et", "cuspfeeder: Couldn't get %s %s %s from waveservers.\n",
                         trigs[j].sta, trigs[j].cmp, trigs[j].net );

/* Create dummy snippets for missing IRIGE
   channels, but not for any other channels.
   ****************************************/
               if ( strncmp( trigs[j].sta, "IRG", 3 ) == 0 )
               {
                  printf( "Creating dummy trace" );
                  printf( " %s %s %s %02d%02d%02d %02d:%02d:%5.2lf %d sec. ",
                          trigs[j].sta, trigs[j].cmp, trigs[j].net,
                          trigs[j].ssYear, trigs[j].ssMonth, trigs[j].ssDay,
                          trigs[j].ssHour, trigs[j].ssMinute, trigs[j].ssSecond,
                          trigs[j].duration );

                  RequestDummy( &trigs[j], tracebuf, &actLen );

                  if ( nDec > 0 )                 /* Decimate trace */
                  {
                     TraceMakeLocal( tracebuf, actLen );
                     FiltDecim( tracebuf, &actLen );
                  }

                  TraceMakeIBM( tracebuf, actLen );

                  printf( "actLen: %d\n", actLen );
                  if ( fwrite( tracebuf, sizeof(char), actLen, fp ) < (size_t)actLen )
                     logit( "e", "Error writing temporary event file.\n" );
                  byteCount += (unsigned)actLen;
               }
            }

/* Send the temporary event file to Cusp.
   If we can't connect, keep retrying forever.
   If the first connection attempt fails, send a status message.
   If a later connection attempt succeeds, send another status message.
   *******************************************************************/
         if ( WaveformDest==DEST_CUSPONLY || 
              WaveformDest==DEST_CUSPANDLOCAL )
         {
            if ( byteCount > 0 )         /* If we have some data to send */
            {
               while ( 1 )               /* Try forever */
               {
                  char errtxt[80];
                  int  nConnectTry = 0;  /* Number of connection attempts */

                  while ( GetCuspConnection() == CUSPFEEDER_FAILURE )
                  {
                     sprintf( errtxt, "Can't connect to Cusp: %s port %d\n",
                              RemoteIpAddress, WellKnownPort );

                     if ( ++nConnectTry == 1 )        /* First connection failure */
                        SendStatus( TypeError, ERR_NOCONNECT, errtxt );
                     else
                        logit( "et", "%s", errtxt );
                     sleep_ew( 1000 * RetryInterval );
                  }

                  if ( ++nConnectTry > 1 )       /* Connection after some failures */
                  {
                     sprintf( errtxt, "Established connection to Cusp: %s port %d\n",
                              RemoteIpAddress, WellKnownPort );
                     SendStatus( TypeError, ERR_NOCONNECT, errtxt );
                  }
                  else
                     logit( "e", "Connection to Cusp succeeded.\n" );

                  logit( "e", "Sending event to Cusp.\n" );
                  logit( "e", "This may take a while, depending on event size.\n" );

                  if ( SendEventToCusp( fp, &byteCount, &event_id ) ==
                       CUSPFEEDER_SUCCESS )
                     break;

                  logit( "et", "Error sending event to Cusp: %s port %d\n",
                              RemoteIpAddress, WellKnownPort );
                  sleep_ew( 1000 * RetryInterval );
               }

               logit( "et", "Event %u sent to remote system (%u bytes)\n",
                      event_id, byteCount );
            }
         } /* end if sending to CUSP */

/* Close event file; erase it if destination is CUSP only;
   ******************************************************/
         fclose( fp );
         if( WaveformDest == DEST_CUSPONLY ) 
         {
            if ( remove( eventFileName ) == -1 )
               logit( "et", "cuspfeeder: Error erasing temporary event file: %s\n",
                       eventFileName );
         }

/* Or rename event file if we're keeping it around
   ***********************************************/
         else 
         {
            sprintf( saveFileName, "%s/%u.tbuf", LocalWaveformDir, event_id );
    
            if ( rename_ew( eventFileName, saveFileName ) != 0 ) 
            {
               logit( "et", "cuspfeeder: Error renaming temporary event file: %s"
                      "to %s\n", eventFileName, saveFileName );
            } else {
               logit( "et", "cuspfeeder: Event waveforms saved in: %s\n",
                       saveFileName );
            }
         }

/* Rename the trigger file to trigsave.xxx
   ***************************************/
         strcpy( saveFileName, "trigsave." );
         strcat( saveFileName, &trigFileName[9] );

         if ( rename_ew( trigFileName, saveFileName ) != 0 )
            logit( "et", "cuspfeeder: Error renaming trigger file: %s\n", 
                    trigFileName );
         else
            printf( "cuspfeeder: Trigger file <%s> renamed <%s>\n", 
                     trigFileName, saveFileName );

      }                     /* End while(GetTriglist()) */
      sleep_ew( 1000 );     /* Wait for more trigger files to show up */
   }                        /* End while(1) */
}
