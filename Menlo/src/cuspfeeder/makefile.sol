#
#                    Make file for cuspfeeder
#                         Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = cuspfeeder.o config.o sender_thread.o sendwave_sol.o getwaves.o \
    gettrig.o makeIBM.o filtdecim.o makeLocal.o setdec.o remeznp.o \
    zeroes.o hqr.o getmsg.o putmsg.o workbuf.o getfilename_sol.o \
    $L/logit_mt.o $L/kom.o $L/getutil.o $L/sleep_ew.o $L/time_ew.o \
    $L/transport.o $L/threads_ew.o $L/ws_clientII.o \
    $L/socket_ew.o $L/socket_ew_common.o $L/chron3.o $L/swap.o \
    $L/parse_trig.o $L/sema_ew.o $L/dirops_ew.o

cuspfeeder: $O
	cc -o $B/cuspfeeder $O -mt -lm -lsocket -lnsl -lposix4 -lthread -lc

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/cuspfeeder*
