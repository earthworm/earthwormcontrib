
                            /**********************
                             *     gettrig.c      *
                             **********************/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>      /* For malloc(),realloc() */
#include <malloc.h>      /* For realloc() */
#include <chron3.h>
#include <earthworm.h>
#include <parse_trig.h>
#include "cuspfeeder.h"

/* Function prototype
   ******************/
void SendStatus( unsigned char, short, char * );


     /***************************************************************
      *                       ParseTrigbuf()                        *
      *                                                             *
      *           Function to decode one trigger line.              *
      *                                                             *
      *  Returns CUSPFEEDER_FAILURE or CUSPFEEDER_SUCCESS           *
      ***************************************************************/

int ParseTrigbuf( char *trigbuf, char **nxtLine, TRIGGER *trig, unsigned *eventId )
{
   char    token[5];
   int     seconds;
   SNIPPET snip;
   int     first = (trigbuf == *nxtLine) ? 1 : 0;

   if ( parseSnippet( trigbuf , &snip , nxtLine ) == EW_FAILURE )
      return CUSPFEEDER_FAILURE;

   strcpy( trig->sta, snip.sta );
   strcpy( trig->cmp, snip.chan );
   strcpy( trig->net, snip.net );

   strncpy( token, snip.startYYYYMMDD, 4 );
   token[4] = '\0';
   trig->ssYear = atoi( token );

   strncpy( token, snip.startYYYYMMDD+4, 2 );
   token[2] = '\0';
   trig->ssMonth = atoi( token );

   strncpy( token, snip.startYYYYMMDD+6, 2 );
   token[2] = '\0';
   trig->ssDay = atoi( token );

/* WARNING: snip.startHHMMSS is apparently unterminated!
            Looks like a bug in parsetrig.
   ****************************************************/
/* printf( "snip.startHHMMSS: %s\n", snip.startHHMMSS ); */

   strncpy( token, snip.startHHMMSS, 2 );
   token[2] = '\0';
   trig->ssHour = atoi( token );

   strncpy( token, snip.startHHMMSS+3, 2 );
   token[2] = '\0';
   trig->ssMinute = atoi( token );

   strncpy( token, snip.startHHMMSS+6, 2 );
   token[2] = '\0';
   seconds = atoi( token );
   trig->ssSecond = seconds + snip.starttime - floor(snip.starttime);

   trig->stime    = snip.starttime;
   trig->duration = snip.duration;
   trig->status   = HAVENT_GOTTEN_IT;                 /* Waveform not yet obtained */

/* snip.eventId is only set the first time parseSnippet is called
   **************************************************************/
   if ( first && sscanf( snip.eventId, "%u", eventId ) < 1 )
   {
      logit( "et", "ParseTrigbuf: snip.eventId is non-integer: %s\n", snip.eventId );
      return CUSPFEEDER_FAILURE;
   }

   return CUSPFEEDER_SUCCESS;
}


     /********************************************************************
      *                          GetTrigList()                           *
      *                                                                  *
      *  Read everything from the trigger file into the trigger list.    *
      *                                                                  *
      *  Accepts:                                                        *
      *    trigFileName = Name of trigger file                           *
      *  Returns:                                                        *
      *    CUSPFEEDER_SUCCESS if all went well                           *
      *    CUSPFEEDER_FAILURE if we ran into problems                    *
      *    trigs    = Array of trigger structures, to be filled with     *
      *               info from the trigger file                         *
      *    nTrigger = Number of triggers read                            *
      *    event_id = Event id obtained from the trigger list header     *
      ********************************************************************/

int GetTrigList( char *trigFileName, TRIGGER **trigsPtr, int *nTrigger,
                 unsigned *event_id )
{
   extern unsigned char TypeError;   /* Message type; get from elsewhere */

   int         GetFname( char [] );

   FILE        *fp;
   int         i;
   int         msgsize = 0;
   int         ntrig   = 0;
   TRIGGER     trig;
   char        *nxtLine;
   unsigned    eventId;
   TRIGGER     *trigs = *trigsPtr;
   static int  firstCall = 1;     /* =1 the first time this function is called */
   static char *trigbuf;          /* Don't discard this pointer */
   int         firstLoop = 1;

/* Allocate the triglist message buffer.
   Max size of carltrig triglist messages is MAX_BYTES_PER_EQ.
   **********************************************************/
   if ( firstCall )
   {
      trigbuf = (char *)malloc( (size_t)MAX_BYTES_PER_EQ );
      if ( trigbuf == NULL )
      {
         logit( "e", "GetTrigList: Can't allocate triglist message buffer. Exiting.\n" );
         exit( -1 );
      }
      firstCall = 0;
   }

/* See if a trigger file has appeared.
   If not, return right away.
   **********************************/
   if ( GetFname( trigFileName ) == CUSPFEEDER_FAILURE )
      return CUSPFEEDER_FAILURE;

/* Open the trigger file
   *********************/
   fp = fopen( trigFileName, "rb" );
   if ( fp == NULL )                      /* This will probably never occur */
   {
      logit( "et", "GetTrigList: Can't open trigger file: %s\n", trigFileName );
      return CUSPFEEDER_FAILURE;
   }

/* Read the entire trigger list into trigbuf
   *****************************************/
   msgsize = fread( trigbuf, sizeof(char), MAX_BYTES_PER_EQ, fp );

   if ( ferror( fp ) )
   {
      logit( "et", "GetTrigList: Error reading trigger file: %s\n", trigFileName );
      fclose( fp );
      return CUSPFEEDER_FAILURE;
   }

   if ( !feof( fp ) )
   {
      logit( "et", "GetTrigList: Triglist buffer too small.\n" );
      fclose( fp );
      return CUSPFEEDER_FAILURE;
   }

   fclose( fp );

/* Null-terminate the message
   **************************/
   if ( msgsize < MAX_BYTES_PER_EQ )
      trigbuf[msgsize++] = '\0';
   else
      trigbuf[msgsize-1] = '\0';

/* logit( "e", "Contents of trigger file:\n%s", trigbuf );*/   /* DEBUG */

/* Get the next trigger from the trigger message.
   The event id returned here is bogus.
   *********************************************/
   nxtLine = trigbuf;
   while ( ParseTrigbuf( trigbuf , &nxtLine, &trig, &eventId ) == CUSPFEEDER_SUCCESS )
   {
      int duplicate = FALSE;
      static int MaxTrig = 0;          /* Max number of triggers in trigs array */

/* The event id is returned along with the first trigger
   *****************************************************/
/*    logit("e","firstloop:%d SCN: %s.%s.%s\n",
             firstLoop,trig.sta,trig.cmp,trig.net); */ /*DEBUG*/
      if ( firstLoop )
      {
      /* logit( "et", "eventId: %d\n", eventId ); */ /*DEBUG*/
         *event_id = eventId;          /* Return event id to caller */
         firstLoop = 0;
      }

/* If the trigger is a duplicate, discard it
   *****************************************/
      for ( i = 0; i < ntrig; i++ )
         if ( strcmp(trig.sta, trigs[i].sta) == 0 &&
              strcmp(trig.cmp, trigs[i].cmp) == 0 &&
              strcmp(trig.net, trigs[i].net) == 0 )
            duplicate = TRUE;
      if ( duplicate )
         continue;

/* Make sure the trigger list is big enough for one more trigger.
   If not, realloc some more array space.
   *************************************************************/
      if ( ntrig >= MaxTrig )
      {
         const int MaxTrigIncr = 50;
         int newSize = (MaxTrig+MaxTrigIncr) * sizeof(TRIGGER);
         TRIGGER *newPtr;
         newPtr = (TRIGGER *) realloc( trigs, newSize );

         if ( newPtr == NULL )
         {
            char errtxt[] = "Can't realloc the trigs[] array\n";
            SendStatus( TypeError, ERR_NOREALLOC, errtxt );
            logit( "et", "%s", errtxt );
            break;
         }
         *trigsPtr = trigs = newPtr;   /* Return new array address to caller */
         MaxTrig += MaxTrigIncr;
      /* logit( "et", "New value of MaxTrig:  %d\n", MaxTrig ); */ /*DEBUG */
      }

/* Append trigger to trigger list
   ******************************/
      trigs[ntrig++] = trig;
   }

/* If any one of the SCNs in the trigger list is a wildcard,
   ie * * *, discard all other SCNs from the list.
   ********************************************************/
   for ( i = 0; i < ntrig; i++ )
      if ( strcmp(trigs[i].sta, "*") == 0 &&
           strcmp(trigs[i].cmp, "*") == 0 &&
           strcmp(trigs[i].net, "*") == 0 )
      {
         trigs[0] = trigs[i];
         ntrig = 1;
         break;
      }

   *nTrigger = ntrig;                        /* Return the number of triggers found */
   return CUSPFEEDER_SUCCESS;
}
