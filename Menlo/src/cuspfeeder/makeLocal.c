
/*
 * makeLocal.c
*/

#include <string.h>
#include <trace_buf.h>
#include <swap.h>


  /*************************************************************************
   *                           TraceMakeLocal()                            *
   *                                                                       *
   *  Convert a sequence of tracebuf messages to local byte order.         *
   *                                                                       *
   *  Accepts:                                                             *
   *     tracebuf = Array containing sequence of tracebuf messages.        *
   *     actLen   = Combined length of tracebuf messages, in bytes.        *
   *                                                                       *
   *  Function return value:                                               *
   *     -1 on error                                                       *
   *      0 otherwise                                                      *
   *************************************************************************/

int TraceMakeLocal( char *tracebuf, long actLen )
{
   int  inLen = 0;
   char *ptr  = tracebuf;              /* Pointer to tracebuf messages */
   int  nsamp;
   TRACE_HEADER *th;

/* Loop through all tracebuf messages
   **********************************/
   while ( inLen < actLen )
   {
      int bytesPerSample;
      int msgLen = sizeof(TRACE_HEADER);

/* Convert one tracebuf message to local byte order.
   Returns -1, if unknown data type, 0 otherwise.
   ************************************************/
      th = (TRACE_HEADER *)ptr;
      if ( WaveMsgMakeLocal( th ) == -1 ) return -1;

/* Calculate message length
   ************************/
      bytesPerSample = th->datatype[1] - 48;
      if ( bytesPerSample != 2 && bytesPerSample != 4 ) return -1;
      memcpy( &nsamp, &th->nsamp, sizeof(int) );
      msgLen += bytesPerSample * nsamp;

      ptr   += msgLen;
      inLen += msgLen;
   }
   return 0;
}
