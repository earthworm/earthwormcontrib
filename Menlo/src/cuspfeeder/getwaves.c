
    /***********************************************************************
     *                              getwaves.c                             *
     *                                                                     *
     ***********************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <earthworm.h>
#include <ws_clientII.h>
#include <trace_buf.h>
#include "cuspfeeder.h"

/* Config file parameters
   **********************/
extern long wsTimeout;              /* Msec to wait for reply from ws */
extern int  nWsv;                   /* Number of waveservers we know about */
extern WSV  Wsv[MAX_WSV];           /* Waveserver IP address and port numbers */
extern int  TraceBufferSize;        /* Size of trace buffer in bytes */


    /*******************************************************************
     *                          Build_Menu ()                          *
     *           Attach to one waveserver and get its menu.            *
     *                                                                 *
     *  Returns: CUSPFEEDER_FAILURE if the connection failed           *
     *           CUSPFEEDER_SUCCESS if the connection succeeded        *
     *******************************************************************/

int Build_Menu( WS_MENU_QUEUE_REC *menu_queue, char *ip, char *port )
{
   int rc;
   int got_connection = CUSPFEEDER_FAILURE;

   rc = wsAppendMenu( ip, port, menu_queue, wsTimeout );

   if ( rc == WS_ERR_NO_CONNECTION )
      logit( "e", "Error. Could not connect.\n" );

   else if ( rc == WS_ERR_SOCKET )
      logit( "e", "Error. Could not create a socket\n" );

   else if ( rc == WS_ERR_BROKEN_CONNECTION )
      logit( "e", "Error. Connection broke during wsAppendMenu\n" );

   else if ( rc == WS_ERR_TIMEOUT )
      logit( "e", "Error. Connection timed out while getting menu.\n" );

   else if ( rc == WS_ERR_MEMORY )
      logit( "e", "Error. Waveserver out of memory.\n" );

   else if ( rc == WS_ERR_INPUT )
      logit( "e", "Error. Connection input error\n" );

   else if ( rc == WS_ERR_PARSE )
      logit( "e", "Error. Parser failed.\n" );

   else if ( rc == WS_ERR_BUFFER_OVERFLOW )
      logit( "e", "Error. Buffer overflowed.\n" );

   else if ( rc == WS_ERR_EMPTY_MENU )
      logit( "e", "Error. Menu is empty.\n" );

   else if ( rc == WS_ERR_NONE )
      got_connection = CUSPFEEDER_SUCCESS;

   else
      logit( "e", "Error. Unknown return from wsAppendMenu(). rc: %d\n", rc );

   return got_connection;
}


    /********************************************************************
     *                           RequestWave()                          *
     *                    Get trace data for one SCN.                   *
     *                                                                  *
     *  Accepts:                                                        *
     *    scnp: Pointer to a waverserver menu entry                     *
     *    trig: Pointer to a trigger structure, containing time and     *
     *          duration of snippet to save                             *
     *                                                                  *
     *  Returns:                                                        *
     *    GOT_IT:          Got the requested trace.                     *
     *    NOT_AVAILABLE:   There is no hope for getting this trace.     *
     *    BAD_CONNECTION:  Bad waveserver connection.                   *
     *    BUFFER_OVERFLOW: tracebuf is too small.                       *
     *                                                                  *
     *    tracebuf: Buffer to contain data received from waveserver     *
     *    actLen:   If successful retrieval, the actual length in       *
     *              bytes is returned here                              *
     ********************************************************************/

int RequestWave( WS_MENU_QUEUE_REC *menu_queue, WS_PSCN scnp, TRIGGER *trig,
                 char tracebuf[], long *actLen )
{
   TRACE_REQ request;
   int       rc;

/* Fill in the wave_client request structure
   *****************************************/
   strcpy( request.sta,  scnp->sta );
   strcpy( request.chan, scnp->chan );
   strcpy( request.net,  scnp->net );

   request.pinno        = 0;
   request.reqStarttime = trig->stime;
   request.reqEndtime   = request.reqStarttime + (double)trig->duration;
   request.pBuf         = tracebuf;
   request.bufLen       = TraceBufferSize;
   request.timeout      = wsTimeout;

/* logit( "e", "Issuing wsGetTraceBin request for %s %s %s",
           scnp->sta, scnp->chan, scnp->net );
   logit( "e", " reqStart:%.3lf reqEnd:%.3lf\n",
           request.reqStarttime, request.reqEndtime ); */

/* Try to get the trace
   ********************/
   rc = wsGetTraceBin( &request, menu_queue, wsTimeout );

   if ( rc == WS_ERR_NONE )
   {
      double delay = request.reqEndtime - request.actEndtime;

/*    logit( "e", "wsGetTraceBin returns WS_ERR_NONE." );
      logit( "e", " actLen=%ld",     request.actLen );
      logit( "e", " actStart=%.3lf", request.actStarttime );
      logit( "e", " actEnd=%.3lf\n", request.actEndtime ); */

      *actLen = request.actLen;
      trig->status = GOT_IT;
   }

/* If rc is WS_WRN_FLAGGED, and...
   1. if retFlag is 'L', request falls entirely left of tank range.
   2. if retFlag is 'R', request falls entirely right of tank range.
   3. if retFlag is 'G', request falls entirely in a gap.
   ****************************************************************/
   else if ( rc == WS_WRN_FLAGGED )
   {
      logit( "e", "Waveserver returned WS_WRN_FLAGGED. retFlag: %c\n",
             request.retFlag );
      trig->status = NOT_AVAILABLE;
   }
   else if ( rc == WS_ERR_EMPTY_MENU )
   {
      logit( "e", "Empty menu.\n" );
      trig->status = NOT_AVAILABLE;
   }
   else if ( rc == WS_ERR_SCN_NOT_IN_MENU )
   {
      logit( "e", "SCN not in menu.\n" );
      trig->status = NOT_AVAILABLE;
   }
   else if ( rc == WS_ERR_NO_CONNECTION )             /* Socket already closed */
   {
      logit( "e", "No connection to wave server.\n" );
      trig->status = BAD_CONNECTION;
   }
   else if ( rc == WS_ERR_BUFFER_OVERFLOW )
   {
      logit( "e", "Trace buffer overflowed.\n" );
      trig->status = BUFFER_OVERFLOW;
   }
   else if ( rc == WS_ERR_TIMEOUT )
   {
      logit( "e", "Waveserver timed out.\n" );
      trig->status = BAD_CONNECTION;
   }
   else if ( rc == WS_ERR_BROKEN_CONNECTION )
   {
      logit( "e", "Broken connection to waveserver.\n" );
      trig->status = BAD_CONNECTION;
   }
   else if ( rc == WS_ERR_SOCKET )             /* Problem changing socket options */
   {
      logit( "e", "No connection to wave server.\n" );
      trig->status = BAD_CONNECTION;
   }
   else
   {
      logit( "e", "Unknown return code from wsGetTraceBin(): %d\n", rc );
      trig->status = NOT_AVAILABLE;
   }
   return trig->status;
}


    /********************************************************************
     *                          RequestDummy()                          *
     *            Build a dummy trace for those unavailable.            *
     *                                                                  *
     *  This is used to make certain that exactly nIrige IRIGE traces   *
     *  get sent, even if some of them are dummies.                     *
     *  RequestDummy builds the dummy trace in local byte order.        *
     *                                                                  *
     *  actLen: The actual trace length in bytes is returned here.      *
     *                                                                  *
     ********************************************************************/

void RequestDummy( TRIGGER *trig, char tracebuf[], long *actLen )
{
   TRACE_HEADER trh;                 /* working tracebuf header */
   char *tin             = tracebuf; /* working ptr into tracebuf */
   const int pinno       = -1;       /* Pin number */
   const int nsamp       = 100;      /* Samples per message */
   const double srate    = 100.0;    /* Nominal sample rate */
#ifdef _SPARC
   const char datatype[] = "s2";     /* Format code */
#else
   const char datatype[] = "i2";     /* Format code */
#endif

   const int nDataBytes = nsamp * (datatype[1] - 48);

   int len = 0;
   double stime = trig->stime;
   double trace_endtime = stime + (double)trig->duration;

/* Fill in unchanging parts of the dummy header
 **********************************************/
    trh.pinno    = pinno;
    trh.nsamp    = nsamp;
    trh.samprate = srate;
    strcpy( trh.sta,  trig->sta );
    strcpy( trh.net,  trig->net );
    strcpy( trh.chan, trig->cmp );
    strcpy( trh.datatype, datatype );
    memset( trh.quality, 0, 2 );         /* Two null bytes */
    memset( trh.pad,     0, 2 );         /* Two null bytes */

/* Loop over tracebuf messages
   ***************************/
   do
   {

/* Fill in header start/end times; copy to target trace
   ****************************************************/
      trh.starttime = stime;
      trh.endtime   = stime + (double)(nsamp-1) / srate;
      memcpy( tin, (char *)&trh, sizeof(TRACE_HEADER) );
      tin   += sizeof(TRACE_HEADER);
      len   += sizeof(TRACE_HEADER);
      stime += (double)nsamp/srate;

/* Append dummy data samples to the trace
   **************************************/
      memset( tin, 0, nDataBytes );   /* Set to all zeros */
      tin += nDataBytes;
      len += nDataBytes;

   } while ( trh.endtime < trace_endtime );

   *actLen = len;                    /* Return the actual snippet size */
   return;
}


      /********************************************************************
       *                          GetPropDelay()                          *
       *                                                                  *
       *  See if enough time has passed after an event for the waveform   *
       *  data to show up at a waveserver process.                        *
       *                                                                  *
       *  propDelay = Time in seconds to wait for data to arrive at the   *
       *              waveserver systems.                                 *
       *  Returns CUSPFEEDER_SUCCESS or CUSPFEEDER_FAILURE                *
       ********************************************************************/

int GetPropDelay( char *ip, char *port, SCN *scns, int nScn, TRIGGER *trigs,
                  int nTrigger, double *propDelay )
{
   WS_MENU_QUEUE_REC queue;                /* Points to the list of menus */
   WS_PSCN           scnp;                 /* Points to an individual menu */
   double            mxd = 0.0;            /* Maximum delay time */
   extern int        MaxWait;              /* Max time in sec to wait for waveforms */

/* Build_Menu() calls wsAppendMenu() for the specified waveserver
   **************************************************************/
   queue.head = queue.tail = NULL;
   if ( Build_Menu( &queue, ip, port ) == CUSPFEEDER_FAILURE )
   {
      wsKillMenu( &queue );
      return CUSPFEEDER_FAILURE;
   }

/* Get a pointer to the pscn list for this waveserver
   **************************************************/
   if ( wsGetServerPSCN( ip, port, &scnp, &queue ) == WS_ERR_EMPTY_MENU )
      return CUSPFEEDER_FAILURE;

/* Traverse the waveserver-menu linked-list to find out
   which SCNs are being served
   ****************************************************/
   while ( 1 )
   {
      int wantIt = 0;    /* Non-zero if we want this SCN */
      int j;             /* Loop index */

/* Have we already gotten this SCN from a waveserver?
   If so, don't get it again.
   *************************************************/
      for ( j = 0; j < nScn; j++ )
         if ( strcmp( scnp->sta,  scns[j].sta ) == 0 &&
              strcmp( scnp->chan, scns[j].cmp ) == 0 &&
              strcmp( scnp->net,  scns[j].net ) == 0 )
            goto GET_NEXT_MENU_ENTRY;

/* Do we want this SCN?  We want it if the waveserver SCN matches
   an SCN in the trigger list.  Wildcards are allowed in trigger
   list SCNs.  The index of the matching trigger (j) is used below.
   ***************************************************************/
      for ( j = 0; j < nTrigger; j++ )
      {
         int sta_ok = strcmp( trigs[j].sta, "*"        ) == 0 ||
                      strcmp( trigs[j].sta, scnp->sta  ) == 0;
         int cmp_ok = strcmp( trigs[j].cmp, "*"        ) == 0 ||
                      strcmp( trigs[j].cmp, scnp->chan ) == 0;
         int net_ok = strcmp( trigs[j].net, "*"        ) == 0 ||
                      strcmp( trigs[j].net, scnp->net  ) == 0;

         if ( wantIt = sta_ok && cmp_ok && net_ok )
            break;
      }

/* If we want this channel, calculate how long until the waveforms
   show up.  The time to sleep is the maximum of the individual
   channel delays.  If an individual delay is greater than MaxWait,
   we assume the channel is dead and we ignore it.
   ***************************************************************/
      if ( wantIt )
      {
         double endtime = trigs[j].stime + (double)trigs[j].duration;
         double delay   = endtime - scnp->tankEndtime;

/*       printf( "channel delay: %.2lf\n", delay ); */

         if ( (delay > mxd) && (delay < MaxWait) ) mxd = delay;
      }

/* Are we finished with this waveserver?
   If not, get the next SCN from its menu.
   **************************************/
GET_NEXT_MENU_ENTRY:
      if ( scnp->next == NULL ) break;
      scnp = scnp->next;
   }                             /* End of loop over waveserver menu entries */

   wsKillMenu( &queue );         /* Close connection and kill menu queue */
   *propDelay = mxd;             /* Return propogation delay to calling routine */
   return CUSPFEEDER_SUCCESS;
}
