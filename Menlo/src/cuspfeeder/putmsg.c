
/*
 * putmsg.c
*/

#include <stdio.h>
#include <string.h>
#include <trace_buf.h>

static int  nwritten;
static char *msgptr;


     /*******************************************************
      *                    InitOutMsg()                     *
      *                                                     *
      *  Initialize static variables.                       *
      *******************************************************/

void InitOutMsg( char *tracebuf )
{
   nwritten = 0;
   msgptr   = tracebuf;
   return;
}


     /*******************************************************
      *                      PutMsg()                       *
      *                                                     *
      *  Write output message to tracebuf, overwriting the  *
      *  undecimated input data.                            *
      *                                                     *
      *  Returns actlen = New trace length in bytes         *
      *******************************************************/

void PutMsg( char *outMsg, long *actlen )
{
   TRACE_HEADER *th   = (TRACE_HEADER *)outMsg;
   int   bytesPerSamp = th->datatype[1] - 48;
   int   nsamp;
   int   msgLen;

/* To ensure proper byte alignment, copy nsamp to local variable
   *************************************************************/
   memcpy( &nsamp, &th->nsamp, sizeof(int) );
   msgLen = sizeof(TRACE_HEADER) + (bytesPerSamp * nsamp);

/* Copy output message to tracebuf
   *******************************/
   memcpy( msgptr, outMsg, msgLen );

/* Reset static variables and return actlen
   ****************************************/
   nwritten += msgLen;
   msgptr   += msgLen;
   *actlen   = nwritten;
   return;
}
