
/*
 * getmsg.c - Get tracebuf messages from a buffer returned by waveserver.
*/

#include <stdio.h>
#include <string.h>
#include <trace_buf.h>

static int    bytesPerSamp;
static int    nread;
static char   *msgptr;
static double timePrev;


     /***************************************************
      *                   InitInMsg()                   *
      *                                                 *
      *  Initialize static variables.                   *
      ***************************************************/

void InitInMsg( char *tracebuf )
{
   TRACE_HEADER *th = (TRACE_HEADER *)tracebuf;

   bytesPerSamp = th->datatype[1] - 48;
   nread        = 0;
   msgptr       = tracebuf;
   timePrev     = 0.0;             /* Gap is guaranteed */
   return;
}


     /***************************************************
      *                    GetMsg()                     *
      *                                                 *
      *  Get a pointer to the next input message or     *
      *  return void if there is no next message.       *
      *                                                 *
      *  Returns gapflag = 1 if there is a data gap     *
                           0 if no gap detected         *
      ***************************************************/

char *GetMsg( long *actlen, int *gapflag )
{
   TRACE_HEADER *th = (TRACE_HEADER *)msgptr;
   int    msgLen;
   double gapsamp;
   char   *mptr;
   double starttime;
   double endtime;
   double samprate;
   int    nsamp;

/* Enough bytes left to make a message header?
   ******************************************/
   if ( *actlen < (nread + sizeof(TRACE_HEADER)) )
      return NULL;

/* To ensure proper byte alignment, copy headers to local variables
   ****************************************************************/
   memcpy( &starttime, &th->starttime, sizeof(double) );
   memcpy( &endtime,   &th->endtime,   sizeof(double) );
   memcpy( &samprate,  &th->samprate,  sizeof(double) );
   memcpy( &nsamp,     &th->nsamp,     sizeof(int)    );

/* Check for gap
   *************/
   gapsamp = (starttime - timePrev) * samprate;
   if ( (gapsamp > 0.5) && (gapsamp < 1.5) )
      *gapflag = 0;
   else
      *gapflag = 1;

/* Reset static variables
   **********************/
   msgLen   = sizeof(TRACE_HEADER) + (bytesPerSamp * nsamp);
   nread   += msgLen;
   mptr     = msgptr;
   msgptr  += msgLen;
   timePrev = endtime;

   if ( *actlen < nread )
      return NULL;
   else
      return mptr;
}
