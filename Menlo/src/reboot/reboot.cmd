@echo off
setlocal

rem # reboot.cmd                                   2005/11/21:LDD
rem # Usage: reboot [now/pau]

rem # This script is a command-line method for rebooting a computer.
rem # It should only be used in emergencies because it may not allow
rem # all applications to shut down cleanly before the reboot.
rem # Earthworm can optionally be stopped first.

rem # Requires shutdown.exe!
rem # WinXP/Win2003: exists as c:\Windows\System32\shutdown.exe
rem # Win2000: does not exist by default. Copy the WinXP/Win2003 version 
rem # of shutdown.exe into any directory in %PATH% and it should work 
rem # just fine.


rem # Verify command-line argument
rem #-----------------------------
if "%1" EQU "PAU"  goto StopEarthworm
if "%1" EQU "pau"  goto StopEarthworm
if "%1" EQU "NOW"  goto RebootNow
if "%1" EQU "now"  goto RebootNow

@echo Usage: reboot [now/pau]
@echo        where now = reboot the system immediately
@echo              pau = stop Earthworm first, then reboot

goto Done


rem # Stop Earthworm first; give user a chance to abort
rem #--------------------------------------------------
:StopEarthworm
pau
@echo Waiting 10 seconds for Earthworm to shut down...
sleep 10
@echo .
@echo Checking Earthworm status...
status


rem # Reboot the computer!
rem #---------------------
:RebootNow
@echo .
@echo Are you sure you want to reboot?
@echo Type "Ctrl-C" to abort, or
pause

@echo The system will shut down in 15 seconds...
@echo Type "shutdown /a" to abort.
shutdown /r /t 15 /f
goto Done


rem # If successful, we'll never get here!
rem #-------------------------------------
:Done
endlocal
