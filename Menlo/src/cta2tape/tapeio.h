
/*
       tapeio.h - Header file for tapeio.c
*/

#ifndef TAPEIO_H
#define TAPEIO_H

#include <sys/mtio.h>
#define STATLEN 8
#define LABLEN  9


typedef struct
{
   char status[STATLEN];    /* Empty, Full, or Unknown */
   char tapeLabel[LABLEN];  /* 8-character label or None */
                            /* If status != Full, tapeLabel is undefined */
}
DRIVE;

typedef struct
{
   char status[STATLEN];    /* Empty, Full, or Unknown */
   char tapeLabel[LABLEN];  /* 8-character label or None */
                            /* If status != Full, tapeLabel is undefined */
}
SLOT;

typedef struct
{
   int ndrive;          /* Number of tape drives in autoloader */
   int nslot;           /* Number of tape slots */
   DRIVE *drive;
   SLOT  *slot;
} MTXSTATUS;

/* Function prototypes
   *******************/
int WriteTarFile( char *, char *, int BlockingFactor, char *, int, int * );
int GetMtxStatus( char *changer );
int LoadNextTape( int logtape, char *changer );
int GetTapeLabel( char *changer, char *tapeLabel );

#endif
