
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <wait.h>
#include <unistd.h>
#include <earthworm.h>
#include "tapeio.h"

#define SLEN   100
#define MSGLEN 256

/* Length of string to contain blocking factor */
#define LENBFSTR 20

static int mtxInit = 0;
MTXSTATUS  mtxStatus;

void SetPriority( pid_t, char *, int );
int  WriteTapeLog( char *message, char *tapeLabel );




   /***********************************************************************
    *                            WriteTarFile()                           *
    *                                                                     *
    *         Copies a file to tape using the Unix tar command.           *
    *                                                                     *
    *  Returns  0 = no error                                              *
    *          -1 = fork error                                            *
    *          -2 = execl error                                           *
    *          -3 = waitpid error                                         *
    *          -4 = tar terminated by signal                              *
    *          -5 = tar stopped by signal                                 *
    *          -6 = unknown tar exit status                               *
    *          -7 = blocking factor too large                             *
    ***********************************************************************/

int WriteTarFile( char *fname,         /* Name of file to write */
                  char *tapename,      /* Name of tape device */
                  int  blockingFactor, /* Argument to tar command */
                  char *tarClass,      /* RT or TS */
                  int  tarPrio,        /* Process priority */
                  int  *stat )         /* Status code */
{
   pid_t pid;
   int   status;
   char  bfstr[LENBFSTR];     /* String to contain blocking factor */

/* Encode the blocking factor.
   snprintf() returns either the number of bytes written, or, if the
   buffer is too small, the number of bytes that would have been written.
   *********************************************************************/
   if ( snprintf(bfstr, LENBFSTR, "%d", blockingFactor) >= LENBFSTR )
      return -7;

/* Fork a child process to run the tar command.
   The "e" option means: exit immediately on any
   write error, with a positive status code.
   *********************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                          /* In child process */
         execlp( "tar", "tar", "cefb", tapename, bfstr, fname, NULL );
         return -2;

      default:
         break;
   }

/* Set the class and priority of the tar process
   *********************************************/
   SetPriority( pid, tarClass, tarPrio );

/* Wait for the tar command to complete
   ************************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( status ) )          /* tar exited */
   {
      *stat = WEXITSTATUS( status );
      return 0;
   }
   else if ( WIFSIGNALED( status ) )   /* tar terminated by signal */
   {
      *stat = WTERMSIG( status );
      return -4;
   }
   else if ( WIFSTOPPED( status ) )    /* tar stopped by signal */
   {
      *stat = WSTOPSIG( status );
      return -5;
   }
   return -6;                         /* Unknown exit status */
}


   /*****************************************************************
    *                        InitMtxStatus()                        *
    *                                                               *
    *              Initialize the mtx status structure.             *
    *                                                               *
    *  Returns  0 = no error                                        *
    *          -1 = popen() failed                                  *
    *          -2 = error getting first line from pipe              *
    *          -3 = error initializing drive info                   *
    *          -4 = error initializing slot info                    *
    *****************************************************************/

int InitMtxStatus( char *changer )           /* Name of tape changer */
{
   FILE *fp;
   int  i;
   char *ptr;
   char s[SLEN];
   int  ndrive;
   int  nslot;
   char command[SLEN];

/* Create a read-only pipe to the output of the mtx command
   ********************************************************/
   snprintf( command, SLEN, "mtx -f %s status", changer );
   fp = popen( command, "r" );
   if ( fp == NULL )
   {
      printf( "popen() failed.\n" );
      return -1;
   }

/* Get first line from pipe
   ************************/
   if ( fgets( s, SLEN, fp ) == NULL ) return -2;
   s[SLEN-1] = 0;

/* Decode ndrive.  Allocate space for drive info.
   *********************************************/
   ptr = strchr( s, ':' );
   if ( ptr++ == NULL ) return -3;
   if ( sscanf( ptr, "%d", &ndrive ) < 1 ) return -3;
   mtxStatus.ndrive = ndrive;
   mtxStatus.drive  = (DRIVE *)calloc( ndrive, sizeof(DRIVE) );
   if ( mtxStatus.drive == NULL ) return -3;
   for ( i = 0; i < ndrive; i++ )
      strcpy( mtxStatus.drive[i].status, "Unknown" );

/* Decode nslot.  Allocate space for slot info.
   *******************************************/
   ptr = strchr( s, ',' );
   if ( ptr++ == NULL ) return -4;
   if ( sscanf( ptr, "%d", &nslot ) < 1 ) return -4;
   mtxStatus.nslot = nslot;
   mtxStatus.slot  = (SLOT *)calloc( nslot, sizeof(SLOT) );
   if ( mtxStatus.slot == NULL ) return -4;
   for ( i = 0; i < nslot; i++ )
      strcpy( mtxStatus.slot[i].status, "Unknown" );


   pclose( fp );    /* Close the pipe */
   return 0;
}


   /*****************************************************************
    *                        GetMtxStatus()                         *
    *                                                               *
    *               Get the status of the autoloader.               *
    *                                                               *
    *  Returns  0 = no error                                        *
    *          -1 = popen() failed                                  *
    *          -2 = error getting first line from pipe              *
    *          -3 = error getting drive status                      *
    *          -4 = error getting slot status                       *
    *          -5 = InitMtxStatus() error                           *
    *****************************************************************/

int GetMtxStatus( char *changer )         /* Name of tape changer */
{
   int  i;
   FILE *fp;
   char *token;
   char s[SLEN];
   char command[SLEN];

/* Initialize the mtx status structure
   ***********************************/
   if ( !mtxInit )
   {
      int rc = InitMtxStatus( &changer[0] );
      if ( rc < 0 )
      {
         printf( "InitMtxStatus() error: %d\n", rc );
         return -5;
      }
      mtxInit = 1;
   }


/* Create a read-only pipe to the output of the mtx command
   ********************************************************/
   snprintf( command, SLEN, "mtx -f %s status", changer );
   fp = popen( command, "r" );
   if ( fp == NULL )
   {
      printf( "popen() failed.\n" );
      return -1;
   }

/* Read first line from pipe.  Ignore contents.
   *******************************************/
   if ( fgets( s, SLEN, fp ) == NULL ) return -2;

/* Read drive status and tape label
   ********************************/
   for ( i = 0; i < mtxStatus.ndrive; i++ )
   {
      do     /* Ignore short lines */
      {
         if ( fgets( s, SLEN, fp ) == NULL ) return -3;
         s[SLEN-1] = 0;
      } while (strlen(s) < 10);

      token = strtok( s, ":" );
      if ( token == NULL ) return -3;
      token = strtok( NULL, "\n " );
      if ( token == NULL ) return -3;
      strncpy( mtxStatus.drive[i].status, token, STATLEN );
      if ( strcmp(token,"Full") == 0 )
      {
         strcpy( mtxStatus.drive[i].tapeLabel, "None" );
         token = strtok( NULL, "=" );
         if ( token != NULL )
         {
            token = strtok( NULL, "\n " );
            if ( token != NULL )
            {
               strncpy( mtxStatus.drive[i].tapeLabel, token, LABLEN );
               mtxStatus.drive[i].tapeLabel[LABLEN-1] = 0;
            }
         }
      }
   }

/* Read slot status and tape label
   *******************************/
   for ( i = 0; i < mtxStatus.nslot; i++ )
   {
      do     /* Ignore short lines */
      {
         if ( fgets( s, SLEN, fp ) == NULL ) return -4;
         s[SLEN-1] = 0;
      } while (strlen(s) < 10);

      token = strtok( s, ":" );
      if ( token == NULL ) return -4;
      token = strtok( NULL, "\n " );
      if ( token == NULL ) return -4;
      strncpy( mtxStatus.slot[i].status, token, STATLEN );
      if ( strcmp(token,"Full") == 0 )
      {
         strcpy( mtxStatus.slot[i].tapeLabel, "None" );
         token = strtok( NULL, "=" );
         if ( token != NULL )
         {
            token = strtok( NULL, "\n " );
            if ( token != NULL )
            {
               strncpy( mtxStatus.slot[i].tapeLabel, token, LABLEN );
               mtxStatus.slot[i].tapeLabel[LABLEN-1] = 0;
            }
         }
      }
   }
   pclose( fp );    /* Close the pipe */
   return 0;
}



   /*****************************************************************
    *                          SpawnMtx()                           *
    *                                                               *
    *            Spawn an mtx process with one argument.            *
    *                                                               *
    *  Returns  0 = no error                                        *
    *          -1 = fork error                                      *
    *          -2 = execl error                                     *
    *          -3 = waitpid error                                   *
    *          -4 = mtx terminated by signal                        *
    *          -5 = mtx stopped by signal                           *
    *          -6 = unknown mtx exit status                         *
    *****************************************************************/

int SpawnMtx( char *changer,        /* Name of tape changer */
              char command[],       /* Load or unload */
              int  mtxArg,          /* Argument to mtx command */
              int  *mtxrc )         /* mtx return code */
{
   pid_t pid;
   int   waitStatus;
   char  slot[20];

/* Convert slot number to text string
   **********************************/
   snprintf( slot, 20, "%d", mtxArg );

/* Fork a child process to run the mtx command
   *******************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                              /* In child process */
         execlp( "mtx", "mtx", "-f", changer, command, slot, NULL );
         return -2;

      default:
         break;
   }

/* Wait for the mtx command to complete
   ************************************/
   if ( waitpid( pid, &waitStatus, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( waitStatus ) )          /* mtx exited */
   {
      *mtxrc = WEXITSTATUS( waitStatus );
      return 0;
   }
   else if ( WIFSIGNALED( waitStatus ) )   /* mtx terminated by signal */
   {
      *mtxrc = WTERMSIG( waitStatus );
      return -4;
   }
   else if ( WIFSTOPPED( waitStatus ) )    /* mtx stopped by signal */
   {
      *mtxrc = WSTOPSIG( waitStatus );
      return -5;
   }
   return -6;                              /* Unknown exit status */
}


   /*****************************************************************
    *                         MtxLoadTape()                         *
    *                                                               *
    *               Load a tape from the autoloader.                *
    *                                                               *
    *  Returns  0 = no error                                        *
    *          -1 = fork error                                      *
    *          -2 = execl error                                     *
    *          -3 = waitpid error                                   *
    *          -4 = mtx terminated by signal                        *
    *          -5 = mtx stopped by signal                           *
    *          -6 = unknown mtx exit status                         *
    *****************************************************************/

int MtxLoadTape( char *changer,        /* Name of tape changer */
                 int  tapeToLoad,      /* Slot number */
                 int  *mtxrc )         /* mtx return code */
{
   return SpawnMtx( changer, "load", tapeToLoad, mtxrc );
}


   /*****************************************************************
    *                        MtxUnloadTape()                        *
    *                                                               *
    *              Unload a tape from the autoloader.               *
    *                                                               *
    *  Returns  0 = no error                                        *
    *          -1 = fork error                                      *
    *          -2 = execl error                                     *
    *          -3 = waitpid error                                   *
    *          -4 = mtx terminated by signal                        *
    *          -5 = mtx stopped by signal                           *
    *          -6 = unknown mtx exit status                         *
    *****************************************************************/

int MtxUnloadTape( char *changer,        /* Name of tape changer */
                   int  tapeToUnload,    /* Slot number */
                   int  *mtxrc )         /* mtx return code */
{
   return SpawnMtx( changer, "unload", tapeToUnload, mtxrc );
}



   /*******************************************************************
    *                         LoadNextTape()                          *
    *                                                                 *
    *  Load the next tape in the autoloader, using mtx.               *
    *  We assume the autoloader contains only one tape drive.         *
    *                                                                 *
    *  Returns  0 = No error.                                         *
    *          -1 = Can't get mtx status                              *
    *          -2 = No tape in tape drive                             *
    *          -3 = No empty slots in autoloader                      *
    *          -4 = No more tapes available in autoloader             *
    *          -5 = mtx error on tape load or unload (see log file)   *
    *******************************************************************/

int LoadNextTape( int LogTape, char *changer )
{
   char message[MSGLEN];
   int  rc;
   int  mtxrc;
   int  i;
   int  fes;           /* First empty slot */
   int  nswt;          /* Next slot with a tape */

/* Make sure the tape drive contains a tape
   ****************************************/
   rc = GetMtxStatus( changer );
   if ( rc < 0 ) return -1;
   if ( mtxStatus.ndrive < 1 ) return -1;
   if ( strcmp( mtxStatus.drive[0].status, "Full" ) != 0 )
      return -2;

/* Get slot number of first empty slot
   ***********************************/
   for ( i = 0; i < mtxStatus.nslot; i++ )
   {
      if ( strcmp(mtxStatus.slot[i].status, "Empty") == 0 )
         break;
   }
   if ( i >= mtxStatus.nslot ) return -3;  /* No empty slots */
   fes = i + 1;                            /* First empty slot */

/* Unload tape into first empty slot
   *********************************/
   rc = MtxUnloadTape( changer, fes, &mtxrc );
   if ( rc != 0 )
      printf( "MtxUnloadTape() rc: %d\n", rc );
   if ( mtxrc != 0 )
      printf( "MtxUnloadTape() mtxrc: %d\n", mtxrc );
   if ( (rc != 0) || (mtxrc != 0) ) return -5;

/* Get label of unloaded tape
   **************************/
   rc = GetMtxStatus( changer );
   if ( rc < 0 ) return -1;
   sprintf( message, "Tape unloaded from tape drive to slot %d.  Label: %s\n",
            fes, mtxStatus.slot[i].tapeLabel );
   logit( "et", message );
   if ( LogTape == 1 )
      WriteTapeLog( message, mtxStatus.slot[i].tapeLabel );

/* Get slot number of next tape to load
   ************************************/
   for ( i = fes; i < mtxStatus.nslot; i++ )
   {
      if ( strcmp(mtxStatus.slot[i].status, "Full") == 0 )
         break;
   }
   if ( i >= mtxStatus.nslot ) return -4;  /* No more empty tapes */
   nswt = i + 1;                           /* Next slot with a tape */

/* Load tape from next full slot into tape drive
   *********************************************/
   rc = MtxLoadTape( changer, nswt, &mtxrc );
   if ( rc != 0 )
      printf( "MtxLoadTape() rc: %d\n", rc );
   if ( mtxrc != 0 )
      printf( "MtxLoadTape() mtxrc: %d\n", mtxrc );
   if ( (rc != 0) || (mtxrc != 0) ) return -5;

/* Get label of tape just loaded into tape drive
   *********************************************/
   rc = GetMtxStatus( changer );
   if ( rc < 0 ) return -1;
   sprintf( message, "Tape loaded into tape drive from slot %d.  Label: %s\n",
            nswt, mtxStatus.drive[0].tapeLabel );
   logit( "et", message );
   if ( LogTape == 1 )
      WriteTapeLog( message, mtxStatus.slot[i].tapeLabel );
   return 0;
}


#ifdef JUNK
   /***********************************************************************
    *                           LoadNextTape()                            *
    *                                                                     *
    *  Take the tape unit offline.  Some Sony autoloaders will then       *
    *  automatically load the next tape into the drive.  Peak 8-Pak       *
    *  autoloaders must be explicitly instructed to load the next tape.   *
    *                                                                     *
    *  Returns  0  = No error.                                            *
    *          -1  = mt fork error.                                       *
    *          -2  = mt command not in search path.                       *
    *          -3  = mt waitpid error.                                    *
    *          -4  = mt error. No tape loaded or drive offline.           *
    *                   Check mstat.                                      *
    *          -5  = mt terminated by signal.                             *
    *          -6  = mt stopped by signal.                                *
    *          -7  = Unknown mt exit status.                              *
    *          -8  = mtx fork error.                                      *
    *          -9  = mtx command not in search path.                      *
    *          -10 = mtx waitpid error.                                   *
    *          -11 = mtx error. Can't load next tape. Check mstat.        *
    *          -12 = mtx terminated by signal.                            *
    *          -13 = mtx stopped by signal.                               *
    *          -14 = Unknown mtx exit status.                             *
    ***********************************************************************/

int LoadNextTape( char *tapename,      /* Name of tape device */
                  char *changer,       /* Name of tape changer or "none" */
                  int  *mstat )        /* Status code */
{
   pid_t pid;
   int   status;

/* Fork a child process to run the mt command
   ******************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;
      case 0:                          /* In child process */
         execlp( "mt", "mt", "-f", tapename, "offline", NULL );
         return -2;                    /* mt command not in search path */
      default:
         break;
   }

/* Wait for the mt command to complete
   ***********************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED(status) )           /* mt exited */
   {
      *mstat = WEXITSTATUS(status);
      if ( *mstat != 0 ) return -4;   /* No tape loaded or drive offline */
   }
   else if ( WIFSIGNALED(status) )    /* mt terminated by signal */
   {
      *mstat = WTERMSIG(status);
      return -5;
   }
   else if ( WIFSTOPPED(status) )     /* mt stopped by signal */
   {
      *mstat = WSTOPSIG(status);
      return -6;
   }
   else
      return -7;                      /* Unknown mt exit status */

/* If the autoloader doesn't contain a separate
   changer device, we're done.
   ********************************************/
   if ( strcmp( changer, "none" ) == 0 ) return 0;

/* The autoloader contains a separate changer device.
   Invoke the mtx command to load the next tape.
   *************************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -8;
      case 0:                          /* In child process */
         execlp( "mtx", "mtx", "-f", changer, "next", NULL );
         return -9;                    /* mtx command not in search path */
      default:
         break;
   }

/* Wait for the mtx command to complete
   ************************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -10;

   if ( WIFEXITED(status) )            /* mtx exited */
   {
      *mstat = WEXITSTATUS(status);
      if ( *mstat != 0 ) return -11;   /* Error loading next tape */
      return 0;                        /* Success */
   }
   else if ( WIFSIGNALED(status) )     /* mtx terminated by signal */
   {
      *mstat = WTERMSIG(status);
      return -12;
   }
   else if ( WIFSTOPPED(status) )      /* mtx stopped by signal */
   {
      *mstat = WSTOPSIG(status);
      return -13;
   }
   else
      return -14;                      /* Unknown mtx exit status */
}
#endif


   /*******************************************************************
    *                         GetTapeLabel()                          *
    *                                                                 *
    *  Get the label of whichever tape is loaded in the tape drive.   *
    *  We assume the autoloader contains only one tape drive.         *
    *                                                                 *
    *  Returns  0 = No error.                                         *
    *          -1 = Can't get mtx status                              *
    *          -2 = No tape in tape drive                             *
    *******************************************************************/

int GetTapeLabel( char *changer,          /* Name of tape changer */
                  char *tapeLabel )
{
   int rc;

/* Make sure the tape drive contains a tape
   ****************************************/
   rc = GetMtxStatus( changer );
   if ( rc < 0 ) return -1;
   if ( mtxStatus.ndrive < 1 ) return -1;
   if ( strcmp( mtxStatus.drive[0].status, "Full" ) != 0 )
      return -2;

/* Return the tape label
   *********************/
   strcpy( tapeLabel, mtxStatus.drive[0].tapeLabel );
   return 0;
}


/* Test program
   ************/
/*
main()
{
   int  mstat = 0;
   char tapename[] = "/dev/rmt/0n";
   char changer[]  = "/dev/changer";

   int rc = LoadNextTape( &tapename[0], &changer[0], &mstat );

   printf( "LoadNextTape(): %d\n", rc );
   printf( "mstat: %d\n", mstat );
   return 0;
}
*/
