#
#                    Make file for cta2tape
#                        Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib
I = $(EW_HOME)/$(EW_VERSION)/include


CTA2TAPE = cta2tape.o getfname.o setprio.o tapeio.o \
           $L/logit.o $L/kom.o $L/getutil.o \
           $L/sleep_ew.o $L/time_ew.o $L/transport.o

all:
	make -f makefile.sol cta2tape
	make -f makefile.sol cta

cta2tape: $(CTA2TAPE)
	cc -o $B/cta2tape $(CTA2TAPE) -lm -lposix4 -lc

cta: cta.sh
	cp cta.sh $B/cta

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/cta2tape*

lint:
	lint -I $I -Nlevel=4 cta2tape.c

