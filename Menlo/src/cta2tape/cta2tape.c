  /********************************************************************
   *                            cta2tape.c                            *
   *       This program reads cta files and writes them to tape.      *
   ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <unistd.h>
#include <kom.h>
#include <transport.h>
#include <time_ew.h>
#include "tapeio.h"

#define MSG_OFFLINE       0          /* Autoloader tape changed */
#define MSG_BARCODE_ERROR 1          /* Error reading barcode label */
#define MAX_FNL     256              /* Max length of file name */
#define MSGLEN      256

/* Function prototypes
   *******************/
void GetConfig( char * );
void LogConfig( void );
void LookupMsgType( void );
void SendStatus( unsigned char, short, char * );
int  GetFileName( char fname[] );
int  WriteTapeLog( char *message, char *tapeLabel );

/* Global variables
   ****************/
static SHM_INFO Region;              /* Shared memory region to use for i/o */
static pid_t    myPid;               /* Process id of this process */

/* Get these from the config file
   ******************************/
static char     RingName[20];        /* Name of transport ring for i/o */
static char     MyModName[20];       /* Speak as this module name/id */
static long     HeartBeatInt;        /* Seconds between heartbeats */
static char     InDir[MAX_FNL];      /* Name of input directory */
static int      LogTape;             /* If 1, log to tape log files */
static int      BlockingFactor;      /* In 512-byte tape blocks */
static char     LogFileDir[MAX_FNL]; /* Where to find tape log files */
static char     Class[20];           /* Class for tar, mt, mtx processes */
static int      Priority;            /* Priority for tar, mt, mtx processes */
static char     TapeName[80];        /* Name of tape device */
static char     Changer[80];         /* Name of tape changer, or "none" */

/* Look these up in the earthworm.h tables with getutil functions
   **************************************************************/
static long          RingKey;        /* Transport ring key */
static unsigned char InstId;         /* Local installation id */
static unsigned char MyModId;        /* Module Id for this program */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;


int main( int argc, char **argv )
{
   long timeNow;           /* Current time */
   long timeLastBeat;      /* Time last heartbeat was sent */
   char *configFile;
   char *ewparams;
   char pidpath[80];
   char pidfname[] = "cta2tape.pid";
   FILE *pidfp;
   extern MTXSTATUS mtxStatus;    /* Declared in tapeio.c */

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      printf( "Usage: cta2tape <configfile>\n" );
      return 0;
   }
   configFile = argv[1];

/* Read the configuration file
   ***************************/
   GetConfig( configFile );

/* Look up message types from earthworm.h tables
   *********************************************/
   LookupMsgType();

/* Initialize name of log-file & open it
   *************************************/
   logit_init( "cta2tape", (short) MyModId, 256, 1 );
   logit( "" , "Read command file <%s>\n", argv[1] );

/* Get the pid of this process for restart purposes
   ************************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Write the pid of this process to a file in the params directory
   ***************************************************************/
   ewparams = getenv( "EW_PARAMS" );
   if ( ewparams == NULL )
   {
      logit( "e", "Can't get EW_PARAMS from environment. Exiting.\n" );
      return -1;
   }
   snprintf( pidpath, 80, "%s/%s", ewparams, pidfname );
   pidfp = fopen( pidpath, "w" );
   if ( pidfp == NULL )
   {
      logit( "e", "Can't create file %s  Exiting.\n", pidpath );
      return -1;
   }
   fprintf( pidfp, "%d\n", myPid );
   fclose( pidfp );

/* Don't log to the tapelogs directory if using a single drive
   ***********************************************************/
   if ( strcmp( Changer, "none" ) == 0 )
      LogTape = 0;

/* Log the configuration file
   **************************/
   LogConfig();

/* Change working directory to InDir
   *********************************/
   if ( chdir( InDir ) == -1 )
   {
      logit( "e", "Can't cwd to %s\n", InDir );
      logit( "e", "Exiting.\n" );
      return -1;
   }

/* Attach to shared memory ring
   ****************************/
   tport_attach( &Region, RingKey );

/* Make sure a tape is loaded in the drive.  This works
   only if there is a tape changer, not a single drive.
   ****************************************************/
   if ( strcmp( Changer, "none") != 0 )
   {
      if ( GetMtxStatus( Changer ) < 0 )
      {
         logit( "et", "Can't get mtx status. Exiting.\n" );
         return -1;
      }
      if ( strcmp( mtxStatus.drive[0].status, "Empty") == 0 )
      {
         logit( "et", "Error. Tape drive is empty. Exiting.\n" );
         return -1;
      }
      if ( strcmp( mtxStatus.drive[0].status, "Unknown") == 0 )
      {
         logit( "et", "Error. Tape drive status is unknown. Exiting.\n" );
         return -1;
      }
      if ( strcmp( mtxStatus.drive[0].status, "Full") != 0 )
      {
         logit( "et", "Invalid tape drive status. Exiting.\n" );
         return -1;
      }
      if ( strcmp( mtxStatus.drive[0].tapeLabel, "None") == 0 )
      {
         logit( "et", "At startup, tape drive contains a tape without a barcode label.\n" );
      }
      else
      {
         char message[MSGLEN];
         sprintf( message, "At startup, tape drive contains a tape with barcode label: %s\n",
                  mtxStatus.drive[0].tapeLabel );
         logit( "et", message );
         if ( LogTape == 1 )
            WriteTapeLog( message, mtxStatus.drive[0].tapeLabel );
      }
   }

/* Force a heartbeat to be issued in first pass thru main loop
   ***********************************************************/
   timeLastBeat = time(&timeNow) - HeartBeatInt - 1;

/* Main loop
   *********/
   while ( tport_getflag( &Region ) != TERMINATE &&
           tport_getflag( &Region ) != myPid )
   {
      char ctafile[80];     /* Name of cta file */

/* Send a heartbeat to statmgr
   ***************************/
      if  ( (time(&timeNow) - timeLastBeat) >= HeartBeatInt )
      {
         timeLastBeat = timeNow;
         SendStatus( TypeHeartBeat, 0, "" );
      }

/* See if a cta file has arrived.
   If so, write it to tape using tar.
   *********************************/
      if ( GetFileName( ctafile ) == 0 )
      {
         char errText[80];
         int  wtfrc, tarstatus;

         logit( "et", "Writing file %s to tape\n", ctafile );
         wtfrc = WriteTarFile( ctafile, TapeName, BlockingFactor,
                               Class, Priority, &tarstatus );

/* If wtfrc < 0, there was a tar error.
   This shouldn't ever happen.
   ***********************************/
         if ( wtfrc < 0 )
         {
            logit( "et", "WriteTarFile() error: %d\n", wtfrc );
            logit( "et", "cta2tape exiting.\n" );
            exit( 0 );
         }

/* Tar succeeded.  Log to cta2file log file and tape log file.
   **********************************************************/
         if ( tarstatus == 0 )
         {
            char message[MSGLEN];
            char tapeLabel[LABLEN];

            if ( strcmp( Changer, "none" ) != 0 )
            {
               if ( GetTapeLabel( Changer, tapeLabel ) == 0 )
               {
                  if ( strcmp( tapeLabel, "None" ) == 0 )
                     sprintf( message, "File %s written to a tape without a barcode label.\n",
                              ctafile );
                  else
                     sprintf( message, "File %s written to tape with barcode label: %s\n",
                              ctafile, tapeLabel );
               }
               else
               {
                  sprintf( message, "File %s written to tape.  Can't read barcode label.\n",
                           ctafile );
                  SendStatus( TypeError, MSG_BARCODE_ERROR, message );
               }
            }
            else
               sprintf( message, "File %s written to tape.\n", ctafile );

            logit( "et", "%s", message );
            if ( LogTape == 1 )
               WriteTapeLog( message, tapeLabel );

/* Erase disk file
   ***************/
            if ( remove( ctafile ) == -1 )
               logit( "et", "Error deleting file: %s\n", ctafile );
         }

/* No tape in drive, possibly because there are
   no more tapes in the magazine.
   Since there is nothing else we can do, exit program.
   statmgr can be set up to send program dead messages.
   ***************************************************/
         else if ( tarstatus == 1 )
         {
            logit( "et", "Tape write error.  No tape in drive.\n" );
            logit( "et", "cta2tape exiting.\n" );
            exit( 0 );
         }

/* Tar failed because of a tape write error.
   Probably, we attempted to write off the end of the tape.
   It's also possible the tape cartridge or tape drive is bad.
   **********************************************************/
         else if ( tarstatus == 2 )
         {
            int lntrc;
            logit( "et", "Tape write error.  Possibly encountered an end-of-tape.\n" );


/* If this is a single drive, we're done.
   *************************************/
            if ( strcmp( Changer, "none" ) == 0 )
            {
               logit( "et", "No autoloader.\n" );
               logit( "et", "cta2tape exiting.\n" );
               exit( 0 );
            }

/* If this is an autoloader, load next tape into tape drive
   ********************************************************/
            logit( "et", "Attempting to load next tape from magazine into drive.\n" );
            lntrc = LoadNextTape( LogTape, Changer );


/* LoadNextTape() failed, possibly because we have
   already loaded the last tape.
   Since there is nothing else we can do, exit program.
   statmgr can be set up to send program-dead messages.
   ***************************************************/
            if ( lntrc < 0 )
            {
               logit( "et", "LoadNextTape(): " );
               if ( lntrc == -1 ) logit( "e", "Can't get mtx status." );
               if ( lntrc == -2 ) logit( "e", "No tape in tape drive." );
               if ( lntrc == -3 ) logit( "e", "No empty slots in autoloader." );
               if ( lntrc == -4 ) logit( "e", "No more tapes available in autoloader." );
               if ( lntrc == -5 ) logit( "e", "mtx error on tape load or unload. See log file." );
               logit( "e", "\n" );
               logit( "et", "cta2tape exiting.\n" );
               exit( 0 );
            }

/* LoadNextTape() succeeded, so we successfully changed tapes.
   Try writing the same tar file to the new tape.
   **********************************************************/
            logit( "et", "LoadNextTape() succeeded.\n" );
            strcpy( errText, "Autoloader tape changed." );
            SendStatus( TypeError, MSG_OFFLINE, errText );
         }                            /* End tarstatus == 2 */

/* Unknown tape write error
   ************************/
         else
         {
            logit( "et", "Unknown tape write error. tarstatus: %d\n", tarstatus );
            logit( "et", "cta2tape exiting.\n" );
            exit( 0 );
         }
      }
      sleep_ew( 500 );       /* Wait for a new cta file to arrive */
   }

/* Log which tape is in the tape drive.
   Then, exit program.
   ***********************************/
   if ( strcmp( Changer, "none" ) != 0 )
   {
      if ( GetMtxStatus( Changer ) < 0 )
      {
         logit( "et", "Can't get mtx status. Exiting.\n" );
         return -1;
      }
      if ( strcmp( mtxStatus.drive[0].status, "Empty") == 0 )
      {
         logit( "et", "Error. Tape drive is empty. Exiting.\n" );
         return -1;
      }
      if ( strcmp( mtxStatus.drive[0].status, "Unknown") == 0 )
      {
         logit( "et", "Error. Tape drive status is unknown. Exiting.\n" );
         return -1;
      }
      if ( strcmp( mtxStatus.drive[0].status, "Full") != 0 )
      {
         logit( "et", "Invalid tape drive status. Exiting.\n" );
         return -1;
      }
      if ( strcmp( mtxStatus.drive[0].tapeLabel, "None") == 0 )
      {
         logit( "et", "At shutdown, tape drive contains a tape without a barcode label.\n" );
      }
      else
      {
         char message[MSGLEN];
         sprintf( message, "At shutdown, tape drive contains a tape with barcode label: %s\n",
                  mtxStatus.drive[0].tapeLabel );
         logit( "et", message );
         if ( LogTape == 1 )
            WriteTapeLog( message, mtxStatus.drive[0].tapeLabel );
      }
   }

   tport_detach( &Region );
   logit( "et", "cta2tape exiting.\n" );
   return 0;
}


  /************************************************************************
   *  GetConfig() processes command file(s) using kom.c functions;        *
   *                    exits if any errors are encountered.              *
   ************************************************************************/

#define NCOMMAND 11

void GetConfig( char *configfile )
{
   const int ncommand = NCOMMAND;    /* Process this many required commands */
   char      init[NCOMMAND];         /* Init flags, one for each command */
   int       nmiss;                  /* Number of missing commands */
   char      *com;
   char      *str;
   int       nfiles;
   int       success;
   int       i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      printf( "Error opening command file <%s>. Exiting.\n",
               configfile );
      exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )             /* Read next line from active file  */
      {
         com = k_str();            /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )          continue;
         if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
         if ( com[0] == '@' )
         {
            success = nfiles+1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "Can't open command file <%s>. Exiting.\n",
                        &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         else if ( k_its("MyModuleId") )
         {
             str = k_str();
             if (str) strcpy( MyModName, str );
             init[0] = 1;
         }
         else if ( k_its("RingName") )
         {
             str = k_str();
             if (str) strcpy( RingName, str );
             init[1] = 1;
         }
         else if ( k_its("HeartBeatInt") )
         {
             HeartBeatInt = k_long();
             init[2] = 1;
         }
         else if ( k_its("InDir") )
         {
             str = k_str();
             if (str)
             {
                strncpy( InDir, str, MAX_FNL );
                init[3] = 1;
             }
         }
         else if ( k_its("Class") )
         {
             str = k_str();
             if (str) strcpy( Class, str );
             init[4] = 1;
         }
         else if ( k_its("Priority") )
         {
             Priority = k_int();
             init[5] = 1;
         }
         else if ( k_its("TapeName") )
         {
             str = k_str();
             if (str) strcpy( TapeName, str );
             init[6] = 1;
         }
         else if ( k_its("Changer") )
         {
             str = k_str();
             if (str) strcpy( Changer, str );
             init[7] = 1;
         }
         else if ( k_its("LogFileDir") )
         {
             str = k_str();
             if (str)
             {
                strncpy( LogFileDir, str, MAX_FNL );
                init[8] = 1;
             }
         }
         else if ( k_its("LogTape") )
         {
             LogTape = k_long();
             init[9] = 1;
         }
         else if ( k_its("BlockingFactor") )
         {
             BlockingFactor = k_int();
             init[10] = 1;
         }

/* Unknown command
   ***************/
         else
         {
             printf( "<%s> Unknown command in <%s>.\n",
                      com, configfile );
             continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "Bad <%s> command in <%s>. Exiting.\n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
       printf( "ERROR, no " );
       if ( !init[0] )  printf( "<MyModuleId> "   );
       if ( !init[1] )  printf( "<RingName> "     );
       if ( !init[2] )  printf( "<HeartBeatInt> " );
       if ( !init[3] )  printf( "<InDir> "        );
       if ( !init[4] )  printf( "<Class> "        );
       if ( !init[5] )  printf( "<Priority> "     );
       if ( !init[6] )  printf( "<TapeName> "     );
       if ( !init[7] )  printf( "<Changer> "      );
       if ( !init[8] )  printf( "<LogFileDir> "   );
       if ( !init[9] )  printf( "<LogTape> "      );
       if ( !init[10] ) printf( "<BlockingFactor> " );
       printf( "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }
   return;
}


  /************************************************************************
   *  LogConfig() prints the config parameters in the log file.           *
   ************************************************************************/

void LogConfig( void )
{
   logit( "", "\nConfig File Parameters:\n" );
   logit( "", "   MyModuleId:     %s\n", MyModName );
   logit( "", "   RingName:       %s\n", RingName );
   logit( "", "   HeartBeatInt:   %d\n", HeartBeatInt );
   logit( "", "   InDir:          %s\n", InDir );
   logit( "", "   Class:          %s\n", Class );
   logit( "", "   Priority:       %d\n", Priority );
   logit( "", "   TapeName:       %s\n", TapeName );
   logit( "", "   Changer:        %s\n", Changer );
   logit( "", "   LogFileDir:     %s\n", LogFileDir );
   logit( "", "   LogTape:        %d\n", LogTape );
   logit( "", "   BlockingFactor: %d\n", BlockingFactor );
   return;
}


  /**********************************************************************
   *  LookupMsgType( )   Look up message types from earthworm.h tables  *
   **********************************************************************/

void LookupMsgType( void )
{
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "Invalid message type <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   if ( ( RingKey = GetKey(RingName) ) == -1 )
   {
        printf( "Invalid ring name <%s>. Exiting.\n", RingName);
        exit( -1 );
   }
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "Error geting local installation id. Exiting.\n" );
      exit( -1 );
   }
   if ( GetModId( MyModName, &MyModId ) != 0 )
   {
      printf( "Invalid module name <%s>; Exiting.\n", MyModName );
      exit( -1 );
   }
   return;
}


/******************************************************************************
 * SendStatus() builds a heartbeat or error message & puts it into            *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/

void SendStatus( unsigned char type, short ierr, char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   long     t;

/* Build the message
   *****************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if ( type == TypeHeartBeat )
      sprintf( msg, "%ld %d\n", t, myPid );
   else if ( type == TypeError )
   {
      sprintf( msg, "%ld %hd %s\n", t, ierr, note );
      logit( "et", "%s\n", note );
   }
   else
      return;

   size = strlen( msg );           /* Don't include null byte in message */

/* Write the message to shared memory
   **********************************/
   if ( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if ( type == TypeHeartBeat )
           logit("et","Error sending heartbeat.\n" );
        else
           if ( type == TypeError )
              logit("et","Error sending error:%d.\n", ierr );
   }
   return;
}


    /****************************************************************
     * WriteTapeLog() creates and writes to individual log files    *
     * for each tape.                                               *
     *                                                              *
     * Returns  0 if no errors detected,                            *
     *         -1 if no tape label,                                 *
     *         -2 if the log file cannot be opened,                 *
     *         -3 if an error occured while writing to the log file *
     ****************************************************************/
#define DATE_LEN 10   /* Length of date field */

int WriteTapeLog( char *message, char *tapeLabel )
{
   FILE *fp;
   char fname[MAX_FNL];
   char msg[256];

   if ( strcmp( tapeLabel, "None" ) == 0 ) return -1;

/* Open tape log file
   ******************/
   strcpy( fname, LogFileDir );
   strcat( fname, "/" );
   strcat( fname, tapeLabel );
   strcat( fname, ".log" );

   fp = fopen( fname, "a" );
   if ( fp == NULL ) return -2;

/* Put current time at front of message
   ************************************/
   {
      time_t now;
      struct tm res;

      time( &now );
      gmtime_ew( &now, &res );
      sprintf( msg, "%4d%02d%02d_UTC_%02d:%02d:%02d %s", (res.tm_year + TM_YEAR_CORR),
               (res.tm_mon + 1), res.tm_mday, res.tm_hour, res.tm_min, res.tm_sec,
               message );
   }

/* Write message to tape log file
   ******************************/
   if ( fputs( msg, fp ) == EOF )
   {
      fclose( fp );
      return -3;
   }
   fclose( fp );
   return 0;
}

