#!/bin/sh
#
# cta script
#
# The cta2tape program writes its process id to a file named
# cta2tape.pid, which is located in the Earthworm params directory.
# This script has one required argument, pause or resume.
# "cta pause" kills the cta2tape program using the Earthworm pidpau program.
# "cta resume" starts (or restarts) the cta2tape program using the
# Earthworm restart program.
# Only one instance of cta2tape can run at the same time, since there is
# only one cta2tape.pid file.
#

pid=`cat $EW_PARAMS/cta2tape.pid`
bin=$EW_HOME/$EW_VERSION/bin

case $1 in
'pause')
	$bin/pidpau $pid
	;;

'resume')
	$bin/restart $pid
	;;

*)
        echo "Usage: cta { pause | resume }"
        exit 1
        ;;
esac
