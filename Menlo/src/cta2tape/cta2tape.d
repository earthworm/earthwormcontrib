#
# This is cta2tape's parameter file
#
MyModuleId    MOD_CTA2TAPE    # Module id for this instance of cta2tape
RingName      WAVE_RING       # Shared memory ring for input/output
HeartBeatInt  30              # Seconds between heartbeats
Class         TS              # Class for tar, mt, and mtx processes
Priority      0               # Priority for tar, mt, and mtx processes
TapeName      /dev/rmt/1n     # Should end in "n" (no rewind)
Changer       /dev/changer    # Enter "none" for single drive,
                              # "/dev/changer" for Overland ARCvault
LogTape       1               # If 1, log to tape log files in
                              #    directory LogFileDir.

# BlockingFactor specifies the number of 512-byte tape blocks to be
# included in each tar write operation.  Warning: The default blocking
# factor for tar is 20.  If you write a tape with a blocking factor
# greater than 20, you will need to read the tape back with a blocking
# factor greater than or equal to its original blocking factor.
# Otherwise, you will get a read error.  For more information, see the
# man page for tar.
BlockingFactor    126

# cta files are read here
InDir  /home/earthworm/run-will/compress_cta

# LogFileDir contains a log file for each tape
LogFileDir   /home/earthworm/run/tapelogs

