
#include <math.h>


     /******************************************************************
      *                             Dstaz()                            *
      *                                                                *
      *  Compute distance, in degrees, from point 1 to 2 on the        *
      *  surface of the Earth.  East longitudes are positive.          *
      *  From "Seismology" by K.E. Bullen.  Azimuth calculation not    *
      *  implemented.                                                  *
      ******************************************************************/

void Dstaz( double lat1d, double lon1d, double lat2d, double lon2d,
            double *dist_deg )
{
   const double pi = 3.14159265359;
   const double degrad = 180. / pi;

   double colat1 = (90. - lat1d) / degrad;
   double colat2 = (90. - lat2d) / degrad;
   double lon1   = lon1d / degrad;
   double lon2   = lon2d / degrad;

   double a1 = sin(colat1) * cos(lon1);
   double b1 = sin(colat1) * sin(lon1);
   double c1 = cos(colat1);
   double a2 = sin(colat2) * cos(lon2);
   double b2 = sin(colat2) * sin(lon2);
   double c2 = cos(colat2);
   double d  = (a1 * a2) + (b1 * b2) + (c1 * c2);

   if ( d <= -1. )
      *dist_deg = 180.;

   else if ( d >=  1. )
      *dist_deg = 0.;

   else
      *dist_deg = degrad * acos( d );
   return;
}

