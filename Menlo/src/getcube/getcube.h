
/*   getcube.h    */
 
#define ERR_LINETOOBIG     0  /* Line in cube file is too big for buffer */
#define ERR_CUBEDECODE     1  /* Error decoding cube event line */
#define ERR_EVENTIDERR     2  /* Can't read event id from file */
#define ERR_PUTMSG         3  /* tport_putmsg() error */

#define MAXC             100  /* Size of buffer to contain line from cube file */
#define MAXCOMP           30  /* Maximum number of component codes */

