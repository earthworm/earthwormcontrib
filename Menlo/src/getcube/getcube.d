#
# getcube.d  -  Configuration file for the getcube program
#
MyModName         MOD_GETCUBE       # Module name for this instance of getcube.
                                    #   The specified name must appear in the
                                    #   earthworm.d configuration file.
RingName          HYPO_RING         # Triglist, heartbeat, and error messages
                                    #   are sent to this ring.
LogFile           1                 # Set to 0 to disable logging to disk.
HeartBeatInterval 15                # Interval between heartbeats sent to
                                    #   statmgr, in seconds.
PreEvent          30                # Start saving this many seconds before
                                    #   event time.
EventLength       1800              # Save this many seconds of data
#
InDir             /home/picker/CUBE/outputdir
                                    # Cube files are read from this directory.
#
SaveDir           /home/picker/CUBE/savedir
                                    # Acceptable CUBE events are saved here.
#
HerrinFname       /home/earthworm/run/params/herrin.table
                                    # File containing Direct P travel times
                                    # from the Herrin tables
#
# A maximum of 30 CompCode lines are allowed.
# They will all appear in the triglist message of each event,
# eg "*  VHZ  *"
#
CompCode          BHE
CompCode          BHN
CompCode          BHZ
CompCode          DFI
CompCode          DHI
CompCode          EHE
CompCode          EHN
CompCode          EHZ
CompCode          EP1
CompCode          EP2
CompCode          EP3
CompCode          HHN
CompCode          HHE
CompCode          HHZ
CompCode          HV1
CompCode          SHZ
CompCode          VDZ
CompCode          VHZ
CompCode          VHN
CompCode          VHE
