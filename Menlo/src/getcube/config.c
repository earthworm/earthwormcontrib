
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include "getcube.h"

/* For getpid */
#ifdef _SOLARIS
#include <unistd.h>
#endif
#ifdef _WINNT
#include <process.h>
#endif

/* Read these from the configuration file
   **************************************/
int   LogFile;              /* Flag value, 0 - 1 */
char  MyModName[20];        /* Speak as this module name/id */
char  RingName[20];         /* Name of transport ring for i/o */
long  HeartBeatInterval;    /* Seconds between heartbeats */
int   PreEvent;             /* Seconds */
int   EventLength;          /* Seconds */
char  InDir[80];            /* Directory containing files to be sent */
char  SaveDir[80];          /* Save acceptable cube events here */
char  HerrinFname[80];      /* Name of file containing Herrin tables */
int   nCompCode = 0;        /* Number of component codes */
char  CompCode[MAXCOMP][4]; /* Component codes */

/* Look these up in the earthworm.h tables with getutil functions
   **************************************************************/
long          RingKey;           /* Key of transport ring for i/o */
unsigned char InstId;            /* Local installation id */
unsigned char MyModId;           /* Module Id for this program */
unsigned char TypeHeartBeat;
unsigned char TypeError;
unsigned char TypeTriglist;
pid_t         MyPid;             /* Process id of this process */


#define NCOMMAND 10

void GetConfig( char *configfile )
{
   const    ncommand = NCOMMAND;    /* Process this many required commands */
   char     init[NCOMMAND];         /* Init flags, one for each command */
   int      nmiss;                  /* Number of missing commands */
   char     *com;
   char     *str;
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;
 
/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
        printf( "getcube: Error opening command file <%s>. Exiting.\n",
                 configfile );
        exit( -1 );
   }
 
/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         com = k_str();         /* Get the first token from line */
 
/* Ignore blank lines & comments
   *****************************/
         if ( !com )           continue;
         if ( com[0] == '#' )  continue;
 
/* Open a nested configuration file
   ********************************/
         if( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "getcube: Error opening command file <%s>. Exiting.\n",
                        &com[1] );
               exit( -1 );
            }
            continue;
         }   
 
/* Process anything else as a command
   **********************************/
         if ( k_its("LogFile") )
         {
            LogFile = k_int();
            init[0] = 1;
         }
         else if ( k_its("MyModName") )
         {
            str = k_str();
            if (str) strcpy( MyModName, str );
            init[1] = 1;
         }
         else if ( k_its("RingName") )
         {
            str = k_str();
            if (str) strcpy( RingName, str );
            init[2] = 1;
         }
         else if ( k_its("HeartBeatInterval") )
         {
            HeartBeatInterval = k_long();
            init[3] = 1;
         }
         else if ( k_its("InDir") )
         {
            str = k_str();
            if (str) strcpy( (char *)InDir, str );
            init[4] = 1;
         }
         else if ( k_its("PreEvent") )
         {
            PreEvent = k_int();
            init[5] = 1;
         }
         else if ( k_its("EventLength") )
         {
            EventLength = k_int();
            init[6] = 1;
         }
         else if ( k_its("HerrinFname") )
         {
            str = k_str();
            if (str) strcpy( (char *)HerrinFname, str );
            init[7] = 1;
         }
         else if ( k_its("SaveDir") )
         {
            str = k_str();
            if (str) strcpy( (char *)SaveDir, str );
            init[8] = 1;
         }
         else if ( k_its("CompCode") )
         {
            if ( nCompCode >= MAXCOMP )
            {
               printf( "getcube: Too many components specified. Max=%d. Exiting.\n",
                       MAXCOMP );
               exit( -1 );
            }
            str = k_str();
            if ( str )
            {
               strncpy( CompCode[nCompCode], str, 4 );
               CompCode[nCompCode][3] = 0;
               nCompCode++;
            }
            init[9] = 1;
         }
 
/* Unknown command
   ***************/
         else
         {
            printf( "getcube: <%s> Unknown command in <%s>.\n",
                     com, configfile );
            continue;
         }
 
/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "getcube: Bad <%s> command in <%s>. Exiting.\n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }
 
/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] ) nmiss++;
 
   if ( nmiss )
   {
       printf( "getcube: ERROR, no " );
       if ( !init[0] ) printf( "<LogFile> "           );
       if ( !init[1] ) printf( "<MyModName> "         );
       if ( !init[2] ) printf( "<RingName> "          );
       if ( !init[3] ) printf( "<HeartBeatInterval> " );
       if ( !init[4] ) printf( "<InDir> "             );
       if ( !init[5] ) printf( "<PreEvent> "          );
       if ( !init[6] ) printf( "<EventLength> "       );
       if ( !init[7] ) printf( "<HerrinFname> "       );
       if ( !init[8] ) printf( "<SaveDir> "           );
       if ( !init[9] ) printf( "<CompCode> "          );
       printf( "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }
   return;
}
 
 
     /*****************************************************************
      *  Lookup()   Look up important info from earthworm.h tables    *
      *****************************************************************/
 
void Lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if ( ( RingKey = GetKey(RingName) ) == -1 )
   {
        printf( "getcube:  Invalid ring name <%s>. Exiting.\n", RingName);
        exit( -1 );
   }
 
/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "getcube: Error getting local installation id. Exiting.\n" );
      exit( -1 );
   }
 
/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 )
   {
      printf( "getcube: Invalid module name <%s>. Exiting.\n", MyModName );
      exit( -1 );
   }
 
/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "getcube: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "getcube: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRIGLIST2K", &TypeTriglist ) != 0 )
   {
      printf( "getcube: Invalid message type <TYPE_TRIGLIST2K>. Exiting.\n" );
      exit( -1 );
   }

/* Get our own pid for restart purposes
   ************************************/
#ifdef _SOLARIS
   MyPid = getpid();
   if ( MyPid == -1 )
   {
      logit( "e", "cuspfeeder: Can't get my pid. Exiting.\n" );
      exit( -1 );
   }
#endif
#ifdef _WINNT
   MyPid = _getpid();
#endif
   return;
}


void LogConfig( void )
{
   int i;

   logit( "", "LogFile:           %d\n", LogFile );
   logit( "", "MyModName:         %s\n", MyModName );
   logit( "", "MyModId:           %u\n", MyModId );
   logit( "", "RingName:          %s\n", RingName );
   logit( "", "HeartBeatInterval: %d\n", HeartBeatInterval );
   logit( "", "PreEvent:          %d\n", PreEvent );
   logit( "", "EventLength:       %d\n", EventLength );
   logit( "", "InDir:             %s\n", InDir );
   logit( "", "MyPid:             %d\n", MyPid );
   logit( "", "HerrinFname:       %s\n", HerrinFname );
   logit( "", "SaveDir:           %s\n", SaveDir );

   for ( i = 0; i < nCompCode; i++ )
      logit( "", "Compcode:          %s\n", CompCode[i] );
   logit( "", "\n" );
   return;
}
