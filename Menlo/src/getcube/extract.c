
/* These functions are in file tele_subs.c
   ***************************************/
void Dstaz( double, double, double, double, double * );
int  ttddel( double, double, double *, double * );
int  pkpint( double, double, double *, double * );


    /************************************************************
     *                     GetTravelTime()                      *
     *                                                          *
     *  Compute first arrival time of seismic waves from an     *
     *  earthquake to the nearest boundary of the Northern      *
     *  California Seismic Network.  Based on Herrin tables.    *
     ************************************************************/

int GetTravelTime( double latEvent, double lonEvent, double depthEvent,
                   double *minDist, double *travelTime )
{
   int    i;
   double dtdd;
   double mindist = 180.;
   double tt      = 0.;

/* Boundaries of Northern California Seismic Network
   *************************************************/
   const int maxfix = 4;         /* Number of boundary points */

   const double latds[] = {  33.,   33.,   42.,   42.};
   const double londs[] = {-114., -120., -125., -120.};

/* Compute distance from earthquake to network boundary
   ****************************************************/
   for ( i = 0; i < maxfix; i++ )
   {
      double dist;
      Dstaz( latEvent, lonEvent, latds[i], londs[i], &dist );
      if ( mindist > dist ) mindist = dist;
   }
   *minDist = mindist;

/* Compute travel time of direct P wave
   ************************************/
   if ( mindist < 98.9 )
   {
      int rc = ttddel( mindist, depthEvent, &tt, &dtdd );

      if ( rc < 0 )
      {
         printf( "ttddel() error: %d\n", rc );
         return -1;
      }
   }

/* Compute PKPIP travel time
   *************************/
   else if ( mindist > 135.0 )
   {
      int rc = pkpint( mindist, depthEvent, &tt, &dtdd );

      if ( rc < 0 )
      {
         printf( "pkpint() error: %d\n", rc );
         return -2;
      }
   }

/* Diffracted P.  Extrapolate the direct-P curve.
   *********************************************/
   else
   {
      int rc = ttddel( 98.9, depthEvent, &tt, &dtdd );

      if ( rc < 0 )
      {
         printf( "ttddel() error: %d\n", rc );
         return -3;
      }
      tt += dtdd*(mindist - 98.9);
   }

   *travelTime = tt;
   return 0;
}

