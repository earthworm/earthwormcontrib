c - aww 10/8/94 added ires and returns to subsroutines
c-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
c
      subroutine herrin (IRES)
c-----READS HERRIN TRAVEL-TIME TABLES AND SETS UP DEPVAL AND DELVAL
c-----IRES=1 FOR NORMAL RETURN (LAST VALUE READ IS CHECKED AGAINST WHAT
c     IT SHOULD BE). IRES < = 1 FOR ABNORMAL RETURN.
c
      integer IRES, IER
      real xmin(21),sec(21)
c
      real tt,delval,depval
      integer i,j
c
      common tt(201,21),delval(201),depval(21)
c
	IRES = 1
	IER = 1

      open (4,file='LIB$MATH:HERRIN.TABLE', STATUS='OLD', ERR= 910)
      rewind(4)
c
      do 15 i=1,201
      read (4,2) (xmin(j),sec(j),j=1,21)
2     format (10x,7(f4.0,f6.2)/10x,7(f4.0,f6.2)/10x,7(f4.0,f6.2))
      do 15 j=1,21
15    tt(i,j)=60.0*xmin(j)+sec(j)
      delval(1)=0.0
      do 70 i=2,201
70    delval(i)=delval(i-1)+0.5
      depval(1)=0.0
      depval(2)=15.0
      depval(3)=40.0
      depval(4)=50.0
      do 75 i=5,8
75    depval(i)=depval(i-1)+25.0
      do 30 i=9,21
30    depval(i)=depval(i-1)+50.0
      if (tt(201,21).ne.745.99) then
	IER = 2
	goto 910
      end if
c
      close(4)
      return

910   continue

      IRES = - IER 

      end
c
c-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
c
      subroutine interp (x,x1,x2,x3,x4,f1,f2,f3,f4,f,cubic,fp,deriv)
c
c-----Found original listing ("Index of Programs and Subroutines of the
c     N.C.E.R. Computer Program Library", by M.S. Hamilton, D.A.
c     Lombardero, W.H.K. Lee, and S.W. Stewart, published by National
c     Center for Earthquake Research, U.S. Geological Survey, Menlo
c     Park, CA, January, 1970).  Compared to listing, found and
c     corrected bug in computation of derivative for cubic
c     interpolation.  --J.R. Evans, 01/26/88.
c
      real x,x1,x2,x3,x4,f,f1,f2,f3,f4,f12,f23,f34,f123,f234,f1234,fp
c
      logical cubic,deriv
c
      f12=(f2-f1)/(x2-x1)
      if (.not.cubic) goto 10
      f23=(f3-f2)/(x3-x2)
      f34=(f4-f3)/(x4-x3)
      f123=(f23-f12)/(x3-x1)
      f234=(f34-f23)/(x4-x2)
      f1234=(f234-f123)/(x4-x1)
10    f=f1+(x-x1)*f12
      if (cubic) f=f+(x-x1)*(x-x2)*(f123+(x-x3)*f1234)
c
      if (.not.deriv) return
      fp=f12
      if (cubic) fp=fp+(2.*x-x1-x2)*f123+(3.*x*x-2.*x*(x1+x2+x3)+x1*x2
     *+x1*x3+x2*x3)*f1234
c
      return
      end
c
c-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
c
      subroutine locate(del,dep,ii,jj, ires)
c
      include 'LIB$MATH:STDPARM.INC'
c
      real tt,delval,depval,del,dep
      integer i,ii,j,jj
c
      common tt(201,21),delval(201),depval(21)
c
	ires = 1
      if(del.lt.0.) then
         write (STDOUT,1) del
1        format (//,' DEL IS LESS THAN 0',e14.4)
	 ires = -1	 
         return
      endif
c
10    do 15 i=1,198
      if(del.lt.delval(i+1)) goto 30
15    continue
      write (STDOUT,20) del
20    format (//,' DEL IS GREATER THAN 100',e14.4)
	ires = -2
      return
c
30    ii=i
      if(dep.gt.0.) goto 40
      write (STDOUT,35) dep
35    format (//,' DEP IS LESS THAN 0',e14.4)
	ires = -3
      return
c
40    do 45 j=1,18
      if(dep.lt.depval(j+1)) goto 60
45    continue
      write (STDOUT,50) dep
50    format (//,' DEP IS GREATER THAN 800',e14.4)
	ires = -4
      return
c
60    jj=j
      return
      end
c
c-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
c
      subroutine pcpcor (delta,depth,dc, ires)
c-----Depth-correction table and interpolation for PcP (Herrin, 1968).
c-----"dcorr" is array of depth corrections.  First index is for
c     distance (one entry per 5 degrees from 0 through 90; beyond 90
c     uses entries for 90).  Second index ("ii") for depth (0, 14, 40,
c     50, 75, 100, 125, 150, 200, ... , 800 km).
c-----J.R.Evans  01/25/88
c-----Menlo Park, CA
c
      include 'LIB$MATH:STDPARM.INC'
c
      real delta,depth,d1,d2,dl1,dl2,dt1,dt2,dc
      integer ii,kk,min0
c
      real dcorr(19,21),dcorrx(190),dcorry(209)
      equivalence (dcorr(1,1),dcorrx(1))
      equivalence (dcorr(1,11),dcorry(1))
c
      data dcorrx/0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
     *0.0,0.0,0.0,0.0,0.0,0.0,2.5,2.5,2.5,2.5,2.5,2.5,2.5,2.5,2.5,2.5,
     *2.5,2.4,2.4,2.4,2.4,2.4,2.4,2.4,2.4,6.2,6.2,6.2,6.2,6.2,6.2,6.1,
     *6.1,6.1,6.1,6.1,6.0,6.0,6.0,6.0,6.0,6.0,6.0,6.0,7.4,7.4,7.4,7.4,
     *7.4,7.4,7.4,7.3,7.3,7.3,7.3,7.2,7.2,7.2,7.2,7.2,7.2,7.2,7.2,10.5,
     *10.5,10.5,10.5,10.5,10.4,10.4,10.4,10.3,10.3,10.2,10.2,10.2,10.2,
     *10.1,10.1,10.1,10.1,10.1,13.6,13.6,13.6,13.6,13.5,13.5,13.4,13.4,
     *13.3,13.3,13.2,13.2,13.1,13.1,13.1,13.0,13.0,13.0,13.0,16.7,16.7,
     *16.7,16.6,16.6,16.5,16.4,16.4,16.3,16.2,16.2,16.1,16.1,16.0,16.0,
     *16.0,15.9,15.9,15.9,19.8,19.7,19.7,19.7,19.6,19.5,19.4,19.3,19.3,
     *19.2,19.1,19.0,19.0,18.9,18.9,18.9,18.8,18.8,18.8,25.8,25.8,25.8,
     *25.7,25.6,25.5,25.4,25.2,25.1,25.0,24.9,24.8,24.7,24.7,24.6,24.6,
     *24.5,24.5,24.5,31.8,31.7,31.7,31.6,31.5,31.3,31.2,31.0,30.9,30.7,
     *30.6,30.5,30.4,30.3,30.2,30.2,30.1,30.1,30.1/
      data dcorry/37.6,37.6,37.5,37.4,
     *37.2,37.1,36.9,36.7,36.5,36.3,36.2,36.0,35.9,35.8,35.7,35.6,35.6,
     *35.5,35.5,43.3,43.3,43.2,43.0,42.9,42.7,42.4,42.2,42.0,41.8,41.6,
     *41.4,41.3,41.1,41.0,40.9,40.9,40.8,40.8,48.8,48.8,48.7,48.5,48.3,
     *48.1,47.9,47.6,47.3,47.1,46.9,46.7,46.5,46.3,46.2,46.1,46.0,46.0,
     *45.9,54.2,54.2,54.1,53.9,53.7,53.4,53.1,52.8,52.5,52.2,52.0,51.7,
     *51.5,51.3,51.2,51.1,51.0,50.9,50.9,59.5,59.5,59.3,59.1,58.9,58.5,
     *58.2,57.9,57.5,57.2,56.9,56.6,56.4,56.2,56.0,55.9,55.8,55.7,55.7,
     *64.6,64.6,64.4,64.2,63.9,63.5,63.1,62.8,62.4,62.0,61.7,61.4,61.1,
     *60.9,60.7,60.6,60.5,60.4,60.3,69.6,69.5,69.4,69.1,68.8,68.4,67.9,
     *67.5,67.1,66.7,66.3,66.0,65.7,65.4,65.2,65.0,64.9,64.8,64.8,74.4,
     *74.4,74.2,73.9,73.5,73.1,72.6,72.1,71.6,71.2,70.8,70.4,70.1,69.8,
     *69.6,69.4,69.2,69.1,69.1,79.1,79.1,78.9,78.5,78.1,77.6,77.1,76.6,
     *76.0,75.5,75.1,74.7,74.3,74.0,73.8,73.6,73.4,73.3,73.2,83.7,83.7,
     *83.4,83.1,82.6,82.1,81.5,80.9,80.3,79.8,79.3,78.8,78.4,78.1,77.8,
     *77.6,77.4,77.3,77.3,88.3,88.2,87.9,87.6,87.0,86.5,85.8,85.2,84.5,
     *83.9,83.4,82.9,82.4,82.1,81.8,81.6,81.4,81.3,81.2/
c
	ires = 1
c-----Get depth range (subscripts ii and ii+1)
      if (depth.ge.800.) goto 2000
      if (depth.ge.150.) then
         d1=depth-amod(depth,50.)
         d2=d1+50.
         ii=ifix(d1/50.)+5
      else if (depth.ge.50.) then
         d1=depth-amod(depth,25.)
         d2=d1+25.
         ii=ifix(d1/25.)+2
      else if (depth.ge.40.) then
         d1=40
         d2=50.
         ii=3
      else if (depth.ge.15.) then
         d1=15
         d2=40.
         ii=2
      else
         d1=0.
         d2=15.
         ii=1
      endif
c
c-----Get distance range ("kk" and "kk+1")
      if (delta.lt.0..or.delta.gt.96.) goto 2020
      kk=min0(19,ifix(delta/5.)+1)
      dl1=float(kk-1)*5.
      dl2=float(kk)*5.
c
c-----linear interpolation of depth and delta for depth correction
c     (beyond 90 degrees, uses depth corrections for 90 degrees)
      dt1=(dcorr(kk,ii+1)-dcorr(kk,ii))*
     *(depth-d1)/(d2-d1) + dcorr(kk,ii)
c
      dt2=(dcorr(min0(19,kk+1),ii+1)-dcorr(min0(19,kk+1),ii))*
     *(depth-d1)/(d2-d1) + dcorr(min0(19,kk+1),ii)
c
      dc=(dt2-dt1)*(delta-dl1)/(dl2-dl1) + dt1
      return
c
c-----error return
 2000 write (STDOUT,2010)
 2010 format (/,' ERROR IN ROUTINE PCPCOR:  Depth >= 800 km')
	ires = -1
      return
 2020 write (STDOUT,2030)
 2030 format (/,' ERROR IN ROUTINE PCPCOR:  Delta > 96 degrees.')
	ires = -2
      return
      end
c
c-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
c
      subroutine pcpint (delta,depth,tt,dtdd,ires)
c-----Table and interpolation for PcP Herrin (1968) table.
c-----Distance interpolation is cubic (and derivative of
c     the interpolating curve is returned as dtdd--slowness).
c-----Table values are corrected for depth before interpolation.
c-----"tabl" is main table, one entry per degree from 0 through 96
c     degrees.  Value is traveltime in seconds with an additional
c     8 minutes implicit.
c-----Bugs:
c     Because cubic interpolation has not been told that PcP has
c        infinite apparent velocity at the epicenter, slowness is
c        returned incorrectly (=0.133 s/deg instead of 0.0) at 0
c        degrees.
c-----J.R.Evans  01/25/88
c-----Menlo Park, CA
c
      include 'LIB$MATH:STDPARM.INC'
c
      real delta,depth,tt,dtdd,dc(0:3)
c*    real del1,del2,tt1,tt2,amin0,amax0
      real t(0:3)
      integer ii,jj,max0,min0
c
      real tabl(0:96)
c
      data tabl/31.3,
     *31.4,31.5,31.7,32.1,32.5,33.0,33.6,34.4,35.2,36.1,
     *37.1,38.1,39.3,40.6,41.9,43.4,44.9,46.5,48.2,50.0,
     *51.9,53.8,55.8,57.9,60.1,62.4,64.7,67.1,69.6,72.1,
     *74.7,77.4,80.2,83.0,85.9,88.8,91.8,94.8,98.0,101.1,
     *104.3,107.6,110.9,114.3,117.7,121.2,124.7,128.3,131.9,135.5,
     *139.2,142.9,146.7,150.4,154.3,158.1,162.0,166.0,169.9,173.9,
     *177.9,181.9,186.0,190.1,194.2,198.3,202.5,206.6,210.8,215.1,
     *219.3,223.5,227.9,232.1,236.5,240.7,245.0,249.3,253.6,258.0,
     *262.4,266.7,271.1,275.5,279.9,284.3,288.7,293.1,297.5,301.9,
     *306.3,310.8,315.2,319.6,324.0,328.5/
c
c-----Get distance range.  Use "tabl" entries "jj-1" through "jj+2", to
c     give pattern:  "x1  x2  delta  x3  x4", except at bottom and top
c     of table where "x1  delta  x2  x3  x4" and "x1  x2  x3  delta
c     x4", respectively, are used.  Thus "jj" points to "x2", the table
c     entry normally just below the distance requested.
c
	ires = 1
      if (depth.ge.800.) goto 2000	! aww 4/25/95
      if (delta.lt.0..or.delta.gt.96.) goto 2020
      jj=min0(94,max0(1,ifix(delta)))
c
c-----Get depth corrections
      do 50 ii=0,3
      call pcpcor (float(jj-1+ii),depth,dc(ii), ires)
   50 continue
c
c-----Find coefficients for cubic interpolation of main table
c-----Extract table times, depth correct, and add in 8 minutes.
c
      do 60 ii=0,3
      t(ii)=tabl(jj-1+ii)-dc(ii)+480.
   60 continue
c
c*----Debug
c*    write (6,100) jj,(tabl(jj-1+ii),dc(ii),ii=0,3),
c*   *(t(ii),(jj-1+ii),ii=0,3)
c*100 format (' In PCPINT, jj=',i3,', tabl (dc)=',/,
c*   *4(f6.1,' (',f8.3,'), '),/,
c*   *'Values passed to "interp"=',4(f9.3,'(',i3,')'))
c
c-----interpolate
      call interp (delta,float(jj-1),float(jj),float(jj+1),float(jj+2),
     *t(0),t(1),t(2),t(3),tt,.true.,dtdd,.true.)
c*
c*----Interpolate for slope.  Sloppy, but adequate, except near 0
c*    and 96 degrees.  Bypasses bug in INTERP for cubic interp. slope.
c*
c*    del1=amax0(0.,delta-0.1)
c*    del2=amin0(96.,delta+0.1)
c*    call interp (del1,float(jj-1),float(jj),float(jj+1),float(jj+2),
c*   *t(0),t(1),t(2),t(3),tt1,.true.,dtdd,.false.)
c*    call interp (del2,float(jj-1),float(jj),float(jj+1),float(jj+2),
c*   *t(0),t(1),t(2),t(3),tt2,.true.,dtdd,.false.)
c*    dtdd=(tt2-tt1)/(del2-del1)

      return
c
c-----error return
 2000 write (STDOUT,2010)
 2010 format (/,' ERROR IN ROUTINE PCPINT:  Depth >= 800 km')
	ires = -1
      return
 2020 write (STDOUT,2030)
 2030 format (/,' ERROR IN ROUTINE PCPINT:  Delta > 96 degrees.')
	ires = -2
      return
      end
c
c-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
c
      subroutine pkpint (delta,depth,tt,dtdd, ires)
c-----Table and interpolation for PKIKP Herrin (1968) table.
c-----Distance interpolation is hyperbolic (and derivative of
c     the interpolating curve is returned as dtdd--slowness).
c-----Table values are corrected for depth before interpolation
c     (depth correction is a linear interpolation
c     by depth and distance of Herrin depth correction table
c     for PKIKP)
c-----J.R.Evans  08/22/79
c-----Error messages made more explicit 05/12/86.
c-----Menlo Park, CA
c
      include 'LIB$MATH:STDPARM.INC'
c
      real delta,depth,tt,dtdd,d1,d2,dl1,dl2,dt1,dt2,dc
      real t0,t1,t2,a2b2,a2
      integer ii,jj,i1,i2,i3,i4
c
      real dcorr(114),tabl(71)
      data tabl/33.0,34.9,36.8,38.7,40.7,42.6,44.5,46.4,48.3,50.2,52.1,
     *54.0,55.9,57.8,59.8,1.7,3.7,5.6,7.5,9.4,11.3,13.2,15.1,17.0,18.9,
     *20.8,22.7,24.6,26.5,28.3,30.1,31.9,33.7,35.4,37.1,38.8,40.5,42.1,
     *43.7,45.3,46.8,48.3,49.8,51.2,52.6,53.9,55.3,56.6,57.8,58.9,0.0,
     *1.0,2.0,3.0,4.0,4.9,5.7,6.5,7.2,7.8,8.4,8.9,9.3,9.7,10.1,10.4,
     *10.6,10.8,10.9,11.0,11.0/
      data dcorr/0.0,0.0,0.0,0.0,0.0,0.0,2.5,2.5,2.5,2.5,2.5,2.5,6.2,
     *6.2,6.2,6.2,6.2,6.2,7.4,7.4,7.4,7.4,7.4,7.4,13.5,13.5,13.6,13.6,
     *13.6,13.6,19.6,19.6,19.6,19.7,19.7,19.8,25.6,25.6,25.7,25.7,
     *25.8,25.8,31.4,31.5,31.6,31.7,31.7,31.8,37.2,37.3,37.4,37.5,37.6,
     *37.6,42.8,42.9,43.0,43.1,43.2,43.3,48.3,48.4,48.5,48.7,48.8,48.8,
     *53.6,53.7,53.9,54.0,54.2,54.2,58.8,58.9,59.1,59.3,59.4,59.5,63.8,
     *63.9,64.1,64.4,64.5,64.6,68.7,68.8,69.1,69.3,69.5,69.6,73.4,73.6,
     *73.8,74.1,74.3,74.4,78.1,78.2,78.5,78.8,79.0,79.1,82.6,82.7,83.0,
     *83.4,83.6,83.7,87.0,87.2,87.5,87.9,88.2,88.3/
c
c-----find depth correction
c-----depth range
	ires = 1
      if (depth.ge.800.) goto 2000
      if (depth.ge.50.) goto 50
      if (depth.ge.40.) goto 40
      if (depth.ge.15.) goto 15
      d1=0.
      d2=15.
      ii=0
      goto 100
15    d1=15
      d2=40.
      ii=1
      goto 100
40    d1=40
      d2=50.
      ii=2
      goto 100
50    d1=depth-amod(depth,50.)
      d2=d1+50.
      ii=ifix(d1/50.+2.0)
100   ii=6*ii
c
c-----distance range
      if (delta.ge.140.) goto 140
      if (delta.lt.110..or.delta.gt.180.) goto 2020
      jj=1
      dl1=110.
      dl2=140.
      goto 200
140   jj=ifix((delta-140.)/10.)+2
      if (jj.eq.6) jj=5
      dl1=140.+10.*float(jj-2)
      dl2=dl1+10.
200   i1=ii+jj
      if (jj.gt.5.or.jj.lt.1) goto 2040
      i2=ii+6+jj
      i3=ii+jj+1
      i4=ii+6+jj+1
c
c-----linear interpolation of depth and delta for depth correction
      dt1=(depth-d1)*(dcorr(i2)-dcorr(i1))/(d2-d1)+dcorr(i1)
      dt2=(depth-d1)*(dcorr(i4)-dcorr(i3))/(d2-d1)+dcorr(i3)
      dc=(delta-dl1)*(dt2-dt1)/(dl2-dl1)+dt1
c
c-----find coefficients for hyperbolic interpolation of main table
c-----t0 is presumed asymptote intersection with 180 degrees
      t0=1306.90-dc
      ii=ifix(delta-110.)
      ii=ii+1
c
c-----extract table times, depth correct, and add in 18,19,or 20 minutes
c-----as apropriate (Note that slowness is not well corrected
c-----for depth since only the depth correction at "delta" is used.
c-----Slowness will be correct at depth=0 and times are correct
c-----at all depths.)
c
      t1=tabl(ii)-dc+1080.
      t2=tabl(ii+1)-dc+1080.
      if (ii.gt.15) t1=t1+60.
      if (ii.gt.14) t2=t2+60.
      if (ii.gt.50) t1=t1+60.
      if (ii.gt.49) t2=t2+60.
      dl1=109.+float(ii)
      dl2=dl1+1.
      a2b2=((t1-t0)*(t1-t0)-(t2-t0)*(t2-t0))/((dl1-180.)*(dl1-180.)-
     *(dl2-180.)*(dl2-180.))
      a2=(t1-t0)*(t1-t0)-a2b2*(dl1-180.)*(dl1-180.)
c
c-----interpolate
      tt=t0-sqrt(a2+a2b2*(delta-180.)*(delta-180.))
      dtdd=a2b2*(180.-delta)/(t0-tt)
      return
c
c-----error return
 2000 write (STDOUT,2010)
 2010 format (/,' ERROR IN ROUTINE PKPINT:  Depth >= 800 km')
	ires = -1
      return
 2020 write (STDOUT,2030)
 2030 format (/,' ERROR IN ROUTINE PKPINT:  Delta < 110 or > 180.')
	ires = -2
      return
 2040 write (STDOUT,2050)
 2050 format (/,' ERROR IN ROUTINE PKPINT:  Delta out of range.')
	ires = -3
      return
      end
c
c-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
c
      subroutine rrout(ne,stname,refsta,qual)
c-----PRINTS TABLE OF RELATIVE RESIDUALS
c-----MODIFIED 12/01/81 BY J. EVANS TO PRINT RESIDUALS RELATIVE
c     TO THE MEAN AND MEDIAN RESIDUALS AND RELATIVE TO THE WEIGHTED
c     MEAN RESIDUAL (Weights used are:
c     "A"="a"="0"=1.0; "B"="b"="1"=0.5; "C"="c"="2"=0.2)
c
      include 'LIB$MATH:STDPARM.INC'
c
      real rr(5),refres(5),rmean,rmed(MAXSTA+1),rmn,rmd,rmeanw,rmnwt
      real resid,resid1,wtot,w
      integer dta,flag,nrs(5),nmn,nsta,nrsta,ne,nr,ns,i
      common /rrcmmn/ nsta,nrsta,dta(5,MAXSTA),resid(5,MAXSTA)
      character*4 stname(MAXSTA+1),refsta(5)
      character*1 qual(5,MAXSTA+1),ex,exx,one,two,three,four
      character*1 ah,be,ce,ahh,bee,cee,q,zero,de,dee
      data flag/0/,ex,exx,four/'x','X','4'/
      data zero,one,two,three/'0','1','2','3'/
      data ah,be,ce,ahh,bee,cee/'a','b','c','A','B','C'/
      data de,dee/'d','D'/
c
c-----FIRST CALL TO SUBROUTINE? (probable bug since "flag" not in
c     common block or call list--result is that this initializing
c     happens always).
c
      if (flag.eq.1) goto 100
      flag=1
c
c-----FIND SUBSCRIPTS OF REFERENCE STATIONS IN OTHER ARRAYS (STNAME...)
      do 50 nr=1,nrsta
      nrs(nr)=0
      do 40 ns=1,nsta
      if (refsta(nr).eq.stname(ns)) nrs(nr)=ns
40    continue
      if (nrs(nr).ne.0) goto 50
      write (STDOUT,45) refsta(nr)
45    format (/,' *****REFERENCE STATION ',a4,' NOT IN STATION LIST',
     *'*****',/)
50    continue
c
c-----WRITE PAGE HEADING
100   write (STDOUT,110) (refsta(i),qual(ne,nrs(i)),i=1,nrsta)
110   format (/,' REF STA-->','       MEAN','  WT. MEAN'
     *,'    MEDIAN',5(3x,a4,'(',a1,')'))
      write (STDOUT,120)
120   format (' QTY STATION',/)
c
c-----FIND MEAN AND MEDIAN RESIDUALS (SKIP "X", "D" AND "4" QUALITIES)
      rmn=0.
      rmnwt=0.
      wtot=0.
      rmd=0.
      nmn=0
      do 140 ns=1,nsta
      if (dta(ne,ns).eq.0) goto 140
      q=qual(ne,ns)
      if (q.eq.ex) goto 140
      if (q.eq.exx) goto 140
      if (q.eq.de) goto 140
      if (q.eq.dee) goto 140
      if (q.eq.four) goto 140
      if (q.eq.three) goto 140
      w=0.
      if (q.eq.ah.or.q.eq.ahh.or.q.eq.zero) w=1.0
      if (q.eq.be.or.q.eq.bee.or.q.eq.one) w=0.5
      if (q.eq.ce.or.q.eq.cee.or.q.eq.two) w=0.2
      wtot=wtot+w
      rmnwt=rmnwt+w*resid(ne,ns)
      nmn=nmn+1
      rmn=rmn+resid(ne,ns)
      rmed(nmn)=resid(ne,ns)
140   continue
      if (nmn.eq.0) goto 180
      rmnwt=rmnwt/wtot
      if (nmn.eq.1) goto 170
      rmn=rmn/float(nmn)
      do 160 nr=1,nmn
      do 150 ns=2,nmn
      if (rmed(ns-1).le.rmed(ns)) goto 150
      rmd=rmed(ns-1)
      rmed(ns-1)=rmed(ns)
      rmed(ns)=rmd
  150 continue
  160 continue
  170 rmd=0.5*(rmed((nmn+1)/2)+rmed((nmn+2)/2))
c
c-----FIND REFERENCE RESIDUALS OR SET EQUAL TO 1000.
180   do 200 nr=1,nrsta
      if (nrs(nr).eq.0) goto 190
      if (dta(ne,nrs(nr)).eq.0) goto 190
      refres(nr)=resid(ne,nrs(nr))
      goto 200
190   refres(nr)=1000.
200   continue
c
c-----LOOP OVER STATIONS FOR RR'S
      do 300 ns=1,nsta
      if (dta(ne,ns).eq.0) goto 300
      resid1=resid(ne,ns)
c
c-----LOOP OVER REFERENCE STATIONS
      do 250 nr=1,nrsta
      if (refres(nr).gt.900.) goto 230
      rr(nr)=resid1-refres(nr)
      goto 250
230   rr(nr)=999.999
250   continue
c
c-----CALCULATE RR'S VERSUS MEAN AND MEDIAN RESIDUALS
      rmean=999.999
      rmeanw=999.999
      rmed(ns)=999.999
      if (nmn.eq.0) goto 255
      rmean=resid1-rmn
      rmeanw=resid1-rmnwt
      rmed(ns)=resid1-rmd
c
c-----WRITE OUT
  255 write (STDOUT,260) qual(ne,ns),stname(ns),rmean,rmeanw,rmed(ns),
     *(rr(i),i=1,nrsta)
260   format (a1,7x,a4,8f10.3)
300   continue
      return
      end
c
c-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
c
      subroutine ttddel (del,dep,t,dtddel, ires)
c
c     CALCULATES THE TRAVEL TIME "t" AND THE DERIVATIVE OF "t" WITH
c     RESPECT TO DISTANCE ("dtddel").
c     ARGUMENTS
c           DEL   DISTANCE (degrees)
c           DEP   DEPTH (km)
c           T     TRAVEL TIME STORED HERE (seconds)
c           DTDDEL DERIVATIVE STORED HERE (seconds/degree)
c
      include 'LIB$MATH:STDPARM.INC'
c
      real tt,delval,depval,del,dep,t,dtddel,f1,f2,f3,f4,fp
      integer ii,jj
c
      common tt(201,21),delval(201),depval(21)
      logical cubic
c
	ires = 1
      call locate (del,dep,ii,jj,ires)
	if (ires .le. 0) return
      cubic=.false.
      if (del.lt.2.) cubic=.true.
      call interp (dep,depval(jj),depval(jj+1),depval(jj+2),
     *depval(jj+3),tt(ii,jj),tt(ii,jj+1),tt(ii,jj+2),tt(ii,jj+3),
     *f1,cubic,fp,.false.)
      call interp (dep,depval(jj),depval(jj+1),depval(jj+2),
     *depval(jj+3),tt(ii+1,jj),tt(ii+1,jj+1),tt(ii+1,jj+2),
     *tt(ii+1,jj+3),f2,cubic,fp,.false.)
      call interp (dep,depval(jj),depval(jj+1),depval(jj+2),
     *depval(jj+3),tt(ii+2,jj),tt(ii+2,jj+1),tt(ii+2,jj+2),
     *tt(ii+2,jj+3),f3,cubic,fp,.false.)
      call interp (dep,depval(jj),depval(jj+1),depval(jj+2),
     *depval(jj+3),tt(ii+3,jj),tt(ii+3,jj+1),tt(ii+3,jj+2),
     *tt(ii+3,jj+3)