
  /*********************************************************************
   *                              getcube                              *
   *                                                                   *
   *  This program grabs a single cube event from a file, converts it  *
   *  to a triglist message, and writes the message to a ring.         *
   *********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include <time_ew.h>
#include "getcube.h"

/* Function declarations
   *********************/
void GetConfig( char * );
void LogConfig( void );
void Lookup( void );
void SendStatus( unsigned char, short, char * );
int  MsgConvert( char *, char *, int );
int  GetEventId( char [], int * );
int  PutEventId( char [], int );
int  RejectEvent( char * );
void Dstaz( double, double, double, double, double * );
int  herrin( const char [] );
int  GetTravelTime( double, double, double, double *, double * );

/* Global variables
   ****************/
static SHM_INFO region;            /* Shared memory region to use for i/o */
static pid_t    myPid;             /* Process id of this process */

extern unsigned char TypeHeartBeat;
extern unsigned char TypeError;
extern unsigned char TypeTriglist;
extern unsigned char InstId;       /* Local installation id */
extern unsigned char MyModId;      /* Module Id for this program */
extern char     HerrinFname[80];   /* Name of file containing Herrin tables */



int main( int argc, char *argv[] )
{
   extern char RingName[20];       /* Name of transport ring for i/o */
   extern char InDir[80];          /* Directory containing files to be sent */
   extern int  LogFile;            /* If 1, log to disk */
   extern long HeartBeatInterval;  /* Seconds between heartbeats */
   extern char SaveDir[80];        /* Save acceptable cube events here */
   extern long RingKey;            /* Key of transport ring for i/o */

   long timeNow;                   /* Current time */
   long timeLastBeat;              /* Time last heartbeat was sent */
   char *trigbuf;                  /* Buffer to hold triglist msg */
   char text[100];                 /* To contain status messages */
   int  event_id;

   char defaultConfig[] = "getcube.d";
   char *configFileName = (argc > 1 ) ? argv[1] : &defaultConfig[0];

/* Read the configuration file
   ***************************/
   GetConfig( configFileName );
     
/* Look up important info in the earthworm.h tables
   ************************************************/
   Lookup();

/* Set up logging
   **************/
   logit_init( argv[0], (short)MyModId, MAX_TRIG_BYTES, LogFile );

/* Get my own pid for restart purposes
   ***********************************/
#ifdef _SOLARIS
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "getcube: Can't get my pid. Exiting.\n" );
      return -1;
   }
#endif

#ifdef _WINNT
   myPid = _getpid();
#endif

/* Log the configuration file parameters
   *************************************/
   LogConfig();

/* Read Herrin tables (only once)
   ******************************/
   if ( herrin( HerrinFname ) < 0 )
   {
      printf( "Error getting Herrin tables from file: %s\n", HerrinFname );
      printf( "Exiting.\n" );
      return -1;
   }

/* Allocate the triglist message buffer
   ************************************/
   trigbuf = (char *) malloc( (size_t)MAX_TRIG_BYTES );
   if ( trigbuf == NULL )
   {
      logit( "e", "getcube: Can't allocate triglist buffer. Exiting.\n" );
      return -1;
   }

/* Attach to Input/Output shared memory ring
   *****************************************/
   tport_attach( &region, RingKey );

/* Change working directory to "InDir"
   **********************************/
   if ( chdir_ew( InDir ) == -1 )
   {
      logit( "e", "Error. Can't change working directory to %s\n Exiting.",
           InDir );
      return -1;
   }

/* Force a heartbeat to be issued in first pass thru main loop
   ***********************************************************/
   timeLastBeat = time(&timeNow) - HeartBeatInterval - 1;

/* Get the name of a cube file.
   If rc==1, the InDir directory is empty.
   **************************************/
   while ( 1 )
   {
      int      rc;
      int      i;
      int      linelen;
      int      nullfound = 0;
      FILE     *fp;
      char     fname[MAXC];
      char     line[MAXC];
      char     command[100];
      MSG_LOGO logo;

/* Beat the heart
   **************/
      if  ( (time(&timeNow) - timeLastBeat) >= HeartBeatInterval )
      {  
          timeLastBeat = timeNow;
          SendStatus( TypeHeartBeat, 0, "" );
      }  
 
/* See if termination has been requested
   *************************************/
      if ( tport_getflag( &region ) == TERMINATE  ||
           tport_getflag( &region ) == myPid )
         break;

/* Get name of a cube file to process
   **********************************/
      rc = GetFileName( fname );

      if ( rc == 1 )       /* Sleep a while and try again */
      {
         sleep_ew( 500 );
         continue;
      }

/* Open file for reading only
   **************************/
      fp = fopen( fname, "rb" );
      if ( fp == NULL )
      {
         sleep_ew( 1000 );
         continue;
      }

/* Get line from file and strip off newline character
   **************************************************/
      line[0] = '\0';
      fgets( line, MAXC, fp );
      fclose( fp );

      for ( i = 0; i < MAXC; i++ )
      {
         if ( line[i] == '\0' )
         {
            nullfound = 1;
            if ( line[i-1] == '\n' )
            {
               line[i-1] = '\0';
               break;
            }
         }
      }

/* The line is too short or long, pitch the event file
   ***************************************************/
      if ( !nullfound )
      {
         sprintf( text, "Line in cube file %s is too long (> %d chars)\n",
                fname, MAXC-1 );
         SendStatus( TypeError, ERR_LINETOOBIG, text );
         if ( remove( fname ) == -1 )
            logit( "et", "Error erasing file %s\n", fname );
         continue;
      }
      linelen = i;
      if ( linelen < 28 )
      {
         if ( remove( fname ) == -1 )
            logit( "et", "Error erasing file %s\n", fname );
         continue;
      }

/* Log the event
   *************/
      logit( "", "\n" );
      logit( "t", "Cube event file found: %s\n", fname );
      logit( "", "%s\n", line );

/* Reject particular events using a set of hardwired rules
   *******************************************************/
      if ( RejectEvent( line ) )
      {
         logit( "", "Event rejected.\n" );
         if ( remove( fname ) == -1 )
            logit( "et", "Error erasing file %s\n", fname );
         continue;
      }

/* Get event id from name of event file.
   Event files are named out.xxxxxxxx
   ************************************/
      if ( sscanf( &fname[4], "%d", &event_id ) < 1 )
      {
         sprintf( text, "Can't read event id from cube file name. Exiting.\n" );
         SendStatus( TypeError, ERR_EVENTIDERR, text );
         return -1;
      }

/* Convert cube event to triglist message
   **************************************/
      if ( MsgConvert( line, trigbuf, event_id ) == -1 )
      {
         sprintf( text, "Can't decode cube event.\n" );
         SendStatus( TypeError, ERR_CUBEDECODE, text );
         if ( remove( fname ) == -1 )
            logit( "et", "Error erasing file %s\n", fname );
         continue;
      }
      logit( "", "CUBE FILE CONVERTED TO TRIGLIST MESSAGE.\n" );

/* Send the trigbuf message to the transport ring
   **********************************************/
      logo.instid = InstId;
      logo.mod    = MyModId;
      logo.type   = TypeTriglist;

      if ( tport_putmsg( &region, &logo, strlen(trigbuf), trigbuf ) != PUT_OK )
      {
         sprintf( text, "Error sending triglist message to transport ring.\n" );
         SendStatus( TypeError, ERR_PUTMSG, text );
      }

/* Save the file for possible later use
   ************************************/
#ifdef _SOLARIS
      sprintf( command, "/usr/bin/cp -f %s %s", fname, SaveDir );
#endif
#ifdef _WINNT
      sprintf( command, "copy %s %s", fname, SaveDir );
#endif
      if ( system( command ) == -1 )
      {
         logit( "et", "Error copying file %s to %s\n", fname, SaveDir );
         sleep_ew( 300000 );
      }

      if ( remove( fname ) == -1 )
      {
         logit( "et", "Error erasing file %s\n", fname );
         sleep_ew( 300000 );
      }
   }
   return 0;
}


/*************************************************************************
 * SendStatus() builds a heartbeat or error message & puts it into       *
 *              shared memory.  Writes errors to log file & screen.      *
 *************************************************************************/

void SendStatus( unsigned char type, short ierr, char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   long     t;

/* Build the message
   *****************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );
 
   if ( type == TypeHeartBeat )
      sprintf( msg, "%ld %d\n", t, myPid );
   else if ( type == TypeError )
   {
      sprintf( msg, "%ld %hd %s\n", t, ierr, note );
      logit( "et", "getcube: %s\n", note );
   }
   else return;
 
   size = strlen( msg );

/* Write the message to shared memory
   **********************************/
   if ( tport_putmsg( &region, &logo, size, msg ) != PUT_OK )
   {
      if ( type == TypeHeartBeat )
         logit( "et", "getcube: Error sending heartbeat.\n" );
      if ( type == TypeError )
         logit( "et", "getcube: Error sending error:%d.\n", ierr );
   }
   return;
}


     /******************************************************************
      *                         RejectEvent()                          *
      *                                                                *
      *  Determine in an event should be rejected, according to a      *
      *  hardwired set of criteria.                                    *
      *                                                                *
      *  Returns 0, if we want this event.                             *
      *          1, if we want to reject this event.                   *
      ******************************************************************/

int RejectEvent( char *event )
{
   char   str[20];
   int    mag, lat, lon, depth;
   double dist_deg;

   const double menlo_lat =   37.455425;
   const double menlo_lon = -122.166417;

/* If the magnitude is >= 6.0, we want this event.
   If the magnitude is < 5.5, reject this event.
   **********************************************/
   memcpy( str, event+47, 2 );
   str[2] = '\0';
   if ( sscanf( str, "%d", &mag ) < 1 ) return 1;
   if ( mag > 59 ) return 0;
   if ( mag < 55 ) return 1;

/* If the focal depth is < 50 km, reject the event 
   ***********************************************/
   memcpy( str, event+43, 4 );
   str[4] = '\0';
   if ( sscanf( str, "%d", &depth ) < 1 ) return 1;
   if ( depth < 500 ) return 1;

/* If the event is > 40 degrees from Menlo Park, reject the event
   **************************************************************/
   memcpy( str, event+28, 7 );
   str[7] = '\0';
   if ( sscanf( str, "%d", &lat ) < 1 ) return 1;

   memcpy( str, event+35, 8 );
   str[8] = '\0';
   if ( sscanf( str, "%d", &lon ) < 1 ) return 1;

   Dstaz( menlo_lat, menlo_lon, (0.0001*lat), (0.0001*lon), &dist_deg );

   if ( dist_deg > 40. ) return 1;

/* logit( "", "dist_deg = %.5lf\n", dist_deg ); */

/* The event passed all tests, so we want it
   *****************************************/
   return 0;
}


     /******************************************************************
      *                          MsgConvert()                          *
      *                                                                *
      *  Convert a Cube event line to a triglist message.              *
      ******************************************************************/

int MsgConvert( char *event, char *trigbuf, int event_id )
{
   char   str[20];
   int    i;
   int    rc;
   int    year, month, day, hour, min, secd;
   int    sec, hunsec;
   int    latd, lond, dep;
   int    nc = 0;
   char   *ptr = trigbuf;
   time_t eventTime;
   double minDist, travelTime;
   double latEvent;
   double lonEvent;
   double depthEvent;
   struct tm stm;

   extern int  PreEvent;             /* Seconds */
   extern int  EventLength;          /* Seconds */
   extern int  nCompCode;            /* Number of component codes */
   extern char CompCode[MAXCOMP][4]; /* Component codes */

/* Decode the event time and location
   **********************************/
   memcpy( str, event+13, 4 );
   str[4] = '\0';
   if ( sscanf( str, "%d", &year ) < 1 )  return -1;
   memcpy( str, event+17, 2 );
   str[2] = '\0';
   if ( sscanf( str, "%d", &month ) < 1 ) return -1;
   memcpy( str, event+19, 2 );
   str[2] = '\0';
   if ( sscanf( str, "%d", &day ) < 1 )   return -1;
   memcpy( str, event+21, 2 );
   str[2] = '\0';
   if ( sscanf( str, "%d", &hour ) < 1 )  return -1;
   memcpy( str, event+23, 2 );
   str[2] = '\0';
   if ( sscanf( str, "%d", &min ) < 1 )   return -1;
   memcpy( str, event+25, 3 );
   str[3] = '\0';
   if ( sscanf( str, "%d", &secd ) < 1 )  return -1;
   sec    = secd / 10;
   hunsec = 10 * (secd - (10 * sec));

   memcpy( str, event+28, 7 );
   str[7] = '\0';
   if ( sscanf( str, "%d", &latd ) < 1 ) return -1;
   latEvent = (double)latd / 10000.;

   memcpy( str, event+35, 8 );
   str[8] = '\0';
   if ( sscanf( str, "%d", &lond ) < 1 ) return -1;
   lonEvent = (double)lond / 10000.;

   memcpy( str, event+43, 4 );
   str[4] = '\0';
   if ( sscanf( str, "%d", &dep  ) < 1 ) return -1;
   depthEvent = (double)dep / 10.;

/* Convert event time to an integer
   ********************************/
   stm.tm_year = year - 1900;
   stm.tm_mon  = month - 1;
   stm.tm_mday = day;
   stm.tm_hour = hour;
   stm.tm_min  = min;
   stm.tm_sec  = sec;
   eventTime   = timegm_ew( &stm );

/* Subtract the pre-event time
   ***************************/
   eventTime -= PreEvent;

/* Add first arrival travel time obtained from Herrin tables.
   If an error occurs, assume zero travel time.
   *********************************************************/
   rc = GetTravelTime( latEvent, lonEvent, depthEvent, &minDist, &travelTime );
   if ( rc < 0 )
   {
      printf( "GetTravelTime() error: %d\n", rc );
      travelTime = 0.;
   }
   eventTime += (time_t)travelTime;

/* Convert time back to a structure
   ********************************/
   gmtime_ew( &eventTime, &stm );

/* Encode the trigbuf message
   **************************/
   ptr += sprintf( ptr, "EVENT DETECTED     " );
   ptr += sprintf( ptr, "%4d%02d%02d", year, month, day );
   ptr += sprintf( ptr, " %02d:%02d:%02d.%02d UTC", hour, min, sec, hunsec );
   ptr += sprintf( ptr, " EVENT ID: %06d", event_id );
   ptr += sprintf( ptr, " AUTHOR: %03u%03u%03u\n", TypeTriglist, MyModId, InstId );
   ptr += sprintf( ptr, "\n" );
   ptr += sprintf( ptr, "Sta/Net/Cmp   Date   Time                       start save       duration in sec.\n" );
   ptr += sprintf( ptr, "-----------   ------ ---------------    -----------------------------------------\n" );

/* Encode each component line
   **************************/
   for ( i = 0; i < nCompCode; i++ )
   {
      ptr += sprintf( ptr, "   * %3s *  P 20000101 00:00:00.00 UTC",
                      CompCode[i] );
      ptr += sprintf( ptr, " save: %4d%02d%02d", stm.tm_year+1900, stm.tm_mon+1,
                      stm.tm_mday );
      ptr += sprintf( ptr, " %02d:%02d:%02d.%02d", stm.tm_hour, stm.tm_min,
                      stm.tm_sec, hunsec );
      ptr += sprintf( ptr, " %8d\n", EventLength );
   }
   return 0;
}

/*
12345678901234567890123456789012345678901234567890123456789012345678901234567890
EVENT DETECTED     20000107 01:31:33.00 UTC EVENT ID: 054629 AUTHOR: 025057003

Sta/Net/Cmp   Date   Time                       start save       duration in sec.
-----------   ------ ---------------    -----------------------------------------
 AAR * NC P 20000107 01:32:08.00 UTC save: 20000107 01:31:03.00      172 
*/

