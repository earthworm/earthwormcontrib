
  /*******************************************************************************
   *                                   kill_it.c                                 *
   *                                                                             *
   *  Program to kill the getevent program by sending it zero byte count.        *
   *  WMK 7/29/98                                                                *
   *******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define SUCCESS   0
#define FAILURE  -1
#define MAXCHAR  80

/* Function declarations
   *********************/
int GetConfig( char * );
int SendKillMsg( void );

/* Parameters to read from configuration file
   ******************************************/
static char RemoteIpAddress[80];    /* IP address of system to receive trigger */
static int  WellKnownPort;          /* The well-known port number */


int main( int argc, char **argv )
{
   char defaultConfig[] = "kill_it.d";
   char *configFileName = defaultConfig;

/* Get config file name from command line
   **************************************/
   if ( argc > 1 )
      configFileName = argv[1];

/* Read the configuration file
   ***************************/
   if ( GetConfig( configFileName ) == FAILURE )
   {
      printf( "Exiting.\n" );
      return FAILURE;
   }

/* Print the configuration file parameters
   ***************************************/
/* printf( "Ip: %s  Port: %d\n", RemoteIpAddress, WellKnownPort ); */

/* Send the kill message to the getevent program
   *********************************************/
   if ( SendKillMsg() == FAILURE )
   {
      printf( "Error sending kill message to the getevent program.\n" );
      return FAILURE;
   }

   return SUCCESS;
}


     /************************************************************
      *                      SendKillMsg()                       *
      ************************************************************/

int SendKillMsg( void )
{
   extern int errno;                  /* Contains socket error numbers */
   struct sockaddr_in server;         /* Server socket address structure */
   int sd;                            /* Socket descriptor */
   int rc;
   const int optVal = 1;
   int nwritten;
   const unsigned byteCount = 0;

/* Get a new socket descriptor
   ***************************/
   sd = socket( AF_INET, SOCK_STREAM, 0 );
   if ( sd == -1 )
   {
      printf( "socket() error. errno: %d\n", errno );
      return FAILURE;
   }

/* Allow reuse of socket addresses
   *******************************/
   if ( setsockopt( sd, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
                    sizeof(int) ) == -1 )
   {
      printf( "setsockopt() error. errno: %d\n", errno );
      close( sd );
      return FAILURE;
   }

/* Fill in socket address structure
   ********************************/
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family      = AF_INET;
   server.sin_port        = htons((unsigned short)WellKnownPort);
   server.sin_addr.s_addr = inet_addr( RemoteIpAddress );

/* Connect to the getevent socket
   ******************************/
   if ( connect( sd, (struct sockaddr *)&server, sizeof(server) ) == -1 )
   {
      printf( "Can't connect to server.\n" );
      close( sd );
      return FAILURE;
   }

/* Send (unsigned)0 to the getevent program
   ****************************************/
   rc = send( sd, (char *)&byteCount, sizeof(unsigned), NULL );
   if ( rc == -1 )
   {
      printf( "send() error. errno: %d\n", errno );
      close( sd );
      return FAILURE;
   }
   nwritten = rc;
   if ( nwritten < sizeof(unsigned) )
   {
      printf( "send() error. nwritten(%d) < %d\n", nwritten, sizeof(unsigned) );
      close( sd );
      return FAILURE;
   }

   close( sd );
   return SUCCESS;
}


        /***************************************************************
         *                        GetConfig()                          *
         *                                                             *
         *                Read the configuration file.                 *
         ***************************************************************/

int GetConfig( char *configfile )
{
   FILE  *fp;
   int   i;
   int   nmiss;                  /* Number of missing commands */
   const ncommand = 2;           /* Process this many required commands */
   char  init[2];                /* Init flags, one for each command */
   char  *com;
   char  *arg;
   char  str[MAXCHAR];

/* Initialize flags
   ****************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the configuration file
   ***************************/
   fp = fopen( configfile, "r" );
   if ( fp == NULL )
   {
      printf( "Error opening command file: <%s>\n", configfile );
      return FAILURE;
   }

/* Read next line from config file
   *******************************/
   while ( fgets( str, MAXCHAR, fp ) != NULL )
   {
      com = strtok( str, " \n" );               /* Get first token from line */

/* Ignore blank lines & comments
   *****************************/
      if ( com == NULL )  continue;
      if ( *com == '#' )  continue;

/* Process anything else as a command
   **********************************/
      if ( strcmp( com, "RemoteIpAddress" ) == 0 )
      {
         arg = strtok( NULL, " " );
         if ( arg == NULL )
         {
            printf( "Missing RemoteIpAddress in config file.\n" );
            return FAILURE;
         }
         strcpy( RemoteIpAddress, arg );
         init[0] = 1;
      }
      else if ( strcmp( com, "WellKnownPort" ) == 0 )
      {
         arg = strtok( NULL, " " );
         if ( arg == NULL )
         {
            printf( "Missing WellKnownPort in config file.\n" );
            return FAILURE;
         }
         if ( sscanf( arg, "%d", &WellKnownPort ) < 1 )
         {
            printf( "Invalid WellKnownPort in config file.\n" );
            return FAILURE;
         }
         init[1] = 1;
      }
      else
      {
         printf( "<%s> Unknown command in <%s>.\n", com, configfile );
         return FAILURE;
      }
   }
 
/* Close the config file
   *********************/
   fclose( fp );

/* Check init flags for missed commands
   ************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
      printf( "ERROR, no " );
      if ( !init[0] ) printf( "<RemoteIpAddress> " );
      if ( !init[1] ) printf( "<WellKnownPort> "   );
      printf( "command(s) in <%s>\n", configfile );
      return FAILURE;
   }
   return SUCCESS;
}

