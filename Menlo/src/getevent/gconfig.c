
  /****************************************************************************
   *                                 config.c                                 *
   *                                                                          *
   *                  Function to read the configuration file.                *
   ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SUCCESS 0
#define FAILURE -1
#define MAXCHAR  80
#define NCOMMAND 3

/* Parameters to get from the configuration file
   *********************************************/
char  MyIpAddress[80];        /* My own IP address */
int   WellKnownPort;          /* The well-known port number */
int   RetryInterval;          /* Sleep this long between net retrys */


               /*****************************************
                *  GetConfig() reads the config file.   *
                *****************************************/

int GetConfig( char *configfile )
{
   FILE     *fp;
   const    ncommand = NCOMMAND;    /* Process this many required commands */
   char     init[NCOMMAND];         /* Init flags, one for each command */
   int      nmiss;                  /* Number of missing commands */
   char     *com;
   char     *arg;
   char     str[MAXCHAR];
   int      i;

/* Initialize flags
   ****************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the configuration file
   ***************************/
   fp = fopen( configfile, "r" );
   if ( fp == NULL )
   {
      printf( "Error opening command file: <%s>\n", configfile );
      return FAILURE;
   }

/* Read next line from config file
   *******************************/
   while ( fgets( str, MAXCHAR, fp ) != NULL )
   {
      com = strtok( str, " \n" );               /* Get first token from line */

/* Ignore blank lines & comments
   *****************************/
      if ( com == NULL )  continue;
      if ( *com == '#' )  continue;

/* Process anything else as a command
   **********************************/
      if ( strcmp( com, "MyIpAddress" ) == 0 )
      {
         arg = strtok( NULL, " " );
         if ( arg == NULL )
         {
            printf( "Missing MyIpAddress in config file.\n" );
            return FAILURE;
         }
         strcpy( MyIpAddress, arg );
         init[0] = 1;
      }
      else if ( strcmp( com, "WellKnownPort" ) == 0 )
      {
         arg = strtok( NULL, " " );
         if ( arg == NULL )
         {
            printf( "Missing WellKnownPort in config file.\n" );
            return FAILURE;
         }
         if ( sscanf( arg, "%d", &WellKnownPort ) < 1 )
         {
            printf( "Invalid WellKnownPort in config file.\n" );
            return FAILURE;
         }
         init[1] = 1;
      }
      else if ( strcmp( com, "RetryInterval" ) == 0 )
      {
         arg = strtok( NULL, " " );
         if ( arg == NULL )
         {
            printf( "Missing RetryInterval in config file.\n" );
            return FAILURE;
         }
         if ( sscanf( arg, "%d", &RetryInterval ) < 1 )
         {
            printf( "Invalid RetryInterval in config file.\n" );
            return FAILURE;
         }
         init[2] = 1;
      }
      else
      {
         printf( "<%s> Unknown command in config file <%s>\n", com, configfile );
         return FAILURE;
      }
   }

/* Close the config file
   *********************/
   fclose( fp );

/* Check init flags for missed commands
   ************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
      printf( "ERROR, no " );
      if ( !init[0] ) printf( "<MyIpAddress> " );
      if ( !init[1] ) printf( "<WellKnownPort> " );
      if ( !init[2] ) printf( "<RetryInterval> " );
      printf( "command(s) in <%s>\n", configfile );
      return FAILURE;
   }

/* Print the config file parameters
   ********************************/
   printf( "MyIpAddress:   %s\n", MyIpAddress );
   printf( "WellKnownPort: %d\n", WellKnownPort );
   printf( "RetryInverval: %d sec\n", RetryInterval );
   return SUCCESS;
}

