#
# This is getevent's parameter file

MyIpAddress      130.118.49.128              # IP address of event data source
WellKnownPort    17001                       # Well-known port number

RetryInterval 60
#  RetryInterval: If an error is detected by the socket system while getting
#  an event message, the program try again after RetryInterval seconds.
#  Also, if the program can't write an event file to disk, it will try again
#  after RetryInterval seconds.

