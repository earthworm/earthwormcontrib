
 /****************************************************************************
  *                                getevent.c                                *
  *                                                                          *
  *  Program to get event waveforms from an Earthworm system via a socket    *
  *  connection.  The events are written to disk files on the local system.  *
  *  The event file names are of the form event.<event id>, eg               *
  *  event.000001, event.000002, etc.                                        *
  *  WMK 8/3/98                                                              *
  ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define SUCCESS 0
#define FAILURE -1

/* Functions declarations
   **********************/
int  GetConfig( char * );
int  InitServerSocket( void ); 
int  GetEventFromSocket( void );

/* Things to read from the configuration file
   ******************************************/
extern int   RetryInterval;        /* Sleep this long between net retrys */


int main( int argc, char **argv )
{
   char   defaultConfig[] = "getevent.d";
   char   *configFileName = defaultConfig;
   int    status;
   extern int errno;

/* Get config file name from command line
   **************************************/
   if ( argc > 1 )
      configFileName = argv[1];

/* Read the configuration file
   ***************************/
   if ( GetConfig( configFileName ) == FAILURE )
   {
      printf( "Exiting.\n" );
      return FAILURE;
   }

/* Initialize the server socket
   ****************************/
   status = InitServerSocket(); 
   if ( status == FAILURE )
   {
      printf( "Can't initialize the server socket. Exiting.\n" );
      return FAILURE;
   }

/* Get event files from Earthworm, via a socket connection.
   GetEventFromSocket() blocks on connect.
   *******************************************************/
   while ( 1 )
      if ( GetEventFromSocket() == FAILURE )
      {
         printf( "Error getting event from socket.\n" );
         printf( "I will sleep %d seconds and try again...\n", RetryInterval ); 
         sleep( RetryInterval );
      }
}
 
