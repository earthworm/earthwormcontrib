#
#                    Make file for getevent
#
B = $(EW_HOME)/$(EW_VERSION)/bin
OBJS  = getevent.o gconfig.o sockets.o

all:
	make -f makefile.sol getevent
	make -f makefile.sol kill_it

getevent: $(OBJS)
	cc -o $B/getevent $(OBJS) -lm -lsocket -lnsl -lposix4 -lc

kill_it: kill_it.o
	cc -o $B/kill_it kill_it.o -lm -lsocket -lnsl -lposix4 -lc


# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/getevent* $B/kill_it*

