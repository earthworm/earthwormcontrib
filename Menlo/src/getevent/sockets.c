
      /******************************************************************
       *                           sockets.c                            *
       *                                                                *
       *               Functions containing socket calls.               *
       ******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define SUCCESS 0
#define FAILURE -1

/* Get BUFLEN characters at a time from the socket
   ***********************************************/
#define BUFLEN 1000
static char buf[BUFLEN];              /* Text buffer */

/* External variables
   ******************/
extern int  errno;                    /* Socket errors here */
extern char MyIpAddress[];            /* My own IP address */
extern int  WellKnownPort;            /* The well-known port number */

/* Global to this file
   *******************/
static int sd;                        /* Accept connections on this socket */


           /***********************************************
            *              InitServerSocket()             *
            ***********************************************/

int InitServerSocket( void )
{
   struct sockaddr_in server;         /* Server socket address structure */
   const int optVal = 1;

/* Get a new socket descriptor
   ***************************/
   sd = socket( AF_INET, SOCK_STREAM, 0 );
   if ( sd == -1 )
   {
      perror( "socket() error\n" );
      return FAILURE;
   }
 
/* Allow reuse of socket addresses
   *******************************/
   if ( setsockopt( sd, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
                    sizeof(int) ) == -1 )
   {
      perror( "setsockopt() error\n" );
      close( sd );
      return FAILURE;
   }

/* Fill in socket address structure
   ********************************/
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family      = AF_INET;
   server.sin_port        = htons( (unsigned short)WellKnownPort );
   server.sin_addr.s_addr = inet_addr( MyIpAddress );

/* Bind a local address to the socket descriptor
   *********************************************/
   if ( bind( sd, (struct sockaddr *)&server, sizeof(server) ) == -1 )
   {
      perror( "bind() error" );
      close( sd );
      return FAILURE;
   }

/* Set the maximum number of pending connections
   *********************************************/
   if ( listen( sd, 5 ) == -1 )
   {
      perror( "listen() error" );
      close( sd );
      return FAILURE;
   }
   return SUCCESS;
}


              /***********************************************
               *             GetEventFromSocket()            *
               ***********************************************/

int GetEventFromSocket( void )
{
   struct sockaddr_in client;              /* Client socket address structure */
   FILE     *fp;                           /* File pointer to temporary disk file */
   int      ns;                            /* Read from this socket */
   int      rc;
   int      clientlen = sizeof(client);    /* Client socket structure length */
   unsigned id;
   unsigned eventFileSize;
   unsigned nread;
   char     fname[80];                     /* Name of event file */
   time_t   now;
   const char ack = (char)6;               /* Ascii ACK character */

/* Accept a TCP connection
   ***********************/
   printf( "\nWaiting for socket connection...\n" );
   ns = accept( sd, (struct sockaddr *)&client, &clientlen );
   if ( ns == -1 )
   {
      perror( "accept() error" );
      close( sd );
      return FAILURE;
   }
   printf( "Connection accepted from %s\n", inet_ntoa(client.sin_addr) );

/* Read the file size from the socket
   **********************************/
   rc = recv( ns, (char *)&eventFileSize, sizeof(unsigned), NULL );
   if ( rc == -1 )
   {
      perror( "recv() error getting file size\n" );
      close( ns );
      return FAILURE;
   }
   nread = sizeof(unsigned);

/* If eventFileSize is zero, kill this program
   *******************************************/
   if ( eventFileSize == 0 )
   {
      printf( "Exiting.\n" );
      close( ns );
      exit( 0 );
   }
   printf( "Event file size: %u\n", eventFileSize );

/* Read the event id from the socket
   *********************************/
   rc = recv( ns, (char *)&id, sizeof(unsigned), NULL );
   if ( rc == -1 )
   {
      perror( "recv() error getting event id" );
      close( ns );
      return FAILURE;
   }
   printf( "Event id: %u\n", id );

/* Open a file to contain the event
   ********************************/
   fp = fopen( "temp", "wb" );
   if ( fp == NULL )
   {
      printf( "Can't open the new event file.\n" );
      close( ns );
      return FAILURE;
   }

/* Write the event id to the file
   ******************************/
   if ( fwrite( (char *)&id, sizeof(unsigned), 1, fp ) < 1 )
   {
      printf( "fwrite error to new event file.\n" );
      close( ns );
      fclose( fp );
      return FAILURE;
   }

/* Read the event from the socket, one buffer at a time,
   and write to the file
   ****************************************************/
   while ( nread < eventFileSize )
   {
      int rc;
      fpos_t pos;

      rc = recv( ns, buf, BUFLEN, NULL );
      if ( rc == -1 )
      {
         perror( "recv() error" );
         close( ns );
         fclose( fp );
         return FAILURE;
      }
      nread += rc;

      fgetpos( fp, &pos );
      while ( fwrite( buf, sizeof(char), rc, fp ) < rc )
      {
         printf( "fwrite error to new event file.\n" );
         printf( "I will wait 60 seconds and try again...\n" );
         sleep( 60 );
         fsetpos( fp, &pos );
      }
   }
 
/* Close the temporary event file
   ******************************/
   fclose( fp );

/* Sleep for a second.  Then, send the ACK (acknowledge) character
   back to earthworm.  This means: "I got the whole socket transmission"
   ********************************************************************/
   sleep( 1 );
   rc = send( ns, &ack, sizeof(char), NULL );
   if ( rc == -1 )
   {
      perror( "Error sending ack character" );
      close( ns );
      return FAILURE;
   }

/* Close the socket connection
   ***************************/
   close( ns );

/* Rename the temporary file to it's real name
   *******************************************/
   strcpy( fname, "event." );
   {
      int len = strlen( fname );
      sprintf( fname+len, "%06u", id );
   }
   rename( "temp", fname );

/* Print the result
   ****************/
   printf( "Event written to file %s on ", fname );
   time( &now );
   printf( "%s", ctime(&now) );

   return SUCCESS;
}

