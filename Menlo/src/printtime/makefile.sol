#
#                     Make file for printtime 
#                         Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = printtime.o

printtime: $O
	cc -o $B/printtime $O 

clean:
	/bin/rm -f printtime *.o

clean_bin:
	/bin/rm -f printtime 
