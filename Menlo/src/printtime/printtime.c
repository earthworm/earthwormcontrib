/* THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU CHECKED IT OUT!
 *
 *  $Id: printtime.c 378 2007-12-06 18:35:10Z dietz $
 * 
 *  Revision history:
 *   $Log$
 *   Revision 1.2  2007/12/06 18:35:10  dietz
 *   modified to write to stdout. If user wants a file, they can re-direct
 *   stdout to a  file.
 *
 *   Revision 1.1  2007/12/06 18:29:46  dietz
 *   New program to print out epoch time to a file
 *
 */

   /********************************************************************
    *                           printtime.c                            *
    *                                                                  *
    *  Command-line program to write system clock time to stdout in    *
    *  seconds-since-1970                                              *
    *                                                                  *
    *  Lynn Dietz  Dec 6, 2007                                         *
    ********************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <time.h>

int main( int argc, char *argv[] )
{
   fprintf( stdout, "%ld\n", (long)time(NULL) );
   return( 0 );
}
