/* THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU CHECKED IT OUT!
 *
 *  $Id: fwsoh2snw.c 609 2011-03-23 23:20:52Z dietz $
 * 
 *  Revision history:
 *   $Log$
 *   Revision 1.10  2009/02/10 21:48:07  dietz
 *   added new keyword for percent receive rate
 *
 *   Revision 1.9  2008/06/26 20:26:06  dietz
 *   Modified to recognize different keywords used by new Freewave firmware.
 *
 *   Revision 1.8  2007/10/05 23:34:39  dietz
 *   Modified not to output "% Rx Rate" for irole=FW_GATEWAY because the
 *   SOH parameter refers to "Rx Rate from Gateway" and will always be zero.
 *
 *   Revision 1.7  2007/09/27 17:17:41  dietz
 *   Sigh, one last tweak in parameter names
 *
 *   Revision 1.6  2007/09/27 17:11:05  dietz
 *   Added newline to end of SeisNetWatch file - it seems to be required!
 *
 *   Revision 1.5  2007/09/26 22:56:05  dietz
 *   Corrected formatting error in SNW output file
 *
 *   Revision 1.4  2007/09/26 22:15:42  dietz
 *   Added argument to set telemetry role of freewave
 *
 *   Revision 1.3  2007/09/25 23:26:50  dietz
 *   one more tweak in SNW text...
 *
 *   Revision 1.2  2007/09/25 22:55:30  dietz
 *   changed text strings in SNW-format output
 *
 *   Revision 1.1  2007/09/25 22:02:40  dietz
 *   Initial version
 *
 */

   /********************************************************************
    *                           fwsoh2snw.c                            *
    *                                                                  *
    *  Command-line program to read a Freewave SOH file which has been *
    *  converted from html to simple text and to produce a file in     *
    *  SeisNetWatch input format.                                      *
    *                                                                  *
    *  Lynn Dietz  Sept 25, 2007                                       *
    ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Define individual Freewave params to find in file
 ***************************************************/
#define N_FW_PARAM   5    /* number of Freewave Params in SOH file */

#define RSSI         0    
#define NOISE        1
#define VOLT         2
#define RXRATE       3
#define RPOWER       4

static char *FWparam[] = { "Signal",
                           "Noise",
                           "Voltage",
                           "Percent Receive Rate",
                           "Reflected Power" };

/* Define the possible telemetry-path roles of the Freewave
 **********************************************************/
#define FW_UNKNOWN  -1
#define FW_ENDPOINT  0
#define FW_REPEATER  1
#define FW_GATEWAY   2

static char *FWrole[]  = { "FW Endpoint",
                           "FW Repeater",
                           "FW Gateway"  };

int main( int argc, char *argv[] )
{
   char  *prog;               /* arvg[0] */
   char  *station;            /* argv[1] */
   char  *network;            /* argv[2] */
   char  *role;               /* argv[3] */
   char  *infile;             /* argv[4] */
   char  *outfile;            /* argv[5] */
   FILE  *ifp;                /* file to read from       */
   FILE  *ofp;                /* file to write to        */
   char   line[256];          /* single line from infile */
   int    irole = FW_UNKNOWN; /* role to assign this fw  */
   int    rssi;               /* signal level (dBm)      */
   int    noise;              /* noise level (dBm)       */
   float  volt;               /* voltage (volt)          */
   float  rxrate;             /* percent receive rate    */
   float  rpower;             /* reflected power (VSWR)  */
   int    found[N_FW_PARAM];  /* flag for each SOH param */
   int    nmiss;              /* number of missed params */
   int    nparam;             /* #params to write to SNW */
   int    i; 

/* Check arguments; assign them to coder-friendly variables
 **********************************************************/
   prog    = argv[0];

   if( argc != 6 )
   {
      fprintf( stderr, 
              "  Usage: %s <station> <network> <role E/R/G> <infile> <outfile>\n"
              "         where role value are: E=endpoint R=repeater G=gateway\n"
              "Example: %s CPM NC E fwsoh.txt fwsoh.snw\n",
               prog, prog );
      return( 1 );
   }
   station = argv[1];
   network = argv[2];
   role    = argv[3];
   infile  = argv[4];
   outfile = argv[5];

/* Validate arguments
 ********************/
   if( strlen(station) > 5 ) 
   {
     fprintf( stderr, 
             "%s: station <%s> too long (must be <= 5 chars); exiting!\n",
              prog, station );
     return( 1 );
   }
   if( strlen(network) > 2 ) 
   {
     fprintf( stderr, 
             "%s: network <%s> too long (must be <= 2 chars); exiting!\n",
              prog, network );
     return( 2 );
   }
   if( strcmp(role,"E")==0 || strcmp(role,"e")==0 ) irole = FW_ENDPOINT;
   if( strcmp(role,"R")==0 || strcmp(role,"r")==0 ) irole = FW_REPEATER;
   if( strcmp(role,"G")==0 || strcmp(role,"g")==0 ) irole = FW_GATEWAY;
   if( irole == FW_UNKNOWN )
   {
     fprintf( stderr, 
             "%s: invalid role <%s> (valid chars: E R G); exiting!\n",
              prog, role );
     return( 3 );
   }

/* Open the input/output files
 *****************************/
   ifp = fopen( infile, "r" );
   if( ifp == NULL )
   {
     fprintf( stderr, "%s: Cannot open input file <%s>\n", prog, infile );
     return( 4 );
   }

   ofp = fopen( outfile, "w" );
   if( ofp == NULL )
   {
     fprintf( stderr, "%s: Cannot open output <%s>\n",  prog, outfile );
     fclose( ifp );
     return( 5 );
   }

/* Read thru file for interesting SOH params.
/* Different FW firmware versions sometimes use different keywords.
 ******************************************************************/
   for( i=0; i<N_FW_PARAM; i++ ) found[i] = 0;

   while( fgets( line, 256, ifp ) != NULL )
   {
      if     ( sscanf( line, " RSSI %d dB",       &rssi  ) == 1 ) found[RSSI] = 1;
      else if( sscanf( line, " Signal %d dB",     &rssi  ) == 1 ) found[RSSI] = 1;
      else if( sscanf( line, " Row('Signal','%d", &rssi  ) == 1 ) found[RSSI] = 1;

      else if( sscanf( line, " Noise %d dB",      &noise ) == 1 ) found[NOISE] = 1;
      else if( sscanf( line, " Row('Noise','%d",  &noise ) == 1 ) found[NOISE] = 1;

      else if( sscanf( line, " Voltage %fv",        &volt ) == 1 ) found[VOLT] = 1;
      else if( sscanf( line, " Row('Voltage','%fv", &volt ) == 1 ) found[VOLT] = 1;

      else if( sscanf( line, " %*s Receive Rate %f%%",       &rxrate ) == 1 ) found[RXRATE] = 1;
      else if( sscanf( line, " Receive Rate %f%%",           &rxrate ) == 1 ) found[RXRATE] = 1;
      else if( sscanf( line, " RX Success Rate %f%%",        &rxrate ) == 1 ) found[RXRATE] = 1;
      else if( sscanf( line, " Row('RX Success Rate','%f%%", &rxrate ) == 1 ) found[RXRATE] = 1;

      else if( sscanf( line, " Reflected Power (VSWR) %f", &rpower ) == 1 ) found[RPOWER] = 1;
      else if( sscanf( line, " Reflected Power %f",        &rpower ) == 1 ) found[RPOWER] = 1;
      else if( sscanf( line, " Row('Reflected Power','%f", &rpower ) == 1 ) found[RPOWER] = 1;
   }

/* Verify that all expected parameters were read from file
 *********************************************************/
   nmiss = 0;
   for( i=0; i<N_FW_PARAM; i++ ) {
      if( !found[i] ) {
         fprintf( stderr, 
                 "%s: error reading <%s> from '%s'; exiting\n",
                  prog, FWparam[i], infile );
         nmiss++;
      }
   }
   if( nmiss ) 
   {
      fclose( ifp );
      fclose( ofp );
      return( -1*nmiss );  /* exit, returning # of missed params */
   }

/* Write SeisNetWatch-formatted file (all on one line!)
 ******************************************************/
   if( irole == FW_GATEWAY ) nparam = N_FW_PARAM;
   else                      nparam = N_FW_PARAM+1;

   fprintf( ofp, "%s-%s:%d:", network, station, nparam );
   fprintf( ofp, "%s Signal Strength RSSI (dBm)=%d;", FWrole[irole], rssi       );
   fprintf( ofp, "%s Signal/Noise (dBm)=%d;",         FWrole[irole], rssi-noise );
   fprintf( ofp, "%s Voltage (V)=%.2f;",              FWrole[irole], volt       );
   if( irole != FW_GATEWAY ) {  /* skip for gateway because it's always = 0 */
     fprintf( ofp, "%s Rx Rate (%%)=%.2f;",           FWrole[irole], rxrate     );
   }
   fprintf( ofp, "%s Reflected Power (VSWR)=%.2f;",   FWrole[irole], rpower     );   
   fprintf( ofp, "UsageLevel=3\n" );  /* tack this on for good measure! */

/* All done, close files!
 ************************/
   fclose( ifp );
   fclose( ofp );
   return( 0 );
}

