#
#                     Make file for fwsoh2snw
#                         Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = fwsoh2snw.o

fwsoh2snw: $O
	cc -o $B/fwsoh2snw $O -lm -lsocket -lnsl -lposix4

clean:
	/bin/rm -f fwsoh2snw *.o

clean_bin:
	/bin/rm -f fwsoh2snw
