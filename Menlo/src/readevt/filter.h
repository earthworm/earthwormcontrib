/******************************************************************************/
/* copyevt.h:                                                                 */
/*                                                                            */
/* JHL May 2002                                                               */
/*                                                                            */
/******************************************************************************/

/* Define Strong Motion file types
 *********************************/
#define MAXTRACELTH 18000
#define NAM_LEN 100	          /* length of full directory name          */
#define BUFLEN  65000         /* define maximum size for an event msg   */

/* Function prototypes
 *********************/
void config_me ( char * );
void ew_lookup ( void );
void ew_status ( unsigned char, short, char * );

int filter( char *fname );
int read_tag( FILE *fp );
int read_head( FILE *fp );
int read_frame( FILE *fp, unsigned long *channels );
