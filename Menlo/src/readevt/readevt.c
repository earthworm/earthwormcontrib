
   /***************************************************
    *                    readevt.c                    *
    * Reads header values from an evt file.           *
    ***************************************************/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "filter.h"
#include "nkwhdrs_jhl.h"


int main( int argc, char *argv[] )
{
   char      *fname;
   char      *header_param;
   struct tm *structTime;
   time_t    tt;
   extern    MW_HEADER head;     // defined in filter.c
   char      timestr[80];
   const int Debug = 0;

/* Kinemetrics time starts 10 years after EW time
   **********************************************/
   const unsigned long Decade = (365*10+2)*24*60*60;

   if ( argc != 3 )
   {
      printf( "Usage:\n" );
      printf( "   readevt -functest <fname>\n" );
      printf( "or\n" );
      printf( "   readevt -trigtime <fname>\n" );
      return -1;
   }

/* Sanity check
   ************/
   header_param = argv[1];
   if ( strcmp(header_param,"-functest") &&
        strcmp(header_param,"-trigtime") )
   {
      printf( "Invalid argument: %s\n", header_param );
      return -1;
   }
   fname = argv[2];
   if ( Debug ) printf( "File name: %s\n", fname );

/* Read the Altus file headers and fill in the tag,
   head, khead, and frame structures.  If filter returns 0,
   file wasn't found or it's not an evt file.
   *******************************************************/
   if ( filter(fname) == 0 ) return 0;

   if ( Debug )
   {
      printf( "startTime:       %u\n", head.roParms.stream.startTime );
      printf( "triggerTime:     %u\n", head.roParms.stream.triggerTime );
      printf( "duration:        %u\n", head.roParms.stream.duration );
      printf( "flags:           %x\n", head.roParms.stream.flags );
      printf( "startTimeMsec:   %u\n", head.roParms.stream.startTimeMsec );
      printf( "triggerTimeMsec: %u\n", head.roParms.stream.triggerTimeMsec );
      printf( "nscans:          %u\n", head.roParms.stream.nscans );
   }

/* If bit 0 of flags is set, this is a function test
   *************************************************/
   if ( strcmp(header_param,"-functest") == 0 )
   {
      if ( head.roParms.stream.flags & 1 )
         printf( "IS_FT\n" );
      else
         printf( "NOT_FT\n" );
      return 0;
   }

/* Convert trigger time to a string
   ********************************/
   if ( strcmp(header_param,"-trigtime") == 0 )
   {
      tt = (time_t)head.roParms.stream.triggerTime + Decade;
      structTime = gmtime( &tt );
      sprintf( timestr, "%4d%02d%02d_%02d%02d%02d",
         structTime->tm_year + 1900,
         structTime->tm_mon + 1,
         structTime->tm_mday,
         structTime->tm_hour,
         structTime->tm_min,
         structTime->tm_sec );
      puts( timestr );
      return 0;
   }
   return 0;
}
