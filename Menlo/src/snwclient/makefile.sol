#
#                     Make file for snwclient
#                         Solaris Version
#
#  The posix4 library is required for nanosleep.
#
O = snwclient.o socket_sol.o config.o chdir_sol.o getfname_sol.o \
    sleep_sol.o log.o tzset_sol.o fopen_sol.o

all:
	make -f makefile.sol snwclient
	make -f makefile.sol install

snwclient: $O
	cc -o snwclient $O -lm -lsocket -lnsl -lposix4

install: 
	cp snwclient $(EW_HOME)/$(EW_VERSION)/bin
