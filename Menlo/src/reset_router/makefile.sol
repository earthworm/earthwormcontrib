#
#                Make file for reset_router
#                      Solaris Version
#
#  The posix4 library is required for nanosleep.
#
O = reset_router.o rs_router.o rs_sol.o
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

reset_router: $O
	cc -o $B/reset_router $O $L/sleep_ew.o -lm -lsocket -lnsl -lposix4
