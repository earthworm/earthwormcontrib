
       /****************************************************
        *                    rs_router.c                   *
        *                                                  *
        *   This file contains the ResetRouter function    *
        ****************************************************/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>   /* for sleep_ew() */

int  ConnectToRouter( char [] );
int  SendToRouter( char *, int );
int  GetFromRouter( char *, int, int * );
void CloseSocketConnection( void );
int  GetPrompt( char [], int );

extern int Quiet;


int ResetRouter( char SerialIP[], char Password1[], char Password2[] )
{
   char  command[80];
   const timeout = 10;     /* seconds */

/* Connect to the router
   *********************/
   if ( ConnectToRouter( SerialIP ) == -1 )
   {
      if ( !Quiet ) printf( "Can't connect to router.\n" );
      return -1;
   }

/* Send first password
   *******************/
   if ( GetPrompt( "Password: ", timeout ) < 0 )
   {
      if ( !Quiet ) printf( "Error getting first password prompt from router.\n" );
      CloseSocketConnection();
      return -1;
   }

   strcpy( command, Password1 );
   strcat( command, "\n" );
   if ( SendToRouter( command, strlen(command) ) == -1 )
   {
      if ( !Quiet ) printf( "Error sending user name to router.\n" );
      CloseSocketConnection();
      return -1;
   }

/* Send enable command to router
   *****************************/
   if ( GetPrompt( ">", timeout ) < 0 )
   {
      if ( !Quiet ) printf( "Error getting > prompt from router.\n" );
      CloseSocketConnection();
      return -1;
   }

   if ( SendToRouter( "enable\n", 7 ) == -1 )
   {
      if ( !Quiet ) printf( "Error sending enable command to router.\n" );
      CloseSocketConnection();
      return -1;
   }

/* Send second password
   ********************/
   if ( GetPrompt( "Password: ", timeout ) < 0 )
   {
      if ( !Quiet ) printf( "Error getting second password prompt from router.\n" );
      CloseSocketConnection();
      return -1;
   }

   strcpy( command, Password2 );
   strcat( command, "\n" );
   if ( SendToRouter( command, strlen(command) ) == -1 )
   {
      if ( !Quiet ) printf( "Error sending second password to router.\n" );
      CloseSocketConnection();
      return -1;
   }

/* Send <clear ip> command and confirmation
   ****************************************/
   if ( GetPrompt( "#", timeout ) < 0 )
   {
      if ( !Quiet ) printf( "Error getting first # prompt from router.\n" );
      CloseSocketConnection();
      return -1;
   }
   if ( SendToRouter( "clear ip ospf process\n", 22 ) == -1 )
   {
      if ( !Quiet ) printf( "Error sending <clear ip> command to router.\n" );
      CloseSocketConnection();
      return -1;
   }

   if ( GetPrompt( "Reset ALL OSPF processes? [no]: ", timeout ) < 0 )
   {
      if ( !Quiet ) printf( "Error getting confirmation prompt from router.\n" );
      CloseSocketConnection();
      return -1;
   }
   if ( SendToRouter( "yes\n", 4 ) == -1 )
   {
      if ( !Quiet ) printf( "Error sending confirmation (yes) to router.\n" );
      CloseSocketConnection();
      return -1;
   }

/* Log out of router
   *****************/
   if ( GetPrompt( "#", timeout ) < 0 )
   {
      if ( !Quiet ) printf( "Error getting second # prompt from router.\n" );
      CloseSocketConnection();
      return -1;
   }
   if ( SendToRouter( "logout\n", 7 ) == -1 )
   {
      if ( !Quiet ) printf( "Error sending logout command to router.\n" );
      CloseSocketConnection();
      return -1;
   }
   CloseSocketConnection();   /* Log out successful */
   return 0;
}


        /*******************************************************
         *                    GetPrompt()                      *
         *                                                     *
         *  Wait for timeout seconds for the router to send    *
         *  the desired prompt.                                *
         *                                                     *
         *  timeout = maximum time to wait, in seconds         *
         *  Returns  0 if we received the prompt               *
         *          -1 if there was a socket read error        *
         *          -2 if prompt wasn't received               *
         *          -3 if buffer is full                       *
         *******************************************************/

int GetPrompt( char prompt[], int timeout )
{
   static char buf[256];
   int         nReceived;
   int         totalReceived = 0;
   time_t      start         = time(0);
   int         spaceAvail    = sizeof(buf);
   int         lenPrompt     = strlen(prompt);

   while ( (time(0) - start) < timeout )
   {
      int j;

      sleep_ew( 50 );                  /* Wait for bytes to show up */

      if ( spaceAvail < 1 )            /* Input buffer is full */
         return -3;

      if ( GetFromRouter( &buf[totalReceived],
                          spaceAvail,
                          &nReceived ) == -1 )
         return -1;                    /* Socket read error */

      for ( j = 0; j < nReceived; j++ )     /* Replace null bytes with carets */
      {
         if ( buf[totalReceived +j] == 0 )
            buf[totalReceived + j] = '^';
/*       putchar( buf[totalReceived + j] ); */    /* Debug code */
      }

      totalReceived += nReceived;
      spaceAvail    -= nReceived;
      buf[totalReceived] = '\0';

      if ( strstr( buf, prompt ) != NULL )
         return 0;                     /* Success! We found the prompt. */
   }
   return -2;                          /* Timed out */
}
