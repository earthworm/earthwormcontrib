
       /**************************************************************
        *                        reset_router                        *
        *                                                            *
        *         Program to reset a Cisco router by issuing         *
        *         the command:     clear ip ospf process             *
        *                                                            *
        *  Usage:                                                    *
        *    reset_router [IP address] [password1] [password2] [-q]  *
        **************************************************************/

#include <stdio.h>
#include <string.h>

int  ResetRouter( char [], char [], char [] );
void SocketSysInit( void );

int Quiet = 0;                   /* If 0, don't print anything to stdout */


int main( int argc, char *argv[] )
{
   int  i;
   char SerialIP[20];            /* IP address of the router serial port */
   char Password1[20];
   char Password2[20];

/* Get command line arguments
   **************************/
   if ( argc < 4 )
   {
      printf( "\nUsage: reset_router <IP address> <password1> <password2> [-q]\n\n" );
      printf( "If -q (quiet) is specified, nothing is written to stdout.\n" );
      return -1;
   }

   strcpy( SerialIP,  argv[1] );
   strcpy( Password1, argv[2] );
   strcpy( Password2, argv[3] );

   for ( i = 4; i < argc; i++ )
      if ( !strcmp(argv[i],"-q") ) Quiet = 1;

/* Initialize the socket system
   ****************************/
   SocketSysInit();

/* Invoke the reset function
   *************************/
   if ( ResetRouter( SerialIP, Password1, Password2 ) < 0 )
   {
      if ( !Quiet )
         printf( "Error resetting the router.\n" );
      return -1;
   }

/* Success!
   ********/
   if ( !Quiet )
      printf( "The router was successfully reset.\n" );

   return 0;
}
