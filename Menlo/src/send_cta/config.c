
        /***************************************************************
         *                         config.c                            *
         *                                                             *
         *  Functions to read and log the configuration files.         *
         ***************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include "send_cta.h"

/* Functions in this source file
   *****************************/
void GetConfig( char * );
void LogConfig( void );

/* Share these
   ***********/
volatile char EventFileName[80];      /* File produced by cta2cusp */
volatile int  EventId;                /* Earthworm event id */
volatile char RemoteIpAddress[80];    /* IP address of system to receive trigger */
volatile int  WellKnownPort;          /* The well-known port number */


/****************************************************************************
 *  GetConfig() processes command file(s) using kom.c functions;            *
 *                    exits if any errors are encountered.                  *
 ****************************************************************************/

#define NCOMMAND 4

void GetConfig( char *configfile )
{
   const    ncommand = NCOMMAND;    /* Process this many required commands */
   char     init[NCOMMAND];         /* Init flags, one for each command */
   int      nmiss;                  /* Number of missing commands */
   char     *com;
   char     *str;
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
        printf( "Error opening command file <%s>. Exiting.\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         com = k_str();         /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )           continue;
         if ( com[0] == '#' )  continue;

/* Open a nested configuration file
   ********************************/
         if( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "Error opening command file <%s>. Exiting.\n",
                        &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         if ( k_its("EventFileName") )
         {
            str = k_str();
            if (str) strcpy( (char *)EventFileName, str );
            init[0] = 1;
         }
         else if ( k_its("EventId") )
         {
            EventId = k_int();
            init[1] = 1;
         }
         else if ( k_its("RemoteIpAddress") )
         {
            str = k_str();
            if (str) strcpy( (char *)RemoteIpAddress, str );
            init[2] = 1;
         }
         else if ( k_its("WellKnownPort") )
         {
            WellKnownPort = k_int();
            init[3] = 1;
         }

/* Unknown command
   ***************/
         else
         {
            printf( "<%s> Unknown command in <%s>.\n",
                     com, configfile );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "Bad <%s> command in <%s>. Exiting.\n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
       printf( "ERROR, no " );
       if ( !init[0]  ) printf( "<EventFileName> " );
       if ( !init[1]  ) printf( "<EventId> " );
       if ( !init[2]  ) printf( "<RemoteIpAddress> " );
       if ( !init[3]  ) printf( "<WellKnownPort> " );
       printf( "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }
   return;
}


/***********************************************************
 *  LogConfig()   Print the configuration file parameters. *
 ***********************************************************/

void LogConfig( void )
{
   printf( "EventFileName:     %s\n", EventFileName );
   printf( "EventId:           %d\n", EventId );
   printf( "RemoteIpAddress:   %s\n", RemoteIpAddress );
   printf( "WellKnownPort:     %d\n", WellKnownPort );
   return;
}
