
  /*******************************************************************************
   *                                  send_cta.c                                 *
   *                                                                             *
   *                  This program feeds event waveforms to Cusp.                *
   *******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <earthworm.h>
#include <time_ew.h>
#include "send_cta.h"

/* Function declarations
   *********************/
void GetConfig( char * );
void LogConfig( void );
int  GetCuspConnection( void );
int  SendEventFileToCusp( FILE *, unsigned *, int * );

/* Read from the config file
   *************************/
extern char EventFileName[80];         /* File produced by cta2cusp */
extern int  EventId;                   /* Earthworm event id */


int main( int argc, char **argv )
{
   char     defaultConfig[] = "send_cta.d";
   char     *configFileName;
   unsigned byteCount;                 /* Send this many bytes to Cusp */
   FILE     *fp;
   DIR      *dirp;
   struct stat statbuf;

/* Read the configuration file
   ***************************/
   configFileName = (argc < 2) ? &defaultConfig[0] : argv[1];
   GetConfig( configFileName );

/* Log the configuration file parameters
   *************************************/
   LogConfig();

/* Open the event file to send to Cusp
   ***********************************/
   fp = fopen( EventFileName, "rb" );
   if ( fp == NULL )
   {
      printf( "Can't open event file: %s  Exiting.\n", EventFileName );
      return -1;
   }

/* Get the file size in bytes
   **************************/
   stat( EventFileName, &statbuf );
   byteCount = statbuf.st_size;
   printf( "Event file size:   %u bytes\n", byteCount );

   if ( byteCount <= 0 )
   {
      printf( "No data to send. Exiting.\n" );
      return -1;
   }

/* Get socket connection to Cusp
   *****************************/
   if ( GetCuspConnection() == SENDCTA_FAILURE )
   {
      printf( "Can't connect to Cusp. Exiting.\n" );
      return -1;
   }

/* Send the event file
   *******************/
   if ( SendEventFileToCusp( fp, &byteCount, &EventId ) == SENDCTA_FAILURE )
   {
      printf( "Can't send the event file. Exiting.\n" );
      return -1;
   }

/* Success
   *******/
   printf( "Event file successfully sent to Cusp.\n" );
   return 0;
}
