#
#     This is send_cta's parameter file (send_cta.d)
#
EventFileName      event.tmp        # This file produced by cta2cusp
EventId            12345            # Earthworm event id
RemoteIpAddress    130.118.49.128   # IP address of Cusp system to receive event
                                    #   files.
WellKnownPort      17001            # Well-known port number for socket connection
                                    #   to the Cusp system.
