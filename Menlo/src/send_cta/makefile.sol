#
#                     Make file for send_cta
#                         Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = send_cta.o config.o sendwave.o $L/kom.o $L/swap.o

send_cta: $O
	cc -o $B/send_cta $O -lm -lsocket -lnsl -lposix4 -lc

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/send_cta*
