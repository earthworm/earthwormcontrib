#
# This is the helinow parameter file. This module gets data gulps
# from the waveserver(s), and creates helicorder displays.
 

#  Basic Earthworm setup:
#
LogSwitch       1                  # 0 to completely turn off disk log file
MyModuleId      MOD_HELIN          # module id for this instance of report 
RingName        HYPO_RING          # ring to get input from
HeartBeatInt    15                 # seconds between heartbeats

wsTimeout 60 # time limit (secs) for any one interaction with a wave server.

# List of wave servers (ip port comment) to contact to retrieve trace data.
# @all_waveservers.d
# WaveServer 130.118.43.11  16022      "wsv  - image1"
# WaveServer 130.118.43.12  16022      "wsv  - image2"
  WaveServer     130.118.43.34   16030     "wsv3 k2np"
  WaveServer     130.118.43.4    16030     "wsv2 k2np"


# Directory in which to store the temporary .gif, .link and .html files.
GifDir   /home/picker/gifs/helinow/  

# Directory in which to store the .gif files for output
 LocalTarget   /home/picker/sendfiler/helinow/

# The file with all the station information.
#StationList     /home/earthworm/run/params/calsta.db
 StationList1    /home/earthworm/run/params/calsta.db1

# Plot Display Parameters - 
		# The following is designed such that each SCN creates it's own
		# helicorder display; one per panel of data.
		
# 01 HoursPerPlot    Total number of hours per gif image
# 02 Seconds/Line    Number of seconds per line of trace
# 03 Plot Previous   On startup, retrieve and plot at least n previous hours from tank.
# 04 XSize           Overall size of plot in inches
# 05 YSize           Setting these > 100 will imply pixels
# 06 Show UTC        UTC will be shown on one of the time axes.
# 07 Use Local       The day page will be local day rather than UTC day.
#                                      
#        01  02 03 04  05 06 07 

# Display  1  3600  1 180  700  1  1  
 Display  1  60  1 700  220  0  1  

# We accept a command "LabelDC" to label each trace line 
# with its dc offset.
#LabelDC

# We accept a command "NoLabelDC" to not label each trace line 
# with its dc offset [default].
 NoLabelDC

# Channel Parameters - sorry it's so complex, but that's the price of versatility
		# The following is designed such that each SCN creates it's own
		# helicorder display; one per panel of data.
# S                  Site
# C                  Component
# N                  Network
# 04 Local Time Diff UTC - Local.  e.g. -7 -> PDT; -8 -> PST
# 05 Local Time Zone Three character time zone name.
# 06 Plot Previous   On startup, retrieve and plot n previous hours from tank.
# 07 Scale Factor    Scale factor to dress up image.
# 08 Mean Removal    Mean of 1st minute of each line will be removed.
# 09 Filter          Apply a lo-cut filter.
#                                      
#     S    C   N  04  05   06   07  08   09

#SCN  KMPB HHZ NC -8  PST   1  0.1   1    0   "Mount Pierce"
#SCN  KMPB HHN NC -8  PST   1  0.1   1    0   "Mount Pierce"
#SCN  KMPB HHE NC -8  PST   1  0.1   1    0   "Mount Pierce"
 SCN  1745 HNC NP -8  PST   1  0.1   1    0   "Roof"
 SCN  1745 HN1 NP -8  PST   1  0.1   1    0   "Basement"
 SCN  1745 HN4 NP -8  PST   1  0.1   1    0   "Free Field"

    # *** Optional Commands ***

# Filename prefix on target computer.  This is useful for identifying
# files for automated deletion via crontab.
Prefix nc

 UpdateInt    60  # Number of seconds between updates; default=120
 RetryCount    1  # Number of attempts to get a trace from server; default=2
 Logo    smusgs.gif   # Name of logo in GifDir to be plotted on each image

# We accept a command "SaveDrifts" which logs drifts to GIF directory.
# SaveDrifts

# We accept a command "BuildOnRestart" to totally rebuild images on restart.

  BuildOnRestart   
 
# The argument, DaysAgo, if >0 will plot old data and exit.
# DaysAgo 2    # Start plotting the requested number of days ago.


# Normally, data is decimated before plotting to be roughly compatible with a
# 72 dpi pixel density.  The decimation factor is computed online based 
# upon the sample rate and plot scaling. This may be factor of 50-200. 
# If a true imitation of a helicorder plot is required, in which the
# "pen" overwrites itself, you can force the decimation to a smaller value.

# Decimate 1

# If this flag is set, local time is automatically changed to/from Daylight
# Savings Time on the appropriate days for North America.
# If you use this feature, make sure that you have specified standard 
# time in SCN above.

  UseDST

# This option allows heli1 to be run as a standalone module.
# Shared memory is not accessed and heartbeats are not sent or received.

# StandAlone      # Run heli outside of earthworm.

# We accept a command "Clip" to clip the traces at N pixels.
# Clip  200     

  Gulp  60       # Specify number of seconds of data to acquire at a time.


# We accept a command "PlotUp" to plot from bottom to top.
# PlotUp     

# We accept a command "PlotDown" to plot from top to bottom.
  PlotDown     

# We accept a command "Debug" which turns on a bunch of log messages
# Debug
# WSDebug
