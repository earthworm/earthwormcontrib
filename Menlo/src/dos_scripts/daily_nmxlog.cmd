@echo off	
rem _______________________________________________________
rem |
rem |  daily_nmxlog.cmd
rem |
rem |  This script runs temporary CMD scripts that in turn
rem |  run NMX summary programs for one day for all NMX
rem |  channels. It writes the log files in the c:\nmx\log
rem |  directory. Finally, it builds the temporary scripts
rem |  with today's date, which will be run the next time
rem |  this scripts is scheduled (tomorrow).
rem |  
rem |  Schedule this script to run daily at a time when
rem |  the onboard memory in all digitizers will contain
rem |  only data from today (UTC).  That means yesterday's
rem |  data in the ringbuffers will be as complete as it
rem |  could possibly be (no more ReTx possible).
rem |
rem |  NCSN HRDs have at most 13 Mb of memory (quite a bit!)
rem |  so it might have to be run at mid-day.
rem |
rem |  at 12:00 /every:M,T,W,Th,F,S,Su cmd /c daily_nmxlog
rem | 
rem |  NOTE: % is the escape character for CMD scripts!
rem |______________________________________________________

cd c:\nmx\log

call tmprbfsum.cmd
call tmpextractp.cmd

del tmprbfsum.cmd
del tmpextractp.cmd

c:\local\bin\date.exe "+onedayrbfsum.cmd all 20%%y %%m %%d"    > tmprbfsum.cmd
c:\local\bin\date.exe "+onedayextractp.cmd all 20%%y %%m %%d"  > tmpextractp.cmd
