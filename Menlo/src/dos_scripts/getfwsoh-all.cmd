rem # Grab SOH from all Freewaves and write to snw_client input dir.
rem # Schedule this script to run once an hour, around the clock.
rem #
rem # SYNTAX: getfwsoh4snw host-or-IP station network role:E/R/G outdir

 call getfwsoh4snw  CPM    CPM NC E c:\earthworm\run\ew2file\snw
 call getfwsoh4snw  RXCPM  CPM NC G c:\earthworm\run\ew2file\snw

 call getfwsoh4snw  NCC    NCC NC E c:\earthworm\run\ew2file\snw
 call getfwsoh4snw  RXNCC  NCC NC G c:\earthworm\run\ew2file\snw
 
rem # THE END
