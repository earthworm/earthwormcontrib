@echo off
rem  RunRM4app.bat                             2001/09/21:LDD
doskey

echo *----------------------------------------------------------*
echo * To communicate with an RM4:                              *
echo *                                                          *
echo *  1. Check PC's Network Properties.                       *
echo *     a. IPaddress:  PC must be on same subnet as RM4.     * 
echo *        First 3 numbers of their addresses should be      *
echo *        identical, the 4th number must be different.      *
echo *     b. Subnet mask:  255.255.255.0                       *
echo *     If you made changes in a or b, you must reboot PC.   * 
echo *                                                          *
echo *  2. Connect PC's ethernet port to either:                *
echo *     a. LAN HUB port using a straight-thru cable, or      *
echo *     b. RM4's ethernet port using a cross-over cable.     *
echo *                                                          *
echo *  3. Type RM4 application command line:                   *
echo *       Usage:    RM4application [IP.addr.of.RM4]          *
echo *       example:  RM4application 192.168.4.56              *
echo *----------------------------------------------------------*

rem  On Windows98, set up this shortcut on the desktop:
rem    Cmdline:      c:\windows\command.com 
rem    Working:      c:\nmx\user
rem    Batch file:   RunRM4app.bat
rem    Shortcut key: None
rem    Run in:       Normal window
rem    x Close on exit

rem On WindowsNT, set up a shortcut to this batch file:
rem    Target:       RunRM4app.bat
rem    Start in:     c:\nmx\user
rem    Shortcut key: None
rem    Run in:       Normal window
rem and uncomment the following line:
cmd.exe
