@echo off

rem #################################################################
rem # pathcheck.cmd                                  2007/12/06:LDD
rem #
rem # Usage: pathcheck pathdesc desthost #hops chksec EWring EWmodid
rem #
rem # This script is a continuous loop which repeats every <chksec>
rem # seconds. It emulates an Earthworm module by sending heartbeats
rem # and error messages via the program file2ring.
rem #
rem # Requires these non-DOS commands in %PATH%
rem #  grep.exe
rem #  wc.exe
rem #  printtime.exe
rem #  file2ring.exe
rem #  c:\local\bin\date.exe
rem #
rem #################################################################

setlocal

rem # Make sure there are no missing arguments
rem #-----------------------------------------
    if "%1" EQU ""  goto Usage
    if "%2" EQU ""  goto Usage
    if "%3" EQU ""  goto Usage
    if "%4" EQU ""  goto Usage
    if "%5" EQU ""  goto Usage
    if "%6" EQU ""  goto Usage

rem # Initialize local variables
rem #---------------------------
    set PATHDESC=%1
    set DESTHOST=%2
    set GOODHOPS=%3
    set CHECKINTERVAL=%4
    set RING=%5
    set MODID=%6
    set PATHSTATUS=OK
    set NHOP=0
    set ERRNO=0
    set ERRORSEQ=0
    set HEARTSEQ=0
    set TMPFILE=%PATHDESC%.tmp

    goto CheckPath

:Usage

@echo Usage: pathcheck pathdesc desthost #hops chksec EWring EWmodid
@echo        where  %%1 = pathdesc = character string to describe path being checked
@echo               %%2 = desthost = hostname or IPaddr to contact for the test
@echo               %%3 = #hops    = number of hops expected if everything's OK
@echo               %%4 = chksec   = interval (seconds) between path checks
@echo               %%5 = EWring   = Earthworm Ring to write EW messages to (heartbeats,error)
@echo               %%6 = EWmodid  = Earthworm ModuleId to use for writing message

@echo Example: pathcheck Vollmer2Tam-uWave ehz-tam-lbo 2 600 SCNL_RING MOD_PATHCHECK
goto Done

:CheckPath

rem # Store current time for writing to EW messages
rem #----------------------------------------------
    printtime >%TMPFILE%
    for /F "tokens=1 delims=" %%n in (%TMPFILE%) do set TIMENOW=%%n
    del %TMPFILE%

rem # Find out number of hops to destination
rem #---------------------------------------
    tracert %DESTHOST% | grep " ms" | wc -l > %TMPFILE%
    for /F "tokens=1 delims=" %%n in (%TMPFILE%) do set NHOP=%%n
    del %TMPFILE%

rem # Check status of path; send Error if status changed
rem #---------------------------------------------------
    if %NHOP% GTR %GOODHOPS%     goto BadPath

:GoodPath

    if "%PATHSTATUS%" EQU "OK"   goto SleepNow
    @echo %TIMENOW% %ERRNO% %PATHDESC% is OK again > %TMPFILE%
    file2ring %TMPFILE% %RING% %EW_INSTALLATION% %MODID% TYPE_ERROR %ERRORSEQ%
    del %TMPFILE%
    set /a ERRORSEQ=%ERRORSEQ%+1
    set PATHSTATUS=OK
    goto SleepNow

:BadPath 

    if "%PATHSTATUS%" EQU "BAD"  goto SleepNow
    @echo %TIMENOW% %ERRNO% %PATHDESC% may be down (%NHOP% hops to %DESTHOST%; expected %GOODHOPS%) > %TMPFILE%
    file2ring %TMPFILE% %RING% %EW_INSTALLATION% %MODID% TYPE_ERROR %ERRORSEQ%
    rm %TMPFILE%
    set /a ERRORSEQ=%ERRORSEQ%+1
    set PATHSTATUS=BAD

:SleepNow

rem # Write info to stdout, send Earthworm heartbeat, then go to sleep
rem #-----------------------------------------------------------------
    c:\local\bin\date
    @echo    %PATHDESC%:%PATHSTATUS% %NHOP% hops to %DESTHOST% (expected %GOODHOPS%)

    @echo %TIMENOW% > %TMPFILE%
    file2ring %TMPFILE% %RING% %EW_INSTALLATION% %MODID% TYPE_HEARTBEAT %HEARTSEQ%
    rm %TMPFILE%

    set /a HEARTSEQ=%HEARTSEQ%+1
    sleep %CHECKINTERVAL%
    goto CheckPath

:Done

    endlocal
