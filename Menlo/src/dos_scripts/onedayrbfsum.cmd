@echo off	
rem _______________________________________________________
rem |
rem |  onedayrbfsum.cmd
rem |
rem |    args: %1 = station name (or first few letters)
rem |               use 'all' or 'ALL' to do all channels
rem |          %2 = 4-digit year
rem |          %3 = 2-digit month
rem |          %4 = 2-digit day-of-month
rem | 
rem |  This file runs an hourly rbfsum on given Nanometrics  
rem |  channels for the given day.  It writes the log file
rem |  in the current working directory.
rem |
rem |  NOTE: % is the escape character for CMD scripts!
rem |______________________________________________________

if "%4"=="" goto Usage
if "%3"=="" goto Usage
if "%2"=="" goto Usage
if "%1"=="" goto Usage

if %1==all goto AllChannels
if %1==ALL goto AllChannels

c:\nmx\bin\rbfsum d:\r*%1* -s %2-%3-%4_00:00:00 -h 24 >  %1.rbfsum_%2%3%4.log
c:\nmx\bin\rbfsum e:\r*%1* -s %2-%3-%4_00:00:00 -h 24 >> %1.rbfsum_%2%3%4.log
goto Done

:AllChannels
c:\nmx\bin\rbfsum d:\r* -s %2-%3-%4_00:00:00 -h 24 >  rbfsum_%2%3%4.log
c:\nmx\bin\rbfsum e:\r* -s %2-%3-%4_00:00:00 -h 24 >> rbfsum_%2%3%4.log
goto Done

:Usage
@echo    Usage: onedayrbfsum sta 4-digit-year 2-digit-month 2-digit-day
@echo Examples: onedayrbfsum KMP 2000 07 22   (all channels from KMP*)
@echo           onedayrbfsum all 2000 11 03   (all channels)

:Done

