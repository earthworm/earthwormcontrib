rem # Stash an image from each node cam.
rem # Schedule this script to run once a day, around 5am local time.

       call getcam edsoncam     c:\earthworm\run\cams  
       call getcam horsecam     c:\earthworm\run\cams 
       call getcam piercecam    c:\earthworm\run\cams
       call getcam cahtocam     c:\earthworm\run\cams
       call getcam southforkcam c:\earthworm\run\cams
       call getcam williamscam  c:\earthworm\run\cams
 
rem # THE END
