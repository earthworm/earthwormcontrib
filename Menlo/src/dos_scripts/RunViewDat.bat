@echo off
rem  RunViewDat.bat                         2001/07/30:LDD
doskey

echo *------------------------------------------------------*
echo * To run viewdat:                                      *
echo *   1. Connect HRD's data cable to PC's COM port.      *
echo *   2. Type viewdat command line (see below).          *
echo *------------------------------------------------------*
echo *                                                      *
echo *  Typical NCSN Usage:                                 *
echo *                                                      *
echo *         viewdat [Port] [Baud] -b[N]                  *
echo *                                                      *
echo *  where: Port = com1       (com2 also valid)          *
echo *         Baud = 96,192,384 (12,24,48,576 also valid)  *
echo *            N = 15 or 19   (bundles per packet)       *
echo *                                                      *
echo *------------------------------------------------------*
echo * Official online help:   viewdat ?                    *
echo * Example command line:   viewdat com1 192 -b19        *  
echo *------------------------------------------------------*

rem  On Windows98, set up this shortcut on the desktop:
rem    Cmdline:      c:\windows\command.com 
rem    Working:      c:\nmx\user
rem    Batch file:   RunViewDat.bat
rem    Shortcut key: None
rem    Run in:       Normal window
rem    x Close on exit

rem On WindowsNT, set up a shortcut to this batch file:
rem    Target:       RunViewDat.bat
rem    Start in:     c:\nmx\user
rem    Shortcut key: None
rem    Run in:       Normal window
rem and uncomment the following line:
cmd.exe
