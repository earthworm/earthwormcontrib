Contents of dos_scripts:
------------------------
This directory contains miscellaneous DOS scripts that I put 
here so I wouldn't totally lose track of them. 

Read comments in the scripts to find out which, if any, 
non-DOS executables are required for the script to run.
Some scripts may require editting to run on a new system.

README.txt          This file with brief descriptions of scripts.

onedayextractp.cmd  Wrapper to simplify Nanometrics extractp.exe.
onedayrbfsum.cmd    Wrapper to simplify Nanometrics rbfsum.exe.
daily_nmxlog.cmd    Schedule to create daily extractp and rbfsum logs.

RunViewDat.bat      Cheat sheet for running Nanometrics ViewDat (HRD).
RunRM4app.bat       Cheat sheet for running Nanometrics RM4 GUI.
   
getcam.cmd          Retrieve image from one NSCN webcam (uses wget.exe).
getnodecams.cmd     Schedule/config to get an image from all NCSN webcam.
    
getfwsoh4snw.cmd    Retrieve SOH from one IP-addressable FreeWave (uses wget.exe).
getfwsoh-all.cmd    Schedule/config to get SOH from all NCSN FreeWaves. 

safecopy.cmd        Creates copy of file in tempdir and moves it to target.
                    Useful for copying files for sendfileII.

vnc-start.cmd       Start VNC service from command line.
vnc-stop.cmd        Stop VNC servic from command line; useful if VNC 
                    gets hung, but SSH still has connectivity.
