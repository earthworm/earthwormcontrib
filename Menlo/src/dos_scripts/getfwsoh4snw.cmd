@echo off

rem #################################################################
rem # getfwsoh4snw.cmd                                2007/9/26:LDD
rem #
rem # Usage: getfwsoh4snw host-or-IP station network outdir
rem #
rem # Requires these non-DOS commands in %PATH%
rem #  wget.exe
rem #  html2txt.exe
rem #  fwsoh2snw.exe
rem #
rem # This script will retrieve the SOH from one Freewave radio 
rem # and store it in the requested directory with the filename: 
rem # NN-SSSS-R.snw  where NN=network code (arg 3)
rem #                      SSSS=station code (arg 2)
rem #                      R=telemetry role of freewave (arg 4)
rem #################################################################

setlocal

rem # Make sure there are no missing arguments
rem #-----------------------------------------
if "%1" EQU ""  goto Usage
if "%2" EQU ""  goto Usage
if "%3" EQU ""  goto Usage
if "%4" EQU ""  goto Usage
if "%5" EQU ""  goto Usage

rem # Make sure role has an expected value
rem #-------------------------------------
if "%4" EQU "E" goto GetSOH
if "%4" EQU "e" goto GetSOH
if "%4" EQU "R" goto GetSOH
if "%4" EQU "r" goto GetSOH
if "%4" EQU "G" goto GetSOH
if "%4" EQU "g" goto GetSOH

:Usage

@echo   Usage: getfwsoh4snw host-or-IP station network role(E/R/G) outdir
@echo          where  %%1 = host-or-IP = hostname or IPaddr of freewave radio
@echo                 %%2 = station    = station code to associate freewave with
@echo                 %%3 = network    = network code to associate freewave with
@echo                 %%4 = role       = role of freewave in telemetry path, where
@echo                                      E=endpoint R=repeater G=gateway
@echo                 %%5 = outdir     = full path of directory for output file
@echo Example: getfwsoh4snw 192.168.1.2 CPM NC E c:\snw
goto Done

:GetSOH

cd c:\temp

rem # Get raw FreeWave SOH HTML (hardcoded user:passwd)
rem #--------------------------------------------------
wget http://admin:ncsn@%1/cgi-bin/cgi_home -O %1.html -t 3

rem # Convert the HTML file to plain text
rem #-------------------------------------
html2txt %1.html %1.txt

rem # Reformat text into SeisNetWatch input & clean up
rem #--------------------------------------------------

fwsoh2snw %2 %3 %4 %1.txt %3-%2-%4.snw
move /Y %3-%2-%4.snw %5

del %1.html 
del %1.txt

:Done
endlocal
