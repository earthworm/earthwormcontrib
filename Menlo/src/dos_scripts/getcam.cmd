@echo off

rem #################################################################
rem # getcam.cmd                                     2007/10/12:LDD
rem # Usage: getcam camname outdir
rem # Requires these non-DOS commands: 
rem #   wget.exe  
rem #   c:\local\bin\date
rem #   safecopy.cmd
rem #
rem # This script will retrieve the image from one NCSN webcam and
rem # store it in the requested directory with a timestamped filename
rem # in the format:  camname_yyyymmdd_HHMMZ.jpg
rem #################################################################

setlocal
if "%1" EQU ""  goto Usage
if "%2" EQU ""  goto Usage
goto :GetImage

:Usage

@echo   Usage: getcam camname outdir optional:outdir2
@echo          where camname = hostname or IPaddr of webcam
@echo                outdir  = directory to write image file to
@echo                outdir2 = second directory to put image in
@echo Example: getcam vollercam1 c:\junk
goto Done

:GetImage

rem Set local environment variables for easy script reading
rem -------------------------------------------------------
set CAM=%1
set OUTDIR=%2
c:\local\bin\date.exe "+set IMAGENAME=%CAM%_20%%y%%m%%d_%%H%%MZ.jpg" > setimagename%1.cmd
call setimagename%1.cmd
del  setimagename%1.cmd

rem Go get the image. Only try twice, then give up
rem ----------------------------------------------
cd %OUTDIR%
wget http://quake:quake@%CAM%/image.jpg -O %IMAGENAME% -t 2

rem Copy image to optional second output directory
rem ----------------------------------------------
if "%3" EQU ""  goto Done
safecopy %IMAGENAME% %3

:Done
endlocal
