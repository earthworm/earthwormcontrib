@echo off	
rem _______________________________________________________
rem |
rem |  onedayextractp.cmd
rem |
rem |    args: %1 = station name (or first few letters)
rem |               use 'all' or 'ALL' to do all channels
rem |          %2 = 4-digit year
rem |          %3 = 2-digit month
rem |          %4 = 2-digit day-of-month
rem | 
rem |  This file runs a summary extractp on given Nanometrics  
rem |  channels for the given day.  It writes the log file
rem |  in the current working directory.
rem |
rem |  NOTE: % is the escape character for CMD scripts!
rem |______________________________________________________

if "%4"=="" goto Usage
if "%3"=="" goto Usage
if "%2"=="" goto Usage
if "%1"=="" goto Usage

if %1==all goto AllChannels
if %1==ALL goto AllChannels

c:\nmx\bin\extractp -s %2-%3-%4-00-00-00 -d 86400 -i d:\r*%1* >  %1.extractp_%2%3%4.log
c:\nmx\bin\extractp -s %2-%3-%4-00-00-00 -d 86400 -i e:\r*%1* >> %1.extractp_%2%3%4.log
goto Done

:AllChannels
c:\nmx\bin\extractp -s %2-%3-%4-00-00-00 -d 86400 -i d:\r* >  extractp_%2%3%4.log
c:\nmx\bin\extractp -s %2-%3-%4-00-00-00 -d 86400 -i e:\r* >> extractp_%2%3%4.log
goto Done

:Usage
@echo    Usage: onedayextractp sta 4-digit-year 2-digit-month 2-digit-day
@echo Examples: onedayextractp KMP 2000 07 22   (all channels from KMP*)
@echo           onedayextractp all 2000 11 03   (all channels)

:Done