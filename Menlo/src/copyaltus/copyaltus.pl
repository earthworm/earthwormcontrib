#
# copyaltus.pl
#
# This program is a replacement for the Kinemetrics NMSD program.
# The Kinemetrics NMS program creates several types of files:
# evt, ft, call, and status.  Copyaltus adds a time stamp to the
# name of each of these files and distributes them to various
# directories.
#
# Updates:
#
# Program modified to send files to nq2, as well as nq1.  WMK 9/17/2012
#
# Program no longer sends to nsmprt1 and nq2.  Now, files that were
# being sent to nsmprt1 are sent to nq1.  WMK 9/4/2012
#
# Program now checks all input directories and exits.  Restart the
# program automatically using Windows Scheduled Tasks.  WMK 9/3/2009
#
# Copy function tests to the shakemap directory.  WMK 9/29/2009
#
# This version copies evt, ft, call, and status files to shakemapdir,
# nq1, and nq2.  Heartbeat files from this script (copyaltus.hb) are
# no longer sent.
# WMK 5/1/2012
#
# This version reads NMS heartbeat files, named nms_heartbeat.txt, from
# directory C:\nms\heartbeats and writes them to shakemapdir, nq1,
# and nq2.  WMK 5/1/2012
# ------------------------------------------------------------------
#
# Use the file copy module that comes with perl.
#
use File::Copy;

# Configuration parameters are here:
# **********************************
#
# Evt files and function-test files from NMS show up here.
$evtindir = 'C:\nms\data';
#
# Call and status files from NMS show up here.
$statusindir = 'C:\nms\data\status';
#
# Heartbeats from NMS show up here.
$heartbeatindir = 'C:\nms\heartbeats';
#
# Call, status, evt, and function-test files are copied to
# nq1dir and nq2dir.
# They are used by SeisnetWatch and ShakeMap.
$nq1dir = 'C:\nms\nq1';
$nq2dir = 'C:\nms\nq2';
#
# Evt files are copied to allevtsdir.  These files live on
# the local machine only.
$allevtsdir = 'C:\nms\allevts';
#
# Call and status files are copied to allstationsdir.
# These files live on the local machine only.
$allstationsdir = 'C:\nms\allstations';
#
# Status and evt files are copied to smproc2dir.
# From there, they are sent to the redoubt dataserver PC.
$smproc2dir = 'C:\nms\smproc2';
#
# Function test files are sent to the ftestsdir directory.
$ftestsdir = 'C:\nms\ftests';
#
# Unrecognized files are sent to the troubledir directory.
$troubledir = 'C:\nms\trouble';
#
# Name of log file.
# If the next line is commented out, write log to stdout.
# $logfile = 'C:\nms\logfile\copyaltus.log';
#
# Full path to readevt executable.
# Readevt gets info from evt file headers.
$readevt = 'C:\local\bin\readevt.exe';
#
# Configuration parameters end here.
# ----------------------------------
#
# Open log file, and send current time to logfile.
# ------------------------------------------------
#
   if ( $logfile eq "" ) {
      open( LOG, ">-" ) || die "Can't open stdout\n"; }
   else {
      open( LOG, ">> $logfile" ) || die "Can't open $logfile: $!"; }
   $currentTime = &getdatetime();
   print LOG "copyaltus started at $currentTime GMT\n";
#
# Loop through all files in the evt input directory.
# Ignore directories.  (They end with / when listed with ls -F)
# -------------------------------------------------------------
#
   unless (chdir $evtindir) {
      print LOG "Can't chdir to $evtindir: $!\n"; exit; }
   $list = `C:\\local\\bin\\ls -F `;
   @list = split (' ',$list);
   EVTFILE: foreach $infile (@list) {
      $lastchar = substr( $infile,-1,1 );
      if ( $lastchar eq "/" ) { next EVTFILE };    # Ignore directories
      print LOG "\ninfile: $infile \n";
#
# If we can't open the file, it may have been opened by another process.
# Skip it for now.
#
      open( EVT, $infile ) || next EVTFILE;
      close EVT;
#
# Determine the file type (evt, ft, or unknown).
#
      ($body = $infile) =~ s/([^.]*)\.([^.]*).*/$1/;
      ($prefix) = split('_', $body);
      ($suffix = $infile) =~ s/([^.]*)\.([^.]*).*/$2/;
      $filetype = "unknown";
      if ($suffix eq "evt" || $suffix eq "EVT") {
         if ( open(READEVT, "$readevt -functest $infile |") ) {
            $readevtout = <READEVT>;
            close READEVT;
            chop $readevtout;     # Remove trailing newline
            $filetype = "evt";
            if ( $readevtout eq "IS_FT" ) {$filetype = "ft";}
         }
      }
      print LOG "filetype: $filetype \n";
#
# Rename the evt/ft file, using the event trigger time.
# Unrecognized files are not renamed.
#
      if ( $filetype eq "evt" || $filetype eq "ft" ) {
         unless (open( READEVT, "$readevt -trigtime $infile |" )) {
            print LOG "Can't get trigger time: $!";
            exit; }
         $trigtime = <READEVT>;
         close READEVT;
         chop $trigtime;     # Remove trailing newline
         ($body1 = $body) =~ s/([^_]*)\_([^_]*)_*/$1/;
         ($body2 = $body) =~ s/([^_]*)\_([^_]*)_*/$2/;
         $outfile = $trigtime.'_'.$body2.'_'.$body1.'.'.$suffix;
      }
      else { $outfile = $infile; }
#
# Copy evt files to the allevts, nq1dir, nq2dir, and scproc2
# directories.  If copy fails, don't delete the input file.
#
      if ( $filetype eq "evt" ) {
         foreach $outdir ($allevtsdir, $nq1dir, $nq2dir, $smproc2dir) {
            $outpath = $outdir.'\\'.$outfile;
            unless (copy( $infile, $outpath )) {
               print LOG "copy error: $!";
               next EVTFILE; }
            print LOG "$outfile copied to $outdir\n";
         }
      }
#
# Copy function-test files to the ftests, nq1dir, and
# nq2dir directories.
#
      if ( $filetype eq "ft" ) {
         foreach $outdir ($ftestsdir, $nq1dir, $nq2dir) {
            $outpath = $outdir.'\\'.$outfile;
            unless (copy( $infile, $outpath )) {
               print LOG "copy error: $!";
               next EVTFILE; }
            print LOG "$outfile copied to $outdir\n";
         }
      }
#
# Copy unrecognized files to the trouble directory.
#
      if ( $filetype eq "unknown" ) {
         $outpath = $troubledir.'\\'.$outfile;
         unless (copy( $infile, $outpath )) {
            print LOG "copy error: $!";
            next EVTFILE; }
         print LOG "$outfile copied to $troubledir\n";
      }
#
# Delete the evt/ft input file, now that it's been copied everywhere.
#
      unlink $infile || print LOG "Can't delete input file: $!";
   }
#
# Loop through all files in the status input directory
# ----------------------------------------------------
#
   unless (chdir $statusindir) {
      print LOG "Can't chdir to $statusindir: $!\n"; exit; }
   $list = `C:\\local\\bin\\ls -F `;
   @list = split (' ',$list);
   STATUSFILE: foreach $infile (@list) {
      $lastchar = substr( $infile,-1,1 );
      if ( $lastchar eq "/" ) { next STATUSFILE };   # Ignore directories
      print LOG "\ninfile: $infile \n";
#
# If we can't open the file, it may have been opened by another process.
# Skip it for now.
#
      open( EVT, $infile ) || next STATUSFILE;
      close EVT;
#
# Is the input file is a call or status?
#
      ($body = $infile) =~ s/([^.]*)\.([^.]*).*/$1/;
      ($prefix) = split('_', $body);
      ($suffix = $infile) =~ s/([^.]*)\.([^.]*).*/$2/;
      if    ($prefix eq "call" && $suffix eq "txt") {$filetype = "call";}
      elsif ($prefix eq "stat" && $suffix eq "txt") {$filetype = "stat";}
      else {$filetype = "unknown";}
      print LOG "filetype: $filetype \n";
#
# Rename the call/stat file using current time from system clock.
# Unrecognized files are not renamed.
#
      if ( $filetype eq "call" || $filetype eq "stat" ) {
         $outfile = $body.'_'.$currentTime.'.'.$suffix;
      }
      else {
         print LOG "Current time: $currentTime \n";
         $outfile = $infile;
      }
#
# Copy call files to the allstations, nq1dir, and nq2dir directories.
#
      if ( $filetype eq "call" ) {
         foreach $outdir ($allstationsdir, $nq1dir, $nq2dir) {
            $outpath = $outdir.'\\'.$outfile;
            unless (copy( $infile, $outpath )) {
               print LOG "copy error: $!";
               next STATUSFILE; }
            print LOG "$outfile copied to $outdir\n";
         }
      }
#
# Copy stat files to the allstations, nq1dir, nq2dir, and
# smproc2 directories.
#
      if ( $filetype eq "stat" ) {
         foreach $outdir ($allstationsdir, $nq1dir, $nq2dir, $smproc2dir) {
            $outpath = $outdir.'\\'.$outfile;
            unless (copy( $infile, $outpath )) {
               print LOG "copy error: $!";
               next STATUSFILE; }
            print LOG "$outfile copied to $outdir\n";
         }
      }
#
# Copy unrecognized files to the trouble directory.
#
      if ( $filetype eq "unknown" ) {
         $outpath = $troubledir.'\\'.$outfile;
         unless (copy( $infile, $outpath )) {
            print LOG "copy error: $!";
            next STATUSFILE; }
         print LOG "$outfile copied to $troubledir\n";
      }
#
# Delete the call or stat input file, now that it's been copied everywhere.
#
      unlink $infile || print LOG "Can't delete file: $!";
   }
#
# Loop through all files in the NMS heartbeat input directory
# -----------------------------------------------------------
#
   unless (chdir $heartbeatindir) {
      print LOG "Can't chdir to $heartbeatindir: $!\n"; exit; }
   $list = `C:\\local\\bin\\ls -F `;
   @list = split (' ',$list);
   HEARTBEATFILE: foreach $infile (@list) {
      $lastchar = substr( $infile,-1,1 );
      if ( $lastchar eq "/" ) { next HEARTBEATFILE };   # Ignore directories
      print LOG "\ninfile: $infile \n";
#
# If we can't open the file, it may have been opened by another process.
# Skip it for now.
#
      open( HB, $infile ) || next HEARTBEATFILE;
      close HB;
#
# Do not rename the heartbeat file.
# Use the name assigned by the Kinemetrics mininmgr program.
#
      print LOG "Current time: $currentTime \n";
#
# Delete the heartbeat file
# -------------------------
      unlink $infile || print LOG "Can't delete file: $!";
   }
#
# End of main program.
#
# Subroutine to get GMT time from system clock.
# ---------------------------------------------
#
sub getdatetime {
   local($datetime);
   local(@months) = qw(01 02 03 04 05 06 07 08 09 10 11 12);
   local($second, $minute, $hour, $dayOfMonth, $month, $yearOffset, $wday, $yday, $isdst) = gmtime();
   local($year) = 1900 + $yearOffset;
   if($dayOfMonth < 10) {$dayOfMonth = "0"."$dayOfMonth";}
   if($hour < 10) {$hour = "0"."$hour";}
   if($minute < 10) {$minute = "0"."$minute";}
   if($second < 10) {$second = "0"."$second";}
   $datetime = "$year$months[$month]$dayOfMonth"."_"."$hour$minute$second";
}
