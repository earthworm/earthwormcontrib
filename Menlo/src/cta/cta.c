
    /***************************************************************************
     *                                  cta.c                                  *
     *                                                                         *
     *  Program to attach to one of earthworm's shared memory region(s) and    *
     *  send a pause message to write_cta.  write_cta will then stop writing   *
     *  to tape to allow the user to change tapes or clean tape heads, etc.    *
     ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>

/* Things to read from configuration file
 ****************************************/
static char RingName[20];        /* Name of transport ring for io */


/************************************************************************
 *  GetConfig() processes command file(s) using kom.c functions;        *
 *              exits if any errors are encountered.                    *
 ************************************************************************/
void GetConfig( char *configfile )
{
   int      ncommand = 1;    /* # of commands you expect to process   */
   char     init[10];        /* init flags, one byte for each required command */
   int      nmiss;           /* number of required commands that were missed   */
   char    *com;
   char    *str;
   int      nfiles;
   int      success;
   int      i;
 
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;
 
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      printf( "Error opening command file <%s>. Exiting.\n", configfile );
      exit( -1 );
   }
 
   while ( nfiles > 0 )   /* While there are command files open */
   {
      while ( k_rd() )        /* Read next line from active file  */
      {  
         com = k_str();         /* Get the first token from line */
 
         if ( !com )          continue;
         if ( com[0] == '#' ) continue;
 
        /* Open a nested configuration file
           ********************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "Error opening command file <%s>. Exiting.\n", &com[1] );
               exit( -1 );
            }
            continue;
         }   
         else if ( k_its("RingName") )
         {
            str = k_str();
            if (str) strcpy( RingName, str );
            init[0] = 1;
         }
         else
         {
            printf( "<%s> Unknown command in <%s>.\n", com, configfile );
            continue;
         }
 
         if ( k_err() )
         {
            printf( "Bad <%s> command in <%s>. Exiting.\n", com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }     
 
/* Check init flags for missed commands
   ************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] ) nmiss++;
   if ( nmiss )
   {
      printf( "ERROR, no " );
      if ( !init[0] ) printf( "<RingName> " );
      printf( "command(s) in <%s>. Exiting.\n", configfile );
      exit( -1 );
   }
   return;
}


int main( int argc, char *argv[] )
{
   SHM_INFO  region;
   long      key;
   char      *command;
   char      *runPath;
   char      configFile[80];
   MSG_LOGO  logo;
   long      size;
   char      msg[40];
   unsigned char type;
   unsigned char inst;

/* Check the program arguments
   ***************************/
   if ( argc < 2 )
   {
      printf( "Usage: cta <pause>|<resume>\n" );
      return -1;
   }

   command = argv[1];
   if ( strcmp(command, "pause")  != 0 &&
        strcmp(command, "resume") != 0 )
   {
      printf( "Usage: cta <pause>|<resume>\n" );
      return -1;
   }
 
/* Get the path to the Earthworm params directory
   **********************************************/
   runPath = getenv( "EW_PARAMS" );
 
   if ( runPath == NULL )
   {
      printf( "Environment variable EW_PARAMS not defined. Exiting.\n" );
      return -1;
   }
 
   if ( *runPath == '\0' )
   {
      printf( "Environment variable EW_PARAMS has no value. Exiting.\n" );
      return -1;
   }
 
/* Get the config file parameters
   ******************************/
   strcpy( configFile, runPath );
   strcat( configFile, "cta.d" );
   GetConfig( configFile );

/* Attach to the transport ring
   ****************************/
   if ( (key = GetKey(RingName)) == -1 )
   {
      printf( "Invalid ring name <%s>. Exiting.\n", RingName );
      return -1;
   }
   tport_attach ( &region, key );

/* Build the message logo
   **********************/
   if ( GetLocalInst( &inst ) != 0 )
   {
      printf(  "Error getting local installation id. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_CTA", &type ) != 0 )
   {
      printf( "Invalid message type <TYPE_CTA>. Exiting.\n" );
      return -1;
   }

   logo.instid = inst;
   logo.mod    = 0;
   logo.type   = type;

   sprintf( msg, "%s\n", command );
   size = strlen( msg );

   if ( tport_putmsg( &region, &logo, size, msg ) != PUT_OK )
      printf( "Error sending pause message to write_cta.\n" );

   tport_detach ( &region );
   return 0;
}
