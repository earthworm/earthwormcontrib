#
#                       Make file for cta
#                        Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib
I = $(EW_HOME)/$(EW_VERSION)/include

CTA = cta.o $L/kom.o $L/getutil.o $L/sleep_ew.o $L/transport.o    

all:
	make -f makefile.sol cta
	make -f makefile.sol copyscripts

cta: $(CTA)
	cc -o $B/cta $(CTA) -lposix4

copyscripts: 
	cp first_last* $B

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/cta*
