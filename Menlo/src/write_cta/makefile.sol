#
#                    Make file for write_cta
#                        Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib


WRITETAPE = write_cta.o ddtrace.o tapeio.o setprio.o \
        $L/logit_mt.o $L/kom.o $L/getutil.o $L/sleep_ew.o \
        $L/time_ew.o $L/transport.o $L/threads_ew.o \
        $L/sema_ew.o $L/swap.o $L/trheadconv.o

write_cta: $(WRITETAPE)
	cc -o $B/write_cta $(WRITETAPE) -mt -lm -lposix4 -lthread -lc

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/write_cta*

