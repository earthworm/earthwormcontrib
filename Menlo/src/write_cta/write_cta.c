
  /************************************************************************
   *                              write_cta.c                             *
   ************************************************************************/
/*
   This program writes tracebuf messages to a set of archive tapes,
   known as a continuous tape archive (cta).

   The data are buffered to temporary disk files.  At regular
   intervals, the disk files are written to tape.  When earthworm is
   told to die,  write_cta won't exit until it has finished writing
   the all the temporary disk files to tape.  Startstop may send a
   SIGTERM signal to write_cta, but write_cta will ignore it.

   The program will optionally compress the data, using the UNIX
   "compress" command, before writing it to tape.

   The program invokes the UNIX "tar" command to write the temporary
   files to tape.

   The UNIX tar, mt, and compress commands must all be in the search
   path.

   If startstop sends a termination signal, compress or tar may die,
   since they are invoked as child processes to write_cta.  To avoid
   this possibility, set KillDelay in startstop_sol.d to a large number.
   Try setting KillDelay to the tape write interval, or larger.

   The program may be paused and resumed by entering the commands
   "cta pause" or "cta resume".  This allows the user to change tapes
   while continuing to store tracebuf messages in the temporary disk
   files.  If the program is left in pause mode, the disk will
   quickly fill up (depending on the amount of free space).  While
   the program is in pause mode, the computer will beep as a reminder
   to resume.

   To avoid dropping messages on the transport ring, it's probably a
   good idea to run this program in realtime (RT) mode.  Parameters
   in the configuration file, write_cta.d, allow the "compress" and
   "tar" commands to be run at lower priorities (recommended).

   This program runs on Solaris only.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <unistd.h>
#include <kom.h>
#include <transport.h>
#include <trace_buf.h>
#include <swap.h>
#include <trheadconv.h>
#include <time_ew.h>
#include "write_cta.h"
#include "tapeio.h"

/* Function prototypes
   *******************/
void    GetConfig( char * );
void    LogConfig( void );
void    LookupMore( void );
void    LookupMsgType( void );
void    WriteTracebuf2Msg( char *, long, int );
thr_ret ddTrace( void * );

/* Global variables
   ****************/
static SHM_INFO Region;              /* Shared memory region to use for i/o */
static pid_t    myPid;               /* Process id of this process */
static FILE     *fp;
static char     Text[150];           /* String for log/error messages */

/* Get these from the config file
   ******************************/
static MSG_LOGO *GetLogo = NULL;     /* Array for module,type,instid */
static size_t   GetLogoSize = 0;     /* In bytes */
static short    nLogo = 0;
static char     RingName[20];        /* Name of transport ring for i/o */
static char     MyModName[20];       /* Speak as this module name/id */
static long     HeartBeatInt;        /* Seconds between heartbeats */
static int      MsgRecSize;          /* In bytes */
static char     TapeName[80];        /* Name of tape device */
static char     Changer[80];         /* Name of tape changer, or "none" */
static char     SpoolDir[100];       /* Name of spool directory */
static int      Compress;            /* If 1, compress */
static int      Priority;            /* Priority for mt, mtx, compress, and tar */
static char     Class[20];           /* Class for mt, mtx, compress, and tar */

/* Look these up in the earthworm.h tables with getutil functions
   **************************************************************/
static long          RingKey;        /* Transport ring key */
static unsigned char InstId;         /* Local installation id */
static unsigned char MyModId;        /* Module Id for this program */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeTracebuf;
static unsigned char TypeTracebuf2;
static unsigned char TypeCta;

/* Share these with the thread function
   ************************************/
volatile char *tapename;            /* Name of tape device */
volatile char *changer;             /* Name of tape changer or "none" */
volatile char *spooldir;            /* Name of spool directory */
volatile int  term_flag = 0;        /* 1 if transport ring term flag set */
volatile int  pauseWrite = 0;       /* 1 if tape writing is paused */
volatile int  TapeWriteIntSec;      /* Write to tape this often */
volatile int  compress;             /* If 1, compress */
volatile char *class;               /* RT or TS */
volatile int  priority;             /* Priority for mt, mtx, compress, and tar */
volatile unsigned char typeError;   /* message type for error */


int main( int argc, char **argv )
{
   long          timeNow;           /* Current time */
   long          timeLastBeat;      /* Time last heartbeat was sent */
   long          recsize;           /* Size of retrieved message */
   MSG_LOGO      reclogo;           /* Logo of retrieved message */
   MSG_LOGO      *logoPtr;
   int           res;
   TRACE_HEADER  *TraceHead;        /* Pointer to tracebuf header */
   TRACE2_HEADER *Trace2Head;       /* Pointer to tracebuf2 header */
   char          *TraceBuf;         /* Buffer to hold event msg */
   unsigned      thread_id;
   char          *configFile;

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      printf( "Usage: write_cta <configfile>\n" );
      return 0;
   }
   configFile = argv[1];

/* Look up message types from earthworm.h tables
   *********************************************/
   LookupMsgType();

/* Read the configuration file
   ***************************/
   GetConfig( configFile );

/* Look up more info from earthworm.h tables
   *****************************************/
   LookupMore();

/* Initialize name of log-file & open it
   *************************************/
   logit_init( "write_cta", (short) MyModId, 256, 1 );
   logit( "" , "Read command file <%s>\n", argv[1] );

/* Get the pid of this process for restart purposes
   ************************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "write_cta: Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Log the configuration file
   **************************/
   LogConfig();

/* Look for TYPE_CTA messages on the transport ring.
   They are used to pause and resume tape writing.
   ************************************************/
   GetLogoSize += sizeof( MSG_LOGO );
   logoPtr = (MSG_LOGO *)realloc( GetLogo, GetLogoSize );
   if ( logoPtr == NULL )
   {
      printf( "write_cta: realloc() error in main(). Exiting.\n" );
      return -1;
   }
   GetLogo = logoPtr;
   GetLogo[nLogo].instid = 0;
   GetLogo[nLogo].mod    = 0;
   GetLogo[nLogo].type   = TypeCta;
   nLogo++;

/* Allocate buffer to hold one tracebuf message
   ********************************************/
   TraceBuf = (char *) malloc( (size_t)(MsgRecSize+1) );
   if ( TraceBuf == NULL )
   {
      logit( "e", "Couldn't allocate the message buffer. Exiting.\n" );
      return -1;
   }
   logit( "", "Message buffer allocated.\n" );

/* Pointer to the tracebuf or tracebuf2 message
   ********************************************/
   TraceHead  = (TRACE_HEADER *) TraceBuf;
   Trace2Head = (TRACE2_HEADER *) TraceBuf;

/* Attach to shared memory ring
   ****************************/
   tport_attach( &Region, RingKey );
   logit( "", "Attached to public memory region: %s\n", RingName );

/* Force a heartbeat to be issued in first pass thru main loop
   ***********************************************************/
   timeLastBeat = time(&timeNow) - HeartBeatInt - 1;

/* Start the ddTrace thread, which will copy trace files to tape
   *************************************************************/
   if ( StartThread( ddTrace, 0, &thread_id ) < 0 )
   {
      logit( "e", "write_cta: Can't start the ddTrace thread.\n" );
      return -1;
   }
   logit( "", "ddtrace thread started.\n" );

/* Main loop
   *********/
   while ( 1 )
   {

/* Send a heartbeat to statmgr
   ***************************/
      if  ( (time(&timeNow) - timeLastBeat) >= HeartBeatInt )
      {
         timeLastBeat = timeNow;
         SendStatus( TypeHeartBeat, 0, "" );
      }

/* Process all new messages
   ************************/
      do
      {

/* See if a termination has been requested.
   If so, close the current disk file, and
   wait for wait for ddtrace to kill the process.
   *********************************************/
         if ( tport_getflag( &Region ) == TERMINATE ||
              tport_getflag( &Region ) == myPid )
         {
            tport_detach( &Region );
            WriteTracebuf2Msg( TraceBuf, recsize, 1 );

            logit( "et", "write_cta: Waiting for tape write completion...\n" );

            sigignore( SIGTERM );             /* Don't let startstop kill me */

            term_flag = 1;                    /* Let ddtrace know we're stopping */

            while ( 1 )                       /* Wait until ddtrace is finished */
               sleep_ew( 1000 );
         }

/* Get type cta, tracebuf, and tracebuf2 messages from transport ring
   ******************************************************************/
         res = tport_getmsg( &Region, GetLogo, nLogo,
                             &reclogo, &recsize, TraceBuf, MsgRecSize );

         if ( res == GET_NONE )              /* No more new messages */
            break;
         if ( res == GET_TOOBIG )            /* Next message was too big */
         {                                   /* Try again */
            sprintf( Text,
               "Retrieved msg[%ld] (i%u m%u t%u) too big for buffer[%d]",
               recsize, reclogo.instid, reclogo.mod, reclogo.type,
               MsgRecSize );
            SendStatus( TypeError, ERR_TOOBIG, Text );
            continue;
         }
         if ( res == GET_MISS )         /* Got a msg, but missed some */
         {
            sprintf( Text, "Missed msg(s)  i%u m%u t%u  %s.",
               reclogo.instid, reclogo.mod, reclogo.type, RingName );
            SendStatus( TypeError, ERR_MISSMSG, Text );
         }
         if ( res == GET_NOTRACK )      /* Got a msg, but can't tell */
         {                                   /* if any were missed */
            sprintf( Text, "Msg received; transport.h NTRACK_GET exceeded" );
            SendStatus( TypeError, ERR_NOTRACK, Text );
         }

/* This is a cta control message
   *****************************/
         if ( reclogo.type == TypeCta )
         {
            if ( strncmp( TraceBuf, "pause",  5 ) == 0 )
            {
               puts( "write_cta: Received tape pause command." );
               puts( "write_cta: Do not change tapes until you are told it is safe." );
               pauseWrite = 1;
            }

            if ( strncmp( TraceBuf, "resume", 6 ) == 0 )
            {
               puts( "write_cta: Tape writing is now resumed." );
               pauseWrite = 0;
            }
            continue;       /* Get the next message */
         }

/* This is a TYPE_TRACEBUF or TYPE_TRACEBUF2 message.
   Convert it to local byte order.
   *************************************************/
         if ( reclogo.type == TypeTracebuf )
         {
            if ( WaveMsgMakeLocal( TraceHead ) < 0 )
            {
               logit( "et", "write_cta: WaveMsgMakeLocal() error.\n" );
               continue;
            }
         }
         else
            if ( WaveMsg2MakeLocal( Trace2Head ) < 0 )
            {
               logit( "et", "write_cta: WaveMsg2MakeLocal error.\n" );
               continue;
            }

/* If this is a TYPE_TRACEBUF message, convert it to TYPE_TRACEBUF2
   ****************************************************************/
         if ( reclogo.type == TypeTracebuf )
            Trace2Head = TrHeadConv( TraceHead );

/* The message is now TYPE_TRACEBUF2.  Write it to a disk file
   ***********************************************************/
         WriteTracebuf2Msg( TraceBuf, recsize, 0 );

      } while ( res != GET_NONE );    /* End of message-processing loop */

      sleep_ew( 250 );                /* Wait for new messages to arrive */
   }
}


  /************************************************************************
   *  GetConfig() processes command file(s) using kom.c functions;        *
   *                    exits if any errors are encountered.              *
   ************************************************************************/

#define NCOMMAND 12

void GetConfig( char *configfile )
{
   const int ncommand = NCOMMAND;    /* Process this many required commands */
   char      init[NCOMMAND];         /* Init flags, one for each command */
   int       nmiss;                  /* Number of missing commands */
   char      *com;
   char      *str;
   int       nfiles;
   int       success;
   int       i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      printf( "write_cta: Error opening command file <%s>. Exiting.\n",
               configfile );
      exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )             /* Read next line from active file  */
      {
         com = k_str();            /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )          continue;
         if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
         if ( com[0] == '@' )
         {
            success = nfiles+1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "write_cta: Can't open command file <%s>. Exiting.\n",
                        &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         else if ( k_its("MyModuleId") )
         {
             str = k_str();
             if (str) strcpy( MyModName, str );
             init[0] = 1;
         }
         else if ( k_its("RingName") )
         {
             str = k_str();
             if (str) strcpy( RingName, str );
             init[1] = 1;
         }
         else if ( k_its("HeartBeatInt") )
         {
             HeartBeatInt = k_long();
             init[2] = 1;
         }
         else if ( k_its("MsgRecSize") )
         {
             MsgRecSize = k_int();
             init[3] = 1;
         }
         else if ( k_its("TapeName") )
         {
             str = k_str();
             if (str) strcpy( TapeName, str );
             tapename = &TapeName[0];
             init[4] = 1;
         }
         else if ( k_its("Changer") )
         {
             str = k_str();
             if (str) strcpy( Changer, str );
             changer = &Changer[0];
             init[5] = 1;
         }
         else if ( k_its("GetEventsFrom") )
         {
             unsigned char instId;
             unsigned char modId;
             MSG_LOGO      *logoPtr;

             if ( ( str=k_str() ) )
             {
                if ( GetInst( str, &instId ) != 0 )
                {
                   printf( "write_cta: Invalid installation name <%s>", str );
                   printf( " in <GetEventsFrom> cmd. Exiting.\n" );
                   exit( -1 );
                }
             }
             if ( ( str=k_str() ) )
             {
                if ( GetModId( str, &modId ) != 0 )
                {
                   printf( "write_cta: Invalid module name <%s>", str );
                   printf( " in <GetEventsFrom> cmd. Exiting.\n" );
                   exit( -1 );
                }
             }
             GetLogoSize += sizeof( MSG_LOGO );
             logoPtr = (MSG_LOGO *)realloc( GetLogo, GetLogoSize );
             if ( logoPtr == NULL )
             {
                printf( "write_cta: realloc() error 1 in GetConfig(). Exiting.\n" );
                exit( -1 );
             }
             GetLogo = logoPtr;
             GetLogo[nLogo].instid = instId;
             GetLogo[nLogo].mod    = modId;
             GetLogo[nLogo].type   = TypeTracebuf;
             nLogo ++;

             GetLogoSize += sizeof( MSG_LOGO );
             logoPtr = (MSG_LOGO *)realloc( GetLogo, GetLogoSize );
             if ( logoPtr == NULL )
             {
                printf( "write_cta: realloc() error 2 in GetConfig(). Exiting.\n" );
                exit( -1 );
             }
             GetLogo = logoPtr;
             GetLogo[nLogo].instid = instId;
             GetLogo[nLogo].mod    = modId;
             GetLogo[nLogo].type   = TypeTracebuf2;
             nLogo ++;
             init[6] = 1;
         }
         else if ( k_its("SpoolDir") )
         {
             str = k_str();
             if (str) strcpy( SpoolDir, str );
             spooldir = &SpoolDir[0];
             init[7] = 1;
         }
         else if ( k_its("TapeWriteIntSec") )
         {
             TapeWriteIntSec = k_int();
             init[8] = 1;
         }
         else if ( k_its("Compress") )
         {
             Compress = k_int();
             compress = Compress;
             init[9] = 1;
         }
         else if ( k_its("Class") )
         {
             str = k_str();
             if (str) strcpy( Class, str );
             class = &Class[0];
             init[10] = 1;
         }
         else if ( k_its("Priority") )
         {
             Priority = k_int();
             priority = Priority;
             init[11] = 1;
         }

/* Unknown command
   ***************/
         else
         {
             printf( "write_cta: <%s> Unknown command in <%s>.\n",
                      com, configfile );
             continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "write_cta: Bad <%s> command in <%s>. Exiting.\n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
       printf( "write_cta: ERROR, no " );
       if ( !init[0] )  printf( "<MyModuleId> "      );
       if ( !init[1] )  printf( "<RingName> "        );
       if ( !init[2] )  printf( "<HeartBeatInt> "    );
       if ( !init[3] )  printf( "<MsgRecSize> "      );
       if ( !init[4] )  printf( "<TapeName> "        );
       if ( !init[5] )  printf( "<Changer> "         );
       if ( !init[6] )  printf( "<GetEventsFrom> "   );
       if ( !init[7] )  printf( "<SpoolDir> "        );
       if ( !init[8] )  printf( "<TapeWriteIntSec> " );
       if ( !init[9] )  printf( "<Compress> "        );
       if ( !init[10])  printf( "<Class> "           );
       if ( !init[11])  printf( "<Priority> "        );
       printf( "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }
   return;
}


  /************************************************************************
   *  LogConfig() prints the config parameters in the log file.           *
   ************************************************************************/

void LogConfig( void )
{
   int i;

   logit( "", "\nConfig File Parameters:\n" );
   logit( "", "   MyModuleId:      %s\n", MyModName );
   logit( "", "   RingName:        %s\n", RingName );
   logit( "", "   HeartBeatInt:    %d\n", HeartBeatInt );
   logit( "", "   MsgRecSize:      %d\n", MsgRecSize );
   logit( "", "   TapeName:        %s\n", TapeName );
   logit( "", "   Changer:         %s\n", Changer );
   logit( "", "   SpoolDir:        %s\n", SpoolDir );
   logit( "", "   TapeWriteIntSec: %d\n", TapeWriteIntSec );
   logit( "", "   Compress:        %d\n", Compress );
   logit( "", "   Class:           %s\n", Class );
   logit( "", "   Priority:        %d\n", Priority );

   for ( i = 0; i < nLogo; i++ )
   {
      logit( "", "   GetEventsFrom:  inst:%u mod:%u type:%u\n",
         GetLogo[i].instid, GetLogo[i].mod, GetLogo[i].type );
   }
   return;
}


  /**********************************************************************
   *  LookupMsgType( )   Look up message types from earthworm.h tables  *
   **********************************************************************/

void LookupMsgType( void )
{
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "write_cta: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "write_cta: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   typeError = TypeError;
   if ( GetType( "TYPE_CTA", &TypeCta ) != 0 )
   {
      printf( "write_cta: Invalid message type <TYPE_CTA>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF", &TypeTracebuf ) != 0 )
   {
      printf( "write_cta: Invalid message type <TYPE_TRACEBUF>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF2", &TypeTracebuf2 ) != 0 )
   {
      printf( "write_cta: Invalid message type <TYPE_TRACEBUF2>. Exiting.\n" );
      exit( -1 );
   }
}


  /**********************************************************************
   *  LookupMore( )   Look up more from earthworm.h tables              *
   **********************************************************************/

void LookupMore( void )
{
   if ( ( RingKey = GetKey(RingName) ) == -1 )
   {
        printf( "write_cta:  Invalid ring name <%s>. Exiting.\n", RingName);
        exit( -1 );
   }
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "write_cta: Error geting local installation id. Exiting.\n" );
      exit( -1 );
   }
   if ( GetModId( MyModName, &MyModId ) != 0 )
   {
      printf( "write_cta: Invalid module name <%s>; Exiting.\n", MyModName );
      exit( -1 );
   }
   return;
}


/******************************************************************************
 * SendStatus() builds a heartbeat or error message & puts it into            *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/

void SendStatus( unsigned char type, short ierr, char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   long     t;

/* Build the message
   *****************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if ( type == TypeHeartBeat )
      sprintf( msg, "%ld %d\n", t, myPid );
   else if ( type == TypeError )
   {
      sprintf( msg, "%ld %hd %s\n", t, ierr, note );
      logit( "et", "write_cta: %s\n", note );
   }
   else
      return;

   size = strlen( msg );           /* Don't include null byte in message */

/* Write the message to shared memory
   **********************************/
   if ( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if ( type == TypeHeartBeat )
           logit("et","write_cta:  Error sending heartbeat.\n" );
        else
           if ( type == TypeError )
              logit("et","write_cta:  Error sending error:%d.\n", ierr );
   }
   return;
}


   /**********************************************************************
    *                         RenameEventFile()                          *
    *  The names of old tracebuf files begin with "wt".                  *
    *  The names of new tracebuf2 files beging with "wu".                *
    *  That's how we keep track of the format of data with the files.    *
    **********************************************************************/

void RenameEventFile( char tempFileName[], time_t *tempFileStartTime )
{
   struct tm strTime;
   char      filename[17];                 /* Allow for the null byte */
   char      eventFileName[80];

   gmtime_ew( tempFileStartTime, &strTime );
   sprintf( filename, "wu%04d%02d%02d%02d%02d%02d",
            strTime.tm_year+1900,
            strTime.tm_mon+1,
            strTime.tm_mday,
            strTime.tm_hour,
            strTime.tm_min,
            strTime.tm_sec );
   strcpy( eventFileName, (char *)SpoolDir );
   strcat( eventFileName, "/" );
   strcat( eventFileName, filename );

   if ( rename( tempFileName, eventFileName ) == -1 )
      logit( "et", "write_cta: Can't rename the temporary file.\n" );
   else
      logit( "et", "write_cta: File %s created.\n", filename );
   return;
}


   /*****************************************************************
    *                      WriteTracebuf2Msg()                      *
    *****************************************************************/

void WriteTracebuf2Msg( char *TraceBuf, long recsize, int termFlag )
{
   time_t        currentTime;
   int           numwritten;
   static time_t tempFileStartTime;
   static int    fileOpen = 0;
   static int    first = 1;
   static char   tempFileName[80];   /* Name of temporary spool file */

/* Build the whole path name of the temporary file
   ***********************************************/
   if ( first )
   {
      strcpy( tempFileName, (char *)SpoolDir );
      strcat( tempFileName, "/write_cta.temp" );
      first = 0;
   }

/* We're shutting down
   *******************/
   if ( termFlag && fileOpen )
   {
      fclose( fp );
      RenameEventFile( tempFileName, &tempFileStartTime );
      return;
   }

/* Get the current time
   ********************/
   time( &currentTime );

/* If there is no open file, let's create
   a temporary file in the SpoolDir directory
   ******************************************/
   if ( fileOpen == 0 )
   {
      tempFileStartTime = currentTime;

      if ( (fp = fopen( tempFileName, "wb" )) == NULL )
      {
         logit( "et", "write_cta: Error creating the temporary file.\n" );
         return;
      }
      fileOpen = 1;                         /* Flag the file as open */
   }

/* A temporary file is already open
   ********************************/
   else
   {

/* The message doesn't belong to the current temporary file.
   Close the file and open a new one.
   ********************************************************/
      if ( (currentTime - tempFileStartTime) >= TapeWriteIntSec )
      {
         fclose( fp );
         fileOpen = 0;                      /* Flag the file as closed */

/* Rename the temporary file to its real name
   ******************************************/
         RenameEventFile( tempFileName, &tempFileStartTime );

/* Open a new temporary file
   *************************/
         tempFileStartTime = currentTime;
         if ( (fp = fopen( tempFileName, "wb" )) == NULL )
         {
            logit( "et", "write_cta: Error creating the temporary file.\n" );
            return;
         }
         fileOpen = 1;                     /* Flag the file as open */
      }
   }

/* Write the tracebuf2 message to the output file
   **********************************************/
   numwritten = fwrite( TraceBuf, sizeof(char), recsize, fp );
   if ( numwritten < recsize )
      logit( "et", "write_cta: Error writing to output file.\n" );
   return;
}
