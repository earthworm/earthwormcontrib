#
# This is write_cta's parameter file
#
MyModuleId       MOD_WRITE_CTA  # Module id for this instance of write_cta
RingName         WAVE_RING      # Shared memory ring for input/output
HeartBeatInt     30             # Seconds between heartbeats
MsgRecSize       4096           # Size of largest waveform msg, in bytes
TapeName         /dev/rmt/0n    # Should end in "n" (no rewind)
Changer          /dev/changer   # Enter "none" for Sony autoloader,
                                # "/dev/changer" for Peak autoloader
TapeWriteIntSec  600            # Tape write interval in seconds
Compress         1              # If 1, use UNIX compress; if 0, don't
Class            TS             # Class for mt, mtx, compress, and tar
Priority         0              # Priority for mt, mtx, compress, and tar
#
# Logos of messages to grab from transport ring
# (TYPE_TRACEBUF and TYPE_TRACEBUF2 only)
#
#               Installation       Module
GetEventsFrom   INST_MENLO       MOD_ADSEND_A
GetEventsFrom   INST_MENLO       MOD_ADSEND_B
GetEventsFrom   INST_MENLO       MOD_ADSEND_CH
GetEventsFrom   INST_MENLO       MOD_ADSEND_MMTH
GetEventsFrom   INST_MENLO       MOD_GETDST2
GetEventsFrom   INST_MENLO       MOD_NAQS2EW
GetEventsFrom   INST_MENLO       MOD_NAQS2EW_MM
GetEventsFrom   INST_MENLO       MOD_NAQS2EW_SAT
GetEventsFrom   INST_MENLO       MOD_CONDENSEK2
GetEventsFrom   INST_MENLO       MOD_CONDENSEK2B
GetEventsFrom   INST_MENLO       MOD_REFTEK2EW
GetEventsFrom   INST_UNR         MOD_WILDCARD
GetEventsFrom   INST_CIT         MOD_WILDCARD

# Data is stored in a spool directory before it is written to tape.
# After being written to tape, the spooled data files are erased.
SpoolDir        /home2/spool

