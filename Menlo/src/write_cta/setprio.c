
#include <synch.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/priocntl.h>
#include <sys/tspriocntl.h>
#include <sys/rtpriocntl.h>


 /********************************************************************
  *                            SetPriority()                         *
  *                                                                  *
  *        Set the priorities of all threads in this process.        *
  *              Valid RT priority range is 0 to 59.                 *
  ********************************************************************/

void SetPriority( pid_t pid, char *ClassName, int Priority )
{
   int i;

   union{
      tsparms_t tsp;
      rtparms_t rtp;
      long      clp[PC_CLPARMSZ];
   } u;

   pcinfo_t  cid;
   pcparms_t pcp;
   rtparms_t rtp;

   strcpy( cid.pc_clname, ClassName );
   if ( priocntl( P_PID, (id_t)pid, PC_GETCID, (caddr_t)&cid ) == -1 )
      perror( "write_cta: priocntl getcid" );

   pcp.pc_cid = cid.pc_cid;

   if ( strcmp( ClassName, "TS" ) == 0 )
   {
      u.tsp.ts_uprilim = TS_NOCHANGE;            /* Use the default */
      u.tsp.ts_upri    = Priority;               /* Desired priority */
   }

   if ( strcmp( ClassName, "RT" ) == 0 )
   {
      u.rtp.rt_pri     = Priority;               /* Desired priority */
      u.rtp.rt_tqsecs  = 0;
      u.rtp.rt_tqnsecs = RT_TQDEF;               /* Use default time quantum */
   }
   for ( i = 0; i < PC_CLPARMSZ; i++ )
      pcp.pc_clparms[i] = u.clp[i];

   if ( priocntl( P_PID, (id_t)pid, PC_SETPARMS, (caddr_t)&pcp ) == -1 )
      perror( "write_cta: priocntl setparms" );
}

