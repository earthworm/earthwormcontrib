
/*
       tapeio.h - Header file for tapeio.c
*/

#ifndef TAPEIO_H
#define TAPEIO_H

#include <sys/mtio.h>

/* Function prototypes
   *******************/
int GetMtStatus( char *, char *, int, int * );
int WriteTarFile( char *, char *, char *, int, int * );
int LoadNextTape( char *, char *, int * );
int CompressFile( char *, char *, int, int * );

#endif
