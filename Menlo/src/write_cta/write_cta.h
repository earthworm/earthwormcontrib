
/* Error messages used by write_cta
   ********************************/
#define  ERR_MISSMSG    0           /* Message missed in transport ring */
#define  ERR_TOOBIG     1           /* Retrieved msg too large for buffer */
#define  ERR_NOTRACK    2           /* Msg retrieved; tracking limit exceeded */
#define  ERR_COMPRESS   3           /* Compress error */
#define  ERR_TAR        4           /* Tar error */
#define  ERR_OFFLINE    5           /* Error taking tape offline */
#define  MSG_OFFLINE    6           /* Autoloader tape changed */

void    SendStatus( unsigned char, short, char * );

