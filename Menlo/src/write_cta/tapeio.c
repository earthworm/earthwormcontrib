
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "tapeio.h"

int errno;

/* Declared in write_cta.c
   ***********************/
extern char *tapename;                /* Tape device name */
extern char *fname;                   /* Name of temporary file */
extern int  taperecsize;              /* In bytes */

void SetPriority( pid_t, char *, int );


   /***********************************************************************
    *                            CompressFile()                           *
    *                                                                     *
    *         Compress a file using the Unix compress command.            *
    *                                                                     *
    *  Returns  0 = no error                                              *
    *          -1 = fork error                                            *
    *          -2 = execl error                                           *
    *          -3 = waitpid error                                         *
    *          -4 = compress terminated by signal                         *
    *          -5 = compress stopped by signal                            *
    *          -6 = unknown compress exit status                          *
    ***********************************************************************/

int CompressFile( char *fname,           /* Name of file to compress */
                  char *compressClass,   /* RT or TS */
                  int  compressPrio,     /* Process priority */
                  int  *stat )           /* Status code */
{
   pid_t pid;
   int   status;

/* Fork a child process to run the compress command.
   fork() forks all the threads in a process.
   fork1() forks only the calling thread.
   ************************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                          /* In child process */
         execlp( "compress", "compress", "-v", "-f", fname, NULL );
         return -2;

      default:
         break;
   }

/* Set the class and priority of the compress process
   **************************************************/
   SetPriority( pid, compressClass, compressPrio );

/* Wait for the compress command to complete
   *****************************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( status ) )          /* compress exited */
   {
      *stat = WEXITSTATUS( status );
      return 0;
   }
   else if ( WIFSIGNALED( status ) )   /* compress terminated by signal */
   {
      *stat = WTERMSIG( status );
      return -4;
   }
   else if ( WIFSTOPPED( status ) )    /* compress stopped by signal */
   {
      *stat = WSTOPSIG( status );
      return -5;
   }
   return -6;                         /* Unknown exit status */
}


   /***********************************************************************
    *                            GetMtStatus()                            *
    *                                                                     *
    *                  Get the status of the tape drive.                  *
    *                                                                     *
    *  Returns  0 = no error                                              *
    *          -1 = fork error                                            *
    *          -2 = execl error                                           *
    *          -3 = waitpid error                                         *
    *          -4 = mt terminated by signal                               *
    *          -5 = mt stopped by signal                                  *
    *          -6 = unknown mt exit status                                *
    ***********************************************************************/

int GetMtStatus( char *tapename,      /* Name of tape device */
                 char *mtClass,       /* RT or TS */
                 int  mtPrio,         /* Process priority */
                 int  *stat )         /* Status code */
{
   pid_t pid;
   int   status;

/* Fork a child process to run the mt command
   ******************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                          /* In child process */
         execlp( "mt", "mt", "-f", tapename, "status", NULL );
         return -2;

      default:
         break;
   }

/* Set the class and priority of the mt process
   ********************************************/
   SetPriority( pid, mtClass, mtPrio );

/* Wait for the mt command to complete
   ***********************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( status ) )          /* mt exited */
   {
      *stat = WEXITSTATUS( status );
      return 0;
   }
   else if ( WIFSIGNALED( status ) )   /* mt terminated by signal */
   {
      *stat = WTERMSIG( status );
      return -4;
   }
   else if ( WIFSTOPPED( status ) )    /* mt stopped by signal */
   {
      *stat = WSTOPSIG( status );
      return -5;
   }
   return -6;                          /* Unknown exit status */
}


   /***********************************************************************
    *                            WriteTarFile()                           *
    *                                                                     *
    *         Copies a file to tape using the Unix tar command.           *
    *                                                                     *
    *  Returns  0 = no error                                              *
    *          -1 = fork error                                            *
    *          -2 = execl error                                           *
    *          -3 = waitpid error                                         *
    *          -4 = tar terminated by signal                              *
    *          -5 = tar stopped by signal                                 *
    *          -6 = unknown tar exit status                               *
    ***********************************************************************/

int WriteTarFile( char *fname,         /* Name of file to write */
                  char *tapename,      /* Name of tape device */
                  char *tarClass,      /* RT or TS */
                  int  tarPrio,        /* Process priority */
                  int  *stat )         /* Status code */
{
   pid_t pid;
   int   status;

/* Fork a child process to run the tar command.
   The "e" option means: exit immediately on any
   write error, with a positive status code.
   *********************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;

      case 0:                          /* In child process */
         execlp( "tar", "tar", "cef", tapename, fname, NULL );
         return -2;

      default:
         break;
   }

/* Set the class and priority of the tar process
   *********************************************/
   SetPriority( pid, tarClass, tarPrio );

/* Wait for the tar command to complete
   ************************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED( status ) )          /* tar exited */
   {
      *stat = WEXITSTATUS( status );
      return 0;
   }
   else if ( WIFSIGNALED( status ) )   /* tar terminated by signal */
   {
      *stat = WTERMSIG( status );
      return -4;
   }
   else if ( WIFSTOPPED( status ) )    /* tar stopped by signal */
   {
      *stat = WSTOPSIG( status );
      return -5;
   }
   return -6;                         /* Unknown exit status */
}


   /***********************************************************************
    *                           LoadNextTape()                            *
    *                                                                     *
    *  Take the tape unit offline.  Some Sony autoloaders will then       *
    *  automatically load the next tape into the drive.  Peak 8-Pak       *
    *  autoloaders must be explicitly instructed to load the next tape.   *
    *                                                                     *
    *  Returns  0  = No error.                                            *
    *          -1  = mt fork error.                                       *
    *          -2  = mt command not in search path.                       *
    *          -3  = mt waitpid error.                                    *
    *          -4  = mt error. No tape loaded or drive offline.           *
    *                   Check mstat.                                      *
    *          -5  = mt terminated by signal.                             *
    *          -6  = mt stopped by signal.                                *
    *          -7  = Unknown mt exit status.                              *
    *          -8  = mtx fork error.                                      *
    *          -9  = mtx command not in search path.                      *
    *          -10 = mtx waitpid error.                                   *
    *          -11 = mtx error. Can't load next tape. Check mstat.        *
    *          -12 = mtx terminated by signal.                            *
    *          -13 = mtx stopped by signal.                               *
    *          -14 = Unknown mtx exit status.                             *
    ***********************************************************************/

int LoadNextTape( char *tapename,      /* Name of tape device */
                  char *changer,       /* Name of tape changer or "none" */
                  int  *mstat )        /* Status code */
{
   pid_t pid;
   int   status;

/* Fork a child process to run the mt command
   ******************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -1;
      case 0:                          /* In child process */
         execlp( "mt", "mt", "-f", tapename, "offline", NULL );
         return -2;                    /* mt command not in search path */
      default:
         break;
   }

/* Wait for the mt command to complete
   ***********************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -3;

   if ( WIFEXITED(status) )           /* mt exited */
   {
      *mstat = WEXITSTATUS(status);
      if ( *mstat != 0 ) return -4;   /* No tape loaded or drive offline */
   }
   else if ( WIFSIGNALED(status) )    /* mt terminated by signal */
   {
      *mstat = WTERMSIG(status);
      return -5;
   }
   else if ( WIFSTOPPED(status) )     /* mt stopped by signal */
   {
      *mstat = WSTOPSIG(status);
      return -6;
   }
   else
      return -7;                      /* Unknown mt exit status */

/* If the autoloader doesn't contain a separate
   changer device, we're done.
   ********************************************/
   if ( strcmp( changer, "none" ) == 0 ) return 0;

/* The autoloader contains a separate changer device.
   Invoke the mtx command to load the next tape.
   *************************************************/
   switch( pid = fork1() )
   {
      case -1:
         return -8;
      case 0:                          /* In child process */
         execlp( "mtx", "mtx", "-f", changer, "next", NULL );
         return -9;                    /* mtx command not in search path */
      default:
         break;
   }

/* Wait for the mtx command to complete
   ************************************/
   if ( waitpid( pid, &status, 0 ) == -1 )
      return -10;

   if ( WIFEXITED(status) )            /* mtx exited */
   {
      *mstat = WEXITSTATUS(status);
      if ( *mstat != 0 ) return -11;   /* Error loading next tape */
      return 0;                        /* Success */
   }
   else if ( WIFSIGNALED(status) )     /* mtx terminated by signal */
   {
      *mstat = WTERMSIG(status);
      return -12;
   }
   else if ( WIFSTOPPED(status) )      /* mtx stopped by signal */
   {
      *mstat = WSTOPSIG(status);
      return -13;
   }
   else
      return -14;                      /* Unknown mtx exit status */
}


/* Test program
   ************/
/*
main()
{
   int  mstat = 0;
   char tapename[] = "/dev/rmt/0n";
   char changer[]  = "/dev/changer";

   int rc = LoadNextTape( &tapename[0], &changer[0], &mstat );

   printf( "LoadNextTape(): %d\n", rc );
   printf( "mstat: %d\n", mstat );
   return 0;
}
*/
