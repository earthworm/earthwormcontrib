/***************************************************************************
 * This group of routines is started by the processor manager.             *
 * We're given a copy of the archive message for this event.               *
 *                                                                         *
 *   MainProcess - This is the process controller.                         *
 *       Read config file                                                  *
 *       Read Arkive msg and stuff info in ark structure.                  *
 *       Build table of data of interest                                   *
 *       Retrieve data traces                                              *
 *       Process traces                                                    *
 *       Plot                                                              *
 *       Publish                                                           *
 ***************************************************************************/

#include <platform.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <chron3.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include "mem_circ_queue.h" 
#include <swap.h>
#include <trace_buf.h>
#include <time_ew.h>
#include <decode.h>
/*
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
*/
#include <ws_clientII.h>
#include "gd.h"
#include "gdfontt.h"   /*  6pt      */
#include "gdfonts.h"   /*  7pt      */
#include "gdfontmb.h"  /*  9pt Bold */
#include "gdfontl.h"   /* 13pt      */
#include "gdfontg.h"   /* 10pt Bold */

#include "lasteq.h"

/* Structure for keeping track of buffer of trace data */
DATABUF  pTrace;
GAP *pGap;

/* Functions in this source file 
 *******************************/
int MainProcess(Global *, Arkive *Ark);
void BuildStationPanel(Global *But, int This, double Stime);
void Save_Plot(Global *But, int This);
void Sort_Servers (Global *, double StartTime);
short Build_Axes(Global *But, double Stime, int This);
void Make_Grid(Global *But, int This);
void Pallette(gdImagePtr GIF, long color[]);
int Plot_Trace(Global *But, double *Data, int This, int chan);
short Get_Ark(Arkive *);
void read_hypy2k(Arkive *, char *);
short Build_Table(Global *, Arkive *);
void Sort_Ark (int, double *, int *);

void Make_Time( double *secs, char *string);
short Build_Menu (Global *);
int In_Menu (Global *, char *sta, char *net, double *, double *);
int In_Menu_list (Global *);
int RequestWave(Global *, int, double *, char *, char *, char *, char *, double, double);
int  WSReqBin(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt);
int IsDST(int, int, int);
void Decode_Time( double, TStrct *);
void Encode_Time( double *secs, TStrct *Time);
void date22( double, char *);
void date11( double, char *);
void GetMaxDist(double *dist, double loga, double mag);
double logA0(double dist);
void SetPlot(double, double);
int ixq(double);
int iyq(double);
void Get_Sta_Info(Global *);
int Put_Sta_Info(Global *But);
int Find_Sta_Info(Global *But);
short distaz (double lat1, double lon1, double lat2, double lon2, 
                double *rngkm, double *faz, double *baz);

#define   XLMARGIN   0.7             /* Margin to left of axes                  */
#define   XRMARGIN   1.0             /* Margin to right of axes                 */
#define   YBMARGIN   1.0             /* Margin at bottom of axes                */
double    XLabel;                    /* Width of label space                    */
double    XLMargin;                  /* Margin to left of axes                  */
double    XRMargin;                  /* Margin to right of axes                 */
double    YBMargin = 1.0;            /* Margin at bottom of axes                */
double    YTMargin = 0.9;            /* Margin at top of axes                   */
double    sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's        */
                                     /* time 0 and 1970-01-01 00:00:00.0 GMT    */
int     Debug;
double  Data[MAXTRACELTH];
static double    xsize, ysize;

/********************************************************************
 *  MainProcess is responsible for shepherding one event through    *
 *  the seismogram plotting string.                                 *
 *                                                                  *
 ********************************************************************/

int MainProcess(Global *But, Arkive *Ark)
{
    char    whoami[50], fname[100], tname[100];
    double  big_screen_secs;
    time_t  current_time;
    long    TooLate, next, We_Need[MAXCHANNELS];
    int     i, j, jj, jjj, k, error, ierr, successful, Traces_done, Plots_done;
    FILE    *out;
    
    /* Build the modname for error messages *
     ****************************************/
    sprintf(But->mod,"lasteq");
    sprintf(whoami, " %s: %s: ", But->mod, "MainProcess");
    
    XLabel    = 1.2;  /* Width of label space     */
    XLabel    = 1.4;  /* Width of label space     */
    XLMargin  = 0.7;  /* Margin to left of axes   */
    YBMargin  = 0.2;  /* Margin at bottom of axes */
    YBMargin  = 1.0;  /* Margin at bottom of axes */
    XRMargin  = 1.0;  /* Margin to right of axes  */
    YTMargin  = 0.9;  /* Margin at top of axes    */

    /* Clear out the gap list */
    pTrace.nGaps   = 0;
    
    /* Let's see the message * 
     *************************/
    Debug = But->Debug;
    if (But->Debug >=2) {
        logit("et", "%s startup of event:\n%.80s...\n", whoami, Ark->ArkivMsg);
    }
    
    /* Go get all available information from the arkive msg. 
    ********************************************************/
    Ark->MaxChannels = But->MaxChannels;
    error = Get_Ark(Ark);
    if(error) {
        logit("e", "%s Error <%hd> reading arkive msg.\n", whoami, error);
    }
    if(But->Debug) logit("et", "%s Got the arkive msg for %s.\n", whoami, Ark->EvntIDS);
    
    if(Ark->mag < But->MinSize) {
        if(But->Debug) logit("e", "%s Event %s too small (%5.2f)\n", whoami, Ark->EvntIDS, Ark->mag);
        return 1;
    }
    
    if(Ark->mag > 2.5 && Ark->NumPhs < 15 || 
       Ark->mag > 4.5 && Ark->NumPhs < 30) {
        if(But->Debug) logit("e", "%s Event %s has too few phases (%d) for it's magnitude (%5.2f)\n",
                   whoami, Ark->EvntIDS, Ark->NumPhs, Ark->mag);
        return 1;
    }
    
    Get_Sta_Info(But);
    
    Find_Sta_Info(But);
    
    
    /* Set up the plotting parameters for each plot. 
     ************************************************/

    Build_Menu(But);  /*  Build the current wave server menus */
    
    if(!But->got_a_menu) {
        logit("e", "%s No Menu from %d servers! Just quit.\n", whoami, But->nServer);
        return 1;
    }
    /* Build the To-Do list from all available information. 
     ******************************************************/
    error = Build_Table(But, Ark);
    if(error) {
        if(error>1) logit("e", "%s Error <%hd> building To Do list.\n", whoami, error);
        if(But->NumIndexedSta > 0) {
            logit("e", "        There are %d entries in table.\n", But->NumIndexedSta);
        } else {
            logit("e", "        There are no entries in table.\n");
            for(j=0;j<But->nServer;j++) wsKillMenu(&(But->menu_queue[j]));
            return 1;
        }
    }
    if(But->Debug) logit("e", "%s Got the To Do list.\n", whoami);
    for(i=0;i<But->NumIndexedSta;i++) We_Need[i] = 1;
    for(i=But->NumIndexedSta;i<Ark->MaxChannels;i++) We_Need[i] = 0;
    /*  Process the traces by looping thru the To-Do list. 
    ******************************************************/
 
    next = 0;  /* First Initialize a bunch of stuff. */
    time(&current_time);
    TooLate = current_time + TIMEOUT;
    
        /*  Go get the traces.
        **********************/
    if(But->Debug) logit("e", "%s Processing traces.\n", whoami);
    while(current_time < TooLate && next < But->NumIndexedSta) {
        if(We_Need[next]) {
            k = But->StaIndex[next];
            BuildStationPanel(But, k, Ark->EvntTime);
        }
        next += 1;
        if(next >= But->NumIndexedSta) break;
        time(&current_time);
    }
    if(But->Debug) logit("e", "%s Traces processed: %d %d %d %d\n", 
                   whoami, next, Traces_done, But->NumIndexedSta, (current_time - TooLate));
    
    for(j=0;j<But->nServer;j++) wsKillMenu(&(But->menu_queue[j]));
    
    /* Clear out the gap list */
    pTrace.nGaps = 0;
    
    return 0;
}


/********************************************************************
 * BuildStationPanel does the initial setup and first set of images *
 ********************************************************************/

void BuildStationPanel(Global *But, int This, double Stime)
{
    char    whoami[90], time1[25], time2[25], sip[25], sport[25], sid[50];
    double  tankStarttime, tankEndtime;
    double  StartTime, EndTime, Duration, ZTime, B[8];
    int     i, j, k, jj, successful, server, hour1, hour2, gotdata;
    int     minPerStep, minOfDay;
    TStrct  t0, Time1, Time2;  
    
    int		ii, order, lx;
    double	dw, aflo, afhi, f, pi, percent;

    sprintf(whoami, " %s: %s: ", But->mod, "BuildStationPanel");
    
    But->GifImage = 0;
    But->xsize = XLMARGIN + XRMARGIN + But->axexmax;
    But->ysize = YBMARGIN + YTMargin + But->axeymax*But->Sta[This].Ninst;
    SetPlot(But->xsize, But->ysize);
    
    StartTime = But->Sta[This].Stime;
    date22 (StartTime, time1);
    logit("e", "%s Requested Start Time: %s. \n", whoami, time1);
    
	if(But->Debug) logit("e", "%s Plotting data for: %s %s with %d channels. \n", 
		whoami, But->Sta[This].Site, But->Sta[This].Net, But->Sta[This].Ninst);
    
    gotdata = 0;
    for(i=0;i<But->Sta[This].Ninst; i++) {
		
		StartTime = But->Sta[This].Stime;
		strcpy( But->Site , But->Sta[This].Site );
		strcpy( But->Comp , But->Sta[This].SCNL[i].Comp );
		strcpy( But->Net  , But->Sta[This].Net );
		strcpy( But->Loc  , But->Sta[This].SCNL[i].Loc );
	    sprintf(But->SCNtxt, "%s %s %s %s",      But->Site, But->Comp, But->Net, But->Loc);

	    Put_Sta_Info(But);
		    if(But->Sta[This].SCNL[i].Sens_unit<=1) But->Scaler = But->Scale*1000.0;
		    if(But->Sta[This].SCNL[i].Sens_unit==2) But->Scaler = But->Scale*10000.0;
		    if(But->Sta[This].SCNL[i].Sens_unit==3) But->Scaler = But->Scale*10.0;
	    
	    if(In_Menu_list(But)) {
	        Sort_Servers(But, StartTime);
	        if(But->nentries <= 0) goto quit;
	                    
	        k = But->index[0];
	        tankStarttime = But->TStime[k];
	        tankEndtime   = But->TEtime[k];
	        
	        if(But->Debug) {
	            date22 (StartTime, time1);
	            date22 (tankStarttime, time1);
	            date22 (tankEndtime,   time2);
	            date22 (But->TStime[k], time1);
	            date22 (But->TEtime[k], time2);
	            strcpy(sip,   But->wsIp[k]);
	            strcpy(sport, But->wsPort[k]);
	            strcpy(sid,   But->wsComment[k]);
	            logit("e", "%s Got menu for: %s. \n", whoami, But->SCNtxt);
	            for(j=0;j<But->nentries;j++) {
	                k = But->index[j];
	                date22 (But->TStime[k], time1);
	                date22 (But->TEtime[k], time2);
	                logit("e", "            %d %d %s %s %s %s <%s>\n", 
	                      j, k, time1, time2, But->wsIp[k], But->wsPort[k], But->wsComment[k]);
	            }
	        }
	        
	        Decode_Time(StartTime, &t0);
	        t0.Sec = 0;
	        
	        Duration = But->TotMins*60;
	        EndTime = StartTime + Duration;      
	        
	        if(i == 0) {
		        Encode_Time( &ZTime, &t0);
		        if(Build_Axes(But, ZTime, This)) {
		            logit("e", "%s Build_axes croaked for: %s. \n", 
		                     whoami, But->SCNtxt);
		            goto quit;
		        }
	        }
	        
			/* Try to get some data
			***********************/
			for(jj=0;jj<But->nentries;jj++) {
				server = But->index[jj];
				successful = RequestWave(But, server, Data, But->Site, 
					But->Comp, But->Net, But->Loc, StartTime, Duration);
				if(successful == 1) {   /*    Plot this trace to memory. */
					break;
				}
				else if(successful == 2) {
					if(But->Debug) {
						logit("e", "%s Data for: %s. RequestWave problem 2\n", 
								whoami, But->SCNtxt);
					}
					continue;
				}
				else if(successful == 3) {   /* Gap in data */
					if(But->Debug) {
						logit("e", "%s Data for: %s. RequestWave problem 3\n", 
								whoami, But->SCNtxt);
					}
				}
			}
			
			if(successful == 1) {   /*    Plot this trace to memory. */
			
				But->Sta[This].SCNL[i].range = But->Max - But->Min;
				date22 (StartTime, time1);
				
				gotdata = 1;
				Plot_Trace(But, Data, This, i);
			}
	            	        
	        
	    } else {
	        logit("e", "%s %s not in menu.\n", whoami, But->SCNtxt);
	    }
    }
	if(But->Debug) 
		logit("e", "%s Finish up: %s. %d %d\n", 
				whoami, But->SCNtxt, But->GifImage, gotdata);
    if(But->GifImage!=0) {
	    if(gotdata) Save_Plot(But, This);
		gdImageDestroy(But->GifImage);
    }
quit:        
    sleep_ew(200);
}


/*********************************************************************
 *   Save_Plot()                                                     *
 *    Saves the current version of the GIF image and ships it out.   *
 *********************************************************************/

void Save_Plot(Global *But, int This)
{
    char    tname[175], string[200], whoami[90];
    FILE    *out;
    int     j, ierr, retry;
    
    sprintf(whoami, " %s: %s: ", But->mod, "Save_Plot");
    Make_Grid(But, This);
    /* Make the GIF file. *
     **********************/        
    sprintf(But->TmpName, "%s_%s", But->Sta[This].Site, But->Sta[This].Net);
    sprintf(But->GifName, "%s%s", But->TmpName, ".gif");
    sprintf(But->LocalGif, "%s%s.%ld", But->loctarget, But->TmpName, But->pid);
    out = fopen(But->LocalGif, "wb");
    if(out == 0L) {
        logit("e", "%s Unable to write GIF File: %s\n", whoami, But->LocalGif); 
    } else {
        gdImageGif(But->GifImage, out);
        fclose(out);
        sprintf(tname,  "%s%s", But->loctarget, But->GifName );
        ierr = rename(But->LocalGif, tname);
        if(ierr) {
            if(But->Debug) logit("e", "%s Error Renaming GIF File %d\n", whoami, ierr);
        } else {
            if(But->Debug) logit("e", "%s GIF File %s written.\n", whoami, tname);
        } 
    }
    
    /* Make the Text file. *
     **********************/        
    sprintf(But->TmpName, "%s_%s", But->Sta[This].Site, But->Sta[This].Net);
    sprintf(But->GifName, "%s%s", But->TmpName, ".txt");
    sprintf(But->LocalGif, "%s%s.%ld", But->loctarget, But->TmpName, But->pid+1);
    out = fopen(But->LocalGif, "wb");
    if(out == 0L) {
        logit("e", "%s Unable to write GIF File: %s\n", whoami, But->LocalGif); 
    } else {
        fprintf(out, "eventid=%s\n", But->Ark.EvntIDS);
        fprintf(out, "lat=%f\n", But->Ark.EvntLat);
        fprintf(out, "lon=%f\n", But->Ark.EvntLon);
        fclose(out);
        sprintf(tname,  "%s%s", But->loctarget, But->GifName );
        ierr = rename(But->LocalGif, tname);
        if(ierr) {
            if(But->Debug) logit("e", "%s Error Renaming Text File %d\n", whoami, ierr);
        } else {
            if(But->Debug) logit("e", "%s Text File %s written.\n", whoami, tname);
        } 
    }
}


/*************************************************************************
 *   Sort_Servers                                                        *
 *      From the table of waveservers containing data for the current    *
 *      SCN, the table is re-sorted to provide an intelligent order of   *
 *      search for the data.                                             *
 *                                                                       *
 *      The strategy is to start by dividing the possible waveservers    *
 *      into those which contain the requested StartTime and those which *
 *      don't.  Those which do are retained in the order specified in    *
 *      the config file allowing us to specify a preference for certain  *
 *      waveservers.  Those waveservers which do not contain the         *
 *      requested StartTime are sorted such that the possible data       *
 *      retrieved is maximized.                                          *
 *************************************************************************/

void Sort_Servers (Global *But, double StartTime)
{
    char    whoami[50], c22[25];
    double  tdiff[MAX_WAVESERVERS*2];
    int     j, k, jj, last_jj, kk, hold, index[MAX_WAVESERVERS*2];
    
    sprintf(whoami, " %s: %s: ", But->mod, "Sort_Servers");
        /* Throw out servers with data too old.
    j = 0;
    while(j<But->nentries) {
        k = But->index[j];
        if(StartTime > But->TEtime[k]) {
            if(But->Debug) {
                date22( StartTime, c22);
                logit("e","%s %d %d  %s", whoami, j, k, c22);
                    logit("e", " %s %s <%s>\n", 
                          But->wsIp[k], But->wsPort[k], But->wsComment[k]);
                date22( But->TEtime[k], c22);
                logit("e","ends at: %s rejected.\n", c22);
            }
            But->inmenu[k] = 0;    
            But->nentries -= 1;
            for(jj=j;jj<But->nentries;jj++) {
                But->index[jj] = But->index[jj+1];
            }
        } else j++;
    } */
    if(But->nentries <= 1) return;  /* nothing to sort */
            
    /* Calculate time differences between StartTime needed and tankStartTime */
    /* And copy positive values to the top of the list in the order given    */
    jj = 0;
    for(j=0;j<But->nentries;j++) {
        k = index[j] = But->index[j];
        tdiff[k] = StartTime - But->TStime[k];
        if(tdiff[k]>=0) {
            But->index[jj++] = index[j];
            tdiff[k] = -65000000; /* two years should be enough of a flag */
        }
    }
    last_jj = jj;
    
    /* Sort the index list copy in descending order */
    j = 0;
    do {
        k = index[j];
        for(jj=j+1;jj<But->nentries;jj++) {
            kk = index[jj];
            if(tdiff[kk]>tdiff[k]) {
                hold = index[j];
                index[j] = index[jj];
                index[jj] = hold;
            }
            k = index[j];
        }
        j += 1;
    } while(j < But->nentries);
    
    /* Then transfer the negatives */
    for(j=last_jj,k=0;j<But->nentries;j++,k++) {
        But->index[j] = index[k];
    }
}    


/********************************************************************
 *    Build_Axes constructs the axes for the plot by drawing the    *
 *    GIF image in memory.                                          *
 *                                                                  *
 ********************************************************************/
short Build_Axes(Global *But, double Stime, int This)
{
    char    whoami[90], c22[30], cstr[150], LocalTimeID[4];
    double  atime, trace_size, tsize, yp, Scale;
    int     mins, CurrentHour, LocalTime;
    int     ix, iy, i, j, jj, k, kk;
    int     xgpix, ygpix, LocalSecs, sec;
    long    black, must_create;
    FILE    *in;
    TStrct  Time1;
    gdImagePtr    im_in;

    sprintf(whoami, " %s: %s: ", But->mod, "Build_Axes");
    i = 0;
    mins  = But->mins;
    Scale = But->Scale;
    LocalTime = But->LocalTime;
    LocalSecs = LocalTime*60*60;
    strcpy(LocalTimeID, But->LocalTimeID);

    Decode_Time( Stime, &Time1);
    Decode_Time( But->Sta[This].Stime, &Time1);
    if(IsDST(Time1.Year,Time1.Month,Time1.Day) && But->UseDST) {
        LocalTime = LocalTime + 1;
        LocalTimeID[1] = 'D';
    }
    
    But->GifImage = 0L;
                 
    xgpix = 72.0*But->xsize + 8;
    ygpix = 72.0*But->ysize + 8;
    CurrentHour = Time1.Hour;

    But->GifImage = gdImageCreate(xgpix, ygpix);
    if(But->GifImage==0) {
        logit("e", "%s Not enough memory! Reduce size of image or increase memory.\n\n", whoami);
        return 2;
    }
    if(But->GifImage->sx != xgpix) {
        logit("e", "%s Not enough memory for entire image! Reduce size of image or increase memory.\n", 
             whoami);
        return 2;
    }
    Pallette(But->GifImage, But->gcolor);

    /* Plot the frame *
     ******************/
    Make_Grid(But, This);

    black = But->gcolor[BLACK];
    trace_size =  But->axeymax; /* height of one trace [Data] */

    /* Put in the date/time labels * 
     **************************/
	k = CurrentHour + LocalTime;
	if(k <  0) k += 24;
	if(k > 23) k -= 24;
	kk = CurrentHour;
	if(kk <  0) kk += 24;
	if(kk > 23) kk -= 24;
	
	yp =  0.0;
	iy = iyq(yp) - 15;  
	
	ix = ixq(-0.5); 
	sec = (int)Time1.Sec;
	sprintf(cstr, "%02d:%02d:%02d", kk, Time1.Min, sec);
	gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);
	
	ix = ixq(But->axexmax + 0.05); 
	if (mins < 60) sprintf(cstr, "%02d:%02d:%02d", k, Time1.Min+mins, sec);
	else {
		k += 1;
		if(k > 23) k -= 24;
		sprintf(cstr, "%02d:00", k);
	}
	gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);
    
    iy = 72.0 * YTMargin-45;
    ix = ixq(But->axexmax + 0.05);
    gdImageString(But->GifImage, gdFontMediumBold, ix, iy, LocalTimeID, black);  
    ix = ixq(-0.5);
    sprintf(cstr, "UTC") ;
    gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);  
      
    /* Write labels with the Date * 
     ******************************/
    atime =  Stime+LocalSecs;
    iy = 72.0 * YTMargin-30;
    
    Decode_Time( atime+60*mins, &Time1);
    Encode_Time( &Time1.Time, &Time1);
    date22 (Time1.Time, c22);
    sprintf(cstr, "%.11s", c22) ;
    gdImageString(But->GifImage, gdFontMediumBold, ixq(But->axexmax + 0.05), iy, cstr, black);    

    atime =  Stime;
    Decode_Time( atime, &Time1);
    Time1.Hour = Time1.Min = Time1.Sec = 0;
    Encode_Time( &Time1.Time, &Time1);
    date22 (Time1.Time, c22);
    sprintf(cstr, "%.11s", c22) ;
    gdImageString(But->GifImage, gdFontMediumBold, ixq(-0.5), iy, cstr, black);    

    /* Write label with the Descriptive name of site * 
     ***********************************/
    ix = ixq(0.0);
    iy = 72.0 * YTMargin-45;
    if(But->Comment[0]!=0) {
        sprintf(cstr, "(%s) ", But->Comment);
        ix = ixq(But->axexmax/2) - 6*(int)strlen(cstr)/2;
        gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);
        iy -= 15;
    }
    sprintf(cstr, "%s_%s ", But->Sta[This].Site, But->Sta[This].Net);
    ix = ixq(But->axexmax/2) - 6*(int)strlen(cstr)/2;
    gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);
    
    sprintf(cstr, "Event %s   Md =%5.2f    %.2d/%.2d/%.4d %.2d:%.2d:%6.3f UTC   Distance =%6.2f km", 
		But->Ark.EvntIDS, But->Ark.mag, 
    	But->Ark.EvntMonth, But->Ark.EvntDay, But->Ark.EvntYear, But->Ark.EvntHour, 
    	But->Ark.EvntMin, But->Ark.EvntSec, But->Sta[This].Dist);
    ix = ixq(But->axexmax/2) - 6*(int)strlen(cstr)/2;
    iy = 72.0 * YTMargin-15;
    gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);
    
    return 0;
}


/********************************************************************
 *    Make_Grid constructs the grid overlayed on the plot.          *
 *                                                                  *
 ********************************************************************/
void Make_Grid(Global *But, int This)
{
    char    string[150];
    double  in_sec, tsize;
    long    isec, jsec, xp, yp0, yp1, yp2, j, k, black, ixmax;
    long    major_incr, minor_incr, tiny_incr;
    int     inx[]={0,1,2,2,2,3,4,4,4,4,5}, iny[]={2,3,2,1,0,1,1,0,2,3,3};
    TStrct  Time1;

    /* Plot the frame *
     ******************/
    black = But->gcolor[BLACK];
    gdImageRectangle( But->GifImage, ixq(0.0), iyq(But->axeymax*But->Sta[This].Ninst), ixq(But->axexmax),  iyq(0.0),  black);

    /* Make the x-axis ticks * 
     *************************/
    Decode_Time( But->Sta[This].Stime, &Time1);
    ixmax = But->mins*60;
    in_sec = But->axexmax / ixmax;
    
    tiny_incr = (in_sec < 0.05)?  0:1;        /* turn on `second' marks */
    major_incr = 60;
    minor_incr = 10;
    
    yp0 = (But->ysize - YBMARGIN)*72;   /* Bottom axis line */
    yp1 = yp0 + 0.15*72;               /* Space below axis for label */
    jsec = 0;
    for(isec=0;isec<=ixmax;isec++) {
        jsec = Time1.Sec + isec;
        if(jsec >= 60) jsec -= 60;
        xp = ixq(isec*in_sec);
        if ((div(jsec, major_incr)).rem == 0) {
            tsize = 0.15;    /* major ticks */
            sprintf(string, "%d", jsec / 60);
            k = (int)strlen(string) * 3;
            gdImageString(But->GifImage, gdFontMediumBold, xp-k, yp1, string, black);
            gdImageLine(But->GifImage, xp, iyq(0.0), xp, iyq(But->axeymax*But->Sta[This].Ninst), But->gcolor[GREY]); /* make the minute mark */
        }
        else if ((div(jsec, minor_incr)).rem == 0)  {
            tsize = 0.10;    /* minor ticks */
            sprintf(string, "%d", jsec);
            k = (int)strlen(string) * 3;
            gdImageString(But->GifImage, gdFontMediumBold, xp-k, yp1, string, black);
        }
        else if(tiny_incr)
            tsize = 0.05;    /*  tiny ticks */
        else
            tsize = 0.0;     /*  no ticks   */
        
        if(tsize > 0.0) {
            yp2 = yp0 + tsize*72;
            gdImageLine(But->GifImage, xp, yp0, xp, yp2, black); /* make the tick */
        }
    }
    strcpy(string, "Time (seconds)");
    yp1 = yp0 + 0.3*72;           /* Space below axis for label */
    gdImageString(But->GifImage, gdFontMediumBold, ixq(But->axexmax/2.0 - 0.5), yp1, string, black);

    /* Initial it *
     **************/
    j = But->ysize*72 - 5;
    for(k=0;k<11;k++) gdImageSetPixel(But->GifImage, inx[k]+2, j+iny[k], black);
}


/*******************************************************************************
 *    Pallette defines the pallete to be used for plotting.                    *
 *     PALCOLORS colors are defined.                                           *
 *                                                                             *
 *******************************************************************************/

void Pallette(gdImagePtr GIF, long color[])
{
    color[WHITE]  = gdImageColorAllocate(GIF, 255, 255, 255);
    color[BLACK]  = gdImageColorAllocate(GIF, 0,     0,   0);
    color[RED]    = gdImageColorAllocate(GIF, 255,   0,   0);
    color[BLUE]   = gdImageColorAllocate(GIF, 0,     0, 255);
    color[GREEN]  = gdImageColorAllocate(GIF, 0,   105,   0);
    color[GREY]   = gdImageColorAllocate(GIF, 125, 125, 125);
    color[YELLOW] = gdImageColorAllocate(GIF, 125, 125,   0);
    color[TURQ]   = gdImageColorAllocate(GIF, 0,   255, 255);
    color[PURPLE] = gdImageColorAllocate(GIF, 200,   0, 200);    
    
    gdImageColorTransparent(GIF, -1);
}

/*******************************************************************************
 *    Plot_Trace plots an individual trace (Data)  and stuffs it into          *
 *     the GIF image in memory.                                                *
 *                                                                             *
 *******************************************************************************/

int Plot_Trace(Global *But, double *Data, int This, int chan)
{
    char    whoami[90], string[160];
    double  x0, x, y, xinc, samp_pix, tsize;
    double  in_sec, xsf, ycenter, sf, mpts, rms, value, trace_size;
    double   max, min, fudge, Sens_gain;
    int     lastx, lasty, decimation, acquired, gap0, gap1;
    int     i, j, jj, k, ix, iy, LineNumber, mins;
    long    black, color, trace_clr;

    sprintf(whoami, " %s: %s: ", But->mod, "Plot_Trace");
    i = 0;
    mins = But->TotMins;
    
    Sens_gain = But->Sta[This].SCNL[chan].Sens_gain;
    
    LineNumber = chan;
    
    trace_size =  But->axeymax; /* height of one trace [Data] */
    ycenter =  trace_size*LineNumber + 0.5*trace_size;
    if(ycenter > But->axeymax*But->Sta[This].Ninst) {
        if(But->Debug) {
            logit("e", "%s %s. ycenter too big: %f \n", 
                whoami, But->SCNtxt, ycenter);
        }   
        return 1;
    }
    
    in_sec   = But->axexmax / (mins*60.0);
    samp_pix = But->samp_sec / in_sec / 72.0;
    decimation = 1;
    if(But->DeciFlag <= 0) 
        decimation = ((int)samp_pix/4 < 1)? 1:(int)samp_pix/4;
    else 
        decimation = But->DeciFlag;
    xinc = decimation * in_sec / But->samp_sec;  /* decimation */
    xsf  = in_sec / But->samp_sec;              /* inches/sample */

    /* Label the component *
     ***********************/
    black = But->gcolor[BLACK];
    sprintf(string, "%s %s ", But->Sta[This].SCNL[chan].Comp, But->Sta[This].SCNL[chan].Loc);
    ix = ixq(But->axexmax/2) - 6*(int)strlen(string)/2;
    ix = 5;
    iy = iyq(ycenter) - 3;
    gdImageString(But->GifImage, gdFontMediumBold, ix, iy, string, black);
    
    /* Keep the traces on-scale *
     ****************************/
    sf = trace_size*But->Scaler;
    
    max = min= Data[0];
    acquired = 0;
    for(j=0;j<But->Npts;j+=decimation) {
        if(Data[j] != 919191) {
            if(acquired) {
	            if ( Data[j] >  max ) max =  Data[j];
	            if ( Data[j] <  min ) min =  Data[j];
            } else {
			    max = min= Data[j];
			    acquired = 1;
            }
        }
    } 
    
    value = (max)/But->Sta[This].SCNL[chan].sensitivity;  /* convert data to zero-mean cm/s */
    value = value*sf;  /* convert data to inches */
    max =  value;
    
    value = (min)/But->Sta[This].SCNL[chan].sensitivity;  /* convert data to zero-mean cm/s */
    value = value*sf;  /* convert data to inches */
    min = value;
    
    fudge = 1.0;
    do {
    	i = 0;
        if ( max >  (trace_size * fudge)/3 ) {
        	i = 1;
        	fudge *=2;
        }
    } while (i);
    do {
    	i = 0;
        if ( min < -(trace_size * fudge)/3 ) {
        	i = 1;
        	fudge *=2;
        }
    } while (i);
    color = fudge == 1.0? But->gcolor[BLACK]:But->gcolor[RED];
    color = But->gcolor[BLACK];
    
    
    /* Measure it *
     **************/
    tsize = 1.0/(But->Scaler);
    tsize /= 10;
    tsize *= fudge;
    /**/
    sf /= fudge;
    
    sf *= 2.0;
    tsize /= 2.0;
    
    strcpy(string, "");
    if(But->Sens_unit<=1) {
        sprintf(string, " = %f microvolts (sensor output)", tsize*Sens_gain*1000000.0) ;
    } 
    if(But->Sens_unit==2) {
        sprintf(string, " = %f cm/sec", tsize) ;
    } 
    if(But->Sens_unit==3) {
        sprintf(string, " = %f cm/sec/sec = %f %%g", tsize, tsize*100.0/978) ;
    } 
    ix = 60;
    iy = But->ysize*72;
    iy = iyq(ycenter) - 10;
    iy = iyq(trace_size*(LineNumber+1)) - 10;
    jj = trace_size*72;
    jj /= 10;
    if((int)strlen(string)!=0) {
	    gdImageLine(But->GifImage, ix-2, iy,    ix+2, iy,    black);
	    gdImageLine(But->GifImage, ix-2, iy-jj, ix+2, iy-jj, black);
	    gdImageLine(But->GifImage, ix,   iy,    ix,   iy-jj, black);
	    gdImageString(But->GifImage, gdFontSmall, ix+5, iy-10, string, color);  
    } 
    

    /* Plot the trace *
     ******************/
    if(But->Debug) {
        logit("e", "%s %s. counts/unit: %f in/unit: %f \n", 
            whoami, But->SCNtxt, But->Sta[This].SCNL[chan].sensitivity, sf);
    }   
    x0 = x = 0.0;
    
    trace_clr = But->gcolor[BLACK];
    k = div(LineNumber, 4).rem + 1;
    trace_clr = But->gcolor[k];
    pGap = pTrace.gapList;
    acquired = 0;
    for(j=0;j<But->Npts;j+=decimation) {
        x = x0 + j*xsf;
        if (x > But->axexmax) break;
        
        gap0 = (i >= pTrace.nGaps)? pTrace.nRaw:pGap->firstSamp;
        gap1 = (i >= pTrace.nGaps)? pTrace.nRaw:pGap->lastSamp;
        if(j > gap1) {
	        if(But->Debug) 
	            logit("e", "PlotTrace: Gap detected: %d %d\n", gap0, gap1);
            pGap = pGap->next;
            if(pGap == (GAP *)NULL) i = pTrace.nGaps;
            i += 1;
            gap0 = (i >= pTrace.nGaps)? pTrace.nRaw:pGap->firstSamp;
            gap1 = (i >= pTrace.nGaps)? pTrace.nRaw:pGap->lastSamp;
        }
        if(j < gap0) {
            value = (Data[j])/But->Sta[This].SCNL[chan].sensitivity;  /* convert data to zero-mean cm/s */
            value = value*sf;  /* convert data to inches */
            y = ycenter + value;
            ix = ixq(x);    iy = iyq(y);
            if(acquired) {
                gdImageLine(But->GifImage, ix, iy, lastx, lasty, trace_clr);
            }
            lastx = ix;  lasty = iy;
            acquired = 1;
        }
        else {
            acquired = 0;  
        }
    } 
    return 0;
}


/********************************************************************
 *    Get_Ark is responsible for reading the archive msg.           *
 *                                                                  *
 *    In this case we are only interested in the time, loc, & mag   *
 *                                                                  *
 ********************************************************************/

short Get_Ark(Arkive *Ark)
{
    char    *in;            /* working pointer to archive message    */
    char    line[MAX_STR];  /* to store lines from msg               */
    
  /* Initialize some stuff
   ***********************/
    in     = Ark->ArkivMsg;
    
   /* Read first data line from arkive; process it.
    ***********************************************/
    
    if ( sscanf( in, "%[^\n]", line ) != 1 )  return( -1 );

    /* Process the hypocenter card (1st line of msg) 
     ***********************************************/
    read_hypy2k(Ark, line );
    
    return(0);
}


/**************************************************************
 * read_hypy2k() reads the hypocenter line from an archive msg   *
 **************************************************************/
/*------------------------------------------------------------------------
 Sample hypoinverse archive summary line and its shadow.  The summary line
 may be up to 188 characters long.  Its full format is described in 
 documentation (shadow.doc) by Fred Klein.
           10        20        30        40        50        60        70        80        90        100       110       120       130       140       
0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
9701151845332336  148120 3703  605 0 19104  5  37 8971 15222513 13518SLA  57 * 0 132 14424  0125  0 27PMMTWWWD 26X                 1007557D175125Z114  8
200402031604110540 1171123 4475 2310  0  8179  3  17 3613 24316770 101315MEN  59    0 236  95 30   0 163  0 14MEN WW D 31X   0  0L  0  0  40152715D315 163Z  0   0  
  ------------------------------------------------------------------------*/

void read_hypy2k(Arkive *Ark,  char *hyp)
{
    char   datestr[20], subname[] = "read_hypy2k";
    int    i, j, io;
    float  deg, min;

    Ark->EvntYear  =  (short)DECODE( hyp+0,  4, atof );         /* year of origin time     */
    Ark->EvntMonth =  (short)DECODE( hyp+4,  2, atof );         /* month of origin time    */
    Ark->EvntDay   =  (short)DECODE( hyp+6,  2, atof );         /* day of origin time      */
    Ark->EvntHour  =  (short)DECODE( hyp+8,  2, atof );         /* hour of origin time     */
    Ark->EvntMin   =  (short)DECODE( hyp+10, 2, atof );         /* minute of origin time   */
    Ark->EvntSec   = (float)((DECODE( hyp+12, 4, atof ))/100.0);  /* seconds of origin time  */
    strncpy( datestr, hyp, 12 );
    datestr[12] = '\0';
    strcat ( datestr, "00.00" );                         /* origin time without seconds */
    if(epochsec17(&(Ark->EvntTime), datestr) ) {
       logit("", "%s: Error converting origin time: %s\n", subname, datestr );
    }
    Ark->EvntTime += (double)Ark->EvntSec;               /* Time of Event (Julian Sec)  */

    deg = (float)DECODE( hyp+16, 2, atof );
    min = (float)((DECODE( hyp+19, 4, atof ))/100.);
    Ark->EvntLat = (float)(deg + min/60.);                       /* Latitude of Event            */
    if( hyp[18]=='S' || hyp[18]=='s' ) Ark->EvntLat = -Ark->EvntLat;

    deg =  (float)DECODE( hyp+23, 3, atof );
    min = (float)((DECODE( hyp+27, 4, atof ))/100.);
    Ark->EvntLon = (float)(deg + min/60.);                       /* Longitude of Event           */
    if( hyp[26]=='W' || hyp[26]=='w' || hyp[26]==' ' ) Ark->EvntLon = -Ark->EvntLon;

    Ark->EvntDepth = (float)((DECODE( hyp+31, 5, atof ))/100.);  /* Depth of Event               */

    Ark->mag       = (float)((DECODE( hyp+70, 3, atof ))/100.);  /* Coda duration mag                    */  
 
    Ark->NumPhs    = (long)(DECODE( hyp+39, 3, atof ));  /* # phases used in solution            */  
    Ark->NumPhs    = (long)(DECODE( hyp+118, 3, atof ));  /* # phases used in solution            */  
 
    strcpy(Ark->EvntIDS, "No_ID");
    for(i=136,j=0;i<146;i++) if(hyp[i]!=' ') {j = i;break;}
    if(j) strncpy(Ark->EvntIDS, &hyp[j], 146-j);
    Ark->EvntIDS[146-j] = 0;
    io = sscanf(Ark->EvntIDS, "%d", & Ark->EvntID);
    if(io != 1) {
        logit("e", "%s: io:%d EvntID: <%s> interpreted as: %d\n", 
            subname, io, Ark->EvntIDS, Ark->EvntID);
    }
}


/********************************************************************
 *    Build_Table is responsible for setting up the ToDo list.      *
 *                                                                  *
 ********************************************************************/

short Build_Table(Global *But, Arkive *Ark)
{
    char    whoami[50];
    int     i, k, error, isec;
    double  offset[MAXCHANNELS];
    double  dist, lat, lon, dlat, dlon, faz, baz, t0, t1, Stime, rv;
    TStrct  Time1;  
    
    sprintf(whoami, " %s: %s: ", But->mod, "Build_Table");
    error = 0;
        
    GetMaxDist(&But->MaxDistance, But->MinLogA, Ark->mag);
	if(But->Debug) logit("e", "%s Max Distance = %f\n", whoami, But->MaxDistance);
    
    But->NumIndexedSta= 0;
    for(i=0;i<But->Nsta;i++)  But->StaIndex[i] = 0;
    
  /* Build the list of stations from the station list */
	lat = Ark->EvntLat;    lon = Ark->EvntLon;
	for(i=0;i<But->Nsta;i++) {
		if( But->NumIndexedSta >= MAXCHANNELS ) {
			logit("e", 
				"%s Site table full; cannot load entire file.\n", whoami );
			logit("e", 
				"%s Use <maxsite> command to increase table size; exiting!\n", whoami );
			break;
		} 
		else {
			dlat = But->Sta[i].Lat;
			dlon = But->Sta[i].Lon;
			distaz(lat, lon, dlat, dlon, &dist, &faz, &baz);
			if(But->Debug) logit("e", "%s %s_%s %f.\n", whoami, But->Sta[i].Site, But->Sta[i].Net, dist);
			But->Sta[i].logA = Ark->mag - 3.0 - log10(dist/100.0) - 0.00301*(dist-100.0);
			if(dist < But->MaxDistance) {
				But->Sta[i].Dist = dist;
			
				if(In_Menu(But, But->Sta[i].Site, But->Sta[i].Net, &t0, &t1)) {    
					
					rv = (dist > 150.0)? 8.0:6.0;
					Stime = Ark->EvntTime + dist/rv;   /* Earliest time needed. */
					Decode_Time(Stime, &Time1);
					isec = Time1.Sec;
					Time1.Sec = 10.0*(isec/10 - 1);
					Encode_Time( &Stime, &Time1);    /* StartTime modulo 10 */
					
					if(But->Debug) logit("e", "%s %s_%s %f %f %f.\n", whoami, But->Sta[i].Site, But->Sta[i].Net, t0, Stime, t1);
					if(Stime > t0 && Stime < t1) {
						But->Sta[i].Dist  = dist;
						But->Sta[i].Stime = Stime;
						offset[But->NumIndexedSta] = dist;
						But->StaIndex[But->NumIndexedSta++] = i;
					}
				} else {
					if(But->Debug) logit("e", "%s %s_%s not in menu.\n", whoami, But->Sta[i].Site, But->Sta[i].Net);
				} 
			}
		}
	}
    
    /* Go get the needed info *
     **************************/
    if(But->NumIndexedSta==0) error = 1;
    
    if(But->NumIndexedSta>1) Sort_Ark (But->NumIndexedSta, offset, &But->StaIndex[0]);
   
    if(But->Debug) {
        logit("e", "%s The following %d requested traces are available:\n Site:  Net:  Comp:   Dist:      Picked:\n", 
              whoami, But->NumIndexedSta);
        for(i=0;i<(But->NumIndexedSta);i++)  {
            k = But->StaIndex[i];
            logit("e", " %s    %s           %f  %f \n", 
                But->Sta[k].Site, But->Sta[k].Net,  
                But->Sta[k].Dist, But->Sta[k].logA);
        }
        logit("e", "%s Md: %f\n", whoami, Ark->mag);
    }
    return error;
}

/*************************************************************************
 *   void Sort_Ark (n,dist,indx)                                         *
 *   --THIS is A VERSION OF THE HEAPSORT SUBROUTINE FROM THE NUMERICAL   *
 *     RECIPES BOOK. dist is REARRANGED IN ASCENDING ORDER &             *
 *     indx is PUT IN THE SAME ORDER AS dist.                            *
 *   long        n       !THE NUMBER OF VALUES OF dist TO BE SORTED      *
 *   FLOAT       dist(n) !SORT THIS ARRAY IN ASCENDING ORDER             *
 *   long        indx(n) !PASSIVELY REARRANGE THIS ARRAY TOO             *
 *************************************************************************/

void Sort_Ark (int n, double *dist, int *indx)
{
    long    indxh, ii, ir, i, j;
    double  disth;
    
    ii = n/2+1;    ir = n;
    
    while(1) {    
        if (ii > 1) {
            ii -= 1;
            disth      = dist[ii-1];   indxh      = indx[ii-1];
        }
        else {
            disth      = dist[ir-1];   indxh      = indx[ir-1];
            dist[ir-1] = dist[0];      indx[ir-1] = indx[0];
            ir -= 1;
            if(ir == 1) {
                dist[0] = disth;       indx[0]    = indxh;
                return;
            }
        }
        i = ii;   j = ii+ii;
        
        while (j <= ir) {
            if (j < ir && dist[j-1] < dist[j]) j += 1;
            
            if (disth < dist[j-1]) {
                dist[i-1] = dist[j-1]; indx[i-1] = indx[j-1];
                i = j;   j = j+j;
            }
            else    j = ir+1;
        }
        dist[i-1] = disth;   indx[i-1] = indxh;
    }
}


/**********************************************************************
 * Make_Time : Encode time from string of form:                       *
 *     HH:MM:SS.SS MM/DD/YYYY to seconds since 1970                   *
 *                                                                    *
 **********************************************************************/
void Make_Time( double *secs, char *string)
{
    struct Greg    g;
    char   *token, str[10];
    double sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's     */
                                      /* time 0 and 1970-01-01 00:00:00.0 GMT */
    
    token = strstr(string, ":");
    strncpy(str, token-2, (size_t)2);
    str[2] = 0;
    sscanf( str, "%d", &g.hour);
    strncpy(str, token+1, (size_t)2);
    sscanf( str, "%d", &g.minute);
    strncpy(str, token+4, (size_t)5);
    str[5] = 0;
    sscanf( str, "%f", &g.second);
    
    token = strstr(token, "/");
    strncpy(str, token-2, (size_t)2);
    str[2] = 0;
    sscanf( str, "%d", &g.month);
    strncpy(str, token+1, (size_t)2);
    sscanf( str, "%d", &g.day);
    strncpy(str, token+4, (size_t)4);
    str[4] = 0;
    sscanf( str, "%d", &g.year);
    
    *secs    = 60.0 * (double) julmin(&g) - sec1970;
}


/*************************************************************************
 *   short Build_Menu ()                                                 *
 *      Builds the waveserver's menu                                     *
 *************************************************************************/

short Build_Menu (Global *But)
{
    char    whoami[50], server[100];
    int     j, retry, ret, rc;
    WS_PSCNL scnp;
    WS_MENU menu; 
      
    /* Build the current wave server menus *
     ***************************************/

    sprintf(whoami, " %s: %s: ", But->mod, "Build_Menu");
    But->got_a_menu = 0;
    
    for(j=0;j<But->nServer;j++) But->index[j]  = j;
    
    for (j=0;j< But->nServer; j++) {
        retry = 0;
        But->inmenu[j] = 0;
        sprintf(server, " %s:%s <%s>", But->wsIp[j], But->wsPort[j], But->wsComment[j]);
        if ( But->wsIp[j][0] == 0 ) continue;
    Append:
        
        ret = wsAppendMenu(But->wsIp[j], But->wsPort[j], &But->menu_queue[j], But->wsTimeout);
        
        if (ret == WS_ERR_NO_CONNECTION) { 
            if(But->Debug) 
                logit("e","%s Could not get a connection to %s to get menu.\n", whoami, server);
        }
        else if (ret == WS_ERR_SOCKET) 
            logit("e","%s Could not create a socket for %s\n", whoami, server);
        else if (ret == WS_ERR_BROKEN_CONNECTION) {
            logit("e","%s Connection to %s broke during menu\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_TIMEOUT) {
            logit("e","%s Connection to %s timed out during menu.\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_MEMORY) 
            logit("e","%s Waveserver %s out of memory.\n", whoami, server);
        else if (ret == WS_ERR_INPUT) {
            logit("e","%s Connection to %s input error\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_PARSE) 
            logit("e","%s Parser failed for %s\n", whoami, server);
        else if (ret == WS_ERR_BUFFER_OVERFLOW) 
            logit("e","%s Buffer overflowed for %s\n", whoami, server);
        else if (ret == WS_ERR_EMPTY_MENU) 
            logit("e","%s Unexpected empty menu from %s\n", whoami, server);
        else if (ret == WS_ERR_NONE) {
            But->inmenu[j] = But->got_a_menu = 1;
        }
        else logit("e","%s Connection to %s returns error: %d\n", 
                   whoami, server, ret);
    }
    /* Let's make sure that servers in our server list have really connected.
       **********************************************************************/  
    for(j=0;j<But->nServer;j++) {
        if ( But->inmenu[j] ) {
            rc = wsGetServerPSCNL( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
            if ( rc == WS_ERR_EMPTY_MENU ) {
                logit("e","%s Empty menu.\n", whoami);
                But->inmenu[j] = 0;
            }
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                logit("e","%s %s not in menu.\n", whoami, But->wsIp[j]);
                But->inmenu[j] = But->wsIp[j][0] = 0;
            }
        /* Then, detach 'em and let RequestWave attach only the ones it needs.
           **********************************************************************/  
            menu = But->menu_queue[j].head;
            if ( menu->sock > 0 ) wsDetachServer( menu );
        }
    }
    return 0;
}


/*************************************************************************
 *   short In_Menu (sta,net)                                             *
 *      Determines if the sn is in the waveserver's menu                 *
 *************************************************************************/

int In_Menu (Global *But, char *sta, char *net, 
                double *tankStarttime, double *tankEndtime)
{
    char    whoami[50];
    int     i, rc;
    WS_PSCNL scnp;
      
    sprintf(whoami, " %s: %s: ", But->mod, "In_Menu");
    for(i=0;i<But->nServer;i++) {
        if(But->inmenu[i]) {
            rc = wsGetServerPSCNL( But->wsIp[i], But->wsPort[i], &scnp, &But->menu_queue[i]);    
            if ( rc == WS_ERR_EMPTY_MENU )         continue;
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                if(But->Debug) logit("e","%s  %s:%s not in menu.\n", whoami, But->wsIp[i], But->wsPort[i]);
                But->inmenu[i] = 0;
                continue;
            }

            while ( 1 ) {
               if(strcmp(scnp->sta,  sta )==0 && 
                  strcmp(scnp->net,  net )==0) {
                  *tankStarttime = scnp->tankStarttime;
                  *tankEndtime   = scnp->tankEndtime;
                  return 1;
               }
               if ( scnp->next == NULL )
                  break;
               else
                  scnp = scnp->next;
            }
        }
    }
    return 0;
}


/*************************************************************************
 *   In_Menu_list                                                        *
 *      Determines if the scn is in the waveservers' menu.               *
 *      If there, the tank starttime and endtime are returned.           *
 *      Also, the Server IP# and port are returned.                      *
 *************************************************************************/

int In_Menu_list (Global *But)
{
    char     whoami[90], server[100];
    int      i, j, rc;
    WS_PSCNL scnp;
    
    sprintf(whoami, " %s: %s: ", But->mod, "In_Menu_list");
    i = 0;
    But->nentries = 0;
    for(j=0;j<But->nServer;j++) {
        if(But->inmenu[j]) {
            sprintf(server, " %s:%s <%s>", But->wsIp[j], But->wsPort[j], But->wsComment[j]);
            rc = wsGetServerPSCNL( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
            if ( rc == WS_ERR_EMPTY_MENU ) {
                if(But->Debug) logit("e","%s Empty menu for %s \n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                if(But->Debug) logit("e","%s  %s not in menu.\n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }

            while ( 1 ) {
               if(strcmp(scnp->sta,  But->Site)==0 && 
                  strcmp(scnp->chan, But->Comp)==0 && 
                  strcmp(scnp->net,  But->Net )==0) {
                  But->TStime[j] = scnp->tankStarttime;
                  But->TEtime[j] = scnp->tankEndtime;
                  But->index[But->nentries]  = j;
                  But->nentries += 1;
               }
               if ( scnp->next == NULL )
                  break;
               else
                  scnp = scnp->next;
            }
        }
    }
    if(But->nentries>0) return 1;
    return 0;
}


/********************************************************************
 *  RequestWave                                                     *
 *   This is the binary version                                     *
 *   k - waveserver index                                           *
 ********************************************************************/
int RequestWave(Global *But, int k, double *Data,  
            char *Site, char *Comp, char *Net, char *Loc, double Stime, double Duration)
{
    char     whoami[50], SCNtxt[17];
    int      i, ret;
    TRACE_REQ   request;
    GAP *pGap;
    
    sprintf(whoami, " %s: %s: ", But->mod, "RequestWave");
    strcpy(request.sta,  Site);
    strcpy(request.chan, Comp);
    strcpy(request.net,  Net );
    strcpy(request.loc,  Loc );   
    request.waitSec = 0;
    request.pinno = 0;
    request.reqStarttime = Stime;
    request.reqEndtime   = request.reqStarttime + Duration;
    request.partial = 1;
    request.pBuf    = But->TraceBuf;
    request.bufLen  = MAXTRACELTH*9;
    request.timeout = But->wsTimeout;
    request.fill    = 919191;
    sprintf(SCNtxt, "%s %s %s %s", Site, Comp, Net, Loc);

    /* Clear out the gap list */
    if(pTrace.nGaps != 0) {
        while ( (pGap = pTrace.gapList) != (GAP *)NULL) {
            pTrace.gapList = pGap->next;
            free(pGap);
        }
    }
    pTrace.nGaps = 0;
    for(i=0;i<MAXTRACELTH;i++) pTrace.rawData[i] = 0.0;

    ret = WSReqBin(But, k, &request, &pTrace, SCNtxt);
    
    But->Npts = pTrace.nRaw;

    if(But->Npts>MAXTRACELTH) {
        logit("e","%s Trace: %s Too many points: %d\n", whoami, SCNtxt, But->Npts);
        But->Npts = MAXTRACELTH;
    }
    for(i=0;i<But->Npts;i++) {
        Data[i] = pTrace.rawData[i];
    }

    But->Mean = 0.0;
    if(pTrace.nGaps >= MAX_GAPS) ret = 4;

    return ret;
}

/********************************************************************
 *  WSReqBin                                                        *
 *                                                                  *
 *   k - waveserver index                                           *
 ********************************************************************/
int WSReqBin(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt)
{
    char     server[wsADRLEN*3], whoami[50];
    int      i, kk, io, retry;
    int      isamp, nsamp, gap0, success, ret, WSDebug = 0;          
    long     iEnd, npoints;
    WS_MENU  menu = NULL;
    WS_PSCNL  pscn = NULL;
    double   mean, traceEnd, samprate;
    long    *longPtr;
    short   *shortPtr;
    
    TRACE2_HEADER *pTH;
    TRACE2_HEADER *pTH4;
    char tbuf[MAX_TRACEBUF_SIZ];
    GAP *pGap, *newGap;
    
    sprintf(whoami, " %s: %s: ", But->mod, "WSReqBin");
    WSDebug = But->WSDebug;
    success = retry = 0;
    
gettrace:
    menu = NULL;
    /*    Put out WaveServer request here and wait for response */
    /* Get the trace
     ***************/
    /* rummage through all the servers we've been told about */
    if ( (wsSearchSCNL( request, &menu, &pscn, &But->menu_queue[k]  )) == WS_ERR_NONE ) {
        strcpy(server, menu->addr);
        strcat(server, "  ");
        strcat(server, menu->port);
    } else {
        strcpy(server, "unknown");
    }
    
/* initialize the global trace buffer, freeing old GAP structures. */
    pTrace->nRaw  = 0L;
    pTrace->delta     = 0.0;
    pTrace->starttime = 0.0;
    pTrace->endtime   = 0.0;
    pTrace->nGaps = 0;
 
    /* Clear out the gap list */
    pTrace->nGaps = 0;
    while ( (pGap = pTrace->gapList) != (GAP *)NULL) {
        pTrace->gapList = pGap->next;
        free(pGap);
    }
 
    if(WSDebug) {     
        logit("e","\n%s Issuing request to wsGetTraceBinL: server: %s Socket: %d.\n", 
             whoami, server, menu->sock);
        logit("e","    %s %f %f %d\n", SCNtxt,
             request->reqStarttime, request->reqEndtime, request->timeout);
    }

    io = wsGetTraceBinL(request, &But->menu_queue[k], But->wsTimeout);
    
    if (io == WS_ERR_NONE ) {
        if(WSDebug) {
            logit("e"," %s server: %s trace %s: went ok first time. Got %ld bytes\n",
                whoami, server, SCNtxt, request->actLen); 
            logit("e","        actStarttime=%lf, actEndtime=%lf, actLen=%ld, samprate=%lf\n",
                  request->actStarttime, request->actEndtime, request->actLen, request->samprate);
        }
    }
    else {
        switch(io) {
        case WS_ERR_EMPTY_MENU:
        case WS_ERR_SCNL_NOT_IN_MENU:
        case WS_ERR_BUFFER_OVERFLOW:
            if (io == WS_ERR_EMPTY_MENU ) 
                logit("e"," %s server: %s No menu found.  We might as well quit.\n", whoami, server); 
            if (io == WS_ERR_SCNL_NOT_IN_MENU ) 
                logit("e"," %s server: %s Trace %s not in menu\n", whoami, server, SCNtxt);
            if (io == WS_ERR_BUFFER_OVERFLOW ) 
                logit("e"," %s server: %s Trace %s overflowed buffer. Fatal.\n", whoami, server, SCNtxt); 
            return 2;                /*   We might as well quit */
        
        case WS_ERR_PARSE:
        case WS_ERR_TIMEOUT:
        case WS_ERR_BROKEN_CONNECTION:
        case WS_ERR_NO_CONNECTION:
            sleep_ew(500);
            retry += 1;
            if (io == WS_ERR_PARSE )
                logit("e"," %s server: %s Trace %s: Couldn't parse server's reply. Try again.\n", 
                        whoami, server, SCNtxt); 
            if (io == WS_ERR_TIMEOUT )
                logit("e"," %s server: %s Trace %s: Timeout to wave server. Try again.\n", whoami, server, SCNtxt); 
            if (io == WS_ERR_BROKEN_CONNECTION ) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Broken connection to wave server. Try again.\n", 
                whoami, server, SCNtxt); 
            }
            if (io == WS_ERR_NO_CONNECTION ) {
                if(WSDebug || retry>1) 
                    logit("e"," %s server: %s Trace %s: No connection to wave server.\n", whoami, server, SCNtxt); 
                if(WSDebug) logit("e"," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
            }
            ret = wsAttachServer( menu, But->wsTimeout );
            if(ret == WS_ERR_NONE && retry < But->RetryCount) goto gettrace;
            if(WSDebug) {   
            }
                switch(ret) {
                case WS_ERR_NO_CONNECTION:
                    logit("e"," %s server: %s: No connection to wave server.\n", whoami, server); 
                    break;
                case WS_ERR_SOCKET:
                    logit("e"," %s server: %s: Socket error.\n", whoami, server); 
                    break;
                case WS_ERR_INPUT:
                    logit("e"," %s server: %s: Menu missing.\n", whoami, server); 
                    break;
                default:
                    logit("e"," %s server: %s: wsAttachServer error %d.\n", whoami, server, ret); 
                }
            return 2;                /*   We might as well quit */
        
        case WS_WRN_FLAGGED:
            if(WSDebug) {
            } 
            
            if((int)strlen(&request->retFlag)>0) {
                logit("e","%s server: %s Trace %s: return flag from wsGetTraceBinL: <%c>\n %.50s\n", 
                        whoami, server, SCNtxt, request->retFlag, But->TraceBuf);
            }
            logit("e"," %s server: %s Trace %s: No trace available. Wave server returned %c\n", 
                whoami, server, SCNtxt, request->retFlag); 
       /*     break; */
            return 2;               /*    We might as well quit */
        
        default:
            logit( "et","%s server: %s Failed.  io = %d\n", whoami, server, io );
        }
    }
    
    /* Transfer trace data from TRACE_BUF packets into Trace buffer */
    traceEnd = (request->actEndtime < request->reqEndtime) ?  request->actEndtime :
                                                              request->reqEndtime;
    pTH  = (TRACE2_HEADER *)request->pBuf;
    pTH4 = (TRACE2_HEADER *)tbuf;
    /*
    * Swap to local byte-order. Note that we will be calling this function
    * twice for the first TRACE_BUF packet; this is OK as the second call
    * will have no effect.
    */

    memcpy( pTH4, pTH, sizeof(TRACE2_HEADER) );
    if(WSDebug) logit( "e","%s server: %s Make Local\n", whoami, server, io );
    if (WaveMsg2MakeLocal(pTH4) == -1) {
        logit("et", "%s server: %s %s.%s.%s.%s unknown datatype <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
        return 5;
    }
    if (WaveMsg2MakeLocal(pTH4) == -2) {
        logit("et", "%s server: %s %s.%s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
    /*  return 5;  */
    }

    if(WSDebug) logit("e"," %s server: %s Trace %s: Data has samprate %g.\n", 
                     whoami, server, SCNtxt, pTH4->samprate); 
    if (pTH4->samprate < 0.1) {
        logit("et", "%s server: %s %s.%s.%s (%s) has zero samplerate (%g); skipping trace\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, SCNtxt, pTH4->samprate);
        return 5;
    }
    But->samp_sec = pTH4->samprate<=0? 100L:(long)pTH4->samprate;

    pTrace->delta = 1.0/But->samp_sec;
    samprate = But->samp_sec;   /* Save rate of first packet to compare with later packets */
    pTrace->starttime = request->reqStarttime;
    /* Set Trace endtime so it can be used to test for gap at start of data */
    pTrace->endtime = ( (pTH4->starttime < request->reqStarttime) ?
                        pTH4->starttime : request->reqStarttime) - 0.5*pTrace->delta ;
    if(WSDebug) logit("e"," pTH->starttime: %f request->reqStarttime: %f delta: %f endtime: %f.\n", 
                     pTH4->starttime, request->reqStarttime, pTrace->delta, pTrace->endtime); 

  /* Look at all the retrieved TRACE_BUF packets 
   * Note that we must copy each tracebuf from the big character buffer
   * to the TRACE_BUF structure pTH4.  This is because of the occasionally 
   * seen case of a channel putting an odd number of i2 samples into
   * its tracebufs!  */
    kk = 0;
    while( pTH < (TRACE2_HEADER *)(request->pBuf + request->actLen) ) {
        memcpy( pTH4, pTH, sizeof(TRACE2_HEADER) );
         /* Swap bytes to local order */
	    if (WaveMsg2MakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsg2MakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	    /*  return 5;  */
        }
    
        nsamp = pTH4->nsamp;
        memcpy( pTH4, pTH, sizeof(TRACE2_HEADER) + nsamp*4 );
         /* Swap bytes to local order */
	    if (WaveMsg2MakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsg2MakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	    /*  return 5;  */
        }
    
        if ( fabs(pTH4->samprate - samprate) > 1.0) {
            logit("et", "%s <%s.%s.%s samplerate change: %f - %f; discarding trace\n",
                whoami, pTH4->sta, pTH4->chan, pTH4->net, samprate, pTH4->samprate);
            return 5;
        }
    
    /* Check for gap */
        if (pTrace->endtime + 1.5 * pTrace->delta < pTH4->starttime) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Gap detected.\n", 
                        whoami, server, SCNtxt); 
            if ( (newGap = (GAP *)calloc(1, sizeof(GAP))) == (GAP *)NULL) {
                logit("et", "getTraceFromWS: out of memory for GAP struct\n");
                return -1;
            }
            newGap->starttime = pTrace->endtime + pTrace->delta;
            newGap->gapLen = pTH4->starttime - newGap->starttime;
            newGap->firstSamp = pTrace->nRaw;
            newGap->lastSamp  = pTrace->nRaw + (long)( (newGap->gapLen * samprate) - 0.5);
            if(WSDebug) logit("e"," starttime: %f gaplen: %f firstSamp: %d lastSamp: %d.\n", 
                        newGap->starttime, newGap->gapLen, newGap->firstSamp, newGap->lastSamp); 
            /* Put GAP struct on list, earliest gap first */
            if (pTrace->gapList == (GAP *)NULL)
                pTrace->gapList = newGap;
            else
                pGap->next = newGap;
            pGap = newGap;  /* leave pGap pointing at the last GAP on the list */
            pTrace->nGaps++;
            if(pTrace->nGaps > MAX_GAPS) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Too many gaps (%d).\n", 
                        whoami, server, SCNtxt, pTrace->nGaps); 
            
            }

            /* Advance the Trace pointers past the gap; maybe gap will get filled */
            pTrace->nRaw = newGap->lastSamp + 1;
            pTrace->endtime += newGap->gapLen;
        }
    
        isamp = (pTrace->starttime > pTH4->starttime)?
                (long)( 0.5 + (pTrace->starttime - pTH4->starttime) * samprate):0;

        if (request->reqEndtime < pTH4->endtime) {
            nsamp = pTH4->nsamp - (long)( 0.5 * (pTH4->endtime - request->reqEndtime) * samprate);
            pTrace->endtime = request->reqEndtime;
        } 
        else {
            nsamp = pTH4->nsamp;
            pTrace->endtime = pTH4->endtime;
        }

    /* Assume trace data is integer valued here, long or short */    
        if (pTH4->datatype[1] == '4') {
            longPtr=(long*) ((char*)pTH4 + sizeof(TRACE2_HEADER) + isamp * 4);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *longPtr;
                longPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF message */
            pTH = (TRACE2_HEADER *)((char *)pTH + sizeof(TRACE2_HEADER) + pTH4->nsamp * 4);
        }
        else {   /* pTH->datatype[1] == 2, we assume */
            shortPtr=(short*) ((char*)pTH4 + sizeof(TRACE2_HEADER) + isamp * 2);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *shortPtr;
                shortPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF packets */
            pTH = (TRACE2_HEADER *)((char *)pTH + sizeof(TRACE2_HEADER) + pTH4->nsamp * 2);
        }
    }  /* End of loop over TRACE_BUF packets */
  
    if(WSDebug) {
        logit("e"," %s server: %s Trace %s: ngaps (%d).\n", 
                    whoami, server, SCNtxt, pTrace->nGaps); 
        pGap = pTrace->gapList;
        for(i=0;i<pTrace->nGaps;i++) {
            logit("e","  %f %f %d %d.\n", 
                        pGap->starttime, pGap->gapLen, pGap->firstSamp, pGap->lastSamp); 
            pGap = pGap->next;
        }  
    }  
  
  /* Find mean value of non-gap data */
    pGap = pTrace->gapList;
    i = npoints = 0L;
    mean    = 0.0;
  /*
   * Loop over all the data, skipping any gaps. Note that a `gap' will not be declared
   * at the end of the data, so the counter `i' will always get to pTrace->nRaw.
   */
    do {
        iEnd = (pGap == (GAP *)NULL)? pTrace->nRaw:pGap->firstSamp - 1;
        if(iEnd > pTrace->nRaw) iEnd = pTrace->nRaw;
        if (pGap != (GAP *)NULL) { /* Test for gap within peak-search window */
            gap0 = pGap->lastSamp - pGap->firstSamp + 1;
            if(Debug) logit("t", "trace from <%s> has %d point gap in window at %d\n", SCNtxt, gap0, pGap->firstSamp);
        }
        for (; i < iEnd; i++) {
            if(WSDebug) {
                if(pTrace->rawData[i] > 1.0e50 || pTrace->rawData[i] < -1.0e50) {
                    logit("e"," %s server: %s Trace %s: Pt %d out of range.\n", 
                                whoami, server, SCNtxt, i); 
                }
            }
            if(i==0) But->Min = But->Max = pTrace->rawData[i];
            if(pTrace->rawData[i] > But->Max) But->Max = pTrace->rawData[i];
            if(pTrace->rawData[i] < But->Min) But->Min = pTrace->rawData[i];
            mean += pTrace->rawData[i];
            npoints++;
        }
        if (pGap != (GAP *)NULL) {     /* Move the counter over this gap */    
            i = pGap->lastSamp + 1;
            pGap = pGap->next;
        }
    } while (i < pTrace->nRaw );
  
    mean /= (double)npoints;
    if(WSDebug) logit("e"," %s server: %s Trace %s: Mean of %d pts = %g.\n", 
                whoami, server, SCNtxt, npoints, mean); 
  
  /* Now remove the mean, and set points inside gaps to zero */
    i = 0;
    do {
        iEnd = (pGap == (GAP *)NULL)? pTrace->nRaw:pGap->firstSamp - 1;
        if(iEnd > pTrace->nRaw) iEnd = pTrace->nRaw;
        for (; i < iEnd; i++) pTrace->rawData[i] -= mean;

        if (pGap != (GAP *)NULL) {    /* Fill in the gap with zeros */    

            for ( ;i < pGap->lastSamp + 1; i++) pTrace->rawData[i] = 0.0;
            pGap = pGap->next;
        }
    } while (i < pTrace->nRaw );
  
    if (io == WS_ERR_NONE ) success = 1;

    return success;
}


/**********************************************************************
 * IsDST : Determine if we are using daylight savings time.           *
 *         This is a valid function for US and Canada thru 31/12/2099 *
 *                                                                    *
 *         Modified 02/16/07 to reflect political changes. JHL        *
 *         Fixed 03/05/08 to run correctly. JHL                       *
 *                                                                    *
 **********************************************************************/
int IsDST(int year, int month, int day)
{
    int     i, leapyr, day1, day2, num, jd, jd1, jd2, jd3, jd4;
    int     dpm[] = {31,28,31,30,31,30,31,31,30,31,30,31};
    
    leapyr = 0;
    if((year-4*(year/4))==0 && (year-100*(year/100))!=0) leapyr = 1;
    if((year-400*(year/400))==0) leapyr = 1;
    
    num = ((year-1900)*5)/4 + 5;
    num = num - 7*(num/7);       /* day # of 1 March                  */
    	day1 = num;
    	if(day1>7) day1 = day1 - 7;
    	jd3 = 59 + day1 + leapyr;    /* Julian day of 2nd Sunday in March */
    day1 = num + 2;
    if(day1>7) day1 = day1 - 7;  /* day # (-1) of 1 April             */
    day1 = 8 - day1;             /* date of 1st Sunday in April       */
    jd1 = 90 + day1 + leapyr;    /* Julian day of 1st Sunday in April */
    day2 = num + 3;
    if(day2>7) day2 = day2 - 7;  /* day # (-1) of 1 Oct               */
    day2 = 8 - day2;             /* date of 1st Sunday in Oct         */
    while(day2<=31) day2 += 7;   /* date of 1st Sunday in Nov         */
    	jd4 = 273 + leapyr + day2;   /* Julian day of 1st Sunday in Nov  */
    day2 -= 7;                   /* date of last Sunday in Oct        */
    jd2 = 273 + day2 + leapyr;   /* Julian day of last Sunday in Oct  */
    
    jd = day;
    for(i=0;i<month-1;i++) jd += dpm[i];
    if(month>2) jd += leapyr;
    
    if(jd>=jd1 && jd<jd2) return 1;
    if(jd>=jd3 && jd<jd4) return 1;
    
    return 0;
}


/**********************************************************************
 * Decode_Time : Decode time from seconds since 1970                  *
 *                                                                    *
 **********************************************************************/
void Decode_Time( double secs, TStrct *Time)
{
    struct Greg  g;
    long    minute;
    double  sex;

    Time->Time = secs;
    secs += sec1970;
    Time->Time1600 = secs;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    Time->Year  = g.year;
    Time->Month = g.month;
    Time->Day   = g.day;
    Time->Hour  = g.hour;
    Time->Min   = g.minute;
    Time->Sec   = sex;
}


/**********************************************************************
 * Encode_Time : Encode time to seconds since 1970                    *
 *                                                                    *
 **********************************************************************/
void Encode_Time( double *secs, TStrct *Time)
{
    struct Greg    g;

    g.year   = Time->Year;
    g.month  = Time->Month;
    g.day    = Time->Day;
    g.hour   = Time->Hour;
    g.minute = Time->Min;
    *secs    = 60.0 * (double) julmin(&g) + Time->Sec - sec1970;
}


/**********************************************************************
 * date22 : Calculate 22 char date in the form Jan23,1988 12:34 12.21 *
 *          from the julian seconds.  Remember to leave space for the *
 *          string termination (NUL).                                 *
 **********************************************************************/
void date22( double secs, char *c22)
{
    char *cmo[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    struct Greg  g;
    long    minute;
    double  sex;

    secs += sec1970;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    sprintf(c22, "%3s%2d,%4d %.2d:%.2d:%05.2f",
            cmo[g.month-1], g.day, g.year, g.hour, g.minute, sex);
}


/***********************************************************************
 * date11 : Calculate 11 char h:m:s time in the form 12:34:12.21       *
 *          from the daily seconds.  Remember to leave space for the   *
 *          string termination (NUL).                                  *
 *          This is a special version used for labelling the time axis *
 ***********************************************************************/
void date11( double secs, char *c11)
{
    long    hour, minute, wholesex;
    double  sex, fracsex;

    if(secs < 86400.0) secs = secs - 86400; /* make negative times negative */
    minute = (long) (secs / 60.0);
    sex    = secs - 60.0 * minute;
    wholesex = (long) sex;
    fracsex  = sex - wholesex;
    hour   = (long) (minute / 60.0);
    minute = minute - 60 * hour;
    while(hour>=24) hour -= 24;
    
    if(hour != 0) {
        sprintf(c11, "%d:%.2d:%05.2f", hour, minute, sex);
        if(fracsex >= 0.01) sprintf(c11, "%.2d:%.2d:%05.2f", hour, minute, sex);
        else sprintf(c11, "%.2d:%.2d:%.2d", hour, minute, wholesex);
    }
    else if(minute != 0) {
        sprintf(c11, "%d:%05.2f", minute, sex);
        if(fracsex >= 0.01) sprintf(c11, "%.2d:%05.2f", minute, sex);
        else sprintf(c11, "%.2d:%.2d", minute, wholesex);
    }
    else if(sex >= 10.0) {
        if(fracsex >= 0.01) sprintf(c11, "%05.2f", sex);
        else sprintf(c11, "%.2d", wholesex);
    }
    else {
        if(fracsex >= 0.01) sprintf(c11, "%4.2f", sex);
        else sprintf(c11, "%.1d", wholesex);
    }
}


/********************************************************************
 * GetMaxDist                                                       *
 *   Returns the distance at which we expect to see an amplitude    *
 *   of loga for an event with magnitude mag.                       *
 *  Bakun&Joyner, 1984, BSSA, 74, 1827-1843.                        *
 *  (for Central California)                                        *
 *  -logA = 1.000*log(r/100) + 0.00301*(r-100) + 3.0;  0<= r <=400  * 
 *          r = sqrt(dist^2+depth^2)                                *
 *                                                                  *
 *  logA  = log10(Amp);                                             *
 *  Ml100 = logA - (-logA0);                                        *
 *                                                                  *
 ********************************************************************/

void GetMaxDist(double *dist, double loga, double mag)
{
    char    whoami[90];
    double  rm, rmin, rmax, eta, fl, fu, fm, fac;
    
    sprintf(whoami, " %s: ", "GetMaxDist");

	rmin = 0.0;
	rmax = 1000.0;
	eta  = 0.1;
	fac  = mag - loga;
	fl = logA0(rmin) - fac;
	fu = logA0(rmax) - fac;
	if(fl*fu > 0.0) {
		logit("e", "%s Error setting up iteration.\n", whoami);
		return;
	}
	
	do{
		rm = (rmin+rmax)/2.0;
		fm = logA0(rm) - fac;
		if(fm*fl > 0.0) {
			rmin = rm;
			fl = fm;
		} else {
			rmax = rm;
			fu = rm;
		}
	} while(fabs(fm) > eta);
	*dist = rm;
}


/********************************************************************
 * logA0                                                            *
 *   Solves the attenuation function at distance dist with an       *
 *   amplitude of loga for an event with magnitude mag.             *
 ********************************************************************/

double logA0(double dist)
{
    double  logA;
    
/*  logA = log10(dist/100.0) + 0.00301*(dist-100.0) + 3.0;  */
    logA = log10(dist) + 0.00301*dist + 0.699;
        
    return(logA);
}


/*************************************************************************
 *   SetPlot sets globals for the current plot.                          *
 *                                                                       *
 *************************************************************************/
void SetPlot(double Xsize, double Ysize)
{
    xsize = Xsize;    
    ysize = Ysize;
}


/*************************************************************************
 *   ixq calculates the x pixel location.                                *
 *   a is the distance in inches from the left margin.                   *
 *************************************************************************/
int ixq(double a)
{
    double   val;
    int      i;
    
    val  = (a + XLMARGIN);
    if(val > xsize) val = xsize;
    if(val < 0.0)   val = 0.0;
    i = val*72;
    return i;
}


/*************************************************************************
 *   iyq calculates the y pixel location.                                *
 *   a is the distance in inches up from the bottom margin.              *
 *                      <or>                                             *
 *   a is the distance in inches down from the top margin.               *
 *************************************************************************/
int iyq(double a)
{
    double   val;
    int      i;
    
    val =  YTMargin + a;   /* times increases down from top */
    
    if(val > ysize) val = ysize;
    if(val < 0.0)   val = 0.0;
    i = val*72;
    return i;
}


/****************************************************************************
 *  Get_Sta_Info(Global *But);                                              *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Sta_Info(Global *But)
{
    char    whoami[50], *com, *str;
    int     i, j, k, nfiles, success, type, sensor, units;
    double  dlat, mlat, dlon, mlon, gain, sens, ssens;

    sprintf(whoami, "%s: %s: ", But->mod, "Get_Sta_Info");
    
	if(But->Debug) logit("e", "%s %d database files.\n", whoami, But->nStaDB );
    But->NSCN = 0;
    for(k=0;k<But->nStaDB;k++) {
            /* Open the main station file
             ****************************/
        nfiles = k_open( But->stationList[k] );
        if(nfiles == 0) {
            logit("e", "%s Error opening command file <%s>; exiting!\n", whoami, But->stationList[k] );
            exit( -1 );
        }

            /* Process all command files
             ***************************/
        while(nfiles > 0) {  /* While there are command files open */
            while(k_rd())  {      /* Read next line from active file  */
                com = k_str();         /* Get the first token from line */

                    /* Ignore blank lines & comments
                     *******************************/
                if( !com )           continue;
                if( com[0] == '#' )  continue;

                    /* Open a nested configuration file
                     **********************************/
                if( com[0] == '@' ) {
                    success = nfiles+1;
                    nfiles  = k_open(&com[1]);
                    if ( nfiles != success ) {
                        logit("e", "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                        exit( -1 );
                    }
                    continue;
                }

                /* Process anything else as a channel descriptor
                 ***********************************************/

                if( But->NSCN >= MAXCHANNELS ) {
                    logit("e", "%s Too many channel entries in <%s>", 
                             whoami, But->stationList[k] );
                    logit("e", "; max=%d; exiting!\n", (int) MAXCHANNELS );
                    exit( -1 );
                }
                j = But->NSCN;
                
                    /* S C N */
                strncpy( But->Chan[j].Site, com,  6);
                str = k_str();
                if(str) strncpy( But->Chan[j].Net,  str,  2);
                str = k_str();
                if(str) strncpy( But->Chan[j].Comp, str, 3);
                str = k_str();
                if(str) strncpy( But->Chan[j].Loc, str, 3);
                for(i=0;i<6;i++) if(But->Chan[j].Site[i]==' ') But->Chan[j].Site[i] = 0;
                for(i=0;i<2;i++) if(But->Chan[j].Net[i] ==' ') But->Chan[j].Net[i]  = 0;
                for(i=0;i<3;i++) if(But->Chan[j].Comp[i]==' ') But->Chan[j].Comp[i] = 0;
                for(i=0;i<2;i++) if(But->Chan[j].Loc[i] ==' ') But->Chan[j].Loc[i]  = 0;
                But->Chan[j].Comp[3] = But->Chan[j].Net[2] = But->Chan[j].Site[5] = But->Chan[j].Loc[2] = 0;
                sprintf(But->Chan[j].SCNnam, "%s_%s_%s_%s", 
                        But->Chan[j].Site, But->Chan[j].Comp, But->Chan[j].Net, But->Chan[j].Loc);

                    /* Lat Lon Elev */
                But->Chan[j].Lat  = k_val();
                But->Chan[j].Lon  = k_val();
                But->Chan[j].Elev = k_val();
                                
                But->Chan[j].Inst_type  = k_int();
                But->Chan[j].Inst_gain  = k_val();
                But->Chan[j].GainFudge  = k_val();
                But->Chan[j].Sens_type  = k_int();
                But->Chan[j].Sens_unit  = k_int();
                But->Chan[j].Sens_gain  = k_val();
                if(But->Chan[j].Sens_unit == 3) But->Chan[j].Sens_gain /= 978.0;
                But->Chan[j].SiteCorr = k_val();
                But->Chan[j].ShkQual  = k_int();
                        
               
                But->Chan[j].sensitivity = (1000000.0*But->Chan[j].Sens_gain/But->Chan[j].Inst_gain)*But->Chan[j].GainFudge;    /*    sensitivity counts/units        */
                
                if (k_err()) {
                    logit("e", "%s Error decoding line in station file\n%s\n  exiting!\n", whoami, k_get() );
                    exit( -1 );
                }
         /*>Comment<*/
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].SiteName, str );
                    
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].Descript, str );
                    
                But->NSCN++;
            }
            nfiles = k_close();
        }
    }
	if(But->Debug) logit("e", "%s %d channels in local database.\n", whoami, But->NSCN );
}


/*************************************************************************
 *  Put_Sta_Info(Global *But);                                           *
 *  Retrieve all the information available about station i               *
 *  and put it into an internal structure for reference.                 *
 *  This should eventually be a call to the database; for now we must    *
 *  supply an ascii file with all the info.                              *
 *************************************************************************/
int Put_Sta_Info(Global *But)
{
    char       whoami[50];
    int        j;

    sprintf(whoami, " %s: %s: ", But->mod, "Put_Sta_Info");
    
    for(j=0;j<But->NSCN;j++) {
        if(strcmp(But->Chan[j].Site, But->Site )==0 && 
           strcmp(But->Chan[j].Comp, But->Comp)==0 && 
           strcmp(But->Chan[j].Net,  But->Net )==0) {
            
            strcpy(But->Comment, But->Chan[j].SiteName);
            But->Inst_type  = But->Chan[j].Inst_type;
            But->Sens_type  = But->Chan[j].Sens_type;
            But->Sens_gain  = But->Chan[j].Sens_gain;
            But->Sens_unit  = But->Chan[j].Sens_unit;
            But->sensitivity  = But->Chan[j].sensitivity;
		    if(But->Sens_unit<=1) But->Scaler = But->Scale*1000.0;
		    if(But->Sens_unit==2) But->Scaler = But->Scale*10000.0;
		    if(But->Sens_unit==3) But->Scaler = But->Scale*10.0;
	        if(But->Debug) {
	            logit("e", "\n%s %s. %d %d %d %f %f %f %s\n\n", 
	                whoami, But->SCNtxt, But->Inst_type, But->Sens_type, 
	                But->Sens_unit, But->Sens_gain, But->sensitivity, 
	                But->Scaler, But->Comment);
	        }   
            return 1;
        }
    }
    
    strcpy(But->Comment, "");
    But->Inst_type  = 0;
    But->Sens_type  = 0;
    But->Sens_gain  = 1.0;
    But->Sens_unit  = 0;
    But->sensitivity  = 1000000.0;
    But->Scaler = But->Scale*1000.0;
	if(But->Debug) {
		logit("e", "\n%s %s. %d %d %d %f %f %f %s\n\n", 
			whoami, But->SCNtxt, But->Inst_type, But->Sens_type, 
			But->Sens_unit, But->Sens_gain, But->sensitivity, 
			But->Scaler, But->Comment);
	}   
    return 0;
}


/*************************************************************************
 *  Find_Sta_Info(Global *But);                                          *
 *  Retrieve all the information available about station i               *
 *  and put it into an internal structure for reference.                 *
 *  This should eventually be a call to the database; for now we must    *
 *  supply an ascii file with all the info.                              *
 *************************************************************************/
int Find_Sta_Info(Global *But)
{
    char    whoami[90];
    int     i, j, jj, k;

    sprintf(whoami, " %s: %s: ", But->mod, "Find_Sta_Info");
    
    if(But->Debug) logit("e", "%s There are  %d channels listed. ***\n", whoami, But->NSCN); 
    But->Nsta = 0;
    for(i=0;i<But->NSCN;i++) {
	    jj = But->Nsta;
	    for(j=0;j<But->Nsta;j++) {
	        if(strcmp(But->Chan[i].Site, But->Sta[j].Site )==0 && 
	           strcmp(But->Chan[i].Net,  But->Sta[j].Net )==0) {
			    jj = j;
	        }
	    }
        if(jj == But->Nsta) {
	        strcpy(But->Sta[jj].Site,     But->Chan[i].Site);
	        strcpy(But->Sta[jj].Net,      But->Chan[i].Net);
            strcpy(But->Sta[jj].SiteName, But->Chan[j].SiteName);
	        But->Sta[jj].Lat  = But->Chan[i].Lat;
	        But->Sta[jj].Lon  = But->Chan[i].Lon;
	        But->Sta[jj].Elev = But->Chan[i].Elev;
	        But->Sta[jj].Ninst = 0;
	        But->Nsta += 1;
        }
        k = But->Sta[jj].Ninst;
        strcpy(But->Sta[jj].SCNL[k].Comp, But->Chan[i].Comp);
        strcpy(But->Sta[jj].SCNL[k].Loc,  But->Chan[i].Loc);
        But->Sta[jj].SCNL[k].sensitivity  = But->Chan[i].sensitivity;
        But->Sta[jj].SCNL[k].Sens_unit    = But->Chan[i].Sens_unit;
        But->Sta[jj].SCNL[k].Sens_gain    = But->Chan[i].Sens_gain;
        But->Sta[jj].SCNL[k].SiteCorr     = But->Chan[i].SiteCorr;
        But->Sta[jj].Ninst += 1;
    }
    
    logit("e", "%s There are  %d stations. ***\n", whoami, But->Nsta); 
    
    return 0;
}


/************************************************************************

    Subroutine distaz (lat1,lon1,lat2,lon2,RNGKM,FAZ,BAZ)
    
c--  COMPUTES RANGE AND AZIMUTHS (FORWARD AND BACK) BETWEEN TWO POINTS.
c--  OPERATOR CHOOSES BETWEEN 3 FIRST ORDER ELLIPSOIDAL MODELS OF THE
c--  EARTH AS DEFINED BY THE MAJOR RADIUS AND FLATTENING.
c--  THE PROGRAM UTILIZES THE SODANO AND ROBINSON (1963) DIRECT SOLUTION
c--  OF GEODESICS (ARMY MAP SERVICE, TECH REP #7, SECTION IV).
c--  (TERMS ARE GIVEN TO ORDER ECCENTRICITY TO THE FOURTH POWER.)
c--  ACCURACY FOR VERY LONG GEODESICS:
c--          DISTANCE < +/-  1 METER
c--          AZIMUTH  < +/- .01 SEC
c
    Ellipsoid            Major Radius    Minor Radius    Flattening
1  Fischer 1960            6378166.0        6356784.28      298.30
2  Clarke1866              6378206.4        6356583.8       294.98
3  S. Am 1967              6378160.0        6356774.72      298.25
4  Hayford Intl 1910       6378388.0        6356911.94613   297.00
5  WGS 1972                6378135.0        6356750.519915  298.26
6  Bessel 1841             6377397.155      6356078.96284   299.1528
7  Everest 1830            6377276.3452     6356075.4133    300.8017
8  Airy                    6377563.396      6356256.81      299.325
9  Hough 1960              6378270.0        6356794.343479  297.00
10 Fischer 1968            6378150.0        6356768.337303  298.30
11 Clarke1880              6378249.145      6356514.86955   293.465
12 Fischer 1960            6378155.0        6356773.32      298.30
13 Intl Astr Union         6378160.0        6378160.0       298.25
14 Krasovsky               6378245.0        6356863.0188    298.30
15 WGS 1984                6378137.0        6356752.31      298.257223563
16 Aust Natl               6378160.0        6356774.719     298.25
17 GRS80                   6378137.0        63567552.31414  298.2572
18 Helmert                 6378200.0        6356818.17      298.30
19 Mod. Airy               6377341.89       6356036.143     299.325
20 Mod. Everest            6377304.063      6356103.039     300.8017
21 Mercury 1960            6378166.0        6356784.283666  298.30
22 S.E. Asia               6378155.0        6356773.3205    298.2305
23 Sphere                  6370997.0        6370997.0         0.0
24 Walbeck                 6376896.0        6355834.8467    302.78
25 WGS 1966                6378145.0        6356759.769356  298.25

************************************************************************/

short distaz (double lat1, double lon1, double lat2, double lon2, 
                double *rngkm, double *faz, double *baz)
{
    double    pi, rd, f, fsq;
    double    a3, a5, a6, a10, a16, a17, a18, a19, a20;
    double    a21, a22, a22t1, a22t2, a22t3, a23, a24, a25, a27, a28, a30;
    double    a31, a32, a33, a34, a35, a36, a38, a39, a40;
    double    a41, a43;
    double    a50, a51, a52, a54, a55, a57, a58, a62;
    double    dlon, alph[2];
    double    p11, p12, p13, p14, p15, p16, p17, p18;
    double    p21, p22, p23, p24, p25, p26, p27, p28, pd1, pd2;
    double    ang45;
    short     k;
    double    RAD, MRAD, FINV;
    
    RAD  = 6378206.4;
    MRAD = 6356583.8;
    FINV = 294.98;
    ang45 = atan(1.0);
    
    pi = 4.0*atan(1.0);
    rd = pi/180.0;

    a5 = RAD;
    a3 = FINV;
    
    if(a3==0.0) {
        a6 = MRAD;                    /*    minor radius of ellipsoid    */
        f = fsq = a10 = 0.0;
    }
    else {
        a6 = (a3-1.0)*a5/a3;          /*    minor radius of ellipsoid    */
        f = 1.0/a3;                   /*    flattening    */
        fsq = 1.0/(a3*a3);
        a10 = (a5*a5-a6*a6)/(a5*a5);  /*    eccentricity squared    */
    }
    
    /*    Following definitions are from Sodano algorithm for long geodesics    */
    a50 = 1.0 + f + fsq;
    a51 = f + fsq;
    a52 = fsq/ 2.0;
    a54 = fsq/16.0;
    a55 = fsq/ 8.0;
    a57 = fsq* 1.25;
    a58 = fsq/ 4.0;
    
/*     THIS IS THE CALCULATION LOOP. */
    
    p11 = lat1*rd;
    p12 = lon1*rd;
    p21 = lat2*rd;
    p22 = lon2*rd;
    *rngkm = *faz = *baz = 0.0;
    if( (lat1==lat2) && (lon1==lon2))    return(0);
    
    /*    Make sure points are not exactly on the equator    */
    if(p11 == 0.0) p11 = 0.000001;
    if(p21 == 0.0) p21 = 0.000001;
    
    /*    Make sure points are not exactly on the same meridian    */
    if(p12 == p22)             p22 += 0.0000000001;
    if(fabs(p12-p22) == pi)    p22 += 0.0000000001;
    
    /*    Correct latitudes for flattening    */
    p13 = sin(p11);
    p14 = cos(p11);
    p15 = p13/p14;
    p18 = p15*(1.0-f);
    a62 = atan(p18);
    p16 = sin(a62);
    p17 = cos(a62);
    
    p23 = sin(p21);
    p24 = cos(p21);
    p25 = p23/p24;
    p28 = p25*(1.0-f);
    a62 = atan(p28);
    p26 = sin(a62);
    p27 = cos(a62);
    
    dlon = p22-p12;
    a16 = fabs(dlon);
    
    /*    Difference in longitude to minimum (<pi)    */
    if(a16 >= pi) a16 = 2.0*pi - a16;

    /*    Compute range (a35)    */
    if(a16==0.0)     {a17 = 0.0;         a18 = 1.0;}
    else             {a17 = sin(a16);    a18 = cos(a16);}
    a19 = p16*p26;
    a20 = p17*p27;
    a21 = a19 + a20*a18;
    
    a40 = a41 = a43 = 0.0;
    
    for(k=0; k<2; k++) {
        a22t1 = (a17*p27)*(a17*p27);
        a22t2 = p26*p17 - p16*p27*a18;
        a22   = sqrt(a22t1 + (a22t2*a22t2));
        if(a22 == 0.0) return(0);
        a22t3 = a22*a21;
        a23 = (a20*a17)/a22;
        a24 = 1.0 - a23*a23;
        a25 = asin(a22);
        if(a21 < 0.0) a25 = pi-a25;
        a27 = (a25*a25)/a22;
        a28 = a21*a27;
        if(k==0) {
            a30 = (a50*a25) + a19*(a51*a22-a52*a27);
            a31 = 0.5*a24*(fsq*a28 - a51*(a25 + a22t3));
            a32 = a19*a19*a52*a22t3;
            a33 = a24*a24*(a54*(a25 + a22t3) - a52*a28 - a55*a22t3*(a21*a21));
            a34 = a19*a24*a52*(a27 + a22t3*a21);
            a35 = (a30 + a31 - a32 + a33 + a34)*a6;
            *rngkm = a35/1000.0;
        }

    /*    Compute azimuths    */
        a36 = (a51*a25) - a19*(a52*a22 + fsq*a27) + a24*(a58*a22t3 - a57*a25 + fsq*a28);
        a38 = a36*a23 + a16;
        a39 = sin(a38);
        a40 = cos(a38);

        if(a39*p27 == 0.0) a43 = 0.0;
        else {
          a41 = (p26*p17 - a40*p16*p27)/(a39*p27);
          if(a41 == 0.0)    a43 = pi/2.0;
          else                a43 = atan(1.0/a41);
        }

        alph[k] = a43;
        if((dlon <= -pi) || ((dlon >= 0.0) && (dlon < pi))) {
          if(a41 >= 0.0)       alph[k] = alph[k]-pi;
        }
        else {
          if(a41 >= 0.0)       alph[k] = pi - alph[k];
          else                 alph[k] = 2.0*pi - alph[k];
        }
        if(alph[k] >= pi)      alph[k] = alph[k] - pi;
        if(alph[k] <  pi)      alph[k] = alph[k] + pi;
        if(alph[k] <  0.0)     alph[k] = alph[k] + 2.0*pi;
        if(alph[k] >= 2.0*pi)  alph[k] = alph[k] - 2.0*pi;
        alph[k] = alph[k]/rd;
        pd1 = p16;
        pd2 = p17;
        p16 = p26;
        p17 = p27;
        p26 = pd1;
        p27 = pd2;
        dlon = -dlon;
    }

    *faz = alph[0];
    *baz = alph[1];
    while (*faz >= 360.0) *faz = *faz - 360.0;
    while (*baz >= 360.0) *baz = *baz - 360.0;
    
    return(0);
}



