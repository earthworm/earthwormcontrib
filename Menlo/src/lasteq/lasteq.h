/*****************************************************************************
 *  lasteq.h                                                                 *
 *                                                                           *
 *  header file for lasteq   .c                                              *
 *****************************************************************************/

/*****************************************************************************
 *  defines                                                                  *
 *****************************************************************************/

#define MAXCHANNELS     2600  /* Maximum number of channels                  */
#define MAXSAMPRATE      500  /* Maximum # of samples/sec.                   */
#define MAXMINUTES         5  /* Maximum # of minutes of trace.              */
#define MAXTRACELTH MAXMINUTES*60*MAXSAMPRATE /* Max. data trace length      */
#define MAXTRACEBUF MAXTRACELTH*10  /* This should work for 24-bit data      */

#define WSTIMEOUT          5  /* Number of seconds 'til waveserver times out */

#define MAXCOLORS         20  /* Number of colors defined                    */

#define MAXPLOTS           8  /* Maximum number of Plots/Event               */
#define MAX_STADBS        20  /* Maximum number of Station database files    */
#define MAX_WAVESERVERS   90  /* Maximum number of Waveservers               */
#define MAX_ADRLEN        20  /* Size of waveserver address arrays           */
#define MAX_TARGETS       10  /* Maximum number of targets                   */

#define PRFXSZ            20  /* Size of string for GIF file prefix          */
#define GDIRSZ           132  /* Size of string for GIF target directory     */
#define STALIST_SIZ      100  /* Size of string for station list file        */
#define MAXLOGO            2  /* Maximum number of Logos                     */

#define TIMEOUT          600  /* Time(sec) after event to wait for data.     */

#define MAX_STR          255  /* Size of strings for arkive records          */
#define MAX_SERV_THRDS     1  /* Maximum number of processor threads         */
#define MAX_GAPS        1000  /* Maximum number of gaps per data request.    */


/*****************************************************************************
 *  Define the structure for time records.                                   *
 *****************************************************************************/

typedef struct TStrct {   
    double  Time1600; /* Time (Sec since 1600/01/01 00:00:00.00)             */
    double  Time;     /* Time (Sec since 1970/01/01 00:00:00.00)             */
    int     Year;     /* Year                                                */
    int     Month;    /* Month                                               */
    int     Day;      /* Day                                                 */
    int     Hour;     /* Hour                                                */
    int     Min;      /* Minute                                              */
    double  Sec;      /* Second                                              */
} TStrct;

/*****************************************************************************
 *  Define the structure for specifying gaps in the data.                    *
 * Note: if a gap would be declared at end of data, the data must be         *
 * truncated instead of adding another GAP structure. A gap may be           *
 * declared at the start of the data, however.                               *
 *****************************************************************************/

typedef struct _GAP *PGAP;
typedef struct _GAP {
  double starttime;  /* time of first sample in the gap                      */
  double gapLen;     /* time from first gap sample to first sample after gap */
  long firstSamp;    /* index of first gap sample in data buffer             */
  long lastSamp;     /* index of last gap sample in data buffer              */
  PGAP next;         /* The next gap structure in the list                   */
} GAP;

/*****************************************************************************
 *  Define the structure for keeping track of buffer of trace data.          *
 *****************************************************************************/

typedef struct _DATABUF {
  double rawData[MAXTRACELTH*5];   /* The raw trace data; native byte order  */
  double delta;      /* The nominal time between sample points               */
  double starttime;  /* time of first sample in raw data buffer              */
  double endtime;    /* time of last sample in raw data buffer               */
  long nRaw;         /* number of samples in raw data buffer, including gaps */
  long lenRaw;       /* length to the rawData array                          */
  GAP *gapList;      /* linked list of gaps in raw data                      */
  int nGaps;         /* number of gaps found in raw data                     */
} DATABUF;

/*****************************************************************************
 *  Define the structure for the Arkive.                                     *
 *  This is an abbreviated structure.                                        *
 ******************************************************************************/

typedef struct Arkive {   /* An Arkive List */
    char    ArkivMsg[MAX_BYTES_PER_EQ];
    double  EvntTime;     /* Time of Event (Julian Sec)                                    */
    short   EvntYear;     /*   0-  1   0-  3 Event Year                                    */
    short   EvntMonth;    /*   2-  3   4-  5 Event Month                                   */
    short   EvntDay;      /*   4-  5   6-  7 Event Day                                     */
    short   EvntHour;     /*   6-  7   8-  9 Event Hour                                    */
    short   EvntMin;      /*   8-  9  10- 11 Event Minute                                  */
    float   EvntSec;      /*  10- 13  12- 15 Event Second*100                              */
    float   EvntLat;      /*  14- 20  16- 22 Geog coord of Event                           */
    float   EvntLon;      /*  21- 28  23- 30 Geog coord of Event                           */
    float   EvntDepth;    /*  29- 33  31- 35 Depth of Event*100                            */
    float   Smag;         /*  34- 35  36- 38 Mag from max S amplitude from NCSN stations   */
    long    NumPhs;       /*  36- 38  39- 41 # phases used in solution (final weights > 0.1) */
    long    Gap;          /*  39- 41  42- 44 Maximum azimuthal gap                         */
    float   Dmin;         /*  42- 44  45- 47 Minimum distance to site                      */
    float   rms;          /*  45- 48  48- 51 RMS travel time residual*100                  */
    float   mag;          /*  67- 68  70- 72 Coda duration mag                             */
    float   erh;          /*  80- 83  85- 88 Horizontal error*100 (km)                     */
    float   erz;          /*  84- 87  89- 92 Vertical error*100 (km)                       */
    char    ExMagType;    /*     114     122 "External" magnitude label or type code       */
    float   ExMag;        /* 115-117 123-125 "External" magnitude                          */
    float   ExMagWgt;     /* 118-120 126-128 Total of the "external" mag weights (~ number of readings) */
    char    AltMagType;   /*     121     129 Alt amplitude magnitude label or type code    */
    float   AltMag;       /* 122-124 130-132 Alt amplitude magnitude                       */
    float   AltMagWgt;    /* 125-127 133-135 Total of the alt amplitude mag weights (~ number of readings) */
    char    EvntIDS[10];  /* 128-137 136-145 Event ID                                      */
    long    EvntID;       /* Event ID                                                      */
    char    PreMagType;   /*     138     146 Preferred magnitude label code chosen from those available */
    float   PreMag;       /* 139-141 147-149 Preferred magnitude                           */
    float   PreMagWgt;    /* 142-144 150-153 Total of the preferred mag weights (~ number of readings) */
    char    AltDmagType;  /*     145     154 Alt coda dur. magnitude label or type code    */
    float   AltDmag;      /* 146-148 155-157 Alt coda dur. magnitude                       */
    float   AltDmagWgt;   /* 149-151 158-161 Total of the alternate coda duration magnitude weights (~ number of readings) */

    long    MaxChannels;  /*  */
    long    index[MAXCHANNELS];
} Arkive;

/*****************************************************************************
 *  Define the structure for Station information.                            *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/

typedef struct Instance {      /* A channel information structure            */
    char    Comp[5];           /* Component                                  */
    char    Loc[5];            /* Loc                                        */

    char    SCN[17];           /* SCN                                        */
    char    SCNtxt[20];        /* S C N                                      */
    char    SCNnam[20];        /* S_C_N                                      */
    char    SiteName[50];      /* Common Name of Site                        */
    double  Lat;               /* Latitude                                   */
    double  Lon;               /* Longitude                                  */
    double  Elev;              /* Elevation                                  */
    
    double  Inst_gain;         /* Gain of instrument (microv/count)          */
    int     Sens_type;         /* Type of sensor                             */
    double  Sens_gain;         /* Gain of sensor (volts/unit)                */
    int     Sens_unit;         /* Sensor units d=1; v=2; a=3                 */
    double  GainFudge;         /* Additional gain factor.                    */
    double  SiteCorr;          /* Site correction factor.                    */
    double  sensitivity;       /* Channel sensitivity  counts/units          */
    int		ShkQual;           /* Station (Chan) type                        */
    
    int		range;             /* Range of digital counts                    */
    double  stddev;            /* Std Dev of digital counts                  */
    double  ratio;             /* Ratio of the range/stddev                  */
} Instance;

/*****************************************************************************
 *  Define the structure for Station information.                            *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/

typedef struct StaInfo {       /* A channel information structure            */
    char    Site[6];           /* Site                                       */
    char    Net[5];            /* Net                                        */
    int     Ninst;             /* Number of data streams for this site       */
    double  logA;              /* Number of data streams for this site       */
    char    SiteName[50];      /* Common Name of Site                        */
    double  Lat;               /* Latitude                                   */
    double  Lon;               /* Longitude                                  */
    double  Elev;              /* Elevation                                  */
    double  Dist;              /* Distance                                   */
    double  Stime;             /* Start time for plot                        */
    Instance SCNL[20];         /* Data stream metadata                       */
} StaInfo;

/*****************************************************************************
 *  Define the structure for Channel information.                            *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/

typedef struct ChanInfo {      /* A channel information structure            */
    char    Site[6];           /* Site                                       */
    char    Comp[5];           /* Component                                  */
    char    Net[5];            /* Net                                        */
    char    Loc[5];            /* Loc                                        */
    char    SCN[15];           /* SCN                                        */
    char    SCNtxt[17];        /* S C N                                      */
    char    SCNnam[17];        /* S_C_N                                      */
    char    SiteName[50];      /* Common Name of Site                        */
    char    Descript[200];     /* Common Name of Site                        */
    double  Lat;               /* Latitude                                   */
    double  Lon;               /* Longitude                                  */
    double  Elev;              /* Elevation                                  */
    
    int     Inst_type;         /* Type of instrument                         */
    double  Inst_gain;         /* Gain of instrument (microv/count)          */
    int     Sens_type;         /* Type of sensor                             */
    double  Sens_gain;         /* Gain of sensor (volts/unit)                */
    int     Sens_unit;         /* Sensor units d=1; v=2; a=3                 */
    double  GainFudge;         /* Additional gain factor.                    */
    double  SiteCorr;          /* Site correction factor.                    */
    double  sensitivity;       /* Channel sensitivity  counts/units          */
    int		ShkQual;           /* Station (Chan) type                        */
    
    double  Scale;             /* Scale factor [Data] (in)                   */
    double  Scaler;            /* Scale factor [Data] (in)                   */
} ChanInfo;

/*****************************************************************************
 *  Define the structure for the Plotting parameters.                        *
 *****************************************************************************/
#define WHITE  0
#define BLACK  1
#define RED    2
#define BLUE   3
#define GREEN  4
#define GREY   5
#define YELLOW 6
#define TURQ   7
#define PURPLE 8

/*****************************************************************************
 *  Define the structure for the individual Global thread.                   *
 *  This is the private area the thread needs to keep track                  *
 *  of all those variables unique to itself.                                 *
 *****************************************************************************/

struct Global {
    int     Debug;             /*                                            */
    int     WSDebug;           /*                                            */

    int     got_a_menu;
    
    char    TraceBuf[MAXTRACEBUF]; /* This should work for 24-bit digitizers */
    char    mod[20];
    
    char    loctarget[100];    /* Target in form-> /directory/      */
    
    pid_t   pid;
    WS_MENU_QUEUE_REC menu_queue[MAX_WAVESERVERS];
    
    int     SecsToWait;    /* Number of seconds to wait before starting plot */
    
    int     NSCN;              /* Number of SCNs we know about               */
    ChanInfo    Chan[MAXCHANNELS];
    
    int     Nsta;              /* Number of SCNs we know about               */
    StaInfo    Sta[MAXCHANNELS];
    int     NumIndexedSta;
    int     StaIndex[MAXCHANNELS];
    
/* Globals to set from configuration file
 ****************************************/
    long    wsTimeout;         /* seconds to wait for reply from ws          */
    int     nServer;           /* number of wave servers we know about       */
    long    RetryCount;        /* Retry count for waveserver errors.         */
                        /* list of available waveServers, from config. file  */
    int     inmenu[MAX_WAVESERVERS];
    char    wsIp[MAX_WAVESERVERS][MAX_ADRLEN];
    char    wsPort[MAX_WAVESERVERS][MAX_ADRLEN];
    char    wsComment[MAX_WAVESERVERS][50];

/* Variables used during the generation of each plot
 ***************************************************/
    long    samp_sec;          /* samples/sec                                */
    long    Npts;              /* Number of points in trace                  */
    double  Mean;              /* Mean value of the last data gulp           */
    double  Min;               /* Min value of the last data gulp            */
    double  Max;               /* Max value of the last data gulp            */
    double  MaxDistance;       /* Maximum offset distance for traces in this plot. */
    
    char    GifName[175];      /* Name of this gif file on host webserver    */
    char    TmpName[175];      /* Name of this gif file on host webserver    */
    char    LocalGif[175];     /* Name of this gif file on local machine     */
    long    gcolor[MAXCOLORS]; /* GIF colors                                 */
    gdImagePtr    GifImage;

    double  MinLogA;           /* Minimum LogA for selection of sites  */

    int     nentries;          /* Number of menu entries for this SCN        */
    double  TStime[MAX_WAVESERVERS*2]; /* Tank start for this entry          */
    double  TEtime[MAX_WAVESERVERS*2]; /* Tank end for this entry            */
    int     index[MAX_WAVESERVERS*2];  /* WaveServer for this entry          */
    
    int     nStaDB;            /* number of station databases we know about          */
    char    stationList[MAX_STADBS][STALIST_SIZ];
    
    char    indir[GDIRSZ];     /* Directory for manual-input arc files               */
    int     arcfileflg;        /* Flag set if we are accepting arc files             */
    int     UseDST;            /* Daylight Savings Time used when needed             */
    int     xpix, ypix;        /* Size of data plot (pixels)                 */
    double  xsize, ysize;      /* Overall Size of plot (inches)              */
    int     mins;              /* # of minutes per display line              */
    double  axexmax;           /* max axe x position [Data] (in)             */
    double  axeymax;           /* max axe y position [Data] (in)             */
    int     TotMins;           /* Total minutes to plot                      */
    
    short   LocalTime;         /* Offset of local time from GMT e.g. -7 = PST        */
    char    LocalTimeID[4];    /* Local time ID e.g. PST                             */
    long    MaxChannels;       /*                                                    */
    long    MaxDist;           /* Maximum offset distance for traces to plot.        */
    double  MinSize;           /* Minimum size event to report                       */
    long    MaxDays;           /* Maximum number of days in event list.              */

    int     DeciFlag;          /* Flag for decimation of trace            */

    double  Scale;             /* Scale factor [Data] (in)                */
    double  Scaler;            /* Scale factor [Data] (in)                */
    char    SCN[15];           /* SCN                                     */
    char    SCNtxt[17];        /* S C N                                   */
    char    SCNnam[17];        /* S_C_N                                   */

    int		Inst_type;         /* Type of instrument                      */
    double  Inst_gain;         /* Gain of instrument (microv/count)       */
    int		Sens_type;         /* Type of sensor                          */
    double  Sens_gain;         /* Gain of sensor (volts/unit)             */
    int		Sens_unit;         /* Sensor units d=1; v=2; a=3              */
    double  sensitivity;       /* Channel sensitivity  counts/units       */

    char    Site[5];           /*  0- 3 Site                              */
    char    Comp[5];           /* Component                               */
    char    Net[5];            /* Net                                     */
    char    Loc[5];            /* Location                                */
    char    Comment[50];       /* Description (for web page)              */
        
    Arkive  Ark;               /* Current arkive structure                */
};
typedef struct Global Global;

