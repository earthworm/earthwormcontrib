#
#   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
#   CHECKED IT OUT USING THE COMMAND CHECKOUT.
#
#    $Id: makefile.sol 2 2004-04-28 23:15:44Z dietz $
#
#    Revision history:
#     $Log$
#     Revision 1.1  2004/04/28 23:15:44  dietz
#     Initial revision
#
#

CFLAGS = ${GLOBALFLAGS}
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

SRCS = addpinno.c
OBJS = addpinno.o

LIBS = -lm -lposix4

EW_LIBS = $(OBJS) $L/logit.o $L/kom.o $L/getutil.o $L/sleep_ew.o \
           $L/time_ew.o $L/transport.o $L/swap.o

addpinno: $(EW_LIBS)
	cc -o $B/addpinno $(EW_LIBS) $(LIBS)

lint:
	lint addpinno.c $(GLOBALFLAGS)

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~ 

clean_bin:
	rm -f $B/addpinno*
