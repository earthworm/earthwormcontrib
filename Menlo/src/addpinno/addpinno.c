/*
 *   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT USING THE COMMAND CHECKOUT.
 *
 *    $Id: addpinno.c 326 2007-04-09 18:08:33Z dietz $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.3  2007/04/09 18:08:33  dietz
 *     Fixed time_t casts in prints and compares
 *
 *     Revision 1.2  2004/04/30 19:08:54  dietz
 *     modified to use location code
 *
 *     Revision 1.1.1.1  2004/04/28 23:15:44  dietz
 *     pre-location code Contrib/Menlo
 */

/*
 * addpinno.c
 *
 * Reads TYPE_TRACEBUF msgs one transport ring and modifies include 
 * a valid pinno number in the header. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include <kom.h>
#include <trace_buf.h>
#include <swap.h>

/* Functions in this source file
 *******************************/
void addpinno_config ( char * );
void addpinno_logparm( void );
void addpinno_lookup ( void );
void addpinno_status ( unsigned char, short, char * );
int  addpinno_compare( const void *s1, const void *s2 );

/* Globals
 *********/
static SHM_INFO  InRegion;    /* shared memory region to use for input  */
static SHM_INFO  OutRegion;   /* shared memory region to use for output */
static pid_t	 MyPid;       /* Our process id is sent with heartbeat  */

/* Things to read or derive from configuration file
 **************************************************/
static int     LogSwitch;          /* 0 if no logfile should be written */
static long    HeartbeatInt;       /* seconds between heartbeats        */
static int     Debug = 0;          /* 0=no debug msgs, non-zero=debug   */
static MSG_LOGO *GetLogo = NULL;   /* logo(s) to get from shared memory */
static short   nLogo     = 0;      /* # logos we're configured to get   */
static int     UseOriginalLogo=0;  /* 0=use addpinno's own logo on output msgs   */
                                   /* non-zero=use original logos on output    */
                                   /*   NOTE: this requires that output go to  */
                                   /*   different transport ring than input    */

/* Globals for site-component-net-loc filter
 *******************************************/
#define INCREMENT_SCNL 10          /* increment limit of # scnl's  */

typedef struct {
   char sta[TRACE2_STA_LEN];       /* original station name  */
   char chan[TRACE2_CHAN_LEN];     /* original channel name  */
   char net[TRACE2_NET_LEN];       /* original network name  */
   char loc[TRACE2_LOC_LEN];       /* original location name */
   int  pinno;                     /* pin number to assign   */
} SCNLstruct;
 
static SCNLstruct *SCNL = NULL;    /* dynamically allocated list channels */
static int     Max_SCNL = 0;       /* maximum number of SCNLs in list     */
static int        nSCNL = 0;       /* current number of channels list     */
  
/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          InRingKey;     /* key of transport ring for input   */
static long          OutRingKey;    /* key of transport ring for output  */
static unsigned char InstId;        /* local installation id             */
static unsigned char MyModId;       /* Module Id for this program        */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeTraceBuf;
static unsigned char TypeTraceBuf2;

/* Error messages used by addpinno
 *********************************/
#define  ERR_MISSGAP       0   /* sequence gap in transport ring         */
#define  ERR_MISSLAP       1   /* missed messages in transport ring      */
#define  ERR_TOOBIG        2   /* retreived msg too large for buffer     */
#define  ERR_NOTRACK       3   /* msg retreived; tracking limit exceeded */
static char  Text[150];        /* string for log/error messages          */

int main( int argc, char **argv )
{

   TracePacket   tpkt;             /* buffer for msgs from ring     */
   time_t        timeNow;          /* current time                  */
   time_t        timeLastBeat;     /* time last heartbeat was sent  */
   long          recsize;          /* size of retrieved message     */
   MSG_LOGO      reclogo;          /* logo of retrieved message     */
   MSG_LOGO      putlogo;          /* logo to use putting message into ring */
   int           res;
   unsigned char seq;

/* Initialize name of log-file & open it
 ***************************************/
   logit_init( argv[1], 0, 1024, 1 );

/* Check command line arguments
 ******************************/
   if ( argc != 2 )
   {
        logit( "e", "Usage: addpinno <configfile>\n" );
        exit( 0 );
   }

/* Read the configuration file(s)
 ********************************/
   addpinno_config( argv[1] );

/* Look up important info from earthworm.h tables
 ************************************************/
   addpinno_lookup();

/*  Set logit to LogSwitch read from configfile
 **********************************************/
   logit_init( argv[1], 0, 256, LogSwitch );
   logit( "" , "addpinno: Read command file <%s>\n", argv[1] );
   addpinno_logparm();

/* Check for different in/out rings if UseOriginalLogo is set
 ************************************************************/
   if( UseOriginalLogo  &&  (InRingKey==OutRingKey) ) 
   {
      logit ("e", "addpinno: InRing and OutRing must be different when"
                  "UseOriginalLogo is non-zero; exiting!\n");
      free( GetLogo );
      free( SCNL    );
      exit( -1 );
   }

/* Get our own process ID for restart purposes
 *********************************************/
   if( (MyPid = getpid()) == -1 )
   {
      logit ("e", "addpinno: Call to getpid failed. Exiting.\n");
      free( GetLogo );
      free( SCNL    );
      exit( -1 );
   }

/* Initialize outgoing logo
 **************************/
   putlogo.instid = InstId;
   putlogo.mod    = MyModId;

/* Attach to shared memory rings
 *******************************/
   tport_attach( &InRegion, InRingKey );
   logit( "", "addpinno: Attached to public memory region: %ld\n",
          InRingKey );
   tport_attach( &OutRegion, OutRingKey );
   logit( "", "addpinno: Attached to public memory region: %ld\n",
           OutRingKey );

/* Force a heartbeat to be issued in first pass thru main loop
 *************************************************************/
   timeLastBeat = time(&timeNow) - (time_t)HeartbeatInt - 1;

/* Flush the incoming transport ring on startup
 **********************************************/ 
   while( tport_copyfrom(&InRegion, GetLogo, nLogo, &reclogo,
          &recsize, tpkt.msg, MAX_TRACEBUF_SIZ, &seq ) != GET_NONE );

/*----------------------- setup done; start main loop -------------------------*/

  while ( tport_getflag( &InRegion ) != TERMINATE  &&
          tport_getflag( &InRegion ) != MyPid )
  {
     /* send addpinno's heartbeat
      ***************************/
        if( HeartbeatInt  &&  time(&timeNow)-timeLastBeat >= (time_t)HeartbeatInt )
        {
            timeLastBeat = timeNow;
            addpinno_status( TypeHeartBeat, 0, "" );
        }

     /* Get msg & check the return code from transport
      ************************************************/
        res = tport_copyfrom( &InRegion, GetLogo, nLogo, &reclogo, &recsize, 
                              tpkt.msg, MAX_TRACEBUF_SIZ, &seq );

        switch( res )
        {
        case GET_OK:      /* got a message, no errors or warnings         */
             break;

        case GET_NONE:    /* no messages of interest, check again later   */
             sleep_ew(100); /* milliseconds */
             continue;

        case GET_NOTRACK: /* got a msg, but can't tell if any were missed */
             sprintf( Text,
                     "Msg received (i%u m%u t%u); transport.h NTRACK_GET exceeded",
                      reclogo.instid, reclogo.mod, reclogo.type );
             addpinno_status( TypeError, ERR_NOTRACK, Text );
             break;

        case GET_MISS_LAPPED:     /* got a msg, but also missed lots      */
             sprintf( Text,
                     "Missed msg(s) from logo (i%u m%u t%u)",
                      reclogo.instid, reclogo.mod, reclogo.type );
             addpinno_status( TypeError, ERR_MISSLAP, Text );
             break;

        case GET_MISS_SEQGAP:     /* got a msg, but seq gap               */
             sprintf( Text,
                     "Saw sequence# gap for logo (i%u m%u t%u s%u)",
                      reclogo.instid, reclogo.mod, reclogo.type, seq );
             addpinno_status( TypeError, ERR_MISSGAP, Text );
             break;

       case GET_TOOBIG:  /* next message was too big, resize buffer      */
             sprintf( Text,
                     "Retrieved msg[%ld] (i%u m%u t%u) too big for tpkt.msg[%ld]",
                      recsize, reclogo.instid, reclogo.mod, reclogo.type,
                      MAX_TRACEBUF_SIZ );
             addpinno_status( TypeError, ERR_TOOBIG, Text );
             continue;

       default:         /* Unknown result                                */
             sprintf( Text, "Unknown tport_copyfrom result:%d", res );
             addpinno_status( TypeError, ERR_TOOBIG, Text );
             continue;
       }

    /* Add pinno number to configured channels
     *****************************************/
       if( reclogo.type == TypeTraceBuf2 ) 
       {
          SCNLstruct  key;
          SCNLstruct *match;

          WaveMsg2MakeLocal( &tpkt.trh2 );
          strcpy( key.sta,  tpkt.trh2.sta  );
          strcpy( key.chan, tpkt.trh2.chan );
          strcpy( key.net,  tpkt.trh2.net  );
          strcpy( key.loc,  tpkt.trh2.loc  );

          match = (SCNLstruct *)bsearch( &key, SCNL, nSCNL, sizeof(SCNLstruct),
                                         addpinno_compare );
          if( match != (SCNLstruct *) NULL )
          {
             if( Debug ) logit("","TYPE_TRACEBUF2: %s.%s.%s.%s pinno %d changed to %d\n",
                               tpkt.trh2.sta, tpkt.trh2.chan, tpkt.trh2.net, 
                               tpkt.trh2.loc, tpkt.trh2.pinno, match->pinno );
             tpkt.trh2.pinno = match->pinno;
          }
          else if( Debug )
          {
             logit("","TYPE_TRACEBUF2: %s.%s.%s.%s pinno %d not changed\n",
                   tpkt.trh2.sta, tpkt.trh2.chan, tpkt.trh2.net,
                   tpkt.trh2.loc, tpkt.trh2.pinno );
          }
       }
       else if( reclogo.type == TypeTraceBuf ) 
       {
          SCNLstruct  key;
          SCNLstruct *match;

          WaveMsgMakeLocal( &tpkt.trh );
          strcpy( key.sta,  tpkt.trh.sta    );
          strcpy( key.chan, tpkt.trh.chan   );
          strcpy( key.net,  tpkt.trh.net    );
          strcpy( key.loc,  LOC_NULL_STRING );

          match = (SCNLstruct *)bsearch( &key, SCNL, nSCNL, sizeof(SCNLstruct),
                                         addpinno_compare );
          if( match != (SCNLstruct *) NULL )
          {
             if( Debug ) logit("","TYPE_TRACEBUF:  %s.%s.%s pinno %d changed to %d\n",
                               tpkt.trh.sta, tpkt.trh.chan, tpkt.trh.net, 
                               tpkt.trh.pinno, match->pinno );
             tpkt.trh.pinno = match->pinno;
          }
          else if( Debug )
          {
             logit("","TYPE_TRACEBUF:  %s.%s.%s pinno %d not changed\n",
                   tpkt.trh.sta, tpkt.trh.chan, tpkt.trh.net, tpkt.trh.pinno );
          }
       }
       else
       {
          logit("t","addpinno: unknown message type: %d\n", reclogo.type );
          continue;
       }
      
       if( UseOriginalLogo ) {
          putlogo.instid = reclogo.instid;
          putlogo.mod    = reclogo.mod;
       }       
       putlogo.type = reclogo.type;

       if( tport_putmsg( &OutRegion, &putlogo, recsize, tpkt.msg ) != PUT_OK )
       {
          logit("et","addpinno: Error writing %d-byte msg to ring; "
                     "original logo (i%u m%u t%u)\n", recsize,
                      reclogo.instid, reclogo.mod, reclogo.type );
       }
   }

/*-----------------------------end of main loop-------------------------------*/

/* free allocated memory */
   free( GetLogo );
   free( SCNL    );
 
/* detach from shared memory */
   tport_detach( &InRegion );
   tport_detach( &OutRegion );
           
/* write a termination msg to log file */
   logit( "t", "addpinno: Termination requested; exiting!\n" );
   fflush( stdout );
   return( 0 );
}

/******************************************************************************
 *  addpinno_config() processes command file(s) using kom.c functions;        *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
#define ncommand 9         /* # of required commands you expect to process   */
void addpinno_config( char *configfile )
{
   char  init[ncommand];   /* init flags, one byte for each required command */
   int   nmiss;            /* number of required commands that were missed   */
   char *com;
   char *str;
   int   nfiles;
   int   success;
   int   i;

/* Set to zero one init flag for each required command
 *****************************************************/
   for( i=0; i<ncommand; i++ )  init[i] = 0;
   nLogo = 0;

/* Open the main configuration file
 **********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 ) {
        logit( "e",
               "addpinno: Error opening command file <%s>; exiting!\n",
                configfile );
        exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  logit( "e",
                         "addpinno: Error opening command file <%s>; exiting!\n",
                          &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                if( LogSwitch<0 || LogSwitch>2 ) {
                   logit( "e",
                          "addpinno: Invalid <LogFile> value %d; "
                          "must = 0, 1 or 2; exiting!\n", LogSwitch );
                   exit( -1 );
                }
                init[0] = 1;
            }

  /*1*/     else if( k_its("MyModuleId") ) {
                if( str=k_str() ) {
                   if( GetModId( str, &MyModId ) != 0 ) {
                      logit( "e",
                             "addpinno: Invalid module name <%s> "
                             "in <MyModuleId> command; exiting!\n", str);
                      exit( -1 );
                   }
                }
                init[1] = 1;
            }

  /*2*/     else if( k_its("InRing") ) {
                if( str=k_str() ) {
                   if( ( InRingKey = GetKey(str) ) == -1 ) {
                      logit( "e",
                             "addpinno: Invalid ring name <%s> "
                             "in <InRing> command; exiting!\n", str);
                      exit( -1 );
                   }
                }
                init[2] = 1;
            }

  /*3*/     else if( k_its("OutRing") ) {
                if( str=k_str() ) {
                   if( ( OutRingKey = GetKey(str) ) == -1 ) {
                      logit( "e",
                             "addpinno: Invalid ring name <%s> "
                             "in <OutRing> command; exiting!\n", str);
                      exit( -1 );
                   }
                }
                init[3] = 1;
            }

  /*4*/     else if( k_its("HeartbeatInt") ) {
                HeartbeatInt = k_long();
                init[4] = 1;
            }

         /* Enter installation & module to get messages from
          **************************************************/
  /*5*/     else if( k_its("GetLogo") ) {
                MSG_LOGO *tlogo = NULL;
                tlogo = (MSG_LOGO *)realloc( GetLogo, (nLogo+2)*sizeof(MSG_LOGO) );
                if( tlogo == NULL )
                {
                   logit( "e", "addpinno: GetLogo: error reallocing"
                           " %d bytes; exiting!\n",
                           (nLogo+2)*sizeof(MSG_LOGO) );
                   exit( -1 );
                }
                GetLogo = tlogo;

                if( str=k_str() ) {
                   if( GetInst( str, &GetLogo[nLogo].instid ) != 0 ) {
                       logit( "e",
                              "addpinno: Invalid installation name <%s>"
                              " in <GetLogo> cmd; exiting!\n", str );
                       exit( -1 );
                   }
                   GetLogo[nLogo+1].instid = GetLogo[nLogo].instid;
                   if( str=k_str() ) {
                      if( GetModId( str, &GetLogo[nLogo].mod ) != 0 ) {
                          logit( "e",
                                 "addpinno: Invalid module name <%s>"
                                 " in <GetLogo> cmd; exiting!\n", str );
                          exit( -1 );
                      }
                      GetLogo[nLogo+1].mod = GetLogo[nLogo].mod;
                      if( GetType( "TYPE_TRACEBUF2", &GetLogo[nLogo].type ) != 0 ) {
                          logit( "e",
                                 "addpinno: Invalid message type <TYPE_TRACEBUF2>" 
                                 "; exiting!\n" );
                          exit( -1 );
                      }
                      if( GetType( "TYPE_TRACEBUF", &GetLogo[nLogo+1].type ) != 0 ) {
                          logit( "e",
                                 "addpinno: Invalid message type <TYPE_TRACEBUF>" 
                                 "; exiting!\n" );
                          exit( -1 );
                      }
                   }
                }
                nLogo+=2;
                init[5] = 1;
            }

  /*6*/     else if( k_its("Debug") ) {
                Debug = k_int();
                init[6] = 1;
            }

  /*7old*/  else if( k_its("SCNpin") )  /* original pre-location code cmd */
            {
                int    iarg;
                size_t size;

                if( nSCNL >= Max_SCNL )
                {
                    Max_SCNL += INCREMENT_SCNL;
                    size      = Max_SCNL * sizeof( SCNLstruct );
                    SCNL      = (SCNLstruct *) realloc( SCNL, size );
                    if( SCNL == NULL )
                    {
                       logit( "e",
                              "addpinno: Error allocating %d bytes"
                              " for SCNLpin list; exiting!\n", size );
                       exit( -1 );
                    }
                }
                for( iarg=1; iarg<=3; iarg++ )   /* Read SCN */
                {
                   str = k_str();
                   if( !str ) break;             /* no string; error! */
                   if( iarg==1 )  /* original station code */
                   {
                      if( strlen(str)>=(size_t)TRACE2_STA_LEN ) break;
                      strcpy(SCNL[nSCNL].sta,str);
                      continue;
                   }
                   if( iarg==2 ) /* original component code */
                   {
                      if( strlen(str)>=(size_t)TRACE2_CHAN_LEN ) break;
                      strcpy(SCNL[nSCNL].chan,str);
                      continue;
                   }
                   if( iarg==3 ) /* original network code */
                   {
                      if( strlen(str)>=(size_t)TRACE2_NET_LEN ) break;
                      strcpy(SCNL[nSCNL].net,str);
                   }
                }
                strcpy(SCNL[nSCNL].loc,LOC_NULL_STRING);  /* default NULL loc code */ 
                SCNL[nSCNL].pinno = k_int();
                nSCNL++;
                init[7]++;
            }

  /*7*/     else if( k_its("SCNLpin") )  /* cmd with location code */
            {
                int    iarg;
                size_t size;

                if( nSCNL >= Max_SCNL )
                {
                    Max_SCNL += INCREMENT_SCNL;
                    size      = Max_SCNL * sizeof( SCNLstruct );
                    SCNL      = (SCNLstruct *) realloc( SCNL, size );
                    if( SCNL == NULL )
                    {
                       logit( "e",
                              "addpinno: Error allocating %d bytes"
                              " for SCNLpin list; exiting!\n", size );
                       exit( -1 );
                    }
                }
                for( iarg=1; iarg<=4; iarg++ )   /* Read SCNL */
                {
                   str = k_str();
                   if( !str ) break;             /* no string; error! */
                   if( iarg==1 )  /* original station code */
                   {
                      if( strlen(str)>=(size_t)TRACE2_STA_LEN ) break;
                      strcpy(SCNL[nSCNL].sta,str);
                      continue;
                   }
                   if( iarg==2 ) /* original component code */
                   {
                      if( strlen(str)>=(size_t)TRACE2_CHAN_LEN ) break;
                      strcpy(SCNL[nSCNL].chan,str);
                      continue;
                   }
                   if( iarg==3 ) /* original network code */
                   {
                      if( strlen(str)>=(size_t)TRACE2_NET_LEN ) break;
                      strcpy(SCNL[nSCNL].net,str);
                   }
                   if( iarg==4 ) /* original location code */
                   {
                      if( strlen(str)>=(size_t)TRACE2_LOC_LEN ) break;
                      strcpy(SCNL[nSCNL].loc,str);
                   }

                }
                SCNL[nSCNL].pinno = k_int();
                nSCNL++;
                init[7]++;
            }
        
  /*8*/     else if( k_its("UseOriginalLogo") )
            {
                UseOriginalLogo = k_int();
                init[8] = 1;
            }

         /* Unknown command
          *****************/
            else {
                logit( "e", "addpinno: <%s> Unknown command in <%s>.\n",
                       com, configfile );
                continue;
            }

        /* See if there were any errors processing the command
         *****************************************************/
            if( k_err() ) {
               logit( "e",
                      "addpinno: Bad <%s> command in <%s>; exiting!\n",
                       com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       logit( "e", "addpinno: ERROR, no " );
       if ( !init[0] )  logit( "e", "<LogFile> "         );
       if ( !init[1] )  logit( "e", "<MyModuleId> "      );
       if ( !init[2] )  logit( "e", "<InRing> "          );
       if ( !init[3] )  logit( "e", "<OutRing> "         );
       if ( !init[4] )  logit( "e", "<HeartbeatInt> "    );
       if ( !init[5] )  logit( "e", "<GetLogo> "         );
       if ( !init[6] )  logit( "e", "<Debug> "           );
       if ( !init[7] )  logit( "e", "<SCNLpin> "         );
       if ( !init[8] )  logit( "e", "<UseOriginalLogo> " );
       logit( "e", "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }

/* Sort list of SCNL's that we're handling
 ****************************************/
   qsort( SCNL, nSCNL, sizeof(SCNLstruct), addpinno_compare );

   return;
}

/******************************************************************************
 *  addpinno_logparm( )   Log operating params                                *
 ******************************************************************************/
void addpinno_logparm( void )
{
   int i;
   logit("","MyModuleId:         %u\n",  MyModId );
   logit("","InRing key:         %ld\n", InRingKey );
   logit("","OutRing key:        %ld\n", OutRingKey );
   logit("","HeartbeatInt:       %ld sec\n", HeartbeatInt );
   logit("","LogFile:            %d\n",  LogSwitch );
   logit("","Debug:              %d\n",  Debug );
   for(i=0;i<nLogo;i++)  logit("","GetLogo[%d]:         i%u m%u t%u\n", i,
                               GetLogo[i].instid, GetLogo[i].mod, 
                               GetLogo[i].type );
   logit("","UseOriginalLogo:    %d\n", UseOriginalLogo );
   logit("", "Add pinno to %d channels:", nSCNL );
   for( i=0; i<nSCNL; i++ ) {
      logit("","\n    channel[%d]: %5s %3s %2s %2s %6d",
             i, SCNL[i].sta, SCNL[i].chan, SCNL[i].net, SCNL[i].loc, SCNL[i].pinno );
   }   
   logit("","\n" );

   return;
}


/******************************************************************************
 *  addpinno_lookup( )   Look up important info from earthworm tables         *
 ******************************************************************************/

void addpinno_lookup( void )
{

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      logit( "e",
             "addpinno: error getting local installation id; exiting!\n" );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      logit( "e",
             "addpinno: Invalid message type <TYPE_HEARTBEAT>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      logit( "e",
             "addpinno: Invalid message type <TYPE_ERROR>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF", &TypeTraceBuf ) != 0 ) {
      logit( "e",
             "addpinno: Invalid message type <TYPE_TRACEBUF>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF2", &TypeTraceBuf2 ) != 0 ) {
      logit( "e",
             "addpinno: Invalid message type <TYPE_TRACEBUF2>; exiting!\n" );
      exit( -1 );
   }

   return;
}


/******************************************************************************
 * addpinno_status() builds a heartbeat or error message & puts it into       *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/
void addpinno_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size;
   time_t      t;

/* Build the message
 *******************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
        sprintf( msg, "%ld %ld\n", (long)t, MyPid );
   }
   else if( type == TypeError )
   {
        sprintf( msg, "%ld %hd %s\n", (long)t, ierr, note);
        logit( "et", "addpinno: %s\n", note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &OutRegion, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","addpinno:  Error sending heartbeat.\n" );
        }
        else if( type == TypeError ) {
           logit("et","addpinno:  Error sending error:%d.\n", ierr );
        }
   }

   return;
}


/******************************************************************************
 *  addpinno_compare()  This function is passed to qsort() * bsearch() so     *
 *     we can sort the pick list by site,chan,net.loc codes  and then look    *
 *     up a station efficiently in the list.                                  *
 ******************************************************************************/
int addpinno_compare( const void *s1, const void *s2 )
{
   int rc;
   SCNLstruct *t1 = (SCNLstruct *) s1;
   SCNLstruct *t2 = (SCNLstruct *) s2;

   rc = strcmp( t1->sta, t2->sta );
   if ( rc != 0 ) return rc;

   rc = strcmp( t1->chan, t2->chan );
   if ( rc != 0 ) return rc;

   rc = strcmp( t1->net,  t2->net );
   if ( rc != 0 ) return rc;

   rc = strcmp( t1->loc,  t2->loc );
   return rc;
}


