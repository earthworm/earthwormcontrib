# addpinno configuration file

#  Basic Earthworm setup:
#
 MyModuleId    MOD_ADDPINNO  # module id for this instance of addpinno 
 InRing        RAW_RING      # shared memory ring for input
 OutRing       WAVE_RING     # shared memory ring for output
 HeartbeatInt  30            # seconds between heartbeats
 LogFile       1             # 0 log to stderr/stdout only 
                             # 1 log to stderr/stdout and to disk file
                             # 2 log to disk file only
 Debug         0

# List the message logos to grab from InRing, WILDCARDs permitted.
# Multiple "GetLogo" commands are allowed, with no hardcoded limit.
#         Installation  Module        # Message Types (hardcoded)
#-----------------------------------------------------------------
 GetLogo  INST_CIT      MOD_WILDCARD  # TYPE_TRACEBUF2 & TYPE_TRACEBUF

# Output logo behavior (for tracedata msgs only)
#------------------------------------------------
 UseOriginalLogo  1          # 0  apply addpinno's logo to any messages that
                             #    it writes to OutRing (normal Earthworm 
                             #    behavior)
                             # non-zero means apply the original logo to any
                             #    tracedata messages that are processed.
                             #    This also requires that InRing and OutRing 
                             #    be different to avoid "missed message" or
                             #    "sequence gap" storms in tbuf-reading modules.

# Change pin numbers on these channels (no wildcards allowed!)
#-------------------------------------------------------------
#         sta cmp net loc  pinno
 SCNLpin  PHL HHZ  CI --   1
 SCNLpin  PHL HHN  CI --   2
 SCNLpin  PHL HHE  CI --   3

