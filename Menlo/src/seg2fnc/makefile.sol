#
#                     Make file for seg2fnc
#                         Solaris Version
#
#  The posix4 library is required for nanosleep.
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = seg2fnc.o getDateTime.o createFileName.o file_sol.o \
    $L/swap.o $L/dirops_ew.o $L/kom.o $L/logit.o $L/time_ew.o \
    $L/sleep_ew.o

all: seg2fnc

seg2fnc: $O
	cc -o $B/seg2fnc $O -lm -lsocket -lnsl -lposix4


clean:
	/bin/rm -f seg2fnc *.o

clean_bin:
	/bin/rm -f seg2fnc
