/*
 *   file_sol.c
 */

#include <stdio.h>
#include <unistd.h>             /* for link */
#include <string.h>
#include <stdlib.h>
#include <dirent.h>             /* for opendir */
#include <sys/types.h>          /* for opendir */
#include <sys/stat.h>
#include <libgen.h>             /* for mkdirp */
#include "seg2fnc.h"

static int first = 1;
static DIR *dp = NULL;          /* Directory pointer */


/****************************************************************
 *  fopen_excl()                                                *
 *                                                              *
 *  Solaris doesn't have an exclusive open. So it is up to the  *
 *  applications feeding this program to make sure the file is  *
 *  complete.                                                   *
 ****************************************************************/

FILE *fopen_excl( const char *fname, const char *mode )
{
    return fopen( fname, mode );
}


/***************************************************************
 *  GetFirstName()                                             *
 *                                                             *
 *  Get the name of the first file in the current directory.   *
 *                                                             *
 *  Returns  0 if all OK                                       *
 *          -1 if no files were found                          *
 ***************************************************************/

int GetFirstName( char fname[] )
{
   struct stat ss;

   dp = opendir( "." );
   if ( dp == NULL ) return -1;        /* Error opening directory */

   do
   {
      struct dirent *dentp = readdir( dp );
      if ( dentp == NULL )             /* No more files */
      {
         closedir( dp );
         first = 1;
         return -1;
      }
      strcpy( fname, dentp->d_name );

      if ( stat( fname, &ss ) == -1 )  /* Get file status */
      {
         closedir( dp );
         first = 1;
         return -1;
      }
   } while ( !S_ISREG(ss.st_mode) );   /* Get only regular files */
                                       /* not directories */
   return 0;
}


/***************************************************************
 *  GetNextName()                                              *
 *                                                             *
 *  Get the name of the next file in the current directory.    *
 *                                                             *
 *  Returns  0 if all ok                                       *
 *          -1 if no files were found                          *
 ***************************************************************/

int GetNextName( char fname[] )
{
   struct stat ss;

   do
   {
      struct dirent *dentp = readdir( dp );
      if ( dentp == NULL )             /* No more files */
      {
         closedir( dp );
         first = 1;
         return -1;
      }
      strcpy( fname, dentp->d_name );

      if ( stat( fname, &ss ) == -1 )  /* Get file status */
      {
         closedir( dp );
         first = 1;
         return -1;
      }
   } while ( !S_ISREG(ss.st_mode) );   /* Get only regular files */
                                       /* not directories */
   return 0;
}


/***************************************************************
 *  GetFileNameDir()                                           *
 *                                                             *
 *  Get the name of a file in the current directory.           *
 *                                                             *
 *  Returns  0 if all ok                                       *
 *          -1 if no files were found                          *
 ***************************************************************/

int GetFileNameDir( char fname[] )
{
   if ( first )
   {
      first = 0;
      return GetFirstName( fname );
   }
   return GetNextName( fname );
}


/***************************************************************
 *  MoveFile2()                                                *
 *                                                             *
 *  Move file fname to directory outdir.                       *
 *  The output directory must be on the same file system       *
 *  (partition).                                               *
 *                                                             *
 *  Returns  0 if all ok                                       *
 *          -1 if the file cannot be moved                     *
 ***************************************************************/

int MoveFile2( char fname[], char outdir[] )
{
   int  rc;
   char outname[256];

   strcpy( outname, outdir );
   strcat( outname, "/" );
   strcat( outname, fname );

   if ( link( fname, outname ) == -1 )
   {
      printf( "link() error.\n" );
      return -1;
   }

   if ( unlink( fname ) == -1 )
   {
      printf( "unlink() error.\n" );
      return -1;
   }
   return 0;
}


/***************************************************************
 *  MakeFileReadWrite()                                        *
 *                                                             *
 *  Makes file read/write by everyone.                         *
 *                                                             *
 *  Returns  0 if permissions are changed                      *
 *          -1 if failed                                       *
 ***************************************************************/

int MakeFileReadWrite( char fname[] )
{
   return chmod( fname, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );
}
