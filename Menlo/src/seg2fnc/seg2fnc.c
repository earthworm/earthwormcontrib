
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm_simple_funcs.h>
#include "seg2fnc.h"

/* Program seg2fnc.
   This program renames SEG2 files produced by the GERI system.
   The new file names contain the acquisition date of the data.

   Program argument 1: Name of directory containing files to rename.
   All SEG2 files in this directory are renamed.
   All non-SEG2 files are unaffected.

   Program argument 2: Name of output directory.
   After SEG2 files are renamed, they are moved to the output directory.

   Program argument 3: Milliseconds to pause before reading file.
   After a file is found in input directory, wait this many milliseconds
   before trying to read and rename it. This allows time for a writing
   program with unknown behavior to complete writing and close the file.
   Required because it seems that the exclusive open is not doing it
   for us on Windows. This argument is optional.
*/


int main( int argc, char *argv[] )
{
   int  rc;
   char workingDir[STRLEN];      /* STRLEN defined in seg2fnc.h */
   char stringDate[STRLEN];
   char stringTime[STRLEN];
   char oldFileName[STRLEN];
   char newFileName[STRLEN+16];
   char outdir[STRLEN];
   int  pause=0;

/* Test number of arguments
   ************************/
   if ( argc < 3 )
   {
      printf( "Usage: %s <indir> <outdir> <optional:pause-millisec>\n",
               argv[0] );
      return -1;
   }

/* Change working directory
   ************************/
   strcpy( workingDir, "." );
   if ( argc > 1 ) strcpy( workingDir, argv[1] );

   rc = chdir_ew( workingDir );
   if ( rc == -1 )
   {
      printf( "%s: Error changing working directory to: %s; "
              "exiting.\n", argv[0], workingDir );
      return -1;
   }

/* Set the output directory to the second command line argument.
   If there are less than two arguments, the files aren't moved.
   ************************************************************/
   strcpy( outdir, "" );
   if ( argc > 2 ) strcpy( outdir, argv[2] );

/* Set the pause duration to the third command line argument.
   If there are less than three arguments, don't pause.
   ************************************************************/
   if ( argc > 3 ) pause = atoi( argv[3] );

/* Every so often, check all files in workingDir to see if
   any new SEG2 files appeared.  If so, renamed them and
   optionally move them to another directory.  This program
   loops forever.
   ********************************************************/
   while ( 1 )
   {

/* Loop through all files in working directory.
   GetFileName() runs on Windows or Solaris.
   *******************************************/
      while ( GetFileNameDir( oldFileName ) == 0 )
      {

/* Pause before reading file if configured to do so
   ************************************************/
         sleep_ew( pause );

/* Decode the SEG2 string headers to get the
   acquisition date and time
   *****************************************/
         rc = GetDateTime( oldFileName, stringDate, stringTime );
         if ( rc != 0 ) continue;    /* Failure */

/* Create a new file name from the acquisition date and time
   *********************************************************/
         rc = CreateFileName( stringDate, stringTime, oldFileName, newFileName );
         if ( rc != 0 ) continue;    /* Failure */

/* Rename the file using rename_ew(), which is system-independent
   **************************************************************/
         rc = rename_ew( oldFileName, newFileName );
         if ( rc == -1 ) continue;   /* Failure */
         printf( "%s: File %s renamed to %s\n", 
                 argv[0], oldFileName, newFileName );

/* Change permissions on new filename
   **********************************/
         rc = MakeFileReadWrite( newFileName );
         if ( rc == -1 ) { 
            printf( "%s: error setting file permission to read/write on %s",
                    argv[0], newFileName );
         }

/* Move the renamed file to another directory
   ******************************************/
         if ( strlen(outdir) > 0 )
         {
            rc = MoveFile2( newFileName, outdir );
            if ( rc != 0 ) continue;   /* Failure */
            printf( "%s: File %s moved to %s\n", 
                    argv[0], newFileName, outdir );
         }
      }
      sleep_ew( 1000 );
   }
}
