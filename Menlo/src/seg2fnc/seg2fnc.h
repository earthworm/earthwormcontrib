
/*   seg2fnc.h    */

#ifndef SEG2FNC_H
#define SEG2FNC_H

#define STRLEN 256

void GetConfig( char * );
void LogConfig( void );
FILE *fopen_excl( const char *, const char * );
int  GetDateTime( char seg2fname[], char stringDate[], char stringTime[] );
int  CreateFileName( char stringDate[], char stringTime[], char oldFileName[], char newFileName[] );
int  RenameFile( char seg2fname[], char newFileName[] );
int  GetFileNameDir( char fname[] );
int  MoveFile2( char fname[], char outdir[] );
int  MakeFileReadWrite( char fname[] );

#endif
