/*
 *  file_nt.c
 */

#include <stdio.h>
#include <share.h>
#include <windows.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <direct.h>
#include <io.h>
#include "seg2fnc.h"

static int first = 1;
static HANDLE fileHandle;

/****************************************************************
 *                          fopen_excl()                        *
 *                        Windows version                       *
 *                                                              *
 *  Open a disk file exclusively.                               *
 *  Returns a file pointer if the file was opened exclusively.  *
 *  Returns NULL if the file can't be opened.                   *
 ****************************************************************/


FILE *fopen_excl( const char *fname, const char *mode )
{
    return _fsopen( fname, mode, _SH_DENYRW );
}


/**************************************************************
 *  WantFile()                                                *
 *                                                            *
 * Determine if we want a file, based on its attributes.      *
 *                                                            *
 * Returns 1, if we want the file                             *
 *         0, if we don't want the file                       *
 **************************************************************/

int WantFile( DWORD attrib )
{
   int wantIt = 1;
   if ( attrib & FILE_ATTRIBUTE_COMPRESSED ) wantIt = 0;
   if ( attrib & FILE_ATTRIBUTE_DIRECTORY  ) wantIt = 0;
   if ( attrib & FILE_ATTRIBUTE_ENCRYPTED  ) wantIt = 0;
   if ( attrib & FILE_ATTRIBUTE_HIDDEN     ) wantIt = 0;
   if ( attrib & FILE_ATTRIBUTE_OFFLINE    ) wantIt = 0;
/* if ( attrib & FILE_ATTRIBUTE_READONLY   ) wantIt = 0; */ /*we'll change file permission to RW*/
   if ( attrib & FILE_ATTRIBUTE_SYSTEM     ) wantIt = 0;
   if ( attrib & FILE_ATTRIBUTE_TEMPORARY  ) wantIt = 0;
   return wantIt;
}


/***************************************************************
 *  GetFirstName()                                             *
 *                                                             *
 *  Get the name of the first file in the current directory.   *
 *                                                             *
 *  Returns  0 if all ok                                       *
 *          -1 if no files were found                          *
 ***************************************************************/

int GetFirstName( char fname[] )
{
   char            fileMask[] = "*";
   WIN32_FIND_DATA findData;

/* Get the name of the first file and see if we want it
   ****************************************************/
   fileHandle = FindFirstFile( fileMask, &findData );

   if ( fileHandle == INVALID_HANDLE_VALUE )   /* No files found */
      return -1;

   if ( WantFile( findData.dwFileAttributes ) )
   {
      strcpy( fname, findData.cFileName );
      return 0;
   }

/* If we don't want the first file, keep looking
   *********************************************/
   while ( FindNextFile( fileHandle, &findData ) )
   {
      if ( WantFile( findData.dwFileAttributes ) )
      {
         strcpy( fname, findData.cFileName );
         return 0;
      }
   }

   FindClose( fileHandle );        /* No desirable files found */
   first = 1;
   return -1;
}


/***************************************************************
 *  GetNextName()                                              *
 *                                                             *
 *  Get the name of the next file in the current directory.    *
 *                                                             *
 *  Returns  0 if all ok                                       *
 *          -1 if no files were found                          *
 ***************************************************************/

int GetNextName( char fname[] )
{
   WIN32_FIND_DATA findData;

/* Search for a desirable file
   ***************************/
   while ( FindNextFile( fileHandle, &findData ) )
   {
      if ( WantFile( findData.dwFileAttributes ) )
      {
         strcpy( fname, findData.cFileName );
         return 0;
      }
   }

   FindClose( fileHandle );        /* No desirable files found */
   first = 1;
   return -1;
}


/***************************************************************
 *  GetFileNameDir()                                           *
 *                                                             *
 *  Get the name of a file in the current directory.           *
 *                                                             *
 *  Returns  0 if all ok                                       *
 *          -1 if no files were found                          *
 ***************************************************************/

int GetFileNameDir( char fname[] )
{
   if ( first )
   {
      first = 0;
      return GetFirstName( fname );
   }
   return GetNextName( fname );
}


/***************************************************************
 *  MoveFile2()                                                *
 *                                                             *
 *  Move file fname to directory outdir.                       *
 *                                                             *
 *  Returns  0 if all ok                                       *
 *          -1 if the file cannot be moved                     *
 ***************************************************************/

int MoveFile2( char fname[], char outdir[] )
{
   char outname[256];

   strcpy( outname, outdir );
   strcat( outname, "\\" );         // Single quote, not double
   strcat( outname, fname );
   if ( MoveFile( fname, outname ) == 0 )
   {
      printf( "MoveFile() error: %d\n", GetLastError() );
      return -1;
   }
   return 0;
}

/***************************************************************
 *  MakeFileReadWrite()                                        *
 *                                                             *
 *  Makes file read/write by everyone.                         *
 *                                                             *
 *  Returns  0 if permissions are changed                      *
 *          -1 if failed                                       *
 ***************************************************************/

int MakeFileReadWrite( char fname[] )
{
   return _chmod( fname, _S_IREAD | _S_IWRITE );
}

