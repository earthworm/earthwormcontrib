
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#if defined(_WINNT)
#define strcasecmp _stricmp
#endif 

/* Returns  0 if no error
           -1 if an error was detected
*/


int CreateFileName( char stringDate[], char stringTime[], char oldFileName[], char newFileName[] )
{
   char smonth[6];
   int year, month, day, hour, minute, second;

/* Decode acquisition date
   ***********************/
   if ( sscanf( &stringDate[16], "%d/%[^/]/%d", &day, smonth, &year ) != 3 )
   {
      printf( "Error decoding date string: %s\n", stringDate );
      return -1;
   }
   if ( (year < 2000) || (year > 2100) )
   {
      printf( "Error. Invalid year: %d\n", year );
      return -1;
   }

   if ( isdigit(smonth[0]) )   /* numeric month string */
   {
      if ( sscanf( smonth, "%d", &month ) != 1 )
      {
         printf( "Error decoding month string: %s\n", smonth );
         return -1;
      }
      if ( (month < 1) || (month > 12) )
      {
         printf( "Error. Invalid month: %d\n", month );
         return -1;
      }
   } else {                    /* alpha-month string */
      month=0;
      if ( strcasecmp( smonth, "Jan" ) == 0 ) month = 1; 
      if ( strcasecmp( smonth, "Feb" ) == 0 ) month = 2; 
      if ( strcasecmp( smonth, "Mar" ) == 0 ) month = 3; 
      if ( strcasecmp( smonth, "Apr" ) == 0 ) month = 4; 
      if ( strcasecmp( smonth, "May" ) == 0 ) month = 5; 
      if ( strcasecmp( smonth, "Jun" ) == 0 ) month = 6; 
      if ( strcasecmp( smonth, "Jul" ) == 0 ) month = 7; 
      if ( strcasecmp( smonth, "Aug" ) == 0 ) month = 8; 
      if ( strcasecmp( smonth, "Sep" ) == 0 ) month = 9; 
      if ( strcasecmp( smonth, "Oct" ) == 0 ) month = 10; 
      if ( strcasecmp( smonth, "Nov" ) == 0 ) month = 11;
      if ( strcasecmp( smonth, "Dec" ) == 0 ) month = 12; 
      if ( month==0 )
      {
         printf( "Error. Invalid month string: %s\n", smonth );
         return -1;
      }
   }

   if ( (day < 1) || (day > 31) )
   {
      printf( "Error. Invalid day: %d\n", day );
      return -1;
   }

/* Decode acquisition time
   ***********************/
   if ( sscanf( &stringTime[16], "%d:%d:%d", &hour, &minute, &second ) != 3 )
   {
      printf( "Error decoding time string: %s\n", stringTime );
      return -1;
   }
   if ( (hour < 0) || (hour > 23) )
   {
      printf( "Error. Invalid hour: %d\n", hour );
      return -1;
   }
   if ( (minute < 0) || (minute > 59) )
   {
      printf( "Error. Invalid minute: %d\n", minute );
      return -1;
   }
   if ( (second < 0) || (second > 59) )
   {
      printf( "Error. Invalid second: %d\n", second );
      return -1;
   }

   sprintf( newFileName, "%4d%02d%02d-%02d%02d%02d.%s",
            year, month, day, hour, minute, second, oldFileName );
   return 0;
}
