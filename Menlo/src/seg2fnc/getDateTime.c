
#include <stdio.h>
#include <string.h>
#include <swap.h>           /* for SwapShort() */
#include "seg2fnc.h"

/*
   GetDateTime() returns  0 if no error
                         -1 if an error was detected
*/


int GetDateTime( char oldFileName[], char stringDate[], char stringTime[] )
{
   char  stringParam[STRLEN];
   FILE  *seg2fp;
   int   nativeByteOrder;   /* 1 if SEG2 file is in native byte order */
   short sotpsb;            /* Size of trace pointer sub-block */
   int   stringParamOffset;
   int   gotDate = 0;
   int   gotTime = 0;

/* Open the file to be renamed.  On Windows, this is an
   exclusive open.  Solaris doesn't support exclusive open.
   *******************************************************/
   seg2fp = fopen_excl( oldFileName, "rb" );
   if ( seg2fp == NULL )
   {
      printf( "Can't open file: %s\n", oldFileName );
      return -1;
   }

/* The first two bytes of the file indicate the byte order.
   This code sets variable: nativeByteOrder.
   *******************************************************/
   {
      short s1;

      if ( fread( &s1, sizeof(short), 1, seg2fp ) != 1 )
      {
         printf( "Error. Can't read first two bytes from file: %s\n",
                  oldFileName );
         fclose( seg2fp );
         return -1;
      }

      if ( s1 == 0x3a55 )
         nativeByteOrder = 1;
      else if ( s1 == 0x553a )
         nativeByteOrder = 0;
      else                      /* Not a SEG2 file */
      {
         fclose( seg2fp );
         return -1;
      }
   }

/* Get size of trace pointer sub-block (sotpsb)
   ********************************************/
   if ( fseek( seg2fp, 4, SEEK_SET ) != 0 )
   {
      printf( "Error. Error seeking byte 4 of file: %s\n",
               oldFileName );
      fclose( seg2fp );
      return -1;
   }

   if ( fread( &sotpsb, sizeof(short), 1, seg2fp ) != 1 )
   {
      printf( "Error. Can't read size of trace pointer sub-block" );
      printf( " from file: %s\n", oldFileName );
      fclose( seg2fp );
      return -1;
   }
   if ( !nativeByteOrder )
      SwapShort( &sotpsb );

/* Move file pointer to beginning of string parameters
   ***************************************************/
   stringParamOffset = 32 + sotpsb;

   if ( fseek( seg2fp, stringParamOffset, SEEK_SET ) != 0 )
   {
      printf( "Error seeking string parameters of file: %s\n",
               oldFileName );
      fclose( seg2fp );
      return -1;
   }

/* Get all string parameters
   *************************/
   while ( 1 )
   {
      short sosp;     /* Size of string parameter */
      char  *sp;      /* String pointer */

                                         /* Get size of string parameter */
      if ( fread( &sosp, sizeof(short), 1, seg2fp ) != 1 )
      {
         printf( "Error. Can't read size of string parameter" );
         printf( " from file: %s\n", oldFileName );
         fclose( seg2fp );
         return -1;
      }
      if ( !nativeByteOrder )
         SwapShort( &sosp );

      if ( sosp == 0 ) break;               /* No more string parameters */

      if ( sosp > STRLEN )
      {
         printf( "Size of string parameter (sosp) too big.\n" );
         printf( "sosp: %d   Max size: %d\n", sosp, STRLEN );
         fclose( seg2fp );
         return -1;
      }

      sp = fgets( stringParam, sosp, seg2fp );   /* Get string parameter */
      if ( sp != (char *)stringParam )
      {
         printf( "Error getting string parameter" );
         fclose( seg2fp );
         return -1;
      }

      if ( strncmp( stringParam, "ACQUISITION_DATE", 16 ) == 0 )
      {
         gotDate = 1;
         strcpy( stringDate, stringParam );
      }

      if ( strncmp( stringParam, "ACQUISITION_TIME", 16 ) == 0 )
      {
         gotTime = 1;
         strcpy( stringTime, stringParam );
      }

      stringParamOffset += sosp;       /* Point to next string parameter */
      if ( fseek( seg2fp, stringParamOffset, SEEK_SET ) != 0 )
      {
         printf( "Error seeking next string parameter of file: %s\n",
                  oldFileName );
         fclose( seg2fp );
         return -1;
      }
   }

   if ( (gotDate == 0) || (gotTime == 0) )
   {
      if ( gotDate == 0 )
         printf( "No AQUISITION_DATE string header found.\n" );
      if ( gotTime == 0 )
         printf( "No AQUISITION_TIME string header found.\n" );
      fclose( seg2fp );
      return -1;
   }

   fclose( seg2fp );
   return 0;           /* Success */
}
