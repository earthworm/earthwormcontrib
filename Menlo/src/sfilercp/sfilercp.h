
/* Define Strong Motion file types
 *********************************/
#define NAM_LEN 	 100     /* length of full directory name            */
#define MAX_TARGETS  15      /* largest number of targets                */
#define MAX_RCPS     25      /* largest number of targets                */

/* Structure Definitions
 ***********************/
typedef struct _INSTANCE_ {
	char    GetFromDir[NAM_LEN];         /* directory to monitor for data       */
    int     ntargets;                    /* Number of remote target directories */
    int     SaveDataFile;                /* Flag to save files                  */
    char    target[MAX_TARGETS][100];    /* Target in form-> UserId@IPname:/directory/    */
    char    UserID[MAX_TARGETS][100];    /* Target in form-> UserId         */
    char    Passwd[MAX_TARGETS][100];    /* Passwd (for OS2)                */
    char    Host[MAX_TARGETS][100];      /* Target in form-> IPname         */
    char    Directory[MAX_TARGETS][100]; /* Target in form-> /directory/    */
} INSTANCE;

