/*****************************************************************************
 * sendfilercp.c:
 *
 * Periodically check specified directories for files.
 * Reads the files, rcp's them to other computers and deletes them.
 * 
 *****************************************************************************/   

#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <errno.h>
#include "sfilercp.h"

/* Functions in this source file 
 *******************************/
void Send_File(INSTANCE *inst, char *fname);
void config_me ( char * );
void HeartBeat(void);
void ewmod_lookup ( void );
void ewmod_status ( unsigned char, short, char * );
int GetOldestFileName( char fname[] );

SHM_INFO  Region;                  /* shared memory region to use for i/o    */
pid_t     myPid;                   /* for restarts by startstop              */
static char *SaveSubdir = "save";  /* subdir for processed files             */

/* Things to read or derive from configuration file
 **************************************************/
static char     RingName[20];        /* name of transport ring for i/o       */
static char     MyModName[50];       /* speak as this module name/id         */
static int      LogSwitch;           /* 0 if no logfile should be written    */
static int      HeartBeatInterval;   /* seconds betweeen beats to statmgr    */
static int      NumDirs;             /* number of input directories to watch */
INSTANCE        rcp[MAX_RCPS];
static unsigned CheckPeriod;         /* secs between looking for new files   */
static int      OpenTries;
static int      OpenWait;
static int      SaveDataFiles;       /* if non-zero, move to SaveSubdir,     */ 
                                     /*           0, remove files            */
       int      Debug;               /* non-zero -> debug logging            */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o         */
static unsigned char InstId;        /* local installation id                 */
static unsigned char MyModId;       /* Module Id for this program            */
static unsigned char TypeHeartBeat; 
static unsigned char TypeError;
static unsigned char TypePage;


/* Error messages used by sm_file2ew 
 ***********************************/
#define ERR_CONVERT       0        /* trouble converting a file              */
#define ERR_PEER_LOST     1        /* no peer heartbeat file for a while     */
#define ERR_PEER_RESUME   2        /* got a peer heartbeat file again        */

#define TEXT_LEN NAM_LEN*3
static char ProgName[NAM_LEN];     /* program name for logging purposes      */
time_t    tnextbeat;               /* next time for local heartbeat          */
time_t    timeNow;                 /* current time                           */
pid_t     MyPid;    /* Our own pid, sent with heartbeat for restart purposes */

/*****************************************************************************   
 *****************************************************************************/   
int main( int argc, char **argv )
{
   int       i, j, ret, ierr, jerr, read_error = 0;
   char      fname[100], fnew[155];
   char     *c;
   FILE     *fp;
   time_t    timeInLoop;         /* start time for loop */

/* Check command line arguments 
 ******************************/
   if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", argv[0] );
        exit( 0 );
   }
   strcpy( ProgName, argv[1] );
   c = strchr( ProgName, '.' );
   if( c ) *c = '\0';

/* Get our own Pid for restart purposes
***************************************/
    MyPid = getpid();
    if( MyPid == -1 ) {
        fprintf( stderr,"%s: Cannot get pid. Exiting.\n", ProgName);
        return -1;
    }
           
/* Read the configuration file(s)
 ********************************/
   config_me( argv[1] );
   
/* Look up important info from earthworm.h tables
 ************************************************/
   ewmod_lookup();
    
/* Initialize name of log-file & open it 
 ***************************************/
   logit_init( argv[1], (short) MyModId, TEXT_LEN*2, LogSwitch );
   logit( "" , "%s: Read command file <%s>\n", argv[0], argv[1] );

/* Get process ID for heartbeat messages 
 ***************************************/
   myPid = getpid();
   if( myPid == -1 ) {
     logit("e","%s: Cannot get pid; exiting!\n", ProgName);
     exit (-1);
   }

/* Change to the directory with the input files
 ***********************************************/
    for(j=0;j<NumDirs;j++) {
        if( chdir_ew( rcp[j].GetFromDir ) == -1 ) {
            logit( "e", "%s: GetFromDir directory <%s> not found; "
                     "exiting!\n", ProgName, rcp[j].GetFromDir );
            exit(-1);
        }
    /* Make sure save subdirectory exists (if it will be used)
     *********************************************************/
        if( SaveDataFiles || rcp[j].SaveDataFile ) {
            if( CreateDir( SaveSubdir ) != EW_SUCCESS ) {
                logit( "e", "%s: trouble creating save directory: %s/%s\n",
                    ProgName,  rcp[j].GetFromDir, SaveSubdir ); 
                return( -1 );
            }
        }
    }
/* Attach to Output shared memory ring 
 *************************************/
   tport_attach( &Region, RingKey );
   logit( "", "%s: Attached to public memory region %s: %d\n", 
          ProgName, RingName, RingKey );

/* Force local heartbeat first time thru main loop
   but give peers the full interval before we expect a file
 **********************************************************/
   tnextbeat  = time(NULL) - 1;


/****************  top of working loop ********************************/
   while(1)  {
     /* Check on heartbeats
      *********************/
        for(j=0;j<NumDirs;j++) {
            timeNow = timeInLoop = time(NULL);
            while(timeNow-timeInLoop < 120) {   /* Make sure we don't get caught in the loop */
                timeNow = time(NULL);
                if( timeNow >= tnextbeat ) {  /* time to beat local heart */
                   ewmod_status( TypeHeartBeat, 0, "" );
                   tnextbeat = timeNow + HeartBeatInterval;
                }

                 /* See if termination has been requested 
                  ****************************************/
                if ( tport_getflag( &Region ) == TERMINATE ||
                     tport_getflag( &Region ) == MyPid ) {      /* detach from shared memory regions*/
                       logit( "t", "%s: Termination requested; exiting!\n", ProgName );
                       break;
                    }

            /* Change to the directory with the input files
             ***********************************************/
                if( chdir_ew( rcp[j].GetFromDir ) == -1 ) {
                  logit( "e", "%s: GetFromDir directory <%s> not found; "
                             "exiting!\n", ProgName, rcp[j].GetFromDir );
                  exit(-1);
                }

                 /* Get a file name
                  ******************/    
                ret = GetOldestFileName( fname );
                
                sleep_ew( 1*1000 );
                HeartBeat();
                
                if( ret == 1 )  break;  /* No files found */
                        
                /*
                if(Debug) logit("et","%s: got file name <%s>\n",ProgName,fname);
                */
                 /* Open the file.
                  ******************/
                 /* We open for write, as that will hopefully get us an exclusive open. 
                  * We don't ever want to look at a file that's being written to. 
                  */
                for(i=0;i<OpenTries;i++) {
                    fp = fopen( fname, "rb" );
                    if ( fp != NULL ) goto itopend;
                    sleep_ew(OpenWait);
                    HeartBeat();
                }
                logit("et","%s: Error: Could not open %s after %d*%d ms\n",
                           ProgName, OpenTries,OpenWait);
                itopend:    
                if(i>0) logit("t","Warning: %d attempts required to open file %s\n",
                            i,fname);

             /* If it's a core file, delete the file
              **************************************/
                if( strcmp(fname,"core")==0 ) {
                    fclose( fp );
                    if( remove( fname ) != 0) {
                        logit("et", "%s: Cannot delete core file <%s>; exiting!", ProgName, fname );
                        break;
                    }
                    continue;
                }

             /* Pass non-heartbeat files to the appropriate file2ewmsg filter 
                *************************************************************/
                if(Debug) logit("et","%s: Reading %s \n", ProgName, fname );
                
                fclose( fp );
                Send_File(&rcp[j], fname);

             /* Everything went fine...
              *************************/
           
                if( SaveDataFiles || rcp[j].SaveDataFile ) {      /* Keep file around */
                    sprintf(fnew,"%s/%s",SaveSubdir,fname );
                    ierr = rename( fname, fnew );
                /* The following silliness is necessary to be Windows compatible */ 
                    if( ierr != 0 ) {
                        if(Debug) logit( "e", "Error moving file %s%s to %s; ierr = %d\n", rcp[j].GetFromDir, fname, fnew, ierr );
                        if( remove( fnew ) != 0 ) {
                            logit("e","error deleting file %s%s\n", rcp[j].GetFromDir, fnew);
                        } else  {
                            if(Debug) logit("e","deleted file %s%s.\n", rcp[j].GetFromDir, fnew);
                            jerr = rename( fname, fnew );
                            if( jerr != 0 ) {
                                logit( "e", "error moving file %s%s to %s; ierr = %d\n", rcp[j].GetFromDir, fname, fnew, ierr );
                            } else {
                                if(Debug) logit("e","%s%s moved to %s\n", rcp[j].GetFromDir, fname, fnew );
                            }
                        }
                    } else {
                        if(Debug) logit("e","%s%s moved to %s\n", rcp[j].GetFromDir, fname, fnew );
                    }
                }
                else {       /* Delete the file */
                    if( remove( fname ) != 0 ) {
                        logit("e","error deleting file %s%s\n", rcp[j].GetFromDir, fname);
                        break;
                    } 
                    else  {
                        if(Debug)logit("e","deleted file %s%s.\n", rcp[j].GetFromDir, fname);
                    }
                }
                if(Debug)logit("e","\n");
            }
        }
         /* See if termination has been requested 
          ****************************************/
        if ( tport_getflag( &Region ) == TERMINATE ||
             tport_getflag( &Region ) == MyPid ) {      /* detach from shared memory regions*/
               logit( "t", "%s: Termination requested; exiting!\n", ProgName );
               break;
        }
        sleep_ew( CheckPeriod*1000 ); 
        HeartBeat();

    } /* end of while */

/************************ end of working loop ****************************/
    
/* detach from shared memory */
    tport_detach( &Region ); 

/* write a termination msg to log file */
    fflush( stdout );
    return( 0 );

}  


/******************************************************************************
 *   Send_File()                                                              *
 *    Sends the current version of the file fname using rcp.                  *
 ******************************************************************************/

void Send_File(INSTANCE *inst, char *fname)
{
    char    tname[175], string[200], whoami[50];
    pid_t   pid;
    int     j, ierr, retry, status;
    
    sprintf(whoami, " %s: %s: ", ProgName, "Send_File");
    /* Send the file. *
     **********************/        
    if(Debug) logit("e", "%s Sending File: %s\n", whoami, fname);
    sprintf(tname,  "%s.%s.%s", fname, "sfilercp", getenv("SYS_NAME") );
    for(j=0;j<inst->ntargets;j++) {
        retry = 0;
        do {
            if(retry>0) sleep_ew(5000);
            HeartBeat();
            string[0] = '\0';
            ierr = remote_copy(fname, tname, fname, inst->Host[j], 
                               inst->Directory[j], inst->UserID[j], inst->Passwd[j], 
                               string, &pid, &status); 
     
        } while(ierr==1 && retry++ < 6); 
     
        if((int)strlen(string)>0)
            logit("e", "%s remote_copy File: %s %s Error# %d\n", 
                   whoami, fname, string, ierr); 
    }
}


/******************************************************************************
 *  config_me() processes command file(s) using kom.c functions;              *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
#define ncommand 10
void config_me( char *configfile )
{
   char     init[ncommand]; /* init flags, one byte for each required command */
   int      nmiss;          /* number of required commands that were missed   */
   char    *com, *str;
   int      i, j, k, n, nfiles, success;
   
/* Set to zero one init flag for each required command 
 *****************************************************/   
   for( i=0; i<ncommand; i++ )  init[i] = 0;
   NumDirs = 0;

/* Open the main configuration file 
 **********************************/
   nfiles = k_open( configfile ); 
   if ( nfiles == 0 ) {
        fprintf( stderr,
                "%s: Error opening command file <%s>; exiting!\n", 
                 ProgName, configfile );
        exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {  
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr, 
                          "%s: Error opening command file <%s>; exiting!\n",
                           ProgName, &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command 
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("GetFromDir") ) {
                if ( NumDirs >= MAX_RCPS ) {
                    fprintf( stderr, "%s Too many <GetFromDir> commands in <%s>", 
                             ProgName, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_RCPS );
                    return;
                }
                rcp[NumDirs].ntargets = k_int();
                rcp[NumDirs].SaveDataFile = k_int();
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( rcp[NumDirs].GetFromDir, str, NAM_LEN );
                if(rcp[NumDirs].ntargets > MAX_TARGETS) {
                    fprintf( stderr, "%s Too many <targets> commands in <%s>", 
                             ProgName, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_TARGETS );
                    return;
                }
                for(j=0;j<rcp[NumDirs].ntargets;j++) {
                    do {
                        k_rd();
                        str=k_str();
                    } while (str[0] == '#' || str[0] == 0);
                    if( (long)(str) != 0 )  {
                        n = strlen(str);   /* Make sure directory name has proper ending! */
                        if( str[n-1] != '/' ) strcat(str, "/");
                        strcpy(rcp[NumDirs].target[j], str);
                        i = 0;
                        while(str[i] != '@') {
                            rcp[NumDirs].UserID[j][i] = str[i];
                            i++;
                        }
                        rcp[NumDirs].UserID[j][i] = 0;
                        rcp[NumDirs].Passwd[j][0] = 0;
                        k = 0;
                        i++;
                        while(str[i] != '@' && str[i] != ':') {
                            rcp[NumDirs].Host[j][k++] = str[i++];
                        }
                        rcp[NumDirs].Host[j][k] = 0;
                        strcpy(rcp[NumDirs].Directory[j], &str[i+1]);
                    }
                }
                NumDirs++;
                init[3] = 1;
            }
  /*4*/     else if( k_its("CheckPeriod") ) {
                CheckPeriod = k_int();
                init[4] = 1;
            }
  /*5*/     else if( k_its("Debug") ) {
                Debug = k_int();
                init[5] = 1;
            }
  /*6*/     else if( k_its("OpenTries") ) {
                OpenTries = k_int();
                init[6] = 1;
            }
  /*7*/     else if( k_its("OpenWait") ) {
                OpenWait = k_int();
                init[7] = 1;
            }

  /*8*/    else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_int();
                init[8] = 1;
            }

  /*9*/    else if( k_its("SaveDataFiles") ) {
                SaveDataFiles = k_int();
                init[9] = 1;
            }

               /* optional commands */

         /* Unknown command
          *****************/ 
            else {
                fprintf( stderr, "%s: <%s> Unknown command in <%s>.\n", 
                         ProgName, com, configfile );
                continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr, 
                       "%s: Bad <%s> command in <%s>; exiting!\n",
                        ProgName, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }
   
/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "%s: ERROR, no ", ProgName );
       if ( !init[0] )  fprintf( stderr, "<LogFile> "       );
       if ( !init[1] )  fprintf( stderr, "<MyModuleId> "    );
       if ( !init[2] )  fprintf( stderr, "<RingName> "      );
       if ( !init[3] )  fprintf( stderr, "<GetFromDir> "    );
       if ( !init[4] )  fprintf( stderr, "<CheckPeriod> "   );
       if ( !init[5] )  fprintf( stderr, "<Debug> "         );
       if ( !init[6] )  fprintf( stderr, "<OpenTries> "     );
       if ( !init[7] )  fprintf( stderr, "<OpenWait> "      );
       if ( !init[8] )  fprintf( stderr, "<HeartBeatInterval> "     );
       if ( !init[9] )  fprintf( stderr, "<SaveDataFiles> " );
       fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }
   
   return;
}

/**********************************************************************************
 * HeartBeat() sends a heartbeat                                                  *
 **********************************************************************************/
void HeartBeat(void)
{
    if  ( time(&timeNow) - tnextbeat  >=  HeartBeatInterval ) {
        tnextbeat = timeNow;
        ewmod_status( TypeHeartBeat, 0, "" );
    }
}


/******************************************************************************
 *  ewmod_lookup( )   Look up important info from earthworm.h tables          *
 ******************************************************************************/
void ewmod_lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 ) {
        fprintf( stderr,
                "%s:  Invalid ring name <%s>; exiting!\n",
                 ProgName, RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr, 
              "%s: error getting local installation id; exiting!\n",ProgName );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid module name <%s>; exiting!\n", ProgName, MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n",ProgName );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_ERROR>; exiting!\n",ProgName );
      exit( -1 );
   }
   return;
} 

/******************************************************************************
 * ewmod_status() builds a heartbeat or error message & puts it into     *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/
void ewmod_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size;
   long        t;
 
/* Build the message
 *******************/ 
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat ) {
        sprintf( msg, "%ld %ld\n\0", t, myPid);
   }
   else if( type == TypeError ) {
        sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
        logit( "et", "%s: %s\n", ProgName, note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */     

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK ) {
        if( type == TypeHeartBeat ) {
           logit("et","%s:  Error sending heartbeat.\n", ProgName );
        }
        else if( type == TypeError ) {
           logit("et","%s:  Error sending error:%d.\n", ProgName, ierr );
        }
   }
   return;
}


/***************************************************************
 *  GetOldestFileName    (from GetFileName, by Will Kohler)    *
 *                                                             *
 *  Function to get the name of the oldest file in the current *
 *  directory.                                                 *
 *                                                             * 
 *  Solaris only version.                                      *
 *                                                             * 
 *  Returns 0 if all ok                                        *
 *          1 if no files were found                           *
 ***************************************************************/
#define  MAX_FILES  500
int GetOldestFileName( char fname[] )
{
    DIR         *dp;
    struct dirent *dentp;
    struct stat ss;
    int    i, j, nfiles, index;
    time_t mtime[MAX_FILES];
    char   name[MAX_FILES][100];

    dp = opendir( "." );
    if ( dp == NULL ) return 2;

    nfiles = 0;
    while((dentp = readdir(dp)) && nfiles < MAX_FILES) {
        if(stat( dentp->d_name, &ss ) == -1) {
            closedir( dp );
            return 1;
        }
        if(S_ISREG(ss.st_mode)) {
            strcpy(name[nfiles], dentp->d_name);
            mtime[nfiles] = ss.st_mtime;
            nfiles++;
        }
    }
    closedir( dp );
    if(nfiles > 0) {
        j = 0;
        for(i=1;i<nfiles;i++) {
            if(mtime[i] < mtime[j]) j = i; 
        }
        strcpy( fname, name[j] );
        return 0;
    }
    return 1;
}



