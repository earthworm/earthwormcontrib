/************************************************************************/
/*  ms2sac -	convert MiniSEED file to SAC format.			*/
/*									*/
/*	Douglas Neuhauser						*/
/*	Seismological Laboratory					*/
/*	University of California, Berkeley				*/
/*	doug@seismo.berkeley.edu					*/
/*									*/
/************************************************************************/

/*
 * Copyright (c) 1996-2001 The Regents of the University of California.
 * All Rights Reserved.
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for educational, research and non-profit purposes,
 * without fee, and without a written agreement is hereby granted,
 * provided that the above copyright notice, this paragraph and the
 * following three paragraphs appear in all copies.
 * 
 * Permission to incorporate this software into commercial products may
 * be obtained from the Office of Technology Licensing, 2150 Shattuck
 * Avenue, Suite 510, Berkeley, CA  94704.
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND
 * ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE
 * PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
 * CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */

#ifndef lint
static char sccsid[] = "$Id: ms2sac.c 524 2010-05-07 19:11:39Z luetgert $ ";
#endif

#include <stdio.h>
#include "version.h"

#ifndef	DEFAULT_COORD_FILE
#define	DEFAULT_COORD_FILE  "/dev/null"
#endif

#ifndef	DEFAULT_EVTNAME_STRING
#define	DEFAULT_EVTNAME_STRING	"seed"
#endif

char *syntax[] = {
"%s version " VERSION,
"%s  [ -h ] [ -f a|b ] [ -c coord_file ] [ -d sac|seed ]",
"    [ -e event_file][ -G tol ] [ -n seed|comp|event_name ]",
"    [ infile [ outfile ]]",
"    where:",
"	-h	Help - prints syntax message.",
"	-f b | a",
"		Output is SAC binary or ASCII file.",
"		Default format is SAC binary.",
"	-c station_coord",
"		Specifies the name of a station coordinate file.",
"		OR the explicit channel coordinates lat,lon,elev.",
"		If no coord file is specified, use the default file",
"		"DEFAULT_COORD_FILE,
"	-d sac | seed",
"		Specifies whether the dip value placed in the SAC header",
"		represents SAC or SEED convention.",
"		The default is SAC convention.",
"	-e event_file",
"		Specifies the name of a gopher event file that contains",
"		specific component orientation in SEED convention.",
"		If no event file is specified, use default orientations.",
"		Orientations are converted into SAC format unless",
"		otherwise specified.",
"	-G TOL	Explicit tolerance for time gaps in ticks (1/10 msec).",
"		A total time slew > TOL will terminate the time series.",
"		Default is 1/2 of data sample interval.",
"	-n seed | comp | event_name",
"		Specify SAC event name to be constructed from either:",
"			seed - date and SEED channel name",
"			comp - date and component and stream (Quanterra format).",
"			event_name - explicit event name.",
"		The default is " DEFAULT_EVTNAME_STRING ".",
"	infile	Input file containing MiniSEED data.",
"		If no input file is specified, data is read from stdin.",
"	outfile	Output file for SAC file.",
"		If no output file is specified, output is written to stdout.",
"NOTES:",
"1.  SEED channel information is stored in the following SAC header fields:",
"	SEED station => KSTNM		SEED channel => KCMPNM",
"	SEED network => KNETWK		SEED location => KHOLE",
NULL };

#include <qlib2.h>

#include "procs.h"
#include "sachead.h"

char *cmdname;
FILE *info;

/************************************************************************/
/*  main routine							*/
/************************************************************************/
main (int argc, char **argv)
{
    struct SAChead SAChead;
    FILE *input, *output;
    char *infile, *outfile;
    char *evt_file = NULL;
    char *coord_file = DEFAULT_COORD_FILE;
    STN_INFO stn;
    EVT_INFO evt;
    DATA_HDR *hdr;
    
    char *stream, *comp;
    int *pdata;
    int npts;
    int format = 0;
    int bin_flag = 0;
    int stream_tol = 0;
    int status;
    char *evtname_string = "seed";
    char *dip_string = "sac";
    char *format_string = "b";
    double dip_offset;

    /* Variables needed for getopt. */
    extern char	*optarg;
    extern int	optind, opterr;
    int		c;

    info = stdout;
    evt.evtname_string = DEFAULT_EVTNAME_STRING;
    cmdname = trim(*argv);
    /*	Parse command line options.					*/
    while ( (c = getopt(argc,argv,"hc:d:e:f:n:G:")) != -1)
	switch (c) {
	case '?':
	case 'h':   print_syntax (cmdname, syntax, info); exit(0); break;
	case 'f':   format_string = optarg; break;	    
	case 'd':   dip_string = optarg; break;
	case 'e':   evt_file = optarg; break;
	case 'c':   coord_file = optarg; break;
	case 'n':   evt.evtname_string = optarg; break;
	case 'G':   stream_tol = atoi(optarg); break;
	default:    FATAL("error parsing options\n");
	}
    /*	Skip over all options and their arguments.			*/
    argv = &(argv[optind]);
    argc -= optind;

    if (strcasecmp(format_string, "a") == 0) format = 0;
    else if (strcasecmp(format_string, "b") == 0) format = 1;
    else FATAL ("Invalid dip format string.\n");

    if (strcasecmp(dip_string,"sac") == 0) dip_offset = 90.;
    else if (strcasecmp(dip_string,"seed") == 0) dip_offset = 0.;
    else FATAL ("Invalid dip format string.\n");

    /* The remaining arguments are [ input [and output] ] files.	*/

    infile = "<stdin>";
    input = stdin;
    outfile = "<stdout>";
    output = stdout;
    switch (argc) {
	case 0:	
	    info = stderr;
	    break;
	case 1:
	    info = stderr;
	    infile = argv[0];
	    if ((input = fopen (infile, "r")) == NULL)
		FATAL ("unable to open input file\n");
	    break;
	case 2: 
	    info = stdout;
	    infile = argv[0];
	    if ((input = fopen (infile, "r")) == NULL)
		FATAL ("unable to open input file\n");
	    outfile = argv[1];
	    if ((output = fopen (outfile, "w")) == NULL)
		FATAL ("unable to open output file\n");
	    break;
	default:
	    print_syntax (cmdname, syntax, info); exit(1); break;
    }

    npts = read_mseed_data (&hdr, &pdata, stream_tol, input);
    if (npts <= 0) exit(npts);
    strcpy (stn.station, hdr->station_id);
    strcpy (stn.channel, hdr->channel_id);
    strcpy (stn.network, hdr->network_id);
    strcpy (stn.location, hdr->location_id);
    seed_to_comp (hdr->channel_id, &stream, &comp);
    strcpy (stn.stream, stream);
    strcpy (stn.comp, comp);
    status = get_stn_evt_info (&stn, coord_file, &evt, evt_file, dip_offset);
    if (status != 0) {
	fprintf (stderr, "Error %d getting station and/or event info\n", status);
	return (status);
    }
    status = init_sac_header (&SAChead, hdr, &stn, &evt, npts);
    if (status != 0) {
	fprintf (stderr, "Error %d filling in sac header\n", status);
	return (status);
    }
    status = write_sac (&SAChead, hdr, pdata, npts, format, output);
    if (status != 0) {
	fprintf (stderr, "Error %d writing sac file\n", status);
	return (status);
    }
    return (status);
}
