/************************************************************************/
/*  Routine to extract station and event info from event file.		*/
/*									*/
/*	Douglas Neuhauser						*/
/*	Seismological Laboratory					*/
/*	University of California, Berkeley				*/
/*	doug@seismo.berkeley.edu					*/
/*									*/
/************************************************************************/

/*
 * Copyright (c) 1996-2001 The Regents of the University of California.
 * All Rights Reserved.
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for educational, research and non-profit purposes,
 * without fee, and without a written agreement is hereby granted,
 * provided that the above copyright notice, this paragraph and the
 * following three paragraphs appear in all copies.
 * 
 * Permission to incorporate this software into commercial products may
 * be obtained from the Office of Technology Licensing, 2150 Shattuck
 * Avenue, Suite 510, Berkeley, CA  94704.
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND
 * ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE
 * PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
 * CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */

#ifndef lint
static char sccsid[] = "$Id: get_stn_evt_info.c 524 2010-05-07 19:11:39Z luetgert $ ";
#endif

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <memory.h>

#include "procs.h"

/************************************************************************/
/*  get_stn_evt_info -							*/
/*	Get station and channel info.					*/
/*  Return: 0 on success, non-zero on failure.				*/
/************************************************************************/
int get_stn_evt_info
   (STN_INFO *stn,		/* ptr to station struct (filled in).	*/
    char *coord_file,		/* station coordinate file name.	*/
    EVT_INFO *evt,		/* ptr to event struct (filled in).	*/
    char *evt_file,		/* event file name.			*/
    double dip_offset)		/* dip offset from SEED convention.	*/
{
    EXT_TIME et;
    char name[8], str[80];
    char line[LINELEN+1];
    FILE *fp;
    int i, n;
    double dip[4] = { -90., 0., 0., -12345. };	/* SEED std dip orient.	*/
    double az[4] = { 0., 0., 90., -12345. };	/* SEED std az orient.	*/
    double d[10];

    /* Initialization.	*/
    stn->lat = stn->lon = stn->el = stn->dip = stn->az = -12345.;
    evt->lat = evt->lon = evt->depth = evt->mag = -12345.;
    memset (&(evt->otime), 0, sizeof(evt->otime));

    /* Get latitude, longitude, and elevation.  */
    if (strchr(coord_file, ',') == NULL) {
	if (coord_file && strlen(coord_file)>0 && (fp=fopen (coord_file,"r"))) {
	    while (fgets (line, LINELEN, fp)) {
		sscanf (line, "%s %lf %lf %lf", name, &d[0], &d[1], &d[2]);
		if (strcasecmp (stn->station, name) == 0) {
		    stn->lat = d[0];
		    stn->lon = d[1];
		    stn->el = d[2];
		    break;
		}
	    }
	    fclose(fp);
	}
	else fprintf (stderr, "Warning - unable to open coord file: %s\n", 
		      (coord_file) ? coord_file : "(null)");
    }
    else {
	/* Coordinates lat,lon,elev explicitly specified in the coord_file string. */
	char *coord_str, *save, *p;
	p = save = coord_str = strdup(coord_file);
	if (coord_str != NULL && (p=strchr(coord_str, ',')) != NULL) *p++ = '\0';
	if (coord_str != NULL) stn->lat = atof(coord_str);
	coord_str = p;
	if (coord_str != NULL && (p=strchr(coord_str, ',')) != NULL) *p++ = '\0';
	if (coord_str != NULL) stn->lon = atof(coord_str);
	coord_str = p;
	if (coord_str != NULL && (p=strchr(coord_str, ',')) != NULL) *p++ = '\0';
	if (coord_str != NULL) stn->el  = atof(coord_str);
	coord_str = p;
	free(save);
    }

    /* Get component orientation and event info from the event file.	*/
    if (evt_file && strlen(evt_file)>0 && (fp=fopen(evt_file, "r"))) {
	while (fgets (line, LINELEN, fp)) {
	    sscanf (line, "%s", str);
	    if (strcasecmp (str, "comp:") == 0) {
		sscanf (line, "%*s %lf %lf %lf %lf %lf %lf", 
			&dip[0], &az[0], &dip[1], &az[1], &dip[2], &az[2]);
		continue;
	    }
	    if (strcasecmp (str, "event:") == 0) {
		n = sscanf (line, "%*s %lf %lf %lf %lf %lf",
		&d[0], &d[1], &d[2], &d[3], &d[4]);
		if (n >= 1) evt->lat = d[0];
		if (n >= 2) evt->lon = d[1];
		if (n >= 3) evt->mag = d[2];
		if (n >= 4) evt->depth = d[3];
		if (n >= 5) {
		    /* Compute diff between ref time and origin time.	*/
		    et.year = (int)(d[4]/10000000000.);
		    d[4] -= et.year*10000000000.;
		    et.month = (int)(d[4]/100000000.);
		    d[4] -= et.month*100000000.;
		    et.day = (int)(d[4]/1000000.);
		    d[4] -= et.day*1000000.;
		    et.hour = (int)(d[4]/10000.);
		    d[4] -= et.hour*10000.;
		    et.minute = (int)(d[4]/100.);
		    d[4] -= et.minute*100.;
		    et.second = (int)d[4];
		    d[4] -= et.second;
		    et.usec = (int)(d[4] * USECS_PER_SEC);
		    if (et.year < 100) {
			et.year += (et.year >= 50) ? 1900 : 2000;
		    }
		    et.doy = mdy_to_doy (et.month, et.day, et.year);
		    evt->otime = et;
		}
	    }
	    if (strcasecmp (str, "station") == 0) {
		n = sscanf (line, "%*s %lf %lf %lf %lf %lf",
		&d[0], &d[1], &d[2], &d[3], &d[4]);
		if (n >= 3 && stn->lat == -12345.) stn->lat = d[2];
		if (n >= 4 && stn->lon == -12345.) stn->lon = d[3];
		if (n >= 5 && stn->lat == -12345.) stn->el = d[4];
	    }
	}
	fclose(fp);
    }

    /* Fill in  dip and azimuth from the info that we have at hand.	*/
    switch (stn->channel[2]) {
      case 'Z': case 'z':   i=0; break;
      case 'N': case 'n':   i=1; break;
      case 'E': case 'e':   i=2; break;
      default:		    i=3; break;
    }
    stn->dip = dip[i] + dip_offset;
    stn->az = az[i];

    return(0);
}
