/*	@(#)defs.h	1.2 11/3/95 12:01:40	*/
#ifndef	EXTERN
#define	EXTERN	extern
#endif	/* EXTERN */

#include <qlib.h>

#define	LINELEN	127
#define	FATAL(str) { fprintf (stderr, str); exit(1); }

typedef struct stn_info {
    char	station[8];
    char	channel[4];
    char	stream[4];
    char	comp[4];
    double	lat;
    double	lon;
    double	el;
    double	dip;
    double	az;
} STN_INFO ;

int write_sac (DATA_HDR *, STN_INFO *, int *, int, int, char *, FILE *);
int get_stn_info (STN_INFO *, char *, char *, double);
int read_mseed_data (DATA_HDR **, int **, int, FILE *);
void print_syntax (char *);
