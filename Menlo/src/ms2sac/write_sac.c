/************************************************************************/
/*  Routines to write SAC file.						*/
/*									*/
/*	Douglas Neuhauser						*/
/*	Seismological Laboratory					*/
/*	University of California, Berkeley				*/
/*	doug@seismo.berkeley.edu					*/
/*									*/
/************************************************************************/

/*
 * Copyright (c) 1996-2000 The Regents of the University of California.
 * All Rights Reserved.
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for educational, research and non-profit purposes,
 * without fee, and without a written agreement is hereby granted,
 * provided that the above copyright notice, this paragraph and the
 * following three paragraphs appear in all copies.
 * 
 * Permission to incorporate this software into commercial products may
 * be obtained from the Office of Technology Licensing, 2150 Shattuck
 * Avenue, Suite 510, Berkeley, CA  94704.
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND
 * ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE
 * PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
 * CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */

#ifndef lint
static char sccsid[] = "$Id: write_sac.c 524 2010-05-07 19:11:39Z luetgert $ ";
#endif

/* Modification History
 *  95/01/13 DSN    Fixed binary output -- was skipping first point.
*/

#include <stdio.h>

#include <qlib2.h>

#include "procs.h"
#include "sachead.h"

#define MAXBOOL	5

/************************************************************************/
/*  write_sac -								*/
/*	Write SAC file.  SAChead has already been filled in.		*/
/*	Convert integer data IN PLACE to floating point data.		*/
/*	THIS DESTROYS THE ORIGINAL INTEGER DATA.			*/
/*  Return: 0 on success, non-zero on failure.				*/
/************************************************************************/
int write_sac 
   (struct SAChead *SAChead,	/* ptr to SAC header structure.		*/
    DATA_HDR *hdr,		/* ptr to DATA_HDR structure.		*/
    int	*pdata,			/* data buffer.				*/
    int npts,			/* # of data points to be written.	*/
    int format,			/* SAC ascii or binary format flag.	*/
    FILE *fp)			/* output FILE.				*/
{
    int i, j, n, done;
    float *fdata = (float *)pdata;
    struct SAChead2 *SAChead2 = (struct SAChead2 *)SAChead;

    /* Convert data points to floats in place.  THIS DESTROYS THE ORIGINAL DATA.*/
    for (i=0; i<npts; i++) *(fdata+i) = *(pdata+i);

    if (format == 0) {
	/* Output SAC ASCII format.	*/
	SAChead->internal1 = 2;			/* SAC ASCII format.	*/
	/* Output floats in SAC header.	*/
	for (i=0; i<NUM_FLOAT; i+=5) {
	    for (j=i; j<i+5 && j<NUM_FLOAT; j++) fprintf (fp, "%13.5f00", SAChead2->SACfloat[j]);
	    fprintf (fp, "\n");
	}
	/* Output ints in SAC header.	*/
	for (i=0; i<MAXINT; i+=5) {
	    for (j=i; j<i+5 && j<MAXINT; j++) fprintf (fp, "%10d", SAChead2->SACint[j]);
	    fprintf (fp, "\n");
	}
	/* Output boolean in SAC header.*/
	for (i=0; i<MAXBOOL; i+=5) {
	    for (j=i; j<i+5 && j<MAXBOOL; j++) fprintf (fp, "%10d", SAChead2->SACun[j]);
	    fprintf (fp, "\n");
	}
	/* Output strings in SAC header.*/
	for (i=0; i<MAXSTRING; i+=3) {
	    for (j=i; j<i+3 && j<MAXSTRING; j++) fprintf (fp, "%-8.8s", &SAChead2->SACstring[j][0]);
	    fprintf (fp, "\n");
	}
	/* Output data points.		*/
	for (i=0; i<npts; i+=5) {
	    for (j=i; j<i+5 && j<npts; j++) fprintf (fp, "%15.1lf", *(fdata+j));
	    fprintf (fp, "\n");
	}
    }

    else {
	/* Output SAC binary format.	*/
	SAChead->internal1 = 1;			/* SAC binary format.	*/
	n = fwrite (SAChead, sizeof(struct SAChead), 1, fp);
	if (n <= 0) {
	    fprintf (stderr, "Error writing sac header\n");
	    return (1);
	}
	done = 0;
	while (done < npts) {
	    n = fwrite (fdata+done, sizeof(float), npts-done, fp);
	    if (n <= 0) {
		fprintf (stderr, "Error writing sac data\n");
		return (1);
	    }
	    else done += n;
	}
    }
    return (0);
}
