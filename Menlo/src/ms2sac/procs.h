/************************************************************************/
/*  Procedure and structure definitions for ms2sac.			*/
/*									*/
/*	Douglas Neuhauser						*/
/*	Seismological Laboratory					*/
/*	University of California, Berkeley				*/
/*	doug@seismo.berkeley.edu					*/
/*									*/
/************************************************************************/

/*
 * Copyright (c) 1996-2000 The Regents of the University of California.
 * All Rights Reserved.
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for educational, research and non-profit purposes,
 * without fee, and without a written agreement is hereby granted,
 * provided that the above copyright notice, this paragraph and the
 * following three paragraphs appear in all copies.
 * 
 * Permission to incorporate this software into commercial products may
 * be obtained from the Office of Technology Licensing, 2150 Shattuck
 * Avenue, Suite 510, Berkeley, CA  94704.
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND
 * ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE
 * PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
 * CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */

/*	$Id: procs.h 524 2010-05-07 19:11:39Z luetgert $ 	*/

#ifndef	EXTERN
#define	EXTERN	extern
#endif	/* EXTERN */

#include <qlib2.h>

#include "sachead.h"

#define	LINELEN	127
#define	FATAL(str) { fprintf (stderr, str); exit(1); }

/* Structure for storing station info.					*/
typedef struct stn_info {
    char	station[8];	/* station name.			*/
    char	network[8];	/* network name.			*/
    char	channel[4];	/* channel name.			*/
    char	location[4];	/* locaion name.			*/
    char	stream[4];	/* stream name.				*/
    char	comp[4];	/* component name.			*/
    double	lat;		/* station coordinates: latitude.	*/
    double	lon;		/* station coordinates: longitude.	*/
    double	el;		/* station coordinates: elevation.	*/
    double	dip;		/* channel dip.				*/
    double	az;		/* channel azimuth.			*/
} STN_INFO ;

/* Structure for storing event info.					*/
typedef struct evt_info {
    double lat;			/* event location: latitude.		*/
    double lon;			/* event location: longitude.		*/
    double depth;		/* event location: depth.		*/
    double mag;			/* event magnitude.			*/
    EXT_TIME otime;		/* event origin time.			*/
    char *evtname_string;	/* event name for SAC header.		*/
} EVT_INFO;

int read_mseed_data 
   (DATA_HDR **phdr,		/* ptr to ptr to DATA_HDR (returned).	*/
    int **pdata,		/* ptr to ptr to data buffer (returned).*/
    int stream_tol,		/* time tolerance for continuous data.	*/
    FILE *fp);			/* input FILE.				*/

int get_stn_evt_info
   (STN_INFO *stn,		/* ptr to station struct (filled in).	*/
    char *coord_file,		/* station coordinate file name.	*/
    EVT_INFO *evt,		/* ptr to event struct (filled in).	*/
    char *evt_file,		/* event file name.			*/
    double dip_offset);		/* dip offset from SEED convention.	*/

int init_sac_header
   (struct SAChead *SAChead,	/* ptr to SAC header structure.		*/
    DATA_HDR *hdr,		/* ptr to DATA_HDR structure.		*/
    STN_INFO *stn,		/* ptr to station info structure.	*/
    EVT_INFO *evt,		/* ptr to event info structure.		*/
    int npts);			/* # of data points to be written.	*/

int write_sac 
   (struct SAChead *SAChead,	/* ptr to SAC header structure.		*/
    DATA_HDR *hdr,		/* ptr to DATA_HDR structure.		*/
    int	*pdata,			/* data buffer.				*/
    int npts,			/* # of data points to be written.	*/
    int format,			/* SAC ascii or binary format flag.	*/
    FILE *fp);			/* output FILE.				*/
