/************************************************************************/
/*  Version number.							*/
/*									*/
/*	Douglas Neuhauser						*/
/*	Seismological Laboratory					*/
/*	University of California, Berkeley				*/
/*	doug@seismo.berkeley.edu					*/
/*									*/
/************************************************************************/

/*
 * Copyright (c) 1996-2001 The Regents of the University of California.
 * All Rights Reserved.
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for educational, research and non-profit purposes,
 * without fee, and without a written agreement is hereby granted,
 * provided that the above copyright notice, this paragraph and the
 * following three paragraphs appear in all copies.
 * 
 * Permission to incorporate this software into commercial products may
 * be obtained from the Office of Technology Licensing, 2150 Shattuck
 * Avenue, Suite 510, Berkeley, CA  94704.
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND
 * ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE
 * PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
 * CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */

/*	$Id: version.h 524 2010-05-07 19:11:39Z luetgert $ 	*/

#define	VERSION	"1.4.5 (2005.035)"

/*
Modification History
------------------------------------------------------------------------
Date	    Ver			Who, What
------------------------------------------------------------------------
2004/02/04  1.4.5 (2004.035)	Doug Neuhauser
	Add support for blockette 100 in input MiniSEED file.
2001/05/30  1.4.4 (2001.150)	Doug Neuhauser
	Fix code to SEED location code in proper sac khole hdr field.
2001/04/03  1.4.3 (2001.094)	Doug Neuhauser
	Allow -c to specify explicit channel coordinates (-c lat,lon,elev)
	instead lieu of station coord file (-c station_coord_file).
	Fix segmentation violation on empty input file.
2000/09/06  1.4.2 (2000.250)	Doug Neuhausaer
	Add SEED network and location in SAC header.
*/
