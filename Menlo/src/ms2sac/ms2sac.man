.\$Id: ms2sac.man 524 2010-05-07 19:11:39Z luetgert $ 
.TH MS2SAC 1 "03 Apr 2001" BDSN
.SH NAME
ms2sac - convert MiniSEED data file to SAC data file
.SH SYNOPSIS
.B ms2sac
[\ \fB\-h\fR\ ]
[\ \fB\-f\ b | a\fR]
[\ \fB\-c\ \fI coordinates\fR]
[\ \fB\-d\ sac | seed\fR]
[\ \fB\-e\ \fI event_file\fR]
[\ \fB\-G\ \fI tol\fR]
[\ \fB\-n\ seed | comp | \fIevent_name\fR]
[\ \fIinput_file\fR\ [\ \fIoutput_file\fR\ ]
.br
.SH DESCRIPTION
The program
.B ms2sac
converts a data file containing a single channel of MiniSEED data into a SAC
format datafile in either ascii or binary format.  ms2sac will attempt to set
the station location in the SAC from station location information in the
explicitly specified coordinates or the coordinates from the station
coordinate file.  If a Spyder event_file is specified, the channel orientation
(dip and azumuth) will be derived from that file.  Otherwise, the dip and
azimuth will be assigned for standard orientations (Z, N, E) based on the
channel code.
.SH OPTIONS
.TP
.B \-h
print out a simple help message with syntax and explaination of the options.
.TP
.B \-f b | a
specifies that the output SAC file should be in SAC binary or ascii format.
The default output file format is SAC binary.
.TP
\fB\-c \fIcoordinates\fR
.br
specifies either an alternate station coordinate file or explicit 
channel coordinates in the format \fIlat,lon,elev\fR.  If the coordinates
string does not contain a comma, it is assumed to be an alternate station
coordinate file.  If the station (or channel) coordinates are available, they
will be inserted into the SAC header.  Otherwise, the default SAC value
12345.0 for "no data" will be used.
.TP
.B \-d sac | seed
specifies that the dip in the SAC header uses either the SAC or SEED convention.
The default convention is SAC dip convention.
.TP
\fB\-e \fIevent_file\fR
.br
specifies the name of the spyder event file that contains the
specific channel orientation.  If no event file is specified, the
default channel orientation will be assumed.  The explicit orientation
is assumed to be in SEED conventions for dip and azimuth irregardless of
the output convention.  For an output SAC dip convention, the SEED dip
will be converted to a SAC dip convention.
.TP
\fB\-G \fItol\fR
will terminate the data being converted into SAC format when the
total time slew for the channel exceeds tol ticks.  
Any remaining data in the file will not be converted into SAC format.
A tick is 1/10 msec.  
.TP
.B -n seed | comp | \fIevent_name\fR
specifies that the event name KEVNM in the SAC header should be
constructed from the SEED channel name and date, the component and
stream name and data (Quanterra format), or explicitly set to the
specified event_name string.
.TP
\fIinput_file\fR
.br
is the name of the input data file containing a single channel of MiniSEED data.  
If no file is specified, data is read from stdin.
.TP
\fIoutput_file\fR
.br
is the name of the output data file which will containing the SAC format data.
If no file is specified, data is written to stdout.
You may not specify an output filename on the command line unless you also 
specify an input filename.
.SH NOTES
The station coordinate file contains one line per station with at least
the following information:
.nf
	ARC   40.877  -124.075    60.0
	BKS   37.877  -122.235   276.0
.fi
where the required fields are: station name, latitude, longitude, and 
elevation.  Latitude and longitude are both in signed decimal degrees, and
elevation is in meters.  Any additional fields following elevation are ignored.

If an event file is specified, the program will look for line in the following format
for dip and azimuth informaton:
.nf
	comp: -90 0 0 0 0 90
.fi
where the numeric values are dip and azimuth for Z, N, and E channels respectively.
.SH NOTES
1.  If the MiniSEED data for a channel in not continuous, you should use
.B sdrsplit,
which has an option to split noncontiguous data for a single channel into
separate continuous MiniSEED files.
.br
2.  The SEED channel information is stored in the following SAC header fields:
.in +1in
.nf
SEED station => SAC KSTNM		
SEED network => SAC KNETWK		
SEED channel => SAC KCMPNM
SEED location => SAC KHOLE
.in -1in
.ad 
.SH SEE ALSO
sdrsplit(1)
.SH AUTHOR
Douglas Neuhauser, UC Berkeley Seismological Laboratory
.SH BUGS
Report problems or suggestions to
doug@seismo.berkeley.edu
