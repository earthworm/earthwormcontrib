/************************************************************************/
/*  Routine to initialize SAC header.					*/
/*									*/
/*	Douglas Neuhauser						*/
/*	Seismological Laboratory					*/
/*	University of California, Berkeley				*/
/*	doug@seismo.berkeley.edu					*/
/*									*/
/************************************************************************/

/*
 * Copyright (c) 1996-2004 The Regents of the University of California.
 * All Rights Reserved.
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for educational, research and non-profit purposes,
 * without fee, and without a written agreement is hereby granted,
 * provided that the above copyright notice, this paragraph and the
 * following three paragraphs appear in all copies.
 * 
 * Permission to incorporate this software into commercial products may
 * be obtained from the Office of Technology Licensing, 2150 Shattuck
 * Avenue, Suite 510, Berkeley, CA  94704.
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND
 * ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE
 * PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
 * CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */

#ifndef lint
static char sccsid[] = "$Id: init_sac_header.c 524 2010-05-07 19:11:39Z luetgert $ ";
#endif

/* Modification History
 *  95/01/13 DSN    Fixed binary output -- was skipping first point.
    96/09/15 DSN    Add event info to SAC header.
*/

#include <stdio.h>

#include <qlib2.h>

#include "procs.h"
#include "sachead.h"

#define	IUNKN	5
#define	ITIME	1
#define	IO	11
#define	IQUAKE	40

#define MAXBOOL	5

/************************************************************************/
/*  init_sac_header -							*/
/*	Initialize SAC file.						*/
/*  Return: 0 on success, non-zero on failure.				*/
/************************************************************************/
int init_sac_header
   (struct SAChead *SAChead,	/* ptr to SAC header structure.		*/
    DATA_HDR *hdr,		/* ptr to DATA_HDR structure.		*/
    STN_INFO *stn,		/* ptr to station info structure.	*/
    EVT_INFO *evt,		/* ptr to event info structure.		*/
    int npts)			/* # of data points to be written.	*/
{
    INT_TIME it;
    EXT_TIME reftime;
    char station_name[K_LEN+1];
    char event_name[80];
    double d, diff;
    int seconds, usecs;
    int i;
    struct SAChead2 *SAChead2 = (struct SAChead2 *)SAChead;
    BS		    *bs;	/* ptr to blockette structure.		*/

    /* Initialize the SAC header.    */
    for (i=0;i<NUM_FLOAT;i++) SAChead2->SACfloat[i] = SACUNDEF;
    for (i=0;i<MAXINT;i++)   SAChead2->SACint[i] =   SACUNDEF;
    for (i=0;i<MAXBOOL;i++)  SAChead2->SACun[i] =    SACUNDEF;
    for (i=0;i<MAXSTRING;i++) 
	strncpy(&(SAChead2->SACstring[i][0]),SACSTRUNDEF,strlen(SACSTRUNDEF));
    /* Compute the event time, B, and E the same way that as Quanterra.	*/
    it = hdr->begtime;
    if (it.usec > (USECS_PER_SEC/2)) {
 	it = add_time (it, 1, 0);
	reftime = int_to_ext (it);
	reftime.usec -= USECS_PER_SEC;
    }
    else reftime = int_to_ext (it);

    /* Fill in the information that we have and/or need.    */

    /* SAC sample interval time delta = 1/samples_per_second */
    /* Use blockette 100 if found.  */
    if ((bs=find_blockette(hdr, 100))) {
	double actual_rate;
        BLOCKETTE_100 *b = (BLOCKETTE_100 *) bs->pb;
	actual_rate = b->actual_rate;
	SAChead->delta = 1.0/actual_rate;
    }
    else {
	SAChead->delta = 1.0/sps_rate(hdr->sample_rate,hdr->sample_rate_mult); 
    }
    SAChead->npts = npts;			/* Number of samples.	*/
    /* Date, and start and stop X axis times.				*/
    /* For unknown reasons, Quanterra sets msec = 0, and incorporates	*/
    /* the sub-second time component into B and E, which are the	*/
    /* begin and end independent variables (time axis).			*/
    SAChead->nzyear = reftime.year;		
    SAChead->nzjday = reftime.doy;
    SAChead->nzhour = reftime.hour;
    SAChead->nzmin = reftime.minute;
    SAChead->nzsec = reftime.second;
    SAChead->nzmsec = 0.;
    SAChead->b = ((double)reftime.usec)/USECS_PER_SEC;
    time_interval2 (npts-1, hdr->sample_rate, hdr->sample_rate_mult, 
		    &seconds, &usecs);
    SAChead->e = ((double)(reftime.usec + (double)seconds*USECS_PER_SEC + usecs))/
	USECS_PER_SEC;
    d = ((double)(reftime.usec + (double)seconds*USECS_PER_SEC + usecs))/
	USECS_PER_SEC;
    reftime.usec = 0;		    /* reftime is now sac reference time.*/

    /* Arrival time - set to 0.  */
    /*:: SAChead->a = 0.; */

    /* Station coordinates. */
    SAChead->stla = stn->lat;
    SAChead->stlo = stn->lon;
    SAChead->stel = stn->el;

    /* Channel orientation. */
    SAChead->cmpaz = stn->az;
    SAChead->cmpinc = stn->dip;

    /* Event info.	    */
    SAChead->evla = evt->lat;
    SAChead->evlo = evt->lon;
    SAChead->evdp = evt->depth;
    if (evt->otime.year != 0) {
	diff = tdiff (ext_to_int(evt->otime), ext_to_int(reftime));
	SAChead->o = diff / USECS_PER_SEC;
    }

    /* Boolean flags.	*/
    SAChead->leven = 1;				/* Evenly spaced pts.	*/
    SAChead->lpspol = 1;			/* Positive polarity.	*/
    SAChead->lovrok = 1;			/* OK to overwrite.	*/
    SAChead->lcalda = 1;			/* calc values from sta	*/

    /* Misc SAC header values.						*/
    SAChead->iftype = ITIME;			/* time series.		*/
    SAChead->idep = IUNKN;			/* unknown dependent var*/
    SAChead->iztype = IO;			/* ref time = event time*/
    SAChead->internal4 = 6;			/* SAC hdr version #	*/
    SAChead->ievtyp = IQUAKE;			/* event type.		*/

    /* Station name and channel.  
    /* Use Quanterra convention for station name.*/
    memset (SAChead->kstnm,  ' ', K_LEN);
    memset (SAChead->kcmpnm, ' ', K_LEN);
    memset (SAChead->knetwk, ' ', K_LEN);
    memset (SAChead->khole,  ' ', K_LEN);
    strncpy (SAChead->kstnm,  stn->station, strlen(stn->station));
    strncpy (SAChead->kcmpnm, stn->channel, strlen(stn->channel));
    strncpy (SAChead->knetwk, stn->network, strlen(stn->network));
    strncpy (SAChead->khole, stn->location, strlen(stn->location));

    /* Determine the format of the event_name string.			*/
    if (strcasecmp(evt->evtname_string,"seed") == 0) {
	/* Use time and seed channel in event field.			*/
	sprintf (event_name, "%02d%03d%02d%02d%s", reftime.year%100, 
		 reftime.doy, reftime.hour, reftime.minute, hdr->channel_id);
    }
    else if (strcasecmp(evt->evtname_string,"comp") == 0) {
	/* Use Quanterra convention for date and channel info in event field.*/
	sprintf (event_name, "%c%02d%03d%02d%02d%c%s", hdr->channel_id[1], 
		 reftime.year%100, reftime.doy, reftime.hour, reftime.minute, 
		 stn->comp[0], stn->stream);
    }
    else {
	strcpy (event_name, evt->evtname_string);
    }

    for (i=strlen(event_name); i<KEVNMLEN; i++) event_name[i] = ' ';
    event_name[i] = '\0';
    strncpy (SAChead->kevnm, event_name, KEVNMLEN);
	     
    /* Fields that are set for unknown reasons.	*/
    SAChead->internal5 = SAChead->internal6 = 0;
    SAChead->lblank1 = 0;

    return (0);
}
