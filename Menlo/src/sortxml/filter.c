/******************************************************************************/
/*  filter.c                                                                  */
/*  Read a strong motion XML data file (from the NSMP system),                */
/*  Extracts the instrument location and determines where the file should go. */
/*                                                                            */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include "sortxmlfile.h"

/* Globals from sortxmlfile.c
 ***************************/
extern AUTHREG     AuthReg[MAX_INST];
extern int         numInst;
extern int         FTOK;
extern int         Debug;

/******************************************************************************
 * filter()  Read and process the .evt file.                                  *
 ******************************************************************************/
int filter( FILE *fp, char *fname )
{
    char     whoami[50], line[250];
    char     *latptr, *lonptr;
    int      i, j, k, tlen, stat;
	int    gotit, gotit2;
    unsigned long  channels;
	float  lat, lon;

/* Initialize variables 
 **********************/
    sprintf(whoami, " %s: %s: ", "sortxmlfile", "filter");

/* Close/reopen file in binary read
 **********************************/
    fclose( fp );
    fp = fopen( fname, "rb" );
    if( fp == NULL )               return 0;
    if(strstr(fname, ".txt") != 0) return 0;
    if(strstr(fname, ".xml") == 0 &&
       strstr(fname, ".XML") == 0) return 0;
        
/* Extract lat and lon of the station from the XML file 
 *********************************************************/
    rewind(fp);
    
    while(fgets(line, 200, fp)) {
    	latptr = strstr(line, "lat=");
    	lonptr = strstr(line, "lon=");
    	if(latptr != 0) {
    		latptr += 5;
    		latptr[7] = '\0';
    		sscanf(latptr, "%f", &lat);
    	}
    	if(lonptr != 0) {
    		lonptr += 5;
    		lonptr[8] = '\0';
    		sscanf(lonptr, "%f", &lon);
    	}
    }

	if (Debug) logit ("e", "Coords %0.2f, %0.2f\n", lat, lon);

	for(i=0;i<numInst;i++) {
		gotit = 0;
		for(j=0; ((j < AuthReg[i].numIncReg) && (gotit == 0)); j++) {
		  /* See if the lat,lon point is inside of this polygon
		   ****************************************************/

			stat = area (AuthReg[i].IncRegion[j].num_sides, AuthReg[i].IncRegion[j].x, 
			             AuthReg[i].IncRegion[j].y, lat, lon);

			if (stat == 1) {
				if (Debug == 1) logit ("e", "in InclRegion %d of %d ==> %s\n", j, i, AuthReg[i].RegionID);
				/* Point was inside one of the IncRegions, now see if the point
				 * falls inside one of the regions which are to be excluded
				 **************************************************************/
				gotit2 = 0;
				for (k=0; ((k <  AuthReg[i].numExcReg) && (gotit2 == 0)); k++) {
					stat = area (AuthReg[i].ExcRegion[j].num_sides, AuthReg[i].ExcRegion[j].x, 
					             AuthReg[i].ExcRegion[j].y, lat, lon);

					if (stat == 1) {  /* This point should be excluded, based on ExclRegion */
						if (Debug == 1) logit ("e", "in ExclRegion %d ==> IGNORING.\n", k);
						gotit2 = 1;
					}
				}

				if (gotit2 == 0) {	
/*					if (Debug == 1) logit ("e", "not in any ExclRegion ==> Okay to pass so far.\n");
					*/
					gotit = 1;
				}
			}

		}
		if (gotit == 0) {
/*      	if(Debug)logit ("e", "not in any InclRegion for %s ==> IGNORING.\n", AuthReg[i].RegionID);
		    */
		}
		AuthReg[i].in_region = gotit;
    }
    
    return( 1 );
}



/******************************************************************************/
/* ** area **                                                                 */
/*                                                                            */
/* this function returns a value of 1 if the point (u,v)                      */
/* lies inside the polygon defined by arrays x and y, 0                       */
/* is returned otherwise                                                      */
/*                                                                            */
/* inputs:                                                                    */
/* 	n - number of sides to the polygon                                        */
/* 	    ( must be less or equal to 20)                                        */
/* 	x - array of dimension 21, contains n+1 x coordinates of polygon          */
/* 	y - array of dimension 21, contains n+1 y coordinates of polygon          */
/* 	  *** note *** the first point of the polygon is duplicated               */
/*         in location n+1 of the x and y arrays.                             */
/* 	u - x coordinate of point to check                                        */
/* 	v - y coordinate of point to check                                        */
/*                                                                            */
/* outputs:                                                                   */
/* 	iout = 1 if the point is inside or on the polygon                         */
/* 		0 otherwise.                                                          */
/*                                                                            */
/* calls: cntsct                                                              */
/*                        algorithm by ron sheen - 1972                       */
/******************************************************************************/
int area (int n, float *x, float *y, float u, float v)
{
    int i, iout;

    i = cntsct(n, x, y, u, v);

    iout = 2 * ((i + 1) / 2) - i;

    return iout;
}


/******************************************************************************/
/* ** cntsct **                                                               */
/*                                                                            */
/* this function returns the count of the number of times a ray               */
/* projected from point (u,v) intersects the polygon defined in the           */
/* arrays x and y.                                                            */
/*                                                                            */
/* inputs:                                                                    */
/* 	n = the number of sides to polygon                                        */
/* 	x = n+1 x coordinates of polygon                                          */
/* 	y = n+1 y coordinates of polygon                                          */
/* 	   *** note *** element n+1 of the x and y arrays are                     */
/* 	   duplicates of element 1 of both arrays.                                */
/* 	u = x coordinate                                                          */
/* 	v = y coordinate                                                          */
/*                                                                            */
/* outputs:                                                                   */
/* 	cntsct = count of intersections                                           */
/*                                                                            */
/* calls:  	isect                                                             */
/*                        algorithm by ron sheen - 1972                       */
/******************************************************************************/
int cntsct (int n, float *x, float *y, float u, float v)
{
    int isec, i, itimes;

    itimes = 0;
    for(i=0; i<n; i++) {
		isec = isect(x[i], y[i], x[i + 1], y[i + 1], u, v);

		if (isec == 2) return (1);
		
		itimes += isec;
    }
    return (itimes);
}


/******************************************************************************/
/* ** isect **                                                                */
/* this function determines whether a ray projected from (u,v)                */
/* intersects or lies on the line through (x1,y1), (x2,y2).                   */
/*                                                                            */
/* inputs:                                                                    */
/* 	x1 = x-coordinate of first point                                          */
/* 	y1 = y-coordinate of first point                                          */
/* 	x2 = x-coordinate of second point                                         */
/* 	y2 = y-coordinate of second point                                         */
/* 	u  = x-coordinate of test point                                           */
/* 	v  = y-coordinate of test point                                           */
/*                                                                            */
/* outputs:                                                                   */
/* 	isect = 0; ray does not intersect line                                    */
/* 	isect = 1; ray does intersect line                                        */
/* 	isect = 2; ray lies on line                                               */
/*                                                                            */
/* calls:  	fabs                                                              */
/*                        algorithm by ron sheen - 1972                       */
/******************************************************************************/
int isect (float x1, float y1, float x2, float y2, float u, float v)
{
    float  yr, xt, eps;
    int    ret_val;

	eps = (float)0.00001;
	yr = v;
	if (((fabs (yr - y1) < eps)  && (fabs (y1 - y2) < eps)) &&
		(((u < (x1 + eps)) && (u > (x2 + eps))) ||
		 ((u < (x2 + eps)) && (u > (x1 - eps)))))
	{
    	ret_val = 2;
	}

	else if (((fabs (u - x1) < eps) && (fabs (x1 - x2) < eps)) && 
			(((yr < (y1 + eps)) && (yr > (y2 - eps))) || 
			((yr < (y2 + eps)) && (yr > (y1 - eps)))))
	{
    	ret_val = 2;
	}

	else if ((fabs (yr - y1) < eps) || (fabs (yr - y2) < eps)) {
		yr = (float) (yr + (eps * 10.0));
    }


    if (((yr < (y2 + eps)) && (yr > (y1 - eps))) || 
			((yr < (y1 + eps)) && (yr > (y2 - eps)))) {
		if ((u < (x1 + eps)) || (yr < (x2 + eps))) {
    		if ((u > (x1 - eps)) || (u > (x2 - eps))) {
    			yr = v;
			    xt = x1 + (((x2 - x1) * (yr - y1)) / (y2 - y1));
			    if (u <= (xt + (eps * 0.001))) {
			    	 ret_val = (fabs (u - xt) < eps)? 2:1;
				}
				else ret_val = 0;
			}
			else ret_val = 1;
		}
		else ret_val = 0;
    }
	else ret_val = 0;
	
    return ret_val;
}


