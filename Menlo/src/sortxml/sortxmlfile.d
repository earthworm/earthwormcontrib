# sortxmlfile.d
#
# Picks up files from a specified directory and reads the header. 
# Find the station location from the header.
# Copy the file to other directory(s) based on location.
#
# If it has trouble interpreting the file, it saves it to subdir ./trouble. 

# Basic Module information
#-------------------------
MyModuleId        MOD_SORTXML      # module id 
RingName          HYPO_RING	       # shared memory ring for output
HeartBeatInterval 30               # seconds between heartbeats to statmgr

LogFile           1                # 0 log to stderr/stdout only; 
                                   # 1 log to stderr/stdout and disk;
                                   # 2 log to disk module log only.

Debug             1                # 1=> debug output. 0=> no debug output

# Data file manipulation
#-----------------------
GetFromDir      /home/picker/evt/smdata/xml  # look for files in this directory
CheckPeriod     1                  # sleep this many seconds between looks
OpenTries       5                  # How many times we'll try to open a file 
OpenWait        200                # Milliseconds to wait between open tries

#
#Keyword     RegionID    Destination directory
#
 RegionDir    ALL         /home/picker/evt/smxml/save
 RegionDir    GLOBAL1     /home/picker/evt/smxml/global1
 RegionDir    GLOBAL2     /home/picker/evt/smxml/global2
 RegionDir    NSMPEIDS    /home/picker/evt/smxml/nsmpeids
 RegionDir    UAF         /home/picker/evt/smxml/uaf
 RegionDir    PACNW       /home/picker/evt/smxml/pacnw
 RegionDir    SCSNA       /home/picker/evt/smxml/socal/streama
 RegionDir    SCSNB       /home/picker/evt/smxml/socal/streamb
 RegionDir    UTAH        /home/picker/evt/smxml/utah
 RegionDir    HAWAII      /home/picker/evt/smxml/hawaii
 RegionDir    CALPINE     /home/picker/evt/smxml/calpine
#
#Keyword     RegionID    NumSides    Lat, Lon  Lat, Lon  ...
#
InclRegion   ALL      4    10. -10.   80. -10.   80. -178.   10. -178.0   10. -10.
InclRegion   GLOBAL1  4    10. -10.   80. -10.   80. -178.   10. -178.0   10. -10.
InclRegion   GLOBAL2  4    10. -10.   80. -10.   80. -178.   10. -178.0   10. -10.
InclRegion   NSMPEIDS 4    10. -10.   80. -10.   80. -178.   10. -178.0   10. -10.
 InclRegion   UAF      5    62.50 -148.00   60.00 -150.00   57.50 -157.00   58.75 -157.00   62.50 -152.50   62.50 -148.00
 InclRegion   UAF      4    51.25 -175.50   52.50 -175.50   52.50 -178.00   51.25 -178.00   51.25 -175.50
InclRegion   PACNW    4    42. -125.   42. -116.   49. -116.   49. -125.0   42. -125.
InclRegion   SCSNA    4    32. -114.   36. -114.   36. -122.   32. -122.0   32. -114.
InclRegion   SCSNB    4    32. -114.   36. -114.   36. -122.   32. -122.0   32. -114.
InclRegion   UTAH     4    37. -109.   42. -109.   42. -114.   37. -114.   37. -109.
InclRegion   HAWAII   4    16. -152.   24. -152.   24. -163.   16. -163.0   16. -152.
InclRegion   CALPINE  4    38.84 -122.76 38.84 -122.69 38.50 -122.69 38.50 -122.76 38.84 -122.76

# Peer (remote partner) heartbeat manipulation
#---------------------------------------------
PeerHeartBeatFile  terra1  HEARTBT.TXT  600 
                                   # PeerHeartBeatFile takes 3 arguments:
                                   # 1st: Name of remote system that is 
                                   #   sending the heartbeat files.
                                   # 2nd: Name of the heartbeat file. 
                                   # 3rd: maximum #seconds between heartbeat 
                                   #   files. If no new PeerHeartBeatFile arrives
                                   #   in this many seconds, an error message will
                                   #   be sent.  An "unerror message" will be
                                   #   sent after next heartbeat file arrives
                                   #   If 0, expect no heartbeat files.
                                   # Some remote systems may have multiple 
                                   # heartbeat files; list each one in a
                                   # seperate PeerHeartBeatFile command
                                   # (up to 5 allowed).
PeerHeartBeatInterval 30           # seconds between heartbeats to statmgr

#LogHeartBeatFile 1                # If non-zero, write contents of each
                                   #   heartbeat file to the daily log.


