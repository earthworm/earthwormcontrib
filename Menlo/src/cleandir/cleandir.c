
// Utility to erase all files in directory <dir> which were last modified
// more than <nday> days ago.

// This program uses WIN32 API calls.

// Will Kohler 4/3/00


#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <direct.h>        /* For _chdir() */
#include <sys/types.h>     /* For _stat() */
#include <sys/stat.h>      /* For _stat() */
#include <windows.h>


int main( int argc, char *argv[] )
{
   int    first = 1;
   char   *dir;
   double nday;
   time_t now = time(0);

/* Get command line arguments
   **************************/
   if ( argc < 3 )
   {
      printf( "Usage: cleandir <dir> <nday>\n" );
      return -1;
   }
   dir = argv[1];

   if ( sscanf( argv[2], "%lf", &nday ) == 0 )
   {
      printf( "Usage: cleandir <dir> <nday>\n" );
      return -1;
   }

/* Change current directory
   ************************/
   if ( _chdir( dir ) == -1 )
   {
      printf( "Error changing current directory to: %s\n", dir );
      printf( "%s\n", strerror(errno) );
      return -1;
   }

/* Process each file in the current directory
   ******************************************/
   while ( 1 )
   {
      static HANDLE   handle;
      const char      fileMask[] = "*";
      WIN32_FIND_DATA findData;
      struct _stat    stat;
      double          ageDays;
      char            *fname;

/* Get the name of a file
   **********************/
      if ( first )
      {
         handle = FindFirstFile( fileMask, &findData );
         if ( handle == INVALID_HANDLE_VALUE ) break;
         first = 0;
      }
      else
         if ( !FindNextFile(handle, &findData) ) break;

      fname = findData.cFileName;

/* Get file status structure
   *************************/
      if ( _stat(fname, &stat) == -1 )
      {
         printf( "_stat() error.\n" );
         printf( "%s\n", strerror(errno) );
         return -1;
      }

/* Look only at regular files which were
   modified more than <nday> days ago.
   *************************************/
      if ( !(stat.st_mode & _S_IFREG) ) continue;

      ageDays = (now - stat.st_mtime) / 86400.;
      if ( ageDays < nday ) continue;

/* Remove the file
   ***************/
      if ( remove(findData.cFileName) == -1 )
      {
         printf( "Error removing file: %s\n", fname );
         printf( "%s\n", strerror(errno) );
         continue;
         return -1;
      }
//    printf( "%30s %10.4lf\n", fname, ageDays );
   }

   return 0;
}


