#! /bin/csh -f

# cleandir.csh

# Utility to erase all files in directory <dir> which were last modified
# more than <nday> days ago.

# Will Kohler 3/30/00

if ($#argv < 2) then
   echo "Usage: cleandir <dir> <nday>"
   exit
endif

if (! -d $argv[1]) then
   echo Directory $argv[1] "does not exist. Exiting."
   exit
endif

find $argv[1] -mtime +$argv[2] -type f -exec /bin/rm {} \;

