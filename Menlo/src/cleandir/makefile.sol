#
#                      Make file for cleandir
#                         Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = cleandir_sol.o $L/sleep_ew.o $L/time_ew.o $L/chron3.o

cleandir: $O
	cc -o $B/cleandir $O -lm -lposix4

.c.o:
	cc -c ${CFLAGS} $<
 
lint:
	lint cleandir_sol.c $(GLOBALFLAGS)
 
# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/cleandir*
