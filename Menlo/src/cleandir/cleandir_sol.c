
/* Utility to erase all files in directory <dir> which were last modified
   more than n units of time ago.

   Solaris version.

   Will Kohler 6/30/06
*/


#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>        /* for chdir() */
#include <fcntl.h>         /* for stat() */
#include <sys/stat.h>      /* for stat() */
#include <sys/types.h>     /* for opendir() */
#include <dirent.h>        /* for opendir() */


int main( int argc, char *argv[] )
{
   char   *dir;                     /* Directory name */
   double nday;
   int    maxAgeSec;
   time_t now = time(0);
   DIR    *dp;                      /* Directory pointer */
   struct stat   status;
   struct dirent *dent;

   if ( argc < 3 )
   {
      printf( "Usage: cleandir <dir> <nday>\n" );
      printf( "       cleandir <dir> <n> day|hour|min|sec\n" );
      printf( "nday and n may be ints or floats.\n" );
      return -1;
   }

/* Three program arguments.
   Assume units of time are days.
   *****************************/
   if ( argc == 3 )
   {
      if ( sscanf( argv[2], "%lf", &nday ) < 1 )
      {
         printf( "Illegal number of days: %s\n", nday );
         return -1;
      }
      maxAgeSec = (int)(86400.0 * nday);
   }

/* Four or more program arguments.
   Get units of time from argv[3].
   ******************************/
   else
   {
      double nhour, nmin, nsec;

      if ( strcmp(argv[3],"day") == 0 )
      {
         if ( sscanf( argv[2], "%lf", &nday ) < 1 )
         {
            printf( "Illegal number of days: %s\n", argv[2] );
            return -1;
         }
         maxAgeSec = (int)(86400.0 * nday);
      }
      else if ( strcmp(argv[3],"hour") == 0 )
      {
         if ( sscanf( argv[2], "%lf", &nhour ) < 1 )
         {
            printf( "Illegal number of hours: %s\n", argv[2] );
            return -1;
         }
         maxAgeSec = (int)(3600.0 * nhour);
      }
      else if ( strcmp(argv[3],"min") == 0 )
      {
         if ( sscanf( argv[2], "%lf", &nmin ) < 1 )
         {
            printf( "Illegal number of minutes: %s\n", argv[2] );
            return -1;
         }
         maxAgeSec = (int)(60.0 * nmin);
      }
      else if ( strcmp(argv[3],"sec") == 0 )
      {
         if ( sscanf( argv[2], "%lf", &nsec ) < 1 )
         {
            printf( "Illegal number of seconds: %s\n", argv[2] );
            return -1;
         }
         maxAgeSec = (int)nsec;
      }
      else
      {
         printf( "Illegal time units: %s\n", argv[3] );
         return -1;
      }
   }

/* Make sure argv[1] is really a directory
   ***************************************/
   if ( stat( argv[1], &status ) == -1 )
   {
      printf( "stat() error.\n" );
      printf( "%s\n", strerror(errno) );
      return -1;
   }
   if ( !S_ISDIR( status.st_mode ) )
   {
      printf( "%s is not a directory. Exiting.\n", argv[1] );
      return -1;
   }
   dir = argv[1];

/* Open the directory to be cleaned
   ********************************/
   if ( chdir( dir ) == -1 )
   {
      printf( "Error changing current directory to: %s\n", dir );
      printf( "%s\n", strerror(errno) );
      return -1;
   }
   dp = opendir( "." );

/* Process each entry in current directory
   ***************************************/
   while ( dent = readdir(dp) )
   {
      int  ageSec;
      char *fname;

/* Get status structure for this entry.
   Look at regular files only.
   ***********************************/
      fname = dent->d_name;
      if ( stat(fname, &status) == -1 )
      {
         printf( "stat() error.\n" );
         printf( "%s\n", strerror(errno) );
         return -1;
      }
      if ( !S_ISREG( status.st_mode ) ) continue;

/* Delete files modified more than maxAgeSec seconds ago
   *****************************************************/
      ageSec = now - status.st_mtime;
      if ( ageSec > maxAgeSec )
      {
         if ( remove(fname) == -1 )
         {
            printf( "Error deleting file: %s\n", fname );
            printf( "%s\n", strerror(errno) );
            return -1;
         }
      }
   }

   closedir( dp );
   return 0;
}


