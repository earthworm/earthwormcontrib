#
#                     Make file for qdm2cube
#                         Solaris Version
#
#  The posix4 library is required for nanaosleep.
#
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = qdm2cube.o config.o sendstatus.o mature_event.o event_file.o \
    event_id.o $L/sleep_ew.o $L/logit.o $L/time_ew.o $L/dirops_ew.o \
    $L/kom.o $L/getutil.o $L/transport.o

qdm2cube: $O
	cc -o $B/qdm2cube $O -lm -lposix4
