
#include <stdio.h>
#include <string.h>
#include <earthworm.h>
#include "qdm2cube.h"

/* Function declarations
   *********************/
int GetEventId( char [], int * );
int PutEventId( char [], int );
int EventTooOld( char [] );


     /*****************************************************************
      *                     PurgeEventsSentFile()                     *
      *                                                               *
      *  Purge old events from the events-sent file.                  *
      *                                                               *
      *  Returns  0 if old events were successfully purged            *
      *          -1 if an error occured.                              *
      *****************************************************************/

int PurgeEventsSentFile()
{
   FILE *fp1, *fp2;
   char tempName[MAXC];

   extern char EventsSentFile[];     /* Name of events-sent file */
   extern int  MaxEventSentAgeSec;   /* Purge events after this many seconds */

/* Open input and temporary files
   ******************************/
   fp1 = fopen( EventsSentFile, "a+" );
   if ( fp1 == NULL )
   {
      logit( "et", "Error in PurgeEventsSentFile() opening EventsSentFile.\n" );
      return -1;
   }

   strcpy( tempName, EventsSentFile );
   strcat( tempName, ".temp" );

   fp2 = fopen( tempName, "w" );
   if ( fp2 == NULL )
   {
      fclose( fp1 );
      logit( "et", "Error in PurgeEventsSentFile() opening temp file.\n" );
      return -1;
   }

/* Copy lines from EventsSentFile to temp file,
   rejecting events that are too old.
   *******************************************/
   while ( 1 )
   {
      char event[MAXC];

      if ( fgets( event, MAXC, fp1 ) == NULL )
      {
         if ( ferror( fp1 ) )
         {
            logit( "et", "Error in PurgeEventsSentFile():\n" );
            logit( "e",  "fgets() error reading events-sent file.\n" );
            fclose( fp1 );
            fclose( fp2 );
            return -1;
         }
         break;    /* End of file encountered */
      }

      if ( strlen( event ) < 80 )
         continue;

      if ( EventTooOld( event ) ) continue;          /* Purge this event */

      if ( fprintf( fp2, "%s", event ) < 0 )         /* Write event to temp file */
      {
         logit( "et", "Error in PurgeEventsSentFile():\n" );
         logit( "e", "fprintf() error writing events-sent file.\n" );
         fclose( fp1 );
         fclose( fp2 );
         return -1;
      }
   }

   fclose( fp1 );
   fclose( fp2 );

/* Erase the EventsSentFile.  Then, rename the
   temp file to the EventsSentFile.
   *******************************************/
   if ( remove( EventsSentFile ) == -1 )
   {
      logit( "et", "Error removing EventsSentFile.\n" );
      return -1;
   }

   if ( rename( tempName, EventsSentFile ) == -1 )
   {
      logit( "et", "Error renaming file %s to %s\n", tempName, EventsSentFile );
      return -1;
   }
   return 0;
}


     /*****************************************************************
      *                       EventAlreadySent()                      *
      *                                                               *
      *  Determine if the event is in the events-sent file.           *
      *  If not, append the event to the file.                        *
      *                                                               *
      *  Returns  1 if "yes",                                         *
      *           0 if "no,                                           *
      *          -1 if an error occured.                              *
      *****************************************************************/

int EventAlreadySent( char event[] )
{
   extern char EventsSentFile[];
   FILE   *fp;
   int    found = 0;

/* Open file for reading and appending
   ***********************************/
   fp = fopen( EventsSentFile, "a+" );

/* Read each line from the events-sent file
   ****************************************/
   if ( fp != NULL )
   {
      while ( 1 )
      {
         char line[MAXC];

         if ( fgets( line, MAXC, fp ) == NULL )
         {
            if ( ferror( fp ) )
            {
               logit( "et", "qdm2cube: fgets() error reading QDM events-sent file.\n" );
               fclose( fp );
               return -1;
            }
            break;    /* End of file encountered */
         }

         if ( memcmp( &line[2], &event[2], 8 ) == 0 )    /* Do the event ids match? */
         {
            found = 1;
            break;
         }
      }
   }

/* If the event wasn't found in the events-sent file,
   append it to the file.
   *************************************************/
   if ( !found )
   {
      event[80] = '\0';

      if ( fprintf( fp, "%s\n", event ) < 0 )
      {
         logit( "et", "qdm2cube: fprintf() error writing events-sent file.\n" );
         fclose( fp );
         return -1;
      }
   }
   fclose( fp );
   return found;
}


     /*****************************************************************
      *                       WriteCubeEvent()                        *
      *                                                               *
      *  Write a cube event to its own file.  Assign an event id.     *
      *                                                               *
      *  Returns  0 if the cube event was successfully written,       *
      *          -1 if an error occured.                              *
      *****************************************************************/

int WriteCubeEvent( char event[], int *evid )
{
   extern char OutputDir[];     /* Name of directory to contain cube files */
   extern char TempSubdir[];    /* Subdir for temporary files */
   extern char EventIdFname[];  /* Name of file containing the event id
                                /*   to be assigned to the next cube event */
   FILE *fp;
   int  event_id;
   char fname[80];              /* Name of output file */
   char fname2[80];

/* Get current event id
   ********************/
   if ( GetEventId( EventIdFname, &event_id ) < 0 )
   {
      logit( "et", "Can't read event id from file.\n" );
      return -1;
   }

/* Write event to temporary file
   *****************************/
   sprintf( fname, "%s/out.%d", TempSubdir, event_id );

   fp = fopen( fname, "w" );
   if ( fp == NULL )
   {
      logit( "et", "qdm2cube: Can't create temporary cube event file.\n" );
      return -1;
   }
   if ( fprintf( fp, "%s\n", event ) < 0 )
   {
      logit( "et", "qdm2cube: fprintf() error writing temporary cube file.\n" );
      fclose( fp );
      return -1;
   }
   fclose( fp );

/* Move cube event file to final destination
   *****************************************/
   sprintf( fname2, "out.%d", event_id );
   if ( rename( fname, fname2 ) == -1 )
   {
      logit( "et", "qdm2cube: Error renaming cube event file.\n" );
      return -1;
   }

/* Return event id to calling program
   **********************************/
   *evid = event_id;

/* Update event id and write it to event id file
   *********************************************/
   event_id++;
   if ( PutEventId( EventIdFname, event_id ) < 0 )
   {
      logit( "et", "qdm2cube: Can't write event id to file.\n" );
      return -1;
   }
   return 0;
}

