
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <time_ew.h>


/***********************************************************
 *                     GetEventTime()                      *
 *                                                         *
 *  Convert the origin time of a cube event to seconds.    *
 ***********************************************************/

time_t GetEventTime( char event[] )
{
   char   *cubePtr = &event[0];
   char   str[20];
   int    year, month, day, hour, min, secd, sec;
   struct tm stm;


/* Decode event time and location
   ******************************/
   memcpy( str, cubePtr+13, 4 );
   str[4] = '\0';
   if ( sscanf( str, "%d", &year ) < 1 )  return -1;
   memcpy( str, cubePtr+17, 2 );
   str[2] = '\0';
   if ( sscanf( str, "%d", &month ) < 1 ) return -1;
   memcpy( str, cubePtr+19, 2 );
   str[2] = '\0';
   if ( sscanf( str, "%d", &day ) < 1 )   return -1;
   memcpy( str, cubePtr+21, 2 );
   str[2] = '\0';
   if ( sscanf( str, "%d", &hour ) < 1 )  return -1;
   memcpy( str, cubePtr+23, 2 );
   str[2] = '\0';
   if ( sscanf( str, "%d", &min ) < 1 )   return -1;
   memcpy( str, cubePtr+25, 3 );
   str[3] = '\0';
   if ( sscanf( str, "%d", &secd ) < 1 )  return -1;
   sec = secd / 10;

/* Convert event time to integer
   *****************************/
   stm.tm_year = year - 1900;
   stm.tm_mon  = month - 1;
   stm.tm_mday = day;
   stm.tm_hour = hour;
   stm.tm_min  = min;
   stm.tm_sec  = sec;
   return timegm_ew( &stm );
}


int MatureEvent( char event[] )
{
   extern int MaturityAgeSec;     /* Events are final after this many days */

   time_t current_time = time(0);
   time_t eventTime    = GetEventTime( event );

   if ( current_time >= (eventTime + MaturityAgeSec) )
      return 1;      /* Mature */
   else
      return 0;      /* Immature */
}


int EventTooOld( char event[] )
{
   extern int MaxEventSentAgeSec; /* Events are purged after this many days */

   time_t current_time = time(0);
   time_t eventTime    = GetEventTime( event );

   if ( current_time >= (eventTime + MaxEventSentAgeSec) )
      return 1;      /* Purge */
   else
      return 0;      /* Don't purge */
}

