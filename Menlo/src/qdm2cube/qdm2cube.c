
  /*********************************************************************
   *                              qdm2cube                             *
   *                                                                   *
   *  This program reads a cube format catalog from QDM and writes a   *
   *  cube format file for each event.  Events are not written until   *
   *  several days after the event origin time.  This allow revisions  *
   *  to be made.                                                      *
   *********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include <time_ew.h>
#include "qdm2cube.h"

/* For getpid */
#ifdef _SOLARIS
#include <unistd.h>
#endif
#ifdef _WINNT
#include <process.h>
#endif

/* Function declarations
   *********************/
void GetConfig( char * );
void LogConfig( void );
void Lookup( void );
void SendStatus( unsigned char, short, char * );
int  MatureEvent( char [] );
int  EventAlreadySent( char [] );
int  WriteCubeEvent( char [], int * );
int  PurgeEventsSentFile( void );

/* Global variables
   ****************/
SHM_INFO region;                   /* Shared memory region */
pid_t    myPid;                    /* Process id of this process */
char     TempSubdir[] = "temp";    /* Subdir for temporary files */


int main( int argc, char *argv[] )
{
   extern char RingName[20];       /* Name of transport ring */
   extern char CatalogFile[80];    /* Name of QDM catalog file */
   extern int  LogFile;            /* If 1, log to disk */
   extern long HeartBeatInterval;  /* Seconds between heartbeats */
   extern long CatCheckInterval;   /* Seconds between catalog checks */
   extern long RingKey;            /* Key of transport ring */
   extern char OutputDir[80];      /* Name of directory to contain cube files */

   extern unsigned char MyModId;   /* Module Id for this program */
   extern unsigned char TypeHeartBeat;
   extern unsigned char TypeError;

   long timeNow;                   /* Current time */
   long timeLastBeat;              /* Time last heartbeat was sent */
   long timeLastCatCheck;          /* Time last QDM catalog check */
   char text[100];                 /* To contain status messages */

   char defaultConfig[] = "qdm2cube.d";
   char *configFileName = (argc > 1 ) ? argv[1] : &defaultConfig[0];

/* Read the configuration file
   ***************************/
   GetConfig( configFileName );

/* Look up important info in the earthworm.h tables
   ************************************************/
   Lookup();

/* Set up logging
   **************/
   logit_init( argv[0], (short)MyModId, 256, LogFile );

/* Get my own pid for restart purposes
   ***********************************/
#ifdef _SOLARIS
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "qdm2cube: Can't get my pid. Exiting.\n" );
      return -1;
   }
#endif
#ifdef _WINNT
   myPid = _getpid();
#endif

/* Log the configuration file parameters
   *************************************/
   LogConfig();

/* Change to directory which will contain new cube files
   *****************************************************/
   if ( chdir_ew( OutputDir ) == -1 )
   {
      logit( "e", "qdm2cube: OutputDir directory <%s> not found. "
             "Exiting.\n", OutputDir );
      return -1;
   }
   logit( "t", "Changed default directory to: %s\n", OutputDir );

/* Make sure temp directory exists
   *******************************/
   if ( CreateDir( TempSubdir ) != EW_SUCCESS )
   {
      logit( "e", "qdm2cube: Can't create the temp directory: %s/%s\n",
              OutputDir, TempSubdir );
      return -1;
   }
   logit( "t", "Created temporary directory: %s\n", TempSubdir );

/* Attach to shared memory ring
   ****************************/
   tport_attach( &region, RingKey );
   logit( "t", "Attached to shared memory ring: %d\n", RingKey );

/* Force a heartbeat and a catalog check to be
   issued in first pass through main loop
   *******************************************/
   timeNow = time(0);
   timeLastBeat     = timeNow - HeartBeatInterval - 1;
   timeLastCatCheck = timeNow - CatCheckInterval  - 1;

/* Execute this loop frequently to beat heart
   and check for termination requests
   ******************************************/
   while ( 1 )
   {
      FILE *fp;
      int  tportFlag = tport_getflag( &region );

/* See if termination has been requested
   *************************************/
      if ( tportFlag == TERMINATE ) break;
      if ( tportFlag == myPid     ) break;

/* Every HeartBeatInterval seconds, beat the heart
   ***********************************************/
      timeNow = time(0);           /* Get current time */
      if ( (timeNow - timeLastBeat) >= HeartBeatInterval )
      {
          timeLastBeat = timeNow;
          SendStatus( TypeHeartBeat, 0, "" );
      }

/* Every CatCheckInterval seconds, get events from QDM catalog
   ***********************************************************/
      if  ( (timeNow - timeLastCatCheck) < CatCheckInterval )
      {
         sleep_ew( 1000 );
         continue;
      }
      timeLastCatCheck = timeNow;

/* Purge the events-sent file
   **************************/
      if ( PurgeEventsSentFile() == -1 )
      {
         logit( "e", "qdm2cube: PurgeEventsSendFile() error. Exiting.\n" );
         break;
      }

/* Open QDM catalog file for reading only
   **************************************/
      fp = fopen( CatalogFile, "rb" );
      if ( fp == NULL )
      {
         logit( "e", "qdm2cube: Can't open the QDM catalog file. Exiting.\n" );
         break;
      }

/* Get all lines from QDM catalog file
   ***********************************/
      while ( 1 )
      {
         char line[MAXC];
         int  nullfound = 0;
         int  linelen;
         int  i;
         int  rc;

         line[0] = '\0';
         if ( fgets( line, MAXC, fp ) == NULL )
         {
            if ( ferror( fp ) )
               logit( "e", "qdm2cube: fgets() error reading QDM catalog file.\n" );
            break;
         }

/* Convert catalog line to string
   ******************************/
         for ( i = 0; i < MAXC; i++ )
            if ( line[i] == '\0' )
            {
               nullfound = 1;
               if ( line[i-1] == '\n' )
               {
                  line[i-1] = '\0';
                  break;
               }
            }

/* Is line too long for buffer?
   ****************************/
         if ( !nullfound )
         {
            sprintf( text, "Line too long in QDM catalog file (> %d chars)\n",
                   MAXC-1 );
            SendStatus( TypeError, ERR_LINETOOLONG, text );
            continue;
         }

/* Is line too short to be of interest?
   ************************************/
         linelen = i;
         if ( linelen < 28 ) continue;

/* Is the line not an NEIC event?
   NEIC events have a network code of "US".
   ***************************************/
         if ( strncmp( line, "E ", 2 ) != 0 ) continue;
         if ( toupper(line[10]) != 'U' )      continue;
         if ( toupper(line[11]) != 'S' )      continue;

/* Does the event exceed the magnitude threshold?
   If we can't decode magnitude, ignore event.
   *********************************************/
   {
      extern int MinMag;   /* In tenths, events smaller than this are ignored */
      int    mag;
      char   str[20];

      memcpy( str, &line[47], 2 );
      str[2] = '\0';
      if ( sscanf( str, "%d", &mag ) < 1 ) continue;
      if ( mag < MinMag ) continue;
   }

/* Is the event old enough?
   ************************/
         rc = MatureEvent( line );
         if ( rc == -1 )
         {
            sprintf( text, "Error decoding cube event\n%s\n", line );
            SendStatus( TypeError, ERR_QDMDECODE, text );
            continue;
         }
         if ( rc == 0 ) continue;   /* Event isn't old enough */

/* Is the event in the events-sent file?
   If, not append the event to the events-sent file,
   and write the event to an output file.
   ************************************************/
         rc = EventAlreadySent( line );
         if ( rc == -1 )
         {
            logit( "et", "EventAlreadySent() error. Exiting.\n" );
            return -1;
         }
         if ( rc == 0 )             /* Event not already sent */
         {
            int event_id;

            if ( WriteCubeEvent( line, &event_id ) == -1 )
            {
               logit( "et", "WriteCubeEvent() error. Exiting.\n" );
               return -1;
            }
            logit( "et", "Event %d written to output file:\n%s\n", event_id, line );
         }
      }

      fclose( fp );                 /* Close catalog file */
   }

/* Program exiting normally
   ************************/
   logit( "et", "qdm2cube: Termination requested. Exiting.\n" );
   return 0;
}

