#
# qdm2cube.d  -  Configuration file for the qdm2cube program
#
MyModName         MOD_QDM2CUBE   # Module name for this instance of qdm2cube.
                                 #   The specified name must appear in the
                                 #   earthworm.d configuration file.
RingName          HYPO_RING      # Triglist, heartbeat, and error messages
                                 #   are sent to this ring.
LogFile           1              # Set to 0 to disable logging to disk.
HeartBeatInterval 15             # Seconds between heartbeats sent to statmgr
CatCheckInterval  3600           # Seconds between QDM catalog checks
MaturityAge       4.0            # Events are considered final this many days
                                 #   origin time.
MaxEventSentAge   14.0           # Purge events from events_sent file this
                                 #   many days after origin time.
MinMag            50             # In tenths; events smaller than this are ignored
#
CatalogFile       /home/picker/QDM/catalog/merge.nts
                                 # Name of the QDM catalog file.
#
EventsSentFile    /home/earthworm/run/params/qdm_events_sent
                                 # Name of events_sent file
#
OutputDir         /home/picker/CUBE/outputdir
                                 # Cube files are written to this directory.
#
EventIdFname      /home/earthworm/run/params/event_id.qdm2cube
                                 # Name of file containing the event id
                                 #   to be assigned to the next qdm event

