
/*   qdm2cube.h    */

#define ERR_LINETOOLONG    0  /* Line in QDM file is too big for buffer */
#define ERR_QDMDECODE      1  /* Error decoding QDM event line */

#define MAXC             100  /* Size of buffer to contain line from QDM file */

