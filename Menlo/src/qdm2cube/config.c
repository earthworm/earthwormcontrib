
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include "qdm2cube.h"

/* Read these from the configuration file
   **************************************/
int    LogFile;                /* Flag value, 0 - 1 */
char   MyModName[20];          /* Speak as this module name/id */
char   RingName[20];           /* Name of transport ring for i/o */
long   HeartBeatInterval;      /* Seconds between heartbeats */
long   CatCheckInterval;       /* Seconds between catalog checks */
char   CatalogFile[80];        /* Name of catalog file */
char   EventsSentFile[80];     /* Name of events-sent file */
int    MaxEventSentAgeSec;     /* Seconds, instead of days */
int    MaturityAgeSec;         /* Seconds, instead of days */
char   OutputDir[80];          /* Name of directory to contain cube files */
char   EventIdFname[80];       /* Name of file containing the event id
                                  to be assigned to the next cube event */
int    MinMag;                 /* In tenths; events smaller than this are ignored */

static double MaxEventSentAge; /* Purge events from events_sent file this
                                  many days after origin time */
static double MaturityAge;     /* Events are final this many days
                                  after origin time */

/* Look these up in the earthworm.h tables with getutil functions
   **************************************************************/
long          RingKey;         /* Key of transport ring for i/o */
unsigned char InstId;          /* Local installation id */
unsigned char MyModId;         /* Module Id for this program */
unsigned char TypeHeartBeat;
unsigned char TypeError;


#define NCOMMAND 12

void GetConfig( char *configfile )
{
   const    ncommand = NCOMMAND;    /* Process this many required commands */
   char     init[NCOMMAND];         /* Init flags, one for each command */
   int      nmiss;                  /* Number of missing commands */
   char     *com;
   char     *str;
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
        printf( "qdm2cube: Error opening command file <%s>. Exiting.\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         com = k_str();         /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )           continue;
         if ( com[0] == '#' )  continue;

/* Open a nested configuration file
   ********************************/
         if( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "qdm2cube: Error opening command file <%s>. Exiting.\n",
                        &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         if ( k_its("LogFile") )
         {
            LogFile = k_int();
            init[0] = 1;
         }
         else if ( k_its("MyModName") )
         {
            str = k_str();
            if (str) strcpy( MyModName, str );
            init[1] = 1;
         }
         else if ( k_its("RingName") )
         {
            str = k_str();
            if (str) strcpy( RingName, str );
            init[2] = 1;
         }
         else if ( k_its("HeartBeatInterval") )
         {
            HeartBeatInterval = k_long();
            init[3] = 1;
         }
         else if ( k_its("CatalogFile") )
         {
            str = k_str();
            if (str) strcpy( (char *)CatalogFile, str );
            init[4] = 1;
         }
         else if ( k_its("CatCheckInterval") )
         {
            CatCheckInterval = k_long();
            init[5] = 1;
         }
         else if ( k_its("EventsSentFile") )
         {
            str = k_str();
            if (str) strcpy( (char *)EventsSentFile, str );
            init[6] = 1;
         }
         else if ( k_its("MaxEventSentAge") )
         {
            MaxEventSentAge    = k_val();
            MaxEventSentAgeSec = (int)(86400.0 * MaxEventSentAge);
            init[7] = 1;
         }
         else if ( k_its("MaturityAge") )
         {
            MaturityAge    = k_val();
            MaturityAgeSec = (int)(86400.0 * MaturityAge);
            init[8] = 1;
         }
         else if ( k_its("OutputDir") )
         {
            str = k_str();
            if (str) strcpy( (char *)OutputDir, str );
            init[9] = 1;
         }
         else if ( k_its("EventIdFname") )
         {
            str = k_str();
            if (str) strcpy( (char *)EventIdFname, str );
            init[10] = 1;
         }
         else if ( k_its("MinMag") )
         {
            MinMag = k_int();
            init[11] = 1;
         }

/* Unknown command
   ***************/
         else
         {
            printf( "qdm2cube: <%s> Unknown command in <%s>.\n",
                     com, configfile );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "qdm2cube: Bad <%s> command in <%s>. Exiting.\n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] ) nmiss++;

   if ( nmiss )
   {
       printf( "qdm2cube: ERROR, no " );
       if ( !init[0]  ) printf( "<LogFile> "           );
       if ( !init[1]  ) printf( "<MyModName> "         );
       if ( !init[2]  ) printf( "<RingName> "          );
       if ( !init[3]  ) printf( "<HeartBeatInterval> " );
       if ( !init[4]  ) printf( "<CatalogFile> "       );
       if ( !init[5]  ) printf( "<CatCheckInterval> "  );
       if ( !init[6]  ) printf( "<EventsSentFile> "    );
       if ( !init[7]  ) printf( "<MaxEventSentAge> "   );
       if ( !init[8]  ) printf( "<MaturityAge> "       );
       if ( !init[9]  ) printf( "<OutputDir> "         );
       if ( !init[10] ) printf( "<EventIdFname> "      );
       if ( !init[11] ) printf( "<MinMag> "            );
       printf( "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }
   return;
}


     /*****************************************************************
      *  Lookup()   Look up important info from earthworm.h tables    *
      *****************************************************************/

void Lookup( void )
{
/* Look up key to shared memory region
   ***********************************/
   if ( ( RingKey = GetKey(RingName) ) == -1 )
   {
        printf( "qdm2cube:  Invalid ring name <%s>. Exiting.\n", RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "qdm2cube: Error getting local installation id. Exiting.\n" );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 )
   {
      printf( "qdm2cube: Invalid module name <%s>. Exiting.\n", MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "qdm2cube: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "qdm2cube: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   return;
}


void LogConfig( void )
{
   logit( "", "LogFile:           %d\n",    LogFile );
   logit( "", "MyModName:         %s\n",    MyModName );
   logit( "", "MyModId:           %u\n",    MyModId );
   logit( "", "RingName:          %s\n",    RingName );
   logit( "", "HeartBeatInterval: %d\n",    HeartBeatInterval );
   logit( "", "CatCheckInterval:  %d\n",    CatCheckInterval );
   logit( "", "CatalogFile:       %s\n",    CatalogFile );
   logit( "", "EventsSentFile:    %s\n",    EventsSentFile );
   logit( "", "OutputDir:         %s\n",    OutputDir );
   logit( "", "EventIdFname:      %s\n",    EventIdFname );
   logit( "", "MaxEventSentAge:   %.2lf\n", MaxEventSentAge );
   logit( "", "MaturityAge:       %.2lf\n", MaturityAge );
   logit( "", "MinMag:            %d\n",    MinMag );
   logit( "", "\n" );
   return;
}
