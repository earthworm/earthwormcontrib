########################################################################
# This is the cisnlist parameter file. 
# This module creates a list of currently served channels.
#
#                                    6/11/03   Jim Luetgert
########################################################################

# List of wave servers (ip port comment) to contact to retrieve trace data.

@/home/earthworm/run/params/all_cisn_waveservers.d
#  WaveServer     130.118.43.34   16010     " wsv3 other"

# The file with all the station information.
 StationList     /home/earthworm/run/params/calsta.db1
 StationList     /home/earthworm/run/params/nsmp.db1

# The file with all the required station information.
 RequiredList     /home/earthworm/run/params/nsmp.db1
 RequiredList     /home/earthworm/run/params/pge.db1

# Directory in which to store the temporary info file.
 GifDir   /home/picker/

# Networks to include in file.
 Network NC
 Network NP
 Network WR
 Network PG
 
# Directory in which to put info file for output.
 LocalTarget   /home/picker/cisn/cgs/
 LocalTarget   /home/picker/cisn/ucb/

# The file with all the waveserver stations.
 MenuList     Station_List

