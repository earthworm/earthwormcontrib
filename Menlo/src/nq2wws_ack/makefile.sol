#
# Makefile for nq2wws_ack
#
#

CFLAGS = -D_REENTRANT $(GLOBALFLAGS)

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

QLIB_DIR = $(EW_HOME)/$(EW_VERSION)/src/libsrc/qlib2/

CFLAGS +=  -g -I$(QLIB_DIR) 

all: qlib2 nq2wws_ack

qlib2:  FORCE
	@echo "Making qlib2"
	cd $(QLIB_DIR); make


BINARIES = nq2wws_ack.o $L/mem_circ_queue.o $L/chron3.o $L/kom.o \
	$L/getsysname_ew.o $L/getutil.o $L/logit_mt.o $L/transport.o \
	$L/sleep_ew.o $L/socket_ew.o $L/socket_ew_common.o $L/dirops_ew.o \
	$L/time_ew.o $L/threads_ew.o $L/sema_ew.o $L/swap.o

all:
	make -f makefile.sol nq2wws_ack


nq2wws_ack: $(BINARIES) 
	cc -o $B/nq2wws_ack $(BINARIES) -L$(QLIB_DIR) -lqlib2nl -lnsl -lsocket -mt -lposix4 -lthread -lc -lm



lint:
	lint nq2wws_ack.c   $(GLOBALFLAGS)


# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~
	(cd $(QLIB_DIR); make clean)

clean_bin:
	rm -f $B/nq2wws_ack* 

FORCE:
