# sortevtfile.d
#
# Picks up files from a specified directory and reads the header. 
# Find the station location from the header.
# Copy the file to other directory(s) based on location.
#
# If it has trouble interpreting the file, it saves it to subdir ./trouble. 

# Basic Module information
#-------------------------
MyModuleId        MOD_SORTEVT      # module id 
RingName          HYPO_RING	       # shared memory ring for output
HeartBeatInterval 30               # seconds between heartbeats to statmgr

LogFile           1                # 0 log to stderr/stdout only; 
                                   # 1 log to stderr/stdout and disk;
                                   # 2 log to disk module log only.

Debug             1                # 1=> debug output. 0=> no debug output

# Data file manipulation
#-----------------------
GetFromDir      /home/picker/evt/indir  # look for files in this directory
CheckPeriod     1                  # sleep this many seconds between looks
OpenTries       5                  # How many times we'll try to open a file 
OpenWait        200                # Milliseconds to wait between open tries

UseFT                    # Allow sorting of Function Tests
#
#Keyword     RegionID    Destination directory
#
RegionDir    ALL         /home/picker/evt/save
RegionDir    ALASKA      /home/picker/evt/avo
RegionDir    PACNW       /home/picker/evt/pacnw
#RegionDir    NCSN        /home/picker/evt/nocal
RegionDir    SCSN        /home/picker/evt/socal
RegionDir    UTAH        /home/picker/evt/utah
#
#Keyword     RegionID    NumSides    Lat, Lon  Lat, Lon  ...
#
InclRegion   ALL      4    10. -10.   80. -10.   80. -178.   10. -178.0   10. -10.
InclRegion   ALASKA   5    62.50 -148.00   60.00 -150.00   57.50 -157.00   58.75 -157.00   62.50 -152.50   62.50 -148.00
InclRegion   ALASKA   4    51.25 -175.50   52.50 -175.50   52.50 -178.00   51.25 -178.00   51.25 -175.50
InclRegion   PACNW    4    42. -125.   42. -116.   49. -116.   49. -125.0   42. -125.
#InclRegion   NCSN     4    35. -117.   43. -117.   43. -125.   35. -125.0   35. -117.
InclRegion   SCSN     4    32. -114.   36. -114.   36. -122.   32. -122.0   32. -114.
InclRegion   UTAH     4    37. -105.   42. -105.   42. -115.   37. -115.0   37. -105.
#InclRegion   EASTUS   4    25. -70.   45. -70.   45. -95.   25. -95.0   25. -70.

# Peer (remote partner) heartbeat manipulation
#---------------------------------------------
PeerHeartBeatFile  terra1  HEARTBT.TXT  600 
                                   # PeerHeartBeatFile takes 3 arguments:
                                   # 1st: Name of remote system that is 
                                   #   sending the heartbeat files.
                                   # 2nd: Name of the heartbeat file. 
                                   # 3rd: maximum #seconds between heartbeat 
                                   #   files. If no new PeerHeartBeatFile arrives
                                   #   in this many seconds, an error message will
                                   #   be sent.  An "unerror message" will be
                                   #   sent after next heartbeat file arrives
                                   #   If 0, expect no heartbeat files.
                                   # Some remote systems may have multiple 
                                   # heartbeat files; list each one in a
                                   # seperate PeerHeartBeatFile command
                                   # (up to 5 allowed).
PeerHeartBeatInterval 30           # seconds between heartbeats to statmgr

#LogHeartBeatFile 1                # If non-zero, write contents of each
                                   #   heartbeat file to the daily log.


