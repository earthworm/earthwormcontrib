/******************************************************************************/
/* sortevtfile.h:                                                             */
/*                                                                            */
/* JHL Dec 2001                                                               */
/*                                                                            */
/******************************************************************************/

/* Define Strong Motion file types
 *********************************/
#define MAXTRACELTH 18000
#define NAM_LEN 100	          /* length of full directory name          */
#define BUFLEN  65000         /* define maximum size for an event msg   */

/* Region structs
 *******************/
#define     MAX_SIDES                20
#define     MAX_REGIONS_PER_INST     5   
#define     MAX_INST                 20
 
typedef struct _region
{
    int     num_sides;
    float   x[MAX_SIDES + 1];
    float   y[MAX_SIDES + 1];
 
} REGION;
 
typedef struct _authreg
{
	int     instid;
	char    RegionID[NAM_LEN];
	int     numIncReg;
	REGION  IncRegion[MAX_REGIONS_PER_INST];
	int     numExcReg;
	REGION  ExcRegion[MAX_REGIONS_PER_INST];
	int     in_region;
	char    OutDir[NAM_LEN];
} AUTHREG;

/* Function prototypes
 *********************/
void config_me ( char * );
int GetIndex (char *, int *);
void ew_lookup ( void );
void ew_status ( unsigned char, short, char * );

int filter( FILE *fp, char *fname );
int read_tag( FILE *fp );
int read_head( FILE *fp );
int read_frame( FILE *fp, unsigned long *channels );
int area (int, float *, float *, float, float);
int cntsct (int, float *, float *, float, float);
int isect (float, float, float, float, float, float);

void logit( char *, char *, ... );   /* logit.c      sys-independent  */

