/******************************************************************************/
/*  filter.c                                                                  */
/*  Read a K2-format .evt data file (from the NSMP system),                   */
/*  Extracts the instrument location and determines where the file should go. */
/*                                                                            */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include "sortevtfile.h"

#include "nkwhdrs_jhl.h"              /* Kinemetrics header definitions */

#ifdef _INTEL
/*  macro returns byte-swapped version of given 16-bit unsigned integer */
#define BYTESWAP_UINT16(var) \
               ((unsigned short)(((unsigned short)(var)>>(unsigned char)8) + \
               ((unsigned short)(var)<<(unsigned char)8)))

/*  macro returns byte-swapped version of given 32-bit unsigned integer */
#define BYTESWAP_UINT32(var) \
                 ((unsigned long)(((unsigned long)(var)>>(unsigned char)24) + \
        (((unsigned long)(var)>>(unsigned char)8)&(unsigned long)0x0000FF00)+ \
        (((unsigned long)(var)<<(unsigned char)8)&(unsigned long)0x00FF0000)+ \
                                 ((unsigned long)(var)<<(unsigned char)24)))
#endif

#ifdef _SPARC
#define BYTESWAP_UINT16(var) (var)
#define BYTESWAP_UINT32(var) (var)
#endif

/* Globals from sortevtfile.c
 ***************************/
extern AUTHREG     AuthReg[MAX_INST];
extern int         numInst;
extern int         FTOK;
extern int         Debug;

static int   print_tag = 0;
static int   print_head = 0;
static int   print_frame = 0;
KFF_TAG      tag;          /* received header tag */
MW_HEADER    head;
K2_HEADER    khead;
FRAME_HEADER frame;

/******************************************************************************
 * filter()  Read and process the .evt file.                                  *
 ******************************************************************************/
int filter( FILE *fp, char *fname )
{
    char     whoami[50];
    int      i, j, k, tlen, stat;
	int    gotit, gotit2;
    unsigned long  channels;
	float  lat, lon;

/* Initialize variables 
 **********************/
    sprintf(whoami, " %s: %s: ", "sortevtfile", "filter");
    memset( &frame,  0, sizeof(FRAME_HEADER) );
    memset( &tag,    0, sizeof(KFF_TAG) );
    memset( &head,   0, sizeof(K2_HEADER) );

/* Close/reopen file in binary read
 **********************************/
    fclose( fp );
    fp = fopen( fname, "rb" );
    if( fp == NULL )               return 0;
    if(strstr(fname, ".txt") != 0) return 0;
    if(strstr(fname, ".evt") == 0 &&
       strstr(fname, ".EVT") == 0) return 0;
        
/* First, just read the header & first frame to get some basic info. 
 ******************************************************************/
    rewind(fp);
    
    tlen = read_tag(fp);
    stat = read_head(fp);
    
    tlen = read_tag(fp);
    read_frame(fp, &channels);
    
/* Extract lat and lon of the station from the .evt header 
 *********************************************************/
/*        
	lat = head.roParms.timing.gpsLatitude;
	lon = head.roParms.timing.gpsLongitude;
	     */
	lat = head.rwParms.misc.latitude;
	lon = head.rwParms.misc.longitude;

	if (Debug) logit ("e", "Coords %0.2f, %0.2f\n ", lat, lon);

	for(i=0;i<numInst;i++) {
		gotit = 0;
		for(j=0; ((j < AuthReg[i].numIncReg) && (gotit == 0)); j++) {
		  /* See if the lat,lon point is inside of this polygon
		   ****************************************************/

			stat = area (AuthReg[i].IncRegion[j].num_sides, AuthReg[i].IncRegion[j].x, 
			             AuthReg[i].IncRegion[j].y, lat, lon);

			if (stat == 1) {
				if (Debug == 1) logit ("e", "in InclRegion %d of %d ==> %s\n", j, i, AuthReg[i].RegionID);
				/* Point was inside one of the IncRegions, now see if the point
				 * falls inside one of the regions which are to be excluded
				 **************************************************************/
				gotit2 = 0;
				for (k=0; ((k <  AuthReg[i].numExcReg) && (gotit2 == 0)); k++) {
					stat = area (AuthReg[i].ExcRegion[j].num_sides, AuthReg[i].ExcRegion[j].x, 
					             AuthReg[i].ExcRegion[j].y, lat, lon);

					if (stat == 1) {  /* This point should be excluded, based on ExclRegion */
						if (Debug == 1) logit ("e", "in ExclRegion %d ==> IGNORING.\n", k);
						gotit2 = 1;
					}
				}

				if (gotit2 == 0) {	
/*					if (Debug == 1) logit ("e", "not in any ExclRegion ==> Okay to pass so far.\n");
					*/
					gotit = 1;
				}
			}

		}
		if (gotit == 0) {
/*      	if(Debug)logit ("e", "not in any InclRegion for %s ==> IGNORING.\n", AuthReg[i].RegionID);
		    */
		}
		AuthReg[i].in_region = gotit;
    }
    if(head.roParms.stream.flags != 0 && FTOK == 0) {
        logit("e", "%s %s not data. flags = %d \n", 
                    whoami, fname, head.roParms.stream.flags);
        for(i=1;i<numInst;i++) AuthReg[i].in_region = 0;
        if(head.roParms.stream.flags != 1) return 0;
    }
    
    return( 1 );
}



/******************************************************************************
 * read_tag(fp)  Read the 16 byte tag, swap bytes if necessary, and print.    *
 ******************************************************************************/
int read_tag( FILE *fp )
{
    int        stat;
    
    stat = fread(&tag, 1, 16, fp);
    tag.type       = BYTESWAP_UINT32(tag.type);
    tag.length     = BYTESWAP_UINT16(tag.length);
    tag.dataLength = BYTESWAP_UINT16(tag.dataLength);
    tag.id         = BYTESWAP_UINT16(tag.id);
    tag.checksum   = BYTESWAP_UINT16(tag.checksum);
    
    if(Debug && print_tag) {
        logit("e", "filter: TAG: %c %d %d %d %d %d %d %d %d  \n", 
                tag.sync, 
                (int)tag.byteOrder,
                (int)tag.version, 
                (int)tag.instrumentType,
                tag.type, tag.length, tag.dataLength,
                tag.id, tag.checksum);
    }
    
    return stat;
}


/******************************************************************************
 * read_head(fp)  Read the file header, swap bytes if necessary, and print.   *
 ******************************************************************************/
int read_head( FILE *fp )
{
   long       la;
   int        i, nchans, stat;
   
/* Read in the file header.
   If a K2, there will be 2040 bytes,
   otherwise assume a Mt Whitney.
 ************************************/
    /*
    stat = fread(&head, tag.length, 1, fp);
    */
    nchans = tag.length==2040? MAX_K2_CHANNELS:MAX_MW_CHANNELS;
    stat = fread(&head, 1, 8, fp);
    stat = fread(&head.roParms.misc,       1, sizeof(struct MISC_RO_PARMS)+sizeof(struct TIMING_RO_PARMS), fp);
    stat = fread(&head.roParms.channel[0], 1, sizeof(struct CHANNEL_RO_PARMS)*nchans, fp);
    stat = fread(&head.roParms.stream,     1, sizeof(struct STREAM_RO_PARMS), fp);
    
    stat = fread(&head.rwParms.misc,       1, sizeof(struct MISC_RW_PARMS)+sizeof(struct TIMING_RW_PARMS), fp);
    stat = fread(&head.rwParms.channel[0], 1, sizeof(struct CHANNEL_RW_PARMS)*nchans, fp);
    if(tag.length==2040) {
        stat = fread(&head.rwParms.stream, 1, sizeof(struct STREAM_K2_RW_PARMS), fp);
    } else {
        stat = fread(&head.rwParms.stream, 1, sizeof(struct STREAM_MW_RW_PARMS), fp);
    }
    stat = fread(&head.rwParms.modem,      1, sizeof(struct MODEM_RW_PARMS), fp);
    
    head.roParms.headerVersion = BYTESWAP_UINT16(head.roParms.headerVersion);
    head.roParms.headerBytes   = BYTESWAP_UINT16(head.roParms.headerBytes);
    
    if(Debug && print_head)
    logit("e", "HEADER: %c%c%c %d %hu %hu \n", 
            head.roParms.id[0], head.roParms.id[1], head.roParms.id[2], 
       (int)head.roParms.instrumentCode, 
            head.roParms.headerVersion, 
            head.roParms.headerBytes);
    
    head.roParms.misc.installedChan  = BYTESWAP_UINT16(head.roParms.misc.installedChan);
    head.roParms.misc.maxChannels    = BYTESWAP_UINT16(head.roParms.misc.maxChannels);
    head.roParms.misc.sysBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.sysBlkVersion);
    head.roParms.misc.bootBlkVersion = BYTESWAP_UINT16(head.roParms.misc.bootBlkVersion);
    head.roParms.misc.appBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.appBlkVersion);
    head.roParms.misc.dspBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.dspBlkVersion);
    head.roParms.misc.batteryVoltage = BYTESWAP_UINT16(head.roParms.misc.batteryVoltage);
    head.roParms.misc.crc            = BYTESWAP_UINT16(head.roParms.misc.crc);
    head.roParms.misc.flags          = BYTESWAP_UINT16(head.roParms.misc.flags);
    head.roParms.misc.temperature    = BYTESWAP_UINT16(head.roParms.misc.temperature);
    nchans = head.roParms.misc.maxChannels;
    
    if(Debug && print_head)
    logit("e", "MISC_RO_PARMS: %d %d %d %hu %hu %hu %hu %hu %hu %d %hu %hu %d \n", 
        (int)head.roParms.misc.a2dBits, 
        (int)head.roParms.misc.sampleBytes, 
        (int)head.roParms.misc.restartSource, 
            head.roParms.misc.installedChan,
            head.roParms.misc.maxChannels,
            head.roParms.misc.sysBlkVersion,
            head.roParms.misc.bootBlkVersion,
            head.roParms.misc.appBlkVersion,
            head.roParms.misc.dspBlkVersion,
            head.roParms.misc.batteryVoltage,
            head.roParms.misc.crc,
            head.roParms.misc.flags,
            head.roParms.misc.temperature );
    
    head.roParms.timing.gpsLockFailCount  = BYTESWAP_UINT16(head.roParms.timing.gpsLockFailCount);
    head.roParms.timing.gpsUpdateRTCCount = BYTESWAP_UINT16(head.roParms.timing.gpsUpdateRTCCount);
    head.roParms.timing.acqDelay          = BYTESWAP_UINT16(head.roParms.timing.acqDelay);
    head.roParms.timing.gpsLatitude       = BYTESWAP_UINT16(head.roParms.timing.gpsLatitude);
    head.roParms.timing.gpsLongitude      = BYTESWAP_UINT16(head.roParms.timing.gpsLongitude);
    head.roParms.timing.gpsAltitude       = BYTESWAP_UINT16(head.roParms.timing.gpsAltitude);
    head.roParms.timing.dacCount          = BYTESWAP_UINT16(head.roParms.timing.dacCount);
    head.roParms.timing.gpsLastDrift[0]   = BYTESWAP_UINT16(head.roParms.timing.gpsLastDrift[0]);
    head.roParms.timing.gpsLastDrift[1]   = BYTESWAP_UINT16(head.roParms.timing.gpsLastDrift[1]);
    
    head.roParms.timing.gpsLastTurnOnTime[0] = BYTESWAP_UINT32(head.roParms.timing.gpsLastTurnOnTime[0]);
    head.roParms.timing.gpsLastTurnOnTime[1] = BYTESWAP_UINT32(head.roParms.timing.gpsLastTurnOnTime[1]);
    head.roParms.timing.gpsLastUpdateTime[0] = BYTESWAP_UINT32(head.roParms.timing.gpsLastUpdateTime[0]);
    head.roParms.timing.gpsLastUpdateTime[1] = BYTESWAP_UINT32(head.roParms.timing.gpsLastUpdateTime[1]);
    head.roParms.timing.gpsLastLockTime[0]   = BYTESWAP_UINT32(head.roParms.timing.gpsLastLockTime[0]);
    head.roParms.timing.gpsLastLockTime[1]   = BYTESWAP_UINT32(head.roParms.timing.gpsLastLockTime[1]);
    
    if(Debug && print_head)
    logit("e", "TIMING_RO_PARMS: %d %d %d %hu %hu %d %d %d %d %hu %d %d %lu %lu %lu %lu %lu %lu \n", 
        (int)head.roParms.timing.clockSource, 
        (int)head.roParms.timing.gpsStatus, 
        (int)head.roParms.timing.gpsSOH, 
            head.roParms.timing.gpsLockFailCount, 
            head.roParms.timing.gpsUpdateRTCCount, 
            head.roParms.timing.acqDelay, 
            head.roParms.timing.gpsLatitude, 
            head.roParms.timing.gpsLongitude, 
            head.roParms.timing.gpsAltitude, 
            head.roParms.timing.dacCount,
            head.roParms.timing.gpsLastDrift[0], 
            head.roParms.timing.gpsLastDrift[1], 
            
            head.roParms.timing.gpsLastTurnOnTime[0], 
            head.roParms.timing.gpsLastTurnOnTime[1], 
            head.roParms.timing.gpsLastUpdateTime[0], 
            head.roParms.timing.gpsLastUpdateTime[1], 
            head.roParms.timing.gpsLastLockTime[0], 
            head.roParms.timing.gpsLastLockTime[1] );
           
    
    
    for(i=0;i<nchans;i++) {
        head.roParms.channel[i].maxPeak       = BYTESWAP_UINT32(head.roParms.channel[i].maxPeak);
        head.roParms.channel[i].maxPeakOffset = BYTESWAP_UINT32(head.roParms.channel[i].maxPeakOffset);
        head.roParms.channel[i].minPeak       = BYTESWAP_UINT32(head.roParms.channel[i].minPeak);
        head.roParms.channel[i].minPeakOffset = BYTESWAP_UINT32(head.roParms.channel[i].minPeakOffset);
        head.roParms.channel[i].mean          = BYTESWAP_UINT32(head.roParms.channel[i].mean);
        head.roParms.channel[i].aqOffset      = BYTESWAP_UINT32(head.roParms.channel[i].aqOffset);
        
        if(Debug && print_head)
        logit("e", "CHANNEL_RO_PARMS: %d %lu %d %lu %d %d \n", 
            head.roParms.channel[i].maxPeak,  
            head.roParms.channel[i].maxPeakOffset,  
            head.roParms.channel[i].minPeak,  
            head.roParms.channel[i].minPeakOffset,  
            head.roParms.channel[i].mean,  
            head.roParms.channel[i].aqOffset );
        
    }
    
    
    head.roParms.stream.startTime       = BYTESWAP_UINT32(head.roParms.stream.startTime);
    head.roParms.stream.triggerTime     = BYTESWAP_UINT32(head.roParms.stream.triggerTime);
    head.roParms.stream.duration        = BYTESWAP_UINT32(head.roParms.stream.duration);
    head.roParms.stream.errors          = BYTESWAP_UINT16(head.roParms.stream.errors);
    head.roParms.stream.flags           = BYTESWAP_UINT16(head.roParms.stream.flags);
    head.roParms.stream.nscans          = BYTESWAP_UINT32(head.roParms.stream.nscans);
    head.roParms.stream.startTimeMsec   = BYTESWAP_UINT16(head.roParms.stream.startTimeMsec);
    head.roParms.stream.triggerTimeMsec = BYTESWAP_UINT16(head.roParms.stream.triggerTimeMsec);
    
    if(Debug && print_head)
    logit("e", "STREAM_RO_PARMS: %d %d %d %d %d %d %d %d \n", 
            head.roParms.stream.startTime,  
            head.roParms.stream.triggerTime,  
            head.roParms.stream.duration,  
            head.roParms.stream.errors,  
            head.roParms.stream.flags,  
            head.roParms.stream.startTimeMsec,  
            head.roParms.stream.triggerTimeMsec,  
            head.roParms.stream.nscans  );
    
    head.rwParms.misc.serialNumber   = BYTESWAP_UINT16(head.rwParms.misc.serialNumber);
    head.rwParms.misc.nchannels      = BYTESWAP_UINT16(head.rwParms.misc.nchannels);
    head.rwParms.misc.elevation      = BYTESWAP_UINT16(head.rwParms.misc.elevation);
                                  la = BYTESWAP_UINT32(*(long *)(&head.rwParms.misc.latitude));
    head.rwParms.misc.latitude       = *(float *)(&(la));
                                  la = BYTESWAP_UINT32(*(long *)(&head.rwParms.misc.longitude));
    head.rwParms.misc.longitude      = *(float *)(&la);
    head.rwParms.misc.cutler_bitmap  = BYTESWAP_UINT32(head.rwParms.misc.cutler_bitmap);
    head.rwParms.misc.channel_bitmap = BYTESWAP_UINT32(head.rwParms.misc.channel_bitmap);
    
    if(Debug && print_head)
    logit("e", "MISC_RW_PARMS: %hu %hu %.5s %.33s %d %f %f %d %d %d %d %d %lo %d %.17s %d %d\n", 
            head.rwParms.misc.serialNumber,
            head.rwParms.misc.nchannels,
            head.rwParms.misc.stnID,  
            head.rwParms.misc.comment, 
            head.rwParms.misc.elevation,
            head.rwParms.misc.latitude, 
            head.rwParms.misc.longitude,
            
        (int)head.rwParms.misc.cutlerCode, 
        (int)head.rwParms.misc.minBatteryVoltage, 
        (int)head.rwParms.misc.cutler_decimation, 
        (int)head.rwParms.misc.cutler_irig_type, 
            head.rwParms.misc.cutler_bitmap,
            head.rwParms.misc.channel_bitmap,
        (int)head.rwParms.misc.cutler_protocol, 
            head.rwParms.misc.siteID, 
        (int)head.rwParms.misc.externalTrigger, 
        (int)head.rwParms.misc.networkFlag );
    
    head.rwParms.timing.localOffset = BYTESWAP_UINT16(head.rwParms.timing.localOffset);
    
    if(Debug && print_head)
    logit("e", "TIMING_RW_PARMS: %d %d %hu \n", 
        (int)head.rwParms.timing.gpsTurnOnInterval,  
        (int)head.rwParms.timing.gpsMaxTurnOnTime, 
            head.rwParms.timing.localOffset  );
    
    for(i=0;i<nchans;i++) {
        head.rwParms.channel[i].north      = BYTESWAP_UINT16(head.rwParms.channel[i].north);
        head.rwParms.channel[i].east       = BYTESWAP_UINT16(head.rwParms.channel[i].east);
        head.rwParms.channel[i].up         = BYTESWAP_UINT16(head.rwParms.channel[i].up);
        head.rwParms.channel[i].altitude   = BYTESWAP_UINT16(head.rwParms.channel[i].altitude);
        head.rwParms.channel[i].azimuth    = BYTESWAP_UINT16(head.rwParms.channel[i].azimuth);
        head.rwParms.channel[i].sensorType = BYTESWAP_UINT16(head.rwParms.channel[i].sensorType);
        head.rwParms.channel[i].sensorSerialNumber    = BYTESWAP_UINT16(head.rwParms.channel[i].sensorSerialNumber);
        head.rwParms.channel[i].sensorSerialNumberExt = BYTESWAP_UINT16(head.rwParms.channel[i].sensorSerialNumberExt);
        head.rwParms.channel[i].gain                  = BYTESWAP_UINT16(head.rwParms.channel[i].gain);
        head.rwParms.channel[i].StaLtaRatio           = BYTESWAP_UINT16(head.rwParms.channel[i].StaLtaRatio);
        
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].fullscale));
        head.rwParms.channel[i].fullscale             = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].sensitivity));
        head.rwParms.channel[i].sensitivity           = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].damping));
        head.rwParms.channel[i].damping               = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].naturalFrequency));
        head.rwParms.channel[i].naturalFrequency      = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].triggerThreshold));
        head.rwParms.channel[i].triggerThreshold      = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].detriggerThreshold));
        head.rwParms.channel[i].detriggerThreshold    = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].alarmTriggerThreshold));
        head.rwParms.channel[i].alarmTriggerThreshold = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].calCoil));
        head.rwParms.channel[i].calCoil               = *(float *)(&la);
        if(head.rwParms.channel[i].range != ' ' && (head.rwParms.channel[i].range < '0' || head.rwParms.channel[i].range > '9') )
        	head.rwParms.channel[i].range = ' ';
        if(head.rwParms.channel[i].sensorgain != ' ' && (head.rwParms.channel[i].sensorgain < '0' || head.rwParms.channel[i].sensorgain > '9') )
        	head.rwParms.channel[i].sensorgain = ' '; 
        	
        if(Debug && print_head)
        logit("e", "CHANNEL_RW_PARMS: %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f %f %f %f %f %f %f %f %c %c %s %s \n", 
            head.rwParms.channel[i].id,  
            head.rwParms.channel[i].sensorSerialNumberExt,  
            head.rwParms.channel[i].north,  
            head.rwParms.channel[i].east,  
            head.rwParms.channel[i].up,  
            head.rwParms.channel[i].altitude,
            head.rwParms.channel[i].azimuth,
            head.rwParms.channel[i].sensorType,
            head.rwParms.channel[i].sensorSerialNumber,
            head.rwParms.channel[i].gain,
        (int)head.rwParms.channel[i].triggerType,  
        (int)head.rwParms.channel[i].iirTriggerFilter,  
        (int)head.rwParms.channel[i].StaSeconds,  
        (int)head.rwParms.channel[i].LtaSeconds,  
            head.rwParms.channel[i].StaLtaRatio,
        (int)head.rwParms.channel[i].StaLtaPercent,  
            head.rwParms.channel[i].fullscale,
            head.rwParms.channel[i].sensitivity,
            head.rwParms.channel[i].damping,
            head.rwParms.channel[i].naturalFrequency,
            head.rwParms.channel[i].triggerThreshold,
            head.rwParms.channel[i].detriggerThreshold,
            head.rwParms.channel[i].alarmTriggerThreshold,
            head.rwParms.channel[i].calCoil,
            head.rwParms.channel[i].range,
            head.rwParms.channel[i].sensorgain,
            head.rwParms.channel[i].networkcode,
            head.rwParms.channel[i].locationcode
        );
        
    }
    
    
    head.rwParms.stream.eventNumber = BYTESWAP_UINT16(head.rwParms.stream.eventNumber);
    head.rwParms.stream.sps         = BYTESWAP_UINT16(head.rwParms.stream.sps);
    head.rwParms.stream.apw         = BYTESWAP_UINT16(head.rwParms.stream.apw);
    head.rwParms.stream.preEvent    = BYTESWAP_UINT16(head.rwParms.stream.preEvent);
    head.rwParms.stream.postEvent   = BYTESWAP_UINT16(head.rwParms.stream.postEvent);
    head.rwParms.stream.minRunTime  = BYTESWAP_UINT16(head.rwParms.stream.minRunTime);
    head.rwParms.stream.Timeout     = BYTESWAP_UINT16(head.rwParms.stream.Timeout);
    head.rwParms.stream.TxBlkSize   = BYTESWAP_UINT16(head.rwParms.stream.TxBlkSize);
    head.rwParms.stream.BufferSize  = BYTESWAP_UINT16(head.rwParms.stream.BufferSize);
    head.rwParms.stream.SampleRate  = BYTESWAP_UINT16(head.rwParms.stream.SampleRate);
    head.rwParms.stream.TxChanMap   = BYTESWAP_UINT32(head.rwParms.stream.TxChanMap);
    head.rwParms.stream.VotesToTrigger   = BYTESWAP_UINT16(head.rwParms.stream.VotesToTrigger);
    head.rwParms.stream.VotesToDetrigger = BYTESWAP_UINT16(head.rwParms.stream.VotesToDetrigger);
    
    
    if(Debug && print_head)
    logit("e", "STREAM_K2_RW_PARMS: |%d| |%d| |%d| %d %d %d %d %d %d %d %d %d %d %d %d %d %d %lu\n", 
        (int)head.rwParms.stream.filterFlag,  
        (int)head.rwParms.stream.primaryStorage,  
        (int)head.rwParms.stream.secondaryStorage,  
            head.rwParms.stream.eventNumber,  
            head.rwParms.stream.sps,  
            head.rwParms.stream.apw,  
            head.rwParms.stream.preEvent,  
            head.rwParms.stream.postEvent,  
            head.rwParms.stream.minRunTime,  
            head.rwParms.stream.VotesToTrigger,
            head.rwParms.stream.VotesToDetrigger,
        (int)head.rwParms.stream.FilterType,  
        (int)head.rwParms.stream.DataFmt,  
            head.rwParms.stream.Timeout,
            head.rwParms.stream.TxBlkSize,
            head.rwParms.stream.BufferSize,
            head.rwParms.stream.SampleRate,
            head.rwParms.stream.TxChanMap
              );
            
    return stat;
}


/******************************************************************************
 * read_frame(fp)  Read the frame header, swap bytes if necessary.            *
 ******************************************************************************/
int read_frame( FILE *fp, unsigned long *channels )
{
    unsigned short   frameStatus, frameStatus2, samprate, streamnumber;
    unsigned char    BitMap[4];
    unsigned long    blockTime, bmap;
    int              stat;
    
    stat = fread(&frame, 32, 1, fp);
    frame.recorderID = BYTESWAP_UINT16(frame.recorderID);
    frame.frameSize  = BYTESWAP_UINT16(frame.frameSize);
    memcpy(&blockTime, &frame.blockTime, 4);
    blockTime  = BYTESWAP_UINT32(blockTime);

    frame.channelBitMap = BYTESWAP_UINT16(frame.channelBitMap);
    
    BitMap[0] = frame.channelBitMap & 255;
    BitMap[1] = frame.channelBitMap >> 8;
    BitMap[2] = frame.channelBitMap1;
    BitMap[3] = 0;
/*  memcpy( &bmap, BitMap, 4 ); */
    bmap = *((long *)(BitMap));
    frame.streamPar  = BYTESWAP_UINT16(frame.streamPar);
    frame.msec       = BYTESWAP_UINT16(frame.msec);
    frameStatus      = frame.frameStatus;
    frameStatus2     = frame.frameStatus2;
    samprate         = frame.streamPar & 4095;
    streamnumber     = frame.streamPar >> 12;
    /**/
    if(Debug && print_frame)
    logit("e", "filter: FRAME: %d %d %d %d   %lu X%ho   %hu X%ho %hu %hu     X%ho X%ho %hu X%ho \n", 
            (int)frame.frameType, 
            (int)frame.instrumentCode, 
            frame.recorderID, 
            frame.frameSize, 
             
            blockTime, 
            frame.channelBitMap, 
            
            frame.streamPar, streamnumber, samprate, samprate>>8,  
            frameStatus, 
            frameStatus2, 
            frame.msec, 
            (int)frame.channelBitMap1);
    
    *channels = bmap;
    return stat;
}



/******************************************************************************/
/* ** area **                                                                 */
/*                                                                            */
/* this function returns a value of 1 if the point (u,v)                      */
/* lies inside the polygon defined by arrays x and y, 0                       */
/* is returned otherwise                                                      */
/*                                                                            */
/* inputs:                                                                    */
/* 	n - number of sides to the polygon                                        */
/* 	    ( must be less or equal to 20)                                        */
/* 	x - array of dimension 21, contains n+1 x coordinates of polygon          */
/* 	y - array of dimension 21, contains n+1 y coordinates of polygon          */
/* 	  *** note *** the first point of the polygon is duplicated               */
/*         in location n+1 of the x and y arrays.                             */
/* 	u - x coordinate of point to check                                        */
/* 	v - y coordinate of point to check                                        */
/*                                                                            */
/* outputs:                                                                   */
/* 	iout = 1 if the point is inside or on the polygon                         */
/* 		0 otherwise.                                                          */
/*                                                                            */
/* calls: cntsct                                                              */
/*                        algorithm by ron sheen - 1972                       */
/******************************************************************************/
int area (int n, float *x, float *y, float u, float v)
{
    int i, iout;

    i = cntsct(n, x, y, u, v);

    iout = 2 * ((i + 1) / 2) - i;

    return iout;
}


/******************************************************************************/
/* ** cntsct **                                                               */
/*                                                                            */
/* this function returns the count of the number of times a ray               */
/* projected from point (u,v) intersects the polygon defined in the           */
/* arrays x and y.                                                            */
/*                                                                            */
/* inputs:                                                                    */
/* 	n = the number of sides to polygon                                        */
/* 	x = n+1 x coordinates of polygon                                          */
/* 	y = n+1 y coordinates of polygon                                          */
/* 	   *** note *** element n+1 of the x and y arrays are                     */
/* 	   duplicates of element 1 of both arrays.                                */
/* 	u = x coordinate                                                          */
/* 	v = y coordinate                                                          */
/*                                                                            */
/* outputs:                                                                   */
/* 	cntsct = count of intersections                                           */
/*                                                                            */
/* calls:  	isect                                                             */
/*                        algorithm by ron sheen - 1972                       */
/******************************************************************************/
int cntsct (int n, float *x, float *y, float u, float v)
{
    int isec, i, itimes;

    itimes = 0;
    for(i=0; i<n; i++) {
		isec = isect(x[i], y[i], x[i + 1], y[i + 1], u, v);

		if (isec == 2) return (1);
		
		itimes += isec;
    }
    return (itimes);
}


/******************************************************************************/
/* ** isect **                                                                */
/* this function determines whether a ray projected from (u,v)                */
/* intersects or lies on the line through (x1,y1), (x2,y2).                   */
/*                                                                            */
/* inputs:                                                                    */
/* 	x1 = x-coordinate of first point                                          */
/* 	y1 = y-coordinate of first point                                          */
/* 	x2 = x-coordinate of second point                                         */
/* 	y2 = y-coordinate of second point                                         */
/* 	u  = x-coordinate of test point                                           */
/* 	v  = y-coordinate of test point                                           */
/*                                                                            */
/* outputs:                                                                   */
/* 	isect = 0; ray does not intersect line                                    */
/* 	isect = 1; ray does intersect line                                        */
/* 	isect = 2; ray lies on line                                               */
/*                                                                            */
/* calls:  	fabs                                                              */
/*                        algorithm by ron sheen - 1972                       */
/******************************************************************************/
int isect (float x1, float y1, float x2, float y2, float u, float v)
{
    float  yr, xt, eps;
    int    ret_val;

	eps = (float)0.00001;
	yr = v;
	if (((fabs (yr - y1) < eps)  && (fabs (y1 - y2) < eps)) &&
		(((u < (x1 + eps)) && (u > (x2 + eps))) ||
		 ((u < (x2 + eps)) && (u > (x1 - eps)))))
	{
    	ret_val = 2;
	}

	else if (((fabs (u - x1) < eps) && (fabs (x1 - x2) < eps)) && 
			(((yr < (y1 + eps)) && (yr > (y2 - eps))) || 
			((yr < (y2 + eps)) && (yr > (y1 - eps)))))
	{
    	ret_val = 2;
	}

	else if ((fabs (yr - y1) < eps) || (fabs (yr - y2) < eps)) {
		yr = (float) (yr + (eps * 10.0));
    }


    if (((yr < (y2 + eps)) && (yr > (y1 - eps))) || 
			((yr < (y1 + eps)) && (yr > (y2 - eps)))) {
		if ((u < (x1 + eps)) || (yr < (x2 + eps))) {
    		if ((u > (x1 - eps)) || (u > (x2 - eps))) {
    			yr = v;
			    xt = x1 + (((x2 - x1) * (yr - y1)) / (y2 - y1));
			    if (u <= (xt + (eps * 0.001))) {
			    	 ret_val = (fabs (u - xt) < eps)? 2:1;
				}
				else ret_val = 0;
			}
			else ret_val = 1;
		}
		else ret_val = 0;
    }
	else ret_val = 0;
	
    return ret_val;
}


