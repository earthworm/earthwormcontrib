/*
  menulist.c

  Get the menu from wave_serverV and put info
  into a file for the use of heli-2-go.
  Uses the same config file as heli2go so that available
  channels match.
  Patterned after getmenu.
  
                           7/28/2000  Jim Luetgert
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <platform.h> /* includes system-dependent socket header files */
#include <chron3.h>
#include <kom.h>
#include <earthworm.h>
#include <ws_clientII.h>

#ifndef INADDR_NONE
#define INADDR_NONE  0xffffffff  /* should be in <netinet/in.h> */
#endif

#define MAX_WAVESERVERS   90
#define MAX_ADRLEN        20
#define MAXTXT           150

/* Function prototypes
   *******************/
void LogWsErr( char [], int );
void config_me( char* configfile );

/* Globals to set from configuration file
   **************************************/
char     MenuList[100];
int      numserv;                              /* Number of wave servers */
char     wsLoc[MAX_WAVESERVERS];
char     wsIp[MAX_WAVESERVERS][MAX_ADRLEN];
char     wsPort[MAX_WAVESERVERS][MAX_ADRLEN];  /* Available wave servers */

/* Constants
   *********/
const long   wsTimeout = 5000;                /* milliSeconds to wait for reply */


/* Time conversion factor for moving between
   Carl Johnson's seconds-since-1600 routines (chron3.c)
   and Solaris' and OS/2's seconds-since-1970 routines
   *****************************************************/
static char   Str1970[18] = "19700101000000.00";
static double Sec1970;


int main( int argc, char *argv[] )
{
  int          rc;                  /* Function return code */
  int          i, j;
  char         line[80], stime[80], etime[80], loc[5];
  FILE        *out;
  WS_MENU_QUEUE_REC queue;

  queue.head = (WS_MENU) NULL;
  queue.tail = (WS_MENU) NULL;

  /* Calculate timebase conversion constant
  **************************************/
    Sec1970 = julsec17( Str1970 );

  /* Check command line arguments
  ****************************/
    if ( argc != 2 ) {
        printf( "Usage: menulist config.d\n" );
        return -1;
    }

  /* Since our routines call logit, we must initialize it, although we don't
   * want to!
   */
    logit_init( "menulist", (short) 0, 256, 0 );

  /* Zero the wave server arrays
  ***************************/
    for ( i = 0; i < MAX_WAVESERVERS; i++ ) {
        memset( wsIp[i], 0, MAX_ADRLEN );
        memset( wsPort[i], 0, MAX_ADRLEN );
    }

    /* Read the configuration file(s)
     ********************************/
    config_me( argv[1] );

  /* Initialize the socket system
  ****************************/
    SocketSysInit();

  /* Build the current wave server menus
  ***********************************/
    for ( i = 0; i < numserv; i++ ) {
        rc = wsAppendMenu( wsIp[i], wsPort[i], &queue, wsTimeout );
        if ( rc != WS_ERR_NONE ) {
            LogWsErr( "wsAppendMenu", rc );
            logit("e", "menulist: Append Error %d for %s:%s \n", rc, wsIp[i], wsPort[i]);
        }
    }

    if (queue.head == NULL ) {
        logit("e", "menulist: nothing in servers\n");
        exit( 0 );
    }

  /* Print contents of all tanks
  ***************************/
    out = fopen(MenuList, "wb");
    if(out == 0L) {
        logit("e", "Unable to open MenuList File: %s\n", MenuList);    
    } else {
        logit("e", "Opened MenuList File: %s\n", MenuList);
	    logit("e", "menulist: numserv: %d\n", numserv);
	    for ( j = 0; j < numserv; j++ ) {
	        WS_PSCNL scnp;

	        rc = wsGetServerPSCNL( wsIp[j], wsPort[j], &scnp, &queue );
	        if ( rc == WS_ERR_EMPTY_MENU ) {
            logit("e", "menulist: Empty Menu Error %d for %s:%s \n", rc, wsIp[i], wsPort[i]);
	        continue;
	        }
	        if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
            logit("e", "menulist: Not in Menu Error %d for %s:%s \n", rc, wsIp[i], wsPort[i]);
	        continue;
	        }

			wsLoc[j] = (strlen(scnp->loc))? 1:0;
	        while ( 1 ) {
	            date17( scnp->tankStarttime+Sec1970, line );
	            for ( i = 0; i < 8; i++ ) stime[i] = line[i];
	            stime[8] =  '_';
	            for ( i = 8; i < 12; i++ ) stime[i+1] = line[i];
	            stime[13] =  '_';
	            for ( i = 12; i < 17; i++ ) stime[i+2] = line[i];
	            stime[19] =  0;

	            date17( scnp->tankEndtime+Sec1970, line );
	            for ( i = 0; i < 8; i++ ) etime[i] = line[i];
	            etime[8] =  '_';
	            for ( i = 8; i < 12; i++ ) etime[i+1] = line[i];
	            etime[13] =  '_';
	            for ( i = 12; i < 17; i++ ) etime[i+2] = line[i];
	            etime[19] =  0;

	            if(strlen(scnp->loc)) strcpy(loc, scnp->loc);
	            else                  strcpy(loc, "--");
	            if(strcmp(scnp->chan,  "T")!=0 && 
	               strcmp(scnp->chan,  "---" )!=0) {
	                fprintf(out, "%-5s  %-2s  %-3s  %-2s  %s  %s \n", 
	                       scnp->sta, scnp->net, scnp->chan, loc, stime, etime );
	                logit("e", "%-5s  %-2s  %-3s  %-2s  %s  %s \n", scnp->sta, scnp->net, scnp->chan, loc, stime, etime );
	         /*            */  
	            }

	            if ( scnp->next == NULL ) break;
	            else scnp = scnp->next;
	        }
	    }
	    fclose(out);
    }

  /* Release the linked list created by wsAppendMenu
  ***********************************************/
    wsKillMenu( &queue );

    return 0;
}


void LogWsErr( char fun[], int rc )
{
  switch ( rc ) {
    case WS_ERR_INPUT:
      printf( "%s: Bad input parameters.\n", fun );
      break;

    case WS_ERR_EMPTY_MENU:
      printf( "%s: Empty menu.\n", fun );
      break;

    case WS_ERR_SERVER_NOT_IN_MENU:
      printf( "%s: Empty menu.\n", fun );
      break;

    case WS_ERR_SCNL_NOT_IN_MENU:
      printf( "%s: SCN not in menu.\n", fun );
      break;

    case WS_ERR_BUFFER_OVERFLOW:
      printf( "%s: Buffer overflow.\n", fun );
      break;

    case WS_ERR_MEMORY:
      printf( "%s: Out of memory.\n", fun );
      break;

    case WS_ERR_PARSE:
      printf( "%s: Couldn't parse server's reply.\n", fun );
      break;

    case WS_ERR_TIMEOUT:
      printf( "%s: Timed out.\n", fun );
      break;

    case WS_ERR_BROKEN_CONNECTION:
      printf( "%s: The connection broke.\n", fun );
      break;

    case WS_ERR_SOCKET:
      printf( "%s: Could not get a connection.\n", fun );
      break;

    case WS_ERR_NO_CONNECTION:
      printf( "%s: Could not get a connection.\n", fun );
      break;

    default:
      printf( "%s: unknown ws_client error: %d.\n", fun, rc );
      break;
    }

  return;
}

/****************************************************************************
 *      config_me() process command file using kom.c functions              *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void config_me( char* configfile )
{
    char    whoami[50], *com, *str;
    char    init[20];       /* init flags, one byte for each required command */
    int     ncommand;       /* # of required commands you expect              */
    int     nmiss;          /* number of required commands that were missed   */
    int     i, j, n, nfiles, success, junk;
    double  val;
    FILE    *in;

    sprintf(whoami, " %s: %s: ", "menulist", "config_me");
        /* Set one init flag to zero for each required command
         *****************************************************/
    ncommand = 2;
    for(i=0; i<ncommand; i++ )  init[i] = 0;
    numserv = 0;
    
        /* Open the main configuration file
         **********************************/
    nfiles = k_open( configfile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, configfile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

                /* Process anything else as a command
                 ************************************/
            
         /* wave server addresses and port numbers to get trace snippets from
          *******************************************************************/
/*0*/          
            else if( k_its("WaveServer") ) {
                if ( numserv >= MAX_WAVESERVERS ) {
                    fprintf( stderr, "%s Too many <WaveServer> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_WAVESERVERS );
                    return;
                }
                if( (long)(str=k_str()) != 0 )  strcpy(wsIp[numserv],str);
                if( (long)(str=k_str()) != 0 )  strcpy(wsPort[numserv],str);
                numserv++;
                init[0]=1;
            }

                /* get menu list path/name
                *****************************/
/*1*/
            else if( k_its("MenuList") ) {
                str = k_str();
                if( (int)strlen(str) >= 100) {
                    fprintf( stderr, "%s Fatal error. Menu list name %s greater than %d char.\n", 
                            whoami, str, 100);
                    exit(-1);
                }
                if(str) strcpy( MenuList , str );
                init[1] = 1;
            }

               /* unused commands */

            else if( k_its("GifDir") )         junk = 1;
            else if( k_its("Display") )        junk = 1;
            else if( k_its("StationList") )    junk = 1;
            else if( k_its("StationList1") )   junk = 1;
            else if( k_its("Target") )         junk = 1;
            else if( k_its("LabelDC") )        junk = 1;
            else if( k_its("NoLabelDC") )      junk = 0;
            else if( k_its("Debug") )          junk = 1;
            else if( k_its("WSDebug") )        junk = 1;
            else if( k_its("UseDST") )         junk = 1;
            else if( k_its("SaveDrifts") )     junk = 1;
            else if( k_its("PlotDown") )       junk = 0;
            else if( k_its("PlotUp") )         junk = 1;
            else if( k_its("Clip") )           junk = k_int();
            else if( k_its("RetryCount") )     junk = 1;
            else if( k_its("Gulp") )           junk = 1;
            else if( k_its("Logo") )           junk = 1;

                /* At this point we give up. Unknown thing.
                *******************************************/
            else {
                fprintf(stderr, "%s <%s> Unknown command in <%s>.\n",
                         whoami, com, configfile );
                continue;
            }

                /* See if there were any errors processing the command
                 *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "%s Bad <%s> command  in <%s>; exiting!\n",
                             whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

        /* After all files are closed, check init flags for missed commands
         ******************************************************************/
    nmiss = 0;
    for(i=0;i<ncommand;i++)  if( !init[i] ) nmiss++;
    if ( nmiss ) {
        fprintf( stderr, "%s ERROR, no ", whoami);
        if ( !init[0] )  fprintf( stderr, "<WaveServer> "   );
        if ( !init[1] )  fprintf( stderr, "<MenuList> "     );
        fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
        exit( -1 );
    }
}


