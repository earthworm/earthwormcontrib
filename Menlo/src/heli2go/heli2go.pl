#!/usr/local/bin/perl

########################################################################
# This PerlScript manages the Heli-2-Go system for creating custom
# helicorder plots for the web.  
# Virtually everything needed to customize the installation is
# contained in the next several lines.
#
#                                  7/28/2000   Jim Luetgert
########################################################################

$logo    = "/waveforms/smusgs.gif";
$altlogo = "USGS";
$netname = "Northern California Seismic Network";
$rootdir = "/home/ppicker";
$self    = "/waveforms/cgi-bin/heli2go.pl";
$datadir = "/waveforms/heli2go";
$gifdir  = "$rootdir"."$datadir";
$gwid  = "150";                    # Width and height of thumbnails
$ghgt  = "300";
$gbwid = "850";                    # Width and height of drum recorder plots
$gbhgt = "1613";
$numpergroup = 4;
$pagecolor   = "#ffffcc";          # Colors of various page elements
$bannercolor = "#669966";
$bordercolor = "#ffebcd";
$bannertxtcolor = "#ffffff";
$timezone = " PDT";                # Time zone info
$localstart = 17;
$gmtstart = 0;

$program  = "/home/earthworm/v5.0/bin/heli2go";
$makelist = "/home/earthworm/v5.0/bin/menulist";
$config   = "/home/earthworm/run/params/heli2go.d";
$database = "/home/earthworm/run/params/calsta.db";
$menulist = "/home/earthworm/run/params/calmenu.db";

# Common links for page headers
$mouseout = "onMouseOut=\"window.status=''; return true;\"";
$tag5 = "<br>";
$tag6 = " <A HREF=\"$self\" onMouseOver=\"window.status='Select From Available Data'; return true;\"    $mouseout> Data Index </A> || ";
$tag7 = " ";
$tag8 = " ";
$tag9 = " ";

# This is an optional custom comment list placed by the user.
# Of form : JSF_VDZ_NC. At the Stanford dish.
# There are two fields, separated by "." First field is the SCN; second is descriptive text.
if(open(NAMES, "$gifdir/zznamelist.dat")) {
     while(<NAMES>) {
        chomp;
        ($sitename = $_) =~ s/([^.]*)\.([^.]*).*/$1/;
        ($siteid = $_) =~ s/([^.]*)\.([^.]*).*/$2/;
        $longname{$sitename} = $siteid;
     }
    close(NAMES);
}

# Get some of the basic file information needed by many of the subroutines.
# Get the lists of available sites and datetimes
# Of Form: nc.JSF_VDZ_NC_00.2000051500.gif

    $list = `ls $gifdir *.gif`;
    @list = split (' ',$list);
    @reverselist = reverse(@list);
    @site = ();
    $numsites = 0;
    @dates = ();
    $numdates = 0;
    foreach $item (@list) {
        ($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
        ($filedate = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3/;
        ($suffix = $item) =~ s/([^.]*)\.([^.]*)\.([^.]*)\.([^.]*).*/$4/;
        if($suffix eq "gif") {
            $flag = 1;
            foreach $name (@site) {
                if($sitename eq $name) { $flag = 0; }
            }
            if($flag) { @site = (@site,$sitename); $numsites = $numsites + 1; }
            $flag = 1;
            foreach $idate (@dates) {
                if($filedate eq $idate) { $flag = 0; }
            }
            if($flag) { @dates = (@dates,$filedate);  $numdates = $numdates + 1; }
        }
    }
    @dates = sort(@dates);
    @reversedates = reverse(@dates);

# Find the client's browser version so we know whether or not
# to send Javascript.
    $JavaOK = 0;       # Assume we can't use javascript for time
    $JavaOK = 1;       # Assume we can use javascript for time
    $ua = $ENV{'HTTP_USER_AGENT'};
    ($bv,$rest) = split ('\(',$ua);
    if($bv =~ /\//) {
        ($browser,$versionpt) = split ('/',$bv);
    }
    else {
        $versionpt = 3;
    }
    if($rest =~ /MSIE/) {$JavaOK = 0; }

    $version = $versionpt;
    if($versionpt =~ /\s/) {
        ($version,$rest) = split ('\s',$versionpt);
    }
    if($version < 4) {$JavaOK = 0; }

# Check the query string for further instructions.
#
    $qname = "NONE";
    $query_string = $ENV{'QUERY_STRING'};

if ( ! defined($query_string) || $query_string eq "") {
   &make_index;
}
else {
    $group = "$query_string";
        ($querytype,$qvalue,$qdate) = split ('&',$query_string);
        
   if($querytype lt 4) { 
        $i = 0;
        $j = 0;
        foreach $name (@site) {
            $i = $i + 1;
            if($name =~ $qvalue) {
                $qname = $name;
                $qvalue = $i;
                $j = 1;
                last;
            }
        }
        if($j == 0 && $qvalue != 0) {
            $querytype = 5;
        }
    }
    
   if($querytype eq 1) { &show_site_thumbs; }
   if($querytype eq 2) { &show_date_thumbs; }
   if($querytype eq 3) { 
       if($JavaOK == 1) { 
                  &show_single_panelx; }
       else {     &show_single_panel;  }
   }
   if($querytype eq 4) { &make_index; }
   if($querytype eq 5) { &select_sta; }
   if($querytype eq 6) { &make_var; }
   if($querytype eq 7) { &make_heli; }
}

exit;


########################################################################
sub make_index {
########################################################################

    $banner1 = "Welcome to the Heli-2-Go Display";
    $banner2 = "Build Real-time Views of Selected Seismograms";

    $tag1 = " ";
    $tag2 = " ";
    $tag3 = " ";
    $tag4 = " <A HREF=\"$self\?5&NULL&0\" onMouseOver=\"window.status='Build A New Helicorder'; return true;\"    $mouseout> Build A New Helicorder  </A>  ";
    $tag6 = "<br>";
    $tag7 = " ";
    $tag8 = " ";
    $tag9 = " ";

    &Build_Banner;

print STDOUT <<EOT;
    
    <P><h4>
    The Helicorders displayed here are built on demand.  
    They are each deleted after 7 days.
    Each panel represents 24 hours of data and its file is between 100kb and 200kb.<br>
    </h4>

EOT
    
    $i = 0;
    foreach $name (@site) {
        $i = $i + 1;
        $lname = $longname{$name};
        ($sta,$comp,$net) = split ('_',$name);
        $inq = "1&$name&0";
        $wstatus = "View Thumbnails of $sta $comp $net for All Available Days";
        print STDOUT "<P><HR><P><h4>     ";
        print STDOUT "  $sta $comp $net \n";
        if($lname) {print STDOUT " ( $lname )  ";}
        print STDOUT " [<A HREF=\"$self\?$inq\" \n";
        print STDOUT "  onMouseOver=\"window.status='$wstatus'; return true;\" \n";
        print STDOUT "  onMouseOut=\"window.status=''; return true;\"> \n";
        print STDOUT " <b>Previews</b></A>]<br>\n";
        print STDOUT "</h4><P>\n";
        foreach $item (@reverselist) {
            ($suffix = $item) =~ s/([^.]*)\.([^.]*)\.([^.]*)\.([^.]*).*/$4/;
            if($suffix eq "gif") {
                ($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
                if($sitename eq $name) {
                    ($datetime = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3_$4/;
                    ($date,$hr) = split ('_',$datetime);
                    ($datepart = $date) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
                    ($year,$mon,$day) = split ('_',$datepart);
                    $inq = "3&$name&$date";
                    $wstatus = "View Data for $sta $comp $net on $mon/$day/$year";
                    print STDOUT "  | <A HREF=\"$self\?$inq\"  \n";
                    print STDOUT "  onMouseOver=\"window.status='$wstatus'; return true;\" \n";
                    print STDOUT "  onMouseOut=\"window.status=''; return true;\"> \n";
                    print STDOUT " <B>$mon/$day/$year</B> </A> | \n";
                }
            }
        }
    }
    
    print STDOUT "<P><h4><HR>Previews of all stations are available for the following dates:</h4><BR>\n";
    foreach $item (@reversedates) {
        ($datepart = $item) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
        ($year,$mon,$day) = split ('_',$datepart);
        $inq = "2&0&$item";
        $wstatus = "View Thumbnails of All Stations for $mon/$day/$year";
        print STDOUT " | <A HREF=\"$self\?$inq\"  \n";
        print STDOUT "  onMouseOver=\"window.status='$wstatus'; return true;\" \n";
        print STDOUT "  onMouseOut=\"window.status=''; return true;\"> \n";
        print STDOUT "  <B>$mon/$day/$year</B></A> | \n";
    }

return;

}

########################################################################
# Display thumbnails of all spectrograms for one site
# querytype = 1
# $qvalue is index to array of site names
########################################################################
sub show_site_thumbs {

    $name = $site[$qvalue-1];
    $lname = $longname{$qname};
    ($sta,$comp,$net) = split ('_',$qname);
    if($lname) {
        $banner1 = "$sta $comp $net";
        $banner2 = " ( $lname )  ";
    } else {
        $banner1 = "$netname";
        $banner2 = "$sta $comp $net";
    }
    
    $count = 0;
    foreach $item (@reverselist) {
        ($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
        if($sitename eq $qname) {
            ($datetime = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3_$4/;
            ($date,$hr) = split ('_',$datetime);
        
            ($datepart = $date) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
            $displayitem[$count] = $item; 
            $displaydate[$count] = $datepart; 
            $filedate[$count] = $date; 
            $count = $count + 1;
        }
    }
    
# Tags to go elsewhere
    
    $psite = $qvalue - 1;
    if($psite < 1) { $psite = $numsites; }
    $pname = $site[$psite-1];
    ($sta,$comp,$net) = split ('_',$pname);
    $inq = "1&$pname&0";
    $tag1 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='All Data for $sta $comp $net'; return true;\" $mouseout>Previous Site ($sta $comp $net)</A> || ";
    
    $nsite = $qvalue + 1;
    if($nsite > $numsites) { $nsite = 1; }
    $nname = $site[$nsite-1];
    ($sta,$comp,$net) = split ('_',$nname);
    $inq = "1&$nname&0";
    $tag2 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='All Data for $sta $comp $net'; return true;\" $mouseout> Next Site ($sta $comp $net)</A> || ";
    
    $tag3 = " ";
    $tag4 = " ";

    &Build_Banner;

print STDOUT <<EOT;

    <P><h4>
    The Helicorders displayed here are built on demand.  
    They are each deleted after 7 days.
    Each panel represents 24 hours of data and its file is between 100kb and 200kb.<br>
    <br></h4>
     
    <center>
    <P><h4><FONT COLOR=red>
    Click on a seismogram for full size display.
    </FONT></h4>
    </center>
EOT

    print STDOUT "<HR>";
    print STDOUT "<center>";
    print STDOUT " <table width=\"75\%\" bordercolorlight=$pagecolor bordercolordark=$pagecolor>\n";
    $j = $count/4;
    for($jj=0; $jj<$j; $jj++) {
        $kk = $count - $jj*4;
        if($kk > 4) { $kk = 4; }
        print STDOUT "<tr>\n";
        for($i=0; $i<$kk;$i++) {
            $ii = $jj*4 + $i;
            ($year,$mon,$day) = split ('_',$displaydate[$ii]);
            print STDOUT "  <td><center>\n <B>$mon/$day/$year</B> \n </center></td> \n";
        }
        print STDOUT "</tr>\n";
        print STDOUT "<tr>\n";
        for($i=0; $i<$kk;$i++) {
            $ii = $jj*4 + $i;
            $inq = "3&$qname&$filedate[$ii]";
            ($year,$mon,$day) = split ('_',$displaydate[$ii]);
            $wstatus = "$qname on $mon/$day/$year";
            print STDOUT "<td><center>\n <A HREF=\"$self\?$inq\" \n ";
            print STDOUT "  onMouseOver=\"window.status='$wstatus'; return true;\" \n";
            print STDOUT "  onMouseOut=\"window.status=''; return true;\"> \n";
            print STDOUT "  <IMG SRC=\"$datadir/$displayitem[$ii]\" WIDTH=$gwid HEIGHT=$ghgt></A></center></td>\n ";
        }
        print STDOUT "</tr>\n";
        print STDOUT "<tr>\n";
        print STDOUT " <td>&nbsp;</td>\n <td>&nbsp;</td>\n <td>&nbsp;</td>\n <td>&nbsp;</td>\n";
        print STDOUT "</tr>\n";
    }
    print STDOUT "</table>\n";
    print STDOUT "</center> \n ";
    
    print STDOUT "<center><h4> \n $tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8 $tag9 </h4></center>\n";

    print STDOUT " <P> <HR> </body> </html> \n";

return;
}


########################################################################
# Display thumbnails of spectrograms for all sites for one day
# querytype = 2
# $qdate of form YYYYMMDD - same as in file name
########################################################################
sub show_date_thumbs {

    @t0 = date2secs();

    $delhr = -24;
    ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
    $monplus = $mon + 1;
    $year = $year + 1900;
    $prev = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;

    $delhr = 24;
    ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
    $monplus = $mon + 1;
    $year = $year + 1900;
    $next = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;
    
    $date = $qdate;
    $day = substr ($date,6,2);
    $mon = substr ($date,4,2);
    $year = substr ($date,0,4);
    $banner1 = "$netname";
    $banner2 = "Drum Recorder Displays for $mon/$day/$year";
    
# Tags to go elsewhere
    
    $flagp = 1;
    $flagn = 1;
    foreach $item (@dates) {
        if($prev eq $item) { $flagp = 0; }
        if($next eq $item) { $flagn = 0; }
    }
    if($flagp) { $tag1 = " Previous Day || "; }
    else {    
        $day = substr ($prev,6,2);
        $mon = substr ($prev,4,2);
        $year = substr ($prev,0,4);
        $inq = "2&0&$prev";
        $tag1 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='All Data for $mon/$day/$year'; return true;\" $mouseout>Previous Day</A> || ";
    }
    
    if($flagn) { $tag2 = " Next Day || "; }
    else {    
        $day = substr ($next,6,2);
        $mon = substr ($next,4,2);
        $year = substr ($next,0,4);
        $inq = "2&0&$next";
        $tag2 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='All Data for $mon/$day/$year'; return true;\" $mouseout>Next Day</A> || ";
    }
    $tag3 = " ";
    $tag4 = " ";

    &Build_Banner;
    
print STDOUT <<EOT;

    <P><h4>
    The Helicorders displayed here are built on demand.  
    They are each deleted after 7 days.
    Each panel represents 24 hours of data and its file is between 100kb and 200kb.<br>
    <br></h4>
     
    <center>
    <P><h4><FONT COLOR=red>
    Click on a seismogram for full size display.
    </FONT></h4>
    </center>
EOT
    
    $count = 0;
    foreach $item (@list) {
        ($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
        ($filedate = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3/;
        ($suffix = $item)   =~ s/([^.]*)\.([^.]*)\.([^.]*)\.([^.]*).*/$4/;
        if($suffix eq "gif") {
            if($filedate eq $qdate) {
                $displayitem[$count] = $item; 
                $displayname[$count] = $sitename; 
                $count = $count + 1;
            }
        }
    }

    $date = $qdate;
    $day = substr ($date,6,2);
    $mon = substr ($date,4,2);
    $year = substr ($date,0,4);
    print STDOUT "<HR>";
    print STDOUT "<center>";
    print STDOUT " <table width=\"75\%\" bordercolorlight=$pagecolor bordercolordark=$pagecolor>\n";
    $j = $count/4;
    for($jj=0; $jj<$j; $jj++) {
        $kk = $count - $jj*4;
        if($kk > 4) {
            $kk = 4;
        }
        print STDOUT "<tr>\n";
        for($i=0; $i<$kk;$i++) {
            $ii = $jj*4 + $i;
            ($sta,$comp,$net) = split ('_',$displayname[$ii]);
            print STDOUT "  <td><center>\n <B>$sta $comp $net</B> \n </center></td> \n";
        }
        print STDOUT "</tr>\n";
        print STDOUT "<tr>\n";
        for($i=0; $i<$kk;$i++) {
            $ii = $jj*4 + $i;
            $iii = $ii + 1;
            ($sta,$comp,$net) = split ('_',$displayname[$ii]);
            $inq = "3&$displayname[$ii]&$qdate";
            $wstatus = "$sta $comp $net on $mon/$day/$year";
            print STDOUT "<td><center>\n <A HREF=\"$self\?$inq\" \n ";
            print STDOUT "  onMouseOver=\"window.status='$wstatus'; return true;\" \n";
            print STDOUT "  onMouseOut=\"window.status=''; return true;\"> \n";
            print STDOUT "  <IMG SRC=\"$datadir/$displayitem[$ii]\" WIDTH=$gwid HEIGHT=$ghgt></A></center></td>\n ";
        }
        print STDOUT "</tr>\n";
        print STDOUT "<tr>\n";
        print STDOUT " <td>&nbsp;</td>\n <td>&nbsp;</td>\n <td>&nbsp;</td>\n <td>&nbsp;</td>\n";
        print STDOUT "</tr>\n";
    }
    print STDOUT "</table>\n";
    print STDOUT "</td></center> \n ";
    
    print STDOUT "<center><h4> \n $tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8 $tag9 </h4></center>\n";

    print STDOUT " <P> <HR> <P> </body> </html> \n";

return;
}

########################################################################
# Display a Drum Recorder for one day
# querytype = 3
# $qvalue  is index to array of site names
# $qdate of form YYYYMMDD - same as in file name
########################################################################
sub show_single_panel {

# Get the previous and next days.

    @t0 = date2secs();

    $delhr = -24;
    ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
    $monplus = $mon + 1;
    $year = $year + 1900;
    $prev = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;

    $delhr = 24;
    ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
    $monplus = $mon + 1;
    $year = $year + 1900;
    $next = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;

# Get the previous and next sites.

    $i = 0;
    $count = 0;
    foreach $name (@site) {
        $i = $i + 1;
        if($i eq $qvalue) { 
            foreach $item (@reverselist) {
                ($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
                if($sitename eq $name) {
                    ($filedate = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3/;
                    ($datetime = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3_$4/;
                    ($date,$hr) = split ('_',$datetime);
                
                    ($datepart = $date) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
                    $displayitem[$count] = $item; 
                    $displaydate[$count] = $datepart; 
                    $count = $count + 1;
                    if($filedate eq $qdate) {
                        $currentitem = $item;
                        $currentname = $name;
                    }
                }
            }
            last; 
        }
    }
    $lname = $longname{$qname};
    ($sta,$comp,$net) = split ('_',$qname);
    if($lname) {
        $banner1 = "$sta $comp $net";
        $banner2 = " ( $lname )  ";
    } else {
        $banner1 = "$netname";
        $banner2 = "$sta $comp $net";
    }

# Tags to go elsewhere
    
    $flagp = 1;
    $flagn = 1;
    foreach $item (@dates) {
        if($prev eq $item) { $flagp = 0; }
        if($next eq $item) { $flagn = 0; }
    }
    $tsite = $qvalue;
    if($flagp) { $tag1 = " Previous Day || \n"; }
    else {    
        $day = substr ($prev,6,2);
        $mon = substr ($prev,4,2);
        $year = substr ($prev,0,4);
        $inq = "3&$qname&$prev";
        $tag1 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='Data for $qname on $mon/$day/$year'; return true;\" $mouseout>Previous Day</A> || ";
    }
    
    if($flagn) { $tag2 = " Next Day || \n"; }
    else {    
        $day = substr ($next,6,2);
        $mon = substr ($next,4,2);
        $year = substr ($next,0,4);
        $inq = "3&$qname&$next";
        $tag2 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='Data for $qname on $mon/$day/$year'; return true;\" $mouseout>Next Day</A> || ";
    }

    $day = substr ($qdate,6,2);
    $mon = substr ($qdate,4,2);
    $year = substr ($qdate,0,4);
    $psite = $qvalue - 1;
    if($psite < 1) { $psite = $numsites; }
    $pname = $site[$psite-1];
    ($sta,$comp,$net) = split ('_',$pname);
    $inq = "3&$pname&$qdate";
    $tag3 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='Data for $sta $comp $net on $mon/$day/$year'; return true;\" $mouseout>Previous Site ($sta $comp $net)</A> || ";
    
    $nsite = $qvalue + 1;
    if($nsite > $numsites) { $nsite = 1; }
    $nname = $site[$nsite-1];
    ($sta,$comp,$net) = split ('_',$nname);
    $inq = "3&$nname&$qdate";
    $tag4 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='Data for $sta $comp $net on $mon/$day/$year'; return true;\" $mouseout>Next Site ($sta $comp $net)</A>  ";
    
    &Build_Banner;
    
    print STDOUT "<center><td>";
    print STDOUT "<A name=\"anchor1\"   \n>";
    print STDOUT "<IMG SRC=\"$datadir/$currentitem\" WIDTH=$gbwid HEIGHT=$gbhgt>  \n";
    print STDOUT "</a>";

    print STDOUT "</td></center> \n ";
    
    print STDOUT "<center><h4> \n $tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8 $tag9 </h4></center>\n";

    print STDOUT " <P> <HR> <P> </body> </html> \n";


return;

}

########################################################################
# Display a Drum Recorder for one day
# Javascript version for displaying time in status bar.
# This only works with Navigater
# querytype = 3
# $qvalue  is index to array of site names
# $qdate of form YYYYMMDD - same as in file name
########################################################################
sub show_single_panelx {

# Get the previous and next days.

    @t0 = date2secs();

    $delhr = -24;
    ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
    $monplus = $mon + 1;
    $year = $year + 1900;
    $prev = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;

    $delhr = 24;
    ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
    $monplus = $mon + 1;
    $year = $year + 1900;
    $next = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;
    $this = $qdate;

# Get the previous and next sites.

    $i = 0;
    $count = 0;
    foreach $name (@site) {
        $i = $i + 1;
        if($i eq $qvalue) { 
            foreach $item (@reverselist) {
                ($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
                if($sitename eq $name) {
                    ($filedate = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3/;
                    ($datetime = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3_$4/;
                    ($date,$hr) = split ('_',$datetime);
                
                    ($datepart = $date) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
                    $displayitem[$count] = $item; 
                    $displaydate[$count] = $datepart; 
                    $count = $count + 1;
                    if($filedate eq $qdate) {
                        $currentitem = $item;
                        $currentname = $name;
                    }
                }
            }
            last; 
        }
    }
    $lname = $longname{$qname};
    ($sta,$comp,$net) = split ('_',$qname);
    $channel = "$sta $comp $net";
    $day  = substr ($this,6,2);
    $mon  = substr ($this,4,2);
    $year = substr ($this,0,4);
    $thisdate = sprintf "%2.2d/%2.2d/%4.4d", $mon, $day, $year;

    if($lname) {
        $banner1 = $channel;
        $banner2 = " ( $lname )  ";
    } else {
        $banner1 = $netname;
        $banner2 = $channel;
    }

# Tags to go elsewhere
    
    $flagp = 1;
    $flagn = 1;
    foreach $item (@dates) {
        if($prev eq $item) { $flagp = 0; }
        if($next eq $item) { $flagn = 0; }
    }
    $tsite = $qvalue;
    if($flagp) { $tag1 = " Previous Day || \n"; }
    else {    
        $day = substr ($prev,6,2);
        $mon = substr ($prev,4,2);
        $year = substr ($prev,0,4);
        $inq = "3&$qname&$prev";
        $tag1 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='Data for $qname on $mon/$day/$year'; return true;\" $mouseout>Previous Day</A> || ";
    }
    
    if($flagn) { $tag2 = " Next Day || \n"; }
    else {    
        $day = substr ($next,6,2);
        $mon = substr ($next,4,2);
        $year = substr ($next,0,4);
        $inq = "3&$qname&$next";
        $tag2 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='Data for $qname on $mon/$day/$year'; return true;\" $mouseout>Next Day</A> || ";
    }

    $day = substr ($qdate,6,2);
    $mon = substr ($qdate,4,2);
    $year = substr ($qdate,0,4);
    $psite = $qvalue - 1;
    if($psite < 1) { $psite = $numsites; }
    $pname = $site[$psite-1];
    ($sta,$comp,$net) = split ('_',$pname);
    $inq = "3&$pname&$qdate";
    $tag3 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='Data for $sta $comp $net on $mon/$day/$year'; return true;\" $mouseout>Previous Site ($sta $comp $net)</A> || ";
    
    $nsite = $qvalue + 1;
    if($nsite > $numsites) { $nsite = 1; }
    $nname = $site[$nsite-1];
    ($sta,$comp,$net) = split ('_',$nname);
    $inq = "3&$nname&$qdate";
    $tag4 = " <A HREF=\"$self\?$inq\" onMouseOver=\"window.status='Data for $sta $comp $net on $mon/$day/$year'; return true;\" $mouseout>Next Site ($sta $comp $net)</A>  ";
    
#    &Build_Banner;

print STDOUT <<EOT;
    Content-type: text/html

    <HTML>
    <HEAD><TITLE>
    $netname - Drum Recorders
    </TITLE>

    <SCRIPT language="JavaScript">

        <!--

        function printTime(evt)
        {
            var xglobal, yglobal;
            var xoffset, yoffset;
            xoffset = document.anchors[0].x + 51;
            yoffset = document.anchors[0].y + 95;
            xglobal = evt.pageX;
            yglobal = evt.pageY;
            var xpos, ypos;
            xpos = xglobal - xoffset;
            ypos = yglobal - yoffset;
            var xmax, ymax;
            xmax = 15*48;
            ymax = 96*15;
            if(xpos >= 0 && xpos < xmax && ypos >= 0 && ypos < ymax) {
                var station = " $channel $thisdate   ";
                var the_lines = 4 + ypos/15;
                var lines = parseInt(the_lines, 10) - 4;
                var the_hour = the_lines/4;
                var hour = parseInt(the_hour, 10) - 1;
                lclhour = hour + 24 + $localstart;
                gmthour = hour + 24 + $gmtstart;
                var localtime = " $timezone";
                var quart = 15*(lines - hour*4);
                var the_minute = 1 + quart + xpos/48;
                var min = parseInt(the_minute, 10) - 1;
                var the_sec = xpos/48 - parseInt(xpos/48+1) + 1;
                the_sec = the_sec*60 + 1;
                var sec = parseInt(the_sec) - 1
                if(min  < 10) min  = "0" + min;
                if(sec  < 10) sec  = "0" + sec;
                if(lclhour >= 24) lclhour = lclhour - 24;
                if(lclhour >= 24) lclhour = lclhour - 24;
                if(lclhour <  10) lclhour = "0" + lclhour;
                if(gmthour >= 24) gmthour = gmthour - 24;
                if(gmthour >= 24) gmthour = gmthour - 24;
                if(gmthour <  10) gmthour = "0" + gmthour;
                var lcltime = lclhour  + ":" + min  + ":" + sec + " $timezone     ";
                var gmttime = gmthour  + ":" + min  + ":" + sec + " UTC   ";
                window.status =  station + gmttime + lcltime;
                return false
            }
            else {
                return true
            }
        }

// Assign event handlers
        function init() {
            document.captureEvents( Event.MOUSEMOVE )
            document.onmousemove = printTime
        }

        //-->

    </SCRIPT>
    
    </HEAD>
    <BODY BGCOLOR=$pagecolor TEXT=#000000 vlink=purple link=blue onLoad="init()">

    <img align=left src="$logo" alt="$altlogo"> 

    <p>&nbsp;</p>

    <dl></dl>

    <table width="75%" border="1" align="center" bgcolor=$bannercolor bordercolordark="#000000">
      <tr bgcolor=$bannercolor> 
        <td height="47"> 
          <center>
            <font face="Times New Roman, Times, serif" size="6" color=$bannertxtcolor>
                $banner1
            </font> </center>
        </td>
      </tr>
      <tr bgcolor=$bannercolor> 
        <td height="34"> 
          <center>
            <font face="Arial"><font size="4" color=$bannertxtcolor face="Times New Roman, Times, serif">
                $banner2
            </font></font> </center>
        </td>
      </tr>
    </table>

    <center><h4>
    $tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8 $tag9
    </h4></center>
    <P> <HR> <P>
    
EOT
    
    print STDOUT "<center> <h4><FONT COLOR=red>";
    print STDOUT "Move the cursor over the seismogram to read time!";
    print STDOUT "</FONT></h4> </center>";
    
    print STDOUT "<center><td>";
    print STDOUT "<A name=\"anchor1\"   \n>";
    
    print STDOUT "<IMG SRC=\"$datadir/$currentitem\" WIDTH=$gbwid HEIGHT=$gbhgt>  \n";
    print STDOUT "</a>";


    print STDOUT "</td></center> \n ";
    
    print STDOUT "<center><h4> \n $tag1 \n $tag2 \n $tag3 \n $tag4 \n $tag5 \n $tag6 \n $tag7 \n $tag8 \n $tag9 </h4></center>\n";

    print STDOUT " <P> <HR> <P> </body> </html> \n";

return;

}

########################################################################
# Select station to use for helicorder
# querytype = 5
########################################################################
sub select_sta {

    $banner1 = $netname;
    $banner2 = "Drum Recorder Data Selector";

    $tag1 = " ";
    $tag2 = " ";
    $tag3 = " ";
    $tag4 = " ";
    $tag5 = " ";
    $tag6 = " <A HREF=\"$self\" onMouseOver=\"window.status='Select From Available Data'; return true;\"    $mouseout> Data Index </A>  ";
    $tag8 = " ";
    $tag9 = " ";

    &Build_Banner;
    
print STDOUT <<EOT;
    <p><font size="3"><b>Select a Station, then Submit it for further plotting variable selection.
     <form method="post" action="$self?6&NULL&0" name="chan_form">
       <select name="chan_sel">
EOT

    $string = "$makelist $config  >menulst 2>&1 &";    
#    $string = "$makelist $config  >/dev/null 2>&1 &";    
#    print "string:  $string","<br>";
    system ($string);

#    if(open(NAMES, $database)) {
    if(open(NAMES, $menulist)) {
         while(<NAMES>) {
            chomp;
            @fields = split(/ +/);
            $dbname = "$fields[0] $fields[1]";
                $flag = 1;
                foreach $item (@chan_names) {
                    if($dbname eq $item) { $flag = 0; }
                }
                if($flag) { @chan_names = (@chan_names,$dbname); }
           
         }
        close(NAMES);
        @chan_names = sort(@chan_names);
    }
    
    foreach $item (@chan_names) {
        print STDOUT " <option value=\"$item\">$item</option> \n";
    }
    
    print STDOUT " </select> <p> <br> \n";
    
    print STDOUT " <input type=submit value=\"Submit Station\"> \n";
    
    print STDOUT " </form> </center> \n";
    
    print STDOUT " </BODY></HTML> \n";

return;
}

########################################################################
# Select additional variables for a helicorder
# querytype = 6
########################################################################
sub make_var {

    $sizeof_line = $ENV{'CONTENT_LENGTH'};
    read (STDIN, $line, $sizeof_line);

    ($field_name, $site_net) = split (/=/, $line);
    ($site, $net) = split (/\+/, $site_net);

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Build the list of available channels
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    $nchan = 0;
#    if(open(NAMES, $database)) {
    if(open(NAMES, $menulist)) {
         while(<NAMES>) {
            chomp;
            @fields = split(/ +/);
            $dbname = "$fields[2]";
                $flag = 0;
                if($fields[0] eq $site && $fields[1] eq $net) { $flag = 1; }
                foreach $item (@chan_names) {
                    if($dbname eq $item) { $flag = 0; }
                }
                if($flag) { @chan_names = (@chan_names,$dbname); $nchan += 1; }
           
         }
        close(NAMES);
        @chan_names = sort(@chan_names);
    }
    if($nchan gt 1) {    
        $scn = "$site $net";
    }
    else {
        $scn = "$site $chan_names[0] $net";
    }

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Build and display the banner
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    $banner1 = "Select Helicorder Parameters";
    $banner2 = "$scn";

    $tag1 = " ";
    $tag2 = " ";
    $tag3 = " ";
    $tag4 = " ";
    $tag5 = " ";
    $tag6 = " <A HREF=\"$self\" onMouseOver=\"window.status='Select From Available Data'; return true;\"    $mouseout> Data Index </A>  ";
    $tag8 = " ";
    $tag9 = " ";

    &Build_Banner;
    
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Build the channel selection menu (if necessary)
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    print STDOUT " <form method=\"post\" action=\"$self?7&NULL&0\" name=\"comp_form\"> \n";
    print STDOUT " <center> \n";
    print STDOUT " <input type=hidden name=\"sn_sel\" value=\"$site $net\"> \n";
    
    if($nchan gt 1) {    
        print STDOUT " <p><font size=\"4\"><b>Select Component. <br>\n";
        print STDOUT " <select name=\"comp_sel\"> \n";
    
        foreach $item (@chan_names) {
            print STDOUT " <option value=\"$item\">$item</option> \n";
        }
        
        print STDOUT " </select>  \n";
    }
    else {
        print STDOUT " <input type=hidden name=\"comp_sel\" value=\"$chan_names[0]\"> \n";
    }
    
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Build the day selection menu
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    print STDOUT " <p><font size=\"4\"><b>Select Day. <br>\n";
    print STDOUT " <select name=\"date_sel\"> \n";
    
    print STDOUT " <option value=0>Today</option> \n";
    print STDOUT " <option value=1>Yesterday</option> \n";
    print STDOUT " <option value=2>Two Days Ago</option> \n";
    print STDOUT " <option value=3>Three Days Ago</option> \n";
    print STDOUT " <option value=4>Four Days Ago</option> \n";
    print STDOUT " <option value=5>Five Days Ago</option> \n";
    print STDOUT " <option value=6>Six Days Ago</option> \n";
    print STDOUT " <option value=\"99\">All Available Data</option> \n";
    
    print STDOUT " </select>  \n";

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Build the starting hour selection menu
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    print STDOUT " <p><font size=\"4\"><b>Select Hour to Start. <br>\n";
    print STDOUT " <select name=\"hr1_sel\"> \n";
    
    for($i=0;$i<24;$i++) { print STDOUT " <option>$i \n"; }
    
    print STDOUT " </select>  \n";

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Build the number of hours selection menu
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    print STDOUT " <p><font size=\"4\"><b>Select Number of Hours to Display. <br>\n";
    print STDOUT " <select name=\"hr2_sel\"> \n";
    
    for($i=24;$i>0;$i--) { 
        if($i eq 4) { print STDOUT " <option selected>$i \n"; }
        else        { print STDOUT " <option>$i \n"; }
    }
    
    print STDOUT " </select>  \n";

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Build the scale factor selection menu
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    print STDOUT " <p><font size=\"4\"><b>Select Amplitude Scaling. <br>\n";
    print STDOUT " <select name=\"sf_sel\"> \n";
    
    print STDOUT " <option value=   0.001>0.001</option> \n";
    print STDOUT " <option value=   0.002>0.002</option> \n";
    print STDOUT " <option value=   0.005>0.005</option> \n";
    print STDOUT " <option value=   0.01> 0.01</option> \n";
    print STDOUT " <option value=   0.02> 0.02</option> \n";
    print STDOUT " <option value=   0.05> 0.05</option> \n";
    print STDOUT " <option value=   0.1>  0.1</option> \n";
    print STDOUT " <option value=   0.2>  0.2</option> \n";
    print STDOUT " <option value=   0.5>  0.5</option> \n";
    print STDOUT " <option value=   1.0 selected>1.0</option> \n";
    print STDOUT " <option value=   2.0>   2.0</option> \n";
    print STDOUT " <option value=   5.0>   5.0</option> \n";
    print STDOUT " <option value=  10.0>  10.0</option> \n";
    print STDOUT " <option value=  20.0>  20.0</option> \n";
    print STDOUT " <option value=  50.0>  50.0</option> \n";
    print STDOUT " <option value= 100.0> 100.0</option> \n";
    print STDOUT " <option value= 200.0> 200.0</option> \n";
    print STDOUT " <option value= 500.0> 500.0</option> \n";
    print STDOUT " <option value=1000.0>1000.0</option> \n";
    
    print STDOUT " </select> <p> <br> \n";
    
    print STDOUT " <p><font size=\"4\"><b>Click Here to Begin Construction of the Helicorder Plot. <br>\n";
    print STDOUT " <input type=submit value=\"Submit\">  \n";
    
    print STDOUT " </form> </center> \n";

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Finish up the html file:
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    print STDOUT "<HR><center>  \n ";
    print STDOUT "<a href=\"$self\?5&NULL&0\">Select Another Helicorder</a>  \n ";
    print STDOUT "</center></body></html>  \n ";

return;
}

########################################################################
# Start up the helicorder program for one channel
# querytype = 7
########################################################################
sub make_heli {

    $sizeof_line = $ENV{'CONTENT_LENGTH'};
    read (STDIN, $line, $sizeof_line);

    ($field0,$field1,$field2,$field3,$field4,$field5) = split (/&/, $line, 6);

    ($field_name, $site_net) = split (/=/, $field0);
    ($site, $net) = split (/\+/, $site_net);

    ($field_name, $comp) = split (/=/, $field1);
    ($field_name, $date) = split (/=/, $field2);
    ($field_name, $hr1)  = split (/=/, $field3);
    ($field_name, $hr2)  = split (/=/, $field4);
    ($field_name, $sf)   = split (/=/, $field5);

    # Debugging statement:
    # print "Raw input: $line","<br>";
    # More debugging statements:
    # print "Site: $site","<br>";
    # print "Net:  $net","<br>";
    # print "Comp:  $comp","<br>";
    # print "Day:  $date","<br>";
    # print "Hr1:  $hr1","<br>";
    # print "Hr2:  $hr2","<br>";
    # print "SF:  $sf","<br>";
        
    $scn = "$site $comp $net";

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Build and display the banner
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    $banner1 = "Plotting Helicorder For";
    $banner2 = "$scn";

    $tag1 = " ";
    $tag2 = " ";
    $tag3 = " ";
    $tag4 = " ";
    $tag5 = " ";
    $tag6 = " <A HREF=\"$self\" onMouseOver=\"window.status='Select From Available Data'; return true;\"    $mouseout> Data Index | </A>  ";
    $tag7 = " <A HREF=\"$self?5&NULL&0\" onMouseOver=\"window.status='Build Another Helicorder'; return true;\"    $mouseout> Build Another Helicorder </A>  ";
    $tag8 = " ";
    $tag9 = " ";

    &Build_Banner;
    
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Start the program to make the helicorder plot
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

if($date < 10) {
    $string = "$program $config $scn $date $hr1 $hr2 $sf >/dev/null 2>&1 &";    
#    $string = "$program $config $scn $date $hr1 $hr2 $sf >heli2go 2>&1 &";    
#    print "string:  $string","<br>";
    system ($string);
}
else {
    for ($i=0;$i<7;$i++) {
        $string = "$program $config $scn $i 0 24 $sf >/dev/null 2>&1 &";
#        $string = "$program $config $scn $i 0 24 $sf >heli2go 2>&1 &";
#        print "string:  $string","<br>";
        system ($string);
    }
}
    
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Finish up the html file:
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
print STDOUT <<EOT;

    <center><font size=+1>
    <h4>
    The Helicorder has been started.  It will take a few minutes to complete. <br>
    To view the Helicorder, return to the <a href="$self">Data Index</a> and
    locate it in the list.
    You may need to re-load the Data Index page.
    </h4>
    </center></font></P>
    <HR>

EOT

return;
}

########################################################################
# Build the generic banner for a page.
########################################################################
sub Build_Banner {

print STDOUT <<EOT;
Content-type: text/html

    <HTML>
    <HEAD><TITLE>
    $netname - Drum Recorders
    </TITLE></HEAD>
    <BODY BGCOLOR=$pagecolor TEXT=#000000 vlink=purple link=blue>

    <img align=left src="$logo" alt="$altlogo"> <br>

    <dl></dl>

    <table width="75%" border="1" align="center" bgcolor=$bannercolor bordercolordark="#000000">
      <tr bgcolor=$bannercolor> 
        <td height="47"> 
          <center>
            <font face="Times New Roman, Times, serif" size="6" color=$bannertxtcolor>
                $banner1
            </font> </center>
        </td>
      </tr>
      <tr bgcolor=$bannercolor> 
        <td height="34"> 
          <center>
            <font face="Arial"><font size="4" color=$bannertxtcolor face="Times New Roman, Times, serif">
                $banner2
            </font></font> </center>
        </td>
      </tr>
    </table>

    <center><h4>
    $tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8 $tag9
    </center>

    <P> <HR> <P>
EOT

return;

}

########################################################################
# Take a time array of form ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)
#  and increments by a given number of hours.
# Returns the new time array.
########################################################################
sub hr_increment {

    local (@t1) = splice (@_,0,9);
    local ($delhr) = @_;

    # print "t1 = @t1 \n";
    # print "delhr = $delhr\n";

    require "timelocal.pl";

    my $t1seconds = &timegm(@t1);
    my $t2seconds = $t1seconds + $delhr*3600.0;

    @t2 = gmtime($t2seconds);

    # print "t2 = @t2 \n";

return @t2;

}

########################################################################
# Take a date string of form YYYYMMDD.
# Returns the new time array.
########################################################################
sub date2secs {

    $date = $qdate;
    $sec0 = 0;
    $min0 = 0;
    $hour0 = 0;
    $mday0 = substr ($date,6,2);
    $mon0 = substr ($date,4,2) - 1;
    $year0 = substr ($date,0,4) - 1900;
    $wday0 = 0;
    $yday0 = 0;
    $isdst0 = 0;
    @t2 = ($sec0,$min0,$hour0,$mday0,$mon0,$year0,$wday0,$yday0,$isdst0);

return @t2;

}

########################################################################
