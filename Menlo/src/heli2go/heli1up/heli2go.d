########################################################################
# This is the heli2go parameter file. 
# This module creates helicorder displays.
#
#                                             7/28/2000    Jim Luetgert
########################################################################

# List of wave servers (ip port comment) to contact to retrieve trace data.
 WaveServer 130.118.43.4   16031      "wsv2 - ad1"
 WaveServer 130.118.43.4   16032      "wsv2 - ad2"
 WaveServer 130.118.43.4   16033      "wsv2 - ad3" 
 WaveServer 130.118.43.4   16034      "wsv2 - ad4"
 WaveServer 130.118.43.4   16035      "wsv2 - ad5"
 WaveServer 130.118.43.4   16036      "wsv2 - ad6" 
 WaveServer 130.118.43.4   16037      "wsv2 - ad7"
 WaveServer 130.118.43.4   16038      "wsv2 - ad8"
 WaveServer 130.118.43.4   16039      "wsv2 - ad9" 
 WaveServer 130.118.43.4   16025      "wsv2 - Nano"
 WaveServer 130.118.43.4   16026      "wsv2 - DST"
 WaveServer 130.118.43.4   16027      "wsv2 - UCB"
 WaveServer 130.118.43.4   16028      "wsv2 - CIT"
 WaveServer 130.118.43.4   16029      "wsv2 - K2"
 WaveServer 130.118.43.3   16025      "wsv1 - Nano"
 WaveServer 130.118.43.3   16026      "wsv1 - DST"
 WaveServer 130.118.43.3   16027      "wsv1 - UCB"
 WaveServer 130.118.43.3   16028      "wsv1 - CIT"
 WaveServer 130.118.43.3   16029      "wsv1 - K2"

# Directory in which to store the temporary .gif, .link and .html files.
 GifDir   /home/ppicker/gifs/heli2go/  

# Directory in which to store the final .gif files.
 Target   /home/ppicker/waveforms/heli2go/  

# The file with all the station information.
 StationList     /home/earthworm/run/params/calsta.db

# The file with all the waveserver stations.
 MenuList     /home/earthworm/run/params/calmenu.db

# Plot Display Parameters - 
		# The following is designed such that each SCN creates it's own
		# helicorder display; one per panel of data.
		
# 01 HoursPerPlot    Total number of hours per gif image
# 02 Minutes/Line    Number of minutes per line of trace
# 03 Plot Previous   On startup, retrieve and plot at least n previous hours from tank.
# 04 XSize           Overall size of plot in inches
# 05 YSize           Setting these > 100 will imply pixels
# 06 Show UTC        UTC will be shown on one of the time axes.
# 07 Use Local       The day page will be local day rather than UTC day.
# 08 Local Time Diff UTC - Local.  e.g. -7 -> PDT; -8 -> PST
# 09 Local Time Zone Three character time zone name.
# 10 Mean Removal    Mean of 1st minute of each line will be removed.
# 11 Filter          Apply a lo-cut filter.
#                                      
#        01  02 03 04  05 06 07   08   09  10   11

 Display 24  15  1 10  20  1  0   -7  PDT   1    0


    # *** Optional Commands ***

 LabelDC    * label each trace line with its dc offset.
#NoLabelDC  * Don'tlabel each trace line with its dc offset [default].

 RetryCount    2  # Number of attempts to get a trace from server; default=2
 Logo    smusgs.gif   # Name of logo in GifDir to be plotted on each image

# SaveDrifts   # Log drifts to GIF directory.

# Clip  20     # Clip the traces at N vertical divisions.

# PlotUp     # Plot from bottom to top.
  PlotDown   # Plot from top to bottom. [default]

# We accept a command "Debug" which turns on a bunch of log messages
  Debug
# WSDebug
