
    /********************************************************************
     *                              cta2cusp                            *
     *                                                                  *
     *  Program to read continuous tape archive (cta) files and feed    *
     *  the data to Cusp.                                               *
     ********************************************************************/

/*
Changed program to use new snippet format with ascii event id's.
cta2cusp doesn't use the event id from the triglist message anyway.
WMK 5/2/2000

Cosmetic changes.  Program will process cta files beginning with wt or wu.
Only old version triglist messages, without location code, will work.
If there are multiple channels with the same SCN, they will be interleaved
in the file sent to CUSP.  This is bad.  WMK 5/12/04
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <kom.h>
#include <trace_buf.h>
#include <time_ew.h>
#include "cta2cusp.h"

#define MAXCHAR  80                  /* Max chars in file names */

/* Function prototypes
   *******************/
void GetConfig( char * );
int  GetTrigList( char *, char *, TRIGGER *, int * );
int  GetScnList( SCN *, int *, char *, TRIGGER *, int );
void SortScnList( SCN *, int );
int  WriteFile( SCN *, int, char * );

/* Read these from the config file
   *******************************/
int  nCtaFile;                       /* Maximum number of cta files */
char **CtaFile;                      /* Array of names of the cta files */
char TriggerFile[MAXCHAR];           /* Name of trigger file */
int  MaxTriglistSize;                /* Max size of trigger list, in bytes */
int  MaxChannels;                    /* The most channels we will ever send to Cusp */
char OutputFile[MAXCHAR];            /* Name of output file */


int main( int argc, char **argv )
{
   long        recsize;              /* Size of retrieved message */
   char        *tracebuf;            /* Buffer to hold event msg */
   char        *configFile;
   SCN         *scns;                /* List of SCNs obtained */
   int         rc;
   int         numread;
   int         i;
   static char defaultConfigFile[] = "cta2cusp.d";
   char        *trigbuf;
   TRIGGER     *trigs;               /* List of triggers */
   int         nTrigger;
   int         nScn;                 /* Number of SCNs obtained */

/* Get the config file name from the command line.
   If not entered in the command line, use the default name.
   ********************************************************/
   configFile = ( argc < 2 ) ? defaultConfigFile : argv[1];

/* Read the configuration file
   ***************************/
   GetConfig( configFile );

/* Allocate the triglist message buffer
   ************************************/
   trigbuf = (char *) malloc( (size_t)MaxTriglistSize );
   if ( trigbuf == NULL )
   {
      printf( "cta2cusp: Can't allocate triglist message buffer. Exiting.\n" );
      return -1;
   }
 
/* Allocate the trigger list
   *************************/
   trigs = (TRIGGER *) calloc( MaxChannels, sizeof(TRIGGER) );
   if ( trigs == NULL )
   {
      printf( "cta2cusp: Can't allocate the trigger list. Exiting.\n" );
      return -1;
   }

/* Allocate the list of SCNs obtained
   **********************************/
   scns = (SCN *) calloc( MaxChannels, sizeof(SCN) );
   if ( scns == NULL )
   {
      printf( "cta2cusp: Can't allocate the list of SCNs obtained. Exiting.\n" );
      return -1;
   }

/* Allocate buffer to hold one tracebuf message
   ********************************************/
   tracebuf = (char *) malloc( (size_t)(MAX_TRACEBUF_SIZ+1) );
   if ( tracebuf == NULL )
   {
      printf( "Couldn't allocate the trace buffer. Exiting.\n" );
      return -1;
   }

/* Read the trigger file
   *********************/
   if ( GetTrigList( TriggerFile, trigbuf, trigs, &nTrigger ) != EW_SUCCESS )
   {
      printf( "cta2cusp: Can't get the trigger file. Exiting.\n" );
      return -1;
   }

/* for ( i = 0; i < nTrigger; i++ )
   {
      printf( "%-5s",    trigs[i].sta );
      printf( " %-3s",   trigs[i].cmp );
      printf( " %-2s",   trigs[i].net );
      printf( " %2d",    trigs[i].ssYear );
      printf( "%02d",    trigs[i].ssMonth );
      printf( "%02d",    trigs[i].ssDay );
      printf( " %02d",   trigs[i].ssHour );
      printf( ":%02d",   trigs[i].ssMinute );
      printf( ":%.3lf",  trigs[i].ssSecond );
      printf( "  %.3lf", trigs[i].stime );
      printf( "  %3d",   trigs[i].duration );
      printf( "\n" );
   } */

/* Get the list of SCNs obtained
   *****************************/
   printf( "Getting list of SCNs to send.\n" );
   if ( GetScnList( scns, &nScn, tracebuf, trigs, nTrigger ) != EW_SUCCESS )
   {
      printf( "cta2cusp: Can't get the SCN obtained list. Exiting.\n" );
      return -1;
   }
   if ( nScn < 1 )
   {
      printf( "cta2cusp: The cta files contain no data for this event. Exiting.\n" );
      return -1;
   }
   printf( "Got the SCN list. Number of channels: %d\n", nScn );

/* Sort the scns list alphabetically by SCN
   ****************************************/
   SortScnList( scns, nScn );

/* printf( "Converting these channels:\n" );
   for ( i = 0; i < nScn; i++ )
   {
      printf( "%-5s",  scns[i].sta );
      printf( " %-3s", scns[i].cmp );
      printf( " %-2s", scns[i].net );
      printf( "  %6d bytes", scns[i].nbyte );
      printf( "\n" );
   } */

/* Write the output file
   *********************/
   printf( "Writing to file: %s\n", OutputFile );
   if ( WriteFile( scns, nScn, tracebuf ) != EW_SUCCESS )
   {
      printf( "cta2cusp: WriteFile() error. Exiting.\n" );
      return -1;
   }
   return 0;
}


  /************************************************************************
   *  GetConfig() processes command file(s) using kom.c functions;        *
   *                    exits if any errors are encountered.              *
   ************************************************************************/

#define NCOMMAND 6

void GetConfig( char *configfile )
{
   const    ncommand = NCOMMAND;    /* Process this many required commands */
   char     init[NCOMMAND];         /* Init flags, one for each command */
   int      nmiss;                  /* Number of missing commands */
   char     *com;
   char     *str;
   int      nfiles;
   int      success;
   int      i;
   int      nCta = 0;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      printf( "Error opening command file <%s>. Exiting.\n",
               configfile );
      exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )             /* Read next line from active file  */
      {
         com = k_str();            /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )          continue;
         if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "Can't open command file <%s>. Exiting.\n",
                        &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         else if ( k_its("nCtaFile") )
         {
            int i;

            nCtaFile = k_int();
            CtaFile = (char **)calloc( nCtaFile, sizeof(char *) );
            if ( CtaFile == NULL )
            {
               printf( "Can't allocate CtaFile. Exiting.\n" );
               exit( -1 );
            }
            for ( i = 0; i < nCtaFile; i++ )
            {
               CtaFile[i] = (char *)calloc( MAXCHAR, sizeof(char) );
               if ( CtaFile[i] == NULL )
               {
                  printf( "Can't allocate CtaFile[i]. Exiting." );
                  exit( -1 );
               }
            }
            init[0] = 1;
         }

         else if ( k_its("CtaFile") )
         {
            if ( !init[0] )
            {
               printf( "In cta2cusp.d, the nCtaFile command must precede" );
               printf( " CtaFile commands. Exiting.\n" );
               exit( -1 );
            }
            if ( nCta+1 > nCtaFile )
            {
                printf( "Too many <CtaFile> commands in <%s>", configfile );
                printf( "; max=%d. Exiting.\n", nCtaFile );
                exit( -1 );
            }
            if ( str=k_str() )
               strcpy( CtaFile[nCta], str );
            nCta ++;
            init[1] = 1;
         }

         else if ( k_its("TriggerFile") )
         {
            if ( str=k_str() )
               strcpy( TriggerFile, str );
            init[2] = 1;
         }
         else if ( k_its("MaxTriglistSize") )
         {
            MaxTriglistSize = k_int();
            init[3] = 1;
         }
         else if ( k_its("MaxChannels") )
         {
            MaxChannels = k_int();
            init[4] = 1;
         }
         else if ( k_its("OutputFile") )
         {
            if ( str=k_str() )
               strcpy( OutputFile, str );
            init[5] = 1;
         }

/* Unknown command
   ***************/
         else
         {
             printf( "<%s> Unknown command in <%s>.\n", com,
                     configfile );
             continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "Bad <%s> command in <%s>. Exiting.\n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
       printf( "ERROR, no " );
       if ( !init[0] )  printf( "<nCtaFile> " );
       if ( !init[1] )  printf( "<CtaFile> " );
       if ( !init[2] )  printf( "<TriggerFile> " );
       if ( !init[3] )  printf( "<MaxTriglistSize> " );
       if ( !init[4] )  printf( "<MaxChannels> " );
       if ( !init[5] )  printf( "<OutputFile> " );
       printf( "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }
   return;
}
