
                            /**********************
                             *     gettrig.c      *
                             **********************/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <chron3.h>
#include <earthworm.h>
#include <parse_trig.h>
#include "cta2cusp.h"

/* Declared volatile in config.c
   *****************************/
extern int MaxTriglistSize;          /* Max size of trigger list, in bytes */
extern int MaxChannels;              /* The most channels we will ever send to Cusp */

static char *trigPtr;


     /***************************************************************
      *                       ParseTrigbuf()                        *
      *                                                             *
      *           Function to decode one trigger line.              *
      *                                                             *
      *  Returns EW_FAILURE or EW_SUCCESS                           *
      ***************************************************************/

int ParseTrigbuf( char *trigbuf, char **nxtLine, TRIGGER *trig )
{
   char    token[5];
   int     seconds;
   SNIPPET snip;

   if ( parseSnippet( trigbuf , &snip , nxtLine ) == EW_FAILURE )
      return EW_FAILURE;

   strcpy( trig->sta, snip.sta );
   strcpy( trig->cmp, snip.chan );
   strcpy( trig->net, snip.net );

   strncpy( token, snip.startYYYYMMDD, 4 );
   token[4] = '\0';
   trig->ssYear = atoi( token );

   strncpy( token, snip.startYYYYMMDD+4, 2 );
   token[2] = '\0';
   trig->ssMonth = atoi( token );

   strncpy( token, snip.startYYYYMMDD+6, 2 );
   token[2] = '\0';
   trig->ssDay = atoi( token );

/* WARNING: snip.startHHMMSS is apparently unterminated!
            Looks like a bug in parsetrig.
   ****************************************************/
/* printf( "snip.startHHMMSS: %s\n", snip.startHHMMSS ); */

   strncpy( token, snip.startHHMMSS, 2 );
   token[2] = '\0';
   trig->ssHour = atoi( token );

   strncpy( token, snip.startHHMMSS+3, 2 );
   token[2] = '\0';
   trig->ssMinute = atoi( token );

   strncpy( token, snip.startHHMMSS+6, 2 );
   token[2] = '\0';
   seconds = atoi( token );
   trig->ssSecond = seconds + snip.starttime - floor(snip.starttime);

   trig->stime    = snip.starttime;
   trig->duration = snip.duration;
   return EW_SUCCESS;
}


     /********************************************************************
      *                          GetTrigList()                           *
      *                                                                  *
      *  Read everything from the trigger file into the trigger list.    *
      *                                                                  *
      *  Accepts:                                                        *
      *    trigFileName = Name of trigger file                           *
      *  Returns:                                                        *
      *    EW_SUCCESS if all went well                                   *
      *    EW_FAILURE if we ran into problems                            *
      *    trigbuf  = Buffer that will contain the entire trigger file   *
      *               as a single ASCII text string.                     *
      *    trigs    = Array of trigger structures, to be filled with     *
      *               info from the trigger file                         *
      *    nTrigger = Number of triggers read                            *
      ********************************************************************/

int GetTrigList( char *trigFileName, char *trigbuf, TRIGGER *trigs, int *nTrigger )
{
   FILE     *fp;
   int      i;
   int      msgsize;
   int      ntrig = 0;
   TRIGGER  trig;
   char     *nxtLine;

/* Open the trigger file
   *********************/
   fp = fopen( trigFileName, "rb" );
   if ( fp == NULL )
   {
      printf( "GetTrigList: Can't open trigger file: %s\n", trigFileName );
      return EW_FAILURE;
   }

/* Read the entire trigger list into trigbuf
   *****************************************/
   msgsize = fread( trigbuf, sizeof(char), MaxTriglistSize, fp );

   if ( ferror( fp ) )
   {
      printf( "GetTrigList: Error reading trigger file: %s\n", trigFileName );
      fclose( fp );
      return EW_FAILURE;
   }

   if ( !feof( fp ) )
   {
      printf( "GetTrigList: Trigger buffer too small.\n" );
      fclose( fp );
      return EW_FAILURE;
   }

   fclose( fp );

/* Null-terminate the message
   **************************/
   if ( msgsize < MaxTriglistSize )
      trigbuf[msgsize++] = '\0';
   else
      trigbuf[msgsize-1] = '\0';

/* printf( "%s", trigbuf ); */       /* For debugging */

/* Get the event id and first trigger from the trigger message,
   and append the trigger to the trigger list
   ***********************************************************/
   nxtLine = trigbuf;
   if ( ParseTrigbuf( trigbuf , &nxtLine, &trig ) == EW_FAILURE )
   {
      printf( "GetTrigList: Can't get event id from trigger message.\n" );
      return EW_FAILURE;
   }

   trigs[ntrig] = trig;
   if ( ++ntrig >= MaxChannels )
   {
      printf( "GetTrigList: Warning: ntrig >= MaxChannels\n" );
      printf( "             Processing only the first %d triggers.\n", ntrig );
      goto FINISHED_PARSEING_TRIGMSG;
   }

/* Get the next trigger from the trigger message.
   The event id returned here is bogus.
   *********************************************/
   while ( ParseTrigbuf( trigbuf , &nxtLine, &trig ) == EW_SUCCESS )
   {
      int duplicate = FALSE;

/* If the trigger is a duplicate, discard it
   *****************************************/
      for ( i = 0; i < ntrig; i++ )
         if ( strcmp(trig.sta, trigs[i].sta) == 0 &&
              strcmp(trig.cmp, trigs[i].cmp) == 0 &&
              strcmp(trig.net, trigs[i].net) == 0 )
            duplicate = TRUE;
      if ( duplicate )
         continue;

/* Otherwise, append the trigger to the trigger list
   *************************************************/
      trigs[ntrig] = trig;
      if ( ++ntrig >= MaxChannels )
      {
         printf( "GetTrigList: Warning: ntrig >= MaxChannels\n" );
         printf( "             Processing only the first %d triggers.\n", ntrig );
         break;
      }
   }
FINISHED_PARSEING_TRIGMSG:

/* TEMPORARY CODE... Add six Irige channels to the trigger list.
   ************************************************************/
/* trigs[ntrig] = trigs[0];
   strcpy( trigs[ntrig].sta, "IRG1a" );
   strcpy( trigs[ntrig].cmp, "T" );
   strcpy( trigs[ntrig].net, "NC" );
   ntrig++;
   trigs[ntrig] = trigs[ntrig-1];
   strcpy( trigs[ntrig].sta, "IRG2a" );
   ntrig++;
   trigs[ntrig] = trigs[ntrig-1];
   strcpy( trigs[ntrig].sta, "IRG1e" );
   ntrig++;
   trigs[ntrig] = trigs[ntrig-1];
   strcpy( trigs[ntrig].sta, "IRG2e" );
   ntrig++;
   trigs[ntrig] = trigs[ntrig-1];
   strcpy( trigs[ntrig].sta, "IRG1i" );
   ntrig++;
   trigs[ntrig] = trigs[ntrig-1];
   strcpy( trigs[ntrig].sta, "IRG2i" );
   ntrig++; */

/* If any one of the SCNs in the trigger list is a wildcard,
   ie * * *, discard all other SCNs from the list.
   ********************************************************/
   for ( i = 0; i < ntrig; i++ )
      if ( strcmp(trigs[i].sta, "*") == 0 &&
           strcmp(trigs[i].cmp, "*") == 0 &&
           strcmp(trigs[i].net, "*") == 0 )
      {
         trigs[0] = trigs[i];
         ntrig = 1;
         break;
      }

   *nTrigger = ntrig;                        /* Return the number of triggers found */
   return EW_SUCCESS;
}
