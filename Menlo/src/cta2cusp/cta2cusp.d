#
# This is cta2cusp's parameter file
#
TriggerFile      /home/earthworm/run/trig/trigtest.019182

OutputFile       cta2cusp.out

MaxTriglistSize  200000   # Max size of a triglist message, in bytes.
                          #   Warning: If this number is too small, large
                          #   events will not be sent.
MaxChannels      800      # Set MaxChannels to be greater than the number
                          #   of channels in the network.
#
#
# Enter the names of the continuous tape archive files below.
# File names may begin with "wt" or "wu".
# The files must be listed in chronological order.
#
nCtaFile    2            # Number of cta files
CtaFile   /home/earthworm/run/spool/wt19990304004933
CtaFile   /home/earthworm/run/spool/wt19990304005101

