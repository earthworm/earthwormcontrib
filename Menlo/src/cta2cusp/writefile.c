
       /**********************************************************
        *                      WriteFile()                       *
        *  Returns EW_SUCCESS or EW_FAILURE.                     *
        **********************************************************/

#include <stdio.h>
#include <earthworm.h>
#include <trace_buf.h>
#include "cta2cusp.h"

/* From the config file
   ********************/
extern int  nCtaFile;        /* Maximum number of cta files */
extern char **CtaFile;       /* Array of names of the cta files */
extern char OutputFile[];    /* Name of output file */

/* Function declaration
   ********************/
int TraceMakeIBM( char *, long );


int WriteFile( SCN *scns, int nScn, char *tracebuf )
{
   int  i;
   FILE *fpOut;
   TRACE_HEADER *WaveHead = (TRACE_HEADER *) tracebuf;
   const int maxDataBytes = MAX_TRACEBUF_SIZ - sizeof(TRACE_HEADER);

/* Open the output file
   ********************/
   if ( (fpOut = fopen( OutputFile, "wb" )) == NULL )
   {  
      printf( "Error opening output file: %s. Exiting.\n", OutputFile );
      return -1;
   }
 
/* Calculate the offset of each SCN in the output file
   ***************************************************/
   scns[0].offset = 0;
   for ( i = 1; i < nScn; i++ )
      scns[i].offset = scns[i-1].offset + scns[i-1].nbyte;

/* Main loop
   *********/
   for ( i = 0; i < nCtaFile; i++ )
   {
      char  *dataPtr = tracebuf + sizeof(TRACE_HEADER);
      FILE  *fpCta;

/* Open a cta disk file
   ********************/
      if ( (fpCta = fopen( CtaFile[i], "rb" )) == NULL )
      {  
         printf( "Error opening cta disk file: %s. Exiting.\n", CtaFile[i] );
         return -1;
      }
       
/* Read tracebuf messages from the cta file
   ****************************************/
      while ( fread( WaveHead, sizeof(TRACE_HEADER), 1, fpCta ) == 1 )
      {
         int j;
         int wantIt = 0;          /* Non-zero if we want this SCN */
         int nDataBytes;
         int nMsgBytes;
         TRACE_HEADER thdr;

/* Make a local copy of the trace header
   *************************************/
         memcpy( &thdr, WaveHead, sizeof(TRACE_HEADER) );

/* Convert tracebuf header to local byte order
   *******************************************/
         if ( TraceHeadMakeLocal( &thdr ) == -1 )
         {
            printf( "Unknown data type: %s Exiting.\n", thdr.datatype );
            exit( -1 );
         }

/* Read the data samples for this message from the cta file
   ********************************************************/
         nDataBytes = thdr.nsamp * (thdr.datatype[1] - 48);
         nMsgBytes  = nDataBytes + sizeof(TRACE_HEADER);
 
         if ( fread( dataPtr, sizeof(char), nDataBytes, fpCta ) < nDataBytes )
         {
            printf( "Error reading cta file: %s. Exiting.\n", CtaFile[i] );
            return -1;
         }

/* If the SCN strings are too long, ignore the message.
   Some Reftek test data was give bogus, long SCN names.
   ****************************************************/
         if ( strlen( thdr.sta  ) > 5 ) continue;
         if ( strlen( thdr.chan ) > 3 ) continue;
         if ( strlen( thdr.net  ) > 2 ) continue;

/* We want this message if its SCN is in the SCN list.
   The index of the matching SCN (j) is used below.
   **************************************************/
         for ( j = 0; j < nScn; j++ )
         { 
            int sta_ok = strcmp( scns[j].sta, thdr.sta  ) == 0;
            int cmp_ok = strcmp( scns[j].cmp, thdr.chan ) == 0;
            int net_ok = strcmp( scns[j].net, thdr.net  ) == 0;

            if ( wantIt = sta_ok && cmp_ok && net_ok )
               break;
         }
         if ( !wantIt ) continue;      /* Get next tracebuf message */

/* Is the message in the desired time window?
   If not, get the next message.
   *****************************************/
         {
            double trigStartTime = scns[j].stime;
            double trigEndTime   = scns[j].etime;
            int    dontWantIt    = ( trigEndTime < thdr.starttime ) ||
                                   ( thdr.endtime < trigStartTime );
            wantIt = !dontWantIt;
         }
         if ( !wantIt ) continue;      /* Get next tracebuf message */

/* We want this message.
   Convert it to IBM/DEC byte order.
   ********************************/
         TraceMakeIBM( tracebuf, nMsgBytes );

/* Write message to output file
   ****************************/
         if ( fseek( fpOut, scns[j].offset, SEEK_SET ) != 0 )
         {
            printf( "fseek() error.\n" );
            return EW_FAILURE;
         }
         if ( fwrite( tracebuf, sizeof(char), nMsgBytes, fpOut ) < nMsgBytes )
         {
            printf( "fwrite() error.\n" );
            return EW_FAILURE;
         }
         scns[j].offset += nMsgBytes;

      }                    /* End of loop that processes one tracebuf msg */
      fclose( fpCta );
   }
   fclose( fpOut );
   return EW_SUCCESS;
}
