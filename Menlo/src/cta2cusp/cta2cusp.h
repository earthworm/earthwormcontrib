
/*   cta2cusp.h    */

typedef struct
{
   char   sta[6];           /* Station */
   char   cmp[4];           /* Component */
   char   net[3];           /* Network */
   int    nbyte;            /* Number of bytes for this SCN */
   int    offset;           /* Write to this position in output file */
   double stime;            /* Trigger start time */
   double etime;            /* Trigger end time */
} SCN;

typedef struct
{
   char   sta[6];           /* Station */
   char   cmp[4];           /* Component */
   char   net[3];           /* Network */
   int    ssYear;           /* Year of start save time (last two digits) */
   int    ssMonth;          /* Month of start save time (January = 1) */
   int    ssDay;            /* Day of start save time */
   int    ssHour;           /* Hour of start save time */
   int    ssMinute;         /* Minute of start save time */
   double ssSecond;         /* Second of start save time */
   double stime;            /* Start save time in sec since Jan 1, 1970 */
   int    duration;         /* Duration in seconds */
} TRIGGER;

