#
#                     Make file for cta2cusp
#                         Solaris Version
#

CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib


OBJ = cta2cusp.o gettrig.o getscnlist.o makelocal.o \
      writefile.o makeIBM.o parse_trig.o \
      $L/kom.o $L/chron3.o $L/time_ew.o $L/swap.o

cta2cusp: $(OBJ)
	cc -o $B/cta2cusp $(OBJ) -mt -lm -lposix4 -lthread -lc


# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/cta2cusp*
