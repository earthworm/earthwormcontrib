
       /**********************************************************
        *                     GetScnList()                       *
        *  Returns EW_SUCCESS or EW_FAILURE.                     *
        **********************************************************/

#include <stdio.h>
#include <string.h>
#include <earthworm.h>
#include <trace_buf.h>
#include "cta2cusp.h"

/* From the config file
   ********************/
extern int  nCtaFile;        /* Maximum number of cta files */
extern char **CtaFile;       /* Array of names of the cta files */
extern int  MaxChannels;     /* The most channels we will ever send to Cusp */

/* Function declaration
   ********************/
int TraceHeadMakeLocal( TRACE_HEADER * );


int GetScnList( SCN *scns, int *nScn, char *tracebuf,
                TRIGGER *trigs, int nTrigger )
{
   int nscn = 0;                /* Number of desired SCNs found in the cta */
   int i;
   TRACE_HEADER *tracehead = (TRACE_HEADER *) tracebuf;
   const int maxDataBytes = MAX_TRACEBUF_SIZ - sizeof(TRACE_HEADER);

/* Loop through all cta files
   **************************/
   for ( i = 0; i < nCtaFile; i++ )
   {
      FILE *fp;
      char *dataPtr = tracebuf + sizeof(TRACE_HEADER);

/* Open a cta disk file
   ********************/
      if ( (fp = fopen( CtaFile[i], "rb" )) == NULL )
      {  
         printf( "Error opening cta disk file: %s. Exiting.\n", CtaFile[i] );
         return -1;
      }
       
/* Read a trace header from the cta file
   *************************************/
      while ( fread( tracehead, sizeof(TRACE_HEADER), 1, fp ) == 1 )
      {
         int    j;
         int    wantIt = 0;          /* Non-zero if we want this SCN */
         int    inScns = 0;          /* Non-zero if have already seen this SCN */
         int    nDataBytes;
         int    nMsgBytes;
         double trigStartTime;
         double trigEndTime;

/* Convert tracebuf header to local byte order
   *******************************************/
         if ( TraceHeadMakeLocal( tracehead ) == -1 )
         {
            printf( "Unknown data type: %s Exiting.\n", tracehead->datatype );
            exit( -1 );
         }

/* Read the data samples for this message from the cta file
   ********************************************************/
         nDataBytes = tracehead->nsamp * (tracehead->datatype[1] - 48);
         if ( nDataBytes > (MAX_TRACEBUF_SIZ - sizeof(TRACE_HEADER)) )
         {
            printf( "\nERROR. Bad tracebuf message:\n" );
            printf( "nsamp:      %d\n",                        tracehead->nsamp );
            printf( "Start time: %.3lf\n",       tracehead->starttime );
            printf( "End time:   %.3lf\n",       tracehead->endtime );
            printf( "SCN:        %5s %3s %2s\n", tracehead->sta, tracehead->chan, tracehead->net );
            printf( "Data type:  %2s\n",                       tracehead->datatype );
            printf( "nDataBytes(%d) exceeds the max(%d).\n", nDataBytes, maxDataBytes );
            printf( "Exiting.\n" );
            exit( -1 );
         }

         nMsgBytes = nDataBytes + sizeof(TRACE_HEADER);
 
         if ( fread( dataPtr, sizeof(char), nDataBytes, fp ) < nDataBytes )
         {
            printf( "Error reading cta file: %s. Exiting.\n", CtaFile[i] );
            return -1;
         }

/* If the SCN strings are too long, ignore the message.
   Some Reftek test data was give bogus, long SCN names.
   ****************************************************/
         if ( strlen( tracehead->sta  ) > 5 ) continue;
         if ( strlen( tracehead->chan ) > 3 ) continue;
         if ( strlen( tracehead->net  ) > 2 ) continue;

/*       printf( "%d",      tracehead->nsamp );
         printf( "  %.3lf", tracehead->starttime );
         printf( "  %.3lf", tracehead->endtime );
         printf( "  %5s",   tracehead->sta );
         printf( " %3s",    tracehead->chan );
         printf( " %2s",    tracehead->net );
         printf( "  %2s\n", tracehead->datatype ); */

/* Do we want this SCN?  We want it if the SCN matches an SCN
   in the trigger list.  Wildcards are allowed in trigger list
   SCNs.  The index of the matching trigger (j) is used below.
   ***********************************************************/
         for ( j = 0; j < nTrigger; j++ )
         { 
            int sta_ok = strcmp( trigs[j].sta, "*"            ) == 0 ||
                         strcmp( trigs[j].sta, tracehead->sta  ) == 0;
            int cmp_ok = strcmp( trigs[j].cmp, "*"            ) == 0 ||
                         strcmp( trigs[j].cmp, tracehead->chan ) == 0;
            int net_ok = strcmp( trigs[j].net, "*"            ) == 0 ||
                         strcmp( trigs[j].net, tracehead->net  ) == 0;

            if ( wantIt = sta_ok && cmp_ok && net_ok )
               break;
         }
         if ( !wantIt ) continue;      /* Get next tracebuf message */

/* Is the message in the desired time window?
   If not, get the next message.
   *****************************************/
         trigStartTime = trigs[j].stime;
         trigEndTime   = trigStartTime + (double)trigs[j].duration;

         {
            int dontWantIt = ( trigEndTime < tracehead->starttime ) ||
                             ( tracehead->endtime < trigStartTime );
            wantIt = !dontWantIt;
         }
         if ( !wantIt ) continue;      /* Get next tracebuf message */

/* We want this message.
   Is the SCN already in the list of SCNs obtained (scns)?
   ******************************************************/
         for ( j = 0; j < nscn; j++ )
            if ( strcmp( tracehead->sta,  scns[j].sta ) == 0 &&
                 strcmp( tracehead->chan, scns[j].cmp ) == 0 &&
                 strcmp( tracehead->net,  scns[j].net ) == 0 )
            {
               inScns = 1;
               break;
            }

/* We have already seen this SCN.  Increment the byte count.
   ********************************************************/
         if ( inScns )
            scns[j].nbyte += nMsgBytes;

/* This is a new SCN.  Add it to the scn list.
   ******************************************/
         else
         {
            if ( nscn >= MaxChannels )
            {
               printf( "ERROR.  Too many channels.\n" );
               printf( "MaxChannels=%d\n", MaxChannels );
               printf( "Exiting.\n" );
               exit( -1 );
            }
            strcpy( scns[nscn].sta, tracehead->sta );
            strcpy( scns[nscn].cmp, tracehead->chan );
            strcpy( scns[nscn].net, tracehead->net );
            scns[nscn].nbyte = nMsgBytes;
            scns[nscn].stime = trigStartTime;
            scns[nscn].etime = trigEndTime;
            nscn++;
         }
      }                    /* End of loop that processes one tracebuf msg */
      fclose( fp );
   }
   *nScn = nscn;           /* Return the number of unique SCNs found */
   return EW_SUCCESS;
}


void SortScnList( SCN *scns, int nScn )
{
   int i;
   int j;
   SCN temp;

   if ( nScn < 2 ) return;       /* Nothing to sort */

   for ( i = 0; i < nScn-1; i++ )
      for ( j = i+1; j < nScn; j++ )
      {
         if ( strcmp( scns[i].sta, scns[j].sta ) < 0 ) continue;
         if ( strcmp( scns[i].sta, scns[j].sta ) == 0 )
         {
            if ( strcmp( scns[i].cmp, scns[j].cmp ) < 0 ) continue;
            if ( strcmp( scns[i].cmp, scns[j].cmp ) == 0 )
               if ( strcmp( scns[i].net, scns[j].net ) < 0 ) continue;
         }
         temp    = scns[i];
         scns[i] = scns[j];
         scns[j] = temp;
      }
}
