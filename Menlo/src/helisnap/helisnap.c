/*************************************************************************
 *      helisnap.c:                                                       *
 * Requests data from waveserver(s) and plots gif files in a helicorder  *
 * format.  These files are then transferred to webserver(s).            *
 *                                                                       *
 * This is an abbreviated command line version of heli1 for producing    *
 * one-time plots on demand from the web interface helisnap.pl.           *
 *                                                                       *
 * Jim Luetgert 07/07/00                                                 *
 *************************************************************************/

#include <platform.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <time_ew.h>
#include <chron3.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include "mem_circ_queue.h" 
#include <swap.h>
#include <trace_buf.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <wait.h>

#include <ws_clientII.h>
#include "gd.h"
#include "gdfontt.h"   /*  6pt      */
#include "gdfonts.h"   /*  7pt      */
#include "gdfontmb.h"  /*  9pt Bold */
#include "gdfontl.h"   /* 13pt      */
#include "gdfontg.h"   /* 10pt Bold */

#include "helisnap.h" 

typedef struct cdouble{double r,i;} cdouble;

#define SWAP(a,b) temp=(a);(a)=(b);(b)=temp
#define SIGN(a,b) (b)*abs((a))

/*
 * Description of a data gap.
 * Note: if a gap would be declared at end of data, the data must be 
 * truncated instead of adding another GAP structure. A gap may be
 * declared at the start of the data, however.
 */
typedef struct _GAP *PGAP;
typedef struct _GAP 
{
  double starttime;  /* time of first sample in the gap                      */
  double gapLen;     /* time from first gap sample to first sample after gap */
  long firstSamp;    /* index of first gap sample in data buffer             */
  long lastSamp;     /* index of last gap sample in data buffer              */
  PGAP next;         /* The next gap structure in the list                   */
} GAP;

/* Structure for keeping track of buffer of trace data */
typedef struct _DATABUF 
{
  double rawData[MAXTRACELTH*5];   /* The raw trace data; native byte order                */
  double delta;      /* The nominal time between sample points               */
  double starttime;  /* time of first sample in raw data buffer              */
  double endtime;    /* time of last sample in raw data buffer               */
  long nRaw;         /* number of samples in raw data buffer, including gaps */
  long lenRaw;       /* length to the rawData array                          */
  GAP *gapList;      /* linked list of gaps in raw data                      */
  int nGaps;         /* number of gaps found in raw data                     */
} DATABUF;


/* Functions in this source file
 *******************************/
void SetUp(Butler *);
void Sort_Servers (Butler *, double);
short Build_Axes(Butler *, double);
void Make_Grid(Butler *);
void Pallette(gdImagePtr GIF, long color[]);
int Plot_Trace(Butler *, double *, double);
void Do_Stats(Butler *, double *, int, double);
void Get_Line_Bias(Butler *, int);
void hpsort(int, double ra[]);

void Make_Date(char *);
void Save_Plot(Butler *);
int Build_Menu (Butler *);
int In_Menu_list (Butler *);
short RequestWave(Butler *, int, double *, char *, char *, char *, double, double);
int  ReqWaveB(Butler *, int, double *, char *, char *, char *, double, double);
int  WSReqBin(Butler *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt);
int IsDST(int, int, int);
void Decode_Time( double, TStrct *);
void Encode_Time( double *, TStrct *);
void date22( double, char *);
void SetPlot(double, double);
int ixq(double);
int iyq(double);
void Get_Sta_Info(Butler *);
int Put_Sta_Info(Butler *);
void config_me( char *,  Butler *); /* reads configuration (.d) file via Carl's routines  */

/* Things to read from configuration file
 ****************************************/
static int  Debug = 1;               /* debug flag                              */

/* Variables for talking to statmgr
 **********************************/
char      Text[150];
pid_t     MyPid;                     /* Our own pid  */

static Butler BStruct;               /* Private area for the threads            */
double    Data[MAXTRACELTH];         /* Trace array                             */
cdouble   cData[MAXTRACELTH];        /* Complex Trace array                     */

/* Other globals
 ***************/
#define   XLMARGIN   0.7             /* Margin to left of axes                  */
#define   XRMARGIN   1.0             /* Margin to right of axes                 */
#define   YBMARGIN   1.0             /* Margin at bottom of axes                */
#define   YTMARGIN   0.9             /* Margin at top of axes plus size of logo */
double    YBMargin = 1.0;            /* Margin at bottom of axes                */
double    YTMargin = 0.7;            /* Margin at top of axes                   */
double    sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's        */
                                     /* time 0 and 1970-01-01 00:00:00.0 GMT    */
char      string[20];
char      module[70];
static double    xsize, ysize, plot_up;

/*************************************************************************
 *  main( int argc, char **argv )                                        *
 *************************************************************************/

main( int argc, char **argv )
{
    char    whoami[90];
    time_t  atime, now;
    int     i, j;

    /* Check command line arguments
     ******************************/
    if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile> \n", module);
        exit( 0 );
    }
    
    /* Zero the wave server arrays *
     *******************************/
    for (i=0; i< MAX_WAVESERVERS; i++) {
        memset( BStruct.wsIp[i],      0, MAX_ADRLEN);
        memset( BStruct.wsPort[i],    0, MAX_ADRLEN);
        memset( BStruct.wsComment[i], 0, MAX_ADRLEN);
    }
    strcpy(module, argv[1]);
    for(i=0;i<(int)strlen(module);i++) if(module[i]=='.') module[i] = 0;
    strcpy(BStruct.mod, module);
    sprintf(whoami, " %s: %s: ", module, "main");

    /* Get our own Pid for restart purposes
    ***************************************/
    MyPid = getpid();
	
    /* Read the configuration file(s)
     ********************************/
    config_me( argv[1], &BStruct );

    fprintf( stderr,"%s Read command file <%s>\n", whoami, argv[1] );
	
	BStruct.DaysAgo = 0;
	BStruct.OldData = 1;
	BStruct.TotHrs = 1;
	BStruct.Scale = 1.0;
		
    Get_Sta_Info(&BStruct);
    
    for(j=0;j<BStruct.Nchans;j++) {
		strcpy( BStruct.Site , BStruct.Disp[j].Site );
		strcpy( BStruct.Comp , BStruct.Disp[j].Comp );
		strcpy( BStruct.Net  , BStruct.Disp[j].Net );
	    sprintf(BStruct.SCNnam, "%s_%s_%s",      BStruct.Site, BStruct.Comp, BStruct.Net);
	    sprintf(BStruct.SCNtxt, "%s %s %s",      BStruct.Site, BStruct.Comp, BStruct.Net);
	    sprintf(BStruct.SCN,    "%s%s%s",        BStruct.Site, BStruct.Comp, BStruct.Net);

	    Put_Sta_Info(&BStruct);
    
    
    if(!Build_Menu (&BStruct)) {
        fprintf( stderr, "%s No Menu! Just quit.\n", whoami);
        for(i=0;i<BStruct.nServer;i++) wsKillMenu(&(BStruct.menu_queue[i]));
        exit(-1);
    }
    
	    SetUp(&BStruct);
    
  /* Kill the used menus */    
    for(i=0;i<BStruct.nServer;i++) wsKillMenu(&(BStruct.menu_queue[i]));
    }

}


/********************************************************************
 *  SetUp does the initial setup and first set of images            *
 ********************************************************************/

void SetUp(Butler *But)
{
    char    whoami[90], time1[25], time2[25], sip[25], sport[25], sid[50];
    double  tankStarttime, tankEndtime, EndPlotTime, LocalTimeOffset;
    double  StartTime, EndTime, Duration, ZTime, B[8];
    time_t  current_time;
    int     i, j, k, jj, successful, server, hour1, hour2;
    int     first_gulp, mins;
    int     minPerStep, minOfDay;
    TStrct  t0, Time1, Time2;  
    
    int		ii, order, lx;
    double	dw, aflo, afhi, f, pi, percent;
    cdouble	caw;

    sprintf(whoami, " %s: %s: ", But->mod, "SetUp");
    first_gulp = 1;        
    i = 0;
    
    mins         = But->mins;
    
    SetPlot(But->xsize, But->ysize);
    LocalTimeOffset = But->LocalTimeOffset;
    
    StartTime = time(&current_time) - 60*60;
    Decode_Time(StartTime, &t0);
    t0.Sec = 0;
    minPerStep = But->secsPerStep/60;
    minOfDay = minPerStep*((t0.Hour*60 + t0.Min)/minPerStep) - minPerStep;
    if(minOfDay<0) minOfDay = 0;
    t0.Hour = But->OldData;
    t0.Hour = minOfDay/60;
    t0.Min = minOfDay - t0.Hour*60;
    t0.Min = 0;
    Encode_Time( &StartTime, &t0);    /* StartTime modulo step */
    date22 (StartTime, time1);
    fprintf( stderr, "%s Requested Start Time: %s. \n", whoami, time1);
    
    if(In_Menu_list(But)) {
        
        Sort_Servers(But, StartTime);
        if(But->nentries <= 0) goto quit;
                    
        k = But->index[0];
        tankStarttime = But->TStime[k];
        tankEndtime   = But->TEtime[k];
        
        if(But->Debug) {
            date22 (StartTime, time1);
            fprintf( stderr, "%s Requested Start Time: %s. \n", 
                  whoami, time1);
            date22 (tankStarttime, time1);
            date22 (tankEndtime,   time2);
            date22 (But->TStime[k], time1);
            date22 (But->TEtime[k], time2);
            strcpy(sip,   But->wsIp[k]);
            strcpy(sport, But->wsPort[k]);
            strcpy(sid,   But->wsComment[k]);
            fprintf( stderr, "%s Got menu for: %s. %s %s %s %s <%s>\n", 
                  whoami, But->SCNtxt, time1, time2, sip, sport, sid);
            for(j=0;j<But->nentries;j++) {
                k = But->index[j];
                date22 (But->TStime[k], time1);
                date22 (But->TEtime[k], time2);
                fprintf( stderr, "            %d %d %s %s %s %s <%s>\n", 
                      j, k, time1, time2, But->wsIp[k], But->wsPort[k], But->wsComment[k]);
            }
        }
        
        if(StartTime < tankStarttime) {
            StartTime = tankStarttime;
            Decode_Time(StartTime, &t0);
            t0.Sec = 0;
            minPerStep = But->secsPerStep/60;
            minOfDay = minPerStep*((t0.Hour*60 + t0.Min)/minPerStep);
            if(minOfDay<0) minOfDay = 0;
            t0.Hour = minOfDay/60;
            t0.Min = minOfDay - t0.Hour*60;
            Encode_Time( &StartTime, &t0);
        }
        
        Decode_Time(StartTime + LocalTimeOffset, &t0);
        t0.Min = t0.Sec = 0;
        t0.Hour = But->HoursPerPlot*(t0.Hour/But->HoursPerPlot);
        But->CurrentHour = t0.Hour;
        But->CurrentDay  = t0.Day;
        
        Duration = But->secsPerGulp;
        EndTime = StartTime + Duration;      
        Encode_Time( &ZTime, &t0);
        
        if(Build_Axes(But, ZTime)) {
            fprintf( stderr, "%s Build_axes croaked for: %s. \n", 
                     whoami, But->SCNtxt);
            goto quit;
        }
        
        if(But->Debug) {
            date22 (StartTime, time1);
            fprintf( stderr, "%s Requested Start Time: %s. \n", 
                  whoami, time1);
            date22 (EndTime, time1);
            fprintf( stderr, "%s Requested End Time:   %s. \n", 
                  whoami, time1);
        }
        
        EndPlotTime = StartTime + But->TotHrs*3600;      
        if(EndPlotTime > tankEndtime) EndPlotTime = tankEndtime;
        while(EndTime <= EndPlotTime) {
            if(But->Debug) {
                date22 (StartTime, time1);
                date22 (EndTime,   time2);
                fprintf( stderr, "%s Data for: %s. %s %s %s %s <%s>\n", 
                     whoami, But->SCNtxt, time1, time2, sip, sport, sid);
            }
            
            /* Try to get some data
            ***********************/
            for(jj=0;jj<But->nentries;jj++) {
                server = But->index[jj];
                /*
                successful = ReqWaveB(But, server, Data, But->Site, 
                */
                successful = RequestWave(But, server, Data, But->Site, 
                    But->Comp, But->Net, StartTime, Duration);
                if(successful == 1) {   /*    Plot this trace to memory. */
                    break;
                }
                else if(successful == 2) {
                    if(But->Debug) {
                        fprintf( stderr, "%s Data for: %s. RequestWave error 2\n", 
                                whoami, But->SCNtxt);
                    }
                    continue;
                }
                else if(successful == 3) {   /* Gap in data */
                    if(But->Debug) {
                        fprintf( stderr, "%s Data for: %s. RequestWave error 3\n", 
                                whoami, But->SCNtxt);
                    }
                    
                }
            }
            
            if(successful == 1) {   /*    Plot this trace to memory. */
                if(first_gulp) {
                    But->DCcorr = But->Mean;
                    for(jj=0;jj<60;jj++) But->MinMean[jj] = But->Mean;
                    for(k=0;k<30;k++) But->MinMean[k] = But->DCcorr;
                    Do_Stats(But, Data, But->Npts, StartTime);
                    Get_Line_Bias(But, 30);
                    first_gulp = 0;
                }
    
                Plot_Trace(But, Data, StartTime);
            }
            
            StartTime += But->secsPerStep;
        
            Decode_Time(StartTime + LocalTimeOffset, &t0);
            hour1 = But->HoursPerPlot*(t0.Hour/But->HoursPerPlot) + 24*t0.Day;
            hour2 = But->CurrentHour + 24*But->CurrentDay;
            if(hour1 != hour2) break;
            Duration = But->secsPerGulp;
            EndTime = StartTime + Duration;      
        }
        Save_Plot(But);
    } else {
        fprintf( stderr, "%s %s not in menu.\n", whoami, But->SCNtxt);
    }
quit:        
    sleep_ew(200);
}


/*************************************************************************
 *   Sort_Servers                                                        *
 *      From the table of waveservers containing data for the current    *
 *      SCN, the table is re-sorted to provide an intelligent order of   *
 *      search for the data.                                             *
 *                                                                       *
 *      The strategy is to start by dividing the possible waveservers    *
 *      into those which contain the requested StartTime and those which *
 *      don't.  Those which do are retained in the order specified in    *
 *      the config file allowing us to specify a preference for certain  *
 *      waveservers.  Those waveservers which do not contain the         *
 *      requested StartTime are sorted such that the possible data       *
 *      retrieved is maximized.                                          *
 *************************************************************************/

void Sort_Servers (Butler *But, double StartTime)
{
    char    whoami[90];
    char    c22[25];
    double  tdiff[MAX_WAVESERVERS*2];
    int     i, j, k, jj, last_jj, kk, hold, index[MAX_WAVESERVERS*2];
    
    sprintf(whoami, " %s: %s: ", But->mod, "Sort_Servers");
    i = 0;
        /* Throw out servers with data too old. */
    j = 0;
    while(j<But->nentries) {
        k = But->index[j];
        if(StartTime > But->TEtime[k]) {
            if(But->Debug) {
                date22( StartTime, c22);
                fprintf( stderr,"%s %d %d  %s", whoami, j, k, c22);
                    fprintf( stderr, " %s %s <%s>\n", 
                          But->wsIp[k], But->wsPort[k], But->wsComment[k]);
                date22( But->TEtime[k], c22);
                fprintf( stderr,"ends at: %s rejected.\n", c22);
            }
            But->nentries -= 1;
            for(jj=j;jj<But->nentries;jj++) {
                But->index[jj] = But->index[jj+1];
            }
        } else j++;
    }
    if(But->nentries <= 1) return;  /* nothing to sort */
            
    /* Calculate time differences between StartTime needed and tankStartTime */
    /* And copy positive values to the top of the list in the order given    */
    jj = 0;
    for(j=0;j<But->nentries;j++) {
        k = index[j] = But->index[j];
        tdiff[k] = StartTime - But->TStime[k];
        if(tdiff[k]>=0) {
            But->index[jj++] = index[j];
            tdiff[k] = -65000000; /* two years should be enough of a flag */
        }
    }
    last_jj = jj;
    
    /* Sort the index list copy in descending order */
    j = 0;
    do {
        k = index[j];
        for(jj=j+1;jj<But->nentries;jj++) {
            kk = index[jj];
            if(tdiff[kk]>tdiff[k]) {
                hold = index[j];
                index[j] = index[jj];
                index[jj] = hold;
            }
            k = index[j];
        }
        j += 1;
    } while(j < But->nentries);
    
    /* Then transfer the negatives */
    for(j=last_jj,k=0;j<But->nentries;j++,k++) {
        But->index[j] = index[k];
    }
}    


/********************************************************************
 *    Build_Axes constructs the axes for the plot by drawing the    *
 *    GIF image in memory.                                          *
 *                                                                  *
 ********************************************************************/

short Build_Axes(Butler *But, double Stime)
{
    char    whoami[90], c22[30], cstr[150], LocalTimeID[4];
    double  atime, trace_size, tsize, yp, Scale;
    int     ntrace, mins, CurrentHour, LocalTime;
    int     ix, iy, i, j, jj, k, kk;
    int     xgpix, ygpix, LocalSecs;
    long    black, must_create;
    FILE    *in;
    TStrct  Time1;
    time_t  current_time;
    gdImagePtr    im_in;

    sprintf(whoami, " %s: %s: ", But->mod, "Build_Axes");
    i = 0;
    mins  = But->mins;
    Scale = But->Scale;
    CurrentHour = But->CurrentHour;
    LocalTime = But->LocalTime;
    LocalSecs = But->LocalSecs;
    strcpy(LocalTimeID, But->LocalTimeID);

    Decode_Time( Stime, &Time1);
    if(Time1.Year < 1997) {
        fprintf( stderr, "%s Bad year (%.4d) in start time for %s. Exiting.\n", whoami, Time1.Year, But->SCNnam);
		Stime = time(&current_time) - But->secsPerStep;
        return 1;
    }
    if(IsDST(Time1.Year,Time1.Month,Time1.Day) && But->UseDST) {
        LocalTime = LocalTime + 1;
        LocalTimeID[1] = 'D';
    }
    sprintf(But->Today, "%.4d%.2d%.2d", Time1.Year, Time1.Month, Time1.Day);
    
    But->GifImage = 0L;
                 
    xgpix = 72.0*But->xsize + 8;
    ygpix = 72.0*But->ysize + 8;

    fprintf( stderr, "%s Building new axes for: %s %s %.2d\n", 
        whoami, But->SCNnam, But->Today, CurrentHour);
    But->GifImage = gdImageCreate(xgpix, ygpix);
    if(But->GifImage==0) {
        fprintf( stderr, "%s Not enough memory! Reduce size of image or increase memory.\n\n", whoami);
        return 2;
    }
    if(But->GifImage->sx != xgpix) {
        fprintf( stderr, "%s Not enough memory for entire image! Reduce size of image or increase memory.\n", 
             whoami);
        return 2;
    }
    Pallette(But->GifImage, But->gcolor);
    if(But->logo) {
        in = fopen(But->logoname, "rb");
        if(in) {
            im_in = gdImageCreateFromGif(in);
            fclose(in);
            gdImageCopy(But->GifImage, im_in, 0, 0, 0, 0, im_in->sx, im_in->sy);
            gdImageDestroy(im_in);
        }
    }

    /* Plot the frame *
     ******************/
    Make_Grid(But);

    /* Put in the time tics and labels * 
     ***********************************/
    black = But->gcolor[BLACK];
    ntrace   =  But->LinesPerHour*But->HoursPerPlot;
    trace_size =  But->axeymax/ntrace; /* height of one trace [Data] */
    for(j=0;j<ntrace;j++) {
        iy = iyq(((float)(j)+0.5)*trace_size);
        gdImageLine(But->GifImage, ixq(0.0), iy, ixq(But->axexmax), iy, black);
    }

    for(j=0;j<But->HoursPerPlot;j++) {
        k = But->UseLocal? 
            j + CurrentHour:
            j + CurrentHour + LocalTime;
        if(k <  0) k += 24;
        if(k > 23) k -= 24;
        kk = But->ShowUTC? k - LocalTime: k;
        if(kk <  0) kk += 24;
        if(kk > 23) kk -= 24;
        
        yp =  trace_size*j*But->LinesPerHour + 0.5*trace_size;
        ix = ixq(-0.5); 
        iy = iyq(yp) - 5;  
        /* don't print too close to the top */
        if ( iy < 72.0 * YTMargin + 0 ) continue;
    
        sprintf(cstr, "%02d:00", kk);
        gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);
        if(kk==0&&j>0) {
            atime =  But->UseLocal? Stime:Stime+LocalSecs;
            atime =  But->ShowUTC? atime-LocalSecs:atime;
            Decode_Time( atime + j*3600, &Time1);
            Decode_Time( atime + (j+3)*3600, &Time1);
            Time1.Hour = Time1.Min = Time1.Sec = 0;
            Encode_Time( &Time1.Time, &Time1);
            date22 (Time1.Time, c22);
            sprintf(cstr, "%.5s", c22) ;
            gdImageString(But->GifImage, gdFontMediumBold, ix, iy-15, cstr, black);    
        }
        
        ix = ixq(But->axexmax + 0.05); 
        if (mins < 60) sprintf(cstr, "%02d:%02d", k, mins);
        else {
            k += 1;
            if(k > 23) k -= 24;
            sprintf(cstr, "%02d:00", k);
        }
        gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);
        if(k==0&&j>0) {
            atime =  But->UseLocal? Stime:Stime+LocalSecs;
            Decode_Time( atime + (j+3)*3600, &Time1);
            Time1.Hour = Time1.Min = Time1.Sec = 0;
            Encode_Time( &Time1.Time, &Time1);
            date22 (Time1.Time, c22);
            sprintf(cstr, "%.5s", c22) ;
            gdImageString(But->GifImage, gdFontMediumBold, ix, iy-15, cstr, black);    
        }
    }
    
    iy = 72.0 * YTMargin-45;
    ix = ixq(But->axexmax + 0.05);
    gdImageString(But->GifImage, gdFontMediumBold, ix, iy, LocalTimeID, black);  
    ix = ixq(-0.5);
    if(But->ShowUTC) sprintf(cstr, "UTC") ;
    else         strcpy(cstr, LocalTimeID);
    gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);  
      
    if(But->DClabeled) {
        ix = ixq(But->axexmax + 0.55);
        iy = 72.0 * YTMargin-15;
        sprintf(cstr, "   DC") ;
        gdImageString(But->GifImage, gdFontTiny, ix, iy, cstr, black);
    }
    
    /* Write labels with the Date * 
     ******************************/
    atime =  But->UseLocal? Stime:Stime+LocalSecs;
    iy = 72.0 * YTMargin-30;
    if(!plot_up || But->UseLocal) {
        Decode_Time( atime+60*mins, &Time1);
        Encode_Time( &Time1.Time, &Time1);
        date22 (Time1.Time, c22);
        sprintf(cstr, "%.11s", c22) ;
        gdImageString(But->GifImage, gdFontMediumBold, ixq(But->axexmax + 0.05), iy, cstr, black);    
    }
    
    if(!plot_up || !But->UseLocal || (But->UseLocal && !But->ShowUTC)) {
        atime =  But->ShowUTC? atime-LocalSecs:atime;
        Decode_Time( atime, &Time1);
        Time1.Hour = Time1.Min = Time1.Sec = 0;
        Encode_Time( &Time1.Time, &Time1);
        date22 (Time1.Time, c22);
        sprintf(cstr, "%.11s", c22) ;
        gdImageString(But->GifImage, gdFontMediumBold, ixq(-0.5), iy, cstr, black);    
    }

    /* Write label with the Channel ID * 
     ***********************************/
    ix = ixq(0.0);
    iy = 72.0 * YTMargin-45;
    if(But->Comment[0]!=0) {
        sprintf(cstr, "(%s) ", But->Comment);
        ix = ixq(But->axexmax/2) - 6*(int)strlen(cstr)/2;
        gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);
        iy -= 15;
    }
    sprintf(cstr, "%s ", But->SCNtxt);
    ix = ixq(But->axexmax/2) - 6*(int)strlen(cstr)/2;
    gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);
    
    /* Measure it *
     **************/
    tsize = 1.0/(But->Scaler);
    strcpy(cstr, "");
    if(But->Sens_unit<=1) {
        sprintf(cstr, " = %7.0f microvolts", tsize*But->Sens_gain*1000000.0) ;
    } 
    if(But->Sens_unit==2) {
        sprintf(cstr, " = %f cm/sec", 
                tsize) ;
    } 
    if(But->Sens_unit==3) {
        sprintf(cstr, " = %f cm/sec/sec = %f %%g", 
                tsize, tsize*100.0/978) ;
    } 
    ix = ixq(But->axexmax/2) - 6*(int)strlen(cstr)/2;
    iy = 72.0 * YTMargin-10;
    jj = trace_size*72;
    if((int)strlen(cstr)!=0) {
	    gdImageLine(But->GifImage, ix-2, iy,    ix+2, iy,    black);
	    gdImageLine(But->GifImage, ix-2, iy-jj, ix+2, iy-jj, black);
	    gdImageLine(But->GifImage, ix,   iy,    ix,   iy-jj, black);
	    gdImageString(But->GifImage, gdFontSmall, ix+5, iy-10, cstr, black);  
    } 
    
    strcpy(cstr, "");
    if(But->Sens_unit<=1) {
        sprintf(cstr, " = %7.0f microvolts", tsize*But->Sens_gain*1000000.0) ;
    } 
    if(But->Sens_unit==2) {
        sprintf(cstr, " = %f cm/sec = %7.0f microvolts", 
                tsize, tsize*But->Sens_gain*1000000.0) ;
    } 
    if(But->Sens_unit==3) {
        sprintf(cstr, " = %f cm/sec/sec = %f %%g = %7.0f microvolts.", 
                tsize, tsize*100.0/978, tsize*But->Sens_gain*1000000.0) ;
    } 
    ix = 15;
    iy = But->ysize*72;
    jj = trace_size*72;
    if((int)strlen(cstr)!=0) {
	    gdImageLine(But->GifImage, ix-2, iy,    ix+2, iy,    black);
	    gdImageLine(But->GifImage, ix-2, iy-jj, ix+2, iy-jj, black);
	    gdImageLine(But->GifImage, ix,   iy,    ix,   iy-jj, black);
	    gdImageString(But->GifImage, gdFontSmall, ix+5, iy-10, cstr, black);  
    } 
    
    /* Label for optional clipping */
    if ( But->Clip ) {
        sprintf(cstr, "Traces clipped at plus/minus %d vertical divisions", But->Clip );
        gdImageString(But->GifImage, gdFontTiny, ixq(But->axexmax/2.0 + 1.0), iy-10, cstr, black );
    }
    
    return 0;
}


/********************************************************************
 *    Make_Grid constructs the grid overlayed on the plot.          *
 *                                                                  *
 ********************************************************************/

void Make_Grid(Butler *But)
{
    char    label[25], string[150];
    double  in_sec, tsize;
    long    isec, xp, yp0, yp1, yp2, j, k, black, ixmax;
    long    major_incr, minor_incr, tiny_incr;
    int     inx[]={0,1,2,2,2,3,4,4,4,4,5}, iny[]={2,3,2,1,0,1,1,0,2,3,3};

    /* Plot the frame *
     ******************/
    black = But->gcolor[BLACK];
    gdImageRectangle( But->GifImage, ixq(0.0), iyq(But->axeymax), ixq(But->axexmax),  iyq(0.0),  black);

    /* Make the x-axis ticks * 
     *************************/
    major_incr = 60;
    minor_incr = 10;
    ixmax = But->mins*60;
    in_sec = But->axexmax / ixmax;
    /* Adjust spacing of major and minor ticks. Based on But->axexmax of 10 inches */
    tiny_incr = (in_sec < 0.05)?  0:1;        /* turn on `second' marks */
    if ( in_sec < 0.005 )    {   /* for 1 hour per line */
        major_incr = 300;
        minor_incr = 60;
    }
    else if (in_sec < 0.05 ) { /* for 30 min or less per line */
        major_incr = 60;
        minor_incr = 10;
    } 
    else {      /* for 5 min or less per line with But->axexmax = 20in */
        major_incr = 60;
        minor_incr = 10;
    }
    yp0 = (ysize - YBMARGIN)*72;   /* Bottom axis line */
    yp1 = yp0 + 0.15*72;           /* Space below axis for label */
    for(isec=0;isec<=ixmax;isec++) {
        xp = ixq(isec*in_sec);
        if ((div(isec, major_incr)).rem == 0) {
            tsize = 0.15;    /* major ticks */
            sprintf(string, "%d", isec / 60);
            k = (int)strlen(string) * 3;
            gdImageString(But->GifImage, gdFontMediumBold, xp-k, yp1, string, black);
            gdImageLine(But->GifImage, xp, iyq(0.0), xp, iyq(But->axeymax), But->gcolor[GREY]); /* make the minute mark */
        }
        else if ((div(isec, minor_incr)).rem == 0) 
            tsize = 0.10;    /* minor ticks */
        else if(tiny_incr)
            tsize = 0.05;    /*  tiny ticks */
        else
            tsize = 0.0;     /*  no ticks   */
        
        if(tsize > 0.0) {
            yp2 = yp0 + tsize*72;
            gdImageLine(But->GifImage, xp, yp0, xp, yp2, black); /* make the tick */
        }
    }
    strcpy(label, "TIME (MINUTES)");
    yp1 = yp0 + 0.3*72;           /* Space below axis for label */
    gdImageString(But->GifImage, gdFontMediumBold, ixq(But->axexmax/2.0 - 0.5), yp1, label, black);

    /* Initial it *
     **************/
    j = But->ysize*72 - 5;
    for(k=0;k<11;k++) gdImageSetPixel(But->GifImage, inx[k]+2, j+iny[k], black);
}


/*******************************************************************************
 *    Pallette defines the pallete to be used for plotting.                    *
 *     PALCOLORS colors are defined.                                           *
 *                                                                             *
 *******************************************************************************/

void Pallette(gdImagePtr GIF, long color[])
{
    color[WHITE]  = gdImageColorAllocate(GIF, 255, 255, 255);
    color[BLACK]  = gdImageColorAllocate(GIF, 0,     0,   0);
    color[RED]    = gdImageColorAllocate(GIF, 255,   0,   0);
    color[BLUE]   = gdImageColorAllocate(GIF, 0,     0, 255);
    color[GREEN]  = gdImageColorAllocate(GIF, 0,   105,   0);
    color[GREY]   = gdImageColorAllocate(GIF, 125, 125, 125);
    color[YELLOW] = gdImageColorAllocate(GIF, 125, 125,   0);
    color[TURQ]   = gdImageColorAllocate(GIF, 0,   255, 255);
    color[PURPLE] = gdImageColorAllocate(GIF, 200,   0, 200);    
    
    gdImageColorTransparent(GIF, -1);
}

/*******************************************************************************
 *    Plot_Trace plots an individual trace (Data)  and stuffs it into          *
 *     the GIF image in memory.                                                *
 *                                                                             *
 *******************************************************************************/

int Plot_Trace(Butler *But, double *Data, double Stime)
{
    char    whoami[90], string[30];
    double  x0, x, y, xinc, Middle, samp_pix, atime;
    double  in_sec, xsf, ycenter, sf, mpts, rms, value, trace_size;
    int     lastx, lasty, decimation, acquired, minutes;
    int     i, j, k, ix, iy, LineNumber, mins;
    long    trace_clr;
    TStrct  StartTime;    

    sprintf(whoami, " %s: %s: ", But->mod, "Plot_Trace");
    i = 0;
    mins = But->mins;
    
    atime = Stime + But->LocalTimeOffset - 3600.0*But->CurrentHour;
    Decode_Time( atime, &StartTime);
    
    LineNumber = But->LinesPerHour*StartTime.Hour + StartTime.Min/mins;
    
    trace_size =  But->axeymax/(But->LinesPerHour*But->HoursPerPlot); /* height of one trace [Data] */
    ycenter =  trace_size*LineNumber + 0.5*trace_size;
    if(ycenter > But->axeymax) {
        if(But->Debug) {
            fprintf( stderr, "%s %s. ycenter too big: %f \n", 
                whoami, But->SCNtxt, ycenter);
        }   
        return 1;
    }
    
    sf = trace_size*But->Scaler;
    in_sec     = But->axexmax / (mins*60.0);
    decimation = 2;
    decimation = 1;
    samp_pix = But->samp_sec / in_sec / 72.0;
    decimation = (samp_pix/4 < 1)? 1:samp_pix/4;
    xinc = decimation * in_sec / But->samp_sec;       /* decimation */
    xsf  = in_sec / But->samp_sec;       /* inches/sample */
    Do_Stats(But, Data, But->Npts, Stime);

    /* Plot the trace *
     ******************/
    Decode_Time( Stime, &StartTime);
    minutes = 60.0*StartTime.Hour + StartTime.Min;
    minutes = div(minutes,mins).rem;
    if(minutes==0) {
        Get_Line_Bias(But, 30);
        ix = ixq(But->axexmax + 0.45);  iy = iyq(ycenter) - 3;
        sprintf(string, "%9.0f", But->DCcorr);
        if(But->DClabeled) gdImageString(But->GifImage, gdFontTiny, ix, iy, string, But->gcolor[BLACK]);
        if(But->Debug) {
            fprintf( stderr, "%s %s. DC Correction: %f \n", 
                whoami, But->SCNtxt, But->DCcorr);
        }
    }
    x0 = x = in_sec*60.0*minutes;
    if(But->Debug) {
        fprintf( stderr, "%s %s. minutes: %d x0: %f %d\n", 
            whoami, But->SCNtxt, minutes, x0, But->Npts);
    }   
    
    trace_clr = But->gcolor[BLACK];
    k = div(LineNumber, But->LinesPerHour).rem;
    k = div(k, 4).rem + 1;
    trace_clr = But->gcolor[k];
    Middle = But->DCremoved?  But->DCcorr:0.0;
    acquired = 0;
    for(j=0;j<But->Npts;j+=decimation) {
        x = x0 + j*xsf;
        if (x > But->axexmax) {
            if(But->Debug) {
                fprintf( stderr, "%s %s. x overflow: %f %d %d %d\n", 
                    whoami, But->SCNtxt, x, j, But->Npts, decimation);
            }   
    
            if(j+2*decimation > But->Npts) break;
            LineNumber += 1;
            ycenter += trace_size;
            if (ycenter > But->axeymax) {
                if(But->Debug) {
                    fprintf( stderr, "%s %s. ycenter too big: %f \n", 
                        whoami, But->SCNtxt, ycenter);
                }   
                return 1;
            }
            x = x0 = 0.0;
            k = div(LineNumber, But->LinesPerHour).rem;
            k = div(k, 4).rem + 1;
            trace_clr = But->gcolor[k];
            atime = Stime + (float)j/But->samp_sec;
            Get_Line_Bias(But, 30);
            ix = ixq(But->axexmax + 0.45);  iy = iyq(ycenter) - 3;
            sprintf(string, "%9.0f", But->DCcorr);
            if(But->DClabeled) 
                gdImageString(But->GifImage, gdFontTiny, ix, iy, string, But->gcolor[BLACK]);
            Middle = But->DCremoved?  But->DCcorr:0.0;
            acquired = 0;  
        }
        if(Data[j] != 919191) {
            value = (Data[j] - Middle)/But->sensitivity;  /* convert data to zero-mean cm/s */
            value = value*sf;  /* convert data to inches */
            if(plot_up) value = -value;
            if (But->Clip) {
                if ( value >  But->Clip * trace_size ) 
                     value =  But->Clip * trace_size;
                if ( value < -But->Clip * trace_size )
                     value = -But->Clip * trace_size;
            }
            y = ycenter + value;
            ix = ixq(x);    iy = iyq(y);
            if(acquired) {
                gdImageLine(But->GifImage, ix, iy, lastx, lasty, trace_clr);
            }
            lastx = ix;  lasty = iy;
            acquired = 1;
        }
        else {
            acquired = 0;  
        }
    } 
    return 0;
}


/*******************************************************************************
 *    Do_Stats finds the mean values for an individual trace (Data) for each   *
 *    minute, and stores them in the ring buffer MinMean.                      *
 *                                                                             *
 *******************************************************************************/

void Do_Stats(Butler *But, double *Data, int maxpts, double Stime)
{
    int     i, j, k, kk, npts, MinMeanPtr;
    double  sum;
    TStrct  StartTime;    

    i = 0;
    
    npts = 60*But->samp_sec;
    if(npts  > maxpts) npts  = maxpts;
    
    Decode_Time( Stime, &StartTime);
    MinMeanPtr = div(StartTime.Min, 30).rem;

    for(j=npts;j<maxpts;j+=npts) {
        sum = 0;
        kk = 0;
        for(k=j-npts;k<j;k++) {
            if(Data[k] != 919191.0) {
                sum += Data[k]; 
                kk++;
            }
        }
        But->MinMean[MinMeanPtr] = (kk > 0)? sum/kk:0;
        MinMeanPtr = div(++MinMeanPtr, 30).rem;
    }
    But->MinMeanPtr = MinMeanPtr;
}


/*******************************************************************************
 *    Get_Line_Bias finds the best estimate of the bias to be subtracted from  *
 *    this line's data to center it upon the zero line. This is accomplished   *
 *    by taking a weighted median of the means of the past nmins minutes.      *
 *                                                                             *
 *******************************************************************************/

void Get_Line_Bias(Butler *But, int nmins)
{
    double  ds[2000];
    int     j, k, m, ind;

    ind = div(But->MinMeanPtr, nmins).rem;
    for(j=0;j<2000;j++) ds[j] = But->MinMean[ind];
    m = 0;
    for(j=0;j<nmins;j++) {
        ind = div(j+But->MinMeanPtr, nmins).rem;
        for(k=0;k<j/4+1;k++,m++) ds[m] = But->MinMean[ind];
    }
    
    hpsort(m-2, ds);
    
    But->DCcorr = ds[m/2];
}


/*******************************************************************************
 *    hpsort performs a sifting sort in place of the n members of the double   *
 *    array ra.  This is from Recipes in C, so it is assumed that the array    *
 *    is 1-based as in fortran rather than 0-based as in C!!                   *
 *******************************************************************************/

void hpsort(int n, double ra[])
{
    int   i, j, k, ir;
    double rra;
    
    if(n<2) return;
    k  = (n >> 1)+1;
    ir = n;
        
    for(;;) {
        if(k > 1) rra = ra[--k];
        else {
            rra = ra[ir];
            ra[ir] = ra[1];
            if(--ir == 1) {
                ra[1] = rra;
                break;
            }
        }
        i = k; j = k+k;
        while(j<=ir) {
            if(j<ir && ra[j]<ra[j+1]) j++;
            if(rra<ra[j]) {
                ra[i] = ra[j];
                i = j;
                j <<= 1;
            } else break;
        }
        ra[i] = rra;
    }
}


/*********************************************************************
 *   Make_Date()                                                     *
 *    Expands a string of form YYYYMMDD to DD/MM/YYYY                *
 *********************************************************************/

void Make_Date(char *date)
{
    char    y[5], m[3], d[3];
    int     i;
    
    for(i=0;i<4;i++) y[i] = date[i];
    for(i=0;i<2;i++) m[i] = date[i+4];
    for(i=0;i<2;i++) d[i] = date[i+6];
    
    for(i=0;i<2;i++) date[i] = m[i];
    date[2] = '/';
    for(i=0;i<2;i++) date[i+3] = d[i];
    date[5] = '/';
    for(i=0;i<4;i++) date[i+6] = y[i];
    date[10] = 0;
}    


/*********************************************************************
 *   Save_Plot()                                                     *
 *    Saves the current version of the GIF image and ships it out.   *
 *********************************************************************/

void Save_Plot(Butler *But)
{
    char    tname[175], string[200], whoami[90];
    FILE    *out;
    int     j, ierr, retry;
    
    sprintf(whoami, " %s: %s: ", But->mod, "Save_Plot");
    Make_Grid(But);
    /* Make the GIF file. *
     **********************/        
    sprintf(But->TmpName, "xx.%s", But->SCNnam);
    sprintf(But->GifName, "%s%s", But->TmpName, ".gif");
    sprintf(But->LocalGif, "%s%s.gif", But->GifDir, But->SCNnam);
    sprintf(But->LocalGif, "%s%s.%ld.gif", But->GifDir, But->TmpName, MyPid);
    sprintf(But->LocalGif, "%s%s.%ld", But->target, But->TmpName, MyPid);
    out = fopen(But->LocalGif, "wb");
    if(out == 0L) {
        fprintf( stderr, "%s Unable to write GIF File: %s\n", whoami, But->LocalGif); 
    } else {
        gdImageGif(But->GifImage, out);
        fclose(out);
        if(But->Debug) fprintf( stderr, "%s Writing GIF File: %s To Directory: %s\n", 
                                whoami, But->GifName, But->target);
        sprintf(tname,  "%s%s", But->target, But->GifName );
        if(But->Debug) fprintf( stderr, "%s Renaming GIF File %s -> %s\n", whoami, But->LocalGif, tname);
        ierr = rename(But->LocalGif, tname);
        if(ierr) {
            if(But->Debug) fprintf( stderr, "%s Error Renaming GIF File %d\n", whoami, ierr);
        }
    }
    gdImageDestroy(But->GifImage);
}


/*************************************************************************
 *   Build_Menu ()                                                       *
 *      Builds the waveservers' menus                                    *
 *      Each waveserver has its own menu so that we can do intelligent   *
 *      searches for data.                                               *
 *************************************************************************/

int Build_Menu (Butler *But)
{
    char    whoami[90], server[100];
    int     i, j, retry, ret, rc, got_a_menu;
    WS_PSCN scnp;
    WS_MENU menu; 

    sprintf(whoami, " %s: %s: ", But->mod, "Build_Menu");
    setWsClient_ewDebug(0);  
    if(But->WSDebug) setWsClient_ewDebug(1);  
    got_a_menu = 0;
    
    for(j=0;j<But->nServer;j++) But->index[j] = j;
    
    for (i=0;i< But->nServer; i++) {
        retry = 0;
        But->inmenu[i] = 0;
        sprintf(server, " %s:%s <%s>", But->wsIp[i], But->wsPort[i], But->wsComment[i]);
Append:
        ret = wsAppendMenu(But->wsIp[i], But->wsPort[i], &But->menu_queue[i], But->wsTimeout);
        
        
        if (ret == WS_ERR_NONE) {
            But->inmenu[i] = got_a_menu = 1;
        }
        else if (ret == WS_ERR_INPUT) {
            fprintf( stderr,"%s Connection to %s input error\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_EMPTY_MENU) 
            fprintf( stderr,"%s Unexpected empty menu from %s\n", whoami, server);
        else if (ret == WS_ERR_BUFFER_OVERFLOW) 
            fprintf( stderr,"%s Buffer overflowed for %s\n", whoami, server);
        else if (ret == WS_ERR_MEMORY) 
            fprintf( stderr,"%s Waveserver %s out of memory.\n", whoami, server);
        else if (ret == WS_ERR_PARSE) 
            fprintf( stderr,"%s Parser failed for %s\n", whoami, server);
        else if (ret == WS_ERR_TIMEOUT) {
            fprintf( stderr,"%s Connection to %s timed out during menu.\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_BROKEN_CONNECTION) {
            fprintf( stderr,"%s Connection to %s broke during menu\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_SOCKET) 
            fprintf( stderr,"%s Could not create a socket for %s\n", whoami, server);
        else if (ret == WS_ERR_NO_CONNECTION) { 
     /*       if(But->Debug) */
                fprintf( stderr,"%s Could not get a connection to %s to get menu.\n", whoami, server);
        }
        else fprintf( stderr,"%s Connection to %s returns error: %d\n", whoami, server, ret);
    }
    /* Let's make sure that servers in our server list have really connected.
       **********************************************************************/  
    if(got_a_menu) {
        for(j=0;j<But->nServer;j++) {
            if ( But->inmenu[j]) {
                rc = wsGetServerPSCN( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
                if ( rc == WS_ERR_EMPTY_MENU ) {
                    if(But->Debug) fprintf( stderr,"%s Empty menu for %s:%s <%s> \n", 
                                        whoami, But->wsIp[j], But->wsPort[j], But->wsComment[j]);
                    But->inmenu[j] = 0; 
                    continue;
                }
                if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                    if(But->Debug) fprintf( stderr,"%s  %s:%s <%s> not in menu.\n", 
                                        whoami, But->wsIp[j], But->wsPort[j], But->wsComment[j]);
                    But->inmenu[j] = 0; 
                    continue;
                }
            }
        }
    }
    /* Now, detach 'em and let RequestWave attach only the ones it needs.
       **********************************************************************/  
    for(j=0;j<But->nServer;j++) {
        if(But->inmenu[j]) {
            menu = But->menu_queue[j].head;
            if ( menu->sock > 0 ) {  
                wsDetachServer( menu );
            }
        }
    }
    return got_a_menu;
}


/*************************************************************************
 *   In_Menu_list                                                        *
 *      Determines if the scn is in the waveservers' menu.               *
 *      If there, the tank starttime and endtime are returned.           *
 *      Also, the Server IP# and port are returned.                      *
 *************************************************************************/

int In_Menu_list (Butler *But)
{
    char    whoami[90], server[100];
    int     i, j, rc;
    WS_PSCN scnp;
    
    sprintf(whoami, " %s: %s: ", But->mod, "In_Menu_list");
    i = 0;
    But->nentries = 0;
    for(j=0;j<But->nServer;j++) {
        if(But->inmenu[j]) {
            sprintf(server, " %s:%s <%s>", But->wsIp[j], But->wsPort[j], But->wsComment[j]);
            rc = wsGetServerPSCN( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
            if ( rc == WS_ERR_EMPTY_MENU ) {
                if(But->Debug) fprintf( stderr,"%s Empty menu for %s \n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                if(But->Debug) fprintf( stderr,"%s  %s not in menu.\n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }

            while ( 1 ) {
               if(strcmp(scnp->sta,  But->Site)==0 && 
                  strcmp(scnp->chan, But->Comp)==0 && 
                  strcmp(scnp->net,  But->Net )==0) {
                  But->TStime[j] = scnp->tankStarttime;
                  But->TEtime[j] = scnp->tankEndtime;
                  But->index[But->nentries]  = j;
                  But->nentries += 1;
               }
               if ( scnp->next == NULL )
                  break;
               else
                  scnp = scnp->next;
            }
        }
    }
    if(But->nentries>0) return 1;
    return 0;
}


/********************************************************************
 *  RequestWave                                                     *
 *                                                                  *
 *   k - waveserver index                                           *
 ********************************************************************/
short RequestWave(Butler *But, int k, double *Data, 
            char *Site, char *Comp, char *Net, double Stime, double Duration)
{
    char     *token, server[wsADRLEN*3], whoami[90], SCNtxt[17];
    double   temp;           
    float    samprate, sample;
    long     j;
    int      i, nsamp, nbytes, io, retry;
    int      success, ret, decimate, WSDebug = 0;          
    TRACE_REQ   request;
    WS_MENU  menu = NULL;
    WS_PSCN  pscn = NULL;
    
    decimate = 1;
    
    sprintf(whoami, " %s: %s: ", But->mod, "RequestWave");
    WSDebug = But->WSDebug;
    success = retry = 0;
gettrace:
    strcpy(request.sta,  Site);
    strcpy(request.chan, Comp);
    strcpy(request.net,  Net );
    request.waitSec = 0;
    request.pinno   = 0;
    request.reqStarttime = Stime;
    request.reqEndtime   = request.reqStarttime + Duration;
    request.partial = 1;
    request.pBuf    = But->TraceBuf;
    request.bufLen  = MAXTRACEBUF;
    request.timeout = But->wsTimeout;
    request.fill    = 919191;
    sprintf(SCNtxt, "%s %s %s", Site, Comp, Net);
    
    /*    Put out WaveServer request here and wait for response */
    /* Get the trace
     ***************/
    /* rummage through all the servers we've been told about */
    if ( (wsSearchSCN( &request, &menu, &pscn, &(But->menu_queue[k])  )) == WS_ERR_NONE ) {
        strcpy(server, menu->addr);
        strcat(server, "  ");
        strcat(server, menu->port);
    } else strcpy(server, "unknown");
 
    if(WSDebug) {   
        fprintf( stderr,"\n%s Issuing request to wsGetTraceAscii: server: %s\n", whoami, server);
        fprintf( stderr,"    %s %f %f %d\n", SCNtxt,
             Stime, request.reqEndtime, request.timeout);
        fprintf( stderr," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
    }
    
    io = wsGetTraceAscii(&request, &But->menu_queue[k], But->wsTimeout);
 /*   io = wsGetTraceAsciiDec(&request, &But->menu_queue[k], But->wsTimeout, decimate);   */
    
    if (io == WS_ERR_NONE ) {
        if(WSDebug) {
            fprintf( stderr," %s server: %s trace %s: went ok first time. Got %ld bytes\n",
                whoami, server, SCNtxt, request.actLen); 
            fprintf( stderr,"%s server: %s Return from wsGetTraceAscii: %d\n", whoami, server, io);
            fprintf( stderr,"        actStarttime=%lf, actEndtime=%lf, actLen=%ld, samprate=%lf\n",
                  request.actStarttime, request.actEndtime, request.actLen, request.samprate);
        }
    }
    else {
        switch(io) {
        case WS_ERR_EMPTY_MENU:
        case WS_ERR_SCN_NOT_IN_MENU:
        case WS_ERR_BUFFER_OVERFLOW:
            if (io == WS_ERR_EMPTY_MENU ) 
                fprintf( stderr," %s server: %s No menu found.  We might as well quit.\n", whoami, server); 
            if (io == WS_ERR_SCN_NOT_IN_MENU ) 
                fprintf( stderr," %s server: %s Trace %s not in menu\n", whoami, server, SCNtxt);
            if (io == WS_ERR_BUFFER_OVERFLOW ) 
                fprintf( stderr," %s server: %s Trace %s overflowed buffer. Fatal.\n", whoami, server, SCNtxt); 
            success = 2;                /*   We might as well quit */
            return success;
        
        case WS_ERR_PARSE:
        case WS_ERR_TIMEOUT:
        case WS_ERR_BROKEN_CONNECTION:
        case WS_ERR_NO_CONNECTION:
            retry += 1;
            if (io == WS_ERR_PARSE )
                fprintf( stderr," %s server: %s Trace %s: Couldn't parse server's reply. Try again.\n", 
                        whoami, server, SCNtxt); 
            if (io == WS_ERR_TIMEOUT )
                fprintf( stderr," %s server: %s Trace %s: Timeout to wave server. Try again.\n", whoami, server, SCNtxt); 
            if (io == WS_ERR_BROKEN_CONNECTION ) {
            if(WSDebug) fprintf( stderr," %s server: %s Trace %s: Broken connection to wave server. Try again.\n", 
                    whoami, server, SCNtxt); 
            }
            if (io == WS_ERR_NO_CONNECTION ) {
                if(WSDebug) fprintf( stderr," %s server: %s Trace %s: No connection to wave server.\n", whoami, server, SCNtxt); 
                if(WSDebug) fprintf( stderr," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
            }
            ret = wsAttachServer( menu, But->wsTimeout );
            if(ret == WS_ERR_NONE && retry < But->RetryCount) goto gettrace;
            if(WSDebug) {   
                switch(ret) {
                case WS_ERR_NO_CONNECTION:
                    fprintf( stderr," %s server: %s: No connection to wave server.\n", whoami, server); 
                    break;
                case WS_ERR_SOCKET:
                    fprintf( stderr," %s server: %s: Socket error.\n", whoami, server); 
                    break;
                case WS_ERR_INPUT:
                    fprintf( stderr," %s server: %s: Menu missing.\n", whoami, server); 
                    break;
                default:
                    fprintf( stderr," %s server: %s: wsAttachServer error %d.\n", whoami, server, ret); 
                }
            }
            return 2;                /*   We might as well quit */
        
        case WS_WRN_FLAGGED:
            if((int)strlen(&request.retFlag)>0) {
                if(WSDebug) fprintf( stderr,"%s server: %s Trace %s: return flag from wsGetTraceAscii: <%c>\n %.80s\n", 
                        whoami, server, SCNtxt, request.retFlag, But->TraceBuf);
                if(request.retFlag == 'L') return 3;                /*   We might as well quit */
                if(request.retFlag == 'R') return 3;                /*   We might as well quit */
                if(request.retFlag == 'G') return 3;                /*   We might as well quit */
            }
            fprintf( stderr," %s server: %s Trace %s: No trace available. Wave server returned %c\n", 
                whoami, server, SCNtxt, request.retFlag); 
            return 2;                /*   We might as well quit */
        
        default:
            fprintf( stderr,"%s server: %s Failed.  io = %d\n", whoami, server, io );
        }
    }

    if((int)strlen(&request.retFlag)>0) {
        if(WSDebug) fprintf( stderr,"%s server: %s Trace %s: return flag from wsGetTraceAscii: <%c>\n %.80s\n", 
                whoami, server, SCNtxt, request.retFlag, But->TraceBuf);
        if(request.retFlag == 'L') return 3;                /*   We might as well quit */
        if(request.retFlag == 'R') return 3;                /*   We might as well quit */
        if(request.retFlag == 'G') return 3;                /*   We might as well quit */
    }

    nbytes = request.actLen;
    But->samp_sec = request.samprate<=0? 100:request.samprate;
    samprate = request.samprate;
    
    nsamp = Duration*samprate;
    if(nsamp > MAXTRACELTH) nsamp = MAXTRACELTH;
    strtok(But->TraceBuf, " ,");
    j = 0;
    token = strtok(0L, " ,");
    
    But->Mean = 0.0;
    nsamp = 0;
    while(token!=0L && j<MAXTRACELTH) {
        if(strcmp(token, "919191")==0) {
            Data[j++] = 919191;
        } else {
            sscanf( token, "%f", &sample);
            Data[j] = sample;
            But->Mean += Data[j];
            j += 1;
            nsamp += 1;
        }
        token = strtok(0L, " ,");
    }
    
    if(nsamp == 0)
    	fprintf( stderr,"%s trace %s is zero length! %lf  %f %f\n",
                            whoami, SCNtxt, Duration, temp, request.reqEndtime - request.reqStarttime);
        
    But->Mean = But->Mean/nsamp;
    But->Npts = j;
    temp = j/samprate + 1.0;
    if(temp < Duration) {
        if (WSDebug) 
        	fprintf( stderr,"%s trace %s incomplete: %lf  %f %f\n",
                            whoami, SCNtxt, Duration, temp, request.reqEndtime - request.reqStarttime);
        sleep_ew(200);
        retry += 1;
        if(retry < But->RetryCount) goto gettrace;
    }   
    if (io == WS_ERR_NONE ) success = 1;
/* 	*/   if (But->Debug) {
	    double max, min;
	    max = min = Data[0];
	    for(j=1;j<nsamp;j++) {
	    if(max<Data[j]) max = Data[j];
	    if(min>Data[j]) min = Data[j];
	    }
	    fprintf( stderr, "%s trace %s: %f  %f  %f \n", whoami, SCNtxt, min, But->Mean, max);
    }

/*    if (WSDebug) {
	    fprintf( stderr,"%s trace %s has %d samples.\n", whoami, SCNtxt, j);
    	for(j=0;j<20;j++) fprintf( stderr," %f ", Data[j]);
    	fprintf( stderr," \n");
    	for(j=0;j<20;j++) fprintf( stderr," %f ", Data[But->Npts - 21 + j]);
    	fprintf( stderr," \n");
    }	*/

    return success;
}

/********************************************************************
 *  ReqWaveB                                                        *
 *                                                                  *
 *   k - waveserver index                                           *
 ********************************************************************/
int ReqWaveB(Butler *But, int k, double *Data,  
            char *Site, char *Comp, char *Net, double Stime, double Duration)
{
    char     whoami[90], SCNtxt[17];
    int      i, ret;
    TRACE_REQ   request;
	DATABUF  pTrace;
	GAP *pGap;
    
    sprintf(whoami, " %s: %s: ", But->mod, "ReqWaveB");
    strcpy(request.sta,  Site);
    strcpy(request.chan, Comp);
    strcpy(request.net,  Net );
    request.waitSec = 0;
    request.pinno = 0;
    request.reqStarttime = Stime;
    request.reqEndtime   = request.reqStarttime + Duration;
    request.partial = 1;
    request.pBuf    = But->TraceBuf;
    request.bufLen  = MAXTRACELTH*9;
    request.timeout = But->wsTimeout;
    request.fill    = 919191;
    sprintf(SCNtxt, "%s %s %s", Site, Comp, Net);

	/* Clear out the gap list */
	pTrace.nGaps = 0;
	pTrace.gapList = NULL;

    ret = WSReqBin(But, k, &request, &pTrace, SCNtxt);
    
    But->Npts = pTrace.nRaw;
    if(But->Npts>MAXTRACELTH) {
        logit("e","%s Trace: %s Too many points: %d\n", whoami, SCNtxt, But->Npts);
    	But->Npts = MAXTRACELTH;
    }
    for(i=0;i<But->Npts;i++) Data[i] = pTrace.rawData[i];
    But->Mean = 0.0;
    if(pTrace.nGaps > 500) ret = 4;

	/* Clear out the gap list */
	if(pTrace.nGaps != 0) {
		while ( (pGap = pTrace.gapList) != (GAP *)NULL) {
			pTrace.gapList = pGap->next;
			free(pGap);
		}
	}
	pTrace.nGaps = 0;

    return ret;
}

/********************************************************************
 *  WSReqBin                                                        *
 *                                                                  *
 *   k - waveserver index                                           *
 ********************************************************************/
int WSReqBin(Butler *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt)
{
    char     server[wsADRLEN*3], whoami[90];
    int      i, j, io, retry;
	int      isamp, nsamp, success, ret, WSDebug = 0;          
	long     iEnd, npoints;
    WS_MENU  menu = NULL;
    WS_PSCN  pscn = NULL;
	double   mean, traceEnd, samprate;
	long    *longPtr;
	short   *shortPtr;
    
	TRACE_HEADER *pTH;
    TRACE_HEADER *pTH4;
    char tbuf[MAX_TRACEBUF_SIZ];
	GAP *pGap, *newGap;
    
    sprintf(whoami, " %s: %s: ", But->mod, "WSReqBin");
    WSDebug = But->WSDebug;
    success = retry = 0;
gettrace:
    menu = NULL;
    /*    Put out WaveServer request here and wait for response */
    /* Get the trace
     ***************/
    /* rummage through all the servers we've been told about */
    if ( (wsSearchSCN( request, &menu, &pscn, &But->menu_queue[k]  )) == WS_ERR_NONE ) {
      strcpy(server, menu->addr);
      strcat(server, "  ");
      strcat(server, menu->port);
    } else {
    	strcpy(server, "unknown");
    }
 
    if(WSDebug) {     
        logit("e","\n%s Issuing request to wsGetTraceBinary: server: %s\n", whoami, server);
        logit("e","    %s %f %f %d\n", SCNtxt,
             request->reqStarttime, request->reqEndtime, request->timeout);
        logit("e"," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
    }
    
/* initialize the global trace buffer, freeing old GAP structures. */
	pTrace->nRaw  = 0L;
	pTrace->delta     = 0.0;
	pTrace->starttime = 0.0;
	pTrace->endtime   = 0.0;
	pTrace->nGaps = 0;

	/* Clear out the gap list */
	pTrace->nGaps = 0;
	while ( (pGap = pTrace->gapList) != (GAP *)NULL) {
		pTrace->gapList = pGap->next;
		free(pGap);
	}

    if(WSDebug) {     
        logit("e","\n%s Issuing request to wsGetTraceBin: server: %s\n", whoami, server);
    }
    io = wsGetTraceBin(request, &But->menu_queue[k], But->wsTimeout);
    
    if (io == WS_ERR_NONE ) {
        if(WSDebug) {
            logit("e"," %s server: %s trace %s: went ok first time. Got %ld bytes\n",
                whoami, server, SCNtxt, request->actLen); 
            logit("e","%s server: %s Return from wsGetTraceBin: %d\n", whoami, server, io);
            logit("e","        actStarttime=%lf, actEndtime=%lf, actLen=%ld, samprate=%lf\n",
                  request->actStarttime, request->actEndtime, request->actLen, request->samprate);
        }
    }
    else {
        switch(io) {
        case WS_ERR_EMPTY_MENU:
        case WS_ERR_SCN_NOT_IN_MENU:
        case WS_ERR_BUFFER_OVERFLOW:
            if (io == WS_ERR_EMPTY_MENU ) 
                logit("e"," %s server: %s No menu found.  We might as well quit.\n", whoami, server); 
            if (io == WS_ERR_SCN_NOT_IN_MENU ) 
                logit("e"," %s server: %s Trace %s not in menu\n", whoami, server, SCNtxt);
            if (io == WS_ERR_BUFFER_OVERFLOW ) 
                logit("e"," %s server: %s Trace %s overflowed buffer. Fatal.\n", whoami, server, SCNtxt); 
            success = 2;                /*   We might as well quit */
            return success;
        
        case WS_ERR_PARSE:
        case WS_ERR_TIMEOUT:
        case WS_ERR_BROKEN_CONNECTION:
        case WS_ERR_NO_CONNECTION:
            sleep_ew(500);
            retry += 1;
            if (io == WS_ERR_PARSE )
                logit("e"," %s server: %s Trace %s: Couldn't parse server's reply. Try again.\n", 
                        whoami, server, SCNtxt); 
            if (io == WS_ERR_TIMEOUT )
                logit("e"," %s server: %s Trace %s: Timeout to wave server. Try again.\n", whoami, server, SCNtxt); 
            if (io == WS_ERR_BROKEN_CONNECTION ) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Broken connection to wave server. Try again.\n", 
                whoami, server, SCNtxt); 
            }
            if (io == WS_ERR_NO_CONNECTION ) {
                if(WSDebug || retry>1) 
                    logit("e"," %s server: %s Trace %s: No connection to wave server.\n", whoami, server, SCNtxt); 
                if(WSDebug) logit("e"," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
            }
            ret = wsAttachServer( menu, But->wsTimeout );
            if(ret == WS_ERR_NONE && retry < But->RetryCount) goto gettrace;
            if(WSDebug) {   
                switch(ret) {
                case WS_ERR_NO_CONNECTION:
                    logit("e"," %s server: %s: No connection to wave server.\n", whoami, server); 
                    break;
                case WS_ERR_SOCKET:
                    logit("e"," %s server: %s: Socket error.\n", whoami, server); 
                    break;
                case WS_ERR_INPUT:
                    logit("e"," %s server: %s: Menu missing.\n", whoami, server); 
                    break;
                default:
                    logit("e"," %s server: %s: wsAttachServer error %d.\n", whoami, server, ret); 
                }
            }
            return 2;                /*   We might as well quit */
        
        case WS_WRN_FLAGGED:
            if((int)strlen(&request->retFlag)>0) {
                if(WSDebug) logit("e","%s server: %s Trace %s: return flag from wsGetTraceBin: <%c>\n %.80s\n", 
                        whoami, server, SCNtxt, request->retFlag, But->TraceBuf);
                if(request->retFlag == 'L') return 3;                /*   We might as well quit */
                if(request->retFlag == 'R') return 3;                /*   We might as well quit */
                if(request->retFlag == 'G') return 3;                /*   We might as well quit */
            }
            logit("e"," %s server: %s Trace %s: No trace available. Wave server returned %c\n", 
                whoami, server, SCNtxt, request->retFlag); 
            return 2;                /*   We might as well quit */
        
        default:
            logit( "et","%s server: %s Failed.  io = %d\n", whoami, server, io );
        }
    }
    
    if((int)strlen(&request->retFlag)>0) {
        if(WSDebug) logit("e","%s server: %s Trace %s: return flag from wsGetTraceBin: <%c>\n %.80s\n", 
                whoami, server, SCNtxt, request->retFlag, But->TraceBuf);
        if(request->retFlag == 'L') return 3;                /*   We might as well quit */
        if(request->retFlag == 'R') return 3;                /*   We might as well quit */
        if(request->retFlag == 'G') return 3;                /*   We might as well quit */
    }
      
	/* Transfer trace data from TRACE_BUF packets into Trace buffer */
	traceEnd = (request->actEndtime < request->reqEndtime) ?  request->actEndtime :
															  request->reqEndtime;
	pTH  = (TRACE_HEADER *)request->pBuf;
    pTH4 = (TRACE_HEADER *)tbuf;
	/*
	* Swap to local byte-order. Note that we will be calling this function
	* twice for the first TRACE_BUF packet; this is OK as the second call
	* will have no effect.
	*/

    /**/
	if (WaveMsgMakeLocal(pTH) < 0) {
		logit("et", "%s %s.%s.%s unknown datatype <%s>; skipping\n",
		      whoami, pTH->sta, pTH->chan, pTH->net, pTH->datatype);
		return +1;
	}

if(WSDebug) logit("e"," %s server: %s Trace %s: Data has samprate %g.\n", whoami, server, SCNtxt, pTH->samprate); 
	if (pTH->samprate < 0.1) {
		logit("et", "%s %s.%s.%s has zero samplerate (%g); skipping trace\n",
		      whoami, pTH->sta, pTH->chan, pTH->net, pTH->samprate);
		return +1;
	}
    But->samp_sec = pTH->samprate<=0? 100:pTH->samprate;

	pTrace->delta = 1.0/pTH->samprate;
	samprate = pTH->samprate;   /* Save rate of first packet to compare with later packets */
	pTrace->starttime = request->reqStarttime;
	/* Set Trace endtime so it can be used to test for gap at start of data */
	pTrace->endtime = ( (pTH->starttime < request->reqStarttime) ?
						pTH->starttime : request->reqStarttime) - pTrace->delta ;

  /* Look at all the retrieved TRACE_BUF packets */
	while( pTH < (TRACE_HEADER *)(request->pBuf + request->actLen) ) {
			memcpy( pTH4, pTH, sizeof(TRACE_HEADER) );
            nsamp = pTH4->nsamp;
            if(pTH4->datatype[0] == 'i') SwapInt(&nsamp);
			memcpy( pTH4, pTH, sizeof(TRACE_HEADER) + nsamp*4 );
	     /* Swap bytes to local order */
/**/
		if (WaveMsgMakeLocal(pTH4) < 0) {
			logit("et", "%s %s.%s.%s unknown datatype <%s>; skipping\n",
			    whoami, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
			return +1;
		}
    
		if ( fabs(pTH4->samprate - samprate) > 1.0) {
			logit("et", "%s <%s.%s.%s samplerate change: %f - %f; discarding trace\n",
			    whoami, pTH4->sta, pTH4->chan, pTH4->net, samprate, pTH4->samprate);
			return +1;
		}
    
    /* Check for gap */
		if (pTrace->endtime + 1.5 * pTrace->delta < pTH4->starttime) {
if(WSDebug) logit("e"," %s server: %s Trace %s: Gap detected  .\n", whoami, server, SCNtxt); 
			if ( (newGap = (GAP *)calloc(1, sizeof(GAP))) == (GAP *)NULL) {
				logit("et", "getTraceFromWS: out of memory for GAP struct\n");
				return -1;
			}
			newGap->starttime = pTrace->endtime + pTrace->delta;
			newGap->gapLen = pTH4->starttime - newGap->starttime;
			newGap->firstSamp = pTrace->nRaw;
			newGap->lastSamp  = pTrace->nRaw + (long)( (newGap->gapLen * samprate) - 0.5);
			/* Put GAP struct on list, earliest gap first */
			if (pTrace->gapList == (GAP *)NULL)
				pTrace->gapList = newGap;
			else
				pGap->next = newGap;
			pGap = newGap;  /* leave pGap pointing at the last GAP on the list */
			pTrace->nGaps++;

			/* Advance the Trace pointers past the gap; maybe gap will get filled */
			pTrace->nRaw = newGap->lastSamp + 1;
			pTrace->endtime += newGap->gapLen;
		}
    
		if (pTrace->starttime > pTH4->starttime)
			isamp = (long)( 0.5 + (pTrace->starttime - pTH4->starttime) * samprate);
		else
			isamp = 0;

		if (request->reqEndtime < pTH4->endtime) {
			nsamp = pTH4->nsamp - (long)( 0.5 * (pTH4->endtime - request->reqEndtime) * samprate);
			pTrace->endtime = request->reqEndtime;
		} 
		else {
			nsamp = pTH4->nsamp;
			pTrace->endtime = pTH4->endtime;
		}

    /* Assume trace data is integer valued here, long or short */    
		if (pTH4->datatype[1] == '4') {
			longPtr=(long*) ((char*)pTH4 + sizeof(TRACE_HEADER) + isamp * 4);
			for ( ;isamp < nsamp; isamp++) {
				pTrace->rawData[pTrace->nRaw] = (double) *longPtr;
				longPtr++;
				pTrace->nRaw++;
			}
			/* Advance pTH to the next TRACE_BUF message */
			pTH = (TRACE_HEADER *)((char *)pTH + sizeof(TRACE_HEADER) + pTH4->nsamp * 4);
		}
		else {   /* pTH->datatype[1] == 2, we assume */
			shortPtr=(short*) ((char*)pTH4 + sizeof(TRACE_HEADER) + isamp * 2);
			for ( ;isamp < nsamp; isamp++) {
				pTrace->rawData[pTrace->nRaw] = (double) *shortPtr;
				shortPtr++;
				pTrace->nRaw++;
			}
			/* Advance pTH to the next TRACE_BUF packets */
			pTH = (TRACE_HEADER *)((char *)pTH + sizeof(TRACE_HEADER) + pTH4->nsamp * 2);
		}
	}  /* End of loop over TRACE_BUF packets */
  
  /* Find mean value of non-gap data */
	pGap = pTrace->gapList;
	i = npoints = 0L;
	mean    = 0.0;
  /*
   * Loop over all the data, skipping any gaps. Note that a `gap' will not be declared
   * at the end of the data, so the counter `i' will always get to pTrace->nRaw.
   */
	do {
	    iEnd = (pGap == (GAP *)NULL)? pTrace->nRaw:pGap->firstSamp - 1;
		if (pGap != (GAP *)NULL) { /* Test for gap within peak-search window */
			logit("t", "trace from <%s> has gap in peak-search window\n", SCNtxt);
		}
		for (; i < iEnd; i++) {
			mean += pTrace->rawData[i];
			npoints++;
		}
		if (pGap != (GAP *)NULL) {     /* Move the counter over this gap */    
			i = pGap->lastSamp + 1;
			pGap = pGap->next;
		}
	} while (i < pTrace->nRaw );
  
	mean /= (double)npoints;
  
  /* Now remove the mean, and set points inside gaps to zero */
	i = 0;
	do {
	    iEnd = (pGap == (GAP *)NULL)? pTrace->nRaw:pGap->firstSamp - 1;

		for (; i < iEnd; i++) pTrace->rawData[i] -= mean;

		if (pGap != (GAP *)NULL) {    /* Fill in the gap with zeros */    
			for ( ;i < pGap->lastSamp + 1; i++) pTrace->rawData[i] = 0.0;
			pGap = pGap->next;
		}
	} while (i < pTrace->nRaw );



    if (io == WS_ERR_NONE ) success = 1;

    return success;
}


/**********************************************************************
 * IsDST : Determine if we are using daylight savings time.           *
 *         This is a valid function for US and Canada thru 31/12/2099 *
 *                                                                    *
 *         Modified 02/16/07 to reflect political changes. JHL        *
 *         Fixed 03/05/08 to run correctly. JHL                       *
 *                                                                    *
 **********************************************************************/
int IsDST(int year, int month, int day)
{
    int     i, leapyr, day1, day2, num, jd, jd1, jd2, jd3, jd4;
    int     dpm[] = {31,28,31,30,31,30,31,31,30,31,30,31};
    
    leapyr = 0;
    if((year-4*(year/4))==0 && (year-100*(year/100))!=0) leapyr = 1;
    if((year-400*(year/400))==0) leapyr = 1;
    
    num = ((year-1900)*5)/4 + 5;
    num = num - 7*(num/7);       /* day # of 1 March                  */
    	day1 = num;
    	if(day1>7) day1 = day1 - 7;
    	jd3 = 59 + day1 + leapyr;    /* Julian day of 2nd Sunday in March */
    day1 = num + 2;
    if(day1>7) day1 = day1 - 7;  /* day # (-1) of 1 April             */
    day1 = 8 - day1;             /* date of 1st Sunday in April       */
    jd1 = 90 + day1 + leapyr;    /* Julian day of 1st Sunday in April */
    day2 = num + 3;
    if(day2>7) day2 = day2 - 7;  /* day # (-1) of 1 Oct               */
    day2 = 8 - day2;             /* date of 1st Sunday in Oct         */
    while(day2<=31) day2 += 7;   /* date of 1st Sunday in Nov         */
    	jd4 = 273 + leapyr + day2;   /* Julian day of 1st Sunday in Nov  */
    day2 -= 7;                   /* date of last Sunday in Oct        */
    jd2 = 273 + day2 + leapyr;   /* Julian day of last Sunday in Oct  */
    
    jd = day;
    for(i=0;i<month-1;i++) jd += dpm[i];
    if(month>2) jd += leapyr;
    
    if(jd>=jd1 && jd<jd2) return 1;
    if(jd>=jd3 && jd<jd4) return 1;
    
    return 0;
}


/**********************************************************************
 * Decode_Time : Decode time from seconds since 1970                  *
 *                                                                    *
 **********************************************************************/
void Decode_Time( double secs, TStrct *Time)
{
    struct Greg  g;
    long    minute;
    double  sex;

    Time->Time = secs;
    secs += sec1970;
    Time->Time1600 = secs;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    Time->Year  = g.year;
    Time->Month = g.month;
    Time->Day   = g.day;
    Time->Hour  = g.hour;
    Time->Min   = g.minute;
    Time->Sec   = sex;
}


/**********************************************************************
 * Encode_Time : Encode time to seconds since 1970                    *
 *                                                                    *
 **********************************************************************/
void Encode_Time( double *secs, TStrct *Time)
{
    struct Greg    g;

    g.year   = Time->Year;
    g.month  = Time->Month;
    g.day    = Time->Day;
    g.hour   = Time->Hour;
    g.minute = Time->Min;
    *secs    = 60.0 * (double) julmin(&g) + Time->Sec - sec1970;
}


/**********************************************************************
 * date22 : Calculate 22 char date in the form Jan23,1988 12:34 12.21 *
 *          from the julian seconds.  Remember to leave space for the *
 *          string termination (NUL).                                 *
 **********************************************************************/
void date22( double secs, char *c22)
{
    char *cmo[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    struct Greg  g;
    long    minute;
    double  sex;

    secs += sec1970;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    sprintf(c22, "%3s%2d,%4d %.2d:%.2d:%05.2f",
            cmo[g.month-1], g.day, g.year, g.hour, g.minute, sex);
}


/*************************************************************************
 *   SetPlot sets globals for the current plot.                          *
 *                                                                       *
 *************************************************************************/
void SetPlot(double Xsize, double Ysize)
{
    xsize = Xsize;    
    ysize = Ysize;
}


/*************************************************************************
 *   ixq calculates the x pixel location.                                *
 *   a is the distance in inches from the left margin.                   *
 *************************************************************************/
int ixq(double a)
{
    double   val;
    int      i;
    
    val  = (a + XLMARGIN);
    if(val > xsize) val = xsize;
    if(val < 0.0)   val = 0.0;
    i = val*72;
    return i;
}


/*************************************************************************
 *   iyq calculates the y pixel location.                                *
 *   a is the distance in inches up from the bottom margin.              *
 *                      <or>                                             *
 *   a is the distance in inches down from the top margin.               *
 *************************************************************************/
int iyq(double a)
{
    double   val;
    int      i;
    
    val = plot_up?
         (ysize - YBMARGIN - a) : /* time increases up from bottom */
                  YTMargin + a;   /* times increases down from top */
    
    if(val > ysize) val = ysize;
    if(val < 0.0)   val = 0.0;
    i = val*72;
    return i;
}


/****************************************************************************
 *  Get_Sta_Info(Butler *But);                                              *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Sta_Info(Butler *But)
{
    char    whoami[90], *com, *str, ns, ew;
    int     i, j, k, nfiles, success, type, sensor, units;
    double  dlat, mlat, dlon, mlon, gain, sens, ssens, sitecorr;

    sprintf(whoami, "%s: %s: ", But->mod, "Get_Sta_Info");
    
    ns = 'N';
    ew = 'W';
    But->NSCN = 0;
    for(k=0;k<But->nStaDB;k++) {
	        /* Open the main station file
	         ****************************/
	    nfiles = k_open( But->stationList[k] );
	    if(nfiles == 0) {
	        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, But->stationList[k] );
	        exit( -1 );
	    }

	        /* Process all command files
	         ***************************/
	    while(nfiles > 0) {  /* While there are command files open */
	        while(k_rd())  {      /* Read next line from active file  */
	            com = k_str();         /* Get the first token from line */

	                /* Ignore blank lines & comments
	                 *******************************/
	            if( !com )           continue;
	            if( com[0] == '#' )  continue;

	                /* Open a nested configuration file
	                 **********************************/
	            if( com[0] == '@' ) {
	                success = nfiles+1;
	                nfiles  = k_open(&com[1]);
	                if ( nfiles != success ) {
	                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
	                    exit( -1 );
	                }
	                continue;
	            }

	            /* Process anything else as a channel descriptor
	             ***********************************************/

	            if( But->NSCN >= MAXCHANNELS ) {
	                fprintf(stderr, "%s Too many channel entries in <%s>", 
	                         whoami, But->stationList[k] );
	                fprintf(stderr, "; max=%d; exiting!\n", (int) MAXCHANNELS );
	                exit( -1 );
	            }
	            j = But->NSCN;
	            
				    /* S C N */
	            strncpy( But->Chan[j].Site, com,  6);
	            str = k_str();
				if(str) strncpy( But->Chan[j].Net,  str,  2);
	            str = k_str();
			    if(str) strncpy( But->Chan[j].Comp, str, 3);
			    for(i=0;i<6;i++) if(But->Chan[j].Site[i]==' ') But->Chan[j].Site[i] = 0;
			    for(i=0;i<2;i++) if(But->Chan[j].Net[i]==' ')  But->Chan[j].Net[i]  = 0;
			    for(i=0;i<3;i++) if(But->Chan[j].Comp[i]==' ') But->Chan[j].Comp[i] = 0;
			    But->Chan[j].Comp[3] = But->Chan[j].Net[2] = But->Chan[j].Site[5] = 0;
                if(But->stationListType[k] == 1) {
	                str = k_str();
	                if(str) strncpy( But->Chan[j].Loc, str, 2);
	                for(i=0;i<2;i++) if(But->Chan[j].Loc[i]==' ')  But->Chan[j].Loc[i]  = 0;
	                But->Chan[j].Loc[2] = 0;
                }


                    /* Lat Lon Elev */
                if(But->stationListType[k] == 0) {
	                dlat = k_int();
	                mlat = k_val();
	                
	                dlon = k_int();
	                mlon = k_val();
	                
	                But->Chan[j].Elev = k_val();
	                
	                    /* convert to decimal degrees */
	                if ( dlat < 0 ) dlat = -dlat;
	                if ( dlon < 0 ) dlon = -dlon;
	                But->Chan[j].Lat = dlat + (mlat/60.0);
	                But->Chan[j].Lon = dlon + (mlon/60.0);
	                    /* make south-latitudes and west-longitudes negative */
	                if ( ns=='s' || ns=='S' )               But->Chan[j].Lat = -But->Chan[j].Lat;
	                if ( ew=='w' || ew=='W' || ew==' ' )    But->Chan[j].Lon = -But->Chan[j].Lon;
                } else if(But->stationListType[k] == 1) {
	                But->Chan[j].Lat  = k_val();
	                But->Chan[j].Lon  = k_val();
	                But->Chan[j].Elev = k_val();
                }

         /*       str = k_str();      Blow past the subnet */
                
                But->Chan[j].Inst_type = k_int();
                But->Chan[j].Inst_gain = k_val();
                But->Chan[j].GainFudge = k_val();
                But->Chan[j].Sens_type = k_int();
                But->Chan[j].Sens_unit = k_int();
                But->Chan[j].Sens_gain = k_val();
                But->Chan[j].SiteCorr  = k_val();
                        
                if(But->Chan[j].Sens_unit == 3) But->Chan[j].Sens_gain /= 978.0;
               
                But->Chan[j].sensitivity = (1000000.0*But->Chan[j].Sens_gain/But->Chan[j].Inst_gain)*But->Chan[j].GainFudge*But->Chan[j].SiteCorr;    /*    sensitivity counts/units        */
                
                if(But->stationListType[k] == 1) But->Chan[j].ShkQual = k_int();
	            
				if (k_err()) {
				    fprintf( stderr, "%s Error decoding line in station file\n%s\n  exiting!\n", whoami, k_get() );
				    exit( -1 );
				}
	     /*>Comment<*/
	            str = k_str();
	            if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].SiteName, str );
				    
	            str = k_str();
	            if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].Descript, str );
				    
	        	But->NSCN++;
	        }
	        nfiles = k_close();
	    }
    }
}


/*************************************************************************
 *  Put_Sta_Info(Butler *But);                                           *
 *  Retrieve all the information available about station i               *
 *  and put it into an internal structure for reference.                 *
 *  This should eventually be a call to the database; for now we must    *
 *  supply an ascii file with all the info.                              *
 *************************************************************************/
int Put_Sta_Info(Butler *But)
{
    char    whoami[90];
    short   j;

    sprintf(whoami, " %s: %s: ", But->mod, "Put_Sta_Info");
    
    for(j=0;j<But->NSCN;j++) {
        if(strcmp(But->Chan[j].Site, But->Site )==0 && 
           strcmp(But->Chan[j].Comp, But->Comp)==0 && 
           strcmp(But->Chan[j].Net,  But->Net )==0) {
            strcpy(But->Comment, But->Chan[j].SiteName);
            But->Inst_type  = But->Chan[j].Inst_type;
            But->Sens_type  = But->Chan[j].Sens_type;
            But->Sens_gain  = But->Chan[j].Sens_gain;
            But->Sens_unit  = But->Chan[j].Sens_unit;
            But->sensitivity  = But->Chan[j].sensitivity;
		    if(But->Sens_unit<=1) But->Scaler = But->Scale*1000.0;
		    if(But->Sens_unit==2) But->Scaler = But->Scale*10000.0;
		    if(But->Sens_unit==3) But->Scaler = But->Scale*10.0;
	        if(But->Debug) {
	            fprintf( stderr, "\n%s %s. %d %d %d %f %f %f %s\n\n", 
	                whoami, But->SCNtxt, But->Inst_type, But->Sens_type, 
	                But->Sens_unit, But->Sens_gain, But->sensitivity, 
	                But->Scale, But->Comment);
	        }   
            return 1;
        }
    }
    strcpy(But->Comment, "");
    But->Inst_type  = 0;
    But->Sens_type  = 0;
    But->Sens_gain  = 1.0;
    But->Sens_unit  = 0;
    But->sensitivity  = 1000000.0;
    But->Scaler = But->Scale*1000.0;
        if(But->Debug) {
            fprintf( stderr, "\n%s %s. %d %d %d %f %f %f %s\n\n", 
                whoami, But->SCNtxt, But->Inst_type, But->Sens_type, 
                But->Sens_unit, But->Sens_gain, But->sensitivity, 
                But->Scale, But->Comment);
        }   
    return 0;
}


/****************************************************************************
 *      config_me() process command file using kom.c functions              *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void config_me( char* configfile, Butler *But )
{
    char    whoami[90], *com, *str;
    char    init[20];       /* init flags, one byte for each required command */
    int     ncommand;       /* # of required commands you expect              */
    int     nmiss;          /* number of required commands that were missed   */
    int     i, j, n, nfiles, success, dclabels, secsPerGulp, nPlots;
    double  val;
    FILE    *in;
    gdImagePtr    im_in;

    sprintf(whoami, " %s: %s: ", But->mod, "config_me");
        /* Set one init flag to zero for each required command
         *****************************************************/
    ncommand = 5;
    for(i=0; i<ncommand; i++ )  init[i] = 0;
    But->Debug = But->WSDebug = But->logo = But->logox = But->logoy = 0;
    But->nServer = But->nStaDB = 0;
    But->SaveDrifts = 0;
    But->DaysAgo = 0;
    But->UseDST = 0;
    secsPerGulp = 0;
    But->RetryCount = 2;
    plot_up = 0;
    But->Clip = 0;
    But->DClabeled = 0;
    But->wsTimeout = 50000;

        /* Open the main configuration file
         **********************************/
    nfiles = k_open( configfile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, configfile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

                /* Process anything else as a command
                 ************************************/
            
         /* wave server addresses and port numbers to get trace snippets from
          *******************************************************************/
/*0*/          
            else if( k_its("WaveServer") ) {
                if ( But->nServer >= MAX_WAVESERVERS ) {
                    fprintf( stderr, "%s Too many <WaveServer> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_WAVESERVERS );
                    return;
                }
                if( (long)(str=k_str()) != 0 )  strcpy(But->wsIp[But->nServer],str);
                if( (long)(str=k_str()) != 0 )  strcpy(But->wsPort[But->nServer],str);
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy(But->wsComment[But->nServer],str);
                But->nServer++;
                init[0]=1;
            }

        /* get Gif directory path/name
        *****************************/
/*1*/
            else if( k_its("GifDir") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. Gif directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                if(str) strcpy( But->GifDir , str );
                init[1] = 1;
            }


          /* get display parameters
        ************************************/
/*2*/
            else if( k_its("Display") ) {
                But->DCcorr = But->Mean = 0.0;
         /*>01<*/
                But->HoursPerPlot = k_int();   /*  # of hours per gif image */
                if(But->HoursPerPlot <  1) But->HoursPerPlot =  1;
                if(But->HoursPerPlot > 24) But->HoursPerPlot = 24;
                But->HoursPerPlot  = 24/(24/But->HoursPerPlot);
         /*>02<*/
                But->mins = k_int();            /* # of minutes/line to display */
                if(But->mins < 1 || But->mins > 60) But->mins = 15;
                But->mins  = 60/(60/But->mins);
                
         /*>03<*/
                But->OldData = k_int();        /* Number of previous hours to retrieve */
                if(But->OldData < 0 || But->OldData > 168) But->OldData = 0;
                
         /*>04<*/
                val = k_val();            /* x-size of data plot in pixels */
                But->xpix = (val >= 100.0)? val:72.0*val;
                
         /*>05<*/
                val = k_val();            /* y-size of data plot in pixels */
                But->ypix = (val >= 100.0)? val:72.0*val;
                
         /*>06<*/
                But->ShowUTC  = k_int();    /* Flag to show UTC time */
                
         /*>07<*/
                But->UseLocal = k_int();    /* Flag to reference plot to local midnight */
         /*>08<*/
                But->LocalTime = k_int();    /* Hour offset of local time from UTC */
                if(But->LocalTime < -24 || But->LocalTime > 24) {
                    But->LocalTime = 0;
                }
                But->LocalSecs = But->LocalTime*3600.0;
         /*>09<*/
                str = k_str();                           /* Local Time ID e.g. PST */
                strncpy(But->LocalTimeID, str, (size_t)3);
                But->LocalTimeID[3] = '\0';
         /*>10<*/
                But->DCremoved = k_int();   /* DCremoved */
                
                But->LinesPerHour = 60/But->mins;
                init[2] = 1;
            }

          /* get the target directory(s)
        ************************************/
            else if( k_its("Target") ) {
                if( (long)(str=k_str()) != 0 )  {
                    n = strlen(str);   /* Make sure directory name has proper ending! */
                    if( str[n-1] != '/' ) strcat(str, "/");
                    strcpy(But->target, str);
                }
                init[3] = 1;
            }

          /* get SCN parameters
        ************************************/
/*4*/
            else if( k_its("SCN") ) {
                if( But->Nchans >= MAXPLOTS ) {
                    fprintf( stderr, "%s Too many <SCN> commands in <%s>", whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAXPLOTS );
                    return;
                }
                nPlots = But->Nchans;
                
         /*>Site<*/
                str = k_str();
                if(str) strcpy( But->Disp[nPlots].Site , str );
         /*>Comp<*/
                str = k_str();
                if(str) strcpy( But->Disp[nPlots].Comp , str );
         /*>Net<*/
                str = k_str();
                if(str) strcpy( But->Disp[nPlots].Net , str );
                
                sprintf(But->Disp[nPlots].SCNnam, "%s_%s_%s", But->Disp[nPlots].Site, But->Disp[nPlots].Comp, But->Disp[nPlots].Net);
                sprintf(But->Disp[nPlots].SCNtxt, "%s %s %s", But->Disp[nPlots].Site, But->Disp[nPlots].Comp, But->Disp[nPlots].Net);
                sprintf(But->Disp[nPlots].SCN,    "%s%s%s",   But->Disp[nPlots].Site, But->Disp[nPlots].Comp, But->Disp[nPlots].Net);
                
                But->Nchans++;
                init[4] = 1;
            }



               /* optional commands */

                /* get station list path/name
                *****************************/
/*9*/
            else if( k_its("StationList") ) {
                if ( But->nStaDB >= MAX_STADBS ) {
                    fprintf( stderr, "%s Too many <StationList> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_STADBS );
                    return;
                }
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->stationList[But->nStaDB] , str );
                But->stationListType[But->nStaDB] = 0;
                But->nStaDB++;
            }
            else if( k_its("StationList1") ) {
                if ( But->nStaDB >= MAX_STADBS ) {
                    fprintf( stderr, "%s Too many <StationList> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_STADBS );
                    return;
                }
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->stationList[But->nStaDB] , str );
                But->stationListType[But->nStaDB] = 1;
                But->nStaDB++;
            }


                /* get menu list path/name
                *****************************/
/*9*/
            else if( k_its("MenuList") ) {
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Menu list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->MenuList , str );
        /*        init[9] = 1;   */
            }


            else if( k_its("LabelDC") )        But->DClabeled = 1;      /* optional command */

            else if( k_its("NoLabelDC") )      But->DClabeled = 0;      /* optional command */

            else if( k_its("Debug") )          But->Debug = 1;   /* optional commands */

            else if( k_its("WSDebug") )        But->WSDebug = 1;   /* optional commands */

            else if( k_its("SaveDrifts") )     But->SaveDrifts = 1;   /* optional commands */
  
            else if( k_its("UseDST") )         But->UseDST = 1;         /* optional command */
      
            else if( k_its("PlotDown") )       plot_up = 0;      /* optional command */

            else if( k_its("PlotUp") )         plot_up = 1;      /* optional command */

            else if( k_its("Clip") )           But->Clip = k_int();   /* optional command */

            else if( k_its("RetryCount") ) {  /*optional command*/
                But->RetryCount = k_int();
                if(But->RetryCount > 20) But->RetryCount = 20; 
                if(But->RetryCount <  2) But->RetryCount =  2; 
            }

            else if( k_its("Gulp") ) {  /*optional command*/
                secsPerGulp = k_int();
                if(secsPerGulp>1000) secsPerGulp = 1000; 
                if(secsPerGulp<  1)  secsPerGulp =   0; 
            }

            else if( k_its("Logo") ) {  /*optional command*/
                str = k_str();
                if(str) {
                    strcpy( But->logoname, str );
                    But->logo = 1;
                }
            }

                /* At this point we give up. Unknown thing.
                *******************************************/
            else {
                fprintf(stderr, "%s <%s> Unknown command in <%s>.\n",
                         whoami, com, configfile );
                continue;
            }

                /* See if there were any errors processing the command
                 *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "%s Bad <%s> command  in <%s>; exiting!\n",
                             whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

        /* After all files are closed, check init flags for missed commands
         ******************************************************************/
    nmiss = 0;
    for(i=0;i<ncommand;i++)  if( !init[i] ) nmiss++;
    if ( nmiss ) {
        fprintf( stderr, "%s ERROR, no ", whoami);
        if ( !init[0] )  fprintf( stderr, "<WaveServer> " );
        if ( !init[1] )  fprintf( stderr, "<GifDir> "     );
        if ( !init[2] )  fprintf( stderr, "<Display> "    );
        if ( !init[3] )  fprintf( stderr, "<Target> "     );
        if ( !init[4] )  fprintf( stderr, "<SCN> "        );
        fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
        exit( -1 );
    }
    YTMargin = YTMARGIN;
    if(But->logo) {
        strcpy(str, But->logoname);
        strcpy(But->logoname, But->GifDir);
        strcat(But->logoname, str);
        in = fopen(But->logoname, "rb");
        if(in) {
            im_in = gdImageCreateFromGif(in);
            fclose(in);
            But->logox = im_in->sx;
            But->logoy = im_in->sy;
            YTMargin += im_in->sy/72.0 + 0.1;
            gdImageDestroy(im_in);
        }
    }
    But->axexmax = But->xpix/72.0;
    But->axeymax = But->ypix/72.0;
    But->xsize = But->axexmax + XLMARGIN + XRMARGIN;
    But->ysize = But->axeymax + YBMARGIN + YTMargin;
    But->secsPerGulp = But->secsPerStep = 60*(60/(60/MAXMINUTES));
    j = (secsPerGulp <= 0)? MAXMINUTES*60 : secsPerGulp;
    j = (MAXMINUTES*60 < j)? MAXMINUTES*60 : j;
    But->secsPerGulp = But->secsPerStep = j;
    
}


