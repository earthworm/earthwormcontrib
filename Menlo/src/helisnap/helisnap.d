########################################################################
# This is the helisnap parameter file. 
# This module creates helicorder displays.
#
#                                             11/4/2003    Jim Luetgert
########################################################################

# List of wave servers (ip port comment) to contact to retrieve trace data.
@/home/earthworm/run/params/all_waveservers.d

# Directory in which to store the temporary .gif, .link and .html files.
 GifDir   /home/picker/gifs/heli2go/  

# Directory in which to store the final .gif files.
 Target   /home/picker/sendfiler/netstat/  
# Target   /home/picker/gifs/nssetup/  

# The file with all the station information.
 StationList1     /home/earthworm/run/params/calsta.db1

# Plot Display Parameters - 
		# The following is designed such that each SCN creates it's own
		# helicorder display; one per panel of data.
		
# 01 HoursPerPlot    Total number of hours per gif image
# 02 Minutes/Line    Number of minutes per line of trace
# 03 Plot Previous   On startup, retrieve and plot at least n previous hours from tank.
# 04 XSize           Overall size of plot in inches
# 05 YSize           Setting these > 100 will imply pixels
# 06 Show UTC        UTC will be shown on one of the time axes.
# 07 Use Local       The day page will be local day rather than UTC day.
# 08 Local Time Diff UTC - Local.  e.g. -7 -> PDT; -8 -> PST
# 09 Local Time Zone Three character time zone name.
# 10 Mean Removal    Mean of 1st minute of each line will be removed.
#                                      
#        01  02 03 04  05 06 07   08   09  10   

 Display  1  15  1 10  10  1  0   -8  PST   1    

@/home/picker/gifs/nssetup/heli_chans

    # *** Optional Commands ***

 LabelDC    * label each trace line with its dc offset.
#NoLabelDC  * Don'tlabel each trace line with its dc offset [default].

 RetryCount    2  # Number of attempts to get a trace from server; default=2
 Logo    smusgs.gif   # Name of logo in GifDir to be plotted on each image

# SaveDrifts   # Log drifts to GIF directory.

# Clip  20     # Clip the traces at N vertical divisions.

# PlotUp     # Plot from bottom to top.
  PlotDown   # Plot from top to bottom. [default]
  
  UseDST

# We accept a command "Debug" which turns on a bunch of log messages
# Debug
# WSDebug
