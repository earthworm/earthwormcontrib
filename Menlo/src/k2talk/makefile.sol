#
#                     Make file for mss
#                      Solaris Version
#
#  The posix4 library is required for nanaosleep.
#
O = mss.o socket_sol.o config.o sleep_sol.o log.o tzset_sol.o

mss: $O
	cc -o mss $O -lm -lsocket -lnsl -lposix4
