
       /****************************************************
        *                      k2talk                      *
        *  Interactive communication program for K2's      *
        *  connected to an MSS100.                         *
        *                                                  *
        *  Usage: k2talk [IP address] [port]               *
        *                                                  *
        *  Default IP address is 130.118.43.40             *
        *  Default port is 3001                            *
        ****************************************************/

#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include "k2talk.h"

void SocketSysInit( void );
int  ConnectToMSS( void );
int  SendBlockToSocket( char *, int );
int  GetBlockFromSocket( char *, int, int * );
void CloseSocketConnection( void );

char ServerIP[20];                  /* IP address of system to receive msg's */
int  ServerPort = 3001;             /* The well-known port number */

int main( int argc, char *argv[] )
{
   int LogFile = 1;                 /* If 1, log to disk */

/* Get command line arguments
   **************************/
   strcpy( ServerIP, "130.118.43.40" );           /* Default value */
   if ( argc > 1 ) strcpy( ServerIP, argv[1] );   /* Optional argument */
   if ( argc > 2 ) ServerPort = atoi( argv[2] );  /* Optional argument */

/* Initialize the socket system
   ****************************/
   SocketSysInit();

/* Connect to the MSS100
   *********************/
   if ( ConnectToMSS() == _FAILURE )
   {
      printf( "Can't connect to MSS100. Exiting.\n" );
      return -1;
   }
   printf( "Connected to IP %s port %d\n", ServerIP, ServerPort );
   printf( "You may enter K2 commands now.\n" );
   printf( "To exit program, enter Control-C.\n\n" );

/* Loop to get commands from the keyboard
   and send them to the K2
   **************************************/
   while ( 1 )
   {
      const int HeartbeatInterval = 0;         /* In seconds */

/* Send heartbeats to the MSS100 custom firmware.
   To turn off this feature, set HeartbeatInterval=0.
   *************************************************/
      if ( HeartbeatInterval > 0 )
      {
         time_t        now = time( 0 );        /* Current time */
         static time_t tPrevHeart = 0;         /* Time of previous heartbeat */

         if ( (now - tPrevHeart) >= HeartbeatInterval )
         {
            if ( SendBlockToSocket( "#HEARTBEAT", 10 ) == _FAILURE )
            {
               printf( "Error sending heartbeat to MSS100. Exiting.\n" );
               CloseSocketConnection();
               return 0;
            }
            printf( "Heartbeat sent to MSS100.\n" );
            tPrevHeart = now;
         }
      }

/* If any characters are available from the keyboard,
   grab them using getch() and send them to the K2.
   Don't print these characters.  They will be
   printed later when the K2 echos them back.
   *************************************************/
      while ( _kbhit() )
      {
         char ch = _getch();

         if ( SendBlockToSocket( &ch, sizeof(char) ) == _FAILURE )
         {
            printf( "Error sending bytes to MSS100. Exiting.\n" );
            CloseSocketConnection();
            return 0;
         }
      }

/* Get bytes from K2 and print them on console terminal
   ****************************************************/
      {
         int i;
         int nBytesReceived;
         static char buf[256];

         if ( GetBlockFromSocket( buf, sizeof(buf), &nBytesReceived ) == _FAILURE )
         {
            printf( "Error getting bytes from MSS100.\n" );
            printf( "Closing and reopening socket connection.\n" );

            CloseSocketConnection();
            while ( ConnectToMSS() == _FAILURE )
               printf( "Can't connect to MSS100. Retrying...\n" );

            printf( "Socket connection to MSS100 reopened.\n" );
            nBytesReceived = 0;
         }

         for ( i = 0; i < nBytesReceived; i++ )
            putchar( buf[i] );
      }

      sleep_ew( 50 );
   }

/* Finish up with this file
   ************************/
   CloseSocketConnection();
   return 0;
}
