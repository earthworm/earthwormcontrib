
   /*************************************************************
    *                       socket_win.c                        *
    *                                                           *
    *  This file contains functions that are specific to        *
    *  Windows.                                                 *
    *************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <earthworm.h>
#include "k2talk.h"

static SOCKET sd;                      /* Socket identifier */


      /********************** SocketSysInit **********************
       *               Initialize the socket system              *
       *         We are using Windows socket version 2.2.        *
       ***********************************************************/
void SocketSysInit( void )
{
   WSADATA Data;
   int     status = WSAStartup( MAKEWORD(2,2), &Data );
   if ( status != 0 )
   {
      printf( "WSAStartup failed. Exiting.\n" );
      exit( -1 );
   }
   return;
}


       /*********************************************************
        *                CloseSocketConnection()                *
        *********************************************************/
void CloseSocketConnection( void )
{
   closesocket( sd );
   return;
}


       /************************************************************
        *                       ConnectToMSS()                     *
        *                                                          *
        *          Get a connection to the MSS100.                 *
        ************************************************************/

int ConnectToMSS( void )
{
   extern char ServerIP[20];     /* IP address of system to receive msg */
   extern int  ServerPort;       /* The well-known port number */
   struct sockaddr_in server;    /* Server socket address structure */
   const int optVal = 1;
   unsigned long address;

/* Get a new socket descriptor
   ***************************/
   sd = socket( AF_INET, SOCK_STREAM, 0 );
   if ( sd == INVALID_SOCKET )
   {
      printf( "socket() error: %d\n", WSAGetLastError() );
      return _FAILURE;
   }

/* Allow reuse of socket addresses
   *******************************/
// if ( setsockopt( sd, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
//                  sizeof(int) ) == SOCKET_ERROR )
// {
//    printf( "setsockopt() error: %d\n", WSAGetLastError() );
//    CloseSocketConnection();
//    return _FAILURE;
// }

/* Fill in socket address structure
   ********************************/
   address = inet_addr( ServerIP );
   if ( address == INADDR_NONE )
   {
      printf( "Bad server IP address: %s  Exiting.\n", ServerIP );
      CloseSocketConnection();
      return _FAILURE;
   }
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family = AF_INET;
   server.sin_port   = htons( (unsigned short)ServerPort );
   server.sin_addr.S_un.S_addr = address;

/* Connect to the server.
   The connect call may block.
   **************************/
// printf( "Connecting to %s port %d\n", ServerIP, ServerPort );
   if ( connect( sd, (struct sockaddr *)&server, sizeof(server) )
        == SOCKET_ERROR )
   {
      int rc = WSAGetLastError();
      if ( rc == WSAECONNREFUSED )
         printf( "Connection refused.\n" );
      else if ( rc == WSAETIMEDOUT )
         printf( "Connection timed out.\n" );
      else
         printf( "connect() error: %d\n", rc );
      CloseSocketConnection();
      return _FAILURE;
   }
   return _SUCCESS;
}


  /*********************************************************************
   *                              Send_all()                           *
   *                                                                   *
   *  Sends the first nbytes characters in buf to the socket sock.     *
   *                                                                   *
   *  Returns 0 if all ok.                                             *
   *  Returns -1 if connection reset by peer or connection aborted.    *
   *  Returns -2 if an error or occurred.                              *
   *********************************************************************/

int Send_all( int sock, char buf[], int nbytes )
{
   int nBytesToWrite = nbytes;
   int nwritten      = 0;

   while ( nBytesToWrite > 0 )
   {
      int rc = send( sock, &buf[nwritten], nBytesToWrite, 0 );

      if ( rc == SOCKET_ERROR )
      {
         int winError = WSAGetLastError();

         if ( winError == WSAECONNRESET )
         {
            printf( "send() error: Connection reset by peer.\n" );
            return -1;
         }
         else if ( winError == WSAECONNABORTED )
         {
            printf( "send() error: Connection aborted.\n" );
            return -1;
         }
         else
         {
            printf( "send() error: %d\n", winError );
            return -2;
         }
      }
      if ( rc == 0 )     /* Shouldn't see this */
      {
         printf( "Error: send()== 0\n" );
         return -2;
      }
      nwritten += rc;
      nBytesToWrite -= rc;
   }
   return 0;
}


       /****************************************************************
        *                      SendBlockToSocket()                     *
        *                                                              *
        *  Send a block of data to the remote system.                  *
        ****************************************************************/

int SendBlockToSocket( char *buf, int blockSize )
{
   while ( 1 )
   {
      int rc = Send_all( sd, buf, blockSize );

      if ( rc ==  0 )
      {
//       int sockerr;
//       int optlen = sizeof(int);
//       if ( getsockopt( sd, SOL_SOCKET, SO_ERROR, (char *)&sockerr,
//            &optlen ) == SOCKET_ERROR )
//       {
//          int winError = WSAGetLastError();
//          printf( "getsockopt() error: %d", winError );
//          return _FAILURE;
//       }
//       printf( "getsockopt() returns sockerr = %d\n", sockerr );
         return _SUCCESS;
      }
      if ( rc == -2 ) return _FAILURE;

      if ( rc == -1 )
      {
         printf( "Closing socket connection.\n" );
         CloseSocketConnection();

         printf( "Reestablishing socket connection.\n" );
         while ( ConnectToMSS() == _FAILURE )
            printf( "Can't connect to MSS100. Retrying...\n" );

         printf( "Socket connection reestablished.\n" );
      }
   }
}


       /****************************************************************
        *                      GetBlockFromSocket()                    *
        *                                                              *
        *  Get a block of data from the remote system.                 *
        ****************************************************************/

int GetBlockFromSocket( char *buf, int bufSize, int *nBytesReceived )
{
   int    selrc;
   fd_set readfds;
   struct timeval selectTimeout;

/* See if the socket is readable
   *****************************/
   selectTimeout.tv_sec  = 0;     /* Do not wait for timeout */
   selectTimeout.tv_usec = 0;
   FD_ZERO( &readfds );
   FD_SET( sd, &readfds );
   selrc = select( sd+1, &readfds, 0, 0, &selectTimeout );
   if ( selrc == SOCKET_ERROR )
   {
      printf( "select() error: %d", WSAGetLastError() );
      return _FAILURE;
   }
   if ( selrc == 0 )            /* No readable events occurred on socket */
   {
      *nBytesReceived = 0;
      return _SUCCESS;
   }
   if ( FD_ISSET( sd, &readfds ) )    /* A readable event occurred */
   {
      int rc = recv( sd, buf, bufSize, 0 );

/* We got some bytes
   *****************/
      if ( rc > 0 )
      {
         *nBytesReceived = rc;
         return _SUCCESS;
      }

/* The MSS100 closed the socket connection
   ***************************************/
      else if ( rc == 0 )
      {
         printf( "The MSS100 closed the socket connection.\n" );
         return _FAILURE;
      }

/* Error detected by recv()
   ************************/
      else if ( rc == SOCKET_ERROR )
      {
         int winError = WSAGetLastError();
         printf( "recv() error: %d", winError );
         return _FAILURE;
      }

      else              /* We shouldn't see this */
      {
         printf( "Unknown return code from recv(): %d", rc );
         return _FAILURE;
      }
   }

   else                 /* We shouldn't see this */
   {
      printf( "GetBlockFromSocket(): A nonreadable event occurred (?????).\n" );
      return _FAILURE;
   }

   printf( "Unknown return from select(). selrc: %d\n", selrc );
   return _FAILURE;
}

