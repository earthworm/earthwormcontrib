#
# This is the udmap parameter file. This module gets site location
# information, and creates index maps and html.
 

# Directory in which to store the .gif files for output
  LocalTarget   /home/picker/sendfiler/heli/

# The file with all the station information.
 infile     /home/picker/sendfiler/heli.sta

# index map path on webserver
  MapOnWeb	/waveforms/helicorder/index_map.gif

# index base map
  IndexBaseMap	 /home/picker/perl_stuff/Maps0/index_map.gif0
  IndexMap	     index_map.gif

# Station base map
  StationBaseMap	 /home/picker/perl_stuff/Maps0/Station_map.gif0
  StationMap	     Station_map.gif

# top, left corner of "All Stations" button
  Button     62  225
  
# Index map bounds in degrees.
#             left  right bottom  top
  IndexDeg   126.0  113.8   32.4 42.2
# Corresponding pixel bounds.
#             left  right bottom  top
  IndexPix     10    396    390   5

# We accept a command "Debug" which turns on a bunch of log messages
  Debug
