/*************************************************************************
 *      udmap.c:                                                         *
 * On a time schedule (scheduler or crontab), reads station location     *
 * info from a file and plots sites onto GIF basemaps for use on the web.*
 * These files are then transferred to webserver(s).                     *
 *                                                                       *
 *                                                                       *
 *                                                                       *
 * Jim Luetgert    07/23/02                                              *
 *************************************************************************/

#include <platform.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <kom.h>

#include "gd.h"
#include "gdfontt.h"   /*  6pt      */
#include "gdfonts.h"   /*  7pt      */
#include "gdfontmb.h"  /*  9pt Bold */
#include "gdfontl.h"   /* 13pt      */
#include "gdfontg.h"   /* 10pt Bold */

/**************************************************************************
 *  defines                                                               *
 **************************************************************************/

#define MAXCHANNELS     2500  /* Maximum number of channels               */
#define MAXCOLORS         20  /* Number of colors defined                 */
#define MAX_TARGETS       10  /* largest number of targets                */
#define GDIRSZ           132  /* Size of string for GIF target directory  */
#define STALIST_SIZ      100  /* Size of string for station list file     */

#define WHITE  0
#define BLACK  1
#define RED    2
#define BLUE   3
#define GREEN  4
#define GREY   5
#define YELLOW 6
#define TURQ   7
#define PURPLE 8

/*********************************************************************
    Define the structure for Channel information.
 *********************************************************************/
typedef struct ChanInfo {      /* A channel information structure         */
    char    Site[6];           /* Site                                    */
    char    Comp[5];           /* Component                               */
    char    Net[5];            /* Net                                     */
    char    Loc[5];            /* Loc                                     */
    char    SCNnam[20];        /* S_C_N                                   */
    char    SiteName[90];      /* Common Name of Site                     */
    double  Lat;               /* Latitude                                */
    double  Lon;               /* Longitude                               */
    double  Elev;              /* Elevation                               */
    int     hour;              /* Label position as clock hour            */
} ChanInfo;

/* Functions in this source file
 *******************************/
void add_data_to_base_map (char *BaseMapFile, char *OutputMapFile);
void initial_tgd_stuff (char *GifFile);
void plot_data_on_index_map(void);
void get_data_clr(char *comp, long *clr);
void get_data_symbol(int xpix, int ypix, int *xlft, int *xrgt, int *ytop, int *ybot); 
void get_label_loc(int hour, int xpix, int ypix, int *xname, int *yname);
void Pallette(gdImagePtr GIF, long color[]);
void update_html_on_index_map(void);
void Get_Sta_Info(void);
void config_me( char *); /* reads config (.d) file via Carl's routines  */

/* Things to read from configuration file
 ****************************************/
static int  Debug = 1;               /* debug flag                      */

int    butlft, buttop, butrgt, butbot; /* button coordinates            */
int    lftpix_ind, toppix_ind, rgtpix_ind, botpix_ind;
double lftindexdeg, topindexdeg, rgtindexdeg, botindexdeg;
int    halfhtpix = 3;                /* Half the symbol size            */

/* Other globals
 ***************/
char      module[50];

char       infile[STALIST_SIZ];      /* the file containing site info         */
ChanInfo   Chan[MAXCHANNELS];
int        NumData;
char       IndexBaseMap[GDIRSZ];     /* input ${Maps0}/${IndexBase}.gif0      */
char       IndexMap[GDIRSZ];         /* output ${OutDir}/${Indexmap}.gif      */
char       StaBaseMap[GDIRSZ];       /* input ${Maps0}/${StationBase}.gif0    */
char       StationMap[GDIRSZ];       /* output ${OutDir}/${StationMap}.gif    */
char       MapFile[GDIRSZ];          /* output ${OutDir}/select.map           */
char       MapOnWeb[GDIRSZ];         /* output ${Indexmapdir}/${Indexmap}.gif */
gdImagePtr im_in, im_out;
long       gcolor[MAXCOLORS];        /* GIF colors                            */
int        nltargets;                /* Number of local target directories    */
char       loctarget[MAX_TARGETS][GDIRSZ];/* Target in form-> /directory/     */
char       GifDir[GDIRSZ];    /* Directory for storage of .gif & .html on local machine */

/*************************************************************************
 *  main( int argc, char **argv )                                        *
 *************************************************************************/

main( int argc, char **argv )
{
    int   i;
    char  outfile[200], s[200];
    time_t  now;
    struct tm *date;
    
    /* Check command line arguments
     ******************************/
    if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", "udmap");
        exit( 0 );
    }
    
    strcpy(module, argv[1]);
    for(i=0;i<(int)strlen(module);i++) if(module[i]=='.') module[i] = 0;

    /* Read the configuration file(s)
     ********************************/
    config_me( argv[1] );
    
    time(&now);
    date = localtime(&now);
    strftime(s,80,"%c",date);
    fprintf(stdout, "%s\n", s);

    if(Debug) fprintf( stdout, "Getting station info. \n");
    Get_Sta_Info();
    if(Debug) {
        fprintf( stdout, "Station info: \n");
        for(i=0;i<NumData;i++) {
            fprintf( stdout, "%s %s %s %s %f %f %f %d\n", 
            Chan[i].Site, Chan[i].Comp, Chan[i].Net, Chan[i].Loc, Chan[i].Lat, Chan[i].Lon, 
            Chan[i].Elev, Chan[i].hour);
        }
    }
    
    for(i=0;i<nltargets;i++) {
    /* This is the index map of stations with links and buttons.
    ***********************************************************/
        if(Debug) fprintf( stdout, "Building files for number %d of %d directories. \n", i+1, nltargets);
        sprintf(outfile, "%s%s", loctarget[i], IndexMap);
        if(Debug) fprintf( stdout, "Adding data to %s. -> %s \n", IndexBaseMap, outfile);
        add_data_to_base_map(IndexBaseMap, outfile);

        sprintf(MapFile, "%s%s", loctarget[i], "select.map");
        if(Debug) fprintf( stdout, "Making html. \n");
        update_html_on_index_map();

    /* This is the simple map of stations without links and buttons.
    ***************************************************************/
        sprintf(outfile, "%s%s", loctarget[i], StationMap);
        if(Debug) fprintf( stdout, "Adding data to %s. -> %s \n", StaBaseMap, outfile);
        add_data_to_base_map(StaBaseMap, outfile);
    }
    
}


/**************************************************************************
 *  add_data_to_base_map - plots data on station_map .gif basemap         *
 **************************************************************************/
void add_data_to_base_map (char *BaseMapFile, char *OutputMapFile)
{
    FILE    *out;

    initial_tgd_stuff(BaseMapFile);

/* Send data location info to command file.
 ******************************************/
    plot_data_on_index_map();

    /* Make the GIF file. *
     **********************/        
    out = fopen(OutputMapFile, "wb");
    if(out == 0L) {
        fprintf( stdout, "%s: %s:  Unable to write GIF File: %s\n", 
                module, "add_data_to_base_map", OutputMapFile); 
        exit(-1);
    } else {
        gdImageGif(im_out, out);
        fclose(out);
    }
    gdImageDestroy(im_out);
}


/**************************************************************************
 *  Initialize the Gif Image stuff.                                       *
 **************************************************************************/
void initial_tgd_stuff (char *GifFile)
{
    FILE    *in;

    in = fopen(GifFile, "rb");
    if(in) {
        im_out = gdImageCreateFromGif(in);
        fclose(in);
        if(im_out) {
            Pallette(im_out, gcolor);
        }
    }
}

/**************************************************************************
 *  Reads the array of data and plots them in colors.                     *
 **************************************************************************/
void plot_data_on_index_map(void) 
{
    int     i, xpix, ypix, xname, yname, xlft, xrgt, ytop, ybot;
    long    black, clr;
    double  xpixperdeg, ypixperdeg;

/*# Define degrees to pixel scale factors: */
   xpixperdeg = (rgtpix_ind - lftpix_ind)/(lftindexdeg - rgtindexdeg);
   ypixperdeg = (botpix_ind - toppix_ind)/(topindexdeg - botindexdeg);
   /*
	if(Debug) fprintf( stdout, "xpixperdeg: %f rgtpix_ind: %d lftpix_ind: %d lftindexdeg: %f rgtindexdeg: %f  \n", 
	           xpixperdeg, rgtpix_ind, lftpix_ind, lftindexdeg, rgtindexdeg );
	if(Debug) fprintf( stdout, "ypixperdeg: %f botpix_ind: %d toppix_ind: %d topindexdeg: %f botindexdeg: %f  \n", 
	           ypixperdeg, botpix_ind, toppix_ind, topindexdeg, botindexdeg );
	*/

/*# Loop Through Data
  # ----------------- */
    for(i=0;i<NumData;i++) {
/*        # Find pixel location of site on index map. */
        xpix = (int)(lftpix_ind + xpixperdeg*(lftindexdeg - Chan[i].Lon) + 0.5);
        ypix = (int)(toppix_ind + ypixperdeg*(topindexdeg - Chan[i].Lat) + 0.5);
   /*
	if(Debug) fprintf( stdout, "xpix: %d xpixperdeg: %f lftpix_ind: %d lftindexdeg: %f Chan[i].Lon: %f  \n", 
	           xpix, xpixperdeg, lftpix_ind, lftindexdeg, Chan[i].Lon );
	if(Debug) fprintf( stdout, "ypix: %d ypixperdeg: %f toppix_ind: %d topindexdeg: %f Chan[i].Lat: %f  \n\n", 
	           ypix, ypixperdeg, toppix_ind, topindexdeg, Chan[i].Lat );
	*/

/*        # Assign color based on comp value. */
        get_data_clr(Chan[i].Comp, &clr);

/*        # Scale and locate square plot symbol. */
        get_data_symbol(xpix, ypix, &xlft, &xrgt, &ytop, &ybot);

        black = gcolor[BLACK];
        gdImageFilledRectangle(im_out, xlft, ytop, xrgt, ybot,  clr);
        gdImageRectangle(im_out, xlft, ytop, xrgt, ybot,  black);

/*        # Locate label.    */
        get_label_loc(Chan[i].hour, xpix, ypix, &xname, &yname);

        gdImageString(im_out, gdFontMediumBold, xname, yname, Chan[i].Site, black);

    }
}

/**************************************************************************
 *  Get the color for the site symbol based on comp code.                 *
 **************************************************************************/
void get_data_clr(char *comp, long *clr) 
{
       if     (strcmp(comp, "VHZ")==0) {*clr = gcolor[BLACK];}
       else if(strcmp(comp, "SHZ")==0) {*clr = gcolor[BLACK];}
       else if(strcmp(comp, "VLZ")==0) {*clr = gcolor[BLACK];}
       else if(strcmp(comp, "SLZ")==0) {*clr = gcolor[BLACK];}
       else if(strcmp(comp, "VLE")==0) {*clr = gcolor[WHITE];}
       else if(strcmp(comp, "EHZ")==0) {*clr = gcolor[RED];}
       else if(strcmp(comp, "VDZ")==0) {*clr = gcolor[RED];}
       else if(strcmp(comp, "VDE")==0) {*clr = gcolor[RED];}
       else if(strcmp(comp, "VDN")==0) {*clr = gcolor[RED];}
       else if(strcmp(comp, "BHZ")==0) {*clr = gcolor[BLUE];}
       else if(strcmp(comp, "HHZ")==0) {*clr = gcolor[BLUE];}
       else if(strcmp(comp, "HNZ")==0) {*clr = gcolor[RED];}
       else if(strcmp(comp, "HNE")==0) {*clr = gcolor[RED];}
       else if(strcmp(comp, "HNN")==0) {*clr = gcolor[RED];}
       else                            {*clr = gcolor[YELLOW];}

}



/**************************************************************************
 *  Get the box bounds for the site symbol based on global halfhtpix.     *
 **************************************************************************/
void get_data_symbol(int xpix, int ypix, int *xlft, int *xrgt, int *ytop, int *ybot) 
{
       *xlft = xpix - halfhtpix;
       *xrgt = xpix + halfhtpix;
       *ytop = ypix - halfhtpix;
       *ybot = ypix + halfhtpix;

}

/**************************************************************************
 *  Get the proper location for the site label.                           *
 **************************************************************************/
void get_label_loc(int hour, int xpix, int ypix, int *xname, int *yname) 
{
    int  xl, yl, xoff, yoff;
    
/*    # Locate center of label from center of symbol in pixels.  */

    if(hour <=  0) {xl =   0; yl = -10;}
    if(hour ==  1) {xl =   9; yl =  -8;}
    if(hour ==  2) {xl =  18; yl =  -5;}
    if(hour ==  3) {xl =  18; yl =   0;}
    if(hour ==  4) {xl =  18; yl =   5;}
    if(hour ==  5) {xl =   9; yl =   8;}
    if(hour ==  6) {xl =   0; yl =  10;}
    if(hour ==  7) {xl =  -9; yl =   8;}
    if(hour ==  8) {xl = -18; yl =   5;}
    if(hour ==  9) {xl = -18; yl =   0;}
    if(hour == 10) {xl = -18; yl =  -5;}
    if(hour == 11) {xl =  -9; yl =  -8;}
    if(hour >= 12) {xl =   0; yl = -10;}

    xoff = -13;
    yoff =  -5;
    *xname = xpix + xl + xoff;
    *yname = ypix + yl + yoff;

}


/********************************************************************
 *    update_html_on_index_map constructs the html (select.map)     *
 *    for mapping the points on the map to other links.             *
 *                                                                  *
 ********************************************************************/

void update_html_on_index_map(void)
 {
    char    whoami[190], linkfile[190];
    int     i, pixwidth_ind0, pixheight_ind0, xpix, ypix;
    double  xpixperdeg, ypixperdeg;
    FILE    *in;
    FILE    *out;
    gdImagePtr    im_in;

    sprintf(whoami, " %s: %s: ", module, "update_html_on_index_map");


/*   # Open the index template map to get its dimensions.  */
    in = fopen(IndexBaseMap, "rb");
    if(in) {
        im_in = gdImageCreateFromGif(in);
        fclose(in);
        pixwidth_ind0 = im_in->sx;
        pixheight_ind0 = im_in->sy;
        gdImageDestroy(im_in);
    }
    else { 
        fprintf( stdout, "%s Unable to Open the index template map to get its dimensions: %s\n", whoami, IndexBaseMap); 
        exit(-1);
    }

/*    # Open server side imagemap .map file for data points.   */
    out = fopen(MapFile, "wb");
    if(out == 0L) {
        fprintf( stdout, "%s Unable to write File: %s\n", whoami, MapFile); 
        exit(-1);
    }
    
    fprintf(out, " <center> \n");
    fprintf(out, "     <img src=%s ", MapOnWeb);
    fprintf(out, " width=%d height=%d  ", pixwidth_ind0, pixheight_ind0);
    fprintf(out, " border=\"2\" usemap=\"#Click Station Map\"><BR> \n");
    fprintf(out, " </center> \n");
    
    fprintf(out, " <P><map name=\"Click Station Map\"> \n");
    fprintf(out, " <area shape=\"rect\" coords=\"%d,%d,%d,%d\" href=\"#\" \n", butlft, buttop, butrgt, butbot);
    fprintf(out, " onMouseOver=\"statusBar('All Stations (Clickable Thumbnails)'); return true;\" \n");
    fprintf(out, " onMouseOut=\"window.status=''; return true;\"  \n");
    fprintf(out, " onClick=\"buildLink('99'); return false;\" alt=\"All Stations\"> \n");
    fprintf(out, "  \n");

/*# Reads the array of data and plots them in colors. */

/*# Define degrees to pixel scale factors: */
   xpixperdeg = (rgtpix_ind - lftpix_ind)/(lftindexdeg - rgtindexdeg);
   ypixperdeg = (botpix_ind - toppix_ind)/(topindexdeg - botindexdeg);
           
/*# Loop Through Data
  # ----------------- */
    for(i=0;i<NumData;i++) {
/*        # Find pixel location of site on index map. */
        xpix = (int)(lftpix_ind + xpixperdeg*(lftindexdeg - Chan[i].Lon) + 0.5);
        ypix = (int)(toppix_ind + ypixperdeg*(topindexdeg - Chan[i].Lat) + 0.5);

        fprintf(out, " <area shape=\"rect\" coords=\"%d,%d,%d,%d\" href=\"#\" \n",
                xpix-halfhtpix, ypix-halfhtpix, xpix+halfhtpix, ypix+halfhtpix);
        fprintf(out, " onMouseOver=\"statusBar('Station %s'); return true;\" \n", Chan[i].Site);
        fprintf(out, " onMouseOut=\"window.status=''; return true;\"  \n");
        sprintf(linkfile, "%s_%s_%s_%s_00", Chan[i].Site, Chan[i].Comp, Chan[i].Net, Chan[i].Loc);
        fprintf(out, " onClick=\"buildLink('%s'); return false;\" alt=\"Station %s\"> \n \n", linkfile, Chan[i].Site);
    }
    fprintf(out, " </map> \n");
     
    fclose(out);

}

/*******************************************************************************
 *    Pallette defines the pallete to be used for plotting.                    *
 *     PALCOLORS colors are defined.                                           *
 *                                                                             *
 *******************************************************************************/

void Pallette(gdImagePtr GIF, long color[])
{
    color[WHITE]  = gdImageColorAllocate(GIF, 255, 255, 255);
    color[BLACK]  = gdImageColorAllocate(GIF, 0,     0,   0);
    color[RED]    = gdImageColorAllocate(GIF, 255,   0,   0);
    color[BLUE]   = gdImageColorAllocate(GIF, 0,     0, 255);
    color[GREEN]  = gdImageColorAllocate(GIF, 0,   105,   0);
    color[GREY]   = gdImageColorAllocate(GIF, 125, 125, 125);
    color[YELLOW] = gdImageColorAllocate(GIF, 125, 125,   0);
    color[TURQ]   = gdImageColorAllocate(GIF, 0,   255, 255);
    color[PURPLE] = gdImageColorAllocate(GIF, 200,   0, 200);    
    
    gdImageColorTransparent(GIF, -1);
}

/****************************************************************************
 *  Get_Sta_Info( );                                                        *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Sta_Info(void)
{
    char    whoami[150], *com, *str, ns, ew;
    int     i, j, nfiles;

    sprintf(whoami, "%s: %s: ", module, "Get_Sta_Info");
    
    ns = 'N';
    ew = 'W';
    NumData = 0;
    
        /* Open the main station file
         ****************************/
    nfiles = k_open( infile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, infile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

            /* Process anything else as a channel descriptor
             ***********************************************/

            if( NumData >= MAXCHANNELS ) {
                fprintf(stderr, "%s Too many channel entries in <%s>", whoami, infile );
                fprintf(stderr, "; max=%d; exiting!\n", (int) MAXCHANNELS );
                exit( -1 );
            }
            j = NumData;
            
                /* S C N */
            strncpy( Chan[j].Site, com,  6);
            str = k_str();
            if(str) strncpy( Chan[j].Comp, str, 3);
            str = k_str();
            if(str) strncpy( Chan[j].Net,  str,  2);
            str = k_str();
            if(str) strncpy( Chan[j].Loc,  str,  2);
            for(i=0;i<6;i++) if(Chan[j].Site[i]==' ') Chan[j].Site[i] = 0;
            for(i=0;i<2;i++) if(Chan[j].Net[i]==' ')  Chan[j].Net[i]  = 0;
            for(i=0;i<3;i++) if(Chan[j].Comp[i]==' ') Chan[j].Comp[i] = 0;
            Chan[j].Comp[3] = Chan[j].Net[2] = Chan[j].Loc[2] = Chan[j].Site[5] = 0;


                /* Lat Lon Elev */
            Chan[j].Lat = k_val();
            Chan[j].Lon = k_val();
            Chan[j].Lon = -Chan[j].Lon;
            Chan[j].Elev = k_val();
                        
            Chan[j].hour   = k_int();
           
     /*>Comment<*/
            str = k_str();
            if( (long)(str) != 0 && str[0]!='#')  strcpy( Chan[j].SiteName, str );
                
            NumData++;
        }
        nfiles = k_close();
    }
}


/****************************************************************************
 *      config_me() process command file using kom.c functions              *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void config_me( char* configfile )
{
    char    whoami[150], *com, *str;
    char    init[20];       /* init flags, one byte for each required command */
    int     ncommand;       /* # of required commands you expect              */
    int     nmiss;          /* number of required commands that were missed   */
    int     i, n, nfiles, success;

    sprintf(whoami, " %s: %s: ", module, "config_me");
        /* Set one init flag to zero for each required command
         *****************************************************/
    ncommand = 10;
    for(i=0; i<ncommand; i++ )  init[i] = 0;
    nltargets = 0;
    Debug = 0;

        /* Open the main configuration file
         **********************************/
    nfiles = k_open( configfile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, configfile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

                /* Process anything else as a command
                 ************************************/

        /* get the local target directory(s)
        ************************************/
/*0*/
            if( k_its("LocalTarget") ) {
                if ( nltargets >= MAX_TARGETS ) {
                    fprintf( stderr, "%s Too many <LocalTarget> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_TARGETS );
                    return;
                }
                if( (long)(str=k_str()) != 0 )  {
                    n = strlen(str);   /* Make sure directory name has proper ending! */
                    if( str[n-1] != '/' ) strcat(str, "/");
                    strcpy(loctarget[nltargets], str);
                }
                nltargets += 1;
                init[0] = 1;
            }

                /* get station list path/name
                *****************************/
/*1*/
            else if( k_its("infile") ) {
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( infile , str );
                init[1] = 1;
            }

        /* get MapOnWeb file path/name
        *****************************/
/*2*/
            else if( k_its("MapOnWeb") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. MapOnWeb file name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                if(str) strcpy( MapOnWeb , str );
                init[2] = 1;
            }

        /* get IndexBaseMap file path/name
        *****************************/
/*3*/
            else if( k_its("IndexBaseMap") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. IndexBaseMap file name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                if(str) strcpy( IndexBaseMap , str );
                init[3] = 1;
            }

        /* get IndexMap file name
        *****************************/
/*4*/
            else if( k_its("IndexMap") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. IndexMap file name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                if(str) strcpy( IndexMap , str );
                init[4] = 1;
            }

        /* get IndexBaseMap file path/name
        *****************************/
/*5*/
            else if( k_its("StationBaseMap") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. StationBaseMap file name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                if(str) strcpy( StaBaseMap , str );
                init[5] = 1;
            }

        /* get IndexMap file name
        *****************************/
/*6*/
            else if( k_its("StationMap") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. StationMap file name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                if(str) strcpy( StationMap , str );
                init[6] = 1;
            }

        /* get IndexMap file name
        *****************************/
/*7*/
            else if( k_its("Button") ) {
                buttop = k_int();
                butlft = k_int();
                butrgt = butlft + 80;
                butbot = buttop + 20;
                init[7] = 1;
            }

        /* get bounds of IndexMap in degrees
        *****************************/
/*8*/
            else if( k_its("IndexDeg") ) {
                lftindexdeg = k_val();
                rgtindexdeg = k_val();
                botindexdeg = k_val();
                topindexdeg = k_val();
                init[8] = 1;
            }

        /* get bounds of IndexMap in pixels
        *****************************/
/*9*/
            else if( k_its("IndexPix") ) {
                lftpix_ind = k_int();
                rgtpix_ind = k_int();
                botpix_ind = k_int();
                toppix_ind = k_int();
                init[9] = 1;
            }

            else if( k_its("Debug") )          Debug = 1;   /* optional commands */


                /* At this point we give up. Unknown thing.
                *******************************************/
            else {
                fprintf(stderr, "%s <%s> Unknown command in <%s>.\n",
                         whoami, com, configfile );
                continue;
            }

                /* See if there were any errors processing the command
                 *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "%s Bad <%s> command  in <%s>; exiting!\n",
                             whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

        /* After all files are closed, check init flags for missed commands
         ******************************************************************/
    nmiss = 0;
    for(i=0;i<ncommand;i++)  if( !init[i] ) nmiss++;
    if ( nmiss ) {
        fprintf( stderr, "%s ERROR, no ", whoami);
        if ( !init[0] )  fprintf( stderr, "<LocalTarget> "    );
        if ( !init[1] )  fprintf( stderr, "<infile> "   );
        if ( !init[2] )  fprintf( stderr, "<MapOnWeb> "     );
        if ( !init[3] )  fprintf( stderr, "<IndexBaseMap> " );
        if ( !init[4] )  fprintf( stderr, "<IndexMap> "   );
        if ( !init[5] )  fprintf( stderr, "<StationBaseMap> "       );
        if ( !init[6] )  fprintf( stderr, "<StationMap> "         );
        if ( !init[7] )  fprintf( stderr, "<Button> "         );
        if ( !init[8] )  fprintf( stderr, "<IndexDeg> "         );
        if ( !init[9] )  fprintf( stderr, "<IndexPix> "         );
        fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
        exit( -1 );
    }
 }


