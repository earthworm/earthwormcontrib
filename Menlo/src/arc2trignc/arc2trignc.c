
   /**************************************************************************
    *                              arc2trignc.c                              *
    *                                                                        *
    *  Takes a hyp2000 arc message as input and produces a triglist message  *
    *  as output.  Similar to program arc2trig, but this version uses        *
    *  subnets.  Written by W Kohler Jan 2002.                               *
    **************************************************************************/

/*
Program modified to ignore events with Md = 0. WMK 3/2/04
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include "arc2trignc.h"

/* Function prototypes
   *******************/
int  GetConfig( char * );                                    /* In config.c */
void LogConfig( void );                                      /* In config.c */
int  LookUpParams( void );                                   /* In config.c */
void SendStatus( unsigned char, short, char * );             /* In sendstatus.c */
void LogLongString( char * );                                /* In logstring.c */
int  CountSubnets( int * );                                  /* In subnet.c */
int  ReadSubnets( SUBNET *, int );                           /* In subnet.c */
void LogSubnets( SUBNET *, int );                            /* In subnet.c */
int  ProcessArc( char *, char *,
                 MSG_LOGO, MSG_LOGO, SUBNET *, int );        /* In arc.c */

/* Share these with sendstatus.c
   *****************************/
SHM_INFO        OutRegion;                 /* Shared memory region for output */
pid_t           MyPid;                     /* Process id is sent with heartbeat */

/* Parameters declared in config.c
   *******************************/
extern MSG_LOGO GetLogo[MAXLOGO];          /* Array for module,type,instid */
extern short    nLogo;                     /* Number of logos in GetLogo[] */
extern char     InRingName[MAX_RING_STR];  /* Name of transport ring for input */
extern char     OutRingName[MAX_RING_STR]; /* Name of transport ring for output */
extern int      LogTrig;                   /* If 1, log trigger messages */
extern long     HeartBeatInterval;         /* Seconds between heartbeats */

/* Parameters from earthworm.h
   ***************************/
extern long          InRingKey;            /* Key of transport ring for input */
extern long          OutRingKey;           /* Key of transport ring for output */
extern unsigned char InstId;               /* Local installation id */
extern unsigned char MyModId;              /* Module Id for this program */
extern unsigned char TypeHeartBeat;        /* Message type code of statmgr heartbeat */
extern unsigned char TypeError;            /* Message type code of statmgr error */
extern unsigned char TypeTrigList;         /* Message type code of triglist msg */


int main( int argc, char *argv[] )
{
   time_t    timeNow;               /* Current time */
   time_t    timeLastHb;            /* Time last heartbeat was sent */
   long      arcMsgSize;            /* Size of incoming arc message */
   MSG_LOGO  inLogo;                /* Logo of incoming arc message */
   MSG_LOGO  outLogo;               /* Logo of outgoing trig message */
   char      *arcMsg;               /* Arc message buffer */
   char      *trigMsg;              /* Trigger message buffer */
   SHM_INFO  inRegion;              /* Shared memory region for input  */
   char      text[150];             /* Buffer for status messages */
   int       nSubnet;               /* Number of subnets in subnet file */
   SUBNET    *subnet;               /* Array of subnets */
   const int LogFile = 1;           /* Argument to logit_init() */

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      printf( "Usage: arc2trignc <configfile>\n" );
      return -1;
   }

/* Initialize/open log file.
   Second argument of logit_init() is ignored.
   ******************************************/
   logit_init( argv[1], 0, MAXLOGITSTR, LogFile );
   logit( "t" , "Read configuration file <%s>\n", argv[1] );

/* Read and log the configuration file
   ***********************************/
   if ( GetConfig( argv[1] ) == -1 )
   {
      logit( "e", "arc2trignc: GetConfig() error. Exiting.\n" );
      return -1;
   }
   LogConfig();

/* Get parameters from earthworm.h
   *******************************/
   if ( LookUpParams() == -1 )
   {
      logit( "e", "arc2trignc: LookUpParams() error. Exiting.\n" );
      return -1;
   }

/* Set up the logo of outgoing triglist messages
   *********************************************/
   outLogo.instid = InstId;
   outLogo.mod    = MyModId;
   outLogo.type   = TypeTrigList;

/* Allocate arc and triglist message buffers
   *****************************************/
   logit( "t", "Max bytes per arc msg (MAX_BYTES_PER_EQ): %d\n", MAX_BYTES_PER_EQ );
   arcMsg = (char *) malloc( MAX_BYTES_PER_EQ );
   if ( arcMsg == NULL )
   {
      logit( "e", "arc2trignc: Can't allocate arc message buffer. Exiting.\n" );
      return -1;
   }
   logit( "t" , "Allocated arc message buffer.\n" );

   logit( "t", "Max bytes per trig msg (MAX_TRIG_BYTES): %d\n", MAX_TRIG_BYTES );
   trigMsg = (char *) malloc( MAX_TRIG_BYTES );
   if ( trigMsg == NULL )
   {
      logit( "e", "arc2trignc: Can't allocate triglist message buffer. Exiting.\n" );
      return -1;
   }
   logit( "t" , "Allocated triglist message buffer.\n" );

/* Count subnets and allocate subnet array.
   Read subnet file into subnet array.  Log subnets.
   ************************************************/
   if ( CountSubnets( &nSubnet ) == -1 )
   {
      logit( "e", "arc2trignc: Error counting subnets. Exiting.\n" );
      return -1;
   }
   logit( "t", "Number of subnets in subnet file: %d\n", nSubnet );

   subnet = (SUBNET *) calloc( nSubnet, sizeof(SUBNET) );
   if ( subnet == NULL )
   {
      logit( "e", "arc2trignc: Can't allocate subnet array. Exiting.\n" );
      return -1;
   }
   logit( "t" , "Allocated subnet array.\n" );

   if ( ReadSubnets( subnet, nSubnet ) == -1 )
   {
      logit( "e", "arc2trignc: Error reading subnet file. Exiting.\n" );
      return -1;
   }
   logit( "t", "Read subnets from file into subnet array.\n" );

   LogSubnets( subnet, nSubnet );


/* Get my own process ID for restart purposes
   ******************************************/
   MyPid = getpid ();
   if ( MyPid == -1 )
   {
      logit( "e", "arc2trignc: getpid() failed. Exiting.\n");
      return -1;
   }

/* Attach to input transport ring
   ******************************/
   tport_attach( &inRegion, InRingKey );
   logit( "t", "Attached to input shared memory region: %s\n", InRingName );

/* Attach to output transport ring
   *******************************/
   tport_attach( &OutRegion, OutRingKey );
   logit( "t", "Attached to output shared memory region: %s\n", OutRingName );

/* Send a heartbeat in first pass through main loop
   ************************************************/
   timeLastHb = time(&timeNow) - (time_t)HeartBeatInterval - 1;

/* Flush the input transport ring
   ******************************/
   while ( tport_getmsg( &inRegion, GetLogo, nLogo, &inLogo, &arcMsgSize,
                         arcMsg, MAX_BYTES_PER_EQ-1) != GET_NONE ) ;

/* Start main loop
   ***************/
   while ( 1 )
   {

/* See if statmgr wants us to terminate
   ************************************/
      int termFlag = tport_getflag( &inRegion );
      if ( termFlag == TERMINATE ) break;
      if ( termFlag == MyPid     ) break;

/* Send heartbeat to statmgr
   *************************/
      if ( (time(&timeNow) - timeLastHb) >= (time_t)HeartBeatInterval )
      {
         timeLastHb = timeNow;
         SendStatus( TypeHeartBeat, 0, "" );
      }

/* Process all new arc messages.
   First, get message from input shared memory ring.
   ************************************************/
      while ( 1 )
      {
         int rcpa;  /* Return code from ProcessArc() */

         int res = tport_getmsg( &inRegion, GetLogo, nLogo, &inLogo, &arcMsgSize,
                                 arcMsg, MAX_BYTES_PER_EQ-1 );

         if ( res == GET_NONE )             /* No more new arc messages */
            break;
         else if ( res == GET_TOOBIG )      /* Arc message was too big. */
         {                                  /* Complain and try again. */
            sprintf( text,
                     "Retrieved msg[%ld] (i%u m%u t%u) too big for arcMsg[%d]",
                     arcMsgSize, inLogo.instid, inLogo.mod, inLogo.type,
                     MAX_BYTES_PER_EQ-1 );
            SendStatus( TypeError, ERR_TOOBIG, text );
            continue;
         }
         else if ( res == GET_MISS )        /* Got a message, but missed some */
         {
            sprintf( text, "Missed msg(s)  i%u m%u t%u  %s.",
                     inLogo.instid, inLogo.mod, inLogo.type, InRingName );
            SendStatus( TypeError, ERR_MISSMSG, text );
         }
         else if ( res == GET_NOTRACK )     /* Got a message, but can't tell if */
         {                                  /*   any were missed */
            sprintf( text, "Msg received (i%u m%u t%u); NTRACK_GET exceeded",
                     inLogo.instid, inLogo.mod, inLogo.type );
            SendStatus( TypeError, ERR_NOTRACK, text );
         }
         arcMsg[arcMsgSize] = '\0';         /* Terminate arc message*/

/* Process arc message using ProcessArc().
   If  0 is returned, we get a complete triglist message.
   If -1 is returned, there is a parsing error.
   If -2 is returned, Md=0.
   *****************************************************/
         rcpa = ProcessArc( arcMsg, trigMsg, inLogo, outLogo, subnet, nSubnet );

         if ( rcpa < 0 )
         {
            if ( rcpa == -1 )
            {
               sprintf( text, "Error processing arc msg. Event skipped.\n" );
               SendStatus( TypeError, ERR_PROCESSARC, text );
            }
            else if ( rcpa == -2 )
            {
               logit( "t", "Skipping event with Md = 0.\n" );
            }
            continue;
         }

/* Write trigger message to output ring
   ************************************/
         if ( tport_putmsg( &OutRegion, &outLogo, strlen(trigMsg), trigMsg ) != PUT_OK )
            logit( "et", "arc2trignc: Error writing trigger message to ring.\n" );

/* Write trigger message to log file
   *********************************/
         if ( LogTrig )
         {
            logit( "", "\n" );           /* Add blank line before trigger message */
            LogLongString( trigMsg );    /* Trigger message is too long for logit() */
            logit( "", "\n" );           /* Add blank line after trigger message */
         }
         logit( "t", "Trigger message written to output ring.\n" );
      }                                  /* End of message-processing loop */
      sleep_ew( 1000 );                  /* Wait for new arc messages to arrive */
   }

/* Exit program
   ************/
   tport_detach( &inRegion );
   logit( "t", "Termination requested. Exiting.\n" );
   return 0;
}

