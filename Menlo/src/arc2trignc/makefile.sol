#
#                         Make file for arc2trignc
#                             Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin 
L = $(EW_HOME)/$(EW_VERSION)/lib

O = arc2trignc.o config.o sendstatus.o logstring.o subnet.o arc.o \
    makedate.o trig.o \
    $L/chron3.o $L/logit.o $L/getutil.o $L/read_arc.o $L/transport.o \
    $L/kom.o $L/sleep_ew.o $L/time_ew.o

arc2trignc: $O
	cc -o $B/arc2trignc $O -lm -lposix4

.c.o:
	cc -c ${CFLAGS} $<

lint:
	lint arc2trignc.c $(GLOBALFLAGS)

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/arc2trignc*
