# Sample file:
# List (incomplete) of subnets used by carlsubtrig and arc2trignc.
#
# Subnet definitions for the Northern California Seismic Network.
# Each Subnet must be on a single line.

#   Subnet  Minimum   List of Station.Component.Network
#   Number  Trigger   Codes (space delimited)
#  -------  -------   -------------------------------------------
# CL: COALINGA 1
Subnet   CL   4  PWM.VHZ.NC.-- PDR.VHZ.NC.-- PAR.VHZ.NC.-- PKE.VHZ.NC.-- PMP.VHZ.NC.-- PBW.VHZ.NC.-- PHR.VHZ.NC.-- PJU.VHZ.NC.-- PHB.VHZ.NC.-- MRH.VHZ.NC.-- PJC.VHZ.NC.--

# CO: COALINGA 2
Subnet   CO   4  PDR.VHZ.NC.-- PAR.VHZ.NC.-- PJU.VHZ.NC.-- PSM.VHZ.NC.-- PCA.VHZ.NC.-- PKE.VHZ.NC.-- PWM.VHZ.NC.-- PHB.VHZ.NC.-- PJC.VHZ.NC.--

# RG
Subnet   RG   5  MLK.VHZ.NC.-- MEM.VHZ.NC.-- MSL.VHZ.NC.-- MMX1.EP1.NC.-- MCS.VHZ.NC.-- MCM.VHZ.NC.-- MLC.VHZ.NC.-- MGP.VHZ.NC.--

# P2: ARROYO GRANDE
Subnet  P2   6  PML.VHZ.NC.-- PMG.VHZ.NC.-- PBM.VHZ.NC.-- SCC.VHZ.CI.-- PBI.VHZ.NC.-- PABB.VHZ.NC.-- PKY.VHZ.NC.-- PPB.VHZ.NC.-- PTA.VHZ.NC.--

# P3
Subnet  P3   7  PMG.VHZ.NC.-- PAD.VHZ.NC.-- PSC.VHZ.NC.-- PMC.VHZ.NC.-- PHO.VHZ.NC.-- PGH.VHZ.NC.-- PHA.VHZ.NC.-- PWK.VHZ.NC.-- PST.VHZ.NC.-- PAN.VHZ.NC.-- PKY.VHZ.NC.-- PTA.VHZ.NC.--

# H5
Subnet  H5   5  HPC.VHZ.NC.-- HCO.VHZ.NC.-- HFE.VHZ.NC.-- HSL.VHZ.NC.-- HLT.VHZ.NC.-- HJS.VHZ.NC.-- BHR.VHZ.NC.-- BEH.VHZ.NC.-- BEM.VHZ.NC.-- BAV.VHZ.NC.-- BMS.VHZ.NC.-- BRM.VHZ.NC.--

# J1
Subnet  J1   5  JPS.VHZ.NC.-- JPSB.EHZ.NC.-- JTG.VHZ.NC.-- JUC.VHZ.NC.-- JPL.VHZ.NC.-- JBN.VDZ.NC.-- JBZ.VHZ.NC.-- JRG.VHZ.NC.-- HMOB.VHZ.NC.-- JEL.VHZ.NC.-- HER.VHZ.NC.-- JBR.EHZ.NC.--

