
   /**************************************************************************
    *                              sendstatus.c                              *
    *                                                                        *
    *  Contains function sendstatus.c                                        *
    **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <chron3.h>
#include <earthworm.h>
#include <transport.h>
#include "arc2trignc.h"

/* Declared in arc2trignc.c
   ************************/
extern SHM_INFO    OutRegion;              /* Shared memory region for output */
extern pid_t       MyPid;                  /* Process id is sent with heartbeat */

/* Parameters from earthworm.h
   ***************************/
extern unsigned char InstId;               /* Local installation id */
extern unsigned char MyModId;              /* Module Id for this program */
extern unsigned char TypeHeartBeat;
extern unsigned char TypeError;


  /***********************************************************************
   * SendStatus() creates a heartbeat or error message & puts it into    *
   *              shared memory.  Writes errors to log file & screen.    *
   ***********************************************************************/

void SendStatus( unsigned char type, short ierr, char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   time_t   now;

/* Build the message
   *****************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &now );

   if ( type == TypeHeartBeat )
   {
      sprintf( msg, "%ld %ld\n", (long)now, MyPid );
   }
   else if ( type == TypeError )
   {
      sprintf( msg, "%ld %hd %s\n", (long)now, ierr, note);
      logit( "et", "arc2trignc: %s\n", note );
   }

   size = strlen( msg );           /* Don't include null byte in message */

/* Write message to shared memory
   ******************************/
   if ( tport_putmsg( &OutRegion, &logo, size, msg ) != PUT_OK )
   {
      if ( type == TypeHeartBeat )
         logit("et","arc2trignc:  Error sending heartbeat.\n" );
      else if ( type == TypeError )
         logit("et","arc2trignc:  Error sending error:%d.\n", ierr );
   }
   return;
}

