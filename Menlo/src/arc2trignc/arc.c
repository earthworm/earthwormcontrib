
   /********************************************************************
    *                               arc.c                              *
    *     This file contains functions to convert one arc message      *
    *     into a triglist message.                                     *
    ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <transport.h>
#include <read_arc.h>
#include "arc2trignc.h"

/* Function prototypes
   *******************/
void BldTrigHyp( char *, MSG_LOGO, MSG_LOGO, struct Hsum );  /* In trig.c */
void BldTrigPhs( HPCK, float, double, double, char * );      /* In trig.c */
void CountPickedSta( SUBNET *, int, HPCK *, int );           /* In subnet.c */
void LogPickedSta( SUBNET *, int );                          /* In subnet.c */
void CountTriggeredSubnets( SUBNET *, int, int * );          /* In subnet.c */


  /***********************************************************************
   *                             GotSta()                                *
   *                                                                     *
   *  Returns 1 if testSta is already in the station list or the pick    *
   *            list.                                                    *
   *  Returns 0 otherwise.                                               *
   ***********************************************************************/

int GotSta( HPCK *pick, int nPick, STA *sta, int nSta, STA testSta )
{
   int i;

/* Is testSta in the pick list?
   ****************************/
   for ( i = 0; i < nPick; i++ )
      if ( ( strcmp( pick[i].site, testSta.site ) == 0 ) &&
           ( strcmp( pick[i].net,  testSta.net  ) == 0 ) ) return 1;

/* Is testSta in the station list?
   *******************************/
   for ( i = 0; i < nSta; i++ )
      if ( ( strcmp( sta[i].site, testSta.site ) == 0 ) &&
           ( strcmp( sta[i].net,  testSta.net  ) == 0 ) ) return 1;
   return 0;
}


  /***********************************************************************
   *                          AddSubnetSta()                             *
   *          Add extra trigger lines for stations in subnets.           *
   *                                                                     *
   *  Returns 0 if all ok;  -1 otherwise.                                *
   ***********************************************************************/

int AddSubnetSta( SUBNET *subnet, int nSubnet, HPCK *pick, int nPick, float Md,
                  double patMin, double patMax, char *trigMsg )
{
   extern int MinPickSta;       /* From configuration file */
   int  i;
   int  nSta = 0;
   STA  *sta = NULL;

   for ( i = 0; i < nSubnet; i++ )
   {
      int j;

      if ( subnet[i].nPick < MinPickSta )     /* Subnet isn't interesting */
         continue;

      for ( j = 0; j < subnet[i].nSta; j++ )
      {
         if ( !GotSta( pick, nPick, sta, nSta, subnet[i].sta[j] ) )
         {
            STA *tempPtr = realloc( sta, (nSta+1)*sizeof(STA) );
            if ( tempPtr == NULL )
            {
               logit( "e", "arc2trignc: Error realloc'ing the sta array." );
               free( sta );
               return -1;
            }
            sta = tempPtr;
            sta[nSta] = subnet[i].sta[j];
            nSta++;
         }
      }
   }

/* Log extra stations
   ******************/
   logit( "", "Extra stations to save (from subnets):" );
   for ( i = 0; i < nSta; i++ )
   {
      if ( (i % 6) == 0 ) logit( "", "\n" );
      logit( "", "  %5s %2s", sta[i].site, sta[i].net );
   }
   logit( "", "\n" );

/* Append extra phase lines to triglist message
   ********************************************/
   for ( i = 0; i < nSta; i++ )
   {
      HPCK extraPick;
      strcpy( extraPick.site, sta[i].site );
      strcpy( extraPick.net,  sta[i].net );
      extraPick.Plabel = 'P';
      extraPick.Pat    = 0.;            /* No measured arrival time */
      BldTrigPhs( extraPick, Md, patMin, patMax, trigMsg );
   }
   free( sta );
   return 0;
}


  /***********************************************************************
   *                         AddTimeCodeChan()                           *
   *          Add extra trigger lines for time-code channels.            *
   ***********************************************************************/

void AddTimeCodeChan( float Md, double patMin, double patMax, char *trigMsg )
{
   int i;
   extern STA   Channel[MAXCHAN];   /* Time-code chans added to all trig msgs */
   extern short nChannel;           /* Number of channels in Channel[] */

   for ( i = 0; i < nChannel; i++ )
   {
      HPCK extraChan;
      strcpy( extraChan.site, Channel[i].site );
      strcpy( extraChan.net,  Channel[i].net );
      extraChan.Plabel = 'P';
      extraChan.Pat    = 0.;                 /* No measured arrival time */
      BldTrigPhs( extraChan, Md, patMin, patMax, trigMsg );
   }
   return;
}


  /***********************************************************************
   *                         AddWildcardTrig()                           *
   *          Add a trigger lines with an all-wildcard SCNL.             *
   ***********************************************************************/

void AddWildcardTrig( float Md, double patMin, double patMax, char *trigMsg )
{
   HPCK extraPick;
   strcpy( extraPick.site, "*" );
   strcpy( extraPick.net,  "*" );
   extraPick.Plabel = 'P';
   extraPick.Pat    = 0.;                    /* No measured arrival time */
   BldTrigPhs( extraPick, Md, patMin, patMax, trigMsg );
   return;
}


  /***********************************************************************
   *                           ProcessArc()                              *
   *  Read a hypoinverse archive message and build a triglist message.   *
   *                                                                     *
   *  Called by main program (in arc2trignc.c).                          *
   *  Returns  0 if all ok,                                              *
   *          -1 if there was a parsing error,                           *
   *          -2 if Md = 0 (skip these events)                           *
   ***********************************************************************/

int ProcessArc( char *arcMsg, char *trigMsg, MSG_LOGO inLogo, MSG_LOGO outLogo,
                SUBNET *subnet, int nSubnet )
{
   int    i;
   char   *arcPtr = arcMsg;            /* Working pointer to archive message */
   char   line[MAX_STR];               /* To store lines from message */
   char   shdw[MAX_STR];               /* To store shadow cards from message */
   int    arcMsgLen = strlen(arcMsg);  /* Length of input archive message */
   struct Hsum Sum;                    /* Hyp2000 summary structure */
   struct Hpck Pick;                   /* Hyp2000 pick structure */
   int    nPick = 0;                   /* Number of picks in arc message */
   HPCK   *pick = NULL;                /* Array of reduced pick structures */
   double patMin = MAXDOUBLE;          /* Minimum P-arrival time */
   double patMax = MINDOUBLE;          /* Maximum P-arrival time */
   int    nTriggeredSub;               /* Number of triggered subnets */
   int    allNetworkEvent;             /* If non-zero, this is an all-net event */
   extern int AllSubnets;              /* If this many subnets trigger, send entire net */

/* Read hypocenter card and its shadow from arc message
   ****************************************************/
   if ( sscanf( arcPtr, "%[^\n]", line ) != 1 )
   {
      logit( "", "Incomplete hypocenter line in arc message.\n" );
      return -1;
   }
   arcPtr += strlen( line ) + 1;
   if ( sscanf( arcPtr, "%[^\n]", shdw ) != 1 )
   {
      logit( "", "Incomplete hypocenter shadow line in arc message.\n" );
      return -1;
   }
   arcPtr += strlen( shdw ) + 1;
   if ( read_hyp( line, shdw, &Sum ) == -1 )
   {
      logit( "", "read_hyp() error. Can't parse hypocenter card from arc msg.\n" );
      return -1;
   }

   BldTrigHyp( trigMsg, inLogo, outLogo, Sum );  /* Write first part of trig msg */

   logit( "t", "Binder event detected.  Id:%d  Md:%.2f\n", Sum.qid, Sum.Md );

/* Early return if Md=0.
   ********************/
   if ( Sum.Md < 0.005 ) return -2;        /* Md specified to two decimal places */

/* Read phase cards and their shadows from arc message
  ****************************************************/
   while ( arcPtr < (arcMsg+arcMsgLen) )
   {
      HPCK *tempPtr;                           /* Temporary pointer */

      if ( sscanf( arcPtr, "%[^\n]", line ) != 1 )
      {
         logit( "", "Incomplete phase line in arc message.\n" );
         free(pick); return -1;
      }
      arcPtr += strlen( line ) + 1;
      if ( sscanf( arcPtr, "%[^\n]", shdw ) != 1 )
      {
         logit( "", "Incomplete phase shadow line in arc message.\n" );
         free(pick); return -1;
      }
      arcPtr += strlen( shdw ) + 1;
      if ( strlen(line) < 75 ) break;          /* Arc message terminator line */

/* Decode phase/shadow cards into Pick structure
   *********************************************/
      if ( read_phs( line, shdw, &Pick ) == -1 )
      {
         logit( "", "read_phs() error. Can't parse phase card from arc msg.\n" );
         free( pick );
         return -1;
      }

/* Copy interesting pick parameters into
   an array of reduced pick structures
   *************************************/
      tempPtr = realloc( pick, (nPick+1)*sizeof(HPCK) );
      if ( tempPtr == NULL )
      {
         logit( "e", "arc2trignc: Error realloc'ing the pick array." );
         free( pick );
         return -1;
      }
      pick = tempPtr;
      memcpy( pick[nPick].site, Pick.site, 6 );
      memcpy( pick[nPick].net,  Pick.net,  3 );
      pick[nPick].Plabel = Pick.Plabel;             /* 'P' */
      pick[nPick].Pat    = Pick.Pat;                /* P arrival time */
      nPick++;
   }

/* Get min and max P-arrival times (patMin, patMax).
   These are passed to BldTrigPhs().
   ************************************************/
   for ( i = 0; i < nPick; i++ )
   {
      if ( pick[i].Pat < patMin ) patMin = pick[i].Pat;
      if ( pick[i].Pat > patMax ) patMax = pick[i].Pat;
   }

/* Count the number of picked stations in each subnet.
   Store result in subnet array.  Log all subnets that
   contain any picked stations.
   **************************************************/
   CountPickedSta( subnet, nSubnet, pick, nPick );
   LogPickedSta( subnet, nSubnet );

/* Is this an all-network event?
   *****************************/
   CountTriggeredSubnets( subnet, nSubnet, &nTriggeredSub );
   logit( "", "Number of triggered subnets: %d\n", nTriggeredSub );
   allNetworkEvent = ( nTriggeredSub >= AllSubnets ) ? 1 : 0;
   if ( allNetworkEvent ) logit( "", "This is an all-network event.\n" );

/* If it's an all-network event, append an
   all-wildcard SCNL to triglist message
   ***************************************/
   if ( allNetworkEvent )
      AddWildcardTrig( Sum.Md, patMin, patMax, trigMsg );

/* Append pick lines to triglist message
   *************************************/
   else
   {
      for ( i = 0; i < nPick; i++ )
         BldTrigPhs( pick[i], Sum.Md, patMin, patMax, trigMsg );

/* Append stations in subnets to triglist message
   **********************************************/
      if ( AddSubnetSta( subnet, nSubnet, pick, nPick, Sum.Md, patMin,
                         patMax, trigMsg ) == -1 )
      {
         logit( "", "AddSubnetSta() error.\n" );
         free( pick ); return -1;
      }

/* Append time-code channels to triglist message
   *********************************************/
      AddTimeCodeChan( Sum.Md, patMin, patMax, trigMsg );
   }
   free( pick );     /* Success */
   return 0;
}
