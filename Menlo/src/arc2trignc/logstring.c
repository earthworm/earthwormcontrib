
     /*********************************************************
      *                      logstring.c                      *
      *                                                       *
      *     This file contains function LogLongString().      *
      *                                                       *
      *  logit_init takes the max length of a logit string    *
      *  as an argument.  Strings longer than that length     *
      *  can't be logged directly using logit().  Function    *
      *  LogLongString cuts a string into bite-size chunks    *
      *  and logs them using logit.                           *
      *********************************************************/

#include <string.h>
#include <earthworm.h>
#include "arc2trignc.h"


void LogLongString( char *str )
{
   char lstr[MAXLOGITSTR];         /* Local string to send to log file */

   int  nToSend = strlen( str );
   char *strPtr = str;
   const int maxSend = MAXLOGITSTR - 1;

   while ( nToSend > 0 )
   {
      int ns = (nToSend < maxSend ) ? nToSend : maxSend;

      memcpy( &lstr[0], strPtr, ns );
      lstr[ns] = '\0';
      logit( "", "%s", lstr );
      nToSend -= ns;
      strPtr  += ns;
   }
   return;
}
