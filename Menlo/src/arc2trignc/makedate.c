
   /****************************************************************
    *                           makedate.c                         *
    ****************************************************************/

#include <string.h>
#include <chron3.h>


  /*******************************************************************
   *                         MakeDateStr()                           *
   *  Takes a time in seconds since year 1600 and converts it into   *
   *  a character string of the form: "19880123 12:34:12.21"         *
   *                                                                 *
   *  This function returns a pointer to the new character string.   *
   *  It requires an output buffer >=21 characters long.             *
   *******************************************************************/

char *MakeDateStr( double t, char *dateStr )
{
    char str17[18];         /* Temporary date string */

/* Convert time to a pick-format character string
   **********************************************/
    date17( t, str17 );

/* Convert a date character string of the form:
   "19880123123412.21"      to one of the form:
   "19880123 12:34:12.21"
    0123456789 123456789
   *******************************************/
    strncpy( dateStr, str17,    8 );        /* yyyymmdd */
    dateStr[8] = '\0';
    strcat ( dateStr, " " );
    strncat( dateStr, str17+8,  2 );        /* hours */
    strcat ( dateStr, ":" );
    strncat( dateStr, str17+10,  2 );       /* minutes */
    strcat ( dateStr, ":" );
    strncat( dateStr, str17+12, 5 );        /* seconds */
    return dateStr;
}

