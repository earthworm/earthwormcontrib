/*
 *   THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT USING THE COMMAND CHECKOUT.
 *
 *    $Id: trig.c 97 2005-01-24 21:10:57Z dietz $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.3  2005/01/24 21:10:57  dietz
 *     added TRIG_VERSION to document what version of message is produced
 *
 */

   /**************************************************************************
    *                                 trig.c                                 *
    *         This file contains functions to build one trig message.        *
    **************************************************************************/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <transport.h>
#include <read_arc.h>
#include "arc2trignc.h"

/* Parameters declared in config.c
   *******************************/
extern double PrePickTime;           /* Seconds to save before pick time */
extern double PostCodaTime;          /* Seconds to save past tau time */

/* Function prototype
   ******************/
char *MakeDateStr( double, char * );      /* In makedate.c */


    /**************************************************************
     *                        BldTrigHyp()                        *
     *       Builds the EVENT line of a triglist message.         *
     **************************************************************/

void BldTrigHyp( char *trigMsg, MSG_LOGO inLogo, MSG_LOGO outLogo, 
                 struct Hsum Sum )
{
   char dateStr[DATESTR_LEN];

/* Sample EVENT line for trigger message:
EVENT DETECTED     970729 03:01:13.22 UTC EVENT ID:123456 AUTHOR: asdf:asdf\n
0123456789 123456789 123456789 123456789 123456789 123456789
************************************************************/
   MakeDateStr( Sum.ot, dateStr );

   sprintf( trigMsg,
      "%s EVENT DETECTED     %s UTC EVENT ID: %u AUTHOR: %03d%03d%03d:%03d%03d%03d\n\n", 
      TRIG_VERSION, dateStr, (int)Sum.qid, 
      inLogo.type, inLogo.mod, inLogo.instid,
      outLogo.type, outLogo.mod, outLogo.instid );

   strcat( trigMsg, "Sta/Cmp/Net/Loc   Date   Time           " );
   strcat( trigMsg, "            start save       duration in sec.\n" );
   strcat( trigMsg, "---------------   ------ ---------------" );
   strcat( trigMsg, "    ------------------------------------------\n" );
   return;
}


   /*************************************************************
    *                          Tau()                            *
    *      Calculate event duration (tau) from magnitude.       *
    *                                                           *
    *  From David Oppenheimer:                                  *
    *     TAU = 10.0**((XMAG - FMA)/FMB)                        *
    *   where TAU  is in seconds,                               *
    *         XMAG is the magnitude,                            *
    *         FMA  is the coda magnitude intercept coefficient  *
    *         FMB  is the coda magnitude duration coefficient   *
    *  Values of fma and fmb are from a paper by Bill Bakun.    *
    *************************************************************/

double Tau( float xmag )
{
   const double fma = -0.87;
   const double fmb = 2.0;
   return pow( 10.0, (xmag-fma)/fmb );     /* Requires math.h */
}


   /**************************************************************
    *                       BldTrigPhs()                         *
    *       Append a phase line to the triglist message          *
    **************************************************************/

void BldTrigPhs( HPCK Pick, float Md, double patMin, double patMax,
                 char *trigMsg )
{
   char   str[MAX_STR];
   char   pcktStr[DATESTR_LEN];
   char   savetStr[DATESTR_LEN];
   int    saveDuration;            /* Total duration to save */
   extern int MaxDuration;         /* From configuration file */

/* Convert times in seconds since 1600 to character strings
   ********************************************************/
   MakeDateStr( Pick.Pat,           pcktStr  );
   MakeDateStr( patMin-PrePickTime, savetStr );

/* Calculate number of seconds of data to save
   *******************************************/
   saveDuration = (int)(PrePickTime + (patMax - patMin) + Tau(Md) + PostCodaTime);
   if ( saveDuration > MaxDuration ) saveDuration = MaxDuration;

/* Build the phase line with wildcarded comp & loc. Here's a sample:
 MCM * NC * N 19970729 03:01:13.34 UTC    save: yyyymmdd 03:00:12.34      120\n
0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
********************************************************************************/
   sprintf( str, " %s * %s * %c %s UTC    save: %s %8d\n", 
            Pick.site, Pick.net, Pick.Plabel, pcktStr, 
            savetStr, saveDuration );
   strcat( trigMsg, str );
   return;
}

