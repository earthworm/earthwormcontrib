
   /**************************************************************************
    *                                subnet.c                                *
    **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include "arc2trignc.h"

/* Global variables
   ****************/
static FILE *fp;
static char line[MAXLINELEN];     /* Buffer to contain one subnet line */

/* Declared in config.c
   ********************/
extern char SubnetFile[80];   /* Name of file containing subnet definitions */


  /**********************************************************************
   *                          CountSubnets()                            *
   *        Returns the number of Subnet lines in a subnet file.        *
   **********************************************************************/

int CountSubnets( int *nSubnet )
{
   int nsub = 0;

/* Open subnet file
   ****************/
   fp = fopen( SubnetFile, "r" );
   if ( fp == NULL )
   {
      logit( "e", "arc2trignc: Error opening subnet file <%s>.\n", SubnetFile );
      return -1;
   }

/* Read subnet file until error or EOF is encountered
   **************************************************/
   while ( 1 )
   {
      char *rp = fgets( line, MAXLINELEN, fp );
      char *token;

      if ( feof( fp ) ) break;                  /* We hit the EOF */
      if ( (rp == NULL) && ferror( fp ) )
      {
         logit( "e", "arc2trignc: Error reading subnet file.\n" );
         fclose( fp );
         return -1;
      }

/* See if first token in line is <Subnet>
   **************************************/
      token = strtok( line, " \t" );
      if ( token == NULL ) continue;           /* No tokens in line */
      if ( strcmp(token,"Subnet") == 0 )       /* We found a subnet line */
         nsub++;
   }
   fclose( fp );
   *nSubnet = nsub;
   return 0;
}


  /**********************************************************************
   *                            DecodeSCNL()                            *
   *           Given the token S.C.N.L, save S and N                    *
   **********************************************************************/

int DecodeSCNL( char *scnl, STA *sta )
{
   int  siteLen;                             /* Length of site code */
   int  compLen;                             /* Length of component code */
   int  netLen;                              /* Length of network code */
   int  scnlLen = strlen(scnl);
   char *ptr    = scnl;

/* Sanity check
   ************/
   if ( scnlLen < 1 )
   {
      logit( "", "Error: SCNL token has zero length.\n" );
      return -1;
   }

/* Get the site code
   *****************/
   siteLen = strcspn( ptr, "." );
   if ( siteLen > 5 )
   {
      logit( "", "Error: Site name in scnl token is too long <%s>\n", scnl );
      return -1;
   }
   strncpy( sta->site, ptr, siteLen );
   sta->site[siteLen] = '\0';

/* Skip the component code
   ***********************/
   ptr += siteLen + 1;
   scnlLen -= (siteLen + 1);
   if ( scnlLen < 1 )
   {
      logit( "", "Error: No component code in scnl token <%s>\n", scnl );
      return -1;
   }
   compLen = strcspn( ptr, "." );

/* Get the network code
   ********************/
   ptr += compLen + 1;
   scnlLen -= (compLen + 1);
   if ( scnlLen < 1 )
   {
      logit( "", "Error: No network code in scnl token <%s>\n", scnl );
      return -1;
   }
   netLen = strcspn( ptr, "." );
   if ( netLen > 2 )
   {
      logit( "", "Error: Network code too long in scnl token <%s>\n", scnl );
      return -1;
   }
   strncpy( sta->net, ptr, netLen );
   sta->net[netLen] = '\0';

/* Ignore the location code
   ************************/
   return 0;
}


  /**********************************************************************
   *                            ReadSubnets()                           *
   *    Read all subnets from the subnet file into the subnet array.    *
   **********************************************************************/

int ReadSubnets( SUBNET subnet[], int nSubnet )
{
   int isub = 0;

/* Open subnet file
   ****************/
   fp = fopen( SubnetFile, "r" );
   if ( fp == NULL )
   {
      logit( "e", "arc2trignc: Error opening subnet file <%s>.\n", SubnetFile );
      return -1;
   }

/* Read all subnet lines from subnet file
   **************************************/
   while ( isub < nSubnet )
   {
      char *rp = fgets( line, MAXLINELEN, fp );
      char *token;
      int  nSta = 0;

      if ( feof( fp ) )
      {
         logit( "e", "arc2trignc: Error. EOF encountered reading subnet file.\n" );
         goto ERROR_RETURN;
      }
      if ( (rp == NULL) && ferror( fp ) )
      {
         logit( "e", "arc2trignc: Error reading subnet file.\n" );
         goto ERROR_RETURN;
      }

/* Skip lines in which the first token isn't <Subnet>
   **************************************************/
      token = strtok( line, " \t" );
      if ( token == NULL ) continue;            /* No tokens in line */
      if ( strcmp(token,"Subnet") != 0 ) continue;

/* Get the subnet code.  Store in subnet array.
   *******************************************/
      token = strtok( NULL, " \t\n" );
      if ( token == NULL )                      /* No subnet code in line */
      {
         subnet[isub].nSta = 0;
         isub++;
         continue;
      }
      if ( strlen(token) >= MAXSUBCODELEN )
      {
         logit( "e", "arc2trignc: Error. Subnet code too long (%d). Max=%d\n",
                strlen(token), MAXSUBCODELEN );
         goto ERROR_RETURN;
      }
      strcpy( subnet[isub].code, token );

/* Skip over the minTrig field.  We don't use it.
   *********************************************/
      token = strtok( NULL, " \t\n" );
      if ( token == NULL )                      /* No minTrig in line */
      {
         subnet[isub].nSta = 0;
         isub++;
         continue;
      }

/* Read all scnl tokens from Subnet line
   ************************************/
      while ( 1 )
      {
         STA sta;

         token = strtok( NULL, " \t\n" );
         if ( token    == NULL ) break;       /* No more scnl tokens in line */
         if ( token[0] == '|'  ) continue;    /* Ignore divider character   */
         if ( strlen(token) >= MAXSCNLLEN )
         {
            logit( "e", "arc2trignc: Error. SCNL token too long (%d). Max=%d\n",
                   strlen(token), MAXSCNLLEN );
            goto ERROR_RETURN;
         }
         if ( DecodeSCNL( token, &sta ) == -1 )
         {
            logit( "e", "arc2trignc: Error decoding SCNL token <%s>\n", token );
            goto ERROR_RETURN;
         }
         subnet[isub].sta = (STA *)realloc( subnet[isub].sta, (nSta+1)*sizeof(STA) );
         subnet[isub].sta[nSta] = sta;
         subnet[isub].nSta = ++nSta;
      }
      isub++;                      /* Increment subnet counter */
   }
   fclose( fp );                   /* No errors encountered */
   return 0;

ERROR_RETURN:
   fclose( fp );
   return -1;
}


  /**********************************************************************
   *                           LogSubnets()                             *
   **********************************************************************/

void LogSubnets( SUBNET subnet[], int nSubnet )
{
   int i;

   for ( i = 0; i < nSubnet; i++ )
   {
      int j;

      logit( "", "\nSubnet %-3s  nSta:%d", subnet[i].code, subnet[i].nSta );

      for ( j = 0; j < subnet[i].nSta; j++ )
      {
         if ( (j % 6) == 0 ) logit( "", "\n" );
         logit( "", "  %5s %2s", subnet[i].sta[j].site, subnet[i].sta[j].net );
      }
      logit( "", "\n" );
   }
   logit( "", "\n" );
   return;
}
 
 
  /***********************************************************************
   *                         CountPickedSta()                            *
   *        Count the number of picked stations in each subnet.          *
   *                                                                     *
   *  Store the result in the subnet array.                              *
   ***********************************************************************/
 
void CountPickedSta( SUBNET *subnet, int nSubnet, HPCK *pick, int nPick )
{
   int i;
   for ( i = 0; i < nSubnet; i++ )
   {
      int j;
      subnet[i].nPick = 0;
      for ( j = 0; j < nPick; j++ )
      {  
         int k;
         for ( k = 0; k < subnet[i].nSta; k++ )
         {
            int sameSite = !strcmp(subnet[i].sta[k].site, pick[j].site );
            int sameNet  = !strcmp(subnet[i].sta[k].net,  pick[j].net  );
            if ( sameSite && sameNet ) subnet[i].nPick++;
         }
      }  
   }
   return;
}
 
 
  /***********************************************************************
   *                          LogPickedSta()                             *
   ***********************************************************************/
 
void LogPickedSta( SUBNET *subnet, int nSubnet )
{
   int i;
   for ( i = 0; i < nSubnet; i++ )
      if ( subnet[i].nPick > 0 )
         logit( "", "Subnet %s contains %hd picked stations.\n",
                subnet[i].code, subnet[i].nPick );
   return;
}


  /***********************************************************************
   *                      CountTriggeredSubnets()                        *
   ***********************************************************************/
 
void CountTriggeredSubnets( SUBNET *subnet, int nSubnet, int *nTriggeredSub )
{
   int    i;
   int    nTrig = 0;
   extern int MinPickSta;       /* From configuration file */

   for ( i = 0; i < nSubnet; i++ )
      if ( subnet[i].nPick  >= MinPickSta ) nTrig++;
   *nTriggeredSub = nTrig;
   return;
}

