
# This is arc2trignc's parameter file

MyModuleId        MOD_ARC2TRIGNC # Module id for this instance of arc2trignc 
InRingName           HYPO_RING   # Shared memory ring for input
OutRingName          HYPO_RING   # Shared memory ring for output
LogTrig              1           # If 0, don't log trigger files; otherwise do. 
HeartBeatInterval    30          # Seconds between heartbeats
PrePickTime          15.0        # Seconds of pre-p-pick data to save
PostCodaTime         10.0        # Seconds of post-coda cutoff data to save
SubnetFile           ncsn_subs.d # File containing subnet definitions
MinPickSta           2           # If this many stations are picked in subnet,
                                 #    include entire subnet in triglist msg.
AllSubnets           25          # If this many subnets trigger, put wildcard 
                                 #    SCNL in triglist message 
MaxDuration          500         # Maximum amount of time to save (sec)

# List the message logos to obtain from transport ring (max lines = 10)
#              Installation       Module          Message Types
GetEventsFrom  INST_MENLO       MOD_EVTC        # hyp2000arc - no choice

# Stations that should be included in all event messages. (Optional)
# List one station/network pair per line.  (max lines = 10)
# Component and location codes will be wildcarded.
Channel  IRG1a NC
Channel  IRG2a NC
Channel  IRG1e NC
Channel  IRG2e NC
Channel  IRG1i NC
Channel  IRG2i NC

