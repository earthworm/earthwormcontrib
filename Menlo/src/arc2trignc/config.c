
   /**************************************************************************
    *                                config.c                                *
    *        Contains the GetConfig() and LookUpParams() functions.          *
    **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <chron3.h>
#include <earthworm.h>
#include <transport.h>
#include <kom.h>
#include "arc2trignc.h"

/* Global variable
   ***************/
static char MyModName[MAX_MOD_STR]; /* Speak as this module */

/* Parameters read from configuration file
   ***************************************/
MSG_LOGO GetLogo[MAXLOGO];          /* Array for module,type,instid */
short    nLogo;                     /* Number of logos in GetLogo[] */
STA      Channel[MAXCHAN];          /* Time-code chans added to all trig msgs */
short    nChannel = 0;              /* Number of channels in Channel[] */
char     InRingName[MAX_RING_STR];  /* Name of transport ring for input */
char     OutRingName[MAX_RING_STR]; /* Name of transport ring for output */
int      LogTrig;                   /* If 1, log trigger messages */
long     HeartBeatInterval;         /* Seconds between heartbeats */
double   PrePickTime;               /* Seconds to save before pick time */
double   PostCodaTime;              /* Seconds past coda or tau time */
char     SubnetFile[80];            /* Name of file containing subnet definitions */
int      MinPickSta;                /* Min picks in reporting subnet */
int      AllSubnets;                /* If this many subnets trigger, send entire net */
int      MaxDuration;               /* Maximum amount of time to save */

/* Parameters from earthworm.h
   ***************************/
long          InRingKey;            /* Key of transport ring for input */
long          OutRingKey;           /* Key of transport ring for output */
unsigned char InstId;               /* Local installation id */
unsigned char MyModId;              /* Module Id for this program */
unsigned char TypeHeartBeat;
unsigned char TypeError;
unsigned char TypeTrigList;


  /**********************************************************************
   *                           GetConfig()                              *
   *          Processes command file(s) using kom.c functions.          *
   *                                                                    *
   *  Returns -1 if any errors are encountered.                         *
   **********************************************************************/
#define NCOMMAND 12      /* Number of parameters in command file */

int GetConfig( char *configfile )
{
   char init[NCOMMAND];  /* Init flags, one byte for each required command */
   int  nmiss;           /* Number of commands that were missed */
   char *com;
   char *str;
   int  nfiles;
   int  success;
   int  i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < NCOMMAND; i++ ) init[i] = 0;
   nLogo = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      logit( "", "Error opening command file <%s>.\n", configfile );
      return -1;
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )             /* Read next line from active file  */
      {
         com = k_str();            /* Get first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )          continue;
         if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
         if ( com[0] == '@' )
         {
            success = nfiles+1;
            nfiles  = k_open(&com[1]);
            if ( nfiles != success )
            {
               logit( "", "Error opening command file <%s>.\n", &com[1] );
               return -1;
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         if ( k_its("MyModuleId") )
         {
            str = k_str();
            if (str) strcpy( MyModName, str );
            init[0] = 1;
         }
         else if ( k_its("InRingName") )
         {
            str = k_str();
            if (str) strcpy( InRingName, str );
            init[1] = 1;
         }
         else if ( k_its("OutRingName") )
         {
            str = k_str();
            if ( str ) strcpy( OutRingName, str );
            init[2] = 1;
         }
         else if ( k_its("HeartBeatInterval") )
         {
            HeartBeatInterval = k_long();
            init[3] = 1;
         }
         else if ( k_its("GetEventsFrom") )
         {
            if ( nLogo+1 >= MAXLOGO )
            {
               logit( "", "Too many <GetEventsFrom> lines in config file." );
               logit( "", " Max=%d\n", MAXLOGO );
               return -1;
            }
            if ( ( str=k_str() ) )
            {
               if ( GetInst( str, &GetLogo[nLogo].instid ) != 0 )
               {
                  logit( "", "Invalid installation name <%s>", str );
                  logit( "", " in <GetEventsFrom> cmd.\n" );
                  return -1;
              }
            }
            if ( ( str=k_str() ) )
            {
               if ( GetModId( str, &GetLogo[nLogo].mod ) != 0 )
               {
                  logit( "", "Invalid module name <%s>", str );
                  logit( "", " in <GetEventsFrom> cmd.\n" );
                  return -1;
               }
            }
            if ( GetType( "TYPE_HYP2000ARC", &GetLogo[nLogo].type ) != 0 )
            {
               logit( "", "Invalid message type <TYPE_HYP2000ARC>.\n" );
               return -1;
            }
            nLogo++;
            init[4] = 1;
         }
         else if ( k_its("PrePickTime") )
         {
            PrePickTime = k_val();
            init[5] = 1;
         }
         else if ( k_its("PostCodaTime") )
         {
            PostCodaTime = k_val();
            init[6] = 1;
         }
         else if ( k_its("LogTrig") )
         {
            LogTrig = k_int();
            init[7] = 1;
         }
         else if ( k_its("SubnetFile") )
         {
            str = k_str();
            if ( str ) strcpy( SubnetFile, str );
            init[8] = 1;
         }
         else if ( k_its("MinPickSta") )
         {
            MinPickSta = k_int();
            init[9] = 1;
         }
         else if ( k_its("AllSubnets") )
         {
            AllSubnets = k_int();
            init[10] = 1;
         }
         else if ( k_its("MaxDuration") )
         {
            MaxDuration = k_int();
            init[11] = 1;
         }
         else if ( k_its("Channel") )         /* Optional command */
         {
            if ( nChannel >= MAXCHAN )
            {
               logit( "", "Too many <Channel> lines in config file." );
               logit( "", " Max=%d\n", MAXCHAN );
               return -1;
            }
            str = k_str();
            if ( str ) strcpy( Channel[nChannel].site, str );
            str = k_str();
            if ( str ) strcpy( Channel[nChannel].net, str );
            nChannel++;
         }

/* Unknown command
   ***************/
         else
         {
             logit( "", "<%s> Unknown command in <%s>.\n", com, configfile );
             continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            logit( "", "Bad <%s> command in <%s>.\n", com, configfile );
            return -1;
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < NCOMMAND; i++ )
      if ( !init[i] ) nmiss++;

   if ( nmiss )
   {
      logit( "", "ERROR, no " );
      if ( !init[0] ) logit( "", "<MyModuleId> "        );
      if ( !init[1] ) logit( "", "<InRingName> "        );
      if ( !init[2] ) logit( "", "<OutRingName> "       );
      if ( !init[3] ) logit( "", "<HeartBeatInterval> " );
      if ( !init[4] ) logit( "", "<GetEventsFrom> "     );
      if ( !init[5] ) logit( "", "<PrePickTime> "       );
      if ( !init[6] ) logit( "", "<PostCodaTime> "      );
      if ( !init[7] ) logit( "", "<LogTrig> "           );
      if ( !init[8] ) logit( "", "<SubnetFile> "        );
      if ( !init[9] ) logit( "", "<MinPickSta> "        );
      if ( !init[10]) logit( "", "<AllSubnets> "        );
      if ( !init[11]) logit( "", "<MaxDuration> "       );
      logit( "", "command(s) in <%s>.\n", configfile );
      return -1;
   }
   return 0;
}


  /*************************************************************************
   *                             LogConfig()                               *
   *************************************************************************/

void LogConfig( void )
{
   int i;
   logit( "t", "Configuration file parameters:\n" );
   logit( "", "  InRingName:        %s\n",    InRingName );
   logit( "", "  OutRingName:       %s\n",    OutRingName );
   logit( "", "  LogTrig:           %d\n",    LogTrig );
   logit( "", "  HeartBeatInterval: %d\n",    HeartBeatInterval );
   logit( "", "  PrePickTime:       %.2lf\n", PrePickTime );
   logit( "", "  PostCodaTime:      %.2lf\n", PostCodaTime );
   logit( "", "  SubnetFile:        %s\n",    SubnetFile );
   logit( "", "  MinPickSta:        %d\n",    MinPickSta );
   logit( "", "  AllSubnets:        %d\n",    AllSubnets );
   logit( "", "  MaxDuration:       %d\n",    MaxDuration );
   for ( i = 0; i < nLogo; i++ )
      logit( "", "  GetLogo:           instid:%u mod:%u type:%u\n",
             GetLogo[i].instid, GetLogo[i].mod, GetLogo[i].type );
   for ( i = 0; i < nChannel; i++ )
      logit( "", "  Channel:           site:%s net:%s\n",
             Channel[i].site, Channel[i].net );
   return;
}


  /*************************************************************************
   *                           LookUpParams( )                             *
   *                Get some parameters from earthworm.h.                  *
   *                                                                       *
   *  Returns -1 if any errors are encountered.                            *
   *************************************************************************/

int LookUpParams( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if ( ( InRingKey = GetKey(InRingName) ) == -1 )
   {
      logit( "", "Invalid ring name <%s>.\n", InRingName);
      return -1;
   }
   if ( ( OutRingKey = GetKey(OutRingName) ) == -1 )
   {
      logit( "", "Invalid ring name <%s>.\n", OutRingName);
      return -1;
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      logit( "", "Error getting local installation id.\n" );
      return -1;
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 )
   {
      logit( "", "Invalid module name <%s>.\n", MyModName );
      return -1;
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      logit( "", "Invalid message type <TYPE_HEARTBEAT>.\n" );
      return -1;
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      logit( "", "Invalid message type <TYPE_ERROR>.\n" );
      return -1;
   }
   if ( GetType( "TYPE_TRIGLIST_SCNL", &TypeTrigList ) != 0 )
   {
      logit( "", "Invalid message type <TYPE_TRIGLIST_SCNL>.\n" );
      return -1;
   }
   return 0;
}

