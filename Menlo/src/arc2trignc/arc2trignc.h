/*
 *   THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT USING THE COMMAND CHECKOUT.
 *
 *    $Id: arc2trignc.h 97 2005-01-24 21:10:57Z dietz $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.3  2005/01/24 21:10:57  dietz
 *     added TRIG_VERSION to document what version of message is produced
 *
 */

/*
 * arc2trignc.h
 */

#define MAXLOGO         10  /* Max number of GetEventsFrom lines in config file */
#define MAXCHAN         10  /* Max number of Channel lines in config file */
#define MAX_STR        255  /* Max length of strings */
#define DATESTR_LEN     22  /* Length of string required by make_datestr */
#define MAXLOGITSTR    256  /* Max length of a string sent to logit */
#define MAXSUBCODELEN    6  /* Max chars in a subnet code name */
#define MAXLINELEN     600  /* Max length of a line in the subnet file */
#define MAXSCNLLEN      16  /* Max length of an scnl string, eg MEM.VHZ.NC.01 */
#define TRIG_VERSION "v2.0" /* version of TYPE_TRIGLIST_SCNL  */

/* The following were obtained from Solaris include file values.h
   They conform to the IEEE 754 standard.
   **************************************************************/
#define MAXDOUBLE       1.79769313486231570e+308
#define MINDOUBLE       4.94065645841246544e-324

/* Error message numbers
   *********************/
#define  ERR_MISSMSG    0   /* Message missed in transport ring */
#define  ERR_TOOBIG     1   /* Retrieved msg too large for buffer */
#define  ERR_NOTRACK    2   /* Msg retrieved; tracking limit exceeded */
#define  ERR_PROCESSARC 3   /* Error processing arc message */

typedef struct
{
   char site[6];            /* Site code */
   char net[3];             /* Network code */
} STA;

typedef struct
{
   char  code[MAXSUBCODELEN];   /* Subnet name code */
   short nSta;              /* Number of sta's in the subnet */
   STA   *sta;              /* Array of sta's in the subnet */
   short nPick;             /* Number of picked sta's in the subnet */
} SUBNET;

typedef struct
{
   char    site[6];         /* Site code */
   char    net[3];          /* Network code */
   char    Plabel;          /* P phase label */
   double  Pat;             /* P-arrival time as sec since 1600 */
} HPCK;

