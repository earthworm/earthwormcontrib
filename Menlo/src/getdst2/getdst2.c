
   /*****************************************************************
    *                          getdst2.c                            *
    *                                                               *
    *  This program reads 16-bit Digital Seismic Telemetry (DST)    *
    *  data from a Digi C/CON 16 system.  Time is obtained from a   *
    *  True-Time PC-SG Clock Card Model 560-5002 using a custom     *
    *  device driver written by Tod Morcott, OS2 consultant.        *
    *  This program was written for use under OS2 only.             *
    *                                                               *
    *  The program is dual threaded.  The main thread is known as   *
    *  the "drinker" thread, with source functions in this file.    *
    *  The second thread is the "descrambler" thread, with source   *
    *  functions in file descramb.c                                 *
    *                                                               *
    *  One file handle is needed for each Digi and True-time port.  *
    *  The maximum number of file handles will probably need to be  *
    *  changed in the config.sys file.                              *
    *                                                               *
    *  All output messages contain 32-bit samples, with one of the  *
    *  four bytes being a status byte.  An output message contains  *
    *  data for one component recorded by one port, ie the          *
    *  messages are demultiplexed.  Most messages are of a fixed    *
    *  length, usually one second.  If errors in the data stream    *
    *  are detected, a short message will be written.  If a byte    *
    *  is missing, a dummy value will be inserted in it's place,    *
    *  and the status byte will have a bit set to indicate dummy    *
    *  data.                                                        *
    *****************************************************************/

/* 1/24/2000  CSL converted from analog to DST station, resulting in
   a total of 16 DST stations.  getdst2 started dying with memory
   overflow errors.  Problem fixed by increasing WbSize from 2500 to
   3000.  Increased MAXDRINK from 110 to 150.  WMK

   5/4/2004   Waveform message type changed from TRACEBUF to
   TRACEBUF2.  TRACEBUF2 messages contain location code.  File
   sta_list2.dst changed also.  WMK */

#define INCL_DOS
#define INCL_DOSFILEMGR
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#define INCL_DOSDATETIME
#define INCL_DOSMISC        /* For DosQuerySysInfo */
#define INCL_ERRORS         /* For DosQuerySysInfo */
#include <os2.h>
#include <io.h>             /* For low-level io */
#include <fcntl.h>          /* For low-level io */
#include <sys\stat.h>       /* For low-level io */
#include <bsedev.h>         /* DosDevIOCtl() constants */
#include <stdio.h>
#include <string.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <kom.h>
#include <earthworm.h>
#include <truetime.h>
#include <transport.h>
#include "getdst2.h"

/* Things to read from configuration file
   **************************************/
volatile long     HeartBeatInterval;  /* Seconds between heartbeats */
volatile int      NPort;              /* Number of Digi ports */
volatile unsigned DescramblerDelay;   /* Descrambler sleeps this many msec */
volatile char     StaFile[40];        /* Name of dst station file */
volatile int      Year;               /* The current year, eg 1996 */
volatile int      MaxDrinkSize;       /* Per port, in bytes */
static char       RingName[20];       /* Waveform transport ring */
static char       MyModuleId[20];     /* speak as this module name/id */
static int        DrinkDelay;         /* Milliseconds between port reads */
static char       dClass[5];          /* Class of descrambler */
static ULONG      Priority;           /* Priority of descrambler */
static long       DrinkRingSize;      /* Kilobytes */
static ULONG      MaxFileHandle;      /* Also, set FILES=xxx in config.sys */ 
static int        MaxPassCount;       /* Poll the PC-SG card this many times */

/* Things to look up in the earthworm.h tables
   *******************************************/
volatile UCHAR MyModId;               /* Module Id for this program */
volatile UCHAR TypeHeartBeat; 
volatile UCHAR TypeError;
static long    DrinkRingKey;          /* Key of drink ring */
static long    OutRingKey;            /* Key of output ring */
static UCHAR   InstId;                /* Local installation id */
static UCHAR   TypeDrink;
static UCHAR   TypeWave;

/* Other stuff shared with the descrambler
   ***************************************/
volatile SHM_INFO DrinkRegion;               /* Private memory region */
volatile SHM_INFO OutRegion;                 /* The region for writing waveforms */
volatile MSG_LOGO DrinkLogo;                 /* Logo of drink messages */
volatile MSG_LOGO WaveLogo;                  /* Logo of DST waveform messages */
volatile TrueTimeStruct TrueTime;

/* Function prototypes
   *******************/
ULONG GetTimerInterval( void );
void  InitLogos( void );
void  GetConfig( char * );
void  LogConfig( void );
void  GetUtilLookup( void );
void  SetDCB( HFILE );
void  Descrambler( void * );
void  LogTrueTimeError( int );
int   LogDigiError( USHORT );
PORT  *AllocPortStruct( int );
UCHAR *AllocInBuf( void );
UCHAR *AllocDrinkBuf( void );
void  PutStatus( unsigned char, short, char * );
void  Setup( void );


             /******************************************
              *     The main program starts here.      *
              ******************************************/

int main( int argc, char *argv[] )
{
   UCHAR    *InBuf;                 /* Bytes from one port */
   UCHAR    *DrinkBuf;              /* Drink message is built here */
   ULONG    result;
   ULONG    pClass;
   int      p;                      /* Port number - 1 */
   int      i;                      /* Index into DrinkBuf[] */
   TID      DescramblerThreadId;
   int      errFlag;
   int      status;                 /* Status of time from PC-SG card */
   int      synch;                  /* 1 if PC-SG clock is synched */
   int      synchPrev;              /* Synch value during previous loop */
   int      rc;
   time_t   tstart, tnow;           /* Used to let the Digi box stabilize */
   int      stable;
   ULONG    nBytesRead;
   long     DrinkSize;
   USHORT   commErr;
   ULONG    TimerInterval;
   ULONG    DataLength;
   ULONG    ParmLength;
   char     Text[80];
   static char line[80];            /* To hold a line of keyboard input */
   HEV      Sem;                    /* Event semaphore set by system clock */
   HTIMER   TimerHandle;            /* Timer handle */
   PORT     *port;                  /* Array of port parameters */

/* Check command line arguments 
   ****************************/
   if ( argc != 2 )
   {
      printf( "Usage: getdst2 <configfile>\n" );
      exit( -1 );
   }
           
/* Read the configuration file
   ***************************/
   GetConfig( argv[1] );

/* Look up important info from earthworm.h tables
 ************************************************/
   GetUtilLookup();

/* Open the log file
   *****************/
   logit_init( "getdst2", (short) MyModId, 256, 1 );
           
/* Write the config file parameters to the log file
   ************************************************/
   LogConfig();

/* Log the system clock timer interval
   ***********************************/
   TimerInterval = GetTimerInterval();

   logit( "t", "System clock tick interval: %u.%u msec\n",
                TimerInterval/10, TimerInterval%10 );

/* Initialize logos of drink and output messages
   *********************************************/
   InitLogos();

/* Allocate the port structure
   ***************************/
   port = AllocPortStruct( NPort );
   logit( "t", "Port structure allocated.\n" );

/* Allocate a buffer to accept bytes one port at a time.
   Bytes from one port for one drink are stored here.
   ****************************************************/
   InBuf = AllocInBuf();
   logit( "t", "Port input buffer allocated.\n" );

/* Allocate the drink buffer for this thread.
   The descrambler thread also has a drink buffer.
   Drink messages are built in this buffer.
   The "1" is for the port byte count.
   **********************************************/
   DrinkBuf = AllocDrinkBuf();
   logit( "t", "Drink buffer allocated.\n" );

/* Create the private shared memory ring 
   *************************************/
   tport_create( (SHM_INFO *)&DrinkRegion, 1024*DrinkRingSize, DrinkRingKey );
   logit( "t", "Private shared memory region created.\n" );

/* Attach to the waveform shared memory ring 
   *****************************************/
   tport_attach( (SHM_INFO *)&OutRegion, OutRingKey );
   logit( "t", "Attached to the waveform shared memory ring.\n" );

/* Initialize the descrambler
   **************************/
   Setup();

/* Start the descrambler thread and set its priority
   *************************************************/
   rc = _beginthread( Descrambler, NULL, 8192, (void *)&DrinkRegion );
   if ( rc == -1 )
   {
      logit( "t", "Error starting descrambler thread.  Exiting.\n" );
      exit( -1 );
   }
   DescramblerThreadId = (TID)rc;

   if (      strcmp( dClass, "Idle" ) == 0 )
      pClass = PRTYC_IDLETIME;
   else if ( strcmp( dClass, "Reg" )  == 0 )
      pClass = PRTYC_REGULAR;
   else if ( strcmp( dClass, "Crit" ) == 0 )
      pClass = PRTYC_TIMECRITICAL;
   else if ( strcmp( dClass, "Fore" ) == 0 )
      pClass = PRTYC_FOREGROUNDSERVER;
   else
   {
      logit( "t", "Invalid priority class: %s.  Exiting.\n", dClass );
      exit( -1 );
   }
   rc = DosSetPriority( PRTYS_THREAD, pClass, Priority, DescramblerThreadId );
   if ( rc != 0 )
   {
      logit( "t", "DosSetPriority() error.  rc = %d  Exiting.\n", rc );
      exit( -1 );
   }

/* Set the maximum number of file handles.
   We need one handle for each Digi and True-time port.
   Max file handles must also be changed by setting
   FILES=xxx in config.sys.
   ****************************************************/
   rc = DosSetMaxFH( MaxFileHandle );
   if ( rc )
   {
      logit( "t", "DosSetMaxFH error.  rc = %d.  Exiting.\n", rc );
      exit( -1 );
   }
   logit( "t", "Max number of file handles set to: %d\n", MaxFileHandle );

/* Open the True-Time port
   ***********************/
   if ( OpenTrueTime( MaxPassCount ) == -1 )
   {
      logit( "t", "OpenTrueTime() error.  Exiting.\n" );
      exit( -1 );
   }

/* See if the True-Time clock is synched to IRIG-B
   ***********************************************/
   status = GetTrueTime( (TrueTimeStruct *)&TrueTime );
   synch  = (status < 0) ? 0 : 1;

   if ( !synch )
   {
      LogTrueTimeError( status );
      strcpy( Text, "At startup, can't get time from PC-SG clock.\n" );
      PutStatus( TypeError, ERR_NOSTARTSYNCH, Text );
      logit( "et", "%s", Text );
   }
   else
      logit( "et", "At startup, time available from PC-SG clock.\n" );

   synchPrev = synch;

/* Open the Digi ports and set DCB bits
   ************************************/
   for ( p = 0; p < NPort; p++ )
   {
      int Port;

      Port = p + 1;
      sprintf( port[p].name, "dg%d", Port );

      rc = DosOpen( port[p].name,
                    &port[p].handle,
                    &result,
                    0L,
                    FILE_NORMAL,
                    FILE_OPEN,
                    (OPEN_ACCESS_READWRITE | OPEN_SHARE_DENYREADWRITE),
                    0L );

      if ( rc != 0 )
      {
         logit( "t", "DosOpen() error on port <%s>. rc: %d  Exiting.\n",
                port[p].name, rc );
         exit( -1 );
      }
      SetDCB( port[p].handle );
   }
   logit( "t", "Digi ports opened. Number of ports: %d\n", NPort );

/* Let the digi ports stablilize
   *****************************/
   sleep_ew( 2000 );

/* Create an event semaphore which is set
   by the system clock at a regular interval.
   *****************************************/
   rc = DosCreateEventSem( NULL, &Sem, DC_SEM_SHARED, FALSE );
   if ( rc != NO_ERROR )
   {
      logit( "t", "DosCreateEventSem() error for Sem.  rc = %d  Exiting.\n", rc );
      exit( -1 );
   }
   logit( "t", "Event semaphore created.\n" );

/* Start the system clock timer
   ****************************/
   rc = DosStartTimer( (ULONG)DrinkDelay, (HSEM)Sem, &TimerHandle );
   if ( rc != NO_ERROR )
   {
      logit( "t", "DosStartTimer() error.  rc = %d  Exiting.\n", rc );
      exit( -1 );
   }
   logit( "t", "System clock timer started.\n" );

/* Let the Digi box stabilize starting here
   ****************************************/
   time( &tstart );
   stable = 0;
   logit( "t", "Waiting for the Digi box to stabilize.\n" );

/* Main loop
   *********/
   while ( 1 )
   {
      ULONG PostCount;

/* Hang on the system clock timer
   ******************************/
      rc = DosWaitEventSem( Sem, SEM_INDEFINITE_WAIT ); 
      if ( rc != NO_ERROR )
      {
         logit( "t", "DosWaitEventSem() error.  rc = %d  Exiting.\n", rc );
         exit( -1 );
      }

      rc = DosResetEventSem( Sem, &PostCount ); 
      if ( rc != NO_ERROR )
      {
         logit( "t", "DosResetEventSem() error.  rc = %d  Exiting.\n", rc );
         exit( -1 );
      }

      if ( PostCount > 1 )
      {
         logit( "t", "WARNING: Incoming data lost." );
         logit( "", " PostCount: %u\n", PostCount );
         continue;
      }

/* If termination has been requested, kill program
   ***********************************************/
      if ( tport_getflag( (SHM_INFO *)&OutRegion ) == TERMINATE )
      {
         int MaxPass;

         tport_detach( (SHM_INFO *)&OutRegion ); 
         DosStopTimer( TimerHandle );
         DosCloseEventSem( Sem );
         MaxPass = CloseTrueTime();

         logit( "t", "Maximum PC-SG loop count: %d\n", MaxPass );
         logit( "t", "Termination requested.  Exiting.\n" );
         exit( 0 );
      }

/* Get a maximum of MaxDrinkSize bytes from each Digi port
   *******************************************************/
      i = sizeof(TrueTimeStruct);
      errFlag = 0;

      for ( p = 0; p < NPort; p++ )
      {
         rc = DosRead( port[p].handle, InBuf, MaxDrinkSize, &nBytesRead );
         if ( rc != 0 )
         {
            logit( "t", "DosRead() error.  rc: %d  Exiting.\n", rc );
            exit( -1 );
         }

/* Save the byte count and data bytes
   **********************************/
         DrinkBuf[i++] = (UCHAR)nBytesRead;
         memcpy( &DrinkBuf[i], InBuf, (size_t)nBytesRead );
         i += nBytesRead;

/* Check for Digi receive errors
   *****************************/
         rc = DosDevIOCtl( port[p].handle,   
                           IOCTL_ASYNC,      
                           ASYNC_GETCOMMERROR, 
                           0L,              /* Parameter packet address */
                           0L,              /* Max size of parameter list */
                           &ParmLength,     /* Parm length address (returned) */
                           &commErr,        /* Packet address */
                           sizeof(USHORT),  /* Max size of data packet */
                           &DataLength );   /* Length address (returned) */
         if ( rc != 0 )
         {
            logit( "t", "ASYNC_GETCOMERROR error. rc = %d  Exiting.\n", rc );
            exit( -1 );
         }

/* Log the Digi errors
   *******************/
         errFlag |= LogDigiError( commErr );
      }
      if ( errFlag ) continue;

/* Wait for the Digi system to stabilize
   *************************************/
      if ( !stable )
      {
         const int time_to_stabilize = 1;
         time( &tnow );
         stable = (tnow - tstart > time_to_stabilize) ? 1 : 0;
         if ( stable )
            logit( "et", "The Digi box is now stable.\n" );
         else
            continue;
      }
 
/* After all Digi ports have been scanned, get the current UTC
   time from the PC-SG board.  Send a message to the status manager
   and log file the first time the PC-SG time cannot be obtained.
   ****************************************************************/
      status = GetTrueTime( (TrueTimeStruct *)&TrueTime );
      synch  = (status < 0) ? 0 : 1;

      if ( !synch && synchPrev )
      {
         LogTrueTimeError( status );
         strcpy( Text, "Lost synch to IRIG-B.\n" );
         PutStatus( TypeError, ERR_NOSYNCH, Text );
         logit( "et", "%s", Text );
      }
      if ( synch && !synchPrev )
      {
         strcpy( Text, "Synched to IRIG-B.\n" );
         PutStatus( TypeError, ERR_SYNCH, Text );
         logit( "et", "%s", Text );
      }
      synchPrev = synch;
      if ( !synch ) continue;

/* Copy time from True-Time clock to drink message
   ***********************************************/
      memcpy( &DrinkBuf[0], (TrueTimeStruct *)&TrueTime,
              sizeof(TrueTimeStruct) );

/* Send drink message to private transport ring
   ********************************************/
      DrinkSize = i;
      rc = tport_putmsg( (SHM_INFO *)&DrinkRegion, (MSG_LOGO *)&DrinkLogo,
                         DrinkSize, (char *)DrinkBuf );

      if ( rc == PUT_TOOBIG )
      {
         logit( "t",
            "Drink Buffer[%ld] too long to fit in region.  Exiting.",
            DrinkSize );
         exit( -1 );
      }
      if ( rc == PUT_NOTRACK )
      {
         logit( "t", "Message sequence tracking limit exceeded.  Exiting." );
         exit( -1 );
      }
   }
   return 0;
}

   /******************************************************************
    *                          LogDigiError()                        *
    *                                                                *
    *             Log an error from the Digi interface.              *
    *             Returns 1 if an error was found.                   *
    *                                                                *
    *  According to Physical Device Driver Reference for OS/2        *
    *  p 486, the device driver can return one of the following      *
    *  four errors:                                                  *
    *                                                                *
    *  RX_QUE_OVERRUN: Receive queue overrun.  No room in the        *
    *     physical device driver receive queue to put a character    *
    *     read in from the receive hardware.                         *
    *  RX_HARDWARE_OVERRUN: Receive hardware overrun.  A character   *
    *     was not read from the hardware before the next character   *
    *     arrived, causing a character to be lost.                   *
    *  PARITY_ERROR: The hardware detected a parity error.           *
    *  FRAMING_ERROR: The hardware detected a framing error.         *
    ******************************************************************/

int LogDigiError( USHORT commErr )
{  
   int ErrFlag = 0;

   if ( commErr & RX_QUE_OVERRUN )
   {
      logit( "et", "Digi error.  Receive queue overrun.\n" );
      ErrFlag = 1;
   }

   if ( commErr & RX_HARDWARE_OVERRUN )
   {
      logit( "et", "Digi hardware receive overrun.\n" );
      ErrFlag = 1;
   }

   if ( commErr & PARITY_ERROR )
   {
      logit( "et", "The Digi hardware detected a parity error.\n" );
      ErrFlag = 1;
   }

   if ( commErr & FRAMING_ERROR )
   {
      logit( "t", "The Digi hardware detected a framing error.\n" );
      ErrFlag = 1;
   }

   return ErrFlag;
}  


     /***********************************************************
      *                       GetConfig()                       *
      *                                                         *
      *    Processes command file(s) using kom.c functions.     *
      *    Exits if any errors are encountered.                 *
      ***********************************************************/

#define NCOMMAND 13                /* Number of commands to process */

void GetConfig( char *configfile )
{
   char   init[NCOMMAND];          /* Init flags, one byte for each command */
   int    nmiss;                   /* Number of commands that were missed */
   char  *com;
   char  *str;
   int    nfiles;
   int    success;
   int    i;

/* Set to zero one init flag for each required command 
   ***************************************************/   
   for( i = 0; i < NCOMMAND; i++ )  init[i] = 0;

/* Open the main configuration file 
   ********************************/
   nfiles = k_open( configfile ); 
   if ( nfiles == 0 )
   {
        printf( "Error opening command file <%s>.  Exiting.\n", configfile );
        exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
        while ( k_rd() )           /* Read next line from active file */
        {  
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
           *****************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
           ********************************/
            if( com[0] == '@' )
            {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success )
               {
                  printf( "Error opening command file <%s>.  Exiting.\n",
                          &com[1] );
                  exit( -1 );
               }
               continue;
            }

/* Process anything else as a command 
   **********************************/
            else if ( k_its( "MyModuleId" ) )
            {
                str = k_str();
                if (str) strcpy( MyModuleId, str );
                init[0] = 1;
            }
            else if ( k_its( "RingName" ) )
            {
                str = k_str();
                if (str) strcpy( RingName, str );
                init[1] = 1;
            }
            else if ( k_its( "HeartBeatInterval" ) )
            {
                HeartBeatInterval = k_long();
                init[2] = 1;
            }
            else if ( k_its( "DrinkDelay" ) )
            {
                DrinkDelay = k_int();
                init[3] = 1;
            }
            else if ( k_its( "Class/Priority" ) )
            {
                str = k_str();
                if (str) strcpy( dClass, str );
                Priority = k_int();
                init[4] = 1;
            }
            else if ( k_its( "DrinkRingSize" ) )
            {
                DrinkRingSize = k_int();
                init[5] = 1;
            }
            else if ( k_its( "NPort" ) )
            {
                NPort = k_int();
                init[6] = 1;
            }
            else if ( k_its( "MaxFileHandle" ) )
            {
                MaxFileHandle = k_int();
                init[7] = 1;
            }
            else if ( k_its( "DescramblerDelay" ) )
            {
                DescramblerDelay = k_int();
                init[8] = 1;
            }
            else if ( k_its( "StaFile" ) )
            {
                str = k_str();
                if (str) strcpy( (char *)StaFile, str );
                init[9] = 1;
            }
            else if ( k_its( "Year" ) )
            {
                Year = k_int();
                init[10] = 1;
            }
            else if ( k_its( "MaxDrinkSize" ) )
            {
                MaxDrinkSize = k_int();
                init[11] = 1;
            }
            else if ( k_its( "MaxPassCount" ) )
            {
                MaxPassCount = k_int();
                init[12] = 1;
            }

/* Unknown command
   ***************/ 
            else
            {
                printf( "<%s> Unknown command in <%s>.\n", com, configfile );
                continue;
            }

/* See if there were any errors processing the command 
   ***************************************************/
            if( k_err() )
            {
               printf( "Bad <%s> command in <%s>.  Exiting.\n", com,
                        configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < NCOMMAND; i++ )
      if ( !init[i] ) nmiss++;
   if ( nmiss )
   {
      printf( "ERROR, no " );
      if ( !init[0] )  printf( "<MyModuleId> " );
      if ( !init[1] )  printf( "<RingName> " );
      if ( !init[2] )  printf( "<HeartBeatInterval> " );
      if ( !init[3] )  printf( "<DrinkDelay> " );
      if ( !init[4] )  printf( "<Class/Priority> " );
      if ( !init[5] )  printf( "<DrinkRingSize> " );
      if ( !init[6] )  printf( "<NPort> " );
      if ( !init[7] )  printf( "<MaxFileHandle> " );
      if ( !init[8] )  printf( "<DescramblerDelay> " );
      if ( !init[9] )  printf( "<StaFile> " );
      if ( !init[10] ) printf( "<Year> " );
      if ( !init[11] ) printf( "<MaxDrinkSize> " );
      if ( !init[12] ) printf( "<MaxPassCount> " );
      printf( "command(s) in <%s>.  Exiting.\n", configfile );
      exit( -1 );
   }
}


     /**************************************************************
      *                         LogConfig()                        *
      *                                                            *
      *  Print the configuration file parameters to the log file.  *
      **************************************************************/
           
void LogConfig( void )
{
   logit( "", "\n" );
   logit( "", "            Configuration File\n" );
   logit( "", "            ------------------\n" );
   logit( "", "        DrinkRingSize:      %d kbytes\n", DrinkRingSize );
   logit( "", "        MyModuleId:         %s\n", MyModuleId );
   logit( "", "        RingName:           %s\n", RingName );
   logit( "", "        NPort:              %d\n", NPort );
   logit( "", "        MaxFileHandle:      %u\n", MaxFileHandle );
   logit( "", "        StaFile:            %s\n", StaFile );
   logit( "", "        Year:               %d\n", Year );
   logit( "", "        MaxDrinkSize:       %d bytes per port\n", MaxDrinkSize );
   logit( "", "        DrinkDelay:         %d millisec\n", DrinkDelay );
   logit( "", "        Class/Priority:     %s %u\n", dClass, Priority );
   logit( "", "        DescramblerDelay:   %u millisec\n", DescramblerDelay );
   logit( "", "        HeartBeatInterval:  %ld seconds\n",HeartBeatInterval );
   logit( "", "        MaxPassCount:       %d\n", MaxPassCount );
   logit( "",  "\n" );
}


    /***************************************************************
     *                       GetUtilLookup()                       *
     *                                                             *
     *       Look up important info from earthworm.h tables        *
     ***************************************************************/

void GetUtilLookup( void )
{
   const char DrinkRingName[] = "DRINK_RING";

/* Look up key to drink shared memory region
   *****************************************/
   if ( ( DrinkRingKey = GetKey( (char *)DrinkRingName ) ) == -1 )
   {
      printf( "Invalid ring name <%s>.  Exiting.\n", DrinkRingName );
      exit( -1 );
   }

/* Look up key to output shared memory region
   ******************************************/
   if ( ( OutRingKey = GetKey( RingName ) ) == -1 )
   {
      printf( "Invalid ring name <%s>.  Exiting.\n", RingName );
      exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "Error getting local installation id.  Exiting.\n" );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModuleId, (UCHAR *)&MyModId ) != 0 )
   {
      printf( "Invalid module name <%s>.  Exiting.\n", MyModuleId );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", (UCHAR *)&TypeHeartBeat ) != 0 )
   {
      printf( "Invalid message type <TYPE_HEARTBEAT>.  Exiting.\n" );
      exit( -1 );
   }

   if ( GetType( "TYPE_ERROR", (UCHAR *)&TypeError ) != 0 )
   {
      printf( "Invalid message type <TYPE_ERROR>.  Exiting.\n" );
      exit( -1 );
   }

   if ( GetType( "TYPE_DSTDRINK", &TypeDrink ) != 0 )
   {
      printf( "Invalid message type <TYPE_DSTDRINK>.  Exiting.\n" );
      exit( -1 );
   }

   if ( GetType( "TYPE_TRACEBUF2", &TypeWave ) != 0 )
   {
      printf( "Invalid message type <TYPE_TRACEBUF2>.  Exiting.\n" );
      exit( -1 );
   }
} 


   /***********************************************************************
    *                            PutStatus()                              *
    *                                                                     *
    *  Builds a heartbeat or error message & puts it into shared memory.  *
    *  Also, writes errors to log file.                                   *
    ***********************************************************************/

void PutStatus( UCHAR type, short ierr, char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   long     t;
 
/* Build the message
   *****************/ 
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if ( type == TypeHeartBeat ) sprintf( msg, "%ld\n\0", t );

   if ( type == TypeError ) sprintf( msg, "%ld %hd %s\0", t, ierr, note );

   size = strlen( msg );   /* Don't include the null byte in the message */     

/* Write the message to shared memory
   **********************************/
   if ( tport_putmsg( (SHM_INFO *)&OutRegion, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat )
           logit( "et", "Error sending heartbeat.\n" );

        if( type == TypeError )
           logit( "et", "Error sending error: %d.\n", ierr );
   }
}


         /*********************************************************
          *                       SetDCB()                        *
          *                                                       *
          *  Set Device Control Block (DCB) bits to:              *
          *                                                       *
          *  1) Disable all hardware and software handshaking.    *
          *  2) No blocking on port read.                         *
          *********************************************************/

void SetDCB( HFILE handle )
{
   APIRET  rc;
   DCBINFO DCB;
   ULONG   DataLength, ParmLength;

   rc = DosDevIOCtl( handle,   
                     IOCTL_ASYNC,      
                     ASYNC_GETDCBINFO, 
                     0L,               /* Parameter packet address */
                     0L,               /* Max size of parameter list */
                     &ParmLength,      /* Parm length address (returned) */
                     &DCB,             /* Packet address */
                     sizeof(DCBINFO),  /* Max size of data packet */
                     &DataLength );    /* Length address (returned) */
   if ( rc != 0 )
   {
      logit( "t", "GETDCBINFO error. rc = %d  Exiting.\n", rc );
      exit( -1 );
   }

   DCB.fbCtlHndShake = 0;            
   DCB.fbFlowReplace = 0;
   DCB.fbTimeout     = MODE_NOWAIT_READ_TIMEOUT;

   rc = DosDevIOCtl( handle,  
                     IOCTL_ASYNC,     
                     ASYNC_SETDCBINFO,
                     &DCB,             /* Parameter packet address */
                     sizeof(DCBINFO),  /* Max size of parameter list */
                     &ParmLength,      /* Parm length address (returned) */
                     0L,               /* Packet address */
                     0L,               /* Max size of data packet */
                     &DataLength );    /* Length address (returned) */
   if ( rc != 0 )
   {
      logit( "t", "SETDCBINFO error. rc = %d  Exiting.\n", rc );
      exit( -1 );
   }
}


      /***********************************************************
       *                    GetTimerInterval()                   *
       *                                                         *
       *          Get the system clock timer interval.           *
       ***********************************************************/

ULONG GetTimerInterval( void )
{
   int   rc;
   ULONG TimerInterval;

   rc = DosQuerySysInfo( QSV_TIMER_INTERVAL, QSV_TIMER_INTERVAL,
                         &TimerInterval, sizeof(ULONG) );
   if ( rc != NO_ERROR )
   {
      printf( "DosQuerySysInfo error: rc = %d\n", rc );
      exit( -1 );
   }
   return TimerInterval;
}


      /***********************************************************
       *                        InitLogos()                      *
       *                                                         *
       *     Initialize logos of drink and output messages.      *
       ***********************************************************/

void InitLogos( void )
{

/* Drink messages
   **************/
   DrinkLogo.type   = TypeDrink;
   DrinkLogo.mod    = MyModId;
   DrinkLogo.instid = InstId;

/* Output waveform messages
   ************************/
   WaveLogo.type   = TypeWave;
   WaveLogo.mod    = MyModId;
   WaveLogo.instid = InstId;
}


      /***********************************************************
       *                    AllocPortStruct()                    *
       *                                                         *
       *  Allocate the Digi port structures, one for each port.  *
       ***********************************************************/

PORT *AllocPortStruct( int NPort )
{
   PORT *port = (PORT *) calloc( NPort, sizeof(PORT) );
   if ( port == NULL )
   {
      logit( "t", "Cannot allocate the Digi port structures.  Exiting.\n" );
      exit( -1 );
   }
   return port;
}


     /**********************************************************
      *                      AllocInBuf()                      *
      *                                                        *
      *  Allocate a buffer to accept bytes one port at a time. *
      *  Bytes from one port for one drink are stored here.    *
      **********************************************************/

UCHAR *AllocInBuf( void )
{
   UCHAR *InBuf;

   if ( (InBuf = (char *) malloc( MaxDrinkSize )) == NULL )
   {
      logit( "t", "Cannot allocate the port input buffer.  Exiting.\n" );
      exit( -1 );
   }
   return InBuf;
}


     /**********************************************************
      *                   AllocDrinkBuf()                      *
      *                                                        *
      *  Allocate the drink buffer for this thread.            *
      *  The descrambler thread also has a drink buffer.       *
      *  Drink messages are built in this buffer.              *
      *  The "1" is for the port byte count.                   *
      **********************************************************/

UCHAR *AllocDrinkBuf( void )
{
   UCHAR *DrinkBuf;
   size_t DrinkBufSize;

   DrinkBufSize = sizeof(TrueTimeStruct) + NPort*(MaxDrinkSize + 1);

   if ( (DrinkBuf = (char *) malloc( DrinkBufSize )) == NULL )
   {
      logit( "t", "Cannot allocate the drink buffer.  Exiting.\n" );
      exit( -1 );
   }
   return DrinkBuf;
}


     /******************************************************
      *                 LogTrueTimeError()                 *
      *                                                    *
      *  Log the GetTrueTime() status code                 *
      ******************************************************/

void LogTrueTimeError( int status )
{
   if ( status >= 0 ) return;         /* No error */

   logit( "et", "GetTrueTime() error.  " );

   if ( status == -1 )
      logit( "e", "DosDevIOCtl error.\n" );

   if ( status == -2 )
      logit( "e", "MaxPassCount(%d) exceeded.\n", MaxPassCount );

   if ( status == -3 )
      logit( "e", "PC-SG translator input error.\n" );

   if ( status == -4 )
      logit( "e", "PC-SG oscillator time base failure.\n" );
   return;
}
