#
#                            File getdst2.d
#              Configuration file for the getdst2 program.
#
#                        Ring-info parameters
#                        --------------------
DrinkRingSize      256         # Size of private ring, in kilobytes
MyModuleId         MOD_GETDST2 # Module id for this instance of getdst
RingName           WAVE_RING   # Shared memory ring for output
#
#              TrueTime PC-SG data collection parameter
#              ----------------------------------------
MaxPassCount       100         # Poll the READ_READY bit at most this many times
#
#                 DST and data collection parameters
#                 ----------------------------------
# Port numbers go from one to NPort.  NPort must be set to some value
# less than or equal to the number of physical ports in the system.
# Year must be changed manually at the end of each year.
#
NPort              15          # Number of Digi ports
MaxFileHandle      30          # One handle is used by each Digi port.
                               # Also, set FILES=xxx in config.sys.
StaFile       "sta_list2.dst"  # File containing station name/pin# info
Year               2004        # The current year
#
#                        Drinker parameters
#                        ------------------
# If DrinkDelay is set too small, the program won't be able to keep up
# with incoming data. If DrinkDelay is set too large, overruns will occur.
#
MaxDrinkSize       256         # Bytes per port. With the current DigiBoard
                               # system, the maximum useful value is 256.
DrinkDelay         60          # Sleep this many milliseconds between drinks
#
#                      Descrambler parameters
#                      ----------------------
# The Class should be Crit.  Set the priority to something
# less than the priority given by startstop.
# The value of DescramblerDelay is not too critical, since drinks are
# buffered in a private ring.
#
Class/Priority     Crit 10     # For the descrambler thread only
DescramblerDelay    200        # Descrambler thread sleeps this many msec
#
#                    Status manager reporting
#                    ------------------------
HeartBeatInterval     30      # Seconds between heartbeats
