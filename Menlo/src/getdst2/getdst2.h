
                       /****************************
                        *        getdst2.h         *
                        ****************************/

#ifndef GETDST2_H
#define GETDST2_H

#include <trace_buf.h>

/* Define some constants (used below)
   **********************************/
#define MAXDRINK  150          /* Maximum number of drinks per message */
#define NCHAN       3          /* Number of channels per port */

/* Status manager error codes
   **************************/
#define ERR_NOSTARTSYNCH  1    /* No IRIG-B synch at startup */
#define ERR_NOSYNCH       2    /* True-Time IRIG-B synch lost */
#define ERR_SYNCH         3    /* True-Time IRIG-B synch attained */
#define ERR_MISSMSG       4    /* Message missed in private transport ring */
#define ERR_LOSTSYNCH     5    /* Bad synch and/or time bytes in data stream */

/* Port and channel structure definitions
   **************************************/
typedef struct
{
   char       name[6];           /* Port device name, eg dg1 */
   HFILE      handle;            /* File handle (int) of each digi port */
} PORT;

typedef struct
{
   double        delay;          /* Station delay in seconds */
   long          *Wb2;           /* Work buffer 2 */
   UCHAR         *OutBuf;        /* Output message buffer */
   TRACE2_HEADER *hd;            /* Pointer to output message header */
   long          *dat;           /* Pointer to data area of OutBuf */
} CHAN;

typedef struct
{
   CHAN       ch[NCHAN];             /* Channel-specific information */
   int        Status;                /* Station status */
   UCHAR      *Wb1;                  /* Work buffer 1 */
   int        nBytesInWb;            /* Number of bytes in WorkBuf */
   int        nDrink;                /* Number of drinks in Workbuf */
   int        DrinkIndex[MAXDRINK];  /* Workbuf index of last byte in drink */
   double     DrinkTime[MAXDRINK];   /* PC-SG time of drink as a double */
   TrueTimeStruct DrinkTS[MAXDRINK]; /* PC-SG time of drink as a struct */
   double     WbStartTime;           /* Work buffer start time */
} STATION;

#endif

