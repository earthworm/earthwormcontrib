
     /**********************************************************
      *                        descramb.c                      *
      *                                                        *
      *  This file contains the DST descrambler functions for  *
      *  the getdst2 program.  These functions execute as a    *
      *  thread.                                               *
      **********************************************************/

#define INCL_DOS
#define INCL_DOSFILEMGR
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#define INCL_DOSDATETIME
#define INCL_DOSMISC        /* For DosQuerySysInfo */
#define INCL_ERRORS         /* For DosQuerySysInfo */
#include <os2.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <time_ew.h>
#include <math.h>
#include <transport.h>
#include <truetime.h>
#include <earthworm.h>
#include <trace_buf.h>
#include <chron3.h>
#include "getdst2.h"

#define DEAD  0
#define ALIVE 1

/* These variables are declared in getdst2.c
   *****************************************/
extern SHM_INFO DrinkRegion;
extern SHM_INFO OutRegion;
extern MSG_LOGO DrinkLogo;          /* Logo of drink messages */
extern MSG_LOGO WaveLogo;           /* Logo of output waveform messages */
extern long     HeartBeatInterval;  /* Seconds between heartbeats */
extern int      NPort;              /* Number of Digi ports */
extern unsigned DescramblerDelay;   /* Descrambler sleeps this many msec */
extern char     StaFile[];          /* Name of dst station file */
extern int      Year;               /* The current year, eg 1997 */
extern int      MaxDrinkSize;       /* Per port, in bytes */
extern UCHAR    MyModId;
extern UCHAR    TypeHeartBeat; 
extern UCHAR    TypeError;
extern TrueTimeStruct TrueTime;

/* Define some constants here
   **************************/
static const long MissingData = 0x01000000;

static const UCHAR StartByte = 0x0d;
static const UCHAR StopByte  = 0x0a;

static const int Dead     = DEAD;        /* Dead station */
static const int Alive    = ALIVE;       /* Living station */
static const int nScan    =  100;        /* Number of samples per message */
static const int WbSize   = 3000;        /* Work buffer size in bytes */
static const int MaxDrink = MAXDRINK;    /* Max number of drinks per msg */
static const int nChan    = NCHAN;       /* Number of channels per port */

static const double SamplingRate = 100.;   /* Nominal sampling rate */

static const char DataType[] = "i4";      /* i4 = Intel-byte order long int */

/* Look-up table to convert from BCD to binary
   *******************************************/
static const unsigned char BTB[] =
   { 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 0, 0, 0, 0, 0, 0,
    10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0, 0, 0, 0, 0, 0,
    20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 0, 0, 0, 0, 0, 0,
    30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 0, 0, 0, 0, 0, 0,
    40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 0, 0, 0, 0, 0, 0,
    50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 0, 0, 0, 0, 0, 0,
    60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 0, 0, 0, 0, 0, 0,
    70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 0, 0, 0, 0, 0, 0,
    80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 0, 0, 0, 0, 0, 0,
    90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 0, 0, 0, 0, 0, 0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0 };

/* Look-up table to convert from binary to BCD
   *******************************************/
static const unsigned char BTB2[] =
   {   0,   1,   2,   3,   4,   5,   6,   7,   8,   9,
      16,  17,  18,  19,  20,  21,  22,  23,  24,  25,
      32,  33,  34,  35,  36,  37,  38,  39,  40,  41,
      48,  49,  50,  51,  52,  53,  54,  55,  56,  57,
      64,  65,  66,  67,  68,  69,  70,  71,  72,  73,
      80,  81,  82,  83,  84,  85,  86,  87,  88,  89,
      96,  97,  98,  99, 100, 101, 102, 103, 104, 105,
     112, 113, 114, 115, 116, 117, 118, 119, 120, 121,
     128, 129, 130, 131, 132, 133, 134, 135, 136, 137,
     144, 145, 146, 147, 148, 149, 150, 151, 152, 153 };

/* Function Prototypes
   *******************/
char    *AllocDrnkBuf( size_t );
void    AllocWorkBuf( STATION [] );
void    AllocOutBuf( STATION [] );
void    ProcessDrink( STATION [], UCHAR * );
void    StationDead( STATION [], int, TrueTimeStruct *, double, int, UCHAR * );
void    StationAlive( STATION [], int, TrueTimeStruct *, double, int, UCHAR * );
void    ResetWb2( STATION [], int );
void    LogWb1( STATION [], int );
void    BuildWb2( STATION [], int,  int, int );
void    GetStaFile( STATION [] );
double  TrueTimeToDsec( TrueTimeStruct *, int );
double  TimeOfTimeByte( STATION [], int, TrueTimeStruct *, double, int );
void    WriteMsg( STATION [], int );
void    PrintTrueTime( void );
STATION *AllocStaStruct( int );
void    InitStaStruct( STATION [] );
void    PutStatus( unsigned char, short, char * );
int     InSynch( UCHAR *, int );
int     GetFrame( STATION [], int, int * );
void    GetFrameTime( STATION [], int, int, double * );
void    ResetWb1( STATION [], int, int );

/* Some more globals
   *****************/
static long      timeNow;             /* Current time */       
static long      timeLastBeat;        /* Time last heartbeat was sent */
static STATION   *st;                 /* Array of station parameters */
static UCHAR     *DrinkBuf;           /* Buffer to contain drink messages */
static size_t    DrinkBufSize;


    /***************************************************************
     *                           Setup()                           *
     *                                                             *
     *  Initialize the descrambler.                                *
     ***************************************************************/

void Setup( void )
{
   int       rc;

/* Do this to get the correct time from gmtime()
   *********************************************/
   if ( getenv( "TZ" ) != NULL ) _tzset();

/* Allocate DrinkBuf
   *****************/
   DrinkBufSize = sizeof(TrueTimeStruct) + NPort*(MaxDrinkSize + 1);
   DrinkBuf = AllocDrnkBuf( DrinkBufSize );
   logit( "t", "Drink buffer allocated.\n" );

/* Allocate station structures
   ***************************/
   st = AllocStaStruct( NPort );
   logit( "t", "Station structures allocated.\n" );

/* Allocate work buffers
   *********************/
   AllocWorkBuf( st );
   logit( "t", "Work buffers allocated.\n" );

/* Allocate output message buffers
   *******************************/
   AllocOutBuf( st );
   logit( "t", "Descrambler output message buffers allocated.\n" );

/* Initialize some values in the station
   structures and the output messages
   *************************************/
   InitStaStruct( st );
   logit( "t", "Station structures initialized.\n" );

/* Get pin numbers and station codes from the
   station file and write to the log file
   ******************************************/
   GetStaFile( st );
   logit( "t", "Pin numbers and station codes read from file.\n" );

/* Beat the heart in first pass through main loop
   **********************************************/
   timeLastBeat = time(&timeNow) - HeartBeatInterval - 1;

/* Print status codes to screen
   ****************************/
/* printf( "\nStatus codes:\n" );
   printf( "   0 = Port dead.\n" );
   printf( "   1 = Port alive.\n\n" ); */
   return;
}


    /***************************************************************
     *                        Descrambler()                        *
     *                                                             *
     *  Thread function to initiate descrambling of DST messages.  *
     *  It gets drinker messages and sends heartbeats to the       *
     *  output transport ring.  Then, it calls ProcessDrink to     *
     *  descramble all the drinks in a message.                    *
     ***************************************************************/

void Descrambler( void *dummy )
{
   int       rc;
   long      recsize;             /* Size of retrieved message */
   MSG_LOGO  reclogo;             /* Logo of retrieved message */

/* Descrambler main loop
   *********************/
   logit( "t", "Entering descrambler main loop.\n" );

/* Send heartbeat message to waveform ring
   ***************************************/
   while ( 1 )
   {
      if  ( (time(&timeNow) - timeLastBeat) >= HeartBeatInterval ) 
      {
         timeLastBeat = timeNow;
         PutStatus( TypeHeartBeat, 0, "" ); 
      }

/* Get a message and check the return code
   from the private transport ring
   ***************************************/
      while ( 1 )
      {
         const short nLogo = 1;         /* Number of logos to look for */
         rc = tport_getmsg( &DrinkRegion, &DrinkLogo, nLogo, &reclogo,
                            &recsize, DrinkBuf, DrinkBufSize );

         if ( rc == GET_NONE )          /* No more new messages     */
            break;

         if ( rc == GET_TOOBIG )        /* Next message was too big */
         {                              /* Complain and try again   */
            logit( "t", 
            "Retrieved msg[%ld] (i%u m%u t%u) too big for DrinkBuf[%d].  Exiting.",
                     recsize, reclogo.instid, reclogo.mod, reclogo.type, 
                     DrinkBufSize );
            exit( -1 );
         }

         if ( rc == GET_NOTRACK )       /* Got a message, but can't tell */
         {                              /* if any were missed            */
            logit( "t",
            "Msg received (i%u m%u t%u); transport.h NTRACK_GET exceeded.  Exiting.",
             reclogo.instid, reclogo.mod, reclogo.type );
            exit( -1 );
         }

         if ( rc == GET_MISS )          /* Got a message, but missed some */
         {
            char Text[150];
            sprintf( Text, "Missed message(s) on private transport ring.\n" );
            PutStatus( TypeError, ERR_MISSMSG, Text );
            logit( "et", "%s", Text );
         }

/* Process all data in one drink
   *****************************/
         ProcessDrink( st, DrinkBuf );
      }                                  /* End of message-processing loop */

/* Sleep before getting more drink messages
   ****************************************/
      sleep_ew( DescramblerDelay );
   }
}


        /*******************************************************
         *                   ProcessDrink()                    *
         *                                                     *
         *  Descrambles one DST drink.                         *
         *                                                     *
         *  DrinkBuf = The drink buffer to process             *
         *******************************************************/

void ProcessDrink( STATION st[], UCHAR *DrinkBuf )
{
   int     s;
   double  TimeStamp;          /* PC-SG time in sec since midnight 1/1/70 */
   static  count = 0;

/* Pointer to bytes within the drink
   *********************************/
   UCHAR *DrinkPtr = DrinkBuf + sizeof(TrueTimeStruct);

/* Pointer to structure containing the time stamp
   **********************************************/
   TrueTimeStruct *TS = (TrueTimeStruct *)DrinkBuf;

/* Convert time from the true-time structure
   to seconds since Jan 1, 1970
   *****************************************/
   TimeStamp = TrueTimeToDsec( TS, Year );

/* Process one station at a time
   *****************************/
   for ( s = 0; s < NPort; s++ )
   {
      int DrinkSize;

/* Unpack the port
   ***************/
      DrinkSize = *(DrinkPtr++);

      switch ( st[s].Status )
      {
         case DEAD:
         StationDead( st, s, TS, TimeStamp, DrinkSize, DrinkPtr );
         break;

         case ALIVE:
         StationAlive( st, s, TS, TimeStamp, DrinkSize, DrinkPtr );
         break;
      }

      DrinkPtr += DrinkSize;
   }

/* Display the status of each station
   **********************************/
/* PrintTrueTime();
   printf( " Port status:" );

   for ( s = 0; s < NPort; s++ )
   {
      if ( (s % 5) == 0 ) putchar( ' ' );
      printf( "%d", st[s].Status );
   }

   putchar( '\r' );
   fflush( stdout ); */
}


    /*******************************************************************
     *                         StationDead()                           *
     *                                                                 *
     *    The station is dead.  See if it should be declared alive.    *
     *                                                                 *
     *  s             = Station number - 1                             *
     *  TS            = Time stamp (struct)                            *
     *  TimeStamp     = Time stamp (double)                            *
     *  DrinkSize     = Number of bytes pointed to by DrinkBuf         *
     *  DrinkBuf      = Pointer to start of data bytes for this port   *
     *******************************************************************/

void StationDead( STATION st[], int s, TrueTimeStruct *TS, double TimeStamp,
                  int DrinkSize, UCHAR *DrinkBuf )
{
   int c;                     /* Channel index */
   int nExtraBytes;
   int rc;
   double tFirst;

/* See if the drink is in synch.
   rc = -1, if the drink is not in synch.
   rc = index of first frame if the drink is in synch.
   **************************************************/
   rc = InSynch( DrinkBuf, DrinkSize );

   if ( rc < 0 )
      return;                 /* The station is still dead */
   else
      nExtraBytes = rc;

/* We just acquired synch.
   Copy bytes to work buffer 1.
   ***************************/
   st[s].nBytesInWb = DrinkSize - nExtraBytes;
   memcpy( st[s].Wb1, DrinkBuf+nExtraBytes, st[s].nBytesInWb );

/* Store information about this drink
   **********************************/
   st[s].DrinkIndex[0] = st[s].nBytesInWb;
   st[s].DrinkTime[0]  = TimeStamp;
   st[s].DrinkTS[0]    = *TS;
   st[s].nDrink = 1;

/* Reset work buffer 2
   *******************/
   ResetWb2( st, s );

/* Get the time of the first sample in the new message.
   The sample time is the time of the first byte of channel 0.
   The index of this byte is 5.
   Set the start time in the output message headers.
   ***************************************************/
   tFirst = TimeOfTimeByte( st, s, TS, TimeStamp, 5 );
   st[s].WbStartTime = tFirst;

/* Declare station alive
   *********************/
/* printf( "Station %s alive\n", st[s].ch[0].hd->sta ); */
   st[s].Status = Alive;
   return;
}


    /*******************************************************************
     *                         StationAlive()                          *
     *                                                                 *
     *    The station is alive.  See if it should be declared dead.    *
     *                                                                 *
     *  s             = Station number - 1                             *
     *  TS            = Time stamp (struct)                            *
     *  TimeStamp     = Time stamp (double)                            *
     *  DrinkSize     = Number of bytes pointed to by DrinkBuf         *
     *  DrinkBuf      = Pointer to start of data bytes for this port   *
     *******************************************************************/

void StationAlive( STATION st[], int s, TrueTimeStruct *TS, double TimeStamp,
                   int DrinkSize, UCHAR *DrinkBuf )
{
   int    i, c;
   UCHAR  *newBytesHere = st[s].Wb1 + st[s].nBytesInWb;
   int    nValid;
   int    frameIndex;
   int    indexLastFrame;
   int    mbiFirstFrame;
   double timeFirstFrame;

/* Copy bytes to the work buffer
   *****************************/
   memcpy( newBytesHere, DrinkBuf, DrinkSize );
   st[s].nBytesInWb += DrinkSize;

/* Store information about this drink
   **********************************/
   st[s].DrinkIndex[st[s].nDrink] = st[s].nBytesInWb;
   st[s].DrinkTime[st[s].nDrink]  = TimeStamp;
   st[s].DrinkTS[st[s].nDrink]    = *TS;
   if ( ++st[s].nDrink == MaxDrink )
   {
      logit( "et", "getdst2 fatal error 1.  Exiting.\n" );
      exit( -1 );
   }

/* We need 1.10 seconds of data to ensure the message is complete
   **************************************************************/
   if ( (TimeStamp - st[s].WbStartTime) < 1.10 )
      return;

/* for ( i = 0; i < st[s].nDrink; i++ )
      logit( "e", "DrinkIndex[%d]: %4d  DrinkTime: %.3lf\n", i,
             st[s].DrinkIndex[i], st[s].DrinkTime[i] ); */

/* LogWb1( st, s ); */

/* Identify a valid frame.
   GetFrame() returns 1 if a frame was found,
   or 0 if there are no more frames in the message.
   frameIndex is the index of the stop byte at the
   beginning of the frame.
   ***********************************************/
   nValid     = 0;
   frameIndex = -1;

   while ( GetFrame( st, s, &frameIndex ) )
   {
      double frameTime;
      int    mbi;           /* Message buffer index */

/* Get the time of the frame
   *************************/
      GetFrameTime( st, s, frameIndex, &frameTime );

/* Compute the message buffer index
   ********************************/
      mbi = (int)(100. * (frameTime - st[s].WbStartTime) + 0.5);
      if ( mbi < 0 )
      {
/*       logit( "et", "%s: mbi = %d\n", st[s].ch[0].hd->sta, mbi ); */
/*       printf( "Station %s lost synch\n", st[s].ch[0].hd->sta ); */
         st[s].Status = Dead;
         return;
      }
      if ( mbi > 99 ) break;
      nValid++;

/* Save the index and time of the first frame
   ******************************************/
      if ( nValid == 1 )
      {
         mbiFirstFrame  = mbi;
         timeFirstFrame = frameTime;
      }

/* Save the index of the last frame in the output message
   ******************************************************/
      indexLastFrame = frameIndex;

/* Copy one sample per channel to the 2'nd work buffers
   ****************************************************/
      BuildWb2( st, s, frameIndex, mbi );

   }     /* end while */

/* If there are no valid frames in the
   message, declare the station dead
   ***********************************/
   if ( nValid == 0 )
   {
/*    printf( "Station %s dead\n", st[s].ch[0].hd->sta ); */
      st[s].Status = Dead;
      return;
   }
   if ( nValid < 100 )
   {
      printf( "Station %s: %2d sample", st[s].ch[0].hd->sta, 100-nValid );
      if ( 100-nValid > 1 ) printf( "s" );
      printf( " lost\n" );
   }
   if ( nValid > 100 )
   {
/*    logit( "et", "%s: nValid = %2d\n", st[s].ch[0].hd->sta, nValid ); */
/*    printf( "Station %s dead\n", st[s].ch[0].hd->sta ); */
      st[s].Status = Dead;
      return;
   }

/* Recompute the start time of work buffer 1
   using the time of the first valid frame
   *****************************************/
   st[s].WbStartTime = timeFirstFrame - (0.01 * (double)mbiFirstFrame);

/* Send the message(s) to the waveform ring
   ****************************************/
   WriteMsg( st, s );

/* Save any bytes after the last valid frame
   copied to the message buffer
   *****************************************/
   ResetWb1( st, s, indexLastFrame );

/* Reset the second work buffer
   ****************************/
   ResetWb2( st, s );

   return;
}


      /***********************************************************
       *                     AllocStaStruct()                    *
       *                                                         *
       *  Allocate a structure for each station.                 *
       ***********************************************************/

STATION *AllocStaStruct( int NPort )
{
   STATION *st;

   st = (STATION *) calloc( NPort, sizeof(STATION) );
   if ( st == NULL )
   {
      logit( "t", "Cannot allocate the station structures.  Exiting.\n" );
      exit( -1 );
   }
   return st;
}


   /******************************************************************
    *                         InitStaStruct()                        *
    *                                                                *  
    *  Initialize some values in the station structure.              *
    ******************************************************************/

void InitStaStruct( STATION st[] )
{
   int s;                              /* Station number - 1 */
   int c;                              /* Channel number */

   for ( s = 0; s < NPort; s++ )
   {
      st[s].Status     = Dead;         /* Station is assumed dead */
      st[s].nBytesInWb = 0;            /* Work buffer is empty */
      st[s].nDrink     = 0;            /* No drinks in work buffer */

      for ( c = 0; c < nChan; c++ )
      {
         st[s].ch[c].hd->samprate = SamplingRate;
         strcpy( st[s].ch[c].hd->datatype, DataType );
         st[s].ch[c].hd->version[0] = TRACE2_VERSION0;
         st[s].ch[c].hd->version[1] = TRACE2_VERSION1;
      }
   }
   return;
}


     /****************************************************************
      *                           LogWb1()                           *
      *                                                              *
      *  Dump work buffer 1 in hex to the log file.                  *
      ****************************************************************/

void LogWb1( STATION st[], int s )
{
   int Port = s + 1;
   int j;

   if ( st[s].nBytesInWb == 0 ) return;

   logit( "t", "Port %2d. Work buffer contains %d bytes.\n", Port,
          st[s].nBytesInWb );

   for ( j = 0; j < st[s].nBytesInWb; j++ )
   {
      int k;
      k = j % 16;
      if ( k == 0  ) logit( "", "  " );
      logit( "", " %02x", st[s].Wb1[j] );
      if ( k == 15 ) logit( "", "\n" );
   }
   if ( (st[s].nBytesInWb % 16) != 0 ) logit( "", "\n" );
   return;
}


        /********************************************************
         *                     GetStaFile()                     *
         *                                                      *
         *  Get pin numbers and SCNLs from the station file     *
         *  and load them into the port structures.             *
         ********************************************************/

void GetStaFile( STATION st[] )
{
   FILE   *fpStaFile;
   int    s;
   int    c;                    /* Channel number */
   int    Port;                 /* Port number */
   int    Chan;                 /* from 0 to nChan-1 */
   int    Pin;                  /* Pin number */
   int    mid;                  /* Module id */
   static char   string[1000];
   char   site[6];              /* Site code */
   char   comp[4];              /* Component/channel code */
   char   net[3];               /* Network code */
   char   loc[3];               /* Location code */
   double delay;                /* Station delay in seconds */

/* Initialize pin numbers to -1
   ****************************/
   for ( s = 0; s < NPort; s++ )
      for ( c = 0; c < nChan; c++ )
         st[s].ch[c].hd->pinno = -1;

/* Open station file and read it's contents
   ****************************************/
   if ( ( fpStaFile = fopen( StaFile, "r") ) == NULL )
   {
      logit( "t", "Error opening station file <%s>.  Exiting.\n",
             StaFile );
      exit( -1 );
   }

   while ( fgets( string, 1000, fpStaFile ) != NULL )
   {
      if ( strlen( string ) < 2 )              /* Skip empty lines */
         continue;

      if ( strncmp( string, "//", 2 ) == 0 )   /* Skip comment lines */
         continue;

      if ( sscanf( string, "%d%d%d%d%s%s%s%s%lf", &mid, &Port, &Chan, &Pin,
                   site, comp, net, loc, &delay ) < 9 )
      {
         logit( "et", "Error decoding station file.  Offending line:\n" );
         logit( "", "%s", string );
         exit( -1 );
      }

      if ( mid != (int)MyModId )               /* Get only pins for */
         continue;                             /* this module. */

      if ( (Port < 1) || (Port > NPort) )      /* Invalid port number */
         continue;

      if ( (Chan < 0) || (Chan >= nChan) )     /* Invalid channel number */
         continue;

      if ( (Pin < 0) || (Pin > 32767) )        /* Invalid pin number */
         continue;

      s = Port - 1;
      c = Chan;
/*    logit( "e", "s: %d  c: %d\n", s, c ); */

      st[s].ch[c].hd->pinno = Pin;
      strcpy( st[s].ch[c].hd->sta,  site );
      strcpy( st[s].ch[c].hd->chan, comp );
      strcpy( st[s].ch[c].hd->net,  net );
      strcpy( st[s].ch[c].hd->loc,  loc );
      st[s].ch[c].delay = delay;
   }

   fclose( fpStaFile );

/* Write pin numbers and SCNLs to log file
   ***************************************/
   logit( "", "\n" );
   logit( "", "                         Pin Numbers and SCNLs\n" );
   logit( "", "                         ---------------------\n" );
   logit( "", " Port   Pin     SCNL       Delay     Pin    SCNL      Delay     Pin    Station     Delay\n" );

   for ( s = 0; s < NPort; s++ )
   {
      Port = s + 1;

      logit( "", "  %2d", Port );
      for ( c = 0; c < nChan; c++ )
      {
         logit( "", "   %4d",  st[s].ch[c].hd->pinno );
         logit( "", "  %-5s",  st[s].ch[c].hd->sta );
         logit( "", "%-3s",    st[s].ch[c].hd->chan );
         logit( "", "%-2s",    st[s].ch[c].hd->net );
         logit( "", "%-2s",    st[s].ch[c].hd->loc );
         logit( "", "  %.4lf", st[s].ch[c].delay );
      }
      logit( "", "\n" );
   }
   logit( "", "\n" );

/* Make sure all pin numbers have been initialized
   ***********************************************/
   for ( s = 0; s < NPort; s++ )
   {
      Port = s + 1;

      for ( c = 0; c < nChan; c++ )
      {
         if ( st[s].ch[c].hd->pinno == -1 )
         {
            logit( "et",
                  "Pin not initialized.  Port %2d.  Chan %d.  Exiting.\n",
                   Port, c );
            exit( -1 );
         }
      }
   }
   return;
}


     /************************************************************
      *                     TimeOfTimeByte()                     *
      *                                                          *
      *  Compute the full time (as a double) of any time byte    *
      *  in the buffer.  We assume the time stamp is no later    *
      *  than 100 msec after the last time byte in the drink.    *
      *                                                          *
      *  TS    = Time stamp expressed as a struct                *
      *  index = Index of time byte to calculate                 *
      ************************************************************/

double TimeOfTimeByte( STATION st[], int s, TrueTimeStruct *TS,
                       double TimeStamp, int index )
{
   int dt, deltat, i, j;

/* Compute index (j) of last time byte in the work
   buffer.  This byte has the last odd index number.
   ************************************************/
   j = (st[s].nBytesInWb % 2) ? st[s].nBytesInWb-2: st[s].nBytesInWb-1;

/* Compute deltat, the relative time of last time byte in
   the work buffer.  We assume the time stamp is between
   0 and 99 msec after the last time byte in the drink.
   deltat will be between 0 and 99.
   ******************************************************/
   dt = 10*TS->TensMillisecs + TS->UnitMillisecs - BTB[st[s].Wb1[j]];
   deltat = (dt < 0) ? (dt + 100): dt;

/* Get relative time of the time byte of interest.
   deltat will be between 0 and 99.
   **********************************************/
   for ( i = index; i < j; i+=2 )
   {
      dt = BTB[st[s].Wb1[i+2]] - BTB[st[s].Wb1[i]];
      deltat += (dt < 0) ? (dt + 100): dt;
   }

/* Return the time of our preferred time byte
   ******************************************/
   return( TimeStamp - (double)deltat / 1000. );
}


       /***********************************************************
        *                    TrueTimeToDsec()                     *
        *                                                         *
        *  Accepts the current year plus a true-time structure    *
        *  and returns the number of seconds since midnight       *
        *  Jan 1, 1970.  Since IRIG-B does not contain year       *
        *  information, the year is passed as an argument.        *
        *                                                         *
        *  TS   = Time structure from True-Time clock             *
        *  Year = Current year, eg 1996 (from the config file)    *
        ***********************************************************/

double TrueTimeToDsec( TrueTimeStruct *TS, int Year )
{
   struct Greg g;
   int days, hours, minutes, seconds, milliseconds;

/* Jan 1, 1970 minus Jan 1, 1600, in sec
   *************************************/
   static const double magic = 11676096000.;

/* Convert TrueTime to days hours, minutes, seconds, and milliseconds
   ******************************************************************/
   days         = 100 * (int)TS->HundredsDays +
                   10 * (int)TS->TensDays +
                        (int)TS->UnitDays;
   hours        =  10 * (int)TS->TensHours +
                        (int)TS->UnitHours;
   minutes      =  10 * (int)TS->TensMinutes +
                        (int)TS->UnitMinutes;
   seconds      =  10 * (int)TS->TensSeconds +
                        (int)TS->UnitSeconds;
   milliseconds = 100 * (int)TS->HundredsMillisecs +
                   10 * (int)TS->TensMillisecs +
                        (int)TS->UnitMillisecs;

/* Convert to seconds since midnight Jan 1, 1970
   *********************************************/
   g.year   = Year;
   g.month  = g.day    = 1;
   g.hour   = g.minute = 0;
   g.second = 0.0;

   return ( (60.0 * julmin(&g)) - magic +
           (86400. * (days - 1)) +
           (3600. * hours) +
           (60. * minutes) +
           (double)seconds +
           (0.001 * milliseconds) );
}


     /****************************************************************
      *                         AllocDrnkBuf()                       *
      *                                                              *
      *  Allocate a buffer to hold messages from the private ring    *
      ****************************************************************/

char *AllocDrnkBuf( size_t DrinkBufSize )
{
   char *DrinkBuf = (char *) malloc( DrinkBufSize );

   if ( DrinkBuf == NULL )
   {
      logit( "t", "ERROR.  Cannot allocate drink buffer.  Exiting.\n" );
      exit( -1 );
   }
   return DrinkBuf;
}


     /****************************************************************
      *                         AllocWorkBuf()                       *
      *                                                              *
      *  Allocate work buffers for each port                         *
      ****************************************************************/

void AllocWorkBuf( STATION st[] )
{
   int s;
   int c;
   int Wb2Size = nScan * sizeof(long);

/* First work buffer
   *****************/
   for ( s = 0; s < NPort; s++ )
   {
      st[s].Wb1 = (UCHAR *) malloc( (size_t) WbSize );
      if ( st[s].Wb1 == NULL )
      {
         logit( "t", "Error allocating 1'st work buffers.  Exiting.\n" );
         exit( -1 );
      }

/* Second work buffer
   ******************/
      for ( c = 0; c < nChan; c++ )
      {
         st[s].ch[c].Wb2 = (long *) malloc( (size_t) Wb2Size );
         if ( st[s].ch[c].Wb2 == NULL )
         {
            logit( "t", "Error allocating 2'nd work buffers.  Exiting.\n" );
            exit( -1 );
         }
      }
   }
}


     /****************************************************************
      *                         AllocOutBuf()                        *
      *                                                              *
      *  Allocate a buffer to hold each output message               *
      ****************************************************************/

void AllocOutBuf( STATION st[] )
{
   int s, c;
   int OutBufSize;

/* Output waveform samples are longs
   *********************************/
   OutBufSize = sizeof(TRACE2_HEADER) + ( sizeof(long) * nScan );

/* One buffer for each channel
   ***************************/
   for ( s = 0; s < NPort; s++ )
   {
      for ( c = 0; c < nChan; c++ )
      {
         st[s].ch[c].OutBuf = (UCHAR *) malloc( (size_t) OutBufSize );
         if ( st[s].ch[c].OutBuf == NULL )
         {
            logit( "t", "Error allocating output buffers.  Exiting.\n" );
            exit( -1 );
         }
         st[s].ch[c].hd  = (TRACE2_HEADER *) st[s].ch[c].OutBuf;
         st[s].ch[c].dat = (long *)(st[s].ch[c].OutBuf + sizeof(TRACE2_HEADER));
      }
   }
}


     /*********************************************************
      *                    PrintTrueTime()                    *
      *                                                       *
      *        Print the time from the Truetime clock.        *
      *                                                       *
      *  Requires time from a true-time card.                 *
      *  Time is stored in the TrueTime structure.            *
      *********************************************************/

void PrintTrueTime( void )
{
   int  hours, minutes, seconds, millisec;

   hours    =  10 * (int)TrueTime.TensHours +
                    (int)TrueTime.UnitHours;
   minutes  =  10 * (int)TrueTime.TensMinutes +
                    (int)TrueTime.UnitMinutes;
   seconds  =  10 * (int)TrueTime.TensSeconds +
                    (int)TrueTime.UnitSeconds;
   millisec = 100 * (int)TrueTime.HundredsMillisecs +
               10 * (int)TrueTime.TensMillisecs +
                    (int)TrueTime.UnitMillisecs;

   printf( "UTC_%02d:%02d:%02d.%03d", hours, minutes, seconds, millisec );
   return;
}


     /****************************************************************
      *                           InSynch()                          *
      *                                                              *
      *  Are synch bytes from the current drink in synch?            *
      *  One of the first 16 bytes should be a stop byte.            *
      *  The drink cannot be declared in synch unless it contains    *
      *  at least 50 bytes.                                          *
      *                                                              *
      *  Returns index of first start byte, or -1 if not in synch    *
      ****************************************************************/

int InSynch( UCHAR *DrinkBuf, int DrinkSize )
{
   int in_synch = 1;
   int i, j;

   if ( DrinkSize < 50 )             /* The drink is too small */
      return 0;

   for ( i = 0; i < 16; i++ )
   {
      if ( DrinkBuf[i] == StopByte )
      {
         for ( j = i + 2; j < DrinkSize; j += 16 )
            if ( DrinkBuf[j] != StartByte )
                in_synch = 0;
   
         for ( j = i + 16; j < DrinkSize; j += 16 )
            if ( DrinkBuf[j] != StopByte )
                in_synch = 0;

         if ( in_synch ) return i;       /* Synch acquired */
      }
   }
   return -1;                            /* Synch not acquired */
}


     /****************************************************************
      *                           GetFrame()                         *
      *                                                              *
      *  Get the index of a valid frame in the message buffer.       *
      *                                                              *
      *  Returns 1 if a valid frame was found, 0 otherwise           *
      ****************************************************************/

int GetFrame( STATION st[], int s, int *frameIndex )
{
   int i = *frameIndex;
   int j;
   int found = 0;
   int dt[9];

   do
   {
      if ( ++i > (st[s].nBytesInWb - 20) ) break;
      if ( st[s].Wb1[i]    != StopByte )  continue;
      if ( st[s].Wb1[i+2]  != StartByte ) continue;
      if ( st[s].Wb1[i+16] != StopByte )  continue;
      if ( st[s].Wb1[i+18] != StartByte ) continue;

/* Require the correct interval between time bytes
   ***********************************************/
      for ( j = 0; j < 9; j++ )
      {
         dt[j] = (int)BTB[st[s].Wb1[i + 2*j + 3]] -
                 (int)BTB[st[s].Wb1[i + 2*j + 1]];
         if ( dt[j] < 0 ) dt[j] += 100;
      }

      if ( dt[0] < 2 ) continue;
      if ( dt[0] > 4 ) continue;

      if ( dt[1] > 2 ) continue;
      if ( dt[2] > 2 ) continue;
      if ( dt[3] > 2 ) continue;
      if ( dt[4] > 2 ) continue;
      if ( dt[5] > 2 ) continue;
      if ( dt[6] > 2 ) continue;
      if ( dt[7] > 2 ) continue;

      if ( dt[8] < 2 ) continue;
      if ( dt[8] > 4 ) continue;

/* This frame is valid
   *******************/
      *frameIndex = i;
      found = 1;

   } while ( !found );

   return found;
}


     /****************************************************************
      *                         GetFrameTime()                       *
      *                                                              *
      *  Get the time of a frame, given its index.                   *
      ****************************************************************/

void GetFrameTime( STATION st[], int s, int frameIndex, double *frameTime )
{
   int dt, deltat, i, j, k;

/* Figure out which PC-SG time stamp to use
   ****************************************/
   for ( i = 0; i < st[s].nDrink; i++ )
      if ( st[s].DrinkIndex[i] >= frameIndex+5 ) goto GOT_IT;

   logit( "et", "getdst2 fatal error 2.  Exiting.\n" );
   exit( -1 );
GOT_IT:

/* Compute index (j) of last time byte preceeding the time stamp.
   This byte has an odd index number.
   *************************************************************/
   j = (st[s].DrinkIndex[i] % 2) ? st[s].DrinkIndex[i]:
        st[s].DrinkIndex[i]-1;

/* Compute deltat, the relative time of last time byte
   preceeding the time stamp.  We assume the time stamp
   is between 0 and 99 msec after the last time byte in
   the drink.  deltat will be between 0 and 99.
   ****************************************************/
   dt = 10*st[s].DrinkTS[i].TensMillisecs + st[s].DrinkTS[i].UnitMillisecs -
        BTB[st[s].Wb1[j]];
   deltat = (dt < 0) ? (dt + 100): dt;

/* Get relative time of the time byte of interest.
   deltat will be between 0 and 99.
   **********************************************/
   for ( k = frameIndex+5; k < j; k+=2 )
   {
      dt = BTB[st[s].Wb1[k+2]] - BTB[st[s].Wb1[k]];
      deltat += (dt < 0) ? (dt + 100): dt;
   }

/* Return the time of our preferred time byte
   ******************************************/
   *frameTime = st[s].DrinkTime[i] - (double)deltat / 1000.;
   return;
}


     /****************************************************************
      *                          BuildWb2()                          *
      *                                                              *
      *  Add data bytes from one frame to the 2'nd work buffer.      *
      *  Bytes are copied from the first work buffer to one of the   *
      *  2'nd work buffers.                                          *
      *  There is one 2'nd work buffer per channel.                  *
      *  This function demultiplexes the data.                       *
      *                                                              *
      *  Returns 1 if the message is complete, otherwise 0;          *
      ****************************************************************/

#define Channel_0   0
#define Channel_1   1
#define Channel_2   2

void BuildWb2( STATION st[], int s,  int frameIndex, int mbi )
{
   int   c;                           /* Channel number */
   UCHAR low_byte;
   UCHAR high_byte;
   UCHAR extend_byte;
   union
   {
      long l;
      UCHAR b[4];
   } u;

/* Get the low, high, and extend bytes for each channel
   ****************************************************/
   for ( c = 0; c < nChan; c++ )
   {
      switch ( c )
      {
         case Channel_0:
         low_byte  = st[s].Wb1[frameIndex+6];
         high_byte = st[s].Wb1[frameIndex+4];
         break;

         case Channel_1:
         low_byte  = st[s].Wb1[frameIndex+10];
         high_byte = st[s].Wb1[frameIndex+8];
         break;

         case Channel_2:
         low_byte  = st[s].Wb1[frameIndex+14];
         high_byte = st[s].Wb1[frameIndex+12];
         break;
      }

      extend_byte = ( high_byte & 0x80 ) ? 0xff : 0x00;

/* Sign extend to 32 bits
   **********************/
      u.b[0] = low_byte;
      u.b[1] = high_byte;
      u.b[2] = extend_byte;
      u.b[3] = extend_byte;
      st[s].ch[c].Wb2[mbi] = u.l;
   }
}


     /****************************************************************
      *                           ResetWb1()                         *
      *                                                              *
      *  Purge out data that has already been processed from work    *
      *  buffer 1.                                                   *
      ****************************************************************/

void ResetWb1( STATION st[], int s, int indexLastFrame )
{
   int nPurge;
   int i, j;

   nPurge = indexLastFrame + 16;
   memmove( st[s].Wb1, st[s].Wb1+nPurge, st[s].nBytesInWb-nPurge );
   st[s].nBytesInWb -= nPurge;

   for ( i = 0; i < st[s].nDrink; i++ )
      if ( st[s].DrinkIndex[i] >= nPurge ) break;
   for ( j = i; j < st[s].nDrink; j++ )
   {
      st[s].DrinkIndex[j-i] = st[s].DrinkIndex[j] - nPurge;
      st[s].DrinkTime[j-i]  = st[s].DrinkTime[j];
      st[s].DrinkTS[j-i]    = st[s].DrinkTS[j];
   }
   st[s].nDrink -= i;
   st[s].WbStartTime += 1.;
   return;
}


   /******************************************************************
    *                            ResetWb2()                          *
    *                                                                *  
    *  Reset some values in the 2'nd work buffer.                    *
    *  Reset sample values to "no data".                             *
    *  These are filled in later.                                    *
    ******************************************************************/

void ResetWb2( STATION st[], int s )
{
   int c;                      /* Channel number */
   int scan;

   for ( c = 0; c < nChan; c++ )
   {
      for ( scan = 0; scan < nScan; scan++ )
         st[s].ch[c].Wb2[scan] = MissingData;
   }
   return;
}


     /****************************************************************
      *                          WriteMsg()                          *
      *                                                              *
      *  Copy samples from the second work buffer to the output      *
      *  message buffer.  Write the output message to shared memory  *
      ****************************************************************/

void WriteMsg( STATION st[], int s )
{
   int i;
   int first;
   int last = -1;
   int start;
   int nbytes;
   int c;
   int rc;
   int nsamp;
   int WaveMsgLen;

/* Loop through all messages (some may be short)
   *********************************************/
   while ( 1 )
   {

/* Find the first and last samples to output as a message
   ******************************************************/
      start = last + 1;

      for ( i = start; i < nScan; i++ )
         if ( st[s].ch[0].Wb2[i] != MissingData ) break;
      first = i;

      if ( first == nScan ) break;            /* No more messages */

      for ( i = first+1; i < nScan; i++ )
         if ( st[s].ch[0].Wb2[i] == MissingData ) break;
      last = i - 1;

/* Compute waveform message length, in bytes
   *****************************************/
      nsamp = last - first + 1;
      nbytes = nsamp * sizeof( long );
      WaveMsgLen = sizeof(TRACE2_HEADER) + ( nsamp * sizeof(long) );

/* Do this for all channels for one station
   ****************************************/
      for ( c = 0; c < nChan; c++ )
      {
         double start = st[s].WbStartTime - st[s].ch[c].delay;

/* Fill the message header with correct values.
   Some header values have already been set.
   *******************************************/
         st[s].ch[c].hd->nsamp      = nsamp;
         st[s].ch[c].hd->starttime  = start + (first / SamplingRate);
         st[s].ch[c].hd->endtime    = start + (last  / SamplingRate);
         st[s].ch[c].hd->quality[0] = 0;
         st[s].ch[c].hd->quality[1] = 0;

/* Copy data samples from work buffer 2 to the output message buffer
   *****************************************************************/
         memcpy( st[s].ch[c].dat, st[s].ch[c].Wb2 + first, nbytes );

/* Send a waveform message to the output ring
   ******************************************/
         rc = tport_putmsg( &OutRegion, &WaveLogo, (long)WaveMsgLen,
                           (char *)st[s].ch[c].OutBuf );

         if ( rc != PUT_OK )
            logit( "et", "Error sending waveform message to output ring.\n" );
      }
   }
   return;
}
