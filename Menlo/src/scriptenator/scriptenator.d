#
# This is the scriptenator parameter file. This module watches designated
# directories for the appearance of files, and initiates scripts to process them.

#  Basic Earthworm setup:
#
LogSwitch     1              # 0 to completely turn off disk log file
MyModuleId    MOD_SCRIPTOR   # module id for this instance of eqwaves 

RingName      HYPO_RING      # ring to get input from
HeartBeatInt  5              # seconds between heartbeats

# Scripts to be run when file show up.

Script   /home/picker/netquakes/indir/  "/home/picker/netquakes/scripts/nq_in_sort.pl"

    # *** Optional Commands ***

SecsToWait  10    # seconds to wait between directory check
 
# We accept a command "Debug" which turns on a bunch of log messages
 
  Debug 1

