/*************************************************************************
 *      scriptenator.c:
 * Monitors one or more input directories; 
 * when files appear, scripts are started to process them.
 *
 * Note: scripts are not spawned as separate threads. If a script takes 
 * a long time to complete, everybody else must wait.
 *
 * 
 * Jim Luetgert 03/21/10
**************************************************************************/
 
#include <platform.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include <decode.h>
#include <kom.h>
#include <transport.h>

#define MAXSCRIPTS        10  /* Maximum number of scripts to run            */
#define SCRIPTSZ         200  /* Maximum length of a command                 */
#define GDIRSZ           132  /* Size of string for GIF target directory     */

/* Functions in this source file
 *******************************/
thr_ret Heartbeat( void * );
void config_me( char * ); /* reads configuration (.d) file via Carl's routines  */
void ewmod_status( unsigned char, short, char *); /* sends heartbeats into ring   */
void lookup_ew ( void );     /* Goes from symbolic names to numeric values, via earthworm.h  */

/* Shared memory
 ***************/
static  SHM_INFO  InRegion;              /* public shared memory for receiving arkive messages */

/* Things to lookup in the earthworm.h table with getutil.c functions
 ********************************************************************/
static long          InRingKey;         /* key of transport ring for input   */
static unsigned char InstId;            /* local installation id             */
static unsigned char MyModId;           /* our module id                     */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;

/* Things to read from configuration file
 ****************************************/
static char InRingName[20];             /* name of transport ring for i/o          */
static char MyModuleId[20];             /* module id for this module               */
static int  LogSwitch;                  /* 0 if no logging should be done to disk  */
static time_t HeartBeatInterval = 5;    /* seconds between heartbeats              */

/* The directories & scripts 
****************************/
static int   nscripts;
static char  Directory[MAXSCRIPTS][GDIRSZ];
static char  Command[MAXSCRIPTS][SCRIPTSZ];
static int   Debug;                            /* debug flag                       */
static int   SecsToWait;                       /* seconds between directory checks */

pid_t MyPid;              /* Our own pid, sent with heartbeat for restart purposes */

/* Server thread stuff
**********************/
#define THREAD_STACK      8096
#define THREAD_STACK_SIZE 8192

/* Other globals
 ***************/
char   progname[] = "scriptenator";

time_t    MyLastInternalBeat;   /* time of last heartbeat into the local Earthworm ring    */
unsigned  TidHeart;             /* thread id. was type thread_t on Solaris! */

/**********************************************************************************
 *  main( int argc, char **argv )                                                 *
 **********************************************************************************/

int main( int argc, char **argv )
{
    char   fname[100], whoami[50];
    int    i, j, ret;

        /* Check command line arguments
         ******************************/
    if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", progname );
        exit( 0 );
    }
    
    sprintf(whoami, "%s: %s: ", progname, "Main");

    /* Get our own Pid for restart purposes
    ***************************************/
    MyPid = getpid();
    if( MyPid == -1 ) {
      fprintf( stderr,"%s: Cannot get pid. Exiting.\n", progname);
      return -1;
    }

        /* Read the configuration file(s)
         ********************************/
    config_me( argv[1] );

        /* Look up important info from earthworm.h tables
         ************************************************/
    lookup_ew();

        /* Initialize name of log-file & open it
         ***************************************/
    logit_init( argv[1], (short) MyModId, 512, LogSwitch );
    logit( "" , "%s Read command file <%s>\n",  whoami, argv[1] );
    
        /* DEBUG: dump variables to stdout
        **********************************/
    if(LogSwitch) {
        logit("","%s MyModuleId:  %s \n",   whoami, MyModuleId);
        logit("","%s InRingName:  %s \n",   whoami, InRingName);
    }

        /* Attach to public HYPO shared memory ring
         ******************************************/
    tport_attach( &InRegion, InRingKey );
    if (LogSwitch) logit( "", "%s Attached to public memory region <%s>: %ld.\n", 
                              whoami, InRingName, InRegion.key );
#ifdef _SOLARIS                    /* SOLARIS ONLY:                         */
    if (Debug) logit( "e", "%s Attached to public memory region <%s> key: %ld mid: %ld sid: %ld.\n", 
                              whoami, InRingName, InRegion.key, InRegion.mid, InRegion.sid );
    if (Debug) logit( "e", "%s nbytes: %ld keymax: %ld keyin: %ld keyold: %ld flag: %d.\n", 
                              whoami, InRegion.addr->nbytes, 
                              InRegion.addr->keymax, InRegion.addr->keyin, 
                              InRegion.addr->keyold, InRegion.addr->flag );
#endif                             /*                                       */


	/* Test that we can access the directories */
	/*******************************************/

	for(i=0;i<nscripts;i++) {
		if( chdir_ew( Directory[i] ) == -1 ) {
			logit( "e", "%s InputDir directory <%s> not found; "
					 "exiting!\n", whoami, Directory[i] );
			exit(-1);
		}
	}

   /* Start the heartbeat thread
   ****************************/
    time(&MyLastInternalBeat); /* initialize our last heartbeat time */
                          
    if ( StartThread( Heartbeat, (unsigned)THREAD_STACK_SIZE, &TidHeart ) == -1 ) {
        logit( "et","%s Error starting Heartbeat thread. Exiting.\n", whoami );
        tport_detach( &InRegion );
        return -1;
    }

	/* sleep for 2 seconds to allow heart to beat so statmgr gets it.  */
	
	sleep_ew(2000);

	/* ------------------------ start working loop -------------------------*/
    while(1) {
                       
        /* -------------- see if a termination has been requested ----------*/
		if ( tport_getflag( &InRegion ) == TERMINATE ||
			 tport_getflag( &InRegion ) == MyPid ) {      /* detach from shared memory regions*/
			sleep_ew( 500 );       /* wait around while threads finish */
			tport_detach( &InRegion );
			logit("et", "%s Termination requested; exiting.\n",  whoami );
			fflush(stdout);
			exit( 0 );
		}

			/* Check input directories for files to be processed */
			/*****************************************************/

        for(i=0;i<nscripts;i++) {
			if(Debug > 1) logit( "et","%s Checking directory: %s\n", whoami, Directory[i]);
			if( chdir_ew( Directory[i] ) == -1 ) {
				logit( "e", "%s InputDir directory <%s> not found; "
						 "exiting!\n", whoami, Directory[i] );
				exit(-1);
			}
            ret = GetFileName( fname );
        
            if( ret != 1 ) {
				if(Debug) logit( "et","%s Files found in directory: %s\n", whoami, Directory[i]);
				if(Debug) logit( "e","%s Running script: %s\n", whoami, Command[i]);
				
				system( Command[i] );  /* Files found; Process them. */
			}
        }
        
        sleep_ew( SecsToWait*1000 );       /* wait around for more files to show up   */

    }
}



/***************************** Heartbeat **************************
 *           Send a heartbeat to the transport ring buffer        *
 ******************************************************************/

thr_ret Heartbeat( void *dummy )
{
    time_t now;

   /* once a second, do the rounds.  */
    while ( 1 ) {
        sleep_ew(1000);
        time(&now);

        /* Beat our heart (into the local Earthworm) if it's time
        ********************************************************/
        if (difftime(now,MyLastInternalBeat) > (double)HeartBeatInterval) {
            ewmod_status( TypeHeartBeat, 0, "" );
            time(&MyLastInternalBeat);
        }
    }
}


/****************************************************************************
 *      config_me() process command file using kom.c functions              *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void config_me( char* configfile )
{
    char    whoami[50], *com, *str;
    char    init[20];       /* init flags, one byte for each required command */
    int     ncommand;       /* # of required commands you expect */
    int     nmiss;          /* number of required commands that were missed   */
    int     i, j, n, nfiles, success;

    sprintf(whoami, "%s: %s: ", progname, "config_me");
    
	/* Set to zero one init flag for each required command
	 *****************************************************/
    ncommand = 5;
    for(i=0; i<ncommand; i++ )  init[i] = 0;
    
	nscripts = 0;
    Debug      =  0;
    SecsToWait = 60;
    
        /* Open the main configuration file
         **********************************/
    nfiles = k_open( configfile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, configfile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

                /* Process anything else as a command
                 ************************************/
  /*0*/
            if( k_its("LogSwitch") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/
            else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModuleId , str );
                init[1] = 1;
            }
  /*2*/
            else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( InRingName, str );
                init[2] = 1;
            }

  /*3*/
            else if( k_its("HeartBeatInt") ) {
                HeartBeatInterval = k_long();
                init[3] = 1;
            }


          /* get the commnds
        ****************************/
/*4*/
            else if( k_its("Script") ) {
                if ( nscripts >= MAXSCRIPTS ) {
                    fprintf(stderr, "%s Too many <Script> commands in <%s>", 
                             whoami, configfile );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAXSCRIPTS );
                    exit( -1 );
                }
                
                if( (long)(str=k_str()) != 0 )  {
					if( (int)strlen(str) >= GDIRSZ) {
						fprintf(stderr,"%s Fatal error. Gif directory name %s greater than %d char.\n",
							whoami, str, GDIRSZ);
						exit( -1 );
					}
					j = strlen(str);   /* Make sure directory name has proper ending! */
					if( str[j-1] != '/' ) strcat(str, "/");
                    strcpy(Directory[nscripts], str);
                }
                if( (long)(str=k_str()) != 0 )  {
					if( (int)strlen(str) >= SCRIPTSZ) {
						fprintf(stderr,"%s Fatal error. Script length %s greater than %d char.\n",
							whoami, str, SCRIPTSZ);
						exit( -1 );
					}
                    strcpy(Command[nscripts], str);
                }
                nscripts += 1;
                init[4] = 1;
            }

               /* optional commands */

            else if( k_its("SecsToWait") ) {  /*optional command*/
                SecsToWait = k_int();
                if(SecsToWait>1000) SecsToWait = 1000; 
                if(SecsToWait<   0) SecsToWait =    0; 
            }

            else if( k_its("Debug") ) {  /*optional command*/
                Debug = k_int();
                if( k_err() ) Debug = 99;
            }
            

                /* At this point we give up. Unknown thing.
                *******************************************/
            else {
                fprintf(stderr, "%s <%s> Unknown command in <%s>.\n",
                         whoami, com, configfile );
                continue;
            }

                /* See if there were any errors processing the command
                 *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "%s Bad <%s> command  in <%s>; exiting!\n",
                             whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
    }

        /* After all files are closed, check init flags for missed commands
         ******************************************************************/
    nmiss = 0;
    for(i=0;i<ncommand;i++)  if( !init[i] ) nmiss++;
    if ( nmiss ) {
        fprintf( stderr, "%s ERROR, no ",  whoami);
        if ( !init[0] )  fprintf( stderr, "<LogSwitch> "       );
        if ( !init[1] )  fprintf( stderr, "<MyModuleId> "      );
        if ( !init[2] )  fprintf( stderr, "<RingName> "        );
        if ( !init[3] )  fprintf( stderr, "<HeartBeatInt> "    );
        if ( !init[4] )  fprintf( stderr, "<Script> "      );
        fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
        exit( -1 );
    }
}


/**********************************************************************************
 * ewmod_status() builds a heartbeat or error msg & puts it into shared memory    *
 **********************************************************************************/
void ewmod_status( unsigned char type,  short ierr,  char *note )
{
    char        whoami[50];
    MSG_LOGO    logo;
    char        msg[512];
    long        size;
    time_t      t;

    sprintf(whoami, "%s: %s: ", progname, "ewmod_status");
    
    logo.instid = InstId;
    logo.mod    = MyModId;
    logo.type   = type;

    time( &t );
    if( type == TypeHeartBeat ) {
        sprintf( msg, "%ld %ld\n\0", t,MyPid);
    } else
    if( type == TypeError ) {
        sprintf( msg, "%ld %hd %s\n", t, ierr, note );
        logit( "t", "%s  %s\n",  whoami, note );
    }

    size = strlen( msg );   /* don't include the null byte in the message */
    if( tport_putmsg( &InRegion, &logo, size, msg ) != PUT_OK ) {
        if( type == TypeHeartBeat ) {
            logit("et","%s  Error sending heartbeat.\n",  whoami );
        } else
        if( type == TypeError ) {
            logit("et","%s  Error sending error:%d.\n",  whoami, ierr );
        }
    }
}


/****************************************************************************
 *  lookup_ew( ) Look up important info from earthworm.h tables           *
 ****************************************************************************/
void lookup_ew( )
{
    char        whoami[50];
    
    sprintf(whoami, "%s: %s: ", progname, "lookup_ew");
    
        /* Look up keys to shared memory regions
         ***************************************/
    if( (InRingKey = GetKey(InRingName)) == -1 ) {
        fprintf( stderr,
                "%s Invalid ring name <%s>; exiting!\n",  whoami, InRingName );
        exit( -1 );
    }

        /* Look up installation Id
         *************************/
    if ( GetLocalInst( &InstId ) != 0 ) {
        fprintf( stderr,
                "%s error getting local installation id; exiting!\n",  whoami );
        exit( -1 );
    }

        /* Look up modules of interest
         *****************************/
    if ( GetModId( MyModuleId, &MyModId ) != 0 ) {
        fprintf( stderr,
                "%s Invalid module name <%s>; exiting!\n",  whoami, MyModuleId );
        exit( -1 );
    }

        /* Look up message types of interest
         ***********************************/
    if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
        fprintf( stderr,
                "%s Invalid message type <TYPE_HEARTBEAT>; exiting!\n",  whoami );
        exit( -1 );
    }
    if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
        fprintf( stderr,
                "%s Invalid message type <TYPE_ERROR>; exiting!\n",  whoami );
        exit( -1 );
    }
}


