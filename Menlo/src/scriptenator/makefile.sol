CFLAGS = -D_REENTRANT $(GLOBALFLAGS) 

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

BINARIES = scriptenator.o \
		$L/logit_mt.o $L/getutil.o  $L/transport.o $L/kom.o $L/sleep_ew.o\
		$L/time_ew.o $L/threads_ew.o $L/sema_ew.o $L/dirops_ew.o

scriptenator: $(BINARIES)
	cc -o $(B)/scriptenator $(BINARIES) -lsocket -lnsl -lm -mt -lposix4 -lthread -lc

.c.o:
	$(CC) $(CFLAGS) -g $(CPPFLAGS) -c  $(OUTPUT_OPTION) $<

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/scriptenator*



