
   /*************************************************************
    *                        sendit_nt.c                        *
    *************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include "sendmsg.h"

#define BUFLEN 1000

static SOCKET sock;                    /* Socket identifier */


/********************** SocketSysInit **********************
 *               Initialize the socket system              *
 *         We are using Windows socket version 2.2.        *
 ***********************************************************/

void SocketSysInit( void )
{
   char    *homeDir;
   int     status;
   WSADATA Data;

   status = WSAStartup( MAKEWORD(2,2), &Data );
   if ( status != 0 )
   {
      printf( "WSAStartup failed. Exiting.\n" );
      exit( -1 );
   }
   return;
}


/********************** SocketClose **********************
 *                    Close a Socket                     *
 *********************************************************/

void SocketClose( int soko )
{
   closesocket( (SOCKET)soko );
   return;
}


       /************************************************************
        *                    ConnectToTelapage()                   *
        *                                                          *
        *          Get a connection to the remote system.          *
        ************************************************************/

int ConnectToTelapage( const char ServerIP[], const int ServerPort )
{
   struct sockaddr_in server;         /* Server socket address structure */
   const int optVal = 1;

/* Get a new socket descriptor
   ***************************/
   sock = socket( AF_INET, SOCK_STREAM, 0 );
   if ( sock == INVALID_SOCKET )
   {
      printf( "socket() error: %d\n", WSAGetLastError() );
      return SENDMSG_FAILURE;
   }

/* Allow reuse of socket addresses
   *******************************/
   if ( setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
                    sizeof(int) ) == SOCKET_ERROR )
   {
      printf( "setsockopt() error: %d\n", WSAGetLastError() );
      SocketClose( sock );
      return SENDMSG_FAILURE;
   }

/* Fill in socket address structure
   ********************************/
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family      = AF_INET;
   server.sin_port        = htons( (unsigned short)ServerPort );
   server.sin_addr.s_addr = inet_addr( ServerIP );

/* Connect to the server.
   The connect call blocks if telapage is not available.
   ****************************************************/
   printf( "Connecting to %s port %d\n", ServerIP, ServerPort );
   if ( connect( sock, (struct sockaddr *)&server, sizeof(server) )
        == SOCKET_ERROR )
   {
      int rc = WSAGetLastError();
      if ( rc == WSAETIMEDOUT )
         printf( "connect() returned WSAETIMEDOUT\n" );
      else
         printf( "connect() error: %d\n", rc );
      SocketClose( sock );
      return SENDMSG_FAILURE;
   }
   return SENDMSG_SUCCESS;
}


       /************************************************************
        *                     SendToTelapage()                     *
        *                                                          *
        *   Send the message to the telapage system.               *
        ************************************************************/

int SendToTelapage( char *buf, int nbytes )
{
   unsigned msgSize = strlen( buf );
   char msgSizeAsc[7];
   char chr;
 
/* Sanity check 
   ************/ 
   if ( msgSize > 999999 )
   {
      printf( "Error. Message is too large to send.\n" );
      SocketClose( sock );
      return SENDMSG_FAILURE;
   }
 
/* Write message size to socket
   ****************************/
   sprintf( msgSizeAsc, "%6u", msgSize );
   if ( send( sock, msgSizeAsc, 6, 0 ) == SOCKET_ERROR )
   {
      printf( "Error sending message size to socket: %s\n", WSAGetLastError() );
      SocketClose( sock );
      return SENDMSG_FAILURE;
   }

/* Send message bytes
   ******************/
   if ( send( sock, buf, msgSize, 0 ) == SOCKET_ERROR )
   {
      printf( "send() error: %s\n", WSAGetLastError() );
      SocketClose( sock );
      return SENDMSG_FAILURE;
   }

/* Life is wonderful
   *****************/
   SocketClose( sock );
   return SENDMSG_SUCCESS;
}


void sleep( int sec )
{
   Sleep( 1000 * sec );
   return;
}
