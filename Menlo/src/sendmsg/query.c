#define LEN 100
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

getstr(query,str,insist)
        char query[],str[];
        int insist;
{
        int j,change;
        char line[LEN],line2[LEN];

        start: ;
        change=0;
        j=0;
        while(!j){
                printf("%s [%s]: ",query,str);
                if(fgets(line,LEN,stdin)==NULL)exit(0);
                if(strlen(line)>1){
                        j=sscanf(line,"%s",line2);
                        if(j){
                                strcpy(str,line2);
                                change=1;
                        }
                }
                else j=1;
        }
        if(insist && !change)goto start;
        return(change);
}

getline(query,str,length,insist)
        char query[],str[];
        int insist,length;
{
        int j,change;
        int i;
        char line[LEN];

        start: ;
        change=0;
        j=0;
        while(!j){
                printf("%s [%s]: ",query,str);
                if(fgets(line,LEN,stdin)==NULL)exit(0);
                j=strlen(line);
                if(j>1){
                        for(i=0;i<j;++i){
                                if(i==length || line[i]=='\n'){
                                        str[i]='\0';
                                        continue;
                                }
                                str[i]=line[i];
                        }
                        change=1;
                }
                else j=1;
        }
        if(insist && !change)goto start;
        return(change);
}



getfloat(query,pfl,insist)
        char query[];
        float *pfl;
        int insist;
{
        int j,change;
        char line[LEN];
        float z;

        start: ;
        change=0;
        j=0;
        while(!j){
                printf("%s [%g]: ",query,*pfl);
                if(fgets(line,LEN,stdin)==NULL)exit(0);
                if(strlen(line)>1){
                        j=sscanf(line,"%f",&z);
                        if(j){
                                *pfl=z;
                                change=1;
                        }
                }
                else j=1;
        }
        if(insist && !change)goto start;
        return(change);
}


getint(query,pint,insist)
        char query[];
        int insist,*pint;
{
        int j,change,i;
        char line[LEN];

        start: ;
        change=0;
        j=0;
        while(!j){
                printf("%s [%d]: ",query,*pint);
                if(fgets(line,LEN,stdin)==NULL)exit(0);
                if(strlen(line)>1){
                        j=sscanf(line,"%d",&i);
                        if(j){
                                *pint=i;
                                change=1;
                        }
                }
                else j=1;
        }
        if(insist && !change)goto start;
        return(change);
}


getdouble(query,pdb,insist)
        char query[];
        double *pdb;
        int insist;
{
        int j,change;
        char line[LEN];
        double z;

        start: ;
        change=0;
        j=0;
        while(!j){
                printf("%s [%g]: ",query,*pdb);
                if(fgets(line,LEN,stdin)==NULL)exit(0);
                if(strlen(line)>1){
                        j=sscanf(line,"%lf",&z);
                        if(j){
                                *pdb=z;
                                change=1;
                        }
                }
                else j=1;
        }
        if(insist && !change)goto start;
        return(change);
}
