
      /*************************************************************
       *                        sendit_sol.c                       *
       *************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "sendmsg.h"

static int  sock;                /* Socket descriptor */


/**********************************************************
 *                     SocketSysInit()                    *
 **********************************************************/

void SocketSysInit( void )
{
   return;
}


/********************** SocketClose **********************
                      Close a Socket
**********************************************************/

void SocketClose( int soko )
{
   close( soko );
   return;
}


      /************************************************************
       *                    ConnectToTelapage()                   *
       *                                                          *
       *          Get a connection to the remote system.          *
       ************************************************************/

int ConnectToTelapage( const char ServerIP[], const int ServerPort )
{
   struct sockaddr_in server;         /* Server socket address structure */
   const int optVal = 1;
   unsigned long address;
   int rc;

/* Get a new socket descriptor
   ***************************/
   sock = socket( AF_INET, SOCK_STREAM, 0 );
   if ( sock == -1 )
   {
      printf( "socket() error: %s\n", strerror(errno) );
      return SENDMSG_FAILURE;
   }

/* Allow reuse of socket addresses
   *******************************/
   if ( setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
                    sizeof(int) ) == -1 )
   {
      printf( "setsockopt() error: %s\n", strerror(errno) );
      SocketClose( sock );
      return SENDMSG_FAILURE;
   }

/* Fill in socket address structure
   ********************************/
   address = inet_addr( ServerIP );
   if ( address == -1 )
   {
      printf( "Bad server IP address: %s\n", ServerIP );
      SocketClose( sock );
      return SENDMSG_FAILURE;
   }
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family      = AF_INET;
   server.sin_port        = htons((unsigned short)ServerPort);
   server.sin_addr.s_addr = address;

/* Connect to the telapage server.
   The connect call blocks if telapage is not available.
   ****************************************************/
   if ( connect( sock, (struct sockaddr *)&server, sizeof(server) ) == -1 )
   {
      printf( "connect() error.\n" );
      SocketClose( sock );
      return SENDMSG_FAILURE;
   }
   return SENDMSG_SUCCESS;
}


       /************************************************************
        *                     SendToTelapage()                     *
        *                                                          *
        *   Send the message to the telapage system.               *
        ************************************************************/

int SendToTelapage( char *buf, int nbytes )
{
   unsigned msgSize = strlen( buf );
   char msgSizeAsc[7];
   char chr;
   int  sockerr;
   int  optlen = sizeof(int);

/* Sanity check
   ************/
   if ( msgSize > 999999 )
   {
      printf( "Error. Message is too large to send.\n" );
      SocketClose( sock );
      return SENDMSG_FAILURE;
   }
 
/* send() will crash if a socket error occurs
   and we don't detect it using getsockopt.
   ******************************************/
   if ( getsockopt( sock, SOL_SOCKET, SO_ERROR, (char *)&sockerr,
        &optlen ) == -1 )
   {
      printf( "getsockopt() error: %s", strerror(errno) );
      SocketClose( sock );
      return SENDMSG_FAILURE;
   }
   if ( sockerr != 0 )
   {
      printf( "Error detected by getsockopt(): %s\n", strerror(sockerr) );
      SocketClose( sock );
      return SENDMSG_FAILURE;
   }

/* Write message size to socket
   ****************************/
   sprintf( msgSizeAsc, "%6u", msgSize );
   if ( send( sock, msgSizeAsc, 6, NULL ) == -1 )
   {
      printf( "Error writing message size to socket: %s\n", strerror(errno) );
      SocketClose( sock );
      return SENDMSG_FAILURE;
   }

/* Send message bytes
   ******************/
   if ( send( sock, buf, msgSize, NULL ) == -1 )
   {
      printf( "send() error: %s\n", strerror(errno) );
      SocketClose( sock );
      return SENDMSG_FAILURE;
   }

/* Life is wonderful
   *****************/
   SocketClose( sock );
   return SENDMSG_SUCCESS;
}
