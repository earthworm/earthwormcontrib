/* a program to send a message to the ascii pagers */
#include <stdio.h>
#include <string.h>
#include "sendmsg.h"
#define MAXLEN 200

void SocketSysInit( void );
void LogConfig( void );
int  ConnectToTelapage( const char [], const int );
int  SendToTelapage( char *, int );
void sleep( int sec );
int  getstr( char [], char [], int );


int main(int argc, char **argv)
{
   char *ip;
   char group[20];
   char message[300];
   char line[1000];
   char name[100];
   char ans[10];
   FILE *fp;
   int inter = (argc < 3) ? 1 : 0;

   const char ServerIP[]    = "130.118.43.76";   /* IP address of server mnlons1 */
   const char AltServerIP[] = "130.118.43.77";  /* Address of alternate server mnlons2 */
   const char AltServer2IP[] = "130.118.43.76";  /* Address of alternate server mnlons1 */
   const char AltServer3IP[] = "130.118.43.77";  /* Address of alternate server mnlons2 */
   const int  ServerPort    = 2345;             /* Well-known port number */

/* Initialize the socket system
   ****************************/
   SocketSysInit();

   if(!inter){
      ++argv;
      fp=fopen(*argv,"r");
      if(fp==NULL){
         fprintf(stderr,"Unable to open file %s\n",*argv);
         return 0;
      }
   }
   else {
      fp=NULL;
      sprintf(name,"");
      while(fp==NULL){
         getstr("\nEnter filename of the message to send: ",name,1);
         fp=fopen(name,"r");
         if(fp==NULL) fprintf(stderr,"Unable to open file %s\n",name);
      }
   }

   if(!inter){
      ++argv;
      strcpy(group,*argv);
   }
   else {
      sprintf(group,"");
      getstr("Enter group to get message (eqalarm, operations, ...)",group,1);
      getstr("Fix spelling of group if needed",group,0);
   }

   if ( argc < 4 )
      sprintf(message,"group:%s ",group);
   else
   {
      ++argv;
      sprintf(message,"%s:%s ",*argv,group);
   }

   while(fgets(line,100,fp)!=NULL){
      strcat(message,line);
      if(strlen(message)>MAXLEN){
         fprintf(stderr,"Your message is too long.\n");
         fprintf(stderr,"It must be under %d characters",MAXLEN);
         fprintf(stderr," including the group:groupname header.\n");
         return 0;
      }
   }

/* Get rid of any trailing spaces and carriage returns.
   Replace any embedded carriage returns with spaces.
   ***************************************************/
   {
      int i;
      int len = strlen(message);

      for ( i = len-1; i >= 0; i-- )
      {
         if ( (message[i] != '\n') && (message[i] != ' ') ) break;
         message[i] = '\0';
         len--;
      }
      for ( i = 0; i < len; i++ )
         if ( message[i] == '\n' || message[i] == '\r' )
            message[i] = ' ';
   }

   strcat(message,"#");

   if(!inter)
      strcpy(ans,"y");
   else
   {
      fprintf(stderr,"\nYour message is:\n%s\n",message);
      sprintf(ans,"");
      getstr("\nDo you want to send it? (y,n)",ans,1);
   }

   if(ans[0]!='y' && ans[0]!='Y') return -1;

/* Connect to the Telapage server.
   If this fails, try connecting to the alternate server.
   *****************************************************/
	if ( ConnectToTelapage(ServerIP, ServerPort) == SENDMSG_SUCCESS )
		ip = (char *)&ServerIP[0];
	else {
		printf( "Can't connect to server at %s port %d.\n", ServerIP, ServerPort );
		
		if ( ConnectToTelapage(AltServerIP, ServerPort) == SENDMSG_FAILURE )
			ip = (char *)&AltServerIP[0];
		else {
			printf( "Can't connect to server at %s port %d.\n", AltServerIP, ServerPort );

			if ( ConnectToTelapage(AltServer2IP, ServerPort) == SENDMSG_FAILURE )
				ip = (char *)&AltServer2IP[0];
			else {
				printf( "Can't connect to server at %s port %d.\n", AltServer2IP, ServerPort );
				return -1;
			}
		}
   }
   printf( "Sending message to %s port %d\n", ip, ServerPort );

   if ( SendToTelapage( message, strlen(message) ) == SENDMSG_FAILURE ) {
		printf( "Can't send message.\n" );
		return -1;
   }

   fprintf(stderr,"MESSAGE SENT.\n");
   return 0;
}
