/*
 *   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT USING THE COMMAND CHECKOUT.
 *
 *    $Id: k2evt2ew.h 710 2012-11-02 20:21:25Z luetgert $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.3  2005/10/11 22:06:08  luetgert
 *     .
 *
 *     Revision 1.2  2004/11/02 22:52:38  luetgert
 *     Made evt2ring location-code compatible.
 *     .CVS: ----------------------------------------------------------------------
 *
 *     Revision 1.1.1.1  2004/04/28 23:15:44  dietz
 *     pre-location code Contrib/Menlo
 *
 *     Revision 1.4  2003/01/08 18:08:33  davidk
 *     Increased the MAXTRACELTH constant to 800k from 20k, in order to support
 *     EVT files collected by urban hazards program @ Golden.
 *
 *     Revision 1.3  2002/11/03 00:11:02  lombard
 *     Protected from multiple includes.
 *
 *     Revision 1.2  2002/08/16 20:49:29  alex
 *     fix for checksums and sanity
 *     Alex
 *
 *     Revision 1.1  2002/03/22 19:53:47  lucky
 *     Initial revision
 *
 *
 */

#ifndef K2EVT2EW_H
#define K2EVT2EW_H

#include <kwhdrs_ew.h>
#include <rw_strongmotionII.h>

#define		MAX_CHANS_PER_BOX		24
#define		MAX_SM_PER_BOX			20
/* DK 2003/01/08  MAXTRACELTH changed to 800000 in order to
   support 4000 second (200 s/s) evt file for Urban Hazards
   programs at Golden.  Note that this change increases the
   VM footprint size of the K2InfoStruct to approx 150MB!!!!
   Backed off to 80000 JL 04/29/03
 ***********************************************************/
#define  MAXTRACELTH  80000
#define	 MAX_REC       4096   /* largest "length" + "dataLength" in any tag */

#define SM_BOX_LEN       25   /* maximum length of a box name               */
#define SM_MAX_CHAN      36   /* max number chans on one strongmotion box   */

/* Structure Definitions
 ***********************/
typedef struct _k2info
{
	KFF_TAG			tag;
	MW_HEADER		head;
	FRAME_HEADER	frame;
	SM_INFO			sm[MAX_SM_PER_BOX];
	float			Databuf[MAX_CHANS_PER_BOX][MAXTRACELTH];
	long			Counts[MAX_CHANS_PER_BOX][MAXTRACELTH];
	int				numDataPoints[MAX_CHANS_PER_BOX];

} K2InfoStruct;


typedef struct _CHANNELNAME_ {
   char box[SM_BOX_LEN];      /* Installation-assigned box name (or serial#) */
   int  chan;                 /* Channel number on this box                  */
   char sta[TRACE2_STA_LEN];   /* NTS: Site code as per IRIS SEED */
   char comp[TRACE2_CHAN_LEN]; /* NTS: Channel/Component code as per IRIS SEED*/
   char net[TRACE2_NET_LEN];   /* NTS: Network code as per IRIS SEED */
   char loc[TRACE2_LOC_LEN];   /* NTS: Location code as per IRIS SEED */
} CHANNELNAME;


int k2evt2ew (FILE *fp, char *fname, K2InfoStruct *pk2info, CHANNELNAME *pChanName,
                            int numChans, char *NetCode, int Debug);
/* Functions in this source file
 *******************************/
static	int read_tag (FILE *fp, KFF_TAG *);
static	int read_head (FILE *fp, MW_HEADER *, int tagLength);
static	int read_frame (FILE *fp, FRAME_HEADER *, unsigned long *channels );

int extract(int pktdatalen, const unsigned char *pktbuff,
            float *Data, long *Counts, float scale, int nchan, int jchan, int *ind,
            int array_size);
int peak_ground(float *Data, int npts, int itype, float dt, SM_INFO *sm);
void demean(float *A, int N);
void locut(float *s, int nd, float fcut, float delt, int nroll, int icaus);
void rdrvaa(float *acc, int na, float omega, float damp, float dt,
            float *rd, float *rv, float *aa);
void amaxper(int npts, float dt, float *fc, float *amaxmm, 
					float *aminmm, float *pmax, int *imin, int *imax);


#endif
