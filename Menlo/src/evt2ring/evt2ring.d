# Configuration file for evt2ring:
# I stuff the trace data from the K2 .evt files onto a transport ring.
#
# When a file is processed, it is moved from the input directory to the output directory.
# When an error is enountered, the file is moved to the error directory and the
# program continues on to the next file until the input directory is empty.

#  Basic Earthworm setup:
#
MyModuleId      MOD_EVT2RING     # module id for this instance of evt2ring 
InRingName      HYPO_RING        # shared memory ring for input
OutRingName     WAVE_RING        # shared memory ring for output
LogFile         1                # 0 to turn off disk log file; 1 to turn it on
                                 # 2 to log to module log but not to stderr/stdout
HeartBeatInterval    30          # seconds between heartbeats

TraceBufInterval     30          # milliseconds between tracebufs sent to ring.
                                 # Optional. default=1000 (pseudo real time)

#StationList  calsta.db

# The entries below assign an IRIS-style SCNL name to each active channel
# of each instrument we may hear from. Each instrument has a 'box' identifier
# which is sent in its message (could be a name or a serial number).
# Use "" to enter a NULL string for any of the SCNL fields.
#
# NOTE: legal channel numbers (as of Nov 00) are 0-17
#
#            box  chan   S     C    N    L
#
#ChannelName  436   0     1002  HN2  NP   ""
#
#ChannelName  1990  0     1023  HN2  NP   ""
#
#ChannelName  1031  0     1103  HN2  NP   ""
#
#ChannelName  556   0     1446  HN2  NP   ""

@NSMP_Chans.db


# Directory where we should look for the pre-chewed ascii K2 files
EVTFilesInDir    /home/picker/evt/evt2ring

# Directory where the successfully processed files are put
EVTFilesOutDir /home/picker/evt/evt2ring/save

# Directory where the problem files are put
EVTFilesErrorDir /home/picker/evt/evt2ring/trouble

NetworkName NP

# If TimeFilter != 0, out of order evts will be rejected.
TimeFilter 1

# Debug switch: the token "Debug" (without the quotes) can be stated.
# If it is, lots of weird debug messages will be produced 
# Debug
