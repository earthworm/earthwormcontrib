/*********************************************************************************
 *                evt2ring
 * We read evt files.
 * We break the data into tracebufs and throw them at an output ring.
 * 
 * This module is location code compatible.  Puts out TRACE2_HEADER.
 * 
 * When we're through with an evt file, we move it to the output directory. 
 * If something went wrong with it, we move it to the error directory and
 * continue. 
 *     Jim Luetgert 11/04
 *********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>  
#include <trace_buf.h>
#include <mem_circ_queue.h>
#include <kom.h>
#include <ws_clientII.h>
#include <k2evt2ew.h> 
#include <rw_strongmotion.h> 

#define   FILE_NAM_LEN 500
#define   MAXWORD    FILE_NAM_LEN 
#define   MAXCHAN    2400
#define   TRCBUFLTH  1024
#define   MAX_INST    300          /* maximum number of stations             */
#define THREAD_STACK_SIZE 8192

typedef struct _evttrack
{
	int     sernum;
	char    staname[TRACE2_STA_LEN];
	double  actStarttime;
	double  actEndtime;
} EVTTRAK;

/* Things to read or derive from configuration file
 **************************************************/
static char  InRingName[MAX_RING_STR];  /* name of transport ring for input  */
static char  OutRingName[MAX_RING_STR]; /* name of transport ring for output */
static char  MyModName[MAX_MOD_STR];    /* speak as this module name/id      */
static int   LogSwitch;                 /* 0 if no logfile should be written */
static long  HeartBeatInterval;         /* seconds between heartbeats        */
SHM_INFO     InRegion;                  /* Input shared memory region info.  */
SHM_INFO     OutRegion;                 /* Output shared memory region info. */


/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          InRingKey;        /* key of transport ring for input   */
static long          OutRingKey;       /* key of transport ring for output  */
static unsigned char InstId;           /* local installation id             */
static unsigned char MyModId;          /* Module Id for this program        */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char typeWaveform2;    /* Waveform message type.            */
/* The Gloal variables:
***********************/
int    Debug;                /* if non-zero, print debug messages */
MSG_LOGO      trcLogo;       /* Logo of outgoing tracebuf message.   */
pid_t         MyPid;         /* My pid for restart from startstop    */
int    TraceBufLen = TRCBUFLTH;   /* Output tracebuf length        */
char   TraceMsgBuf[TRCBUFLTH*sizeof(long) + sizeof(TRACE2_HEADER)];

time_t MyLastInternalBeat;      /* time of last heartbeat into the local Earthworm ring    */
unsigned  TidHeart;             /* thread id. was type thread_t on Solaris! */

int NchanNames=0;            /* number of names in this config file  */
CHANNELNAME ChannelName[MAXCHAN];  /* table of box/channel to SCNL   */
static long  TBInterval;         /* milliseconds between tracebufs        */

char   EVTFilesInDir[FILE_NAM_LEN];    /* directory from whence cometh the */
                                       /* evt input files */
char   EVTFilesOutDir[FILE_NAM_LEN];   /* directory to which we put processed files */
char   EVTFilesErrorDir[FILE_NAM_LEN]; /* directory to which we put problem files */
char   outFname[FILE_NAM_LEN];         /* Full name of output file */
char   NetworkName[TRACE2_NET_LEN];     /* Network name. Constant from trace_buf.h */
char   progname[] = "evt2ring";
K2InfoStruct    pk2Info;

EVTTRAK     EvtTrak[MAX_INST];
int         numInst;
int         TimeFilter;

/* Functions in this source file
 *******************************/
thr_ret Heartbeat( void * );
int DoFile(K2InfoStruct*,long **traceData );
int makeEwSnippet( K2InfoStruct*, long*, char*, char*, char*, char*, char*);
int config_me(char*);
void ew_lookup( void );
void ew_status( unsigned char type, short ierr, char *note );

static	int read_tag (FILE *fp, KFF_TAG *);
static	int read_head (FILE *fp, MW_HEADER *, int tagLength);
static	int read_frame (FILE *fp, FRAME_HEADER *, unsigned long *channels );

/*************************************************************************
 *  main( int argc, char **argv )                                        *
 *************************************************************************/

int main( int argc, char **argv )
{
    char     whoami[50], subname[] = "Main";
    char     fname[FILE_NAM_LEN];    /* name of ascii version of evt file */
    long    *traceBuf[MAX_CHANS_PER_BOX];        /* To store the trace data */
    FILE    *fp;
    char     defaultConfig[] = "evt2ring.d";
    char    *configFileName = (argc > 1 ) ? argv[1] : &defaultConfig[0];
    int      i, j, index, iscan, ichan, ret, tracesize, rc;
    int      numfiles = 0;
    KFF_TAG  tag;
    double   actStarttime, actEndtime;

    /* Check command line arguments 
     ******************************/
    if (argc != 2) {
        fprintf (stderr, "Usage: evt2ring <configfile>\n");
        exit (EW_FAILURE);
    }
    sprintf(whoami, "%s: %s: ", progname, "Main");

    /* Read config file
     *******************/
    if (config_me (configFileName) != EW_SUCCESS) {
        fprintf (stderr, "%s configure() failed \n", whoami);
        return -1;
    }

	/* Look up important info from earthworm.h tables
	 ************************************************/
	ew_lookup();
    
    /* Set up logiting and log the config file
     **************************************/
	logit_init( argv[1], (short) MyModId, 256, LogSwitch );

	/* Get our own process ID for restart purposes
	 **********************************************/

	if ((MyPid = getpid ()) == -1) {
		logit ("e", "%s Call to getpid failed. Exiting.\n", whoami);
		exit (-1);
	}

	/* Attach to Input shared memory ring
	 ************************************/
	tport_attach( &InRegion, InRingKey );
	logit( "", "%s Attached to public memory region %s: %d\n",
	      whoami, InRingName, InRingKey );

	/* Attach to Output shared memory ring
	 *************************************/
	tport_attach( &OutRegion, OutRingKey );
	logit( "", "%s Attached to public memory region %s: %d\n",
	      whoami, OutRingName, OutRingKey );



    /* Change working directory to "EVTFilesInDir" - where the evt files should be
     ************************************************************************/
    if ( chdir_ew( EVTFilesInDir ) == -1 ) {
        logit( "e", "Error. Can't change working directory to %s\n Exiting.", EVTFilesInDir );
        return -1;
    }

   /* Start the heartbeat thread
   ****************************/
    time(&MyLastInternalBeat); /* initialize our last heartbeat time */
                          
    if ( StartThread( Heartbeat, (unsigned)THREAD_STACK_SIZE, &TidHeart ) == -1 ) {
        logit( "et","%s Error starting Heartbeat thread. Exiting.\n", whoami );
        tport_detach( &InRegion );
        tport_detach( &OutRegion );
        return -1;
    }

	/* sleep for 2 seconds to allow heart to beat so statmgr gets it.  */
	
	sleep_ew(2000);

logit ("et", "%s Starting main loop\n", whoami);

/*----------------------- setup done; start main loop -------------------------*/

	while(1) {
     /* Process all new messages
      **************************/
		
        /* see if a termination has been requested
         *****************************************/
			if ( tport_getflag( &InRegion ) == TERMINATE ||
			     tport_getflag( &InRegion ) == MyPid ) {
           /* detach from shared memory */
                tport_detach( &InRegion );
                tport_detach( &OutRegion );
           /* write a termination msg to log file */
                logit( "t", "%s Termination requested; exiting!\n", whoami );
                fflush( stdout );
                break;
			}

	    /* Start of working loop over files.
	    ************************************/
	    while ((GetFileName (fname)) != 1) {    
	        /* Open the file for reading only.  Since the file name
	        is in the directory we know the file exists, but it
	        may be in use by another process.  If so, fopen_excl()
	        will return NULL.  In this case, wait a second for the
	        other process to finish and try opening the file again.
	        ******************************************************/
	        if ((fp = fopen(fname, "rb" )) == NULL) {
	            logit ("e", "Could not open file <%s>\n", fname);
	            continue;
	        }
	
			else if( strcmp(fname,"core")==0 ) {
				fclose( fp );
				if( remove( fname ) != 0) {
					logit("et", "%s Cannot delete core file <%s>; exiting!", whoami, fname );
					break;
				}
				continue;
			}
	        
	        /* Parse the file header
	        ************************/
	        if ((rc=k2evt2ew (fp, fname, &pk2Info,  (CHANNELNAME *) NULL,
	                                0, NetworkName, Debug )) != EW_SUCCESS) {
	            if(rc == EW_WARNING)
	                logit("e","K2 file was too long.  Data was truncated at %d samples.\n"
	                  " File is valid but truncated.  Skipping this file %s\n", pk2Info.numDataPoints[0],fname);
	            else
	                logit("e","Error reading K2 file %s. Skipping this file\n", fname);

	            /* close the file and move it to the error directory */
	            fclose (fp);

	            sprintf (outFname, "%s/%s", EVTFilesErrorDir, fname);

	            if (rename (fname, outFname) != 0 ) {
	                logit( "e", "Fatal: Error moving %s: %s\n", fname, strerror(errno));
	                return(-1);
	            }
	            continue;
	        } 
	        
	        if(TimeFilter) {
				j = numInst;
				for(i=0;i<numInst;i++) {
					if(pk2Info.head.rwParms.misc.serialNumber == EvtTrak[i].sernum) {
						if(strcmp(pk2Info.head.rwParms.misc.stnID, EvtTrak[i].staname) == 0) j = i;
					}
				}
				if(j == numInst) {
					EvtTrak[j].sernum = pk2Info.head.rwParms.misc.serialNumber;
					strcpy(EvtTrak[j].staname, pk2Info.head.rwParms.misc.stnID);
					EvtTrak[j].actStarttime = 0.0;
					EvtTrak[j].actEndtime   = 0.0;
					if(numInst < MAX_INST) numInst++;
				}
				
				actStarttime = (double)pk2Info.head.roParms.stream.startTime + 
							   (double)(pk2Info.head.roParms.stream.startTimeMsec)/1000.;
				actEndtime = actStarttime + (pk2Info.head.roParms.stream.duration/10);
				
				if(actStarttime < EvtTrak[j].actEndtime) {
					logit ("e", "Out of order Error for %s! Last EndTime: %f This StartTime: %f\n ", 
						fname, EvtTrak[j].actEndtime, actStarttime);
					/* close the file and move it to the error directory */
					fclose (fp);
	
					sprintf (outFname, "%s/%s", EVTFilesErrorDir, fname);
	
					if (rename (fname, outFname) != 0 ) {
						logit( "e", "Fatal: Error moving %s: %s\n", fname, strerror(errno));
						return(-1);
					}
					continue;
				}
				EvtTrak[j].actStarttime = actStarttime;
				EvtTrak[j].actEndtime   = actEndtime;
	        }

	        fclose (fp);

	        logit("et", " *** Processing file %s, Station %s ***\n\n", 
	                                        fname, pk2Info.head.rwParms.misc.stnID);

	        /* Debug: show some file paramers
	        *********************************/
	        if (Debug)
	        {
	            logit("e","StaID: %s\n",
	                        pk2Info.head.rwParms.misc.stnID);

	            logit("e","StartTime: %d (1980) %d (1970) \n", 
	                    pk2Info.head.roParms.stream.startTime, 
	                    pk2Info.head.roParms.stream.startTime  + 
	                             (10*365*24*60*60) + (2*24*60*60) );

	            logit("e","StartMsecTimeMs: %d\n", 
	                    pk2Info.head.roParms.stream.startTimeMsec);

	            logit("e","ClockSource: %d\n",
	                    pk2Info.head.roParms.timing.clockSource);

	            logit("e","GPSstatus: %d\n",
	                    pk2Info.head.roParms.timing.gpsStatus);

	            logit("e","Duration: %f\n", 
	                    pk2Info.head.roParms.stream.duration);
	                    
	            logit("e","Lat: %f\n", 
	                    pk2Info.head.rwParms.misc.latitude);
	            logit("e","Lon: %f\n", 
	                    pk2Info.head.rwParms.misc.longitude);
	            logit("e","Elev: %f\n",
	                    pk2Info.head.rwParms.misc.elevation);

	            logit("e","Nscans: %d\n", 
	                    pk2Info.head.roParms.stream.nscans);

	            logit("e","Nchannels: %d\n", 
	                    pk2Info.head.rwParms.misc.nchannels);

	            logit("e","SerialNumber: %d\n", 
	                    pk2Info.head.rwParms.misc.serialNumber);


	            logit("e","SensorType:");
	            for (i=0; i<pk2Info.head.rwParms.misc.nchannels; i++)    
	                logit("e"," %d",
	                    pk2Info.head.rwParms.channel[i].sensorType);
	            logit("e"," \n");

	            logit("e","Sensitivity:");
	            for(i=0;i<pk2Info.head.rwParms.misc.nchannels;i++)    
	                logit("e"," %f",
	                    pk2Info.head.rwParms.channel[i].sensitivity);
	            logit("e"," \n");

	            logit("e"," FullScale:\n");
	            for(i=0;i<pk2Info.head.rwParms.misc.nchannels;i++)    
	                logit("e"," %f",
	                    pk2Info.head.rwParms.channel[i].fullscale);
	            logit("e"," \n");


	            logit("e"," SensorGain:\n");
	            for(i=0;i<pk2Info.head.rwParms.misc.nchannels;i++)    
	                logit("e"," %f",
	                    pk2Info.head.rwParms.channel[i].gain);
	            logit("e"," \n");
	        }

	        /* Change snippet start time to seconds since 1970
	        **************************************************/
	        pk2Info.head.roParms.stream.startTime = 
	                pk2Info.head.roParms.stream.startTime  + 
	                                (10*365*24*60*60) + (2*24*60*60);

	        /* See if the GPS clock was weird
	        *********************************/
	        if (Debug) {
	            /* Bit 0=1 if currently checking for presence of GPS board
	               Bit 1=1 if GPS board present
	               Bit 2=1 if error communicating with GPS
	               Bit 3=1 if failed to lock within an allotted time (gpsMaxTurnOnTime)
	               Bit 4=1 if not locked
	               Bit 5=1 when GPS power is ON
	               Bits 6,7=undefined
	             */
	            unsigned char badClockBits = 025;     
	            badClockBits = 00;
	            badClockBits &= pk2Info.head.roParms.timing.gpsStatus;
	            if(badClockBits != 0 && pk2Info.head.roParms.timing.clockSource == 3) 
	                logit("e","WARNING: Still looking for GPS board\n");
	            badClockBits = 04;
	            badClockBits &= pk2Info.head.roParms.timing.gpsStatus;
	            if(badClockBits != 0 && pk2Info.head.roParms.timing.clockSource == 3) 
	                logit("e","WARNING: Error communicating with GPS board\n");
	            badClockBits = 010;
	            badClockBits &= pk2Info.head.roParms.timing.gpsStatus;
	            if(badClockBits != 0 && pk2Info.head.roParms.timing.clockSource == 3) 
	                logit("e","WARNING: GPS board failed to lock within allotted time\n");
	            badClockBits = 020;
	            badClockBits &= pk2Info.head.roParms.timing.gpsStatus;
	            if(badClockBits != 0 && pk2Info.head.roParms.timing.clockSource == 3) 
	                logit("e","WARNING: GPS board not locked on\n");
	        }

	        /* Allocate trace buffers
	        *************************/
            for (ichan = 0; ichan < pk2Info.head.rwParms.misc.nchannels; ichan++) {
		        tracesize = pk2Info.numDataPoints[ichan] * sizeof(long); 

                traceBuf[ichan] = (long*)malloc( tracesize );

                if (traceBuf[ichan] == NULL) {
                    logit("e", "Failed to allocate %d bytes for channel %d\n", tracesize, ichan);

                    /* move the file to the error directory */
                    sprintf (outFname, "%s/%s", EVTFilesErrorDir, fname);
    
                    if (rename (fname, outFname) != 0 ) {
                        logit( "e", "Fatal: Error moving %s: %s\n", fname, strerror(errno));
                        return(-1);
                    }
                    continue;
                } 
            }

	        /* Copy the trace data
	        ***********************/
            for (ichan=0; ichan<pk2Info.head.rwParms.misc.nchannels; ichan++) {
                for (index=0; index < pk2Info.numDataPoints[ichan]; index++) {
                    traceBuf[ichan][index] = (long) pk2Info.Counts[ichan][index];
                }
            }

	        /* Process this event
	        *********************/
	        if (DoFile (&pk2Info, traceBuf ) == EW_SUCCESS) {
	            /* all ok; move the file to the output directory */

	            sprintf (outFname, "%s/%s", EVTFilesOutDir, fname);
	            if ( rename( fname, outFname ) != 0 ) {
	                logit( "e", "Error moving %s: %s\n", fname, strerror(errno));
	                return -1;
	            }
	        }
	        else {      /* something blew up on this file. Preserve the evidence */
	            logit("e","Error processing file %s\n",fname);

	            /* move the file to the error directory */
	            sprintf (outFname, "%s/%s", EVTFilesErrorDir, fname);

	            if (rename (fname, outFname) != 0 ) {
	                logit( "e", "Fatal: Error moving %s: %s\n", fname, strerror(errno));
	                return(-1);
	            }
	            continue;
	        }

	        numfiles += 1;

	        for (ichan = 0; ichan < pk2Info.head.rwParms.misc.nchannels; ichan++)
	            free (traceBuf[ichan]);

	    } /* end of  working loop */
	    
	    sleep_ew(5*1000);
	}
    return -1;
}


/***************************** Heartbeat **************************
 *           Send a heartbeat to the transport ring buffer        *
 ******************************************************************/

thr_ret Heartbeat( void *dummy )
{
    time_t now;

   /* once a second, do the rounds.  */
    while ( 1 ) {
        sleep_ew(1000);
        time(&now);

        /* Beat our heart (into the local Earthworm) if it's time
        ********************************************************/
        if (difftime(now,MyLastInternalBeat) > (double)HeartBeatInterval) {
            ew_status( TypeHeartBeat, 0, "" );
            time(&MyLastInternalBeat);
        }
    }
}


/*********************************************************************************
 *                DoFile
 * Given a structure of evt parameters and the trace, turn it into tracebufs
 *
 *********************************************************************************/
int DoFile( K2InfoStruct *pK2Info , long **Trace )
{
    char  Chan[10], Net[10], Sta[10], Loc[10];
    int   i, ret, ichan;

    if ((pK2Info == NULL) || (Trace == NULL)) {
        logit ("", "Invalid arguments passed in.\n");
        return EW_FAILURE;
    }

    /* Loop over channels from this evt file
    ****************************************/
    for (ichan=0; ichan<pK2Info->head.rwParms.misc.nchannels; ichan++) {
	    sprintf (Chan, "%d", ichan);
		strcpy (Chan, pK2Info->head.rwParms.channel[ichan].id);
	    strcpy (Net, NetworkName);
	    strcpy (Net, pK2Info->head.rwParms.channel[ichan].networkcode);
	    strcpy (Loc, pK2Info->head.rwParms.channel[ichan].locationcode);
		strcpy (Sta, pK2Info->head.rwParms.misc.stnID);
	    for (i=0; i<NchanNames; i++) {
	        if ( strcmp(pK2Info->head.rwParms.misc.stnID, ChannelName[i].sta)==0 &&
	             ichan == ChannelName[i].chan) {
				strcpy (Sta, ChannelName[i].sta);
				strcpy (Chan, ChannelName[i].comp);
				strcpy (Net, ChannelName[i].net);
				strcpy (Loc, ChannelName[i].loc);
	        }
	    }
        /* Yes; set things up for insertion
        ***********************************/
        if (Debug) logit("e","DoFile: %s %s %s %s\n", Sta, Chan, Net, Loc);
        ret=makeEwSnippet(pK2Info, Trace[ichan], Sta, Chan, Net, Loc, TraceMsgBuf);
    } 
    return (EW_SUCCESS);
}

/*********************************************************************************
 *                   makeEwSnippet
 * To create all the stuff for inserting a snippet into the WaveRing
 *********************************************************************************/
int makeEwSnippet( K2InfoStruct *pK2Info, long* Trace, 
                   char* Sta, char* Chan, char* Net, char* Loc, char* pEwTrace)
{
    char          whoami[100];
    TRACE2_HEADER traceBufHdr;
    long          traceBufTrace[MAX_TRACEBUF_SIZ];
    char*         myTracePtr;
    double        actStarttime, actEndtime, nextStartTime, sampleInterval;
    int           i, outBufLen, nextSample, samplesRemaining, samplesInThisMsg, samplesSent;

    sprintf(whoami, "%s: %s: ", progname, "makeEwSnippet");
	if ((pK2Info == NULL) || (Trace == NULL) || (pEwTrace == NULL)) {
        logit ("", "Invalid arguments passed in.\n");;
        return (EW_FAILURE);
    }
    if(TraceBufLen > MAX_TRACEBUF_SIZ) {
    	TraceBufLen = MAX_TRACEBUF_SIZ;
        logit ("", "TraceBufLen reset to %d.\n", TraceBufLen);;
    }

    /* Enter start and stop times
    *****************************/
    actStarttime = (double)pK2Info->head.roParms.stream.startTime + 
                   (double)(pK2Info->head.roParms.stream.startTimeMsec)/1000.;
    actEndtime = actStarttime + (pK2Info->head.roParms.stream.duration/10);

    /* Make up trace_buf messages
    *****************************/
    /*we're going to create a trace_buf message, and then copy it to the supplied
    buffer. We're going to do that until the last (possibly) short message. */

    samplesInThisMsg = 0;  /* number of samples in current trace_buf message      */
    nextSample = 0;        /* total number of samples moved - next sample to move */
    myTracePtr = pEwTrace; /* where we plunk the next message                     */
    
    /* Fill in the fixed portions of the trace_buf header: */
    traceBufHdr.pinno    = 0;
    traceBufHdr.samprate = (float) (pK2Info->frame.streamPar & 4095);
    strncpy( traceBufHdr.sta,  pK2Info->head.rwParms.misc.stnID, TRACE2_STA_LEN);
    strncpy( traceBufHdr.chan, Chan, TRACE2_CHAN_LEN);
    strncpy( traceBufHdr.net,  Net,  TRACE2_NET_LEN);
    strncpy( traceBufHdr.loc,  Loc,  TRACE2_LOC_LEN);
    traceBufHdr.version[0] =  TRACE2_VERSION0;
    traceBufHdr.version[1] =  TRACE2_VERSION1;
#ifdef _INTEL
    strcpy ( traceBufHdr.datatype,"i4"); 
#else
    strcpy ( traceBufHdr.datatype,"s4"); 
#endif
    traceBufHdr.quality[0] =  '\0';
    traceBufHdr.quality[1] =  '\0';

	trcLogo.instid = InstId;
	trcLogo.mod    = MyModId;
	trcLogo.type   = typeWaveform2;
    nextStartTime  = actStarttime;
    sampleInterval = (double)1/(double)traceBufHdr.samprate;
    samplesRemaining = pK2Info->head.roParms.stream.nscans;
    samplesSent = 0;

nextMsg:
    /* How many samples into this message? */
    samplesInThisMsg = (samplesRemaining >= TraceBufLen/sizeof(long)) ? 
                       TraceBufLen/sizeof(long) : samplesRemaining ;

    samplesRemaining = samplesRemaining - samplesInThisMsg; 

    /* Fill out the variable portion of the header */
    traceBufHdr.nsamp     =  samplesInThisMsg;
    traceBufHdr.starttime =  nextStartTime;
    traceBufHdr.starttime =  actStarttime + (double)samplesSent * sampleInterval;
    traceBufHdr.endtime   =  traceBufHdr.starttime + (double)(samplesInThisMsg-1) * sampleInterval;
    nextStartTime = traceBufHdr.endtime + sampleInterval; /* for next message */
    
    /* Fill the message with samples */
    for (i = 0; i < samplesInThisMsg; i++) {
        traceBufTrace[i] = Trace[nextSample++];
    }

    memcpy( (void*)myTracePtr, (void*)&traceBufHdr, sizeof(TRACE2_HEADER) ); 
    myTracePtr = myTracePtr + sizeof(TRACE2_HEADER);

    /* Move the trace */
    memcpy( (void*)myTracePtr, (void*)&traceBufTrace, samplesInThisMsg*sizeof(long) ); 
    outBufLen = sizeof(TRACE2_HEADER) + samplesInThisMsg*sizeof(long);
         
    /* Ship the packet out to the transport ring */
    if (tport_putmsg (&OutRegion, &trcLogo, outBufLen, pEwTrace) != PUT_OK) {
        logit ("et", "Acaus:  Error sending TRACE2_BUF message.\n" );
        return EW_FAILURE;
    }

    /* Are we done yet? */
    if ( samplesRemaining > 0 ) {
	    samplesSent += samplesInThisMsg;
		myTracePtr = pEwTrace; /* where we plunk the next message */
        sleep_ew(TBInterval);
        goto nextMsg;
    }

    return (EW_SUCCESS);
}


/***********************************************************************
 *  config_me() processes command file(s) using kom.c functions;       *
 *                  returns error codes indicating which param is bad  *
 ***********************************************************************/
int config_me( char *configfile )
{
    char    whoami[50], *com, *str;
    int     ncommand;     /* # of required commands you expect to process   */ 
    char    init[20];     /* init flags, one byte for each required command */
    int     nmiss;        /* number of required commands that were missed   */
    int     nfiles, success, i;
    char    envEW_LOG[256];

    sprintf(whoami, "%s: %s: ", progname, "config_me");
/* Set to zero one init flag for each required command 
 *****************************************************/   
	ncommand = 9;
	for( i=0; i<ncommand; i++ ) init[i] = 0;
	
	NchanNames = 0;
	TimeFilter = 1;
	numInst = 0;
	TBInterval = 1000;

/* Open the main configuration file 
 **********************************/
	nfiles = k_open( configfile ); 
	if ( nfiles == 0 ) {
		printf("Error opening command file <%s>\n", configfile );
		return(EW_FAILURE);
	}

/* Process all command files
 ***************************/
	while(nfiles > 0) {  /* While there are command files open */
		while(k_rd()) {       /* Read next line from active file  */
			com = k_str();         /* Get the first token from line */
        /* Ignore blank lines & comments
         *******************************/
			if( !com )           continue;
			if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
         **********************************/
			if( com[0] == '@' ) {
				success = nfiles+1;
				nfiles  = k_open(&com[1]);
				if ( nfiles != success ) {
					printf( "Error opening command file <%s>\n", &com[1] );
					return( EW_FAILURE );
				}
				continue;
			}

        /* Process anything else as a command 
         ************************************/
  /*0*/     
            if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
   /*1*/     
			else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     
			else if( k_its("InRingName") ) {
                str = k_str();
                if(str) strcpy( InRingName, str );
                init[2] = 1;
            }
  /*3*/     
			else if( k_its("OutRingName") ) {
                str = k_str();
                if(str) strcpy( OutRingName, str );
                init[3] = 1;
            }
  /*4*/     
			else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_long();
                init[4] = 1;
            }
  /*5*/     
			else if( k_its("EVTFilesInDir") ) {
                str = k_str();
                if(str) strcpy( EVTFilesInDir, str );
                init[5] = 1;
            }
  /*6*/     
			else if( k_its("NetworkName") ) {
                str = k_str();
                if(str) strncpy( NetworkName, str, TRACE2_NET_LEN );
                init[6] = 1;
            }
    /*7*/     
			else if( k_its("EVTFilesOutDir") ) {
                str = k_str();
                if(str) strcpy( EVTFilesOutDir, str );
                init[7] = 1;
            }
  /*8*/     
			else if( k_its("EVTFilesErrorDir") ) {
                str = k_str();
                if(str) strcpy( EVTFilesErrorDir, str );
                init[8] = 1;
            }

         /* Get the mappings from box id to SCNL name
          ********************************************/ 
  /*opt*/   
			else if( k_its("ChannelName") ) {
                if( NchanNames >= MAXCHAN ) {
                    fprintf( stderr, 
                        "%s Too many <ChannelName> commands"
                        " in <%s>; max=%d; exiting!\n", whoami, 
                        configfile,(int) MAXCHAN );
                    exit( -1 );
                }
         /* Get the box name */
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_VENDOR_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "%s box name <%s> too long" 
                                       " in <ChannelName> cmd; exiting!\n", whoami, str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].box, str);
                }
         /* Get the channel number */
                ChannelName[NchanNames].chan = k_int();
                if(ChannelName[NchanNames].chan > SM_MAX_CHAN){
                    fprintf(stderr, "%s Channel number %d greater "
                                       "than %d in <ChannelName> cmd; exiting\n", whoami,
                    ChannelName[NchanNames].chan,SM_MAX_CHAN);
                    exit(-1);
                }
         /* Get the SCNL name */
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_STA_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "%s station name <%s> too long" 
                                           " in <ChannelName> cmd; exiting!\n", whoami, str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].sta, str);
                } 
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_COMP_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "%s component name <%s> too long"
                                           " in <ChannelName> cmd; exiting!\n", whoami, str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].comp, str);
                } 
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_NET_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "%s network name <%s> too long"
                                           " in <ChannelName> cmd; exiting!\n", whoami, str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].net, str);
                } 
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_LOC_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "%s location name <%s> too long"
                                           " in <ChannelName> cmd; exiting!\n", whoami, str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].loc, str);
                }
                NchanNames++;
            }         
      
			else if( k_its("TraceBufInterval") ) {
                TBInterval = k_int();
            }
      
			else if( k_its("TimeFilter") ) {
                TimeFilter = k_int();
            }

            else if( k_its("Debug") ) {  /*optional command*/
                Debug = 1;
            }

        /* Unknown command
         *****************/ 
        else {
                fprintf( stderr, "<%s> Unknown command in <%s>.\n", com, configfile );
                continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
              printf( "asciiK2ora: Bad <%s> command in <%s>; exitting!\n", 
                                                    com, configfile );
              return( EW_FAILURE );
            }
    }
    nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       printf( "ERROR, no " );
       if ( !init[0] )  printf( "<LogFile> "       );
       if ( !init[1] )  printf( "<MyModuleId> "    );
       if ( !init[2] )  printf( "<InRingName> "      );
       if ( !init[3] )  printf( "<OutRingName> "  );
       if ( !init[4] )  printf( "<HeartBeatInterval> "     );
       if ( !init[5] )  printf( "<EVTFilesInDir> "    );
       if ( !init[6] )  printf( "<NetworkName> "    );
       if ( !init[7] )  printf( "<EVTFilesOutDir> "    );
       if ( !init[8] )  printf( "<EVTFilesErrorDir> "    );
       printf("command(s) in <%s>", configfile );
       return(EW_FAILURE);
   }
   return(EW_SUCCESS);
}


/******************************************************************************
 *  ew_lookup( )   Look up important info from earthworm.h tables             *
 ******************************************************************************/
void ew_lookup( void )
{
	char   whoami[50];
	
	sprintf(whoami, "%s: %s: ", progname, "ew_lookup");
/* Look up keys to shared memory regions
   *************************************/
	if( ( InRingKey = GetKey(InRingName) ) == -1 ) {
		fprintf( stderr, "%s Invalid ring name <%s>; exiting!\n", whoami, InRegion);
		exit( -1 );
	}

/* Look up keys to shared memory regions
   *************************************/
	if( ( OutRingKey = GetKey(OutRingName) ) == -1 ) {
		fprintf( stderr, "%s Invalid ring name <%s>; exiting!\n", whoami, OutRegion);
		exit( -1 );
	}

/* Look up installations of interest
   *********************************/
	if ( GetLocalInst( &InstId ) != 0 ) {
		fprintf( stderr, "%s error getting local installation id; exiting!\n", whoami );
		exit( -1 );
	}

/* Look up modules of interest
   ***************************/
	if ( GetModId( MyModName, &MyModId ) != 0 ) {
		fprintf( stderr, "%s Invalid module name <%s>; exiting!\n", whoami, MyModName );
		exit( -1 );
	}

/* Look up message types of interest
   *********************************/
	if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
		fprintf( stderr, "%s Invalid message type <TYPE_HEARTBEAT>; exiting!\n", whoami );
		exit( -1 );
	}
	if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
		fprintf( stderr, "%s Invalid message type <TYPE_ERROR>; exiting!\n", whoami );
		exit( -1 );
	}
	if ( GetType( "TYPE_TRACEBUF2", &typeWaveform2 ) != 0 ) {
		fprintf( stderr, "%s Invalid message type <TYPE_TRACEBUF2>; exiting!\n", whoami );
		exit( -1 );
	}
	return;
}

/******************************************************************************
 * ew_status() builds a heartbeat or error message & puts it into             *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/
void ew_status( unsigned char type, short ierr, char *note )
{
	MSG_LOGO  logo;
	char      msg[256];
	long      size, t;
	char      whoami[50];
	
	sprintf(whoami, "%s: %s: ", progname, "ew_status");

/* Build the message
 *******************/
	logo.instid = InstId;
	logo.mod    = MyModId;
	logo.type   = type;

	time( &t );

	if( type == TypeHeartBeat ) {
		sprintf( msg, "%ld %ld\n\0", t, MyPid );
	} else if( type == TypeError ) {
		sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
		logit( "et", "%s %s\n", whoami, note );
	}

	size = strlen( msg );   /* don't include the null byte in the message */

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &OutRegion, &logo, size, msg ) != PUT_OK ) {
        if( type == TypeHeartBeat ) {
           logit("et","%s Error sending heartbeat.\n", whoami );
        } else if( type == TypeError ) {
           logit("et","%s Error sending error:%d.\n", whoami, ierr );
        }
   }
   return;
}

/******************************************************************************
 * read_tag(fp)  Read the 16 byte tag, swap bytes if necessary, and print.    *
 ******************************************************************************/
int read_tag (FILE *fp, KFF_TAG *tag)
{
	if ((fp == NULL) || (tag == NULL)) {
		logit ("e", "read_tag: Invalid arguments passed in\n");
		return EW_FAILURE;
	}

    if (fread(tag, 1, 16, fp) != 16) {
		logit ("e", "read_tag: read of file failed.\n");
		return EW_FAILURE;
	}

#ifdef _INTEL
    SwapLong  (&tag->type);
    SwapShort (&tag->length);
    SwapShort (&tag->dataLength);
    SwapShort (&tag->id);
    SwapShort (&tag->checksum);
#endif _INTEL

    if (Debug > 0) 
	{
        logit("e", "TAG: %c %d %d %d %d %d %d %d %d  \n",
                tag->sync,
                (int)tag->byteOrder,
                (int)tag->version,
                (int)tag->instrumentType,
                tag->type, tag->length, tag->dataLength,
                tag->id, tag->checksum);
    }

	/* look ahead, and check on the upcoming record for sanity
	**********************************************************/
	{
		long fpos;
		char checkBuffer[MAX_REC];
		unsigned short checksum=0;
		int bytesToCheck;
		int i;

		fpos = ftell (fp); /* remember where we were */
		bytesToCheck = tag->length + tag->dataLength;
		if (bytesToCheck > MAX_REC) 
		{
			logit ("e", "read_tag: record too long.\n");
			logit ("e", "record + data length > MAX_REC. \n");
			return EW_FAILURE;
		}
		if (fread(checkBuffer, 1, bytesToCheck, fp) != bytesToCheck)
		{
			logit ("e", "read_tag: read of file failed.\n");
			return EW_FAILURE;
		}
		/* look at the synch character */
		if( tag->sync != 'K')
		{
			logit ("e", "read_tag: bad synch character.\n");
			return EW_FAILURE;
		}
		for ( i=0; i<bytesToCheck; i++)
			checksum = checksum + (unsigned char) checkBuffer[i];
		if (checksum != tag->checksum)
		{
			logit("","read_tag: checksum error\n");
			return EW_FAILURE;
		}

		/* now put things back the way they were */
		fseek(fp, fpos, SEEK_SET );
	}


    return EW_SUCCESS;
}


/******************************************************************************
 * read_head(fp)  Read the file header, swap bytes if necessary, and print.   *
 ******************************************************************************/
int read_head (FILE *fp, MW_HEADER *head, int tagLength)
{
   int        i, maxchans, siz;

	if ((fp == NULL) || (head == NULL))
	{
		logit ("e", "read_head: Invalid arguments passed in\n");
		return EW_FAILURE;
	}

/* Read in the file header.
   If a K2, there will be 2040 bytes,
   otherwise assume a Mt Whitney.
 ************************************/
    maxchans = tagLength==2040? MAX_K2_CHANNELS:MAX_MW_CHANNELS;

    if (fread(head, 1, 8, fp) != 8)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}

	siz = sizeof(struct MISC_RO_PARMS) + sizeof(struct TIMING_RO_PARMS);
    if (fread(&head->roParms.misc, 1, siz, fp) != (unsigned int) siz)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}

	siz = sizeof(struct CHANNEL_RO_PARMS)*maxchans;
    if (fread(&head->roParms.channel[0], 1, siz, fp) != (unsigned int) siz)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}

	siz = sizeof(struct STREAM_RO_PARMS);
    if (fread(&head->roParms.stream, 1, siz, fp) != (unsigned int) siz)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}

	siz = sizeof(struct MISC_RW_PARMS)+sizeof(struct TIMING_RW_PARMS);
    if (fread(&head->rwParms.misc, 1, siz, fp) != (unsigned int) siz)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}

    siz = sizeof(struct CHANNEL_RW_PARMS)*maxchans;
    if (fread(&head->rwParms.channel[0], 1, siz, fp) != (unsigned int) siz)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}

    if(tagLength==2040) {
        siz = sizeof(struct STREAM_K2_RW_PARMS);
        if (fread(&head->rwParms.stream, 1, siz, fp) != (unsigned int) siz)
		{
			logit ("e", "read_head: read of file failed.\n");
			return EW_FAILURE;
		}
    } else {
        siz = sizeof(struct STREAM_MW_RW_PARMS);
        if (fread(&head->rwParms.stream, 1, siz, fp) != (unsigned int) siz)
		{
			logit ("e", "read_head: read of file failed.\n");
			return EW_FAILURE;
		}
    }
    siz = sizeof(struct MODEM_RW_PARMS);
    if (fread(&head->rwParms.modem, 1, siz, fp) != (unsigned int) siz)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}


#ifdef _INTEL
	/* Byte-swap values, if necessary */

	/* roParms */
    SwapShort (&head->roParms.headerVersion);
    SwapShort (&head->roParms.headerBytes);

    SwapShort (&head->roParms.misc.installedChan);
    SwapShort (&head->roParms.misc.maxChannels);
    SwapShort (&head->roParms.misc.sysBlkVersion);
    SwapShort (&head->roParms.misc.bootBlkVersion);
    SwapShort (&head->roParms.misc.appBlkVersion);
    SwapShort (&head->roParms.misc.dspBlkVersion);
    SwapShort (&head->roParms.misc.batteryVoltage);
    SwapShort (&head->roParms.misc.crc);
    SwapShort (&head->roParms.misc.flags);
    SwapShort (&head->roParms.misc.temperature);

    SwapShort (&head->roParms.timing.gpsLockFailCount);
    SwapShort (&head->roParms.timing.gpsUpdateRTCCount);
    SwapShort (&head->roParms.timing.acqDelay);
    SwapShort (&head->roParms.timing.gpsLatitude);
    SwapShort (&head->roParms.timing.gpsLongitude);
    SwapShort (&head->roParms.timing.gpsAltitude);
    SwapShort (&head->roParms.timing.dacCount);
    SwapShort (&head->roParms.timing.gpsLastDrift[0]);
    SwapShort (&head->roParms.timing.gpsLastDrift[1]);
    SwapLong (&head->roParms.timing.gpsLastTurnOnTime[0]);
    SwapLong (&head->roParms.timing.gpsLastTurnOnTime[1]);
    SwapLong (&head->roParms.timing.gpsLastUpdateTime[0]);
    SwapLong (&head->roParms.timing.gpsLastUpdateTime[1]);
    SwapLong (&head->roParms.timing.gpsLastLockTime[0]);
    SwapLong (&head->roParms.timing.gpsLastLockTime[1]);

    SwapLong (&head->roParms.stream.startTime);
    SwapLong (&head->roParms.stream.triggerTime);
    SwapLong (&head->roParms.stream.duration);
    SwapShort (&head->roParms.stream.errors);
    SwapShort (&head->roParms.stream.flags);
    SwapShort (&head->roParms.stream.startTimeMsec);
    SwapShort (&head->roParms.stream.triggerTimeMsec);
    SwapLong (&head->roParms.stream.nscans);


    for(i=0;i<head->roParms.misc.maxChannels;i++) 
	{
        SwapLong (&head->roParms.channel[i].maxPeak);
        SwapLong (&head->roParms.channel[i].maxPeakOffset);
        SwapLong (&head->roParms.channel[i].minPeak);
        SwapLong (&head->roParms.channel[i].minPeakOffset);
        SwapLong (&head->roParms.channel[i].mean);
	}


	/* rwParams */
    SwapShort (&head->rwParms.misc.serialNumber);
    SwapShort (&head->rwParms.misc.nchannels);
    SwapShort (&head->rwParms.misc.elevation);
    SwapFloat (&head->rwParms.misc.latitude);
    SwapFloat (&head->rwParms.misc.longitude);
    SwapShort (&head->rwParms.misc.userCodes[0]);
    SwapShort (&head->rwParms.misc.userCodes[1]);
    SwapShort (&head->rwParms.misc.userCodes[2]);
    SwapShort (&head->rwParms.misc.userCodes[3]);
    SwapLong (&head->rwParms.misc.cutler_bitmap);
    SwapLong (&head->rwParms.misc.channel_bitmap);

    SwapShort (&head->rwParms.timing.localOffset);

    for(i=0;i<head->rwParms.misc.nchannels;i++) 
	{
        SwapShort (&head->rwParms.channel[i].sensorSerialNumberExt);
        SwapShort (&head->rwParms.channel[i].north);
        SwapShort (&head->rwParms.channel[i].east);
        SwapShort (&head->rwParms.channel[i].up);
        SwapShort (&head->rwParms.channel[i].altitude);
        SwapShort (&head->rwParms.channel[i].azimuth);
        SwapShort (&head->rwParms.channel[i].sensorType);
        SwapShort (&head->rwParms.channel[i].sensorSerialNumber);
        SwapShort (&head->rwParms.channel[i].gain);
        SwapShort (&head->rwParms.channel[i].StaLtaRatio);
        SwapFloat (&head->rwParms.channel[i].fullscale);
        SwapFloat (&head->rwParms.channel[i].sensitivity);
        SwapFloat (&head->rwParms.channel[i].damping);
        SwapFloat (&head->rwParms.channel[i].naturalFrequency);
        SwapFloat (&head->rwParms.channel[i].triggerThreshold);
        SwapFloat (&head->rwParms.channel[i].detriggerThreshold);
        SwapFloat (&head->rwParms.channel[i].alarmTriggerThreshold);
    }

    SwapShort (&head->rwParms.stream.eventNumber);
    SwapShort (&head->rwParms.stream.sps);
    SwapShort (&head->rwParms.stream.apw);
    SwapShort (&head->rwParms.stream.preEvent);
    SwapShort (&head->rwParms.stream.postEvent);
    SwapShort (&head->rwParms.stream.minRunTime);
    SwapShort (&head->rwParms.stream.VotesToTrigger);
    SwapShort (&head->rwParms.stream.VotesToDetrigger);
    SwapShort (&head->rwParms.stream.Timeout);
    SwapShort (&head->rwParms.stream.TxBlkSize);
    SwapShort (&head->rwParms.stream.BufferSize);
    SwapShort (&head->rwParms.stream.SampleRate);
    SwapLong (&head->rwParms.stream.TxChanMap);
#endif _INTEL


    if(Debug > 0)
	{
	    logit("e", "HEADER: %c%c%c %d %hu %hu \n",
   	         head->roParms.id[0], head->roParms.id[1], head->roParms.id[2],
   	         (int)head->roParms.instrumentCode,
             head->roParms.headerVersion,
             head->roParms.headerBytes);

	    logit("e", "MISC_RO_PARMS: %d %d %d %hu %hu %hu %hu %hu %hu %d %hu %hu %d \n",
            (int)head->roParms.misc.a2dBits,
            (int)head->roParms.misc.sampleBytes,
            (int)head->roParms.misc.restartSource,
            head->roParms.misc.installedChan,
            head->roParms.misc.maxChannels,
            head->roParms.misc.sysBlkVersion,
            head->roParms.misc.bootBlkVersion,
            head->roParms.misc.appBlkVersion,
            head->roParms.misc.dspBlkVersion,
            head->roParms.misc.batteryVoltage,
            head->roParms.misc.crc,
            head->roParms.misc.flags,
            head->roParms.misc.temperature );


	    logit("e", "TIMING_RO_PARMS: %d %d %d %hu %hu %d %d %d %d %hu %d %d %lu %lu %lu %lu %lu %lu \n",
            (int)head->roParms.timing.clockSource,
            (int)head->roParms.timing.gpsStatus,
            (int)head->roParms.timing.gpsSOH,
            head->roParms.timing.gpsLockFailCount,
            head->roParms.timing.gpsUpdateRTCCount,
            head->roParms.timing.acqDelay,
            head->roParms.timing.gpsLatitude,
            head->roParms.timing.gpsLongitude,
            head->roParms.timing.gpsAltitude,
            head->roParms.timing.dacCount,
            head->roParms.timing.gpsLastDrift[0],
            head->roParms.timing.gpsLastDrift[1],
            head->roParms.timing.gpsLastTurnOnTime[0],
            head->roParms.timing.gpsLastTurnOnTime[1],
            head->roParms.timing.gpsLastUpdateTime[0],
            head->roParms.timing.gpsLastUpdateTime[1],
            head->roParms.timing.gpsLastLockTime[0],
            head->roParms.timing.gpsLastLockTime[1] );


	    for(i=0;i<head->roParms.misc.maxChannels;i++) 
		{
        	logit("e", "CHANNEL_RO_PARMS: %d %lu %d %lu %d %d \n",
	            head->roParms.channel[i].maxPeak,
                head->roParms.channel[i].maxPeakOffset,
   	            head->roParms.channel[i].minPeak,
                head->roParms.channel[i].minPeakOffset,
                head->roParms.channel[i].mean,
                head->roParms.channel[i].aqOffset );
    	}

    	logit("e", "STREAM_RO_PARMS: %d %d %d %d %d %d %d %d \n",
            head->roParms.stream.startTime,
            head->roParms.stream.triggerTime,
            head->roParms.stream.duration,
            head->roParms.stream.errors,
            head->roParms.stream.flags,
            head->roParms.stream.startTimeMsec,
            head->roParms.stream.triggerTimeMsec,
            head->roParms.stream.nscans  );



	    logit("e", "MISC_RW_PARMS: %hu %hu %.5s %.33s %d %f %f %d %d %d %d %d %lo %d %.17s %d %d\n",
            head->rwParms.misc.serialNumber,
            head->rwParms.misc.nchannels,
            head->rwParms.misc.stnID,
            head->rwParms.misc.comment,
            head->rwParms.misc.elevation,
            head->rwParms.misc.latitude,
            head->rwParms.misc.longitude,
           (int)head->rwParms.misc.cutlerCode,
           (int)head->rwParms.misc.minBatteryVoltage,
           (int)head->rwParms.misc.cutler_decimation,
           (int)head->rwParms.misc.cutler_irig_type,
            head->rwParms.misc.cutler_bitmap,
            head->rwParms.misc.channel_bitmap,
           (int)head->rwParms.misc.cutler_protocol,
            head->rwParms.misc.siteID,
           (int)head->rwParms.misc.externalTrigger,
           (int)head->rwParms.misc.networkFlag );

	    logit("e", "TIMING_RW_PARMS: %d %d %hu \n",
           (int)head->rwParms.timing.gpsTurnOnInterval,
           (int)head->rwParms.timing.gpsMaxTurnOnTime,
               head->rwParms.timing.localOffset  );

	    for(i=0;i<head->rwParms.misc.nchannels;i++) 
		{
        	logit("e", 
				"CHANNEL_RW_PARMS: %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f %f %f %f %f %f %f \n",
                   head->rwParms.channel[i].id,
                   head->rwParms.channel[i].sensorSerialNumberExt,
                   head->rwParms.channel[i].north,
                   head->rwParms.channel[i].east,
                   head->rwParms.channel[i].up,
                   head->rwParms.channel[i].altitude,
                   head->rwParms.channel[i].azimuth,
                   head->rwParms.channel[i].sensorType,
                   head->rwParms.channel[i].sensorSerialNumber,
                   head->rwParms.channel[i].gain,
               (int)head->rwParms.channel[i].triggerType,
               (int)head->rwParms.channel[i].iirTriggerFilter,
               (int)head->rwParms.channel[i].StaSeconds,
               (int)head->rwParms.channel[i].LtaSeconds,
                   head->rwParms.channel[i].StaLtaRatio,
               (int)head->rwParms.channel[i].StaLtaPercent,
                   head->rwParms.channel[i].fullscale,
                   head->rwParms.channel[i].sensitivity,
                   head->rwParms.channel[i].damping,
                   head->rwParms.channel[i].naturalFrequency,
                   head->rwParms.channel[i].triggerThreshold,
                   head->rwParms.channel[i].detriggerThreshold,
                   head->rwParms.channel[i].alarmTriggerThreshold);
		}

	    logit("e", "STREAM_K2_RW_PARMS: |%d| |%d| |%d| %d %d %d %d %d %d %d %d %d %d %d %d %d %d %lu\n",
            (int)head->rwParms.stream.filterFlag,
            (int)head->rwParms.stream.primaryStorage,
            (int)head->rwParms.stream.secondaryStorage,
                head->rwParms.stream.eventNumber,
                head->rwParms.stream.sps,
                head->rwParms.stream.apw,
                head->rwParms.stream.preEvent,
                head->rwParms.stream.postEvent,
                head->rwParms.stream.minRunTime,
                head->rwParms.stream.VotesToTrigger,
                head->rwParms.stream.VotesToDetrigger,
            (int)head->rwParms.stream.FilterType,
            (int)head->rwParms.stream.DataFmt,
                head->rwParms.stream.Timeout,
                head->rwParms.stream.TxBlkSize,
                head->rwParms.stream.BufferSize,
                head->rwParms.stream.SampleRate,
                head->rwParms.stream.TxChanMap);

	} /* If debug */

    return EW_SUCCESS;

} /* read_head */


/******************************************************************************
 * read_frame(fp)  Read the frame header, swap bytes if necessary.            *
 ******************************************************************************/
int read_frame (FILE *fp, FRAME_HEADER *frame, unsigned long *channels )
{
    unsigned short   frameStatus, frameStatus2, samprate, streamnumber;
    unsigned char    BitMap[4];
    unsigned long    bmap;

	if ((fp == NULL) || (frame == NULL) || (channels == NULL))
	{
		logit ("e", "read_frame: invalid arguments passed in.\n");
		return EW_FAILURE;
	}


    if (fread(frame, 32, 1, fp) != 1)
	{
		logit ("e", "read_frame: read of file failed.\n");
		return EW_FAILURE;
	}


#ifdef _INTEL
    SwapShort (&frame->recorderID);
    SwapShort (&frame->frameSize);
    SwapShort (&frame->blockTime);
    SwapShort (&frame->blockTime2);
    SwapShort (&frame->channelBitMap);
    SwapShort (&frame->streamPar);
    SwapShort (&frame->msec); 
#endif _INTEL

    BitMap[0] = frame->channelBitMap & 255;
    BitMap[1] = frame->channelBitMap >> 8;
    BitMap[2] = frame->channelBitMap1;
    BitMap[3] = 0;

    bmap = *((long *)(BitMap));
    frameStatus      = frame->frameStatus;
    frameStatus2     = frame->frameStatus2;
    samprate         = frame->streamPar & 4095;
    streamnumber     = frame->streamPar >> 12;
    
    if(Debug > 0)
   	    logit("e", "FRAME: %d %d %d %d   %lu X%ho   %hu X%ho %hu %hu     X%ho X%ho %hu X%ho \n",
            (int)frame->frameType,
            (int)frame->instrumentCode,
            frame->recorderID,
            frame->frameSize,
            frame->blockTime,
            frame->channelBitMap,
            frame->streamPar, streamnumber, samprate, samprate>>8,
            frameStatus,
            frameStatus2,
            frame->msec,
            (int)frame->channelBitMap1);

    *channels = bmap;

    return EW_SUCCESS;
}



