 
// time_conv.c

#include <time.h>
#include <windows.h>


//
// Function EwTimeToLongLong()
// Convert Earthworm time to Windows 64-bit system time.
//

LONGLONG EwTimeToLongLong( double ewTime )
{
   SYSTEMTIME    st;
   FILETIME      ft;
   LARGE_INTEGER li;
   time_t        tmt = (time_t)ewTime;
   struct tm     *tmptr;

   tmptr = gmtime( &tmt );
   st.wYear   = tmptr->tm_year + 1900;
   st.wMonth  = tmptr->tm_mon  + 1;
   st.wDay    = tmptr->tm_mday;
   st.wHour   = tmptr->tm_hour;
   st.wMinute = tmptr->tm_min;
   st.wSecond = tmptr->tm_sec;
   st.wMilliseconds = (WORD)(1000.0 * (ewTime - (double)tmt));

// logit( "e", "%04u/%02u/%02u %02u:%02u:%02u.%03u\n",
//        st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds );

   SystemTimeToFileTime( &st, &ft );


   li.LowPart  = ft.dwLowDateTime;
   li.HighPart = ft.dwHighDateTime;
   return li.QuadPart;
}
