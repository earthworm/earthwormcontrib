/*********************************************************************
*
* Program:
*    helisend.c
*
* Description:
*    Continuously outputs a waveform from one analog output channel,
*     using internal timing (uses low-level NI-DAQ functions)
*
* List of key parameters:
*    DBmodeON, OldDataStop, PartialTransferStop, Iterations, WFMstopped
*
* List of NI-DAQ Functions used in this example:
*    Timeout_Config, NIDAQErrorHandler, NIDAQMakeBuffer, WFM_DB_Config,
*     WFM_Group_Setup, WFM_Scale, WFM_Load, WFM_Rate, WFM_ClockRate,
*     WFM_Group_Control, WFM_DB_HalfReady, WFM_DB_Transfer, NIDAQYield,
*     WFM_Check, AO_VWrite
*
* Pin Connection Information:
*    The analog output signal will be available at AO channel 0. The
*     default analog output mode for the DAQ device will be used.
*
*********************************************************************/

#include <stdio.h>
#include <earthworm.h>
#include <transport.h>
#include "nidaqex.h"

#define COUNT 5000                    // Must be divisible by 2

int GetConfig( char *configfile );
void LogConfig( void );


// Main program starts here
// ************************
void main( int argc, char *argv[] )
{
   SHM_INFO region;                   // Structure for shared-memory region

   extern long   RingKey;             // Key to transport ring
   extern unsigned char ModuleId;     // Module id of this process
   extern short  Device;              // NI device number
   extern short  NumChans;            // Number of D/A channels

   const i16 IgnoreWarning = 0;
   const i16 Group = 1;               // Waveform generation group number
   const i16 YieldON = 1;
   const u32 Count = COUNT;
   const u32 HalfCount = Count / 2;
   const i16 Chan = 0;

   i16 status;
   static i16 chanVect[1] = {0};      // Array of channel numbers
   static i16 iBuffer[COUNT] = {0};
   static f64 dBuffer[COUNT] = {0};
   i16 updateTB = 0;
   u32 updateInt = 0;
   i16 WFMstopped;

/* Initialize log file and log configuration parameters
   ****************************************************/
   if ( GetConfig( argv[1] ) == -1 ) return;

   {
      const LogSwitch = 1;            // If 1, log; if 0, don't log
      logit_init( argv[1], ModuleId, 256, LogSwitch );
   }
   LogConfig();

// Attach to existing transport ring
// *********************************
   tport_attach( &region, RingKey );
   logit( "t", "Attached to transport ring: %d\n", RingKey );

// This sets a timeout limit so that if there is something
// wrong, the program won't hang on the WFM_DB_Transfer call.
// *********************************************************
   printf( "Before Timeout_Config\n" );
   {
      extern long Timeout;                     // Number of timer ticks; 55 msec/tick
      status = Timeout_Config( Device, Timeout );
      NIDAQErrorHandler( status, "Timeout_Config", IgnoreWarning );
   }
   printf( "After Timeout_Config\n" );

// Fill dBuffer with 5 volt signal
// *******************************
   {
      unsigned i;
      for ( i = 0; i < Count; i++ )
         dBuffer[i] = 5.00;
   }

// Enable double-buffer mode
// *************************
   printf( "Before WFM_DB_Config\n" );
   {
      const i16 DBmodeON = 1;              // Flag to enable double-buffer mode
      const i16 OldDataStop = 1;           // If 1, disallow regeneration of data
      const i16 PartialTransferStop = 0;   // Allow partial half-buffer transfers

      status = WFM_DB_Config( Device, NumChans, chanVect, DBmodeON,
                              OldDataStop, PartialTransferStop );
      NIDAQErrorHandler( status, "WFM_DB_Config", IgnoreWarning );
   }

// Assign one or more analog output channels to a waveform generation group
// ************************************************************************
   {
      status = WFM_Group_Setup( Device, NumChans, chanVect, Group );
      NIDAQErrorHandler( status, "WFM_Group_Setup", IgnoreWarning );
   }

// Translate an array of floating point voltages to a binary array
// ***************************************************************
   {
      status = WFM_Scale( Device, Chan, Count, 1.0, dBuffer, iBuffer );
      NIDAQErrorHandler( status, "WFM_Scale", IgnoreWarning );
   }

// Assign a waveform buffer to one or more out channels and
// indicate the number of waveform cycles to generate.
// ********************************************************
   {
      const u32 Iterations = 0;
      const i16 FIFOMode = 0;

      status = WFM_Load( Device, NumChans, chanVect, iBuffer, Count,
                         Iterations, FIFOMode );
      NIDAQErrorHandler( status, "WFM_Load", IgnoreWarning );
   }

// Convert a waveform generation update rate into the timebase and
// update-interval values needed to produce the rate you want.
// ***************************************************************
   {
      extern double UpdateRate;  // Rate at which output voltage is updated
      const i16 Units = 0;

      status = WFM_Rate( UpdateRate, Units, &updateTB, &updateInt );
      NIDAQErrorHandler( status, "WFM_Rate", IgnoreWarning );
      logit( "e", "The waveform should be output at a rate of %lf updates/sec.\n",
              UpdateRate );
   }

// Set an update rate and delay rate for a group of analog output channels
// ***********************************************************************
   {
      const i16 WhichClock = 0;
      const i16 DelayMode = 0;

      status = WFM_ClockRate( Device, Group, WhichClock, updateTB,
                              updateInt, DelayMode );
      NIDAQErrorHandler( status, "WFM_ClockRate", IgnoreWarning );
   }

// Start waveform generation for a group of analog output channels
// ***************************************************************
   {
      const i16 OpSTART = 1;

      status = WFM_Group_Control( Device, Group, OpSTART );
      NIDAQErrorHandler( status, "WFM_Group_Control/START", IgnoreWarning );
   }

// Main program loop
// *****************
   while ( tport_getflag(&region) != TERMINATE )
   {
      static i16 loopCount = 0;
      i16 halfReady;

      if ( status != 0 ) break;

      status = WFM_DB_HalfReady( Device, NumChans, chanVect, &halfReady );

// Update next half buffer.  WFM_DB_Transfer will return upon
// updating the circular buffer, but it does NOT indicate that
// the actual data has been generated.
// You can dynamically change the output buffer here!
// This example is reusing the same buffer to ensure simplicity
// of the source code.
// ************************************************************
      if ( (halfReady == 1) && (status == 0) )
      {
         status = WFM_DB_Transfer( Device, NumChans, chanVect, iBuffer, HalfCount );
         NIDAQErrorHandler( status, "WFM_DB_Transfer", IgnoreWarning );

         logit( "e", " %d half buffers generated.\n", ++loopCount );
      }
      else
         NIDAQErrorHandler( status, "WFM_DB_HalfReady", IgnoreWarning );

      NIDAQYield( YieldON );
   }

// If OldDataStop is 1, then NI-DAQ will stop the waveform generation
// when the last updated half buffer is actually completed.  This is
// to prevent underflow conditions.  Check if WFMstopped is set in
// WFM_Check to make sure the last half buffer is 'really' completed.
// When the last half buffer is actually done, WFM_Check will return
// an underflow error, so don't check for errors on purpose.
// ******************************************************************
   do
   {
      u32 itersDone;
      u32 ptsDone;

      WFM_Check( Device, Chan, &WFMstopped, &itersDone, &ptsDone );
      NIDAQYield( YieldON );
   } while ( WFMstopped != 1 );

// Set WFM group back to initial state
// ***********************************
   {
      const i16 OpCLEAR = 0;
      WFM_Group_Control( Device, Group, OpCLEAR );
   }

// Set DB mode back to initial state
// *********************************
   {
      const i16 DBmodeOFF = 0;
      WFM_DB_Config( Device, NumChans, chanVect, DBmodeOFF, 0, 0 );
   }

// Set output to 0 volts
// *********************
   AO_VWrite( Device, Chan, 0.0 );

   logit( "e", "The waveform generation is done!\n");

// Disable timeouts
// ****************
   Timeout_Config( Device, -1 );
   return;
}
