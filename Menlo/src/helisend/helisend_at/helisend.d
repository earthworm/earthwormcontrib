#
#                    CONFIGURATION FILE FOR HELISEND
#                    -------------------------------
#
ModuleId         MOD_HELISEND   # Module id of this instance of helisend
RingName         WAVE_RING      # Transport ring where waveforms are found
HeartbeatInt     15             # Heartbeat interval in seconds
#
#
#                        CHANNEL CONFIGURATION
Device           1              # NI device number
Timeout          180            # Number of timer ticks; 55 msec/tick
NumChans         1              # Number of channels on the DAQ board
UpdateRate       1000.0         # Update rate in samples/second
#
#
#                  SCN VALUES FOR EACH DAQ CHANNEL
# Chan lines must follow the Channel Configuration lines in this file.
# Unused channels may be omitted from the list.
#
#       DAQ      Station/
#     Channel    Comp/Net
#     -------    --------
  Chan    0    GUIDa --- NC
# Chan    1    IRG1a T   NC
# Chan    2    IRG2a T   NC
# Chan    3    MMC   VHZ NN
# Chan    4    GUIDb --- NC
# Chan    5    NFI   VHZ NC
# Chan    6    MBU   VHZ NC
# Chan    7    GUIDc --- NC
# Chan    8    NRL   VHZ NC
# Chan    9    BMR   VHZ NN
# Chan   10    GUIDd --- NC
# Chan   11    RYS   VHZ CI
# Chan   12    PDR   VHZ NC
# Chan   13    LPG   VHZ NC
# Chan   14    JSA   VHZ NC
# Chan   15    RCC   VHZ NN
