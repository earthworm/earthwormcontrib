
//  File config.c

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <kom.h>
#include <earthworm.h>
#include "helisend.h"


// Function declaration
// ********************
int IsComment( char [] );     // Defined below

// The configuration file parameters
// *********************************
short         Device;         // NI device number
long          Timeout;        // Number of timer ticks; 55 msec/tick
long          RingKey;        // Key to transport ring
short         NumChans;       // Number of D/A channels
double        UpdateRate;     // Rate at which output voltage is updated
unsigned char ModuleId;       // Module id of this program
int           HeartbeatInt;   // Heartbeat interval in seconds
SCN           *ChanList;      // Array to fill with SCN values


     /***************************************************************
      *                          GetConfig()                        *
      *         Processes command file using kom.c functions.       *
      *           Returns -1 if any errors are encountered.         *
      ***************************************************************/

#define NCOMMAND  7           // Number of commands in the config file

int GetConfig( char *configfile )
{
   const int ncommand = NCOMMAND;

   char     init[NCOMMAND];     // Flags, one for each command
   int      nmiss;              // Number of commands that were missed
   int      nfiles;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      printf( "Error opening configuration file <%s>\n", configfile );
      return -1;
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          // While there are config files open
   {
      while ( k_rd() )           // Read next line from active file
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          // Get the first token from line

         if ( !com ) continue;             // Ignore blank lines
         if ( com[0] == '#' ) continue;    // Ignore comments

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "Error opening command file <%s>.\n", &com[1] );
               return -1;
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "ModuleId" ) )
         {
            if ( str = k_str() )
            {
               if ( GetModId(str, &ModuleId) == -1 )
               {
                  printf( "Invalid ModuleId <%s>. Exiting.\n", str );
                  return -1;
               }
            }
            init[0] = 1;
         }

         else if ( k_its( "NumChans" ) )
         {
            NumChans = k_int();
            init[1] = 1;
         }

         else if ( k_its( "UpdateRate" ) )
         {
            UpdateRate = k_val();
            init[2] = 1;
         }

         else if ( k_its( "RingName" ) )
         {
            if ( str = k_str() )
            {
               if ( (RingKey = GetKey(str)) == -1 )
               {
                  printf( "Invalid RingName <%s>. Exiting.\n", str );
                  return -1;
               }
            }
            init[3] = 1;
         }

         else if ( k_its( "HeartbeatInt" ) )
         {
            HeartbeatInt = k_int();
            init[4] = 1;
         }

         else if ( k_its( "Device" ) )
         {
            Device = k_int();
            init[5] = 1;
         }

         else if ( k_its( "Timeout" ) )
         {
            Timeout = k_int();
            init[6] = 1;
         }

/* Get the channel list
   ********************/
         else if ( k_its( "Chan" ) )                // Scn value for each channel
         {
            static int first = 1;
            int        chan;

            if ( first )                            // First time a Chan line was found
            {
               if ( init[1]==0 )
               {
                  printf( "Error. In the config file, the NumChans line must appear\n" );
                  printf( "       before any Chan lines.\n" );
                  return -1;
               }
               ChanList = (SCN *) calloc( NumChans, sizeof(SCN) );
               if ( ChanList == NULL )
               {
                  printf( "Error. Cannot allocate the channel list.\n" );
                  return -1;
               }
               first = 0;
            }

            chan = k_int();                          // Get channel number
            if ( chan>=NumChans || chan<0 )
            {
               printf( "Error. Bad channel number (%d) in config file.\n", chan );
               return -1;
            }

            strcpy( ChanList[chan].sta,  k_str() );  // Store Scn value in chan list
            strcpy( ChanList[chan].comp, k_str() );
            strcpy( ChanList[chan].net,  k_str() );
         }

/* An unknown parameter was encountered
   ************************************/
         else
         {
            printf( "<%s> unknown parameter in <%s>\n", com, configfile );
            return -1;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "Bad <%s> command in <%s>.\n", com, configfile );
            return -1;
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss > 0 )
   {
      printf( "ERROR, no " );
      if ( !init[0] ) printf( "<ModuleId> " );
      if ( !init[1] ) printf( "<NumChans> " );
      if ( !init[2] ) printf( "<UpdateRate> " );
      if ( !init[3] ) printf( "<RingName> " );
      if ( !init[4] ) printf( "<HeartbeatInt> " );
      if ( !init[5] ) printf( "<Device> " );
      if ( !init[6] ) printf( "<Timeout> " );
      printf( "command(s) in <%s>.\n", configfile );
      return -1;
   }
   return 0;
}


 /***********************************************************************
  *                              LogConfig()                            *
  *                                                                     *
  *                   Log the configuration parameters                  *
  ***********************************************************************/

void LogConfig( void )
{
   int i;
   int used   = 0;
   int unused = 0;

   logit( "", "Device:          %8d\n",    Device );
   logit( "", "Timeout:         %8d\n",    Timeout );
   logit( "", "ModuleId:        %8u\n",    ModuleId );
   logit( "", "NumChans:        %8d\n",    NumChans );
   logit( "", "UpdateRate:      %8.3lf\n", UpdateRate );
   logit( "", "RingKey:         %8d\n",    RingKey );
   logit( "", "HeartbeatInt:    %8d\n",    HeartbeatInt );
   logit( "", "\n\n" );
   logit( "", "  DAQ\n" );
   logit( "", "channel   Sta Comp Net\n" );
   logit( "", "-------   --- ---- ---\n" );

   for ( i = 0; i < NumChans; i++ )
   {
      if ( strlen( ChanList[i].sta  ) > 0 )          /* This channel is used */
      {
         used++;
         logit( "", "  %4d   %-5s %-3s %-2s %5d\n", i,
            ChanList[i].sta,
            ChanList[i].comp,
            ChanList[i].net );
      }
      else                                           /* This channel is unused */
      {
         unused++;
         logit( "", "  %4d     Unused\n", i );
      }
   }

   logit( "", "\n" );
   logit( "", "Number of channels used:   %3d\n", used );
   logit( "", "Number of channels unused: %3d\n", unused );
   return;
}


    /*********************************************************************
     *                             IsComment()                           *
     *                                                                   *
     *  Accepts: String containing one line from a config file.          *
     *  Returns: 1 if it's a comment line                                *
     *           0 if it's not a comment line                            *
     *********************************************************************/

int IsComment( char string[] )
{
   int i;

   for ( i = 0; i < (int)strlen( string ); i++ )
   {
      char test = string[i];

      if ( test!=' ' && test!='\t' )
      {
         if ( test == '#'  )
            return 1;          /* It's a comment line */
         else
            return 0;          /* It's not a comment line */
      }
   }
   return 1;                   /* It contains only whitespace */
}
