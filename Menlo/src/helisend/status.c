 
#include <stdio.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>

extern unsigned char LocalInstId;
extern unsigned char ModuleId;       // Module id of this program
extern unsigned char TypeHeartBeat;
extern unsigned char TypeError;
extern SHM_INFO      region;         // Structure for shared-memory region
extern pid_t         myPid;          // For restarts by startstop


/****************************************************************************
 * SendHeartbeat() builds a heartbeat message & puts it into shared memory. *
 *                 Writes errors to log file & screen.                      *
 ****************************************************************************/

void SendHeartbeat( void )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   long     t = time(0);     // Get current time

// Build the message
// *****************
   logo.instid = LocalInstId;
   logo.mod    = ModuleId;
   logo.type   = TypeHeartBeat;

   sprintf( msg, "%ld %ld\n", t, myPid );

   size = strlen( msg );   // Don't include null byte in message

// Write message to shared memory
// ******************************
   if ( tport_putmsg( &region, &logo, size, msg ) != PUT_OK )
      logit( "et","Error sending heartbeat.\n" );
   return;
}


/**********************************************************************
 * SendStatus() builds a status message & puts it into shared memory. *
 *              Writes errors to log file & screen.                   *
 **********************************************************************/

void SendStatus( short ierr, char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   long     t = time(0);     // Get current time

// Build the message
// *****************
   logo.instid = LocalInstId;
   logo.mod    = ModuleId;
   logo.type   = TypeError;

   sprintf( msg, "%ld %hd %s\n", t, ierr, note );
   logit( "et", "template: %s\n", note );

   size = strlen( msg );   // Don't include null byte in message

// Write message to shared memory
// ******************************
   if ( tport_putmsg( &region, &logo, size, msg ) != PUT_OK )
      logit( "et","Error sending status message:%d.\n", ierr );
   return;
}
