# 
#                    CONFIGURATION FILE FOR HELISEND
#                    -------------------------------
#
ModuleId         MOD_HELISEND     # Module id of this instance of helisend
RingName         WAVE_RING        # Transport ring where waveforms are found
HeartbeatInt     15               # Heartbeat interval in seconds
NI_Device        1                # National Instruments device number
StaCompNet       CMB BHZ BK       # SCN of tracebuf msg
Chan             0                # A/O channel (0-15)
UpdateDelay      5                # Delay A/O by this many seconds
Debug            0                #   0 = Minimal logging
                                  #   1 = Log received tracebuf messages

# DC_Offset: This value is subtracted from each digital sample before
# converting to analog.  DC_Offset is applied before BitShift.
DC_Offset        100

# BitShift:  Each signed data sample is bit-shifted by BitShift bits.
# This is equivalent to multiplying each sample by 2**BitShift.
# The resulting value is clipped to the range -32768 to 32767.
# DC_Offset is applied before BitShift.
BitShift         +1
