// 
// **********************************
// ni_error.c
//
// National Instruments error handler
// **********************************

#include <earthworm.h>


void NI_Error( char *functionName, unsigned short status )
{
   if ( status < 0 )
      logit( "e", "%s did not execute because of an error.\n",
         functionName );

   if ( status > 0 )
      logit( "e", "%s executed, but with a potentially serious side effect.\n",
         functionName );

   if ( status )
   {
      logit( "e", "NI status code: %hd  Exiting.\n", status );
      exit( -1 );
   }

   return;
}
