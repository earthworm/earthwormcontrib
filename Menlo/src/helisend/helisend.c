/* 
 *   THIS FILE IS UNDER CVS - 
 *   DO NOT MODIFY UNLESS YOU HAVE CHECKED IT OUT!
 *
 *    $Id: helisend.c 218 2006-07-20 21:34:32Z kohler $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.4  2006/07/20 21:34:32  kohler
 *     Bug fix.  Bit-shifting done incorrectly.
 *
 *     Revision 1.3  2006/07/20 19:53:07  kohler
 *     *** empty log message ***
 *
 *     Revision 1.2  2006/07/20 19:12:54  dietz
 *     added CVS logging
 *
 */

/******************************************************
*
* Program:
*    helisend.c
*
* Description:
*    Continuously outputs one analog channel using a
*    National Instruments PCI 6703 board.
*
*******************************************************/

#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <earthworm.h>
#include <transport.h>
#include <trace_buf.h>
#include <swap.h>
#include "helisend.h"
#include "nidaq.h"

int      GetConfig( char * );
void     LogConfig( void );
void     SendHeartbeat( void );
void     NI_Error( char *, unsigned short );
LONGLONG EwTimeToLongLong( double );

pid_t         myPid;              // For restarts by startstop
SHM_INFO      region;             // Structure for shared-memory region
unsigned char LocalInstId;
unsigned char TypeHeartBeat;
unsigned char TypeError;


// Main program starts here
// ************************
int main( int argc, char *argv[] )
{
   static char    traceBuf[MAX_TRACEBUF_SIZ];
   TRACE_HEADER   *trh       = (TRACE_HEADER *)traceBuf;
   long           *longData  =  (long *)(traceBuf + sizeof(TRACE_HEADER));
   short          *shortData = (short *)(traceBuf + sizeof(TRACE_HEADER));

   MSG_LOGO       getlogo;            // Msg logo to get from transport ring
   MSG_LOGO       rcvlogo;            // Msg logo of msg received from transport ring
   long           rcvlen;             // Length of received message
   HANDLE         timerHandle;        // Handle of waitable timer
   short          status;             // Return status of NI-DAQ functions
   long           timeNow;            // Current time
   long           timeLastBeat;       // Time last heartbeat was sent

   extern long    RingKey;            // Key to transport ring
   extern unsigned char ModuleId;     // Module id of this process
   extern short   NI_Device;          // NI device number
   extern short   Chan;               // A/O channel number (0-15)
   extern int     HeartbeatInt;       // Heartbeat interval in seconds
   extern int     Debug;              // For extra logging

// Read parameters from configuration file
// ***************************************
   if ( GetConfig( argv[1] ) == -1 ) return -1;

// Initialize log file and log configuration parameters
// ****************************************************
   {
      const LogSwitch = 1;       // If 1, log; if 0, don't log
      logit_init( argv[1], ModuleId, 256, LogSwitch );
   }
   LogConfig();

// Get process ID for heartbeat messages
// *************************************
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "Cannot get myPid. Exiting.\n" );
      return -1;
   }

// Attach to existing transport ring
// *********************************
   tport_attach( &region, RingKey );

// Create an unnamed waitable timer object.
// Timer will reset when a wait function succeeds.
// **********************************************
   {
      const BOOL manualReset = FALSE;    // Synchronization timer

      timerHandle = CreateWaitableTimer( NULL, manualReset, NULL );
      if ( timerHandle == NULL )
      {
         int lastError = GetLastError();
         logit( "e", "CreateWaitableTimer error: %d  Exiting.\n", lastError  );
         return -1;
      }
   }

// Get logos
// *********
   if ( GetLocalInst( &LocalInstId ) != 0 )
   {
      logit( "e", "Error getting local installation id. Exiting.\n" );
      return -1;
   }
   if ( GetInst( "INST_WILDCARD", &getlogo.instid ) != 0 )
   {
      logit( "e", "Invalid installation id <INST_WILDCARD>. Exiting.\n" );
      return -1;
   }
   if ( GetModId( "MOD_WILDCARD", &getlogo.mod ) != 0 )
   {
      logit( "e", "Invalid module id <MOD_WILDCARD>. Exiting.\n" );
      return -1;
   }
   if ( GetType( "TYPE_TRACEBUF", &getlogo.type ) != 0 )
   {
      logit( "e", "Invalid message type <TYPE_TRACEBUF>. Exiting.\n" );
      return -1;
   }
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      logit( "e", "Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      return -1;
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      logit( "e", "Invalid message type <TYPE_ERROR>. Exiting.\n" );
      return -1;
   }

// Flush transport ring
// ********************
   while ( tport_getmsg( &region, &getlogo, 1, &rcvlogo, &rcvlen, traceBuf,
                         MAX_TRACEBUF_SIZ-1 ) != GET_NONE );

// Set analog output to 0.0 volts
// ******************************
   status = AO_VWrite( NI_Device, Chan, 0.0 );
   NI_Error( "AO_VWrite", status );

// Send heartbeat in first pass through main loop
// **********************************************/
   timeLastBeat = time( &timeNow ) - HeartbeatInt - 1;

// In this loop, get and process one tracebuf message at a time
// ************************************************************
   while ( 1 )
   {
      extern char   Sta[6];                    // Station code
      extern char   Comp[4];                   // Component code
      extern char   Net[3];                    // Network code
      extern long   UpdateDelay;               // Delay A/O this many seconds

      static int    first = 1;                 // First time through this loop
      int           tprc;                      // Transport function return code
      int           samp;                      // Trace buffer loop counter
      LARGE_INTEGER dueTime;                   // When the timer event is to occur
      LONGLONG      updateInterval;            // Update interval in 100 ns units

      int           termFlag = tport_getflag(&region);

// Has termination been requested?
// *******************************
      if ( (termFlag == TERMINATE) || (termFlag == myPid) ) break;

// Send heartbeat message to statmgr
// *********************************
      if  ( time(&timeNow) - timeLastBeat >=  HeartbeatInt )
      {
         timeLastBeat = timeNow;
         SendHeartbeat();
      }

// Get a message of type TYPE_TRACEBUF
// ***********************************
      tprc = tport_getmsg( &region, &getlogo, 1, &rcvlogo, &rcvlen, traceBuf,
                           MAX_TRACEBUF_SIZ-1 );
      if ( tprc == GET_NONE )
      {
         sleep_ew( 1000 );
         continue;
      }
      else if ( tprc == GET_TOOBIG )
      {
         logit( "et", "Retrieved message too big for buffer\n" );
         sleep_ew( 1000 );
         continue;
      }
      else if ( tprc == GET_MISS )
         logit( "et", "Got a message but missed some\n" );
      else if ( tprc == GET_NOTRACK )
         logit( "et", "Got a message but can't tell if any were missed\n" );
      else if ( tprc != GET_OK )
      {
         logit( "et", "Unknown return code from tport_getmsg: %d\n", tprc );
         sleep_ew( 1000 );
         continue;
      }

// Do we want this SCN?
// ********************
      if ( strcmp( trh->sta,  Sta  ) != 0 ) continue;
      if ( strcmp( trh->chan, Comp ) != 0 ) continue;
      if ( strcmp( trh->net,  Net  ) != 0 ) continue;

      WaveMsgMakeLocal( trh );                   // Convert to local byte order

      if ( Debug )
         logit( "et", "Received msg: %s %s %s\n", Sta, Comp, Net );

// Estimate DC offset by averaging samples in first message
// ********************************************************
      if ( first )
      {
         double offset = 0.0;
         for ( samp = 0; samp < trh->nsamp; samp++ )
            offset += (trh->datatype[1] == '2') ? (double)shortData[samp] :
                      (double)longData[samp];
         offset /= (double)trh->nsamp;
         logit( "et", "Estimated DC offset for %s %s %s = %.1lf counts\n",
                Sta, Comp, Net, offset );
         first = 0;
      }

// Compute time of first analog sample, in 100 ns units.
// dueTime.QuadPart is a 64-bit integer.
// Convert sample rate to units of 100 ns.
// ****************************************************
      dueTime.QuadPart = EwTimeToLongLong( trh->starttime ) +
                         Int32x32To64(10000000, UpdateDelay);
      updateInterval   = (LONGLONG)(10000000.0 / trh->samprate + 0.5);

// Loop through all samples in trace buffer.  For each sample,
// activate waitable timer and wait for timer event to occur.
// **********************************************************
      for ( samp = 0; samp < trh->nsamp; samp++ )
      {
         const DWORD timeout = INFINITE;           // Milliseconds or INFINITE
         DWORD       rc;                           // Return code
         long        sample;

         if ( SetWaitableTimer( timerHandle, &dueTime, 0, NULL, NULL, FALSE  ) == 0 )
         {
            int errNum = GetLastError();
            logit( "et", "SetWaitableTimer error: %d.\n", errNum  );
            logit( "e", "Exiting program.\n" );
            return -1;
         }

         rc = WaitForSingleObject( timerHandle, timeout );
         if ( rc == WAIT_FAILED )
         {
            int errNum = GetLastError();
            logit( "et", "WaitForSingleObject error: %d\n", errNum );
            logit( "e", "Exiting program.\n" );
            return -1;
         }
         else if ( rc == WAIT_TIMEOUT )            // Get next tracebuf message
         {
            logit( "et", "WaitForSingleObject timed out after %u msec\n", timeout );
            break;
         }
         else                                      // It's time to write the sample
         {
            extern int DC_Offset;                  // Subtract from each sample
            extern int BitShift;                   // Shift samples this many bits

            sample = (trh->datatype[1] == '2') ? (long)shortData[samp] : longData[samp];
            sample -= DC_Offset;
//          if ( BitShift > 0 ) sample = sample << BitShift;
//          if ( BitShift < 0 ) sample = sample >> (-BitShift);
            sample *= pow( 2.0, BitShift );
            if ( sample < -32768 ) sample = -32768;
            if ( sample >  32767 ) sample =  32767;
            status = AO_Write( NI_Device, Chan, (i16)(sample + 32768) );
            NI_Error( "AO_Write", status );
         }
         dueTime.QuadPart += updateInterval;       // Compute time of next sample
      }
   }

// Cancel the waitable timer
// *************************
   if ( CancelWaitableTimer( timerHandle ) == 0 )
      logit( "et", "CancelWaitableTimer failed. Last error: %u\n", GetLastError() );

// Set analog output to 0.0 volts
// ******************************
   status = AO_VWrite( NI_Device, Chan, 0.0 );
   NI_Error( "AO_VWrite", status );

// End program
// ***********
   logit( "t", "Terminate request received. Exiting program.\n");
   return 0;
}

