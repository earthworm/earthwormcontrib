 
//  File config.c

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <kom.h>
#include <earthworm.h>
#include "helisend.h"


// Function declaration
// ********************
int IsComment( char [] );     // Defined below

// The configuration file parameters
// *********************************
long          RingKey;        // Key to transport ring
unsigned char ModuleId;       // Module id of this program
int           HeartbeatInt;   // Heartbeat interval in seconds
short         NI_Device;      // NI device number
short         Chan;           // A/O channel number (0-15)
long          UpdateDelay;    // Delay A/O this many seconds
char          Sta[6];         // Station code
char          Comp[4];        // Component code
char          Net[3];         // Network code
int           BitShift;       // Shift samples this many bits
int           DC_Offset;      // Subtract from each sample
int           Debug;          // For extra logging


     /***************************************************************
      *                          GetConfig()                        *
      *         Processes command file using kom.c functions.       *
      *           Returns -1 if any errors are encountered.         *
      ***************************************************************/

#define NCOMMAND  10            // Number of commands in config file

int GetConfig( char *configfile )
{
   const int ncommand = NCOMMAND;

   char     init[NCOMMAND];     // Flags, one for each command
   int      nmiss;              // Number of commands that were missed
   int      nfiles;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      printf( "Error opening configuration file <%s>\n", configfile );
      return -1;
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          // While there are config files open
   {
      while ( k_rd() )           // Read next line from active file
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          // Get the first token from line

         if ( !com ) continue;             // Ignore blank lines
         if ( com[0] == '#' ) continue;    // Ignore comments

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "Error opening command file <%s>.\n", &com[1] );
               return -1;
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "ModuleId" ) )
         {
            if ( str = k_str() )
            {
               if ( GetModId(str, &ModuleId) == -1 )
               {
                  printf( "Invalid ModuleId <%s>. Exiting.\n", str );
                  return -1;
               }
            }
            init[0] = 1;
         }

         else if ( k_its( "RingName" ) )
         {
            if ( str = k_str() )
            {
               if ( (RingKey = GetKey(str)) == -1 )
               {
                  printf( "Invalid RingName <%s>. Exiting.\n", str );
                  return -1;
               }
            }
            init[1] = 1;
         }

         else if ( k_its( "HeartbeatInt" ) )
         {
            HeartbeatInt = k_int();
            init[2] = 1;
         }

         else if ( k_its( "NI_Device" ) )
         {
            NI_Device = k_int();
            init[3] = 1;
         }

         else if ( k_its( "Chan" ) )
         {
            Chan = k_int();
            init[4] = 1;
         }

         else if ( k_its( "UpdateDelay" ) )
         {
            UpdateDelay = k_int();
            init[5] = 1;
         }

         else if ( k_its( "StaCompNet" ) )
         {
            if ( str = k_str() ) strcpy( Sta,  str );
            if ( str = k_str() ) strcpy( Comp, str );
            if ( str = k_str() ) strcpy( Net,  str );
            init[6] = 1;
         }

         else if ( k_its( "BitShift" ) )
         {
            BitShift = k_int();
            init[7] = 1;
         }

         else if ( k_its( "DC_Offset" ) )
         {
            DC_Offset = k_int();
            init[8] = 1;
         }

         else if ( k_its( "Debug" ) )
         {
            Debug = k_int();
            init[9] = 1;
         }

/* An unknown parameter was encountered
   ************************************/
         else
         {
            printf( "<%s> unknown parameter in <%s>\n", com, configfile );
            return -1;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "Bad <%s> command in <%s>.\n", com, configfile );
            return -1;
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss > 0 )
   {
      printf( "ERROR: No " );
      if ( !init[0] ) printf( "<ModuleId> " );
      if ( !init[1] ) printf( "<RingName> " );
      if ( !init[2] ) printf( "<HeartbeatInt> " );
      if ( !init[3] ) printf( "<NI_Device> " );
      if ( !init[4] ) printf( "<Chan> " );
      if ( !init[5] ) printf( "<UpdateDelay> " );
      if ( !init[6] ) printf( "<StaCompNet> " );
      if ( !init[7] ) printf( "<BitShift> " );
      if ( !init[8] ) printf( "<DC_Offset> " );
      if ( !init[9] ) printf( "<Debug> " );
      printf( "command(s) in <%s>.\n", configfile );
      return -1;
   }
   return 0;
}


 /***********************************************************************
  *                              LogConfig()                            *
  *                                                                     *
  *                   Log the configuration parameters                  *
  ***********************************************************************/

void LogConfig( void )
{
   logit( "", "StaCompNet:   %s %s %s\n", Sta, Comp, Net );
   logit( "", "NI_Device:    %6d\n", NI_Device );
   logit( "", "Chan:         %6d\n", Chan );
   logit( "", "ModuleId:     %6u\n", ModuleId );
   logit( "", "RingKey:      %6d\n", RingKey );
   logit( "", "HeartbeatInt: %6d\n", HeartbeatInt );
   logit( "", "UpdateDelay:  %6d\n", UpdateDelay );
   logit( "", "BitShift:     %6d\n", BitShift );
   logit( "", "DC_Offset:    %6d\n", DC_Offset );
   logit( "", "Debug:        %6d\n", Debug );
   logit( "", "\n" );
   return;
}


    /*****************************************************************
     *                            IsComment()                        *
     *                                                               *
     *  Accepts: String containing one line from a config file.      *
     *  Returns: 1 if it's a comment line                            *
     *           0 if it's not a comment line                        *
     *****************************************************************/

int IsComment( char string[] )
{
   int i;

   for ( i = 0; i < (int)strlen( string ); i++ )
   {
      char test = string[i];

      if ( test!=' ' && test!='\t' )
      {
         if ( test == '#'  )
            return 1;          // It's a comment line
         else
            return 0;          // It's not a comment line
      }
   }
   return 1;                   // It contains only whitespace
}
