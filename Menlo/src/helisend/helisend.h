  
//    helisend.h

#define MAX_MUXBUF_SIZE 4096

typedef struct
{
   char sta[6];
   char comp[4];
   char net[3];
}
CHAN;

typedef struct
{
   LONGLONG time;    // Time of first sample in 100 ns units
   short    nscan;   // Number of samples per channel
   short    nchan;   // Number of channels
   long     sint;    // Sample interval in milliseconds
}
MUXBUF_HEADER;

