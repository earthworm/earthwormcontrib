/******************************************************************************/
/* evtswitch.c:                                                               */
/*                                                                            */
/* Periodically check a specified directory for .evt files.                   */
/* Reads the files, decodes the station and S/N,                              */
/* and copies to the appropriate directory.                                   */
/*                                                                            */
/* JHL Nov 2004                                                               */
/*                                                                            */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <errno.h>
#include "evtswitch.h"

#include "nkwhdrs_jhl.h"              /* Kinemetrics header definitions */

#ifdef _INTEL
/*  macro returns byte-swapped version of given 16-bit unsigned integer */
#define BYTESWAP_UINT16(var) \
               ((unsigned short)(((unsigned short)(var)>>(unsigned char)8) + \
               ((unsigned short)(var)<<(unsigned char)8)))

/*  macro returns byte-swapped version of given 32-bit unsigned integer */
#define BYTESWAP_UINT32(var) \
                 ((unsigned long)(((unsigned long)(var)>>(unsigned char)24) + \
        (((unsigned long)(var)>>(unsigned char)8)&(unsigned long)0x0000FF00)+ \
        (((unsigned long)(var)<<(unsigned char)8)&(unsigned long)0x00FF0000)+ \
                                 ((unsigned long)(var)<<(unsigned char)24)))
#endif

#ifdef _SPARC
#define BYTESWAP_UINT16(var) (var)
#define BYTESWAP_UINT32(var) (var)
#endif

SHM_INFO  Region;                   /* shared memory region to use for i/o    */
pid_t     myPid;                    /* for restarts by startstop              */

static char MsgBuf[BUFLEN];         /* char string to hold output message     */

EVTTRAK     EvtTrak[MAX_INST];
int         numInst;

static char *TroubleSubdir = "trouble";    /* subdir for problem files        */
static char *TempSubdir    = "temp";       /* subdir for temporary files      */

/* Things to read or derive from configuration file
 **************************************************/
static char     RingName[20];        /* name of transport ring for i/o        */
static char     MyModName[50];       /* speak as this module name/id          */
static int      LogSwitch;           /* 0 if no logfile should be written     */
static int      HeartBeatInterval;   /* seconds betweeen beats to statmgr     */
static char     GetFromDir[NAM_LEN]; /* directory to monitor for data         */
static char     PutInDir[MAX_OUTDIR][NAM_LEN]; /* directory to put data       */
static int      numdir;              /* Number of output directories          */
static unsigned CheckPeriod;         /* secs between looking for new files    */
static int      OpenTries;
static int      OpenWait;
static char     PeerHeartBeatFile[NAM_LEN]; /* name of heartbeat file         */
static int      PeerHeartBeatInterval; /* seconds between heartbeat files     */
       int      Debug;                 /* non-zero -> debug logging           */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o          */
static unsigned char InstId;        /* local installation id                  */
static unsigned char MyModId;       /* Module Id for this program             */
static unsigned char TypeHeartBeat; 
static unsigned char TypeError;

/* Error messages used by evtswitch 
 ***********************************/
#define ERR_CONVERT       0     /* trouble converting a file                  */
#define ERR_PEER_LOST     1     /* no peer heartbeat file for a while         */
#define ERR_PEER_RESUME   2     /* got a peer heartbeat file again            */

#define TEXT_LEN NAM_LEN*3
static char Text[TEXT_LEN];     /* string for log/error messages              */
static char ProgName[NAM_LEN];  /* program name for logging purposes          */

/* File handling stuff
**********************/
static int   print_tag = 0;
static int   print_head = 0;
static int   print_frame = 0;
KFF_TAG      tag;          /* received header tag */
MW_HEADER    head;
K2_HEADER    khead;

int main( int argc, char **argv )
{
	int	     i, j, ret, read_error = 0;
	char      fname[100], ftemp[155], fnew[155], buf[BUFLEN];
	char     *c;
	FILE     *fp, *fout;
	time_t    tnextbeat;    /* next time for local heartbeat  */
	time_t    tnextpeer;    /* next time for peer's heartbeat */
	time_t    tnow;         /* current time */
	int       peerstatus;   /* current status of peer */

/* Check command line arguments 
 ******************************/
	if ( argc != 2 ) {
		fprintf( stderr, "Usage: %s <configfile>\n", argv[0] );
		exit( 0 );
	}
	strcpy( ProgName, argv[1] );
	c = strchr( ProgName, '.' );
	if( c ) *c = '\0';
           
/* Read the configuration file(s)
 ********************************/
	config_me( argv[1] );
   
/* Look up important info from earthworm.h tables
 ************************************************/
	ew_lookup();
   
/* Initialize name of log-file & open it 
 ***************************************/
	logit_init( argv[1], (short) MyModId, TEXT_LEN*2, LogSwitch );
	logit( "" , "%s: Read command file <%s>\n", argv[0], argv[1] );

/* Get process ID for heartbeat messages 
 ***************************************/
	myPid = getpid();
	if( myPid == -1 ) {
		logit("e","%s: Cannot get pid; exiting!\n", ProgName);
		exit (-1);
	}

/* Change to the directory with the input files
 ***********************************************/
	if( chdir_ew( GetFromDir ) == -1 ) {
		logit( "e", "%s: GetFromDir directory <%s> not found; "
		         "exiting!\n", ProgName, GetFromDir );
		exit(-1);
	}
	if(Debug)logit("et","%s: changed to directory <%s>\n", ProgName,GetFromDir);

/* Make sure trouble subdirectory exists
 ***************************************/
   if( CreateDir( TroubleSubdir ) != EW_SUCCESS ) {
      logit( "e", "%s: trouble creating trouble directory: %s/%s\n",
              ProgName, GetFromDir, TroubleSubdir ); 
      return( -1 );
   }

/* Make sure temp subdirectory exists
 ***************************************/
   if( CreateDir( TempSubdir ) != EW_SUCCESS ) {
      logit( "e", "%s: trouble creating temp directory: %s/%s\n",
              ProgName, GetFromDir, TempSubdir ); 
      return( -1 );
   }

/* Attach to Output shared memory ring 
 *************************************/
   tport_attach( &Region, RingKey );
   logit( "", "%s: Attached to public memory region %s: %d\n", 
          ProgName, RingName, RingKey );

/* Force local heartbeat first time thru main loop
   but give peers the full interval before we expect a file
 **********************************************************/
   tnextbeat  = time(NULL) - 1;
   tnextpeer  = time(NULL) + PeerHeartBeatInterval;
   peerstatus = ERR_PEER_RESUME; 


/****************  top of working loop ********************************/
	while(1)  {
     /* Check on heartbeats
      *********************/
        tnow = time(NULL);
        if( tnow >= tnextbeat ) {  /* time to beat local heart */
           ew_status( TypeHeartBeat, 0, "" );
           tnextbeat = tnow + HeartBeatInterval;
        }
        if( PeerHeartBeatInterval &&  tnow > tnextpeer ) {  /* peer is late! */
           if( peerstatus == ERR_PEER_RESUME ) {  /* complain! */
              sprintf(Text,"No PeerHeartBeatFile in %s for over %d sec!",
                      GetFromDir, PeerHeartBeatInterval );
              /*
              ew_status( TypeError, ERR_PEER_LOST, Text );
              */
              peerstatus = ERR_PEER_LOST;            
           }
        }

     /* See if termination has been requested 
      ****************************************/
		if( tport_getflag( &Region ) == TERMINATE ) {
		   logit( "t", "%s: Termination requested; exiting!\n", ProgName );
		   break;
		}

     /* Get a file name
      ******************/    
		ret = GetFileName( fname );
    
		sleep_ew( 1*1000 );
    
		if( ret == 1 ) {  /* No files found; wait for one to appear */
			sleep_ew( CheckPeriod*1000 ); 
			continue;
		}
   
        if(Debug)logit("et","%s: got file name <%s>\n",ProgName,fname);

     /* Open the file.
      ******************/
     /* We open for write, as that will hopefully get us an exclusive open. 
      * We don't ever want to look at a file that's being written to. 
      */
		for(i=0;i<OpenTries;i++) {
			fp = fopen( fname, "rb" );
			if ( fp != NULL ) goto itopend;
			sleep_ew(OpenWait);
		}
		logit("et","%s: Error: Could not open %s after %d*%d ms\n",
		           ProgName, OpenTries,OpenWait);
		itopend:    
		if(i>0) logit("t","Warning: %d attempts required to open file %s\n", i,fname);

     /* If it's a heartbeat file, reset tnextpeer and delete the file
      ****************************************************************/
     /* We're not decoding the contents of the file.
      * The idea is that we don't care when the heartbeat was created, 
      * only when we saw it. This prevents 'heartbeats from the past' 
      * from confusing things, but may not be wise... 
      */
		if( strcmp(fname,PeerHeartBeatFile)==0 ) {
			tnextpeer = time(NULL) + PeerHeartBeatInterval;

			if( peerstatus == ERR_PEER_LOST ) {  /* announce resumption */
				sprintf(Text, "Received PeerHeartBeatFile in %s (Peer is alive)",
				        GetFromDir );
				ew_status( TypeError, ERR_PEER_RESUME, Text );
				peerstatus = ERR_PEER_RESUME;            
			}
			fclose( fp );
			if( remove( fname ) != 0) {
				logit("et", "%s: Cannot delete heartbeat file <%s>; exiting!", 
				    ProgName, fname );
				break;
			}
			continue;
		}

		else if( strcmp(fname,"core")==0 ) {
            fclose( fp );
            if( remove( fname ) != 0) {
                logit("et", "%s: Cannot delete core file <%s>; exiting!", 
                    ProgName, fname );
                break;
            }
            continue;
		}

     /* Pass files to the filter 
        *************************/
        j = filter( fp, fname );
        fclose( fp );

     /* Everything went fine...
      *************************/
        /* Keep file around */
        if( j >= 0 ) { 
			sprintf(ftemp,"%s/%s",TempSubdir,fname );
			fp = fopen( fname, "rb" );
			if ( fp == NULL ) {
				logit( "et", "Error. Can't open file %s\n", fname );
				continue;
			}
			fout = fopen( ftemp, "wb" );
			if ( fout == NULL ) {
				logit( "et", "Error. Can't open new file %s\n", ftemp );
				fclose( fp );
				continue;
			}

		/* Read BUFLEN bytes at a time and write them to fout
		   ****************************************************/
			while ( 1 ) {
				int nbytes = fread( buf, sizeof(char), BUFLEN, fp );
				if ( nbytes > 0 ) {
					if ( fwrite( buf, sizeof(char), nbytes, fout ) == 0 ) {
						logit( "et", "Error writing new file.\n" );
						break;
					}
				}
				if ( feof( fp ) ) break;

		/* If an fread error occurs, exit loop.  
		   ************************************/
				if ( (nbytes == 0) && ferror( fp ) ) {
					logit( "et", "fread() error on %s File partially copied. \n", fname );
					break;
				}
			}

		/* Finish up with this file
		   ************************/
			fclose( fp );
			fclose( fout );

			sprintf(fnew,"%s%s",PutInDir[EvtTrak[j].nextdir], fname );
			if( rename( ftemp, fnew ) != 0 ) {
				logit( "et", "error moving file to %s\n; exiting!", fnew );
				break;
			} else {
				if(Debug)logit("e","%s moved to %s\n", fname, PutInDir[EvtTrak[j].nextdir] );
			}
    
            if( remove( fname ) != 0 ) {
                logit("et","%s: error deleting file: %s\n", ProgName, fname);
            } else  {
                if(Debug)logit("e","%s: Removed %s \n \n", ProgName, fname );
            }
            
        }

        /* Delete the file */
        else  if( j == -1 ){ 
            if( remove( fname ) != 0 ) {
                logit("e","%s: Error deleting file %s; exiting!\n", ProgName, fname);
                break;
            } else  {
                logit("e","%s: Deleted file %s.\n \n", ProgName, fname);
            }
        }

    /* ...or there was trouble! 
     **************************/
		else { 
			logit("e","\n");
			sprintf( Text,"Trouble processing: %s ;", fname );
			ew_status( TypeError, ERR_CONVERT, Text );
			sprintf(fnew,"%s/%s",TroubleSubdir,fname );
			if( rename( fname, fnew ) != 0 ) {
				logit( "e", " error moving file to %s ; exiting!\n", fnew );
				break;
			} else {
				logit( "e", " moved to %s\n", fnew );
			}
		}
	}
    
/* detach from shared memory */
   tport_detach( &Region ); 

/* write a termination msg to log file */
   fflush( stdout );
   return( 0 );

}  


/******************************************************************************
 * filter()  Read and process the .evt file.                                  *
 *  Read a K2-format .evt data file (from the NSMP system),                   * 
 *  Extracts the station name and S/N to determine where the file should go.  * 
 ******************************************************************************/
int filter( FILE *fp, char *fname )
{
    char     whoami[50], sta[10];
    int      i, j, tlen, stat, sernum;
    unsigned long  channels;
    double   actStarttime, actEndtime;

/* Initialize variables 
 **********************/
    sprintf(whoami, " %s: %s: ", "evtswitch", "filter");
    memset( &tag,    0, sizeof(KFF_TAG) );
    memset( &head,   0, sizeof(K2_HEADER) );

/* Close/reopen file in binary read
 **********************************/
    fclose( fp );
    fp = fopen( fname, "rb" );
    if( fp == NULL )               return 0;
    if(strstr(fname, ".txt") != 0) return 0;
    if(strstr(fname, ".evt") == 0 &&
       strstr(fname, ".EVT") == 0) return 0;
        
/* First, just read the header to get some basic info. 
 ******************************************************************/
    rewind(fp);
    tlen = read_tag(fp);
    stat = read_head(fp);
    
/* Extract station name and serial number from the .evt header 
 *********************************************************/
	sernum = head.rwParms.misc.serialNumber;
	strcpy(sta, head.rwParms.misc.stnID);

	if (Debug) logit ("e", "%s Name %s S/N %d\n ", fname, sta, sernum);

	j = numInst;
	for(i=0;i<numInst;i++) {
		if(sernum == EvtTrak[i].sernum) {
			if(strcmp(sta, EvtTrak[i].staname) == 0) j = i;
		}
	}
	if(j == numInst) {
		EvtTrak[j].sernum = sernum;
		strcpy(EvtTrak[j].staname, sta);
		if(numInst < MAX_INST) numInst++;
	}
	
    if(head.roParms.stream.flags != 0) {
        logit("e", "%s %s not data. flags = %d \n", 
                    whoami, fname, head.roParms.stream.flags);
        if(head.roParms.stream.flags != 1) return -1;
    }
    actStarttime = (double)head.roParms.stream.startTime + 
                   (double)(head.roParms.stream.startTimeMsec)/1000.;
    actEndtime = actStarttime + (head.roParms.stream.duration/10);
    
    if(actStarttime < EvtTrak[j].actEndtime) {
		if (Debug) logit ("e", "Out of order! Last EndTime: %f This StartTime: %f\n ", EvtTrak[j].actEndtime, actStarttime);
    }
    EvtTrak[j].actStarttime = actStarttime;
    EvtTrak[j].actEndtime   = actEndtime;
    
	if(++EvtTrak[j].nextdir >= numdir) EvtTrak[j].nextdir = 0;

	if (Debug) logit ("e", "Name %s S/N %d Nextdir %d Index %d\n ", sta, sernum, EvtTrak[j].nextdir, j);
    
    return( j );
}


/******************************************************************************
 * read_tag(fp)  Read the 16 byte tag, swap bytes if necessary, and print.    *
 ******************************************************************************/
int read_tag( FILE *fp )
{
    int        stat;
    
    stat = fread(&tag, 1, 16, fp);
    tag.type       = BYTESWAP_UINT32(tag.type);
    tag.length     = BYTESWAP_UINT16(tag.length);
    tag.dataLength = BYTESWAP_UINT16(tag.dataLength);
    tag.id         = BYTESWAP_UINT16(tag.id);
    tag.checksum   = BYTESWAP_UINT16(tag.checksum);
    
    if(Debug && print_tag) {
        logit("e", "filter: TAG: %c %d %d %d %d %d %d %d %d  \n", 
                tag.sync, 
                (int)tag.byteOrder,
                (int)tag.version, 
                (int)tag.instrumentType,
                tag.type, tag.length, tag.dataLength,
                tag.id, tag.checksum);
    }
    return stat;
}


/******************************************************************************
 * read_head(fp)  Read the file header, swap bytes if necessary, and print.   *
 ******************************************************************************/
int read_head( FILE *fp )
{
   long       la;
   int        i, nchans, stat;
   
/* Read in the file header.
   If a K2, there will be 2040 bytes,
   otherwise assume a Mt Whitney.
 ************************************/
    /*
    stat = fread(&head, tag.length, 1, fp);
    */
    nchans = tag.length==2040? MAX_K2_CHANNELS:MAX_MW_CHANNELS;
    stat = fread(&head, 1, 8, fp);
    stat = fread(&head.roParms.misc,       1, sizeof(struct MISC_RO_PARMS)+sizeof(struct TIMING_RO_PARMS), fp);
    stat = fread(&head.roParms.channel[0], 1, sizeof(struct CHANNEL_RO_PARMS)*nchans, fp);
    stat = fread(&head.roParms.stream,     1, sizeof(struct STREAM_RO_PARMS), fp);
    
    stat = fread(&head.rwParms.misc,       1, sizeof(struct MISC_RW_PARMS)+sizeof(struct TIMING_RW_PARMS), fp);
    stat = fread(&head.rwParms.channel[0], 1, sizeof(struct CHANNEL_RW_PARMS)*nchans, fp);
    if(tag.length==2040) {
        stat = fread(&head.rwParms.stream, 1, sizeof(struct STREAM_K2_RW_PARMS), fp);
    } else {
        stat = fread(&head.rwParms.stream, 1, sizeof(struct STREAM_MW_RW_PARMS), fp);
    }
    stat = fread(&head.rwParms.modem,      1, sizeof(struct MODEM_RW_PARMS), fp);
    
    head.roParms.headerVersion = BYTESWAP_UINT16(head.roParms.headerVersion);
    head.roParms.headerBytes   = BYTESWAP_UINT16(head.roParms.headerBytes);
    
    if(Debug && print_head)
    logit("e", "HEADER: %c%c%c %d %hu %hu \n", 
            head.roParms.id[0], head.roParms.id[1], head.roParms.id[2], 
       (int)head.roParms.instrumentCode, 
            head.roParms.headerVersion, 
            head.roParms.headerBytes);
    
    head.roParms.misc.installedChan  = BYTESWAP_UINT16(head.roParms.misc.installedChan);
    head.roParms.misc.maxChannels    = BYTESWAP_UINT16(head.roParms.misc.maxChannels);
    head.roParms.misc.sysBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.sysBlkVersion);
    head.roParms.misc.bootBlkVersion = BYTESWAP_UINT16(head.roParms.misc.bootBlkVersion);
    head.roParms.misc.appBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.appBlkVersion);
    head.roParms.misc.dspBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.dspBlkVersion);
    head.roParms.misc.batteryVoltage = BYTESWAP_UINT16(head.roParms.misc.batteryVoltage);
    head.roParms.misc.crc            = BYTESWAP_UINT16(head.roParms.misc.crc);
    head.roParms.misc.flags          = BYTESWAP_UINT16(head.roParms.misc.flags);
    head.roParms.misc.temperature    = BYTESWAP_UINT16(head.roParms.misc.temperature);
    nchans = head.roParms.misc.maxChannels;
    
    if(Debug && print_head)
    logit("e", "MISC_RO_PARMS: %d %d %d %hu %hu %hu %hu %hu %hu %d %hu %hu %d \n", 
        (int)head.roParms.misc.a2dBits, 
        (int)head.roParms.misc.sampleBytes, 
        (int)head.roParms.misc.restartSource, 
            head.roParms.misc.installedChan,
            head.roParms.misc.maxChannels,
            head.roParms.misc.sysBlkVersion,
            head.roParms.misc.bootBlkVersion,
            head.roParms.misc.appBlkVersion,
            head.roParms.misc.dspBlkVersion,
            head.roParms.misc.batteryVoltage,
            head.roParms.misc.crc,
            head.roParms.misc.flags,
            head.roParms.misc.temperature );
    
    head.roParms.timing.gpsLockFailCount  = BYTESWAP_UINT16(head.roParms.timing.gpsLockFailCount);
    head.roParms.timing.gpsUpdateRTCCount = BYTESWAP_UINT16(head.roParms.timing.gpsUpdateRTCCount);
    head.roParms.timing.acqDelay          = BYTESWAP_UINT16(head.roParms.timing.acqDelay);
    head.roParms.timing.gpsLatitude       = BYTESWAP_UINT16(head.roParms.timing.gpsLatitude);
    head.roParms.timing.gpsLongitude      = BYTESWAP_UINT16(head.roParms.timing.gpsLongitude);
    head.roParms.timing.gpsAltitude       = BYTESWAP_UINT16(head.roParms.timing.gpsAltitude);
    head.roParms.timing.dacCount          = BYTESWAP_UINT16(head.roParms.timing.dacCount);
    head.roParms.timing.gpsLastDrift[0]   = BYTESWAP_UINT16(head.roParms.timing.gpsLastDrift[0]);
    head.roParms.timing.gpsLastDrift[1]   = BYTESWAP_UINT16(head.roParms.timing.gpsLastDrift[1]);
    
    head.roParms.timing.gpsLastTurnOnTime[0] = BYTESWAP_UINT32(head.roParms.timing.gpsLastTurnOnTime[0]);
    head.roParms.timing.gpsLastTurnOnTime[1] = BYTESWAP_UINT32(head.roParms.timing.gpsLastTurnOnTime[1]);
    head.roParms.timing.gpsLastUpdateTime[0] = BYTESWAP_UINT32(head.roParms.timing.gpsLastUpdateTime[0]);
    head.roParms.timing.gpsLastUpdateTime[1] = BYTESWAP_UINT32(head.roParms.timing.gpsLastUpdateTime[1]);
    head.roParms.timing.gpsLastLockTime[0]   = BYTESWAP_UINT32(head.roParms.timing.gpsLastLockTime[0]);
    head.roParms.timing.gpsLastLockTime[1]   = BYTESWAP_UINT32(head.roParms.timing.gpsLastLockTime[1]);
    
    if(Debug && print_head)
    logit("e", "TIMING_RO_PARMS: %d %d %d %hu %hu %d %d %d %d %hu %d %d %lu %lu %lu %lu %lu %lu \n", 
        (int)head.roParms.timing.clockSource, 
        (int)head.roParms.timing.gpsStatus, 
        (int)head.roParms.timing.gpsSOH, 
            head.roParms.timing.gpsLockFailCount, 
            head.roParms.timing.gpsUpdateRTCCount, 
            head.roParms.timing.acqDelay, 
            head.roParms.timing.gpsLatitude, 
            head.roParms.timing.gpsLongitude, 
            head.roParms.timing.gpsAltitude, 
            head.roParms.timing.dacCount,
            head.roParms.timing.gpsLastDrift[0], 
            head.roParms.timing.gpsLastDrift[1], 
            
            head.roParms.timing.gpsLastTurnOnTime[0], 
            head.roParms.timing.gpsLastTurnOnTime[1], 
            head.roParms.timing.gpsLastUpdateTime[0], 
            head.roParms.timing.gpsLastUpdateTime[1], 
            head.roParms.timing.gpsLastLockTime[0], 
            head.roParms.timing.gpsLastLockTime[1] );
           
    
    
    for(i=0;i<nchans;i++) {
        head.roParms.channel[i].maxPeak       = BYTESWAP_UINT32(head.roParms.channel[i].maxPeak);
        head.roParms.channel[i].maxPeakOffset = BYTESWAP_UINT32(head.roParms.channel[i].maxPeakOffset);
        head.roParms.channel[i].minPeak       = BYTESWAP_UINT32(head.roParms.channel[i].minPeak);
        head.roParms.channel[i].minPeakOffset = BYTESWAP_UINT32(head.roParms.channel[i].minPeakOffset);
        head.roParms.channel[i].mean          = BYTESWAP_UINT32(head.roParms.channel[i].mean);
        head.roParms.channel[i].aqOffset      = BYTESWAP_UINT32(head.roParms.channel[i].aqOffset);
        
        if(Debug && print_head)
        logit("e", "CHANNEL_RO_PARMS: %d %lu %d %lu %d %d \n", 
            head.roParms.channel[i].maxPeak,  
            head.roParms.channel[i].maxPeakOffset,  
            head.roParms.channel[i].minPeak,  
            head.roParms.channel[i].minPeakOffset,  
            head.roParms.channel[i].mean,  
            head.roParms.channel[i].aqOffset );
        
    }
    
    
    head.roParms.stream.startTime       = BYTESWAP_UINT32(head.roParms.stream.startTime);
    head.roParms.stream.triggerTime     = BYTESWAP_UINT32(head.roParms.stream.triggerTime);
    head.roParms.stream.duration        = BYTESWAP_UINT32(head.roParms.stream.duration);
    head.roParms.stream.errors          = BYTESWAP_UINT16(head.roParms.stream.errors);
    head.roParms.stream.flags           = BYTESWAP_UINT16(head.roParms.stream.flags);
    head.roParms.stream.nscans          = BYTESWAP_UINT32(head.roParms.stream.nscans);
    head.roParms.stream.startTimeMsec   = BYTESWAP_UINT16(head.roParms.stream.startTimeMsec);
    head.roParms.stream.triggerTimeMsec = BYTESWAP_UINT16(head.roParms.stream.triggerTimeMsec);
    
    if(Debug && print_head)
    logit("e", "STREAM_RO_PARMS: %d %d %d %d %d %d %d %d \n", 
            head.roParms.stream.startTime,  
            head.roParms.stream.triggerTime,  
            head.roParms.stream.duration,  
            head.roParms.stream.errors,  
            head.roParms.stream.flags,  
            head.roParms.stream.startTimeMsec,  
            head.roParms.stream.triggerTimeMsec,  
            head.roParms.stream.nscans  );
    
    head.rwParms.misc.serialNumber   = BYTESWAP_UINT16(head.rwParms.misc.serialNumber);
    head.rwParms.misc.nchannels      = BYTESWAP_UINT16(head.rwParms.misc.nchannels);
    head.rwParms.misc.elevation      = BYTESWAP_UINT16(head.rwParms.misc.elevation);
                                  la = BYTESWAP_UINT32(*(long *)(&head.rwParms.misc.latitude));
    head.rwParms.misc.latitude       = *(float *)(&(la));
                                  la = BYTESWAP_UINT32(*(long *)(&head.rwParms.misc.longitude));
    head.rwParms.misc.longitude      = *(float *)(&la);
    head.rwParms.misc.cutler_bitmap  = BYTESWAP_UINT32(head.rwParms.misc.cutler_bitmap);
    head.rwParms.misc.channel_bitmap = BYTESWAP_UINT32(head.rwParms.misc.channel_bitmap);
    
    if(Debug && print_head)
    logit("e", "MISC_RW_PARMS: %hu %hu %.5s %.33s %d %f %f %d %d %d %d %d %lo %d %.17s %d %d\n", 
            head.rwParms.misc.serialNumber,
            head.rwParms.misc.nchannels,
            head.rwParms.misc.stnID,  
            head.rwParms.misc.comment, 
            head.rwParms.misc.elevation,
            head.rwParms.misc.latitude, 
            head.rwParms.misc.longitude,
            
        (int)head.rwParms.misc.cutlerCode, 
        (int)head.rwParms.misc.minBatteryVoltage, 
        (int)head.rwParms.misc.cutler_decimation, 
        (int)head.rwParms.misc.cutler_irig_type, 
            head.rwParms.misc.cutler_bitmap,
            head.rwParms.misc.channel_bitmap,
        (int)head.rwParms.misc.cutler_protocol, 
            head.rwParms.misc.siteID, 
        (int)head.rwParms.misc.externalTrigger, 
        (int)head.rwParms.misc.networkFlag );
    
    head.rwParms.timing.localOffset = BYTESWAP_UINT16(head.rwParms.timing.localOffset);
    
    if(Debug && print_head)
    logit("e", "TIMING_RW_PARMS: %d %d %hu \n", 
        (int)head.rwParms.timing.gpsTurnOnInterval,  
        (int)head.rwParms.timing.gpsMaxTurnOnTime, 
            head.rwParms.timing.localOffset  );
    
    for(i=0;i<nchans;i++) {
        head.rwParms.channel[i].north      = BYTESWAP_UINT16(head.rwParms.channel[i].north);
        head.rwParms.channel[i].east       = BYTESWAP_UINT16(head.rwParms.channel[i].east);
        head.rwParms.channel[i].up         = BYTESWAP_UINT16(head.rwParms.channel[i].up);
        head.rwParms.channel[i].altitude   = BYTESWAP_UINT16(head.rwParms.channel[i].altitude);
        head.rwParms.channel[i].azimuth    = BYTESWAP_UINT16(head.rwParms.channel[i].azimuth);
        head.rwParms.channel[i].sensorType = BYTESWAP_UINT16(head.rwParms.channel[i].sensorType);
        head.rwParms.channel[i].sensorSerialNumber    = BYTESWAP_UINT16(head.rwParms.channel[i].sensorSerialNumber);
        head.rwParms.channel[i].sensorSerialNumberExt = BYTESWAP_UINT16(head.rwParms.channel[i].sensorSerialNumberExt);
        head.rwParms.channel[i].gain                  = BYTESWAP_UINT16(head.rwParms.channel[i].gain);
        head.rwParms.channel[i].StaLtaRatio           = BYTESWAP_UINT16(head.rwParms.channel[i].StaLtaRatio);
        
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].fullscale));
        head.rwParms.channel[i].fullscale             = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].sensitivity));
        head.rwParms.channel[i].sensitivity           = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].damping));
        head.rwParms.channel[i].damping               = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].naturalFrequency));
        head.rwParms.channel[i].naturalFrequency      = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].triggerThreshold));
        head.rwParms.channel[i].triggerThreshold      = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].detriggerThreshold));
        head.rwParms.channel[i].detriggerThreshold    = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].alarmTriggerThreshold));
        head.rwParms.channel[i].alarmTriggerThreshold = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].calCoil));
        head.rwParms.channel[i].calCoil               = *(float *)(&la);
        if(head.rwParms.channel[i].range != ' ' && (head.rwParms.channel[i].range < '0' || head.rwParms.channel[i].range > '9') )
        	head.rwParms.channel[i].range = ' ';
        if(head.rwParms.channel[i].sensorgain != ' ' && (head.rwParms.channel[i].sensorgain < '0' || head.rwParms.channel[i].sensorgain > '9') )
        	head.rwParms.channel[i].sensorgain = ' '; 
        	
        if(Debug && print_head)
        logit("e", "CHANNEL_RW_PARMS: %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f %f %f %f %f %f %f %f %c %c %s %s \n", 
            head.rwParms.channel[i].id,  
            head.rwParms.channel[i].sensorSerialNumberExt,  
            head.rwParms.channel[i].north,  
            head.rwParms.channel[i].east,  
            head.rwParms.channel[i].up,  
            head.rwParms.channel[i].altitude,
            head.rwParms.channel[i].azimuth,
            head.rwParms.channel[i].sensorType,
            head.rwParms.channel[i].sensorSerialNumber,
            head.rwParms.channel[i].gain,
        (int)head.rwParms.channel[i].triggerType,  
        (int)head.rwParms.channel[i].iirTriggerFilter,  
        (int)head.rwParms.channel[i].StaSeconds,  
        (int)head.rwParms.channel[i].LtaSeconds,  
            head.rwParms.channel[i].StaLtaRatio,
        (int)head.rwParms.channel[i].StaLtaPercent,  
            head.rwParms.channel[i].fullscale,
            head.rwParms.channel[i].sensitivity,
            head.rwParms.channel[i].damping,
            head.rwParms.channel[i].naturalFrequency,
            head.rwParms.channel[i].triggerThreshold,
            head.rwParms.channel[i].detriggerThreshold,
            head.rwParms.channel[i].alarmTriggerThreshold,
            head.rwParms.channel[i].calCoil,
            head.rwParms.channel[i].range,
            head.rwParms.channel[i].sensorgain,
            head.rwParms.channel[i].networkcode,
            head.rwParms.channel[i].locationcode
        );
        
    }
    
    
    head.rwParms.stream.eventNumber = BYTESWAP_UINT16(head.rwParms.stream.eventNumber);
    head.rwParms.stream.sps         = BYTESWAP_UINT16(head.rwParms.stream.sps);
    head.rwParms.stream.apw         = BYTESWAP_UINT16(head.rwParms.stream.apw);
    head.rwParms.stream.preEvent    = BYTESWAP_UINT16(head.rwParms.stream.preEvent);
    head.rwParms.stream.postEvent   = BYTESWAP_UINT16(head.rwParms.stream.postEvent);
    head.rwParms.stream.minRunTime  = BYTESWAP_UINT16(head.rwParms.stream.minRunTime);
    head.rwParms.stream.Timeout     = BYTESWAP_UINT16(head.rwParms.stream.Timeout);
    head.rwParms.stream.TxBlkSize   = BYTESWAP_UINT16(head.rwParms.stream.TxBlkSize);
    head.rwParms.stream.BufferSize  = BYTESWAP_UINT16(head.rwParms.stream.BufferSize);
    head.rwParms.stream.SampleRate  = BYTESWAP_UINT16(head.rwParms.stream.SampleRate);
    head.rwParms.stream.TxChanMap   = BYTESWAP_UINT32(head.rwParms.stream.TxChanMap);
    head.rwParms.stream.VotesToTrigger   = BYTESWAP_UINT16(head.rwParms.stream.VotesToTrigger);
    head.rwParms.stream.VotesToDetrigger = BYTESWAP_UINT16(head.rwParms.stream.VotesToDetrigger);
    
    
    if(Debug && print_head)
    logit("e", "STREAM_K2_RW_PARMS: |%d| |%d| |%d| %d %d %d %d %d %d %d %d %d %d %d %d %d %d %lu\n", 
        (int)head.rwParms.stream.filterFlag,  
        (int)head.rwParms.stream.primaryStorage,  
        (int)head.rwParms.stream.secondaryStorage,  
            head.rwParms.stream.eventNumber,  
            head.rwParms.stream.sps,  
            head.rwParms.stream.apw,  
            head.rwParms.stream.preEvent,  
            head.rwParms.stream.postEvent,  
            head.rwParms.stream.minRunTime,  
            head.rwParms.stream.VotesToTrigger,
            head.rwParms.stream.VotesToDetrigger,
        (int)head.rwParms.stream.FilterType,  
        (int)head.rwParms.stream.DataFmt,  
            head.rwParms.stream.Timeout,
            head.rwParms.stream.TxBlkSize,
            head.rwParms.stream.BufferSize,
            head.rwParms.stream.SampleRate,
            head.rwParms.stream.TxChanMap
              );
            
    return stat;
}


/******************************************************************************
 *  config_me() processes command file(s) using kom.c functions;              *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
#define ncommand 12
void config_me( char *configfile )
{
	char     init[ncommand]; /* init flags, one byte for each required command */
	int      nmiss;          /* number of required commands that were missed   */
	char    *com, *str;
	int      i, j, k, n, num_sides, nfiles, success;
   
/* Set to zero one init flag for each required command 
 *****************************************************/   
	for( i=0; i<ncommand; i++ )  init[i] = 0;
	numdir = numInst = 0;

/* Open the main configuration file 
 **********************************/
	nfiles = k_open( configfile ); 
	if ( nfiles == 0 ) {
		fprintf( stderr, "%s: Error opening command file <%s>; exiting!\n", 
		         ProgName, configfile );
		exit( -1 );
	}

/* Process all command files
 ***************************/
   while(nfiles > 0) {  /* While there are command files open */
        while(k_rd()) {       /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr, 
                          "%s: Error opening command file <%s>; exiting!\n",
                           ProgName, &com[1] );
                  exit( -1 );
               }
               continue;
			}

        /* Process anything else as a command 
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("GetFromDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( GetFromDir, str, NAM_LEN );
                init[3] = 1;
            }
  /*4*/     else if( k_its("CheckPeriod") ) {
                CheckPeriod = k_int();
                init[4] = 1;
            }
  /*5*/     else if( k_its("Debug") ) {
                Debug = k_int();
                init[5] = 1;
            }
  /*6*/     else if( k_its("OpenTries") ) {
                OpenTries = k_int();
                init[6] = 1;
            }
  /*7*/     else if( k_its("OpenWait") ) {
                OpenWait = k_int();
                init[7] = 1;
            }
  /*8*/     else if( k_its("PeerHeartBeatFile") ) {
                str = k_str();
                if(str) strncpy( PeerHeartBeatFile, str, NAM_LEN);
                init[8] = 1;
            }

  /*9*/     else if( k_its("PeerHeartBeatInterval") ) {
                PeerHeartBeatInterval = k_int();
                init[9] = 1;
            }
  /*10*/    else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_int();
                init[10] = 1;
			}
  /*11*/    else if( k_its("PutInDir") ) {
                if(numdir < MAX_OUTDIR) {
					str = k_str();
					n = strlen(str);   /* Make sure directory name has proper ending! */
					if( str[n-1] != '/' ) strcat(str, "/");
					if(str) strncpy( PutInDir[numdir], str, NAM_LEN );
					numdir++;
					init[11] = 1;
                }
            }

               /* optional commands */

/*NR*/


         /* Unknown command
          *****************/ 
            else {
                fprintf( stderr, "%s: <%s> Unknown command in <%s>.\n", 
                         ProgName, com, configfile );
                continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr, 
                       "%s: Bad <%s> command in <%s>; exiting!\n",
                        ProgName, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }
   
/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "%s: ERROR, no ", ProgName );
       if ( !init[0] )  fprintf( stderr, "<LogFile> "       );
       if ( !init[1] )  fprintf( stderr, "<MyModuleId> "    );
       if ( !init[2] )  fprintf( stderr, "<GetFromDir> "    );
       if ( !init[3] )  fprintf( stderr, "<CheckPeriod> "   );
       if ( !init[4] )  fprintf( stderr, "<Debug> "         );
       if ( !init[5] )  fprintf( stderr, "<OpenTries> "     );
       if ( !init[6] )  fprintf( stderr, "<CheckPeriod> "   );
       if ( !init[7] )  fprintf( stderr, "<OpenWait> "      );
       if ( !init[8] )  fprintf( stderr, "<PeerHeartBeatFile> "     );
       if ( !init[9] )  fprintf( stderr, "<PeerHeartBeatInterval> " );
       if ( !init[10])  fprintf( stderr, "<HeartBeatInterval> "     );
       fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }
   
   return;
}


/******************************************************************************/
/*                                                                            */
/*  ew_lookup( )   Look up important info from earthworm.h tables             */
/*                                                                            */
/******************************************************************************/
void ew_lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 ) {
        fprintf( stderr,
                "%s:  Invalid ring name <%s>; exiting!\n",
                 ProgName, RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr, 
              "%s: error getting local installation id; exiting!\n",ProgName );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid module name <%s>; exiting!\n", ProgName, MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n",ProgName );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_ERROR>; exiting!\n",ProgName );
      exit( -1 );
   }
   return;
} 

/******************************************************************************/
/*                                                                            */
/* ew_status() builds a heartbeat or error message & puts it into             */
/*                   shared memory.  Writes errors to log file & screen.      */
/*                                                                            */
/******************************************************************************/
void ew_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size, t;
 
/* Build the message
 *******************/ 
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat ) {
        sprintf( msg, "%ld %ld\n\0", t, myPid);
   }
   else if( type == TypeError ) {
        sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
        logit( "et", "%s: %s\n", ProgName, note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */     

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK ) {
        if( type == TypeHeartBeat ) {
           logit("et","%s:  Error sending heartbeat.\n", ProgName );
        }
        else if( type == TypeError ) {
           logit("et","%s:  Error sending error:%d.\n", ProgName, ierr );
        }
   }
   return;
}



