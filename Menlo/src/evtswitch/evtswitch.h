/******************************************************************************/
/* evtswitch.h:                                                             */
/*                                                                            */
/* JHL Nov 2004                                                               */
/*                                                                            */
/******************************************************************************/

/* Define Strong Motion file types
 *********************************/
#define NAM_LEN 	  100     /* length of full directory name          */
#define BUFLEN      65000     /* define maximum size for an event msg   */
#define MAX_OUTDIR      5     /* maximum number of output directories   */
#define MAX_INST      300     /* maximum number of stations             */

typedef struct _evttrack
{
	int     sernum;
	char    staname[5];
	int     nextdir;
	double  actStarttime;
	double  actEndtime;
} EVTTRAK;

/* Function prototypes
 *********************/
void config_me ( char * );
void ew_lookup ( void );
void ew_status ( unsigned char, short, char * );

int filter( FILE *fp, char *fname );
int read_tag( FILE *fp );
int read_head( FILE *fp );

void logit( char *, char *, ... );   /* logit.c      sys-independent  */

