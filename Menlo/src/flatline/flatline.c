/*************************************************************************
 *      flatline.c:                                                       *
 * Requests data from waveserver(s) and plots gif files in a helicorder  *
 * format.  These files are then transferred to webserver(s).            *
 *                                                                       *
 * This is an abbreviated command line version of heli1 for producing    *
 * one-time plots on demand from the web interface flatline.pl.           *
 *                                                                       *
 * Jim Luetgert 07/07/03                                                 *
 *************************************************************************/

#include <platform.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <time_ew.h>
#include <chron3.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include "mem_circ_queue.h" 
#include <swap.h>
#include <trace_buf.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <wait.h>

#include <ws_clientII.h>
#include "gd.h"
#include "gdfontt.h"   /*  6pt      */
#include "gdfonts.h"   /*  7pt      */
#include "gdfontmb.h"  /*  9pt Bold */
#include "gdfontl.h"   /* 13pt      */
#include "gdfontg.h"   /* 10pt Bold */

#include "flatline.h" 

/*
 * Description of a data gap.
 * Note: if a gap would be declared at end of data, the data must be 
 * truncated instead of adding another GAP structure. A gap may be
 * declared at the start of the data, however.
 */
typedef struct _GAP *PGAP;
typedef struct _GAP 
{
  double starttime;  /* time of first sample in the gap                      */
  double gapLen;     /* time from first gap sample to first sample after gap */
  long firstSamp;    /* index of first gap sample in data buffer             */
  long lastSamp;     /* index of last gap sample in data buffer              */
  PGAP next;         /* The next gap structure in the list                   */
} GAP;

/* Structure for keeping track of buffer of trace data */
typedef struct _DATABUF 
{
  double rawData[MAXTRACELTH*5];   /* The raw trace data; native byte order                */
  double delta;      /* The nominal time between sample points               */
  double starttime;  /* time of first sample in raw data buffer              */
  double endtime;    /* time of last sample in raw data buffer               */
  long nRaw;         /* number of samples in raw data buffer, including gaps */
  long lenRaw;       /* length to the rawData array                          */
  GAP *gapList;      /* linked list of gaps in raw data                      */
  int nGaps;         /* number of gaps found in raw data                     */
} DATABUF;


/* Functions in this source file
 *******************************/
void Process(Global *But);
void SetUp(Global *, FILE *out);
void Sort_Servers (Global *, double);
void hpsort(int, double ra[]);

int Build_Menu (Global *);
int In_Menu_list (Global *);
short RequestWave(Global *, int, double *, char *, char *, char *, char *, double, double);
void Decode_Time( double, TStrct *);
void Encode_Time( double *, TStrct *);
void date22( double, char *);
void Get_Req_Chans(Global *But);
void Get_Sta_Info(Global *);
int Put_Sta_Info(Global *);
void config_me( char *,  Global *); /* reads configuration (.d) file via Carl's routines  */


/* Things to read from configuration file
 ****************************************/
static int  Debug = 1;               /* debug flag                              */

/* Variables for talking to statmgr
 **********************************/
char      Text[150];
pid_t     MyPid;                     /* Our own pid  */

static Global BStruct;               /* Private area for the threads            */
double    Data[MAXTRACELTH];         /* Trace array                             */

/* Other globals
 ***************/
double    sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's        */
                                     /* time 0 and 1970-01-01 00:00:00.0 GMT    */
char      string[20];
char      module[70];

/*************************************************************************
 *  main( int argc, char **argv )                                        *
 *************************************************************************/

main( int argc, char **argv )
{
    char    whoami[50];
    time_t  atime, now;
    int     i, j;

    /* Check command line arguments
     ******************************/
    if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>  %d\n", module, argc);
        exit( 0 );
    }
    
    /* Zero the wave server arrays *
     *******************************/
    for (i=0; i< MAX_WAVESERVERS; i++) {
        memset( BStruct.wsIp[i],      0, MAX_ADRLEN);
        memset( BStruct.wsPort[i],    0, MAX_ADRLEN);
        memset( BStruct.wsComment[i], 0, MAX_ADRLEN);
    }
    strcpy(module, argv[1]);
    for(i=0;i<(int)strlen(module);i++) if(module[i]=='.') module[i] = 0;
    strcpy(BStruct.mod, module);
    sprintf(whoami, " %s: %s: ", module, "main");

    /* Get our own Pid for restart purposes
    ***************************************/
    MyPid = getpid();

    /* Read the configuration file(s)
     ********************************/
    config_me( argv[1], &BStruct );
    logit_init( argv[1], (short) 99, 256, 1 );

    fprintf( stderr,"%s Read command file <%s>\n", whoami, argv[1] );

    Get_Req_Chans(&BStruct);

    Get_Sta_Info(&BStruct);
    
    
    if(!Build_Menu (&BStruct)) {
        fprintf( stderr, "%s No Menu! Just quit.\n", whoami);
        for(i=0;i<BStruct.nServer;i++) wsKillMenu(&(BStruct.menu_queue[i]));
        exit(-1);
    }
    
    
    Process(&BStruct);
    
  /* Kill the used menus */    
    for(i=0;i<BStruct.nServer;i++) wsKillMenu(&(BStruct.menu_queue[i]));

}


/********************************************************************
 *  Process does the actual processing for each SCN                 *
 ********************************************************************/

void Process(Global *But)
{
    char    whoami[50], psdfile[50];
    int     i, j;
    int     gif_is_there, psd_is_there;
    time_t  current_time;
    FILE    *out;
    
    sprintf(whoami, " %s: %s: ", But->mod, "Process");
    
    sprintf( psdfile, "%sflatline.txt",But->target);

    out = fopen(psdfile, "wb");
    if(out == 0L) {
        fprintf( stderr, "Unable to open PSD output File: %s\n", psdfile);    
    } else {

	    But->FirstTime = time(&current_time) - 60*10;
	    if(But->TrigTime.Year == 0) {
		    But->FirstTime = time(&current_time) - 60*10;
	    } else {
		    Encode_Time( &But->FirstTime, &(But->TrigTime));  
	    }
	    for(i=0;i<But->RSCNL;i++) {
	        strncpy( But->Site, But->ReqChan[i].Site,  6);
	        strncpy( But->Comp, But->ReqChan[i].Comp,  6);
	        strncpy( But->Net,  But->ReqChan[i].Net,  6);
	        strncpy( But->Loc,  But->ReqChan[i].Loc,  6);

		    sprintf(But->SCNnam, "%s_%s_%s_%s", 
		    	But->ReqChan[i].Site, But->ReqChan[i].Comp, But->ReqChan[i].Net, But->ReqChan[i].Loc);
		    sprintf(But->SCNtxt, "%5s %3s %2s %2s", 
		    	But->ReqChan[i].Site, But->ReqChan[i].Comp, But->ReqChan[i].Net, But->ReqChan[i].Loc);
		    sprintf(But->SCN,    "%s%s%s%s",   
		    	But->ReqChan[i].Site, But->ReqChan[i].Comp, But->ReqChan[i].Net, But->ReqChan[i].Loc);
			But->current  = -1;
		    for(j=0;j<But->NSCN;j++) {
		        if(strcmp(But->Chan[j].Site, But->Site )==0 && 
		           strcmp(But->Chan[j].Comp, But->Comp)==0 && 
		           strcmp(But->Chan[j].Loc,  But->Loc)==0 && 
		           strcmp(But->Chan[j].Net,  But->Net )==0) {
		            But->current  = j;
		        }
		    }
		    if(But->current  != -1) {
			    Put_Sta_Info(But);
				SetUp(But, out);
		    }
	    }
	    fclose(out);
    }
}
 
 
/********************************************************************
 *  SetUp does the initial setup and first set of images            *
 ********************************************************************/

void SetUp(Global *But, FILE *out)
{
    char    whoami[50], time1[25], time2[25], sip[25], sport[25], sid[50];
    double  tankStarttime, tankEndtime, EndPlotTime;
    double  StartTime, EndTime, Duration, ZTime, B[8];
    double  sum, sumsq, mean, meansq, sqmean, stddev, rat;
    int     i, j, k, jj, successful, server, hour1, hour2;
    int     diff, ntrys, req_samples;
    int     minPerStep, minOfDay;
    TStrct  t0, Time1, Time2;  
    
    int		ii, order, lx;
    double	dw, aflo, afhi, f, pi, percent;

    sprintf(whoami, " %s: %s: ", But->mod, "SetUp");
    
    StartTime = But->FirstTime;
    
    if(In_Menu_list(But)) {
        
        Sort_Servers(But, StartTime);
        if(But->nentries <= 0) {
            fprintf( stderr, "%s Sort_Servers rejected request for: %s. \n", 
                  whoami, But->SCNtxt);
	        goto quit;
        }
                    
        k = But->index[0];
        tankStarttime = But->TStime[k];
        tankEndtime   = But->TEtime[k];
        
        if(But->Debug) {
            date22 (StartTime, time1);
            fprintf( stderr, "%s Requested Start Time: %s. \n", 
                  whoami, time1);
            date22 (tankStarttime, time1);
            date22 (tankEndtime,   time2);
            date22 (But->TStime[k], time1);
            date22 (But->TEtime[k], time2);
            strcpy(sip,   But->wsIp[k]);
            strcpy(sport, But->wsPort[k]);
            strcpy(sid,   But->wsComment[k]);
            fprintf( stderr, "%s Got menu for: %s. %s %s %s %s <%s>\n", 
                  whoami, But->SCNtxt, time1, time2, sip, sport, sid);
            for(j=0;j<But->nentries;j++) {
                k = But->index[j];
                date22 (But->TStime[k], time1);
                date22 (But->TEtime[k], time2);
                fprintf( stderr, "            %d %d %s %s %s %s <%s>\n", 
                      j, k, time1, time2, But->wsIp[k], But->wsPort[k], But->wsComment[k]);
            }
        }
        
        if(StartTime > tankEndtime) {
            StartTime = tankEndtime - 120;
        }
        Duration = But->secsPerGulp;
        StartTime = StartTime - Duration;
        if(StartTime < tankStarttime) {
            StartTime = tankStarttime;
        }
        
        EndTime = StartTime + Duration;      
        
        EndPlotTime = tankEndtime;
        
        if(But->Debug) {
            date22 (StartTime, time1);
            date22 (EndTime,   time2);
            fprintf( stderr, "%s Data for: %s. %s %s %s %s <%s>\n", 
                 whoami, But->SCNtxt, time1, time2, But->wsIp[k], But->wsPort[k], But->wsComment[k]);
        }
        
        /* Try to get some data
        ***********************/
        i = 0;
        ntrys = 10;
        while(EndTime < EndPlotTime && i < ntrys) {
	        for(jj=0;jj<But->nentries;jj++) {
	            server = But->index[jj];
	            successful = RequestWave(But, server, Data, But->Site, 
	                But->Comp, But->Net, But->Loc, StartTime, Duration);
	            if(successful == 1) {   /*    Plot this trace to memory. */
	                break;
	            }
	            else if(successful == 2) {
	                if(But->Debug) {
	                    fprintf( stderr, "%s Data for: %s. RequestWave error 2\n", 
	                            whoami, But->SCNtxt);
	                }
	                continue;
	            }
	            else if(successful == 3) {   /* Gap in data */
	                if(But->Debug) {
	                    fprintf( stderr, "%s Data for: %s. RequestWave error 3\n", 
	                            whoami, But->SCNtxt);
	                }
	                
	            }
	        }
	        
	        if(successful == 1) {   /*    Plot this trace to memory. */
	            diff = But->Max - But->Min;
	            date22 (StartTime, time1);
	            
				sum = 0;
				sumsq = 0;
				for (j=0; j<But->Npts; ++j) {
				    sum += Data[j]-But->Mean;
				    sumsq += (Data[j]-But->Mean)*(Data[j]-But->Mean);
				}
				mean = sum/But->Npts;
				meansq = mean*mean;
				sqmean = sumsq/But->Npts;
				stddev = sqrt(sqmean - meansq);
				rat = diff/stddev;
				req_samples = But->samp_sec*Duration + 1;
	            
	            if(But->Debug) {
		        	fprintf( stderr, "%s %12s %7d  %12.6f %d %d %d at  %s\n", 
		        		whoami, But->SCNtxt, diff, stddev, req_samples, But->Npts_wsv, But->Npts, time1);
                }
	            else if(diff < But->LoBound || diff > But->HiBound || 
	            	    stddev < But->StdBound || rat > But->RatBound || abs(req_samples - But->Npts_wsv) > 1) {
		        	fprintf( stderr, "%s %12s %7d  %12.6f  %12.6f  %d %d %d %d at  %s\n", 
		        		whoami, But->SCNtxt, diff, stddev, rat, But->samp_sec, req_samples, But->Npts_wsv, But->Npts, time1);
		        }
	            if(diff < But->LoBound || diff > But->HiBound || stddev < But->StdBound || rat > But->RatBound) {
		        	fprintf( out, "%s %10d  %14.4f  %12.4f\n", But->SCNtxt, diff, stddev, rat);
		        }
		        break;
		    } else {
		        if(i >= ntrys-1) fprintf( stderr, "%s %s Error on data request.\n", whoami, But->SCNtxt);
		    }
		    StartTime -= Duration;
	        EndTime = StartTime + Duration;
	        i += 1;      
        }

    } else {
        fprintf( stderr, "%s %s not in menu.\n", whoami, But->SCNtxt);
    }
quit:        
    sleep_ew(10);
}


/*************************************************************************
 *   Sort_Servers                                                        *
 *      From the table of waveservers containing data for the current    *
 *      SCN, the table is re-sorted to provide an intelligent order of   *
 *      search for the data.                                             *
 *                                                                       *
 *      The strategy is to start by dividing the possible waveservers    *
 *      into those which contain the requested StartTime and those which *
 *      don't.  Those which do are retained in the order specified in    *
 *      the config file allowing us to specify a preference for certain  *
 *      waveservers.  Those waveservers which do not contain the         *
 *      requested StartTime are sorted such that the possible data       *
 *      retrieved is maximized.                                          *
 *************************************************************************/

void Sort_Servers (Global *But, double StartTime)
{
    char    whoami[50];
    char    c22[25];
    double  tdiff[MAX_WAVESERVERS*2];
    int     i, j, k, jj, last_jj, kk, hold, index[MAX_WAVESERVERS*2];
    
    sprintf(whoami, " %s: %s: ", But->mod, "Sort_Servers");
    i = 0;
        /* Throw out servers with data too old. */
    j = 0;
    while(j<But->nentries) {
        k = But->index[j];
        if(StartTime > But->TEtime[k]) {
            if(But->Debug) {
                date22( StartTime, c22);
                fprintf( stderr,"%s %d %d  %s", whoami, j, k, c22);
                    fprintf( stderr, " %s %s <%s>\n", 
                          But->wsIp[k], But->wsPort[k], But->wsComment[k]);
                date22( But->TEtime[k], c22);
                fprintf( stderr,"ends at: %s rejected.\n", c22);
            }
            But->nentries -= 1;
            for(jj=j;jj<But->nentries;jj++) {
                But->index[jj] = But->index[jj+1];
            }
        } else j++;
    }
    if(But->nentries <= 1) return;  /* nothing to sort */
            
    /* Calculate time differences between StartTime needed and tankStartTime */
    /* And copy positive values to the top of the list in the order given    */
    jj = 0;
    for(j=0;j<But->nentries;j++) {
        k = index[j] = But->index[j];
        tdiff[k] = StartTime - But->TStime[k];
        if(tdiff[k]>=0) {
            But->index[jj++] = index[j];
            tdiff[k] = -65000000; /* two years should be enough of a flag */
        }
    }
    last_jj = jj;
    
    /* Sort the index list copy in descending order */
    j = 0;
    do {
        k = index[j];
        for(jj=j+1;jj<But->nentries;jj++) {
            kk = index[jj];
            if(tdiff[kk]>tdiff[k]) {
                hold = index[j];
                index[j] = index[jj];
                index[jj] = hold;
            }
            k = index[j];
        }
        j += 1;
    } while(j < But->nentries);
    
    /* Then transfer the negatives */
    for(j=last_jj,k=0;j<But->nentries;j++,k++) {
        But->index[j] = index[k];
    }
}    


/*******************************************************************************
 *    hpsort performs a sifting sort in place of the n members of the double   *
 *    array ra.  This is from Recipes in C, so it is assumed that the array    *
 *    is 1-based as in fortran rather than 0-based as in C!!                   *
 *******************************************************************************/

void hpsort(int n, double ra[])
{
    int   i, j, k, ir;
    double rra;
    
    if(n<2) return;
    k  = (n >> 1)+1;
    ir = n;
        
    for(;;) {
        if(k > 1) rra = ra[--k];
        else {
            rra = ra[ir];
            ra[ir] = ra[1];
            if(--ir == 1) {
                ra[1] = rra;
                break;
            }
        }
        i = k; j = k+k;
        while(j<=ir) {
            if(j<ir && ra[j]<ra[j+1]) j++;
            if(rra<ra[j]) {
                ra[i] = ra[j];
                i = j;
                j <<= 1;
            } else break;
        }
        ra[i] = rra;
    }
}


/*************************************************************************
 *   Build_Menu ()                                                       *
 *      Builds the waveservers' menus                                    *
 *      Each waveserver has its own menu so that we can do intelligent   *
 *      searches for data.                                               *
 *************************************************************************/

int Build_Menu (Global *But)
{
    char    whoami[50], server[100];
    int     i, j, retry, ret, rc, got_a_menu;
    WS_PSCN scnp;
    WS_MENU menu; 

    sprintf(whoami, " %s: %s: ", But->mod, "Build_Menu");
    setWsClient_ewDebug(0);  
    if(But->WSDebug) setWsClient_ewDebug(1);  
    got_a_menu = 0;
    
    for(j=0;j<But->nServer;j++) But->index[j] = j;
    
    for (i=0;i< But->nServer; i++) {
        retry = 0;
        But->inmenu[i] = 0;
        sprintf(server, " %s:%s <%s>", But->wsIp[i], But->wsPort[i], But->wsComment[i]);
Append:
        ret = wsAppendMenu(But->wsIp[i], But->wsPort[i], &But->menu_queue[i], But->wsTimeout);
        
        
        if (ret == WS_ERR_NONE) {
            But->inmenu[i] = got_a_menu = 1;
        }
        else if (ret == WS_ERR_INPUT) {
            fprintf( stderr,"%s Connection to %s input error\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_EMPTY_MENU) 
            fprintf( stderr,"%s Unexpected empty menu from %s\n", whoami, server);
        else if (ret == WS_ERR_BUFFER_OVERFLOW) 
            fprintf( stderr,"%s Buffer overflowed for %s\n", whoami, server);
        else if (ret == WS_ERR_MEMORY) 
            fprintf( stderr,"%s Waveserver %s out of memory.\n", whoami, server);
        else if (ret == WS_ERR_PARSE) 
            fprintf( stderr,"%s Parser failed for %s\n", whoami, server);
        else if (ret == WS_ERR_TIMEOUT) {
            fprintf( stderr,"%s Connection to %s timed out during menu.\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_BROKEN_CONNECTION) {
            fprintf( stderr,"%s Connection to %s broke during menu\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_SOCKET) 
            fprintf( stderr,"%s Could not create a socket for %s\n", whoami, server);
        else if (ret == WS_ERR_NO_CONNECTION) { 
     /*       if(But->Debug) */
                fprintf( stderr,"%s Could not get a connection to %s to get menu.\n", whoami, server);
        }
        else fprintf( stderr,"%s Connection to %s returns error: %d\n", whoami, server, ret);
    }
    /* Let's make sure that servers in our server list have really connected.
       **********************************************************************/  
    if(got_a_menu) {
        for(j=0;j<But->nServer;j++) {
            if ( But->inmenu[j]) {
                rc = wsGetServerPSCN( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
                if ( rc == WS_ERR_EMPTY_MENU ) {
                    if(But->Debug) fprintf( stderr,"%s Empty menu for %s:%s <%s> \n", 
                                        whoami, But->wsIp[j], But->wsPort[j], But->wsComment[j]);
                    But->inmenu[j] = 0; 
                    continue;
                }
                if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                    if(But->Debug) fprintf( stderr,"%s  %s:%s <%s> not in menu.\n", 
                                        whoami, But->wsIp[j], But->wsPort[j], But->wsComment[j]);
                    But->inmenu[j] = 0; 
                    continue;
                }
            }
        }
    }
    /* Now, detach 'em and let RequestWave attach only the ones it needs.
       **********************************************************************/  
    for(j=0;j<But->nServer;j++) {
        if(But->inmenu[j]) {
            menu = But->menu_queue[j].head;
            if ( menu->sock > 0 ) {  
                wsDetachServer( menu );
            }
        }
    }
    return got_a_menu;
}


/*************************************************************************
 *   In_Menu_list                                                        *
 *      Determines if the scn is in the waveservers' menu.               *
 *      If there, the tank starttime and endtime are returned.           *
 *      Also, the Server IP# and port are returned.                      *
 *************************************************************************/

int In_Menu_list (Global *But)
{
    char    whoami[50], server[100];
    int     i, j, rc;
    WS_PSCN scnp;
    
    sprintf(whoami, " %s: %s: ", But->mod, "In_Menu_list");
    i = 0;
    But->nentries = 0;
    for(j=0;j<But->nServer;j++) {
        if(But->inmenu[j]) {
            sprintf(server, " %s:%s <%s>", But->wsIp[j], But->wsPort[j], But->wsComment[j]);
            rc = wsGetServerPSCN( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
            if ( rc == WS_ERR_EMPTY_MENU ) {
                if(But->Debug) fprintf( stderr,"%s Empty menu for %s \n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                if(But->Debug) fprintf( stderr,"%s  %s not in menu.\n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }

            while ( 1 ) {
               if(strcmp(scnp->sta,  But->Site)==0 && 
                  strcmp(scnp->chan, But->Comp)==0 && 
                  strcmp(scnp->net,  But->Net )==0) {
                  But->TStime[j] = scnp->tankStarttime;
                  But->TEtime[j] = scnp->tankEndtime;
                  But->index[But->nentries]  = j;
                  But->nentries += 1;
               }
               if ( scnp->next == NULL )
                  break;
               else
                  scnp = scnp->next;
            }
        }
    }
    if(But->nentries>0) return 1;
    return 0;
}


/********************************************************************
 *  RequestWave                                                     *
 *                                                                  *
 *   k - waveserver index                                           *
 ********************************************************************/
short RequestWave(Global *But, int k, double *Data, 
            char *Site, char *Comp, char *Net, char *Loc, double Stime, double Duration)
{
    char     *token, server[wsADRLEN*3], whoami[50], SCNtxt[17];
    double   temp;           
    float    samprate, sample;
    long     j;
    int      i, nsamp, nbytes, io, retry;
    int      success, ret, decimate, WSDebug = 0;          
    TRACE_REQ   request;
    WS_MENU  menu = NULL;
    WS_PSCN  pscn = NULL;
    
    decimate = 1;
    
    sprintf(whoami, " %s: %s: ", But->mod, "RequestWave");
    WSDebug = But->WSDebug;
    success = retry = 0;
gettrace:
    strcpy(request.sta,  Site);
    strcpy(request.chan, Comp);
    strcpy(request.net,  Net );
    request.waitSec = 0;
    request.pinno   = 0;
    request.reqStarttime = Stime;
    request.reqEndtime   = request.reqStarttime + Duration;
    request.partial = 1;
    request.pBuf    = But->TraceBuf;
    request.bufLen  = MAXTRACEBUF;
    request.timeout = But->wsTimeout;
    request.fill    = 919191;
    sprintf(SCNtxt, "%s %s %s %s", Site, Comp, Net, Loc);
    
    /*    Put out WaveServer request here and wait for response */
    /* Get the trace
     ***************/
    /* rummage through all the servers we've been told about */
    if ( (wsSearchSCN( &request, &menu, &pscn, &(But->menu_queue[k])  )) == WS_ERR_NONE ) {
        strcpy(server, menu->addr);
        strcat(server, "  ");
        strcat(server, menu->port);
    } else strcpy(server, "unknown");
 
    if(WSDebug) {   
        fprintf( stderr,"\n%s Issuing request to wsGetTraceAscii: server: %s\n", whoami, server);
        fprintf( stderr,"    %s %f %f %d\n", SCNtxt,
             Stime, request.reqEndtime, request.timeout);
        fprintf( stderr," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
    }
    
    io = wsGetTraceAscii(&request, &But->menu_queue[k], But->wsTimeout);
 /*   io = wsGetTraceAsciiDec(&request, &But->menu_queue[k], But->wsTimeout, decimate);   */
    
    if (io == WS_ERR_NONE ) {
        if(WSDebug) {
            fprintf( stderr," %s server: %s trace %s: went ok first time. Got %ld bytes\n",
                whoami, server, SCNtxt, request.actLen); 
            fprintf( stderr,"%s server: %s Return from wsGetTraceAscii: %d\n", whoami, server, io);
            fprintf( stderr,"        actStarttime=%lf, actEndtime=%lf, actLen=%ld, samprate=%lf\n",
                  request.actStarttime, request.actEndtime, request.actLen, request.samprate);
        }
    }
    else {
        switch(io) {
        case WS_ERR_EMPTY_MENU:
        case WS_ERR_SCN_NOT_IN_MENU:
        case WS_ERR_BUFFER_OVERFLOW:
            if (io == WS_ERR_EMPTY_MENU ) 
                fprintf( stderr," %s server: %s No menu found.  We might as well quit.\n", whoami, server); 
            if (io == WS_ERR_SCN_NOT_IN_MENU ) 
                fprintf( stderr," %s server: %s Trace %s not in menu\n", whoami, server, SCNtxt);
            if (io == WS_ERR_BUFFER_OVERFLOW ) 
                fprintf( stderr," %s server: %s Trace %s overflowed buffer. Fatal.\n", whoami, server, SCNtxt); 
            success = 2;                /*   We might as well quit */
            return success;
        
        case WS_ERR_PARSE:
        case WS_ERR_TIMEOUT:
        case WS_ERR_BROKEN_CONNECTION:
        case WS_ERR_NO_CONNECTION:
            retry += 1;
            if (io == WS_ERR_PARSE )
                fprintf( stderr," %s server: %s Trace %s: Couldn't parse server's reply. Try again.\n", 
                        whoami, server, SCNtxt); 
            if (io == WS_ERR_TIMEOUT )
                fprintf( stderr," %s server: %s Trace %s: Timeout to wave server. Try again.\n", whoami, server, SCNtxt); 
            if (io == WS_ERR_BROKEN_CONNECTION ) {
            if(WSDebug) fprintf( stderr," %s server: %s Trace %s: Broken connection to wave server. Try again.\n", 
                    whoami, server, SCNtxt); 
            }
            if (io == WS_ERR_NO_CONNECTION ) {
                if(WSDebug) fprintf( stderr," %s server: %s Trace %s: No connection to wave server.\n", whoami, server, SCNtxt); 
                if(WSDebug) fprintf( stderr," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
            }
            ret = wsAttachServer( menu, But->wsTimeout );
            if(ret == WS_ERR_NONE && retry < But->RetryCount) goto gettrace;
            if(WSDebug) {   
                switch(ret) {
                case WS_ERR_NO_CONNECTION:
                    fprintf( stderr," %s server: %s: No connection to wave server.\n", whoami, server); 
                    break;
                case WS_ERR_SOCKET:
                    fprintf( stderr," %s server: %s: Socket error.\n", whoami, server); 
                    break;
                case WS_ERR_INPUT:
                    fprintf( stderr," %s server: %s: Menu missing.\n", whoami, server); 
                    break;
                default:
                    fprintf( stderr," %s server: %s: wsAttachServer error %d.\n", whoami, server, ret); 
                }
            }
            return 2;                /*   We might as well quit */
        
        case WS_WRN_FLAGGED:
            if((int)strlen(&request.retFlag)>0) {
                if(WSDebug) fprintf( stderr,"%s server: %s Trace %s: return flag from wsGetTraceAscii: <%c>\n %.80s\n", 
                        whoami, server, SCNtxt, request.retFlag, But->TraceBuf);
                if(request.retFlag == 'L') return 3;                /*   We might as well quit */
                if(request.retFlag == 'R') return 3;                /*   We might as well quit */
                if(request.retFlag == 'G') return 3;                /*   We might as well quit */
            }
            fprintf( stderr," %s server: %s Trace %s: No trace available. Wave server returned %c\n", 
                whoami, server, SCNtxt, request.retFlag); 
            return 2;                /*   We might as well quit */
        
        default:
            fprintf( stderr,"%s server: %s Failed.  io = %d\n", whoami, server, io );
        }
    }

    if((int)strlen(&request.retFlag)>0) {
        if(WSDebug) fprintf( stderr,"%s server: %s Trace %s: return flag from wsGetTraceAscii: <%c>\n %.80s\n", 
                whoami, server, SCNtxt, request.retFlag, But->TraceBuf);
        if(request.retFlag == 'L') return 3;                /*   We might as well quit */
        if(request.retFlag == 'R') return 3;                /*   We might as well quit */
        if(request.retFlag == 'G') return 3;                /*   We might as well quit */
    }

    nbytes = request.actLen;
    But->samp_sec = request.samprate<=0? 100:request.samprate;
    samprate = request.samprate;
    
    nsamp = Duration*samprate;
    if(nsamp > MAXTRACELTH) nsamp = MAXTRACELTH;
    strtok(But->TraceBuf, " ,");
    j = 0;
    token = strtok(0L, " ,");
    
    But->Mean = 0.0;
    But->Npts_wsv = 0;
    nsamp = 0;
    while(token!=0L && j<MAXTRACELTH) {
        But->Npts_wsv += 1;
        if(strcmp(token, "919191")!=0) {
            sscanf( token, "%f", &sample);
            Data[j] = sample;
            if(j==0) But->Min = But->Max = Data[j];
            if(Data[j] > But->Max) But->Max = Data[j];
            if(Data[j] < But->Min) But->Min = Data[j];
            But->Mean += Data[j];
            j += 1;
            nsamp += 1;
        }
        token = strtok(0L, " ,");
    }
    
    if(nsamp == 0)
    	fprintf( stderr,"%s trace %s is zero length! %lf  %f %f\n",
                            whoami, SCNtxt, Duration, temp, request.reqEndtime - request.reqStarttime);
        
    But->Mean = But->Mean/nsamp;
    But->Npts = j;
    temp = j/samprate + 1.0;
    if(temp < Duration) {
        if (WSDebug) 
        	fprintf( stderr,"%s trace %s incomplete: %lf  %f %f\n",
                            whoami, SCNtxt, Duration, temp, request.reqEndtime - request.reqStarttime);
        sleep_ew(200);
        retry += 1;
        if(retry < But->RetryCount) goto gettrace;
    }   
    if (io == WS_ERR_NONE ) success = 1;
/* 	*/   if (But->Debug) {
	    double max, min;
	    max = min = Data[0];
	    for(j=1;j<nsamp;j++) {
	    if(max<Data[j]) max = Data[j];
	    if(min>Data[j]) min = Data[j];
	    }
	    fprintf( stderr, "%s trace %s: %f  %f  %f \n", whoami, SCNtxt, min, But->Mean, max);
    }

/*    if (WSDebug) {
	    fprintf( stderr,"%s trace %s has %d samples.\n", whoami, SCNtxt, j);
    	for(j=0;j<20;j++) fprintf( stderr," %f ", Data[j]);
    	fprintf( stderr," \n");
    	for(j=0;j<20;j++) fprintf( stderr," %f ", Data[But->Npts - 21 + j]);
    	fprintf( stderr," \n");
    }	*/

    return success;
}

/**********************************************************************
 * Decode_Time : Decode time from seconds since 1970                  *
 *                                                                    *
 **********************************************************************/
void Decode_Time( double secs, TStrct *Time)
{
    struct Greg  g;
    long    minute;
    double  sex;

    Time->Time = secs;
    secs += sec1970;
    Time->Time1600 = secs;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    Time->Year  = g.year;
    Time->Month = g.month;
    Time->Day   = g.day;
    Time->Hour  = g.hour;
    Time->Min   = g.minute;
    Time->Sec   = sex;
}


/**********************************************************************
 * Encode_Time : Encode time to seconds since 1970                    *
 *                                                                    *
 **********************************************************************/
void Encode_Time( double *secs, TStrct *Time)
{
    struct Greg    g;

    g.year   = Time->Year;
    g.month  = Time->Month;
    g.day    = Time->Day;
    g.hour   = Time->Hour;
    g.minute = Time->Min;
    *secs    = 60.0 * (double) julmin(&g) + Time->Sec - sec1970;
}


/**********************************************************************
 * date22 : Calculate 22 char date in the form Jan23,1988 12:34 12.21 *
 *          from the julian seconds.  Remember to leave space for the *
 *          string termination (NUL).                                 *
 **********************************************************************/
void date22( double secs, char *c22)
{
    char *cmo[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    struct Greg  g;
    long    minute;
    double  sex;

    secs += sec1970;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    sprintf(c22, "%3s%2d,%4d %.2d:%.2d:%05.2f",
            cmo[g.month-1], g.day, g.year, g.hour, g.minute, sex);
}


/****************************************************************************
 *  Get_Req_Chans(Global *But);                                             *
 *  Retrieve a list of channels which should be processed.                  *
 *  This list may be manually created or generated by menulist.             *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Req_Chans(Global *But)
{
    char    whoami[50], *com, *str, ns, ew;
    int     i, j, k, nfiles, success, type, sensor, units;
    double  dlat, mlat, dlon, mlon, gain, sens, ssens, sitecorr;

    sprintf(whoami, "%s: %s: ", But->mod, "Get_Sta_Info");
    
    But->RSCNL = 0;
	        /* Open the main station file
	         ****************************/
    nfiles = k_open( But->MenuList );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, But->MenuList );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

            /* Process anything else as a channel descriptor
             ***********************************************/

            if( But->RSCNL >= MAXCHANNELS ) {
                fprintf(stderr, "%s Too many channel entries in <%s>", 
                         whoami, But->MenuList );
                fprintf(stderr, "; max=%d; exiting!\n", (int) MAXCHANNELS );
                exit( -1 );
            }
            j = But->RSCNL;
            
			    /* S C N */
            strncpy( But->ReqChan[j].Site, com,  6);
            str = k_str();
			if(str) strncpy( But->ReqChan[j].Net,  str,  2);
            str = k_str();
		    if(str) strncpy( But->ReqChan[j].Comp, str, 3);
            str = k_str();
		    if(str) strncpy( But->ReqChan[j].Loc, str, 2);
		    for(i=0;i<6;i++) if(But->ReqChan[j].Site[i]==' ') But->ReqChan[j].Site[i] = 0;
		    for(i=0;i<2;i++) if(But->ReqChan[j].Net[i]==' ')  But->ReqChan[j].Net[i]  = 0;
		    for(i=0;i<3;i++) if(But->ReqChan[j].Comp[i]==' ') But->ReqChan[j].Comp[i] = 0;
		    for(i=0;i<2;i++) if(But->ReqChan[j].Loc[i]==' ')  But->ReqChan[j].Loc[i]  = 0;
		    But->ReqChan[j].Comp[3] = But->ReqChan[j].Net[2] = But->ReqChan[j].Site[5] = But->ReqChan[j].Loc[2] = 0;

			if (k_err()) {
			    fprintf( stderr, "%s Error decoding line in station file\n%s\n  exiting!\n", whoami, k_get() );
			    exit( -1 );
			}
			    
        	But->RSCNL++;
        }
        nfiles = k_close();
    }
}


/****************************************************************************
 *  Get_Sta_Info(Global *But);                                              *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Sta_Info(Global *But)
{
    char    whoami[50], *com, *str, ns, ew;
    int     i, j, k, nfiles, success, type, sensor, units;
    double  dlat, mlat, dlon, mlon, gain, sens, ssens, sitecorr;

    sprintf(whoami, "%s: %s: ", But->mod, "Get_Sta_Info");
    
    ns = 'N';
    ew = 'W';
    But->NSCN = 0;
    for(k=0;k<But->nStaDB;k++) {
	        /* Open the main station file
	         ****************************/
	    nfiles = k_open( But->stationList[k] );
	    if(nfiles == 0) {
	        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, But->stationList[k] );
	        exit( -1 );
	    }

	        /* Process all command files
	         ***************************/
	    while(nfiles > 0) {  /* While there are command files open */
	        while(k_rd())  {      /* Read next line from active file  */
	            com = k_str();         /* Get the first token from line */

	                /* Ignore blank lines & comments
	                 *******************************/
	            if( !com )           continue;
	            if( com[0] == '#' )  continue;

	                /* Open a nested configuration file
	                 **********************************/
	            if( com[0] == '@' ) {
	                success = nfiles+1;
	                nfiles  = k_open(&com[1]);
	                if ( nfiles != success ) {
	                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
	                    exit( -1 );
	                }
	                continue;
	            }

	            /* Process anything else as a channel descriptor
	             ***********************************************/

	            if( But->NSCN >= MAXCHANNELS ) {
	                fprintf(stderr, "%s Too many channel entries in <%s>", 
	                         whoami, But->stationList[k] );
	                fprintf(stderr, "; max=%d; exiting!\n", (int) MAXCHANNELS );
	                exit( -1 );
	            }
	            j = But->NSCN;
	            
				    /* S C N */
	            strncpy( But->Chan[j].Site, com,  6);
	            str = k_str();
				if(str) strncpy( But->Chan[j].Net,  str,  2);
	            str = k_str();
			    if(str) strncpy( But->Chan[j].Comp, str, 3);
			    for(i=0;i<6;i++) if(But->Chan[j].Site[i]==' ') But->Chan[j].Site[i] = 0;
			    for(i=0;i<2;i++) if(But->Chan[j].Net[i]==' ')  But->Chan[j].Net[i]  = 0;
			    for(i=0;i<3;i++) if(But->Chan[j].Comp[i]==' ') But->Chan[j].Comp[i] = 0;
			    But->Chan[j].Comp[3] = But->Chan[j].Net[2] = But->Chan[j].Site[5] = 0;
                if(But->stationListType[k] == 1) {
	                str = k_str();
	                if(str) strncpy( But->Chan[j].Loc, str, 2);
	                for(i=0;i<2;i++) if(But->Chan[j].Loc[i]==' ')  But->Chan[j].Loc[i]  = 0;
	                But->Chan[j].Loc[2] = 0;
                }


                    /* Lat Lon Elev */
                if(But->stationListType[k] == 0) {
	                dlat = k_int();
	                mlat = k_val();
	                
	                dlon = k_int();
	                mlon = k_val();
	                
	                But->Chan[j].Elev = k_val();
	                
	                    /* convert to decimal degrees */
	                if ( dlat < 0 ) dlat = -dlat;
	                if ( dlon < 0 ) dlon = -dlon;
	                But->Chan[j].Lat = dlat + (mlat/60.0);
	                But->Chan[j].Lon = dlon + (mlon/60.0);
	                    /* make south-latitudes and west-longitudes negative */
	                if ( ns=='s' || ns=='S' )               But->Chan[j].Lat = -But->Chan[j].Lat;
	                if ( ew=='w' || ew=='W' || ew==' ' )    But->Chan[j].Lon = -But->Chan[j].Lon;
                } else if(But->stationListType[k] == 1) {
	                But->Chan[j].Lat  = k_val();
	                But->Chan[j].Lon  = k_val();
	                But->Chan[j].Elev = k_val();
                }

         /*       str = k_str();      Blow past the subnet */
                
                But->Chan[j].Inst_type = k_int();
                But->Chan[j].Inst_gain = k_val();
                But->Chan[j].GainFudge = k_val();
                But->Chan[j].Sens_type = k_int();
                But->Chan[j].Sens_unit = k_int();
                But->Chan[j].Sens_gain = k_val();
                But->Chan[j].SiteCorr  = k_val();
                        
                if(But->Chan[j].Sens_unit == 3) But->Chan[j].Sens_gain /= 978.0;
               
                But->Chan[j].sensitivity = (1000000.0*But->Chan[j].Sens_gain/But->Chan[j].Inst_gain)*But->Chan[j].GainFudge*But->Chan[j].SiteCorr;    /*    sensitivity counts/units        */
                
                if(But->stationListType[k] == 1) But->Chan[j].ShkQual = k_int();
	            
				if (k_err()) {
				    fprintf( stderr, "%s Error decoding line in station file\n%s\n  exiting!\n", whoami, k_get() );
				    exit( -1 );
				}
	     /*>Comment<*/
	            str = k_str();
	            if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].SiteName, str );
				    
	            str = k_str();
	            if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].Descript, str );
                
	        	But->NSCN++;
	        }
	        nfiles = k_close();
	    }
    }
}


/*************************************************************************
 *  Put_Sta_Info(Global *But);                                           *
 *  Retrieve all the information available about station i               *
 *  and put it into an internal structure for reference.                 *
 *  This should eventually be a call to the database; for now we must    *
 *  supply an ascii file with all the info.                              *
 *************************************************************************/
int Put_Sta_Info(Global *But)
{
    char    whoami[50];
    short   j;

    sprintf(whoami, " %s: %s: ", But->mod, "Put_Sta_Info");
    
    for(j=0;j<But->NSCN;j++) {
        if(strcmp(But->Chan[j].Site, But->Site )==0 && 
           strcmp(But->Chan[j].Comp, But->Comp)==0 && 
           strcmp(But->Chan[j].Loc,  But->Loc)==0 && 
           strcmp(But->Chan[j].Net,  But->Net )==0) {
            strcpy(But->Comment, But->Chan[j].SiteName);
            But->Inst_type  = But->Chan[j].Inst_type;
            But->Sens_type  = But->Chan[j].Sens_type;
            But->Sens_gain  = But->Chan[j].Sens_gain;
            But->Sens_unit  = But->Chan[j].Sens_unit;
            But->sensitivity  = But->Chan[j].sensitivity;
		    if(But->Sens_unit<=1) But->Scaler = But->Scale*1000.0;
		    if(But->Sens_unit==2) But->Scaler = But->Scale*10000.0;
		    if(But->Sens_unit==3) But->Scaler = But->Scale*10.0;
	        if(But->Debug) {
	            fprintf( stderr, "\n%s %s. %d %d %d %f %f %f %s\n\n", 
	                whoami, But->SCNtxt, But->Inst_type, But->Sens_type, 
	                But->Sens_unit, But->Sens_gain, But->sensitivity, 
	                But->Scale, But->Comment);
	        }   
            return 1;
        }
    }
    strcpy(But->Comment, "");
    But->Inst_type  = 0;
    But->Sens_type  = 0;
    But->Sens_gain  = 1.0;
    But->Sens_unit  = 0;
    But->sensitivity  = 1000000.0;
    But->Scaler = But->Scale*1000.0;
        if(But->Debug) {
            fprintf( stderr, "\n%s %s. %d %d %d %f %f %f %s\n\n", 
                whoami, But->SCNtxt, But->Inst_type, But->Sens_type, 
                But->Sens_unit, But->Sens_gain, But->sensitivity, 
                But->Scale, But->Comment);
        }   
    return 0;
}


/****************************************************************************
 *      config_me() process command file using kom.c functions              *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void config_me( char* configfile, Global *But )
{
    char    whoami[50], *com, *str;
    char    init[20];       /* init flags, one byte for each required command */
    int     ncommand;       /* # of required commands you expect              */
    int     nmiss;          /* number of required commands that were missed   */
    int     i, j, n, nfiles, success, dclabels, secsPerGulp;
    double  val;
    FILE    *in;
    gdImagePtr    im_in;

    sprintf(whoami, " %s: %s: ", But->mod, "config_me");
        /* Set one init flag to zero for each required command
         *****************************************************/
    ncommand = 2;
    for(i=0; i<ncommand; i++ )  init[i] = 0;
    But->Debug = But->WSDebug = 0;
    But->nServer = But->nStaDB = 0;
    secsPerGulp = 0;
    But->RetryCount = 2;
    But->wsTimeout = 50000;
    But->wsTimeout = 10000;
    But->LoBound = 5;
    But->HiBound = 10000;
    But->StdBound =  1.0;
    But->RatBound = 50.0;
    But->TrigTime.Year = 0;

        /* Open the main configuration file
         **********************************/
    nfiles = k_open( configfile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, configfile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

                /* Process anything else as a command
                 ************************************/
            
         /* wave server addresses and port numbers to get trace snippets from
          *******************************************************************/
/*0*/          
            else if( k_its("WaveServer") ) {
                if ( But->nServer >= MAX_WAVESERVERS ) {
                    fprintf( stderr, "%s Too many <WaveServer> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_WAVESERVERS );
                    return;
                }
                if( (long)(str=k_str()) != 0 )  strcpy(But->wsIp[But->nServer],str);
                if( (long)(str=k_str()) != 0 )  strcpy(But->wsPort[But->nServer],str);
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy(But->wsComment[But->nServer],str);
                But->nServer++;
                init[0]=1;
            }

          /* get the target directory(s)
        ************************************/
/*1*/
            else if( k_its("Target") ) {
                if( (long)(str=k_str()) != 0 )  {
                    n = strlen(str);   /* Make sure directory name has proper ending! */
                    if( str[n-1] != '/' ) strcat(str, "/");
                    strcpy(But->target, str);
                }
                init[1] = 1;
            }


               /* optional commands */

            else if( k_its("TrigTime") ) {
                But->TrigTime.Year = k_int();            /* # of minutes/line to display */
                But->TrigTime.Month = k_int();            /* # of minutes/line to display */
                But->TrigTime.Day = k_int();            /* # of minutes/line to display */
                But->TrigTime.Hour = k_int();            /* # of minutes/line to display */
                But->TrigTime.Min = k_int();            /* # of minutes/line to display */
            }

                /* get station list path/name
                *****************************/
/*9*/
            else if( k_its("StationList") ) {
                if ( But->nStaDB >= MAX_STADBS ) {
                    fprintf( stderr, "%s Too many <StationList> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_STADBS );
                    return;
                }
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->stationList[But->nStaDB] , str );
                But->stationListType[But->nStaDB] = 0;
                But->nStaDB++;
            }
            else if( k_its("StationList1") ) {
                if ( But->nStaDB >= MAX_STADBS ) {
                    fprintf( stderr, "%s Too many <StationList> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_STADBS );
                    return;
                }
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->stationList[But->nStaDB] , str );
                But->stationListType[But->nStaDB] = 1;
                But->nStaDB++;
            }


                /* get menu list path/name
                *****************************/
/*9*/
            else if( k_its("MenuList") ) {
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Menu list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->MenuList , str );
        /*        init[9] = 1;   */
            }


            else if( k_its("Debug") )          But->Debug = 1;   /* optional commands */

            else if( k_its("WSDebug") )        But->WSDebug = 1;   /* optional commands */
        
            else if( k_its("RetryCount") ) {  /*optional command*/
                But->RetryCount = k_int();
                if(But->RetryCount > 20) But->RetryCount = 20; 
                if(But->RetryCount <  2) But->RetryCount =  2; 
            }

            else if( k_its("Gulp") ) {  /*optional command*/
                secsPerGulp = k_int();
                if(secsPerGulp>1000) secsPerGulp = 1000; 
                if(secsPerGulp<  1)  secsPerGulp =   0; 
            }

            else if( k_its("LoBound") ) {  /*optional command*/
                But->LoBound = k_int();
            }

            else if( k_its("HiBound") ) {  /*optional command*/
                But->HiBound = k_int();
            }

            else if( k_its("RatBound") ) {  /*optional command*/
                But->RatBound = k_val();
            }

            else if( k_its("StdBound") ) {  /*optional command*/
                But->StdBound = k_val();
            }

                /* At this point we give up. Unknown thing.
                *******************************************/
            else {
                fprintf(stderr, "%s <%s> Unknown command in <%s>.\n",
                         whoami, com, configfile );
                continue;
            }

                /* See if there were any errors processing the command
                 *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "%s Bad <%s> command  in <%s>; exiting!\n",
                             whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

        /* After all files are closed, check init flags for missed commands
         ******************************************************************/
    nmiss = 0;
    for(i=0;i<ncommand;i++)  if( !init[i] ) nmiss++;
    if ( nmiss ) {
        fprintf( stderr, "%s ERROR, no ", whoami);
        if ( !init[0] )  fprintf( stderr, "<WaveServer> "   );
        if ( !init[1] )  fprintf( stderr, "<Target> "       );
        fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
        exit( -1 );
    }
    But->secsPerGulp = But->secsPerStep = 60*(60/(60/MAXMINUTES));
    j = (secsPerGulp <= 0)? MAXMINUTES*60 : secsPerGulp;
    j = (MAXMINUTES*60 < j)? MAXMINUTES*60 : j;
    But->secsPerGulp = But->secsPerStep = j;
}

