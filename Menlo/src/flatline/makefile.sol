CFLAGS = -D_REENTRANT $(GLOBALFLAGS)

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib
S = $(EW_HOME)/$(EW_VERSION)/src/libsrc/solaris
G = $(EW_HOME)/$(EW_VERSION)/lib
U = $(EW_HOME)/$(EW_VERSION)/src/libsrc/util

BINARIES = flatline.o  \
		$L/swap.o $L/logit.o $L/getutil.o  $L/transport.o $L/kom.o \
		$L/sleep_ew.o $L/time_ew.o $L/threads_ew.o $L/sema_ew.o\
		$L/mem_circ_queue.o $L/copyfile.o \
		$L/getavail.o $L/getsysname_ew.o $L/chron3.o\
		$L/parse_trig.o $L/pipe.o $L/remote_copy.o\
		$L/socket_ew.o $L/socket_ew_common.o $L/ws_clientII.o\
		$G/gd.o $G/gdfontt.o $G/gdfonts.o $G/gdfontmb.o \
		$G/gdfontl.o $G/gdfontg.o 

flatline: $(BINARIES)
	cc -o $(B)/flatline $(BINARIES) -lsocket -lnsl -lm -mt -lposix4 -lthread -lc

.c.o:
	$(CC) $(CFLAGS) -g $(CPPFLAGS) -c  $(OUTPUT_OPTION) $<
