########################################################################
# This is the flatline parameter file. 
# This module looks for flatline channels or large-excursion channels.
#
#                                             7/28/2003    Jim Luetgert
########################################################################

# List of wave servers (ip port comment) to contact to retrieve trace data.
@/home/earthworm/run/params/all_waveservers.d
#  WaveServer     130.118.43.4    16021     " wsv2 nano3"
#  WaveServer     130.118.43.4    16022     " wsv2 reftek"
#  WaveServer     130.118.43.4    16023     " wsv2 unr"
#  WaveServer     130.118.43.4    16024     " wsv2 nano2"
#  WaveServer     130.118.43.4    16025     " wsv2 nano1"
#  WaveServer     130.118.43.4    16026     " wsv2 dst"
#  WaveServer     130.118.43.4    16027     " wsv2 ucb"
#  WaveServer     130.118.43.4    16028     " wsv2 cit"
#  WaveServer     130.118.43.4    16029     " wsv2 k2nc1"
#  WaveServer     130.118.43.4    16030     " wsv2 k2np"
#  WaveServer     130.118.43.4    16031     " wsv2 ad1"
#  WaveServer     130.118.43.4    16032     " wsv2 ad2"
#  WaveServer     130.118.43.4    16033     " wsv2 ad3"
#  WaveServer     130.118.43.4    16034     " wsv2 ad4"
#  WaveServer     130.118.43.4    16035     " wsv2 ad5"
#  WaveServer     130.118.43.4    16036     " wsv2 ad6"
#  WaveServer     130.118.43.4    16039     " wsv2 k2nc2"

# Directory in which to store the final flatline.txt files.
 Target   /home/picker/gifs/nssetup/  

# The file with all the station information.
 StationList1     /home/earthworm/run/params/calsta.db1

# The file with all the waveserver stations.
 MenuList     /home/earthworm/run/params/calsta.db1

LoBound      15
LoBound      25
HiBound   10000
StdBound    1.0
RatBound  100.0

  TrigTime 2003 11 14 01 00

    # *** Optional Commands ***

 RetryCount    2  # Number of attempts to get a trace from server; default=2

# We accept a command "Debug" which turns on a bunch of log messages
# Debug
# WSDebug
