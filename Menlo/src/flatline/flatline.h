/* Include file for flatline */
/*****************************************************************************
 *  defines                                                                  *
 *****************************************************************************/

#define MAXCHANNELS     2000  /* Maximum number of channels                  */
#define MAXSAMPRATE      500  /* Maximum # of samples/sec.                   */
#define MAXMINUTES        15  /* Maximum # of minutes of trace.              */
#define MAXTRACELTH MAXMINUTES*60*MAXSAMPRATE /* Max. data trace length      */
#define MAXTRACEBUF MAXTRACELTH*10  /* This should work for 24-bit data      */

#define MAX_STADBS        20  /* Maximum number of Station database files    */
#define MAX_WAVESERVERS   60  /* Maximum number of Waveservers               */
#define MAX_ADRLEN        20  /* Size of waveserver address arrays           */

#define GDIRSZ           132  /* Size of string for GIF target directory     */
#define STALIST_SIZ      100  /* Size of string for station list file        */

/*****************************************************************************
 *  Define the structure for time records.                                   *
 *****************************************************************************/

typedef struct TStrct {   
    double  Time1600; /* Time (Sec since 1600/01/01 00:00:00.00)             */
    double  Time;     /* Time (Sec since 1970/01/01 00:00:00.00)             */
    int     Year;     /* Year                                                */
    int     Month;    /* Month                                               */
    int     Day;      /* Day                                                 */
    int     Hour;     /* Hour                                                */
    int     Min;      /* Minute                                              */
    double  Sec;      /* Second                                              */
} TStrct;

/*****************************************************************************
 *  Define the structure for Channel name.                                   *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/

typedef struct ChanName {      /* A channel information structure            */
    char    Site[6];           /* Site                                       */
    char    Comp[5];           /* Component                                  */
    char    Net[5];            /* Net                                        */
    char    Loc[5];            /* Loc                                        */
    char    SCN[17];           /* SCN                                        */
    char    SCNtxt[20];        /* S C N                                      */
    char    SCNnam[20];        /* S_C_N                                      */
    char    SiteName[50];      /* Common Name of Site                        */
} ChanName;

/*****************************************************************************
 *  Define the structure for Channel information.                            *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/

typedef struct ChanInfo {      /* A channel information structure            */
    char    Site[6];           /* Site                                       */
    char    Comp[5];           /* Component                                  */
    char    Net[5];            /* Net                                        */
    char    Loc[5];            /* Loc                                        */
    char    SCN[17];           /* SCN                                        */
    char    SCNtxt[20];        /* S C N                                      */
    char    SCNnam[20];        /* S_C_N                                      */
    char    SiteName[50];      /* Common Name of Site                        */
    char    Descript[200];     /* Common Name of Site                        */
    double  Lat;               /* Latitude                                   */
    double  Lon;               /* Longitude                                  */
    double  Elev;              /* Elevation                                  */
    
    int     Inst_type;         /* Type of instrument                         */
    double  Inst_gain;         /* Gain of instrument (microv/count)          */
    int     Sens_type;         /* Type of sensor                             */
    double  Sens_gain;         /* Gain of sensor (volts/unit)                */
    int     Sens_unit;         /* Sensor units d=1; v=2; a=3                 */
    double  GainFudge;         /* Additional gain factor.                    */
    double  SiteCorr;          /* Site correction factor.                    */
    double  sensitivity;       /* Channel sensitivity  counts/units          */
    int		ShkQual;           /* Station (Chan) type                        */
} ChanInfo;

/*****************************************************************************
 *  Define the structure for the individual Global thread.                   *
 *  This is the private area the thread needs to keep track                  *
 *  of all those variables unique to itself.                                 *
 *****************************************************************************/

struct Global {
    int     Debug;             /*                                            */
    int     WSDebug;           /*                                            */
    int     Clip;              /* number of divisions to clip trace;         *
				                * 0 for no clipping                          */
    char    TraceBuf[MAXTRACEBUF]; /* This should work for 24-bit digitizers */
    char    mod[70];
    
    char    target[100];       /* Target directory /directory/               */
    char    GifDir[GDIRSZ];    /* Directory for storage of .gif on local machine */
    int     nStaDB;            /* number of station databases we know about  */
    char    stationList[MAX_STADBS][STALIST_SIZ];
    int     stationListType[MAX_STADBS];
    char    MenuList[STALIST_SIZ];
    int     RSCNL;              /* Number of requested SCNs                  */
    ChanName    ReqChan[MAXCHANNELS];
    WS_MENU_QUEUE_REC menu_queue[MAX_WAVESERVERS];
    
    int     NSCN;              /* Number of SCNs we know about               */
    ChanInfo    Chan[MAXCHANNELS];
    
    int     current;           /* Index of current SCN                       */
    
    double  FirstTime;
    int     LoBound;           /* Lower bound for flagging                   */
    int     HiBound;           /* Upper bound for flagging                   */
    double  RatBound;
    double  StdBound;
    
/* Globals to set from configuration file
 ****************************************/
    long    wsTimeout;         /* seconds to wait for reply from ws          */
    int     nServer;           /* number of wave servers we know about       */
    long    RetryCount;        /* Retry count for waveserver errors.         */
                        /* list of available waveServers, from config. file  */
    int     inmenu[MAX_WAVESERVERS];
    char    wsIp[MAX_WAVESERVERS][MAX_ADRLEN];
    char    wsPort[MAX_WAVESERVERS][MAX_ADRLEN];
    char    wsComment[MAX_WAVESERVERS][50];

/* Variables used during the generation of each plot
 ***************************************************/
    long    samp_sec;          /* samples/sec                                */
    long    Npts;              /* Number of points in trace                  */
    long    Npts_wsv;          /* Number of points returned by waveserver    */
    double  Mean;              /* Mean value of the last data gulp           */
    double  Min;               /* Min value of the last data gulp            */
    double  Max;               /* Max value of the last data gulp            */

    int     nentries;          /* Number of menu entries for this SCN        */
    double  TStime[MAX_WAVESERVERS*2]; /* Tank start for this entry          */
    double  TEtime[MAX_WAVESERVERS*2]; /* Tank end for this entry            */
    int     index[MAX_WAVESERVERS*2];  /* WaveServer for this entry          */

    int     secsPerStep;       /* # of seconds per processing step        */
    int     secsPerGulp;       /* # of seconds per acquisition            */
    
    double  Scale;             /* Scale factor [Data] (in)                */
    double  Scaler;            /* Scale factor [Data] (in)                */
    char    SCN[15];           /* SCN                                     */
    char    SCNtxt[17];        /* S C N                                   */
    char    SCNnam[17];        /* S_C_N                                   */

    int		Inst_type;         /* Type of instrument                      */
    double  Inst_gain;         /* Gain of instrument (microv/count)       */
    int		Sens_type;         /* Type of sensor                          */
    double  Sens_gain;         /* Gain of sensor (volts/unit)             */
    int		Sens_unit;         /* Sensor units d=1; v=2; a=3              */
    double  sensitivity;       /* Channel sensitivity  counts/units       */

    char    Site[5];           /*  0- 3 Site                              */
    char    Comp[5];           /* Component                               */
    char    Net[5];            /* Net                                     */
    char    Loc[5];            /* Location                                */
    char    Comment[50];       /* Description (for web page)              */
    
    TStrct  TrigTime;
};
typedef struct Global Global;

