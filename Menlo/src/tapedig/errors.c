
                  /*******************************************
                   *                errors.c                 *
                   *  Error handling functions for adsend    *
                   *******************************************/

#include <stdio.h>
#include <time.h>
#include <earthworm.h>

/* Function prototypes
   *******************/
void SetCurPos( int, int );


void LogAndReportError( int errNum, char errMsg[] )
{
   const int  maxchr = 70;
   int        i,
              len;              // Length of current message
   struct tm  res;              // time string stuff stolen from logit()
   time_t     now;

/* To the log file
   ***************/
   logit( "t", "%s", errMsg );

/* Limit the message to fit the console window
   *******************************************/
   len = strlen( errMsg );
   if ( len > maxchr )
   {
      errMsg[maxchr] = '\0';
      len = maxchr;
   }

/* Log the current message to console.
   Pad the screen message with blanks.
   ***********************************/
   time( &now );
   gmtime_ew( &now, &res );

   SetCurPos( 2, 23 );
   sprintf(stderr,"%4d%02d%02d_UTC_%02d:%02d:%02d  %s",(res.tm_year + 1900),
            (res.tm_mon + 1), res.tm_mday, res.tm_hour, res.tm_min, res.tm_sec, errMsg);

   for ( i = len; i < maxchr; i++ )
      putchar( ' ' );
   return;
}


              /****************************************************
               *                 DAQ_ErrorHandler()               *
               *                                                  *
               *           Function to handle DAQ errors          *
               ****************************************************/

void DAQ_ErrorHandler( char *fun_name, short status )
{
   char     errMsg[100];
   const    errNum = 1;                 // DAQ errors are number 1

   if ( status == 0 )                   // No error
      return;


/* Encode the error message
   ************************/
   if ( status < 0 )
      sprintf( errMsg, "%s did not execute because of an error: %hd\n",
             fun_name, status );

   if ( status > 0 )
      sprintf( errMsg, "%s executed but with a potentially serious side effect: %hd\n",
             fun_name, status );

/* Log and report the error message
   ********************************/
   LogAndReportError( errNum, errMsg );
   return;
}

