/*
 *   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT USING THE COMMAND CHECKOUT.
 *
 *    $Id: threads.c 2 2004-04-28 23:15:44Z dietz $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.1  2004/04/28 23:15:46  dietz
 *     Initial revision
 *
 *     Revision 1.1  2000/02/14 16:00:43  lucky
 *     Initial revision
 *
 *
 */

   /******************************************************************
    *              Dummy thread functions for adsend.                *
    *  adsend is single-threaded so it must link to these functions. *
    ******************************************************************/


int _beginthread()
{
   return 0;
}


void _endthread()
{
   return;
}
