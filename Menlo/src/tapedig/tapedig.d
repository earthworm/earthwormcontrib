#
#                    CONFIGURATION FILE FOR TAPEDIG
#                    ------------------------------
#
#  If there are no mux boards in the system, OnboardChans (16) channels are
#  digitized.  If there are mux boards in the system,
#  (4 * OnboardChans * NumMuxBoards) channels are digitized.
#  The station file must contain an entry for each channel digitized.
#
#
ModuleId         MOD_TAPEDIG    # Module id of this instance of tapedig
OutRing          WAVE_RING      # Transport ring to write waveforms to
HeartbeatInt     15             # Heartbeat interval in seconds
#
#
#                        CHANNEL CONFIGURATION
OnboardChans     16             # Number of channels on the DAQ card
NumMuxBoards     1              # Number of 64-channel mux's (0,1,2, or 4)
ChanRate         100.0          # Sampling rate in samples/second
ChanMsgSize      100            # TraceBuf message size, in samples
Gain             2              # Channel gains (-1: +/-10V)(1: +/-5V)(2: +/-2.5V)
SampRate         250000.0       # Sample rate/scan (max for PCI-MIO-16E-4 = 250000.)
#
#
#                           GUIDE CHANNELS
NumGuide         0              # Number of guide channels (usually one per mux)
GuideChan        0 4            # The channels carrying the guide signal
MinGuideSignal   110.0          # Guides are declared dead if the mean value of
                                #   guide 1st differences < MinGuideSignal
MaxGuideNoise    8.0            # Guides are declared dead if standard error of
                                #   guide 1st differences > MaxGuideNoise
#
#
#                         EXTERNAL TRIGGERING
# This program uses "High-Hysteresis Analog Triggering Mode".
# Triggering occurs when the trigger voltage becomes greater than HighValue.
# Detriggering occurs when the trigger voltage becomes less than LowValue.
# HighValue minus LowValue must be >= 0.1 volt.
# For more information on triggering, see the National Instruments "PCI-MIO
# E Series User Manual", Jan 1997 edition, chapter 3.
#
LowValue         2.75           # In volts (-10V to 10V) (must be < HighValue)
HighValue        3.25           # In volts (-10V to 10V) (must be > LowValue)
