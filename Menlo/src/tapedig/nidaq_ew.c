
     /*********************************************************************
      *                             nidaq_ew.c                            *
      *                                                                   *
      *  Calls to National Instruments Data Aquisition (NIDAQ) functions  *
      *  are isolated in this file.                                       *
      *********************************************************************/

#include <stdlib.h>
#include <windows.h>
#include <nidaq.h>
#include <nidaqcns.h>
#include "tapedig.h"

/* Global configuration parameters
   *******************************/
extern double ChanRate;              // Rate in samples per second per channel
extern double SampRate;              // Sample rate per scan (in samp/s)
extern int    ChanMsgSize;           // Message size in samples per channel
extern int    OnboardChans;          // The number of onboard channels
extern int    NumMuxBoards;          // Number of mux cards
extern int    Gain;                  // Gain of amp in front of A/D converter.
extern double LowValue;              // The low hysteresis voltage for external triggering
extern double HighValue;             // The high hysteresis voltage for external triggering
extern short  DAQ_device;            // The device number of the DAQ board
extern int    TimeoutNoTrig;         // Take action if no trigger for this many secs

/* Global to this file
   *******************/
static short   *Buffer;              // The DAQ buffer
static unsigned BufSize;             // The size of the DAQ buffer

/* Function prototypes
   *******************/
void DAQ_ErrorHandler( char *, short );       // In errors.c


      /**************************************************************
       *                          InitDAQ()                         *
       *                  Initialize the DAQ system.                *
       **************************************************************/

void InitDAQ( void )
{
   short status;                   // Return value from the DAQ functions
   short sampTimebase;             // Onboard source signal used for sample rate
   unsigned short sampInterval;    // Number of timebase units between consecutive samples

   int totalChans = (NumMuxBoards == 0) ? OnboardChans : (4 * OnboardChans * NumMuxBoards);

/* Allocate memory to contain digitized samples
   ********************************************/
   BufSize = (unsigned)(2 * ChanMsgSize * totalChans);

   Buffer = (short *) calloc( BufSize, sizeof(short) );
   if ( Buffer == NULL )
   {
      logit( "", "Cannot allocate DAQ buffer\n" );
      return;
   }

/* Initialize the hardware and software for the DAQ board.
   Device number code 206 is the PCI-MIO-16E-4 DAQ board.
   ******************************************************/
   {
      short deviceNumberCode;

      status = Init_DA_Brds( DAQ_device, &deviceNumberCode );
      DAQ_ErrorHandler( "Init_DA_Brds", status );
   }

/* This sets a timeout limit (#Sec * 18ticks/Sec) so that if there is
   something wrong, the program won't hang on the DAQ_DB_Transfer call.
   *******************************************************************/
   {
      const long lTimeout = 180;      // Equivalent to 10 seconds

      status = Timeout_Config( DAQ_device, lTimeout );
      DAQ_ErrorHandler( "Timeout_Config", status );
   }

/* AI_Configure() informs NIDAQ of the input mode (single-ended or
   differential) and input polarity selected for the DAQ_device.
   If inputMode = 2 (non-referenced, single-ended mode, NRSE),
   the input signal's ground reference is connected to AISENSE,
   which is tied to the negative input of the instrumentation
   amplifier.  We jumper the AISENSE pin to AIGND.
   inputRange and driveAIS are ignored for the AT-MIO-16F-5 and
   PCI-MIO-16E-4 DAQ cards.
   ***************************************************************/
   {
      const short chan      = -1;      // -1 means "set all channels identically"
      const short inputMode =  2;      // 2 means NRSE configuration
      const short inputRange = 0;      // Ignored. Set in SCAN_Setup().
      const short polarity  =  0;      // 0 means bipolar operation (+ or - voltage)
      const short driveAIS  =  0;      // Ignored

      status = AI_Configure( DAQ_device, chan, inputMode, inputRange,
                              polarity, driveAIS );
      DAQ_ErrorHandler( "AI_Configure", status );
   }

/* AI_Mux_Config() informs NI-DAQ of the presence
   of any AMUX-64T devices attached to the system.
   The system supports 0, 1, 2, or 4 MUX boards.
   **********************************************/
   status = AI_Mux_Config( DAQ_device, (short)NumMuxBoards );
   DAQ_ErrorHandler( "AI_Mux_Config", status );

/* Set up "High-Hysteresis Analog Triggering Mode" as follows:
   1. Compute lowValue and highValue (0-255)
   2. Set up analog triggering using Configure_HW_Analog_Trigger().
   3. Link scan timing to analog trigger using SelectSignal().
   4. Specify external scan timing using DAQ_Config().
   ***************************************************************/
   {
      int         lowValue;           // Detrigger threshhold (0=-10V, 255=10V)
      int         highValue;          // Trigger threshhold (0=-10V, 255=10V)
      const short StartTrig = 0,      // 0 = internal start trigger
                  ExtScan   = 2;      // 2 = external scan timing

      lowValue  = (int)((LowValue/10.  + 1.0) * 128.);     // Compute lowValue
      if ( lowValue  <   0 ) lowValue  = 0;
      if ( lowValue  > 254 ) lowValue  = 254;
      logit( "t", "lowValue:  %d\n", lowValue );

      highValue = (int)((HighValue/10. + 1.0) * 128.);     // Compute highValue
      if ( highValue <   1 ) highValue = 1;
      if ( highValue > 255 ) highValue = 255;
      logit( "t", "highValue: %d\n", highValue );

      status = Configure_HW_Analog_Trigger( DAQ_device, ND_ON, lowValue,
                  highValue, ND_HIGH_HYSTERESIS, ND_PFI_0 );
      DAQ_ErrorHandler( "Configure_HW_Analog_Trigger", status );

      status = Select_Signal( DAQ_device, ND_IN_SCAN_START, ND_PFI_0,
                  ND_LOW_TO_HIGH );
      DAQ_ErrorHandler( "SelectSignal", status );

      status = DAQ_Config( DAQ_device, StartTrig, ExtScan );
      DAQ_ErrorHandler( "DAQ_Config", status );
   }

/* Turn ON software double-buffered mode.
   *************************************/
   {
      const short DBmodeON = 1;

      status = DAQ_DB_Config( DAQ_device, DBmodeON );
      DAQ_ErrorHandler( "DAQ_DB_Config", status );
   }

/* Convert sample rate (samples/sec) to appropriate
   timebase and sample interval values.
   ************************************************/
   {
      const short  iUnits = 0;          // 0 = points per second

      status = DAQ_Rate( SampRate, iUnits, &sampTimebase, &sampInterval );
      DAQ_ErrorHandler( "DAQ_Rate", status );
   }

/* SCAN_Setup() initializes circuitry for a scanned data acquisition
   operation.  Initialization includes storing a table of the channel
   sequence and gain setting for each channel to be digitized.
   ******************************************************************/
   {
      int i;
      short *chans;            // The analog input channels to be sampled
      short *gains;            // The gains for input channels

      chans = malloc( OnboardChans * sizeof(short) );
      if ( chans == NULL )
      {
         logit( "et", "Error allocating the chans array. Exiting.\n" );
         exit( -1 );
      }
      gains = malloc( OnboardChans * sizeof(short) );
      if ( gains == NULL )
      {
         logit( "et", "Error allocating the gains array. Exiting.\n" );
         free( chans );
         exit( -1 );
      }

      for ( i = 0; i < OnboardChans; i++ )
      {
         chans[i] = (short)i;       // See NIDAQ user's manual for details
         gains[i] = (short)Gain;
      }

      status = SCAN_Setup( DAQ_device, (short)OnboardChans, chans, gains );
      DAQ_ErrorHandler( "SCAN_Setup", status );

      free( chans );
      free( gains );
   }

/* SCAN_Start() initiates a multiple channel scanned DAQ data
   acquisition operation, and stores its input in an array.
   **********************************************************/
   {
      const short scanTimebase = 0;           // Unused for external triggering
      const unsigned short scanInterval = 0;  // Unused for external triggering

      status = SCAN_Start( DAQ_device,
                           Buffer, BufSize,
                           sampTimebase, sampInterval,
                           scanTimebase, scanInterval );
      DAQ_ErrorHandler( "SCAN_Start", status );
   }
   return;
}


      /**************************************************************
       *                          GetDAQ()                          *
       *                                                            *
       *  Get a half buffer of data from the DAQ system.            *
       *                                                            *
       *  Returns -1 if timed out waiting for data.                 *
       *           0 if all is ok.                                  *
       **************************************************************/

int GetDAQ( short         *halfBuf,   // Buffer to contain the data obtained
            unsigned long *ptsTfr )   // Number of points transferred to halfBuf
{
   short   status;                    // Return value from the DAQ functions
   short   daqStopped;                // 1 if DAQ operations are stopped
   int     SleepLen;                  // Check data ready every SleepLen msec
   int     nLoop;                     // Time out if no data after this many loops
   int     i;                         // Loop counter

/* DAQ_DB_HalfReady() checks whether the next half buffer is
   ready to transfer.  SleepLen is 0.1 * the message length in msec.
   ****************************************************************/
   SleepLen = (int)(100. * ChanMsgSize / ChanRate + 0.5);

   nLoop = 1000 * TimeoutNoTrig / SleepLen;

   for ( i = 0; i < nLoop; i++ )
   {
      short halfReady;               // 1 if data is available, 0 otherwise

      status = DAQ_DB_HalfReady( DAQ_device, &halfReady, &daqStopped );
      DAQ_ErrorHandler( "DAQ_DB_HalfReady", status );

/* DAQ_DB_Transfer() transfers half of the data from the buffer being
   used for double-buffered data acquisition to another buffer, which
   is passed to the function.  This function waits until the data to
   be transferred is available before returning (blocking).  You can
   execute DAQ_DB_Transfer repeatedly to return sequential half
   buffers of the data.
   ******************************************************************/
      if ( halfReady )
      {
         status = DAQ_DB_Transfer( DAQ_device, halfBuf, ptsTfr, &daqStopped );
         DAQ_ErrorHandler( "DAQ_DB_Transfer", status );
         return 0;
      }
      sleep_ew( SleepLen );          // Sleep for 0.1 of the message length
   }

/* Timed out waiting for the buffer to fill
   ****************************************/
   return -1;
}


      /**************************************************************
       *                          StopDAQ()                         *
       *                     Stop the DAQ system.                   *
       **************************************************************/

void StopDAQ( void )
{
   const short DBmodeOFF = 0;

/* DAQ_Clear() cancels the current data acquisition operation
   **********************************************************/
   DAQ_Clear( DAQ_device );

/* Set DB mode back to initial state
   *********************************/
   DAQ_DB_Config( DAQ_device, DBmodeOFF );

/* Release the double buffer
   *************************/
   free( Buffer );
   return;
}


         /****************************************************
          *                    InitDIO()                     *
          *                                                  *
          *  DIO_device = Device number of the DIO-96 card.  *
          *  port = [0 - 11] for the DIO-96 card.            *
          ****************************************************/

void InitDIO( short DIO_device, short port )
{
   short     status;          // Return value from the DAQ functions
   const i16 mode = 0;
   const i16 dir  = 0;

   status = DIG_Prt_Config( DIO_device, port, mode, dir );
   DAQ_ErrorHandler( "DIG_Prt_Config", status );
   return;
}


       /**********************************************************
        *                        GetDIO()                        *
        *                                                        *
        *  DIO_device = Device number of the DIO-96 card         *
        *  dig_in     = Address of structure to be filled with   *
        *               DIO stuff.                               *
        **********************************************************/

void GetDIO( short DIO_device, DIG_IN *dig_in )
{
   short status;          // Return value from the DAQ functions
   i32   pattern;
   i16   port;

/* Get the time (day, hour, minute, second)
   ****************************************/
   status = DIG_In_Prt( DIO_device, port=0, &pattern );
   DAQ_ErrorHandler( "DIG_In_Prt", status );
   dig_in->sec  = 10 * ((pattern & 0x70) >> 4) + (pattern & 0xF);

   status = DIG_In_Prt( DIO_device, port=1, &pattern );
   DAQ_ErrorHandler( "DIG_In_Prt", status );
   dig_in->min  = 10 * ((pattern & 0x70) >> 4) + (pattern & 0xF);

   status = DIG_In_Prt( DIO_device, port=2, &pattern );
   DAQ_ErrorHandler( "DIG_In_Prt", status );
   dig_in->hour = 10 * ((pattern & 0x30) >> 4) + (pattern & 0xF);

   status = DIG_In_Prt( DIO_device, port=3, &pattern );
   DAQ_ErrorHandler( "DIG_In_Prt", status );
   dig_in->day  = 10 * ((pattern & 0xF0) >> 4) + (pattern & 0xF);

   status = DIG_In_Prt( DIO_device, port=4, &pattern );
   DAQ_ErrorHandler( "DIG_In_Prt", status );
   dig_in->day += 100 * (pattern & 0x3);

   status = DIG_In_Prt( DIO_device, port=9, &pattern );
   DAQ_ErrorHandler( "DIG_In_Prt", status );
   dig_in->hunSec  = 10 * ((pattern & 0xF0) >> 4) + (pattern & 0xF);

/* Get error light (1 = light on)
   ******************************/
   status = DIG_In_Prt( DIO_device, port=5, &pattern );
   DAQ_ErrorHandler( "DIG_In_Prt", status );
   dig_in->errorLight = pattern & 0x1;

/* Get footage
   ***********/
   status = DIG_In_Prt( DIO_device, port=6, &pattern );
   DAQ_ErrorHandler( "DIG_In_Prt", status );
   dig_in->footage = pattern & 0xFF;

   status = DIG_In_Prt( DIO_device, port=7, &pattern );
   DAQ_ErrorHandler( "DIG_In_Prt", status );
   dig_in->footage += (pattern & 0xFF) << 8;

/* Get tape id number
   ******************/
   status = DIG_In_Prt( DIO_device, port=8, &pattern );
   DAQ_ErrorHandler( "DIG_In_Prt", status );
   dig_in->tapeId = pattern & 0xFF;

   return;
}

