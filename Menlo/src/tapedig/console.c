
     /***************************************************************
      *                  Control the console display                *
      ***************************************************************/

#include <time.h>
#include <earthworm.h>

static HANDLE outHandle;        // The console file handle


void SetCurPos( int x, int y )
{
   COORD     coord;

   coord.X = x;
   coord.Y = y;
   SetConsoleCursorPosition( outHandle, coord );
   return;
}


void InitCon( void )
{
   WORD   color;
   COORD  coord;
   DWORD  numWritten;
   extern short DAQ_device;         // The device number of the DAQ board

/* Get the console handle
   **********************/
   outHandle = GetStdHandle( STD_OUTPUT_HANDLE );

/* Set foreground and background colors
   ************************************/
   color = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
   if ( DAQ_device == 1 )
      color |= BACKGROUND_BLUE;
   else
      color |= BACKGROUND_GREEN;

   coord.X = coord.Y = 0;

   FillConsoleOutputAttribute( outHandle, color, 2000, coord, &numWritten );
   SetConsoleTextAttribute( outHandle, color );

/* Fill in the labels
   ******************/
   SetCurPos( 32, 1 );
   printf( "TAPE DIGITIZER" );

   SetCurPos( 4, 4 );
   printf( "DAQ device:   %hd", DAQ_device );

   SetCurPos( 4, 6 );
   printf( "DIO time:" );

   SetCurPos( 4, 8 );
   printf( "Tape Footage:" );

   SetCurPos( 4, 10 );
   printf( "Tape ID:" );

   SetCurPos( 4, 12 );
   printf( "Error Light:" );

   SetCurPos( 4, 21 );
   printf( "Last Error/Warning:" );

   SetCurPos( 46, 4 );
   printf( "Guide channel   Signal  Noise" );

   SetCurPos( 46, 12 );
   printf( "DAQ Restarts" );
   SetCurPos( 53, 13 );
   printf( "None" );

   SetCurPos( 0, 0 );
   return;
}
