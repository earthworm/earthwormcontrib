
               /**********************************************
                *               Program tapedig              *
                **********************************************/

#include <earthworm.h>
#include <transport.h>
#include "tapedig.h"


int main( int argc, char *argv[] )
{
   void InitCon();                  // Function prototype
   void InitDIO( short, short );
   void GetDIO( short, DIG_IN * );
   void InitDAQ( void );

   SHM_INFO     Region;             // Transport ring structure
   extern long  RingKey;            // Key to transport ring
   int    i;
   DIG_IN dig_in;                   // Obtained from DIO card

   short     *halfBuf;              // Pointer to half buffer of A/D data
   unsigned  halfBufSize;           // Size of the half buffer in number of samples
   int       totalChans;            // Total number of mux channels

   extern short  DIO_device;
   extern int    n_DIO_ports;
   extern int    ChanMsgSize;       // Message size in samples per channel
   extern int    NumMuxBoards;      // Number of mux cards
   extern int    OnboardChans;      // The number of channels on the DAQ board

/* Get command line arguments
   **************************/
   if ( argc < 2 )
   {
      printf( "Usage: tapedig <config file>\n" );
      return -1;
   }

/* Read configuration parameters
   *****************************/
   if ( GetConfig( argv[1] ) < 0 )
   {
      printf( "tapedig: Error reading configuration file. Exiting.\n" );
      return -1;
   }

/* Initialize and open log file
   ****************************/
   logit_init( argv[1], 256, 1 );

/* Log the configuration file
   **************************/
   LogConfig();

/* Allocate some array space
   *************************/
   totalChans = (NumMuxBoards == 0) ? OnboardChans : (4 * OnboardChans * NumMuxBoards);
   halfBufSize = (unsigned)(ChanMsgSize * totalChans);
   logit( "t", "Half-size of DAQ buffer: %u samples\n", halfBufSize );

   halfBuf = (short *) calloc( halfBufSize, sizeof(short) );
   if ( halfBuf == NULL )
   {
      logit( "", "Cannot allocate the DAQ buffer\n" );
      return -1;
   }
   logit( "t", "Allocated the DAQ buffer\n" );

/* Attach to existing transport ring
   *********************************/
   tport_attach( &Region, RingKey );
   logit( "t", "Attached to transport ring: %d\n", RingKey );

/* Initialize the console display
   ******************************/
   InitCon();

/* Initialize the DIO ports
   ************************/
   for ( i = 0; i < n_DIO_ports; i++ )
      InitDIO( DIO_device, (short)i );

/* Initialize the National Instruments DAQ system
   **********************************************/
   InitDAQ();                      // In nidaq_ew.c

/* Loop until the terminate flag gets set
   **************************************/
   while ( tport_getflag( &Region ) != TERMINATE  )
   {

/* Get DIO stuff
   *************/
      GetDIO( DIO_device, &dig_in );

/* Printf DIO stuff to console window
   **********************************/
      SetCurPos( 18, 6 );
      printf( "%03hd",  dig_in.day );
      SetCurPos( 22, 6 );
      printf( "%02hd",  dig_in.hour );
      SetCurPos( 25, 6 );
      printf( "%02hd",  dig_in.min );
      SetCurPos( 28, 6 );
      printf( "%02hd.", dig_in.sec );
      SetCurPos( 31, 6 );
      printf( "%02hd",  dig_in.hunSec );
      SetCurPos( 18, 8 );
      printf( "%04x",   dig_in.footage );
      SetCurPos( 18, 10 );
      printf( "%02x",  dig_in.tapeId );
      SetCurPos( 18, 12 );
      if ( dig_in.errorLight == 1 )
         printf( "On " );
      else
         printf( "Off" );

      sleep_ew( 10 );
   }

/* Clean up and exit program
   *************************/
   StopDAQ();
   free( halfBuf );
   logit( "t", "Tapedig terminating.\n" );
   return 0;
}

