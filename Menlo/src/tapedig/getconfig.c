
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <kom.h>
#include <earthworm.h>
#include "tapedig.h"


/* Function declaration
   ********************/
int IsComment( char [] );    // Defined below

/* The configuration file parameters
   *********************************/
int      OnboardChans;       // The number of channels on the DAQ board
int      NumMuxBoards;       // Number of mux cards
double   ChanRate;           // Rate in samples per second per channel
int      ChanMsgSize;        // Message size in samples per channel
double   SampRate=250000.0;  // Sample rate per scan (in samp/s)
int      Gain;               // Gain of amp in front of A/D converter.
long     RingKey;            // Transport ring key
double   MinGuideSignal;     // Guides are declared dead if the mean value of
                             //   guide 1st differences < MinGuideSignal
double   MaxGuideNoise;      // Guides are declared dead if standard error of
                             //   guide 1st differences > MaxGuideNoise
int      GuideChan;          // The channel where we find the triangle wave
double   LowValue;           // The low hysteresis voltage for external triggering
double   HighValue;          // The high hysteresis voltage for external triggering
short    DAQ_device;         // The device number of the DAQ board
int      TimeoutNoTrig;      // Take action if no trigger for this many secs
short    DIO_device;         // Device number of the DIO card
int      n_DIO_ports;        // Number of ports on the DIO card


     /***************************************************************
      *                          GetConfig()                        *
      *         Processes command file using kom.c functions.       *
      *           Returns -1 if any errors are encountered.         *
      ***************************************************************/

#define NCOMMAND 15            // Number of commands in the config file

int GetConfig( char *configfile )
{
   const int ncommand = NCOMMAND;

   char     init[NCOMMAND];     /* Flags, one for each command */
   int      nmiss;              /* Number of commands that were missed */
   int      nfiles;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      printf( "tapedig: Error opening configuration file <%s>\n", configfile );
      return -1;
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          /* Get the first token from line */

         if ( !com ) continue;             /* Ignore blank lines */
         if ( com[0] == '#' ) continue;    /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "tapedig: Error opening command file <%s>.\n", &com[1] );
               return -1;
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "OnboardChans" ) )
         {
            OnboardChans = k_int();
            init[0] = 1;
         }

         else if ( k_its( "NumMuxBoards" ) )
         {
            NumMuxBoards = k_int();
            init[1] = 1;
         }

         else if ( k_its( "ChanRate" ) )
         {
            ChanRate = k_val();
            init[2] = 1;
         }

         else if ( k_its( "ChanMsgSize" ) )
         {
            ChanMsgSize = k_int();
            init[3] = 1;
         }

         else if ( k_its( "SampRate" ) )
         {
            SampRate = k_val();
         }

         else if ( k_its( "Gain" ) )
         {
            Gain = k_int();
            init[4] = 1;
         }

         else if ( k_its( "MinGuideSignal" ) )
         {
            MinGuideSignal = k_val();
            init[5] = 1;
         }

         else if ( k_its( "MaxGuideNoise" ) )
         {
            MaxGuideNoise = (float)k_val();
            init[6] = 1;
         }

         else if ( k_its( "GuideChan" ) )
         {
            GuideChan = k_int();
            init[7] = 1;
         }

         else if ( k_its( "LowValue" ) )
         {
            LowValue = k_val();
            init[8] = 1;
         }

         else if ( k_its( "HighValue" ) )
         {
            HighValue = k_val();
            init[9] = 1;
         }

         else if ( k_its( "DAQ_device" ) )
         {
            DAQ_device = (short)k_int();
            init[10] = 1;
         }

         else if ( k_its( "TimeoutNoTrig" ) )
         {
            TimeoutNoTrig = k_int();
            init[11] = 1;
         }

         else if ( k_its( "RingName" ) )
         {
            if ( str = k_str() )
            {
               if ( (RingKey = GetKey(str)) == -1 )
               {
                  printf( "tapedig: Invalid RingName <%s>\n", str );
                  return -1;
               }
            }
            init[12] = 1;
         }

         else if ( k_its( "DIO_device" ) )
         {
            DIO_device = (short)k_int();
            init[13] = 1;
         }

         else if ( k_its( "n_DIO_ports" ) )
         {
            n_DIO_ports = k_int();
            init[14] = 1;
         }

/* An unknown parameter was encountered
   ************************************/
         else
         {
            printf( "tapedig: <%s> unknown parameter in <%s>\n", com, configfile );
            return -1;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "tapedig: Bad <%s> command in <%s>.\n", com, configfile );
            return -1;
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss > 0 )
   {
      printf( "tapedig: ERROR, no " );
      if ( !init[0]  ) printf( "<OnboardChans> " );
      if ( !init[1]  ) printf( "<NumMuxBoards> " );
      if ( !init[2]  ) printf( "<ChanRate> " );
      if ( !init[3]  ) printf( "<ChanMsgSize> " );
      if ( !init[4]  ) printf( "<Gain> " );
      if ( !init[5]  ) printf( "<MinGuideSignal> " );
      if ( !init[6]  ) printf( "<MaxGuideNoise> " );
      if ( !init[7]  ) printf( "<GuideChan> " );
      if ( !init[8]  ) printf( "<LowValue> " );
      if ( !init[9]  ) printf( "<HighValue> " );
      if ( !init[10] ) printf( "<DAQ_device> " );
      if ( !init[11] ) printf( "<TimeoutNoTrig> " );
      if ( !init[12] ) printf( "<RingName> " );
      if ( !init[13] ) printf( "<DIO_device> " );
      if ( !init[14] ) printf( "<n_DIO_ports> " );
      printf( "command(s) in <%s>.\n", configfile );
      return -1;
   }

/* Make sure the values of LowValue and HighValue are valid
   ********************************************************/
   if ( HighValue - LowValue < 0.1)
   {
      logit( "et", "Error: HighValue - LowValue < 0.1 volts\n" );
      return -1;
   }
   if ( LowValue < -10.0 )
   {
      logit( "et", "Error: LowValue < -10.0 volts\n" );
      return -1;
   }
   if ( HighValue > 10.0 )
   {
      logit( "et", "Error: HighValue > 10.0 volts\n" );
      return -1;
   }
   return 0;
}


 /***********************************************************************
  *                              LogConfig()                            *
  *                                                                     *
  *                   Log the configuration parameters                  *
  ***********************************************************************/

void LogConfig( void )
{
   logit( "", "RingKey:              %8d\n",    RingKey );
   logit( "", "DAQ_device:           %8hd\n",   DAQ_device );
   logit( "", "OnboardChans:         %8d\n",    OnboardChans );
   logit( "", "NumMuxBoards:         %8d\n",    NumMuxBoards );
   logit( "", "ChanRate:             %8.3lf\n", ChanRate );
   logit( "", "ChanMsgSize:          %8d\n",    ChanMsgSize );
   logit( "", "SampRate:             %10.2lf\n",SampRate );
   logit( "", "Gain:                 %8d\n",    Gain );
   logit( "", "MinGuideSignal:       %8.1lf\n", MinGuideSignal );
   logit( "", "MaxGuideNoise:        %8.1lf\n", MaxGuideNoise );
   logit( "", "GuideChan:            %8d\n",    GuideChan );
   logit( "", "TimeoutNoTrig:        %8d\n",    TimeoutNoTrig );
   logit( "", "LowValue:             %8.3lf\n", LowValue );
   logit( "", "HighValue:            %8.3lf\n", HighValue );
   logit( "", "DIO_device:           %8hd\n",   DIO_device );
   logit( "", "n_DIO_ports:          %8d\n",    n_DIO_ports );
   logit( "", "\n" );
   return;
}









    /*********************************************************************
     *                             IsComment()                           *
     *                                                                   *
     *  Accepts: String containing one line from a config file.          *
     *  Returns: 1 if it's a comment line                                *
     *           0 if it's not a comment line                            *
     *********************************************************************/

int IsComment( char string[] )
{
   int i;

   for ( i = 0; i < (int)strlen( string ); i++ )
   {
      char test = string[i];

      if ( test!=' ' && test!='\t' )
      {
         if ( test == '#'  )
            return 1;          /* It's a comment line */
         else
            return 0;          /* It's not a comment line */
      }
   }
   return 1;                   /* It contains only whitespace */
}
