
# This is mux2ew's parameter file

MyModuleId         MOD_MUX2EW  # module id for this instance of mux2ew
RingName           SCNL_RING   # shared memory ring for input/output
LogFile            1           # 0 to turn off disk log file; 1 to turn it on
                               # to log to module log but not stderr/stdout
HeartBeatInterval  15          # seconds between heartbeats
