/*
 * mux2ew.c:  Read a file created by the fmtape program and convert
 *            to tracebuf messages.  These messages are sent to waveserver
 *            via transport ring.  First tracebuf message is time-stamped
 *            from computer system clock.  A sample rate of 100 per second
 *            is assumed.  For now, mux2ew reads only one fmt file.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <trace_buf.h>
#include <lockfile.h>

#define N_DIO_PORTS 12

/* Functions in this source file
 *******************************/
void mux2ew_config( char * );
void mux2ew_lookup( void );
void mux2ew_status( unsigned char, short, char * );

static  SHM_INFO  Region;    /* shared memory region to use for i/o    */
pid_t   myPid;               /* for restarts by startstop               */

/* Things to read or derive from configuration file
 **************************************************/
static char    RingName[MAX_RING_STR];        /* name of transport ring for i/o    */
static char    MyModName[MAX_MOD_STR];       /* speak as this module name/id      */
static int     LogSwitch;           /* 0 if no logfile should be written */
static long    HeartBeatInterval;   /* seconds between heartbeats        */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o     */
static unsigned char InstId;        /* local installation id             */
static unsigned char MyModId;       /* Module Id for this program        */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeTracebuf2;

/* Error messages used by mux2ew
 *********************************/
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer     */
#define  ERR_NOTRACK       2   /* msg retreived; tracking limit exceeded */
static char  Text[150];        /* string for log/error messages          */


void main( int argc, char **argv )
{
   time_t    timeNow;           // current time
   time_t    timeLastBeat;      // time last heartbeat was sent
   int       res;
   char      *lockfile;
   int       lockfile_fd;
   MSG_LOGO  sendlogo;          // logo of outgoing tbuf message
   int       ChanMsgSize = 400; // Number of samples in tracebuf msg
   int       traceBufSize;      // Size of the trace buffer, in bytes
   char      *traceBuf;         // Where the trace message is assembled
   TRACE2_HEADER   *traceHead;  // Where the trace header is stored
   short     *traceDat;         // Where the data points are stored in the trace msg
   char      fname[80];         // Name of fmt file
   FILE      *fpfmt;
   long      nport;
   long      nchan;
   long      nscan;
   short     *daqBuf;           // Pointer to DAQ buffer
   int       daqBufSize;        // Size of the DAQ buffer in samples
   int       numRead;
   long   pattern[N_DIO_PORTS]; // Data from PCI-DIO-96

/* Check command line arguments
 ******************************/
   if ( argc != 2 )
   {
        fprintf( stderr, "Usage: mux2ew <configfile>\n" );
        exit( 0 );
   }

/* Initialize name of log-file & open it
 ***************************************/
   logit_init( argv[1], 0, 256, 1 );

/* Read the configuration file(s)
 ********************************/
   mux2ew_config( argv[1] );
   logit( "" , "%s: Read command file <%s>\n", argv[0], argv[1] );

/* Look up important info from earthworm.h tables
 ************************************************/
   mux2ew_lookup();

/* Reinitialize logit to desired logging level
 **********************************************/
   logit_init( argv[1], 0, 256, LogSwitch );
   lockfile = ew_lockfile_path(argv[1]);
   if ( (lockfile_fd = ew_lockfile(lockfile) ) == -1) {
        fprintf(stderr, "one  instance of %s is already running, exiting\n", argv[0]);
        exit(-1);
   }

/* Get process ID for heartbeat messages
   *************************************/
   myPid = getpid();
   if( myPid == -1 )
   {
     logit("e","mux2ew: Cannot get pid. Exiting.\n");
     exit (-1);
   }

/* Attach to Input/Output shared memory ring
 *******************************************/
   tport_attach( &Region, RingKey );
   logit( "", "mux2ew: Attached to public memory region %s: %d\n",
          RingName, RingKey );

/* Force a heartbeat to be issued in first pass thru main loop
 *************************************************************/
   timeLastBeat = time(&timeNow) - HeartBeatInterval - 1;

/* Allocate the trace buffer
   *************************/
   traceBufSize = sizeof(TRACE2_HEADER) + (ChanMsgSize * sizeof(short));
   logit( "t", "Trace buffer size: %d bytes\n", traceBufSize );
   traceBuf = (char *) malloc( MAX_TRACEBUF_SIZ );
   if ( traceBuf == NULL )
   {
      logit( "", "Error: Cannot allocate the trace buffer. Exiting.\n" );
      return;
   }
   traceHead = (TRACE2_HEADER *) &traceBuf[0];
   traceDat  = (short *) &traceBuf[sizeof(TRACE2_HEADER)];

/* We will keep incrementing this time by nominal packet length
   ************************************************************/
   time( &timeNow );
   traceHead->starttime = (double)timeNow;

/* Set up logo of outgoing tracebuf msgs
   *************************************/
   sendlogo.instid = InstId;
   sendlogo.mod    = MyModId;
   sendlogo.type   = TypeTracebuf2;

/* Open the fmt file and read header record.
   nport, nscan, and nscan are written only once at start of fmt file.
   ******************************************************************/
   strcpy( fname, "C:\\earthworm\\run-will\\fmt\\CNET870003_20091211_192827.fmt" );
   logit( "", "fmt file: %s\n", fname );
   fpfmt = fopen( fname, "rb" );
   if ( fpfmt == NULL )
   {
      logit( "e", "Error. Can't open file %s\n", fname );
      return;
   }
   if ( fread( &nport, sizeof(long), 1, fpfmt ) < 1 )
   {
      logit( "e", "Error reading nport from file header. Exiting.\n" );
      return;
   }
   logit( "", "nport: %d", nport );

   if ( fread( &nchan, sizeof(long), 1, fpfmt ) < 1 )
   {
      logit( "e", "Error reading nchan from file header. Exiting.\n" );
      return;
   }
   logit( "", "   nchan: %d", nchan );

   if ( fread( &nscan, sizeof(long), 1, fpfmt ) < 1 )
   {
      logit( "e", "Error reading nscan from file header. Exiting.\n" );
      return;
   }
   logit( "", "   nscan: %d\n", nscan );

/* Allocate buffer to contain DAQ data
   ***********************************/
   daqBufSize = nchan * nscan;
   daqBuf = (short *) calloc( daqBufSize, sizeof(short) );
   if ( daqBuf == NULL )
   {
      logit( "e", "Error: Cannot allocate the DAQ buffer. Exiting.\n" );
      return;
   }

/*----------------------- setup done; start main loop -------------------------*/

   while ( 1 )
   {
      int muxscan;  // Scan number read from muxed data packet
      int pin;      // Pin number index

   /* Send mux2ew's heartbeat
      ***********************/
      if ( time(&timeNow) - timeLastBeat  >=  HeartBeatInterval )
      {
         timeLastBeat = timeNow;
         mux2ew_status( TypeHeartBeat, 0, "" );
      }

   /* See if a termination has been requested
      *****************************************/
      if ( tport_getflag( &Region ) == TERMINATE ||
           tport_getflag( &Region ) == myPid )
      {
         tport_detach( &Region );
         logit( "t", "mux2ew: Termination requested; exiting!\n" );
         ew_unlockfile(lockfile_fd);
         ew_unlink_lockfile(lockfile);
         fclose( fpfmt );
         exit( 0 );
      }

   /* Get DIO ports, scan count, and muxed DAQ data from fmt file
      ***********************************************************/
      numRead = fread( pattern, sizeof(long), N_DIO_PORTS, fpfmt );
      if ( numRead < N_DIO_PORTS )
      {
         if ( feof(fpfmt) )
            printf( "End of fmt file encountered while reading DIO ports.\n" );
         else
            printf( "Error reading DIO ports from fmt file. Exiting.\n" );
         fclose( fpfmt );
         exit( 0 );
      }
      if ( fread( &muxscan, sizeof(long), 1, fpfmt ) < 1 )
      {
         printf( "Error reading scan count (muxscan) from fmt file. Exiting.\n" );
         fclose( fpfmt );
         exit( 0 );
      }
//    printf( "muxscan: %d\n", muxscan );
      numRead = fread( daqBuf, sizeof(short), daqBufSize, fpfmt );
      if ( numRead < daqBufSize )
      {
         logit( "e", "Error reading DAQ buffer from fmt file. Exiting.\n" );
         fclose( fpfmt );
         exit( 0 );
      }

   /* Fill in some of the trace buffer header.
      Trace start and end times are in seconds since midnight 1/1/1970
      ****************************************************************/
      traceHead->nsamp      = ChanMsgSize;         // Number of samples in message
      traceHead->samprate   = 100.0;               // Sample rate; nominal
      traceHead->version[0] = TRACE2_VERSION0;     // Header version number
      traceHead->version[1] = TRACE2_VERSION1;     // Header version number
      traceHead->quality[0] = '\0';                // One bit per condition
      traceHead->quality[1] = '\0';                // One bit per condition
      strcpy( traceHead->net,  "NC" );             // Network name
      strcpy( traceHead->chan, "VHZ" );            // Component/channel code
      strcpy( traceHead->loc,  "--" );             // Location code
      strcpy( traceHead->datatype, "i2" );         // Data format code

      traceHead->starttime += ChanMsgSize / traceHead->samprate;
      traceHead->endtime   = traceHead->starttime + (ChanMsgSize - 1)/traceHead->samprate;

   /* Demux DAQ buffer and write tracebuf packets to transport ring
      *************************************************************/
      for ( pin = 0; pin < nchan; pin++ )
      {
         int scan;     // Scan number index
         int db = pin;

   /* Fill the rest of the trace buffer header
      ****************************************/
         traceHead->pinno = pin;                    // Pin number
         sprintf( traceHead->sta, "P%03d", pin );   // Site name

   /* Fill trace buffer with samples from mux data block
      **************************************************/
         for ( scan = 0; scan < nscan; scan++ )
         {
            traceDat[scan] = daqBuf[db];
            db += nchan;
         }

   /* Put tracebuf message to transport ring
      **************************************/
         res = tport_putmsg( &Region, &sendlogo, traceBufSize, traceBuf );
         if ( res == PUT_TOOBIG )
         {                              /* complain and exit   */
              sprintf(Text, "Trace message too big for tport_putmsg" );
              mux2ew_status( TypeError, ERR_TOOBIG, Text );
              exit( 0 );
         }
         if ( res == PUT_NOTRACK )
         {                             /* if any were missed        */
              sprintf( Text, "Tracking error detected by tport_putmsg\n" );
              mux2ew_status( TypeError, ERR_NOTRACK, Text );
              exit( 0 );
         }
      }
      sleep_ew( 4000 );
   }
}
/*-----------------------------end of main program -------------------------------*/


/******************************************************************************
 *  mux2ew_config() processes command file(s) using kom.c functions;        *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
void mux2ew_config( char *configfile )
{
   int      ncommand;     /* # of required commands you expect to process   */
   char     init[10];     /* init flags, one byte for each required command */
   int      nmiss;        /* number of required commands that were missed   */
   char    *com;
   char    *str;
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command
 *****************************************************/
   ncommand = 4;
   for( i=0; i<ncommand; i++ )  init[i] = 0;

/* Open the main configuration file
 **********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 ) {
        logit( "e",
                "mux2ew: Error opening command file <%s>; exiting!\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  logit( "e",
                          "mux2ew: Error opening command file <%s>; exiting!\n",
                           &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_long();
                init[3] = 1;
            }

         /* Unknown command
          *****************/
            else {
                logit( "e", "mux2ew: <%s> Unknown command in <%s>.\n",
                         com, configfile );
                continue;
            }

        /* See if there were any errors processing the command
         *****************************************************/
            if( k_err() ) {
               logit( "e",
                       "mux2ew: Bad <%s> command in <%s>; exiting!\n",
                        com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       logit( "e", "mux2ew: ERROR, no " );
       if ( !init[0] )  logit( "e", "<LogFile> "           );
       if ( !init[1] )  logit( "e", "<MyModuleId> "        );
       if ( !init[2] )  logit( "e", "<RingName> "          );
       if ( !init[3] )  logit( "e", "<HeartBeatInterval> " );
       logit( "e", "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }

   return;
}

/******************************************************************************
 *  mux2ew_lookup( )   Look up important info from earthworm.h tables       *
 ******************************************************************************/
void mux2ew_lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 ) {
        fprintf( stderr,
                "mux2ew:  Invalid ring name <%s>; exiting!\n", RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr,
              "mux2ew: error getting local installation id; exiting!\n" );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr,
              "mux2ew: Invalid module name <%s>; exiting!\n", MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr,
              "mux2ew: Invalid message type <TYPE_HEARTBEAT>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr,
              "mux2ew: Invalid message type <TYPE_ERROR>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF2", &TypeTracebuf2 ) != 0 ) {
      fprintf( stderr,
              "mux2ew: Invalid message type <TYPE_TRACEBUF2>; exiting!\n" );
      exit( -1 );
   }
   return;
}

/******************************************************************************
 * mux2ew_status() builds a heartbeat or error message & puts it into       *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/
void mux2ew_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size;
   time_t        t;

/* Build the message
 *******************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
        sprintf( msg, "%ld %ld\n\0", (long) t, (long) myPid);
   }
   else if( type == TypeError )
   {
        sprintf( msg, "%ld %hd %s\n\0", (long) t, ierr, note);
        logit( "et", "mux2ew: %s\n", note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","mux2ew:  Error sending heartbeat.\n" );
        }
        else if( type == TypeError ) {
           logit("et","mux2ew:  Error sending error:%d.\n", ierr );
        }
   }

   return;
}
