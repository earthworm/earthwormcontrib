#
#
#                 import_ms configuration file
#
#
# The intent is that specific import_xxx modules can be cloned from this.
# This program includes a site-specific routine "import_filter" which
# determines what to do with the received messages. The default 
# "import_filter" routine assumes that the message originated from
# an  "export_generic" with a default "export_filter" routine: That routine
# attaches the logo of the originating message as the first nine characters
# (3 groups of 3) of the outgoing message, and exports it.
# The local "import_filter" removes this logo from the message, decodes it,
# and broadcasts the message onto its ring under this logo.

# This version of import uses the "import_filter" to determine if the
# exporting data stream is master or slave and only passes info on
# to the transport ring if master.

 MyModuleId     MOD_IMPORT_B         # module id for this import,
 RingName       HYPO_RING            # transport ring to use for input/output,
 HeartBeatInt        30              # Heartbeat interval in seconds
 LogFile              1              # If 0, don't write logfile,
 MaxMsgSize       60000              # maximum msg size (bytes) for i/o
 SocketTimeout   150000 	         #Timeout for Socket_ew calls in ms
                                     # Must be > SenderHeartRate!
 HeartbeatDebug       0  # normally 0
 SocketDebug          0  # normally 0
#
#  Messages are assumed to terminate with two newline '\n\n'  
#  characters. The '\n\n' are both removed, and replaced by a zero byte.
#  Someday this should be an option defined in this file...
#  If an oversized message is received, it will be discarded, the 
#  socket will be closed, log an error, and it will connect and try
#  again. This might flood the log file if all incoming messages are
#  long. Maybe it should terminate instead...
#
# the two lines below are read, but not implemented - who needs it?
#
 MyAliveString  "alive"  	# heartbeat text to foreign server machine
 MyAliveInt     30      	# seconds between alive msgs sent to server
				# 0 => no alive messages to sending machine
# 
# Sender description follows:
#
SenderIpAdr	130.118.50.149	# Address of the machine we listen to (ew01)
SenderPort	16011		# IP port number used by sender
SenderHeartRate	120		# Sender's heart beat interval in seconds
				# 0 => no heartbeat expected
SenderHeartText "alive"   # Sender's heartbeat text

# 
# Master/Slave stuff:
#
MyMasterA    kapulu         # Which machine do we hope is master?
MasterAfile  "ppicker@kapulu.wr.usgs.gov:/we/rtp/ppicker/params/master-slave";

MyMasterB    redhot         # Which machine is the other one?
MasterBfile  "ppicker@redhot.wr.usgs.gov:/we/rtp/ppicker/params/master-slave";
#
# Optional save directory
# If SaveDir defined, arc message will be written to a file only,
# If not defined arc message will be written to the transport ring.
#
TempDir   /home/picker/arc/temp
SaveDir   /home/picker/arc/indir


Debug 0