/*
 *   import_ms.c:  Program to receive messages from far away
 *                      via socket and put them into a transport ring.

 *   The intent is that there will be a specific import_xxx module for
 *   importing from institution xxx. That is, there will be as many
 *   import modules as places to import from, each module having it's
 *   own name, and living in its own directory.  Each such module could
 *   have it's own "import_filter" routine, coded for the specific job.
 *   
 *   Upgraded to move binary messages. Alex 10/4/96.
 *   
 *   Change to use sendall() instead of send() to fix partial send() return
 *   under NT (and maybe even Solaris) Alex 11/13/97
 *   
 *   Installed Bug fix suggested by Doug Neuhauser:
 *   "...I believe the following code in state 2:
 *                   if (chr==endCharacter && lastChr != escape)   
 *   should be:
 *                   if (chr==endCharacter)   ..."
 *   Alex 11/13/97
 *   See comments in export_generic.c
 *   
 *     
 *   Modified 11/20/97 to perform Multi-Byte recv() calls 
 *   and buffer information internally.  Also replaced state
 *   numbers with #defined constants  Davidk

 *   This version designed to receive messages from a Master/Slave setup
 *   And pass along only the info from the Master. Jim Luetgert 4/28/98
 *
 *   Import_ms originally determined master/slave status by pulling the
 *   master-slave file from each of the ebirds using rcp.  Now that rcp
 *   is no longer being used, the routine has been changed to assume that
 *   the ebirds regularly push their master-slave file to the params dir.
 *   Jim Luetgert 10/26/05
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>
#include <errno.h>
#include <signal.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <socket_ew.h>
#include <imp_exp_gen.h>


#define MAX_LOGO  20

/* Functions in this source file
   *****************************/
void    config( char * );
void    lookup( void );
void    output_status( unsigned char, short, char * );
void    import_filter( char *, int);
int     WriteToSocket( int, char *, MSG_LOGO * );
int     Whos_on_First( char  *errtxt);
int     Whos_on_First_rcp( char  *errtxt);
thr_ret Heartbeat( void * );
thr_ret MessageReceiver( void * );

extern int  errno;
static SHM_INFO  Region;      /* shared memory region to use for i/o    */

/* Things to read or derive from configuration file
 **************************************************/
static char    RingName[20];         /* name of transport ring for i/o    */
static char    MyModName[20];        /* speak as this module name/id      */
static int     LogSwitch;            /* 0 if no logfile should be written */
static int     HeartBeatInt;         /* seconds between heartbeats (to local ring)  */
static long    MaxMsgSize;           /* max size for input/output msgs    */
static int     MyAliveInt;           /* Seconds between sending alive message to foreign sender */
static char    MyAliveString[100];   /* Text of above alive message */

static char    SenderIpAdr[20];      /* Foreign sender's address, in dot notation */
static int     SenderPort;           /* Server's well-known port number   */
static int     SenderHeartRate;      /* Expect alive messages this often from foreign sender     */
static char    SenderHeartText[100]; /* Text making up the sender's heart beat message           */
MSG_LOGO       PutAsLogo;            /* logo to be used for placing received messages into ring. */
                                     /* May be superceeded in the message processing routine
                                      "import_filter" */
static unsigned int SocketTimeoutLength; /* Length of timeouts on SOCKET_ew calls */
static int HeartbeatDebug=0;         /* set to 1 in for heartbeat debug messages  */
static int SOCKET_ewDebug=0;         /* set to 1 in for heartbeat debug messages  */
static int Debug=0;                  /* set to 1 in for general debug messages    */
static char MyMasterA[20];           /* master name for A                         */
static char MyMasterB[20];           /* master name for B                         */
static char   MasterAfile[100];
static char   MasterBfile[100];
static int    save_arcs;             /* Save the arc msg as a file.               */
static char   TempDir[100];          /* Temp Directory for the arc files.         */
static char   SaveDir[100];          /* Directory for the arc files.              */
/* Globals: timers, etc, used by both threads */
/**********************************************/
#define CONNECT_WAIT_DT 10      /* Seconds wait between connect attempts */
#define THEAD_STACK_SIZE 8192   /* Implies different things on different systems !! */
                /* on os2, the stack grows dynamically beyond this number if needed, but likey */
                /* at the expesene of performance. Solaris: not sure, but overflow might be fatal */
time_t LastServerBeat;          /* times of heartbeats from the server machine             */
time_t MyLastInternalBeat;      /* time of last heartbeat into the local Earthworm ring    */
time_t MyLastSocketBeat;        /* time of last heartbeat to server - via socket           */
int HeartThreadStatus = 0;      /* goes negative if server croaks. Set by heartbeat thread */
int MessageReceiverStatus =0;   /* status of message receiving thread: -1 means bad news   */
char* MsgBuf;                   /* incoming message buffer; used by receiver thread        */
int  Sd;                        /* Socket descriptor                        */
unsigned  TidHeart;             /* thread id. was type thread_t on Solaris! */
unsigned  TidMsgRcv;            /* thread id. was type thread_t on Solaris! */
char Text[256];                 /* quite crude - array for error text       */

char  Master[80];
char  MSstring[80];
char  module[] = "import_ms";

pid_t MyPid;            /* Our own pid, sent with heartbeat for restart purposes */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o     */
static unsigned char MyInstId;      /* local installation id             */
static unsigned char MyModId;       /* Module Id for this program        */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;

/* Error messages used by import
 *********************************/
#define  ERR_SOCK_READ       0   /* error reading from socket  */
#define  ERR_TOOBIG          1   /* retrieved msg too large for buffer     */
#define  ERR_TPORTPUT        2   /* error putting message into ring */
#define  ERR_SERVER_DEAD     3   /* server machine heartbeats not received on time */
#define  ERR_GARBAGE_IN      4   /* something other than STX after ETX */

int main( int argc, char **argv )
{
    struct sockaddr_in insocket;
    int    on = 1;
    time_t lastQuit=0;
    time_t now;
    char   whoami[50];
    char   startCharacter=STX;    /* frame start character; from imp_exp_gen.h (ASCII STX) */
    char   endCharacter=ETX;      /* ditto frame end. (ASCII ETX) */
    int    quit;
    int    retryCount;            /* to prevent flooding the log file */
    int    ThreadHappy;           /* to prevent flooding the log file */

   /* Check command line arguments
   ******************************/
    if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", module );
        return -1;
    }
    sprintf(whoami, " %s: %s: ", module, "main");

   /* Read the configuration file(s)
   ********************************/
    config( argv[1] );

   /* Set Socket Debug to on 
   ********************************/
    setSocket_ewDebug(SOCKET_ewDebug);

   /* Get our own Pid for restart purposes
   ***************************************/
   MyPid = getpid();
   if( MyPid == -1 )
   {
      fprintf( stderr,"%s Cannot get pid. Exiting.\n", whoami);
      return -1;
   }
   
   /* Look up important info from earthworm.h tables
   ************************************************/
    lookup();

   /* Initialize name of log-file & open it
   ***************************************/
    logit_init( argv[1], (short) MyModId, 256, LogSwitch );
    logit( "" , "%s Read command file <%s>\n", whoami, argv[1] );

   /* Attach to Input/Output shared memory ring
   *******************************************/
    tport_attach( &Region, RingKey );
    logit( "", "%s Attached to public memory region %s: %d\n", whoami, RingName, RingKey );

   /* Allocate the message buffer
   ******************************/
    if ( ( MsgBuf = (char *) malloc(MaxMsgSize) ) == (char *) NULL ) {
        logit("e","%s Can't allocate buffer of size %ld bytes\n", whoami, MaxMsgSize);
        return -1;
    }

   /* Initialize the socket system
   ******************************/
    SocketSysInit();

/* Stuff address and port into socket structure
   ********************************************/
    memset( (char *)&insocket, '\0', sizeof(insocket) );
    insocket.sin_family = AF_INET;
    insocket.sin_port   = htons( (short)SenderPort );

    if ((int)(insocket.sin_addr.S_un.S_addr = inet_addr(SenderIpAdr)) == -1) {
        logit( "e", "%s inet_addr failed for SenderIpAdr <%s>; exiting!\n", whoami, SenderIpAdr );
        return -1;
    }

   /* Find out who is master
   *************************/
    do { Whos_on_First(MSstring); } 
        while(strcmp(MSstring, MyMasterA) && strcmp(MSstring, MyMasterB));
    strcpy(Master, MSstring);
    logit( "et", "%s %s is Master!\n", whoami, Master );
    
 
   /* to prevent flooding the log file during long reconnect attempts */
   /********************************************************************/
    ThreadHappy = retryCount = 0;  /* it may be reset elsewere */

                                 /********************/
                                      reconnect:
                                 /********************/
    retryCount++;
    
   /* Create a socket
   ****************/
    if ( ( Sd = socket_ew( AF_INET, SOCK_STREAM, 0/* ,SocketTimeoutLength timeout ignored */ ) ) == -1 ) {
        SocketPerror( "import_ms: socket" );
        logit( "et", "import_ms: Error opening socket.  Exitting\n" );
        closesocket_ew(Sd,SOCKET_CLOSE_IMMEDIATELY_EW);
        free(MsgBuf);
        return -1;
    }

   /* Try for a network connection - and keep trying forever !!
   ************************************************************/
    if(retryCount< 4)logit("t","import_ms: Trying to connect to %s on port %d\n",SenderIpAdr,SenderPort);
    if(retryCount==4)logit("t","import_ms: repetitions not logged\n");
    if(connect_ew(Sd, (struct sockaddr *)&insocket, sizeof(insocket),SocketTimeoutLength /* wait ten seconds */ ) == -1 ) {

      /* Are we being told to quit */
      /*****************************/
        if ( tport_getflag( &Region ) == TERMINATE ||
             tport_getflag( &Region ) == MyPid ) {
            tport_detach( &Region );
            logit("t","import_ms: terminating on request\n");
            (void)KillThread(TidMsgRcv);
            free(MsgBuf);
            (void)KillThread(TidHeart);
            return 0;
        }
        if(Debug) logit("t","import_ms:%s %s Failed to connect. Waiting\n", whoami, argv[1] );

        closesocket_ew( Sd, SOCKET_CLOSE_IMMEDIATELY_EW);

        if(retryCount< 4) logit("t","import_ms: Failed to connect. Waiting\n");

        if(retryCount==4) logit("t","import_ms: Repetitions not logged\n");

        sleep_ew(CONNECT_WAIT_DT*1000);
        goto reconnect;    /*** JUMP to reconnect ***/
    }
    logit("t","import_ms: Connected after %d seconds\n",
         CONNECT_WAIT_DT*(retryCount-1));
    retryCount=0;

   /* Start the heartbeat thread
   ****************************/
    HeartThreadStatus=0;       /* set it's status flag to ok */
    time(&MyLastInternalBeat); /* initialize our last heartbeat time */
    time(&MyLastSocketBeat);   /* initialize time of our heartbeat over socket */
    time(&LastServerBeat);     /* initialize time of last heartbeat from */
                          /*   serving machine */
    if ( StartThread( Heartbeat, (unsigned)THEAD_STACK_SIZE, &TidHeart ) == -1 ) {
        logit( "e","import_ms: Error starting Heartbeat thread. Exitting.\n" );
        tport_detach( &Region );
        free(MsgBuf);
        return -1;
    }

   /* Start the message receiver thread
   **********************************/
    MessageReceiverStatus =0; /* set it's status flag to ok */
    if ( StartThread( MessageReceiver, (unsigned)THEAD_STACK_SIZE, &TidMsgRcv ) == -1 ) {
        logit( "e","import_ms: Error starting MessageReceiver thread. Exitting.\n" );
        tport_detach( &Region );
        free(MsgBuf);
        return -1;
    }

   /* Working loop: check on server heartbeat status, check on receive thread health.
      check for shutdown requests. If things go wrong, kill all  threads and restart
   *********************************************************************************/
    quit=0; /* to restart or not to restart */
    while (1) {
        sleep_ew(1000); /* sleep one second. Remember, the receive thread is awake */
        time(&now);

      /* How's the server's heart? */
      /*****************************/
        if (difftime(now,LastServerBeat) > (double)SenderHeartRate && SenderHeartRate !=0) {
            output_status(TypeError,ERR_SERVER_DEAD," Server heartbeat lost. Restarting");
            quit=1; /*restart*/
        }

      /* How's the receive thread feeling ? */
      /**************************************/
        if ( MessageReceiverStatus == -1) {
            ThreadHappy += 1;
            logit("et","import_ms: Receiver thread unhappy. Restarting\n");
            sleep_ew(1000*ThreadHappy);
            quit=1;
        } else ThreadHappy = 0;

      /* How's the heartbeat thread feeling ? */
      /**************************************/
        if ( HeartThreadStatus == -1) {
            logit("et","import_ms: Heartbeat thread unhappy. Restarting\n");
            quit=1;
        }

      /* Are we being told to quit */
      /*****************************/
        if ( tport_getflag( &Region ) == TERMINATE ||
             tport_getflag( &Region ) == MyPid ) {
            tport_detach( &Region );
            logit("t","import_ms: terminating on request\n");
            (void)KillThread(TidMsgRcv);
            free(MsgBuf);
            (void)KillThread(TidHeart);
            return 0;
        }

      /* Any other shutdown conditions here */
      /**************************************/
      /* blah blah blah */

      /* restart preparations */
      /************************/
        if (quit == 1) {
            (void)KillThread(TidMsgRcv);
            (void)KillThread(TidHeart);
            closesocket_ew( Sd, SOCKET_CLOSE_IMMEDIATELY_EW);
            quit=0;
            goto reconnect;
        }

    }  /* end of working loop */
}

/************************Messge Receiver Thread *********************
*          Listen for client heartbeats, and set global variable     *
*          showing time of last heartbeat. Main thread will take     *
*          it from there                                             *
**********************************************************************/
/*
Modified to read binary messages, alex 10/10/96: The scheme (I got it from Carl) is define
some sacred characters. Sacred characters are the start-of-message and end-of-message
framing characters, and an escape character. The sender's job is to cloak unfortunate bit
patters in the data which look like sacred characters by inserting before them an 'escape'
character.  Our problem here is to recognize, and use, the 'real' start- and end-of-
messge characters, and to 'decloak' any unfortunate look-alikes within the message body.
*/



thr_ret MessageReceiver( void *dummy )
{
    static int state;
    static char chr, lastChr;
    static int nr;
    static char errText[256];
    static long  nchar;                       /* counter for above buffer */
    static char startCharacter=STX;           /* ASCII STX characer */
    static char endCharacter=ETX;             /* ASCII ETX character */
    static char escape=ESC;                   /* our escape character */
    static int iret;                          /* misc. retrun values */
    static time_t now;
    static char inBuffer[INBUFFERSIZE];
    static long  inchar;                       /* counter for above buffer */

                                             /* Tell the main thread we're ok
  *******************************/
    MessageReceiverStatus = 0;

    state=SEARCHING_FOR_MESSAGE_START; /* we're initializing */
                                       /* Start of New code Section DK 11/20 Multi-byte Read 
                                       Set inchar to be nr-1, so that when inchar is incremented, they will be the
                                       same and a new read from socket will take place.  Set chr to 0 to indicate
                                       a null character, since we haven't read any yet.  */
    inchar=-1;
    nr=0;
    chr=0;
    /* End of New code Section */



  /* Old Code IGNORE */
  /* Read first character from socket
  *********************************** /
  nr=recv(Sd,&chr,1,0); if (nr <= 0) goto suicide; /* get one character * /
  nchar=0; /* next character position to load * /
  /* End of Old Code */

  /* Working loop: receive and process messages
  ********************************************/
  /* We are either  (0) initializing: searching for message start
  (1) expecting a message start: error if not
  (2) assembling a message
  the variable "state' determines our mood */

    while(1) { /* loop over bytes read from socket */
 
    /* Get next char operation */
        if (++inchar == nr) {      /* Read from socket operation */
            nr=recv_ew(Sd,inBuffer,INBUFFERSIZE-1,0, SocketTimeoutLength);
            if (nr<=0) goto suicide;  /* Connection Closed */
            inchar=0;      /* End of Read from socket operation */
        }
        lastChr=chr;
        chr=inBuffer[inchar];
    /* End of Get next char operation */

    /* Initialization */
    /******************/
        if (state==SEARCHING_FOR_MESSAGE_START) {   /* throw all away until we see a naked start character */
   
    /* Do we have a real start character?
      *************************************/
            if ( lastChr!=escape && chr==startCharacter) {
                state=ASSEMBLING_MESSAGE;  /*from now on, assemble message */
                continue;
            }
        }

    /* Confirm message start */
    /*************************/
        if (state==EXPECTING_MESSAGE_START) {  /* the next char had better be a start character - naked, that is */
   
    /* Is it a naked start character?
      *********************************/
            if ( chr==startCharacter &&  lastChr != escape) { /* This is it: message start!! */
                nchar=0; /* start with firsts char position */
                state=ASSEMBLING_MESSAGE; /* go into assembly mode */
                continue;
            }
            else {   /* we're eating garbage */

                logit("t","unexpected character from client. Re-synching\n");
                state=SEARCHING_FOR_MESSAGE_START; /* search for next start sequence */
                continue;
            }
        }

    /* In the throes of assembling a message */
    /****************************************/
        if (state==ASSEMBLING_MESSAGE) {
    /* Is this the end?
      *******************/
            if (chr==endCharacter) {   /* naked end character: end of the message is at hand */
        /* We have a complete message */
        /*****************************/
                MsgBuf[nchar]=0; /* terminate as a string */
                if(strcmp(&MsgBuf[9],SenderHeartText)==0) { /* Server's heartbeat */
                    if(HeartbeatDebug) {
                        logit("et","import_ms: Received heartbeat\n"); 
                    }
                    time(&LastServerBeat); /* note time of heartbeat */
                    state=EXPECTING_MESSAGE_START; /* reset for next message */
                    MsgBuf[0]=' '; MsgBuf[9]=' ';
                }
                else {
          /* got a non-heartbeat message */
                    if(HeartbeatDebug) {
                        logit("et","import_ms: Received non-heartbeat\n"); 
                    }
                    time(&LastServerBeat); /* note time of heartbeat */
          /* This is not a genuine heartbeat, but our major concern is that the
             export module on the other end is still alive, and any message that
             we receive should indicate life on the other side(in a non occult 
             kind of way).
          */
                    import_filter(MsgBuf,nchar); /* process the message via user-routine */
                }
                state=EXPECTING_MESSAGE_START;
                continue;
            }
            else {
        /* process the message byte we just read: we know it's not a naked end character*/
        /********************************************************************************/
        /* Escape sequence? */
                if (chr==escape) {  /*  process the escape sequence */

                    /* Read from buffer
                    *******************/
                    /* Get next char operation */
                    if (++inchar == nr) {
                        /* Read from socket operation */
                        nr=recv_ew(Sd,inBuffer,99,0,SocketTimeoutLength);
                        if (nr<=0) goto suicide;  /* Connection Closed */
                        inchar=0;    /* End of Read from socket operation */
                    }
                    lastChr=chr;
                    chr=inBuffer[inchar];
                    /* End of Get next char operation */

                    if( chr != startCharacter && chr != endCharacter && chr != escape) {   /* bad news: unknown escape sequence */
                        logit("t","unknown escape sequence in message. Re-synching\n");
                        state=SEARCHING_FOR_MESSAGE_START; /* search for next start sequence */
                        continue;
                    }
                    else {   /* it's ok: it's a legit escape sequence, and we save the escaped byte */
                        MsgBuf[nchar++]=chr; if(nchar>MaxMsgSize) goto freak; /*save character */
                        continue;
                    }
                }

                /*  Naked start character? */
                if (chr==startCharacter) {   /* bad news: unescaped start character */
                    logit("t","unescaped start character in message. Re-synching\n");
                    state=SEARCHING_FOR_MESSAGE_START; /* search for next start sequence */
                    continue;
                }

                /* So it's not a naked start, escape, or a naked end: Hey, it's just a normal byte */
                MsgBuf[nchar++]=chr; if(nchar>MaxMsgSize) goto freak; /*save character */
                continue;

                        freak:  /* freakout: message won't fit */
                {
                    logit("t","import_ms: receive buffer overflow after %ld bytes\n",MaxMsgSize);
                    state=SEARCHING_FOR_MESSAGE_START; /* initialize again */
                    nchar=0;
                    continue;
                }
            } /* end of not-an-endCharacter processing */
        } /* end of state==ASSEMBLING_MESSAGE processing */
    }  /* end of loop over characters */
suicide:
    SocketPerror("import_ms: recv()");
    sprintf(errText,"Bad socket read: %d\n",nr);
    logit("t",errText);
    MessageReceiverStatus=-1; /* file a complaint to the main thread */
    KillSelfThread(); /* main thread will restart us */
    logit("t","Fatal system error: Receiver thread could not KillSelf\n");
    exit(-1);

}  /* end of MessageReceiver thread */


/***************************** Heartbeat **************************
 *           Send a heartbeat to the transport ring buffer        *
 *           Send a heartbeat to the server via socket            *
 *                 Check on our server's hearbeat                 *
 *           Slam socket shut if no Server heartbeat: that        *
 *            really shakes up the main thread                    *
 ******************************************************************/

thr_ret Heartbeat( void *dummy )
{
    MSG_LOGO     reclogo;
    time_t now;

   /* once a second, do the rounds. If anything looks bad, set HeartThreadStatus to -1
      and go into a long sleep. The main thread should note that our status is -1,
      and launch into re-start procedure, which includes killing and restarting us. */
    while ( 1 ) {
        sleep_ew(1000);
        time(&now);

        /* Beat our heart (into the local Earthworm) if it's time
        ********************************************************/
        if (difftime(now,MyLastInternalBeat) > (double)HeartBeatInt) {
            output_status( TypeHeartBeat, 0, "" );
            time(&MyLastInternalBeat);
        }

      /* Beat our heart (over the socket) to our server
      **************************************************/
        if (difftime(now,MyLastSocketBeat) > (double)MyAliveInt && MyAliveInt != 0) {
            reclogo.instid = MyInstId;
            reclogo.mod    = MyModId;
            reclogo.type   = TypeHeartBeat;
            if ( WriteToSocket( Sd, MyAliveString, &reclogo ) != 0 ) {
                /* If we get an error, simply quit */
                sprintf( Text, "error sending alive msg to socket. Heart thread quitting\n" );
                output_status( TypeError, ERR_SOCK_READ, Text );
                logit("t",Text);
                HeartThreadStatus=-1;
                KillSelfThread();  /* the main thread will resurect us */
                logit("et","Fatal system error: Heart thread could not KillSelf\n");
                exit(-1);
            }
            if(HeartbeatDebug) {
                logit("et","Heartbeat sent to export\n");
            }
            MyLastSocketBeat=now;
        }
    }
}


/*****************************************************************************
 *  config() processes command file(s) using kom.c functions;                *
 *                    exits if any errors are encountered.                   *
 *****************************************************************************/
void config( char *configfile )
{
    int      ncommand;     /* # of required commands you expect to process   */
    char     init[20];     /* init flags, one byte for each required command */
    int      nmiss;        /* number of required commands that were missed   */
    char    *com;
    char    *str;
    int      nfiles;
    int      success;
    int      i, j;

/* Set to zero one init flag for each required command
 *****************************************************/
    ncommand = 15;
    for( i=0; i<ncommand; i++ )  init[i] = 0;
    save_arcs = 0;

/* Open the main configuration file
 **********************************/
    nfiles = k_open( configfile );
    if ( nfiles == 0 ) {
        fprintf( stderr,
            "import_ms: Error opening command file <%s>; exiting!\n",
             configfile );
        exit( -1 );
    }

/* Process all command files
 ***************************/
    while(nfiles > 0) {   /* While there are command files open */
        while(k_rd()) {        /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file
         **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr,
                          "import_ms: Error opening command file <%s>; exiting!\n",
                           &com[1] );
                    exit( -1 );
                }
                continue;
            }

        /* Process anything else as a command
         ************************************/
  /*0*/     if( k_its("MyModuleId") ) {
                str=k_str();
                if(str) strcpy(MyModName,str);
                init[0] = 1;
            }
  /*1*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("HeartBeatInt") ) {
                HeartBeatInt = k_int();
                init[2] = 1;
            }
  /*3*/     else if(k_its("LogFile") ) {
                LogSwitch=k_int();
                init[3]=1;
            }

         /* Maximum size (bytes) for incoming messages
          ********************************************/
  /*4*/     else if( k_its("MaxMsgSize") ) {
                MaxMsgSize = k_long();
                init[4] = 1;
            }

        /* 5 Interval for alive messages to sending machine
        ***************************************************/
            else if( k_its("MyAliveInt") ) {
                MyAliveInt = k_int();
                init[5]=1;
            }

        /* 6 Text of alive message to sending machine
        *********************************************/
        else if( k_its("MyAliveString") ){
                str=k_str();
                if(str) strcpy(MyAliveString,str);
                init[6]=1;
        }

        /* 7 Sender's internet address, in dot notation
        ***********************************************/
        else if(k_its("SenderIpAdr") ) {
                str=k_str();
                if(str) strcpy(SenderIpAdr,str);
                init[7]=1;
        }

        /* 8 Sender's Port Number
        *************************/
            else if( k_its("SenderPort") ) {
                SenderPort = k_int();
                init[8]=1;
            }

        /* 9 Sender's Heart beat interval
        ********************************/
            else if( k_its("SenderHeartRate") ) {
                SenderHeartRate = k_int();
                init[9]=1;
                SocketTimeoutLength = SenderHeartRate * 1000;
                /* Convert heartrate from sec to msec,
               and then base the sockettimeoutlength on
               the heartrate 
               */ 
            }

        /* 10 Sender's heart beat text
        ******************************/
        else if(k_its("SenderHeartText") ) {
                str=k_str();
                if(str) strcpy(SenderHeartText,str);
                init[10]=1;
            }


        /* 11 Name of master A machine
        ******************************/
        else if( k_its("MyMasterA") ) {
                str = k_str();
                if(str) strcpy( MyMasterA , str );
                init[11] = 1;
            }

        /* 12 Name of master B machine
        ******************************/
        else if( k_its("MyMasterB") ) {
                str = k_str();
                if(str) strcpy( MyMasterB , str );
                init[12] = 1;
            }

        /* 13 Full path to master A master-slave file
        *********************************************/
        else if( k_its("MasterAfile") ) {
                str = k_str();
                if(str) strcpy( MasterAfile , str );
                init[13] = 1;
            }

        /* 14 Full path to master B master-slave file
        *********************************************/
        else if( k_its("MasterBfile") ) {
                str = k_str();
                if(str) strcpy( MasterBfile , str );
                init[14] = 1;
            }
        /* Optional:  11 Socket timeout length 
        ******************************/
        else if(k_its("SocketTimeout") ) {
             if(SenderHeartRate == 0)
               {
                SocketTimeoutLength = k_int();
               }
            }

        /* Optional Command:  Temp directory for arc files
        ******************************/
        else if(k_its("TempDir") ) {
                str = k_str();
                if(str) {
					j = strlen(str);   /* Make sure directory name has proper ending! */
					if( str[j-1] != '/' ) strcat(str, "/");
					strcpy( TempDir , str );
                }
            }

        /* Optional Command:  Save directory for arc files
        ******************************/
        else if(k_its("SaveDir") ) {
                str = k_str();
                if(str) {
					j = strlen(str);   /* Make sure directory name has proper ending! */
					if( str[j-1] != '/' ) strcat(str, "/");
					strcpy( SaveDir , str );
					save_arcs = 1;
                }
            }

        /* Optional Command:  Heartbeat Debug Flag
        ******************************/
        else if(k_its("HeartbeatDebug") ) {
                HeartbeatDebug = k_int();
            }

        /* Optional Command:  Socket Debug Flag
        ******************************/
        else if(k_its("SocketDebug") ) {
                SOCKET_ewDebug = k_int();
            }

        /* Optional Command:  General Debug Flag
        ******************************/
        else if(k_its("Debug") ) {
                Debug = k_int();
            }

        /* Unknown command
        *****************/
            else {
                fprintf( stderr, "<%s> Unknown command in <%s>.\n",
                         com, configfile );
                continue;
            }

        /* See if there were any errors processing the command
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr,
                       "Bad <%s> command in <%s>; exiting!\n",
                        com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "import_ms: ERROR, no " );
       if ( !init[0] )  fprintf( stderr, "<MyModuleId> "      );
       if ( !init[1] )  fprintf( stderr, "<RingName> "   );
       if ( !init[2] )  fprintf( stderr, "<HeartBeatInt> "     );
       if ( !init[3] )  fprintf( stderr, "<LogFile> " );
       if ( !init[4] )  fprintf( stderr, "<MaxMsgSize> "   );
       if ( !init[5] )  fprintf( stderr, "<MyAliveInt> "   );
       if ( !init[6] )  fprintf( stderr, "<MyAliveString> " );
       if ( !init[7] )  fprintf( stderr, "<SenderIpAdr> "    );
       if ( !init[8] )  fprintf( stderr, "<SenderPort> "   );
       if ( !init[9] )  fprintf( stderr, "<SenderHeartRate> "   );
       if ( !init[10] )  fprintf( stderr, "<SenderHeartText argh> "   );
       if ( !init[11] )  fprintf( stderr, "<MyMasterA> "   );
       if ( !init[12] )  fprintf( stderr, "<MyMasterB> "   );
       if ( !init[13] )  fprintf( stderr, "<MasterAfile> "   );
       if ( !init[14] )  fprintf( stderr, "<MasterBfile> "   );
       if ( !init[15] )  fprintf( stderr, "<SocketTimeout> "   );
      fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }

   return;
}

/****************************************************************************
 *  lookup( )   Look up important info from earthworm.h tables       *
 ****************************************************************************/
void lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 ) {
        fprintf( stderr,
                "import_ms:  Invalid ring name <%s>; exiting!\n", RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &MyInstId ) != 0 ) {
      fprintf( stderr,
              "import_ms: error getting local installation id; exiting!\n" );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr,
              "import_ms: Invalid module name <%s>; exiting!\n", MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr,
              "import_ms: Invalid message type <TYPE_HEARTBEAT>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr,
              "import_ms: Invalid message type <TYPE_ERROR>; exiting!\n" );
      exit( -1 );
   }
   return;
}

/*******************************************************************************
 * output_status() builds a heartbeat or error message & puts it into          *
 *                 shared memory.  Writes errors to log file & screen.         *
 *******************************************************************************/
void output_status( unsigned char type, short ierr, char *note )
{
    MSG_LOGO    logo;
    char        msg[256];
    long        size;
    time_t        t;

/* Build the message
 *******************/
    logo.instid = MyInstId;
    logo.mod    = MyModId;
    logo.type   = type;

    time( &t );

    if( type == TypeHeartBeat ) {
        sprintf( msg, "%ld %ld\n\0", t,MyPid);
    }
    else if( type == TypeError ) {
        sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
        logit( "t", "import_ms: %s\n", note );
    }

    size = strlen( msg );   /* don't include the null byte in the message */

/* Write the message to shared memory
 ************************************/
    if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK ) {
        if( type == TypeHeartBeat ) {
           logit("et","import_ms:  Error sending heartbeat.\n" );
        }
        else if( type == TypeError ) {
           logit("et","import_ms:  Error sending error:%d.\n", ierr );
        }
    }

    return;
}

/************************** import_filter *************************
 *           Decide  what to do with this message                 *
 *                                                                *
 ******************************************************************
/*      This routine is handed each incoming message, and can do what it likes
with it. The intent is that here is where installation-specificd processing
is done, such as deciding whether we want this message (e.g. is the pick from
an intersting station?), changing its format, etc.
        The default action is to assume that the incoming message was generated
by the Earthworm standard "export_generic" module, which has a companion "export-filter"
routine. The default form of that routine attaches the logo at the start of the
message and exports it. So we peel it off, and drop it into our local transport
medium under that logo.
        We assume that the first 9 characters are three three-character fields
giving IstallationId, ModuleId, and MessageType.
*/


void import_filter( char *msg, int msgLen )
{
    char cInst[4], cMod[4], cType[4];
    int  ierr;
    MSG_LOGO logo;
    
    char EvntIDS[20], filename[100], tname[100];
    int  i, j;
    FILE    *out;

    /* We attempt to find out who is now master.  If the attempt fails, we
       assume that the past master is still in charge.  The worst-case result
       of this will be downstream processing of both a master and slave version
       of an event.  Big deal.*/
    ierr = Whos_on_First(MSstring);
    if(ierr == 0) strcpy(Master, MSstring);
    if(ierr != 0 && Debug) logit("et", "Whos_on_First error: %d\n", ierr);
        
	if(Debug) {
        strncpy(cInst,msg,    3);  cInst[3]=0;
        strncpy(cMod ,&msg[3],3);  cMod[3] =0;
        strncpy(cType,&msg[6],3);  cType[3]=0;
        logit("et", "Logo out: %s %s %s\n", cInst, cMod, cType);
	}

    if(strcmp(Master, MyMasterA) == 0) {
        /* We assume that this message was created by our bretheren "export_default",
           which attaches the logo as three groups of three characters at the front of
           the message */
        /* Peel off the logo chacters */
        strncpy(cInst,msg,    3);  cInst[3]=0;  logo.instid =(unsigned char)atoi(cInst);
        strncpy(cMod ,&msg[3],3);  cMod[3] =0;  logo.mod    =(unsigned char)atoi(cMod);
        strncpy(cType,&msg[6],3);  cType[3]=0;  logo.type   =(unsigned char)atoi(cType);

        /* Write the message to shared memory or to file
        ************************************************/
        if(save_arcs) {
			for(i=136,j=0;i<146;i++) if(msg[i+9]!=' ') {j = i;break;}
			if(j) strncpy(EvntIDS, &msg[j+9], 146-j);
			EvntIDS[146-j] = 0;
        	sprintf(filename, "%s%s.arc", TempDir, EvntIDS);
			out = fopen(filename, "wb");
			if(out == 0L) {
				logit("e", "import_filter: Unable to open Arc File: %s\n", filename);    
			} else {
				msg[msgLen] = 0;
				fprintf(out, "%s", &msg[9]);
				fclose(out);
				sprintf(tname, "%sNC%s.arc", SaveDir, EvntIDS);
				ierr = rename(filename, tname); 
			}
        } else {
			if( tport_putmsg( &Region, &logo, (long)(msgLen-9), &msg[9]) != PUT_OK ) {
				logit("t","import_ms:  Error sending message via transport:\n" );
			}
        }
    }
    return;
}


/*************************** WriteToSocket ************************
 *    send a message logo and message to the socket               *
 *    returns  0 if there are no errors                           *
 *            -1 if any errors are detected                       *
 ******************************************************************/

int WriteToSocket( int ActiveSocket, char *msg, MSG_LOGO *logo )
{
    char asciilogo[11];       /* ascii version of outgoing logo */
    char startmsg = STX;       /* flag for beginning of message  */
    char endmsg   = ETX;       /* flag for end of message        */
    int  msglength;           /* total length of msg to be sent */
    int  rc;

    msglength=strlen(msg);  /* Assumes msg is null terminated */


/* Send "start of transmission" flag & ascii representation of logo
 ***********************************/
    sprintf( asciilogo, "%c%3d%3d%3d",startmsg,
           (int) logo->instid, (int) logo->mod, (int) logo->type );
    rc = send_ew( ActiveSocket, asciilogo, 10, 0, SocketTimeoutLength);
    if( rc != 10 ) {
        SocketPerror( "import_ms: socket send" );
        return( -1 );
    }

/* Debug print of message
*************************/
 /* printf("import_ms: sending under logo (%s):\n%s\n",asciilogo,msg); */

/* Send message; break it into chunks if it's big!
 *************************************************/
    rc = send_ew( ActiveSocket, msg, msglength, 0, SocketTimeoutLength );
    if ( rc == -1 ) {
        SocketPerror( "export_generic: sendall error" );
        return( -1 );
    }
/* Send "end of transmission" flag
 *********************************/
    rc = send_ew( ActiveSocket, &endmsg, 1, 0, SocketTimeoutLength);
    if( rc != 1 ) {
        SocketPerror( "import_ms: socket send" );
        return( -1 );
    }

    return( 0 );
}

/*****************************************************************************
 *  Whos_on_First_rcp( )  copies a master/slave file from a remote Solaris   *
 *  machine and uses it to determine who is master and who is slave          *
 *****************************************************************************/   

int Whos_on_First_rcp( char  *errtxt)    /* external process id to preserve re-entrancy      */
{
    char   masterAname[30], masterBname[30];
    char   subname[] = "Whos_on_First";
    char   line[256];
    FILE   *file;
    int    status, exitstat;
    pid_t  pid;

    strcpy(errtxt, "none");
    strcpy(masterAname, MyMasterA);
    strcpy(masterBname, MyMasterB);
    strcat(masterAname, ".master");
    strcat(masterBname, ".master");
   
/* Start new process to copy file
 ********************************/
    pid = fork1();
    switch( pid ) {
        case -1: /* fork failed */
            logit("e", "%s: <%s>: copy fork failed", subname, MyMasterA );
            sprintf( errtxt, "%s: <%s>: copy fork failed", subname, MyMasterA );
            perror( errtxt );
            return(1);

        case  0: /* in new child process */
            execl( "/usr/bin/rcp", "rcp", "-p", MasterAfile, masterAname, (char *) 0  );
            perror( "Whos_on_First: execl" );
            logit("e","%s: exit 1 from copy file\n", subname);
            exit(1);

        default: /* in parent */
            break;
    }

    if ( waitpid( pid, &status, 0 ) == -1 ) {
        logit("e", "%s: <%s>: waitpid error", subname, "redhot" );
        return( 2 );
    }

/* See if the child (in this case, rcp) terminated abnormally
 ************************************************************/
    if ( WIFSIGNALED(status) ) {
        logit("e","%s: <%s>: rcp terminated by signal %d\n", 
               subname, MyMasterA, WTERMSIG(status) );
        return( 3 );
    }
    else if ( WIFSTOPPED(status) ) {
        logit("e", "%s: <%s>: rcp stopped by signal %d", 
               subname, MyMasterA, WSTOPSIG(status) );      
        return( 3 );
    }
    else if ( WIFEXITED(status) ) {
        exitstat = WEXITSTATUS(status);
        if( exitstat != 0 ) {
            logit("e", "%s: <%s>: rcp exitted with status %d", 
                     subname, MyMasterA, exitstat );      
            return( 3 );
        }
    }
   
    if( (file = fopen( masterAname, "r" )) == (FILE *) NULL ) {
        logit("e",
            "%s: Cannot open master-slave file <%s>!\n", subname, masterAname);
    } else {
        fgets( line, sizeof(line), file );
        if(line[0] == 'm') strcpy(errtxt, MyMasterA);
        fclose(file);
    }
    
/* Start new process to copy file
 ********************************/
   pid = fork1();
   switch( pid ) {
      case -1: /* fork failed */
               logit("e", "%s: <%s>: copy fork failed", subname, MyMasterB );
               sprintf( errtxt, "%s: <%s>: copy fork failed", subname, MyMasterB );
               perror( errtxt );
               return(4);

      case  0: /* in new child process */
               execl( "/usr/bin/rcp", "rcp", "-p", MasterBfile, masterBname, (char *) 0  );
               perror( "Whos_on_First: execl" );
               logit("e","%s: exit 1 from copy file\n", subname);
               exit(1);
      
      default: /* in parent */
                break;
   }

   if ( waitpid( pid, &status, 0 ) == -1 ) {
      logit("e", "%s: <%s>: waitpid error", subname, MyMasterB );
      return( 5 );
   }

/* See if the child (in this case, rcp) terminated abnormally
 ************************************************************/
   if ( WIFSIGNALED(status) ) {
      logit("e", "%s: <%s>: rcp terminated by signal %d", 
               subname, MyMasterB, WTERMSIG(status) );      
      return( 6 );
   }
   else if ( WIFSTOPPED(status) ) {
      logit("e", "%s: <%s>: rcp stopped by signal %d", 
               subname, MyMasterB, WSTOPSIG(status) );      
      return( 6 );
   }
   else if ( WIFEXITED(status) ) {
      exitstat = WEXITSTATUS(status);
      if( exitstat != 0 ) {
        logit("e", "%s: <%s>: rcp exitted with status %d", 
                 subname, MyMasterB, exitstat );      
        return( 6 );
      }
   }
   
    if( (file = fopen( masterBname, "r" )) == (FILE *) NULL ) {
        logit("e",
            "%s: Cannot open master-slave file <%s>!\n", subname, masterBname);
    } else {
        fgets( line, sizeof(line), file );
        if(line[0] == 'm') strcpy(errtxt, MyMasterB);
        fclose(file);
    }
    
/* Everything went smoothly
 **************************/
   return( 0 );

}

/*****************************************************************************
 *  Whos_on_First( )  Reads master/slave files copied from remote Solaris    *
 *  machines and uses them to determine who is master and who is slave       *
 *****************************************************************************/   

int Whos_on_First( char  *errtxt)    /* external process id to preserve re-entrancy      */
{
    char   masterAname[30], masterBname[30];
    char   subname[] = "Whos_on_First";
    char   line[256];
    FILE   *file;

    strcpy(errtxt, "none");
    strcpy(masterAname, "master-slave.");
    strcpy(masterBname, "master-slave.");
    strcat(masterAname, MyMasterA);
    strcat(masterBname, MyMasterB);
   
    if( (file = fopen( masterAname, "r" )) == (FILE *) NULL ) {
        logit("e",
            "%s: Cannot open master-slave file <%s>!\n", subname, masterAname);
    } else {
        fgets( line, sizeof(line), file );
        if(line[0] == 'm') strcpy(errtxt, MyMasterA);
        fclose(file);
    }
    
   
    if( (file = fopen( masterBname, "r" )) == (FILE *) NULL ) {
        logit("e",
            "%s: Cannot open master-slave file <%s>!\n", subname, masterBname);
    } else {
        fgets( line, sizeof(line), file );
        if(line[0] == 'm') strcpy(errtxt, MyMasterB);
        fclose(file);
    }
    
/* Everything went smoothly
 **************************/
   return( 0 );

}




