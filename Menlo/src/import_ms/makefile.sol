CFLAGS = -D_REENTRANT $(GLOBALFLAGS)

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

BINARIES = import_ms.o $L/kom.o $L/getutil.o $L/logit.o $L/transport.o \
	   $L/sleep_ew.o $L/socket_ew.o $L/socket_ew_common.o \
           $L/time_ew.o $L/threads_ew.o 
          

import_ms: $(BINARIES)
	cc -o $B/import_ms $(BINARIES) -lnsl -lsocket -mt -lposix4 -lthread


# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/import_ms*
