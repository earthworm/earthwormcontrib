#!/opt/PUBperl53/bin/perl -w -I/opt/PUBperl/lib/perl

# This is the perl script which creates and drives the helicorder pages.
# There are 4 basic displays, each with its own subroutine.  Two of these
# have versions with and without javascript.  We make the assumption that
# browsers with version numbers >= 4 have javascript.
# 

# Define a bunch of global variables for consistency and ease of modification.

$rootdir = "/webd1/ncweb";
$other   = "http://ncweb-menlo";
$other   = " ";
$self  = "helicorder.pl";
$webdir = "/waveforms/helicorder";
$stationmap = "/helicorders/Station_map.gif";
$selectormap = "/helicorders/Data_selector_map.gif";
$infodir = "$other"."/helicorders";
$gifdir = "$rootdir"."$webdir";
$gwid  = "150";					# Width and height of thumbnails
$ghgt  = "300";
$gbwid  = "850";				# Width and height of drum recorder plots
$gbhgt  = "1613";
$numpergroup = 4;
$pagecolor = "#ffffcc";			# Colors of various page elements
$bannercolor = "#669966";
$bordercolor = "#ffebcd";
$bannertxtcolor = "#ffffff";
$timezone = " PDT";				# Time zone info
$localstart = 17;
$gmtstart = 0;

# Common links for page headers
$mouseout = "onMouseOut=\"window.status=''; return true;\"";
$tag5 = "<br>";
$tag6 = " <A HREF=\"/cgi-bin/$self\" onMouseOver=\"window.status='Select Different Data'; return true;\"	$mouseout> Data Selector </A> || ";
$onclick = "onClick=\"var new_window = window.open('$stationmap','mapwindow', 'width=400,height=400,resizable'); new_window.focus();\"";
$tag7 = " <A HREF=\"#\" onMouseOver=\"window.status='View Station Map'; return true;\"	$mouseout $onclick> Station Map </A> || ";
$onclick = "onClick=\"var new_window = window.open('/recenteqs/Quakes/quakes0.html','listwindow', 'width=400,height=400,resizable,scrollbars'); new_window.focus();\"";
$tag8 = " <A HREF=\"#\" onMouseOver=\"window.status='Show List of Recent Earthquakes'; return true;\"	$mouseout $onclick> List of Recent Earthquakes </A> ||  ";
$onclick = "onClick=\"var new_window = window.open('/recenteqs/','recentmapwindow', 'width=400,height=400,resizable,scrollbars'); new_window.focus();\"";
$tag9 = " <A HREF=\"#\" onMouseOver=\"window.status='View Recent Earthquake Map'; return true;\"	$mouseout $onclick> Index Map of Recent Earthquakes </A> <br> ";

# This is the default comment list sent by heli1 on startup
# Of form : JSF_VDZ_NC. At the Stanford dish.
	if(open(NAMES, "$gifdir/znamelist.dat")) {
	 	while(<NAMES>) {
			chomp;
			($sitename = $_) =~ s/([^.]*)\.([^.]*).*/$1/;
			($siteid = $_) =~ s/([^.]*)\.([^.]*).*/$2/;
			$longname{$sitename} = $siteid;
	 	}
		close(NAMES);
	}

# This is an optional custom comment list placed by the user.
	if(open(NAMES, "$gifdir/zznamelist.dat")) {
	 	while(<NAMES>) {
			chomp;
			($sitename = $_) =~ s/([^.]*)\.([^.]*).*/$1/;
			($siteid = $_) =~ s/([^.]*)\.([^.]*).*/$2/;
			$longname{$sitename} = $siteid;
	 	}
		close(NAMES);
	}

# Get some of the basic file information needed by all subroutines.
# Get the lists of available sites and datetimes
# Of Form: nc.JSF_VDZ_NC_00.2000051500.gif

	$list = `ls $gifdir *.gif`;
	@list = split (' ',$list);
	@reverselist = reverse(@list);
	@site = ();
	$numsites = 0;
	@dates = ();
	$numdates = 0;
	foreach $item (@list) {
		($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
		($filedate = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3/;
		($suffix = $item) =~ s/([^.]*)\.([^.]*)\.([^.]*)\.([^.]*).*/$4/;
		if($suffix eq "gif") {
			$flag = 1;
			foreach $name (@site) {
				if($sitename eq $name) { $flag = 0; }
			}
		    if($flag) { @site = (@site,$sitename); $numsites = $numsites + 1; }
			$flag = 1;
			foreach $idate (@dates) {
				if($filedate eq $idate) { $flag = 0; }
			}
		    if($flag) { @dates = (@dates,$filedate);  $numdates = $numdates + 1; }
		}
	}
	@dates = sort(@dates);
	@reversedates = reverse(@dates);

# Find the client's browser version so we know whether or not
# to send Javascript.
	$JavaOK = 1;       # Assume we can use javascript for time
	$ua = $ENV{'HTTP_USER_AGENT'};
	($bv,$rest) = split ('\(',$ua);
	if($bv =~ /\//) {
		($browser,$versionpt) = split ('/',$bv);
	}
	else {
		$versionpt = 3;
	}
	if($rest =~ /MSIE/) {$JavaOK = 0; }

	$version = $versionpt;
	if($versionpt =~ /\s/) {
		($version,$rest) = split ('\s',$versionpt);
	}
	if($version < 4) {$JavaOK = 0; }

# Check the query string for further instructions.
#
	$qname = "NONE";
	$query_string = $ENV{'QUERY_STRING'};

	if ( ! defined($query_string) || $query_string eq "") {
	   if($version >= 4) { &make_map; }
	   else 			 { &make_index; }
	}
	else {
		$group = "$query_string";
		($querytype,$qvalue,$qdate) = split ('&',$query_string);
		
		$i = 0;
		$j = 0;
		foreach $name (@site) {
			$i = $i + 1;
			if($name =~ $qvalue) {
				$qname = $name;
				$qvalue = $i;
				$j = 1;
				last;
			}
		}
		if($j == 0 && $qvalue != 0) {
			$querytype = 5;
		}
		
	   if($querytype eq 1) { &show_site_thumbs; }
	   if($querytype eq 2) { &show_date_thumbs; }
	   if($querytype eq 3) { 
		   if($JavaOK == 1) { 
					&show_single_panelx; }
		   else { 	&show_single_panel;  }
	   }
	   if($querytype eq 4) { &make_index; }
	   if($querytype eq 5) { 
		   if($version >= 4) { &make_map; }
		   else 			 { &make_index; }
	   }
	}

exit;


########################################################################
sub make_index {
########################################################################

	$banner1 = "Welcome to the NCSN Drum Recorder Display";
	$banner2 = "Real-time Views of Selected Seismograms";

	$tag1 = " ";
	$tag2 = " ";
	$tag3 = " ";
	$tag4 = " ";
	$tag5 = " ";
	$tag8 = " <A HREF=\"./heliLV.pl\" onMouseOver=\"window.status='View Seismograms from Long Valley, CA'; return true;\"	$mouseout> Long Valley Seismograms </A>  ";

	&Build_Banner;
	
	if($version < 4) {
		print STDOUT "<center><H2><P> \n";
		print STDOUT "Please update your browser to experience the full capabilities of these pages. <br>\n";
		print STDOUT "<A HREF=\"http://home.netscape.com/computing/download/index.html\" onMouseOver=\"window.status='Download new browser from Netscape'; return true;\"	$mouseout> Netscape </A> \n";
		print STDOUT "&nbsp &nbsp \n";
		print STDOUT "<A HREF=\"http://microsoft.com\" onMouseOver=\"window.status='Download new browser from Microsoft'; return true;\"	$mouseout> Explorer </A> \n";
		print STDOUT "</H2><HR></center> \n";
	}
	
	print STDOUT "<P><h4>\n";
		print STDOUT "The records shown below are recorded in real-time in Menlo Park, Ca. \n";
		print STDOUT "They are each updated on this page every five minutes. \n";
		print STDOUT "Each panel represents 24 hours of data from one station. \n";
		print STDOUT "The file sizes are approximately 100-200 kbytes.\n";
	print STDOUT "<br></h4>\n";
	
	$i = 0;
	foreach $name (@site) {
		$i = $i + 1;
		$lname = $longname{$name};
		($sta,$comp,$net) = split ('_',$name);
		$inq = "1&$name&0";
		$wstatus = "View Thumbnails of $sta $comp $net for All Available Days";
		print STDOUT "<P><HR><P><h4>     ";
		print STDOUT "  $sta $comp $net \n";
		if($lname) {print STDOUT " ( $lname )  ";}
		print STDOUT " [<A HREF=\"/cgi-bin/$self\?$inq\" \n";
		print STDOUT "  onMouseOver=\"window.status='$wstatus'; return true;\" \n";
		print STDOUT "  onMouseOut=\"window.status=''; return true;\"> \n";
		print STDOUT " <b>Previews</b></A>]<br>\n";
		print STDOUT "</h4><P>\n";
		foreach $item (@reverselist) {
			($suffix = $item) =~ s/([^.]*)\.([^.]*)\.([^.]*)\.([^.]*).*/$4/;
			if($suffix eq "gif") {
				($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
				if($sitename eq $name) {
					($datetime = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3_$4/;
					($date,$hr) = split ('_',$datetime);
					($datepart = $date) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
					($year,$mon,$day) = split ('_',$datepart);
					$inq = "3&$name&$date";
					$wstatus = "View Data for $sta $comp $net on $mon/$day/$year";
					print STDOUT "  | <A HREF=\"/cgi-bin/$self\?$inq\"  \n";
					print STDOUT "  onMouseOver=\"window.status='$wstatus'; return true;\" \n";
					print STDOUT "  onMouseOut=\"window.status=''; return true;\"> \n";
					print STDOUT " <B>$mon/$day/$year</B> </A> | \n";
				}
			}
		}
	}
	
	print STDOUT "<P><h4><HR>Previews of all stations are available for the following dates:</h4><BR>\n";
	foreach $item (@reversedates) {
		($datepart = $item) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
		($year,$mon,$day) = split ('_',$datepart);
		$inq = "2&0&$item";
		$wstatus = "View Thumbnails of All Stations for $mon/$day/$year";
		print STDOUT " | <A HREF=\"/cgi-bin/$self\?$inq\"  \n";
		print STDOUT "  onMouseOver=\"window.status='$wstatus'; return true;\" \n";
		print STDOUT "  onMouseOut=\"window.status=''; return true;\"> \n";
		print STDOUT "  <B>$mon/$day/$year</B></A> | \n";
	}

return;

}

########################################################################
# Display thumbnails of all spectrograms for one site
# querytype = 1
# $qvalue is index to array of site names
########################################################################
sub show_site_thumbs {

	$name = $site[$qvalue-1];
	$lname = $longname{$qname};
	($sta,$comp,$net) = split ('_',$qname);
	if($lname) {
		$banner1 = $qname;
		$banner1 = "$sta $comp $net";
		$banner2 = " ( $lname )  ";
	} else {
		$banner1 = "Northern California Seismic Network";
		$banner2 = $qname;
		$banner2 = "$sta $comp $net";
	}
	
	$count = 0;
	foreach $item (@reverselist) {
		($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
		if($sitename eq $qname) {
			($datetime = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3_$4/;
			($date,$hr) = split ('_',$datetime);
		
			($datepart = $date) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
			$displayitem[$count] = $item; 
			$displaydate[$count] = $datepart; 
			$filedate[$count] = $date; 
			$count = $count + 1;
		}
	}
	
# Tags to go elsewhere
	
	$psite = $qvalue - 1;
	if($psite < 1) { $psite = $numsites; }
	$pname = $site[$psite-1];
	($sta,$comp,$net) = split ('_',$pname);
	$inq = "1&$pname&0";
		$tag1 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='All Data for $sta $comp $net'; return true;\" $mouseout>Previous Site ($sta $comp $net)</A> || ";
	
	$nsite = $qvalue + 1;
	if($nsite > $numsites) { $nsite = 1; }
	$nname = $site[$nsite-1];
	($sta,$comp,$net) = split ('_',$nname);
	$inq = "1&$nname&0";
		$tag2 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='All Data for $sta $comp $net'; return true;\" $mouseout> Next Site ($sta $comp $net)</A> || ";
	
	$tag3 = " ";
	$tag4 = " ";

	&Build_Banner;
	
	print STDOUT "<P><h4>\n";
		print STDOUT "The records shown below are recorded in real-time in Menlo Park, Ca. \n";
		print STDOUT "They are each updated on this page every five minutes. \n";
		print STDOUT "Each panel represents 24 hours of data from one station. \n";
		print STDOUT "The file sizes are approximately 100-200 kbytes.\n";
	print STDOUT "<br></h4>\n";
	
	print STDOUT "<center><P><h4><FONT COLOR=red>\n";
		print STDOUT "Click on a seismogram for full size display.\n";
	print STDOUT "</FONT></h4></center>\n";

	print STDOUT "<HR>";
	print STDOUT "<center>";
	print STDOUT " <table width=\"75\%\" bordercolorlight=$pagecolor bordercolordark=$pagecolor>\n";
	$j = $count/4;
	for($jj=0; $jj<$j; $jj++) {
		$kk = $count - $jj*4;
		if($kk > 4) {
			$kk = 4;
		}
		print STDOUT "<tr>\n";
		for($i=0; $i<$kk;$i++) {
			$ii = $jj*4 + $i;
				($year,$mon,$day) = split ('_',$displaydate[$ii]);
				print STDOUT "  <td><center>\n <B>$mon/$day/$year</B> \n </center></td> \n";
		}
		print STDOUT "</tr>\n";
		print STDOUT "<tr>\n";
		for($i=0; $i<$kk;$i++) {
			$ii = $jj*4 + $i;
			$inq = "3&$qname&$filedate[$ii]";
			($year,$mon,$day) = split ('_',$displaydate[$ii]);
			$wstatus = "$qname on $mon/$day/$year";
			print STDOUT "<td><center>\n <A HREF=\"/cgi-bin/$self\?$inq\" \n ";
			print STDOUT "  onMouseOver=\"window.status='$wstatus'; return true;\" \n";
			print STDOUT "  onMouseOut=\"window.status=''; return true;\"> \n";
			print STDOUT "  <IMG SRC=\"$other${webdir}/$displayitem[$ii]\" WIDTH=$gwid HEIGHT=$ghgt></A></center></td>\n ";
		}
		print STDOUT "</tr>\n";
		print STDOUT "<tr>\n";
		print STDOUT " <td>&nbsp;</td>\n <td>&nbsp;</td>\n <td>&nbsp;</td>\n <td>&nbsp;</td>\n";
		print STDOUT "</tr>\n";
	}
	print STDOUT "</table>\n";

	print STDOUT "</center> \n ";
	
	print STDOUT "<center><h4> \n $tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8 $tag9 </h4></center>\n";

	print STDOUT " <P> <HR> <P> </body> </html> \n";


return;

}

			
########################################################################
# Display thumbnails of spectrograms for all sites for one day
# querytype = 2
# $qdate of form YYYYMMDD - same as in file name
########################################################################
sub show_date_thumbs {

	@t0 = date2secs();

	$delhr = -24;
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
	$monplus = $mon + 1;
	$year = $year + 1900;
	$prev = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;

	$delhr = 24;
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
	$monplus = $mon + 1;
	$year = $year + 1900;
	$next = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;
	
	$date = $qdate;
	$day = substr ($date,6,2);
	$mon = substr ($date,4,2);
	$year = substr ($date,0,4);
	$banner1 = "Northern California Seismic Network";
	$banner2 = "Drum Recorder Displays for $mon/$day/$year";
	
# Tags to go elsewhere
	
	$flagp = 1;
	$flagn = 1;
	foreach $item (@dates) {
		if($prev eq $item) { $flagp = 0; }
		if($next eq $item) { $flagn = 0; }
	}
	if($flagp) { $tag1 = " Previous Day || "; }
	else {	
		$day = substr ($prev,6,2);
		$mon = substr ($prev,4,2);
		$year = substr ($prev,0,4);
		$inq = "2&0&$prev";
		$tag1 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='All Data for $mon/$day/$year'; return true;\" $mouseout>Previous Day</A> || ";
	}
	
	if($flagn) { $tag2 = " Next Day || "; }
	else {	
		$day = substr ($next,6,2);
		$mon = substr ($next,4,2);
		$year = substr ($next,0,4);
		$inq = "2&0&$next";
		$tag2 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='All Data for $mon/$day/$year'; return true;\" $mouseout>Next Day</A> || ";
	}
	$tag3 = " ";
	$tag4 = " ";

	&Build_Banner;
	
	print STDOUT "<P><h4>\n";
		print STDOUT "The records shown below are recorded in real-time in Menlo Park, Ca. \n";
		print STDOUT "They are each updated on this page every five minutes. \n";
		print STDOUT "Each panel represents 24 hours of data from one station. \n";
		print STDOUT "The file sizes are approximately 100-200 kbytes.\n";
	print STDOUT "<br></h4>\n";
	
	print STDOUT "<center><P><h4><FONT COLOR=red>\n";
		print STDOUT "Click on a seismogram for full size display.\n";
	print STDOUT "</FONT></h4></center>\n";

	$count = 0;
	foreach $item (@list) {
		($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
		($filedate = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3/;
		($suffix = $item)   =~ s/([^.]*)\.([^.]*)\.([^.]*)\.([^.]*).*/$4/;
		if($suffix eq "gif") {
			if($filedate eq $qdate) {
				$displayitem[$count] = $item; 
				$displayname[$count] = $sitename; 
				$count = $count + 1;
			}
		}
	}

	$date = $qdate;
	$day = substr ($date,6,2);
	$mon = substr ($date,4,2);
	$year = substr ($date,0,4);
	print STDOUT "<HR>";
	print STDOUT "<center>";
	print STDOUT " <table width=\"75\%\" bordercolorlight=$pagecolor bordercolordark=$pagecolor>\n";
	$j = $count/4;
	for($jj=0; $jj<$j; $jj++) {
		$kk = $count - $jj*4;
		if($kk > 4) {
			$kk = 4;
		}
		print STDOUT "<tr>\n";
		for($i=0; $i<$kk;$i++) {
			$ii = $jj*4 + $i;
			($sta,$comp,$net) = split ('_',$displayname[$ii]);
			print STDOUT "  <td><center>\n <B>$sta $comp $net</B> \n </center></td> \n";
		}
		print STDOUT "</tr>\n";
		print STDOUT "<tr>\n";
		for($i=0; $i<$kk;$i++) {
			$ii = $jj*4 + $i;
			$iii = $ii + 1;
			($sta,$comp,$net) = split ('_',$displayname[$ii]);
			$inq = "3&$displayname[$ii]&$qdate";
			$wstatus = "$sta $comp $net on $mon/$day/$year";
			print STDOUT "<td><center>\n <A HREF=\"/cgi-bin/$self\?$inq\" \n ";
			print STDOUT "  onMouseOver=\"window.status='$wstatus'; return true;\" \n";
			print STDOUT "  onMouseOut=\"window.status=''; return true;\"> \n";
			print STDOUT "  <IMG SRC=\"$other${webdir}/$displayitem[$ii]\" WIDTH=$gwid HEIGHT=$ghgt></A></center></td>\n ";
		}
		print STDOUT "</tr>\n";
		print STDOUT "<tr>\n";
		print STDOUT " <td>&nbsp;</td>\n <td>&nbsp;</td>\n <td>&nbsp;</td>\n <td>&nbsp;</td>\n";
		print STDOUT "</tr>\n";
	}
	print STDOUT "</table>\n";

	print STDOUT "</td></center> \n ";
	
	print STDOUT "<center><h4> \n $tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8 $tag9 </h4></center>\n";

	print STDOUT " <P> <HR> <P> </body> </html> \n";

	
return;

}

########################################################################
# Display a Drum Recorder for one day
# querytype = 3
# $qvalue  is index to array of site names
# $qdate of form YYYYMMDD - same as in file name
########################################################################
sub show_single_panel {

# Get the previous and next days.

	@t0 = date2secs();

	$delhr = -24;
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
	$monplus = $mon + 1;
	$year = $year + 1900;
	$prev = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;

	$delhr = 24;
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
	$monplus = $mon + 1;
	$year = $year + 1900;
	$next = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;

# Get the previous and next sites.

	$i = 0;
	$count = 0;
	foreach $name (@site) {
		$i = $i + 1;
		if($i eq $qvalue) { 
			foreach $item (@reverselist) {
				($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
				if($sitename eq $name) {
		            ($filedate = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3/;
					($datetime = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3_$4/;
					($date,$hr) = split ('_',$datetime);
				
					($datepart = $date) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
					$displayitem[$count] = $item; 
					$displaydate[$count] = $datepart; 
					$count = $count + 1;
					if($filedate eq $qdate) {
					    $currentitem = $item;
					    $currentname = $name;
					}
				}
			}
			last; 
		}
	}
	$banner1 = "Drum Recorder Display for $currentname  ";
	$banner2 = $longname{$currentname};
	$lname = $longname{$qname};
	($sta,$comp,$net) = split ('_',$qname);
	if($lname) {
		$banner1 = $qname;
		$banner1 = "$sta $comp $net";
		$banner2 = " ( $lname )  ";
	} else {
		$banner1 = "Northern California Seismic Network";
		$banner2 = $qname;
		$banner2 = "$sta $comp $net";
	}

# Tags to go elsewhere
	
	$flagp = 1;
	$flagn = 1;
	foreach $item (@dates) {
		if($prev eq $item) { $flagp = 0; }
		if($next eq $item) { $flagn = 0; }
	}
	$tsite = $qvalue;
	if($flagp) { $tag1 = " Previous Day || \n"; }
	else {	
		$day = substr ($prev,6,2);
		$mon = substr ($prev,4,2);
		$year = substr ($prev,0,4);
		$inq = "3&$qname&$prev";
		$tag1 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='Data for $qname on $mon/$day/$year'; return true;\" $mouseout>Previous Day</A> || ";
	}
	
	if($flagn) { $tag2 = " Next Day || \n"; }
	else {	
		$day = substr ($next,6,2);
		$mon = substr ($next,4,2);
		$year = substr ($next,0,4);
		$inq = "3&$qname&$next";
		$tag2 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='Data for $qname on $mon/$day/$year'; return true;\" $mouseout>Next Day</A> || ";
	}

	$day = substr ($qdate,6,2);
	$mon = substr ($qdate,4,2);
	$year = substr ($qdate,0,4);
	$psite = $qvalue - 1;
	if($psite < 1) { $psite = $numsites; }
	$pname = $site[$psite-1];
	($sta,$comp,$net) = split ('_',$pname);
	$inq = "3&$pname&$qdate";
	$tag3 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='Data for $sta $comp $net on $mon/$day/$year'; return true;\" $mouseout>Previous Site ($sta $comp $net)</A> || ";
	
	$nsite = $qvalue + 1;
	if($nsite > $numsites) { $nsite = 1; }
	$nname = $site[$nsite-1];
	($sta,$comp,$net) = split ('_',$nname);
	$inq = "3&$nname&$qdate";
	$tag4 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='Data for $sta $comp $net on $mon/$day/$year'; return true;\" $mouseout>Next Site ($sta $comp $net)</A>  ";
	
	&Build_Banner;
	
	print STDOUT "<center><td>";
	print STDOUT "<A name=\"anchor1\"   \n>";
	print STDOUT "<IMG SRC=\"$other${webdir}/$currentitem\" WIDTH=$gbwid HEIGHT=$gbhgt>  \n";
	print STDOUT "</a>";

	print STDOUT "</td></center> \n ";
	
	print STDOUT "<center><h4> \n $tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8 $tag9 </h4></center>\n";
	
	print STDOUT "<center><h5> \n This page best viewed with Navigator 4.0 or later. </h5></center>\n";

	print STDOUT " <P> <HR> <P> </body> </html> \n";


return;

}

########################################################################
# Display a Drum Recorder for one day
# Javascript version for displaying time in status bar.
# This only works with Navigater
# querytype = 3
# $qvalue  is index to array of site names
# $qdate of form YYYYMMDD - same as in file name
########################################################################
sub show_single_panelx {

# Get the previous and next days.

	@t0 = date2secs();

	$delhr = -24;
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
	$monplus = $mon + 1;
	$year = $year + 1900;
	$prev = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;

	$delhr = 24;
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
	$monplus = $mon + 1;
	$year = $year + 1900;
	$next = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;

# Get the previous and next sites.

	$i = 0;
	$count = 0;
	foreach $name (@site) {
		$i = $i + 1;
		if($i eq $qvalue) { 
			foreach $item (@reverselist) {
				($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
				if($sitename eq $name) {
		            ($filedate = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3/;
					($datetime = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3_$4/;
					($date,$hr) = split ('_',$datetime);
				
					($datepart = $date) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
					$displayitem[$count] = $item; 
					$displaydate[$count] = $datepart; 
					$count = $count + 1;
					if($filedate eq $qdate) {
					    $currentitem = $item;
					    $currentname = $name;
					}
				}
			}
			last; 
		}
	}
	$banner1 = "Drum Recorder Display for $currentname  ";
	$banner2 = $longname{$currentname};
	$lname = $longname{$qname};
	($sta,$comp,$net) = split ('_',$qname);
	if($lname) {
		$banner1 = $qname;
		$banner1 = "$sta $comp $net";
		$banner2 = " ( $lname )  ";
	} else {
		$banner1 = "Northern California Seismic Network";
		$banner2 = $qname;
		$banner2 = "$sta $comp $net";
	}

# Tags to go elsewhere
	
	$flagp = 1;
	$flagn = 1;
	foreach $item (@dates) {
		if($prev eq $item) { $flagp = 0; }
		if($next eq $item) { $flagn = 0; }
	}
	$tsite = $qvalue;
	if($flagp) { $tag1 = " Previous Day || \n"; }
	else {	
		$day = substr ($prev,6,2);
		$mon = substr ($prev,4,2);
		$year = substr ($prev,0,4);
		$inq = "3&$qname&$prev";
		$tag1 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='Data for $qname on $mon/$day/$year'; return true;\" $mouseout>Previous Day</A> || ";
	}
	
	if($flagn) { $tag2 = " Next Day || \n"; }
	else {	
		$day = substr ($next,6,2);
		$mon = substr ($next,4,2);
		$year = substr ($next,0,4);
		$inq = "3&$qname&$next";
		$tag2 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='Data for $qname on $mon/$day/$year'; return true;\" $mouseout>Next Day</A> || ";
	}

	$day = substr ($qdate,6,2);
	$mon = substr ($qdate,4,2);
	$year = substr ($qdate,0,4);
	$psite = $qvalue - 1;
	if($psite < 1) { $psite = $numsites; }
	$pname = $site[$psite-1];
	($sta,$comp,$net) = split ('_',$pname);
	$inq = "3&$pname&$qdate";
	$tag3 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='Data for $sta $comp $net on $mon/$day/$year'; return true;\" $mouseout>Previous Site ($sta $comp $net)</A> || ";
	
	$nsite = $qvalue + 1;
	if($nsite > $numsites) { $nsite = 1; }
	$nname = $site[$nsite-1];
	($sta,$comp,$net) = split ('_',$nname);
	$inq = "3&$nname&$qdate";
	$tag4 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='Data for $sta $comp $net on $mon/$day/$year'; return true;\" $mouseout>Next Site ($sta $comp $net)</A>  ";
	
#	&Build_Banner;

print STDOUT <<EOT;
	Content-type: text/html

	<HTML>
	<HEAD><TITLE>
	Northern California Seismic Network - Drum Recorders
	</TITLE>
	

	<SCRIPT language="JavaScript">

		<!--

// Global flags
var inData

		function printTime(evt)
		{
			var xglobal, yglobal;
			var xoffset, yoffset;
			xoffset = document.anchors[0].x + 51;
			yoffset = document.anchors[0].y + 95;
			xglobal = evt.pageX;
			yglobal = evt.pageY;
			var xpos, ypos;
            xpos = xglobal - xoffset;
            ypos = yglobal - yoffset;
			var xmax, ymax;
			xmax = 15*48;
			ymax = 96*15;
            if(xpos >= 0 && xpos < xmax && ypos >= 0 && ypos < ymax) {
				inData = 1
				var the_lines = 4 + ypos/15;
				var lines = parseInt(the_lines, 10) - 4;
				var the_hour = the_lines/4;
				var hour = parseInt(the_hour, 10) - 1;
				var localhour = hour + 24 + $localstart;
				var gmthour = hour + 24 + $gmtstart;
				var localtime = " $timezone";
				var quart = 15*(lines - hour*4);
				var the_minute = 1 + quart + xpos/48;
				var min = parseInt(the_minute, 10) - 1;
				var the_sec = xpos/48 - parseInt(xpos/48+1) + 1;
				the_sec = the_sec*60 + 1;
				var sec = parseInt(the_sec) - 1
				if(hour < 10) hour = "0" + hour;
				if(min  < 10) min  = "0" + min;
				if(sec  < 10) sec  = "0" + sec;
				if(localhour >= 24) localhour = localhour - 24;
				if(localhour >= 24) localhour = localhour - 24;
				if(localhour <  10) localhour = "0" + localhour;
				if(gmthour >= 24) gmthour = gmthour - 24;
				if(gmthour >= 24) gmthour = gmthour - 24;
				if(gmthour <  10) gmthour = "0" + gmthour;
				window.status =  gmthour  + ":" + min  + ":" + sec  + " UTC    " + localhour  + ":" + min  + ":" + sec + localtime;
//				window.status =  hour  + ":" + min + " UTC  " + localhour  + ":" + min + localtime;
                return false
			}
			else {
				if(inData == 1) {
					inData = 0;
//					window.status =  "Click for Time";
				}
                return true
			}
		}

// Assign event handlers
function init() {
        document.captureEvents( Event.MOUSEMOVE )
        document.onmousemove = printTime
}

		//-->

	</SCRIPT>
	
	</HEAD>
	<BODY BGCOLOR=$pagecolor TEXT=#000000 vlink=purple link=blue onLoad="init()">

	<img align=left src="/ICONS/ID348.gif" alt="USGS"> 

	<table>
	  <tr> 
	    <td> <a href="$other/QUAKES/CURRENT/current.html">Latest quake info</a> </td>
	    <td> <a href="$other/hazprep/">Hazards & Preparedness</a> </td>
	    <td> <a href="$other/more/">More about earthquakes</a> </td>
	  </tr>
	  <tr> 
	    <td> <a href="$other/study/">Studying Earthquakes</a> </td>
	    <td> <a href="$other/WhatsNew/">What's new</a> </td>
	    <td> <a href="/">Home</a> </td>
	  </tr>
	</table>

	<p>&nbsp;</p>
	<a href="$infodir/index.html"
	onMouseOver="window.status='Description of the Drum Recorder Data'; return true;"
	onMouseOut="window.status=''; return true;">
	About the Drum Recorders</a> 

	<dl></dl>

	<table width="75%" border="1" align="center" bgcolor=$bannercolor bordercolordark="#000000">
	  <tr bgcolor=$bannercolor> 
	    <td height="47"> 
	      <center>
	        <font face="Times New Roman, Times, serif" size="6" color=$bannertxtcolor>
	        	$banner1
	        </font> </center>
	    </td>
	  </tr>
	  <tr bgcolor=$bannercolor> 
	    <td height="34"> 
	      <center>
	        <font face="Arial"><font size="4" color=$bannertxtcolor face="Times New Roman, Times, serif">
				$banner2
	        </font></font> </center>
	    </td>
	  </tr>
	</table>

	<center><h4>
	$tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8 $tag9
	</h4></center>
	<P> <HR> <P>
	
EOT
	
	print STDOUT "<center> <h4><FONT COLOR=red>";
	print STDOUT "Move the cursor over the seismogram to read time!";
	print STDOUT "</FONT></h4> </center>";
	
	print STDOUT "<center><td>";
	print STDOUT "<A name=\"anchor1\"   \n>";
	
	print STDOUT "<IMG SRC=\"$other${webdir}/$currentitem\" WIDTH=$gbwid HEIGHT=$gbhgt>  \n";
	print STDOUT "</a>";


	print STDOUT "</td></center> \n ";
	
	print STDOUT "<center><h4> \n $tag1 \n $tag2 \n $tag3 \n $tag4 \n $tag5 \n $tag6 \n $tag7 \n $tag8 \n $tag9 </h4></center>\n";

	print STDOUT " <P> <HR> <P> </body> </html> \n";

return;

}

########################################################################
# Display a Drum Recorder for one day
# Javascript version
# querytype = 3
# $qvalue  is index to array of site names
# $qdate of form YYYYMMDD - same as in file name
########################################################################
sub show_single_panely {

# Get the previous and next days.

	@t0 = date2secs();

	$delhr = -24;
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
	$monplus = $mon + 1;
	$year = $year + 1900;
	$prev = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;

	$delhr = 24;
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = hr_increment (@t0,$delhr);
	$monplus = $mon + 1;
	$year = $year + 1900;
	$next = sprintf "%4.4d%2.2d%2.2d", $year,$monplus,$mday;

# Get the previous and next sites.

	$i = 0;
	$count = 0;
	foreach $name (@site) {
		$i = $i + 1;
		if($i eq $qvalue) { 
			foreach $item (@reverselist) {
				($sitename = $item) =~ s/([^.]*)\.([^.]*).*/$2/;
				if($sitename eq $name) {
		            ($filedate = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3/;
					($datetime = $item) =~ s/([^.]*)\.([^.]*)\.(\d{8})(\d{2}).*\..*/$3_$4/;
					($date,$hr) = split ('_',$datetime);
				
					($datepart = $date) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
					$displayitem[$count] = $item; 
					$displaydate[$count] = $datepart; 
					$count = $count + 1;
					if($filedate eq $qdate) {
					    $currentitem = $item;
					    $currentname = $name;
					}
				}
			}
			last; 
		}
	}
	$banner1 = "Drum Recorder Display for $currentname  ";
	$banner2 = $longname{$currentname};
	$lname = $longname{$qname};
	($site,$comp,$net) = split ('_',$qname);
	if($lname) {
		$banner1 = $qname;
		$banner1 = "$site $comp $net";
		$banner2 = " ( $lname )  ";
	} else {
		$banner1 = "Northern California Seismic Network";
		$banner2 = $qname;
		$banner2 = "$site $comp $net";
	}

# Tags to go elsewhere
	
	$flagp = 1;
	$flagn = 1;
	foreach $item (@dates) {
		if($prev eq $item) { $flagp = 0; }
		if($next eq $item) { $flagn = 0; }
	}
	$tsite = $qvalue;
	if($flagp) { $tag1 = " Previous Day || \n"; }
	else {	
		$day = substr ($prev,6,2);
		$mon = substr ($prev,4,2);
		$year = substr ($prev,0,4);
		$inq = "3&$qname&$prev";
		$tag1 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='Data for $qname on $mon/$day/$year'; return true;\" $mouseout>Previous Day</A> || ";
	}
	
	if($flagn) { $tag2 = " Next Day || \n"; }
	else {	
		$day = substr ($next,6,2);
		$mon = substr ($next,4,2);
		$year = substr ($next,0,4);
		$inq = "3&$qname&$next";
		$tag2 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='Data for $qname on $mon/$day/$year'; return true;\" $mouseout>Next Day</A> || ";
	}

	$day = substr ($qdate,6,2);
	$mon = substr ($qdate,4,2);
	$year = substr ($qdate,0,4);
	$psite = $qvalue - 1;
	if($psite < 1) { $psite = $numsites; }
	$pname = $site[$psite-1];
	($site,$comp,$net) = split ('_',$pname);
	$inq = "3&$pname&$qdate";
	$tag3 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='Data for $pname on $mon/$day/$year'; return true;\" $mouseout>Previous Site ($pname)</A> || ";
	
	$nsite = $qvalue + 1;
	if($nsite > $numsites) { $nsite = 1; }
	$nname = $site[$nsite-1];
	($site,$comp,$net) = split ('_',$nname);
	$inq = "3&$nname&$qdate";
	$tag4 = " <A HREF=\"/cgi-bin/$self\?$inq\" onMouseOver=\"window.status='Data for $nname on $mon/$day/$year'; return true;\" $mouseout>Next Site ($nname)</A>  ";
	
#	&Build_Banner;

print STDOUT <<EOT;
	Content-type: text/html

	<HTML>
	<HEAD><TITLE>
	Northern California Seismic Network - Drum Recorders
	</TITLE>
	

	<SCRIPT language="JavaScript">

		<!--


		function statusBar(stationid)
		{
			window.status =  "Click for Time";
		}

		function newstatusBar(stationid)
		{
				var xglobal, yglobal;
				var xoffset, yoffset;
				var xanchor, yanchor;
				xanchor = document.anchors[0].x;
				yanchor = document.anchors[0].y;
				xoffset = document.anchors[0].x + 53;
				yoffset = document.anchors[0].y + 95;
                xglobal = window.event.x;
                yglobal = window.event.y;
			var xpos, ypos;
	                xpos = xglobal - xoffset;
	                ypos = yglobal - yoffset;
			var xmax, ymax;
			xmax = 15*48;
			ymax = 96*15;
			var the_lines = 4 + ypos/15;
			var lines = parseInt(the_lines, 10) - 4;
			var the_hour = the_lines/4;
			var hour = parseInt(the_hour, 10) - 1;
			var localhour = hour + 24 - $hoursfromgmt;
			var localtime = " $timezone";
			var quart = 15*(lines - hour*4);
			var the_minute = 1 + quart + xpos/48;
			var min = parseInt(the_minute, 10) - 1;
			if(hour < 10) hour = "0" + hour;
			if(min < 10) min = "0" + min;
			if(localhour >= 24) localhour = localhour - 24;
			if(localhour < 10) localhour = "0" + localhour;
			window.status =  hour  + ":" + min + " UTC  " + localhour  + ":" + min + localtime;
//			window.status =  xpos  + "  " + ypos + "  " + xglobal  + "  " + yglobal + "  " + xanchor  + "  " + yanchor;
		}


		if(!window.event && window.captureEvents) {
		// set up event capturing for mouse events (add or subtract as desired)
			window.captureEvents(Event.MOUSEOVER|Event.MOUSEOUT|Event.MOUSEMOVE|Event.CLICK);
		// set window event handlers (add or subtract as desired)
			window.onmouseover = WM_getCursorHandler;
			window.onmouseout = WM_getCursorHandler;
			window.onmousemove = WM_getCursorHandler;
			window.onclick = WM_getCursorHandler;
		// create an object to store the event properties 
			window.event = new Object;
		}

		function WM_getCursorHandler(e) {
		// set event properties to global vars (add or subtract as desired)
			window.event.clientX = e.pageX;
			window.event.clientY = e.pageY;
			window.event.x = e.layerX;
			window.event.y = e.layerY;
			window.event.screenX = e.screenX;
			window.event.screenY = e.screenY;
		// route the event back to the intended function
			if ( routeEvent(e) == false ) {
				return false;
			} else {
				return true;
			}
		}

		//-->

	</SCRIPT>
	
	</HEAD>
	<BODY BGCOLOR=$pagecolor TEXT=#000000 vlink=purple link=blue>

	<img align=left src="/ICONS/ID348.gif" alt="USGS"> 

	<table>
	  <tr> 
	    <td> <a href="$other/QUAKES/CURRENT/current.html">Latest quake info</a> </td>
	    <td> <a href="$other/hazprep/">Hazards & Preparedness</a> </td>
	    <td> <a href="$other/more/">More about earthquakes</a> </td>
	  </tr>
	  <tr> 
	    <td> <a href="$other/study/">Studying Earthquakes</a> </td>
	    <td> <a href="$other/WhatsNew/">What's new</a> </td>
	    <td> <a href="/">Home</a> </td>
	  </tr>
	</table>

	<p>&nbsp;</p>
	<a href="$infodir/index.html"
	onMouseOver="window.status='Description of the Drum Recorder Data'; return true;"
	onMouseOut="window.status=''; return true;">
	About the Drum Recorders</a> 

	<dl></dl>

	<table width="75%" border="1" align="center" bgcolor=$bannercolor bordercolordark="#000000">
	  <tr bgcolor=$bannercolor> 
	    <td height="47"> 
	      <center>
	        <font face="Times New Roman, Times, serif" size="6" color=$bannertxtcolor>
	        	$banner1
	        </font> </center>
	    </td>
	  </tr>
	  <tr bgcolor=$bannercolor> 
	    <td height="34"> 
	      <center>
	        <font face="Arial"><font size="4" color=$bannertxtcolor face="Times New Roman, Times, serif">
				$banner2
	        </font></font> </center>
	    </td>
	  </tr>
	</table>

	<center><h4>
	$tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8 $tag9
	</h4></center>
	<P> <HR> <P>
	
EOT
	
	if($JavaOK == 1) { 
		print STDOUT "<center> <h4><FONT COLOR=red>";
		print STDOUT "Click on the seismogram to read time!";
		print STDOUT "</FONT></h4> </center>";
	}
	
	print STDOUT "<center><td>";
	print STDOUT "<A name=\"anchor1\"   \n>";
	if($JavaOK == 1) { 
		print STDOUT "<IMG SRC=\"$other${webdir}/$currentitem\" WIDTH=$gbwid HEIGHT=$gbhgt usemap=\"#Click_Image\">  \n";
	}
	else { 
		print STDOUT "<IMG SRC=\"$other${webdir}/$currentitem\" WIDTH=$gbwid HEIGHT=$gbhgt>  \n";
	}
	print STDOUT "</a>";

	if($JavaOK == 1) { 
		print STDOUT "<P><map name=\"Click_Image\"> \n \n";

		$ybase = 100 - 7;
		$ystep = 15;
		$xbase = 50;
		$xstep = 48;
		$ysize = $ystep*24*4;
		$y1 = $ybase;
		$y2 = $y1 + $ysize;
		
	# comb 1
		$x1 = $xbase;
		print STDOUT "<area shape=\"poly\" coords=\" ";
		for($i=0;$i<15;$i+=2) {
			$x2 = $x1 + $xstep;
			print STDOUT "$x1,$y1,$x1,$y2,$x2,$y2,$x2,$y1,";
			$x1 = $x2 + $xstep;
		}
		$y3 = $y1 - 1;
		
		print STDOUT "$x1,$y1,$x1,$y3,$xbase,$y3,$xbase,$y1\" \n  ";
		print STDOUT "href=\"#\"  ";
		print STDOUT "onMouseOver=\"statusBar(' '); return true;\" \n";
		print STDOUT "onMouseMove=\"newstatusBar('test3'); return true;\" \n";
		print STDOUT "onMouseOut=\"statusBar(' '); return true;\" \n";
		print STDOUT "onClick=\"newstatusBar('999'); return false;\" ";
		print STDOUT " > \n";

	# comb 2
		$x1 = $xbase + $xstep;
		print STDOUT "<area shape=\"poly\" coords=\" ";
		for($i=0;$i<13;$i+=2) {
			$x2 = $x1 + $xstep;
			print STDOUT "$x1,$y1,$x1,$y2,$x2,$y2,$x2,$y1,";
			$x1 = $x2 + $xstep;
		}
		$y3 = $y1 - 1;
		$x2 = $xbase + $xstep;
		
		print STDOUT "$x1,$y1,$x1,$y3,$x2,$y3,$x2,$y1\" \n  ";
		print STDOUT "href=\"#\"  ";
		print STDOUT "onMouseOver=\"statusBar(' '); return true;\" \n";
		print STDOUT "onMouseMove=\"newstatusBar('test3'); return true;\" \n";
		print STDOUT "onMouseOut=\"statusBar(' '); return true;\" \n";
		print STDOUT "onClick=\"newstatusBar('999'); return false;\" ";
		print STDOUT " > \n";

		print STDOUT " </map> \n";
	}

	print STDOUT "</td></center> \n ";
	
	print STDOUT "<center><h4> \n $tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8 $tag9 </h4></center>\n";

	print STDOUT " <P> <HR> <P> </body> </html> \n";

return;

}

########################################################################
# Display map for selection of helicorders
# querytype = 5
########################################################################
sub make_map {

	$banner1 = "Northern California Drum Recorders";
	$banner2 = $ua;
	$banner2 = "$browser   $ua   $versionpt  $version";
	$banner2 = "Data Selector";
	
print STDOUT <<EOT;
	Content-type: text/html

	<HTML>
	<HEAD><TITLE>
	NCSN HELICORDER STATION MAP
	</TITLE>

	<SCRIPT language="JavaScript">

		<!--

		function buildLink(stationid)
		{
			var the_sel_num = window.document.date_form.date_sel.selectedIndex;
			var the_sel_value = window.document.date_form.date_sel.options[the_sel_num].value;
			
			var the_sel_site = stationid;
			
			if (the_sel_value == 99) {
				alert ("Sorry, no data available at this time.");
			}
		// Asking for all stations
			else if (the_sel_site == 99) {
				if (the_sel_value == 0) {
					var newlink = "/cgi-bin/$self?4&0&" + the_sel_value;
				}
				else {
					var newlink = "/cgi-bin/$self?2&0&" + the_sel_value;
				}
				window.location = newlink; 
			}
		// Asking for all available dates from clicked station
			else if (the_sel_value == 0) {
				var newlink = "/cgi-bin/$self?1&" + the_sel_site + "&0";
				window.location = newlink;
			}
		// Asking for selected date and clicked station
			else {
				var newlink = "/cgi-bin/$self?3&" + the_sel_site + "&" + the_sel_value;
				window.location = newlink;
			}
		}

		function statusBar(stationid)
		{
			var the_sel_num = window.document.date_form.date_sel.selectedIndex;
			var the_sel_text = window.document.date_form.date_sel.options[the_sel_num].text;
			window.status =  stationid  + " " + the_sel_text;
		}

		function newstatusBar(stationid)
		{
			window.status =  stationid  + " " + window.event.x;
		}


		if(!window.event && window.captureEvents) {
		// set up event capturing for mouse events (add or subtract as desired)
			window.captureEvents(Event.MOUSEOVER|Event.MOUSEOUT|Event.CLICK|Event.DBLCLICK);
		// set window event handlers (add or subtract as desired)
			window.onmouseover = WM_getCursorHandler;
			window.onmouseout = WM_getCursorHandler;
			window.onclick = WM_getCursorHandler;
			window.ondblclick = WM_getCursorHandler;
		// create an object to store the event properties 
			window.event = new Object;
		}

		function WM_getCursorHandler(e) {
		// set event properties to global vars (add or subtract as desired)
			window.event.clientX = e.pageX;
			window.event.clientY = e.pageY;
			window.event.x = e.layerX;
			window.event.y = e.layerY;
			window.event.screenX = e.screenX;
			window.event.screenY = e.screenY;
		// route the event back to the intended function
			if ( routeEvent(e) == false ) {
				return false;
			} else {
				return true;
			}
		}

		//-->

	</SCRIPT>



	</HEAD>
	<BODY TEXT="#000000" BGCOLOR=$pagecolor LINK=blue VLINK=purple>
	<A NAME="top"></A>
	  
	<img align=left src="/ICONS/ID348.gif" alt="USGS"> 
	<table>
		<tr> 
			<td> <a href="$other/QUAKES/CURRENT/current.html">Latest quake info</a> </td>
			<td> <a href="$other/hazprep/">Hazards & Preparedness</a> </td>
			<td> <a href="$other/more/">More about earthquakes</a> </td>
		</tr>
		<tr> 
			<td> <a href="$other/study/">Studying Earthquakes</a> </td>
			<td> <a href="$other/WhatsNew/">What's new</a> </td>
			<td> <a href="/">Home</a> </td>
		</tr>
	</table>
	<P>&nbsp;</P>

	<table width="75%" border="1" align="center" bgcolor=$bannercolor bordercolordark="#000000">
		<tr bgcolor=$bannercolor> 
			<td height="47"> 
				<center>
				<font face="Times New Roman, Times, serif" size="6" color=$bannertxtcolor>
					$banner1
				</font> </center>
			</td>
		</tr>
		<tr bgcolor=$bannercolor> 
			<td height="34"> 
				<center>
				<font face="Arial"><font size="4" color=$bannertxtcolor face="Times New Roman, Times, serif">
					$banner2
				</font></font> </center>
			</td>
		</tr>
	</table>

	<table width="81%" align="center" bordercolorlight="$bordercolor" bordercolordark="$bordercolor" bgcolor="$pagecolor">
	  <tr> 
	    <td width="30%"> 
	      <center>
			<a href="$infodir/index.html"
			onMouseOver="window.status='Description of the Drum Recorder Data'; return true;"
			onMouseOut="window.status=''; return true;">
			<b>About the Drum Recorders</b></a> 
	      </center>
	    </td>
	    <td width="34%"> 
	      <center>
	        <a href="$other/recenteqs"
				onMouseOver="window.status='Go To Map of Recent Earthquakes'; return true;"
				onMouseOut="window.status=''; return true;">
	        <b>Map of Recent Earthquakes</b></a> 
	      </center>
	    </td>
	    <td width="36%"> 
	      <center>
	        <a href="./heliLV.pl"
				onMouseOver="window.status='View Seismograms from Long Valley, CA'; return true;"
				onMouseOut="window.status=''; return true;">
	        <b>Long Valley Seismograms</b></a> 
	      </center>
	    </td>
	  </tr>
	</table>

	<center><p><font size="4"><b>Select date, then click on station.
	 <form method="post" action="/cgi-bin/SelectDate.pl" name="date_form">
	   <select name="date_sel">
EOT
            
	foreach $item (@reversedates) {
		($datepart = $item) =~ s/(\d{4})(\d{2})(\d{2})/$1_$2_$3/;
		($year,$mon,$day) = split ('_',$datepart);
		print STDOUT " <option value=\"$item\">$mon/$day/$year</option> \n";
	}
	if($numdates > 0) {
		print STDOUT " <option value=\"0\">All Available Dates</option> \n";
	} else {
		print STDOUT " <option value=\"99\">No data available</option> \n";
	}
	
print STDOUT <<EOT;
            
	  </select>
	 </form>
	</center>

	   <center>
	<img src="$selectormap" width="395" height="396"  border="2" usemap="#Click Station Map"><BR>
	<p></p></center>

	<P><map name="Click Station Map">
	    <area shape="rect" coords="224,61,317,87" href="#" 
	    	onMouseOver="statusBar('All Stations (Clickable Thumbnails)'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('99'); return false;" alt="All Stations">
	    <area shape="poly" coords="179,163,179,161,186,151,209,150,207,163,191,170,179,168,179,162" href="#" 
	    	onMouseOver="statusBar('Station MPR'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('MPR'); return false;" alt="Station MPR">
	    <area shape="poly" coords="221,179,221,178,232,163,255,163,255,179,234,185,222,178" href="#" 
	    	onMouseOver="statusBar('Station MEM'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('MEM'); return false;" alt="Station MEM">
	    <area shape="poly" coords="170,267,179,254,198,257,199,270,177,274,170,274,171,268,171,267" href="#" 
	    	onMouseOver="statusBar('Station PMG'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('PMG'); return false;" alt="Station PMG">
	    <area shape="poly" coords="161,243,162,242,176,234,196,236,196,246,174,251,160,252,163,242" href="#" 
	    	onMouseOver="statusBar('Station PMM'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('PMM'); return false;" alt="Station PMM">
	    <area shape="poly" coords="147,222,163,215,177,216,177,227,152,233,148,222" href="#" 
	    	onMouseOver="statusBar('Station BBG'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('BBG'); return false;" alt="Station BBG">
	    <area shape="poly" coords="140,206,140,207,150,203,162,203,160,212,133,219,132,210,139,209,140,210" href="#" 
	    	onMouseOver="statusBar('Station HFP'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('HFP'); return false;" alt="Station HFP">
	    <area shape="poly" coords="117,203,125,191,146,191,146,203,126,205,116,203" href="#" 
	    	onMouseOver="statusBar('Station JUM'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('JUM'); return false;" alt="Station JUM">
	    <area shape="poly" coords="92,188,112,184,119,186,108,195,93,197,94,188" href="#" 
	    	onMouseOver="statusBar('Station JSF'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('JSF'); return false;" alt="Station JSF">
	    <area shape="poly" coords="121,165,141,165,141,174,117,178,112,172,116,165" href="#" 
	    	onMouseOver="statusBar('Station CBR'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('CBR'); return false;" alt="Station CBR">
	    <area shape="poly" coords="85,176,108,172,112,181,102,184,89,185,88,176" href="#" 
	    	onMouseOver="statusBar('Station JSB'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('JSB'); return false;" alt="Station JSB">
	    <area shape="poly" coords="82,151,107,151,110,165,101,168,86,163,83,152" href="#" 
	    	onMouseOver="statusBar('Station NHF'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('NHF'); return false;" alt="Station NHF">
	    <area shape="poly" coords="85,137,98,124,116,125,115,137,94,143,87,135" href="#" 
	    	onMouseOver="statusBar('Station GGP'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('GGP'); return false;" alt="Station GGP">
	    <area shape="poly" coords="142,112,157,98,176,99,175,112,151,122,145,109" href="#" 
	    	onMouseOver="statusBar('Station OWY'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('OWY'); return false;" alt="Station OWY">
	    <area shape="poly" coords="63,94,64,93,78,81,93,83,91,93,71,104,62,100,66,93" href="#" 
	    	onMouseOver="statusBar('Station KCP'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('KCP'); return false;" alt="Station KCP">
	    <area shape="poly" coords="131,76,143,67,159,69,156,79,137,84,130,78,133,74" href="#" 
	    	onMouseOver="statusBar('Station LDB'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('LDB'); return false;" alt="Station LDB">
	    <area shape="poly" coords="50,39,70,29,85,30,82,43,53,51,52,38" href="#" 
	    	onMouseOver="statusBar('Station KRP'); return true;" onMouseOut="window.status=''; return true;" 
	    		onClick="buildLink('KRP'); return false;" alt="Station KRP">

	</map>
	</BODY></HTML>

EOT


return;

}

########################################################################
# Build the generic banner for a page.
########################################################################
sub Build_Banner {


print STDOUT <<EOT;
	Content-type: text/html

	<HTML>
	<HEAD><TITLE>
	Northern California Seismic Network - Drum Recorders
	</TITLE></HEAD>
	<BODY BGCOLOR=$pagecolor TEXT=#000000 vlink=purple link=blue>

	<img align=left src="/ICONS/ID348.gif" alt="USGS"> 

	<table>
	  <tr> 
	    <td> <a href="$other/QUAKES/CURRENT/current.html">Latest quake info</a> </td>
	    <td> <a href="$other/hazprep/">Hazards & Preparedness</a> </td>
	    <td> <a href="$other/more/">More about earthquakes</a> </td>
	  </tr>
	  <tr> 
	    <td> <a href="$other/study/">Studying Earthquakes</a> </td>
	    <td> <a href="$other/WhatsNew/">What's new</a> </td>
	    <td> <a href="/">Home</a> </td>
	  </tr>
	</table>

	<p>&nbsp;</p>
	<a href="$infodir/index.html"
	onMouseOver="window.status='Description of the Drum Recorder Data'; return true;"
	onMouseOut="window.status=''; return true;">
	About the Drum Recorders</a> 

	<dl></dl>

	<table width="75%" border="1" align="center" bgcolor=$bannercolor bordercolordark="#000000">
	  <tr bgcolor=$bannercolor> 
	    <td height="47"> 
	      <center>
	        <font face="Times New Roman, Times, serif" size="6" color=$bannertxtcolor>
	        	$banner1
	        </font> </center>
	    </td>
	  </tr>
	  <tr bgcolor=$bannercolor> 
	    <td height="34"> 
	      <center>
	        <font face="Arial"><font size="4" color=$bannertxtcolor face="Times New Roman, Times, serif">
				$banner2
	        </font></font> </center>
	    </td>
	  </tr>
	</table>

	<center><h4>
	$tag1 $tag2 $tag3 $tag4 $tag5 $tag6 $tag7 $tag8 $tag9
	</h4></center>
	<P> <HR> <P>
EOT

return;

}

########################################################################
# Take a time array of form ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)
#  and increments by a given number of hours.
# Returns the new time array.
########################################################################
sub hr_increment {

local (@t1) = splice (@_,0,9);
local ($delhr) = @_;

# print "t1 = @t1 \n";
# print "delhr = $delhr\n";

require "timelocal.pl";

my $t1seconds = &timegm(@t1);
my $t2seconds = $t1seconds + $delhr*3600.0;

@t2 = gmtime($t2seconds);

# print "t2 = @t2 \n";

return @t2;

}

########################################################################
# Take a date string of form YYYYMMDD.
# Returns the new time array.
########################################################################
sub date2secs {

	$date = $qdate;
	$sec0 = 0;
	$min0 = 0;
	$hour0 = 0;
	$mday0 = substr ($date,6,2);
	$mon0 = substr ($date,4,2) - 1;
	$year0 = substr ($date,0,4) - 1900;
	$wday0 = 0;
	$yday0 = 0;
	$isdst0 = 0;
	@t2 = ($sec0,$min0,$hour0,$mday0,$mon0,$year0,$wday0,$yday0,$isdst0);

return @t2;

}

########################################################################
