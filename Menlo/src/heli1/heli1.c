/*************************************************************************
 *      heli1.c:                                                         *
 * On a time schedule, requests data from waveserver(s) and plots gif    *
 * files in a helicorder format.  These files are then transferred to    *
 * webserver(s).                                                         *
 *                                                                       *
 * Principal Author: Jim Luetgert 10/07/98, 07/07/00, 06/14/02, 01/29/03 *
 *************************************************************************/

#include <platform.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <chron3.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include "mem_circ_queue.h" 
#include <swap.h>
#include <trace_buf.h>
/*
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <wait.h>
*/

#include <ws_clientII.h>
#include "gd.h"
#include "gdfontt.h"   /*  6pt      */
#include "gdfonts.h"   /*  7pt      */
#include "gdfontmb.h"  /*  9pt Bold */
#include "gdfontl.h"   /* 13pt      */
#include "gdfontg.h"   /* 10pt Bold */

#include "heli1.h" 

DATABUF  pTrace;
GAP *pGap;

/* Functions in this source file
 *******************************/
thr_ret Heartbeat( void * );
void SetUp(Global *);
void UpDate(Global *);
void Sort_Servers (Global *, double);
short Build_Axes(Global *, double, int, PltPar *);
void Make_Grid(Global *, PltPar *);
void Pallette(gdImagePtr GIF, long color[]);
int Plot_Trace(Global *, double *, double);
void Do_Stats(Global *, double *, int, double);
void Get_Line_Bias(Global *, int);
void hpsort(int, double ra[]);
void CommentList(Global *);
void Save_Plot(Global *, PltPar *);
int Build_Menu (Global *);
int In_Menu_list (Global *);
int  RequestWave(Global *, int, double *, char *, char *, char *, char *, double, double);
int  WSReqBin(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt);
int  WSReqBin2(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt);
int IsDST(int, int, int);
void Decode_Time( double, TStrct *);
void Encode_Time( double *, TStrct *);
void date22( double, char *);
void SetPlot(double, double);
int ixq(double);
int iyq(double);
void Erase_Labels(Global *But, LocInfo *plot, long);
void Get_Sta_Info(Global *);
int Put_Sta_Info(int, Global *);
void config_me( char *,  Global *); 
void ewmod_status( unsigned char, short, char *); 
void lookup_ew ( void );  

/* Shared memory
 ******************************/
static  SHM_INFO  InRegion;          /* public shared memory for receiving arkive messages */

/* Things to lookup in the earthworm.h table with getutil.c functions
 **********************************************************************/
static long          InRingKey;      /* key of transport ring for input         */
static unsigned char InstId;         /* local installation id                   */
static unsigned char MyModId;        /* our module id                           */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;

/* Things to read from configuration file
 ****************************************/
static char InRingName[20];          /* name of transport ring for i/o          */
static char MyModuleId[20];          /* module id for this module               */
static int  LogSwitch;               /* 0 if no logging should be done to disk  */
static int  Transport;               /* 0 if no transport ring has been defined */
static time_t HeartBeatInterval = 5; /* seconds between heartbeats              */
static int  Debug = 1;               /* debug flag                              */
static int  EWmodule = 1;            /* Earthworm Module flag (0 for standalone)*/

/* Variables for talking to statmgr
 **********************************/
char      Text[150];
pid_t     MyPid;       /* Our own pid, sent with heartbeat for restart purposes */

#define THREAD_STACK_SIZE 8192
time_t MyLastInternalBeat;      /* time of last heartbeat into the local Earthworm ring    */
unsigned  TidHeart;             /* thread id. was type thread_t on Solaris! */

/* Error words used by this module
 *********************************/
#define   ERR_NETWORK       0
#define   ERR_NOMENU        1
#define   ERR_TIMETRACK     2
#define   ERR_FILEWRITE     3

static Global BStruct;               /* Private area for the threads            */
double    Data[MAXTRACELTH];         /* Trace array                             */

/* Other globals
 ***************/
#define   XLMARGIN   0.7             /* Margin to left of axes                  */
#define   XRMARGIN   1.0             /* Margin to right of axes                 */
#define   YBMARGIN   1.0             /* Margin at bottom of axes                */
#define   YTMARGIN   0.9             /* Margin at top of axes plus size of logo */
double    YBMargin = 1.0;            /* Margin at bottom of axes                */
double    YTMargin = 0.7;            /* Margin at top of axes                   */
double    sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's        */
                                     /* time 0 and 1970-01-01 00:00:00.0 GMT    */
char      string[20];
char      module[50];
static double    xsize, ysize, plot_up;

/*************************************************************************
 *  main( int argc, char **argv )                                        *
 *************************************************************************/

int main( int argc, char **argv )
{
    char    whoami[50];
    time_t  atime, now;
    int     i, j;

    /* Check command line arguments
     ******************************/
    if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", module);
        exit( 0 );
    }
    
    /* Zero the wave server arrays *
     *******************************/
    for (i=0; i< MAX_WAVESERVERS; i++) {
        memset( BStruct.wsIp[i],      0, MAX_ADRLEN);
        memset( BStruct.wsPort[i],    0, MAX_ADRLEN);
        memset( BStruct.wsComment[i], 0, MAX_ADRLEN);
    }
    strcpy(module, argv[1]);
    for(i=0;i<(int)strlen(module);i++) if(module[i]=='.') module[i] = 0;
    strcpy(BStruct.mod, module);
    sprintf(whoami, " %s: %s: ", module, "main");

    /* Clear out the gap list */
    pTrace.nGaps   = 0;
    pTrace.gapList = NULL;

    /* Get our own Pid for restart purposes
    ***************************************/
    MyPid = getpid();
    if( MyPid == -1 ) {
        fprintf( stderr,"%s Cannot get pid. Exiting.\n", whoami);
        return -1;
    }

    /* Read the configuration file(s)
     ********************************/
    config_me( argv[1], &BStruct );

    /* Look up important info from earthworm.h tables
     ************************************************/
    if(EWmodule) lookup_ew();

    /* Initialize name of log-file & open it
     ***************************************/
    LogSwitch = EWmodule? LogSwitch:0;
    logit_init( argv[1], (short) MyModId, 256, LogSwitch );
    logit( "" , "%s Read command file <%s>\n", whoami, argv[1] );

    /* DEBUG: dump variables to stdout
    **********************************/
    if(LogSwitch) {
        logit("","%s MyModuleId: %s  InRingName: %s\n",    whoami, MyModuleId, InRingName);
    }

    /* Attach to public HYPO shared memory ring
     ******************************************/
    Transport = 0;
    if(EWmodule) {
        tport_attach( &InRegion, InRingKey );
        Transport = 1;
        if (LogSwitch) logit( "", "%s Attached to public memory region <%s>: %ld.\n", 
                                  whoami, InRingName, InRegion.key );
#ifdef _SOLARIS                    /* SOLARIS ONLY:                         */
        if(BStruct.Debug) logit( "e", "%s Attached to public memory region <%s> key: %ld mid: %ld sid: %ld.\n", 
                                  whoami, InRingName, InRegion.key, InRegion.mid, InRegion.sid );
        if(BStruct.Debug) logit( "e", "%s nbytes: %ld keymax: %ld keyin: %ld keyold: %ld flag: %d.\n", 
                                  whoami, InRegion.addr->nbytes, 
                                  InRegion.addr->keymax, InRegion.addr->keyin, 
                                  InRegion.addr->keyold, InRegion.addr->flag );
#endif                             /*                                       */
    }
    
   /* Start the heartbeat thread
   ****************************/
    time(&MyLastInternalBeat); /* initialize our last heartbeat time */
                          
    if ( StartThread( Heartbeat, (unsigned)THREAD_STACK_SIZE, &TidHeart ) == -1 ) {
        logit( "et","%s Error starting Heartbeat thread. Exiting.\n", whoami );
        tport_detach( &InRegion );
        return -1;
    }

	/* sleep for 2 seconds to allow heart to beat so statmgr gets it.  */
	
	sleep_ew(2000);
    
    Get_Sta_Info(&BStruct);
    for(i=0;i<BStruct.nPlots;i++) Put_Sta_Info(i, &BStruct);
    
    CommentList(&BStruct);
    
    while(1) {
	    if(!Build_Menu (&BStruct)) {
	        logit("e", "%s No Menu! Wait 5 minutes and try again.\n", whoami);
	        for(i=0;i<BStruct.nServer;i++) wsKillMenu(&(BStruct.menu_queue[i]));
	        ewmod_status( TypeError, ERR_NOMENU, "" );
	        for(j=0;j<60;j++) {
	            sleep_ew( 5000 );       /* wait around */
	        }
	    } else { break;}
    }
    
    for(i=0;i<BStruct.nPlots;i++) {
        BStruct.Current_Plot = i;
    
        Put_Sta_Info(i, &BStruct);
    
        SetUp(&BStruct);
    }
  /* Kill the used menus */    
    for(i=0;i<BStruct.nServer;i++) wsKillMenu(&(BStruct.menu_queue[i]));
    if(BStruct.OneDayOnly) {
        logit("et", "%s Exiting as result of flag.\n",  whoami);
        exit(0);
    }

    /* ------------------------ start working loop -------------------------*/
    while(1) {
        
        logit("et", "%s Updating Images.\n",  whoami);
    
            if(!Build_Menu (&BStruct)) {
            logit("e", "%s No Menu! Wait 5 minutes and try again.\n", whoami);
            for(j=0;j<60;j++) {
                sleep_ew( 5000 );       /* wait around */
            }
            BStruct.NoMenuCount += 1;
            if(BStruct.NoMenuCount > 5) {
                logit("et", "%s No menu in 5 consecutive trys. Exiting.\n", whoami);
                for(j=0;j<BStruct.nServer;j++) wsKillMenu(&(BStruct.menu_queue[j]));
                if(BStruct.Debug) {
                    sleep_ew( 500 );
                } else {
                    ewmod_status( TypeError, ERR_NOMENU, "" );
                    exit(-1);
                }
            }
        }
        else {
            if(BStruct.Debug) logit("e", "%s Got a Menu! \n", whoami);
            BStruct.NoMenuCount = 0;

            for(i=0;i<BStruct.nPlots;i++) {
                BStruct.Current_Plot = i;
    
                Put_Sta_Info(i, &BStruct);
    
                UpDate(&BStruct);
                /* see if a termination has been requested 
                   ***************************************/
                if(Transport) {
					if ( tport_getflag( &InRegion ) == TERMINATE ||
						 tport_getflag( &InRegion ) == MyPid ) {      /* detach from shared memory regions*/
						sleep_ew( 500 );       /* wait around */
						tport_detach( &InRegion );
						logit("et", "%s Termination requested; exiting.\n", whoami);
						fflush(stdout);
						exit( 0 );
					}
                }
            }
          /* Kill the used menus */    
            for(i=0;i<BStruct.nServer;i++) wsKillMenu(&(BStruct.menu_queue[i]));

            time(&atime);
            do {
                /* see if a termination has been requested */
                /* *************************************** */
                if(Transport) {
					if ( tport_getflag( &InRegion ) == TERMINATE ||
						 tport_getflag( &InRegion ) == MyPid ) {      /* detach from shared memory regions*/
						sleep_ew( 500 );       /* wait around */
						tport_detach( &InRegion );
						logit("et", "%s Termination requested; exiting.\n", whoami);
						fflush(stdout);
						exit( 0 );
					}
                }
                sleep_ew( 5000 );       /* wait around */
            } while(time(&now)-atime < BStruct.UpdateInt);
        }
    }   /*------------------------------end of working loop------------------------------*/
}


/***************************** Heartbeat **************************
 *           Send a heartbeat to the transport ring buffer        *
 ******************************************************************/

thr_ret Heartbeat( void *dummy )
{
    time_t now;

   /* once a second, do the rounds.  */
    while ( 1 ) {
        sleep_ew(1000);
        time(&now);

        /* Beat our heart (into the local Earthworm) if it's time
        ********************************************************/
        if (difftime(now,MyLastInternalBeat) > (double)HeartBeatInterval) {
            ewmod_status( TypeHeartBeat, 0, "" );
            time(&MyLastInternalBeat);
        }
    }
}


/********************************************************************
 *  SetUp does the initial setup and first set of images            *
 ********************************************************************/

void SetUp(Global *But)
{
    char    whoami[50], time1[25], time2[25], sip[25], sport[25], sid[50];
    double  tankStarttime, tankEndtime, EndPlotTime, LocalTimeOffset;
    double  StartTime, EndTime, Duration, ZTime;
    time_t  current_time;
    int     i, j, k, jj, successful, server, ForceRebuild, hour1, hour2;
    int     first_gulp, mins;
    int     minPerStep, minOfDay;
    TStrct  t0, Time1, Time2;    

    sprintf(whoami, " %s: %s: ", But->mod, "SetUp");
    first_gulp = 1;        
    i = But->Current_Plot;
    
    mins         = But->mins;
    ForceRebuild = But->BuildOnRestart;
    
    SetPlot(But->xsize, But->ysize);
    
    StartTime = time(&current_time) - 120;
    LocalTimeOffset = But->plt[i].LocalTimeOffset;
    Decode_Time( StartTime, &Time1);
    if(IsDST(Time1.Year,Time1.Month,Time1.Day) && (But->plt[i].UseDST || But->UseDST) && But->UseLocal) {
        LocalTimeOffset = LocalTimeOffset + 3600;
    }
/*    if(But->DaysAgo > 0) StartTime -= But->DaysAgo*60*60*But->HoursPerPlot;    */
    if(But->DaysAgo > 0) StartTime -= But->DaysAgo*60*60*24;
    Decode_Time(StartTime, &t0);
    t0.Sec = 0;
    minPerStep = But->secsPerStep/60;
    minOfDay = minPerStep*((t0.Hour*60 + t0.Min)/minPerStep) - minPerStep;
    if(minOfDay<0) minOfDay = 0;
    t0.Hour = minOfDay/60;
    t0.Min = minOfDay - t0.Hour*60;
    Encode_Time( &StartTime, &t0);    /* StartTime modulo step */
    
    /* If we are requesting older data, make sure we are on the current page.  */
    Decode_Time(StartTime + LocalTimeOffset, &Time1);
    hour1 = But->HoursPerPlot*((24*Time1.Day + Time1.Hour)/But->HoursPerPlot);
    StartTime = StartTime - But->plt[i].OldData*60*60;
    Decode_Time(StartTime + LocalTimeOffset, &Time2);
    hour2 = But->HoursPerPlot*((24*Time2.Day + Time2.Hour)/But->HoursPerPlot);

    if(hour1 != hour2) {
        Time1.Hour = But->HoursPerPlot*(Time1.Hour/But->HoursPerPlot);
        Time1.Min = 0;
        Time1.Sec = 0.0;
        Encode_Time( &StartTime, &Time1);
        StartTime = StartTime - LocalTimeOffset;
        ForceRebuild = 1;
    }

    if(In_Menu_list(But)) {
        
        Sort_Servers(But, StartTime);
        if(But->nentries <= 0) goto quit;
                    
        k = But->index[0];
        tankStarttime = But->TStime[k];
        tankEndtime   = But->TEtime[k];
        if(tankEndtime > time(&current_time)) tankEndtime = time(&current_time);
        if(tankStarttime > tankEndtime) tankStarttime = tankEndtime;
        
        if(But->Debug) {
            date22 (StartTime, time1);
            logit("e", "%s Requested Start Time: %s. \n", 
                  whoami, time1);
            date22 (tankStarttime, time1);
            date22 (tankEndtime,   time2);
            date22 (But->TStime[k], time1);
            date22 (But->TEtime[k], time2);
            strcpy(sip,   But->wsIp[k]);
            strcpy(sport, But->wsPort[k]);
            strcpy(sid,   But->wsComment[k]);
            logit("e", "%s Got menu for: %s. %s %s %s %s <%s>\n", 
                  whoami, But->plt[i].SCNtxt, time1, time2, sip, sport, sid);
            for(j=0;j<But->nentries;j++) {
                k = But->index[j];
                date22 (But->TStime[k], time1);
                date22 (But->TEtime[k], time2);
                logit("e", "            %d %d %s %s %s %s <%s>\n", 
                      j, k, time1, time2, But->wsIp[k], But->wsPort[k], But->wsComment[k]);
            }
        }
        
        if(StartTime < tankStarttime) {
            StartTime = tankStarttime;
            Decode_Time(StartTime, &t0);
            t0.Sec = 0;
            minPerStep = But->secsPerStep/60;
            minOfDay = minPerStep*((t0.Hour*60 + t0.Min)/minPerStep);
            if(minOfDay<0) minOfDay = 0;
            t0.Hour = minOfDay/60;
            t0.Min = minOfDay - t0.Hour*60;
            Encode_Time( &StartTime, &t0);
        }
        
        Decode_Time(StartTime + LocalTimeOffset, &t0);
        t0.Min = 0;
        t0.Sec = 0.0;
        t0.Hour = But->HoursPerPlot*(t0.Hour/But->HoursPerPlot);
        But->plt[i].CurrentHour = t0.Hour;
        But->plt[i].CurrentDay  = t0.Day;
        But->plt[i].LastTime = StartTime;
        
        Duration = But->secsPerGulp;
        EndTime = StartTime + Duration;      
        Encode_Time( &ZTime, &t0);
        
        if(Build_Axes(But, ZTime, ForceRebuild, &(But->plt[i]))) {
            logit("e", "%s Build_axes croaked for: %s. \n", 
                     whoami, But->plt[i].SCNtxt);
            goto quit;
        }
        
        if(But->Debug) {
            date22 (StartTime, time1);
            logit("e", "%s Requested Start Time: %s. \n", 
                  whoami, time1);
            date22 (EndTime, time1);
            logit("e", "%s Requested End Time:   %s. \n", 
                  whoami, time1);
        }
        
        EndPlotTime = tankEndtime;
        if(EndPlotTime > time(&current_time)) EndPlotTime = time(&current_time);
        while(EndTime < EndPlotTime) {
            if(But->Debug) {
                date22 (StartTime, time1);
                date22 (EndTime,   time2);
                logit("e", "%s Data for: %s. %s %s %s %s <%s>\n", 
                     whoami, But->plt[i].SCNtxt, time1, time2, sip, sport, sid);
            }
            
            /* Try to get some data
            ***********************/
            for(jj=0;jj<But->nentries;jj++) {
                server = But->index[jj];
                successful = RequestWave(But, server, Data, But->plt[i].Site, 
                    But->plt[i].Comp, But->plt[i].Net, But->plt[i].Loc, StartTime, Duration);
                if(successful == 1) {   /*    Plot this trace to memory. */
                    break;
                }
                else if(successful == 2) {
                    if(But->Debug) {
                        logit("e", "%s Data for: %s. RequestWave error 2\n", 
                                whoami, But->plt[i].SCNtxt);
                    }
                    continue;
                }
                else if(successful == 3) {   /* Gap in data */
                    if(But->Debug) {
                        logit("e", "%s Data for: %s. RequestWave error 3\n", 
                                whoami, But->plt[i].SCNtxt);
                    }
                    
                }
                else if(successful == 4) {   /* Too many small gaps in data */
                    if(But->Debug) {
                        logit("e", "%s Data for: %s. RequestWave error 4\n", 
                                whoami, But->plt[i].SCNtxt);
                    }
                    
                }
            }
            
            if(successful == 1) {   /*    Plot this trace to memory. */
                if(first_gulp) {
                    But->plt[i].DCcorr = But->Mean;
                    for(jj=0;jj<60;jj++) But->plt[i].MinMean[jj] = But->Mean;
                    for(k=0;k<30;k++) But->plt[i].MinMean[k] = But->plt[i].DCcorr;
                    Do_Stats(But, Data, But->Npts, StartTime);
                    Get_Line_Bias(But, 30);
                    first_gulp = 0;

                    if(But->plt[i].Filter) {
                  /*  Prime the two-pole Butterworth   */    
                        But->plt[i].x0 = But->plt[i].x1 = But->plt[i].x2 = Data[0];
                        But->plt[i].y0 = But->plt[i].y1 = But->plt[i].y2 = 0.0;
                    }
                }
                Plot_Trace(But, Data, StartTime);
            }
            
            StartTime += But->secsPerStep;
        
            Decode_Time(StartTime + LocalTimeOffset, &t0);
            hour1 = But->HoursPerPlot*(t0.Hour/But->HoursPerPlot) + 24*t0.Day;
            hour2 = But->plt[i].CurrentHour + 24*But->plt[i].CurrentDay;
            if(hour1 != hour2) {
                if(But->OneDayOnly > 0) break;
                t0.Min = 0;
                t0.Sec = 0.0;
                t0.Hour = But->HoursPerPlot*(t0.Hour/But->HoursPerPlot);
                But->plt[i].CurrentHour = t0.Hour;
                But->plt[i].CurrentDay = t0.Day;
                Encode_Time( &ZTime, &t0);
                StartTime = ZTime - LocalTimeOffset;
                Save_Plot(But, &(But->plt[i]));
            
                if(Build_Axes(But, ZTime, 1, &(But->plt[i]))) {
                    logit("e", "%s Build_axes croaked for: %s. \n", 
                             whoami, But->plt[i].SCNtxt);
                    goto quit;
                }

            }
            But->plt[i].LastTime = StartTime;
            Duration = But->secsPerGulp;
            EndTime = StartTime + Duration;      
        }
        Save_Plot(But, &(But->plt[i]));

        /* Clear out the gap list */
        if(pTrace.nGaps != 0) {
            while ( (pGap = pTrace.gapList) != (GAP *)NULL) {
                pTrace.gapList = pGap->next;
                free(pGap);
            }
        }
        pTrace.nGaps = 0;

    } else {
        logit("e", "%s %s not in menu.\n", whoami, But->plt[i].SCNtxt);
    }
quit:        
    sleep_ew(200);
}


/********************************************************************
 *  UpDate does the image updates                                   *
 *                                                                  *
 ********************************************************************/

void UpDate(Global *But)
{
    char    whoami[50], time1[25], time2[25], sip[25], sport[25], sid[50];
    char    string[200];
    double  tankStarttime, tankEndtime, EndPlotTime, LocalTimeOffset;
    double  StartTime, EndTime, Duration, ZTime;
    time_t  current_time;
    int     i, j, k, jj, successful, server, ForceRebuild, hour1, hour2;
    int     UseLocal, LocalSecs, mins, secsPerGulp, secsPerStep;
    int     minPerStep, minOfDay;
    TStrct  t0, Time1;    

    sprintf(whoami, " %s: %s: ", But->mod, "UpDate");
    i = But->Current_Plot;
    date22 (But->plt[i].LastTime, time1);
    
    UseLocal    = But->UseLocal;
    LocalSecs   = But->LocalSecs;
    mins        = But->mins;
    secsPerGulp = But->secsPerGulp;
    secsPerStep = But->secsPerStep;
    ForceRebuild = 0;
    
    SetPlot(But->xsize, But->ysize);
    
    if(In_Menu_list(But)) {    
        if(But->plt[i].LastTime==0) {
            logit("et", "%s %s LastTime came up 0! %s\n", whoami, But->plt[i].SCNtxt, time1); 
            But->plt[i].LastTime = time(&current_time) - But->secsPerStep;
            sprintf(string, "LastTime==0 %s", But->plt[i].SCNtxt);
         /* 
            ewmod_status( TypeError, ERR_TIMETRACK, string );
         */
        }
        
        if(fabs(But->plt[i].LastTime-time(&current_time)) > 86400) {
            date22 (But->plt[i].LastTime, time1);
            logit("et", "%s %s LastTime came up %f %s!\n", 
                whoami, But->plt[i].SCNtxt, But->plt[i].LastTime, time1);
            But->plt[i].LastTime = time(&current_time) - But->secsPerStep;
            sprintf(string, "Last time too far away. Dead channel? %s", But->plt[i].SCNtxt);
         /* 
            ewmod_status( TypeError, ERR_TIMETRACK, string );
         */
        }

        Decode_Time( But->plt[i].LastTime, &Time1);
        if(Time1.Year < 2002) {
            logit("et", "%s Bad year (%.4d) in start time for %s. Correct to current.\n", 
                    whoami, Time1.Year, But->plt[i].SCNtxt);
            sprintf(string, "Update: bad year %s", But->plt[i].SCNtxt);
            ewmod_status( TypeError, ERR_TIMETRACK, string );
            But->plt[i].LastTime = time(&current_time) - But->secsPerStep;
        }
        StartTime = But->plt[i].LastTime;   /* Earliest time needed. */
            
        Sort_Servers(But, StartTime);
        if(But->nentries <= 0) goto quit;
                    
        k = But->index[0];
        tankStarttime = But->TStime[k];
        tankEndtime   = But->TEtime[k];
        if(tankEndtime > time(&current_time)) tankEndtime = time(&current_time);
        if(tankStarttime > tankEndtime) tankStarttime = tankEndtime;
        
        if(But->Debug) {
            date22 (tankStarttime, time1);
            date22 (tankEndtime,   time2);
            strcpy(sip,   But->wsIp[k]);
            strcpy(sport, But->wsPort[k]);
            strcpy(sid,   But->wsComment[k]);
            logit("e", "%s Got menu for: %s. %s %s %s %s <%s>\n", 
                  whoami, But->plt[i].SCNtxt, time1, time2, sip, sport, sid);
            for(j=0;j<But->nentries;j++) {
                k = But->index[j];
                date22 (But->TStime[k], time1);
                date22 (But->TEtime[k], time2);
                logit("e", "            %d %d %s %s %s %s <%s>\n", 
                      j, k, time1, time2, But->wsIp[k], But->wsPort[k], But->wsComment[k]);
            }
        }
        
        if(StartTime < tankStarttime) {
            StartTime = tankStarttime;
            Decode_Time(StartTime, &t0);
            t0.Sec = 0;
            minPerStep = But->secsPerStep/60;
            minOfDay = minPerStep*((t0.Hour*60 + t0.Min)/minPerStep);
            if(minOfDay<0) minOfDay = 0;
            t0.Hour = minOfDay/60;
            t0.Min = minOfDay - t0.Hour*60;
            Encode_Time( &StartTime, &t0);
        }
        
        LocalTimeOffset = But->plt[i].LocalTimeOffset;
        Decode_Time( StartTime, &Time1);
        if(IsDST(Time1.Year,Time1.Month,Time1.Day) && (But->plt[i].UseDST || But->UseDST) && But->UseLocal) {
            LocalTimeOffset = LocalTimeOffset + 3600;
        }
        Decode_Time(StartTime + LocalTimeOffset, &t0);
        t0.Min = 0;
        t0.Sec = 0.0;
        t0.Hour = But->HoursPerPlot*(t0.Hour/But->HoursPerPlot);
        hour1 = But->HoursPerPlot*(t0.Hour/But->HoursPerPlot) + 24*t0.Day;
        hour2 = But->plt[i].CurrentHour + 24*But->plt[i].CurrentDay;
        if(hour1 != hour2) {
            But->plt[i].CurrentHour = t0.Hour;
            But->plt[i].CurrentDay  = t0.Day;
                Encode_Time( &ZTime, &t0);
            StartTime = ZTime - LocalTimeOffset;
            ForceRebuild = 1;
        }
        
        Duration = But->secsPerGulp;
        EndTime = StartTime + Duration;      
        Encode_Time( &ZTime, &t0);

        Decode_Time( ZTime, &Time1);
        if(Time1.Year < 1997) {
            logit("et", "%s Bad year (%.4d) in start time for %s. %f.\n", 
                    whoami, Time1.Year, But->plt[i].SCNnam, ZTime);
            ZTime = time(&current_time) - But->secsPerStep;
        }
        
        if(Build_Axes(But, ZTime, ForceRebuild, &(But->plt[i]))) {
            logit("e", "%s Build_axes croaked for: %s. \n", 
                     whoami, But->plt[i].SCNtxt);
            But->plt[i].LastTime = time(&current_time) - But->secsPerStep;
            goto quit;
        }

        EndPlotTime = tankEndtime;
        if(EndPlotTime > time(&current_time)) EndPlotTime = time(&current_time);
        while(EndTime < EndPlotTime) {
            if(But->Debug) {
                date22 (StartTime, time1);
                date22 (EndTime,   time2);
                logit("e", "%s Data for: %s. %s %s %s %s <%s>\n", 
                     whoami, But->plt[i].SCNtxt, time1, time2, sip, sport, sid);
            }
            
            /* Try to get some data
            ***********************/
            for(jj=0;jj<But->nentries;jj++) {
                server = But->index[jj];
                successful = RequestWave(But, server, Data, But->plt[i].Site, 
                    But->plt[i].Comp, But->plt[i].Net, But->plt[i].Loc, StartTime, Duration);
                if(successful == 1) {   /*    Plot this trace to memory. */
                    break;
                }
                else if(successful == 2) {
                    if(But->Debug) {
                        logit("e", "%s Data for: %s. RequestWave error 2\n", 
                            whoami, But->plt[i].SCNtxt);
                    }
                   continue;
                }
                else if(successful == 3) {   /* Gap in data */
                    if(But->Debug) {
                        logit("e", "%s Data for: %s. RequestWave error 3\n", 
                            whoami, But->plt[i].SCNtxt);
                    }
                }
                else if(successful == 4) {   /* Too many small gaps in data */
                    if(But->Debug) {
                        logit("e", "%s Data for: %s. RequestWave error 4\n", 
                                whoami, But->plt[i].SCNtxt);
                    }
                    
                }
            }
            
            if(successful == 1) {   /*    Plot this trace to memory. */
                Plot_Trace(But, Data, StartTime);
            }
            
            StartTime += But->secsPerStep;
        
            Decode_Time(StartTime + LocalTimeOffset, &t0);
            hour1 = But->HoursPerPlot*(t0.Hour/But->HoursPerPlot) + 24*t0.Day;
            hour2 = But->plt[i].CurrentHour + 24*But->plt[i].CurrentDay;
            if(hour1 != hour2) {
                t0.Min = 0;
                t0.Sec = 0.0;
                t0.Hour = But->HoursPerPlot*(t0.Hour/But->HoursPerPlot);
                But->plt[i].CurrentHour = t0.Hour;
                But->plt[i].CurrentDay = t0.Day;
                Encode_Time( &ZTime, &t0);
                StartTime = ZTime - LocalTimeOffset;
                Save_Plot(But, &(But->plt[i]));
            
                if(Build_Axes(But, ZTime, 1, &(But->plt[i]))) {
                    logit("e", "%s Build_axes croaked for: %s. \n", 
                             whoami, But->plt[i].SCNtxt);
                    goto quit;
                }

            }
            date22 (But->plt[i].LastTime, time1);
            if(StartTime==0) {
                logit("et", "%s %s StartTime came up 0! %s\n", whoami, But->plt[i].SCNtxt, time1); 
                StartTime = time(&current_time) - But->secsPerStep;
                sprintf(string, "StartTime==0 %s", But->plt[i].SCNtxt);
                ewmod_status( TypeError, ERR_TIMETRACK, string );
            }
            
            if(fabs(StartTime-time(&current_time)) > 86400) {
                date22 (StartTime, time1);
                date22 (current_time, time2);
                logit("et", "%s %s StartTime came up %f %s! Current time is %s.\n", 
                    whoami, But->plt[i].SCNtxt, StartTime, time1, time2);
                StartTime = time(&current_time) - But->secsPerStep;
                sprintf(string, "StartTime too far away %s", But->plt[i].SCNtxt);
                ewmod_status( TypeError, ERR_TIMETRACK, string );
            }
            But->plt[i].LastTime = StartTime;
            Duration = But->secsPerGulp;
            EndTime = StartTime + Duration;      
        }
        Save_Plot(But, &(But->plt[i]));

        /* Clear out the gap list */
        if(pTrace.nGaps != 0) {
            while ( (pGap = pTrace.gapList) != (GAP *)NULL) {
                pTrace.gapList = pGap->next;
                free(pGap);
            }
        }
        pTrace.nGaps = 0;

    } else {
        logit("e", "%s %s not in menu.\n", whoami, But->plt[i].SCNtxt);
    }
quit:        
    sleep_ew(200);
}


/*************************************************************************
 *   Sort_Servers                                                        *
 *      From the table of waveservers containing data for the current    *
 *      SCN, the table is re-sorted to provide an intelligent order of   *
 *      search for the data.                                             *
 *                                                                       *
 *      The strategy is to start by dividing the possible waveservers    *
 *      into those which contain the requested StartTime and those which *
 *      don't.  Those which do are retained in the order specified in    *
 *      the config file allowing us to specify a preference for certain  *
 *      waveservers.  Those waveservers which do not contain the         *
 *      requested StartTime are sorted such that the possible data       *
 *      retrieved is maximized.                                          *
 *************************************************************************/

void Sort_Servers (Global *But, double StartTime)
{
    char    whoami[50], c22[25];
    double  tdiff[MAX_WAVESERVERS*2];
    int     j, k, jj, last_jj, kk, hold, index[MAX_WAVESERVERS*2];
    
    sprintf(whoami, " %s: %s: ", But->mod, "Sort_Servers");
        /* Throw out servers with data too old. */
    j = 0;
    while(j<But->nentries) {
        k = But->index[j];
        if(StartTime > But->TEtime[k]) {
            if(But->Debug) {
                date22( StartTime, c22);
                logit("e","%s %d %d  %s", whoami, j, k, c22);
                    logit("e", " %s %s <%s>\n", 
                          But->wsIp[k], But->wsPort[k], But->wsComment[k]);
                date22( But->TEtime[k], c22);
                logit("e","ends at: %s rejected.\n", c22);
            }
            But->nentries -= 1;
            for(jj=j;jj<But->nentries;jj++) {
                But->index[jj] = But->index[jj+1];
            }
        } else j++;
    }
    if(But->nentries <= 1) return;  /* nothing to sort */
            
    /* Calculate time differences between StartTime needed and tankStartTime */
    /* And copy positive values to the top of the list in the order given    */
    jj = 0;
    for(j=0;j<But->nentries;j++) {
        k = index[j] = But->index[j];
        tdiff[k] = StartTime - But->TStime[k];
        if(tdiff[k]>=0) {
            But->index[jj++] = index[j];
            tdiff[k] = -65000000; /* two years should be enough of a flag */
        }
    }
    last_jj = jj;
    
    /* Sort the index list copy in descending order */
    j = 0;
    do {
        k = index[j];
        for(jj=j+1;jj<But->nentries;jj++) {
            kk = index[jj];
            if(tdiff[kk]>tdiff[k]) {
                hold = index[j];
                index[j] = index[jj];
                index[jj] = hold;
            }
            k = index[j];
        }
        j += 1;
    } while(j < But->nentries);
    
    /* Then transfer the negatives */
    for(j=last_jj,k=0;j<But->nentries;j++,k++) {
        But->index[j] = index[k];
    }
}    


/********************************************************************
 *    Build_Axes constructs the axes for the plot by drawing the    *
 *    GIF image in memory.                                          *
 *                                                                  *
 ********************************************************************/

short Build_Axes(Global *But, double Stime, int ForceRebuild, PltPar *plt)
{
    char    whoami[50], c22[30], cstr[150], LocalTimeID[4];
    double  atime, trace_size, tsize, yp, Scale;
    int     ntrace, mins, CurrentHour, LocalTime;
    int     ix, iy, i, j, jj, k, kk;
    int     xgpix, ygpix;
    long    black, must_create;
    gdImagePtr    im_in;
    LocInfo  plot;
    FILE    *in;
    TStrct  Time1;
    time_t  current_time;

    sprintf(whoami, " %s: %s: ", But->mod, "Build_Axes");
    i = But->Current_Plot;
    mins  = But->mins;
    Scale = plt->Scale;
    CurrentHour = plt->CurrentHour;
    LocalTime = plt->LocalTime;
    strcpy(LocalTimeID, plt->LocalTimeID);

    Decode_Time( Stime, &Time1);
    if(Time1.Year < 2002) {
        logit("et", "%s Bad year (%.4d) in start time for %s. Exiting.\n", whoami, Time1.Year, plt->SCNnam);
        ewmod_status( TypeError, ERR_TIMETRACK, "Build_Axes lost time" );
        Stime = time(&current_time) - But->secsPerStep;
        return 1;
    }
    if(IsDST(Time1.Year,Time1.Month,Time1.Day) && (plt->UseDST || But->UseDST)) {
        LocalTime = LocalTime + 1;
        LocalTimeID[1] = 'D';
    }
    sprintf(But->plt[i].Today, "%.4d%.2d%.2d", Time1.Year, Time1.Month, Time1.Day);
    sprintf(But->TmpName, "%s%s.%s%.2d", But->Prefix, plt->SCNnam, But->plt[i].Today, CurrentHour);
    sprintf(But->GifName, "%s%s", But->TmpName, ".gif");
    sprintf(But->LocalGif, "%s%s.gif", But->GifDir, plt->SCNnam);
    
    But->GifImage = 0L;
    if(ForceRebuild) {
        must_create = 1;
    } else {
        must_create = 0;
        in = fopen(But->LocalGif, "rb");
        if(!in) must_create = 1;
        if(in) {
            But->GifImage = gdImageCreateFromGif(in);
            fclose(in);
            if(!But->GifImage) must_create = 1;
            else {
                for(j=0;j<MAXCOLORS;j++) gdImageColorDeallocate(But->GifImage, j);
                Pallette(But->GifImage, But->gcolor);
            }
        }
    }
    
    plt->CurrentTime = Stime;
    if(must_create) {
        xgpix = (int)(But->xsize*72.0) + 8;
        ygpix = (int)(But->ysize*72.0) + 8;

        logit("et", "%s Building new axes for: %s %s %.2d\n", 
            whoami, plt->SCNnam, But->plt[i].Today, CurrentHour);
        But->GifImage = gdImageCreate(xgpix, ygpix);
        if(But->GifImage==0) {
            logit("et", "%s Not enough memory! Reduce size of image or increase memory.\n\n", whoami);
            return 2;
        }
        if(But->GifImage->sx != xgpix) {
            logit("et", "%s Not enough memory for entire image! Reduce size of image or increase memory.\n", 
                 whoami);
            return 2;
        }
        Pallette(But->GifImage, But->gcolor);
        if(But->logo) {
            in = fopen(But->logoname, "rb");
            if(in) {
                im_in = gdImageCreateFromGif(in);
                fclose(in);
                gdImageCopy(But->GifImage, im_in, 0, 0, 0, 0, im_in->sx, im_in->sy);
                gdImageDestroy(im_in);
            }
        }
    }
    
    /* Make sure all but the data is blank *
     ***************************************/
    plot.Gif = But->GifImage;
    plot.x1 = 0; plot.x2 = (int)(XLMARGIN*72.0); 
    plot.x3 = (int)((But->xsize-XRMARGIN)*72.0); plot.x4 = (int)(But->xsize*72.0) + 8;
    plot.y1 = 0; plot.y2 = (int)(YTMargin*72.0); 
    plot.y3 = (int)((But->ysize-YBMARGIN)*72.0); plot.y4 = (int)(But->ysize*72.0) + 8;
    Erase_Labels(But, &plot, But->gcolor[WHITE]);

    /* Plot the frame *
     ******************/
    Make_Grid(But, plt);

    /* Put in the trace center lines * 
     *********************************/
    black = But->gcolor[BLACK];
    ntrace   =  But->LinesPerHour*But->HoursPerPlot;
    trace_size =  But->axeymax/ntrace; /* height of one trace [Data] */
    for(j=0;j<ntrace;j++) {
        iy = iyq(((float)(j)+0.5)*trace_size);
        gdImageLine(But->GifImage, ixq(0.0), iy, ixq(But->axexmax), iy, black);
    }

    for(j=0;j<But->HoursPerPlot;j++) {
        k = But->UseLocal? 
            j + CurrentHour:
            j + CurrentHour + LocalTime;
        if(k <  0) k += 24;
        if(k > 23) k -= 24;
        kk = But->ShowUTC? k - LocalTime: k;
        if(kk <  0) kk += 24;
        if(kk > 23) kk -= 24;
        
        yp =  trace_size*j*But->LinesPerHour + 0.5*trace_size;
        ix = ixq(-0.5); 
        iy = iyq(yp) - 5;  
        /* don't print too close to the top */
        if ( iy < 72.0 * YTMargin + 0 ) continue;
    
        sprintf(cstr, "%02d:00", kk);
        gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);
        if(kk==0&&j>0) {
            atime =  But->UseLocal? Stime:Stime+LocalTime*3600;
            atime =  But->ShowUTC? atime-LocalTime*3600:atime;
            Decode_Time( atime + j*3600, &Time1);
            Decode_Time( atime + (j+3)*3600, &Time1);
            Time1.Hour = Time1.Min = 0;
            Time1.Sec = 0.0;
            Encode_Time( &Time1.Time, &Time1);
            date22 (Time1.Time, c22);
            sprintf(cstr, "%.5s", c22) ;
            gdImageString(But->GifImage, gdFontMediumBold, ix, iy-15, cstr, black);    
        }
        
        ix = ixq(But->axexmax + 0.05); 
        if (mins < 60) sprintf(cstr, "%02d:%02d", k, mins);
        else {
            k += 1;
            if(k > 23) k -= 24;
            sprintf(cstr, "%02d:00", k);
        }
        gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);
        if(k==0&&j>0) {
            atime =  But->UseLocal? Stime:Stime+LocalTime*3600;
            Decode_Time( atime + (j+3)*3600, &Time1);
            Time1.Hour = Time1.Min = 0;
            Time1.Sec = 0.0;
            Encode_Time( &Time1.Time, &Time1);
            date22 (Time1.Time, c22);
            sprintf(cstr, "%.5s", c22) ;
            gdImageString(But->GifImage, gdFontMediumBold, ix, iy-15, cstr, black);    
        }
    }
    
    iy = (int)(YTMargin*72.0) - 45;
    ix = ixq(But->axexmax + 0.05);
    gdImageString(But->GifImage, gdFontMediumBold, ix, iy, LocalTimeID, black);  
    ix = ixq(-0.5);
    if(But->ShowUTC) sprintf(cstr, "UTC") ;
    else         strcpy(cstr, LocalTimeID);
    gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);  
      
    if(plt->DClabeled) {
        ix = ixq(But->axexmax + 0.55);
        iy = (int)(YTMargin*72.0) - 15;
        sprintf(cstr, "   DC") ;
        gdImageString(But->GifImage, gdFontTiny, ix, iy, cstr, black);
    }
    
    /* Write labels with the Date * 
     ******************************/
    atime =  But->UseLocal? Stime:Stime+LocalTime*3600;
    iy = (int)(YTMargin*72.0) - 30;
    if(!plot_up || But->UseLocal) {
        Decode_Time( atime+60*mins, &Time1);
        Encode_Time( &Time1.Time, &Time1);
        date22 (Time1.Time, c22);
        sprintf(cstr, "%.11s", c22) ;
        gdImageString(But->GifImage, gdFontMediumBold, ixq(But->axexmax + 0.05), iy, cstr, black);    
    }
    
    if(!plot_up || !But->UseLocal || (But->UseLocal && !But->ShowUTC)) {
        atime =  But->ShowUTC? atime-LocalTime*3600:atime;
        Decode_Time( atime, &Time1);
        Time1.Hour = Time1.Min = 0;
        Time1.Sec = 0.0;
        Encode_Time( &Time1.Time, &Time1);
        date22 (Time1.Time, c22);
        sprintf(cstr, "%.11s", c22) ;
        gdImageString(But->GifImage, gdFontMediumBold, ixq(-0.5), iy, cstr, black);    
    }

    /* Write label with the Channel ID * 
     ***********************************/
    ix = ixq(0.0);
    iy = (int)(YTMargin*72.0) - 45;
    if(plt->Comment[0]!=0) {
        sprintf(cstr, "(%s) ", plt->Comment);
        ix = ixq(But->axexmax/2) - 6*(int)strlen(cstr)/2;
        gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);
        iy -= 15;
    }
    sprintf(cstr, "%s ", plt->SCNtxt);
    ix = ixq(But->axexmax/2) - 6*(int)strlen(cstr)/2;
    gdImageString(But->GifImage, gdFontMediumBold, ix, iy, cstr, black);
    
    /* Measure it *
     **************/
    tsize = 1.0/(But->plt[i].Scaler);
    strcpy(cstr, "");
    if(But->plt[i].Sens_unit<=1) {
        sprintf(cstr, " = %7.0f microvolts", tsize*But->plt[i].Sens_gain*1000000.0) ;
    } 
    if(But->plt[i].Sens_unit==2) {
        sprintf(cstr, " = %f cm/sec", 
                tsize) ;
    } 
    if(But->plt[i].Sens_unit==3) {
        sprintf(cstr, " = %f cm/sec/sec = %f %%g", 
                tsize, tsize*100.0/981) ;
    } 
    ix = ixq(But->axexmax/2) - 6*(int)strlen(cstr)/2;
    iy = (int)(YTMargin*72.0) - 10;
    jj = (int)(trace_size*72.0);
    if((int)strlen(cstr)!=0) {
        gdImageLine(But->GifImage, ix-2, iy,    ix+2, iy,    black);
        gdImageLine(But->GifImage, ix-2, iy-jj, ix+2, iy-jj, black);
        gdImageLine(But->GifImage, ix,   iy,    ix,   iy-jj, black);
        gdImageString(But->GifImage, gdFontSmall, ix+5, iy-10, cstr, black);  
    } 
    
    strcpy(cstr, "");
    if(But->plt[i].Sens_unit<=1) {
        sprintf(cstr, " = %7.0f microvolts", tsize*But->plt[i].Sens_gain*1000000.0) ;
    } 
    if(But->plt[i].Sens_unit==2) {
        sprintf(cstr, " = %f cm/sec = %7.0f microvolts", 
                tsize, tsize*But->plt[i].Sens_gain*1000000.0) ;
    } 
    if(But->plt[i].Sens_unit==3) {
        sprintf(cstr, " = %f cm/sec/sec = %f %%g = %7.0f microvolts.", 
                tsize, tsize*100.0/981, tsize*But->plt[i].Sens_gain*1000000.0) ;
    } 
    ix = 15;
    iy = (int)(But->ysize*72.0);
    jj = (int)(trace_size*72.0);
    if((int)strlen(cstr)!=0) {
        gdImageLine(But->GifImage, ix-2, iy,    ix+2, iy,    black);
        gdImageLine(But->GifImage, ix-2, iy-jj, ix+2, iy-jj, black);
        gdImageLine(But->GifImage, ix,   iy,    ix,   iy-jj, black);
        gdImageString(But->GifImage, gdFontSmall, ix+5, iy-10, cstr, black);  
    } 
    
    /* Label for optional clipping */
    if ( But->Clip ) {
        sprintf(cstr, "Traces clipped at plus/minus %d vertical divisions", But->Clip );
        gdImageString(But->GifImage, gdFontTiny, ixq(But->axexmax/2.0 + 1.0), iy-10, cstr, black );
    }
    
    return 0;
}


/********************************************************************
 *    Make_Grid constructs the grid overlayed on the plot.          *
 *                                                                  *
 ********************************************************************/

void Make_Grid(Global *But, PltPar *plt)
{
    char    string[150];
    double  in_sec, tsize;
    long    isec, xp, yp0, yp1, yp2, j, k, black, ixmax;
    long    major_incr, minor_incr, tiny_incr;
    int     inx[]={0,1,2,2,2,3,4,4,4,4,5}, iny[]={2,3,2,1,0,1,1,0,2,3,3};

    /* Plot the frame *
     ******************/
    black = But->gcolor[BLACK];
    gdImageRectangle( But->GifImage, ixq(0.0), iyq(But->axeymax), ixq(But->axexmax),  iyq(0.0),  black);

    /* Make the x-axis ticks * 
     *************************/
    major_incr = 60;
    minor_incr = 10;
    ixmax = But->mins*60;
    in_sec = But->axexmax / ixmax;
    /* Adjust spacing of major and minor ticks. Based on But->axexmax of 10 inches */
    tiny_incr = (in_sec < 0.05)?  0:1;        /* turn on `second' marks */
    if ( in_sec < 0.005 )    {   /* for 1 hour per line */
        major_incr = 300;
        minor_incr = 60;
    }
    else if (in_sec < 0.05 ) { /* for 30 min or less per line */
        major_incr = 60;
        minor_incr = 10;
    } 
    else {      /* for 5 min or less per line with But->axexmax = 20in */
        major_incr = 60;
        minor_incr = 10;
    }
    yp0 = (int)((ysize - YBMARGIN)*72.0);   /* Bottom axis line */
    yp1 = yp0 + (int)(0.15*72.0);           /* Space below axis for label */
    for(isec=0;isec<=ixmax;isec++) {
        xp = ixq(isec*in_sec);
        if ((div(isec, major_incr)).rem == 0) {
            tsize = 0.15;    /* major ticks */
            sprintf(string, "%d", isec / 60);
            k = (int)strlen(string) * 3;
            gdImageString(But->GifImage, gdFontMediumBold, xp-k, yp1, string, black);
            gdImageLine(But->GifImage, xp, iyq(0.0), xp, iyq(But->axeymax), But->gcolor[GREY]); /* make the minute mark */
        }
        else if ((div(isec, minor_incr)).rem == 0) 
            tsize = 0.10;    /* minor ticks */
        else if(tiny_incr)
            tsize = 0.05;    /*  tiny ticks */
        else
            tsize = 0.0;     /*  no ticks   */
        
        if(tsize > 0.0) {
            yp2 = yp0 + (int)(tsize*72.0);
            gdImageLine(But->GifImage, xp, yp0, xp, yp2, black); /* make the tick */
        }
    }
    strcpy(string, "TIME (MINUTES)");
    yp1 = yp0 + (int)(0.3*72.0);           /* Space below axis for label */
    xp  = ixq(But->axexmax/2.0 - 0.5);
    gdImageString(But->GifImage, gdFontMediumBold, xp, yp1, string, black);

    /* Initial it *
     **************/
    j = (int)(But->ysize*72.0) - 5;
    for(k=0;k<11;k++) gdImageSetPixel(But->GifImage, inx[k]+2, j+iny[k], black);
}


/*******************************************************************************
 *    Pallette defines the pallete to be used for plotting.                    *
 *     PALCOLORS colors are defined.                                           *
 *                                                                             *
 *******************************************************************************/

void Pallette(gdImagePtr GIF, long color[])
{
    color[WHITE]  = gdImageColorAllocate(GIF, 255, 255, 255);
    color[BLACK]  = gdImageColorAllocate(GIF, 0,     0,   0);
    color[RED]    = gdImageColorAllocate(GIF, 255,   0,   0);
    color[BLUE]   = gdImageColorAllocate(GIF, 0,     0, 255);
    color[GREEN]  = gdImageColorAllocate(GIF, 0,   105,   0);
    color[GREY]   = gdImageColorAllocate(GIF, 125, 125, 125);
    color[YELLOW] = gdImageColorAllocate(GIF, 125, 125,   0);
    color[TURQ]   = gdImageColorAllocate(GIF, 0,   255, 255);
    color[PURPLE] = gdImageColorAllocate(GIF, 200,   0, 200);    
    
    gdImageColorTransparent(GIF, -1);
}

/*******************************************************************************
 *    Plot_Trace plots an individual trace (Data)  and stuffs it into          *
 *     the GIF image in memory.                                                *
 *                                                                             *
 *******************************************************************************/

int Plot_Trace(Global *But, double *Data, double Stime)
{
    char    whoami[50], string[30];
    double  x0, x, y, xinc, Middle, samp_pix, atime, LocalTimeOffset;
    double  in_sec, xsf, ycenter, sf, value, trace_size;
    int     lastx, lasty, decimation, acquired, minutes;
    int     i, j, j0, k, ix, iy, LineNumber, mins, gap0, gap1;
    long    trace_clr;
    TStrct  StartTime, Time1;    

    sprintf(whoami, " %s: %s: ", But->mod, "Plot_Trace");
    i = But->Current_Plot;
    mins = But->mins;
    
    LocalTimeOffset = But->plt[i].LocalTimeOffset;
    Decode_Time( But->plt[i].CurrentTime, &Time1);
    if(IsDST(Time1.Year,Time1.Month,Time1.Day) && (But->plt[i].UseDST || But->UseDST) && But->UseLocal) {
        LocalTimeOffset = LocalTimeOffset + 3600;
    }
    atime = Stime + LocalTimeOffset - 3600.0*But->plt[i].CurrentHour;
    Decode_Time( atime, &StartTime);
    
    LineNumber = But->LinesPerHour*StartTime.Hour + StartTime.Min/mins;
    
    trace_size =  But->axeymax/(But->LinesPerHour*But->HoursPerPlot); /* height of one trace [Data] */
    ycenter =  trace_size*LineNumber + 0.5*trace_size;
    if(ycenter > But->axeymax) {
        if(But->Debug) {
            logit("e", "%s %s. ycenter too big: %f \n", 
                whoami, But->plt[i].SCNtxt, ycenter);
        }   
        return 1;
    }
    
    sf = trace_size*But->plt[i].Scaler;
    in_sec     = But->axexmax / (mins*60.0);
    samp_pix = But->samp_sec / in_sec / 72.0;
    decimation = 2;
    decimation = 1;
    if(But->DeciFlag <= 0) 
        decimation = ((int)samp_pix/4 < 1)? 1:(int)samp_pix/4;
    else 
        decimation = But->DeciFlag;
    xinc = decimation * in_sec / But->samp_sec;       /* decimation */
    xsf  = in_sec / But->samp_sec;       /* inches/sample */
    Do_Stats(But, Data, But->Npts, Stime);

    if(But->plt[i].Filter) {  /*  two-pole Butterworth   */    
  /*  For now, we'll Prime the two-pole Butterworth for each buffer.  
        But->plt[i].x0 = But->plt[i].x1 = But->plt[i].x2 = Data[0];
        But->plt[i].y0 = But->plt[i].y1 = But->plt[i].y2 = 0.0;    */ 
        for(j=0;j<But->Npts;j++) {
            But->plt[i].x0 = But->plt[i].x1;
            But->plt[i].x1 = But->plt[i].x2;
            But->plt[i].x2 = Data[j]/1.001467227;
   /*         But->plt[i].x2 = (Data[j]-But->plt[i].DCcorr)/1.001467227;   */
            
            But->plt[i].y0 = But->plt[i].y1;
            But->plt[i].y1 = But->plt[i].y2;
            But->plt[i].y2 = (But->plt[i].x0 + But->plt[i].x2) - 2.0*But->plt[i].x1 + 
                                (-0.9970719923*But->plt[i].y0) + 1.9970676994*But->plt[i].y1;
            Data[j] = But->plt[i].y2;
        }
    /*    for(j=0;j<But->Npts;j++) {
            k = But->Npts - j - 1;
            But->plt[i].x0 = But->plt[i].x1;
            But->plt[i].x1 = But->plt[i].x2;
            But->plt[i].x2 = Data[k]/1.001467227;
            
            But->plt[i].y0 = But->plt[i].y1;
            But->plt[i].y1 = But->plt[i].y2;
            But->plt[i].y2 = (But->plt[i].x0 + But->plt[i].x2) - 2.0*But->plt[i].x1 + 
                                (-0.9970719923*But->plt[i].y0) + 1.9970676994*But->plt[i].y1;
            Data[k] = But->plt[i].y2;
        }   */
    }
    
    /* Plot the trace *
     ******************/
    Decode_Time( Stime, &StartTime);
    minutes = 60*StartTime.Hour + StartTime.Min;
    minutes = div(minutes,mins).rem;
    if(minutes==0) {
        Get_Line_Bias(But, 30);
        ix = ixq(But->axexmax + 0.45);  iy = iyq(ycenter) - 3;
        sprintf(string, "%9.0f", But->plt[i].DCcorr);
        if(But->plt[i].DClabeled) gdImageString(But->GifImage, gdFontTiny, ix, iy, string, But->gcolor[BLACK]);
        if(But->Debug) {
            logit("e", "%s %s. DC Correction: %f \n", 
                whoami, But->plt[i].SCNtxt, But->plt[i].DCcorr);
        }
    }
    x0 = x = in_sec*60.0*minutes;
    if(But->Debug) {
        logit("e", "%s %s. minutes: %d x0: %f %d\n", 
            whoami, But->plt[i].SCNtxt, minutes, x0, But->Npts);
    }   
    
    trace_clr = But->gcolor[BLACK];
    k = div(LineNumber, But->LinesPerHour).rem;
    k = div(k, 4).rem + 1;
    trace_clr = But->gcolor[k];
    Middle = But->plt[i].DCremoved?  But->plt[i].DCcorr:0.0;
    if(But->plt[i].Filter) Middle = 0.0;
    pGap = pTrace.gapList;
    acquired = 0;
    j0 = 0;
    for(j=0;j<But->Npts;j+=decimation) {
        x = x0 + j0*xsf;
        j0 += decimation;
        if (x > But->axexmax) {
            if(But->Debug) {
                logit("e", "%s %s. x overflow: %f %d %d %d\n", 
                    whoami, But->plt[i].SCNtxt, x, j, But->Npts, decimation);
            }   
    
            if(j+2*decimation > But->Npts) break;
            LineNumber += 1;
            ycenter += trace_size;
            if (ycenter > But->axeymax) {
                if(But->Debug) {
                    logit("e", "%s %s. ycenter too big: %f \n", 
                        whoami, But->plt[i].SCNtxt, ycenter);
                }   
                return 1;
            }
            x = x0 = 0.0;
            j0 = 0;
            k = div(LineNumber, But->LinesPerHour).rem;
            k = div(k, 4).rem + 1;
            trace_clr = But->gcolor[k];
            atime = Stime + (float)j/But->samp_sec;
            Get_Line_Bias(But, 30);
            ix = ixq(But->axexmax + 0.45);  iy = iyq(ycenter) - 3;
            sprintf(string, "%9.0f", But->plt[i].DCcorr);
            if(But->plt[i].DClabeled) 
                gdImageString(But->GifImage, gdFontTiny, ix, iy, string, But->gcolor[BLACK]);
            Middle = But->plt[i].DCremoved?  But->plt[i].DCcorr:0.0;
            if(But->plt[i].Filter) Middle = 0.0;
            acquired = 0;  
        }
        gap0 = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->firstSamp;
        gap1 = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->lastSamp;
        if(j > gap1) {
            pGap = pGap->next;
            gap0 = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->firstSamp;
            gap1 = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->lastSamp;
        }
        /*
        if(Data[j] != 919191) {
        */
        if(j < gap0) {
            value = (Data[j] - Middle)/But->plt[i].sensitivity;  /* convert data to zero-mean cm/s */
            value = value*sf;  /* convert data to inches */
            if(plot_up) value = -value;
            if (But->Clip) {
                if ( value >  But->Clip * trace_size ) 
                     value =  But->Clip * trace_size;
                if ( value < -But->Clip * trace_size )
                     value = -But->Clip * trace_size;
            }
            y = ycenter + value;
            ix = ixq(x);    iy = iyq(y);
            if(acquired) {
                gdImageLine(But->GifImage, ix, iy, lastx, lasty, trace_clr);
            }
            lastx = ix;  lasty = iy;
            acquired = 1;
        }
        else {
            acquired = 0;  
        }
    } 
    return 0;
}


/*******************************************************************************
 *    Do_Stats finds the mean values for an individual trace (Data) for each   *
 *    minute, and stores them in the ring buffer MinMean.                      *
 *                                                                             *
 *******************************************************************************/

void Do_Stats(Global *But, double *Data, int maxpts, double Stime)
{
    int     i, j, k, kk, npts, MinMeanPtr, gap0, gap1;
    double  sum;
    TStrct  StartTime;    

    i = But->Current_Plot;
    
    npts = 60*But->samp_sec;
    if(npts  > maxpts) npts  = maxpts;
    
    Decode_Time( Stime, &StartTime);
    MinMeanPtr = div(StartTime.Min, 30).rem;

    pGap = pTrace.gapList;
    for(j=npts;j<maxpts;j+=npts) {
        sum = 0;
        kk = 0;
        gap0 = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->firstSamp;
        gap1 = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->lastSamp;
        for(k=j-npts;k<j;k++) {
            if(k > gap1) {
                pGap = pGap->next;
                gap0 = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->firstSamp;
                gap1 = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->lastSamp;
            }
            /*
            if(Data[k] != 919191.0) {
            */
            if(k < gap0) {
                sum += Data[k]; 
                kk++;
            }
        }
        But->plt[i].MinMean[MinMeanPtr] = (kk > 0)? sum/kk:0;
        MinMeanPtr = div(++MinMeanPtr, 30).rem;
    }
    But->plt[i].MinMeanPtr = MinMeanPtr;
}


/*******************************************************************************
 *    Get_Line_Bias finds the best estimate of the bias to be subtracted from  *
 *    this line's data to center it upon the zero line. This is accomplished   *
 *    by taking a weighted median of the means of the past nmins minutes.      *
 *                                                                             *
 *******************************************************************************/

void Get_Line_Bias(Global *But, int nmins)
{
    double  ds[2000];
    int     i, j, k, m, ind;

    i = But->Current_Plot;
    
    ind = div(But->plt[i].MinMeanPtr, nmins).rem;
    for(j=0;j<2000;j++) ds[j] = But->plt[i].MinMean[ind];
    m = 0;
    for(j=0;j<nmins;j++) {
        ind = div(j+But->plt[i].MinMeanPtr, nmins).rem;
        for(k=0;k<j/4+1;k++,m++) ds[m] = But->plt[i].MinMean[ind];
    }
    
    hpsort(m-2, ds);
    
    But->plt[i].DCcorr = ds[m/2];
}


/*******************************************************************************
 *    hpsort performs a sifting sort in place of the n members of the double   *
 *    array ra.  This is from Recipes in C, so it is assumed that the array    *
 *    is 1-based as in fortran rather than 0-based as in C!!                   *
 *******************************************************************************/

void hpsort(int n, double ra[])
{
    int   i, j, k, ir;
    double rra;
    
    if(n<2) return;
    k  = (n >> 1)+1;
    ir = n;
        
    for(;;) {
        if(k > 1) rra = ra[--k];
        else {
            rra = ra[ir];
            ra[ir] = ra[1];
            if(--ir == 1) {
                ra[1] = rra;
                break;
            }
        }
        i = k; j = k+k;
        while(j<=ir) {
            if(j<ir && ra[j]<ra[j+1]) j++;
            if(rra<ra[j]) {
                ra[i] = ra[j];
                i = j;
                j <<= 1;
            } else break;
        }
        ra[i] = rra;
    }
}


/*********************************************************************
 *   CommentList()                                                   *
 *    Build and send a file relating SCNs to their comments.         *
 *********************************************************************/

void CommentList(Global *But)
{
    char    tname[200], fname[175], whoami[50];
    int     i, j, ierr, jerr;
    FILE    *out;
    
    sprintf(whoami, " %s: %s: ", module, "CommentList");
    sprintf(tname, "%sznamelist.dat", But->GifDir);
    
    for(j=0;j<But->nltargets;j++) {
        out = fopen(tname, "wb");
        if(out == 0L) {
            logit("e", "%s Unable to open NameList File: %s\n", whoami, tname);    
        } else {
            for(i=0;i<But->nPlots;i++) {
                fprintf(out, "%s. %s.\n", But->plt[i].SCNnam, But->plt[i].Descrip);
            }
            fclose(out);
            sprintf(fname,  "%sznamelist.dat", But->loctarget[j] );
            ierr = rename(tname, fname); 
            /* The following silliness is necessary to be Windows compatible */ 
            if( ierr != 0 ) {
                if(Debug) logit( "e", "Error moving file %s to %s; ierr = %d\n", tname, fname, ierr );
                if( remove( fname ) != 0 ) {
                    logit("e","error deleting file %s\n", fname);
                } else  {
                    if(Debug) logit("e","deleted file %s.\n", fname);
                    jerr = rename( tname, fname );
                    if( jerr != 0 ) {
                        logit( "e", "error moving file %s to %s; ierr = %d\n", tname, fname, ierr );
                    } else {
                        if(Debug) logit("e","%s moved to %s\n", tname, fname );
                    }
                }
            } else {
                if(Debug) logit("e","%s moved to %s\n", tname, fname );
            }
        }
    }
}


/*********************************************************************
 *   Save_Plot()                                                     *
 *    Saves the current version of the GIF image and ships it out.   *
 *********************************************************************/

void Save_Plot(Global *But, PltPar *plt)
{
    char    tname[175], fname[175], whoami[50];
    FILE    *out;
    int     j, ierr, jerr;
    
    Make_Grid(But, plt);
    sprintf(whoami, " %s: %s: ", But->mod, "Save_Plot");
    
    /* Save the GIF file. *
     **********************/        
    out = fopen(But->LocalGif, "wb");
    if(out == 0L) {
        logit("e", "%s Unable to write GIF File: %s\n", whoami, But->LocalGif); 
        But->NoGIFCount += 1;
        if(But->NoGIFCount > 5) {
            logit("et", "%s Unable to write GIF in 5 consecutive trys. Exiting.\n", whoami);
            ewmod_status( TypeError, ERR_FILEWRITE, "Unable to write GIF" );
            exit(-1);
        }
    } else {
        But->NoGIFCount = 0;
        gdImageGif(But->GifImage, out);
        fclose(out);
    }
    
    /* Put the GIF file in output directory(s). *
     ********************************************/        
    for(j=0;j<But->nltargets;j++) {
        sprintf(tname,  "%stempgif.%s", But->GifDir, But->mod );
        out = fopen(tname, "wb");
        if(out == 0L) {
            logit("e", "%s Unable to write GIF File: %s\n", whoami, tname); 
            But->NoGIFCount += 1;
            if(But->NoGIFCount > 5) {
                logit("et", "%s Unable to write GIF in 5 consecutive trys. Exiting.\n", whoami);
                ewmod_status( TypeError, ERR_FILEWRITE, "Unable to write GIF" );
                exit(-1);
            }
        } else {
            But->NoGIFCount = 0;
            gdImageGif(But->GifImage, out);
            fclose(out);
            sprintf(fname,  "%s%s", But->loctarget[j], But->GifName );
            ierr = rename(tname, fname); 
            /* The following silliness is necessary to be Windows compatible */ 
            if( ierr != 0 ) {
                if(Debug) logit( "e", "Error moving file %s to %s; ierr = %d\n", tname, fname, ierr );
                if( remove( fname ) != 0 ) {
                    logit("e","error deleting file %s\n", fname);
                } else  {
                    if(Debug) logit("e","deleted file %s.\n", fname);
                    jerr = rename( tname, fname );
                    if( jerr != 0 ) {
                        logit( "e", "error moving file %s to %s; ierr = %d\n", tname, fname, ierr );
                    } else {
                        if(Debug) logit("e","%s moved to %s\n", tname, fname );
                    }
                }
            } else {
                if(Debug) logit("e","%s moved to %s\n", tname, fname );
            }
        }
    }
    gdImageDestroy(But->GifImage);
}


/*************************************************************************
 *   Build_Menu ()                                                       *
 *      Builds the waveservers' menus                                    *
 *      Each waveserver has its own menu so that we can do intelligent   *
 *      searches for data.                                               *
 *************************************************************************/
int Build_Menu (Global *But)
{
    char    whoami[50], server[100];
    int     i, j, retry, ret, rc, got_a_menu;
    WS_PSCNL scnp;
    WS_MENU menu; 

    sprintf(whoami, " %s: %s: ", But->mod, "Build_Menu");
    setWsClient_ewDebug(0);  
    if(But->WSDebug) setWsClient_ewDebug(1);  
    got_a_menu = 0;
    
    for(j=0;j<But->nServer;j++) But->index[j] = j;
    
    for (i=0;i< But->nServer; i++) {
        retry = 0;
        But->inmenu[i] = 0;
        sprintf(server, " %s:%s <%s>", But->wsIp[i], But->wsPort[i], But->wsComment[i]);
Append:
        ret = wsAppendMenu(But->wsIp[i], But->wsPort[i], &But->menu_queue[i], But->wsTimeout);
        
        if (ret == WS_ERR_NONE) {
            But->inmenu[i] = got_a_menu = 1;
        }
        else if (ret == WS_ERR_INPUT) {
            logit("e","%s Connection to %s input error\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_EMPTY_MENU) 
            logit("e","%s Unexpected empty menu from %s\n", whoami, server);
        else if (ret == WS_ERR_BUFFER_OVERFLOW) 
            logit("e","%s Buffer overflowed for %s\n", whoami, server);
        else if (ret == WS_ERR_MEMORY) 
            logit("e","%s Waveserver %s out of memory.\n", whoami, server);
        else if (ret == WS_ERR_PARSE) 
            logit("e","%s Parser failed for %s\n", whoami, server);
        else if (ret == WS_ERR_TIMEOUT) {
            logit("e","%s Connection to %s timed out during menu.\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_BROKEN_CONNECTION) {
            logit("e","%s Connection to %s broke during menu\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_SOCKET) 
            logit("e","%s Could not create a socket for %s\n", whoami, server);
        else if (ret == WS_ERR_NO_CONNECTION) { 
     /*       if(But->Debug) */
                logit("e","%s Could not get a connection to %s to get menu.\n", whoami, server);
        }
        else logit("e","%s Connection to %s returns error: %d\n", whoami, server, ret);
    }
    /* Let's make sure that servers in our server list have really connected.
       **********************************************************************/  
    if(got_a_menu) {
        for(j=0;j<But->nServer;j++) {
            if ( But->inmenu[j]) {
                rc = wsGetServerPSCNL( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
                if ( rc == WS_ERR_EMPTY_MENU ) {
                    if(But->Debug) logit("e","%s Empty menu for %s:%s <%s> \n", 
                                        whoami, But->wsIp[j], But->wsPort[j], But->wsComment[j]);
                    But->inmenu[j] = 0; 
                    continue;
                }
                if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                    if(But->Debug) logit("e","%s  %s:%s <%s> not in menu.\n", 
                                        whoami, But->wsIp[j], But->wsPort[j], But->wsComment[j]);
                    But->inmenu[j] = 0; 
                    continue;
                }
            }
        }
    }
    /* Now, detach 'em and let RequestWave attach only the ones it needs.
       **********************************************************************/  
    for(j=0;j<But->nServer;j++) {
        if(But->inmenu[j]) {
            menu = But->menu_queue[j].head;
            if ( menu->sock > 0 ) {  
                wsDetachServer( menu );
            }
        }
    }
    return got_a_menu;
}


/*************************************************************************
 *   In_Menu_list                                                        *
 *      Determines if the scn is in the waveservers' menu.               *
 *      If there, the tank starttime and endtime are returned.           *
 *      Also, the Server IP# and port are returned.                      *
 *************************************************************************/

int In_Menu_list (Global *But)
{
    char    whoami[50], server[100];
    int     i, j, rc;
    WS_PSCNL scnp;
    
    sprintf(whoami, " %s: %s: ", But->mod, "In_Menu_list");
    i = But->Current_Plot;
    But->nentries = 0;
    for(j=0;j<But->nServer;j++) {
        if(But->inmenu[j]) {
            sprintf(server, " %s:%s <%s>", But->wsIp[j], But->wsPort[j], But->wsComment[j]);
            rc = wsGetServerPSCNL( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
            if ( rc == WS_ERR_EMPTY_MENU ) {
                if(But->Debug) logit("e","%s Empty menu for %s \n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                if(But->Debug) logit("e","%s  %s not in menu.\n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }

			But->wsLoc[j] = (strlen(scnp->loc))? 1:0;
            while ( 1 ) {
               if(strcmp(scnp->sta,  But->plt[i].Site)==0 && 
                  strcmp(scnp->chan, But->plt[i].Comp)==0 && 
                  strcmp(scnp->net,  But->plt[i].Net )==0 && 
                  (strcmp(scnp->loc,  But->plt[i].Loc )==0 || But->wsLoc[j]==0) ) {  
                  But->TStime[j] = scnp->tankStarttime;
                  But->TEtime[j] = scnp->tankEndtime;
                  But->index[But->nentries]  = j;
                  But->nentries += 1;
               }
               if ( scnp->next == NULL )
                  break;
               else
                  scnp = scnp->next;
            }
        }
    }
    if(But->nentries>0) return 1;
    return 0;
}


/********************************************************************
 *  RequestWave                                                     *
 *   This is the binary version                                     *
 *   k - waveserver index                                           *
 ********************************************************************/
int RequestWave(Global *But, int k, double *Data,  
            char *Site, char *Comp, char *Net, char *Loc, double Stime, double Duration)
{
    char     whoami[50], SCNtxt[17];
    int      i, ret, iEnd, npoints, gap0;
    double   mean;
    TRACE_REQ   request;
    GAP *pGap;
    
    sprintf(whoami, " %s: %s: ", But->mod, "RequestWave");
    strcpy(request.sta,  Site);
    strcpy(request.chan, Comp);
    strcpy(request.net,  Net );
    strcpy(request.loc,  Loc );   
    request.waitSec = 0;
    request.pinno = 0;
    request.reqStarttime = Stime;
    request.reqEndtime   = request.reqStarttime + Duration;
    request.partial = 1;
    request.pBuf    = But->TraceBuf;
    request.bufLen  = MAXTRACELTH*9;
    request.timeout = But->wsTimeout;
    request.fill    = 919191;
    sprintf(SCNtxt, "%s %s %s %s", Site, Comp, Net, Loc);

    /* Clear out the gap list */
    if(pTrace.nGaps != 0) {
        while ( (pGap = pTrace.gapList) != (GAP *)NULL) {
            pTrace.gapList = pGap->next;
            free(pGap);
        }
    }
    pTrace.nGaps = 0;

    if(But->wsLoc[k]==0) {
	    ret = WSReqBin(But, k, &request, &pTrace, SCNtxt);
    } else {
	    ret = WSReqBin2(But, k, &request, &pTrace, SCNtxt);
    }
  
  /* Find mean value of non-gap data */
    pGap = pTrace.gapList;
    i = npoints = 0L;
    mean    = 0.0;
  /*
   * Loop over all the data, skipping any gaps. Note that a `gap' will not be declared
   * at the end of the data, so the counter `i' will always get to pTrace.nRaw.
   */
    do {
        iEnd = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->firstSamp - 1;
        if (pGap != (GAP *)NULL) { /* Test for gap within peak-search window */
            gap0 = pGap->lastSamp - pGap->firstSamp + 1;
            if(Debug) logit("t", "trace from <%s> has %d point gap in window at %d\n", SCNtxt, gap0, pGap->firstSamp);
        }
        for (; i < iEnd; i++) {
            mean += pTrace.rawData[i];
            npoints++;
        }
        if (pGap != (GAP *)NULL) {     /* Move the counter over this gap */    
            i = pGap->lastSamp + 1;
            pGap = pGap->next;
        }
    } while (i < pTrace.nRaw );
  
    mean /= (double)npoints;
  
  /* Now remove the mean, and set points inside gaps to zero */
    i = 0;
    do {
        iEnd = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->firstSamp - 1;
        for (; i < iEnd; i++) {
/*
            pTrace.rawData[i] -= mean;
*/
        }
        if (pGap != (GAP *)NULL) {    /* Fill in the gap with zeros */    

            for ( ;i < pGap->lastSamp + 1; i++) {
/*
                pTrace.rawData[i] = 0.0;
*/
                pTrace.rawData[i] = 919191;
            }

            pGap = pGap->next;
        }
    } while (i < pTrace.nRaw );

    
    But->Npts = pTrace.nRaw;
    if(But->Npts>MAXTRACELTH) {
        logit("e","%s Trace: %s Too many points: %d\n", whoami, SCNtxt, But->Npts);
        But->Npts = MAXTRACELTH;
    }
    for(i=0;i<But->Npts;i++) Data[i] = pTrace.rawData[i];
    But->Mean = 0.0;
    if(pTrace.nGaps > 500) ret = 4;

    return ret;
}

/********************************************************************
 *  WSReqBin                                                        *
 *                                                                  *
 *   k - waveserver index                                           *
 ********************************************************************/
int WSReqBin(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt)
{
    char     server[wsADRLEN*3], whoami[50];
    int      i, kk, io, retry;
    int      isamp, nsamp, gap0, success, ret, WSDebug = 0;          
    long     iEnd, npoints;
    WS_MENU  menu = NULL;
    WS_PSCNL  pscn = NULL;
    double   mean, traceEnd, samprate;
    long    *longPtr;
    short   *shortPtr;
    
    TRACE_HEADER *pTH;
    TRACE_HEADER *pTH4;
    char tbuf[MAX_TRACEBUF_SIZ];
    GAP *pGap, *newGap;
    
    sprintf(whoami, " %s: %s: ", But->mod, "WSReqBin");
    WSDebug = But->WSDebug;
    success = retry = 0;
    
gettrace:
    menu = NULL;
    /*    Put out WaveServer request here and wait for response */
    /* Get the trace
     ***************/
    /* rummage through all the servers we've been told about */
    if ( (wsSearchSCN( request, &menu, &pscn, &But->menu_queue[k]  )) == WS_ERR_NONE ) {
        strcpy(server, menu->addr);
        strcat(server, "  ");
        strcat(server, menu->port);
    } else {
        strcpy(server, "unknown");
    }
    
/* initialize the global trace buffer, freeing old GAP structures. */
    pTrace->nRaw  = 0L;
    pTrace->delta     = 0.0;
    pTrace->starttime = 0.0;
    pTrace->endtime   = 0.0;
    pTrace->nGaps = 0;

    /* Clear out the gap list */
    pTrace->nGaps = 0;
    while ( (pGap = pTrace->gapList) != (GAP *)NULL) {
        pTrace->gapList = pGap->next;
        free(pGap);
    }
 
    if(WSDebug) {     
        logit("e","\n%s Issuing request to wsGetTraceBin: server: %s Socket: %d.\n", 
             whoami, server, menu->sock);
        logit("e","    %s %f %f %d\n", SCNtxt,
             request->reqStarttime, request->reqEndtime, request->timeout);
    }

    io = wsGetTraceBin(request, &But->menu_queue[k], But->wsTimeout);
    
    if (io == WS_ERR_NONE ) {
        if(WSDebug) {
            logit("e"," %s server: %s trace %s: went ok first time. Got %ld bytes\n",
                whoami, server, SCNtxt, request->actLen); 
            logit("e","        actStarttime=%lf, actEndtime=%lf, actLen=%ld, samprate=%lf\n",
                  request->actStarttime, request->actEndtime, request->actLen, request->samprate);
        }
    }
    else {
        switch(io) {
        case WS_ERR_EMPTY_MENU:
        case WS_ERR_SCNL_NOT_IN_MENU:
        case WS_ERR_BUFFER_OVERFLOW:
            if (io == WS_ERR_EMPTY_MENU ) 
                logit("e"," %s server: %s No menu found.  We might as well quit.\n", whoami, server); 
            if (io == WS_ERR_SCNL_NOT_IN_MENU ) 
                logit("e"," %s server: %s Trace %s not in menu\n", whoami, server, SCNtxt);
            if (io == WS_ERR_BUFFER_OVERFLOW ) 
                logit("e"," %s server: %s Trace %s overflowed buffer. Fatal.\n", whoami, server, SCNtxt); 
            return 2;                /*   We might as well quit */
        
        case WS_ERR_PARSE:
        case WS_ERR_TIMEOUT:
        case WS_ERR_BROKEN_CONNECTION:
        case WS_ERR_NO_CONNECTION:
            sleep_ew(500);
            retry += 1;
            if (io == WS_ERR_PARSE )
                logit("e"," %s server: %s Trace %s: Couldn't parse server's reply. Try again.\n", 
                        whoami, server, SCNtxt); 
            if (io == WS_ERR_TIMEOUT )
                logit("e"," %s server: %s Trace %s: Timeout to wave server. Try again.\n", whoami, server, SCNtxt); 
            if (io == WS_ERR_BROKEN_CONNECTION ) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Broken connection to wave server. Try again.\n", 
                whoami, server, SCNtxt); 
            }
            if (io == WS_ERR_NO_CONNECTION ) {
                if(WSDebug || retry>1) 
                    logit("e"," %s server: %s Trace %s: No connection to wave server.\n", whoami, server, SCNtxt); 
                if(WSDebug) logit("e"," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
            }
            ret = wsAttachServer( menu, But->wsTimeout );
            if(ret == WS_ERR_NONE && retry < But->RetryCount) goto gettrace;
            if(WSDebug) {   
                switch(ret) {
                case WS_ERR_NO_CONNECTION:
                    logit("e"," %s server: %s: No connection to wave server.\n", whoami, server); 
                    break;
                case WS_ERR_SOCKET:
                    logit("e"," %s server: %s: Socket error.\n", whoami, server); 
                    break;
                case WS_ERR_INPUT:
                    logit("e"," %s server: %s: Menu missing.\n", whoami, server); 
                    break;
                default:
                    logit("e"," %s server: %s: wsAttachServer error %d.\n", whoami, server, ret); 
                }
            }
            return 2;                /*   We might as well quit */
        
        case WS_WRN_FLAGGED:
            if((int)strlen(&request->retFlag)>0) {
                if(WSDebug)  
                logit("e","%s server: %s Trace %s: return flag from wsGetTraceBin: <%c>\n %.50s\n", 
                        whoami, server, SCNtxt, request->retFlag, But->TraceBuf);
            }
            if(WSDebug) logit("e"," %s server: %s Trace %s: No trace available. Wave server returned %c\n", 
                whoami, server, SCNtxt, request->retFlag); 
        /*    break; */
            return 2;                /*   We might as well quit */
        
        default:
            logit( "et","%s server: %s Failed.  io = %d\n", whoami, server, io );
        }
    }
    
    /* Transfer trace data from TRACE_BUF packets into Trace buffer */
    traceEnd = (request->actEndtime < request->reqEndtime) ?  request->actEndtime :
                                                              request->reqEndtime;
    pTH  = (TRACE_HEADER *)request->pBuf;
    pTH4 = (TRACE_HEADER *)tbuf;
    /*
    * Swap to local byte-order. Note that we will be calling this function
    * twice for the first TRACE_BUF packet; this is OK as the second call
    * will have no effect.
    */

    memcpy( pTH4, pTH, sizeof(TRACE_HEADER) );
    if(WSDebug) logit( "e","%s server: %s Make Local\n", whoami, server, io );
    if (WaveMsgMakeLocal(pTH4) == -1) {
        logit("et", "%s server: %s %s.%s.%s unknown datatype <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
        return 5;
    }
    if (WaveMsgMakeLocal(pTH4) == -2) {
        logit("et", "%s server: %s %s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
    /*  return 5;  */
    }

    if(WSDebug) logit("e"," %s server: %s Trace %s: Data has samprate %g.\n", 
                     whoami, server, SCNtxt, pTH4->samprate); 
    if (pTH4->samprate < 0.1) {
        logit("et", "%s server: %s %s.%s.%s (%s) has zero samplerate (%g); skipping trace\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, SCNtxt, pTH4->samprate);
        return 5;
    }
    But->samp_sec = pTH4->samprate<=0? 100L:(long)pTH4->samprate;

    pTrace->delta = 1.0/But->samp_sec;
    samprate = But->samp_sec;   /* Save rate of first packet to compare with later packets */
    pTrace->starttime = request->reqStarttime;
    /* Set Trace endtime so it can be used to test for gap at start of data */
    pTrace->endtime = ( (pTH4->starttime < request->reqStarttime) ?
                        pTH4->starttime : request->reqStarttime) - 0.5*pTrace->delta ;
    if(WSDebug) logit("e"," pTH->starttime: %f request->reqStarttime: %f delta: %f endtime: %f.\n", 
                     pTH4->starttime, request->reqStarttime, pTrace->delta, pTrace->endtime); 

  /* Look at all the retrieved TRACE_BUF packets 
   * Note that we must copy each tracebuf from the big character buffer
   * to the TRACE_BUF structure pTH4.  This is because of the occasionally 
   * seen case of a channel putting an odd number of i2 samples into
   * its tracebufs!  */
    kk = 0;
    while( pTH < (TRACE_HEADER *)(request->pBuf + request->actLen) ) {
        memcpy( pTH4, pTH, sizeof(TRACE_HEADER) );
         /* Swap bytes to local order */
	    if (WaveMsgMakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsgMakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
	    /*  return 5;  */
	    }
    
        nsamp = pTH4->nsamp;
        memcpy( pTH4, pTH, sizeof(TRACE_HEADER) + nsamp*4 );
         /* Swap bytes to local order */
	    if (WaveMsgMakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsgMakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
	    /*  return 5;  */
	    }
    
        if ( fabs(pTH4->samprate - samprate) > 1.0) {
            logit("et", "%s <%s.%s.%s samplerate change: %f - %f; discarding trace\n",
                whoami, pTH4->sta, pTH4->chan, pTH4->net, samprate, pTH4->samprate);
            return 5;
        }
    
    /* Check for gap */
        if (pTrace->endtime + 1.5 * pTrace->delta < pTH4->starttime) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Gap detected.\n", 
                        whoami, server, SCNtxt); 
            if ( (newGap = (GAP *)calloc(1, sizeof(GAP))) == (GAP *)NULL) {
                logit("et", "getTraceFromWS: out of memory for GAP struct\n");
                return -1;
            }
            newGap->starttime = pTrace->endtime + pTrace->delta;
            newGap->gapLen = pTH4->starttime - newGap->starttime;
            newGap->firstSamp = pTrace->nRaw;
            newGap->lastSamp  = pTrace->nRaw + (long)( (newGap->gapLen * samprate) - 0.5);
            if(WSDebug) logit("e"," starttime: %f gaplen: %f firstSamp: %d lastSamp: %d.\n", 
                newGap->starttime, newGap->gapLen, newGap->firstSamp, newGap->lastSamp); 
            /* Put GAP struct on list, earliest gap first */
            if (pTrace->gapList == (GAP *)NULL)
                pTrace->gapList = newGap;
            else
                pGap->next = newGap;
            pGap = newGap;  /* leave pGap pointing at the last GAP on the list */
            pTrace->nGaps++;

            /* Advance the Trace pointers past the gap; maybe gap will get filled */
            pTrace->nRaw = newGap->lastSamp + 1;
            pTrace->endtime += newGap->gapLen;
        }
    
        isamp = (pTrace->starttime > pTH4->starttime)?
                (long)( 0.5 + (pTrace->starttime - pTH4->starttime) * samprate):0;

        if (request->reqEndtime < pTH4->endtime) {
            nsamp = pTH4->nsamp - (long)( 0.5 * (pTH4->endtime - request->reqEndtime) * samprate);
            pTrace->endtime = request->reqEndtime;
        } 
        else {
            nsamp = pTH4->nsamp;
            pTrace->endtime = pTH4->endtime;
        }

    /* Assume trace data is integer valued here, long or short */    
        if (pTH4->datatype[1] == '4') {
            longPtr=(long*) ((char*)pTH4 + sizeof(TRACE_HEADER) + isamp * 4);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *longPtr;
                longPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF message */
            pTH = (TRACE_HEADER *)((char *)pTH + sizeof(TRACE_HEADER) + pTH4->nsamp * 4);
        }
        else {   /* pTH->datatype[1] == 2, we assume */
            shortPtr=(short*) ((char*)pTH4 + sizeof(TRACE_HEADER) + isamp * 2);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *shortPtr;
                shortPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF packets */
            pTH = (TRACE_HEADER *)((char *)pTH + sizeof(TRACE_HEADER) + pTH4->nsamp * 2);
        }
    }  /* End of loop over TRACE_BUF packets */
  

    if (io == WS_ERR_NONE ) success = 1;

    return success;
}


/********************************************************************
 *  WSReqBin2                                                       *
 *                                                                  *
 *  Retrieves binary data from location code enabled waveservers.   *
 *   k - waveserver index                                           *
 ********************************************************************/
int WSReqBin2(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt)
{
    char     server[wsADRLEN*3], whoami[50];
    int      i, kk, io, retry;
    int      isamp, nsamp, gap0, success, ret, WSDebug = 0;          
    long     iEnd, npoints;
    WS_MENU  menu = NULL;
    WS_PSCNL  pscn = NULL;
    double   mean, traceEnd, samprate;
    long    *longPtr;
    short   *shortPtr;
    
    TRACE2_HEADER *pTH;
    TRACE2_HEADER *pTH4;
    char tbuf[MAX_TRACEBUF_SIZ];
    GAP *pGap, *newGap;
    
    sprintf(whoami, " %s: %s: ", But->mod, "WSReqBin");
    WSDebug = But->WSDebug;
    success = retry = 0;
    
gettrace:
    menu = NULL;
    /*    Put out WaveServer request here and wait for response */
    /* Get the trace
     ***************/
    /* rummage through all the servers we've been told about */
    if ( (wsSearchSCNL( request, &menu, &pscn, &But->menu_queue[k]  )) == WS_ERR_NONE ) {
        strcpy(server, menu->addr);
        strcat(server, "  ");
        strcat(server, menu->port);
    } else {
        strcpy(server, "unknown");
    }
    
/* initialize the global trace buffer, freeing old GAP structures. */
    pTrace->nRaw  = 0L;
    pTrace->delta     = 0.0;
    pTrace->starttime = 0.0;
    pTrace->endtime   = 0.0;
    pTrace->nGaps = 0;

    /* Clear out the gap list */
    pTrace->nGaps = 0;
    while ( (pGap = pTrace->gapList) != (GAP *)NULL) {
        pTrace->gapList = pGap->next;
        free(pGap);
    }
 
    if(WSDebug) {     
        logit("e","\n%s Issuing request to wsGetTraceBinL: server: %s Socket: %d.\n", 
             whoami, server, menu->sock);
        logit("e","    %s %f %f %d\n", SCNtxt,
             request->reqStarttime, request->reqEndtime, request->timeout);
    }

    io = wsGetTraceBinL(request, &But->menu_queue[k], But->wsTimeout);
    
    if (io == WS_ERR_NONE ) {
        if(WSDebug) {
            logit("e"," %s server: %s trace %s: went ok first time. Got %ld bytes\n",
                whoami, server, SCNtxt, request->actLen); 
            logit("e","        actStarttime=%lf, actEndtime=%lf, actLen=%ld, samprate=%lf\n",
                  request->actStarttime, request->actEndtime, request->actLen, request->samprate);
        }
    }
    else {
        switch(io) {
        case WS_ERR_EMPTY_MENU:
        case WS_ERR_SCNL_NOT_IN_MENU:
        case WS_ERR_BUFFER_OVERFLOW:
            if (io == WS_ERR_EMPTY_MENU ) 
                logit("e"," %s server: %s No menu found.  We might as well quit.\n", whoami, server); 
            if (io == WS_ERR_SCNL_NOT_IN_MENU ) 
                logit("e"," %s server: %s Trace %s not in menu\n", whoami, server, SCNtxt);
            if (io == WS_ERR_BUFFER_OVERFLOW ) 
                logit("e"," %s server: %s Trace %s overflowed buffer. Fatal.\n", whoami, server, SCNtxt); 
            return 2;                /*   We might as well quit */
        
        case WS_ERR_PARSE:
        case WS_ERR_TIMEOUT:
        case WS_ERR_BROKEN_CONNECTION:
        case WS_ERR_NO_CONNECTION:
            sleep_ew(500);
            retry += 1;
            if (io == WS_ERR_PARSE )
                logit("e"," %s server: %s Trace %s: Couldn't parse server's reply. Try again.\n", 
                        whoami, server, SCNtxt); 
            if (io == WS_ERR_TIMEOUT )
                logit("e"," %s server: %s Trace %s: Timeout to wave server. Try again.\n", whoami, server, SCNtxt); 
            if (io == WS_ERR_BROKEN_CONNECTION ) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Broken connection to wave server. Try again.\n", 
                whoami, server, SCNtxt); 
            }
            if (io == WS_ERR_NO_CONNECTION ) {
                if(WSDebug || retry>1) 
                    logit("e"," %s server: %s Trace %s: No connection to wave server.\n", whoami, server, SCNtxt); 
                if(WSDebug) logit("e"," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
            }
            ret = wsAttachServer( menu, But->wsTimeout );
            if(ret == WS_ERR_NONE && retry < But->RetryCount) goto gettrace;
            if(WSDebug) {   
                switch(ret) {
                case WS_ERR_NO_CONNECTION:
                    logit("e"," %s server: %s: No connection to wave server.\n", whoami, server); 
                    break;
                case WS_ERR_SOCKET:
                    logit("e"," %s server: %s: Socket error.\n", whoami, server); 
                    break;
                case WS_ERR_INPUT:
                    logit("e"," %s server: %s: Menu missing.\n", whoami, server); 
                    break;
                default:
                    logit("e"," %s server: %s: wsAttachServer error %d.\n", whoami, server, ret); 
                }
            }
            return 2;                /*   We might as well quit */
        
        case WS_WRN_FLAGGED:
            if((int)strlen(&request->retFlag)>0) {
                if(WSDebug)  
                logit("e","%s server: %s Trace %s: return flag from wsGetTraceBin: <%c>\n %.50s\n", 
                        whoami, server, SCNtxt, request->retFlag, But->TraceBuf);
            }
            if(WSDebug) logit("e"," %s server: %s Trace %s: No trace available. Wave server returned %c\n", 
                whoami, server, SCNtxt, request->retFlag); 
        /*    break; */
            return 2;                /*   We might as well quit */
        
        default:
            logit( "et","%s server: %s Failed.  io = %d\n", whoami, server, io );
        }
    }
    
    /* Transfer trace data from TRACE_BUF packets into Trace buffer */
    traceEnd = (request->actEndtime < request->reqEndtime) ?  request->actEndtime :
                                                              request->reqEndtime;
    pTH  = (TRACE2_HEADER *)request->pBuf;
    pTH4 = (TRACE2_HEADER *)tbuf;
    /*
    * Swap to local byte-order. Note that we will be calling this function
    * twice for the first TRACE_BUF packet; this is OK as the second call
    * will have no effect.
    */

    memcpy( pTH4, pTH, sizeof(TRACE2_HEADER) );
    if(WSDebug) logit( "e","%s server: %s Make Local\n", whoami, server, io );
    if (WaveMsg2MakeLocal(pTH4) == -1) {
        logit("et", "%s server: %s %s.%s.%s.%s unknown datatype <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
        return 5;
    }
    if (WaveMsg2MakeLocal(pTH4) == -2) {
        logit("et", "%s server: %s %s.%s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
    /*  return 5;  */
    }

    if(WSDebug) logit("e"," %s server: %s Trace %s: Data has samprate %g.\n", 
                     whoami, server, SCNtxt, pTH4->samprate); 
    if (pTH4->samprate < 0.1) {
        logit("et", "%s server: %s %s.%s.%s.%s (%s) has zero samplerate (%g); skipping trace\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, SCNtxt, pTH4->samprate);
        return 5;
    }
    But->samp_sec = pTH4->samprate<=0? 100L:(long)pTH4->samprate;

    pTrace->delta = 1.0/But->samp_sec;
    samprate = But->samp_sec;   /* Save rate of first packet to compare with later packets */
    pTrace->starttime = request->reqStarttime;
    /* Set Trace endtime so it can be used to test for gap at start of data */
    pTrace->endtime = ( (pTH4->starttime < request->reqStarttime) ?
                        pTH4->starttime : request->reqStarttime) - 0.5*pTrace->delta ;
    if(WSDebug) logit("e"," pTH->starttime: %f request->reqStarttime: %f delta: %f endtime: %f.\n", 
                     pTH4->starttime, request->reqStarttime, pTrace->delta, pTrace->endtime); 

  /* Look at all the retrieved TRACE_BUF packets 
   * Note that we must copy each tracebuf from the big character buffer
   * to the TRACE_BUF structure pTH4.  This is because of the occasionally 
   * seen case of a channel putting an odd number of i2 samples into
   * its tracebufs!  */
    kk = 0;
    while( pTH < (TRACE2_HEADER *)(request->pBuf + request->actLen) ) {
        memcpy( pTH4, pTH, sizeof(TRACE2_HEADER) );
         /* Swap bytes to local order */
	    if (WaveMsg2MakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsg2MakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	    /*  return 5;  */
	    }
    
        nsamp = pTH4->nsamp;
        memcpy( pTH4, pTH, sizeof(TRACE2_HEADER) + nsamp*4 );
         /* Swap bytes to local order */
	    if (WaveMsg2MakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsg2MakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	    /*  return 5;  */
	    }
    
        if ( fabs(pTH4->samprate - samprate) > 1.0) {
            logit("et", "%s <%s.%s.%s.%s samplerate change: %f - %f; discarding trace\n",
                whoami, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, samprate, pTH4->samprate);
            return 5;
        }
    
    /* Check for gap */
        if (pTrace->endtime + 1.5 * pTrace->delta < pTH4->starttime) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Gap detected.\n", 
                        whoami, server, SCNtxt); 
            if ( (newGap = (GAP *)calloc(1, sizeof(GAP))) == (GAP *)NULL) {
                logit("et", "getTraceFromWS: out of memory for GAP struct\n");
                return -1;
            }
            newGap->starttime = pTrace->endtime + pTrace->delta;
            newGap->gapLen = pTH4->starttime - newGap->starttime;
            newGap->firstSamp = pTrace->nRaw;
            newGap->lastSamp  = pTrace->nRaw + (long)( (newGap->gapLen * samprate) - 0.5);
            if(WSDebug) logit("e"," starttime: %f gaplen: %f firstSamp: %d lastSamp: %d.\n", 
                newGap->starttime, newGap->gapLen, newGap->firstSamp, newGap->lastSamp); 
            /* Put GAP struct on list, earliest gap first */
            if (pTrace->gapList == (GAP *)NULL)
                pTrace->gapList = newGap;
            else
                pGap->next = newGap;
            pGap = newGap;  /* leave pGap pointing at the last GAP on the list */
            pTrace->nGaps++;

            /* Advance the Trace pointers past the gap; maybe gap will get filled */
            pTrace->nRaw = newGap->lastSamp + 1;
            pTrace->endtime += newGap->gapLen;
        }
    
        isamp = (pTrace->starttime > pTH4->starttime)?
                (long)( 0.5 + (pTrace->starttime - pTH4->starttime) * samprate):0;

        if (request->reqEndtime < pTH4->endtime) {
            nsamp = pTH4->nsamp - (long)( 0.5 * (pTH4->endtime - request->reqEndtime) * samprate);
            pTrace->endtime = request->reqEndtime;
        } 
        else {
            nsamp = pTH4->nsamp;
            pTrace->endtime = pTH4->endtime;
        }

    /* Assume trace data is integer valued here, long or short */    
        if (pTH4->datatype[1] == '4') {
            longPtr=(long*) ((char*)pTH4 + sizeof(TRACE2_HEADER) + isamp * 4);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *longPtr;
                longPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF message */
            pTH = (TRACE2_HEADER *)((char *)pTH + sizeof(TRACE2_HEADER) + pTH4->nsamp * 4);
        }
        else {   /* pTH->datatype[1] == 2, we assume */
            shortPtr=(short*) ((char*)pTH4 + sizeof(TRACE2_HEADER) + isamp * 2);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *shortPtr;
                shortPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF packets */
            pTH = (TRACE2_HEADER *)((char *)pTH + sizeof(TRACE2_HEADER) + pTH4->nsamp * 2);
        }
    }  /* End of loop over TRACE_BUF packets */


    if (io == WS_ERR_NONE ) success = 1;

    return success;
}


/**********************************************************************
 * IsDST : Determine if we are using daylight savings time.           *
 *         This is a valid function for US and Canada thru 31/12/2099 *
 *                                                                    *
 *         Modified 02/16/07 to reflect political changes. JHL        *
 *         Fixed 03/05/08 to run correctly. JHL                       *
 *                                                                    *
 **********************************************************************/
int IsDST(int year, int month, int day)
{
    int     i, leapyr, day1, day2, num, jd, jd1, jd2, jd3, jd4;
    int     dpm[] = {31,28,31,30,31,30,31,31,30,31,30,31};
    
    leapyr = 0;
    if((year-4*(year/4))==0 && (year-100*(year/100))!=0) leapyr = 1;
    if((year-400*(year/400))==0) leapyr = 1;
    
    num = ((year-1900)*5)/4 + 5;
    num = num - 7*(num/7);       /* day # of 1 March                  */
    	day1 = num-1;      
    	if(day1<=0) day1 = day1 + 7;  /* day # (-1) of 1 March             */
    	day1 = 8 - day1;              /* day # of 1st Sunday in March      */
    	jd3 = 59 + 7 + day1 + leapyr; /* Julian day of 2nd Sunday in March */
    day1 = num + 2;
    if(day1>7) day1 = day1 - 7;  /* day # (-1) of 1 April             */
    day1 = 8 - day1;             /* date of 1st Sunday in April       */
    jd1 = 90 + day1 + leapyr;    /* Julian day of 1st Sunday in April */
    day2 = num + 3;
    if(day2>7) day2 = day2 - 7;  /* day # (-1) of 1 Oct               */
    day2 = 8 - day2;             /* date of 1st Sunday in Oct         */
    while(day2<=31) day2 += 7;   /* date of 1st Sunday in Nov         */
    	jd4 = 273 + leapyr + day2;   /* Julian day of 1st Sunday in Nov  */
    day2 -= 7;                   /* date of last Sunday in Oct        */
    jd2 = 273 + day2 + leapyr;   /* Julian day of last Sunday in Oct  */
    
    jd = day;
    for(i=0;i<month-1;i++) jd += dpm[i];
    if(month>2) jd += leapyr;
    
    if(jd>=jd1 && jd<jd2) return 1; 
    if(jd>=jd3 && jd<jd4) return 1;
    
    return 0;
}


/**********************************************************************
 * Decode_Time : Decode time from seconds since 1970                  *
 *                                                                    *
 **********************************************************************/
void Decode_Time( double secs, TStrct *Time)
{
    struct Greg  g;
    long    minute;
    double  sex;

    Time->Time = secs;
    secs += sec1970;
    Time->Time1600 = secs;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    Time->Year  = g.year;
    Time->Month = g.month;
    Time->Day   = g.day;
    Time->Hour  = g.hour;
    Time->Min   = g.minute;
    Time->Sec   = sex;
}


/**********************************************************************
 * Encode_Time : Encode time to seconds since 1970                    *
 *                                                                    *
 **********************************************************************/
void Encode_Time( double *secs, TStrct *Time)
{
    struct Greg    g;

    g.year   = Time->Year;
    g.month  = Time->Month;
    g.day    = Time->Day;
    g.hour   = Time->Hour;
    g.minute = Time->Min;
    *secs    = 60.0 * (double) julmin(&g) + Time->Sec - sec1970;
}


/**********************************************************************
 * date22 : Calculate 22 char date in the form Jan23,1988 12:34 12.21 *
 *          from the julian seconds.  Remember to leave space for the *
 *          string termination (NUL).                                 *
 **********************************************************************/
void date22( double secs, char *c22)
{
    char *cmo[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    struct Greg  g;
    long    minute;
    double  sex;

    secs += sec1970;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    sprintf(c22, "%3s%2d,%4d %.2d:%.2d:%05.2f",
            cmo[g.month-1], g.day, g.year, g.hour, g.minute, sex);
}


/*************************************************************************
 *   SetPlot sets globals for the current plot.                          *
 *                                                                       *
 *************************************************************************/
void SetPlot(double Xsize, double Ysize)
{
    xsize = Xsize;    
    ysize = Ysize;
}


/*************************************************************************
 *   ixq calculates the x pixel location.                                *
 *   a is the distance in inches from the left margin.                   *
 *************************************************************************/
int ixq(double a)
{
    double   val;
    int      i;
    
    val  = (a + XLMARGIN);
    if(val > xsize) val = xsize;
    if(val < 0.0)   val = 0.0;
    i = (int)(val*72.0);
    return i;
}


/*************************************************************************
 *   iyq calculates the y pixel location.                                *
 *   a is the distance in inches up from the bottom margin.              *
 *                      <or>                                             *
 *   a is the distance in inches down from the top margin.               *
 *************************************************************************/
int iyq(double a)
{
    double   val;
    int      i;
    
    val = plot_up?
         (ysize - YBMARGIN - a) : /* time increases up from bottom */
                  YTMargin + a;   /* times increases down from top */
    
    if(val > ysize) val = ysize;
    if(val < 0.0)   val = 0.0;
    i = (int)(val*72.0);
    return i;
}


/********************************************************************
 *    Erase_Labels clears the non-data areas on the plot.           *
 *                                                                  *
 *    x1    x2                  x3     x4                           *
 *  y1+-----+-------------------+------+                            *
 *    |     |                   |      |                            *
 *  y2+-----+-------------------+------+                            *
 *    |     |                   |   |  |                            *
 *  y3+-----+-------------------+------+                            *
 *    |     |                   |      |                            *
 *  y4+-----+-------------------+------+                            *
 *                                                                  *
 ********************************************************************/

void Erase_Labels(Global *But, LocInfo *plot, long white)
{
    int   i, x4;
    
    i = But->Current_Plot;
    x4 = But->plt[i].DClabeled? ixq(But->axexmax + 0.45):plot->x4;
    /* Erase the four rectangles surrounding the data *
     **************************************************/
    white = But->gcolor[WHITE];
    gdImageFilledRectangle( plot->Gif, plot->x1, plot->y1, plot->x4, plot->y2, white);
    gdImageFilledRectangle( plot->Gif, plot->x1, plot->y1, plot->x2, plot->y4, white);
    gdImageFilledRectangle( plot->Gif, plot->x3, plot->y1, x4, plot->y4, white);
    gdImageFilledRectangle( plot->Gif, plot->x1, plot->y3, plot->x4, plot->y4, white);

}


/****************************************************************************
 *  Get_Sta_Info(Global *But);                                              *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Sta_Info(Global *But)
{
    char    whoami[50], *com, *str, ns, ew;
    int     i, j, k, nfiles, success;
    double  dlat, mlat, dlon, mlon;

    sprintf(whoami, "%s: %s: ", But->mod, "Get_Sta_Info");
    
    ns = 'N';
    ew = 'W';
    But->NSCN = 0;
    for(k=0;k<But->nStaDB;k++) {
            /* Open the main station file
             ****************************/
        nfiles = k_open( But->stationList[k] );
        if(nfiles == 0) {
            fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, But->stationList[k] );
            exit( -1 );
        }

            /* Process all command files
             ***************************/
        while(nfiles > 0) {  /* While there are command files open */
            while(k_rd())  {      /* Read next line from active file  */
                com = k_str();         /* Get the first token from line */

                    /* Ignore blank lines & comments
                     *******************************/
                if( !com )           continue;
                if( com[0] == '#' )  continue;

                    /* Open a nested configuration file
                     **********************************/
                if( com[0] == '@' ) {
                    success = nfiles+1;
                    nfiles  = k_open(&com[1]);
                    if ( nfiles != success ) {
                        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                        exit( -1 );
                    }
                    continue;
                }

                /* Process anything else as a channel descriptor
                 ***********************************************/

                if( But->NSCN >= MAXCHANNELS ) {
                    fprintf(stderr, "%s Too many channel entries in <%s>", 
                             whoami, But->stationList[k] );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAXCHANNELS );
                    exit( -1 );
                }
                j = But->NSCN;
                
                    /* S C N */
                strncpy( But->Chan[j].Site, com,  6);
                str = k_str();
                if(str) strncpy( But->Chan[j].Net,  str,  2);
                str = k_str();
                if(str) strncpy( But->Chan[j].Comp, str, 3);
                for(i=0;i<6;i++) if(But->Chan[j].Site[i]==' ') But->Chan[j].Site[i] = 0;
                for(i=0;i<2;i++) if(But->Chan[j].Net[i]==' ')  But->Chan[j].Net[i]  = 0;
                for(i=0;i<3;i++) if(But->Chan[j].Comp[i]==' ') But->Chan[j].Comp[i] = 0;
                But->Chan[j].Comp[3] = But->Chan[j].Net[2] = But->Chan[j].Site[5] = 0;
                if(But->stationListType[k] == 1) {
                    str = k_str();
                    if(str) strncpy( But->Chan[j].Loc, str, 2);
                    for(i=0;i<2;i++) if(But->Chan[j].Loc[i]==' ')  But->Chan[j].Loc[i]  = 0;
                    But->Chan[j].Loc[2] = 0;
                }


                    /* Lat Lon Elev */
                if(But->stationListType[k] == 0) {
                    dlat = k_int();
                    mlat = k_val();
                    
                    dlon = k_int();
                    mlon = k_val();
                    
                    But->Chan[j].Elev = k_val();
                    
                        /* convert to decimal degrees */
                    if ( dlat < 0 ) dlat = -dlat;
                    if ( dlon < 0 ) dlon = -dlon;
                    But->Chan[j].Lat = dlat + (mlat/60.0);
                    But->Chan[j].Lon = dlon + (mlon/60.0);
                        /* make south-latitudes and west-longitudes negative */
                    if ( ns=='s' || ns=='S' )               But->Chan[j].Lat = -But->Chan[j].Lat;
                    if ( ew=='w' || ew=='W' || ew==' ' )    But->Chan[j].Lon = -But->Chan[j].Lon;
                } else if(But->stationListType[k] == 1) {
                    But->Chan[j].Lat  = k_val();
                    But->Chan[j].Lon  = k_val();
                    But->Chan[j].Elev = k_val();
                }

         /*       str = k_str();      Blow past the subnet */
                
                But->Chan[j].Inst_type = k_int();
                But->Chan[j].Inst_gain = k_val();
                But->Chan[j].GainFudge = k_val();
                But->Chan[j].Sens_type = k_int();
                But->Chan[j].Sens_unit = k_int();
                But->Chan[j].Sens_gain = k_val();
                But->Chan[j].SiteCorr  = k_val();
                        
                if(But->Chan[j].Sens_unit == 3) But->Chan[j].Sens_gain /= 981.0;
               
                But->Chan[j].sensitivity = (1000000.0*But->Chan[j].Sens_gain/But->Chan[j].Inst_gain)*But->Chan[j].GainFudge;    /*    sensitivity counts/units        */
                
                if(But->stationListType[k] == 1) But->Chan[j].ShkQual = k_int();
                
                if (k_err()) {
                    logit("e", "%s Error decoding line in station file\n%s\n  exiting!\n", whoami, k_get() );
                    exit( -1 );
                }
         /*>Comment<*/
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].SiteName, str );
                    
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].Descript, str );
                    
                But->NSCN++;
            }
            nfiles = k_close();
        }
    }
}


/*************************************************************************
 *  Put_Sta_Info(Global *But);                                           *
 *  Retrieve all the information available about station i               *
 *  and put it into an internal structure for reference.                 *
 *  This should eventually be a call to the database; for now we must    *
 *  supply an ascii file with all the info.                              *
 *************************************************************************/
int Put_Sta_Info(int i, Global *But)
{
    char    whoami[50];
    short   j;

    sprintf(whoami, " %s: %s: ", But->mod, "Put_Sta_Info");
    
    for(j=0;j<But->NSCN;j++) {
        if(strcmp(But->Chan[j].Site, But->plt[i].Site )==0 && 
           strcmp(But->Chan[j].Comp, But->plt[i].Comp)==0 && 
           strcmp(But->Chan[j].Net,  But->plt[i].Net )==0) {
            if(strcmp("**",  But->plt[i].Loc )==0) {
            	strcpy(But->plt[i].Loc, But->Chan[j].Loc);
                sprintf(But->plt[i].SCNnam, "%s_%s_%s_%s_00", But->plt[i].Site, But->plt[i].Comp, But->plt[i].Net, But->plt[i].Loc);
		    	strcpy(string,  But->plt[i].Loc);
		    	if(strcmp(string,  "--" )==0) strcpy(string,  "  ");
		        sprintf(But->plt[i].SCNtxt, "%s %s %s %s", 
		        	But->plt[i].Site, But->plt[i].Comp, 
		        	But->plt[i].Net, string);
            }
            if(strcmp(But->Chan[j].Loc,  But->plt[i].Loc )==0) {
	            strcpy(But->plt[i].Comment, But->Chan[j].SiteName);
	            But->plt[i].Inst_type  = But->Chan[j].Inst_type;
	            But->plt[i].GainFudge  = But->Chan[j].GainFudge;
	            But->plt[i].Sens_type  = But->Chan[j].Sens_type;
	            But->plt[i].Sens_gain  = But->Chan[j].Sens_gain;
	            But->plt[i].Sens_unit  = But->Chan[j].Sens_unit;
	            But->plt[i].sensitivity  = But->Chan[j].sensitivity;
	            if(But->plt[i].Sens_unit<=1) But->plt[i].Scaler = But->plt[i].Scale*1000.0;
	            if(But->plt[i].Sens_unit==2) But->plt[i].Scaler = But->plt[i].Scale*10000.0;
	            if(But->plt[i].Sens_unit==3) But->plt[i].Scaler = But->plt[i].Scale*10.0;
		        if(But->Debug) {
		            logit("e", "\n%s %s %s. %d %d %d %f %f %f %f %s %s\n\n", 
		                whoami, But->plt[i].SCNtxt, But->plt[i].SCNnam, But->plt[i].Inst_type, But->plt[i].Sens_type, 
		                But->plt[i].Sens_unit, But->plt[i].Sens_gain, But->plt[i].GainFudge, But->plt[i].sensitivity, 
		                But->plt[i].Scale, But->plt[i].Comment, But->plt[i].Descrip);
		        }   
	            return 1;
            }
        }
    }
    strcpy(But->plt[i].Comment, "");
    But->plt[i].Inst_type  = 0;
    But->plt[i].GainFudge  = 1;
    But->plt[i].Sens_type  = 0;
    But->plt[i].Sens_gain  = 1.0;
    But->plt[i].Sens_unit  = 0;
    But->plt[i].sensitivity  = 1000000.0;    /* Default: 1 volt/unit & 1 microvolt/count */
    But->plt[i].Scaler = But->plt[i].Scale*1000.0;
    if(But->Debug) {
        logit("e", "\n%s %s %s. %d %d %d %f %f %f %f %s\n\n", 
            whoami, But->plt[i].SCNtxt, But->plt[i].SCNnam, But->plt[i].Inst_type, But->plt[i].Sens_type, 
            But->plt[i].Sens_unit, But->plt[i].Sens_gain, But->plt[i].GainFudge, But->plt[i].sensitivity, 
            But->plt[i].Scale, But->plt[i].Comment);
    }   
    return 0;
}


/****************************************************************************
 *      config_me() process command file using kom.c functions              *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void config_me( char* configfile, Global *But )
{
    char    whoami[50], *com, *str, string[20];
    char    init[20];       /* init flags, one byte for each required command */
    int     ncommand;       /* # of required commands you expect              */
    int     nmiss;          /* number of required commands that were missed   */
    int     i, j, n, nPlots, nfiles, success, dclabels, secsPerGulp;
    double  val;
    FILE    *in;
    gdImagePtr    im_in;

    sprintf(whoami, " %s: %s: ", But->mod, "config_me");
        /* Set one init flag to zero for each required command
         *****************************************************/
    ncommand = 8;
    for(i=0; i<ncommand; i++ )  init[i] = 0;
    But->nltargets = 0;
    But->Debug = Debug = But->WSDebug = 0;
    But->logo = But->logox = But->logoy = 0;
    But->nServer = But->nStaDB = But->nPlots = 0;
    But->DeciFlag = 0;
    But->SaveDrifts = 0;
    But->BuildOnRestart = 0;
    But->DaysAgo = But->OneDayOnly = 0;
    secsPerGulp = 0;
    But->RetryCount = 2;
    But->UpdateInt =   120;
    But->wsTimeout = WSTIMEOUT*1000;
    But->NoMenuCount = 0;
    But->NoGIFCount  = 0;
    But->UseDST = 0;
    strcpy(But->Prefix, "");
    plot_up = 0;
    dclabels = 0;
    But->Clip = 0;
    EWmodule = 1;

        /* Open the main configuration file
         **********************************/
    nfiles = k_open( configfile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, configfile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

                /* Process anything else as a command
                 ************************************/
/*0*/                 
            if( k_its("LogSwitch") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
/*1*/            
            else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModuleId , str );
                init[1] = 1;
            }
/*2*/            
            else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( InRingName, str );
                init[2] = 1;
            }
/*3*/ 
            else if( k_its("HeartBeatInt") ) {
                HeartBeatInterval = k_long();
                init[3] = 1;
            }

         /* wave server addresses and port numbers to get trace snippets from
          *******************************************************************/
/*4*/          
            else if( k_its("WaveServer") ) {
                if ( But->nServer >= MAX_WAVESERVERS ) {
                    fprintf( stderr, "%s Too many <WaveServer> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_WAVESERVERS );
                    return;
                }
                if( (long)(str=k_str()) != 0 )  strcpy(But->wsIp[But->nServer],str);
                if( (long)(str=k_str()) != 0 )  strcpy(But->wsPort[But->nServer],str);
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy(But->wsComment[But->nServer],str);
                But->nServer++;
                init[4]=1;
            }

        /* get Gif directory path/name
        *****************************/
/*5*/
            else if( k_its("GifDir") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. Gif directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return;
                }
                if(str) strcpy( But->GifDir , str );
                init[5] = 1;
            }

          /* get display parameters
        ************************************/
/*6*/
            else if( k_its("Display") ) {
         /*>01<*/
                But->HoursPerPlot = k_int();   /*  # of hours per gif image */
                if(But->HoursPerPlot <  1) But->HoursPerPlot =  1;
                if(But->HoursPerPlot > 24) But->HoursPerPlot = 24;
                But->HoursPerPlot  = 24/(24/But->HoursPerPlot);
         /*>02<*/
                But->mins = k_int();            /* # of minutes/line to display */
                if(But->mins < 1 || But->mins > 60) But->mins = 15;
                But->mins  = 60/(60/But->mins);
                
         /*>03<*/
                But->OldData = k_int();        /* Number of previous hours to retrieve */
                if(But->OldData < 0 || But->OldData > 168) But->OldData = 0;
                
         /*>04<*/
                val = k_val();            /* x-size of data plot in pixels */
                But->xpix = (val >= 100.0)? (int)val:(int)(72.0*val);
                
         /*>05<*/
                val = k_val();            /* y-size of data plot in pixels */
                But->ypix = (val >= 100.0)? (int)val:(int)(72.0*val);
                
         /*>06<*/
                But->ShowUTC  = k_int();    /* Flag to show UTC time */
                
         /*>07<*/
                But->UseLocal = k_int();    /* Flag to reference plot to local midnight */
                
                But->LinesPerHour = 60/But->mins;
                init[6] = 1;
            }

          /* get SCN parameters
        ************************************/
/*7*/
            else if( k_its("SCN") ) {
                if( But->nPlots >= MAXPLOTS ) {
                    fprintf( stderr, "%s Too many <Plot> commands in <%s>", whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAXPLOTS );
                    return;
                }
                nPlots = But->nPlots;
                But->plt[nPlots].DCcorr = But->Mean = 0.0;
                But->plt[nPlots].UseDST = 0;
                But->plt[nPlots].Filter = 0;
                
         /*>Site<*/
                str = k_str();
                if(str) strcpy( But->plt[nPlots].Site , str );
         /*>Comp<*/
                str = k_str();
                if(str) strcpy( But->plt[nPlots].Comp , str );
         /*>Net<*/
                str = k_str();
                if(str) strcpy( But->plt[nPlots].Net , str );
         /*>Loc<*/
                strcpy( But->plt[nPlots].Loc , "--" );
                strcpy( But->plt[nPlots].Loc , "**" );
         /*>04<*/
                But->plt[nPlots].LocalTime = k_int();    /* Hour offset of local time from UTC */
                if(But->plt[nPlots].LocalTime < -24 || But->plt[nPlots].LocalTime > 24) {
                    But->plt[nPlots].LocalTime = 0;
                }
         /*>05<*/
                str = k_str();                           /* Local Time ID e.g. PST */
                strncpy(But->plt[nPlots].LocalTimeID, str, (size_t)3);
                But->plt[nPlots].LocalTimeID[3] = '\0';
         /*>06<*/
                But->plt[nPlots].OldData = k_int();        /* Number of previous hours to retrieve */
                if(But->plt[nPlots].OldData < 0 || But->plt[nPlots].OldData > 168) {
                    But->plt[nPlots].OldData = 0;
                }
         /*>07<*/
                But->plt[nPlots].Scale = k_val();         /* Scale */
         /*>08<*/
                But->plt[nPlots].DCremoved = k_int();   /* DCremoved */
         /*>09<*/
                But->plt[nPlots].Filter = k_int();    /* Filter */
         /*>Comment<*/
                str = k_str();
                if(str) strcpy( But->plt[nPlots].Descrip , str );
                
                sprintf(But->plt[nPlots].SCNnam, "%s_%s_%s_%.2d", But->plt[nPlots].Site, But->plt[nPlots].Comp, But->plt[nPlots].Net, But->plt[nPlots].Filter);
                sprintf(But->plt[nPlots].SCNnam, "%s_%s_%s_%s_00", But->plt[nPlots].Site, But->plt[nPlots].Comp, But->plt[nPlots].Net, But->plt[nPlots].Loc);
                sprintf(But->plt[nPlots].SCNtxt, "%s %s %s", But->plt[nPlots].Site, But->plt[nPlots].Comp, But->plt[nPlots].Net);
                
                But->nPlots++;
                init[7] = 1;
            }

          /* get SCNL parameters
        ************************************/
/*7*/
            else if( k_its("SCNL") ) {
                if( But->nPlots >= MAXPLOTS ) {
                    fprintf( stderr, "%s Too many <Plot> commands in <%s>", whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAXPLOTS );
                    return;
                }
                nPlots = But->nPlots;
                But->plt[nPlots].DCcorr = But->Mean = 0.0;
                But->plt[nPlots].UseDST = 0;
                But->plt[nPlots].Filter = 0;
                
         /*>Site<*/
                str = k_str();
                if(str) strcpy( But->plt[nPlots].Site , str );
         /*>Comp<*/
                str = k_str();
                if(str) strcpy( But->plt[nPlots].Comp , str );
         /*>Net<*/
                str = k_str();
                if(str) strcpy( But->plt[nPlots].Net , str );
         /*>Loc<*/
                str = k_str();
                if(str) strcpy( But->plt[nPlots].Loc , str );
         /*>04<*/
                But->plt[nPlots].LocalTime = k_int();    /* Hour offset of local time from UTC */
                if(But->plt[nPlots].LocalTime < -24 || But->plt[nPlots].LocalTime > 24) {
                    But->plt[nPlots].LocalTime = 0;
                }
         /*>05<*/
                str = k_str();                           /* Local Time ID e.g. PST */
                strncpy(But->plt[nPlots].LocalTimeID, str, (size_t)3);
                But->plt[nPlots].LocalTimeID[3] = '\0';
         /*>06<*/
                But->plt[nPlots].OldData = k_int();        /* Number of previous hours to retrieve */
                if(But->plt[nPlots].OldData < 0 || But->plt[nPlots].OldData > 168) {
                    But->plt[nPlots].OldData = 0;
                }
         /*>07<*/
                But->plt[nPlots].Scale = k_val();         /* Scale */
         /*>08<*/
                But->plt[nPlots].DCremoved = k_int();   /* DCremoved */
         /*>09<*/
                But->plt[nPlots].Filter = k_int();    /* Filter */
         /*>Comment<*/
                str = k_str();
                if(str) strcpy( But->plt[nPlots].Descrip , str );
                
                sprintf(But->plt[nPlots].SCNnam, "%s_%s_%s_%.2d", But->plt[nPlots].Site, But->plt[nPlots].Comp, But->plt[nPlots].Net, But->plt[nPlots].Filter);
                sprintf(But->plt[nPlots].SCNnam, "%s_%s_%s_%s_00", But->plt[nPlots].Site, But->plt[nPlots].Comp, But->plt[nPlots].Net, But->plt[nPlots].Loc);
                
            	strcpy(string,  But->plt[nPlots].Loc);
            	if(strcmp(string,  "--" )==0) strcpy(string,  "  ");
                sprintf(But->plt[nPlots].SCNtxt, "%s %s %s %s", 
                	But->plt[nPlots].Site, But->plt[nPlots].Comp, 
                	But->plt[nPlots].Net, string);
                
                But->nPlots++;
                init[7] = 1;
            }


               /* optional commands */

                /* get station list path/name
                *****************************/

            else if( k_its("StationList") ) {
                if ( But->nStaDB >= MAX_STADBS ) {
                    fprintf( stderr, "%s Too many <StationList> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_STADBS );
                    return;
                }
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->stationList[But->nStaDB] , str );
                But->stationListType[But->nStaDB] = 0;
                But->nStaDB++;
            }
            else if( k_its("StationList1") ) {
                if ( But->nStaDB >= MAX_STADBS ) {
                    fprintf( stderr, "%s Too many <StationList> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_STADBS );
                    return;
                }
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->stationList[But->nStaDB] , str );
                But->stationListType[But->nStaDB] = 1;
                But->nStaDB++;
            }


        /* get Prefix for deletable files on the target
        ***********************************************/
            else if( k_its("Prefix") ) {
                str = k_str();
                if( (int)strlen(str) >= PRFXSZ) {
                    fprintf( stderr, "%s Fatal error. File prefix %s greater than %d char.\n",
                        whoami, str, PRFXSZ);
                    return;
                }
                if(str) {
                    strcpy(But->Prefix, str );
                    strcat(But->Prefix, ".");
                }
            }

        /* get the timeout interval in seconds
        **************************************/
            else if( k_its("wsTimeout") ) {
                But->wsTimeout = k_int()*1000;
            }
            
        /* get the local target directory(s)
        ************************************/
            else if( k_its("LocalTarget") ) {
                if ( But->nltargets >= MAX_TARGETS ) {
                    logit("e", "%s Too many <LocalTarget> commands in <%s>", 
                             whoami, configfile );
                    logit("e", "; max=%d; exiting!\n", (int) MAX_TARGETS );
                    return;
                }
                if( (long)(str=k_str()) != 0 )  {
                    n = strlen(str);   /* Make sure directory name has proper ending! */
                    if( str[n-1] != '/' ) strcat(str, "/");
                    strcpy(But->loctarget[But->nltargets], str);
                }
                But->nltargets += 1;
            }

            else if( k_its("LabelDC") )        dclabels = 1;            /* optional command */

            else if( k_its("NoLabelDC") )      dclabels = 0;            /* optional command */

            else if( k_its("Debug") )          But->Debug = Debug = 1;  /* optional command */

            else if( k_its("WSDebug") )        But->WSDebug = 1;        /* optional command */

            else if( k_its("SaveDrifts") )     But->SaveDrifts = 1;     /* optional command */

            else if( k_its("UseDST") )         But->UseDST = 1;         /* optional command */

            else if( k_its("BuildOnRestart") ) But->BuildOnRestart = 1; /* optional command */
        
            else if( k_its("DaysAgo") ) {   /* optional commands */
                But->BuildOnRestart = 1;
                But->OneDayOnly = 1;
                But->DaysAgo = k_int();
                if(But->DaysAgo>300) But->DaysAgo = 300; 
                if(But->DaysAgo< 1) But->DaysAgo =  0; 
            }

            else if( k_its("StandAlone") )     EWmodule = 0;             /* optional command */
        
            else if( k_its("PlotDown") )       plot_up = 0;              /* optional command */

            else if( k_its("PlotUp") )         plot_up = 1;              /* optional command */

            else if( k_its("Clip") )           But->Clip = k_int();      /* optional command */

            else if( k_its("Decimate") )       But->DeciFlag = k_int();  /* optional command */

            else if( k_its("RetryCount") ) {  /*optional command*/
                But->RetryCount = k_int();
                if(But->RetryCount > 20) But->RetryCount = 20; 
                if(But->RetryCount <  2) But->RetryCount =  2; 
            }

            else if( k_its("UpdateInt") ) {   /*optional command*/
                But->UpdateInt = k_int()*60;
                if(But->UpdateInt>10000) But->UpdateInt = 10000; 
                if(But->UpdateInt<  1)   But->UpdateInt =   120; 
            }

            else if( k_its("Gulp") ) {        /*optional command*/
                secsPerGulp = k_int();
                if(secsPerGulp>3600) secsPerGulp = 3600; 
                if(secsPerGulp<  1)  secsPerGulp =    0; 
            }

            else if( k_its("Logo") ) {        /*optional command*/
                str = k_str();
                if(str) {
                    strcpy( But->logoname, str );
                    But->logo = 1;
                }
            }

                /* At this point we give up. Unknown thing.
                *******************************************/
            else {
                fprintf(stderr, "%s <%s> Unknown command in <%s>.\n",
                         whoami, com, configfile );
                continue;
            }

                /* See if there were any errors processing the command
                 *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "%s Bad <%s> command  in <%s>; exiting!\n",
                             whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

        /* After all files are closed, check init flags for missed commands
         ******************************************************************/
    nmiss = 0;
    for(i=0;i<ncommand;i++)  if( !init[i] ) nmiss++;
    if ( nmiss ) {
        fprintf( stderr, "%s ERROR, no ", whoami);
        if ( !init[0] )  fprintf( stderr, "<LogSwitch> "    );
        if ( !init[1] )  fprintf( stderr, "<MyModuleId> "   );
        if ( !init[2] )  fprintf( stderr, "<RingName> "     );
        if ( !init[3] )  fprintf( stderr, "<HeartBeatInt> " );
        if ( !init[4] )  fprintf( stderr, "<WaveServer> "   );
        if ( !init[5] )  fprintf( stderr, "<GifDir> "       );
        if ( !init[6] )  fprintf( stderr, "<Display> "         );
        if ( !init[7] )  fprintf( stderr, "<SCN> "         );
        fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
        exit( -1 );
    }
    YTMargin = YTMARGIN;
    if(But->logo) {
        strcpy(str, But->logoname);
        strcpy(But->logoname, But->GifDir);
        strcat(But->logoname, str);
        in = fopen(But->logoname, "rb");
        if(in) {
            im_in = gdImageCreateFromGif(in);
            fclose(in);
            But->logox = im_in->sx;
            But->logoy = im_in->sy;
            YTMargin += im_in->sy/72.0 + 0.1;
            gdImageDestroy(im_in);
        }
    }
    But->axexmax = But->xpix/72.0;
    But->axeymax = But->ypix/72.0;
    But->xsize = But->axexmax + XLMARGIN + XRMARGIN;
    But->ysize = But->axeymax + YBMARGIN + YTMargin;
    But->secsPerGulp = But->secsPerStep = 60*(60/(60/MAXMINUTES));
    j = (secsPerGulp <= 0)? MAXMINUTES*60 : secsPerGulp;
    j = (MAXMINUTES*60 < j)? MAXMINUTES*60 : j;
    j = (j < But->UpdateInt)? j : But->UpdateInt;
    But->secsPerGulp = But->secsPerStep = j;
    for(i=0;i<But->nPlots;i++) {
        But->plt[i].UseDST = But->UseDST;
        But->plt[i].DClabeled = dclabels? 1:0;
        if(But->plt[i].OldData < But->OldData) But->plt[i].OldData = But->OldData;
        But->plt[i].LocalTimeOffset = But->UseLocal? But->plt[i].LocalTime*3600:0;
    }
}

/**********************************************************************************
 * ewmod_status() builds a heartbeat or error msg & puts it into shared memory    *
 **********************************************************************************/
void ewmod_status( unsigned char type,  short ierr,  char *note )
{
    char        subname[] = "ewmod_status";
    MSG_LOGO    logo;
    char        msg[256];
    long        size;
    time_t      t;

    logo.instid = InstId;
    logo.mod    = MyModId;
    logo.type   = type;

    time( &t );
    if( type == TypeHeartBeat ) {
        sprintf( msg, "%ld %ld\n\0", t,MyPid);
    } else
    if( type == TypeError ) {
        sprintf( msg, "%ld %hd %s\n", t, ierr, note );
        logit( "t", "%s: %s:  %s\n",  module, subname, note );
    }

    size = strlen( msg );   /* don't include the null byte in the message */
    if(Transport) {
        if( tport_putmsg( &InRegion, &logo, size, msg ) != PUT_OK ) {
            if( type == TypeHeartBeat ) {
                logit("et","%s: %s:  Error sending heartbeat.\n",  module, subname );
            } else
            if( type == TypeError ) {
                logit("et","%s: %s:  Error sending error:%d.\n",  module, subname, ierr );
            }
        }
    }
}


/****************************************************************************
 *  lookup_ew( ) Look up important info from earthworm.h tables             *
 ****************************************************************************/
void lookup_ew( )
{
    char    subname[] = "lookup_ew";
    
        /* Look up keys to shared memory regions
         ***************************************/
    if( (InRingKey = GetKey(InRingName)) == -1 ) {
        fprintf( stderr,
                "%s: %s: Invalid ring name <%s>; exiting!\n",  module, subname, InRingName );
        exit( -1 );
    }

        /* Look up installation Id
         *************************/
    if( GetLocalInst( &InstId ) != 0 ) {
        fprintf( stderr,
                "%s: %s: error getting local installation id; exiting!\n",  module, subname );
        exit( -1 );
    }

        /* Look up modules of interest
         *****************************/
    if( GetModId( MyModuleId, &MyModId ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid module name <%s>; exiting!\n",  module, subname, MyModuleId );
        exit( -1 );
    }

        /* Look up message types of interest
         ***********************************/
    if( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n",  module, subname );
        exit( -1 );
    }
    if( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid message type <TYPE_ERROR>; exiting!\n",  module, subname );
        exit( -1 );
    }
}


