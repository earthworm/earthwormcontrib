/******************************************************************************/
/* xml2xcel.h:                                                                */
/*                                                                            */
/* JHL Jul 2003                                                               */
/*                                                                            */
/******************************************************************************/

/* Define Constants
 ******************/
#define NAM_LEN 100	          /* length of full directory name          */
#define BUFLEN  65000         /* define maximum size for an event msg   */

typedef struct _sm
{
    int    year, month, day, hour, minute;
    float  second;
    float  pga[3];
    float  pgv[3];
    float  pgd[3];
    float  spec03[3];
    float  spec10[3];
    float  spec30[3];
    char   sta[6];
    char   net[3];
    char   comp[3][4];
    char   loc[3][3];
    char   id[60];
} SMDATA;
 
/* Function prototypes
 *********************/
void config_me ( char * );
void ew_lookup ( void );
void ew_status ( unsigned char, short, char * );

int procxml( FILE *, char *, SMDATA * );
int read_xml( FILE *, SMDATA * );
int read_NQxml( FILE *, SMDATA * );

void logit( char *, char *, ... );   /* logit.c      sys-independent  */

