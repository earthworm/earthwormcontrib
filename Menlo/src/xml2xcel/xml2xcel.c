/******************************************************************************/
/*                                                                            */
/* xml2xcel.c:                                                                */
/*                                                                            */
/* Periodically check a specified directory for xml files.                    */
/* Reads the files, decodes the strong motion parameters,                     */
/* and copies to the appropriate directory.                                   */
/* The strong motion parameters are prepended to an Excel-type file and       */
/* copied to a designated directory.                                          */
/*                                                                            */
/*                  Jim Luetgert 07/09/03                                     */
/* XMLType is a flag to determine input format of XML files. THis is so the   */
/* same module can be used for NSMP and NetQuakes XML data.                   */
/*                                                                            */
/*                  Jim Luetgert 09/29/10                                     */
/******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <errno.h>
#include "xml2xcel.h"

SHM_INFO  Region;                   /* shared memory region to use for i/o    */
pid_t     myPid;                    /* for restarts by startstop              */

static char MsgBuf[BUFLEN];         /* char string to hold output message     */

static char *TroubleSubdir = "trouble";    /* subdir for problem files        */
static char *TempSubdir    = "temp";       /* subdir for temporary files      */

/* Things to read or derive from configuration file
 **************************************************/
static char     RingName[20];        /* name of transport ring for i/o        */
static char     MyModName[50];       /* speak as this module name/id          */
static int      LogSwitch;           /* 0 if no logfile should be written     */
static int      HeartBeatInterval;   /* seconds betweeen beats to statmgr     */
static char     GetFromDir[NAM_LEN]; /* directory to monitor for data         */
static char     OutputDir[NAM_LEN];  /* directory to place data               */
static char     SumDir[NAM_LEN];     /* directory to place summary data       */
static unsigned CheckPeriod;         /* secs between looking for new files    */
static int      OpenTries;
static int      OpenWait;
static char     PeerHeartBeatFile[NAM_LEN]; /* name of heartbeat file         */
static int      PeerHeartBeatInterval; /* seconds between heartbeat files     */
       int      Debug;                 /* non-zero -> debug logging           */
       int      MaxList;               /* Maximum entries in list             */ 
       int      XMLType;               /* Format type of the XML files        */ 

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o          */
static unsigned char InstId;        /* local installation id                  */
static unsigned char MyModId;       /* Module Id for this program             */
static unsigned char TypeHeartBeat; 
static unsigned char TypeError;

/* Error messages used by sm_file2ew 
 ***********************************/
#define ERR_CONVERT       0     /* trouble converting a file                  */
#define ERR_PEER_LOST     1     /* no peer heartbeat file for a while         */
#define ERR_PEER_RESUME   2     /* got a peer heartbeat file again            */

#define TEXT_LEN NAM_LEN*3
static char Text[TEXT_LEN];     /* string for log/error messages              */
static char ProgName[NAM_LEN];  /* program name for logging purposes          */

/* File handling stuff
**********************/
int main( int argc, char **argv )
{
    int       i, ret, read_error = 0;
    int       MaxEvents;
    char      fname[100], ftemp[155], fold[155], fnew[155], buf[BUFLEN];
    char     *c;
    FILE     *fp, *fout;
    SMDATA    xml;
    time_t    tnextbeat;    /* next time for local heartbeat  */
    time_t    tnextpeer;    /* next time for peer's heartbeat */
    time_t    tnow;         /* current time */
    int       peerstatus;   /* current status of peer */

/* Check command line arguments 
 ******************************/
    if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", argv[0] );
        exit( 0 );
    }
    strcpy( ProgName, argv[1] );
    c = strchr( ProgName, '.' );
    if( c ) *c = '\0';
           
/* Read the configuration file(s)
 ********************************/
    config_me( argv[1] );
   
/* Look up important info from earthworm.h tables
 ************************************************/
    ew_lookup();
   
/* Initialize name of log-file & open it 
 ***************************************/
    logit_init( argv[1], (short) MyModId, TEXT_LEN*2, LogSwitch );
    logit( "" , "%s: Read command file <%s>\n", argv[0], argv[1] );

/* Get process ID for heartbeat messages 
 ***************************************/
    myPid = getpid();
    if( myPid == -1 ) {
        logit("e","%s: Cannot get pid; exiting!\n", ProgName);
        exit (-1);
    }

/* Change to the directory with the input files
 ***********************************************/
    if( chdir_ew( GetFromDir ) == -1 ) {
        logit( "e", "%s: GetFromDir directory <%s> not found; "
                 "exiting!\n", ProgName, GetFromDir );
        exit(-1);
    }
    if(Debug)logit("et","%s: changed to directory <%s>\n", ProgName,GetFromDir);

/* Make sure trouble subdirectory exists
 ***************************************/
   if( CreateDir( TroubleSubdir ) != EW_SUCCESS ) {
      logit( "e", "%s: trouble creating trouble directory: %s/%s\n",
              ProgName, GetFromDir, TroubleSubdir ); 
      return( -1 );
   }

/* Make sure temp subdirectory exists
 ***************************************/
   if( CreateDir( TempSubdir ) != EW_SUCCESS ) {
      logit( "e", "%s: trouble creating temp directory: %s/%s\n",
              ProgName, GetFromDir, TempSubdir ); 
      return( -1 );
   }

/* Attach to Output shared memory ring 
 *************************************/
   tport_attach( &Region, RingKey );
   logit( "", "%s: Attached to public memory region %s: %d\n", 
          ProgName, RingName, RingKey );

/* Force local heartbeat first time thru main loop
   but give peers the full interval before we expect a file
 **********************************************************/
   tnextbeat  = time(NULL) - 1;
   tnextpeer  = time(NULL) + PeerHeartBeatInterval;
   peerstatus = ERR_PEER_RESUME; 


/****************  top of working loop ********************************/
    while(1)  {
     /* Check on heartbeats
      *********************/
        tnow = time(NULL);
        if( tnow >= tnextbeat ) {  /* time to beat local heart */
           ew_status( TypeHeartBeat, 0, "" );
           tnextbeat = tnow + HeartBeatInterval;
        }
        if( PeerHeartBeatInterval &&  tnow > tnextpeer ) {  /* peer is late! */
           if( peerstatus == ERR_PEER_RESUME ) {  /* complain! */
              sprintf(Text,"No PeerHeartBeatFile in %s for over %d sec!",
                      GetFromDir, PeerHeartBeatInterval );
              /*
              ew_status( TypeError, ERR_PEER_LOST, Text );
              */
              peerstatus = ERR_PEER_LOST;            
           }
        }

     /* See if termination has been requested 
      ****************************************/
        if( tport_getflag( &Region ) == TERMINATE ) {
           logit( "t", "%s: Termination requested; exiting!\n", ProgName );
           break;
        }

     /* Get a file name
      ******************/    
        ret = GetFileName( fname );
    
        sleep_ew( 1*1000 );
    
        if( ret == 1 ) {  /* No files found; wait for one to appear */
            sleep_ew( CheckPeriod*1000 ); 
            continue;
        }
   
        if(Debug)logit("et","%s: got file name <%s>\n",ProgName,fname);

     /* Open the file.
      ******************/
     /* We open for write, as that will hopefully get us an exclusive open. 
      * We don't ever want to look at a file that's being written to. 
      */
        for(i=0;i<OpenTries;i++) {
            fp = fopen( fname, "rb" );
            if ( fp != NULL ) goto itopend;
            sleep_ew(OpenWait);
        }
        logit("et","%s: Error: Could not open %s after %d*%d ms\n",
                   ProgName, fname, OpenTries,OpenWait);
        itopend:    
        if(i>0) logit("t","Warning: %d attempts required to open file %s\n", i,fname);
   
        if(Debug)logit("et","%s: File named <%s> is open\n",ProgName,fname);
        fclose( fp );

     /* If it's a heartbeat file, reset tnextpeer and delete the file
      ****************************************************************/
     /* We're not decoding the contents of the file.
      * The idea is that we don't care when the heartbeat was created, 
      * only when we saw it. This prevents 'heartbeats from the past' 
      * from confusing things, but may not be wise... 
      */
        if( strcmp(fname,PeerHeartBeatFile)==0 ) {
            tnextpeer = time(NULL) + PeerHeartBeatInterval;

            if( peerstatus == ERR_PEER_LOST ) {  /* announce resumption */
                sprintf(Text, "Received PeerHeartBeatFile in %s (Peer is alive)",
                        GetFromDir );
                ew_status( TypeError, ERR_PEER_RESUME, Text );
                peerstatus = ERR_PEER_RESUME;            
            }
      /*      fclose( fp );    */
            if( remove( fname ) != 0) {
                logit("et", "%s: Cannot delete heartbeat file <%s>; exiting!", 
                    ProgName, fname );
                break;
            }
            continue;
        }

        else if( strcmp(fname,"core")==0 ) {
       /*     fclose( fp );    */
            if( remove( fname ) != 0) {
                logit("et", "%s: Cannot delete core file <%s>; exiting!", 
                    ProgName, fname );
                break;
            } else {
                logit("et", "%s: Core file deleted <%s>.", ProgName, fname );
			}
            continue;
        }

     /* Pass file to the xml processor
        *******************************/
   
        ret = procxml( fp, fname, &xml );
if (Debug) logit ("e", "Return from procxml.  ret = %d\n", ret); 

     /* Everything went fine...
      *************************/
        /* Keep file around */
        if( ret > 0 ) { 
            
            if(strcmp(OutputDir, "none/")) {
if (Debug) logit ("e", "Open %s.\n", fname); 
	            fp = fopen( fname, "rb" );
	            if ( fp == NULL ) {
	                logit( "et", "Error. Can't open file %s\n", fname );
	                continue;
	            }
	            sprintf(ftemp,"%s/%s",TempSubdir,fname );
if (Debug) logit ("e", "Open %s.\n", ftemp); 
	            fout = fopen( ftemp, "wb" );
	            if ( fout == NULL ) {
	                logit( "et", "Error. Can't open new file %s\n", ftemp );
	                fclose( fp );
	                continue;
	            }

        /* Read BUFLEN bytes at a time and write them to fout
           ****************************************************/
	            while ( 1 ) {
	                int nbytes = fread( buf, sizeof(char), BUFLEN, fp );
	                if ( nbytes > 0 ) {
	                    if ( fwrite( buf, sizeof(char), nbytes, fout ) == 0 ) {
	                        log( "et", "Error writing new file.\n" );
	                        break;
	                    }
	                }
	                if ( feof( fp ) ) break;

        /* If an fread error occurs, exit loop.  
           ************************************/
	                if ( (nbytes == 0) && ferror( fp ) ) {
	                    log( "et", "fread() error on %s File partially copied. \n", fname );
	                    break;
	                }
	            }

        /* Finish up with this file
           ************************/
	            fclose( fp );
	            fclose( fout );
if (Debug) logit ("e", "File %s saved to %s.\n", fname, ftemp); 

	            sprintf(fnew,"%s%s",OutputDir, fname );
	            if( rename( ftemp, fnew ) != 0 ) {
	                logit( "et", "error moving file to %s\n; exiting!", fnew );
	                break;
	            } else {
	                if(Debug)logit("e","%s moved to %s\n", fname, OutputDir );
	            }
            }
    
            if( remove( fname ) != 0 ) {
                logit("et","%s: error deleting file: %s\n", ProgName, fname);
            } else  {
                if(Debug)logit("e","%s: Removed %s \n \n", ProgName, fname );
            }
            
        /* Update the excel file
           ****************************************************/
            sprintf(fnew,"%s/%s",TempSubdir,"sm_sum.txt" );
            fout = fopen( fnew, "wb" );
            if ( fout == NULL ) {
                logit( "et", "Error. Can't open new file %s\n", fnew );
                continue;
            }
            sprintf(fold,"%s/%s",TempSubdir,"data.old" );
            fp = fopen( fold, "rb" );
            if ( fp == NULL ) {
                logit( "et", "Error. Can't open old file %s\n", fold );
                fp = fopen( fold, "wb" );
                if ( fp == NULL ) {
                    logit( "et", "Error. Can't open old file %s\n", fold );
                    fclose( fout );
                    continue;
                }
                fprintf(fp, "%s%s%s%s  \n", 
	                "ID                                   STA      Date       Time      ", 
	                "Comp   PGA       PGV      Spec 0.3  Spec 1.0  Spec 3.0 ", 
	                "Comp   PGA       PGV      Spec 0.3  Spec 1.0  Spec 3.0 ", 
	                "Comp   PGA       PGV      Spec 0.3  Spec 1.0  Spec 3.0" );
                fprintf(fp, "%s%s%s%s  \n", 
	                "                                                                   ", 
	                "      (cm/s/s)  (cm/s)    (cm/s/s)  (cm/s/s)  (cm/s/s) ", 
	                "      (cm/s/s)  (cm/s)    (cm/s/s)  (cm/s/s)  (cm/s/s) ", 
	                "      (cm/s/s)  (cm/s)    (cm/s/s)  (cm/s/s)  (cm/s/s)" );
        
                fclose( fp );
                fp = fopen( fold, "rb" );
                if ( fp == NULL ) {
                    logit( "et", "Error. Can't open old file %s\n", fold );
                    fclose( fout );
                    continue;
                }
            }
            
        
    if(XMLType == 0) {
            if (Debug) {
            	logit ("e", "%s%s%s%s  \n", 
                "ID                                   STA      Date       Time      ", 
                "Comp   PGA       PGV      Spec 0.3  Spec 1.0  Spec 3.0 ", 
                "Comp   PGA       PGV      Spec 0.3  Spec 1.0  Spec 3.0 ", 
                "Comp   PGA       PGV      Spec 0.3  Spec 1.0  Spec 3.0" );
                
            	logit ("e", "%s%s%s%s  \n", 
                "                                                                   ", 
                "      (cm/s/s)  (cm/s)    (cm/s/s)  (cm/s/s)  (cm/s/s) ", 
                "      (cm/s/s)  (cm/s)    (cm/s/s)  (cm/s/s)  (cm/s/s) ", 
                "      (cm/s/s)  (cm/s)    (cm/s/s)  (cm/s/s)  (cm/s/s)" );
            	logit ("e", "%-36s %-4s %.2d/%.2d/%.2d/ %.2d:%.2d:%06.3f %.3s %10.6f %9.6f %9.6f %9.6f %9.6f %.3s %10.6f %9.6f %9.6f %9.6f %9.6f %.3s %10.6f %9.6f %9.6f %9.6f %9.6f \n", 
                xml.id, xml.sta, xml.month, xml.day, xml.year, xml.hour, xml.minute, xml.second,
                xml.comp[0], xml.pga[0], xml.pgv[0], xml.spec03[0], xml.spec10[0], xml.spec30[0], 
                xml.comp[1], xml.pga[1], xml.pgv[1], xml.spec03[1], xml.spec10[1], xml.spec30[1], 
                xml.comp[2], xml.pga[2], xml.pgv[2], xml.spec03[2], xml.spec10[2], xml.spec30[2] );
            }
    
            fprintf(fout, "%s%s%s%s  \n", 
                "ID                                   STA      Date       Time      ", 
                "Comp   PGA       PGV      Spec 0.3  Spec 1.0  Spec 3.0 ", 
                "Comp   PGA       PGV      Spec 0.3  Spec 1.0  Spec 3.0 ", 
                "Comp   PGA       PGV      Spec 0.3  Spec 1.0  Spec 3.0" );
    
            fprintf(fout, "%s%s%s%s  \n", 
                "                                                                   ", 
                "      (cm/s/s)  (cm/s)    (cm/s/s)  (cm/s/s)  (cm/s/s) ", 
                "      (cm/s/s)  (cm/s)    (cm/s/s)  (cm/s/s)  (cm/s/s) ", 
                "      (cm/s/s)  (cm/s)    (cm/s/s)  (cm/s/s)  (cm/s/s)" );
    
            fprintf(fout, "%-36s %-4s %.2d/%.2d/%.2d/ %.2d:%.2d:%06.3f %.3s %10.6f %9.6f %9.6f %9.6f %9.6f %.3s %10.6f %9.6f %9.6f %9.6f %9.6f %.3s %10.6f %9.6f %9.6f %9.6f %9.6f \n", 
                xml.id, xml.sta, xml.month, xml.day, xml.year, xml.hour, xml.minute, xml.second,
                xml.comp[0], xml.pga[0], xml.pgv[0], xml.spec03[0], xml.spec10[0], xml.spec30[0], 
                xml.comp[1], xml.pga[1], xml.pgv[1], xml.spec03[1], xml.spec10[1], xml.spec30[1], 
                xml.comp[2], xml.pga[2], xml.pgv[2], xml.spec03[2], xml.spec10[2], xml.spec30[2] );
    } else {
            if (Debug) {
            	logit ("e", "%s%s%s%s  \n", 
                "ID                             STA          Date       Time      ", 
                "Comp   PGA       PGV      PGD       Spec 0.3  Spec 1.0  Spec 3.0 ", 
                "Comp   PGA       PGV      PGD       Spec 0.3  Spec 1.0  Spec 3.0 ", 
                "Comp   PGA       PGV      PGD       Spec 0.3  Spec 1.0  Spec 3.0" );
                
            	logit ("e", "%s%s%s%s  \n", 
                "                                                                 ", 
                "      (cm/s/s)  (cm/s)    (cm)      (cm/s/s)  (cm/s/s)  (cm/s/s) ", 
                "      (cm/s/s)  (cm/s)    (cm)      (cm/s/s)  (cm/s/s)  (cm/s/s) ", 
                "      (cm/s/s)  (cm/s)    (cm)      (cm/s/s)  (cm/s/s)  (cm/s/s)" );
                
            	logit ("e", "%-30s %-5s_%-2s %.2d/%.2d/%.2d/ %.2d:%.2d:%06.3f %.3s %10.5f %9.5f %9.5f %9.5f %9.5f %9.5f %.3s %10.5f %9.5f %9.5f %9.5f %9.5f %9.5f %.3s %10.5f %9.5f %9.5f %9.5f %9.5f %9.5f \n", 
                xml.id, xml.sta, xml.loc[0], xml.month, xml.day, xml.year, xml.hour, xml.minute, xml.second,
                xml.comp[0], xml.pga[0], xml.pgv[0], xml.pgd[0], xml.spec03[0], xml.spec10[0], xml.spec30[0], 
                xml.comp[1], xml.pga[1], xml.pgv[1], xml.pgd[1], xml.spec03[1], xml.spec10[1], xml.spec30[1], 
                xml.comp[2], xml.pga[2], xml.pgv[2], xml.pgd[2], xml.spec03[2], xml.spec10[2], xml.spec30[2] );
            }
    
            fprintf(fout, "%s%s%s%s  \n", 
                "ID                             STA          Date       Time      ", 
                "Comp   PGA       PGV      PGD       Spec 0.3  Spec 1.0  Spec 3.0 ", 
                "Comp   PGA       PGV      PGD       Spec 0.3  Spec 1.0  Spec 3.0 ", 
                "Comp   PGA       PGV      PGD       Spec 0.3  Spec 1.0  Spec 3.0" );
    
            fprintf(fout, "%s%s%s%s  \n", 
                "                                                                 ", 
                "      (cm/s/s)  (cm/s)    (cm)      (cm/s/s)  (cm/s/s)  (cm/s/s) ", 
                "      (cm/s/s)  (cm/s)    (cm)      (cm/s/s)  (cm/s/s)  (cm/s/s) ", 
                "      (cm/s/s)  (cm/s)    (cm)      (cm/s/s)  (cm/s/s)  (cm/s/s)" );
    
            fprintf(fout, "%-30s %-5s_%-2s %.2d/%.2d/%.2d/ %.2d:%.2d:%06.3f %.3s %10.5f %9.5f %9.5f %9.5f %9.5f %9.5f %.3s %10.5f %9.5f %9.5f %9.5f %9.5f %9.5f %.3s %10.5f %9.5f %9.5f %9.5f %9.5f %9.5f \n", 
                xml.id, xml.sta, xml.loc[0], xml.month, xml.day, xml.year, xml.hour, xml.minute, xml.second,
                xml.comp[0], xml.pga[0], xml.pgv[0], xml.pgd[0], xml.spec03[0], xml.spec10[0], xml.spec30[0], 
                xml.comp[1], xml.pga[1], xml.pgv[1], xml.pgd[1], xml.spec03[1], xml.spec10[1], xml.spec30[1], 
                xml.comp[2], xml.pga[2], xml.pgv[2], xml.pgd[2], xml.spec03[2], xml.spec10[2], xml.spec30[2] );
    }
            
            MaxEvents = 0;
            fgets(buf, 480, fp);
            fgets(buf, 480, fp);
            while(fgets(buf, 480, fp)!=0L && MaxEvents < MaxList) {
                    fprintf(fout, "%s", buf);
                    MaxEvents += 1;
            }
            fclose(fout);
            fclose(fp);

        /* Copy file back to tempdir
           *************************/
            sprintf(fnew,"%s/%s",TempSubdir,"data.old" );
            fout = fopen( fnew, "wb" );
            if ( fout == NULL ) {
                log( "et", "Error. Can't open new file %s\n", fnew );
                continue;
            }
            sprintf(fold,"%s/%s",TempSubdir,"sm_sum.txt" );
            fp = fopen( fold, "rb" );
            if ( fp == NULL ) {
                log( "et", "Error. Can't open old file %s\n", fold );
                fclose( fout );
                continue;
            }
            while(fgets(buf, 480, fp)!=0L) {
                    fprintf(fout, "%s", buf);
            }
            fclose(fout);
            fclose(fp);

        /* Copy file to output dir
           *************************/
            if(strcmp(SumDir, "none/")) {
	            sprintf(ftemp,"%s/%s",TempSubdir,"temp.txt" );
	            fout = fopen( ftemp, "wb" );
	            if ( fout == NULL ) {
	                log( "et", "Error. Can't open new file %s\n", ftemp );
	                continue;
	            }
	            sprintf(fold,"%s/%s",TempSubdir,"sm_sum.txt" );
	            fp = fopen( fold, "rb" );
	            if ( fp == NULL ) {
	                log( "et", "Error. Can't open old file %s\n", fold );
	                fclose( fout );
	                continue;
	            }
	            while(fgets(buf, 480, fp)!=0L) {
	                    fprintf(fout, "%s", buf);
	            }
	            fclose(fout);
	            fclose(fp);

	            sprintf(fnew,"%s%s",SumDir, "sm_sum.txt" );
	            if( rename( ftemp, fnew ) != 0 ) {
	                logit( "et", "error moving file to %s\n; exiting!", fnew );
	                break;
	            } else {
	                if(Debug)logit("e","sm_sum.txt moved to %s\n", SumDir );
	            }
    
            }

            
        }

        /* Delete the file */
        else  if( ret == 0 ){ 
            if( remove( fname ) != 0 ) {
                logit("e","%s: Error deleting file %s; exiting!\n", ProgName, fname);
                break;
            } else  {
                logit("e","%s: Deleted file %s.\n \n", ProgName, fname);
            }
        }

    /* ...or there was trouble! 
     **************************/
        else { 
            logit("e","\n");
            sprintf( Text,"Trouble processing: %s ;", fname );
            ew_status( TypeError, ERR_CONVERT, Text );
            sprintf(fnew,"%s/%s",TroubleSubdir,fname );
            if( rename( fname, fnew ) != 0 ) {
                logit( "e", " error moving file to %s ; exiting!\n", fnew );
                break;
            } else {
                logit( "e", " moved to %s\n", fnew );
            }
        }
    }
    
/* detach from shared memory */
   tport_detach( &Region ); 

/* write a termination msg to log file */
   fflush( stdout );
   return( 0 );

}  


/******************************************************************************
 * procxml()  Read and process the .xml file.                                 *
 ******************************************************************************/
int procxml( FILE *fp, char *fname, SMDATA *xml )
{
    char     whoami[50];
    int      i, j, tlen;

/* Initialize variables 
 **********************/
    sprintf(whoami, " %s: %s: ", "xml2xcel", "procxml");

/* Close/reopen file in binary read
 **********************************/
    fp = fopen( fname, "rb" );
    if( fp == NULL )               {
    	if (Debug) logit ("e", "%s Unable to open %s.\n", whoami, fname);
    	return 0;
    }
    if(strstr(fname, ".xml") == 0) {
    	if (Debug) logit ("e", "%s %s is not an xml file. can't use.\n", whoami, fname);
	    fclose( fp );
    	return 0;
    }
    rewind(fp);
    
/* Extract strong motion parameters from the .xml file 
 *****************************************************/
    if(XMLType == 0) {
		tlen = read_xml(fp, xml);
    } else {
		tlen = read_NQxml(fp, xml);
    }
    
    strcpy(xml->id, fname);
    i = 0;
    j = strlen(xml->id);
    while(xml->id[i] != '.' && i<j) {
        i++;
    }
    xml->id[i] = '\0';
    
    if (Debug) logit ("e", "%s%-36s %5s %2s %.2d/%.2d/%.2d/ %.2d:%.2d:%6.3f %.3s %9.6f %9.6f %9.6f %9.6f %9.6f %.3s %9.6f %9.6f %9.6f %9.6f %9.6f %.3s %9.6f %9.6f %9.6f %9.6f %9.6f \n", 
        whoami, xml->id, xml->sta, xml->loc[0], xml->month, xml->day, xml->year, xml->hour, xml->minute, xml->second,
        xml->comp[0], xml->pga[0], xml->pgv[0], xml->spec03[0], xml->spec10[0], xml->spec30[0], 
        xml->comp[1], xml->pga[1], xml->pgv[1], xml->spec03[1], xml->spec10[1], xml->spec30[1], 
        xml->comp[2], xml->pga[2], xml->pgv[2], xml->spec03[2], xml->spec10[2], xml->spec30[2] );
    
    fclose( fp );
    
    return( 1 );
}



/******************************************************************************
 * read_xml(fp)  Read the XML file and extract the strong motion parameters.  *
 *  This is hard-wired for a three-component file.                            *
 ******************************************************************************/
int read_xml( FILE *fp, SMDATA *xml )
{
    char    whoami[50], buf[BUFLEN], *a, *b, *c, num[20];
    int     i, j, stat;
    float   msec;
    
    sprintf(whoami, " %s: %s: ", "xml2xcel", "read_xml");
    
    /* Read the whole file. It's not that big.  */
    stat = fread(&buf, 1, BUFLEN, fp);
    
    /* Decode the date/time */
    a = strstr(buf, "year value=");
    b = strchr(a, '"');
    i = 1;
    while(b[i] != '"') {
        num[i-1] = b[i];
        i++;
    }
    num[i-1] = '\0';
    sscanf(num, "%d", &xml->year);
    
    a = strstr(buf, "month value=");
    b = strchr(a, '"');
    i = 1;
    while(b[i] != '"') {
        num[i-1] = b[i];
        i++;
    }
    num[i-1] = '\0';
    sscanf(num, "%d", &xml->month);
    
    a = strstr(buf, "day value=");
    b = strchr(a, '"');
    i = 1;
    while(b[i] != '"') {
        num[i-1] = b[i];
        i++;
    }
    num[i-1] = '\0';
    sscanf(num, "%d", &xml->day);
    
    a = strstr(buf, "hour value=");
    b = strchr(a, '"');
    i = 1;
    while(b[i] != '"') {
        num[i-1] = b[i];
        i++;
    }
    num[i-1] = '\0';
    sscanf(num, "%d", &xml->hour);
    
    a = strstr(buf, "minute value=");
    b = strchr(a, '"');
    i = 1;
    while(b[i] != '"') {
        num[i-1] = b[i];
        i++;
    }
    num[i-1] = '\0';
    sscanf(num, "%d", &xml->minute);
    
    a = strstr(buf, "second value=");
    b = strchr(a, '"');
    i = 1;
    while(b[i] != '"') {
        num[i-1] = b[i];
        i++;
    }
    num[i-1] = '\0';
    sscanf(num, "%f", &xml->second);
    
    a = strstr(buf, "msec value=");
    b = strchr(a, '"');
    i = 1;
    while(b[i] != '"') {
        num[i-1] = b[i];
        i++;
    }
    num[i-1] = '\0';
    sscanf(num, "%f", &msec);
    xml->second += msec/1000.0;
    
    /* Decode the station name */
    a = strstr(buf, "station code=");
    b = strchr(a, '"');
    i = 1;
    while(b[i] != '"') {
        xml->sta[i-1] = b[i];
        i++;
    }
    xml->sta[i-1] = '\0';
    
    for(j=0;j<3;j++) {
        xml->loc[j][0] = '\0';
        a = strstr(b, "component name=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            xml->comp[j][i-1] = b[i];
            i++;
        }
        xml->comp[j][i-1] = '\0';
    
        a = strstr(b, "acc value=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            num[i-1] = b[i];
            i++;
        }
        num[i-1] = '\0';
        sscanf(num, "%f", &xml->pga[j]);
    
        a = strstr(b, "vel value=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            num[i-1] = b[i];
            i++;
        }
        num[i-1] = '\0';
        sscanf(num, "%f", &xml->pgv[j]);
    
        a = strstr(b, " value=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            num[i-1] = b[i];
            i++;
        }
        num[i-1] = '\0';
        sscanf(num, "%f", &xml->spec03[j]);
    
        a = strstr(b, " value=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            num[i-1] = b[i];
            i++;
        }
        num[i-1] = '\0';
        sscanf(num, "%f", &xml->spec10[j]);
    
        a = strstr(b, " value=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            num[i-1] = b[i];
            i++;
        }
        num[i-1] = '\0';
        sscanf(num, "%f", &xml->spec30[j]);
    }
        
    return stat;
}



/********************************************************************************
 * read_NQxml(fp)  Read the XML file and extract the strong motion parameters.  *
 *  This is hard-wired for a three-component NetQuakes file.                    *
 ********************************************************************************/
int read_NQxml( FILE *fp, SMDATA *xml )
{
    char    whoami[50], buf[BUFLEN], *a, *b, *c, num[50], dum[5];
    int     i, j, stat;
    float   msec;
    
    sprintf(whoami, " %s: %s: ", "xml2xcel", "read_NQxml");
    
    /* Read the whole file. It's not that big.  */
    stat = fread(&buf, 1, BUFLEN, fp);

    /* Decode the date/time */
    a = strstr(buf, "PGMTime");
    b = strchr(a, '>');
    i = 1;
    while(b[i] != 'Z') {
        num[i-1] = b[i];
        i++;
    }
    num[i-1] = '\0';
if (Debug) logit ("e", "%s num: %s %d\n", whoami, num, i);
    sscanf(num, "%4d %1s %2d %1s %2d  %1s %2d %1s %2d %1s %f", 
    &xml->year, dum, &xml->month, dum, &xml->day, dum, &xml->hour, dum, &xml->minute, dum, &xml->second);
        
if (Debug) logit ("e", "%s %d %d %d    %d %d %6.3f\n", whoami, xml->year, xml->month, xml->day, xml->hour, xml->minute, xml->second);
    
    /* Decode the station name */
    a = strstr(buf, "station code=");
    b = strchr(a, '"');
    i = 1;
    while(b[i] != '"') {
        xml->sta[i-1] = b[i];
        i++;
    }
    xml->sta[i-1] = '\0';
    
    for(j=0;j<3;j++) {
        xml->loc[j][0] = '\0';
        a = strstr(b, "component name=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            xml->comp[j][i-1] = b[i];
            i++;
        }
        xml->comp[j][i-1] = '\0';
    
        a = strstr(b, "loc=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            xml->loc[j][i-1] = b[i];
            i++;
        }
        xml->loc[j][i-1] = '\0';
    	strcpy(dum,xml->loc[j]);
    
        a = strstr(b, "pga value=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            num[i-1] = b[i];
            i++;
        }
        num[i-1] = '\0';
        sscanf(num, "%f", &xml->pga[j]);
    
        a = strstr(b, "pgv value=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            num[i-1] = b[i];
            i++;
        }
        num[i-1] = '\0';
        sscanf(num, "%f", &xml->pgv[j]);
    
        a = strstr(b, "pgd value=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            num[i-1] = b[i];
            i++;
        }
        num[i-1] = '\0';
        sscanf(num, "%f", &xml->pgd[j]);
    
    
        a = strstr(b, " value=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            num[i-1] = b[i];
            i++;
        }
        num[i-1] = '\0';
        sscanf(num, "%f", &xml->spec03[j]);
    
        a = strstr(b, " value=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            num[i-1] = b[i];
            i++;
        }
        num[i-1] = '\0';
    
        sscanf(num, "%f", &xml->spec10[j]);
    
        a = strstr(b, " value=");
        b = strchr(a, '"');
        i = 1;
        while(b[i] != '"') {
            num[i-1] = b[i];
            i++;
        }
        num[i-1] = '\0';
        sscanf(num, "%f", &xml->spec30[j]);
    }
            
    return stat;
}


/******************************************************************************
 *  config_me() processes command file(s) using kom.c functions;              *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
#define ncommand 10
void config_me( char *configfile )
{
    char     init[ncommand]; /* init flags, one byte for each required command */
    int      nmiss;          /* number of required commands that were missed   */
    char    *com, *str;
    int      i, j, n, num_sides, nfiles, success;
   
/* Set to zero one init flag for each required command 
 *****************************************************/   
    for( i=0; i<ncommand; i++ )  init[i] = 0;
    
    OpenTries = 5;
    OpenWait = 200;
    CheckPeriod = 1;
    MaxList = 300;
    XMLType = 0;

/* Open the main configuration file 
 **********************************/
    nfiles = k_open( configfile ); 
    if ( nfiles == 0 ) {
        fprintf( stderr, "%s: Error opening command file <%s>; exiting!\n", 
                 ProgName, configfile );
        exit( -1 );
    }

/* Process all command files
 ***************************/
   while(nfiles > 0) {  /* While there are command files open */
        while(k_rd()) {       /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr, 
                          "%s: Error opening command file <%s>; exiting!\n",
                           ProgName, &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command 
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("GetFromDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( GetFromDir, str, NAM_LEN );
                init[3] = 1;
            }
  /*4*/     else if( k_its("OutputDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( OutputDir, str, NAM_LEN );
                init[4] = 1;
            }
  /*5*/     else if( k_its("SumDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( SumDir, str, NAM_LEN );
                init[5] = 1;
            }
  /*6*/     else if( k_its("Debug") ) {
                Debug = k_int();
                init[6] = 1;
            }
  /*7*/     else if( k_its("PeerHeartBeatFile") ) {
                str = k_str();
                if(str) strncpy( PeerHeartBeatFile, str, NAM_LEN);
                init[7] = 1;
            }

  /*8*/     else if( k_its("PeerHeartBeatInterval") ) {
                PeerHeartBeatInterval = k_int();
                init[8] = 1;
            }
  /*9*/     else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_int();
                init[9] = 1;
            }

               /* optional commands */

            else if( k_its("MaxList") ) {
                MaxList = k_int();
            }

            else if( k_its("XMLType") ) {
                XMLType = k_int();
                if(XMLType > 1) XMLType = 1;
            }

         /* Unknown command
          *****************/ 
            else {
                fprintf( stderr, "%s: <%s> Unknown command in <%s>.\n", 
                         ProgName, com, configfile );
                continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr, 
                       "%s: Bad <%s> command in <%s>; exiting!\n",
                        ProgName, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }
   
/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "%s: ERROR, no ", ProgName );
       if ( !init[0] )  fprintf( stderr, "<LogFile> "       );
       if ( !init[1] )  fprintf( stderr, "<MyModuleId> "    );
       if ( !init[2] )  fprintf( stderr, "<RingName> "      );
       if ( !init[3] )  fprintf( stderr, "<GetFromDir> "    );
       if ( !init[4] )  fprintf( stderr, "<OutputDir> "     );
       if ( !init[5] )  fprintf( stderr, "<SumDir> "        );
       if ( !init[6] )  fprintf( stderr, "<Debug> "         );
       if ( !init[7] )  fprintf( stderr, "<PeerHeartBeatFile> "     );
       if ( !init[8] )  fprintf( stderr, "<PeerHeartBeatInterval> " );
       if ( !init[9] )  fprintf( stderr, "<HeartBeatInterval> "     );
       fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }
   
   if(Debug) fprintf( stderr, "command(s) read from <%s>\n", configfile );
   
   return;
}


/******************************************************************************/
/*                                                                            */
/*  ew_lookup( )   Look up important info from earthworm.h tables             */
/*                                                                            */
/******************************************************************************/
void ew_lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 ) {
        fprintf( stderr,
                "%s:  Invalid ring name <%s>; exiting!\n",
                 ProgName, RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr, 
              "%s: error getting local installation id; exiting!\n",ProgName );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid module name <%s>; exiting!\n", ProgName, MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n",ProgName );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_ERROR>; exiting!\n",ProgName );
      exit( -1 );
   }
   return;
} 

/******************************************************************************/
/*                                                                            */
/* ew_status() builds a heartbeat or error message & puts it into             */
/*                   shared memory.  Writes errors to log file & screen.      */
/*                                                                            */
/******************************************************************************/
void ew_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size, t;
 
/* Build the message
 *******************/ 
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat ) {
        sprintf( msg, "%ld %ld\n\0", t, myPid);
   }
   else if( type == TypeError ) {
        sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
        logit( "et", "%s: %s\n", ProgName, note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */     

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK ) {
        if( type == TypeHeartBeat ) {
           logit("et","%s:  Error sending heartbeat.\n", ProgName );
        }
        else if( type == TypeError ) {
           logit("et","%s:  Error sending error:%d.\n", ProgName, ierr );
        }
   }
   return;
}



