# xml2xcel.d
#
# Picks up xml files from a specified directory and reads the contents. 
# Adds the data to xcel list
# Copy the file and current xcel list to other directory.
#
# If it has trouble interpreting the file, it saves it to subdir ./trouble. 

# Basic Module information
#-------------------------
MyModuleId        MOD_XML2XCEL     # module id 
RingName          HYPO_RING	       # shared memory ring for output
HeartBeatInterval 30               # seconds between heartbeats to statmgr

LogFile           1                # 0 log to stderr/stdout only; 
                                   # 1 log to stderr/stdout and disk;
                                   # 2 log to disk module log only.

Debug             1                # 1=> debug output. 0=> no debug output

# Data file manipulation
#-----------------------
GetFromDir      /home/picker/smxml/calpine  # look for files in this directory
OutputDir       /home/picker/smxml/calpine/outdir # put files in this directory
SumDir          /home/picker/smxml/calpine/summary # put summary file in this directory

# Peer (remote partner) heartbeat manipulation
#---------------------------------------------
PeerHeartBeatFile  HEARTBT.TXT     # Name of the heartbeat file. 
PeerHeartBeatInterval 30           # seconds between heartbeats to statmgr

#LogHeartBeatFile 1                # If non-zero, write contents of each
                                   #   heartbeat file to the daily log.


