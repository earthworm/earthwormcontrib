#
#                      Make file for ls_cta
#                         Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib


WRITETAPE = ls_cta.o tapeio.o

ls_cta: $(WRITETAPE)
	cc -o $B/ls_cta $(WRITETAPE) -mt -lm -lposix4 -lthread -lc


# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/ls_cta*
