
    /*******************************************************************
     *                              ls_cta                             *
     *                                                                 *
     *  Program to list files from a continuous tape archive (cta)     *
     *  This is a Solaris-only program.                                *
     *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <time_ew.h>

int TarTv( char *, int * );


int main( int argc, char **argv )
{
   int  rc;
   int  status;
   char *tapeName;
   char defaultTapeName[] = "/dev/rmt/0n";

/* Get the name of the tape device
   *******************************/
   tapeName = (argc < 2) ? &defaultTapeName[0] : argv[1];

/* Use tar tv to list the contents of the tape.
   tar exit code 3 = tape read error
   *******************************************/
   while ( 1 )
   {
      rc = TarTv( tapeName, &status );

      if ( rc < 0 )                        /* Couldn't invoke tar */
      {  
         printf( "Couldn't invoke tar.  TarTv rc = %d\n", rc );
         break;
      }  

      if ( status > 0 )                    /* A tar error occurred */
      {
         printf( "Tar exit status: %d\n", status );
         break;
      }
   }
   return 0;
}
