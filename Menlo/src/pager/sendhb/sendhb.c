
/* Program to send a heartbeat to getpage via a socket connection.
   Usage: sendhb <clientName> <ServerIP> <ServerPort>
   **************************************************************/

#include <stdio.h>
#include <stdlib.h>             /* for atoi() */
#include <string.h>
#include "sendhb.h"

void SocketSysInit( void );
int  ConnectToGetpage( char *, int );
int  SendToGetpage( char * );


int main( int argc, char *argv[] )
{
   char hb[100];
   char *clientName;
   char *serverIP;
   char *serverPort;

/* Get parameters from command line
   ********************************/
   if ( argc < 4 )
   {
      printf( "Usage: sendhb <clientName> <serverIP> <serverPort>\n" );
      return 0;
   }
   clientName = argv[1];
   serverIP   = argv[2];
   serverPort = argv[3];

/* Initialize the socket system
   ****************************/
   SocketSysInit();

/* Construct the heartbeat message
   *******************************/
   strcpy( hb, "alive:" );
   strcat( hb, clientName );
   strcat( hb, "#" );

/* printf( "Your hb is: %s\n",hb ); */

/* Send the heartbeat message to getpage
   *************************************/
   if ( ConnectToGetpage( serverIP, atoi(serverPort) ) == SENDHB_FAILURE )
   {
      printf( "Can't connect to getpage. Heartbeat not sent.\n" );
      return -1;
   }

   if ( SendToGetpage( hb ) == SENDHB_FAILURE )
   {
      printf( "Error sending heartbeat to getpage.\n" );
      return -1;
   }
   return 0;
}
