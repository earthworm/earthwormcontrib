
   /*************************************************************
    *                        sendit_nt.c                        *
    *************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include "sendhb.h"

/* Declared elsewhere
   ******************/
static SOCKET sock;                    /* Socket identifier */


/**********************************************************
 *                     SocketSysInit()                    *
 *                                                        *
 *              Initialize the socket system              *
 *        We are using Windows socket version 2.2.        *
 **********************************************************/

void SocketSysInit( void )
{
   int     status;
   WSADATA Data;

   status = WSAStartup( MAKEWORD(2,2), &Data );
   if ( status != 0 )
   {
      printf( "WSAStartup failed. Exiting.\n" );
      exit( -1 );
   }
   return;
}


/********************************************************
 *                     SocketClose()                    *
 *                    Close a Socket                    *
 ********************************************************/

void SocketClose( int soko )
{
   closesocket( (SOCKET)soko );
   return;
}


       /************************************************************
        *                    ConnectToGetpage()                    *
        *                                                          *
        *          Get a connection to the remote system.          *
        ************************************************************/

int ConnectToGetpage( char *ServerIP, int ServerPort )
{
   struct sockaddr_in server;         /* Server socket address structure */
   const int          optVal = 1;
   unsigned long      address;

/* Get a new socket descriptor
   ***************************/
   sock = socket( AF_INET, SOCK_STREAM, 0 );
   if ( sock == INVALID_SOCKET )
   {
      printf( "socket() error: %d\n", WSAGetLastError() );
      return SENDHB_FAILURE;
   }

/* Allow reuse of socket addresses
   *******************************/
   if ( setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
                    sizeof(int) ) == SOCKET_ERROR )
   {
      printf( "setsockopt() error: %d\n", WSAGetLastError() );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }

/* Fill in socket address structure
   ********************************/
   address = inet_addr( ServerIP );
   if ( address == INADDR_NONE )
   {
      printf( "Bad server IP address: %s\n", ServerIP );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family      = AF_INET;
   server.sin_port        = htons( (unsigned short)ServerPort );
   server.sin_addr.s_addr = address;

/* Connect to the getpage server.
   The connect call blocks if getpage is not available.
   ***************************************************/
   if ( connect( sock, (struct sockaddr *)&server, sizeof(server) )
        == SOCKET_ERROR )
   {
      int rc = WSAGetLastError();
      if ( rc == WSAETIMEDOUT )
         printf( "connect() returned WSAETIMEDOUT\n" );
      else
         printf( "connect() error: %d\n", rc );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }
   return SENDHB_SUCCESS;
}


       /************************************************************
        *                     SendToGetpage()                      *
        *                                                          *
        *   Send the message to the getpage system.                *
        ************************************************************/

int SendToGetpage( char *buf )
{
   unsigned msgSize = strlen( buf );
   char msgSizeAsc[7];

/* Sanity check
   ************/
   if ( msgSize > 999999 )
   {
      printf( "Error. Message is too large to send.\n" );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }

/* Write message size to socket
   ****************************/
   sprintf( msgSizeAsc, "%6u", msgSize );
   if ( send( sock, msgSizeAsc, 6, 0 ) == SOCKET_ERROR )
   {
      printf( "Error sending message size to socket: %s\n", WSAGetLastError() );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }

/* Send message bytes
   ******************/
   if ( send( sock, buf, msgSize, 0 ) == SOCKET_ERROR )
   {
      printf( "send() error: %s\n", WSAGetLastError() );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }

/* Life is wonderful
   *****************/
   SocketClose( sock );
   return SENDHB_SUCCESS;
}

