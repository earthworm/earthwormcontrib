
      /*************************************************************
       *                        sendit_sol.c                       *
       *************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "sendhb.h"

/* Declared elsewhere
   ******************/
extern int  errno;               /* Contains socket error numbers */
static int  sock;                /* Socket descriptor */


/**********************************************************
 *                     SocketSysInit()                    *
 *                                                        *
 *               Dummy function in Solaris                *
 **********************************************************/

void SocketSysInit( void )
{
   return;
}


/********************************************************
 *                     SocketClose()                    *
 *                    Close a Socket                    *
 ********************************************************/

void SocketClose( int soko )
{
   close( soko );
   return;
}


      /************************************************************
       *                    ConnectToGetpage()                    *
       *                                                          *
       *          Get a connection to the remote system.          *
       ************************************************************/

int ConnectToGetpage( char *ServerIP, int ServerPort )
{
   struct sockaddr_in server;         /* Server socket address structure */
   const int optVal = 1;
   unsigned long address;

/* Get a new socket descriptor
   ***************************/
   sock = socket( AF_INET, SOCK_STREAM, 0 );
   if ( sock == -1 )
   {
      printf( "socket() error: %s\n", strerror(errno) );
      return SENDHB_FAILURE;
   }

/* Allow reuse of socket addresses
   *******************************/
   if ( setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
                    sizeof(int) ) == -1 )
   {
      printf( "setsockopt() error: %s\n", strerror(errno) );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }

/* Fill in socket address structure
   ********************************/
   address = inet_addr( ServerIP );
   if ( address == -1 )
   {
      printf( "Bad server IP address: %s\n", ServerIP );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family      = AF_INET;
   server.sin_port        = htons((unsigned short)ServerPort);
   server.sin_addr.s_addr = address;

/* Connect to the getpage server.
   The connect call blocks if getpage is not available.
   ***************************************************/
   if ( connect( sock, (struct sockaddr *)&server, sizeof(server) ) == -1 )
   {
      printf( "connect() error: %s\n", strerror(errno) );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }
   return SENDHB_SUCCESS;
}


       /************************************************************
        *                     SendToGetpage()                      *
        *                                                          *
        *   Send the message to the getpage system.                *
        ************************************************************/

int SendToGetpage( char *buf )
{
   unsigned msgSize = strlen( buf );
   char msgSizeAsc[7];
   int  sockerr;
   int  optlen = sizeof(int);

/* Sanity check
   ************/
   if ( msgSize > 999999 )
   {
      printf( "Error. Message is too large to send.\n" );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }

/* send() will crash if a socket error occurs
   and we don't detect it using getsockopt.
   ******************************************/
   if ( getsockopt( sock, SOL_SOCKET, SO_ERROR, (char *)&sockerr,
        &optlen ) == -1 )
   {
      printf( "getsockopt() error: %s", strerror(errno) );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }
   if ( sockerr != 0 )
   {
      printf( "Error detected by getsockopt(): %s\n", strerror(sockerr) );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }

/* Write message size to socket
   ****************************/
   sprintf( msgSizeAsc, "%6u", msgSize );
   if ( send( sock, msgSizeAsc, 6, NULL ) == -1 )
   {
      printf( "Error writing message size to socket: %s\n", strerror(errno) );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }

/* Send message bytes
   ******************/
   if ( send( sock, buf, msgSize, NULL ) == -1 )
   {
      printf( "send() error: %s\n", strerror(errno) );
      SocketClose( sock );
      return SENDHB_FAILURE;
   }

/* Life is wonderful
   *****************/
   SocketClose( sock );
   return SENDHB_SUCCESS;
}

