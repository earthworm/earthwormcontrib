#
#                    Make file for getpage
#

O = getpage.o config.o socket.o procmsg.o gettime.o patient.o \
    log.o filesize.o toqpage.o sendhb.o

getpage: $O
	cc -o getpage $O -lm -lsocket -lnsl -lposix4 -lc

