#
# getpage.d   The getpage configuration file.
#
#
# This program acts as a server to remote computers that send pager messages
# and heartbeats.
#
# MyIP is the IP address of the computer running getpage.
# If MyIP = 0.0.0.0, getpage will listen to all addresses.
# MyPort is the well-known port number.
# -------------------------------------------------------
-MyIP            0.0.0.0
-MyPort          2345
#
# This program will queue connection requests from clients which are trying
# to send pager messages and heartbeats.  <backlog> is the maximum number
# of connections which will be queued.
# -------------------------------------------------------------------------
-Backlog         10
#
# If the program doesn't get a whole message within TimeOut seconds of
# accepting a connection, it kills the connection and accepts the next
# connection.
# --------------------------------------------------------------------
-TimeOut         10
#
# Getpage will create up to <nLogFiles> log files of size <maxLogFileSize>
# bytes.  The most recent log file is named <logFileName>.  Successively older
# log files are named <logFileName>.0, <logFileName>.1, <logFileName>.2, etc.
# ----------------------------------------------------------------------------
-logFileName     /home/picker/getpage/log/getpageLog
-nLogFiles       10
-maxLogFileSize  500000
#
# Getpage can make heartbeats by invoking the sendhb program.
# <HbSec> is the getpage heartbeat interval in seconds.
# If HbSec is 0, no heartbeats are sent.
# HbName is the name assigned to each heartbeat.
# ----------------------------------------------------------
-HbSec          120
-HbName         mnlons1_getpage   # from mnlons1
-ToIP           130.118.43.77     # to mnlons2
-ToPort         2345
#
# If a patient dies, the <NextOfKin> pager group will receive an obituary.
# The message will include SysName, eg:   mnlons1: cta1 dead
# -----------------------------------------------------------------------
-SysName        mnlons1
#
# <Patient> lines specify the names of systems that send heartbeat messages
# to getpage.  If no heartbeats are received for <tsec> seconds, getpage
# will send a page to the NextOfKin group saying the patient is dead.
# -------------------------------------------------------------------------
#
#          Patient           Next
#           Name            of Kin     tsec
#          -------          ------     ----
-Patient   ad1mp           earthworm   3600
-Patient   ad1ch           earthworm   3600
-Patient   campbell        earthworm   1800
-Patient   ctsmenlo1       earthworm   3600
-Patient   heli3           earthworm    900
-Patient   image1          earthworm   3600
-Patient   k2-1            earthworm   3600
-Patient   nano1           earthworm   3600
-Patient   mnlodd1         earthworm    900
-Patient   mnlons2         earthworm    900
-Patient   mnlons2_getpage earthworm    900
-Patient   mnlons2_qpage   earthworm    900
-Patient   ncss3           earthworm    600
