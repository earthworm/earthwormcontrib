
 /****************************************************************************
  *                                getpage.c                                 *
  *                                                                          *
  *  Program to get pager messages via socket connections from client        *
  *  machines.  Messages are passed on to qpage.                             *
  *                                                                          *
  *  getpage accepts heartbeat messages from client machines and sends out   *
  *  dead/alive pages.                                                       *
  *                                                                          *
  *  getpage sends heartbeat messages to another instance of getpage, so     *
  *  the other getpage can report if getpage dies on this machine.           *
  ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include "getpage.h"

/* Function declarations
   *********************/
void     GetConfig( char * );
void     LogConfig( void );
void     InitServerSocket( void );
int      GetMsgFromSocket( char [] );
int      ProcessPagerMsg( char [] );
int      CheckDeadPatient( void );
void     SocketSysInit( void );
unsigned sleep( unsigned int );
int      SendHb( char *, char *, int );

char configFileName[80];


int main( int argc, char *argv[] )
{
   char   *progName = argv[0];
   time_t tHbPrev   = 0;             /* Time of previous heartbeat */

   extern char logFileName[];        /* Full pathname of primary log file */
   extern int  nLogFiles;            /* Number of log files */
   extern int  maxLogFileSize;       /* In bytes */
   extern int  HbSec;                /* Sec between heartbeats sent by getpage */
   extern char HbName[128];          /* Name assigned to heartbeats */
   extern char ToIP[80];             /* IP address to send heartbeats */
   extern int  ToPort;               /* Port to send heartbeats to */

/* Get the name of the configuration file
   from the command line
   **************************************/
   strcpy( configFileName, "/home/picker/getpage/etc/getpage.d" );
   if ( argc > 1 )
      strcpy( configFileName, argv[1] );

/* Initialize the socket system.
   Dummy function in Solaris.
   ****************************/
   SocketSysInit();

/* Read configuration file, initialize logging,
   and log the configuration file.
   *******************************************/
   GetConfig( configFileName );
   log_init( progName, logFileName, nLogFiles, maxLogFileSize );
   LogConfig();

/* Initialize the server socket
   ****************************/
   InitServerSocket();

/* Get pager messages via socket connection.
   This is an infinite loop, so the program never exits.
   ****************************************************/
   while ( 1 )
   {
      static char msg[BUFLEN];
      time_t now = time( 0 );

/* Generate meaningful heartbeat
   *****************************/
      if ( (HbSec > 0) && ((now-tHbPrev)>=HbSec) )
      {
         if ( SendHb( HbName, ToIP, ToPort ) == _FAILURE )
            log( "ft", "Error sending heartbeat to remote getpage.\n" );
         tHbPrev = now;
      }

/* Get all available heartbeats and pager messages
   from client programs
   ***********************************************/
      while ( GetMsgFromSocket( msg ) == _SUCCESS )
      {
         if ( ProcessPagerMsg(msg) != _SUCCESS )
            log( "ft", "ProcessPagerMsg() error.\n" );
      }

/* Invoke the heartbeat processor
   ******************************/
      if ( CheckDeadPatient() != _SUCCESS )
         log( "ft", "CheckDeadPatient() error.\n" );

      sleep( 1 );
   }
}

