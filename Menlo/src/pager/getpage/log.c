
     /*************************************************************
      *                           log.c                           *
      *                                                           *
      *           This is a standalone version of logit.          *
      *                                                           *
      *  First, call log_init().  Then, call log().               *
      *  These functions are NOT multi-thread safe.               *
      *************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

void TzSet( char * );
int  FileSize( char *, int * );

static char   LogName[128];          /* Name of primary config file */
static int    NLogFiles;
static int    MaxFileSize;           /* In bytes */
static FILE   *fpl;                  /* Pointer to log file */
static int    init = 0;              /* 1 if log_init has been called */


   /*************************************************************************
    *                              log_init                                 *
    *                                                                       *
    *       Call this function once before the other log routines.          *
    *                                                                       *
    *   logName:     Name of the log file, including path.                  *
    *                                                                       *
    *   nLogFiles:   Maximum number of log files.  Older files will be      *
    *                named <logName>.0, <logName>.1, etc.                   *
    *                Zero is a valid entry.                                 *
    *                                                                       *
    *   maxFileSize: Maximum file size in bytes.                            *
    *************************************************************************/

void log_init( char *progname, char *logName, int nLogFiles, int maxFileSize )
{
   struct tm *res;
   time_t    now;
   int       fileSize;

/* Check init flag
   ***************/
   if ( init )
   {
      printf( "Error: log_init called more than once. Exiting.\n" );
      exit( -1 );
   }
   init = 1;

/* Save parameters as static variables
   ***********************************/
   strncpy( LogName, logName, 128 );
   LogName[127] = 0;
   NLogFiles    = nLogFiles;
   MaxFileSize  = maxFileSize;
   if ( NLogFiles < 1 ) return;

/* If the primary log file exists and it is too big,
   rename the whole set of log files.
   ************************************************/
   if ( (FileSize( LogName, &fileSize ) == 0) &&
        (fileSize > maxFileSize ) )
   {
      int i;
      char oldName[128];
      char newName[128];

      for ( i = NLogFiles-2; i >= 0; i-- )
      {
         if ( i > 0 )
            sprintf( oldName, "%s.%d", LogName, i-1 );
         else
            strcpy( oldName, LogName );

         sprintf( newName, "%s.%d", LogName, i );
         rename( oldName, newName );
      }
   }

/* Open new log file or append to the existing one
   ***********************************************/
   fpl = fopen( LogName, "a" );
   if ( fpl == NULL )
   {
      printf( "Error opening log file <%s>. Exiting.\n", LogName );
      exit( -1 );
   }

/* Print startup message to log file
   *********************************/
   time( &now );
   res = gmtime( &now );
   fprintf( fpl, "\n----------------------------------------------------\n" );
   fprintf( fpl, "Starting %s at UTC_%04d%02d%02d_%02d:%02d:%02d\n", progname,
            (res->tm_year + 1900), (res->tm_mon + 1), res->tm_mday,
             res->tm_hour, res->tm_min, res->tm_sec );
   fprintf( fpl, "----------------------------------------------------\n" );
   fflush ( fpl );
   return;
}


   /*******************************************************************
    *                              log()                              *
    *                                                                 *
    *           Function to log a message to a disk file.             *
    *                                                                 *
    *  Optionally, also log to standard error.                        *
    *                                                                 *
    *  flag: A string controlling where output is written:            *
    *        If any character is 'f', output is written to log file.  *
    *        If any character is 'e', output is written to stderr.    *
    *        If any character is 'c', output is written to stdout.   *
    *        If any character is 't', output is time stamped.         *
    *                                                                 *
    *  The rest of calling sequence is identical to printf.           *
    *******************************************************************/

void log( char *flag, char *format, ... )
{
   struct tm    *res;
   auto va_list ap;
   static char  *fl;
   int          sterr      = 0;      /* 1 if output goes to stderr */
   int          stout      = 0;      /* 1 if output goes to stdout */
   int          file       = 0;      /* 1 if output goes to log file */
   int          time_stamp = 0;      /* 1 if output is time-stamped */
   int          fileSize;

/* Check init flag
   ***************/
   if ( !init )
   {
      fprintf( stderr, "Error. Call log_init() before log(). Exiting.\n" );
      return;
   }

/* No logging
   **********/
   if ( NLogFiles < 1 ) return;

/* Check flag argument
   *******************/
   fl = flag;
   while ( *fl != '\0' )
   {
      if ( *fl == 'f' ) file       = 1;
      if ( *fl == 'e' ) sterr      = 1;
      if ( *fl == 'c' ) stout      = 1;
      if ( *fl == 't' ) time_stamp = 1;
      fl++;
   }

/* If the primary log file is too big, close it.
   Then, rename the whole set of log files,
   and create a new primary log file.
   *********************************************/
   if ( file )
   {
      if ( (FileSize( LogName, &fileSize ) == 0) &&
           (fileSize > MaxFileSize ) )
      {
         int i;
         char oldName[128];
         char newName[128];

         fclose( fpl );

         for ( i = NLogFiles-2; i >= 0; i-- )
         {
            if ( i > 0 )
               sprintf( oldName, "%s.%d", LogName, i-1 );
            else
               strcpy( oldName, LogName );

            sprintf( newName, "%s.%d", LogName, i );
            rename( oldName, newName );
         }

         fpl = fopen( LogName, "a" );
         if ( fpl == NULL )
         {
            printf( "Error opening log file <%s>. Exiting.\n", LogName );
            exit( -1 );
         }
      }
   }

/* Write UTC time and argument list to buffer
   ******************************************/
   if ( time_stamp )
   {
      char   tbuf[30];
      time_t now = time( 0 );

      res = gmtime( &now );
      sprintf( tbuf, "%04d%02d%02d_UTC_%02d:%02d:%02d ",
            (res->tm_year + 1900), (res->tm_mon + 1), res->tm_mday,
             res->tm_hour, res->tm_min, res->tm_sec );

      if ( file  ) fprintf( fpl,    "%s", tbuf );
      if ( stout ) printf( "%s", tbuf );
      if ( sterr ) fprintf( stderr, "%s", tbuf );
   }

/* Write to log file
   *****************/
   if ( file )
   {
      va_start( ap, format );
      vfprintf( fpl, format, ap );
      va_end( ap );
      fflush( fpl );
   }

/* Write buffer to standard output
   *******************************/
   if ( stout )
   {
      va_start( ap, format );
      vfprintf( stdout, format, ap );
      va_end( ap );
   }

/* Write buffer to standard error
   ******************************/
   if ( sterr )
   {
      va_start( ap, format );
      vfprintf( stderr, format, ap );
      va_end( ap );
   }

   return;
}

