
     /*****************************************************
      *                     sleep()                       *
      *                                                   *
      *  Windows emulation of the Unix sleep() function.  *
      *****************************************************/

#include <windows.h>


unsigned int sleep( unsigned int sec )
{
   Sleep( (1000 * sec) );
   return 0;
}
