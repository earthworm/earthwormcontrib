
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>               /* for fstat */
#include <sys/stat.h>                /* for fstat */
#include "getpage.h"

extern int     nPatient;             /* Number of patients */
extern PATIENT Patient[MAXPATIENT];  /* Array of patients */

void GetTimeStr( char [] );
int  SendToQpage( char *, char * );


        /**********************************************************
         *                  ProcessHeartBeat()                    *
         *                                                        *
         *  If a heartbeat is received from a patient, declare    *
         *  the patient to be alive.  If the patient was          *
         *  previously dead, notify the NextOfKin pager group.    *
         **********************************************************/

int ProcessHeartbeat( char *patientName )
{
   char   pmsg[256];            /* String to contain pager message */
   int    j;
   time_t now = time( 0 );      /* Current time */
   char   stringNow[20];

/* Look up patient name in patient list.
   If the patient has returned from the dead, send a page.
   ******************************************************/
   for ( j = 0; j < nPatient; j++ )
   {
      if ( strcmp( patientName, Patient[j].name ) == 0 )
      {
         if ( Patient[j].health == DEAD )
         {
            int rc;

/* Assemble the final pager message from the date-time
   string and the pager message from the input command
   ***************************************************/

            GetTimeStr( stringNow );
            sprintf( pmsg, "%s %s alive", stringNow, Patient[j].name );
            log( "cft", "Sending page to %s\n", Patient[j].nextOfKin );
            log( "cf", "%s\n", pmsg );

            rc = SendToQpage( Patient[j].nextOfKin, pmsg );
            if ( rc == -1 )
            {
               log( "ft", "Pager message not sent. Error invoking qpage.\n" );
               return _FAILURE;
            }
            log( "ft", "Message sent to qpage.\n" );
         }
         Patient[j].health     = ALIVE;
         Patient[j].tLastHeart = now;
         break;
      }
   }
   return _SUCCESS;
}


        /**********************************************************
         *                  CheckDeadPatient()                    *
         *                                                        *
         *  Determine whether any patients have died.  If so,     *
         *  notify the NextOfKin pager group.                     *
         **********************************************************/

int CheckDeadPatient( void )
{
   char   pmsg[256];            /* String to contain pager message */
   int    j;
   time_t now = time( 0 );      /* Current time */
   char   stringNow[20];

/* If a patient has died, send an obituary page
   ********************************************/
   for ( j = 0; j < nPatient; j++ )
   {
      if ( Patient[j].health == ALIVE )
      {
         if ( (now - Patient[j].tLastHeart) >= Patient[j].tsec )
         {
            int rc;

            Patient[j].health = DEAD;
            GetTimeStr( stringNow );
            sprintf( pmsg, "%s %s dead", stringNow, Patient[j].name );
            log( "cft", "Sending page to %s\n", Patient[j].nextOfKin );
            log( "cf", "%s\n", pmsg );
            rc = SendToQpage( Patient[j].nextOfKin, pmsg );
            if ( rc == -1 )
            {
               log( "ft", "Pager message not sent. Error invoking qpage.\n" );
               return _FAILURE;
            }
            log( "ft", "Message sent to qpage.\n" );
         }
      }
   }
   return _SUCCESS;
}

