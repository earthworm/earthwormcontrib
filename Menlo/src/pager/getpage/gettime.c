
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

      /*****************************************************
       *                   GetTimeStr()                    *
       *                                                   *
       *  Get the current time from the system clock as a  *
       *  string, which will be prepended to the pager     *
       *  message.                                         *
       *****************************************************/

void GetTimeStr( char timeStr[] )
{
   time_t now = time(0);
   struct tm    *res;

   res = gmtime( &now );
   sprintf( timeStr, " %d/%d/%02d %02d:%02d ",
         (res->tm_mon + 1), res->tm_mday,
         (res->tm_year + 1900) % 100,
          res->tm_hour, res->tm_min );
   return;
}

