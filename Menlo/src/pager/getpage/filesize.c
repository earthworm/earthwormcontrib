
#include <sys/types.h>
#include <sys/stat.h>


int FileSize( char *fname, int *fileSize )
{
   struct stat buf;

   if ( stat( fname, &buf ) == -1 )
      return -1;

   *fileSize = buf.st_size;
   return 0;
}

