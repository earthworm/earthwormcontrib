
        /*****************************************************
         *                     toqpage.c                     *
         *****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "getpage.h"


      /*************************************************************
       *                       SendToQpage()                       *
       *                                                           *
       *  Send message to recipient, using qpage.                  *
       *  Qpage will add the message to its message queue and      *
       *  immediately return.                                      *
       *  Returns -1 if qpage cannot be invoked.                   *
       *************************************************************/

int SendToQpage( char *recip, char *msg )
{
   char command[BUFLEN];
// int  msglen;
// int  maxmsglen = BUFLEN - NAMESIZE - 12;

/* Truncate message to BUFLEN characters
   *************************************/
// msglen = (msglen < maxmsglen) ? msglen : maxmsglen;
// msg[msglen] = 0;

   snprintf( command, BUFLEN, "qpage -p %s \"%s\"", recip, msg );
   command[BUFLEN-1] = 0;
// log( "c", "%s\n", command );
   return system( command );
}
