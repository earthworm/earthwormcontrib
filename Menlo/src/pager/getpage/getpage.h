
/*   getpage.h    */

#include <time.h>

#define _TIMEOUT     -2
#define _FAILURE     -1
#define _SUCCESS      0
#define DEAD          0
#define ALIVE         1
#define BUFLEN      500
#define MAXPATIENT  100
#define NAMESIZE     40

/* Function prototypes
   *******************/
void log_init( char *, char *, int, int );
void log( char *, char *, ... );

typedef struct
{
   char   name[NAMESIZE];
   int    tsec;
   int    health;                /* 1 if alive, 0 if dead */
   time_t tLastHeart;            /* Time of last heartbeat */
   char   nextOfKin[NAMESIZE];   /* Next of kin pager group */
} PATIENT;

