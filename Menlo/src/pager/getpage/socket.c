
      /*************************************************************
       *                          socket.c                         *
       *************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/filio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <stropts.h>
#include "getpage.h"

/* Declared elsewhere
   ******************/
extern char MyIP[80];            /* IP address of system to receive msg's */
extern int  MyPort;              /* The well-known port number */
extern int  Backlog;             /* Max number of queued connections */

static int listensock;           /* Accept connections on this socket */
static int connectsock;          /* Connecting socket descriptor */


     /**********************************************************
      *                     SocketSysInit()                    *
      **********************************************************/

void SocketSysInit( void )
{
   return;
}


          /************************************************
           *                SocketClose()                 *
           ************************************************/

void SocketClose( int sock )
{
   close( sock );
   return;
}


           /***********************************************
            *              InitServerSocket()             *
            ***********************************************/

void InitServerSocket( void )
{
   struct sockaddr_in server;    /* Server socket address structure */
   const int optVal = 1;
   unsigned long lOnOff = 1;
   unsigned long address;

/* Get a new socket descriptor
   ***************************/
   listensock = socket( AF_INET, SOCK_STREAM, 0 );
   if ( listensock == -1 )
   {
      log( "ft", "socket() error: %s\n", strerror(errno) );
      exit( -1 );
   }

/* Allow reuse of socket addresses
   *******************************/
   if ( setsockopt( listensock, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
                    sizeof(int) ) == -1 )
   {
      log( "ft", "setsockopt() error: %s\n", strerror(errno) );
      SocketClose( listensock );
      exit( -1 );
   }

/* Fill in socket address structure
   ********************************/
   address = inet_addr( MyIP );
   if ( address == -1 )
   {
      log( "ft", "Bad server IP address: %s  Exiting.\n", MyIP );
      SocketClose( listensock );
      exit( -1 );
   }
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family      = AF_INET;
   server.sin_port        = htons( (unsigned short)MyPort );
   server.sin_addr.S_un.S_addr = address;

/* Bind a local address to the socket descriptor
   *********************************************/
   if ( bind( listensock, (struct sockaddr *)&server, sizeof(server) ) == -1 )
   {
      log( "ft", "bind() error: %s\n", strerror(errno) );
      SocketClose( listensock );
      exit( -1 );
   }

/* Set the maximum number of pending connections
   *********************************************/
   if ( listen( listensock, Backlog ) == -1 )
   {
      log( "ft", "listen() error: %s\n", strerror(errno) );
      SocketClose( listensock );
      exit( -1 );
   }

/* Set the listening socket to non-blocking mode
   *********************************************/
   if ( ioctl( listensock, FIONBIO, &lOnOff ) == -1 )
   {
      log( "ft", "ioctl() FIONBIO error: %d\n", strerror(errno) );
      SocketClose( listensock );
      exit( -1 );
   }
   return;
}


           /*****************************************************
            *                      recvn()                      *
            *  Receive msgSize bytes from a socket connection.  *
            *****************************************************/

int recvn( int connsock, char msg[], int msgSize, int timeOut )
{
   int    nBytesToRead = msgSize;
   int    nread = 0;
   time_t startTime = time(0);

   while ( 1 )
   {
      int rc = recv( connsock, &msg[nread], nBytesToRead, 0 );

      if ( rc == -1 )
      {
         if ( errno != EWOULDBLOCK )
         {
            log( "cft", "recv() error: %s\n", strerror(errno) );
            return _FAILURE;
         }
      }
      else                  /* Successful read from socket */
      {
         nread += rc;
         nBytesToRead -= rc;

         if ( nBytesToRead <= 0 )
            return _SUCCESS;
      }

      if ( (time(0) - startTime) > timeOut )
         return _TIMEOUT;

      sleep( 1 );           /* Wait for more bytes to show up */
   }
}


              /***********************************************
               *              GetMsgFromSocket()             *
               ***********************************************/

int GetMsgFromSocket( char msg[] )
{
   struct sockaddr_in client;          /* Client socket address structure */
   int    connsock;                    /* Read from this socket */
   int    clientlen = sizeof(client);  /* Client socket structure length */
   char   msgSizeAsc[7];
   int    msgSize;
   int    rc;
   const  int nchar = 6;               /* Number of characters in message */

   extern int TimeOut;                 /* Seconds */

/* Accept a TCP socket connection.  accept() will not block.
   ********************************************************/
   connsock = accept( listensock, (struct sockaddr *)&client, &clientlen );
   if ( connsock == -1 )
   {
      if ( errno != EWOULDBLOCK )
         log( "ft", "accept() error: %s\n", strerror(errno) );

      return _FAILURE;
   }
/* log( "ft", "Accepted connection from %s\n", inet_ntoa(client.sin_addr) ); */

/* Read the message size from the socket.
   The size is six ASCII characters long,
   right justified and padded with blanks.
   **************************************/
   rc = recvn( connsock, msgSizeAsc, nchar, TimeOut );
   if ( (rc == _FAILURE) || (rc == _TIMEOUT) )
   {
      SocketClose( connsock );
      return _FAILURE;
   }
   msgSizeAsc[nchar] = 0;               /* Null Terminate */

   if ( sscanf( msgSizeAsc, "%d", &msgSize ) < 1 )
   {
      log( "ft", "Error decoding message size.\n" );
      SocketClose( connsock );
      return _FAILURE;
   }

   if ( msgSize > BUFLEN )
   {
      log( "ft", "Error. Message too large for buffer.\n" );
      SocketClose( connsock );
      return _FAILURE;
   }

/* Read the message from the socket
   ********************************/
   rc = recvn( connsock, msg, msgSize, TimeOut );
   if ( (rc == _FAILURE) || (rc == _TIMEOUT) )
   {
      SocketClose( connsock );
      return _FAILURE;
   }
   msg[msgSize] = 0;          /* Null terminate the message */

/* Close the socket connection
   ***************************/
   SocketClose( connsock );
   return _SUCCESS;
}


       /************************************************************
        *                    ConnectToGetpage()                    *
        *                                                          *
        *          Get a connection to the remote system.          *
        ************************************************************/

int ConnectToGetpage( char *ServerIP, int ServerPort )
{
   struct sockaddr_in server;         /* Server socket address structure */
   const int optVal = 1;
   unsigned long address;

/* Get a new socket descriptor
   ***************************/
   connectsock = socket( AF_INET, SOCK_STREAM, 0 );
   if ( connectsock == -1 )
   {
      log( "ft", "socket() error: %s\n", strerror(errno) );
      return _FAILURE;
   }

/* Allow reuse of socket addresses
   *******************************/
   if ( setsockopt( connectsock, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
                    sizeof(int) ) == -1 )
   {
      log( "ft", "setsockopt() error: %s\n", strerror(errno) );
      SocketClose( connectsock );
      return _FAILURE;
   }

/* Fill in socket address structure
   ********************************/
   address = inet_addr( ServerIP );
   if ( address == -1 )
   {
      printf( "Bad server IP address: %s\n", ServerIP );
      SocketClose( connectsock );
      return _FAILURE;
   }
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family      = AF_INET;
   server.sin_port        = htons( (unsigned short)ServerPort );
   server.sin_addr.s_addr = address;

/* Connect to the server.
   The connect call blocks if getpage is not available.
   ***************************************************/
   if ( connect( connectsock, (struct sockaddr *)&server, sizeof(server) ) == -1 )
   {
      log( "ft", "connect() error: %s\n", strerror(errno) );
      SocketClose( connectsock );
      return _FAILURE;
   }
   return _SUCCESS;
}


       /************************************************************
        *                     SendToGetpage()                      *
        *                                                          *
        *   Send the message to the getpage system.                *
        ************************************************************/

int SendToGetpage( char *msg )
{
   unsigned msgSize = strlen( msg );
   char     msgSizeAsc[7];
   int      sockerr;
   int      optlen = sizeof(int);

/* Sanity check
   ************/
   if ( msgSize > 999999 )
   {
      log( "ft", "Error. Message is too large to send.\n" );
      SocketClose( connectsock );
      return _FAILURE;
   }

/* send() will crash if a socket error occurs
   and we don't detect it using getsockopt.
   ******************************************/
   if ( getsockopt( connectsock, SOL_SOCKET, SO_ERROR, (char *)&sockerr,
        &optlen ) == -1 )
   {
      printf( "getsockopt() error: %s", strerror(errno) );
      SocketClose( connectsock );
      return _FAILURE;
   }
   if ( sockerr != 0 )
   {
      printf( "Error detected by getsockopt(): %s\n", strerror(sockerr) );
      SocketClose( connectsock );
      return _FAILURE;
   }

/* Write message size to socket
   ****************************/
   sprintf( msgSizeAsc, "%6u", msgSize );
   if ( send( connectsock, msgSizeAsc, 6, 0 ) == -1 )
   {
      log( "ft", "Error sending message size to socket: %s\n", strerror(errno) );
      SocketClose( connectsock );
      return _FAILURE;
   }

/* Send message bytes
   ******************/
   if ( send( connectsock, msg, msgSize, 0 ) == -1 )
   {
      log( "ft", "send() error: %s\n", strerror(errno) );
      SocketClose( connectsock );
      return _FAILURE;
   }

   SocketClose( connectsock );
   return _SUCCESS;
}

