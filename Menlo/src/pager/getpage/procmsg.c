
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "getpage.h"

void GetTimeStr( char [] );
int  SendToQpage( char *, char * );
int  ProcessHeartbeat( char * );


      /*************************************************************
       *                     ProcessPagerMsg()                     *
       *                                                           *
       *  This function decodes a command from one of the client   *
       *  systems.  There are two valid commands: "group" and      *
       *  "alive".  group commands contains pager messages to be   *
       *  sent to qpage.  alive commands contain heartbeats.       *
       *                                                           *
       *  Accepts:                                                 *
       *     msg = Message obtained from client program.           *
       *************************************************************/

int ProcessPagerMsg( char msg[] )
{
   int len = strlen( msg );
   static char buf[BUFLEN];   /* Work buffer */
   char *command_ptr;         /* Could be "group" or "alive" */
   char command[30];
   char timeStr[16];          /* String to prepend to pager message */
   char *patientName = NULL;

/* Get the command
   ***************/
   strcpy( buf, msg );
   command_ptr = strtok( buf, " :#\n\t" );
   if ( command_ptr == NULL )
   {
      log( "ft", "Error. No command in pager message.\n" );
      return _FAILURE;
   }
   strcpy( command, command_ptr );

/* This is a "group" command
   *************************/
   if ( strcmp( command, "group" ) == 0 )
   {
      char *group_ptr;            /* Pointer to pager group */
      char *pmsg_ptr;             /* Pointer to pager message */
      char *pound_ptr;            /* Pointer to # sign */
      char group[80];             /* Pager group */
      static char pmsg[BUFLEN];   /* Final pager message */
      int  rc;

/* Get the group name from the command
   ***********************************/
      group_ptr = strtok( NULL, " :#\n\t" );
      if ( group_ptr == NULL )
      {
         log( "ft", "Error. Got a group command, but no group specified.\n" );
         return _FAILURE;
      }
      strcpy( group, group_ptr );

/* Get the pager message from the command
   **************************************/
      pmsg_ptr = strtok( NULL, " :#\n\t" );
      if ( pmsg_ptr == NULL )
      {
         log( "ft", "Error. No pager message in group command.\n" );
         return _FAILURE;
      }
      strcpy( buf, msg );           /* Get new copy of original command */

/* Replace the first # sign in pager message with a newline character.
   The # sign is the terminator for the pager command.
   ******************************************************************/
      pound_ptr = strchr( pmsg_ptr, '#' );
      if ( pound_ptr != NULL ) *pound_ptr = '\0';

/* Assemble the final pager message from the date-time
   string and the pager message from the input command
   ***************************************************/
      GetTimeStr( timeStr );
      strcpy( pmsg, timeStr );
      strcat( pmsg, pmsg_ptr );

/* Write to log file and stdout
   ****************************/
      log( "cft", "Sending page to %s\n%s\n", group, pmsg );

/* Invoke qpage
   ************/
      rc = SendToQpage( group, pmsg );
      if ( rc == -1 )
      {
         log( "ft", "Pager message not sent. Error invoking qpage.\n" );
         return _FAILURE;
      }
      log( "ft", "Message sent to qpage.\n" );
   }

/* This is a heartbeat. Get name of patient that sent it.
   *****************************************************/
   else if ( strcmp( command, "alive" ) == 0 )
   {
      int  i;
      char *patientName   = strtok( NULL, " :#\n\t" );
      int  patientNameLen = strlen( patientName );

      if ( patientName == NULL )
      {
         log( "ft", "Error. Got an alive command, but no patient specified.\n" );
         return _FAILURE;
      }
      patientName[patientNameLen++] = 0;

      for ( i = 0; i < strlen(patientName); i++ )
         patientName[i] = tolower( patientName[i] );

      log( "ft", "Heartbeat received from %s\n", patientName );

      if ( ProcessHeartbeat( patientName ) == _FAILURE )
         return _FAILURE;
   }

/* Unknown command
   ***************/
   else
   {
      log( "ft", "Unknown command: [%s]\n", command );
      return _FAILURE;
   }

   return _SUCCESS;
}
