
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "getpage.h"

/* Global configuration file parameters
   ************************************/
char    MyIP[80];             /* IP address of system to receive msg's */
int     MyPort;               /* The well-known port number */
char    logFileName[128];     /* Full pathname of primary log file */
int     nLogFiles;            /* Number of log files */
int     maxLogFileSize;       /* In bytes */
int     Backlog;              /* Max number of queued connections */
int     nPatient;             /* Number of patients */
PATIENT Patient[MAXPATIENT];  /* Array of patients */
int     HbSec;                /* Sec between heartbeats sent by getpage */
char    HbName[128];          /* Name assigned to heartbeats */
char    ToIP[80];             /* IP address to send heartbeats */
int     ToPort;               /* Port to send heartbeats to */
int     TimeOut;              /* Recv() timeout interval, sec */


void GetConfig( char *configFileName )
{
   FILE *fp;
   int  gotMyIP           = 0;
   int  gotMyPort         = 0;
   int  gotLogFileName    = 0;
   int  gotNLogFiles      = 0;
   int  gotMaxLogFileSize = 0;
   int  gotBacklog        = 0;
   int  gotHbSec          = 0;
   int  gotHbName         = 0;
   int  gotToIP           = 0;
   int  gotToPort         = 0;
   int  gotTimeOut        = 0;

/* Set default values
   ******************/
   nPatient = 0;
   HbSec    = 120;
   ToPort   = 2345;
   strcpy( HbName, "getpage_4" );
   strcpy( ToIP, "130.118.43.8" );

   fp = fopen( configFileName, "r" );
   if ( fp == NULL )
   {
      printf( "Error opening file %s. Exiting.\n", configFileName );
      exit( -1 );
   }

   while ( 1 )
   {
      char line[80];
      char *rc;

      fgets( line, 80, fp );
      if ( ferror( fp ) ) break;
      if ( feof( fp ) )   break;
      rc = strtok( line, " \t\n" );
      if ( rc == NULL ) continue;
      if ( strcmp( rc, "-MyIP" ) == 0 )
      {
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         strcpy( MyIP, rc );
         gotMyIP = 1;
      }
      else if ( strcmp( rc, "-MyPort" ) == 0 )
      {
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         MyPort = atoi( rc );
         gotMyPort = 1;
      }
      else if ( strcmp( rc, "-logFileName" ) == 0 )
      {
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         strcpy( logFileName, rc );
         gotLogFileName = 1;
      }
      else if ( strcmp( rc, "-nLogFiles" ) == 0 )
      {
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         nLogFiles = atoi( rc );
         gotNLogFiles = 1;
      }
      else if ( strcmp( rc, "-maxLogFileSize" ) == 0 )
      {
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         maxLogFileSize = atoi( rc );
         gotMaxLogFileSize = 1;
      }
      else if ( strcmp( rc, "-Backlog" ) == 0 )
      {
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         Backlog = atoi( rc );
         gotBacklog = 1;
      }
      else if ( strcmp( rc, "-Patient" ) == 0 )
      {
         int i;
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         strncpy( Patient[nPatient].name, rc, NAMESIZE );
         for ( i = 0; i < strlen(Patient[nPatient].name); i++ )
            Patient[nPatient].name[i] = tolower( Patient[nPatient].name[i] );
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         strncpy( Patient[nPatient].nextOfKin, rc, NAMESIZE );
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         if ( nPatient < MAXPATIENT )
         {
            Patient[nPatient].tsec       = atoi( rc );
            Patient[nPatient].health     = ALIVE;
            Patient[nPatient].tLastHeart = time( 0 );
            nPatient++;
         }
         else
            log( "f", "Too many patients.  MAXPATIENT = %d\n", MAXPATIENT );
      }
      else if ( strcmp( rc, "-HbSec" ) == 0 )
      {
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         HbSec = atoi( rc );
         gotHbSec = 1;
      }
      else if ( strcmp( rc, "-HbName" ) == 0 )
      {
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         strcpy( HbName, rc );
         gotHbName = 1;
      }
      else if ( strcmp( rc, "-ToIP" ) == 0 )
      {
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         strcpy( ToIP, rc );
         gotToIP = 1;
      }
      else if ( strcmp( rc, "-ToPort" ) == 0 )
      {
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         ToPort = atoi( rc );
         gotToPort = 1;
      }
      else if ( strcmp( rc, "-TimeOut" ) == 0 )
      {
         rc = strtok( NULL, " \t\n" );
         if ( rc == NULL ) continue;
         TimeOut = atoi( rc );
         gotTimeOut = 1;
      }
   }
   fclose( fp );

   if ( !gotMyIP )
   {
      printf( "No -MyIP commands in getpage.d  Exiting.\n" );
      exit( -1 );
   }
   if ( !gotMyPort )
   {
      printf( "No -MyPort commands in getpage.d  Exiting.\n" );
      exit( -1 );
   }
   if ( !gotLogFileName )
   {
      printf( "No -logFileName commands in getpage.d  Exiting.\n" );
      exit( -1 );
   }
   if ( !gotNLogFiles )
   {
      printf( "No -nLogFiles commands in getpage.d  Exiting.\n" );
      exit( -1 );
   }
   if ( !gotMaxLogFileSize )
   {
      printf( "No -maxLogFileSize commands in getpage.d  Exiting.\n" );
      exit( -1 );
   }
   if ( !gotBacklog )
   {
      printf( "No -Backlog commands in getpage.d  Exiting.\n" );
      exit( -1 );
   }
   if ( !gotTimeOut )
   {
      printf( "No -TimeOut commands in getpage.d  Exiting.\n" );
      exit( -1 );
   }
   if ( !nPatient )
      printf( "Warning: No -Patient commands in getpage.d\n" );
   if ( !gotHbSec )
      printf( "Warning: No -HbSec commands in getpage.d\n" );
   if ( !gotHbName )
      printf( "Warning: No -HbName commands in getpage.d\n" );
   if ( !gotToIP )
      printf( "Warning: No -ToIP commands in getpage.d\n" );
   if ( !gotToPort )
      printf( "Warning: No -ToPort commands in getpage.d\n" );
   return;
}


void LogConfig( void )
{
   int i;

   log( "f", "MyIP:           %s\n", MyIP );
   log( "f", "MyPort:         %d\n", MyPort );
   log( "f", "logFileName:    %s\n", logFileName );
   log( "f", "nLogFiles:      %d\n", nLogFiles );
   log( "f", "maxLogFileSize: %d\n", maxLogFileSize );
   log( "f", "Backlog:        %d\n", Backlog );
   log( "f", "HbSec:          %d\n", HbSec );
   log( "f", "HbName:         %s\n", HbName );
   log( "f", "ToIP:           %s\n", ToIP );
   log( "f", "ToPort:         %d\n", ToPort );

   for ( i = 0; i < nPatient; i++ )
      log( "f", "Patient:        %-12s   %-12s   tsec: %4d\n", Patient[i].name,
              Patient[i].nextOfKin, Patient[i].tsec );
   log( "f", "\n" );
   return;
}

