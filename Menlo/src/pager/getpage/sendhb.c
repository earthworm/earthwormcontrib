
     /**********************************************************
      *                        sendhb.c                        *
      *                                                        *
      *  Send a heartbeat to another getpage program via a     *
      *  socket connection.                                    *
      *  Arguments:                                            *
      *     clientName: Name of this system                    *
      *     ServerIP:   IP address of getpage to receive       *
      *                 heartbeat                              *
      *     ServerPort: Port of getpage to receive heartbeat   *
      **********************************************************/

#include <stdio.h>
#include <string.h>
#include "getpage.h"

int ConnectToGetpage( char *, int );
int SendToGetpage( char * );


int SendHb( char *clientName, char *serverIP, int serverPort )
{
   char hb[100];

/* Construct the heartbeat message
   *******************************/
   strcpy( hb, "alive:" );
   strcat( hb, clientName );
   strcat( hb, "#" );

/* Send the heartbeat message to getpage
   *************************************/
   if ( ConnectToGetpage( serverIP, serverPort ) == _FAILURE )
   {
      log( "ft", "ConnectToGetpage() error.\n" );
      return _FAILURE;
   }

   if ( SendToGetpage( hb ) == _FAILURE )
   {
      log( "ft", "SendToGetpage() error.\n" );
      return _FAILURE;
   }
   return _SUCCESS;
}
