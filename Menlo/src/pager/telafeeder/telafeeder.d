#
#                    Telafeeder Configuration File
#                            (telafeeder.d)
#
MyModuleId   MOD_TELAFEEDER
#
#   <HeartBeatStatmgr> is the time in seconds between heartbeats
#   sent to the status manager.  The status manager will report an error
#   if heartbeats are not received from the status manager at regular
#   intervals.
#
HeartbeatStatmgr  30
#
#   <RingName> is the name of the transport ring that telafeeder
#   lives on.  Should be the same ring as the statmgr.
#
RingName       SCNL_RING
#
# List the message logos to grab from transport ring:
#
#              Installation       Module       Message Type
GetPagesFrom   INST_MENLO      MOD_WILDCARD    # pager msg
#
#   <LogFile> sets switch for writing a log file to disk.
#   If 1, write a log file; if 0, don't.
#
LogFile  1
#
#   <TimeOutSec> is the number of seconds telafeeder will try to connect
#   to getpage and send a pager message.
#
TimeOutSec  30
#
#   <ServerIP> is the IP address of the computer running the getpage
#   server program.
#
ServerIP   130.118.43.77   # mnlons2
#
#   <ServerPort> is the well-known port used by getpage
#
ServerPort  2345

