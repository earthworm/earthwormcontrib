
    /************************************************************
     *                       telafeeder                         *
     *                                                          *
     *  Grab pager messages from a transport ring and send to   *
     *  the getpage program using a socket connection.          *
     *                                                          *
     *  Also, send heartbeats to the getpage program.           *
     ************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>

#define MAXLOGO   2

/* Function declarations
   *********************/
void GetConfig( char * );
void LookupParams( void );
void LogConfig( void );
void SocketSysInit( void );
int  GetpageConnect( char [], int );
int  TelaSend( char [], int, int );
void CloseSocketConnection( void );
int  SendEWMessage( unsigned char, short, char * );

/* Parameters read from configuration file
   ***************************************/
static char   RingName[20];      /* Name of transport ring to attach to */
unsigned char MyModuleId;         /* Telafeeder's module id */
static int    HeartbeatStatmgr;   /* Heartbeat interval(sec) to statmgr */
static int    LogFile;            /* 1 if logging should be done to disk */
MSG_LOGO      GetLogo[MAXLOGO];   /* Array of requested module,type,inst */
static short  nLogo;              /* Number of logos being requested */
static int    TimeOutSec;         /* Time out if can't send to getpage */
static char   ServerIP[20];       /* IP address of computer to receive pages */
static int    ServerPort;         /* Well-known port number */

/* Get these from the earthworm.h tables using getutil.c functions
   ***************************************************************/
static long          RingKey;      /* Key to transport ring */
static unsigned char InstId;       /* Local installation id */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static pid_t         myPid;       /* For restarts by startstop   */
static SHM_INFO      region;      /* Memory region info structure */

/* Error messages used by telafeeder
 ***********************************/
#define ERR_MSGTOOBIG   0         /* message on ring too big for buffer */
#define ERR_GETPAGE     1         /* error communicating with getpage   */


int main( int argc, char *argv[] )
{

   MSG_LOGO  putlogo;
   MSG_LOGO  logo;
   char      msg[200];
   char      note[256];
   int       rc;
   long      length;
   time_t    t1, tnow;


/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      fprintf( stderr, "Usage: telafeeder <configfile>\n" );
      return 0;
   }

/* Read the configuration file
   ***************************/
   GetConfig( argv[1] );

/* Get parameters from the earthworm.d file
   ****************************************/
   LookupParams();

/* Initialize name of log-file & open it
   *************************************/
   logit_init( argv[1], (short)MyModuleId, 1024, LogFile );

/* Log parameters from the configuration file
   ******************************************/
   LogConfig();

/* Initialize the socket system
   ****************************/
   SocketSysInit();

/* Get process ID for heartbeat messages
   *************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
     logit( "e","telafeeder: Cannot get pid. Exiting.\n" );
     return -1;
   }

/* Attach to the transport ring (Must already exist)
   *************************************************/
   tport_attach( &region, RingKey );

   putlogo.type   = TypeHeartBeat;
   putlogo.mod    = MyModuleId;
   putlogo.instid = InstId;

/* Flush transport ring on startup
   *******************************/
   while ( tport_getmsg( &region, GetLogo, nLogo, &logo, &length,
                         msg, sizeof(msg) ) != GET_NONE );

/* Initialize timers
   *****************/
   time( &tnow );
   t1 = 0;        /* force heartbeat right away */

/* Wait for terminate flag to be set
   *********************************/
   while ( tport_getflag( &region ) != TERMINATE &&
           tport_getflag( &region ) != myPid )
   {
      int  i;
      char *group;
      char *pmsg;
      char byteCount[7];
      sleep_ew( 1000 );

/* Send heartbeat to status manager
   ********************************/
      time( &tnow );

      if ( (tnow - t1) >= HeartbeatStatmgr )
      {
         if ( SendEWMessage( TypeHeartBeat, 0, "" ) != PUT_OK ) 
         {
            logit( "et", "telafeeder: Error sending TYPE_HEARTBEAT to ring.\n");
         }
         t1 = tnow;
      }

/* Get message of TYPE_PAGE from transport ring.
   This could be an "alive" or "group" message.
   ********************************************/
      rc = tport_getmsg( &region, GetLogo, nLogo, &logo, &length,
                          msg, sizeof(msg) );
      if ( rc == GET_NONE   ) continue;
      if ( rc == GET_TOOBIG )
      {
         sprintf( note, "Skipped TYPE_PAGE msg[%hd] too big for msgbuffer[%ld]", 
                  length, sizeof(msg) );
         if( SendEWMessage( TypeError, (short)ERR_MSGTOOBIG, note ) != PUT_OK ) 
         {
            logit( "et", "telafeeder: Error sending TYPE_ERROR msg to ring.\n");
         }
         continue;
      }
      msg[length] = '\0';
      logit( "t", "Received TYPE_PAGE: %s\n", msg );

/* Open a socket connection to the getpage server
   **********************************************/
      rc = GetpageConnect( ServerIP, ServerPort );
      if ( rc == -1 )
      {
         if( SendEWMessage( TypeError, (short)ERR_GETPAGE, 
                           "Cannot connect to getpage." ) != PUT_OK ) 
         {
            logit( "et", "telafeeder: Error sending TYPE_ERROR msg to ring.\n");
         }
         sleep_ew( 5000 );
         continue;
      }

/* Send byte count to getpage server
   *********************************/
      sprintf( byteCount, "%6d", length );
      rc = TelaSend( byteCount, 6, TimeOutSec );
      if ( rc < 0 )
      {
         if( SendEWMessage( TypeError, (short)ERR_GETPAGE, 
                           "Error sending byte count to getpage." ) != PUT_OK ) 
         {
            logit( "et", "telafeeder: Error sending TYPE_ERROR msg to ring.\n");
         }
         CloseSocketConnection();
         sleep_ew( 5000 );
         continue;
      }

/* Send pager message to getpage server
   ************************************/
      rc = TelaSend( msg, length, TimeOutSec );
      if ( rc < 0 )
      {
         if( SendEWMessage( TypeError, (short)ERR_GETPAGE, 
                           "Error sending heartbeat or page to getpage." ) != PUT_OK ) 
         {
            logit( "et", "telafeeder: Error sending TYPE_ERROR msg to ring.\n");
         }
         CloseSocketConnection();
         sleep_ew( 5000 );
         continue;
      }
      if ( strncmp(msg, "alive:", 6) == 0 )
      {
         for ( i = 0; i < strlen(msg); i++ )
            if ( msg[i] == '#' ) msg[i] = '\0';
         logit( "t", "Heartbeat sent from %s to %s:%d\n", msg+6, ServerIP, ServerPort );
      }

      if ( strncmp(msg, "group:", 6) == 0 )
      {
         group = strtok( msg+7, " " );
         pmsg  = strtok( NULL, "#\n" );
         logit( "t", "Page sent to %s via qpage on %s:%d\n", group, ServerIP, ServerPort );
         logit( "", "        %s\n", pmsg );
      }

/* Message sent ok
   ***************/
      CloseSocketConnection();
   }
   logit( "t", "telafeeder: Termination requested. Exiting.\n" );
   return 0;
}


 /*****************************************************************
  * GetConfig()  processes command file using kom.c functions     *
  *              Exits if any errors are encountered              *
  *****************************************************************/

void GetConfig( char *configfile )
{
   int  ncommand;     /* # of commands you expect to process */
   char init[10];     /* Flags, one byte for each command */
   int  nmiss;        /* Number of commands that were missed */
   char *com;
   char *str;
   int  nfiles;
   int  success;
   int  i;

/* Set to zero one init flag for each required command
   ***************************************************/
   ncommand = 8;
   nLogo    = 0;

   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
 **********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      fprintf( stderr,
              "telafeeder: Error opening command file <%s>. Exiting.\n",
               configfile );
      exit( -1 );
   }

/* Process all configuration files
   *******************************/
   while ( nfiles > 0 )       /* While there are configuration files open */
   {
      while ( k_rd() )        /* Read next line from active file  */
      {
         com = k_str();       /* Get first token from line */

/* Ignore blank lines and comments
   *******************************/
         if( !com )          continue;
         if( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
         if ( com[0] == '@' )
         {
            success = nfiles+1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               fprintf( stderr,
                  "telafeeder: Error opening command file <%s>. Exiting.\n",
                   &com[1] );
               exit( -1 );
            }
               continue;
         }

/* Process anything else as a command
   **********************************/
         if ( k_its( "HeartbeatStatmgr" ) )
         {
            HeartbeatStatmgr = k_int();
            init[0] = 1;
         }
         else if ( k_its( "RingName" ) )
         {
            str = k_str();
            if( str ) strcpy( RingName, str );
            init[1] = 1;
         }
         else if ( k_its( "LogFile" ) )
         {
            LogFile = k_int();
            init[2] = 1;
         }
         else if ( k_its( "MyModuleId" ) )
         {
            if ( ( str=k_str() ) )
            {
               if ( GetModId( str, &MyModuleId ) != 0 )
               {
                  fprintf( stderr,
                          "telafeeder: Invalid module name <%s>. Exiting.\n",
                           str );
                  exit( -1 );
                }
             }
             init[3] = 1;
         }
         else if ( k_its( "GetPagesFrom" ) )
         {
            if ( nLogo >= MAXLOGO )
            {
               fprintf( stderr,
                   "telafeeder: Too many <GetPagesFrom> commands in <%s>",
                    configfile );
               fprintf( stderr, "; max=%d. Exiting.\n", (int) MAXLOGO );
               exit( -1 );
            }
            if ( ( str=k_str() ) )
            {
               if ( GetInst( str, &GetLogo[nLogo].instid ) != 0 )
               {
                  fprintf( stderr,
                      "telafeeder: Invalid installation name <%s>", str );
                  fprintf( stderr, " in <GetPagesFrom> cmd. Exiting.\n" );
                  exit( -1 );
               }
            }
            if ( ( str=k_str() ) )
            {
               if ( GetModId( str, &GetLogo[nLogo].mod ) != 0 )
               {
                  fprintf( stderr,
                      "telafeeder: Invalid module name <%s>", str );
                  fprintf( stderr, " in <GetPagesFrom> cmd. Exiting.\n" );
                  exit( -1 );
               }
            }
            if ( GetType( "TYPE_PAGE", &GetLogo[nLogo].type ) != 0 )
            {
               fprintf( stderr,
                   "telafeeder: Invalid message type <TYPE_PAGE>" );
               fprintf( stderr, ". Exiting.\n" );
               exit( -1 );
            }
            nLogo++;
            init[4] = 1;
         }
         else if ( k_its( "TimeOutSec" ) )
         {
            TimeOutSec = k_int();
            init[5] = 1;
         }
         else if ( k_its( "ServerIP" ) )
         {
            str = k_str();
            if( str ) strcpy( ServerIP, str );
            init[6] = 1;
         }
         else if ( k_its( "ServerPort" ) )
         {
            ServerPort = k_int();
            init[7] = 1;
         }

/* Unknown command
   ***************/
         else
         {
            fprintf( stderr, "telafeeder: <%s> unknown command in <%s>.\n",
                     com, configfile );
            continue;
         }
         if ( k_err() )
         {
            fprintf( stderr,
                    "telafeeder: Bad <%s> command in <%s>. Exiting.\n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* Check flags for missed commands
   *******************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
       fprintf( stderr, "telafeeder: Error. No " );
       if ( !init[0] ) fprintf( stderr, "<HeartbeatStatmgr> " );
       if ( !init[1] ) fprintf( stderr, "<RingName> " );
       if ( !init[2] ) fprintf( stderr, "<LogFile> " );
       if ( !init[3] ) fprintf( stderr, "<MyModuleId> " );
       if ( !init[4] ) fprintf( stderr, "<GetPagesFrom> " );
       if ( !init[5] ) fprintf( stderr, "<TimeOutSec> " );
       if ( !init[6] ) fprintf( stderr, "<ServerIP> " );
       if ( !init[7] ) fprintf( stderr, "<ServerPort> " );
       fprintf( stderr, "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }
   return;
}


 /***********************************************************
  *  LogConfig()  Log parameters from the config file.      *
  ***********************************************************/

void LogConfig( void )
{
   int i;

   logit( "", "HeartbeatStatmgr: %d\n", HeartbeatStatmgr );
   logit( "", "RingName:         %s\n", RingName );
   logit( "", "LogFile:          %d\n", LogFile );
   logit( "", "MyModuleId:       %u\n", MyModuleId );
   logit( "", "TimeOutSec:       %d\n", TimeOutSec );
   logit( "", "ServerIP:         %s\n", ServerIP );
   logit( "", "ServerPort:       %d\n", ServerPort );

   for ( i = 0; i < nLogo; i++ )
      logit( "", "GetPagesFrom:     inst %u  mod %u\n",
         GetLogo[i].instid, GetLogo[i].mod );

   logit( "", "\n" );
   return;
}


 /***********************************************************
  *  LookupParams( ) Get info from the earthworm.d file     *
  ***********************************************************/

void LookupParams( void )
{
/* Look up key to shared memory region
   ***********************************/
   RingKey = GetKey( RingName );
   if ( RingKey == -1 )
   {
      fprintf( stderr, "telafeeder: Invalid ring name <%s>. Exiting.\n",
               RingName );
      exit( -1 );
   }

/* Look up installation of interest
   ********************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      fprintf( stderr,
         "telafeeder: Error getting local installation id. Exiting.\n" );
      exit( -1 );
   }

/* Look up heartbeat message type
   ******************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      fprintf( stderr,
         "telafeeder: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      fprintf( stderr,
         "telafeeder: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   return;
}

/********************************************************************** 
 * SendEWMessage()                                                    *
 * Builds heartbeat and error msgs and puts them in shared memory.    * 
 * Puts any other message types in shared memory without reformatting *
 **********************************************************************/

int SendEWMessage( unsigned char msg_type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[512];
   int         res;
   long        size;
   time_t      t;

   logo.instid = InstId;
   logo.mod    = MyModuleId;
   logo.type   = msg_type;

   time( &t );

   if( msg_type == TypeHeartBeat ) {
      sprintf( msg, "%ld %d\n", (long)t, myPid );
   }
   else if( msg_type == TypeError ) {
      sprintf( msg, "%ld %d %s\n", (long)t, ierr, note);
      logit("et", "%s\n", note );
   }
   else { /* just pass the note without reformatting */
      sprintf( msg, "%s", note );
   }
   size = strlen( msg );  /* don't include null byte in message */
   res = tport_putmsg( &region, &logo, size, msg );

   return res;
}

