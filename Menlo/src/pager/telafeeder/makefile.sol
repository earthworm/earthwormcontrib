
#                          Make file for telafeeder
#                               Solaris Version

CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib


O = telafeeder.o socket_sol.o $L/kom.o $L/logit.o \
    $L/getutil.o $L/sleep_ew.o $L/time_ew.o $L/transport.o


telafeeder: $O
	cc -o $B/telafeeder $O -lm -lsocket -lnsl -lposix4

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/telafeeder*
