
      /*************************************************************
       *                        socket_sol.c                       *
       *                                                           *
       *  This file contains functions that are specific to        *
       *  Solaris.                                                 *
       *************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/filio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <earthworm.h>

static int sock;              /* Socket descriptor */


      /**********************************************************
       *                     SocketSysInit()                    *
       *  Dummy in Solaris.                                     *
       **********************************************************/

void SocketSysInit( void )
{
   return;
}


      /**********************************************************
       *                 CloseSocketConnection()                *
       **********************************************************/

void CloseSocketConnection( void )
{
   close( sock );
   return;
}


      /************************************************************
       *                     GetpageConnect()                     *
       *                                                          *
       *         Get a connection to the getpage server.          *
       *                                                          *
       *  Returns  0 if all ok.                                   *
       *  Returns -1 if an error occurred.                        *
       ************************************************************/

int GetpageConnect( char ServerIP[], int ServerPort )
{
   struct sockaddr_in server;        /* Server socket address structure */
   const int          optVal = 1;
   unsigned long      lOnOff = 1;
   unsigned long      address;

/* Get a new socket descriptor
   ***************************/
   sock = socket( AF_INET, SOCK_STREAM, 0 );
   if ( sock == -1 )
   {
      logit( "et", "socket() error: %s\n", strerror(errno) );
      return -1;
   }

/* Allow reuse of socket addresses
   *******************************/
   if ( setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
                    sizeof(int) ) == -1 )
   {
      logit( "et", "setsockopt() error: %s\n", strerror(errno) );
      close( sock );
      return -1;
   }

/* Fill in socket address structure
   ********************************/
   address = inet_addr( ServerIP );
   if ( address == -1 )
   {
      logit( "et", "Bad server IP address: %s  Exiting.\n", ServerIP );
      close( sock );
      return -1;
   }
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family = AF_INET;
   server.sin_port   = htons((unsigned short)ServerPort);
   server.sin_addr.S_un.S_addr = address;

/* Connect to the server.
   connect() blocks if getpage server is not available.
   ***************************************************/
   if ( connect( sock, (struct sockaddr *)&server, sizeof(server) ) == -1 )
   {
      logit( "et", "connect() error: %s\n", strerror(errno) );
      close( sock );
      return -1;
   }

/* Set socket to non-blocking mode
   *******************************/
   if ( ioctl( sock, FIONBIO, &lOnOff ) == -1 )
   {
      logit( "et", "ioctl() FIONBIO error: %s\n", strerror(errno) );
      close( sock );
      return -1;
   }
   return 0;
}


  /*********************************************************************
   *                              TelaSend()                           *
   *                                                                   *
   *  Sends the first nbytes characters in buf to the socket sock.     *
   *  The data is sent in blocks of up to 4096 bytes.                  *
   *  The function will time out if the data can't be sent within      *
   *  TimeOutSec seconds.                                              *
   *                                                                   *
   *  Returns 0 if all ok.                                             *
   *  Returns -1 if an error or timeout occurred.                      *
   *********************************************************************/

int TelaSend( char buf[], int nbytes, int TimeOutSec )
{
   int nBytesToWrite = nbytes;
   int nwritten      = 0;

   while ( nBytesToWrite > 0 )
   {
      int    selrc;
      fd_set writefds;
      struct timeval selectTimeout;

/* See if the socket is writable.  If the socket is not
   writeable within TimeOutSec seconds, return an error.
   ****************************************************/
      selectTimeout.tv_sec  = TimeOutSec;
      selectTimeout.tv_usec = 0;
      FD_ZERO( &writefds );
      FD_SET( sock, &writefds );
      selrc = select( sock+1, 0, &writefds, 0, &selectTimeout );
      if ( selrc == -1 )
      {
         logit( "et", "select() error: %s", strerror(errno) );
         return -1;
      }
      if ( selrc == 0 )
      {
         logit( "et", "select() timed out\n" );
         return -1;
      }
      if ( FD_ISSET( sock, &writefds ) )    /* A writeable event occurred */
      {
         int rc;
         int sockerr;
         int optlen = sizeof(int);

/* send() will crash if a socket error occurs
   and we don't detect it using getsockopt.
   ******************************************/
         if ( getsockopt( sock, SOL_SOCKET, SO_ERROR, (char *)&sockerr,
              &optlen ) == -1 )
         {
            logit( "et", "getsockopt() error: %s", strerror(errno) );
            return -1;
         }
         if ( sockerr != 0 )
         {
            logit( "et", "Error detected by getsockopt(): %s\n",
                     strerror(sockerr) );
            return -1;
         }

/* Send the data.  It's unlikely that send() would block,
   since we know that a writeable event occurred.
   *****************************************************/
         rc = send( sock, &buf[nwritten], nBytesToWrite, 0 );
         if ( rc == -1 )
         {
            if ( errno == EWOULDBLOCK )    /* Unlikely */
            {
               logit( "et", "send() would block\n" );
               continue;
            }
            else
            {
               logit( "et", "send() error: %d", strerror(errno) );
               return -1;
            }
         }
         if ( rc == 0 )           /* Shouldn't see this */
         {
            logit( "et", "Error: send() == 0\n" );
            return -1;
         }
         nwritten += rc;
         nBytesToWrite -= rc;
      }
   }
   return 0;
}
