
   /*************************************************************
    *                       socket_win.c                        *
    *                                                           *
    *  This file contains functions that are specific to        *
    *  Windows.                                                 *
    *************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <earthworm.h>

static SOCKET sock;                      /* Socket identifier */


      /********************** SocketSysInit **********************
       *               Initialize the socket system              *
       *         We are using Windows socket version 2.2.        *
       ***********************************************************/
void SocketSysInit( void )
{
   WSADATA Data;
   int     status = WSAStartup( MAKEWORD(2,2), &Data );
   if ( status != 0 )
   {
      logit( "et", "WSAStartup failed. Exiting.\n" );
      exit( -1 );
   }
   return;
}


       /*********************************************************
        *                CloseSocketConnection()                *
        *********************************************************/
void CloseSocketConnection( void )
{
   closesocket( sock );
   return;
}


      /************************************************************
       *                     GetpageConnect()                     *
       *                                                          *
       *         Get a connection to the getpage server.          *
       *                                                          *
       *  Returns  0 if all ok.                                   *
       *  Returns -1 if an error occurred.                        *
       ************************************************************/

int GetpageConnect( char ServerIP[], int ServerPort )
{
   struct sockaddr_in server;        /* Server socket address structure */
   const int          optVal = 1;
   unsigned long      lOnOff=1;
   unsigned long      address;

/* Get a new socket descriptor
   ***************************/
   sock = socket( AF_INET, SOCK_STREAM, 0 );
   if ( sock == INVALID_SOCKET )
   {
      logit( "et", "socket() error: %d\n", WSAGetLastError() );
      return -1;
   }

/* Allow reuse of socket addresses
   *******************************/
   if ( setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
                    sizeof(int) ) == SOCKET_ERROR )
   {
      logit( "et", "setsockopt() error: %d\n", WSAGetLastError() );
      CloseSocketConnection();
      return -1;
   }

/* Fill in socket address structure
   ********************************/
   address = inet_addr( ServerIP );
   if ( address == INADDR_NONE )
   {
      logit( "et", "Bad server IP address: %s  Exiting.\n", ServerIP );
      CloseSocketConnection();
      return -1;
   }
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family = AF_INET;
   server.sin_port   = htons( (unsigned short)ServerPort );
   server.sin_addr.S_un.S_addr = address;

/* Connect to the server.
   The connect call may block if getfile is not available.
   ******************************************************/
   if ( connect( sock, (struct sockaddr *)&server, sizeof(server) )
        == SOCKET_ERROR )
   {
      int rc = WSAGetLastError();
      if ( rc == WSAETIMEDOUT )
         logit( "et", "Error: connect() timed out.\n" );
      else
         logit( "et", "connect() error: %d\n", rc );
      CloseSocketConnection();
      return -1;
   }

/* Set socket to non-blocking mode
   *******************************/
   if ( ioctlsocket( sock, FIONBIO, &lOnOff ) == SOCKET_ERROR )
   {
      logit( "et", "Error %d, changing socket to non-blocking mode\n",
               WSAGetLastError() );
      CloseSocketConnection();
      return -1;
   }
   return 0;
}


  /*********************************************************************
   *                              TelaSend()                           *
   *                                                                   *
   *  Sends the first nbytes characters in buf to the socket.          *
   *  The data is sent in blocks of up to 4096 bytes.                  *
   *  The function will time out if the data can't be sent within      *
   *  TimeOutSec seconds.                                              *
   *                                                                   *
   *  Returns 0 if all ok.                                             *
   *  Returns -1 if an error or timeout occurred.                      *
   *********************************************************************/

int TelaSend( char buf[], int nbytes, int TimeOutSec )
{
   int nBytesToWrite = nbytes;
   int nwritten      = 0;

   while ( nBytesToWrite > 0 )
   {
      int    selrc;
      fd_set writefds;
      struct timeval selectTimeout;

/* See if the socket is writable.  If the socket is not
   writeable within TimeOutSec seconds, return an error.
   ****************************************************/
      selectTimeout.tv_sec  = TimeOutSec;
      selectTimeout.tv_usec = 0;
      FD_ZERO( &writefds );
      FD_SET( sock, &writefds );
      selrc = select( sock+1, 0, &writefds, 0, &selectTimeout );
      if ( selrc == SOCKET_ERROR )
      {
         logit( "et", "select() error: %d", WSAGetLastError() );
         return -1;
      }
      if ( selrc == 0 )
      {
         logit( "et", "select() timed out\n" );
         return -1;
      }
      if ( FD_ISSET( sock, &writefds ) )    /* A writeable event occurred */
      {

/* Send the data.  It's unlikely that send() would block,
   since we know that a writeable event occurred.
   *****************************************************/
         int rc = send( sock, &buf[nwritten], nBytesToWrite, 0 );
         if ( rc == SOCKET_ERROR )
         {
            int winError = WSAGetLastError();

            if ( winError == WSAEWOULDBLOCK )   /* Unlikely */
            {
               logit( "et", "send() would block\n" );
               continue;
            }
            else if ( winError == WSAECONNRESET )
            {
               logit( "et", "send() error: Connection reset by peer.\n" );
               return -1;
            }
            else
            {
               logit( "et", "send() error: %d\n", winError );
               return -1;
            }
         }
         if ( rc == 0 )     /* Shouldn't see this */
         {
            logit( "et", "Error: send() == 0\n" );
            return -1;
         }
         nwritten += rc;
         nBytesToWrite -= rc;
      }
   }
   return 0;
}

