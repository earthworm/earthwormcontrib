/*
 *   THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT.
 *
 *    $Id: organizefile.c 154 2006-01-07 00:34:52Z dietz $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.1  2006/01/07 00:34:52  dietz
 *     Initial file moving program, based heavily on file2ew
 *
 */

/*
 * organizefile.c:
 *
 * Based on code from file2ew.c
 *
 * Periodically checks a specified directory for files.
 * Organizes files into a set of subdirectories based on 
 * the first N characters of the filename.   
 * 
 * Recognizes heartbeat file names; knows the interval at which 
 * they should arrive; writes explicit TYPE_ERROR message to the
 * ring if that interval is exceeded.
 *
 * LDD January2006
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <errno.h>

/* Functions in this source file 
 *******************************/
void organizefile_config ( char * );
void organizefile_lookup ( void );
void organizefile_page   ( char *, char * );
void organizefile_logbeat( FILE *, char * );
void organizefile_status( unsigned char, short, char * );

/* String length definitions
 ***************************/
#define GRP_LEN   25          /* length of page group name              */
#define NAM_LEN  100          /* length of full directory name          */
#define TEXT_LEN NAM_LEN*3

SHM_INFO  Region;             /* shared memory region to use for i/o    */
pid_t     myPid;              /* for restarts by startstop              */

#define MAXPEER 5             /* maximum # of peer heartbeats to expect  */
static int nPeer = 0;         /* actual number of configured peers       */
static int nPage = 0;         /* # groups to page on loss of peer        */
typedef struct _PEERHEART_ {
   char   sysname[NAM_LEN];   /* name of system sending heartbeat        */
   char   hbname[NAM_LEN];    /* name of heartbeat file to expect        */
   int    hbinterval;         /* max allowed secs between heartbeat files*/     
   time_t tlate;              /* time at which this file is overdue      */
   int    hbstatus;           /* current status of this heartbeat        */
} PEERHEART;

/* Things to read or derive from configuration file
 **************************************************/
static char      RingName[20];        /* name of transport ring for i/o    */
static char      MyModName[50];       /* speak as this module name/id      */
static int       LogSwitch;           /* 0 if no logfile should be written */
static int       HeartBeatInterval;   /* seconds betweeen beats to statmgr */
static char      GetFromDir[NAM_LEN]; /* directory to monitor for data     */
static char      MoveToDir[NAM_LEN];  /* top-level dir to move files into  */
static int       SubDirNumChar;       /* build subdir name from 1st NumChar*/
                                      /*   characters of the file name     */
static unsigned  CheckPeriod;         /* secs between looking for new files*/
static int	 OpenTries;
static int	 OpenWait;
static PEERHEART PeerHeartBeat[MAXPEER];   /* monitor heartbeat files here */
static char      PageOnLostPeer[MAXPEER][GRP_LEN]; /* notify if peer dies  */
static int       LogHeartBeatFile;    /* if non-zero, write contents of    */ 
                                      /*  each heartbeat file to daily log */
int	         Debug;               /* non-zero -> debug logging         */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o     */
static unsigned char InstId;        /* local installation id             */
static unsigned char MyModId;       /* Module Id for this program        */
static unsigned char TypeHeartBeat; 
       unsigned char TypeError;
       unsigned char TypePage;

char Text[TEXT_LEN];         /* string for log/error messages      */
char ProgName[NAM_LEN];      /* program name for logging purposes  */
#define LOGIT_LEN TEXT_LEN*2

/* Error messages used by organizefile 
 *************************************/
#define ERR_FILE_OPEN     0   /* error opening a file exclusively   */
#define ERR_PEER_LOST     1   /* no peer heartbeat file for a while */
#define ERR_PEER_ALIVE    2   /* got a peer heartbeat file again    */

/* File handling stuff
**********************/
int main( int argc, char **argv )
{
   int       result;
   int 	     read_error = 0;
   int	     i;
   char      fname[NAM_LEN];
   char      subdir[NAM_LEN];   /* subdirectory to move files into  */
   char      fnew[NAM_LEN*4];   /* complete path of new file name   */
   char     *c;
   FILE     *fp;
   time_t    tnextbeat;    /* next time for local heartbeat  */
   time_t    tnow;         /* current time */
   int       flag;         /* transport flag value */

/* Check command line arguments 
 ******************************/
   if ( argc != 2 )
   {
        fprintf( stderr, "Usage: %s <configfile>\n", argv[0] );
        exit( 0 );
   }
   strcpy( ProgName, argv[1] );
   c = strchr( ProgName, '.' );
   if( c ) *c = '\0';

/* Initialize name of log-file & open it 
 ***************************************/
   logit_init( argv[1], 0, LOGIT_LEN, 1 );   

/* Read the configuration file(s)
 ********************************/
   organizefile_config( argv[1] );
   logit( "" , "%s: Read command file <%s>\n", argv[0], argv[1] );

/* Look up important info from earthworm.h tables
 ************************************************/
   organizefile_lookup();

/* Reinitialize logit do desired logging level
 *********************************************/
   logit_init( argv[1], 0, LOGIT_LEN, LogSwitch );

/* Get process ID for heartbeat messages 
 ***************************************/
   myPid = getpid();
   if( myPid == -1 )
   {
     logit("e","%s: Cannot get pid; exiting!\n", ProgName);
     return( -1 );
   }

/* Make sure target directory exists
 ***********************************/
   if( RecursiveCreateDir( MoveToDir ) != EW_SUCCESS ) {
      logit( "e", "%s: trouble creating MoveToDir directory: %s\n",
             ProgName, MoveToDir ); 
      return( -1 );
   }
   if(Debug)logit("et","%s: MoveToDir directory <%s> exists.\n", 
                   ProgName, MoveToDir);

/* Change to the directory with the input files (and stay here)
 **************************************************************/
   if( chdir_ew( GetFromDir ) == -1 )
   {
      logit( "e", "%s: GetFromDir directory <%s> not found; "
                 "exiting!\n", ProgName, GetFromDir );
      return( -1 );
   }
   if(Debug)logit("et","%s: changed to GetFromDir directory <%s>\n", 
                   ProgName, GetFromDir);

/* Attach to Output shared memory ring 
 *************************************/
   tport_attach( &Region, RingKey );
   logit( "", "%s: Attached to public memory region %s: %d\n", 
          ProgName, RingName, RingKey );

/* Force local heartbeat first time thru main loop
   but give peers the full interval before we expect a file
 **********************************************************/
   tnextbeat  = time(NULL) - 1;
   for( i=0; i<nPeer; i++ ) {
     PeerHeartBeat[i].tlate    = time(NULL) + PeerHeartBeat[i].hbinterval;
     PeerHeartBeat[i].hbstatus = ERR_PEER_ALIVE; 
   }


/****************  top of working loop ********************************/
   while(1)
   {
     /* Check on heartbeats
      *********************/
        tnow = time(NULL);
        if( tnow >= tnextbeat ) {  /* time to beat local heart */
           organizefile_status( TypeHeartBeat, 0, "" );
           tnextbeat = tnow + HeartBeatInterval;
        }
        if( PeerHeartBeat[0].hbinterval ) {     /* if we're monitoring hrtbeats */
           for( i=0; i<nPeer; i++ ) {           /* check each heartbeat file */
              if( tnow > PeerHeartBeat[i].tlate ) {    /* this peer is late! */
                 if( PeerHeartBeat[i].hbstatus == ERR_PEER_ALIVE ) { 
                    int j;                                       /* complain */
                    sprintf(Text, 
                           "No PeerHeartBeatFile <%s> from %s in over %d sec!",
                            PeerHeartBeat[i].hbname,PeerHeartBeat[i].sysname, 
                            PeerHeartBeat[i].hbinterval );
                    organizefile_status( TypeError, ERR_PEER_LOST, Text );
                    for(j=0;j<nPage; j++) organizefile_page(PageOnLostPeer[j],Text);
                 }      
                 PeerHeartBeat[i].hbstatus = ERR_PEER_LOST;   
              }
           }
        }

     /* See if termination has been requested 
      ****************************************/
        flag = tport_getflag( &Region );
 	if( flag == TERMINATE  ||  flag == myPid ) 
        {
           logit( "t", "%s: Termination requested; exiting!\n", ProgName );
           break;
        }

     /* Get a file name
      ******************/    
	result = GetFileName( fname );
	if( result == 1 ) {  /* No files found; wait for one to appear */
           sleep_ew( CheckPeriod*1000 ); 
	   continue;
	}
        if(Debug)logit("et","%s: got file name <%s>\n",ProgName,fname);

     /* Open the file.
      * We open for updating (even though we only want to read it), 
      * as that will hopefully get us an exclusive open. 
      * We don't ever want to move a file that's being written to. 
      ************************************************************/
        fp = NULL;
	for( i=0; i<OpenTries; i++ ) {
           fp = fopen( fname, "rb+" );
           if( fp != NULL ) break;
           sleep_ew( OpenWait );
	}
        if( fp == NULL ) { /* failed to open file; try later! */
           sprintf( Text,"Error: Could not open %s after %d*%d msec",
                    fname, OpenTries, OpenWait );
           organizefile_status( TypeError, ERR_FILE_OPEN, Text );
           continue;
        }
	if( i>0 ) {
           logit("t","Warning: %d attempts required to open file %s\n",
                  i+1, fname);
        }

     /* If it's a heartbeat file, reset tlate and delete the file.
      * We don't care when the heartbeat was created, only when we
      * saw it. This prevents 'heartbeats from the past' from 
      * confusing things, but may not be wise... 
      ***********************************************************/
        for( i=0; i<nPeer; i++ ) { /* does fname match a heartbeatfile? */
           if( strcmp(fname,PeerHeartBeat[i].hbname)==0 ) break;
        }
        if( i < nPeer ) {          /* yes, it's a heartbeat file */
           PeerHeartBeat[i].tlate = time(NULL) + PeerHeartBeat[i].hbinterval;
           if( PeerHeartBeat[i].hbstatus == ERR_PEER_LOST ) {  /* it's alive! */
              int j;
              sprintf(Text,"Received PeerHeartBeatFile <%s> from %s "
                      "(Peer is alive)",
                      PeerHeartBeat[i].hbname, PeerHeartBeat[i].sysname );
              organizefile_status( TypeError, ERR_PEER_ALIVE, Text );
              for(j=0;j<nPage; j++) organizefile_page(PageOnLostPeer[j],Text);
              PeerHeartBeat[i].hbstatus = ERR_PEER_ALIVE;            
           }
           if( LogHeartBeatFile ) organizefile_logbeat( fp, fname );

        /* Remove heartbeat file */
           fclose( fp );
	   if( remove( fname ) != 0) {
	      logit("et",
                    "%s: Cannot delete heartbeat file <%s>;"
                    " exiting!", ProgName, fname );
	      break;
	   }
           continue;
        }

     /* Ignore any core files in the directory (Solaris issue)
        ******************************************************/
        else if( strcmp(fname,"core")==0 ) {
            fclose( fp );
            if( remove( fname ) != 0) {
                logit("et",
                    "%s: Cannot delete core file <%s>;"
                    " exiting!", ProgName, fname );
                break;
            }
            continue;
        }

     /* Build subdirectory name from the 1st chars of filename 
      ********************************************************/
        fclose( fp );
        strncpy( subdir, fname, SubDirNumChar );  /* copy from filename */
        subdir[SubDirNumChar] = 0;                /* null terminate it  */
        sprintf( fnew, "%s/%s", MoveToDir, subdir );

     /* Make sure target subdirectory exists and move the file there
      **************************************************************/
        if( RecursiveCreateDir( fnew ) != EW_SUCCESS ) {
           logit( "e", "%s: trouble creating target directory: %s; exiting!\n",
                   ProgName, fnew ); 
           break;
        }

        sprintf( fnew, "%s/%s/%s", MoveToDir, subdir, fname );
        if( rename_ew( fname, fnew ) != 0 ) {
           logit( "et", "%s: error moving file to %s; exiting!\n", 
                   ProgName, fnew );
           break;
        } else {
	   logit("et","%s: moved file to %s\n",  ProgName, fnew );
        }

   } /* end of while */

/************************ end of working loop ****************************/
	
/* detach from shared memory */
   tport_detach( &Region ); 

/* write a termination msg to log file */
   fflush( stdout );
   return( 0 );

}  
/************************* end of main ***********************************/



/******************************************************************************
 *  organizefile_config() processes command file(s) using kom.c functions;    *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
#define ncommand 13
void organizefile_config( char *configfile )
{
   char     init[ncommand]; /* init flags, one byte for each required command */
   int      nmiss;          /* number of required commands that were missed   */
   char    *com;
   char    *str;
   char     processor[20];
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command 
 *****************************************************/   
   for( i=0; i<ncommand; i++ )  init[i] = 0;

/* Open the main configuration file 
 **********************************/
   nfiles = k_open( configfile ); 
   if ( nfiles == 0 ) {
        logit("e","%s: Error opening command file <%s>; exiting!\n", 
               ProgName, configfile );
        exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {  
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  logit("e","%s: Error opening command file <%s>; exiting!\n",
                         ProgName, &com[1] );
                  exit( -1 );
               }
               continue;
            }
            strcpy( processor, "organizefile_config" );

        /* Process anything else as a command 
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("GetFromDir") ) {
                str = k_str();
                if( str && strlen(str)<NAM_LEN ) {
                   strcpy( GetFromDir, str );
                   init[3] = 1;
                }
            }
  /*4*/     else if( k_its("CheckPeriod") ) {
                CheckPeriod = k_int();
                init[4] = 1;
            }
  /*5*/     else if( k_its("Debug") ) {
                Debug = k_int();
                init[5] = 1;
            }
  /*6*/     else if( k_its("OpenTries") ) {
                OpenTries = k_int();
                init[6] = 1;
            }
  /*7*/     else if( k_its("OpenWait") ) {
                OpenWait = k_int();
                init[7] = 1;
            }
  /*8*/     else if( k_its("PeerHeartBeatFile") ) {
                if( nPeer >= MAXPEER ) {
                   logit( "e", "%s: too many PeerHeartBeatFile cmds "
                          "(max=%d); exiting!\n", ProgName, MAXPEER );
                   exit( -1 );
                }
                if( str = k_str() ) {
                   strncpy( PeerHeartBeat[nPeer].sysname, str, NAM_LEN-1);
                   PeerHeartBeat[nPeer].sysname[NAM_LEN-1]='\0';
                   if( str = k_str() ) {
                      strncpy( PeerHeartBeat[nPeer].hbname, str, NAM_LEN-1);
                      PeerHeartBeat[nPeer].hbname[NAM_LEN-1]='\0';
                      PeerHeartBeat[nPeer].hbinterval = k_int();
                      nPeer++;
                   }
                }
                init[8] = 1;
            }

  /*9*/     else if( k_its("MoveToDir") ) {
                str = k_str();
                if( str && strlen(str)<NAM_LEN ) {
                   strcpy( MoveToDir, str );
                   init[9] = 1;
                }
            }

  /*10*/    else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_int();
                init[10] = 1;
            }
  /*11*/    else if( k_its("LogHeartBeatFile") ) {
                LogHeartBeatFile = k_int();
                init[11] = 1;
            }
  /*12*/    else if( k_its("SubDirNumChar") ) {
                SubDirNumChar = k_int();
                if( SubDirNumChar<=0 || SubDirNumChar>=NAM_LEN ) {
                   logit("e", "%s: Invalid SubDirNumChar value: %d "
                         "(valid range: 1-%d); exiting!\n",
                         ProgName, SubDirNumChar, NAM_LEN-1 );
                   exit( -1 );
                }
                init[12] = 1;
            }

  /*opt*/   else if( k_its("PageOnLostPeer") ) {
                if( nPage >= MAXPEER ) {
                   logit("e", "%s: too many PageOnLostPeer cmds "
                         "(max=%d); exiting!\n", ProgName, MAXPEER );
                   exit( -1 );
                }
                str = k_str();
                if( str && strlen(str)<GRP_LEN ) {
                   strcpy( PageOnLostPeer[nPage], str);
                   nPage++;
                } else {
                   logit("e", "%s: PageOnLostPeer arg <%s> too long; "
                         "must be 1-%d chars; exiting!\n", 
                          ProgName, str, GRP_LEN-1 );
                   exit( -1 );
               }
            }

         /* Unknown command
          *****************/ 
            else {
               logit( "e", "%s: <%s> Unknown command in <%s>.\n", 
                     ProgName, com, configfile );
               continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
               logit( "e", "%s: Bad <%s> command for %s() in <%s>; exiting!\n",
                      ProgName, com, processor, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       logit("e", "%s: ERROR, no ", ProgName );
       if ( !init[0] )  logit("e", "<LogFile> "            );
       if ( !init[1] )  logit("e", "<MyModuleId> "         );
       if ( !init[2] )  logit("e", "<RingName> "           );
       if ( !init[3] )  logit("e", "<GetFromDir> "         );
       if ( !init[4] )  logit("e", "<CheckPeriod> "        );
       if ( !init[5] )  logit("e", "<Debug> "              );
       if ( !init[6] )  logit("e", "<OpenTries> "          );
       if ( !init[7] )  logit("e", "<OpenWait> "           );
       if ( !init[8] )  logit("e", "<PeerHeartBeatFile> "  );
       if ( !init[9] )  logit("e", "<MoveToDir> "          );
       if ( !init[10])  logit("e", "<HeartBeatInterval> "  );
       if ( !init[11])  logit("e", "<LogHeartBeatFile> "   );
       if ( !init[12])  logit("e", "<SubDirNumChar> "      );
       logit("e", "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }

   return;
}

/******************************************************************************
 *  organizefile_lookup( )   Look up important info from earthworm.h tables   *
 ******************************************************************************/
void organizefile_lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 ) {
        logit( "e", "%s:  Invalid ring name <%s>; exiting!\n",
               ProgName, RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      logit( "e", "%s: error getting local installation id; exiting!\n",
             ProgName );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      logit( "e", "%s: Invalid module name <%s>; exiting!\n", 
             ProgName, MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      logit( "e", "%s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n",
             ProgName );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      logit( "e", "%s: Invalid message type <TYPE_ERROR>; exiting!\n",
             ProgName );
      exit( -1 );
   }
   if ( GetType( "TYPE_PAGE", &TypePage ) != 0 ) {
      logit( "e", "%s: Invalid message type <TYPE_PAGE>; exiting!\n",
             ProgName );
      exit( -1 );
   }
   return;
} 

/******************************************************************************
 * organizefile_status() builds a heartbeat or error message & puts it into   *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/
void organizefile_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size;
   long        t;
 
/* Build the message
 *******************/ 
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
        sprintf( msg, "%ld %ld\n\0", t, myPid);
   }
   else if( type == TypeError )
   {
        sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
        logit( "et", "%s: %s\n", ProgName, note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */     

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","%s:  Error sending heartbeat.\n", ProgName );
        }
        else if( type == TypeError ) {
           logit("et","%s:  Error sending error:%d.\n", ProgName, ierr );
        }
   }

   return;
}

/******************************************************************************
 * organizefile_page() builds TYPE_PAGE message & puts it into shared memory. *
 *                     Writes pager messages to log file & screen.            *
 ******************************************************************************/
void organizefile_page( char *group, char *note )
{
   MSG_LOGO    logo;
   char        msg[512];
 
/* Set logo values
 ******************/ 
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = TypePage;

/* Assemble pageit message in msg
   ******************************/
   sprintf( msg, "group: %s %s#\0", group, note ); 
   logit("et","%s: Sending TYPE_PAGE msg: %s\n", ProgName, msg );

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, strlen(msg), msg ) != PUT_OK )
   {
      logit("et","%s: Error sending TYPE_PAGE msg to transport region:\n", 
             ProgName );
   }
   return;
}


/******************************************************************************
 * organizefile_logbeat() writes contents of a file to the log file           *
 ******************************************************************************/
void organizefile_logbeat( FILE *fp, char *name )
{
   char line[NAM_LEN];

   logit( "t","Contents of PeerHeartBeatFile <%s>:\n", name );
   while( fgets(line, NAM_LEN, fp) ) logit("","%s",line);

   return;
}
