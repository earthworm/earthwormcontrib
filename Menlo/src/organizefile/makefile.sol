#
#   THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU HAVE
#   CHECKED IT OUT.
#
#    $Id: makefile.sol 154 2006-01-07 00:34:52Z dietz $
#
#    Revision history:
#     $Log$
#     Revision 1.1  2006/01/07 00:34:52  dietz
#     Initial file moving program, based heavily on file2ew
#
#

CFLAGS = ${GLOBALFLAGS}

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

BINARIES = organizefile.o $L/chron3.o $L/logit.o $L/kom.o $L/getutil.o \
	   $L/sleep_ew.o $L/swap.o $L/time_ew.o $L/transport.o $L/dirops_ew.o 

organizefile: $(BINARIES)
	cc -o $B/organizefile $(BINARIES)  -lm -lposix4

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/organizefile*
