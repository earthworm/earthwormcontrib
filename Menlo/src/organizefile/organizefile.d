# organizefile.d
#
# Picks up files from a specified directory and moves them into a
# new directory/subdirectories based on the file names. This is useful
# for sorting files by date. 
#
# Maintains its own local heartbeat and also monitors the peer's
# heartbeat via a file. Complains if the peer's expected heartbeat
# interval is exceeded; announces resumption of peer's heartbeat.

# Basic Module information
#-------------------------
MyModuleId        MOD_ORGANIZEFILE # module id 
RingName          HYPO_RING	   # shared memory ring for output
HeartBeatInterval 30               # seconds between heartbeats to statmgr

LogFile           1                # 0 log to stderr/stdout only; 
                                   # 1 log to stderr/stdout and disk;
                                   # 2 log to disk module log only.

Debug             0                # 1=> debug output. 0=> no debug output

# Data file manipulation
#-----------------------
GetFromDir      c:\incoming_files  # Look for files in this directory.
CheckPeriod     1                  # Sleep this many seconds between looks.
OpenTries       5                  # How many times we'll try to open a file. 
OpenWait        200                # Milliseconds to wait between open tries.  

MoveToDir       c:\archive         # Top-level directory to move files into.
SubDirNumChar   11                 # Build subdirectory names from the first
                                   #   SubDirNumChar characters of the filename.

# Peer (remote partner) heartbeat manipulation
#---------------------------------------------
PeerHeartBeatFile  terra1  HEARTBT.TXT  600 
                                   # PeerHeartBeatFile takes 3 arguments:
                                   # 1st: Name of remote system that is 
                                   #   sending the heartbeat files.
                                   # 2nd: Name of the heartbeat file. 
                                   # 3rd: maximum #seconds between heartbeat 
                                   #   files. If no new PeerHeartBeatFile arrives
                                   #   in this many seconds, an error message will
                                   #   be sent.  An "unerror message" will be
                                   #   sent after next heartbeat file arrives
                                   #   If 0, expect no heartbeat files.
                                   # Some remote systems may have multiple 
                                   # heartbeat files; list each one in a
                                   # seperate PeerHeartBeatFile command
                                   # (up to 5 allowed).

PageOnLostPeer technician          # Optional command: Name of group to page 
                  	           #   if PeerHeartBeatFile is late. This allows
                                   #   pages to be sent to groups other than 
                                   #   those listed in statmgr.d. Up to 5 
                                   #   PageOnLostPeer commands can be used.
                                   # Must run telafeeder on same system to 
                                   #   actually get the pages sent.

LogHeartBeatFile 1                 # If non-zero, write contents of each
                                   #   heartbeat file to the daily log.


