/***************************************************************
 Utility to erase all files in directory <dir> which were last modified
 more than <nday> days ago.
 This program uses WIN32 API calls.
 Will Kohler 4/3/00
 
 This version of cleandir only operates on files containing substr
 Jim Luetgert 7/16/02
 ***************************************************************/
 
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <direct.h>        /* For _chdir() */
#include <sys/types.h>     /* For _stat() */
#include <sys/stat.h>      /* For _stat() */
#include <windows.h>
#include <kom.h>

#define NAM_LEN 	 100     /* length of full directory name            */
#define MAX_DIRS     50      /* largest number of targets                */

void config_me ( char * );

/* Structure Definitions
 ***********************/
typedef struct _INSTANCE_ {
	char    GetFromDir[NAM_LEN];         /* directory to monitor for data       */
    int     ndays;                       /* Number of remote target directories */
	char    SubString[NAM_LEN];          /* directory to monitor for data       */
} INSTANCE;

INSTANCE        rcp[MAX_DIRS];
static int      NumDirs;             /* number of input directories to watch */
       int      Debug;               /* non-zero -> debug logging            */
static char ProgName[NAM_LEN];     /* program name for logging purposes      */
 
int main( int argc, char *argv[] )
{
   int    i, first;
   char   *c;
   time_t now = time(0);

/* Get command line arguments
   **************************/
   if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", argv[0] );
        exit( 0 );
   }
   strcpy( ProgName, argv[1] );
   c = strchr( ProgName, '.' );
   if( c ) *c = '\0';
           
/* Read the configuration file
 *****************************/
   config_me( argv[1] );

/* Process each file in each directory
   ******************************************/
   for(i=0;i<NumDirs;i++) {

	/* Change current directory
	   ************************/
	   first = 1;
	   if ( _chdir( rcp[i].GetFromDir ) == -1 ) {
	      printf( "Error changing current directory to: %s\n", rcp[i].GetFromDir );
	      printf( "%s\n", strerror(errno) );
	      return -1;
	   }
	   if(Debug) printf( "Changing current directory to: %s\n", rcp[i].GetFromDir );

/* Process each file in the current directory
   ******************************************/
	   while ( 1 ) {
	      static HANDLE   handle;
	      const char      fileMask[] = "*";
	      WIN32_FIND_DATA findData;
	      struct _stat    stat;
	      double          ageDays;
	      char            *fname;

	/* Get the name of a file
	   **********************/
	      if ( first ) {
	         handle = FindFirstFile( fileMask, &findData );
	         if ( handle == INVALID_HANDLE_VALUE ) break;
	         first = 0;
	      } else if ( !FindNextFile(handle, &findData) ) break;

	      fname = findData.cFileName;
	      if(strstr(fname, rcp[i].SubString) == NULL) continue;

	/* Get file status structure
	   *************************/
	      if ( _stat(fname, &stat) == -1 ) {
	         printf( "_stat() error.\n" );
	         printf( "%s\n", strerror(errno) );
	         return -1;
	      }

	/* Look only at regular files which were
	   modified more than <nday> days ago.
	   *************************************/
	      if ( !(stat.st_mode & _S_IFREG) ) continue;

	      ageDays = (now - stat.st_mtime) / 86400.;
	      if ( ageDays < (double)rcp[i].ndays ) continue;

	/* Remove the file
	   ***************/
	      if ( remove(findData.cFileName) == -1 ) {
	         printf( "Error removing file: %s\n", fname );
	         printf( "%s\n", strerror(errno) );
	         continue;
	         return -1;
	      }
	       if(Debug) printf( "Removing file: %s\n", fname );
	/*    printf( "%30s %10.4lf\n", fname, ageDays ); */
	   }
   }
    return 0;
}


/******************************************************************************
 *  config_me() processes command file(s) using kom.c functions;              *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
void config_me( char *configfile )
{
   char    *com, *str;
   int      init = 0;
   int      n, nfiles;
   
   Debug = NumDirs = 0;

/* Open the main configuration file 
 **********************************/
   nfiles = k_open( configfile ); 
   if ( nfiles == 0 ) {
        fprintf( stderr, "%s: Error opening command file <%s>; exiting!\n", 
                 ProgName, configfile );
        exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0) {  /* While there are command files open */
        while(k_rd()) {       /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Process anything else as a command 
         ************************************/
  /*0*/     if( k_its("Purge") ) {
                if ( NumDirs >= MAX_DIRS ) {
                    fprintf( stderr, "%s Too many <Purge> commands in <%s>", 
                             ProgName, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_DIRS );
                    return;
                }
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] == '/' ) str[n-1] = '\0';
                if(str) strncpy( rcp[NumDirs].GetFromDir, str, NAM_LEN );
                
                rcp[NumDirs].ndays = k_int();
                
                str = k_str();
                if(str) strncpy( rcp[NumDirs].SubString, str, NAM_LEN );
                
                NumDirs++;
                init = 1;
            }
  /*1*/     else if( k_its("Debug") ) {
                Debug = k_int();
            }

               /* optional commands */

         /* Unknown command
          *****************/ 
            else {
                fprintf( stderr, "%s: <%s> Unknown command in <%s>.\n", 
                         ProgName, com, configfile );
                continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr, 
                       "%s: Bad <%s> command in <%s>; exiting!\n",
                        ProgName, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }
   
/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   if( !init ) {
       fprintf( stderr, "%s: ERROR, no <Purge> command(s) in <%s>; exiting!\n", ProgName, configfile );
       exit( -1 );
   }
   return;
}

