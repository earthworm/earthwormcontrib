#
# This is the snwgetmenu parameter file. This module gets menu lists
# from the waveserver(s), and creates a file with the tabulated results.

# List of wave servers (ip port comment) to contact to retrieve trace data.
 @/home/earthworm/run/params/all_waveservers.d
  WaveServer 130.118.43.66  16022      "wsv  - wsv4"
  WaveServer 130.118.43.67  16022      "wsv  - wsv5"

# Directory in which to store the menu data for output to the webserver
 LocalTarget   /home/picker/sendfiles/snw/

# Directory in which to store the SNW messages for output to 
# SNW by snwclient.
 SNWDir   /home/picker/gifs/snw/

    # *** Optional Commands ***
# wsTimeout 60 # time limit (secs) for any one interaction with a wave server.

# We accept a command "Debug" which turns on a bunch of log messages
# Debug
