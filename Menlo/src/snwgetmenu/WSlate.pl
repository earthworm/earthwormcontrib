#!/usr/local/bin/perl 

########################################################################
# This is the perl script which creates and drives the waveserver latency pages.
# 
#                                  Jim Luetgert   3/2006
########################################################################

# Define a bunch of global variables for consistency and ease of modification.

$lastmod = "Aug 23, 2005";
$rootdir = "/home/html/ncsn";
$webdir = "/waveforms/snw";
$gifdir = "$rootdir"."$webdir";
$pagecolor = "#ffffcc";			# Colors of various page elements

# Check the query string for further instructions.
#
	$qname = "NO DATA AVAILABLE";
	$query_string = $ENV{'QUERY_STRING'};

	if ( ! defined($query_string) || $query_string eq "") {
	   &make_index;
	}
	else {
		$group = "$query_string";
		($net,$site) = split ('-',$query_string);
	
		&Build_Banner;
		

# This is the default comment list sent by heli1 on startup
# Of form :  
#   PTR.SHZ.NC.--  Start: 20060223_2253_39.76  End:  20060302_0014_38.62               3.38    130.118.43.34:16021

		if(open(NAMES, "$gifdir/WSlatency.txt")) {
			while(<NAMES>) {
				chomp;
				($sitename = $_) =~ s/([^.]*)\.([^.]*).*/$1/;
				($netr,$chan,$siter,$loc) = split ('.',$sitename);
				if($net =~ $netr && $site =~ $siter) {
					print STDOUT "$_ \n";
				}
			}
			close(NAMES);
		}

		print STDOUT " <P> <HR> <P> </body> </html> \n";

}

exit;


########################################################################
# Construct and display the index of all available helicorders
# querytype = 4  (or really old browser)
########################################################################
sub make_index {

	&Build_Banner;
	

print STDOUT <<EOT;
<P><h4>
 The Azgrams 
 displayed here are a selection of channels from the 
 DSTs currently being recorded in Menlo Park.  
 They are each updated once a day for the previous day.
 Each panel represents 24 hours of data and its file is between 100kb and 200kb.<br>
 <P>

EOT
	

	print STDOUT " <P> <HR> <P> </body> </html> \n";

return;

}


########################################################################
# Build the generic banner for a page.
########################################################################
sub Build_Banner {


print STDOUT <<EOT;
	Content-type: text/html

	<HTML>
	<HEAD><TITLE>
	DSTs - Azgrams
	</TITLE></HEAD>
	<BODY BGCOLOR=$pagecolor TEXT=#000000 vlink=purple link=blue>

	<img align=left src="/waveforms/smusgs.gif" alt="USGS"> 

EOT
# Finish up the html file:
print STDOUT <<EOT;

<P> <HR> <P>
EOT

return;

}

########################################################################
