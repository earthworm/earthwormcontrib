/*************************************************************************
 * snwgetmenu.c                                                          *
 *                                                                       *
 * Snwgetmenu is a SeisNetWatch support routine for monitoring the       *
 * latency of data flowing to WaveServers.  Snwgetmenu is designed to be *
 * run from the crontab.  When run, it accesses the menus of a list of   *
 * waveservers and produces 2 text files.  The first, snwdata.txt, is a  *
 * series of messages to be sent to SNW by snwclient containing the      *
 * minimum and maximum latency for all channels/waveservers for each     *
 * station.  These messages, which will appear on each station's data    *
 * page are of the form:                                                 *
 *     Minimum Secs of WaveServer Latency: x.xx                          *
 *     Maximum Secs of WaveServer Latency: x.xx                          *
 * The second file, WSlatency.txt, gives the menu data for every channel *
 * on the waveservers. This file may be sent to the SNW webserver        *
 * directory for processing by the perlscript WSlate.pl.                 *
 * WSlate.pl should live in the cgi-bin directory.                       *
 * The stations_info.iniin SNW's conf directory should have a line like  *
 * the following example for NN-PNT for each station:                    *
 * "Waveserver Latencies" = "http://ncsn.wr.usgs.gov/cgi-bin/WSlate.pl?NN-PNT" *
 *                                                                       *
 *                                                                       *
 *                                 Jim Luetgert  03/01/06                *
 *************************************************************************/
 
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <platform.h> /* includes system-dependent socket header files */
#include <chron3.h>
#include <earthworm.h>
#include <kom.h>
#include <ws_clientII.h>
#include <stdlib.h>
#include <ctype.h>

#ifndef INADDR_NONE
#define INADDR_NONE  0xffffffff  /* should be in <netinet/in.h> */
#endif

#define MAX_WAVESERVERS   80
#define MAX_ADRLEN        20
#define MAXTXT           150
#define MAXSITES        5000

/* Function prototypes
   *******************/
void LogWsErr( char [], int, int );
void Stuffdat( char * sta, char * net, double difft);
void config_me( char* configfile );

/* Globals to set from configuration file
   **************************************/
char     MyModName[MAX_MOD_STR];               /* Speak as this module */
int      numserv;                              /* Number of wave servers */
char     wsIp[MAX_WAVESERVERS][MAX_ADRLEN];
char     wsPort[MAX_WAVESERVERS][MAX_ADRLEN];  /* Available wave servers */
char     wsComment[MAX_WAVESERVERS][50];
int      ServerNotThere[MAX_WAVESERVERS];
int      Debug = 0;                     
char     loctarget[MAXTXT];                    /* Target directory */
char     snwdir[MAXTXT];                       /* Target directory */

/*****************************************************************************
 *  Define the structure for SNW message data.                               *
 *****************************************************************************/

typedef struct snwdata {   
    char    Site[6];           /* Site                                       */
    char    Comp[5];           /* Component                                  */
    char    Net[5];            /* Net                                        */
    char    Loc[5];            /* Loc                                        */
    double  minlat;            /* Minimum latency (Sec )                     */
    double  maxlat;            /* Maximum latency (Sec )                     */
} snwdata;

snwdata  sdat[MAXSITES];
int      ndat = 0;

/* Constants
   *********/
int   wsTimeout = 30000;                /* milliSeconds to wait for reply */


/* Time conversion factor for moving between
   Carl Johnson's seconds-since-1600 routines (chron3.c)
   and Solaris' and OS/2's seconds-since-1970 routines
   *****************************************************/
static char   Str1970[18] = "19700101000000.00";
static double Sec1970;


int main( int argc, char *argv[] )
{
	int          rc;                  /* Function return code */
	int          i, j;
	char         whoami[50], tname[200], line[80], line1[80], line2[80], sncl[20];
	WS_MENU_QUEUE_REC queue;
	time_t  current_time;
	double   difft;
    FILE    *out;
	
	queue.head = (WS_MENU) NULL;
	queue.tail = (WS_MENU) NULL;
    sprintf(whoami, " snwgetmenu: %s: ", "main");

  /* Calculate timebase conversion constant
  **************************************/
	Sec1970 = julsec17( Str1970 );

  /* Check command line arguments
  ****************************/
	if ( argc != 2 ) {
		printf( "Usage: snwgetmenu snwgetmenu.d\n" );
		return -1;
	}

  /* Since our routines call logit, we must initialize it, although we don't want to! */
	logit_init( "getmenu", (short) 0, 256, 0 );


  /* Zero the wave server arrays
  ***************************/
	for ( i = 0; i < MAX_WAVESERVERS; i++ ) {
		memset( wsIp[i], 0, MAX_ADRLEN );
		memset( wsPort[i], 0, MAX_ADRLEN );
		ServerNotThere[i] = 0;
	}

    /* Read the configuration file(s)
     ********************************/
    config_me( argv[1] );

  /* Initialize the socket system
  ****************************/
	SocketSysInit();

  /* Build the current wave server menus
  ***********************************/
	for(i=0; i<numserv; i++) {
		rc = wsAppendMenu( wsIp[i], wsPort[i], &queue, wsTimeout );
		if ( rc != WS_ERR_NONE ) {
			LogWsErr( "wsAppendMenu", rc, i );
			ServerNotThere[i] = 1;
		}
	}

	if (queue.head == NULL ) {
		logit("e", "SNWGETMENU: nothing in server\n");
		exit( 0 );
	} 

  /* Print contents of all tanks
  ***************************/
    sprintf(tname, "%sWSlatency.txt", loctarget);
	out = fopen(tname, "wb");
	if(out == 0L) {
		logit("e", "%s Unable to open SNW message File: %s\n", whoami, tname);   
	}  else {
		if(Debug) printf( "\n# Tank contents:\n" );
		for(i=0; i<numserv; i++) {
			WS_PSCNL scnlp;
			if(ServerNotThere[i]) continue;
			if(Debug) printf( "# SNWGETMENU: Channels from. %s:%s %s\n", wsIp[i], wsPort[i], wsComment[i] );
			
			rc = wsGetServerPSCNL( wsIp[i], wsPort[i], &scnlp, &queue );
			if ( rc == WS_ERR_EMPTY_MENU ) continue;
			time(&current_time);
			
			while ( 1 ) {
				difft = current_time - scnlp->tankEndtime;
				if(Debug) {
					if (strlen(scnlp->loc)) 
						printf( "  %5s.%-3s.%-2s.%s", scnlp->sta, scnlp->chan, scnlp->net, scnlp->loc);
					else
						printf( "  %5s.%-3s.%-2s.--", scnlp->sta, scnlp->chan, scnlp->net);
		
					date17( scnlp->tankStarttime+Sec1970, line );
					printf( "  Start: " );
					for ( j = 0; j < 8; j++ ) putchar( line[j] );
					putchar( '_' );
					for ( j = 8; j < 12; j++ ) putchar( line[j] );
					putchar( '_' );
					for ( j = 12; j < 17; j++ ) putchar( line[j] );
					
					date17( scnlp->tankEndtime+Sec1970, line );
					printf( "  End: " );
					for ( j = 0; j < 8; j++ ) putchar( line[j] );
					putchar( '_' );
					for ( j = 8; j < 12; j++ ) putchar( line[j] );
					putchar( '_' );
					for ( j = 12; j < 17; j++ ) putchar( line[j] );
					printf( "  %17.2f", difft);
					putchar( '\n' );
				}
				
				if (strlen(scnlp->loc)) 
					sprintf(sncl, "  %5s %-3s %-2s %s", scnlp->sta, scnlp->chan, scnlp->net, scnlp->loc);
				else
					sprintf(sncl, "  %5s %-3s %-2s --", scnlp->sta, scnlp->chan, scnlp->net);
				date17( scnlp->tankStarttime+Sec1970, line1 );
				date17( scnlp->tankEndtime+Sec1970, line2 );
				fprintf(out, "%s  Start: %.8s_%.4s_%.5s  End:  %.8s_%.4s_%.5s  %17.2f    %s:%s\n", 
				sncl, &line1[0], &line1[8], &line1[12], &line2[0], &line2[8], &line2[12], difft, wsIp[i], wsPort[i]);
	
				
				
				Stuffdat(scnlp->sta, scnlp->net, difft);
				
				if ( scnlp->next == NULL )
					break;
				else
					scnlp = scnlp->next;
			}
		}
		fclose(out);
	
	}

  /* Release the linked list created by wsAppendMenu
  **************************************************/
	wsKillMenu( &queue );

  /* Write out the message file for SNW
  **************************************************/
    sprintf(tname, "%ssnwmsg.txt", snwdir);
	out = fopen(tname, "wb");
	if(out == 0L) {
		logit("e", "%s Unable to open SNW message File: %s\n", whoami, tname);    
	} else {
		for(i=0;i<ndat;i++) {
			fprintf(out, "%s-%s:2:Minimum Secs of WaveServer Latency=%.2f;Maximum Secs of WaveServer Latency=%.2f\n", 
			sdat[i].Net, sdat[i].Site, sdat[i].minlat, sdat[i].maxlat);
		}
		fclose(out);
		
	}
	return 0;
}


void LogWsErr( char fun[], int rc, int i )
{
  switch ( rc )
    {
    case WS_ERR_INPUT:
      printf( "%s: Bad input parameters. %s:%s %s\n", fun, wsIp[i], wsPort[i], wsComment[i] );
      break;

    case WS_ERR_EMPTY_MENU:
      printf( "%s: Empty menu. %s:%s %s\n", fun, wsIp[i], wsPort[i], wsComment[i] );
      break;

    case WS_ERR_SERVER_NOT_IN_MENU:
      printf( "%s: Empty menu. %s:%s %s\n", fun, wsIp[i], wsPort[i], wsComment[i] );
      break;

    case WS_ERR_SCNL_NOT_IN_MENU:
      printf( "%s: SCN not in menu. %s:%s %s\n", fun, wsIp[i], wsPort[i], wsComment[i] );
      break;

    case WS_ERR_BUFFER_OVERFLOW:
      printf( "%s: Buffer overflow. %s:%s %s\n", fun, wsIp[i], wsPort[i], wsComment[i] );
      break;

    case WS_ERR_MEMORY:
      printf( "%s: Out of memory. %s:%s %s\n", fun, wsIp[i], wsPort[i], wsComment[i] );
      break;

    case WS_ERR_BROKEN_CONNECTION:
      printf( "%s: The connection broke. %s:%s %s\n", fun, wsIp[i], wsPort[i], wsComment[i] );
      break;

    case WS_ERR_SOCKET:
      printf( "%s: Could not get a connection. %s:%s %s\n", fun, wsIp[i], wsPort[i], wsComment[i] );
      break;

    case WS_ERR_NO_CONNECTION:
      printf( "%s: Could not get a connection. %s:%s %s\n", fun, wsIp[i], wsPort[i], wsComment[i] );
      break;

    default:
      printf( "%s: unknown ws_client error: %d. %s:%s %s\n", fun, rc, wsIp[i], wsPort[i], wsComment[i] );
      break;
    }

  return;
}

/******************************************************************
 * Keep track of the minimum and maximum latency for each station *
 * in an array. We will use this array later to send messages.    *
 ******************************************************************/
void Stuffdat( char * sta, char * net, double difft)
{
	int   i, first;
	
	first = 1;
	for(i=0;i<ndat;i++) {
		if(strcmp(sta, sdat[i].Site) == 0 && strcmp(net, sdat[i].Net) == 0) {
			first = 0;
			break;
		}
	}
	if(first) {
		strcpy(sdat[i].Site, sta);
		strcpy(sdat[i].Net, net);
		sdat[i].minlat = sdat[i].maxlat = difft;
		ndat++;
		if(ndat >= MAXSITES) {
			printf( "# SNWGETMENU: Stuffdat: Error, More than %d unique stations.\n", MAXSITES);
			ndat = MAXSITES;
		}
	} else {
		if(difft > sdat[i].maxlat) sdat[i].maxlat = difft;
		if(difft < sdat[i].minlat) sdat[i].minlat = difft;
	}
}

/****************************************************************************
 *      config_me() process command file using kom.c functions              *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void config_me( char* configfile )
{
    char    whoami[50], *com, *str, string[20];
    char    init[20];       /* init flags, one byte for each required command */
    int     ncommand;       /* # of required commands you expect              */
    int     nmiss;          /* number of required commands that were missed   */
    int     i, j, n, nPlots, nfiles, success, dclabels, secsPerGulp;
    double  val;
    FILE    *in;

    sprintf(whoami, " snwgetmenu: %s: ", "config_me");
        /* Set one init flag to zero for each required command
         *****************************************************/
    ncommand = 2;
    for(i=0; i<ncommand; i++ )  init[i] = 0;
    
    Debug = 0;
    numserv = 0;

        /* Open the main configuration file
         **********************************/
    nfiles = k_open( configfile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, configfile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

                /* Process anything else as a command
                 ************************************/

         /* wave server addresses and port numbers to get trace snippets from
          *******************************************************************/
/*0*/          
            else if( k_its("WaveServer") ) {
                if ( numserv >= MAX_WAVESERVERS ) {
                    fprintf( stderr, "%s Too many <WaveServer> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_WAVESERVERS );
                    return;
                }
                if( (long)(str=k_str()) != 0 )  strcpy(wsIp[numserv],str);
                if( (long)(str=k_str()) != 0 )  strcpy(wsPort[numserv],str);
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy(wsComment[numserv],str);
                numserv++;
                init[0]=1;
            }

        /* get LocalTarget directory path/name
        *****************************/
/*1*/
            else if( k_its("LocalTarget") ) {
                str = k_str();
                if( (int)strlen(str) >= MAXTXT) {
                    fprintf( stderr, "%s Fatal error. LocalTarget directory name %s greater than %d char.\n",
                        whoami, str, MAXTXT);
                    return;
                }
                if(str) strcpy( loctarget , str );
                init[1] = 1;
            }

        /* get SNWDir directory path/name
        *****************************/
/*1*/
            else if( k_its("SNWDir") ) {
                str = k_str();
                if( (int)strlen(str) >= MAXTXT) {
                    fprintf( stderr, "%s Fatal error. SNWDir directory name %s greater than %d char.\n",
                        whoami, str, MAXTXT);
                    return;
                }
                if(str) strcpy( snwdir , str );
                init[1] = 1;
            }

               /* optional commands */

 
        /* get the timeout interval in seconds
        **************************************/
            else if( k_its("wsTimeout") ) {
                wsTimeout = k_int()*1000;
            }

            else if( k_its("Debug") )          Debug = 1;  /* optional command */


                /* At this point we give up. Unknown thing.
                *******************************************/
            else {
                fprintf(stderr, "%s <%s> Unknown command in <%s>.\n",
                         whoami, com, configfile );
                continue;
            }

                /* See if there were any errors processing the command
                 *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "%s Bad <%s> command  in <%s>; exiting!\n",
                             whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

        /* After all files are closed, check init flags for missed commands
         ******************************************************************/
    nmiss = 0;
    for(i=0;i<ncommand;i++)  if( !init[i] ) nmiss++;
    if ( nmiss ) {
        fprintf( stderr, "%s ERROR, no ", whoami);
        if ( !init[0] )  fprintf( stderr, "<WaveServer> "   );
        if ( !init[1] )  fprintf( stderr, "<LocalTarget> "  );
        fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
        exit( -1 );
    }
}


