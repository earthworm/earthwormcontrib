# seg2toew.d
#
# Picks up SEG2 files from a specified directory, converts the contents
# of selected channels to tracebuf msg(s) and places into a message ring.
# Option to save (to subdir ./save) or delete the file afterwards.
# If it has trouble converting the file, it saves it to subdir ./trouble.
# Maintains its own local heartbeat and also monitors the peer's
# heartbeat via a file. Complains if the peer's expected heartbeat
# interval is exceeded; announces resumption of peer's heartbeat.

# Basic Module information
#-------------------------
MyModuleId        MOD_SEG2TOEW     # module id
RingName          WAVE_RING        # shared memory ring for output
HeartBeatInterval 30               # seconds between heartbeats to statmgr

LogFile           1                # 0 log to stderr/stdout only; 
                                   # 1 log to stderr/stdout and disk;
                                   # 2 log to disk module log only.

Debug             0                # 1=> debug output. 0=> no debug output

# Data file manipulation
#-----------------------
GetFromDir      /home/dietz/seg2   # for files in this directory
CheckPeriod     1                  # sleep this many seconds between looks
OpenTries       5                  # How many times we'll try to open a file
OpenWait        100                # Milliseconds to wait between open tries
SaveDataFiles   1                  # 0 = delete files after processing
                                   # non-zero = move files to save subdir
                                   #            after processing
LogOutgoingMsg  0                  # If non-zero, write contents of each
                                   #   outgoing msg to the daily log.
 

# Peer (remote partner) heartbeat manipulation
#---------------------------------------------
PeerHeartBeatFile geri1  none  0
                                   # PeerHeartBeatFile takes 3 arguments:
                                   # 1st: Name of remote system that is 
                                   #   sending the heartbeat files.
                                   # 2nd: Name of the heartbeat file. 
                                   # 3rd: maximum #seconds between heartbeat 
                                   #   files. If no new PeerHeartBeatFile arrives
                                   #   in this many seconds, an error message will
                                   #   be sent.  An "unerror message" will be
                                   #   sent after next heartbeat file arrives
                                   #   If 0, expect no heartbeat files.
                                   # Some remote systems may have multiple 
                                   # heartbeat files; list each one in a
                                   # seperate PeerHeartBeatFile command
                                   # (up to 5 allowed).

LogHeartBeatFile 0                 # If non-zero, write contents of each
                                   #   heartbeat file to the daily log.

PageOnLostPeer dietz		   # Optional command: Name of group to page 
                                   #   if PeerHeartBeatFile is late. This allows
                                   #   pages to be sent to groups other than that
                                   #   listed in statmgr.d. Up to 5 PageOnLostPeer
                                   #   commands can be used.
                                   # Must run telafeeder on same system to 
                                   #   actually get the pages sent.
                         
# Commands specific to seg2toew
#------------------------------

OutputMsgType TYPE_TRACEBUF2  # choose either TYPE_TRACEBUF or TYPE_TRACEBUF2

OutputTimeFactor 0.25  # Control how quickly packets are written to output.
                       #  0.0 = output pkts as quickly as possible
                       # <1.0 = faster than true rate.
                       #  1.0 = output pkts at "true" data rate. In other words,
                       #        it would take ~10sec to convert a 10sec data file.
                       # >1.0 = slower than true rate 
                       #        (allowed, but not recommended!)

MaxSamplePerMsg  1000  # each trace message will contain no more than this many
                       # samples. Some messages may contain fewer samples.

TimeJumpTolerance 600  # (in seconds) This term is used to catch files with
                       # dubious timestamps.  If a time gap is detected in an
                       # incoming data stream, the new file's timestamp is
                       # compared to the system clock.  If it is later than 
                       # the current system time by more than TimeJumpTolerance
                       # seconds, seg2toew assumes the file timestamp is bogus
                       # (it's in the future) and it ignores the entire file.
                       # NOTE: if you use this feature with small tolerances, 
                       # the PC's system clock must be kept pretty close to 
                       # network time!!!
                       # Set to -1 if you never want to compare file times
                       # to the system clock.
                       # Valid values are -1 or any number >= 0.0

SEG2DebugFlag      0   # Set SEG2DebugFlag to the sum of the items you
                       # want to have reported in the log file:
                       #   1  file descriptor #traces and trace pointers
                       #   2  file descriptor values                 
                       #   4  file descriptor strings (keywd=value)   
                       #   8  trace descriptor values                 
                       #  16  trace descriptor strings (keywd=value)  
                       #  32  trace data min,max data values.         
                       #  64  trace data raw data values.     

AddDelayToTimestamp  1    # Optional command (default=0)
                          # 0 = do not include trace descriptor DELAY
                          #     field in computing timestamp for trace data.
                          # non-zero = add DELAY value to ACQUISITION_TIME
                          #     when computing timestamp for trace data.  
                          # NOTE: use 0 for Oyo Geores SEG2 files
                          #       use 1 for GeoMetrics/Geode SEG2 files

ApplyDescalingFactor 0    # Optional command (default=0)
                          # NOTE: this option applies only to data stored
                          #       in SEG2 IEEE-float format!
                          # 0 = ignore trace descriptor DESCALING_FACTOR 
                          #     when converting trace data values to 
                          #     Earthworm format. 
                          # non-zero = multiply SEG2 IEEE-float data values
                          #     by DESCALING_FACTOR before converting 
                          #     to 4-byte integer for Earthworm format.

FloatScalingFactor 10000  # Multiply SEG2 IEEE-float data by this value
                          # to convert it to 4-byte integer data. 
                          # Optional command (default=10000)

#---------------------------------------------------------------------
# Set up list of traces you want to process from the SEG2 file.
# Use only one type of 'Send*' command to map from SEG2 trace
# descriptor to Earthworm SCNL:
#  'SendChannel'     choose traces by CHANNEL_NUMBER in SEG2 file.
#  'SendLineStaRcvr' choose traces by LINE_ID, RECEIVER_STATION_NUMBER
#                    and RECEIVER fields in the SEG2 trace descriptor.
#---------------------------------------------------------------------
# 'SendChannel' command has these arguments:
#
#   channel   SEG2 CHANNEL_NUMBER to process.
#   sta       station code (1-5 chars) to apply to the trace (no wildcards).
#   cmp       component code (1-3 chars) to apply to the trace (no wildcards).
#   nt        2-character network code to apply to the trace (no wildcards).
#   lc        2-character location code to apply to the trace (no wildcards). 
#             Valid characters are 0-9 and A-Z (upper case). 
#             Use "--" to denote a blank location code.
#   pinno     pin number to assign to this trace (0-32767)
#
#             chan   sta   cmp nt lc  pinno
#SendChannel     1   CH1   DP1 SF 01   2001
#SendChannel     2   CH2   DP1 SF 01   2002
#SendChannel     3   CH3   DP1 SF 01   2003
#SendChannel    45   CH45  DP1 SF 01   2045

#---------------------------------------------------------------------
# 'SendLineStaRcvr' command has these arguments:
#
#   line      LINE_ID (integer) to process.
#   sta#      RECEIVER_STATION_NUMBER (integer) to process.
#   rcvr      RECEIVER (1-127 chars) to process. 
#   sta       station code (1-5 chars) to apply to the trace (no wildcards).
#   cmp       component code (1-3 chars) to apply to the trace (no wildcards).
#   nt        2-character network code to apply to the trace (no wildcards).
#   lc        2-character location code to apply to the trace (no wildcards). 
#             Valid characters are 0-9 and A-Z (upper case). 
#             Use "--" to denote a blank location code.
#   pinno     pin number to assign to this trace (0-32767)
#
#                line sta# rcvr                           sta   cmp nt lc  pinno
 SendLineStaRcvr    3   1  HORIZONTAL_INLINE_GEOPHONE     L3S01 CH1 SF 01      1
 SendLineStaRcvr    3   1  HORIZONTAL_CROSSLINE_GEOPHONE  L3S01 CH2 SF 01      2
 SendLineStaRcvr    3   1  HORIZONTAL_OTHER_GEOPHONE      L3S01 CH3 SF 01      3
 SendLineStaRcvr    5   1  HORIZONTAL_INLINE_GEOPHONE     L5S01 CH1 SF 01      4
 SendLineStaRcvr    5   1  HORIZONTAL_CROSSLINE_GEOPHONE  L5S01 CH2 SF 01      5
 SendLineStaRcvr    5   1  HORIZONTAL_OTHER_GEOPHONE      L5S01 CH3 SF 01      6
 SendLineStaRcvr    5   2  HORIZONTAL_INLINE_GEOPHONE     L5S02 CH1 SF 01      7
 SendLineStaRcvr    5   2  HORIZONTAL_CROSSLINE_GEOPHONE  L5S02 CH2 SF 01      8
 SendLineStaRcvr    5   2  HORIZONTAL_OTHER_GEOPHONE      L5S02 CH3 SF 01      9

# THE END
