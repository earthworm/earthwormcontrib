/* 
 *   THIS FILE IS UNDER CVS. 
 *   DO NOT MODIFY UNLESS YOU HAVE CHECKED IT OUT.
 *
 *    $Id: procs.h 227 2006-08-16 18:41:14Z dietz $ 
 * 
 *    Revision history: 
 *     $Log$
 *     Revision 1.2  2006/08/16 18:41:14  dietz
 *     Modified free_key_val_pairs() to fix a memory leak.
 *
 *     Revision 1.1  2005/04/08 16:32:02  dietz
 *     Initial version. SEG2 file readin code based on UCB's seg2toms code.
 * 
 */ 


#ifndef procs_h
#define procs_h

#include "seg2_struct.h"

/************************************************************************/
/*  Routines for seg2toms.                                              */
/*                                                                      */
/*      Cedric de La Beaujardiere                                       */
/*      Seismological Laboratory                                        */
/*      University of California, Berkeley                              */
/*      cedric@seismo.berkeley.edu                                      */
/*                                                                      */
/************************************************************************/

/*
 * Copyright (c) 2004 The Regents of the University of California.
 * All Rights Reserved.
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for educational, research and non-profit purposes,
 * without fee, and without a written agreement is hereby granted,
 * provided that the above copyright notice, this paragraph and the
 * following three paragraphs appear in all copies.
 * 
 * Permission to incorporate this software into commercial products may
 * be obtained from the Office of Technology Licensing, 2150 Shattuck
 * Avenue, Suite 510, Berkeley, CA  94704.
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND
 * ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE
 * PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
 * CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */


/************************************************************************
*  read_seg2_file_desc:							*
*	Read SEG2 file descriptor block,				*
*	determining whether the file's data is stored in native or      *
*	swapped byte order.                                     	*
*	Fill in file descriptor block structure (in native byte order).	*
*	Fill in nTraces with the Number of traces in the SEG2 file.	*
*  return:								*
*	0 for success, 1 for failure.					*
************************************************************************/
char read_seg2_file_desc
  (FILE	         *input,
   SEG2FILEDESC  *file_desc,
   int           *nTraces);

/************************************************************************
*  read_seg2_trace_desc:                                                *
*       Read SEG2 trace descriptor blocks (e.g. data block headers) and *
*       fill in trace descriptor block structure (in native byte order).*
*         A pointer to the SEG-2's file descriptor is used to determine *
*       whether the data is stored in native or swapped byte order,     *
*       and acts accordingly to set the *read_chars function pointer    * 
*       to fread() or to fread_and_swap(), respectively.                *
*  return:                                                              *
*	0 for success, 1 for failure.					*
************************************************************************/
char read_seg2_trace_desc
  (FILE          *input,
   SEG2FILEDESC  *file_desc,
   SEG2TRACEDESC *trace_desc);

int read_seg2_trace_data
   (FILE	  *input,
    SEG2FILEDESC  *file_desc,
    SEG2TRACEDESC *trace_desc,
    int		 **pdata);

/* fread_and_swap reads characters and swaps their order before writing       *
 * them to buffer, in order to put data into native byte order.               */
size_t fread_and_swap(void *buffer, size_t size, size_t count, FILE *input);

/* readSEG2failed:                                  *
 *  if numRead == expected: returns 0,              *
 *  else: prints a message to stderr and returns 1. */
char readSEG2failed            
  (int   numRead,             /* number of bytes actually read */
   int   expected,            /* number of bytes that should have been read */
   char *fieldNameString,     /* string describing what may have not be read */
   char *skipString);         /* string indicating what will be skipped if read failed, *
                                * such as "File Descriptor" or "Trace". */


char parse_strings_of_file_desc (SEG2FILEDESC *file_desc);
char parse_strings_of_trace_desc(SEG2FILEDESC *file_desc, SEG2TRACEDESC *trace_desc);

char end_of_string(char *strings,
                   int   index,
                   char *strTerm,
                   char  strTermSize);

KEY_VAL_PAIR * get_key_val_pair
  (SEG2FILEDESC *file_desc,
   char         *strings,
   int           index,
   int          *chars_to_next_pair);

int categorize_keys
  (KEY_VAL_PAIR **keys_and_vals,
   int            num_keys,
   int           *key_categories,
   SEG2_KEYWORDS *keywords_list);


int  init_file_and_trace_desc_keywords ( SEG2_KEYWORDS **fdk, 
                                         SEG2_KEYWORDS **tdk );

void free_keywords      (void);
void free_key_val_pairs (KEY_VAL_PAIR  **pairs, int num_pairs);
void free_trace_desc    (SEG2TRACEDESC **trace_desc_block);
void free_file_desc     (SEG2FILEDESC  **file_desc_block);

void print_strings( SEG2FILEDESC  *file_desc, SEG2TRACEDESC *trace_desc );

void output_debug_fd_vals        (SEG2FILEDESC  *file_desc);
void output_debug_fd_traces      (SEG2FILEDESC  *file_desc);
void output_debug_fd_strs_whole  (SEG2FILEDESC  *file_desc);
void output_debug_fd_strs_parsed (SEG2FILEDESC  *file_desc);
void output_debug_td_vals        (SEG2TRACEDESC *trace_desc);
void output_debug_td_strs_whole  (SEG2TRACEDESC *trace_desc);
void output_debug_td_strs_parsed (SEG2TRACEDESC *trace_desc);

int override_file_key_val (SEG2FILEDESC *file_desc, KEY_VAL_PAIR **orfv, int orfc);
int override_trace_key_val (SEG2TRACEDESC *trace_desc, KEY_VAL_PAIR **orfv, int orfc);
int override_known_key_val (KEY_VAL_PAIR *ka[], int n, KEY_VAL_PAIR *kp);
int override_unknown_key_val (KEY_VAL_PAIR ***ap, int *n, KEY_VAL_PAIR *kp);
int key_val_store (char *str, KEY_VAL_PAIR ***p, int *n);
KEY_VAL_PAIR *key_val_define (char *str);

void logit( char *, char *, ... );          /* logit.c      sys-independent  */

#endif /* end of #ifndef procs_h */

