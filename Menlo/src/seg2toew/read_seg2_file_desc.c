/* 
 *   THIS FILE IS UNDER CVS. 
 *   DO NOT MODIFY UNLESS YOU HAVE CHECKED IT OUT.
 *
 *    $Id: read_seg2_file_desc.c 133 2005-06-10 20:21:45Z dietz $ 
 * 
 *    Revision history: 
 *     $Log$
 *     Revision 1.3  2005/06/10 20:21:45  dietz
 *     Added debug logging of min/max trace values and raw data values.
 *     Made SEG2DebugFlag configurable.
 *
 *     Revision 1.2  2005/06/03 17:39:53  dietz
 *     Added fseek to set file pointer to the start of the free-format file
 *     strings sub-block to account for the fact that the trace pointer
 *     sub-block may be longer than required by the number of channels.
 *
 *     Revision 1.1  2005/04/08 16:32:02  dietz
 *     Initial version. SEG2 file readin code based on UCB's seg2toms code.
 * 
 */ 


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include "seg2_struct.h"
#include "procs.h"

extern int SEG2DebugFlag;   /* debug flag using DBG_FLAGS bit set,    */
                            /* defined in seg2_struct.h               */

/************************************************************************
*  read_seg2_file_desc:							*
*	Read SEG2 file descriptor block, determining whether the	*
*	file's data is stored in native or swapped byte order. 		*
*	Fill in file descriptor block structure (in native byte order).	*
*  return:								*
*	Number of traces in the SEG2 file.				*
************************************************************************/
char read_seg2_file_desc
   (FILE	  *input,
    SEG2FILEDESC  *file_desc,
    int           *numTraces)
{
  size_t (*read_chars)(void *, size_t, size_t, FILE *); /* function pointer */
  int    numRead; /* Number of chars read from input, for error checking */
  int    i;       /* index to file_desc->trace_ptrs[] array */
  char   temp_chars_18[18];

  /***************************************************************
  * Read the SEG-2 File Descriptor's Block ID (First two bytes), *
  * determine if the format of the file is native or swapped,    *
  * and set the file character reading function accordingly.     *
  ***************************************************************/
  numRead = fread(&file_desc->block_ID, sizeof(short int), 1, input);
  if (numRead != 1) {
    logit( "e",
           "Error reading SEG-2 File Descriptor Block ID. # bytes read = %d, wanted 2; string read = %s.\n",
            numRead, file_desc->block_ID);
    return(1);
  }
  if      (file_desc->block_ID == NATIVE_BYTEORDER_ID)  { /* 0x3A55 */
    read_chars = fread; /* assign the function pointer */
    logit( "e","Input file is in native byte order\n");
  }
  else if (file_desc->block_ID == SWAPPED_BYTEORDER_ID) { /* 0x553A */
    /* fread_and_swap swaps the order of the bytes it reads. */
    read_chars = fread_and_swap; /* assign the function pointer */
    logit( "e","Input file is in swapped byte order\n");
  }
  else {
    logit( "e",
            "Error: invalid SEG-2 File Descriptor Block ID: 0x%X. Skipping SEG-2 file.\n",
            file_desc->block_ID);
    return(1);
  }

  /************************************************
  * Read the next 12 bytes of the file descriptor *
  ************************************************/
  numRead = read_chars(&file_desc->revisionNum,          sizeof(short int), 1, input);
  if (readSEG2failed(numRead, 1, "File Desc: Revision Number",   "SEG-2 File" )) { return(1); }

  numRead = read_chars(&file_desc->tracePtrSubBlockSize, sizeof(short int), 1, input);
  if (readSEG2failed(numRead, 1,
                     "File Desc: Trace Ptr Sub Block Size",      "SEG-2 File" )) { return(1); }

  numRead = read_chars(&file_desc->numOfTraces,          sizeof(short int), 1, input);
  if (readSEG2failed(numRead, 1, "File Desc: Number of Traces",  "SEG-2 File" )) { return(1); }

  /* Single bytes can be read directly with fread (for speed): *
   * byte order doesn't matter, obviously.                     */
  numRead = fread(&file_desc->strTermSize,  sizeof(char), 1, input);
  if (readSEG2failed(numRead, 1, "File Desc: String Term size",  "SEG-2 File" )) { return(1); }

  numRead = fread(&file_desc->strTerm1,     sizeof(char), 1, input);
  if (readSEG2failed(numRead, 1, "File Desc: String Term 1",     "SEG-2 File" )) { return(1); }

  numRead = fread(&file_desc->strTerm2,     sizeof(char), 1, input);
  if (readSEG2failed(numRead, 1, "File Desc: String Term 2",     "SEG-2 File" )) { return(1); }
    
  file_desc->strTerm[0] = file_desc->strTerm1;
  file_desc->strTerm[1] = (file_desc->strTermSize == 2) ? file_desc->strTerm2 : 0;
  file_desc->strTerm[2] = 0;

  numRead = fread(&file_desc->lineTermSize, sizeof(char), 1, input);
  if (readSEG2failed(numRead, 1, "File Desc: Line Term Size",    "SEG-2 File" )) { return(1); }

  numRead = fread(&file_desc->lineTerm1,    sizeof(char), 1, input);
  if (readSEG2failed(numRead, 1, "File Desc: Line Terminator 1", "SEG-2 File" )) { return(1); }

  numRead = fread(&file_desc->lineTerm2,    sizeof(char), 1, input);
  if (readSEG2failed(numRead, 1, "File Desc: Line Terminator 2", "SEG-2 File" )) { return(1); }

  /* Skip 18 reserved bytes (bytes 14 - 31) */
  numRead = fread(temp_chars_18, sizeof(char), 18, input);
  if (numRead != 18) {
    logit( "e",
            "Error: while reading SEG2 File Descriptor, set of new file position "
            "failed on input %s.\n", *input);
    return(1);
  }

  /**************************************************************
  * Allocate space for an array of trace pointers, and fill     *
  * the array by reading each pointer from the file descriptor. *
  **************************************************************/
  *numTraces = file_desc->numOfTraces;
  file_desc->trace_ptrs = (int *)malloc(sizeof(int) * (*numTraces));
  for (i = 0; i < *numTraces; i++) {
    numRead = read_chars(&(file_desc->trace_ptrs)[i], sizeof(int), 1, input);
    if (numRead != 1) {
      logit( "e",
              "Error: can't read SEG2's File Desc: Trace Pointer %d. Skipping SEG-2 File.\n",
              i);
      return(1);
    }
  }
 
  output_debug_fd_vals(file_desc);   /* output any debugging info requested */
  output_debug_fd_traces(file_desc); /* output any debugging info requested */

  /*****************************************************************************
  * Seek to the of start of the free format file strings,                      *
  * allocate space for an array of characters, and fill the array by reading   *
  * each character from the file descriptor.                                   *
  * The number of characters to read for the strings sub block is equal to     *
  *   the address of the first trace                                           *
  *   - (the size of the fixed part of the header [32 bytes]                   *
  *      + the size of the header's trace pointer sub block).                  *
  *****************************************************************************/
  if( fseek(input, (long)(32 + file_desc->tracePtrSubBlockSize), SEEK_SET) ) {
    logit( "e", "Error in fseek to file strings sub-block. Skipping SEG-2 File.\n" );
    return(1);
  }
  file_desc->charsIn_strings = file_desc->trace_ptrs[0] - (32 + file_desc->tracePtrSubBlockSize);
  file_desc->strings = (char *)malloc(sizeof(char) * file_desc->charsIn_strings);
  numRead = fread(file_desc->strings, sizeof(char), file_desc->charsIn_strings, input);
  if (readSEG2failed(numRead, file_desc->charsIn_strings,
                     "File Desc: strings", "SEG-2 File" )) { return(1); }
  if (parse_strings_of_file_desc(file_desc) != 0) {
    logit( "e", "Error parsing SEG-2 File Descriptor strings.  Skipping SEG-2 file.\n");
    return(1);
  }
  output_debug_fd_strs_parsed(file_desc); /* output any debugging info requested */

  return 0;
}


char parse_strings_of_file_desc(SEG2FILEDESC *file_desc)
{
  extern SEG2_KEYWORDS * file_desc_keywords; /* defined in seg2toms.c */

/*int  max_possible_num_of_kv_pairs = file_desc->charsIn_strings / (2 + file_desc->strTermSize);*/
/*KEY_VAL_PAIR *keys_and_vals[max_possible_num_of_kv_pairs]; */
  int  max_possible_num_of_kv_pairs;
  KEY_VAL_PAIR **keys_and_vals;
  int  key_val_index = 0;
  int  index = 0;
  int  chars_to_next_pair;
  char return_val = 0;
  int  unknown_keys_index = 0; /* used when unknown key/value pairs are finally stored in file_desc. */
  int  unknown_keys_count = 0;
  int *key_categories;         /* number of elements will be key_val_index, once it is determined */

  /* Make Windows happy */
  max_possible_num_of_kv_pairs = file_desc->charsIn_strings / (2 + file_desc->strTermSize);
  keys_and_vals = (KEY_VAL_PAIR **)malloc(sizeof(KEY_VAL_PAIR *)*max_possible_num_of_kv_pairs);
  if (keys_and_vals == NULL) {
    logit( "e", "parse_strings_of_file_desc ERROR: Can't allocate memory "
            "block for key_and_vals[%d].\n", max_possible_num_of_kv_pairs);
    return(1);
  }

  /* Initiallize all the pointers of the known_keys to NULL */
  for ( key_val_index = 0; key_val_index < NUM_OF_FD_KEYWORDS; key_val_index++) {
    file_desc->known_keys[key_val_index] = NULL;
  }
  key_val_index = 0;

  keys_and_vals[key_val_index] = get_key_val_pair(file_desc, file_desc->strings, index, &chars_to_next_pair);
  while (keys_and_vals[key_val_index] != NULL) {
    index += chars_to_next_pair;
    key_val_index++;
    keys_and_vals[key_val_index] = get_key_val_pair(file_desc, file_desc->strings, index, &chars_to_next_pair);
  }

  key_categories = (int *)malloc(sizeof(int) * key_val_index);
  if (key_categories == NULL) {
    logit( "e",
            "parse_strings_of_file_desc ERROR: Can't allocate memory block for int key_categories[%d].\n",
            key_val_index);
    return(1);
  }

  unknown_keys_count = categorize_keys(keys_and_vals, key_val_index, key_categories, file_desc_keywords);
  file_desc->unknown_keys_count = unknown_keys_count;

  if (unknown_keys_count > 0) {
    /* Allocate memory space for the unknown_keys array in file_desc */
    file_desc->unknown_keys = (KEY_VAL_PAIR **)malloc(sizeof(KEY_VAL_PAIR *) * unknown_keys_count);
    if (file_desc->unknown_keys == NULL) {
      logit( "e",
               "parse_strings_of_file_desc ERROR: Can't allocate memory block for KEY_VAL_PAIR unknown_keys[%d].\n",
               unknown_keys_count);
      return(1);
    }
    /* Clear the unknown_keys array */
    memset ((void *)file_desc->unknown_keys, 0, sizeof(KEY_VAL_PAIR *) * unknown_keys_count);
  }

  /* Store the KEY_VAL_PAIRs found in the strings section of the            *
   * file descriptor block according to whether or not each key matched the *
   * known keywords (those listed in the SEG2 Specification, or those known *
   * at the time of programming: more can be added in seg2_struct.h).       */
  for (index = 0; index < key_val_index; index++) {
    if (key_categories[index] != -1) {
      file_desc->known_keys[key_categories[index]] = keys_and_vals[index];
    }
    else {
      file_desc->unknown_keys[unknown_keys_index++] = keys_and_vals[index];
    }
  }

  free(keys_and_vals);
  free(key_categories);

  return return_val;
}


void output_debug_fd_vals(SEG2FILEDESC *file_desc)
{
  if (SEG2DebugFlag & DBG_FLAG_FD_VALS) {
    logit( "e",
            "The file descriptor fields are as follows:\n"
            "  block_ID             = 0x%X\n"
            "    (0x3A55: native byte order, 0x553A: swapped byte order.\n"
            "     All necessary swaps already done on rest of multi-byte data.)\n"
            "  revisionNum          = %d\n"
            "  tracePtrSubBlockSize = %d\n"
            "  numOfTraces          = %d\n"
            "  strTermSize          = %d\n"
            "  strTerm1             = %-2d (ASCII numerical representation)\n"
            "  strTerm2             = %-2d (ASCII numerical representation)\n"
            "  lineTermSize         = %d\n"
            "  lineTerm1            = %-2d (ASCII numerical representation)\n"
            "  lineTerm2            = %-2d (ASCII numerical representation)\n",
            file_desc->block_ID,
            file_desc->revisionNum,
            file_desc->tracePtrSubBlockSize,
            file_desc->numOfTraces,
            file_desc->strTermSize,
            file_desc->strTerm1,
            file_desc->strTerm2,
            file_desc->lineTermSize,
            file_desc->lineTerm1,
            file_desc->lineTerm2);
  }
}

void output_debug_fd_traces(SEG2FILEDESC *file_desc)
{
  int i;

  if (SEG2DebugFlag & DBG_FLAG_FD_TRACES) {
    logit( "e",
            "The file descriptor's %d trace pointers are as follows:\n",
            file_desc->numOfTraces);

    for (i = 0; i < file_desc->numOfTraces; i++) {
      logit( "e",
              "  trace_ptrs[%03d] = %10d\n",
              i, (file_desc->trace_ptrs)[i]);
    }
  }
}

void output_debug_fd_strs_parsed(SEG2FILEDESC *file_desc)
{
  int i;

  if (SEG2DebugFlag & DBG_FLAG_FD_STRS_PARSED) {
    logit( "e",
            "The file descriptor's parsed strings are as follows:\n");
    for (i = 0; i < NUM_OF_FD_KEYWORDS; i++) {
      if ((file_desc->known_keys)[i]) {
        logit( "e",
                "  %30s = %s\n",
                file_desc->known_keys[i]->key,
                file_desc->known_keys[i]->val);
      }
    }
    if (file_desc->unknown_keys_count > 0) {
      logit( "e",
              " In addition to the above, expected, file descriptor strings,\n"
              " the following keyword = value pairs were also found in the strings section:\n");
      for (i = 0; i < file_desc->unknown_keys_count; i++) {
        if ((file_desc->unknown_keys)[i]) {
          logit( "e",
                  "  %30s = %s\n",
                  file_desc->unknown_keys[i]->key,
                  file_desc->unknown_keys[i]->val);
        }
      }
    }
  }
}


