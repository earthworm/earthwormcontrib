/*
 *  THIS FILE IS UNDER CVS
 *  DO NOT MODIFY UNLESS YOU HAVE CHECKED IT OUT.
 *
 *    $Id: seg2toew.h 306 2007-03-27 18:23:37Z dietz $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.3  2007/03/27 18:23:37  dietz
 *     Added the option to choose SEG2 traces by LineID-StationNumber-Receiver
 *     in the trace descriptor (new 'SendLineStaRcvr' command). You can still
 *     choose traces by SEG2 channel ('SendChannel' command), but you cannot use
 *     both types of 'Send*' commands in the same configuration file.
 *
 *     Revision 1.2  2006/01/18 20:57:26  dietz
 *     Added OutputTimeFactor command to control how quickly data are output.
 *
 *     Revision 1.1  2005/04/08 16:32:03  dietz
 *     Initial version. SEG2 file readin code based on UCB's seg2toms code.
 *
 */
 
#ifndef _SEG2TOEW_H
#define _SEG2TOEW_H

#include <time.h>
#include <trace_buf.h>

#define STA_LEN  (TRACE2_STA_LEN)-1  /* max string-length of station code  */
#define CMP_LEN  (TRACE2_CHAN_LEN)-1 /* max string-length of channel code  */
#define NET_LEN  (TRACE2_NET_LEN)-1  /* max string-length of network code  */
#define LOC_LEN  (TRACE2_LOC_LEN)-1  /* max string-length of location code */
#define NOTSET   -9.0                /* value to denote unset parameters   */

#define MAX_VAL_LEN            127   /* max string-length for SEG2 values  */

#define ABS(X) (((X) >= 0) ? (X) : -(X))
 
/* Structure for tracking requested channels
 *******************************************/
typedef struct _SCNL_INFO {
   char             sta[STA_LEN+1];
   char             cmp[CMP_LEN+1];
   char             net[NET_LEN+1];
   char             loc[LOC_LEN+1];
   int              pinno;               /* EW pin to assign to this chan */
   int              channel;             /* channel number in SEG2 file   */
   int              lineid;              /* line-id in SEG2 trace desc    */
   int              stanum;              /* rcvr-station-number in tr desc*/
   char             rcvr[MAX_VAL_LEN+1]; /* receiver name from trace desc */
   int              lastrate;            /* sample rate of last data rcvd */
   double           texp;                /* expected time of next packet  */
   int             *carryover;           /* data not-yet shipped (<1sec)  */
/* NMX_DECOMP_DATA  carryover; */        /* data not-yet shipped (<1sec)  */
} SCNL_INFO;

typedef struct _SEG2_TIME {
   struct tm tacq;             /* from ACQUISITION_DATE dd/mm/yyyy        */
                               /*      ACQUISITION_TIME hh:mm:ss          */
   double    tacq_sec_frac;    /* from ACQUISITION_SECOND_FRACTION 0.ffff */
   double    starttime;        /* converted time in seconds since 1970    */
} SEG2_TIME;

#endif
