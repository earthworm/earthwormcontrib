/* 
 *   THIS FILE IS UNDER CVS. 
 *   DO NOT MODIFY UNLESS YOU HAVE CHECKED IT OUT.
 *
 *    $Id: read_seg2_trace_desc.c 133 2005-06-10 20:21:45Z dietz $ 
 * 
 *    Revision history: 
 *     $Log$
 *     Revision 1.2  2005/06/10 20:21:45  dietz
 *     Added debug logging of min/max trace values and raw data values.
 *     Made SEG2DebugFlag configurable.
 *
 *     Revision 1.1  2005/04/08 16:32:03  dietz
 *     Initial version. SEG2 file readin code based on UCB's seg2toms code.
 * 
 */ 


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include "seg2_struct.h"
#include "procs.h"

extern int SEG2DebugFlag;   /* debug flag using DBG_FLAGS bit set,    */
                            /* defined in seg2_struct.h               */


/************************************************************************
*  read_seg2_trace_desc:                                                *
*       Read SEG2 trace descriptor blocks (e.g. data block headers) and *
*       fill in trace descriptor block structure (in native order).     *
*         The file descriptor's block_ID is used to determine whether   *
*       the file's data is stored in the native or swapped byte order,  *
*       and acts accordingly to set the *read_chars function pointer to *
*       fread() or to fread_and_swap(), respectively.                   * 
*  return:                                                              *
*       Number of samples in the data block.                            *
************************************************************************/
char read_seg2_trace_desc
   (FILE	  *input,
    SEG2FILEDESC  *file_desc,
    SEG2TRACEDESC *trace_desc)
{
  int    numRead;    /* Number of chars read from input, for error checking */
  size_t (*read_chars)(void *, size_t, size_t, FILE *); /* function pointer */
  char   temp_chars_19[19];

  /* Look at the SEG-2 File Descriptor's Block ID to               *
   * determine if the byte order of the file is native or swapped, *
   * and set the file character reading function accordingly.      */
  if      (file_desc->block_ID == NATIVE_BYTEORDER_ID)  { /* 0x3A55 */
    read_chars = fread;                /* assign the function pointer */
  }
  else if (file_desc->block_ID == SWAPPED_BYTEORDER_ID) { /* 0x553A */
    /* fread_and_swap swaps the order of the bytes it reads. */
    read_chars = fread_and_swap;      /* assign the function pointer */
  }
  else {
    logit( "e",
            "Error: invalid SEG-2 File Descriptor Block ID: 0x%X. Skipping SEG-2 file.\n",
            file_desc->block_ID);
    return(1);
  }

  numRead = read_chars(&trace_desc->block_ID, sizeof(short int), 1, input);
  if (trace_desc->block_ID != 0x4422) {
    logit( "e",
            "Error: invalid SEG-2 Trace Descriptor Block ID: 0x%X. Skipping Trace.\n",
            trace_desc->block_ID);
    return(1);
  } 

  numRead = read_chars(&trace_desc->traceDescBlockSize, sizeof(short int), 1, input);
  if (readSEG2failed(numRead, 1, "Trace Descriptor Block Size",   "Trace" )) { return(1); }

  numRead = read_chars(&trace_desc->traceDataBlockSize, sizeof(int),       1, input);
  if (readSEG2failed(numRead, 1, "Trace Desc. Data Block Size",   "Trace" )) { return(1); }

  numRead = read_chars(&trace_desc->numOfSamples,       sizeof(int),       1, input);
  if (readSEG2failed(numRead, 1, "Trace Desc. Number of Samples", "Trace" )) { return(1); }

  numRead = fread(     &trace_desc->dataFormatCode,     sizeof(char),      1, input);
  if (readSEG2failed(numRead, 1, "Trace Data Format Code",        "Trace" )) { return(1); }

  /* Skip 19 reserved bytes (bytes 13 - 31) */
  numRead = fread(temp_chars_19, sizeof(char), 19, input);
  if (numRead != 19) {
    logit( "e",
            "Error: while reading SEG2 File Descriptor, set of new file position "
            "failed on input %s.\n", *input);
    return(1);
  }

  output_debug_td_vals(trace_desc); /* output any debugging info requested */

  if ( trace_desc->traceDescBlockSize > 32) {
    trace_desc->charsIn_strings = trace_desc->traceDescBlockSize - 32;
    trace_desc->strings = (char *)malloc(sizeof(char) * trace_desc->charsIn_strings);
    if (trace_desc->strings == NULL) {
      logit( "e",
              "read_seg2_trace_desc on input %s: Can't allocate memory block for strings\n",
              *input);
      return(1);
    }
    numRead = fread(trace_desc->strings, sizeof(char), trace_desc->charsIn_strings, input);
    if (readSEG2failed(numRead, trace_desc->charsIn_strings,
                       "Trace Desc. strings", "Trace" )) { return(1); }
    parse_strings_of_trace_desc(file_desc, trace_desc);
  }
  else {
    trace_desc->charsIn_strings = 0;
    trace_desc->strings = NULL;
  }

  output_debug_td_strs_parsed(trace_desc); /* output any debugging info requested */

  return (0);
}

char parse_strings_of_trace_desc(SEG2FILEDESC  *file_desc,
                                 SEG2TRACEDESC *trace_desc)
{
  extern SEG2_KEYWORDS * trace_desc_keywords; /* defined in seg2toew.c */
/*int  max_possible_num_of_kv_pairs = trace_desc->charsIn_strings / (2 + file_desc->strTermSize);*/
/*KEY_VAL_PAIR *keys_and_vals[max_possible_num_of_kv_pairs]; */
  int  max_possible_num_of_kv_pairs;
  KEY_VAL_PAIR **keys_and_vals;
  int  key_val_index = 0;
  int  index = 0;
  int  chars_to_next_pair;
  char return_val = 0;
  int  unknown_keys_index = 0; /* used when unknown key/value pairs are finally stored in trace_desc. */
  int  unknown_keys_count = 0;
  int *key_categories;         /* Number of elements will be key_val_index once determined */

  /* Make Windows happy */
  max_possible_num_of_kv_pairs = trace_desc->charsIn_strings / (2 + file_desc->strTermSize);
  keys_and_vals = (KEY_VAL_PAIR **)malloc(sizeof(KEY_VAL_PAIR *)*max_possible_num_of_kv_pairs);
  if (keys_and_vals == NULL) {
    logit( "e", "parse_strings_of_trace_desc ERROR: Can't allocate memory "
            "block for key_and_vals[%d].\n", max_possible_num_of_kv_pairs);
    return(1);
  }

  /* Initiallize all the pointers of the known_keys to NULL */
  for ( key_val_index = 0; key_val_index < NUM_OF_TD_KEYWORDS; key_val_index++) {
    trace_desc->known_keys[key_val_index] = NULL;
  }

  key_val_index = 0;
  keys_and_vals[key_val_index] = get_key_val_pair(file_desc, trace_desc->strings, index, &chars_to_next_pair);
  while (keys_and_vals[key_val_index] != NULL) {
    index += chars_to_next_pair;
    key_val_index++;
    keys_and_vals[key_val_index] = get_key_val_pair(file_desc, trace_desc->strings, index, &chars_to_next_pair);
  }

  key_categories = (int *)malloc(sizeof(int) * key_val_index);
  if (key_categories == NULL) {
    logit( "e",
            "parse_strings_of_trace_desc ERROR: Can't allocate memory block for int key_categories[%d].\n",
            key_val_index);
    return(1);
  }

  unknown_keys_count = categorize_keys(keys_and_vals, key_val_index, key_categories, trace_desc_keywords);
  trace_desc->unknown_keys_count = unknown_keys_count;

  if (unknown_keys_count > 0) {
    /* Allocate memory space for the unknown_keys array in trace_desc */
    trace_desc->unknown_keys = (KEY_VAL_PAIR **)malloc(sizeof(KEY_VAL_PAIR *) * unknown_keys_count);
    if (trace_desc->unknown_keys == NULL) {
      logit( "e",
              "parse_strings_of_trace_desc ERROR: Can't allocate memory block for KEY_VAL_PAIR unknown_keys[%d].\n",
              unknown_keys_count);
      return(1);
    }
    /* Clear the unknown_keys array */
    memset ((void *)trace_desc->unknown_keys, 0, sizeof(KEY_VAL_PAIR *) * unknown_keys_count);
  }

  /* Store the KEY_VAL_PAIRs found in the strings section of the            *
   * trace descriptor block according to whether or not each key matched the *
   * known keywords (those listed in the SEG2 Specification, or those known *
   * at the time of programming: more can be added in seg2_struct.h).       */
  for (index = 0; index < key_val_index; index++) {
    if (key_categories[index] != -1) {
      trace_desc->known_keys[key_categories[index]] = keys_and_vals[index];
    }
    else {
      trace_desc->unknown_keys[unknown_keys_index++] = keys_and_vals[index];
    }
  }

  free(keys_and_vals);
  free(key_categories);

  return return_val;
}

void output_debug_td_vals(SEG2TRACEDESC *trace_desc)
{
  if (SEG2DebugFlag & DBG_FLAG_TD_VALS) {
    logit( "e",
            "The trace descriptor fields are as follows:\n"
            "  block_ID           = 0x%X\n"
            "  traceDescBlockSize = %d\n"
            "  traceDataBlockSize = %d\n"
            "  numOfSamples       = %d\n"
            "  dataFormatCode     = %d\n"
            "  charsIn_strings    = %d\n",
            trace_desc->block_ID,
            trace_desc->traceDescBlockSize,
            trace_desc->traceDataBlockSize,
            trace_desc->numOfSamples,
            trace_desc->dataFormatCode,
            trace_desc->charsIn_strings);
  }
}

void output_debug_td_strs_parsed(SEG2TRACEDESC *trace_desc)
{
  int i;

  if (SEG2DebugFlag & DBG_FLAG_TD_STRS_PARSED) {
    logit( "e",
            "The trace descriptor's parsed strings are as follows:\n");
    for (i = 0; i < NUM_OF_TD_KEYWORDS; i++) {
      if ((trace_desc->known_keys)[i]) {
        logit( "e",
                "  %30s = %s\n",
                trace_desc->known_keys[i]->key,
                trace_desc->known_keys[i]->val);
      }
    }
    if (trace_desc->unknown_keys_count > 0) {
      logit( "e",
              " In addition to the above, expected, trace descriptor strings,\n"
              " the following keyword = value pairs were also found in the strings section:\n");
      for (i = 0; i < trace_desc->unknown_keys_count; i++) {
        if ((trace_desc->unknown_keys)[i]) {
          logit( "e",
                  "  %30s = %s\n",
                  trace_desc->unknown_keys[i]->key,
                  trace_desc->unknown_keys[i]->val);
        }
      }
    }
  }
}


