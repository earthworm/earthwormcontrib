/* 
 *   THIS FILE IS UNDER CVS. 
 *   DO NOT MODIFY UNLESS YOU HAVE CHECKED IT OUT.
 *
 *    $Id: readSEG2failed.c 126 2005-04-08 16:32:03Z dietz $ 
 * 
 *    Revision history: 
 *     $Log$
 *     Revision 1.1  2005/04/08 16:32:02  dietz
 *     Initial version. SEG2 file readin code based on UCB's seg2toms code.
 * 
 */ 


#include <stdio.h>
#include "procs.h"

/***************************************************
* Function:   readSEG2failed:                      *
*  if numRead == expected: returns 0,              *
*  else: prints a message to stderr and returns 1. *
*                                                  *
* written by: Cedric de La Beaujardiere            *
* date:       July 26, 2004                        *
***************************************************/
char readSEG2failed ( int numRead, int expected, char * fieldNameString, char * skipString ) {
  char returnval = 0;
  if (numRead != expected) {
    logit( "e",
            "Error: can't read SEG2's %s. Skipping %s.\n",
             fieldNameString, skipString);
    returnval = 1;
  }
  return returnval;
}


