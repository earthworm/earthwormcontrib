/*
 *   THIS FILE IS UNDER CVS. 
 *   DO NOT MODIFY UNLESS YOU HAVE CHECKED IT OUT.
 *
 *    $Id: fread_and_swap.c 126 2005-04-08 16:32:03Z dietz $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.1  2005/04/08 16:32:02  dietz
 *     Initial version. SEG2 file readin code based on UCB's seg2toms code.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include "procs.h"

#define DEBUGGING_FREAD 0

/* This function reads size * count characters into buffer,        *
 * then swaps the byte order for each object read.                 */
size_t fread_and_swap(void *buffer, size_t size, size_t count, FILE *input)
{
  int  max_to_read_at_once = 1000;
  char temp         = 0;
  int  objects_read = 0;
  int  i            = 0;
  int  first        = 0;
  int  last         = 0;
  int  numToRead    = 0;
  int  numRead      = 0;

  objects_read = fread( buffer, size, count, input );

#if 0
  /*if (count == 1) {*/
  if (count <= max_to_read_at_once) {
    objects_read = fread( buffer, size, count, input );
  }
  else {
    switch (size) {
    case 1: /* Cast buffer to char * */
      while (i < count) {
        /* Read up to max_to_read_at_once characters at a time */
        numToRead = ((count - i) > max_to_read_at_once) ?
                    max_to_read_at_once :
                    count - i;
        numRead = fread( (void *)( &(((char *)buffer)[i]) ), size, numToRead, input );
        if (numRead != numToRead) {
          logit( "e",
                  "Error w/ SEG2 File %s: tried to read %d objs of %d bytes, but only read %d.\n",
                  *input, numToRead, size, numRead);
        }
#ifdef DEBUGGING_FREAD
        logit( "e", "1 byte buffer[%d] = 0x%X\n", i, ((char *)buffer)[i]);
#endif
        objects_read += numRead;
        i            += numRead;
      }
      break;
    case 2: /* Cast buffer to short int * */
      while (i < count) {
        /* Read up to max_to_read_at_once characters at a time */
        numToRead = ((count - i) > max_to_read_at_once) ?
                    max_to_read_at_once :
                    count - i;
        numRead = fread( (void *)( &(((short int *)buffer)[i]) ), size, numToRead, input );
        if (numRead != numToRead) {
          logit( "e",
                  "Error w/ SEG2 File %s: tried to read %d objs of %d bytes, but only read %d.\n",
                  *input, numToRead, size, numRead);
        }
#ifdef DEBUGGING_FREAD
        logit( "e", "2 byte buffer[%d] = 0x%X\n", i, ((short int *)buffer)[i]);
#endif
        objects_read += numRead;
        i            += numRead;
      }
      break;
    case 4: /* Cast buffer to int * */
      while (i < count) {
        /* Read up to max_to_read_at_once characters at a time */
        numToRead = ((count - i) > max_to_read_at_once) ?
                    max_to_read_at_once :
                    count - i;
        numRead = fread( (void *)( &(((int *)buffer)[i]) ), size, numToRead, input );
        if (numRead != numToRead) {
          logit( "e",
                  "Error w/ SEG2 File %s: tried to read %d objs of %d bytes, but only read %d.\n",
                  *input, numToRead, size, numRead);
        }
#ifdef DEBUGGING_FREAD
        logit( "e", "4 byte buffer[%d] = 0x%X\n", i, ((int *)buffer)[i]);
#endif
        objects_read += numRead;
        i            += numRead;
      }
      break;
    default: /* irregular size: just read the whole thing, as if count were less than max */
      objects_read = fread( buffer, size, count, input );
      break;
    }
  }
#endif

  /* Reverse the byte order of each element of the read data */
  if (size > 1) {
    for (i = 0; i < objects_read; i++) {
      for ( first = i * size, last = first + size - 1;
            first < last;
            first++, last-- ) {
        temp = ((char *)buffer)[first];
        ((char *)buffer)[first] = ((char *)buffer)[last];
        ((char *)buffer)[last]  = temp;
      }
    }
  }

  return(objects_read); /* should be less than or equal to count */
}

#if 0
/* This function reads characters and swaps their order before writing them   *
 * to buffer.                                                                 *
size_t fread_and_swap_old(void *buffer, size_t size, size_t count, FILE *input)
{
  /*int  char_count = count * sizeof(size) / sizeof(char);*/
  int  char_count = count * size / sizeof(char);
  char swapped_buffer[char_count];
  int  bytes_read;
  int  le_buff_last_char_index;
  int  i;

  bytes_read = fread( (void *)swapped_buffer, sizeof(char), char_count, input );
  le_buff_last_char_index = bytes_read - 1;

  /* Reverse the byte order of the read data */
  for (i = 0; i < bytes_read; i++) {
    ((char *)buffer)[i] = swapped_buffer[le_buff_last_char_index - i];
  }

  return(bytes_read * sizeof(char) / size); /* reverse of char_count calculation, should equal count */
}
#endif
