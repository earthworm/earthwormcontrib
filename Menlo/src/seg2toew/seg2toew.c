/* 
 *   THIS FILE IS UNDER CVS. 
 *   DO NOT MODIFY UNLESS YOU HAVE CHECKED IT OUT.
 *
 *    $Id: seg2toew.c 678 2011-09-01 22:52:40Z dietz $ 
 * 
 *    Revision history: 
 *     $Log$
 *     Revision 1.7  2007/03/27 18:23:37  dietz
 *     Added the option to choose SEG2 traces by LineID-StationNumber-Receiver
 *     in the trace descriptor (new 'SendLineStaRcvr' command). You can still
 *     choose traces by SEG2 channel ('SendChannel' command), but you cannot use
 *     both types of 'Send*' commands in the same configuration file.
 *
 *     Revision 1.6  2006/08/16 18:41:14  dietz
 *     Modified free_key_val_pairs() to fix a memory leak.
 *
 *     Revision 1.5  2006/01/18 20:57:26  dietz
 *     Added OutputTimeFactor command to control how quickly data are output.
 *
 *     Revision 1.4  2005/06/10 20:21:45  dietz
 *     Added debug logging of min/max trace values and raw data values.
 *     Made SEG2DebugFlag configurable.
 *
 *     Revision 1.3  2005/06/08 20:47:13  dietz
 *     Added optional command "FloatScalingFactor" to override the default
 *     scaling factor for converting IEEE float data into 4-byte integers.
 *
 *     Revision 1.2  2005/06/03 17:37:54  dietz
 *     Modified getSEG2acqtime() to be able to read date strings with either
 *     numeric-months (21/12/2004) or alpha-months (21/Dec/2004). Both formats
 *     are allowed by the SEG2 definition.
 *
 *     Revision 1.1  2005/04/08 16:32:03  dietz
 *     Initial version. SEG2 file readin code based on UCB's seg2toms code.
 * 
 */ 


/************************************************************************ 
 *  seg2toew.c 	 convert a SEG2 file to Earthworm TYPE_TRACEBUF2.       * 
 *   This file contains file2ewfilter functions which are called        *
 *   from EW_VERSION/src/data_exchange/file2ew/file2ew.c                *
 *									*
 *   The code to read the SEG2 files into structures is heavily based   * 
 *   on UCB's seg2toms code (see Copyright info below).                 * 
 ************************************************************************/

/************************************************************************/
/*  seg2toms -	convert a SEG2 file to MiniSEED.			*/
/*									*/
/*	Cedric de La Beaujardiere 					*/
/*	Seismological Laboratory					*/
/*	University of California, Berkeley				*/
/*									*/
/*	Original segy2ms.c written by:    				*/
/*	Douglas Neuhauser						*/
/*	doug@seismo.berkeley.edu					*/
/*									*/
/************************************************************************/

/*
 * Copyright (c) 1996-2004 The Regents of the University of California.
 * All Rights Reserved.
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for educational, research and non-profit purposes,
 * without fee, and without a written agreement is hereby granted,
 * provided that the above copyright notice, this paragraph and the
 * following three paragraphs appear in all copies.
 * 
 * Permission to incorporate this software into commercial products may
 * be obtained from the Office of Technology Licensing, 2150 Shattuck
 * Avenue, Suite 510, Berkeley, CA  94704.
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND
 * ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE
 * PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
 * CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>

#include <earthworm.h>
#include <kom.h>
#include <time_ew.h>
#include <trace_buf.h>
#include <transport.h>

#include "seg2_struct.h" 
#include "procs.h"
#include "seg2toew.h"

/* Variables declared in file2ew.c
 *********************************/
extern char   ProgName[];    /* program name for logging purposes          */
extern int    Debug;         /* Debug logging flag (optionally configured) */

/* Variables declared in read_seg2_trace_data.c
 **********************************************/
extern double FloatScalingFactor;    /* default value can be changed in config */
extern int    ApplyDescalingFactor;  /* default value can be changed in config */

/* Functions from file2ew.c used here
 ************************************/
int  file2ew_ship( unsigned char type, unsigned char iid,
                   char *msg, size_t msglen );
void file2ew_status( unsigned char, short, char * );
int  file2ewfilter_init( void );

/* Functions used in this source file
 ************************************/
int IsValidLocChar( char );
int seg2toew_shipdata( SCNL_INFO     *scnl,       
                       SEG2_TIME     *tseg2, 
                       SEG2TRACEDESC *trace_desc, 
                       int           *data );
int getSEG2acqtime( SEG2FILEDESC *file_desc, SEG2_TIME *tseg2 );
SCNL_INFO *FindChannel( SCNL_INFO *list, int nlist, int channel );
SCNL_INFO *FindLineStaRcvr( SCNL_INFO *list, int nlist, 
                            int lineid, int stanum, char *rcvr );

int compare_SCNL_INFO_chan( const void *s1, const void *s2 );
int compare_SCNL_INFO_lsr( const void *s1, const void *s2 );

/* Parameters read or derived from configuration file
 ****************************************************/
int           MaxSamplePerMsg   = 0;      /* max# samples to pack in a tracebuf msg */
int           MaxSendSCNL       = 0;      /* max# SCNLs in SendSCNL                 */
int           nSendSCNL         = 0;      /* # of scnl's found in config file       */
SCNL_INFO    *SendSCNL          = NULL;   /* SCNLs requested in config file         */
int           nSendChannel      = 0;      /* number of SendChannel commands         */
int           nSendLineStaRcvr  = 0;      /* number of SendLineStaRcvr commands     */
unsigned char OutputMsgType     = 0;
double        OutputTimeFactor  = NOTSET; /* used in spacing packets in time while  */
                                          /* outputing them to the ring.            */
                                          /* 0.0 = output pkts as fast as possible  */
                                          /* 1.0 = output pkts at true data rate    */
                                          /*    (take ~10s to convert a 10s file)   */
double        TimeJumpTolerance = NOTSET; /* used in comparing packet time to       */
                                          /* system time when looking for bogus pkt */
                                          /* timetamps. -1 means don't do check,    */
int           SEG2DebugFlag     = 0;      /* debug flag using DBG_FLAGS bit set,    */
                                          /* defined in seg2_struct.h               */
int           AddDelayToTimestamp = 0;    /* 0= ignore DELAY field in computing     */
                                          /*    timestamp for trace data            */
                                          /* non-zero= add DELAY value in computing */
                                          /*    timestamp for trace data            */
                
/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static unsigned char TypeTraceBuf;
static unsigned char TypeTraceBuf2;

/* Other Globals
 ***************/
SEG2_KEYWORDS *file_desc_keywords  = NULL;
SEG2_KEYWORDS *trace_desc_keywords = NULL;
DBG_FLAGS     dbgflag              = 0;
int           seg2toewInit         = 0;      /* initialization flag       */
int           SCNLchansorted       = 0;      /* channel-sorted init flag  */
int           SCNLlsrsorted        = 0;      /* line-sta-rcvr init flag   */
double        ShipRate             = NOTSET; /* sample/sec to write to EW */

/* I hate to use ifdefs, but Windows is not Posix
 ************************************************/
#ifdef _WINNT
#define strcasecmp _stricmp
#endif

/******************************************************************************
 * file2ewfilter_com() processes config-file commands to set up the           *
 *                     SEG2toew-specific parameters                           *
 * Returns  1 if the command was recognized & processed                       *
 *          0 if the command was not recognized                               *
 * Note: this function may exit the process if it finds serious errors in     *
 *       any commands                                                         *
 ******************************************************************************/
int file2ewfilter_com( void )
{
   char  *str;
        
/* Add a channel to process
 **************************/
   if(k_its("SendChannel") ) {
      int badarg = 0;
      char *argname[] = {"","channel","sta","cmp","net","loc","pinno"};
      if( nSendSCNL >= MaxSendSCNL )
      {
         size_t size;
         MaxSendSCNL += 10;
         size     = MaxSendSCNL * sizeof( SCNL_INFO );
         SendSCNL  = (SCNL_INFO *) realloc( SendSCNL, size );
         if( SendSCNL == NULL )
         {
            logit( "e", "%s: Error allocating %d bytes"
                   " for requested channel list; exiting!\n", ProgName, size );
            exit( -1 );
         }
      }
      memset( &SendSCNL[nSendSCNL], 0, sizeof(SCNL_INFO) );

      SendSCNL[nSendSCNL].channel = k_int();
      if( SendSCNL[nSendSCNL].channel <= 0 || 
          SendSCNL[nSendSCNL].channel > 32767 ) {
         badarg = 1; goto EndSendChannel;
      }

      str=k_str();
      if( !str || strlen(str)>(size_t)STA_LEN ) {
         badarg = 2; goto EndSendChannel;
      }
      strcpy(SendSCNL[nSendSCNL].sta,str);

      str=k_str();
      if( !str || strlen(str)>(size_t)CMP_LEN) {
         badarg = 3; goto EndSendChannel;
      }
      strcpy(SendSCNL[nSendSCNL].cmp,str);

      str=k_str();
      if( !str || strlen(str)>(size_t)NET_LEN) {
         badarg = 4; goto EndSendChannel;
      }
      strcpy(SendSCNL[nSendSCNL].net,str);

      str=k_str();
      if( !str || strlen(str)!=(size_t)LOC_LEN) {
         badarg = 5; goto EndSendChannel;
      }
      if( !IsValidLocChar(str[0]) ||
          !IsValidLocChar(str[1])    ) {
         logit("e","Valid location code characters are 0-9,A-Z,'-'\n");
         badarg = 5; goto EndSendChannel;
      }
      strcpy(SendSCNL[nSendSCNL].loc,str);
      
      SendSCNL[nSendSCNL].pinno = k_int();
      if( SendSCNL[nSendSCNL].pinno <= 0 || SendSCNL[nSendSCNL].pinno > 32767 ) {
         badarg = 6; goto EndSendChannel;
      }

   EndSendChannel:
      if( badarg ) {
         logit( "e", "%s: Argument %d (%s) bad in <SendChannel> "
                "command (too long, missing, or invalid value):\n"
                "   \"%s\"\n", ProgName, badarg, argname[badarg], k_com() );
         logit( "e", "%s: exiting!\n", ProgName );
         free( SendSCNL );
         exit( -1 );
      }
      nSendSCNL++;
      nSendChannel++;
      return( 1 );
   } 

/* Add a Line-Station-Receiver to process
 ****************************************/
   if(k_its("SendLineStaRcvr") ) {
      int badarg = 0;
      char *argname[] = {"","lineid","stanum","rcvr","sta","cmp","net","loc","pinno"};
      if( nSendSCNL >= MaxSendSCNL )
      {
         size_t size;
         MaxSendSCNL += 10;
         size     = MaxSendSCNL * sizeof( SCNL_INFO );
         SendSCNL  = (SCNL_INFO *) realloc( SendSCNL, size );
         if( SendSCNL == NULL )
         {
            logit( "e", "%s: Error allocating %d bytes"
                   " for requested channel list; exiting!\n", ProgName, size );
            exit( -1 );
         }
      }
      memset( &SendSCNL[nSendSCNL], 0, sizeof(SCNL_INFO) );

      SendSCNL[nSendSCNL].lineid = k_int();
      if( SendSCNL[nSendSCNL].lineid <= 0 || 
          SendSCNL[nSendSCNL].lineid > 32767 ) {
         badarg = 1; goto EndSendLineStaRcvr;
      }

      SendSCNL[nSendSCNL].stanum = k_int();
      if( SendSCNL[nSendSCNL].stanum <= 0 || 
          SendSCNL[nSendSCNL].stanum > 32767 ) {
         badarg = 2; goto EndSendLineStaRcvr;
      }

      str=k_str();
      if( !str || strlen(str)>(size_t)MAX_VAL_LEN ) {
         badarg = 3; goto EndSendLineStaRcvr;
      }
      strcpy(SendSCNL[nSendSCNL].rcvr,str);

      str=k_str();
      if( !str || strlen(str)>(size_t)STA_LEN) {
         badarg = 4; goto EndSendLineStaRcvr;
      }
      strcpy(SendSCNL[nSendSCNL].sta,str);

      str=k_str();
      if( !str || strlen(str)>(size_t)CMP_LEN) {
         badarg = 5; goto EndSendLineStaRcvr;
      }
      strcpy(SendSCNL[nSendSCNL].cmp,str);

      str=k_str();
      if( !str || strlen(str)>(size_t)NET_LEN) {
         badarg = 6; goto EndSendLineStaRcvr;
      }
      strcpy(SendSCNL[nSendSCNL].net,str);

      str=k_str();
      if( !str || strlen(str)!=(size_t)LOC_LEN) {
         badarg = 7; goto EndSendLineStaRcvr;
      }
      if( !IsValidLocChar(str[0]) ||
          !IsValidLocChar(str[1])    ) {
         logit("e","Valid location code characters are 0-9,A-Z,'-'\n");
         badarg = 7; goto EndSendLineStaRcvr;
      }
      strcpy(SendSCNL[nSendSCNL].loc,str);
      
      SendSCNL[nSendSCNL].pinno = k_int();
      if( SendSCNL[nSendSCNL].pinno <= 0 || 
          SendSCNL[nSendSCNL].pinno > 32767 ) {
         badarg = 8; goto EndSendLineStaRcvr;
      }

   EndSendLineStaRcvr:
      if( badarg ) {
         logit( "e", "%s: Argument %d (%s) bad in <SendLineStaRcvr> "
                "command (too long, missing, or invalid value):\n"
                "   \"%s\"\n", ProgName, badarg, argname[badarg], k_com() );
         logit( "e", "%s: exiting!\n", ProgName );
         free( SendSCNL );
         exit( -1 );
      }
      nSendSCNL++;
      nSendLineStaRcvr++;
      return( 1 );
   } 

/* Set the maximum # samples per tracebuf message
 ************************************************/
   if(k_its("MaxSamplePerMsg") ) {
      int max;
      MaxSamplePerMsg = k_int();
      max = (MAX_TRACEBUF_SIZ-sizeof(TRACE2_HEADER))/sizeof(int);
      if( MaxSamplePerMsg > max  ||  MaxSamplePerMsg < 0 )
      {
         logit( "e", "%s: <MaxSamplePerMsg %d> outside valid range "
                "(0 to %d); exiting!\n", ProgName, MaxSamplePerMsg, max );
         exit( -1 );
      }
      return( 1 );
   }

/* Set the type of tracebuf to write out
 ***************************************/
   if(k_its("OutputMsgType") ) {
      str=k_str();
      if( !str || 
         (strcmp(str,"TYPE_TRACEBUF") !=0 &&
          strcmp(str,"TYPE_TRACEBUF2")!=0    ) ) 
      {
         logit("e","%s: <OutputMsgType %s> invalid; must be either "
               "TYPE_TRACEBUF or TYPE_TRACEBUF2; exiting!\n", 
               ProgName, str );
         exit( -1 );
      }
      if( GetType( str, &OutputMsgType ) != 0 ) {
         logit("e","%s: Invalid message type %s!\n", ProgName, str );
         exit( -1 );
      }
      return( 1 );
   }

/* Control how quickly each file is converted
 ********************************************/
   if(k_its("OutputTimeFactor") ) {
      OutputTimeFactor = k_val();
      if( OutputTimeFactor < 0.0 )
      {
         logit( "e", "%s: <OutputTimeFactor %.1lf> outside valid range "
               "(must be >=0.0); exiting!\n", ProgName, OutputTimeFactor );
         exit( -1 );
      }
      return( 1 );
   }

/* Set a limit on how far forward time can jump
 **********************************************/
   if(k_its("TimeJumpTolerance") ) {
      TimeJumpTolerance = k_val();
      if( TimeJumpTolerance < 0.0  &&  TimeJumpTolerance != -1.0 )
      {
         logit( "e", "%s: <TimeJumpTolerance %.1lf> outside valid range "
               "(-1.0 or >=0.0); exiting!\n", ProgName, TimeJumpTolerance );
         exit( -1 );
      }
      return( 1 );
   }

/* Set scaling factor for float to integer conversion
   (used in read_seg2_trace_data.c)
 ****************************************************/
   if(k_its("FloatScalingFactor") ) {
      FloatScalingFactor = k_val();
      if( FloatScalingFactor <= 0.0 )
      {
         logit( "e", "%s: <FloatScalingFactor %.1lf> outside valid range "
               "(>0.0); exiting!\n", ProgName, FloatScalingFactor );
         exit( -1 );
      }
      return( 1 );
   }

/* Set debug level for reading SEG2 file
 ***************************************/
   if(k_its("SEG2DebugFlag") ) {
      SEG2DebugFlag = k_int();
      if( SEG2DebugFlag < 0 )
      {
         logit( "e", "%s: <SEG2DebugFlag %d> outside valid range "
               "(>=0); exiting!\n", ProgName, SEG2DebugFlag );
         exit( -1 );
      }
      return( 1 );
   }

/* Set flag for using DELAY value in timestamp calculation
 *********************************************************/
   if(k_its("AddDelayToTimestamp") ) {
      AddDelayToTimestamp = k_int();
      return( 1 );
   }

/* Set flag for using DESCALING_FACTOR value in trace data conversion
 ********************************************************************/
   if(k_its("ApplyDescalingFactor") ) {
      ApplyDescalingFactor = k_int();
      return( 1 );
   }

/* Didn't recognize the command
 ******************************/
   return( 0 );
}

/******************************************************************************
 * file2ewfilter_init()  Make sure everything's ready for SEG2 conversions    *
 ******************************************************************************/
int file2ewfilter_init( void ) 
{
   int ret = 0;

/* If already initialized, return immediately 
 ********************************************/
   if( seg2toewInit ) return( ret );

/* Make sure all required config file params were set
 ****************************************************/
   if( nSendSCNL == 0 ) {
      logit("e","%s: ERROR: no <SendChannel> or <SendLineStaRcvr>"
            " commands in config file!\n", ProgName );
      ret = -1;
   }
   if( nSendChannel > 0  &&  nSendLineStaRcvr > 0 )
   {
      logit("e", "%s: ERROR: cannot use both <SendChannel> and <SendLineStaRcvr> "
            "commands in the same config file!\n", ProgName );
      ret = -1 ;
   } 
   if( MaxSamplePerMsg == 0 ) {
      logit("e","%s: ERROR: no <MaxSamplePerMsg> command in config file!\n",
            ProgName );
      ret = -1;
   }
   if( OutputMsgType == 0 ) {
      logit("e","%s: ERROR: no <OutputMsgType> command in config file!\n",
            ProgName );
      ret = -1;
   }
   if( OutputTimeFactor == NOTSET )
   {
     logit("e", "%s: ERROR no <OutputTimeFactor> command in config file!\n",
            ProgName );
     ret = -1 ;
   }
   if( TimeJumpTolerance == NOTSET )
   {
     logit("e", "%s: ERROR no <TimeJumpTolerance> command in config file!\n",
            ProgName );
     ret = -1 ;
   }

/* Look up some things in earthworm*d
 ************************************/
   if ( GetType( "TYPE_TRACEBUF", &TypeTraceBuf ) != 0 ) {
      logit( "e", "%s: ERROR: Invalid message type <TYPE_TRACEBUF>\n",
             ProgName );
      ret = -1;
   }
   if ( GetType( "TYPE_TRACEBUF2", &TypeTraceBuf2 ) != 0 ) {
      logit( "e", "%s: ERROR: Invalid message type <TYPE_TRACEBUF2>\n",
             ProgName );
      ret = -1;
   }

/* Initialize some SEG2 stuff 
 ****************************/
   if( init_file_and_trace_desc_keywords( &file_desc_keywords, 
                                          &trace_desc_keywords ) ) {
      logit( "e", "%d: ERROR initializing SEG2 keywords\n", ProgName );
      ret = -1;
   }

   seg2toewInit = 1;
   return( ret );
}

/************************************************************************
 *  file2ewfilter() Given an open file pointer to a SEG2 file and the   *
 *   filename, process the file and write out tracebuf message   	* 
 ************************************************************************/
int file2ewfilter( FILE *input, char *inputfile )
{
  int            i;
  int           *data  = NULL;
  int            status;
  SEG2FILEDESC  *file_desc  = NULL;
  SEG2TRACEDESC *trace_desc = NULL;
  SCNL_INFO     *match;
  SEG2_TIME      tseg2;
  int            channel;
  int            line; 
  int            stanum;
  char          *rcvr;
  int            ntraces;
  int            nmsgout =  0;  /* # messages produced   */
  int            err     = -1;  /* error code for return */

/* Initialize variables
 **********************/
   if( !seg2toewInit ) file2ewfilter_init();
   ShipRate = NOTSET;  /* we will reset this for each file */

/* Read the File Descriptor (already opened in file2ew.c)  
 ********************************************************/
   if( input == NULL )     return( err );
   if( file_desc != NULL ) free_file_desc(&file_desc);
   file_desc = (SEG2FILEDESC *)malloc(sizeof(SEG2FILEDESC));
   if( file_desc == NULL ) {
      logit( "e", "%s: Can't allocate memory block for file_desc.\n",
              ProgName);
      return( err );
   }
   memset (file_desc, 0, sizeof(SEG2FILEDESC));

   status = read_seg2_file_desc (input, file_desc, &ntraces);
   if( status != 0 ) {
      logit( "e", "%s: Error reading SEG2 file descriptor block in %s\n", 
              ProgName, inputfile);
      return( err );
   }

/* Interpret the SEG2 acquisition time
 *************************************/
   status = getSEG2acqtime( file_desc, &tseg2 );
   if( status != 0 ){
      logit( "e", "%s: Error reading SEG2 acquisition time in %s\n", 
              ProgName, inputfile);
      return( err );
   }
   if(Debug) logit("e","SEG2 acqtime as struct: %d/%d/%d %d:%d:%d+%.6lf\n"
                       "SEG2 acqtime as double: %.6lf\n",
                    tseg2.tacq.tm_mday, tseg2.tacq.tm_mon+1, tseg2.tacq.tm_year+1900,
                    tseg2.tacq.tm_hour, tseg2.tacq.tm_min, tseg2.tacq.tm_sec,
                    tseg2.tacq_sec_frac, tseg2.starttime );

/* Loop over all traces, process one trace completely each time thru 
 *******************************************************************/
   for( i=0; i<ntraces; i++ ) 
   {      
      if( trace_desc != NULL ) free_trace_desc(&trace_desc);
      trace_desc = (SEG2TRACEDESC *)malloc(sizeof(SEG2TRACEDESC));
      if( trace_desc == NULL ) {
         logit( "e", "%s: Cannot allocate memory block for trace_desc. "
                "Skipping trace %d in %s\n",  ProgName, i, inputfile);
         continue;
      }
      memset (trace_desc, 0, sizeof(SEG2TRACEDESC));

   /* Seek to the start of the trace, using its pointer in the file descriptor 
    * LDD: simple fseek works for me! For some reason, UCB had trouble...      
     **************************************************************************/
      if( fseek(input, (long)file_desc->trace_ptrs[i], SEEK_SET) ) {
         logit( "e", "%s: Error in fseek to trace data. Skipping trace %d in %s\n", 
               ProgName, i, inputfile);
         free_trace_desc(&trace_desc);
         continue;
      }
      
   /* Read the Trace Descriptor 
    ***************************/
      status = read_seg2_trace_desc(input, file_desc, trace_desc);
      if (status != 0) {
         logit( "e", "%s: Error reading trace desc block. "
                "Skipping trace %d in %s.\n", ProgName, i, inputfile);
         free_trace_desc(&trace_desc);
         continue;
      }

   /* Decide from trace descriptor if we want to process this trace 
    ***************************************************************/
      match = NULL;
   /* Choose traces by SEG2 channel number */
      if( nSendChannel ) {
         channel = atoi( trace_desc->known_keys[CHANNEL_NUMBER]->val );
         match   = FindChannel( SendSCNL, nSendSCNL, channel );
      }
   /* Choose traces by SEG2 LineId, StationNumber, Receiver */
      else if( nSendLineStaRcvr ) {
         line    = atoi( trace_desc->known_keys[LINE_ID]->val );
         stanum  = atoi( trace_desc->known_keys[RECEIVER_STATION_NUMBER]->val );
         rcvr    = trace_desc->known_keys[RECEIVER]->val;
         match   = FindLineStaRcvr( SendSCNL, nSendSCNL, line, stanum, rcvr );
      }
      if( match == NULL ) continue;  /* not interested in this trace */

   /* LDD: ADD CODE: Perform sanity checks on trace descriptor values */

   /* Check for command-line overrides of file or trace descriptor strings. */
   /* if (orfv) override_file_key_val (file_desc, orfv, orfc);
      if (ortv) override_trace_key_val (trace_desc, ortv, ortc);
    */

   /* Check if the size of the trace descriptor is divisible by 4, because *
    * all file and trace descriptor blocks, their strings subblocks, and   *
    * the data blocks must begin on double word boundaries.                *
    * If not, move the file pointer until it is at the next boundary.      */
      if((trace_desc->traceDescBlockSize % 4) != 0) {
         if( fseek(input, (long)(4-(trace_desc->traceDescBlockSize%4)), SEEK_CUR) ) {
            logit( "e", "%s: Error in fseek forward %d bytes to reach "
                   "double word boundary. Skipping trace %d in %s\n", ProgName, 
                    (4-(trace_desc->traceDescBlockSize%4)), i, inputfile );
            free_trace_desc(&trace_desc);
            continue;
         }
      }

   /* Once per file, compute EW shipping rate (sample/sec);
      assumes all channels in file have same sample interval
    ********************************************************/
      if( ShipRate == NOTSET ) {
         if( OutputTimeFactor == 0.0 ) {
            ShipRate = 0.0;
         }
         else {
            double sample_interval; 
            sample_interval = atof( trace_desc->known_keys[SAMPLE_INTERVAL]->val ); 
            ShipRate = (double) nSendSCNL / (sample_interval * OutputTimeFactor);
         }
      }
   
   /* Read trace data samples into buffer 
    *************************************/
      status = read_seg2_trace_data(input, file_desc, trace_desc, &data);
      if( status != 0 || data == NULL ) {
         logit( "e", "%s: Error reading data block. Skipping trace %d in %s.\n", 
                ProgName, i, inputfile );
         free_trace_desc( &trace_desc );
         continue;
      }
  
   /* Convert SEG2 to Earthworm TYPE_TRACEBUF2 and write to ring
    ************************************************************/
      status = seg2toew_shipdata( match, &tseg2, trace_desc, data );
      if( status > 0 ) nmsgout += status;

   /* Clean up after converting this trace 
    **************************************/
      free_trace_desc(&trace_desc);
      free           (data);
      data  = NULL;

    } /* end for over all traces */

    free_file_desc(&file_desc); /* clean up after converting this file */
    return( nmsgout );
} 

/******************************************************************************
 * file2ewfilter_hbeat()  read heartbeat file, check for trouble              *
 ******************************************************************************/
int file2ewfilter_hbeat( FILE *fp, char *fname, char *system )
{
   return( 0 );
}

/******************************************************************************
 * file2ewfilter_shutdown()  free memory, other cleanup tasks                 *
 ******************************************************************************/
void file2ewfilter_shutdown( void ) 
{
   free_keywords();
   return;
}

/************************************************************************ 
 *  free_file_desc() Free file descriptor structs to start fresh        * 
 ************************************************************************/
void free_file_desc(SEG2FILEDESC **file_desc) 
{
  if (*file_desc) {
    if ((*file_desc)->strings) {
      free               ((*file_desc)->strings);
    }
    if ((*file_desc)->trace_ptrs) {
      free               ((*file_desc)->trace_ptrs);
    }
    if ((*file_desc)->known_keys) {
      free_key_val_pairs ((*file_desc)->known_keys,NUM_OF_FD_KEYWORDS);
    }
    if ((*file_desc)->unknown_keys) {
      free_key_val_pairs ((*file_desc)->unknown_keys,(*file_desc)->unknown_keys_count);
      free               ((*file_desc)->unknown_keys); 
    }
    free                  (*file_desc);
    *file_desc = NULL;
  }
}

/************************************************************************ 
 *  free_trace_desc() Free trace descriptor blocks to start fresh       * 
 ************************************************************************/
void free_trace_desc(SEG2TRACEDESC **trace_desc) 
{
  if (*trace_desc) {
    if ((*trace_desc)->strings) {
      free               ((*trace_desc)->strings);
    }
    if ((*trace_desc)->known_keys) {
      free_key_val_pairs ((*trace_desc)->known_keys,NUM_OF_TD_KEYWORDS);
    }
    if ((*trace_desc)->unknown_keys) {
      free_key_val_pairs ((*trace_desc)->unknown_keys,(*trace_desc)->unknown_keys_count);
      free               ((*trace_desc)->unknown_keys);
    }
    free                 (*trace_desc);
    *trace_desc = NULL;
  }
}

/************************************************************************ 
 *  free_key_val_pairs() Free key value pairs structs to start fresh    * 
 ************************************************************************/
void free_key_val_pairs( KEY_VAL_PAIR **pairs, int num_pairs ) 
{
  int i;

  for( i=num_pairs-1; i>=0; i-- ) {
    if( pairs[i] == NULL ) continue;
    if( pairs[i]->key )    free (pairs[i]->key);
    if( pairs[i]->val )    free (pairs[i]->val);
    free( pairs[i] );
  }
}

/************************************************************************ 
 *  free_keywords() Free key words to start fresh                       * 
 ************************************************************************/
void free_keywords(void)
{
  if (file_desc_keywords) {
    if (file_desc_keywords->keywords) {
      free(file_desc_keywords->keywords);
    }
    free(file_desc_keywords);
    file_desc_keywords  = NULL;
  }
  if (trace_desc_keywords) {
    if (trace_desc_keywords->keywords) {
      free(trace_desc_keywords->keywords);
    }
    free(trace_desc_keywords);
    trace_desc_keywords = NULL;
  }
}

/*******************************************************************************
 * IsValidLocChar() returns 1 if arg is a valid location code character;       *
 *                  returns 0 if arg is not a valid location code character    *
 *******************************************************************************/
int IsValidLocChar( char c )
{
   if( c >= '0'  &&  c <= '9' ) return( 1 );
   if( c >= 'A'  &&  c <= 'Z' ) return( 1 );
   if( c == '-' )               return( 1 );
   return( 0 );
}

/*****************************************************************************
 * seg2toew_shipdata() takes a buffer of SEG2 data, translates it into       *
 *   TYPE_TRACEBUF2 message(s) and writes them to the transport region       *
 *   Returns number of messages shipped on success, -1 on error              *
 *****************************************************************************/
int seg2toew_shipdata( SCNL_INFO     *scnl,       
                       SEG2_TIME     *tseg2, 
                       SEG2TRACEDESC *trace_desc, 
                       int           *data )
{
   char          tbuf[MAX_TRACEBUF_SIZ];   /* buffer for outgoing message    */
   TRACE2_HEADER thd;
   double        dt              = 0.0;   
   double        dtsys           = 0.0; 
   double        starttime       = 0.0; /* from tseg2      */
   double        sample_interval = 0.0; /* from trace_desc */
   double        delay           = 0.0; /* from trace_desc */
   int           isamprate       = 0;   /* from trace_desc */
   int           nsamp           = 0;   /* from trace_desc */
   int           nskip           = 0;   /* # samples to ignore due to time overlap */
   int           nshipped;
   int           nleft;
   int           samplepermsg;
   int           newsamprate     = 0;
   int           nmsg            = 0;   /* # messages sent to ring                 */

/* Initialize some things 
 ************************/
   memset( &tbuf, 0, MAX_TRACEBUF_SIZ );
   memset( &thd,  0, sizeof(TRACE2_HEADER) );

/* Set DELAY value, if needed 
 ****************************/
   if( AddDelayToTimestamp != 0 )
   {
     if( trace_desc->known_keys[DELAY] ) { 
       delay = (double) atof( trace_desc->known_keys[DELAY]->val );
     }
     else {
       logit("e","Trace DELAY key/value does not exist, setting to 0.0!\n" );
       delay = 0.0;
     }
   }
   if(Debug) logit("e","%s.%s.%s.%s delay=%.4lf\n",
                   scnl->sta, scnl->cmp, scnl->net, scnl->loc, delay );
 
/* Interpret important stuff from SEG2 descriptor blocks
 *******************************************************/
   sample_interval = atof( trace_desc->known_keys[SAMPLE_INTERVAL]->val ); 
   isamprate       = (int)(1.0/sample_interval);
   starttime       = (tseg2->starttime) + delay;  
   nsamp           = trace_desc->numOfSamples;

/* First data for this channel.  
   Initialize fields for gap/overlap testing (lastrate,texp)
 ***********************************************************/
   if( scnl->lastrate == 0 ) 
   {
      scnl->lastrate = isamprate;
      scnl->texp     = starttime;
   }

/* Check for time tears (overlaps/abnormal forward jumps). 
   If any are found, ignore the whole packet!
 *********************************************************/
   dt = starttime - scnl->texp;
   if(Debug) logit("e","%s.%s.%s.%s dt=%.4lf\n",
                   scnl->sta, scnl->cmp, scnl->net, scnl->loc, dt );
   if( ABS(dt) > (double)0.5/scnl->lastrate ) 
   {
      if( dt > 0.0 ) /* time gap */
      {
      /* If starttime later than system-clock (with slop), it's "abnormal" */
      /* time jump.  Probably a bad timestamp - ignore the packet!         */
         dtsys = starttime - (double)time(NULL);
         if( dtsys >= TimeJumpTolerance  &&  TimeJumpTolerance != -1.0 ) {
            logit("e","%s: %s.%s.%s.%s %.4lf %.4lf s gap detected; "
                  "dtsys %.0lf s; ignoring packet with probable bogus timestamp\n",
                   ProgName, scnl->sta, scnl->cmp, scnl->net, scnl->loc, 
                   scnl->texp, dt, dtsys );
            return( nmsg );
         }
         logit("e","%s: %s.%s.%s.%s %.3lf %.3lf s gap detected\n",
                ProgName, scnl->sta, scnl->cmp, scnl->net, scnl->loc, 
                scnl->texp, dt );
      }
      else  /* time overlap - decide how many samples to skip  */
      {     /* so we don't create a time overlap in data to EW */
         nskip = (int)(ABS(dt)/sample_interval);
         logit("e","%s: %s.%s.%s.%s %.4lf %.4lf s overlap detected; "
               "skipping %d samples\n", ProgName, scnl->sta, scnl->cmp, 
                scnl->net, scnl->loc, scnl->texp, ABS(dt), nskip );
      }
   }

/* Fill out static parts of the tracebuf header
 **********************************************/
   strncpy( thd.sta,  scnl->sta, STA_LEN+1 );
   strncpy( thd.chan, scnl->cmp, CMP_LEN+1 );
   strncpy( thd.net,  scnl->net, NET_LEN+1 );

   if( OutputMsgType == TypeTraceBuf2 )
   {
      strncpy( thd.loc, scnl->loc, LOC_LEN+1 );
      thd.version[0] = TRACE2_VERSION0;
      thd.version[1] = TRACE2_VERSION1;
   }

   thd.pinno  = scnl->pinno;
#ifdef _INTEL
   strncpy( thd.datatype, "i4", 3 );
#else
   strncpy( thd.datatype, "s4", 3 );
#endif
   thd.quality[0] = thd.quality[1] = 0;
   thd.pad[0]     = thd.pad[1]     = 0;

/* Set maximum message size (we want to send 1-second msgs if possible)
 **********************************************************************/
   thd.samprate = (double)isamprate;
   samplepermsg = isamprate;
   if( samplepermsg > MaxSamplePerMsg ) samplepermsg = MaxSamplePerMsg;

/* Package & ship data samples
 *****************************/
   nshipped = nskip;
   nleft    = nsamp - nskip;
   while( nleft > 0  &&  samplepermsg )
   {
      int nload = nleft;
      int msglen;
      if( nload > samplepermsg ) nload = samplepermsg;

      thd.starttime = starttime + (double)nshipped/thd.samprate;
      thd.endtime   = thd.starttime + (double)(nload-1)/thd.samprate;
      thd.nsamp     = nload;

      memcpy( tbuf, &thd, sizeof(TRACE2_HEADER) );
      memcpy( tbuf+sizeof(TRACE2_HEADER),
              data+nshipped, nload*sizeof(int) );
      msglen = sizeof(TRACE2_HEADER) + thd.nsamp*sizeof(int);

      if( file2ew_ship( OutputMsgType, 0, tbuf, msglen ) != 0 )
      {
         logit("et", "%s: Error putting %d-byte tracebuf msg in ring\n",
                ProgName, msglen );
      } else {
         nmsg++;
      }
      nshipped += nload;
      nleft    -= nload;
      scnl->lastrate = isamprate;      
      scnl->texp     = thd.endtime + (double)1.0/thd.samprate;
      if( Debug ) logit("e","%s.%s.%s.%s texp=%.3lf after new data shipped\n",
                        scnl->sta,scnl->cmp,scnl->net,scnl->loc,scnl->texp );
      if( ShipRate != 0.0 ) {   /* pause between packets */
         int pause = (int) (1000.0 * nload / ShipRate );
         sleep_ew( pause );  
      }      
   }
   return( nmsg );
}

/*******************************************************************************
 * getSEG2acqtime() converts the acquisition time strings in file desciptor    *
 *                  into a struct tm and double seconds since 1970             *
 *                  Returns 0 on success, 1 on failure                         *
 *******************************************************************************/
int getSEG2acqtime( SEG2FILEDESC *file_desc, SEG2_TIME *tseg2 )
{
   char *val;
   char  smonth[4];
   int   day,month,year,hour,min,sec;
   int   nread;

   memset( tseg2, 0, sizeof(SEG2_TIME) );

/* ACQUISITION DATE: DD/MM/YYYY or DD/Mon/YYY 
 ********************************************/
  if ((file_desc->known_keys)[ACQUISITION_DATE]) { /* this key & val pair exists */
    val = ((file_desc->known_keys)[ACQUISITION_DATE])->val;
    if( strlen(val)!=10 &&      /* 10 chars would form the string "DD/MM/YYYY"  */
        strlen(val)!=11    ) {  /* 11 chars would form the string "DD/Mon/YYYY" */
      logit("e","Error: 'ACQUISITION_DATE' string (%s) is not 10 or 11 chars long\n", 
             val );
      return( 1 );
    }
    nread = sscanf( val, "%d/%[^/]/%d", &day, smonth, &year );
    if( nread != 3 ) {
      logit("e","Error reading ACQUISITION_DATE string: %s\n", val );
      return( 1 );
    }
  }
  else { 
    logit("e","ACQUISITION_DATE key/value does not exist!\n" );
    return( 1 );
  }
  
/* Month can be alpha or numeric; figure out which */
  if     ( strcasecmp( smonth, "jan" ) == 0 )  month = 1;
  else if( strcasecmp( smonth, "feb" ) == 0 )  month = 2;
  else if( strcasecmp( smonth, "mar" ) == 0 )  month = 3;
  else if( strcasecmp( smonth, "apr" ) == 0 )  month = 4;
  else if( strcasecmp( smonth, "may" ) == 0 )  month = 5;
  else if( strcasecmp( smonth, "jun" ) == 0 )  month = 6;
  else if( strcasecmp( smonth, "jul" ) == 0 )  month = 7;
  else if( strcasecmp( smonth, "aug" ) == 0 )  month = 8;
  else if( strcasecmp( smonth, "sep" ) == 0 )  month = 9;
  else if( strcasecmp( smonth, "oct" ) == 0 )  month = 10;
  else if( strcasecmp( smonth, "nov" ) == 0 )  month = 11;
  else if( strcasecmp( smonth, "dec" ) == 0 )  month = 12;
  else                                         month = atoi( smonth );

/* ACQUISITION_TIME: HH:MM:SS 
 ****************************/
  if( (file_desc->known_keys)[ACQUISITION_TIME] ) { 
    val = ((file_desc->known_keys)[ACQUISITION_TIME])->val;
    if( strlen(val) != 8 ) { /* presumably the 8 chars form the string "HH:mm:ss" */
      logit("e","Error: 'ACQUISITION_TIME' string (%s) is not 8 chars long\n", val );
      return( 1 );
    }
    nread = sscanf( val, "%d:%d:%d", &hour, &min, &sec );
    if( nread != 3 ) {
      logit("e","Error reading ACQUISITION_TIME string: %s\n", val );
      return( 1 );
    }
  }
  else {
    logit("e","ACQUISITION_TIME key/value does not exist!\n" );
    return( 1 );
  }

/* ACQUISITION_SECOND_FRACTION 
 *****************************/
  if ((file_desc->known_keys)[ACQUISITION_SECOND_FRACTION]) { 
    tseg2->tacq_sec_frac = atof(((file_desc->known_keys)[ACQUISITION_SECOND_FRACTION])->val);
  }
  else {
    logit("e","ACQUISITION_SECOND_FRACTION key/value does not exist, setting to 0.0!\n" );
    tseg2->tacq_sec_frac = 0.0;
  }

/* Fill out struct tm and convert time to double 
 ***********************************************/
  tseg2->tacq.tm_sec  = sec;       /* seconds after the minute - [0, 61] */
                                  /* for leap seconds */
  tseg2->tacq.tm_min  = min;       /* minutes after the hour - [0, 59] */
  tseg2->tacq.tm_hour = hour;      /* hour since midnight - [0, 23] */
  tseg2->tacq.tm_mday = day;       /* day of the month - [1, 31] */
  tseg2->tacq.tm_mon  = month-1;   /* months since January - [0, 11] */
  tseg2->tacq.tm_year = year-1900; /* years since 1900 */

  tseg2->starttime = (double) timegm_ew( &(tseg2->tacq) ) +
                     tseg2->tacq_sec_frac;

  return( 0 );
}


/*********************************************************************
 * FindChannel()  looks thru a list on SCNL_INFO structures to find  *
 *   one that matches the given channel key.                         *
 *                                                                   *
 *   list      array of structures which holds all the channels      *
 *              to be searched thru (will not contain wildcards)     *
 *   nlist     number of channels in 'list'                          *
 *   chankey   channel key that you want to find in 'list'           *
 *                                                                   *
 *   Returns:  valid pointer to the matching SCNL_INFO structure     *
 *                  on success                                       *
 *             NULL on failure  or no match                          *
 *********************************************************************/
SCNL_INFO *FindChannel( SCNL_INFO *list, int nlist, int channel )
{
   SCNL_INFO *pmatch = NULL;
   SCNL_INFO  find;

   find.channel = channel;

   if( !SCNLchansorted ) {
      qsort( list, (size_t)nlist, sizeof(SCNL_INFO), compare_SCNL_INFO_chan );
      SCNLchansorted = 1;
      SCNLlsrsorted  = 0;
   }
   pmatch = (SCNL_INFO *) bsearch( &find, list, (size_t)nlist,
                                   sizeof(SCNL_INFO), compare_SCNL_INFO_chan );
   return( pmatch );
}


/*********************************************************************
 * FindLineStaRcvr()  looks thru a list on SCNL_INFO structures to   *
 *   find one that matches the given line_id, station_number,        *
 *   receiver name.                                                  *
 *                                                                   *
 *   list      array of structures which holds all the channels      *
 *              to be searched thru (will not contain wildcards)     *
 *   nlist     number of channels in 'list'                          *
 *   lineid    SEG2 line_id that you want to find in 'list'          *
 *   stanum    SEG2 station number that you want to find in 'list'   *
 *   rcvr      SEG2 receiver string that you want to find in 'list'  *
 *                                                                   *
 *   Returns:  valid pointer to the matching SCNL_INFO structure     *
 *                  on success                                       *
 *             NULL on failure  or no match                          *
 *********************************************************************/
SCNL_INFO *FindLineStaRcvr( SCNL_INFO *list, int nlist, 
                            int lineid, int stanum, char *rcvr )
{
   SCNL_INFO *pmatch = NULL;
   SCNL_INFO  find;

   find.lineid = lineid;
   find.stanum = stanum;
   strncpy( find.rcvr, rcvr, MAX_VAL_LEN );
   find.rcvr[MAX_VAL_LEN+1] = 0;

   if( !SCNLlsrsorted ) {
      qsort( list, (size_t)nlist, sizeof(SCNL_INFO), compare_SCNL_INFO_lsr );
      SCNLlsrsorted  = 1;
      SCNLchansorted = 0;
   }
   pmatch = (SCNL_INFO *) bsearch( &find, list, (size_t)nlist,
                                   sizeof(SCNL_INFO), compare_SCNL_INFO_lsr );
   return( pmatch );
}


/*********************************************************************
 * compare_SCNL_INFO_chan()  sort by SEG2 channel number             *
 *  This function is passed to qsort() and bsearch().                *
 *  We use qsort() to sort the SCNL_INFO struct by SEG2 channel, and *
 *  we use bsearch() to look up a SEG2 channel in the SCNL_INFO list.*
 *********************************************************************/
int compare_SCNL_INFO_chan( const void *s1, const void *s2 )
{
   SCNL_INFO *ch1 = (SCNL_INFO *) s1;
   SCNL_INFO *ch2 = (SCNL_INFO *) s2;

   if( ch1->channel > ch2->channel ) return  1;
   if( ch1->channel < ch2->channel ) return -1;
   return 0;
}


/*********************************************************************
 * compare_SCNL_INFO_lsr()  sort by lineid, station number, receiver *
 *  This function is passed to qsort() and bsearch().                *
 *  We use qsort() to sort the SCNL_INFO struct by SEG2 channel, and *
 *  we use bsearch() to look up a SEG2 channel in the SCNL_INFO list.*
 *********************************************************************/
int compare_SCNL_INFO_lsr( const void *s1, const void *s2 )
{
   SCNL_INFO *ch1 = (SCNL_INFO *) s1;
   SCNL_INFO *ch2 = (SCNL_INFO *) s2;

   if( ch1->lineid > ch2->lineid  ) return  1;
   if( ch1->lineid < ch2->lineid  ) return -1;
   if( ch1->stanum > ch2->stanum  ) return  1;
   if( ch1->stanum < ch2->stanum  ) return -1;
   return( strncmp( ch1->rcvr, ch2->rcvr, MAX_VAL_LEN ) );
}
