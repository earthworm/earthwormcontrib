#
#   THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU HAVE
#   CHECKED IT OUT.
#
#    $Id: makefile.sol 126 2005-04-08 16:32:03Z dietz $
#
#    Revision history:
#     $Log$
#     Revision 1.1  2005/04/08 16:32:02  dietz
#     Initial version. SEG2 file readin code based on UCB's seg2toms code.
#

CFLAGS = -D_REENTRANT $(GLOBALFLAGS)

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib
F = $(EW_HOME)/$(EW_VERSION)/src/data_exchange/file2ew

OBJS = read_seg2_file_desc.o read_seg2_trace_desc.o read_seg2_trace_data.o \
       seg2_strings.o readSEG2failed.o fread_and_swap.o 

EWOBJS = $L/kom.o $L/getutil.o $L/logit.o $L/dirops_ew.o \
         $L/transport.o $L/sleep_ew.o $L/swap.o $L/time_ew.o 

F2EWOBJS = $F/file2ew.o

seg2toew: seg2toew.o $(OBJS) $(EWOBJS) $(F2EWOBJS)
	cc -o $B/seg2toew seg2toew.o $(OBJS) $(EWOBJS) $(F2EWOBJS) -lm -lposix4 

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/seg2toew*
