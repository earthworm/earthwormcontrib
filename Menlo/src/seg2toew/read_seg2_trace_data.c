/* 
 *   THIS FILE IS UNDER CVS. 
 *   DO NOT MODIFY UNLESS YOU HAVE CHECKED IT OUT.
 *
 *    $Id: read_seg2_trace_data.c 678 2011-09-01 22:52:40Z dietz $ 
 * 
 *    Revision history: 
 *     $Log$
 *     Revision 1.4  2005/07/25 23:13:09  dietz
 *     Changed default FloatScalingFactor to 10000 (had been 100000)
 *
 *     Revision 1.3  2005/06/10 20:21:45  dietz
 *     Added debug logging of min/max trace values and raw data values.
 *     Made SEG2DebugFlag configurable.
 *
 *     Revision 1.2  2005/06/08 20:47:13  dietz
 *     Added optional command "FloatScalingFactor" to override the default
 *     scaling factor for converting IEEE float data into 4-byte integers.
 *
 *     Revision 1.1  2005/04/08 16:32:03  dietz
 *     Initial version. SEG2 file readin code based on UCB's seg2toms code.
 * 
 */ 

#ifndef lint
static char sccsid[] = "$Id: read_seg2_trace_data.c 678 2011-09-01 22:52:40Z dietz $ ";
#endif

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "seg2_struct.h"
#include "procs.h"

#define FLOAT_SCALING_FACTOR 10000  /* default value by which float data are  */
                                    /* multiplied to get into integer form    */
double  FloatScalingFactor = FLOAT_SCALING_FACTOR;    /* can change in config */

int     ApplyDescalingFactor = 0;   /* 0= ignore DESCALING_FACTOR while       */
                                    /*    converting trace data values to     */
                                    /*    Earthworm format                    */
                                    /* non-zero= multiply trace data values   */
                                    /*    by DESCALING_FACTOR when converting */ 
                                    /*    to Earthworm format                 */

extern int Debug;

extern int SEG2DebugFlag;           /* debug flag using DBG_FLAGS bit set,    */
                                    /* defined in seg2_struct.h               */


/****************************************************************************
*  read_seg2_trace_data:						    *
*	Read SEG2 trace data.						    *
*       UCB's original version also filled out a MiniSEED header.           *
*       This new Earthworm version strictly reads the data samples from     *
*       the input file and returns. All Earthworm format conversion will    * 
*       occur in a different function.                                      *
*  return:								    *
*	0 on success, 1 on failure.					    *
*	Set *data to point to allocated data buffer.			    *
*	Caller must free data buffer.                                       *
*  note:                                                                    *
*       It is necessary for pdata to be a pointer to pointer,               *
*       in order to be able to assign to it the memory block                *
*       which has been allocated for it and its contents.                   * 
****************************************************************************/
int read_seg2_trace_data
   (FILE	  *input,
    SEG2FILEDESC  *file_desc,
    SEG2TRACEDESC *trace_desc,
    int		 **pdata)
{
  double     descalingfactor = 1.0;  /* can be overridden by header value */
  int        num_samples = trace_desc->numOfSamples;
  int       *data    = NULL;
  short int *sdata   = NULL;
  float     *fdata   = NULL;
  int	     iminval = 0;
  int	     imaxval = 0;
  float      fminval = 0;
  float      fmaxval = 0;
 

  int        i;
  size_t   (*read_chars)(void *, size_t, size_t, FILE *); /* function pointer */
    
/* Check for known Data Formats before moving on
 ***********************************************/
  switch (trace_desc->dataFormatCode) {
    case SEG2_INT_16:         break;
    case SEG2_INT_32:         break;
    case SEG2_FLOAT_32_IEEE:  break; /* 32-bit IEEE Floating Point, Single Precision */
    case SEG2_FLOAT_20_SEGD:   
         logit( "e", "20-bit floating point (SEG-D) format unsupported.\n");
         return(1);
    case SEG2_FLOAT_64_IEEE:   
         logit( "e", "64-bit floating point (IEEE standard) format unsupported.\n"); 
         return(1);
    default:  
         logit( "e", "Invalid data format code: %d\n", trace_desc->dataFormatCode);
         return(1);
  }
    
/* Read SEG2 trace data.						
 ***********************/
  data = (int *)malloc(sizeof(int) * num_samples);
  if (data == NULL) {
    logit( "e", "Error mallocing data array for %d samples\n",num_samples);
    return (1);
  }

  /* Look at the SEG-2 File Descriptor's Block ID to               *
   * determine if the byte order of the file is native or swapped, *
   * and set the file character reading function accordingly.      */
  if      (file_desc->block_ID == NATIVE_BYTEORDER_ID)  { /* 0x3A55 */
    read_chars = fread;           /* assign the function pointer */
  }
  else if (file_desc->block_ID == SWAPPED_BYTEORDER_ID) { /* 0x553A */
    /* fread_and_swap swaps the order of the bytes it reads. */
    read_chars = fread_and_swap;  /* assign the function pointer */
  }
  else {
    logit( "e",
            "Error: invalid SEG-2 File Descriptor Block ID: 0x%X. Skipping SEG-2 file.\n",
            file_desc->block_ID);
    return(1);
  }

  switch (trace_desc->dataFormatCode) {
    case SEG2_INT_32:
      if (read_chars(data,
                     sizeof(int),
                     num_samples,
                     input) != (size_t) num_samples) {
        logit( "e", "Error reading %d samples of int data\n", num_samples);
        return(1);
      }
      if( SEG2DebugFlag & DBG_FLAG_TD_MIN_MAX ) {
	for (i=0; i<num_samples; i++) {
	    if (data[i] > imaxval) imaxval = data[i];
	    if (data[i] < iminval) iminval = data[i];
	}
	logit( "e", "Trace min,max values are: %d %d\n", iminval, imaxval);
      }
      if( SEG2DebugFlag & DBG_FLAG_TD_DUMP ) {
	logit( "e", "Integer (4-byte) trace data:\n");
	for( i=0; i<num_samples; i++ ) {
	    logit( "e", " %10d", data[i]);
            if( i%10==9 ) logit( "e", "\n" );
	}
	logit( "e", "\n");
      }
      break;

    case SEG2_INT_16:
      sdata = (short int *)malloc(sizeof(short int) * num_samples);
      if (sdata == NULL) {
        logit( "e", "Error mallocing short int data array for %d samples\n", num_samples);
        return (1);
      }
      if (read_chars(sdata,
                     sizeof(short int),
                     num_samples,
                     input) != (size_t) num_samples) {
        logit( "e", "Error reading %d samples of short int data\n", num_samples);
        return(1);
      }
      for (i=0; i<num_samples; i++) {
        data[i] = sdata[i];
      }
      if( SEG2DebugFlag & DBG_FLAG_TD_MIN_MAX ) {
	for (i=0; i<num_samples; i++) {
	    if (data[i] > imaxval) imaxval = data[i];
	    if (data[i] < iminval) iminval = data[i];
	}
	logit( "e", "Trace min,max values are: %d %d\n", iminval, imaxval);
      }
      if( SEG2DebugFlag & DBG_FLAG_TD_DUMP ) {
	logit( "e", "Integer (2-byte) trace data:\n");
	for( i=0; i<num_samples; i++ ) {
	    logit( "e", " %10hd", sdata[i]);
            if( i%10==9 ) logit( "e", "\n" );
	}
	logit( "e", "\n");
      }
      free (sdata);
      break;

    case SEG2_FLOAT_32_IEEE:
      fdata = (float *)malloc(sizeof(float) * num_samples);
      if (fdata == NULL) {
        logit( "e", "Error mallocing float data array for %d samples\n", num_samples);
        return (1);
      }
      if (read_chars(fdata,
                     sizeof(float),
                     num_samples,
                     input) != (size_t) num_samples) {
        logit( "e", "Error reading %d samples of float data\n", num_samples);
        return(1);
      }
      if( SEG2DebugFlag & DBG_FLAG_TD_MIN_MAX ) {
	for (i=0; i<num_samples; i++) {
	    if (fdata[i] > fmaxval) fmaxval = fdata[i];
	    if (fdata[i] < fminval) fminval = fdata[i];
	}
	logit( "e", "Trace min,max values are: %.8f %.8f\n", fminval, fmaxval);
      }
      if( SEG2DebugFlag & DBG_FLAG_TD_DUMP ) {
	logit( "e", "Float trace data:\n" );
	for (i=0; i<num_samples; i++) {
	    logit( "e", " %14.8f", fdata[i] );
            if( i%10==4 ) logit( "e", "\n" );
            if( i%10==9 ) logit( "e", "\n" );
	}
	logit( "e", "\n" );
      }

      /* Look up descaling factor, if configured to use */
      if( ApplyDescalingFactor != 0 )
      {
        if( trace_desc->known_keys[DESCALING_FACTOR] ) { 
          descalingfactor = atof( trace_desc->known_keys[DESCALING_FACTOR]->val );
        }
        else {
          logit("e","Trace DESCALING_FACTOR key/value does not exist, setting to 1.0!\n" );
          descalingfactor = 1.0;
        }
      }
      if(Debug) logit("e","descalingfactor=%.9lf\n", descalingfactor );
   
      /* Convert float data to integer */
      for (i=0; i<num_samples; i++) {
       /* Add 0.5 to positive scaled float values to round off the *
        * value correctly, rather than simply truncating it.       * 
        * Subtract 0.5 from negative values to do proper rounding! */
        double ddata = descalingfactor*FloatScalingFactor*(double)fdata[i];
        if( ddata >= 0.0 ) data[i] = (int)(ddata + 0.5000);
        else               data[i] = (int)(ddata - 0.5000);

      /*logit( "e", "%7d %19.16f  %19.9f  %8d\n",
                i, fdata[i], descalingfactor*FloatScalingFactor*(double)fdata[i], data[i] );*/
      }
      free (fdata);
      break;

    default:
      logit( "e", "Unsupported data format: %d\n", trace_desc->dataFormatCode );
      return (1);
      break;
  }

  *pdata = data;

  return (0);
}


void print_strings( SEG2FILEDESC  *file_desc, SEG2TRACEDESC *trace_desc ) {
  int i = 0;

  logit( "e", "The file descriptor keywords and values (from its strings section) are as follows:\n");
  for (i = 0; i < MAX_FD_KEY_INDEX; i++) {
    if (file_desc->known_keys[i])
      logit( "e", "%30s = %s\n", file_desc->known_keys[i]->key, file_desc->known_keys[i]->val);
  }
  for (i = 0; i < file_desc->unknown_keys_count; i++) {
    logit( "e", "%30s = %s\n", file_desc->unknown_keys[i]->key, file_desc->unknown_keys[i]->val);
  }

  logit( "e", "\nThe trace descriptor keywords and values (from its strings section) are as follows:\n");
  for (i = 0; i < MAX_TD_KEY_INDEX; i++) {
    if (trace_desc->known_keys[i])
      logit( "e", "%30s = %s\n", trace_desc->known_keys[i]->key, trace_desc->known_keys[i]->val);
  }
  for (i = 0; i < trace_desc->unknown_keys_count; i++) {
    logit( "e", "%30s = %s\n", trace_desc->unknown_keys[i]->key, trace_desc->unknown_keys[i]->val);
  }
}
