/* 
 *   THIS FILE IS UNDER CVS. 
 *   DO NOT MODIFY UNLESS YOU HAVE CHECKED IT OUT.
 *
 *    $Id: seg2_struct.h 133 2005-06-10 20:21:45Z dietz $ 
 * 
 *    Revision history: 
 *     $Log$
 *     Revision 1.4  2005/06/10 20:21:45  dietz
 *     Added debug logging of min/max trace values and raw data values.
 *     Made SEG2DebugFlag configurable.
 *
 *     Revision 1.3  2005/06/08 20:51:28  dietz
 *     Reverting to revision 1.1
 *
 *     Revision 1.1  2005/04/08 16:32:03  dietz
 *     Initial version. SEG2 file readin code based on UCB's seg2toms code.
 * 
 */ 

#ifndef seg2_struct_h
#define seg2_struct_h

#define NATIVE_BYTEORDER_ID  0x3A55  /* formerly BIG_ENDIAN_ID    */
#define SWAPPED_BYTEORDER_ID 0x553A  /* formerly LITTLE_ENDIAN_ID */

/* Define all possible SEG2 data formats */
#define SEG2_INT_16          1
#define SEG2_INT_32          2
#define SEG2_FLOAT_20_SEGD   3
#define SEG2_FLOAT_32_IEEE   4
#define SEG2_FLOAT_64_IEEE   5

#define TRUE                 1
#define FALSE                0

typedef enum debug_flags {
  DBG_FLAG_FD_TRACES      =  1, /* file descriptor #traces and trace ptrs. */
  DBG_FLAG_FD_VALS        =  2, /* file descriptor values                  */
  DBG_FLAG_FD_STRS_PARSED =  4, /* file descriptor strings (keywd=value)   */
  DBG_FLAG_TD_VALS        =  8, /* trace descriptor values                 */
  DBG_FLAG_TD_STRS_PARSED = 16, /* trace descriptor strings (keywd=value)  */
  DBG_FLAG_TD_MIN_MAX	  = 32, /* trace data min,max data values.         */
  DBG_FLAG_TD_DUMP	  = 64,	/* trace data raw data values.             */
  ALL_DBG_FLAGS_ON = 1 | 2 | 4 | 8 | 16 | 32 | 64,
} DBG_FLAGS;


/*******************************************************************************
*                    KEYWORD / VALUE PAIRS OF STRINGS SECTIONS:                *
* Definitions of keywords (and structures used to search for them in strings)  * 
* defined or known for SEG2 File and Trace Descriptor Blocks' strings section. *
*******************************************************************************/

/*
 * Each KEY_VAL_PAIR structure contains one keyword and its associated
 * value from the File and Trace Descriptor Blocks' string sections.
 */
typedef struct key_val_pair {
  int   key_length;
  int   val_length;
  char *key; /* array length to be determined when string is parsed, but will equal key_length. */
  char *val; /* array length to be determined when string is parsed, but will equal val_length. */
} KEY_VAL_PAIR;

/*
 * The SEG2_KEYWORDS structure is used to store the list of known keywords for
 * either the file descriptor block's string section, or
 * for  the  trace descriptor block's string section.
 * When it is being searched through to find a keyword, the keywords[] array is
 * treated like a circular buffer, the current_index indexes the array at
 * either the first keyword or at the first keyword after the last keyword
 * matched.  Since the keywords are stored in this sturcture in alphabetical
 * order, and they are supposed to be stored in alphabetical order in the SEG2
 * file and trace descriptors, this ensures the least number of searching will
 * be done to match the keywords, while still allowing for the keywords not to
 * be stored in the SEG2 file in alphabetical order.  The max index is used to
 * determine if the index has reached the end of the list and needs to be set
 * to zero in order to keep searching through all the words.  The current_index
 * is not reset unless the search for a keyword was successful.
 */
typedef struct seg2_keywords {
  int    current_index;
  int    max_index;
  char **keywords; /* gets assigned either the File or Trace Descriptor keywords */
} SEG2_KEYWORDS;

/*******  FILE DESCRIPTOR SPECIFIC KEYWORDS *******/
/*
 * The enumerations in fd_key_indices must be in the same order as
 * the strings list in the file_desc_keywords->keywords[] array.
 */
typedef enum fd_key_indices {
  ACQUISITION_DATE,
  ACQUISITION_SECOND_FRACTION,
  ACQUISITION_TIME,
  CLIENT,
  COMPANY,
  GENERAL_CONSTANT,
  INSTRUMENT,
  JOB_ID,
  OBSERVER,
  PROCESSING_DATE,
  PROCESSING_TIME,
  TRACE_SORT,
  UNITS,
  FD_NOTE,
  MAX_FD_KEY_INDEX = FD_NOTE,
  NUM_OF_FD_KEYWORDS,
} FD_KEY_INDICES;


/*******  TRACE DESCRIPTOR SPECIFIC KEYWORDS *******/
/*
 * The enumerations in td_key_indices must be in the same order as
 * the strings list in the trace_desc_keywords->keywords[] array.
 */
typedef enum td_key_indices {
  ALIAS_FILTER,
  AMPLITUDE_RECOVERY,
  BAND_REJECT_FILTER,
  CDP_NUMBER,
  CDP_TRACE,
  CHANNEL_NUMBER,
  DATUM,
  DELAY,
  DESCALING_FACTOR,
  DIGITAL_BAND_REJECT_FILTER,
  DIGITAL_LOW_CUT_FILTER,
  END_OF_GROUP,
  FIXED_GAIN,
  HIGH_CUT_FILTER,
  LINE_ID,
  LOW_CUT_FILTER,
  NOTCH_FREQUENCY,
  POLARITY,
  RAW_RECORD,
  RECEIVER,
  RECEIVER_GEOMETRY,
  RECEIVER_LOCATION,
  RECEIVER_SPECS,
  RECEIVER_STATION_NUMBER,
  SAMPLE_INTERVAL,
  SHOT_SEQUENCE_NUMBER,
  SKEW,
  SOURCE,
  SOURCE_LOCATION,
  SOURCE_STATION_NUMBER,
  STACK,
  STATIC_CORRECTIONS,
  TRACE_TYPE,
  TD_NOTE,
  MAX_TD_KEY_INDEX = TD_NOTE,
  NUM_OF_TD_KEYWORDS,
} TD_KEY_INDICES ;


/*********************************/
/*  SEG2 File Descriptor Block.  */
/*********************************/
typedef struct Seg2FileDescriptor {
  short int      block_ID;             /* 0x3A55: native, 0x553A: swapped */
  short int      revisionNum;          /*  */
  short int      tracePtrSubBlockSize; /*  */
  short int      numOfTraces;          /*  */
  char           strTermSize;          /*  */
  char           strTerm1;             /*  */
  char           strTerm2;             /*  */
  char           strTerm[3];           /* a string which combines strTerm1 and, if used, strTerm2. */
  char           lineTermSize;         /*  */
  char           lineTerm1;            /*  */
  char           lineTerm2;            /*  */
  /* bytes 14 - 31 are reserved and unused (where the first byte is 0). */
  int           *trace_ptrs;           /*  */
  char          *strings;              /*  */
  /* The rest of this structure is not in the SEG2 specification */
  unsigned int   charsIn_strings;      /* Size of the strings array. */
  int            unknown_keys_count;   /* count of key/value pairs whose keys didn't match known keys     */
  KEY_VAL_PAIR  *known_keys[NUM_OF_FD_KEYWORDS];
  KEY_VAL_PAIR **unknown_keys;         /* array size TBD and allocated when strings are parsed. */
  /* The strings[] array is parsed out using the SEG2 specification's
   * formatting rules, and each extracted keyword and value pair is stored
   * in a KEY_VAL_PAIR structure.  These pairs are then grouped into either
   * the known_keys[] or unknown_keys[], array based on whether each pair's
   * keyword was matched to one of the keywords in the array
   * file_desc_keywords->keywords[].
   */
} SEG2FILEDESC;


/**********************************/
/*  SEG2 Trace Descriptor Block.  */
/**********************************/
typedef struct Seg2TraceDescriptor {
  short int      block_ID;           /* 0x3A55: native, 0x553A: swapped */
  short int      traceDescBlockSize; /*  */
  int            traceDataBlockSize; /*  */
  int            numOfSamples;       /* NS: Number of Samples in Data Block */
  char           dataFormatCode;     /*  */
  /* bytes 13 - 31 are reserved and unused                      *
   * (where the first byte of the trace descriptor block is 0). */
  char          *strings;            /*  */
  /* The rest of this structure is not in the SEG2 specification */
  unsigned int   charsIn_strings;    /* Size of the strings array. */
  int            unknown_keys_count; /* count of key/value pairs whose keys didn't match known keys     */
  KEY_VAL_PAIR  *known_keys[NUM_OF_TD_KEYWORDS];
  KEY_VAL_PAIR **unknown_keys;       /* array size TBD and allocated when strings are parsed. */
  /* The strings[] array is parsed out using the SEG2 specification's
   * formatting rules, and each extracted keyword and value pair is stored
   * in a KEY_VAL_PAIR structure.  These pairs are then grouped into either
   * the known_keys[] or unknown_keys[], array based on whether each pair's
   * keyword was matched to one of the keywords in the array
   * trace_desc_keywords->keywords[].
   */
} SEG2TRACEDESC;

#endif

