/* 
 *   THIS FILE IS UNDER CVS. 
 *   DO NOT MODIFY UNLESS YOU HAVE CHECKED IT OUT.
 *
 *    $Id: seg2_strings.c 126 2005-04-08 16:32:03Z dietz $ 
 * 
 *    Revision history: 
 *     $Log$
 *     Revision 1.1  2005/04/08 16:32:03  dietz
 *     Initial version. SEG2 file readin code based on UCB's seg2toms code.
 * 
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <ctype.h>
#include "seg2_struct.h"
#include "procs.h"

/************************************************************************ 
 *  init_file_and_trace_desc_keywords() Initialize structs for reading  * 
 *     SEG2 file and trace descriptors                                  * 
 ************************************************************************/
int init_file_and_trace_desc_keywords( SEG2_KEYWORDS **fdk, 
                                       SEG2_KEYWORDS **tdk )
{
  SEG2_KEYWORDS *file_desc_keywords  = NULL;
  SEG2_KEYWORDS *trace_desc_keywords = NULL;

  /*** Allocate space for and assign values to file_desc_keywords ***/
  file_desc_keywords = (SEG2_KEYWORDS *)malloc(sizeof(SEG2_KEYWORDS));
  if (file_desc_keywords == NULL) {
    fprintf (stderr, "seg2toew error: init of keywords unable to malloc space for file_desc_keywords.\n");
    return (1);
  }

  file_desc_keywords->keywords = (char **)malloc(sizeof(char *) * NUM_OF_FD_KEYWORDS);
  if (file_desc_keywords->keywords == NULL) {
    fprintf (stderr,
             "seg2toew error: init of keywords unable to malloc space for file_desc_keywords->keywords, char * array of %d ptrs.\n",
             NUM_OF_FD_KEYWORDS);
    return (1);
  }

  file_desc_keywords->current_index = 0;
  file_desc_keywords->max_index = MAX_FD_KEY_INDEX;
  file_desc_keywords->keywords[ACQUISITION_DATE]            = "ACQUISITION_DATE";
  file_desc_keywords->keywords[ACQUISITION_SECOND_FRACTION] = "ACQUISITION_SECOND_FRACTION";
  file_desc_keywords->keywords[ACQUISITION_TIME]            = "ACQUISITION_TIME";
  file_desc_keywords->keywords[CLIENT]                      = "CLIENT";
  file_desc_keywords->keywords[COMPANY]                     = "COMPANY";
  file_desc_keywords->keywords[GENERAL_CONSTANT]            = "GENERAL_CONSTANT";
  file_desc_keywords->keywords[INSTRUMENT]                  = "INSTRUMENT";
  file_desc_keywords->keywords[JOB_ID]                      = "JOB_ID";
  file_desc_keywords->keywords[OBSERVER]                    = "OBSERVER";
  file_desc_keywords->keywords[PROCESSING_DATE]             = "PROCESSING_DATE";
  file_desc_keywords->keywords[PROCESSING_TIME]             = "PROCESSING_TIME";
  file_desc_keywords->keywords[TRACE_SORT]                  = "TRACE_SORT";
  file_desc_keywords->keywords[UNITS]                       = "UNITS";
  file_desc_keywords->keywords[FD_NOTE]                     = "NOTE";

  /*** Allocate space for and assign values to trace_desc_keywords ***/
  trace_desc_keywords =   (SEG2_KEYWORDS *)malloc(sizeof(SEG2_KEYWORDS));
  if (trace_desc_keywords == NULL) {
    fprintf (stderr, "seg2toew error: init of keywords unable to malloc space for trace_desc_keywords.\n");
    return (1);
  }

  trace_desc_keywords->keywords = (char **)malloc(sizeof(char *) * NUM_OF_TD_KEYWORDS);
  if (trace_desc_keywords->keywords == NULL) {
    fprintf (stderr,
             "seg2toew error: init of keywords unable to malloc space for trace_desc_keywords->keywords, char * array of %d ptrs.\n",
             NUM_OF_TD_KEYWORDS);
    return (1);
  }

  trace_desc_keywords->current_index = 0;
  trace_desc_keywords->max_index = MAX_TD_KEY_INDEX;
  trace_desc_keywords->keywords[ALIAS_FILTER]               = "ALIAS_FILTER";
  trace_desc_keywords->keywords[AMPLITUDE_RECOVERY]         = "AMPLITUDE_RECOVERY";
  trace_desc_keywords->keywords[BAND_REJECT_FILTER]         = "BAND_REJECT_FILTER";
  trace_desc_keywords->keywords[CDP_NUMBER]                 = "CDP_NUMBER";
  trace_desc_keywords->keywords[CDP_TRACE]                  = "CDP_TRACE";
  trace_desc_keywords->keywords[CHANNEL_NUMBER]             = "CHANNEL_NUMBER";
  trace_desc_keywords->keywords[DATUM]                      = "DATUM";
  trace_desc_keywords->keywords[DELAY]                      = "DELAY";
  trace_desc_keywords->keywords[DESCALING_FACTOR]           = "DESCALING_FACTOR";
  trace_desc_keywords->keywords[DIGITAL_BAND_REJECT_FILTER] = "DIGITAL_BAND_REJECT_FILTER";
  trace_desc_keywords->keywords[DIGITAL_LOW_CUT_FILTER]     = "DIGITAL_LOW_CUT_FILTER";
  trace_desc_keywords->keywords[END_OF_GROUP]               = "END_OF_GROUP";
  trace_desc_keywords->keywords[FIXED_GAIN]                 = "FIXED_GAIN";
  trace_desc_keywords->keywords[HIGH_CUT_FILTER]            = "HIGH_CUT_FILTE";
  trace_desc_keywords->keywords[LINE_ID]                    = "LINE_ID";
  trace_desc_keywords->keywords[LOW_CUT_FILTER]             = "LOW_CUT_FILTER";
  trace_desc_keywords->keywords[NOTCH_FREQUENCY]            = "NOTCH_FREQUENCY";
  trace_desc_keywords->keywords[POLARITY]                   = "POLARITY";
  trace_desc_keywords->keywords[RAW_RECORD]                 = "RAW_RECORD";
  trace_desc_keywords->keywords[RECEIVER]                   = "RECEIVER";
  trace_desc_keywords->keywords[RECEIVER_GEOMETRY]          = "RECEIVER_GEOMETRY";
  trace_desc_keywords->keywords[RECEIVER_LOCATION]          = "RECEIVER_LOCATION";
  trace_desc_keywords->keywords[RECEIVER_SPECS]             = "RECEIVER_SPECS";
  trace_desc_keywords->keywords[RECEIVER_STATION_NUMBER]    = "RECEIVER_STATION_NUMBER";
  trace_desc_keywords->keywords[SAMPLE_INTERVAL]            = "SAMPLE_INTERVAL";
  trace_desc_keywords->keywords[SHOT_SEQUENCE_NUMBER]       = "SHOT_SEQUENCE_NUMBER";
  trace_desc_keywords->keywords[SKEW]                       = "SKEW";
  trace_desc_keywords->keywords[SOURCE]                     = "SOURCE";
  trace_desc_keywords->keywords[SOURCE_LOCATION]            = "SOURCE_LOCATION";
  trace_desc_keywords->keywords[SOURCE_STATION_NUMBER]      = "SOURCE_STATION_NUMBER";
  trace_desc_keywords->keywords[STACK]                      = "STACK";
  trace_desc_keywords->keywords[STATIC_CORRECTIONS]         = "STATIC_CORRECTIONS";
  trace_desc_keywords->keywords[TRACE_TYPE]                 = "TRACE_TYPE";
  trace_desc_keywords->keywords[TD_NOTE]                    = "NOTE";

  *fdk = file_desc_keywords;
  *tdk = trace_desc_keywords;

  return (0);
}


/************************************************************************ 
 *  get_key_val_pair() parses the strings of the SEG2 file and trace    * 
 *    descriptors                                                       * 
 ************************************************************************/
KEY_VAL_PAIR * get_key_val_pair(SEG2FILEDESC *file_desc,
                                char         *strings,
                                int           index,
                                int          *chars_to_next_pair)
{
  KEY_VAL_PAIR *key_val = NULL;
  short int block_ID    = file_desc->block_ID;
  char     *strTerm     = file_desc->strTerm;
  char      strTermSize = file_desc->strTermSize;
  short int offset;
  int       index_to_next;
  int       max_str_length;
  char     *key;
  char     *val;
  int       key_length = 0;
  int       val_length = 0;
  char      key_name_violation = 0; /* Set to 1 if the keyword has any lowercase letters */

  if( block_ID == NATIVE_BYTEORDER_ID ) {
     memcpy( &offset, &(strings[index]), 2 );
  }
  else {
     char tmp[2];
     tmp[0] = strings[index+1];
     tmp[1] = strings[index];
     memcpy( &offset, tmp, 2 );
  }
  *chars_to_next_pair = (int) offset;
  if (offset == 0) { /* This is the last string and it is empty.  Return immediately. */
    return key_val;
  }

  index_to_next = index + offset;
  max_str_length = offset - 2 - strTermSize; /* (2 == # of chars in offset) */
  key = (char *)malloc(sizeof(char) * max_str_length); /* Max possible array length, until actual length is known. */
  val = (char *)malloc(sizeof(char) * max_str_length); /* Max possible array length, until actual length is known. */

  index += 2; /* skip past the offset, which was just read */

  /* Read the keyword until the space or tab (which seperates keyword from value) is found */
  while ( (strings[index] != ' ' ) &&
          (strings[index] != '\t') &&
          (! end_of_string(strings, index, strTerm, strTermSize)) &&
          (index < index_to_next) ) {
    if (islower(strings[index]))  key_name_violation = 1;
    key[key_length++] = strings[index++];
  }

  if (key_name_violation) {
    /* This is just a warning, but won't prevent processing of the data */
    fprintf(stderr,
            "SEG-2 strings format WARNING: keyword (%s) should not contain lowercase (but will still be processed).\n",
            key);
  }

  /* Skip the blanks and tabs until the value starts */
  while ( ((strings[index] == ' ' ) ||
           (strings[index] == '\t')  ) &&
          (! end_of_string(strings, index, strTerm, strTermSize)) &&
          (index < index_to_next) ) {
    index++;
  }

  /* Get the value until the string terminator or the index reaches the next string */
  while ( (! end_of_string(strings, index, strTerm, strTermSize)) &&
          (index < index_to_next) ) {
    val[val_length++] = strings[index++];
  }

  /* Allocate memory for the KEY_VAL_PAIR structure */
  key_val = (KEY_VAL_PAIR *)malloc(sizeof(KEY_VAL_PAIR));
  if (key_val == NULL) {
    fprintf(stderr,
            "get_key_val_pair: Can't allocate memory block for key_val structure. current keyword: %s\n",
            key);
    return(NULL);
  } 
  /* Allocate memory for the KEY_VAL_PAIR structure's keyword array */
  key_val->key = (char *)malloc(sizeof(char) * (key_length + 1));
  if (key_val->key == NULL) {
    fprintf(stderr,
            "get_key_val_pair: Can't allocate memory block for key_val's key array. current keyword: %s\n",
            key);
    free(key_val);
    return(NULL);
  }
  /* Allocate memory for the KEY_VAL_PAIR structure's value array */
  key_val->val = (char *)malloc(sizeof(char) * (val_length + 1));
  if (key_val->val == NULL) {
    fprintf(stderr,
            "get_key_val_pair: Can't allocate memory block for key_val's val array. current keyword: %s\n",
            key);
    free(key_val->key);
    free(key_val);
    return(NULL);
  }

  /* Copy the keyword and value into the KEY_VAL_PAIR structure */
  strncpy(key_val->key, key, key_length);
  strncpy(key_val->val, val, val_length);
  key_val->key[key_length] = 0;
  key_val->val[val_length] = 0;
  key_val->key_length = key_length;
  key_val->val_length = val_length;

  free(key);
  free(val);

  return(key_val); 
}

char end_of_string(char * strings, int index, char * strTerm, char strTermSize) {
  char returnval = FALSE;

  if ( ((strTermSize == 1) && (strings[index] == strTerm[0])) ||
       ((strTermSize == 2) && (strings[index] == strTerm[0]) && (strings[index+1] == strTerm[1])) )
    returnval = TRUE;

  return returnval;
}

/*******************************************************************************
* FUNCTION: categorize_keys:                                                   *
* INPUTS:   KEY_VAL_PAIR   keys_and_vals[]                                     *
*           int            num_keys                                            *
*           int            key_categories[] (values are filled in by function) *
*           SEG2_KEYWORDS *keywords_list                                       *
* OUTPUTS:  int            key_categories[]                                     *
* RETURNS:  int            unknown_keys_count                                   *
* DESCRIPTION: The key of each KEY_VAL_PAIR in keys_and_vals[], is compared to *
* the key names in the keywords_list[].                                        *
*   If a match is found, then the index of that key in the keyword_list[] is   *
* stored in the key_categories[] array, at the same index as the KEY_VAL_PAIR  *
* being examined in the keys_and_vals[] array.                                 *
*   If a match is not found for the current key/val pair, then its             *
* corresponding entry in key_categories gets the value -1, indicating that the *
* keyword is unknown.                                                          *
*   When this function is done, it returns the number of unknown keys found.   *
*   The calling function will then use all this information to store the       *
* KEY_VALUE_PAIRs into the file descriptor or trace descriptor structure's     *
* parsed_strings known or unknown arrays.  The known arrays can be indexed     *
* with the constants corresponding to the known keywords, to quickly get at    *
* their value without having to search through the strings for a matching      *
* keyword, which would get inefficient if the same set of string is searched   *
* through multiple times.                                                      *
*******************************************************************************/
int categorize_keys (KEY_VAL_PAIR **keys_and_vals,
                     int            num_keys,
                     int           *key_categories,
                     SEG2_KEYWORDS *keywords_list) {
  int  key_val_index;
  int  known_keys_index;
  int  key_search_start_index;
  int  unknown_keys_count = 0; /* count of key/value pairs whose keys didn't match known keys */
  char search_for_key;         /* boolean TRUE or FALSE */
  int  comparison;

  /* First, clear out the category list, flagging all the keys as unknown */
  for (key_val_index = 0; key_val_index < num_keys; key_val_index++) {
    key_categories[key_val_index] = -1;
  }

  /* For each KEY_VAL_PAIR of keys_and_vals[] */
  for (key_val_index = 0; key_val_index < num_keys; key_val_index++) {
    /* set up keyword search parameters */
    search_for_key = TRUE;
    key_search_start_index = known_keys_index = keywords_list->current_index;
  
    /* try to match the key of the KEY_VAL_PAIR to the *
     * known keywords in keywords_list->keywords[].    */
    while (search_for_key == TRUE) {
      comparison = strcmp((keys_and_vals[key_val_index])->key, keywords_list->keywords[known_keys_index]);
      if (comparison == 0) { /* keyword is matched */
        key_categories[key_val_index] = known_keys_index;

        /* The list of known keywords is treated like a circular buffer, ensuring that all  *
         * known keywords present can be found, even if they are not stored alphabetically. */
        known_keys_index = (known_keys_index < keywords_list->max_index) ? known_keys_index + 1 : 0;

        /* The next keyword search will start at the word after the last one found. */
        keywords_list->current_index = known_keys_index;

        search_for_key = FALSE;
      }
      else { /* keyword is not yet matched.  technically they should be *
              * ordered alphabetically, but I won't be that strict.     */
        /* The list of known keywords is treated like a circular buffer, ensuring that all  *
         * known keywords present can be found, even if they are not stored alphabetically. */
        known_keys_index = (known_keys_index < keywords_list->max_index) ? known_keys_index + 1 : 0;

        if (known_keys_index == key_search_start_index) {
          /* All kown keywords have been searched and none matched this one. *
           * Add it to the list of unknown keys, all of which will be added  *
           * to file_desc at the end, when a total count is available.       */
          key_categories[key_val_index] = -1;
          unknown_keys_count++;
          search_for_key = FALSE;
        }
      }
    }
  }

  return (unknown_keys_count);
}




/**********  The function below is not used *************/

/*******************************************************************************
*  get_keyword_val_from_strings:                                               *
*    Compare the begining of each string to the keyword.                       *
*    If the start of the string does not match the keyword,                    *
*      then if the string's start is lexicographically less than the keyword,  *
*        then look at the start of the next string.                            *
*      else if the start of the string is lexicographically greater than the   *
*      keyword,                                                                *
*        then stop searching, because the keys must be ordered alphabetically  *
*        (lexicographically) (unless the keyword is "NOTE", which is supposed  *
*        to be the last of the strings.).                                      *
*      else the start of the string matches the keyword                        *
*        if the next character is a space or tab, then the keyword is found,   *
*        and the rest of the string (after spaces) is returned as the value.   *
*                                                                              * 
*******************************************************************************/
/* Another way to do the following would be to search repetitively for the
 * keyword in the string with 
 *   ptr_to_key_word_match = strstr(strings[index], keyword);
 * and then check if the keyword found was immediately preceded by an
 * offset value, and immediately followed by a space or tab, in which case
 * the keyword would have been found.  If not, increment the index and
 * search again.
 *   But that is an entirely different implementation that I didn't do,
 * and might have it's own hidden issues, such as how to determine if the
 * string contents before the keyword found is a numeric value or something
 * else (although, presumably, everything in the strings section are ASCII
 * characters, so one could use the "isdigit" function).
 */
char get_keyword_val_from_strings(SEG2FILEDESC *file_desc,
                                  char         *strings,
                                  int           index,
                                  char         *keyword,
                                  char         *value,
                                  int          *chars_in_value)
{
  char  returnval    = 0;
  int   start;
  char  strTermSize  = file_desc->strTermSize;
  char  lineTermSize = file_desc->lineTermSize;
/*char  strTerm[strTermSize];   */
/*char  lineTerm[lineTermSize]; */
  char *strTerm;
  char *lineTerm;
  char  offset_str[3];
  int   offset_val;
  int   length_of_key = strlen(keyword);
  char  still_searching = 1;
  int   index_to_strTerm;
  int   index_to_end_of_str;
  char *ptr_to_strTerm;
  signed char comparison; /* result of strncmp: -1, 0, or 1 */

/* Make Windows happy */
   strTerm  = (char *)malloc(strTermSize);
   lineTerm = (char *)malloc(lineTermSize);

  *chars_in_value = 0;

  strTerm[0] = file_desc->strTerm1;
  if (strTermSize == 2)
    strTerm[1] = file_desc->strTerm2;

  lineTerm[0] = file_desc->lineTerm1;
  if (lineTermSize == 2)
    lineTerm[1] = file_desc->lineTerm2;

  while (still_searching) {
    start = index;

    /* determine the offset to the next string */
    strncpy(offset_str, &strings[start], 2);
    offset_str[2] = '\0';
    offset_val = atoi(offset_str);

    /* Look for the keyword in the strings */
    comparison = strncmp((const char *)strings[index], keyword, length_of_key);
  
    if (comparison > 0) {
      if (strcmp (keyword, "NOTE") == 0) {
        /* The "NOTE" keyword is the only keyword that is not in alphabetical *
         * order, but is instead located at the end of the strings section.   */
        index += offset_val;
      }
      else {
        /* Stop searching, because the keyword is not in the strings: each *
         * string must be stored in alphabetical order of its keyword, and *
         * the current string's keyword is alphabetized after the keyword. */
        still_searching = 0;
        value[0] = '\0';
        returnval = 1;
      }
    }
    else if (comparison < 0) {
      /* This string does not contain the keyword.  Skip to the next string. */
      index += offset_val;
    }
    else { /* The start of the string matches the keyword */
      if ((strings[index + length_of_key] != ' ' ) && (strings[index + length_of_key] != '\t')) {
        /* The start of the string matches the keyword,                  *
         * but it is not followed by a space or tab,                     *
         * so it is not the exact keyword and we need to keep searching. */
        index += offset_val;
      }
      else { /* Looks like the keyword is found! */
        still_searching = 0; /* stop searching */
        index += length_of_key;

        /* Skip past the keyword and following blanks to get to its value */
        while ((strings[index] == ' ' ) || 
               (strings[index] == '\t')) {
          index++;
        }
    
        /* determine if the String Terminator is really at the end of the      *
         * string, as defined by the offset and the size of the terminator, or *
         * if it is actually before the end of the string (perhaps the spacing *
         * for this keyword/value pair is formatted to always be constant,     *
         * despite the value string's actual length), or if--heaven forfend--  *
         * the string Terminator lies beyond the stated length of the string,  *
         * which would be a violation of the SEG2 data format specification.   */
        ptr_to_strTerm      = strstr((const char *)strings[index], strTerm);
        index_to_strTerm    = (ptr_to_strTerm - strings) / sizeof(char*);
        index_to_end_of_str = start + offset_val - strTermSize;
  
        if (index_to_strTerm == index_to_end_of_str) {
          /* the current index position is at the start of the value string. */
          *chars_in_value = index_to_end_of_str - index;
        }
        else if (index_to_strTerm <= index_to_end_of_str) {
          /* The string terminator comes before the end of the string as given by the offset */
          /* Determine the actual number of characters left in the sring */
          *chars_in_value = index_to_strTerm - index;
        }
        else {
          /* The string terminator lies beyond the stated length of the string, *
           * which is a violation of the SEG2 data format specification.        *
           * Get the value up to the end of the string, and print a warning.    */
          *chars_in_value = index_to_end_of_str - index;
          fprintf(stderr,
                  "SEG-2 strings format ERROR:\n str term (%s) at index %d beyond next string offset (%d) from string start (%d).\n",
                  strTerm, index_to_strTerm, offset_val, start);
        }
    
        /* copy the value to the temporary array */
        strncpy(value, (const char *)strings[index], *chars_in_value);
      }
    }
  }

  return returnval;
}

