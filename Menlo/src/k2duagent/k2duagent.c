/*
 * k2duagent.c:
 *
 * Periodically check a specified directory for .evt files.
 * Reads the files, plucks interesting values,
 * and sends them to SeisNetWatch.
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <ctype.h>
#include <math.h>
#include <chron3.h>
#include <transport.h>
#include <errno.h>
#include "sm_file2ew.h"

/* Functions in this source file 
 *******************************/
int get_evt( void );
int get_txt( void );
int read_txt( char *fname, char *Bmsg );
void config_me ( char * );
void sm_file2ew_lookup ( void );
void sm_file2ew_status ( unsigned char, short, char * );
int AddInt( char *aname, char *site, int value );
int AddVal( char *aname, char *site, double value );
int AddTime(char *, char *, double);
int SendTime( char *msg );
int SendOneTime( char *aname, char *type, char *msg );
int SendVal( void );
int SendOneVal( char *aname, char *type, char *outname );
int SendOneInt( char *aname, char *type, char *outname );
int Str2HMS( char *str, int *hour, int *minute, double *seconds );
int EMail( char person[], char *subject, char *msg );
void date22( double, char *);

SHM_INFO  Region;             /* shared memory region to use for i/o    */
MSG_LOGO  SMlogo;              /* outgoing msg logo: module,type,instid  */
pid_t     myPid;              /* for restarts by startstop              */

static char MsgBuf[BUFLEN];   /* char string to hold output message     */

int NchanNames=0;             /* number of names in this config file     */
CHANNELNAME ChannelName[MAXCHAN];  /* table of box/channel to SCNL       */

static char *TroubleSubdir = "trouble";   /* subdir for problem files    */
static char *SaveSubdir = "save";         /* subdir for processed files  */

/* Things to read or derive from configuration file
 **************************************************/
static char     RingName[20];        /* name of transport ring for i/o    */
static char     MyModName[50];       /* speak as this module name/id      */
static int      LogSwitch;           /* 0 if no logfile should be written */
static int      HeartBeatInterval;   /* seconds betweeen beats to statmgr */
static char     GetFromDir[NAM_LEN]; /* directory to monitor for data     */
static char     GetTxtFromDir[NAM_LEN]; /* directory to monitor for data     */
       char     ArchiveDir[NAM_LEN]; /* directory to store known headers  */
       char     HeaderDir[NAM_LEN];  /* directory to store known headers  */
       char     OutputDir[NAM_LEN];  /* directory to put SNW messages     */
static unsigned CheckPeriod;         /* secs between looking for new files*/
static int      OpenTries;
static int      OpenWait;
static int      SaveDataFiles;       /* if non-zero, move to SaveSubdir,  */ 
                                     /*           0, remove files         */
static char     PeerHeartBeatFile[NAM_LEN]; /* name of heartbeat file     */
static int      PeerHeartBeatInterval; /* seconds between heartbeat files */
static int      HistoryScanInterval;  /* seconds between history scans    */
       int      LabelSwitch;         /* 0: net-site 1: site-s/n 2: net-site_s/n*/
       int      ftflag;               /* flag         */
       int      Debug;               /* non-zero -> debug logging         */
       int      nPager;
       char     person[MAX_PAGERS][50];
       char     manager[50];
       char     sitehist[50];
       double   contactime;

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o     */
static unsigned char InstId;        /* local installation id             */
static unsigned char MyModId;       /* Module Id for this program        */
static unsigned char TypeHeartBeat; 
static unsigned char TypeError;
static unsigned char TypeSM;
static unsigned char TypePage;


/* Error messages used by sm_file2ew 
 ***********************************/
#define ERR_CONVERT       0   /* trouble converting a file  */
#define ERR_PEER_LOST     1   /* no peer heartbeat file for a while */
#define ERR_PEER_RESUME   2   /* got a peer heartbeat file again    */
#define BMSG_LEN 30000
#define MAX_HIST 1000

#define TEXT_LEN NAM_LEN*3
static char Text[TEXT_LEN];     /* string for log/error messages      */
static char ProgName[NAM_LEN];  /* program name for logging purposes  */

/* File handling stuff
**********************/
int main( int argc, char **argv )
{
   int         i, ret, read_error = 0;
   char      whoami[50], fname[100], fnew[155], Bmsg[BMSG_LEN];
   char     *c;
   FILE     *fp;
   time_t    tnextbeat;    /* next time for local heartbeat  */
   time_t    tnextpeer;    /* next time for peer's heartbeat */
   time_t    tnexthist;    /* next time for scan of history  */
   time_t    tnow;         /* current time */
   int       peerstatus;   /* current status of peer */


/* Check command line arguments 
 ******************************/
   if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", argv[0] );
        exit( 0 );
   }
   strcpy( ProgName, argv[1] );
   c = strchr( ProgName, '.' );
   if( c ) *c = '\0';
	sprintf(whoami, " %s: %s: ", ProgName, "main");
           
/* Read the configuration file(s)
 ********************************/
   config_me( argv[1] );
   
/* Look up important info from earthworm.h tables
 ************************************************/
   sm_file2ew_lookup();
   
/* Set the outgoing logo fields
 ******************************/
   SMlogo.instid = InstId;
   SMlogo.mod    = MyModId;
   SMlogo.type   = TypeSM;
 
/* Initialize name of log-file & open it 
 ***************************************/
   logit_init( argv[1], (short) MyModId, TEXT_LEN*2, LogSwitch );
   logit( "" , "%s: Read command file <%s>\n", argv[0], argv[1] );

/* Get process ID for heartbeat messages 
 ***************************************/
   myPid = getpid();
   if( myPid == -1 ) {
     logit("e","%s Cannot get pid; exiting!\n", whoami);
     exit (-1);
   }

/* Change to the directory with the input text files
 ***********************************************/
   if( chdir_ew( GetTxtFromDir ) == -1 ) {
      logit( "e", "%s GetTxtFromDir directory <%s> not found; "
                 "exiting!\n", whoami, GetTxtFromDir );
      exit(-1);
   }
   if(Debug)logit("et","%s changed to directory <%s>\n", whoami, GetTxtFromDir);

/* Make sure trouble subdirectory exists
 ***************************************/
   if( CreateDir( TroubleSubdir ) != EW_SUCCESS ) {
      logit( "e", "%s trouble creating trouble directory: %s/%s\n",
              whoami, GetTxtFromDir, TroubleSubdir ); 
      return( -1 );
   }

/* Make sure save subdirectory exists (if it will be used)
 *********************************************************/
   if( SaveDataFiles ) {
      if( CreateDir( SaveSubdir ) != EW_SUCCESS ) {
         logit( "e", "%s trouble creating save directory: %s/%s\n",
                whoami, GetTxtFromDir, SaveSubdir ); 
      return( -1 );
      }
   }

/* Change to the directory with the input files
 ***********************************************/
   if( chdir_ew( GetFromDir ) == -1 ) {
      logit( "e", "%s GetFromDir directory <%s> not found; "
                 "exiting!\n", whoami, GetFromDir );
      exit(-1);
   }
   if(Debug)logit("et","%s changed to directory <%s>\n", whoami, GetFromDir);

/* Make sure trouble subdirectory exists
 ***************************************/
   if( CreateDir( TroubleSubdir ) != EW_SUCCESS ) {
      logit( "e", "%s trouble creating trouble directory: %s/%s\n",
              whoami, GetFromDir, TroubleSubdir ); 
      return( -1 );
   }

/* Make sure save subdirectory exists (if it will be used)
 *********************************************************/
   if( SaveDataFiles ) {
      if( CreateDir( SaveSubdir ) != EW_SUCCESS ) {
         logit( "e", "%s trouble creating save directory: %s/%s\n",
                whoami, GetFromDir, SaveSubdir ); 
      return( -1 );
      }
   }

/* Attach to Output shared memory ring 
 *************************************/
   tport_attach( &Region, RingKey );
   logit( "", "%s Attached to public memory region %s: %d\n", 
          whoami, RingName, RingKey );

/* Force local heartbeat first time thru main loop
   and force history refresh first time thru main loop
   but give peers the full interval before we expect a file
 **********************************************************/
   tnextbeat  = time(NULL) - 1;
   tnexthist  = time(NULL) - 1;
   tnextpeer  = time(NULL) + PeerHeartBeatInterval;
   peerstatus = ERR_PEER_RESUME; 


/****************  top of working loop ********************************/
   while(1)  {
     /* Check on heartbeats
      *********************/
        tnow = time(NULL);
        if( tnow >= tnextbeat ) {  /* time to beat local heart */
           sm_file2ew_status( TypeHeartBeat, 0, "" );
           tnextbeat = tnow + HeartBeatInterval;
        }
        if( PeerHeartBeatInterval &&  tnow > tnextpeer ) {  /* peer is late! */
           if( peerstatus == ERR_PEER_RESUME ) {  /* complain! */
              sprintf(Text,"No PeerHeartBeatFile in %s for over %d sec!",
                      GetFromDir, PeerHeartBeatInterval );
              /*
              sm_file2ew_status( TypeError, ERR_PEER_LOST, Text );
              */
              peerstatus = ERR_PEER_LOST;            
           }
        }

     /* See if termination has been requested 
      ****************************************/
		if( tport_getflag( &Region ) == TERMINATE ) {
           logit( "t", "%s Termination requested; exiting!\n", whoami );
           break;
        }

     /* See if it's time to send the history 
      ****************************************/
        if( tnow >= tnexthist ) {  /* time to check history */
			SendTime( Bmsg );
			SendVal( );
			tnexthist = tnow + HistoryScanInterval;
        }
        
		ret = get_evt();
		if(ret == 1) break;

		ret = get_txt();
		if(ret == 1) break;


   } /* end of while */

/************************ end of working loop ****************************/
    
/* detach from shared memory */
   tport_detach( &Region ); 

/* write a termination msg to log file */
   fflush( stdout );
   return( 0 );

}  
/************************* end of main ***********************************/

/******************************************************************************
 *  get_evt( )   Process .evt headers                                         *
 ******************************************************************************/
int get_evt( void )
{
   int         i, ret;
   char      whoami[50], fname[100], aname[100], fnew[155], Bmsg[BMSG_LEN];
   FILE     *fp;
   time_t    tnextpeer;    /* next time for peer's heartbeat */
   time_t    tnow;         /* current time */
   int       peerstatus;   /* current status of peer */

    sprintf(whoami, " %s: %s: ", ProgName, "get_evt");
/* Change to the directory with the input files
 ***********************************************/
   if( chdir_ew( GetFromDir ) == -1 ) {
      logit( "e", "%s: GetFromDir directory <%s> not found; "
                 "exiting!\n", whoami, GetFromDir );
      exit(-1);
   }
 /*  if(Debug)logit("et","%s: changed to directory <%s>\n", whoami,GetFromDir);  */

     /* Get a file name
      ******************/    
	ret = GetFileName( fname );
	
	sleep_ew( 1*1000 );
    /**/
    if( ret == 1 ) {  /* No files found; wait for one to appear */
           sleep_ew( CheckPeriod*1000 ); 
       return(0);
    }
    /**/
	if(Debug)logit("e","\n%s: got file name <%s>\n",whoami,fname);

     /* Open the file.
      ******************/
     /* We open for write, as that will hopefully get us an exclusive open. 
      * We don't ever want to look at a file that's being written to. 
      */
	for(i=0;i<OpenTries;i++) {
		fp = fopen( fname, "rb" );
		if ( fp != NULL ) goto itopend;
		sleep_ew(OpenWait);
	}
    logit("et","%s: Error: Could not open %s after %d*%d ms\n",
               whoami, OpenTries,OpenWait);
    itopend:    
    if(i>0) logit("t","Warning: %d attempts required to open file %s\n", i,fname);

     /* If it's a heartbeat file, reset tnextpeer and delete the file
      ****************************************************************/
     /* We're not decoding the contents of the file.
      * The idea is that we don't care when the heartbeat was created, 
      * only when we saw it. This prevents 'heartbeats from the past' 
      * from confusing things, but may not be wise... 
      */
        if( strcmp(fname,PeerHeartBeatFile)==0 ) {
            tnextpeer = time(NULL) + PeerHeartBeatInterval;

            if( peerstatus == ERR_PEER_LOST ) {  /* announce resumption */
                sprintf(Text,"Received PeerHeartBeatFile in %s (Peer is alive)",
                        GetFromDir );
                sm_file2ew_status( TypeError, ERR_PEER_RESUME, Text );
                peerstatus = ERR_PEER_RESUME;            
            }
            fclose( fp );
            if( remove( fname ) != 0) {
                logit("et",
                    "%s: Cannot delete heartbeat file <%s>;"
                    " exiting!", whoami, fname );
                    return(1);
            }
			return(0);
        }

        else if( strcmp(fname,"core")==0 ) {
            fclose( fp );
            if( remove( fname ) != 0) {
                logit("et",
                    "%s: Cannot delete core file <%s>;"
                    " exiting!", whoami, fname );
                    return(1);
            }
			return(0);
        }

     /* Pass files to the auditor 
        *************************/
        if(Debug)logit("e","%s: Reading %s \n", whoami, fname );
        
        ftflag = 0;
        ret = nsmp2ew( fp, fname, Bmsg );
        fclose( fp );

     /* Everything went fine...
      *************************/
        if( ret >= 0 ) {  
               
            /* Keep file around */
            if( ret > 0 && SaveDataFiles ) { 
                sprintf(fnew,"%s/%s",SaveSubdir,fname );
                if(Debug)logit("et","%s Saving %s as %s \n", whoami, fname, fnew );
                if( rename( fname, fnew ) != 0 ) {
                    logit( "e", "error moving file to ./%s\n; exiting!", 
                        fnew );
                    return(1);
                } else {
                /*  if(Debug)logit("e","moved to ./%s\n", SaveSubdir );   */
                }
            }

            /* Delete the file */
            else { 
                if(Debug)logit("et","%s Removing %s \n", whoami, fname );
                if( remove( fname ) != 0 ) {
                    logit("e","error deleting file %s; exiting!\n", fname);
                    return(1);
                } else  {
                /*  logit("e","deleted file.\n");   */
                }
            }
            /* Add contact time to archive */
			if(ftflag) {
				sprintf(aname, "%sft", ArchiveDir);
				AddTime(aname, sitehist, contactime);
            }
			else {
				sprintf(aname, "%sdata", ArchiveDir);
				AddTime(aname, sitehist, contactime);
            }
        }

    /* ...or there was trouble! 
     **************************/
        else { 
            logit("e","\n");
            if(Debug)logit("et","%s: Trouble processing: %s \n", whoami, fname );
            sprintf( Text,"Trouble processing: %s ;", fname );
            sm_file2ew_status( TypeError, ERR_CONVERT, Text );
            sprintf(fnew,"%s/%s",TroubleSubdir,fname );
            if( rename( fname, fnew ) != 0 ) {
                logit( "e", " error moving file to ./%s ; exiting!\n", fnew );
				return(1);
            } else {
                logit( "e", " moved to ./%s\n", fnew );
            }
        }
        return(0);
}


/******************************************************************************
 *  get_txt( )   Process .txt files                                           *
 ******************************************************************************/
int get_txt( void )
{
   int         i, ret;
   char      whoami[50], fname[100], aname[100], fnew[155], Bmsg[BMSG_LEN];
   FILE     *fp;
   time_t    tnextpeer;    /* next time for peer's heartbeat */
   time_t    tnow;         /* current time */
   int       peerstatus;   /* current status of peer */

    sprintf(whoami, " %s: %s: ", ProgName, "get_txt");
/* Change to the directory with the input files
 ***********************************************/
   if( chdir_ew( GetTxtFromDir ) == -1 ) {
      logit( "e", "%sGetTxtFromDir directory <%s> not found; "
                 "exiting!\n", whoami, GetTxtFromDir );
      exit(-1);
   }
 /*  if(Debug)logit("e","%schanged to directory <%s>\n", whoami,GetTxtFromDir);  */

     /* Get a file name
      ******************/    
	ret = GetFileName( fname );
	
	sleep_ew( 1*1000 );
    /**/
    if( ret == 1 ) {  /* No files found; wait for one to appear */
           sleep_ew( CheckPeriod*1000 ); 
       return(0);
    }
    /**/
	if(Debug)logit("e","\n****************************************************************\n%s got file name <%s>\n",whoami,fname);
	if(Debug)logit("et","\n");

     /* Open the file.
      ******************/
     /* We open for write, as that will hopefully get us an exclusive open. 
      * We don't ever want to look at a file that's being written to. 
      */
	for(i=0;i<OpenTries;i++) {
		fp = fopen( fname, "rb" );
		if ( fp != NULL ) goto itopend;
		sleep_ew(OpenWait);
	}
    logit("e","%s Error: Could not open %s after %d*%d ms\n",
               whoami, OpenTries,OpenWait);
    itopend:    
    if(i>0) logit("e","Warning: %d attempts required to open file %s\n", i,fname);

     /* If it's a heartbeat file, reset tnextpeer and delete the file
      ****************************************************************/
     /* We're not decoding the contents of the file.
      * The idea is that we don't care when the heartbeat was created, 
      * only when we saw it. This prevents 'heartbeats from the past' 
      * from confusing things, but may not be wise... 
      */
        if( strcmp(fname,PeerHeartBeatFile)==0 ) {
            tnextpeer = time(NULL) + PeerHeartBeatInterval;

            if( peerstatus == ERR_PEER_LOST ) {  /* announce resumption */
                sprintf(Text,"Received PeerHeartBeatFile in %s (Peer is alive)",
                        GetTxtFromDir );
                sm_file2ew_status( TypeError, ERR_PEER_RESUME, Text );
                peerstatus = ERR_PEER_RESUME;            
            }
            fclose( fp );
            if( remove( fname ) != 0) {
                logit("e",
                    "%s Cannot delete heartbeat file <%s>;"
                    " exiting!", whoami, fname );
                    return(1);
            }
			return(0);
        }

        else if( strcmp(fname,"core")==0 ) {
            fclose( fp );
            if( remove( fname ) != 0) {
                logit("e",
                    "%s Cannot delete core file <%s>;"
                    " exiting!", whoami, fname );
                    return(1);
            }
			return(0);
        }

     /* Pass files to the auditor 
        *************************/
        if(Debug)logit("e","%s Reading %s \n", whoami, fname );
        
        fclose( fp );
        ret = read_txt( fname, Bmsg );

     /* Everything went fine...
      *************************/
        if( ret >= 0 ) {  
               
            /* Keep file around */
            if( ret > 0 && SaveDataFiles ) { 
                sprintf(fnew,"%s/%s",SaveSubdir,fname );
                if(Debug)logit("e","%s Saving %s as %s \n", whoami, fname, fnew );
                if( rename( fname, fnew ) != 0 ) {
                    logit( "e", "error moving file to ./%s\n; exiting!", 
                        fnew );
                    return(1);
                } else {
                /*  if(Debug)logit("e","moved to ./%s\n", SaveSubdir );   */
                }
            }

            /* Delete the file */
            else { 
                if(Debug)logit("e","%s: Removing %s \n", whoami, fname );
                if( remove( fname ) != 0 ) {
                    logit("e","error deleting file %s; exiting!\n", fname);
                    return(1);
                } else  {
                /*  logit("e","deleted file.\n");   */
                }
            }
            /* Add contact time to archive */
            if( ret > 0 ) {
				if(strstr(fname, "_CALL") != 0 || strstr(fname, "_call") != 0 || strstr(fname, "call_") != 0) {
					sprintf(aname, "%scall", ArchiveDir);
					AddTime(aname, sitehist, contactime);
				}
				if(strstr(fname, "_STAT") != 0 || strstr(fname, "_stat") != 0 || strstr(fname, "stat_") != 0) {
					sprintf(aname, "%sstat", ArchiveDir);
					AddTime(aname, sitehist, contactime);
				}
            }
        }

    /* ...or there was trouble! 
     **************************/
        else { 
            logit("e","\n");
            if(Debug)logit("e","%s: Trouble processing: %s \n", whoami, fname );
            sprintf( Text,"Trouble processing: %s ;", fname );
            sm_file2ew_status( TypeError, ERR_CONVERT, Text );
            sprintf(fnew,"%s/%s",TroubleSubdir,fname );
            if( rename( fname, fnew ) != 0 ) {
                logit( "e", " error moving file to ./%s ; exiting!\n", fnew );
				return(1);
            } else {
                logit( "e", " moved to ./%s\n", fnew );
            }
        }
        return(0);
}


/******************************************************************************
 * read_txt()  Read and process the .txt file.                                 *
 ******************************************************************************/
int read_txt(char *fname, char *Bmsg )
{
    char     whoami[50], string[150], string2[150], serial[10], stid[10], station[10], aname[100];
    char     month[5], date[5], tstring[20], subject[200], msg[200], c22[30];
    char *cmo[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	char    *com;
	char    *str;
    FILE     *fx;      
    int      i, ii, iii, istat, baud, unknown, troubleflag;  
    unsigned long  decade;
    time_t   tnow;         /* current time */
    
    int      hour, minute;
    double   seconds;
    
    double   val, secs, sex, deltasecs, deltadays, nowt;
    double   sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's        */
                                        /* time 0 and 1970-01-01 00:00:00.0 GMT    */
    struct Greg  g;

    sprintf(whoami, " %s: %s: ", ProgName, "read_txt");
    decade = (365*10+2)*24*60*60;     /*  Kinemetric time starts 10 years after EW time  */
	tnow = time(NULL);
	nowt = tnow;
    
	contactime = time(NULL) - decade;
	strcpy(sitehist, "unknown:");
	logit( "e", "%sProcessing text file: %s  \n", whoami, fname );
	istat = 1;
	troubleflag = 0;

/* Open file for reading
 **********************************/
    if(strstr(fname, ".txt") == 0) {
		logit("e","%s%s not a .txt file. \n", whoami, fname );
		return 0;
    }
    if(strstr(fname, "heartbeat") != 0) {
		if(Debug)logit("e","%s%s is a heartbeat file. \n", whoami, fname );
		return 0;
    }
	iii = k_open( fname ); 
    if( iii == 0 ) {
		logit("e","%s%s cant be opened. (%d) \n", whoami, fname, iii );
		return -1;
    }

	strncpy(stid, &fname[16], 4);
	stid[4] = 0;
	if(stid[3] == '-') stid[3] = 0;
/*	sprintf(sitehist, "NP-%s:", stid);
	sprintf(Bmsg, "%s1:UsageLevel=3\n", sitehist );  */
    if(strstr(fname, "_CALL") != 0 || strstr(fname, "_call") != 0 || strstr(fname, "call_") != 0) {
        while(k_rd()) {       /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */
        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;
            if( k_its("CONNECT") ) {
                baud = k_int();
            }
            else if( k_its("FROM:") ) {
                str = k_str();
            }
            else if( k_its("S/N") ) {
                ii = k_int();
				sprintf(serial, "%d", ii );
            }
            else if( k_its("Stn") ) {
                str = k_str();
                str = k_str();
                if(str) strcpy( station, str );
                for(ii=0;ii<strlen(station);ii++) {
                	if(!isalnum(station[ii])&&!ispunct(station[ii])) station[ii] = 0;
                }
                if(Debug)logit( "e", "%s%s <%s> <%s>  %d\n", 
                         whoami, fname, station, serial,  NchanNames);
                
				unknown = 1;
				for(ii=0;ii<NchanNames;ii++) {
					if(strcmp(serial, ChannelName[ii].box) == 0 && 
					   strcmp(station, ChannelName[ii].sta) == 0) {
						if(Debug)logit( "e", "%sMatch found %d %s %s %s.\n",  
						whoami, ii, serial, ChannelName[ii].box, ChannelName[ii].sta );
						strcpy(stid, ChannelName[ii].sta);
						if(LabelSwitch == 0) 
							sprintf(sitehist, "%s-%s:", ChannelName[ii].net, ChannelName[ii].sta);
						else if(LabelSwitch == 1)
							sprintf(sitehist, "%s-%s:", ChannelName[ii].sta, ChannelName[ii].box);
						else if(LabelSwitch == 2)
							sprintf(sitehist, "%s-%s.%s:", ChannelName[ii].net, ChannelName[ii].sta, ChannelName[ii].box);

						unknown = 0;
					}
				}
				if(!unknown) {
					sprintf(Bmsg, "%s1:UsageLevel=3\n", sitehist );
					if(Debug)logit( "e", "%s%s",  whoami, Bmsg );
                }
				if(unknown) {
					if(Debug)logit( "e", "%sUnknown %s <%s> <%s>\n",  whoami, fname, station, serial );
                }
            }
            else if( k_its("Site") ) {
                str = k_str();
            }
            else if( k_its("Comment:") ) {
                str = k_str();
            }
            else if( k_its("DATE:") ) {
                str = k_str();
                if(str) strcpy( month, str );
                str = k_str();
                if(str) strcpy( date, str );
                g.year = k_int();
                str = k_str();
                if(str) strcpy( tstring, str );
                g.month = 0;
                for(i=0;i<12;i++) {
                	if(strcmp(cmo[i],month)==0) g.month = i+1;
                }
                g.day = ((int)date[0]-48)*10 + ((int)date[1]-48);
                g.hour = ((int)tstring[0]-48)*10 + ((int)tstring[1]-48);
                g.minute = ((int)tstring[3]-48)*10 + ((int)tstring[4]-48);
                sex = ((int)tstring[6]-48)*10 + ((int)tstring[7]-48) + ((int)tstring[9]-48)*0.1;
                secs    = 60.0 * (double) julmin(&g) + sex - sec1970;
				
				contactime = secs - decade;
                
            }
            else if( k_its("MESSAGE(S):\r") ) {
                k_rd();
                str = k_str();
            }
            else if( k_its("MESSAGE(S):") ) {
                logit("e","%s%s \n", whoami, k_com() );
            }
            else if( k_its("NEW") ) {   /*  NEW EVENT(S) RECORDED:  */
                str = k_str();
                if(str) logit("e","%s%s \n", whoami, k_com() );
            }
            else if( k_its("DAILY") ) {
                str = k_str();
                if(str) logit("e","%s%s \n", whoami, k_com() );
            }
            else if( k_its("GPS") ) {   /*  GPS FAILED TO LOCK  */
                str = k_str();
                if(str) logit("e","%s%s \n", whoami, k_com() );
            }
            else if( k_its("BATTERY") ) {  /* BATTERY NOT CHARGING  */
                str = k_str();
                if(str) logit("e","%s%s \n", whoami, k_com() );
            }
         /* Unknown command
          *****************/ 
            else {
                if(Debug)logit( "e", "%s: <%s> Unknown command in <%s>.\n", 
                         whoami, com, fname );
            }
    	}
    }
    else if(strstr(fname, "_STAT") != 0 || strstr(fname, "_stat") != 0 || strstr(fname, "stat_") != 0) {
        while(k_rd()) {       /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */
        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;
            if( k_its("Drive") ) {
                str = k_str();
                i = k_int();
                str = k_str();
                if(strstr(str, "KB") != 0 || strstr(str, "kb") != 0) i = i/1000;
				sprintf(string, "%s1:K2 Disk Drive A Free Space Mb=%d\n", sitehist, i );
				logit("e", "%s", string);
				if((strlen(Bmsg)+strlen(string)+10)<BMSG_LEN) strcat(Bmsg, string);
				sprintf(aname, "%sdisca", ArchiveDir);
				AddInt(aname, sitehist, i);
            }
            else if( k_its("PEM") ) {
                str = k_str();
                i = k_int();
				sprintf(string, "%s1:K2 PEM Banks=%d\n", sitehist, i );
				logit("e", "%s", string);
				if((strlen(Bmsg)+strlen(string)+10)<BMSG_LEN) strcat(Bmsg, string);
				sprintf(aname, "%spem", ArchiveDir);
				AddInt(aname, sitehist, i);
            }
            else if( k_its("Temperature:") ) {
                val = k_val();
				sprintf(string, "%s1:K2 Temperature in Celsius=%f\n", sitehist, val );
				logit("e", "%s", string);
				if((strlen(Bmsg)+strlen(string)+10)<BMSG_LEN) strcat(Bmsg, string);
				sprintf(aname, "%stemp", ArchiveDir);
				AddVal(aname, sitehist, val);
            }
            else if( k_its("Battery:") ) {
                val = k_val();
				sprintf(string, "%s1:K2 Internal Battery Voltage=%f\n", sitehist, val );
				logit("e", "%s", string);
				if((strlen(Bmsg)+strlen(string)+10)<BMSG_LEN) strcat(Bmsg, string);
				sprintf(aname, "%svolt", ArchiveDir);
				AddVal(aname, sitehist, val);
            }
            else if( k_its("Alarm:") ) {
                str = k_str();
            }
            else if( k_its("Acquisition:") ) {
                str = k_str();
            }
            else if( k_its("Events:") ) {
                i = k_int();
				sprintf(string, "%s1:K2 Events=%d\n", sitehist, i );
				logit("e", "%s", string);
				if((strlen(Bmsg)+strlen(string)+10)<BMSG_LEN) strcat(Bmsg, string);
				sprintf(aname, "%sevent", ArchiveDir);
				AddInt(aname, sitehist, i);
            }
            else if( k_its("GPS:") ) {
                str = k_str();
            }
            else if( k_its("Last") ) {
				troubleflag = 0;
                str = k_str();
                str = k_str();
                str = k_str();
                if(str) strcpy( month, str );
                if(strcmp("none",month)!=0) {
					str = k_str();
					if(str) strcpy( date, str );
					g.year = k_int();
					if(g.year < 2000 || g.year > 2030) {
						logit("e","%s: Invalid Last year. %d \n", whoami, g.year );
						troubleflag = 1;
					}
					str = k_str();
					if(str) strcpy( tstring, str );
					g.month = 0;
					for(i=0;i<12;i++) {
						if(strcmp(cmo[i],month)==0) g.month = i+1;
					}
					if(g.month == 0) {
						logit("e","%s: Invalid Last month. %d \n", whoami, g.month );
						troubleflag = 1;
					}
					g.day = ((int)date[0]-48)*10 + ((int)date[1]-48);
					if(g.day < 0 || g.day > 31) {
						logit("e","%s: Invalid Last day. %d \n", whoami, g.day );
						troubleflag = 1;
					}
					g.hour = ((int)tstring[0]-48)*10 + ((int)tstring[1]-48);
					if(g.hour < 0 || g.hour > 24) {
						logit("e","%s: Invalid Last hour. %d \n", whoami, g.hour );
						troubleflag = 1;
					}
					g.minute = ((int)tstring[3]-48)*10 + ((int)tstring[4]-48);
					if(g.minute < 0 || g.minute > 60) {
						logit("e","%s: Invalid Last minute. %d \n", whoami, g.minute );
						troubleflag = 1;
					}
					if(troubleflag) istat = -1;
					else {
						sex = ((int)tstring[6]-48)*10 + ((int)tstring[7]-48) + ((int)tstring[9]-48)*0.1;
						secs    = 60.0 * (double) julmin(&g) + sex - sec1970;
						
						Str2HMS( tstring, &hour, &minute, &seconds );
						g.hour = hour;
						g.minute = minute;
						secs    = 60.0 * (double) julmin(&g) + seconds - sec1970;
						
						sprintf(aname, "%slock", ArchiveDir);
						AddTime(aname, sitehist, secs);
						
						sprintf(string2, "%.2d/%.2d/%4d %.2d:%.2d:%05.2f", 
									  g.month, g.day, g.year, g.hour, g.minute, seconds);
						sprintf(string, "%s1:GPS Last Lock=%s\n", sitehist, string2 );
						logit("e", "%s", string);
						if((strlen(Bmsg)+strlen(string)+10)<BMSG_LEN) strcat(Bmsg, string);
						
						deltasecs = nowt - secs;
						deltadays = deltasecs/(60*60);
						sprintf(string, "%s1:K2 Hours Since Last GPS Lock=%.2f\n", sitehist, deltadays );
						logit("e", "%s", string);
						if((strlen(Bmsg)+strlen(string)+10)<BMSG_LEN) strcat(Bmsg, string);
					}
				}
            }
            else if( k_its("Current") ) {
				troubleflag = 0;
                str = k_str();
                str = k_str();
                if(str) strcpy( month, str );
                str = k_str();
                if(str) strcpy( date, str );
                g.year = k_int();
				if(g.year < 2000 || g.year > 2030) {
					logit("e","%s: Invalid Current year. %d \n", whoami, g.year );
					troubleflag = 1;
				}
                str = k_str();
                if(str) strcpy( tstring, str );
                g.month = 0;
                for(i=0;i<12;i++) {
                	if(strcmp(cmo[i],month)==0) g.month = i+1;
                }
				if(g.month == 0) {
					logit("e","%s: Invalid Current month. %d \n", whoami, g.month );
					troubleflag = 1;
				}
                g.day = ((int)date[0]-48)*10 + ((int)date[1]-48);
				if(g.day < 0 || g.day > 31) {
					logit("e","%s: Invalid Current day. %d \n", whoami, g.day );
					troubleflag = 1;
				}
                g.hour = ((int)tstring[0]-48)*10 + ((int)tstring[1]-48);
				if(g.hour < 0 || g.hour > 24) {
					logit("e","%s: Invalid Current hour. %d \n", whoami, g.hour );
					troubleflag = 1;
				}
                g.minute = ((int)tstring[3]-48)*10 + ((int)tstring[4]-48);
				if(g.minute < 0 || g.minute > 60) {
					logit("e","%s: Invalid Current minute. %d \n", whoami, g.minute );
					troubleflag = 1;
				}
				if(troubleflag) istat = -1;
				else {
					sex = ((int)tstring[6]-48)*10 + ((int)tstring[7]-48) + ((int)tstring[9]-48)*0.1;
					secs    = 60.0 * (double) julmin(&g) + sex - sec1970;
					
					if(g.year == 1980) {
						sprintf(subject, "Bad GPS date in %s ", fname);
						sprintf(msg, "Bad GPS date in %s ", fname);
						for(ii=0;ii<nPager;ii++) EMail( person[ii], subject, msg );
						if(Debug)logit( "e", "%s%s  \n", whoami, subject);
					}
					
					contactime = secs - decade;
					if(Debug)logit( "e", "%s<%s> %d <%s> %d %d %s %d  %d \n", 
							 whoami, month, g.month, date, g.day, g.year, tstring, g.hour, g.minute );
                
				}
            }
            else if( k_its("Restart") ) {
                str = k_str();
                if(strcmp(str,"Counts:")==0) {
					i = k_int();
					sprintf(string, "%s1:K2 Restart Counts=%d\n", sitehist, i );
					logit("e", "%s", string);
					if((strlen(Bmsg)+strlen(string)+10)<BMSG_LEN) strcat(Bmsg, string);
					sprintf(aname, "%srestarts", ArchiveDir);
					AddInt(aname, sitehist, i);
                }
                if(strcmp(str,"Time:")==0) {
					troubleflag = 0;
					str = k_str();
					if(str) strcpy( month, str );
					if(strcmp("none",month)!=0) {
						str = k_str();
						if(str) strcpy( date, str );
						g.year = k_int();
						if(g.year < 1980 || g.year > 2030) {
							logit("e","%s: Invalid Restart year. %d \n", whoami, g.year );
							troubleflag = 1;
						}
						str = k_str();
						if(str) strcpy( tstring, str );
						g.month = 0;
						for(i=0;i<12;i++) {
							if(strcmp(cmo[i],month)==0) g.month = i+1;
						}
						if(g.month == 0) {
							logit("e","%s: Invalid Restart month. %d \n", whoami, g.month );
							troubleflag = 1;
						}
						g.day = ((int)date[0]-48)*10 + ((int)date[1]-48);
						if(g.day < 0 || g.day > 31) {
							logit("e","%s: Invalid Restart day. %d \n", whoami, g.day );
							troubleflag = 1;
						}
						g.hour = ((int)tstring[0]-48)*10 + ((int)tstring[1]-48);
						if(g.hour < 0 || g.hour > 24) {
							logit("e","%s: Invalid Restart hour. %d \n", whoami, g.hour );
							troubleflag = 1;
						}
						g.minute = ((int)tstring[3]-48)*10 + ((int)tstring[4]-48);
						if(g.minute < 0 || g.minute > 60) {
							logit("e","%s: Invalid Restart minute. %d \n", whoami, g.minute );
							troubleflag = 1;
						}
						if(troubleflag) istat = -1;
						else {
							sex = ((int)tstring[6]-48)*10 + ((int)tstring[7]-48) + ((int)tstring[9]-48)*0.1;
							secs    = 60.0 * (double) julmin(&g) + sex - sec1970;
							
							Str2HMS( tstring, &hour, &minute, &seconds );
							g.hour = hour;
							g.minute = minute;
							secs    = 60.0 * (double) julmin(&g) + seconds - sec1970;
							
							sprintf(aname, "%srestarttime", ArchiveDir);
							AddTime(aname, sitehist, secs);
							
							sprintf(string2, "%.2d/%.2d/%4d %.2d:%.2d:%05.2f", 
										  g.month, g.day, g.year, g.hour, g.minute, seconds);
							sprintf(string, "%s1:K2 Restart Time=%s\n", sitehist, string2 );
							logit("e", "%s", string);
							if((strlen(Bmsg)+strlen(string)+10)<BMSG_LEN) strcat(Bmsg, string);
							
							deltasecs = nowt - secs;
							deltadays = deltasecs/(60*60);
							sprintf(string, "%s1:K2 Hours Since Last Restart=%.2f\n", sitehist, deltadays );
							logit("e", "%s", string);
							if((strlen(Bmsg)+strlen(string)+10)<BMSG_LEN) strcat(Bmsg, string);
						}
					}
                
                }
            }
            else if( k_its("Status") ) {
                str = k_str();
                str = k_str();
                str = k_str();
                if(str) strcpy( stid, str );
                str = k_str();
                ii = k_int();
				sprintf(serial, "%d", ii );
                if(Debug)logit( "e", "%s%s <%s> <%s>\n", 
                         whoami, fname, stid, serial );
                
				unknown = 1;
				for(ii=0;ii<NchanNames;ii++) {
					if(strcmp(serial, ChannelName[ii].box) == 0 && 
					   strcmp(stid, ChannelName[ii].sta) == 0) {
						if(Debug)logit( "e", "%sMatch found %d %s %s %s.\n",  
						whoami, ii, serial, ChannelName[ii].box, ChannelName[ii].sta );
						strcpy(stid, ChannelName[ii].sta);
						if(LabelSwitch == 0) 
							sprintf(sitehist, "%s-%s:", ChannelName[ii].net, ChannelName[ii].sta);
						else if(LabelSwitch == 1)
							sprintf(sitehist, "%s-%s:", ChannelName[ii].sta, ChannelName[ii].box);
						else if(LabelSwitch == 2)
							sprintf(sitehist, "%s-%s.%s:", ChannelName[ii].net, ChannelName[ii].sta, ChannelName[ii].box);

						unknown = 0;
					}
				}
				if(!unknown) {
					sprintf(Bmsg, "%s1:UsageLevel=3\n", sitehist );
					if(Debug)logit( "e", "%s%s",  whoami, Bmsg );
                }
				if(unknown) {
					logit( "e", "%sUnknown %s <%s> <%s>\n",  whoami, fname, stid, serial );
					break;
                }
            }
         /* Unknown command
          *****************/ 
            else {
                if(Debug)logit( "e", "%s<%s> Unknown command in <%s>.\n", whoami, com, fname );
                continue;
            }
    	}
    
    }
    else {
		logit( "e", "%sText file <%s> is neither STAT nor CALL.\n", whoami, fname );
		istat = -1;
    }
	if(strcmp(sitehist, "unknown:")==0) {
		logit( "e", "%sText file <%s> has invalid site name.\n", whoami, fname );
		istat = -1;
	}
	date22 (contactime+decade, c22);
	if(istat>0) logit( "e", "%s%s: %s %lf %s \n", whoami, fname,  sitehist, contactime, c22);
        
	iii = k_close();
	if(unknown) istat = -1;
	if(istat>0) {
		sprintf(aname, "%s%s.%s", OutputDir, stid, serial);
		if(Debug) logit("e", "%s Output file: %s   \n\n", whoami, aname);
		fx = fopen( aname, "w" );
		if( fx == NULL ) {
			logit( "e", "%s: trouble opening output file: %s \n",
				  whoami, aname ); 
		} else {
			fwrite(Bmsg, 1, strlen(Bmsg), fx);
			fclose(fx);
		}
	}
    return( istat );
}



/******************************************************************************
 *  config_me() processes command file(s) using kom.c functions;              *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
#define ncommand 16
void config_me( char *configfile )
{
   char     whoami[50], init[ncommand]; /* init flags, one byte for each required command */
   int      nmiss;          /* number of required commands that were missed   */
   char    *com;
   char    *str;
   int      i, n, nfiles, success;
   
    sprintf(whoami, " %s: %s: ", ProgName, "config_me");
/* Set to zero one init flag for each required command 
 *****************************************************/   
   for( i=0; i<ncommand; i++ )  init[i] = 0;
   nPager = 0;
   LabelSwitch = 0;
   HistoryScanInterval = 60*60;

/* Open the main configuration file 
 **********************************/
   nfiles = k_open( configfile ); 
   if ( nfiles == 0 ) {
        fprintf( stderr,
                "%s: Error opening command file <%s>; exiting!\n", 
                 whoami, configfile );
        exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0) {  /* While there are command files open */
        while(k_rd()) {       /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr, 
                          "%s: Error opening command file <%s>; exiting!\n",
                           whoami, &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command 
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("GetFromDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( GetFromDir, str, NAM_LEN );
                init[3] = 1;
            }
  /*4*/     else if( k_its("GetTxtFromDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( GetTxtFromDir, str, NAM_LEN );
                init[4] = 1;
            }
  /*5*/     else if( k_its("CheckPeriod") ) {
                CheckPeriod = k_int();
                init[5] = 1;
            }
  /*6*/     else if( k_its("Debug") ) {
                Debug = k_int();
                init[6] = 1;
            }
  /*7*/     else if( k_its("OpenTries") ) {
                OpenTries = k_int();
                init[7] = 1;
            }
  /*8*/     else if( k_its("OpenWait") ) {
                OpenWait = k_int();
                init[8] = 1;
            }
  /*9*/     else if( k_its("PeerHeartBeatFile") ) {
                str = k_str();
                if(str) strncpy( PeerHeartBeatFile, str, NAM_LEN);
                init[9] = 1;
            }
  /*10*/    else if( k_its("ArchiveDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( ArchiveDir, str, NAM_LEN );
                init[10] = 1;
            }
  /*11*/     else if( k_its("HeaderDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( HeaderDir, str, NAM_LEN );
                init[11] = 1;
            }
  /*12*/     else if( k_its("OutputDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( OutputDir, str, NAM_LEN );
                init[12] = 1;
            }
  /*13*/    else if( k_its("SaveDataFiles") ) {
                SaveDataFiles = k_int();
                init[13] = 1;
            }
  /*14*/    else if( k_its("PeerHeartBeatInterval") ) {
                PeerHeartBeatInterval = k_int();
                init[14] = 1;
            }
  /*15*/    else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_int();
                init[15] = 1;
            }

         /* Get the mappings from box id to SCNL name
          ********************************************/ 
  /*opt*/   else if( k_its("ChannelName") ) {
                if( NchanNames >= MAXCHAN ) {
                    fprintf( stderr, 
                       "sm_reftek2ew: Too many <ChannelName> commands "
                       "in <%s>; max=%d; exiting!\n", 
                        configfile,(int) MAXCHAN );
                    exit( -1 );
                }
         /* Get the box name */
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_VENDOR_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "sm_reftek2ew: box name <%s> too long " 
                                         "in <ChannelName> cmd; exiting!\n", str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].box, str);
                }
         /* Get the channel number */
                ChannelName[NchanNames].chan = k_int();
                if(ChannelName[NchanNames].chan > SM_MAX_CHAN){
                    fprintf(stderr, "sm_reftek2ew: Channel number %d greater "
                                    "than %d in <ChannelName> cmd; exiting\n",
                                     ChannelName[NchanNames].chan,SM_MAX_CHAN);
                    exit(-1);
                }
         /* Get the SCNL name */
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_STA_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "sm_reftek2ew: station name <%s> too long " 
                                         "in <ChannelName> cmd; exiting!\n", str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].sta, str);
                } 
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_COMP_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "sm_reftek2ew: component name <%s> too long "
                                         "in <ChannelName> cmd; exiting!\n", str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].comp, str);
                } 
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_NET_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "sm_reftek2ew: network name <%s> too long "
                                         "in <ChannelName> cmd; exiting!\n", str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].net, str);
                } 
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_LOC_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "sm_reftek2ew: location name <%s> too long "
                                         "in <ChannelName> cmd; exiting!\n", str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].loc, str);
                }
                NchanNames++;
            }         

               /* optional commands */
            else if( k_its("LabelSwitch") ) {
                LabelSwitch = k_int();
            }
            else if( k_its("HistoryScanInterval") ) {
                HistoryScanInterval = k_int();
            }
            else if( k_its("Pager") ) {
                if (nPager >= MAX_PAGERS ) {
                    fprintf(stderr, "%s Too many <Pager> commands in <%s>", 
                             "k2duagent: ", configfile );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAX_PAGERS );
                    exit( -1 );
                }
                if( (long)(str=k_str()) != 0 )  {
                    strcpy(person[nPager++], str);
                }
            }

            else if( k_its("Manager") ) {
                if( (long)(str=k_str()) != 0 )  {
                    strcpy(manager, str);
                }
            }

         /* Unknown command
          *****************/ 
            else {
                fprintf( stderr, "%s: <%s> Unknown command in <%s>.\n", 
                         whoami, com, configfile );
                continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr, 
                       "%s: Bad <%s> command in <%s>; exiting!\n",
                        whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }
   
/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "%s: ERROR, no ", whoami );
       if ( !init[0] )  fprintf( stderr, "<LogFile> "       );
       if ( !init[1] )  fprintf( stderr, "<MyModuleId> "    );
       if ( !init[2] )  fprintf( stderr, "<GetFromDir> "    );
       if ( !init[3] )  fprintf( stderr, "<GetTxtFromDir> "    );
       if ( !init[4] )  fprintf( stderr, "<CheckPeriod> "   );
       if ( !init[5] )  fprintf( stderr, "<Debug> "         );
       if ( !init[6] )  fprintf( stderr, "<OpenTries> "     );
       if ( !init[7] )  fprintf( stderr, "<CheckPeriod> "   );
       if ( !init[8] )  fprintf( stderr, "<OpenWait> "      );
       if ( !init[9] )  fprintf( stderr, "<PeerHeartBeatFile> " );
       if ( !init[10] ) fprintf( stderr, "<ArchiveDir> "        );
       if ( !init[11])  fprintf( stderr, "<HeaderDir> "         );
       if ( !init[12])  fprintf( stderr, "<OutputDir> "         );
       if ( !init[13])  fprintf( stderr, "<SaveDataFiles> "     );
       if ( !init[14])  fprintf( stderr, "<PeerHeartBeatInterval> " );
       if ( !init[15])  fprintf( stderr, "<HeartBeatInterval> " );
       fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }
   
   return;
}

/******************************************************************************
 *  sm_file2ew_lookup( )   Look up important info from earthworm.h tables     *
 ******************************************************************************/
void sm_file2ew_lookup( void )
{
	char        whoami[50];
 
    sprintf(whoami, " %s: %s: ", ProgName, "sm_file2ew_lookup");
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 ) {
        fprintf( stderr,
                "%s  Invalid ring name <%s>; exiting!\n",
                 whoami, RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr, 
              "%s error getting local installation id; exiting!\n", whoami );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr, 
              "%s Invalid module name <%s>; exiting!\n", whoami, MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr, 
              "%s Invalid message type <TYPE_HEARTBEAT>; exiting!\n", whoami );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr, 
              "%s Invalid message type <TYPE_ERROR>; exiting!\n", whoami );
      exit( -1 );
   }
   if ( GetType( "TYPE_STRONGMOTION", &TypeSM ) != 0 ) {
      fprintf( stderr, 
              "%s Invalid message type <TYPE_STRONGMOTION>; exiting!\n", whoami );
      exit( -1 );
   }
   return;
} 

/******************************************************************************
 * sm_file2ew_status() builds a heartbeat or error message & puts it into     *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/
void sm_file2ew_status( unsigned char type, short ierr, char *note )
{
	MSG_LOGO    logo;
	char        whoami[50], msg[256];
	long        size;
	long        t;
 
    sprintf(whoami, " %s: %s: ", ProgName, "sm_file2ew_status");
/* Build the message
 *******************/ 
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
        sprintf( msg, "%ld %ld\n\0", t, myPid);
   }
   else if( type == TypeError )
   {
        sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
        logit( "e", "%s %s\n", whoami, note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */     

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("e","%s  Error sending heartbeat.\n", whoami );
        }
        else if( type == TypeError ) {
           logit("e","%s  Error sending error:%d.\n", whoami, ierr );
        }
   }

   return;
}

/******************************************************************************
 * AddInt(fp)  Add the integer value for this site to the history file.       *
 ******************************************************************************/
int AddInt( char *aname, char *site, int value )
{
	char	whoami[50], string[150], hname[150], hsite[MAX_HIST][50];
    FILE     *fx;      
	int     hval[MAX_HIST];
	int     i, nsites, new, stat, iret;
    
    sprintf(whoami, " %s: %s: ", ProgName, "AddInt");
    if(Debug) logit("e", "\n%s history file: %s   \n", whoami, aname);
    iret = 0;
	if(strcmp(site, "unknown:") == 0) {
		logit( "e", "%s %s is not a valid site %s \n", whoami, site );
		return iret;
	}
	
    fx = fopen( aname, "rb" );
    if( fx == NULL ) {
    	fx = fopen( aname, "w" );
		if( fx == NULL ) {
			logit( "e", "%s trouble opening history file: %s \n", whoami, aname ); 
		} else {
	        sprintf(string, "%s %d\n", site, value);
	    	logit("e", "%s Write data to history file: %s for the first time  \n", whoami, aname);
	    	fprintf(fx, "%s %d\n", site, value );
	    	fclose(fx);
			logit( "e", "%s Created new history file: %s \n", whoami, aname ); 
	        logit("e", "%s %s  \n", whoami, string);
		    
    	}
		iret = 1;
    } else {
    	nsites = 0;
    	/* Read all previous values into array */
    	while(fgets(string, 150, fx ) != NULL && nsites < MAX_HIST) {
    		stat = sscanf(string, "%s %d", hsite[nsites], &hval[nsites]);
			/*
			if(Debug) logit("e", "%s %d %s  %d  \n", whoami, nsites, hsite[nsites], hval[nsites]);
    		*/
    		nsites++;
    	}
		if(Debug) logit("e", "%s nsites: %d entries.  \n", whoami, nsites );
    	
    	/* Scan the array for this site.
    	   Update it's value, or add if site is new */
    	new = 1;
    	for(i=0;i<nsites; i++) {
			if(strcmp(site, hsite[i]) == 0) {
				hval[i] = value;
				new = 0;
				if(Debug) logit("e", "%s entry: %d (%s) updated to %d  \n", whoami, i, site, value );
			}
    	}
    	if(new) {
    		hval[nsites] = value;
	        sprintf(hsite[nsites], "%s", site);
			if(Debug) logit("e", "%s entry: %d (%s) updated to %d  \n", whoami, nsites, site, value );
    		nsites++;
    	}
    	fclose( fx );
    	
    	/* Write the array back to file */
    	fx = fopen( aname, "w" );
		if( fx == NULL ) {
			logit( "e", "%s trouble opening history file: %s \n", whoami, aname ); 
		} else {
            for(i=0;i<nsites; i++) {
				fprintf(fx, "%s %d\n", hsite[i], hval[i] );
            }
	    	fclose(fx);
    	}
    }
	return( iret );
}


/******************************************************************************
 * AddVal(fp)  Add the double value for this site to the history file.        *
 ******************************************************************************/
int AddVal( char *aname, char *site, double value )
{
	char	whoami[50], string[150], hname[150], hsite[MAX_HIST][50];
    FILE     *fx;      
	double	hval[MAX_HIST];
	int     i, nsites, new, stat, iret;
    
    sprintf(whoami, " %s: %s: ", ProgName, "AddVal");
    if(Debug) logit("e", "\n%s history file: %s   \n", whoami, aname);
    iret = 0;
	if(strcmp(site, "unknown:") == 0) {
		logit( "e", "%s %s is not a valid site %s \n", whoami, site );
		return iret;
	}
	
    fx = fopen( aname, "rb" );
    if( fx == NULL ) {
    	fx = fopen( aname, "w" );
		if( fx == NULL ) {
			logit( "e", "%s trouble opening history file: %s \n", whoami, aname ); 
		} else {
	        sprintf(string, "%s %f\n", site, value);
	    	logit("e", "%s Write data to history file: %s for the first time  \n", whoami, aname);
	    	fprintf(fx, "%s %f\n", site, value );
	    	fclose(fx);
			logit( "e", "%s Created new history file: %s \n", whoami, aname ); 
	        logit( "e", "%s %s  \n", whoami, string);
		    
    	}
		iret = 1;
    } else {
    	nsites = 0;
    	/* Read all previous values into array */
    	while(fgets(string, 150, fx ) != NULL && nsites < MAX_HIST) {
    		stat = sscanf(string, "%s %lf", hsite[nsites], &hval[nsites]);
			/*
			if(Debug) logit("e", "%s %d %s  %f  \n", whoami, nsites, hsite[nsites], hval[nsites]);
    		*/
    		nsites++;
    	}
		if(Debug) logit("e", "%s nsites: %d entries.  \n", whoami, nsites );
    	
    	/* Scan the array for this site.
    	   Update it's value, or add if site is new */
    	new = 1;
    	for(i=0;i<nsites; i++) {
			if(strcmp(site, hsite[i]) == 0) {
				hval[i] = value;
				new = 0;
				if(Debug) logit("e", "%s entry: %d (%s) updated to %f  \n", whoami, i, site, value );
			}
    	}
    	if(new) {
    		hval[nsites] = value;
	        sprintf(hsite[nsites], "%s", site);
			if(Debug) logit("e", "%s entry: %d (%s) updated to %f  \n", whoami, nsites, site, value );
    		nsites++;
    	}
    	fclose( fx );
    	
    	/* Write the array back to file */
    	fx = fopen( aname, "w" );
		if( fx == NULL ) {
			logit( "e", "%s trouble opening history file: %s \n", whoami, aname ); 
		} else {
            for(i=0;i<nsites; i++) {
				fprintf(fx, "%s %.1f\n", hsite[i], hval[i] );
            }
	    	fclose(fx);
    	}
    }
	return( iret );
}


/******************************************************************************
 * AddTime(fp)  Add the contact time for this site to the history file.       *
 ******************************************************************************/
int AddTime( char *aname, char *site, double contactime )
{
	char	whoami[50], string[150], hname[150], hsite[MAX_HIST][50], c22[30];
    FILE     *fx;      
	double	htime[MAX_HIST], nowt;
	int     i, nsites, new, stat, iret;
    unsigned long  decade;
    time_t   tnow;         /* current time */
    
    sprintf(whoami, " %s: %s: ", ProgName, "AddTime");
    decade = (365*10+2)*24*60*60;     /*  Kinemetric time starts 10 years after EW time  */
	tnow = time(NULL);
	nowt = tnow - decade;
    if(Debug) logit("e", "\n%s history file: %s   \n", whoami, aname);
    iret = 0;
	if(strcmp(site, "unknown:") == 0) {
		logit( "e", "%s %s is not a valid site %s \n", whoami, site );
		return iret;
	}
	
    if(contactime > nowt) contactime = nowt;
    
    fx = fopen( aname, "rb" );
    if( fx == NULL ) {
    	fx = fopen( aname, "w" );
		if( fx == NULL ) {
			logit( "e", "%s trouble opening history file: %s \n", whoami, aname ); 
		} else {
	        sprintf(string, "%s %f\n", site, contactime);
	    	logit("e", "%s Write data to history file: %s for the first time  \n", whoami, aname);
	    	fprintf(fx, "%s %f\n", site, contactime );
	    	fclose(fx);
			logit( "e", "%s Created new history file: %s \n", whoami, aname ); 
	        logit("e", "%s %s  \n", whoami, string);
		    
    	}
		iret = 1;
    } else {
    	nsites = 0;
    	/* Read all previous values into array */
    	while(fgets(string, 150, fx ) != NULL && nsites < MAX_HIST) {
    		stat = sscanf(string, "%s %lf", hsite[nsites], &htime[nsites]);
			/*
			if(Debug) logit("e", "%s %d %s  %f  \n", whoami, nsites, hsite[nsites], htime[nsites]);
    		*/
    		nsites++;
    	}
		if(Debug) logit("e", "%s nsites: %d entries.  \n", whoami, nsites );
    	
    	/* Scan the array for this site.
    	   Update it's value, or add if site is new */
    	new = 1;
    	for(i=0;i<nsites; i++) {
			if(strcmp(site, hsite[i]) == 0) {
				if(contactime > htime[i]) htime[i] = contactime;
				date22 (contactime+decade, c22);
				new = 0;
				if(Debug) logit("e", "%s entry: %d (%s) updated to %f  %s \n", whoami, i, site, contactime, c22 );
			}
    	}
    	if(new) {
    		htime[nsites] = contactime;
	        sprintf(hsite[nsites], "%s", site);
			date22 (contactime+decade, c22);
			if(Debug) logit("e", "%s entry: %d (%s) updated to %f  %s \n", whoami, nsites, site, contactime, c22 );
    		nsites++;
    	}
    	fclose( fx );
    	
    	/* Write the array back to file */
    	fx = fopen( aname, "w" );
		if( fx == NULL ) {
			logit( "e", "%s trouble opening history file: %s \n", whoami, aname ); 
		} else {
            for(i=0;i<nsites; i++) {
				fprintf(fx, "%s %.1f\n", hsite[i], htime[i] );
            }
	    	fclose(fx);
    	}
    }
    
	if(strstr(aname, "history") == 0) {
		sprintf(hname, "%shistory", ArchiveDir);
		AddTime(hname, site, contactime);
	}
    
	return( iret );
}


/******************************************************************************
 * SendTime(fp)  Sends the time since contact to SeisNetWatch.                *
 ******************************************************************************/
int SendTime( char *msg )
{
	char	 whoami[50], aname[100];
    
    sprintf(whoami, " %s: %s: ", ProgName, "SendTime");
    
    sprintf(aname, "%sft", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneTime(aname, "FT", "ft.snw");
    
    sprintf(aname, "%sdata", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneTime(aname, "Trigger", "trigger.snw");
    
    sprintf(aname, "%scall", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneTime(aname, "Call-In", "callin.snw");
    
    sprintf(aname, "%sstat", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneTime(aname, "Status", "status.snw");
    
    sprintf(aname, "%shistory", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneTime(aname, "Contact", "contact.snw");
    
	return( 0 );
}

/******************************************************************************
 * SendOneTime(fp)  Sends the time since contact to SeisNetWatch.             *
 * aname = ArchiveDir/history                                                 *
 *       = ArchiveDir/ft                                                      *
 *       = ArchiveDir/data                                                    *
 *       = ArchiveDir/call                                                    *
 *       = ArchiveDir/stat                                                    *
 ******************************************************************************/
int SendOneTime( char *aname, char *type, char *outname )
{
	char	 whoami[50], fname[50], string[300], hsite[MAX_HIST][50], c22[30], d22[30];
    FILE     *fx, *fy;      
	double	 htime[MAX_HIST], deltasecs, deltadays, nowt;
	int      i, nsites;
    unsigned long  decade;
    time_t   tnow;         /* current time */
    
    decade = (365*10+2)*24*60*60;     /*  Kinemetric time starts 10 years after EW time  */
	tnow = time(NULL);
	nowt = tnow;
    sprintf(whoami, " %s: %s: ", ProgName, "SendOneTime");
    if(Debug) logit("e", "\n%s history file: %s   \n", whoami, aname);
    fx = fopen( aname, "rb" );
    if( fx == NULL ) {
		logit( "e", "%s trouble opening history file: %s \n", whoami, aname ); 
		return( 1 );
    } else {
		sprintf(fname, "%s%s", OutputDir, outname);
		if(Debug) logit("e", "%s Output file: %s   \n", whoami, fname);
    	fy = fopen( fname, "w" );
		if( fy == NULL ) {
			logit( "e", "%s: trouble opening output file: %s \n", whoami, fname ); 
			fclose(fx);
			return( 1 );
		} else {
			nsites = 0;
			/* Read all previous values into array */
			while(fgets(string, 150, fx ) != NULL && nsites < MAX_HIST) {
				sscanf(string, "%s %lf", hsite[nsites], &htime[nsites]);
				/* if(Debug) logit("e", "%s %d %s  %f  \n", whoami, nsites, hsite[nsites], htime[nsites]); */
				nsites++;
			}
			if(Debug) logit("e", "%s %d sites in history file.   \n", whoami, nsites);
			fclose(fx);
			
		/* Scan the array, building the SNW file */
			sprintf(string, "%s1:UsageLevel=3\n", hsite[0] );
			fwrite(string, 1, strlen(string), fy);
			date22 (nowt, c22);
			for(i=0;i<nsites; i++) {
				date22 (htime[i]+decade, d22);
				deltasecs = nowt - htime[i] - decade;
				deltadays = deltasecs/(60*60*24);
				if(deltadays > 0.0) {
					sprintf(string, "%s2:Days Since Last %s=%.2f;UsageLevel=3\n", hsite[i], type, deltadays );
					if(Debug) logit("e", "%s  %s2:Days Since Last %s=%.2f;UsageLevel=3  %s  %s\n", whoami, hsite[i], type, deltadays, c22, d22);
					fwrite(string, 1, strlen(string), fy);
				}
			}
	    	fclose(fy);
    	}
    }
	return( 0 );
}

/******************************************************************************
 * SendVal(fp)  Sends the stored values to SeisNetWatch.                      *
 ******************************************************************************/
int SendVal( void )
{
	char	 whoami[50], aname[100];
    
    sprintf(whoami, " %s: %s: ", ProgName, "SendVal");
    
    sprintf(aname, "%sdisca", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneInt(aname, "K2 Disk Drive A Free Space Mb=", "disca.snw");
    
    sprintf(aname, "%sclock", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneInt(aname, "K2 Clock Source=", "clock.snw");
    
    sprintf(aname, "%sgpssoh", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneInt(aname, "K2 GPS State of Health=", "gpssoh.snw");
    
    sprintf(aname, "%sgpslock", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneInt(aname, "K2 GPS Lock Failures=", "gpslock.snw");
    
    sprintf(aname, "%stemp", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneVal(aname, "K2 Temperature in Celsius=", "temp.snw");
    
    sprintf(aname, "%svolt", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneVal(aname, "K2 Internal Battery Voltage=", "volt.snw");
    
    sprintf(aname, "%ssysversion", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneVal(aname, "Software Version-System=", "sys.snw");
    
    sprintf(aname, "%sbootversion", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneVal(aname, "Software Version-Boot=", "boot.snw");
    
    sprintf(aname, "%sappversion", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneVal(aname, "Software Version-Application=", "app.snw");
    
    sprintf(aname, "%sdspversion", ArchiveDir);
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    SendOneVal(aname, "Software Version-DSP=", "dsp.snw");
    
	return( 0 );
}

/******************************************************************************
 * SendOneVal(fp)  Sends the stored double to SeisNetWatch.                  *
 * aname = ArchiveDir/temp                                                 *
 *       = ArchiveDir/volt                                                      *
 ******************************************************************************/
int SendOneVal( char *aname, char *type, char *outname )
{
	char	 whoami[50], fname[50], string[300], hsite[MAX_HIST][50];
    FILE     *fx, *fy;      
	double	 hval[MAX_HIST];
	int      i, nsites;
    
    sprintf(whoami, " %s: %s: ", ProgName, "SendOneVal");
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    fx = fopen( aname, "rb" );
    if( fx == NULL ) {
		logit( "e", "%s trouble opening history file: %s \n", whoami, aname ); 
		return( 1 );
    } else {
		sprintf(fname, "%s%s", OutputDir, outname);
		if(Debug) logit("e", "%s Output file: %s   \n", whoami, fname);
    	fy = fopen( fname, "w" );
		if( fy == NULL ) {
			logit( "e", "%s: trouble opening output file: %s \n", whoami, fname ); 
			fclose(fx);
			return( 1 );
		} else {
			nsites = 0;
			/* Read all previous values into array */
			while(fgets(string, 150, fx ) != NULL && nsites < MAX_HIST) {
				sscanf(string, "%s %lf", hsite[nsites], &hval[nsites]);
				/* if(Debug) logit("e", "%s %d %s  %f  \n", whoami, nsites, hsite[nsites], hval[nsites]); */
				nsites++;
			}
			if(Debug) logit("e", "%s %d sites in history file.   \n", whoami, nsites);
			fclose(fx);
			
		/* Scan the array, building the SNW file */
			for(i=0;i<nsites; i++) {
				sprintf(string, "%s2:%s%.2f;UsageLevel=3\n", hsite[i], type, hval[i] );
				if(Debug) logit("e", "%s  %s", whoami, string);
				fwrite(string, 1, strlen(string), fy);
			}
	    	fclose(fy);
    	}
    }
	return( 0 );
}


/******************************************************************************
 * SendOneInt(fp)  Sends the stored double to SeisNetWatch.                   *
 * aname = ArchiveDir/temp                                                    *
 *       = ArchiveDir/volt                                                    *
 ******************************************************************************/
int SendOneInt( char *aname, char *type, char *outname )
{
	char	 whoami[50], fname[50], string[300], hsite[MAX_HIST][50];
    FILE     *fx, *fy;      
	int      i, nsites, hval[MAX_HIST];
    
    sprintf(whoami, " %s: %s: ", ProgName, "SendOneInt");
    if(Debug) logit("et", "\n%s history file: %s   \n", whoami, aname);
    fx = fopen( aname, "rb" );
    if( fx == NULL ) {
		logit( "e", "%s trouble opening history file: %s \n", whoami, aname ); 
		return( 1 );
    } else {
		sprintf(fname, "%s%s", OutputDir, outname);
		if(Debug) logit("e", "%s Output file: %s   \n", whoami, fname);
    	fy = fopen( fname, "w" );
		if( fy == NULL ) {
			logit( "e", "%s: trouble opening output file: %s \n", whoami, fname ); 
			fclose(fx);
			return( 1 );
		} else {
			nsites = 0;
			/* Read all previous values into array */
			while(fgets(string, 150, fx ) != NULL && nsites < MAX_HIST) {
				sscanf(string, "%s %d", hsite[nsites], &hval[nsites]);
				/* if(Debug) logit("e", "%s %d %s  %d  \n", whoami, nsites, hsite[nsites], hval[nsites]); */
				nsites++;
			}
			if(Debug) logit("e", "%s %d sites in history file.   \n", whoami, nsites);
			fclose(fx);
			
		/* Scan the array, building the SNW file */
			for(i=0;i<nsites; i++) {
				sprintf(string, "%s2:%s%d;UsageLevel=3\n", hsite[i], type, hval[i] );
				if(Debug) logit("e", "%s  %s", whoami, string);
				fwrite(string, 1, strlen(string), fy);
			}
	    	fclose(fy);
    	}
    }
	return( 0 );
}


/******************************************************************************
 * Str2HMS( )  Sends the stored double to SeisNetWatch.                   *
 * aname = ArchiveDir/temp                                                    *
 *       = ArchiveDir/volt                                                    *
 ******************************************************************************/
int Str2HMS( char *str, int *hour, int *minute, double *seconds )
{
	char	 whoami[50];
	int      i, j, k, kk, hr, min, len;
	double   sec;
    
    sprintf(whoami, " %s: %s: ", ProgName, "Str2HMS");
    if(Debug) logit("e", "\n%s time string: %s   \n", whoami, str);
    
    i = j = k = kk = hr = min = 0;
    len = strlen(str);
    while(i<len && isdigit((int)str[i])) {
    	hr = 10*hr + (int)str[i++] - 48;
    }
    i++;
    while(i<len && isdigit((int)str[i])) {
    	min = 10*min + (int)str[i++] - 48;
    }
    i++;
    while(i<len && isdigit((int)str[i])) {
    	j = 10*j + (int)str[i++] - 48;
    }
    i++;
    while(i<len && isdigit((int)str[i])) {
    	k = 10*k + (int)str[i++] - 48;
    	kk++;
    }
    
    *hour = hr;
    *minute = min;
    *seconds = j + (double)k/pow(10.0,(double)kk);
    if(Debug) logit("e", "%s time string: %02d:%02d:%06.3f   \n", whoami, *hour, *minute, *seconds);
    
	return( 0 );
}

/******************************************************************************
 * EMail ()
 *   recipient
 *   subject of the message
 *   message to send
 *
 ******************************************************************************/
int EMail( char person[], char *subject, char *msg )
{
   FILE *f;
   char cmnd[100];          /* Command line */
   char *msg_ptr, mailProg[100];	
   int  i, max_char;

   strcpy (mailProg, "/usr/ucb/Mail");

	/* Mail message without a subject */
	if (subject == NULL) {
		(void) sprintf( cmnd, "%s %s", mailProg, person );
	}
	/* Mail message with the given subject */
	else {
		(void) sprintf( cmnd, "%s -s \"%s\" %s", mailProg, subject, person );
	}

	if ( ( f = popen( cmnd, "w" ) ) == NULL ) {
		fprintf( stderr, "sendmail: Can't run command \"%s\"\n", cmnd );
		return( -1 );
	}

	/*  Body of the message  */
	msg_ptr = msg;
	max_char = BMSG_LEN;
	while ( *msg_ptr && max_char--) putc( *msg_ptr++, f );

	pclose( f );
   
   return( 0 );
}


/**********************************************************************
 * date22 : Calculate 22 char date in the form Jan23,1988 12:34 12.21 *
 *          from the julian seconds.  Remember to leave space for the *
 *          string termination (NUL).                                 *
 **********************************************************************/
void date22( double secs, char *c22)
{
    char *cmo[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    struct Greg  g;
    long    minute;
    double  sex;

    secs += GSEC1970;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    sprintf(c22, "%3s%2d,%4d %.2d:%.2d:%05.2f",
            cmo[g.month-1], g.day, g.year, g.hour, g.minute, sex);
}




