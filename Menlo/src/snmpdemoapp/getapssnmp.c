#define WIN32
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <string.h>
#include "getapssnmp.h"


int GetApsSnmp( char *peer, SNMP_RESULTS *results )
{
   netsnmp_session session, *ss;   // Contains info about who we're talking to
   netsnmp_pdu *pdu;               // Fill with info we send to the remote host
   netsnmp_pdu *response;          // Sent back from the remote host to us
   netsnmp_variable_list *vars;    // Variables we want to access via SNMP

   oid    anOID[MAX_OID_LEN];      // The location of information we want to retrieve
   size_t anOID_len;
   int    status;
   int    return_code;

/* Initialize the SNMP library
   ***************************/
   init_snmp( "GetApsSnmp" );

/* Initialize a "session" that defines who we're going to
   talk to and what authentication we will be using
   ******************************************************/
   snmp_sess_init( &session );                   // Set up defaults
   session.peername      = strdup( peer );       // Memory allocated here
   session.version       = SNMP_VERSION_1;
   session.community     = "public";
   session.community_len = strlen( session.community );

/* Open the "session" (conversation with SNMP server)
   **************************************************/
   SOCK_STARTUP;
   ss = snmp_open( &session );                   // Allocates session "ss"

   if ( !ss )
   {
      snmp_sess_perror( "GetApsSnmp", &session );
      return_code = CANT_OPEN_SESSION;
      goto ERROR_OPENING_SESSION;
   }

/* Create the PDU packet for our request
   *************************************/
   pdu = snmp_pdu_create( SNMP_MSG_GET );        // Does this allocate "pdu" ???

/* Add requests for parameters of interest.
   Add a null request after each parameter request.
   get_node will fail if the MIB is not found, or if
   the parameter isn't found in the MIB.
   ***********************************************/
   anOID_len = MAX_OID_LEN;
   if ( !get_node( "RPS-SMX5-MIB::ac-Voltage.0", anOID, &anOID_len ) )
   {
      snmp_perror( "get_node failed for RPS-SMX5-MIB::ac-Voltage.0. Exiting." );
      exit( 0 );
   }
   snmp_add_null_var( pdu, anOID, anOID_len );

   anOID_len = MAX_OID_LEN;
   if ( !get_node( "RPS-SMX5-MIB::bus-Voltage.0", anOID, &anOID_len ) )
   {
      snmp_perror( "get_node failed for RPS-SMX5-MIB::bus-Voltage.0. Exiting." );
      exit( 0 );
   }
   snmp_add_null_var( pdu, anOID, anOID_len );

   anOID_len = MAX_OID_LEN;
   if ( !get_node( "RPS-SMX5-MIB::system-Power.0", anOID, &anOID_len ) )
   {
      snmp_perror( "get_node failed for RPS-SMX5-MIB::system-Power.0. Exiting." );
      exit( 0 );
   }
   snmp_add_null_var( pdu, anOID, anOID_len );

   anOID_len = MAX_OID_LEN;
   if ( !get_node( "RPS-SMX5-MIB::load-Power.0", anOID, &anOID_len ) )
   {
      snmp_perror( "get_node failed for RPS-SMX5-MIB::load-Power.0. Exiting." );
      exit( 0 );
   }
   snmp_add_null_var( pdu, anOID, anOID_len );

   anOID_len = MAX_OID_LEN;
   if ( !get_node( "RPS-SMX5-MIB::load-Current.0", anOID, &anOID_len ) )
   {
      snmp_perror( "get_node failed for RPS-SMX5-MIB::load-Current.0. Exiting." );
      exit( 0 );
   }
   snmp_add_null_var( pdu, anOID, anOID_len );

   anOID_len = MAX_OID_LEN;
   if ( !get_node( "RPS-SMX5-MIB::battery-Current.0", anOID, &anOID_len ) )
   {
      snmp_perror( "get_node failed for RPS-SMX5-MIB::battery-Current.0. Exiting." );
      exit( 0 );
   }
   snmp_add_null_var( pdu, anOID, anOID_len );

/* Send request to SNMP server and wait for response
   *************************************************/
   status = snmp_synch_response( ss, pdu, &response );

/* Process the response
   ********************/
   if ( status == STAT_SUCCESS && response->errstat == SNMP_ERR_NOERROR )
   {

/* Success. Get the result variables.
   *********************************/
//    for ( vars = response->variables; vars; vars = vars->next_variable )
//       print_variable( vars->name, vars->name_length, vars );

      vars = response->variables;
      results->acVoltage = *vars->val.integer;
      if ( vars->type != ASN_INTEGER )
      {
         printf( "Error. ac-Voltage.0 is not an integer. Exiting.\n" );
         exit( 0 );
      }

      vars = vars->next_variable;
      results->busVoltage = *vars->val.integer;
      if ( vars->type != ASN_INTEGER )
      {
         printf( "Error. bus-Voltage.0 is not an integer. Exiting.\n" );
         exit( 0 );
      }

      vars = vars->next_variable;
      results->systemPower = *vars->val.integer;
      if ( vars->type != ASN_INTEGER )
      {
         printf( "Error. system-Power.0 is not an integer. Exiting.\n" );
         exit( 0 );
      }

      vars = vars->next_variable;
         results->loadPower = *vars->val.integer;
      if ( vars->type != ASN_INTEGER )
      {
         printf( "Error. load-Power.0 is not an integer. Exiting.\n" );
         exit( 0 );
      }

      vars = vars->next_variable;
      results->loadCurrent = *vars->val.integer;
      if ( vars->type != ASN_INTEGER )
      {
         printf( "Error. load-Current.0 is not an integer. Exiting.\n" );
         exit( 0 );
      }

      vars = vars->next_variable;
      results->batteryCurrent = *vars->val.integer;
      if ( vars->type != ASN_INTEGER )
      {
         printf( "Error. battery-Current.0 is not an integer. Exiting.\n" );
         exit( 0 );
      }
      return_code = COMPLETED_OK;
   }

/* Request failed. Print what went wrong.
   *************************************/
   else
   {
      if ( status == STAT_SUCCESS )
      {
         printf( "Error in packet\n" );
         printf( "Reason: %s\n", snmp_errstring(response->errstat) );
         return_code = ERROR_IN_PACKET;
      }
      else if ( status == STAT_TIMEOUT )
      {
         printf( "Timeout: No response from %s.\n", session.peername );
         return_code = SNMP_TIMEOUT;
      }
      else
      {
         snmp_sess_perror( "GetApsSnmp", ss );
         return_code = UNKNOWN_ERROR;
      }
   }

/* Close session and return
   ************************/
   if ( response )
      snmp_free_pdu( response );
   snmp_close( ss );                // Frees "ss", I think

ERROR_OPENING_SESSION:
   free( session.peername );        // Allocated by strdup above
   SOCK_CLEANUP;
   return return_code;
}
