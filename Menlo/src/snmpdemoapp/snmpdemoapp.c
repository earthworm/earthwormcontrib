#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "getapssnmp.h"


int main( int argc, char *argv[] )
{
   int  i;
   char *peer[] = {"uwave-aps-gp",
                   "uwave-aps-sm",
                   "uwave-aps-tam",
                   "uwave-aps-vp",
                   "uwave-aps-cp",
                   "uwave-aps-fp" };
   SNMP_RESULTS results;
   int npeer = sizeof(peer) / sizeof(char *);

   while ( 1 )
   {
      char datestr[9], timestr[9];
      _strdate( datestr );
      _strtime( timestr );
      printf( "%s %s\n", datestr, timestr );

      for ( i = 0; i < npeer; i++ )
      {
         int rc;

         rc = GetApsSnmp( peer[i], &results );
         if ( rc != COMPLETED_OK )
         {
            printf( "Uh oh! GetApsSnmp returned error: %d\n", rc );
            printf( "Exiting.\n" );
            exit( 0 );
         }

         printf( "%-13s", peer[i] );
         printf( "  AC volts:%d",       results.acVoltage );
         printf( "  DC volts:%.2f",     0.01 * results.busVoltage );
         printf( "  Power percent:%d",  results.systemPower );        // percent
         printf( "  Load power:%.2f",   0.01 * results.loadPower );   // kW
         printf( "  Load amps:%2d",     results.loadCurrent );        // amps
         printf( "  Battery amps:%d\n", results.batteryCurrent );     // amps
      }
         _sleep( 10000 );
   }
}
