
#define COMPLETED_OK       0
#define CANT_OPEN_SESSION -1
#define ERROR_IN_PACKET   -2
#define SNMP_TIMEOUT      -3
#define UNKNOWN_ERROR     -4

typedef struct
{
   int   acVoltage;
   int   busVoltage;     // Units of 0.01 volt
   int   systemPower;
   int   loadPower;      // Units of 0.01 volt
   int   loadCurrent;
   int   batteryCurrent;
} SNMP_RESULTS;

int GetApsSnmp( char *peer, SNMP_RESULTS *results );

