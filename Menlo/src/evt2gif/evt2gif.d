# Configuration file for evt2gif:
# I stuff the trace data from the K2 .evt files into a gif file.
#
# When a file is processed, it is moved from the input directory to the output directory.
# When an error is enountered, the file is moved to the error directory and the
# program continues on to the next file until the input directory is empty.
#
#  Basic Earthworm setup:
#
MyModuleId      MOD_EVT2GIF      # module id for this instance of evt2gif 
InRingName      HYPO_RING        # shared memory ring for input
LogFile         1                # 0 to turn off disk log file; 1 to turn it on
                                 # 2 to log to module log but not to stderr/stdout
HeartBeatInterval    30          # seconds between heartbeats
#
StationList  /home/earthworm/run-jim/params/nsmp.db1
#
# The entries below assign an IRIS-style SCNL name to each active channel
# of each instrument we may hear from. Each instrument has a 'box' identifier
# which is sent in its message (could be a name or a serial number).
# Use "" to enter a NULL string for any of the SCNL fields.
#
# NOTE: legal channel numbers (as of Nov 00) are 0-17
#
#            box  chan   S     C    N    L
#
#ChannelName  436   0     1002  HN2  NP   ""
#
#ChannelName  1990  0     1023  HN2  NP   ""
#
#ChannelName  1031  0     1103  HN2  NP   ""
#
#ChannelName  556   0     1446  HN2  NP   ""
#
@/home/earthworm/run-jim/params/NSMP_Chans.db 
#
#
# Directory where we should look for the pre-chewed ascii K2 files
EVTFilesInDir    /home/luetgert/getfiles/evt2gif
#
# Directory where the successfully processed files are put
EVTFilesOutDir /home/luetgert/getfiles/evt2gif/save
#
# Directory where the problem files are put
EVTFilesErrorDir /home/luetgert/getfiles/evt2gif/trouble
#
NetworkName NP
#
# Debug switch: the token "Debug" (without the quotes) can be stated.
# If it is, lots of weird debug messages will be produced 
# Debug
#
GifDir       /home/luetgert/gifs/evt2gif/           # Directory for temporary .gif files.
LocalTarget  /home/luetgert/sendfiles/netquakes/    # Directory for .gif files for output
#
#
# Plot Display Parameters - 
		# The following is designed such that each SCN creates it's own
		# helicorder display; one per panel of data.
#		
# 01 SecsPerPlot     Total number of seconds per gif image
# 02 XSize           Overall size of plot in inches Setting these > 100 will imply pixels
# 03 YSize           Ysize/channel.
#                                      
#         01    02   03   04  05 06 07 
#
 Display  30  1200  200 
#
#
 Logo    smusgs.gif   # Name of logo in GifDir to be plotted on each image
  UseDST
  
