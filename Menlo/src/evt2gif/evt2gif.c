/*********************************************************************************
 *                evt2gif
 * We read evt files.
 * We read the data and plot them in gif format.
 * 
 * 
 * When we're through with an evt file, we move it to the output directory. 
 * If something went wrong with it, we move it to the error directory and
 * continue. 
 *     Jim Luetgert 3/07
 *********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>  
#include <trace_buf.h>
#include <mem_circ_queue.h>
#include <kom.h>
#include <ws_clientII.h>
#include <swap.h>
#include <k2evt2ew.h> 
#include <rw_strongmotion.h> 
#include "evt2gif.h" 

#define   FILE_NAM_LEN 500
#define   MAXWORD    FILE_NAM_LEN 
#define   MAXCHAN    4000
#define   TRCBUFLTH  1024
#define THREAD_STACK_SIZE 8192

/* Things to read or derive from configuration file
 **************************************************/
static char  InRingName[MAX_RING_STR];  /* name of transport ring for input  */
static char  MyModName[MAX_MOD_STR];    /* speak as this module name/id      */
static int   LogSwitch;                 /* 0 if no logfile should be written */
static long  HeartBeatInterval;         /* seconds between heartbeats        */
SHM_INFO     InRegion;                  /* Input shared memory region info.  */
int GifNameType;            /* Default naming convention for gif file  */
int NchanNames=0;            /* number of names in this config file  */
CHANNELNAME ChannelName[MAXCHAN];  /* table of box/channel to SCNL   */
char   EVTFilesInDir[FILE_NAM_LEN];    /* directory from whence cometh the */
                                       /* evt input files */
char   EVTFilesOutDir[FILE_NAM_LEN];   /* directory to which we put processed files */
char   EVTFilesErrorDir[FILE_NAM_LEN]; /* directory to which we put problem files */
char   outFname[FILE_NAM_LEN];         /* Full name of output file */
char   NetworkName[TRACE2_NET_LEN];     /* Network name. Constant from trace_buf.h */


/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          InRingKey;        /* key of transport ring for input   */
static unsigned char InstId;           /* local installation id             */
static unsigned char MyModId;          /* Module Id for this program        */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char typeWaveform2;    /* Waveform message type.            */
/* The Gloal variables:
***********************/
int    Debug;                /* if non-zero, print debug messages */
pid_t         MyPid;         /* My pid for restart from startstop    */

char   progname[] = "evt2gif";
K2InfoStruct    pk2Info;

time_t MyLastInternalBeat;      /* time of last heartbeat into the local Earthworm ring    */
unsigned  TidHeart;             /* thread id. was type thread_t on Solaris! */
  

/* Other globals
 ***************/
#define   XLMARGIN   1.2             /* Margin to left of axes                  */
#define   XRMARGIN   1.0             /* Margin to right of axes                 */
#define   YBMARGIN   0.8             /* Margin at bottom of axes                */
#define   YTMARGIN   0.9             /* Margin at top of axes plus size of logo */
double    YBMargin = 0.8;            /* Margin at bottom of axes                */
double    YTMargin = 0.7;            /* Margin at top of axes                   */


/* Functions in this source file
 *******************************/
thr_ret Heartbeat( void * );
int DoFile(K2InfoStruct*,long **traceData, Global* );
int evt_audit(long **bytes, K2InfoStruct *pK2Info, Global *But);
int set_parms(Global *But); 
int Build_Axes(Global *But); 
void Pallette(gdImagePtr GIF, long color[]);
int Plot_Trace(Global *But, int this, int *Data, double mint);
void Save_Plot(Global *But);
void CommentList(Global *But);
void Get_Sta_Info(Global *);
void Decode_Time( double, TStrct *);
void Encode_Time( double *secs, TStrct *Time);
void date22( double, char *);
void date11( double, char *);

int config_me(char*, Global *But);
void ew_lookup( void );
void ew_status( unsigned char type, short ierr, char *note );
    Global BStruct;  

/* Functions in this source file
 *******************************/
static	int read_tag (FILE *fp, KFF_TAG *);
static	int read_head (FILE *fp, MW_HEADER *, int tagLength);
static	int read_frame (FILE *fp, FRAME_HEADER *, unsigned long *channels );

/*************************************************************************
 *  main( int argc, char **argv )                                        *
 *************************************************************************/

int main( int argc, char **argv )
{
    char     whoami[50], subname[] = "Main";
    char     fname[FILE_NAM_LEN];    /* name of ascii version of evt file */
    long    *traceBuf[MAX_CHANS_PER_BOX];        /* To store the trace data */
    FILE    *fp;
    char     defaultConfig[] = "evt2gif.d";
    char    *configFileName = (argc > 1 ) ? argv[1] : &defaultConfig[0];
    int      i, j, index, iscan, ichan, ret, tracesize, rc;
    int      numfiles = 0;
    KFF_TAG  tag;
    double   actStarttime, actEndtime;
    time_t now, MyLastGetSta;

    /* Check command line arguments 
     ******************************/
    if (argc != 2) {
        fprintf (stderr, "Usage: evt2gif <configfile>\n");
        exit (EW_FAILURE);
    }
    sprintf(whoami, "%s: %s: ", progname, "Main");

    /* Read config file
     *******************/
    if (config_me (configFileName, &BStruct) != EW_SUCCESS) {
        fprintf (stderr, "%s configure() failed \n", whoami);
        return -1;
    }

	/* Look up important info from earthworm.h tables
	 ************************************************/
	ew_lookup();
    
    /* Set up logiting and log the config file
     **************************************/
	logit_init( argv[1], (short) MyModId, 256, LogSwitch );

/* Get our own process ID for restart purposes
 **********************************************/

	if ((MyPid = getpid ()) == -1) {
		logit ("e", "%s Call to getpid failed. Exiting.\n", whoami);
		exit (-1);
	}

/* Attach to Input shared memory ring
 ************************************/
	tport_attach( &InRegion, InRingKey );
	logit( "", "%s Attached to public memory region %s: %d\n",
	      whoami, InRingName, InRingKey );



    /* Change working directory to "EVTFilesInDir" - where the evt files should be
     ************************************************************************/
    if ( chdir_ew( EVTFilesInDir ) == -1 ) {
        logit( "e", "Error. Can't change working directory to %s\n Exiting.", EVTFilesInDir );
        return -1;
    }

   /* Start the heartbeat thread
   ****************************/
    time(&MyLastInternalBeat); /* initialize our last heartbeat time */
                          
    if ( StartThread( Heartbeat, (unsigned)THREAD_STACK_SIZE, &TidHeart ) == -1 ) {
        logit( "et","%s Error starting Heartbeat thread. Exiting.\n", whoami );
        tport_detach( &InRegion );
        return -1;
    }

	/* sleep for 2 seconds to allow heart to beat so statmgr gets it.  */
	
	sleep_ew(2000);

	Get_Sta_Info(&BStruct);
	CommentList(&BStruct);
	time(&MyLastGetSta);

	logit ("et", "%s Starting main loop\n", whoami);

/*----------------------- setup done; start main loop -------------------------*/

	while(1) {
		
        time(&now);

        /* Beat our heart (into the local Earthworm) if it's time
        ********************************************************/
        if (difftime(now,MyLastGetSta) > 600.0) {
            Get_Sta_Info(&BStruct);
            CommentList(&BStruct);
            time(&MyLastGetSta);
        }
		
		
		
     /* Process all new messages
      **************************/
		
        /* see if a termination has been requested
         *****************************************/
		if ( tport_getflag( &InRegion ) == TERMINATE ) {
	   /* detach from shared memory */
			tport_detach( &InRegion );
	   /* write a termination msg to log file */
			logit( "t", "%s Earthworm global TERMINATE request\n", whoami );
			fflush( stdout );
			break;
		}
		if ( tport_getflag( &InRegion ) == MyPid ) {
	   /* detach from shared memory */
			tport_detach( &InRegion );
	   /* write a termination msg to log file */
			logit( "t", "%s Earthworm pid TERMINATE request\n", whoami );
			fflush( stdout );
			break;
		}

	    /* Start of working loop over files.
	    ************************************/
	    while ((GetFileName (fname)) != 1) {    
	        /* Open the file for reading only.  Since the file name
	        is in the directory we know the file exists, but it
	        may be in use by another process.  If so, fopen_excl()
	        will return NULL.  In this case, wait a second for the
	        other process to finish and try opening the file again.
	        ******************************************************/
			sleep_ew(2000);
	        if ((fp = fopen(fname, "rb" )) == NULL) {
	            logit ("e", "Could not open file <%s>\n", fname);
	            continue;
	        }
	
			else if( strcmp(fname,"core")==0 ) {
				fclose( fp );
				if( remove( fname ) != 0) {
					logit("et", "%s Cannot delete core file <%s>; exiting!", 
						whoami, fname );
					break;
				}
				continue;
			}
	        
	        /* Parse the file header
	        ************************/
	        if ((rc=k2evt2ew (fp, fname, &pk2Info,  (CHANNELNAME *) NULL,
	                                0, NetworkName, Debug )) != EW_SUCCESS) {
	            if(rc == EW_WARNING)
	                logit("e","K2 file was too long.  Data was truncated at %d samples.\n"
	                  " File is valid but truncated.  Skipping this file %s\n", pk2Info.numDataPoints[0],fname);
	            else
	                logit("e","Error reading K2 file %s. Skipping this file\n", fname);

	            /* close the file and move it to the error directory */
	            fclose (fp);

	            sprintf (outFname, "%s/%s", EVTFilesErrorDir, fname);

	            if (rename (fname, outFname) != 0 ) {
	                logit( "e", "Fatal: Error moving %s: %s\n", fname, strerror(errno));
	                return(-1);
	            }
	            continue;
	        } 
	        
	        fclose (fp);

	        logit("et", " *** Processing file %s, Station %s ***\n\n", 
	                                        fname, pk2Info.head.rwParms.misc.stnID);

	        /* Debug: show some file paramers
	        *********************************/
	        if (Debug)
	        {
	            logit("e","StaID: %s\n",
	                        pk2Info.head.rwParms.misc.stnID);

	            logit("e","StartTime: %d (1980) %d (1970) \n", 
	                    pk2Info.head.roParms.stream.startTime, 
	                    pk2Info.head.roParms.stream.startTime  + 
	                             (10*365*24*60*60) + (2*24*60*60) );

	            logit("e","StartMsecTimeMs: %d\n", 
	                    pk2Info.head.roParms.stream.startTimeMsec);

	            logit("e","ClockSource: %d\n",
	                    pk2Info.head.roParms.timing.clockSource);

	            logit("e","GPSstatus: %d\n",
	                    pk2Info.head.roParms.timing.gpsStatus);

	            logit("e","Duration: %f\n", 
	                    pk2Info.head.roParms.stream.duration);
	                    
	            logit("e","Lat: %f\n", 
	                    pk2Info.head.rwParms.misc.latitude);
	            logit("e","Lon: %f\n", 
	                    pk2Info.head.rwParms.misc.longitude);
	            logit("e","Elev: %f\n",
	                    pk2Info.head.rwParms.misc.elevation);

	            logit("e","Nscans: %d\n", 
	                    pk2Info.head.roParms.stream.nscans);

	            logit("e","Nchannels: %d\n", 
	                    pk2Info.head.rwParms.misc.nchannels);

	            logit("e","SerialNumber: %d\n", 
	                    pk2Info.head.rwParms.misc.serialNumber);


	            logit("e","SensorType:");
	            for (i=0; i<pk2Info.head.rwParms.misc.nchannels; i++)    
	                logit("e"," %d",
	                    pk2Info.head.rwParms.channel[i].sensorType);
	            logit("e"," \n");

	            logit("e","Sensitivity:");
	            for(i=0;i<pk2Info.head.rwParms.misc.nchannels;i++)    
	                logit("e"," %f",
	                    pk2Info.head.rwParms.channel[i].sensitivity);
	            logit("e"," \n");

	            logit("e"," FullScale:\n");
	            for(i=0;i<pk2Info.head.rwParms.misc.nchannels;i++)    
	                logit("e"," %f",
	                    pk2Info.head.rwParms.channel[i].fullscale);
	            logit("e"," \n");


	            logit("e"," SensorGain:\n");
	            for(i=0;i<pk2Info.head.rwParms.misc.nchannels;i++)    
	                logit("e"," %f",
	                    pk2Info.head.rwParms.channel[i].gain);
	            logit("e"," \n");
	        }

	        /* Change snippet start time to seconds since 1970
	        **************************************************/
	        pk2Info.head.roParms.stream.startTime = 
	                pk2Info.head.roParms.stream.startTime  + 
	                                (10*365*24*60*60) + (2*24*60*60);

	        /* See if the GPS clock was weird
	        *********************************/
	        if (Debug) {
	            /* Bit 0=1 if currently checking for presence of GPS board
	               Bit 1=1 if GPS board present
	               Bit 2=1 if error communicating with GPS
	               Bit 3=1 if failed to lock within an allotted time (gpsMaxTurnOnTime)
	               Bit 4=1 if not locked
	               Bit 5=1 when GPS power is ON
	               Bits 6,7=undefined
	             */
	            unsigned char badClockBits = 025;     
	            badClockBits = 00;
	            badClockBits &= pk2Info.head.roParms.timing.gpsStatus;
	            if(badClockBits != 0 && pk2Info.head.roParms.timing.clockSource == 3) 
	                logit("e","WARNING: Still looking for GPS board\n");
	            badClockBits = 04;
	            badClockBits &= pk2Info.head.roParms.timing.gpsStatus;
	            if(badClockBits != 0 && pk2Info.head.roParms.timing.clockSource == 3) 
	                logit("e","WARNING: Error communicating with GPS board\n");
	            badClockBits = 010;
	            badClockBits &= pk2Info.head.roParms.timing.gpsStatus;
	            if(badClockBits != 0 && pk2Info.head.roParms.timing.clockSource == 3) 
	                logit("e","WARNING: GPS board failed to lock within allotted time\n");
	            badClockBits = 020;
	            badClockBits &= pk2Info.head.roParms.timing.gpsStatus;
	            if(badClockBits != 0 && pk2Info.head.roParms.timing.clockSource == 3) 
	                logit("e","WARNING: GPS board not locked on\n");
	        }

	        /* Allocate trace buffers
	        *************************/
            for (ichan = 0; ichan < pk2Info.head.rwParms.misc.nchannels; ichan++) {
		        tracesize = pk2Info.numDataPoints[ichan] * sizeof(long); 

                traceBuf[ichan] = (long*)malloc( tracesize );

                if (traceBuf[ichan] == NULL) {
                    logit("e", "Failed to allocate %d bytes for channel %d\n", tracesize, ichan);

                    /* move the file to the error directory */
                    sprintf (outFname, "%s/%s", EVTFilesErrorDir, fname);
    
                    if (rename (fname, outFname) != 0 ) {
                        logit( "e", "Fatal: Error moving %s: %s\n", fname, strerror(errno));
                        return(-1);
                    }
                    continue;
                } 
            }

	        /* Copy the trace data
	        ***********************/
            for (ichan=0; ichan<pk2Info.head.rwParms.misc.nchannels; ichan++) {
                for (index=0; index < pk2Info.numDataPoints[ichan]; index++) {
                    traceBuf[ichan][index] = (long) pk2Info.Counts[ichan][index];
                }
            }

	        /* Process this event
	        *********************/
	        if (DoFile (&pk2Info, traceBuf, &BStruct ) == EW_SUCCESS) {
	            /* all ok; move the file to the output directory */

	            sprintf (outFname, "%s/%s", EVTFilesOutDir, fname);
	            if ( rename( fname, outFname ) != 0 ) {
	                logit( "e", "Error moving %s: %s\n", fname, strerror(errno));
	                return -1;
	            }
	        }
	        else {      /* something blew up on this file. Preserve the evidence */
	            logit("e","Error processing file %s\n",fname);

	            /* move the file to the error directory */
	            sprintf (outFname, "%s/%s", EVTFilesErrorDir, fname);

	            if (rename (fname, outFname) != 0 ) {
	                logit( "e", "Fatal: Error moving %s: %s\n", fname, strerror(errno));
	                return(-1);
	            }
	            continue;
	        }

	        numfiles += 1;

	        for (ichan = 0; ichan < pk2Info.head.rwParms.misc.nchannels; ichan++)
	            free (traceBuf[ichan]);

	    } /* end of  working loop */
	    
	    sleep_ew(5*1000);
	}
    return -1;
}


/***************************** Heartbeat **************************
 *           Send a heartbeat to the transport ring buffer        *
 ******************************************************************/

thr_ret Heartbeat( void *dummy )
{
    time_t now;

   /* once a second, do the rounds.  */
    while ( 1 ) {
        sleep_ew(1000);
        time(&now);

        /* Beat our heart (into the local Earthworm) if it's time
        ********************************************************/
        if (difftime(now,MyLastInternalBeat) > (double)HeartBeatInterval) {
            ew_status( TypeHeartBeat, 0, "" );
            time(&MyLastInternalBeat);
        }
    }
}


/*********************************************************************************
 *                DoFile
 * Given a structure of evt parameters and the trace, turn it into tracebufs
 *
 *********************************************************************************/
int DoFile( K2InfoStruct *pK2Info , long **Trace, Global *But )
{
    char  whoami[90], Chan[10], Net[10], Sta[10], Loc[10];
    int   i, ret, ichan;

    sprintf(whoami, " %s: %s: ", progname, "DoFile");

    if ((pK2Info == NULL) || (Trace == NULL)) {
        logit ("", "Invalid arguments passed in.\n");
        return EW_FAILURE;
    }
	
	/* Read the evt data to get time and channel limits
	*****************************************************/
	evt_audit(Trace, pK2Info, But);
	
	/* Build the axes, labels, etc.
	*****************************************************/
	
	set_parms(But);
	
	if(Build_Axes(But)) {
		logit("e", "%s Build_axes croaked for: %s.%s_%s. \n", whoami, But->Sta.Site, But->Sta.SN, But->Sta.Net);
        return EW_FAILURE;
	}

		
    /* Loop over channels from this evt file
    ****************************************/
    for (ichan=0; ichan<pK2Info->head.rwParms.misc.nchannels; ichan++) {
		
		Plot_Trace(But, ichan, (int*)Trace[ichan], But->Sta.SNCL[ichan].mintime);
				 
    } 
	
	/* Close out the gif image and put it in the output directory
	*****************************************************/
	
	Save_Plot(But);
    
    
    return (EW_SUCCESS);
}



/********************************************************************
 *  evt_audit previews evt file data before plotting                *
int mseed_audit(int *bytes, DATA_HDR *mseed_hdr, Global *But) 
 ********************************************************************/

int evt_audit(long **bytes, K2InfoStruct *pK2Info, Global *But) 
{
    int    i, j;
    char   SNCL[20];
    char   Site[6], SN[6], Net[5], Comp[5], Loc[5];     
    int    ichan, minv, maxv, val;
    double mint, maxt;
		
    /* Loop over channels from this evt file
    ****************************************/
    But->Sta.Nchan = pK2Info->head.rwParms.misc.nchannels;
    for (ichan=0; ichan<But->Sta.Nchan; ichan++) {
		strcpy (Comp, pK2Info->head.rwParms.channel[ichan].id);
	    strcpy (Net,  pK2Info->head.rwParms.channel[ichan].networkcode);
	    strcpy (Loc,  pK2Info->head.rwParms.channel[ichan].locationcode);
		strcpy (Site, pK2Info->head.rwParms.misc.stnID);
		strcpy (But->Sta.SiteName, pK2Info->head.rwParms.misc.comment);
		sprintf(SN, "%d", pK2Info->head.rwParms.misc.serialNumber);
	    for (i=0; i<NchanNames; i++) {
	        if ( strcmp(pK2Info->head.rwParms.misc.stnID, ChannelName[i].sta)==0 &&
	             ichan == ChannelName[i].chan) {
				strcpy (Site, ChannelName[i].sta);
				strcpy (Comp, ChannelName[i].comp);
				strcpy (Net, ChannelName[i].net);
				strcpy (Loc, ChannelName[i].loc);
	        }
	    }
		sprintf(SNCL, "%s_%s_%s_%s", Site, Net, Comp, Loc);
		strcpy(But->Sta.SNCL[ichan].Site,Site);
		strcpy(But->Sta.SNCL[ichan].Net,Net);
		strcpy(But->Sta.SNCL[ichan].Comp,Comp);
		strcpy(But->Sta.SNCL[ichan].Loc,Loc);
		sprintf(But->Sta.SNCL[ichan].SNCLnam, "%s_%s_%s_%s", Site, Net, Comp, Loc);
	    But->Sta.SNCL[ichan].mintime = (double)pK2Info->head.roParms.stream.startTime + 
                   (double)(pK2Info->head.roParms.stream.startTimeMsec)/1000.;
	    But->Sta.SNCL[ichan].maxtime = But->Sta.SNCL[ichan].mintime + 
	                       (pK2Info->head.roParms.stream.duration/10);
	    
		
		val = (int)bytes[ichan][0];
		But->Sta.SNCL[ichan].minval = val;
		But->Sta.SNCL[ichan].maxval = val;
		But->Sta.SNCL[ichan].sigma  = 0.0;
		But->Sta.SNCL[ichan].npts = pK2Info->head.roParms.stream.nscans;
		for(i=0;i<But->Sta.SNCL[ichan].npts;i++) {
			val = (int)bytes[ichan][i];
			if(But->Sta.SNCL[ichan].minval > val) But->Sta.SNCL[ichan].minval = val;
			if(But->Sta.SNCL[ichan].maxval < val) But->Sta.SNCL[ichan].maxval = val;
			But->Sta.SNCL[ichan].sigma += val;
		}
		But->Sta.SNCL[ichan].samprate = (pK2Info->frame.streamPar & 4095);
			
		if(Debug) {
			logit("e", "evt_audit: S_C_N_L: %s  nsamp: %d samprate: %f sigma: %f\n", 
					But->Sta.SNCL[ichan].SNCLnam, But->Sta.SNCL[ichan].npts, 
					But->Sta.SNCL[ichan].samprate, But->Sta.SNCL[ichan].sigma );
			logit("e", "evt_audit: mintime: %f maxtime: %f \n             minval: %d maxval: %d Mean: %f\n", 
					But->Sta.SNCL[ichan].mintime, But->Sta.SNCL[ichan].maxtime, 
					But->Sta.SNCL[ichan].minval, But->Sta.SNCL[ichan].maxval,
					But->Sta.SNCL[ichan].sigma/But->Sta.SNCL[ichan].npts );
		}
	    
	    
        /* Yes; set things up for insertion
        ***********************************/
        if (Debug) logit("e","evt_audit: %s %s %s %s\n\n", Site, Comp, Net, Loc);
    } 
	strcpy(But->Sta.Site,Site);
	strcpy(But->Sta.SN,SN);
	strcpy(But->Sta.Net,Net);

         
	return 0;
} 





/********************************************************************
 *  set_parms sets up values needed for plotting                    *
 ********************************************************************/

int set_parms(Global *But) 
{
    int              i, j;
    
	But->Sta.mintime = But->Sta.maxtime = But->Sta.SNCL[0].mintime;
	for(i=0;i<But->Sta.Nchan;i++) {
		if(But->Sta.mintime > But->Sta.SNCL[i].mintime) 
					But->Sta.mintime = But->Sta.SNCL[i].mintime;
		if(But->Sta.maxtime < But->Sta.SNCL[i].maxtime) 
					But->Sta.maxtime = But->Sta.SNCL[i].maxtime;
		if(But->Sta.minval > But->Sta.SNCL[i].minval) 
					But->Sta.minval = But->Sta.SNCL[i].minval;
		if(But->Sta.maxval < But->Sta.SNCL[i].maxval) 
					But->Sta.maxval = But->Sta.SNCL[i].maxval;
		for(j=0;j<But->NSCN;j++) {
			if(strcmp(But->Chan[j].SCNnam, But->Sta.SNCL[i].SNCLnam)==0) 
					But->Sta.SNCL[i].meta = j;
		}
		But->Sta.SNCL[i].mean = But->Sta.SNCL[i].sigma/But->Sta.SNCL[i].npts;
	}
	
	But->T0 = floor(But->Sta.mintime) - 1.0;
	But->Tn =  ceil(But->Sta.maxtime) + 1.0;
	if((But->Tn - But->T0) <  10.0) But->Tn = But->T0 +  10.0;
	if((But->Tn - But->T0) > 120.0) But->Tn = But->T0 + 120.0;
	
	
	But->xsf = But->xpix/(But->Tn - But->T0);
	But->ysf = (double)But->ypix/(double)(But->Sta.maxval - But->Sta.minval);
	
	if(Debug) {
		logit("e", "%s_%s %15.2f %15.2f %15.2f \n", But->Sta.Site, But->Sta.Net, But->Sta.mintime, But->Sta.maxtime, But->Sta.maxtime - But->Sta.mintime);
		logit("e", "%s_%s %15.2f %15.2f %15.2f \n", But->Sta.Site, But->Sta.Net, But->T0, But->Tn, But->Tn - But->T0);
		logit("e", "%d %d \n", But->Sta.minval, But->Sta.maxval);
		logit("e", "%f %f \n", But->xsf, But->ysf);
	}
	
	for(i=0;i<But->Sta.Nchan;i++) {
		But->Sta.SNCL[i].page.x1 = 0;
		But->Sta.SNCL[i].page.x2 = XLMARGIN*72;
		But->Sta.SNCL[i].page.x3 = XLMARGIN*72 + But->xpix;
		But->Sta.SNCL[i].page.x4 = XLMARGIN*72 + But->xpix + XRMARGIN*72;
		But->Sta.SNCL[i].page.y1 = 0 + i*(YTMargin*72 + But->ypix + YBMargin*72);
		But->Sta.SNCL[i].page.y2 = YTMargin*72 + i*(YTMargin*72 + But->ypix + YBMargin*72);
		But->Sta.SNCL[i].page.y3 = YTMargin*72 + But->ypix + i*(YTMargin*72 + But->ypix + YBMargin*72);
		But->Sta.SNCL[i].page.y4 = YTMargin*72 + But->ypix + YBMargin*72 + i*(YTMargin*72 + But->ypix + YBMargin*72);
		But->Sta.SNCL[i].page.ycen = But->Sta.SNCL[i].page.y2 + But->ypix/2;
		But->Sta.SNCL[i].ysf = (double)But->ypix/(double)(But->Sta.SNCL[i].maxval - But->Sta.SNCL[i].minval);
		if(Debug) {
			logit("e", "x: %d %d %d %d\n", But->Sta.SNCL[i].page.x1, But->Sta.SNCL[i].page.x2, But->Sta.SNCL[i].page.x3, But->Sta.SNCL[i].page.x4);
			logit("e", "y: %d %d %d %d %d\n", But->Sta.SNCL[i].page.y1, But->Sta.SNCL[i].page.y2, But->Sta.SNCL[i].page.ycen, But->Sta.SNCL[i].page.y3, But->Sta.SNCL[i].page.y4);
			logit("e", "mean: %f meta: %d \n", But->Sta.SNCL[i].mean, But->Sta.SNCL[i].meta);
			logit("e", "mean: %f min: %d max: %d \n", But->Sta.SNCL[i].mean, But->Sta.SNCL[i].minval, But->Sta.SNCL[i].maxval);
		}
	}
	
    Decode_Time(But->T0, &But->Stime);
	
	return 0;
}


/********************************************************************
 *    Build_Axes constructs the axes for the plot by drawing the    *
 *    GIF image in memory.                                          *
 *                                                                  *
 ********************************************************************/

int Build_Axes(Global *But) 
{
    char    whoami[90], c22[30], cstr[150];
    int     i, j;
    int     xgpix, ygpix, black, ix, iy, iyc, iy2, metindex;
    double  time, value;
    TStrct  Ts;
    gdImagePtr    im_in;

    sprintf(whoami, " %s: %s: ", progname, "Build_Axes");
    
    But->GifImage = 0L;
                 
    xgpix = 72.0*But->xsize + 8;
    ygpix = 72.0*But->ysize + 8;
    xgpix = But->Sta.SNCL[0].page.x4;
    ygpix = But->Sta.SNCL[But->Sta.Nchan-1].page.y4;

    But->GifImage = gdImageCreate(xgpix, ygpix);
    if(But->GifImage==0) {
        logit("e", "%s Not enough memory! Reduce size of image or increase memory.\n\n", whoami);
        return 2;
    }
    if(But->GifImage->sx != xgpix) {
        logit("e", "%s Not enough memory for entire image! Reduce size of image or increase memory.\n", 
             whoami);
        return 2;
    }
    Pallette(But->GifImage, But->gcolor);
    
    black = But->gcolor[BLACK];
    for(i=0;i<But->Sta.Nchan;i++) {
		gdImageRectangle( But->GifImage, But->Sta.SNCL[i].page.x2, But->Sta.SNCL[i].page.y2, 
										 But->Sta.SNCL[i].page.x3, But->Sta.SNCL[i].page.y3,  black);
		gdImageString(But->GifImage, gdFontMediumBold, But->Sta.SNCL[i].page.x2+100, But->Sta.SNCL[i].page.y2 - 15, But->Sta.SNCL[i].SNCLnam, black);    
		date22 (But->Sta.mintime, c22);
		sprintf(cstr, "%.11s", c22) ;
		gdImageString(But->GifImage, gdFontMediumBold, But->Sta.SNCL[i].page.x2+220, But->Sta.SNCL[i].page.y2 - 15, cstr, black);    
		gdImageString(But->GifImage, gdFontMediumBold, But->Sta.SNCL[i].page.x2+400, But->Sta.SNCL[i].page.y2 - 15, But->Sta.SiteName, black);    
		iy = But->Sta.SNCL[i].page.y2;
		iyc = But->Sta.SNCL[i].page.ycen;
		iy2 = But->Sta.SNCL[i].page.y3;
		time = But->T0;
		while(time <= But->Tn) {
			Decode_Time( time, &Ts);
			sprintf(cstr, "%02d:%02d:%02.0f", Ts.Hour, Ts.Min, Ts.Sec);
			ix = But->Sta.SNCL[i].page.x2 + (time - But->T0)*But->xsf;
			gdImageLine(But->GifImage, ix, iy, ix, iy+5, black);
			gdImageLine(But->GifImage, ix, iy2, ix, iy2-5, black);
			if(fmod(time,10)==0.0) {
				gdImageString(But->GifImage, gdFontMediumBold, ix-25, iy2+15, cstr, black);
				gdImageLine(But->GifImage, ix, iy2-10, ix, iy2+10, black);    
			}
			time += 1.0;
		}
		metindex = But->Sta.SNCL[i].meta;
		ix = But->Sta.SNCL[i].page.x2;
		gdImageLine(But->GifImage, ix-10, iy, ix, iy, black);
		value = (double)(But->Sta.SNCL[i].maxval-But->Sta.SNCL[i].minval)/2.0;
		if(metindex>=0) value = (value)/But->Chan[metindex].sensitivity;  
		sprintf(cstr, "%8.3f", value);
		gdImageString(But->GifImage, gdFontMediumBold, ix-80, iy-5, cstr, black);
		
		gdImageLine(But->GifImage, ix-10, iyc, ix, iyc, black);
		sprintf(cstr, "%8.0f", 0.0);
		gdImageString(But->GifImage, gdFontMediumBold, ix-80, iyc-5, cstr, black);
		
		gdImageLine(But->GifImage, ix-10, iy2, ix, iy2, black);
		sprintf(cstr, "%8.3f", -value);
		gdImageString(But->GifImage, gdFontMediumBold, ix-80, iy2-5, cstr, black);
		sprintf(cstr, "counts");
		if(metindex>=0) {
			if(But->Chan[metindex].Sens_unit == 3) sprintf(cstr, "cm/sec/sec");
			if(But->Chan[metindex].Sens_unit == 2) sprintf(cstr, "cm/sec");
		}
		gdImageString(But->GifImage, gdFontMediumBold, ix-80, iyc-25, cstr, black);
	}
	
	
	return 0;
}


/*******************************************************************************
 *    Pallette defines the pallete to be used for plotting.                    *
 *     PALCOLORS colors are defined.                                           *
 *                                                                             *
 *******************************************************************************/

void Pallette(gdImagePtr GIF, long color[])
{
    color[WHITE]  = gdImageColorAllocate(GIF, 255, 255, 255);
    color[BLACK]  = gdImageColorAllocate(GIF, 0,     0,   0);
    color[RED]    = gdImageColorAllocate(GIF, 255,   0,   0);
    color[BLUE]   = gdImageColorAllocate(GIF, 0,     0, 255);
    color[GREEN]  = gdImageColorAllocate(GIF, 0,   105,   0);
    color[GREY]   = gdImageColorAllocate(GIF, 125, 125, 125);
    color[YELLOW] = gdImageColorAllocate(GIF, 125, 125,   0);
    color[TURQ]   = gdImageColorAllocate(GIF, 0,   255, 255);
    color[PURPLE] = gdImageColorAllocate(GIF, 200,   0, 200);    
    
    gdImageColorTransparent(GIF, -1);
}



/*******************************************************************************
 *    Plot_Trace plots an individual trace (Data)  and stuffs it into          *
 *     the GIF image in memory.                                                *
 *                                                                             *
 *******************************************************************************/

int Plot_Trace(Global *But, int this, int *Data, double mint)
{
    char    whoami[90], string[160];
    double  time;
    double  x0, x, y, xinc, samp_pix, tsize;
    double  in_sec, xsf, ycenter, sf, rms, value, trace_size;
    double  max, min, fudge, Sens_gain;
    int     lastx, lasty, decimation, acquired, ixmin, ixmax, imin, imax, idmin, idmax;
    int     i, j, jj, k, ix, iy, LineNumber, secs, metindex;
    long    black, color, trace_clr;

    sprintf(whoami, " %s: %s: ", progname, "Plot_Trace");
    
    metindex = But->Sta.SNCL[this].meta;
    i = 0;
    secs = But->SecsPerPlot;
    decimation = 1;
    
    imin = imax = But->Sta.SNCL[this].page.ycen - Data[0]*But->ysf;
    idmin = idmax = Data[0];
    ixmin =  9999999999;
    ixmax = -9999999999;
    
    trace_clr = But->gcolor[BLACK];
    acquired = 0;
    for(j=0;j<But->Sta.SNCL[this].npts;j+=decimation) {
        time = mint + j/But->Sta.SNCL[this].samprate;
        
        if (time > But->Tn) break;
        
        ix = But->Sta.SNCL[this].page.x2 + (time - But->T0)*But->xsf;
        iy = But->Sta.SNCL[this].page.ycen - (Data[j]-But->Sta.SNCL[this].mean)*But->ysf;
        iy = But->Sta.SNCL[this].page.ycen - (Data[j]-But->Sta.SNCL[this].mean)*But->Sta.SNCL[this].ysf;
		if(acquired) {
			gdImageLine(But->GifImage, ix, iy, lastx, lasty, trace_clr);
		}
		lastx = ix;  lasty = iy;
		acquired = 1;
		if(imin > iy) imin = iy;
		if(imax < iy) imax = iy;
		if(ixmin > ix) ixmin = ix;
		if(ixmax < ix) ixmax = ix;
		if(idmin > Data[j]) idmin = Data[j];
		if(idmax < Data[j]) idmax = Data[j];
    } 
	if(Debug) 
		logit("e", "%s %s %d %d %d %d %d %d %d %15.2f %15.2f\n", 
		whoami, But->Sta.SNCL[this].SNCLnam, ixmin, ixmax, imin, imax, But->Sta.SNCL[this].npts, idmin, idmax, But->T0, mint);
    
    return 0;
}

/*********************************************************************
 *   Save_Plot()                                                     *
 *    Saves the current version of the GIF image and ships it out.   *
 *********************************************************************/

void Save_Plot(Global *But)
{
    char    tname[175], string[200], whoami[90];
    FILE    *out;
    int     j, ierr, retry;
    
    sprintf(whoami, " %s: %s: ", progname, "Save_Plot");
    /* Make the GIF file. *
     **********************/        
   /*
    sprintf(But->TmpName, "%s_%s.%4d%02d%02d.%02d%02d%02.0f", But->Sta.Site, But->Sta.Net, 
             But->Stime.Year, But->Stime.Month, But->Stime.Day, 
             But->Stime.Hour, But->Stime.Min,   But->Stime.Sec);
             */
    if(GifNameType == 0) {
		sprintf(But->TmpName, "%s_%s.%4d%02d%02d.%02d%02d%02.0f", But->Sta.Site, But->Sta.Net, 
             But->Stime.Year, But->Stime.Month, But->Stime.Day, 
             But->Stime.Hour, But->Stime.Min,   But->Stime.Sec);
    } else if(GifNameType == 1) {
		sprintf(But->TmpName, "%s.%s_%s.%4d%02d%02d.%02d%02d%02.0f", But->Sta.Site, But->Sta.SN, But->Sta.Net, 
             But->Stime.Year, But->Stime.Month, But->Stime.Day, 
             But->Stime.Hour, But->Stime.Min,   But->Stime.Sec);
    } else if(GifNameType >= 2) {
		sprintf(But->TmpName, "%s.%s_%s", But->Sta.Site, But->Sta.SN, But->Sta.Net);
    }
    sprintf(But->GifName, "%s%s", But->TmpName, ".gif");
    sprintf(But->LocalGif, "%s%s.%d", But->loctarget, But->TmpName, But->Stime.Min);
    out = fopen(But->LocalGif, "wb");
    if(out == 0L) {
        logit("e", "%s Unable to write GIF File: %s\n", whoami, But->LocalGif); 
    } else {
        gdImageGif(But->GifImage, out);
        fclose(out);
        sprintf(tname,  "%s%s", But->loctarget, But->GifName );
        ierr = rename(But->LocalGif, tname);
        if(ierr) {
            if(But->Debug) logit("e", "%s Error Renaming GIF File %d\n", whoami, ierr);
        }
    }
}

/*********************************************************************
 *   CommentList()                                                   *
 *    Build and send a file relating SCNs to their comments.         *
 *********************************************************************/

void CommentList(Global *But)
{
    char    tname[200], fname[175], whoami[50];
    char    stanet[20];
    int     i, j, k, n, ierr, jerr;
    FILE    *out;
    
    sprintf(whoami, " %s: %s: ", progname, "CommentList");

	n = 0;
    for(j=0;j<But->NSCN;j++) {
    	k = 1;
    	for(i=0;i<n;i++) {
			if(strcmp(But->Chan[j].StaNet, But->zzSta[i].StaNet)==0) k = 0;
    	}
    	if(k) {
    		strcpy(But->zzSta[n].StaNet, But->Chan[j].StaNet);
    		strcpy(But->zzSta[n].SiteName, But->Chan[j].SiteName);
    		n++;
    	}
    }
	But->Nsta = n;
    sprintf(tname, "%sznamelist.dat", But->GifDir);
    
    for(j=0;j<But->nltargets;j++) {
        out = fopen(tname, "wb");
        if(out == 0L) {
            logit("e", "%s Unable to open NameList File: %s\n", whoami, tname);    
        } else {
            for(i=0;i<But->Nsta;i++) {
                fprintf(out, "%s. %s.\n", But->zzSta[i].StaNet, But->zzSta[i].SiteName);
            }
            fclose(out);
            sprintf(fname,  "%sznamelist.dat", But->loctarget[j] );
            ierr = rename(tname, fname); 
            /* The following silliness is necessary to be Windows compatible */ 
            if( ierr != 0 ) {
                if(Debug) logit( "e", "Error moving file %s to %s; ierr = %d\n", tname, fname, ierr );
                if( remove( fname ) != 0 ) {
                    logit("e","error deleting file %s\n", fname);
                } else  {
                    if(Debug) logit("e","deleted file %s.\n", fname);
                    jerr = rename( tname, fname );
                    if( jerr != 0 ) {
                        logit( "e", "error moving file %s to %s; ierr = %d\n", tname, fname, ierr );
                    } else {
                        if(Debug) logit("e","%s moved to %s\n", tname, fname );
                    }
                }
            } else {
                if(Debug) logit("e","%s moved to %s\n", tname, fname );
            }
        }
    }
}




/****************************************************************************
 *  Get_Sta_Info(Global *But);                                              *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Sta_Info(Global *But)
{
    char    whoami[50], *com, *str, ns, ew;
    int     i, j, k, nfiles, success;
    double  dlat, mlat, dlon, mlon;

    sprintf(whoami, "%s: %s: ", progname, "Get_Sta_Info");
    
    ns = 'N';
    ew = 'W';
    But->NSCN = 0;
    for(k=0;k<But->nStaDB;k++) {
            /* Open the main station file
             ****************************/
        nfiles = k_open( But->stationList[k] );
        if(nfiles == 0) {
            fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, But->stationList[k] );
            exit( -1 );
        }

            /* Process all command files
             ***************************/
        while(nfiles > 0) {  /* While there are command files open */
            while(k_rd())  {      /* Read next line from active file  */
                com = k_str();         /* Get the first token from line */

                    /* Ignore blank lines & comments
                     *******************************/
                if( !com )           continue;
                if( com[0] == '#' )  continue;

                    /* Open a nested configuration file
                     **********************************/
                if( com[0] == '@' ) {
                    success = nfiles+1;
                    nfiles  = k_open(&com[1]);
                    if ( nfiles != success ) {
                        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                        exit( -1 );
                    }
                    continue;
                }

                /* Process anything else as a channel descriptor
                 ***********************************************/

                if( But->NSCN >= MAXCHANNELS ) {
                    fprintf(stderr, "%s Too many channel entries in <%s>", 
                             whoami, But->stationList[k] );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAXCHANNELS );
                    exit( -1 );
                }
                j = But->NSCN;
                
                    /* S C N */
                strncpy( But->Chan[j].Site, com,  6);
                str = k_str();
            /*    
                str = k_str();
                str = k_str();
             */   
                if(str) strncpy( But->Chan[j].Net,  str,  2);
                str = k_str();
                if(str) strncpy( But->Chan[j].Comp, str, 3);
                for(i=0;i<6;i++) if(But->Chan[j].Site[i]==' ') But->Chan[j].Site[i] = 0;
                for(i=0;i<2;i++) if(But->Chan[j].Net[i]==' ')  But->Chan[j].Net[i]  = 0;
                for(i=0;i<3;i++) if(But->Chan[j].Comp[i]==' ') But->Chan[j].Comp[i] = 0;
                But->Chan[j].Comp[3] = But->Chan[j].Net[2] = But->Chan[j].Site[5] = 0;
                
				str = k_str();
				if(str) strncpy( But->Chan[j].Loc, str, 2);
				for(i=0;i<2;i++) if(But->Chan[j].Loc[i]==' ')  But->Chan[j].Loc[i]  = 0;
				But->Chan[j].Loc[2] = 0;
                
				sprintf(But->Chan[j].SCNnam, "%s_%s_%s_%s", But->Chan[j].Site, But->Chan[j].Net, But->Chan[j].Comp, But->Chan[j].Loc);
				sprintf(But->Chan[j].StaNet, "%s_%s", But->Chan[j].Site, But->Chan[j].Net);


				/* Lat Lon Elev */
				But->Chan[j].Lat  = k_val();
				But->Chan[j].Lon  = k_val();
				But->Chan[j].Elev = k_val();
			

                But->Chan[j].Inst_type = k_int();
                But->Chan[j].Inst_gain = k_val();
                But->Chan[j].GainFudge = k_val();
                But->Chan[j].Sens_type = k_int();
                But->Chan[j].Sens_unit = k_int();
                But->Chan[j].Sens_gain = k_val();
                But->Chan[j].SiteCorr  = k_val();
                        
                if(But->Chan[j].Sens_unit == 3) But->Chan[j].Sens_gain /= 981.0;
               
                But->Chan[j].sensitivity = (1000000.0*But->Chan[j].Sens_gain/But->Chan[j].Inst_gain)*But->Chan[j].GainFudge;    /*    sensitivity counts/units        */
                
                But->Chan[j].ShkQual = k_int();
                
                if (k_err()) {
                    logit("e", "%s Error decoding line %d in station file\n%s\n  exiting!\n", whoami, j, k_get() );
                    logit("e", "%s Previous line was %s\n  exiting!\n", whoami, But->Chan[j-1].SCNnam );
                    exit( -1 );
                }
         /*>Comment<*/
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].SiteName, str );
                    
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].Descript, str );
                    
                But->NSCN++;
            }
            nfiles = k_close();
        }
    }
	if(Debug) logit("e","%s %d channels read from %s.\n", whoami, But->NSCN, But->stationList[0] );
}



/**********************************************************************
 * Decode_Time : Decode time from seconds since 1970                  *
 *                                                                    *
 **********************************************************************/
void Decode_Time( double secs, TStrct *Time)
{
    struct Greg  g;
    long    minute;
    double  sex;

    Time->Time = secs;
    secs += GSEC1970;
    Time->Time1600 = secs;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    Time->Year  = g.year;
    Time->Month = g.month;
    Time->Day   = g.day;
    Time->Hour  = g.hour;
    Time->Min   = g.minute;
    Time->Sec   = sex;
}


/**********************************************************************
 * Encode_Time : Encode time to seconds since 1970                    *
 *                                                                    *
 **********************************************************************/
void Encode_Time( double *secs, TStrct *Time)
{
    struct Greg    g;

    g.year   = Time->Year;
    g.month  = Time->Month;
    g.day    = Time->Day;
    g.hour   = Time->Hour;
    g.minute = Time->Min;
    *secs    = 60.0 * (double) julmin(&g) + Time->Sec - GSEC1970;
}


/**********************************************************************
 * date22 : Calculate 22 char date in the form Jan23,1988 12:34 12.21 *
 *          from the julian seconds.  Remember to leave space for the *
 *          string termination (NUL).                                 *
 **********************************************************************/
void date22( double secs, char *c22)
{
    char *cmo[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    struct Greg  g;
    long    minute;
    double  sex;

    secs += GSEC1970;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    sprintf(c22, "%3s%2d,%4d %.2d:%.2d:%05.2f",
            cmo[g.month-1], g.day, g.year, g.hour, g.minute, sex);
}


/***********************************************************************
 * date11 : Calculate 11 char h:m:s time in the form 12:34:12.21       *
 *          from the daily seconds.  Remember to leave space for the   *
 *          string termination (NUL).                                  *
 *          This is a special version used for labelling the time axis *
 ***********************************************************************/
void date11( double secs, char *c11)
{
    long    hour, minute, wholesex;
    double  sex, fracsex;

    if(secs < 86400.0) secs = secs - 86400; /* make negative times negative */
    minute = (long) (secs / 60.0);
    sex    = secs - 60.0 * minute;
    wholesex = (long) sex;
    fracsex  = sex - wholesex;
    hour   = (long) (minute / 60.0);
    minute = minute - 60 * hour;
    while(hour>=24) hour -= 24;
    
    if(hour != 0) {
        sprintf(c11, "%d:%.2d:%05.2f", hour, minute, sex);
        if(fracsex >= 0.01) sprintf(c11, "%.2d:%.2d:%05.2f", hour, minute, sex);
        else sprintf(c11, "%.2d:%.2d:%.2d", hour, minute, wholesex);
    }
    else if(minute != 0) {
        sprintf(c11, "%d:%05.2f", minute, sex);
        if(fracsex >= 0.01) sprintf(c11, "%.2d:%05.2f", minute, sex);
        else sprintf(c11, "%.2d:%.2d", minute, wholesex);
    }
    else if(sex >= 10.0) {
        if(fracsex >= 0.01) sprintf(c11, "%05.2f", sex);
        else sprintf(c11, "%.2d", wholesex);
    }
    else {
        if(fracsex >= 0.01) sprintf(c11, "%4.2f", sex);
        else sprintf(c11, "%.1d", wholesex);
    }
}


/***********************************************************************
 *  config_me() processes command file(s) using kom.c functions;       *
 *                  returns error codes indicating which param is bad  *
 ***********************************************************************/
int config_me( char *configfile, Global *But )
{
    char    whoami[50], *com, *str, string[120];
    int     ncommand;     /* # of required commands you expect to process   */ 
    char    init[20];     /* init flags, one byte for each required command */
    int     nmiss;        /* number of required commands that were missed   */
    int     nfiles, success, i, n;
    char    envEW_LOG[256];
    double  val;

    sprintf(whoami, "%s: %s: ", progname, "config_me");
/* Set to zero one init flag for each required command 
 *****************************************************/   
	ncommand = 12;
	for( i=0; i<ncommand; i++ ) init[i] = 0;
	
	NchanNames = 0;
	But->nStaDB = 0;
	GifNameType = 0;            /* Default naming convention for gif file  */

/* Open the main configuration file 
 **********************************/
	nfiles = k_open( configfile ); 
	if ( nfiles == 0 ) {
		printf("Error opening command file <%s>\n", configfile );
		return(EW_FAILURE);
	}

/* Process all command files
 ***************************/
	while(nfiles > 0) {  /* While there are command files open */
		while(k_rd()) {       /* Read next line from active file  */
			com = k_str();         /* Get the first token from line */
        /* Ignore blank lines & comments
         *******************************/
			if( !com )           continue;
			if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
         **********************************/
			if( com[0] == '@' ) {
				success = nfiles+1;
				nfiles  = k_open(&com[1]);
				if ( nfiles != success ) {
					printf( "Error opening command file <%s>\n", &com[1] );
					return( EW_FAILURE );
				}
				continue;
			}

        /* Process anything else as a command 
         ************************************/
  /*0*/     
            if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
   /*1*/     
			else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     
			else if( k_its("InRingName") ) {
                str = k_str();
                if(str) strcpy( InRingName, str );
                init[2] = 1;
            }
  /*3*/     
			else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_long();
                init[3] = 1;
            }
  /*4*/     
			else if( k_its("EVTFilesInDir") ) {
                str = k_str();
                if(str) strcpy( EVTFilesInDir, str );
                init[4] = 1;
            }
  /*5*/     
			else if( k_its("NetworkName") ) {
                str = k_str();
                if(str) strncpy( NetworkName, str, TRACE2_NET_LEN );
                init[5] = 1;
            }
    /*6*/     
			else if( k_its("EVTFilesOutDir") ) {
                str = k_str();
                if(str) strcpy( EVTFilesOutDir, str );
                init[6] = 1;
            }
  /*7*/     
			else if( k_its("EVTFilesErrorDir") ) {
                str = k_str();
                if(str) strcpy( EVTFilesErrorDir, str );
                init[7] = 1;
            }
  /*8*/     
            else if( k_its("GifDir") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf( stderr, "%s Fatal error. Gif directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    return (-1);
                }
                if(str) strcpy( But->GifDir , str );
				init[8] = 1;
            }
/*9*/
            else if( k_its("Display") ) {
         /*>01<*/
                But->SecsPerPlot = k_int();   /*  # of seconds per gif image */
                if(But->SecsPerPlot <  10) But->SecsPerPlot =  10;
                if(But->SecsPerPlot > 240) But->SecsPerPlot = 240;
         /*>02<*/
                val = k_val();            /* x-size of data plot in pixels */
                But->xpix = (val >= 100.0)? (int)val:(int)(72.0*val);
                
         /*>03<*/
                val = k_val();            /* y-size of data plot in pixels */
                But->ypix = (val >= 100.0)? (int)val:(int)(72.0*val);
                
                init[9] = 1;
            }
/*10*/
            else if( k_its("StationList") ) {
                if ( But->nStaDB >= MAX_STADBS ) {
                    fprintf( stderr, "%s Too many <StationList> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_STADBS );
                    return (-1);
                }
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->stationList[But->nStaDB] , str );
                But->nStaDB++;
                init[10] = 1;
            }
            
        /* get the local target directory(s)
        ************************************/
/*11*/
            else if( k_its("LocalTarget") ) {
                if ( But->nltargets >= MAX_TARGETS ) {
                    logit("e", "%s Too many <LocalTarget> commands in <%s>", 
                             whoami, configfile );
                    logit("e", "; max=%d; exiting!\n", (int) MAX_TARGETS );
                    return (-1);
                }
                if( (long)(str=k_str()) != 0 )  {
                    n = strlen(str);   /* Make sure directory name has proper ending! */
                    if( str[n-1] != '/' ) strcat(str, "/");
                    strcpy(But->loctarget[But->nltargets], str);
                }
                But->nltargets += 1;
                init[11] = 1;
            }

         /* Get the mappings from box id to SCNL name
          ********************************************/ 
  /*opt*/   
			else if( k_its("ChannelName") ) {
                if( NchanNames >= MAXCHAN ) {
                    fprintf( stderr, 
                        "%s Too many <ChannelName> commands"
                        " in <%s>; max=%d; exiting!\n", whoami, 
                        configfile,(int) MAXCHAN );
                    exit( -1 );
                }
         /* Get the box name */
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_VENDOR_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "%s box name <%s> too long" 
                                       " in <ChannelName> cmd; exiting!\n", whoami, str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].box, str);
                }
         /* Get the channel number */
                ChannelName[NchanNames].chan = k_int();
                if(ChannelName[NchanNames].chan > SM_MAX_CHAN){
                    fprintf(stderr, "%s Channel number %d greater "
                                       "than %d in <ChannelName> cmd; exiting\n", whoami,
                    ChannelName[NchanNames].chan,SM_MAX_CHAN);
                    exit(-1);
                }
         /* Get the SCNL name */
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_STA_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "%s station name <%s> too long" 
                                           " in <ChannelName> cmd; exiting!\n", whoami, str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].sta, str);
                } 
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_COMP_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "%s component name <%s> too long"
                                           " in <ChannelName> cmd; exiting!\n", whoami, str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].comp, str);
                } 
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_NET_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "%s network name <%s> too long"
                                           " in <ChannelName> cmd; exiting!\n", whoami, str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].net, str);
                } 
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_LOC_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "%s location name <%s> too long"
                                           " in <ChannelName> cmd; exiting!\n", whoami, str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].loc, str);
                }
                NchanNames++;
            }         

            else if( k_its("Logo") ) {        /*optional command*/
                str = k_str();
                if(str) {
                    strcpy( But->logoname, str );
                    But->logo = 1;
                }
            }
      
            else if( k_its("GifNameType") ) {  /*optional command*/
                GifNameType = k_int();
            }

      
            else if( k_its("Debug") ) {  /*optional command*/
                But->Debug = Debug = 1;
            }

            else if( k_its("UseDST") )         But->UseDST = 1;         /* optional command */


        /* Unknown command
         *****************/ 
        else {
                fprintf( stderr, "<%s> Unknown command in <%s> .\n", string, configfile );
                continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
              printf( "asciiK2ora: Bad <%s> command in <%s>; exitting!\n", 
                                                    com, configfile );
              return( EW_FAILURE );
            }
    }
    nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       printf( "ERROR, no " );
       if ( !init[0] )  printf( "<LogFile> "       );
       if ( !init[1] )  printf( "<MyModuleId> "    );
       if ( !init[2] )  printf( "<InRingName> "      );
       if ( !init[3] )  printf( "<HeartBeatInterval> "     );
       if ( !init[4] )  printf( "<EVTFilesInDir> "    );
       if ( !init[5] )  printf( "<NetworkName> "    );
       if ( !init[6] )  printf( "<EVTFilesOutDir> "    );
       if ( !init[7] )  printf( "<EVTFilesErrorDir> "    );
       if ( !init[8] )  printf( "<GifDir> "  );
       if ( !init[9] )  printf( "<Display> "  );
       if ( !init[10] )  printf( "<StationList> "  );
       if ( !init[11] )  printf( "<LocalTarget> "  );
       printf("command(s) in <%s>", configfile );
       return(EW_FAILURE);
   }
   return(EW_SUCCESS);
}


/******************************************************************************
 *  ew_lookup( )   Look up important info from earthworm.h tables             *
 ******************************************************************************/
void ew_lookup( void )
{
	char   whoami[50];
	
	sprintf(whoami, "%s: %s: ", progname, "ew_lookup");
/* Look up keys to shared memory regions
   *************************************/
	if( ( InRingKey = GetKey(InRingName) ) == -1 ) {
		fprintf( stderr, "%s Invalid ring name <%s>; exiting!\n", whoami, InRegion);
		exit( -1 );
	}

/* Look up installations of interest
   *********************************/
	if ( GetLocalInst( &InstId ) != 0 ) {
		fprintf( stderr, "%s error getting local installation id; exiting!\n", whoami );
		exit( -1 );
	}

/* Look up modules of interest
   ***************************/
	if ( GetModId( MyModName, &MyModId ) != 0 ) {
		fprintf( stderr, "%s Invalid module name <%s>; exiting!\n", whoami, MyModName );
		exit( -1 );
	}

/* Look up message types of interest
   *********************************/
	if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
		fprintf( stderr, "%s Invalid message type <TYPE_HEARTBEAT>; exiting!\n", whoami );
		exit( -1 );
	}
	if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
		fprintf( stderr, "%s Invalid message type <TYPE_ERROR>; exiting!\n", whoami );
		exit( -1 );
	}
	if ( GetType( "TYPE_TRACEBUF2", &typeWaveform2 ) != 0 ) {
		fprintf( stderr, "%s Invalid message type <TYPE_TRACEBUF2>; exiting!\n", whoami );
		exit( -1 );
	}
	return;
}

/******************************************************************************
 * ew_status() builds a heartbeat or error message & puts it into             *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/
void ew_status( unsigned char type, short ierr, char *note )
{
	MSG_LOGO  logo;
	char      msg[256];
	long      size, t;
	char      whoami[50];
	
	sprintf(whoami, "%s: %s: ", progname, "ew_status");

	/* Build the message
	 *******************/
	logo.instid = InstId;
	logo.mod    = MyModId;
	logo.type   = type;

	time( &t );

	if( type == TypeHeartBeat ) {
		sprintf( msg, "%ld %ld\n\0", t, MyPid );
	} else if( type == TypeError ) {
		sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
		logit( "et", "%s %s\n", whoami, note );
	}

	size = strlen( msg );   /* don't include the null byte in the message */
	
	/* Write the message to shared memory
	 ************************************/
	if( tport_putmsg( &InRegion, &logo, size, msg ) != PUT_OK ) {
        if( type == TypeHeartBeat ) {
           logit("et","%s Error sending heartbeat.\n", whoami );
        } else if( type == TypeError ) {
           logit("et","%s Error sending error:%d.\n", whoami, ierr );
        }
	}
	return;
}

/******************************************************************************
 * read_tag(fp)  Read the 16 byte tag, swap bytes if necessary, and print.    *
 ******************************************************************************/
int read_tag (FILE *fp, KFF_TAG *tag)
{
	if ((fp == NULL) || (tag == NULL)) {
		logit ("e", "read_tag: Invalid arguments passed in\n");
		return EW_FAILURE;
	}

    if (fread(tag, 1, 16, fp) != 16) {
		logit ("e", "read_tag: read of file failed.\n");
		return EW_FAILURE;
	}

#ifdef _INTEL
    SwapLong  (&tag->type);
    SwapShort (&tag->length);
    SwapShort (&tag->dataLength);
    SwapShort (&tag->id);
    SwapShort (&tag->checksum);
#endif _INTEL

    if (Debug > 0) 
	{
        logit("e", "TAG: %c %d %d %d %d %d %d %d %d  \n",
                tag->sync,
                (int)tag->byteOrder,
                (int)tag->version,
                (int)tag->instrumentType,
                tag->type, tag->length, tag->dataLength,
                tag->id, tag->checksum);
    }

	/* look ahead, and check on the upcoming record for sanity
	**********************************************************/
	{
		long fpos;
		char checkBuffer[MAX_REC];
		unsigned short checksum=0;
		int bytesToCheck;
		int i;

		fpos = ftell (fp); /* remember where we were */
		bytesToCheck = tag->length + tag->dataLength;
		if (bytesToCheck > MAX_REC) 
		{
			logit ("e", "read_tag: record too long.\n");
			logit ("e", "record + data length > MAX_REC. \n");
			return EW_FAILURE;
		}
		if (fread(checkBuffer, 1, bytesToCheck, fp) != bytesToCheck)
		{
			logit ("e", "read_tag: read of file failed.\n");
			return EW_FAILURE;
		}
		/* look at the synch character */
		if( tag->sync != 'K')
		{
			logit ("e", "read_tag: bad synch character.\n");
			return EW_FAILURE;
		}
		for ( i=0; i<bytesToCheck; i++)
			checksum = checksum + (unsigned char) checkBuffer[i];
		if (checksum != tag->checksum)
		{
			logit("","read_tag: checksum error\n");
			return EW_FAILURE;
		}

		/* now put things back the way they were */
		fseek(fp, fpos, SEEK_SET );
	}


    return EW_SUCCESS;
}


/******************************************************************************
 * read_head(fp)  Read the file header, swap bytes if necessary, and print.   *
 ******************************************************************************/
int read_head (FILE *fp, MW_HEADER *head, int tagLength)
{
   int        i, maxchans, siz;

	if ((fp == NULL) || (head == NULL))
	{
		logit ("e", "read_head: Invalid arguments passed in\n");
		return EW_FAILURE;
	}

/* Read in the file header.
   If a K2, there will be 2040 bytes,
   otherwise assume a Mt Whitney.
 ************************************/
    maxchans = tagLength==2040? MAX_K2_CHANNELS:MAX_MW_CHANNELS;

    if (fread(head, 1, 8, fp) != 8)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}

	siz = sizeof(struct MISC_RO_PARMS) + sizeof(struct TIMING_RO_PARMS);
    if (fread(&head->roParms.misc, 1, siz, fp) != (unsigned int) siz)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}

	siz = sizeof(struct CHANNEL_RO_PARMS)*maxchans;
    if (fread(&head->roParms.channel[0], 1, siz, fp) != (unsigned int) siz)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}

	siz = sizeof(struct STREAM_RO_PARMS);
    if (fread(&head->roParms.stream, 1, siz, fp) != (unsigned int) siz)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}

	siz = sizeof(struct MISC_RW_PARMS)+sizeof(struct TIMING_RW_PARMS);
    if (fread(&head->rwParms.misc, 1, siz, fp) != (unsigned int) siz)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}

    siz = sizeof(struct CHANNEL_RW_PARMS)*maxchans;
    if (fread(&head->rwParms.channel[0], 1, siz, fp) != (unsigned int) siz)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}

    if(tagLength==2040) {
        siz = sizeof(struct STREAM_K2_RW_PARMS);
        if (fread(&head->rwParms.stream, 1, siz, fp) != (unsigned int) siz)
		{
			logit ("e", "read_head: read of file failed.\n");
			return EW_FAILURE;
		}
    } else {
        siz = sizeof(struct STREAM_MW_RW_PARMS);
        if (fread(&head->rwParms.stream, 1, siz, fp) != (unsigned int) siz)
		{
			logit ("e", "read_head: read of file failed.\n");
			return EW_FAILURE;
		}
    }
    siz = sizeof(struct MODEM_RW_PARMS);
    if (fread(&head->rwParms.modem, 1, siz, fp) != (unsigned int) siz)
	{
		logit ("e", "read_head: read of file failed.\n");
		return EW_FAILURE;
	}


#ifdef _INTEL
	/* Byte-swap values, if necessary */

	/* roParms */
    SwapShort (&head->roParms.headerVersion);
    SwapShort (&head->roParms.headerBytes);

    SwapShort (&head->roParms.misc.installedChan);
    SwapShort (&head->roParms.misc.maxChannels);
    SwapShort (&head->roParms.misc.sysBlkVersion);
    SwapShort (&head->roParms.misc.bootBlkVersion);
    SwapShort (&head->roParms.misc.appBlkVersion);
    SwapShort (&head->roParms.misc.dspBlkVersion);
    SwapShort (&head->roParms.misc.batteryVoltage);
    SwapShort (&head->roParms.misc.crc);
    SwapShort (&head->roParms.misc.flags);
    SwapShort (&head->roParms.misc.temperature);

    SwapShort (&head->roParms.timing.gpsLockFailCount);
    SwapShort (&head->roParms.timing.gpsUpdateRTCCount);
    SwapShort (&head->roParms.timing.acqDelay);
    SwapShort (&head->roParms.timing.gpsLatitude);
    SwapShort (&head->roParms.timing.gpsLongitude);
    SwapShort (&head->roParms.timing.gpsAltitude);
    SwapShort (&head->roParms.timing.dacCount);
    SwapShort (&head->roParms.timing.gpsLastDrift[0]);
    SwapShort (&head->roParms.timing.gpsLastDrift[1]);
    SwapLong (&head->roParms.timing.gpsLastTurnOnTime[0]);
    SwapLong (&head->roParms.timing.gpsLastTurnOnTime[1]);
    SwapLong (&head->roParms.timing.gpsLastUpdateTime[0]);
    SwapLong (&head->roParms.timing.gpsLastUpdateTime[1]);
    SwapLong (&head->roParms.timing.gpsLastLockTime[0]);
    SwapLong (&head->roParms.timing.gpsLastLockTime[1]);

    SwapLong (&head->roParms.stream.startTime);
    SwapLong (&head->roParms.stream.triggerTime);
    SwapLong (&head->roParms.stream.duration);
    SwapShort (&head->roParms.stream.errors);
    SwapShort (&head->roParms.stream.flags);
    SwapShort (&head->roParms.stream.startTimeMsec);
    SwapShort (&head->roParms.stream.triggerTimeMsec);
    SwapLong (&head->roParms.stream.nscans);


    for(i=0;i<head->roParms.misc.maxChannels;i++) 
	{
        SwapLong (&head->roParms.channel[i].maxPeak);
        SwapLong (&head->roParms.channel[i].maxPeakOffset);
        SwapLong (&head->roParms.channel[i].minPeak);
        SwapLong (&head->roParms.channel[i].minPeakOffset);
        SwapLong (&head->roParms.channel[i].mean);
	}


	/* rwParams */
    SwapShort (&head->rwParms.misc.serialNumber);
    SwapShort (&head->rwParms.misc.nchannels);
    SwapShort (&head->rwParms.misc.elevation);
    SwapFloat (&head->rwParms.misc.latitude);
    SwapFloat (&head->rwParms.misc.longitude);
    SwapShort (&head->rwParms.misc.userCodes[0]);
    SwapShort (&head->rwParms.misc.userCodes[1]);
    SwapShort (&head->rwParms.misc.userCodes[2]);
    SwapShort (&head->rwParms.misc.userCodes[3]);
    SwapLong (&head->rwParms.misc.cutler_bitmap);
    SwapLong (&head->rwParms.misc.channel_bitmap);

    SwapShort (&head->rwParms.timing.localOffset);

    for(i=0;i<head->rwParms.misc.nchannels;i++) 
	{
        SwapShort (&head->rwParms.channel[i].sensorSerialNumberExt);
        SwapShort (&head->rwParms.channel[i].north);
        SwapShort (&head->rwParms.channel[i].east);
        SwapShort (&head->rwParms.channel[i].up);
        SwapShort (&head->rwParms.channel[i].altitude);
        SwapShort (&head->rwParms.channel[i].azimuth);
        SwapShort (&head->rwParms.channel[i].sensorType);
        SwapShort (&head->rwParms.channel[i].sensorSerialNumber);
        SwapShort (&head->rwParms.channel[i].gain);
        SwapShort (&head->rwParms.channel[i].StaLtaRatio);
        SwapFloat (&head->rwParms.channel[i].fullscale);
        SwapFloat (&head->rwParms.channel[i].sensitivity);
        SwapFloat (&head->rwParms.channel[i].damping);
        SwapFloat (&head->rwParms.channel[i].naturalFrequency);
        SwapFloat (&head->rwParms.channel[i].triggerThreshold);
        SwapFloat (&head->rwParms.channel[i].detriggerThreshold);
        SwapFloat (&head->rwParms.channel[i].alarmTriggerThreshold);
    }

    SwapShort (&head->rwParms.stream.eventNumber);
    SwapShort (&head->rwParms.stream.sps);
    SwapShort (&head->rwParms.stream.apw);
    SwapShort (&head->rwParms.stream.preEvent);
    SwapShort (&head->rwParms.stream.postEvent);
    SwapShort (&head->rwParms.stream.minRunTime);
    SwapShort (&head->rwParms.stream.VotesToTrigger);
    SwapShort (&head->rwParms.stream.VotesToDetrigger);
    SwapShort (&head->rwParms.stream.Timeout);
    SwapShort (&head->rwParms.stream.TxBlkSize);
    SwapShort (&head->rwParms.stream.BufferSize);
    SwapShort (&head->rwParms.stream.SampleRate);
    SwapLong (&head->rwParms.stream.TxChanMap);
#endif _INTEL


    if(Debug > 0)
	{
	    logit("e", "HEADER: %c%c%c %d %hu %hu \n",
   	         head->roParms.id[0], head->roParms.id[1], head->roParms.id[2],
   	         (int)head->roParms.instrumentCode,
             head->roParms.headerVersion,
             head->roParms.headerBytes);

	    logit("e", "MISC_RO_PARMS: %d %d %d %hu %hu %hu %hu %hu %hu %d %hu %hu %d \n",
            (int)head->roParms.misc.a2dBits,
            (int)head->roParms.misc.sampleBytes,
            (int)head->roParms.misc.restartSource,
            head->roParms.misc.installedChan,
            head->roParms.misc.maxChannels,
            head->roParms.misc.sysBlkVersion,
            head->roParms.misc.bootBlkVersion,
            head->roParms.misc.appBlkVersion,
            head->roParms.misc.dspBlkVersion,
            head->roParms.misc.batteryVoltage,
            head->roParms.misc.crc,
            head->roParms.misc.flags,
            head->roParms.misc.temperature );


	    logit("e", "TIMING_RO_PARMS: %d %d %d %hu %hu %d %d %d %d %hu %d %d %lu %lu %lu %lu %lu %lu \n",
            (int)head->roParms.timing.clockSource,
            (int)head->roParms.timing.gpsStatus,
            (int)head->roParms.timing.gpsSOH,
            head->roParms.timing.gpsLockFailCount,
            head->roParms.timing.gpsUpdateRTCCount,
            head->roParms.timing.acqDelay,
            head->roParms.timing.gpsLatitude,
            head->roParms.timing.gpsLongitude,
            head->roParms.timing.gpsAltitude,
            head->roParms.timing.dacCount,
            head->roParms.timing.gpsLastDrift[0],
            head->roParms.timing.gpsLastDrift[1],
            head->roParms.timing.gpsLastTurnOnTime[0],
            head->roParms.timing.gpsLastTurnOnTime[1],
            head->roParms.timing.gpsLastUpdateTime[0],
            head->roParms.timing.gpsLastUpdateTime[1],
            head->roParms.timing.gpsLastLockTime[0],
            head->roParms.timing.gpsLastLockTime[1] );


	    for(i=0;i<head->roParms.misc.maxChannels;i++) 
		{
        	logit("e", "CHANNEL_RO_PARMS: %d %lu %d %lu %d %d \n",
	            head->roParms.channel[i].maxPeak,
                head->roParms.channel[i].maxPeakOffset,
   	            head->roParms.channel[i].minPeak,
                head->roParms.channel[i].minPeakOffset,
                head->roParms.channel[i].mean,
                head->roParms.channel[i].aqOffset );
    	}

    	logit("e", "STREAM_RO_PARMS: %d %d %d %d %d %d %d %d \n",
            head->roParms.stream.startTime,
            head->roParms.stream.triggerTime,
            head->roParms.stream.duration,
            head->roParms.stream.errors,
            head->roParms.stream.flags,
            head->roParms.stream.startTimeMsec,
            head->roParms.stream.triggerTimeMsec,
            head->roParms.stream.nscans  );



	    logit("e", "MISC_RW_PARMS: %hu %hu %.5s %.33s %d %f %f %d %d %d %d %d %lo %d %.17s %d %d\n",
            head->rwParms.misc.serialNumber,
            head->rwParms.misc.nchannels,
            head->rwParms.misc.stnID,
            head->rwParms.misc.comment,
            head->rwParms.misc.elevation,
            head->rwParms.misc.latitude,
            head->rwParms.misc.longitude,
           (int)head->rwParms.misc.cutlerCode,
           (int)head->rwParms.misc.minBatteryVoltage,
           (int)head->rwParms.misc.cutler_decimation,
           (int)head->rwParms.misc.cutler_irig_type,
            head->rwParms.misc.cutler_bitmap,
            head->rwParms.misc.channel_bitmap,
           (int)head->rwParms.misc.cutler_protocol,
            head->rwParms.misc.siteID,
           (int)head->rwParms.misc.externalTrigger,
           (int)head->rwParms.misc.networkFlag );

	    logit("e", "TIMING_RW_PARMS: %d %d %hu \n",
           (int)head->rwParms.timing.gpsTurnOnInterval,
           (int)head->rwParms.timing.gpsMaxTurnOnTime,
               head->rwParms.timing.localOffset  );

	    for(i=0;i<head->rwParms.misc.nchannels;i++) 
		{
        	logit("e", 
				"CHANNEL_RW_PARMS: %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f %f %f %f %f %f %f \n",
                   head->rwParms.channel[i].id,
                   head->rwParms.channel[i].sensorSerialNumberExt,
                   head->rwParms.channel[i].north,
                   head->rwParms.channel[i].east,
                   head->rwParms.channel[i].up,
                   head->rwParms.channel[i].altitude,
                   head->rwParms.channel[i].azimuth,
                   head->rwParms.channel[i].sensorType,
                   head->rwParms.channel[i].sensorSerialNumber,
                   head->rwParms.channel[i].gain,
               (int)head->rwParms.channel[i].triggerType,
               (int)head->rwParms.channel[i].iirTriggerFilter,
               (int)head->rwParms.channel[i].StaSeconds,
               (int)head->rwParms.channel[i].LtaSeconds,
                   head->rwParms.channel[i].StaLtaRatio,
               (int)head->rwParms.channel[i].StaLtaPercent,
                   head->rwParms.channel[i].fullscale,
                   head->rwParms.channel[i].sensitivity,
                   head->rwParms.channel[i].damping,
                   head->rwParms.channel[i].naturalFrequency,
                   head->rwParms.channel[i].triggerThreshold,
                   head->rwParms.channel[i].detriggerThreshold,
                   head->rwParms.channel[i].alarmTriggerThreshold);
		}

	    logit("e", "STREAM_K2_RW_PARMS: |%d| |%d| |%d| %d %d %d %d %d %d %d %d %d %d %d %d %d %d %lu\n",
            (int)head->rwParms.stream.filterFlag,
            (int)head->rwParms.stream.primaryStorage,
            (int)head->rwParms.stream.secondaryStorage,
                head->rwParms.stream.eventNumber,
                head->rwParms.stream.sps,
                head->rwParms.stream.apw,
                head->rwParms.stream.preEvent,
                head->rwParms.stream.postEvent,
                head->rwParms.stream.minRunTime,
                head->rwParms.stream.VotesToTrigger,
                head->rwParms.stream.VotesToDetrigger,
            (int)head->rwParms.stream.FilterType,
            (int)head->rwParms.stream.DataFmt,
                head->rwParms.stream.Timeout,
                head->rwParms.stream.TxBlkSize,
                head->rwParms.stream.BufferSize,
                head->rwParms.stream.SampleRate,
                head->rwParms.stream.TxChanMap);

	} /* If debug */

    return EW_SUCCESS;

} /* read_head */


/******************************************************************************
 * read_frame(fp)  Read the frame header, swap bytes if necessary.            *
 ******************************************************************************/
int read_frame (FILE *fp, FRAME_HEADER *frame, unsigned long *channels )
{
    unsigned short   frameStatus, frameStatus2, samprate, streamnumber;
    unsigned char    BitMap[4];
    unsigned long    bmap;

	if ((fp == NULL) || (frame == NULL) || (channels == NULL))
	{
		logit ("e", "read_frame: invalid arguments passed in.\n");
		return EW_FAILURE;
	}


    if (fread(frame, 32, 1, fp) != 1)
	{
		logit ("e", "read_frame: read of file failed.\n");
		return EW_FAILURE;
	}


#ifdef _INTEL
    SwapShort (&frame->recorderID);
    SwapShort (&frame->frameSize);
    SwapShort (&frame->blockTime);
    SwapShort (&frame->blockTime2);
    SwapShort (&frame->channelBitMap);
    SwapShort (&frame->streamPar);
    SwapShort (&frame->msec); 
#endif _INTEL

    BitMap[0] = frame->channelBitMap & 255;
    BitMap[1] = frame->channelBitMap >> 8;
    BitMap[2] = frame->channelBitMap1;
    BitMap[3] = 0;

    bmap = *((long *)(BitMap));
    frameStatus      = frame->frameStatus;
    frameStatus2     = frame->frameStatus2;
    samprate         = frame->streamPar & 4095;
    streamnumber     = frame->streamPar >> 12;
    
    if(Debug > 0)
   	    logit("e", "FRAME: %d %d %d %d   %lu X%ho   %hu X%ho %hu %hu     X%ho X%ho %hu X%ho \n",
            (int)frame->frameType,
            (int)frame->instrumentCode,
            frame->recorderID,
            frame->frameSize,
            frame->blockTime,
            frame->channelBitMap,
            frame->streamPar, streamnumber, samprate, samprate>>8,
            frameStatus,
            frameStatus2,
            frame->msec,
            (int)frame->channelBitMap1);

    *channels = bmap;

    return EW_SUCCESS;
}



