CFLAGS = -D_REENTRANT $(GLOBALFLAGS)

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

BINARIES = evt2gif.o k2evt2ew.o \
		$L/logit_mt.o $L/getutil.o  $L/transport.o $L/kom.o \
		$L/sleep_ew.o $L/time_ew.o $L/threads_ew.o $L/sema_ew.o\
		$L/swap.o $L/mem_circ_queue.o $L/getavail.o $L/pipe.o \
		$L/getsysname_ew.o $L/chron3.o $L/parse_trig.o $L/dirops_ew.o \
		$L/socket_ew.o $L/socket_ew_common.o $L/ws_clientII.o \
		$L/gd.o $L/gdfontt.o $L/gdfonts.o $L/gdfontmb.o \
		$L/gdfontl.o $L/gdfontg.o

evt2gif: $(BINARIES)
	cc -o $(B)/evt2gif $(BINARIES) -lsocket -lnsl -lm -mt -lposix4 -lthread -lc

.c.o:
	$(CC) $(CFLAGS) -g $(CPPFLAGS) -c  $(OUTPUT_OPTION) $<

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/evt2gif*

