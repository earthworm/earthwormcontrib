
    /********************************************************************
     *                            update_cta                            *
     *                                                                  *
     *  Program to update pin numbers in continuous tape archive (cta)  *
     *  files.  This is done "in place", ie the cta file is modified.   *
     ********************************************************************/

/* This program is used to modify continuous tape files with non-unique
   pin numbers.  K2 data written to tape before 10/22/02 at 3:45PM PDT
   are affected.  K2 data written after this date do not require that
   their pin numbers be updated.  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include <trace_buf.h>
#include "update_cta.h"

/* Function prototypes
   *******************/
void GetScnList( char *, SCN *, int * );
void SortScnList( SCN *, int );
void UpdateCtaFile( char *ctaFile, SCN *scn, int nScn );


int main( int argc, char *argv[] )
{
   long        recsize;              /* Size of retrieved message */
   char        *scnFile;             /* Name of scn file */
   SCN         *scn;                 /* List of SCNs obtained */
   int         numread;
   static char defaultScnFile[] = "/home/earthworm/run/params/k2_pinnos.new";
   int         nScn;                 /* Number of SCNs obtained */
   char        *ctaFile;             /* Name of cta file to update */

/* Get cta file name from command line
   ***********************************/
   if ( argc < 2 )
   {
      printf( "Usage:\nupdate_cta <cta file name> [scn file name]\n" );
      return -1;
   }
   ctaFile = argv[1];

/* Get scn file name from command line
   ***********************************/
   scnFile = ( argc < 3 ) ? defaultScnFile : argv[2];

/* Allocate the list of SCNs
   *************************/
   scn = (SCN *) calloc( MAXSCN, sizeof(SCN) );
   if ( scn == NULL )
   {
      printf( "update_cta: Can't allocate SCN list. Exiting.\n" );
      return -1;
   }

/* Get list of SCNs
   ****************/
   printf( "Getting list of SCNs from file: %s\n", scnFile );
   GetScnList( scnFile, scn, &nScn );
   if ( nScn < 1 )
   {
      printf( "update_cta: The scn file contains no data. Exiting.\n" );
      return -1;
   }
   printf( "Number of unique SCNs in file: %d\n", nScn );

/* Sort scn list alphabetically by SCN
   ***********************************/
   SortScnList( scn, nScn );

/* {
      int i;
      for ( i = 0; i < nScn; i++ )
      {
         printf( "%-5s",   scn[i].sta );
         printf( " %-3s",  scn[i].cmp );
         printf( " %-2s",  scn[i].net );
         printf( "  %-4d", scn[i].pinno );
         printf( "\n" );
      }
   }  */

/* Update pin numbers in cta file
   ******************************/
   UpdateCtaFile( ctaFile, scn, nScn );
   printf( "Exiting.\n" );
   return 0;
}
