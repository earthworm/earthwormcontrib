
       /**********************************************************
        *                    UpdateCtaFile()                     *
        *                                                        *
        *  Returns UPDATECTA_SUCCESS or UPDATECTA_FAILURE        *
        **********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <trace_buf.h>
#include "update_cta.h"


void UpdateCtaFile( char *ctaFile, SCN *scn, int nScn )
{
   char         *tracebuf;
   TRACE_HEADER *waveHead;
   char         *dataPtr;
   FILE         *fpCta;

/* Allocate trace buffer
   *********************/
   tracebuf = (char *)malloc( MAX_TRACEBUF_SIZ );
   if ( tracebuf == NULL )
   {
      printf( "Error: Can't allocate trace buffer. Exiting.\n" );
      exit( -1 );
   }
   waveHead = (TRACE_HEADER *)tracebuf;
   dataPtr  = tracebuf + sizeof(TRACE_HEADER);

/* Open cta file for updating
   **************************/
   if ( (fpCta = fopen( ctaFile, "rb+" )) == NULL )
   {  
      printf( "Error opening cta disk file: %s. Exiting.\n", ctaFile );
      exit( -1 );
   }

/* Loop through the whole cta file
   *******************************/
   printf( "Updating pin numbers in cta file. Wait...\n" );
   fflush( stdout );
   while ( 1 )
   {
      char   byteOrder;
      int    j;
      int    nDataBytes;
      int    pinno;
      int    nsamp;
      fpos_t pos;

/* Get position of file pointer
   ****************************/
      if ( fgetpos( fpCta, &pos ) )
      {
         printf( "fgetpos() error.\n" );
         break;
      }
       
/* Read tracebuf headers until error or EOF
   ****************************************/
      if ( fread( waveHead, sizeof(TRACE_HEADER), 1, fpCta ) != 1 )
      {
         if ( ferror( fpCta ) ) printf( "fread() error.\n" );
         if ( feof( fpCta ) ) printf( "Finished updating the cta file.\n" );
         break;
      }

/* Is this message in the SCN list?
   ********************************/
      for ( j = 0; j < nScn; j++ )
         if ( !strcmp( scn[j].sta, waveHead->sta  ) &&
              !strcmp( scn[j].cmp, waveHead->chan ) &&
              !strcmp( scn[j].net, waveHead->net  ) ) break;

      byteOrder = waveHead->datatype[0];

/* Yes, this message is in the list.
   Set pinno to the correct value.
   ********************************/
      if ( j < nScn )
      {
/*       printf( "%-5s %-3s %-2s", scn[j].sta, scn[j].cmp, scn[j].net );
         printf( " %4d\n", scn[j].pinno ); */

         waveHead->pinno = scn[j].pinno;
#if defined( _SPARC )
         if ( byteOrder == 'i' ) SwapInt( &waveHead->pinno );
#elif defined( _INTEL )
         if ( byteOrder == 's' ) SwapInt( &waveHead->pinno );
#endif

/* Set file pointer back to beginning to trace header.
   Write edited trace header back to the cta file.
   **************************************************/
         if ( fsetpos( fpCta, &pos ) )
         {
            printf( "fsetpos() error.\n" );
            break;
         }
         if ( fwrite( waveHead, sizeof(TRACE_HEADER), 1, fpCta ) != 1 )
         {
            printf( "fwrite() error.\n" );
            break;
         }
      }

/* Skip forward to next tracebuf message
   *************************************/
      nsamp = waveHead->nsamp;
#if defined( _SPARC )
      if ( byteOrder == 'i' ) SwapInt( &nsamp );
#elif defined( _INTEL )
      if ( byteOrder == 's' ) SwapInt( &nsamp );
#endif
      nDataBytes = nsamp * (waveHead->datatype[1] - 48);

      if ( fseek( fpCta, nDataBytes, SEEK_CUR ) != 0 )
      {
         printf( "fseek() error.\n" );
         fclose( fpCta );
         exit( -1 );
      }
   }                    /* End of tracebuf processing loop */
   fclose( fpCta );
   return;
}
