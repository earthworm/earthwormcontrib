
/*   update_cta.h    */

#define UPDATECTA_FAILURE  -1
#define UPDATECTA_SUCCESS   0
#define MAXSCN            800
#define MAXCHAR            80    /* Max chars in file names */

typedef struct
{
   char sta[6];           /* Station */
   char cmp[4];           /* Component */
   char net[3];           /* Network */
   char pad[3];
   int  pinno;            /* Pin number */
} SCN;

