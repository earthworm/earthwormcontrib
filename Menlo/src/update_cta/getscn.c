
       /**********************************************************
        *                     GetScnList()                       *
        *                                                        *
        *  Returns UPDATECTA_SUCCESS or UPDATECTA_FAILURE        *
        **********************************************************/

#include <stdio.h>
#include <trace_buf.h>
#include "update_cta.h"

/* Function declaration
   ********************/
int WaveHeaderMakeLocal( TRACE_HEADER * );


void GetScnList( char *scnFile, SCN *scn, int *nScn )
{
   char line[MAXCHAR];
   int  nscn = 0;          /* Number of SCNs found in scn file */
   SCN  scnl;              /* Local scn */
   FILE *fp;               /* Pointer to scn file structure */

/* Open scn file
   *************/
   if ( (fp = fopen( scnFile, "r" )) == NULL )
   {  
      printf( "Error opening scn file: %s. Exiting.\n", scnFile );
      exit( -1 );
   }
       
/* Read pin numbers from scn file
   ******************************/
   while ( fgets( line, MAXCHAR-1, fp ) != NULL )
   {
      if ( strlen( line ) == 0 ) continue;      /* Blank line */
      if ( line[0]      == '#' ) continue;      /* Comment line */

      if ( sscanf( line, "%s%s%s%d", &scnl.sta[0], &scnl.cmp[0], &scnl.net[0], &scnl.pinno ) < 4 )
      {
         printf( "Bad line in scn file:\n%s\n", line );
         printf( "Exiting.\n" );
         exit( -1 );
      }
      if ( nscn >= MAXSCN )
      {
         printf( "Too many scn's. Exiting.\n" );
         exit( -1 );
      }
      memcpy( &scn[nscn++], &scnl, sizeof(SCN) );

   }               /* End of loop that processes one line in scn file */

   fclose( fp );
   *nScn = nscn;
   return;
}


void SortScnList( SCN *scn, int nScn )
{
   int i;
   int j;
   SCN temp;

   if ( nScn < 2 ) return;       /* Nothing to sort */

   for ( i = 0; i < nScn-1; i++ )
      for ( j = i+1; j < nScn; j++ )
      {
         if ( strcmp( scn[i].sta, scn[j].sta ) < 0 ) continue;
         if ( strcmp( scn[i].sta, scn[j].sta ) == 0 )
         {
            if ( strcmp( scn[i].cmp, scn[j].cmp ) < 0 ) continue;
            if ( strcmp( scn[i].cmp, scn[j].cmp ) == 0 )
               if ( strcmp( scn[i].net, scn[j].net ) < 0 ) continue;
         }
         temp   = scn[i];
         scn[i] = scn[j];
         scn[j] = temp;
      }
}
