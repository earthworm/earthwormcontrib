#
#                     Make file for update_cta
#                         Solaris Version
#

CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib


OBJ = update_cta.o getscn.o update.o makelocal.o \
      $L/swap.o

update_cta: $(OBJ)
	cc -o $B/update_cta $(OBJ) -mt -lm -lposix4 -lthread -lc


# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/update_cta*
