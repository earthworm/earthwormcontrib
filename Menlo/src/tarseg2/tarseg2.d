#
#                  tarseg2's Configuration File
#
MyModuleId    MOD_TARSEG2   # module id for this program,
RingName        HYPO_RING   # transport ring to write to,
LogFile                 1   # If 0, don't write logfile at all,
HeartBeatInterval      30   # Send heartbeats to statmgr this often (sec)

# Directory containing SEG2 hourly directories
TopLevelDir   /home/earthworm/run-will/seg2

# Directory for temporary storage of tar files
TarTempDir    /home/earthworm/run-will/tempfiles/tartmp

# Where to put the resulting tar files
TarOutDir     /home/earthworm/run-will/seg2tar

# What is the hourly directory name format?
FileNameFormat  1        # 1 = seg2fnc: format YYYYMMDD-HH
                         # 2 =  ew2cta: format wuYYYYMMDDHH

# THE END
