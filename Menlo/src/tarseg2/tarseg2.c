
      /**********************************************************************
       *                          tarseg2 program                           *
       *                                                                    *
       *  On the SAFOD tape backup systems, SEG2 files are stored in        *
       *  directories that contain one hour's worth of data.                *
       *  tarseg2 waits until a directory is full.  Then, it tars the       *
       *  directory to a single tar file.  Finally, it deletes the          *
       *  original directory.                                               *
       **********************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <dirent.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>

#define CMD_BUFLEN  256
#define MAX_FNL     256

#define SEG2FNC_NAMES 1    
#define EW2CTA_NAMES  2

/* Function prototypes
 *********************/
void GetConfig( char * );
void LogConfig( void );
int  SendStatus( unsigned char, short, char * );
void TestSEG2dirs( void );

/* Things to read or derive from config file
   *****************************************/
static unsigned char MyModId;            /* Tarseg2's module id               */
static long     RingKey;                 /* Key of transport ring to write to */
static int      LogFile;                 /* If 0, don't log to disk */
static int      HeartBeatInterval;       /* Send heartbeats to statmgr this often (sec) */
static char     TopLevelDir[MAX_FNL];    /* Directory containing SEG2 hourly directories */
static char     TarTempDir[MAX_FNL];     /* For temporary storage of tar files */
static char     TarOutDir[MAX_FNL];      /* Where to put resulting tar files */
static int      FileNameFormat;          /* which program created file names */ 

/* Things to look up in the earthworm.h tables with getutil.c functions
   ********************************************************************/
static unsigned char LocalInstId;
static unsigned char TypeHeartBeat;
static unsigned char TypeError;

SHM_INFO region;
static pid_t myPid;


int main( int argc, char *argv[] )
{
   time_t t_heart_prev;
   time_t t_test_prev;
   int    testInterval = 5;    /* Wait this many seconds before testing again */

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
        printf( "Usage: tarseg2 <configfile>\n" );
        return 0;
   }

/* Initialize name of log file & open it
   *************************************/
   logit_init( argv[1], 0, 256, 1 );

/* Read configuration file
   ***********************/
   GetConfig( argv[1] );

/* Look up local installation id
   *****************************/
   if ( GetLocalInst( &LocalInstId ) != 0 )
   {
      logit( "e", "tarseg2: Error getting local installation id. Exiting.\n" );
      return -1;
   }

/* Look up message types from earthworm.h tables
   *********************************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      logit( "e", "tarseg2: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      logit( "e", "tarseg2: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      return -1;
   }

/* Initialize logging and log the config file
   ******************************************/
   logit_init( argv[1], 0, 256, LogFile );
   logit( "" , "tarseg2: Read command file <%s>\n", argv[1] );
   LogConfig();

/* Get my own pid for restart purposes
   ***********************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "tarseg2: Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Attach to shared memory ring
   ****************************/
   tport_attach( &region, RingKey );

/* Send the first heartbeat
   ************************/
   time( &t_heart_prev );
   time( &t_test_prev );
   t_test_prev -= testInterval;

   if ( SendStatus( TypeHeartBeat, 0, "" ) != PUT_OK )
   {
      logit( "t", "tarseg2:  Error sending heartbeat to ring. Exiting.\n");
      return -1;
   }

/* Loop until kill flag is set
   ***************************/
   while ( 1 )
   {
      time_t t_heart;
      time_t t_test;

/* Check kill flag
   ***************/
      if ( tport_getflag( &region ) == TERMINATE  ||
           tport_getflag( &region ) == myPid )
      {
         tport_detach( &region );
         logit( "et", "tarseg2: Termination requested. Exiting.\n" );
         return 0;
      }

/* Send heartbeat every HeartBeatInterval seconds
   **********************************************/
      time( &t_heart );
      if ( t_heart - t_heart_prev > HeartBeatInterval )
      {
         if ( SendStatus( TypeHeartBeat, 0, "" ) != PUT_OK )
            logit( "et", "tarseg2:  Error sending heartbeat to ring.\n");
         t_heart_prev = t_heart;
      }

/* See if any SEG2 directories have complete data.
   If so, tar the earliest SEG2 directory to a file
   and delete the directory.
   ************************************************/
      time( &t_test );
      if ( t_test - t_test_prev > testInterval )
      {
         TestSEG2dirs();
         t_test_prev = t_test;
      }
      sleep_ew( 1000 );
   }
}


     /********************************************************
      *                   TestSEG2dirs()                     *
      * See if any SEG2 hourly directories are complete.     *
      * If so, tar each directory to a single tar file and   *
      * erase the directory.                                 *
      ********************************************************/

void TestSEG2dirs( void )
{
   DIR    *dp;
   struct dirent *dentp;
   int    ndir = 0;
   char   earlydir[16];

/* Open the top-level directory
   ****************************/
   dp = opendir( TopLevelDir );
   if ( dp == NULL )
   {
      printf( "Error opening top-level directory: %s\n", TopLevelDir );
      printf( "Exiting.\n" );
      exit( 0 );
   }

   if     ( FileNameFormat == SEG2FNC_NAMES ) strcpy( earlydir, "30000101-00"  );
   else if( FileNameFormat == EW2CTA_NAMES  ) strcpy( earlydir, "wu3000010100" );

/* Get the name of the earliest hourly directory
   *********************************************/
   while (  dentp = readdir( dp ) )
   {
      struct stat buf;
      char   fullPath[MAX_FNL];
      int    rc;
      int    year, mon, day, hour;

/* Ignore non-directories
   **********************/
      sprintf( fullPath, "%s/%s", TopLevelDir, dentp->d_name );
      stat( fullPath, &buf );
      if ( !S_ISDIR(buf.st_mode) ) continue;

/* Read year-month-day-hour from hourly directory name */
/* Ignore non-date-based directory (name must contain a valid date).
   *****************************************************************/
      if( FileNameFormat == SEG2FNC_NAMES )      /* 11 chars: YYYYMMDD-HH */
      {
         if ( strlen(dentp->d_name) != 11 ) continue;
         rc = sscanf( dentp->d_name, "%4d%2d%2d-%2d", &year, &mon, &day, &hour );
         if ( rc < 4 ) continue;
         if ( dentp->d_name[8] != '-' ) continue;
      } 
      else if( FileNameFormat == EW2CTA_NAMES )  /* 12 chars: wuYYYYMMDDHH */
      {
         if ( strlen(dentp->d_name) != 12 ) continue;
         rc = sscanf( dentp->d_name, "wu%4d%2d%2d%2d", &year, &mon, &day, &hour );
         if ( rc < 4 ) continue;
      } 
      else continue;

      if ( (year < 2000) || (year > 3000) ) continue;
      if ( ( mon < 1)    || ( mon > 12)   ) continue;
      if ( ( day < 1)    || ( day > 31)   ) continue;
      if ( (hour < 0)    || (hour > 23)   ) continue;
      ndir++;
      if ( strcmp(dentp->d_name, earlydir) < 0 )
         strcpy( earlydir, dentp->d_name );
   }

/* If there is more than one directory,
   tar and erase the earliest one
   ***********************************/
   if ( ndir > 1 )
   {
      char tartempfile[MAX_FNL];
      char seg2dir[MAX_FNL];
      char tarfile[MAX_FNL];
      char syscmd[CMD_BUFLEN];

      snprintf( tartempfile, MAX_FNL, "%s/%s.tar", TarTempDir, earlydir );
      snprintf( seg2dir, MAX_FNL, "%s/%s", TopLevelDir, earlydir );
      logit( "t", "Tarring %s\n", seg2dir );
      snprintf( syscmd, CMD_BUFLEN, "/bin/tar cf %s %s", tartempfile, seg2dir );
      system( syscmd );

/* Move the tar file to its final destination
   ******************************************/
      snprintf( tarfile, MAX_FNL, "%s/%s.tar", TarOutDir, earlydir );
      snprintf( syscmd, CMD_BUFLEN, "/bin/mv %s %s", tartempfile, tarfile );
      system( syscmd );
      logit( "t", "Tarring complete\n" );

/* Erase the original hourly SEG2 directory
   ****************************************/
      sprintf( syscmd, "/bin/rm -r %s", seg2dir );
      system( syscmd );
   }

   closedir( dp );
   return;
}


         /**************************************************
          *                   SendStatus                   *
          *  Builds heartbeat or error msg and puts it in  *
          *  shared memory.                                *
          **************************************************/

int SendStatus( unsigned char msg_type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   int         res;
   long        size;
   time_t      t;

   logo.instid = LocalInstId;
   logo.mod    = MyModId;
   logo.type   = msg_type;

   time( &t );

   if( msg_type == TypeHeartBeat )
      sprintf ( msg, "%ld %d\n", t, myPid );
   else if( msg_type == TypeError )
      sprintf ( msg, "%ld %d %s\n", t, ierr, note);

   size = strlen( msg );           /* don't include null byte in message */
   res = tport_putmsg( &region, &logo, size, msg );

   if ( msg_type == TypeError )
      logit( "t", "%s\n", note );

   return res;
}


   /*************************************************************
    * GetConfig() processes command file using kom.c functions  *
    *             Exits if any errors are detected              *
    *************************************************************/
#define NCOMMAND 8

void GetConfig(char *configfile)
{
   char init[NCOMMAND];  /* init flags, one for each required command */
   int  nmiss;           /* number of required commands that were missed */
   char *com;
   char *str;
   char blank = ' ';
   int  nfiles;
   int  success;
   int  i;

/* Set to zero one init flag for each required command
 *****************************************************/
   for ( i = 0; i < NCOMMAND; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      logit( "e", "tarseg2: Error opening command file <%s>. Exiting.\n",
               configfile );
      exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )   /* While there are command files open */
   {
      while ( k_rd() )    /* Read next line from active file  */
      {
         com = k_str();   /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )          continue;
         if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
         if ( com[0] == '@' )
         {
            success = nfiles+1;
            nfiles  = k_open(&com[1]);
            if ( nfiles != success )
            {
               logit( "e", "tarseg2: Error opening command file <%s>. Exiting.\n",
                        &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Read module id for this program
   *******************************/
         if ( k_its( "MyModuleId" ) )
         {
             str = k_str();
             if ( str )
             {
                if ( GetModId( str, &MyModId ) < 0 )
                {
                   logit( "e", "tarseg2: Invalid MyModuleId <%s> in <%s>. Exiting.\n",
                            str, configfile );
                   exit( -1 );
                }
             }
             init[0] = 1;
         }

/* Name of transport ring to write to
   **********************************/
         else if( k_its( "RingName" ) )
         {
             str = k_str();
             if ( str )
             {
                if ( ( RingKey = GetKey(str) ) == -1 )
                {
                   logit( "e", "tarseg2: Invalid RingName <%s> in <%s>. Exiting.\n",
                            str, configfile );
                   exit( -1 );
                }
             }
             init[1] = 1;
         }

/* Set Logfile switch
   ******************/
         else if( k_its( "LogFile" ) )
         {
             LogFile = k_int();
             init[2] = 1;
         }

/* Set heartbeat interval
   **********************/
         else if( k_its( "HeartBeatInterval" ) )
         {
             HeartBeatInterval = k_int();
             init[3] = 1;
         }

/* Set directory names
   *******************/
         else if( k_its( "TopLevelDir" ) )
         {
             str = k_str();
             if ( str )
             {
                strncpy( TopLevelDir, str, MAX_FNL );
                init[4] = 1;
             }
         }

         else if( k_its( "TarTempDir" ) )
         {
             str = k_str();
             if ( str )
             {
                strncpy( TarTempDir, str, MAX_FNL );
                init[5] = 1;
             }
         }

         else if( k_its( "TarOutDir" ) )
         {
             str = k_str();
             if ( str )
             {
                strncpy( TarOutDir, str, MAX_FNL );
                init[6] = 1;
             }
         }

         else if( k_its( "FileNameFormat" ) )
         {
             FileNameFormat = k_int();
             if     ( FileNameFormat == SEG2FNC_NAMES ) init[7] = 1;
             else if( FileNameFormat == EW2CTA_NAMES  ) init[7] = 1;
             else {
               logit( "e", "tarseg2: Invalid <FileNameFormat> value %d in <%s>; \n",
                      FileNameFormat, configfile );
               exit( -1 );
             }
         }

         else
         {
             logit( "e", "tarseg2: <%s> unknown command in <%s>.\n",
                     com, configfile );
             continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            logit( "e", "tarseg2: Bad <%s> command in <%s>; \n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < NCOMMAND; i++ )
      if ( !init[i] ) nmiss++;

   if ( nmiss )
   {
       logit( "e", "tarseg2: ERROR, no " );
       if ( !init[0] ) logit( "e", "<MyModuleId> " );
       if ( !init[1] ) logit( "e", "<RingName> "   );
       if ( !init[2] ) logit( "e", "<LogFile> "    );
       if ( !init[3] ) logit( "e", "<HeartBeatInterval> " );
       if ( !init[4] ) logit( "e", "<TopLevelDir> " );
       if ( !init[5] ) logit( "e", "<TarTempDir> " );
       if ( !init[6] ) logit( "e", "<TarOutDir> " );
       if ( !init[7] ) logit( "e", "<FileNameFormat> " );
       logit( "e", "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }
   return;
}


   /*************************************************************
    * LogConfig()  Log the configuration file parameters.       *
    *************************************************************/

void LogConfig( void )
{
   logit( "", "MyModId:           %u\n", MyModId );
   logit( "", "RingKey:           %d\n", RingKey );
   logit( "", "HeartBeatInterval: %d\n", HeartBeatInterval );
   logit( "", "LogFile:           %d\n", LogFile );
   logit( "", "TopLevelDir:       %s\n", TopLevelDir );
   logit( "", "TarTempDir:        %s\n", TarTempDir );
   logit( "", "TarOutDir:         %s\n", TarOutDir );
   logit( "", "\n" );
   return;
}

