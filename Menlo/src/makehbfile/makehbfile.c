
  /*********************************************************************
   *                             makehbfile                            *
   *    Make heartbeat files that are sent by sendfile to getfile.     *
   *********************************************************************/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "makehbfile.h"

void GetConfig( char * );
void LogConfig( void );
int  chdir_ew( char * );
int  mkdir_ew( char * );
int  rename_ew( char *, char * );
void sleep( int sec );


int main( int argc, char *argv[] )
{
   extern char Path[PATHLEN];        /* Store heartbeat files here */
   char   tempDirName[] = "temp.dir";
   char defaultConfig[] = "makehbfile.d"; 
   char *configFileName = (argc > 1 ) ? argv[1] : &defaultConfig[0];

/* Read the configuration file
   ***************************/
   GetConfig( configFileName );
/* LogConfig(); */
 
/* Change working directory to "Path"
   *********************************/
   if ( chdir_ew( Path ) == -1 )
   {
      printf( "Error. Can't change working directory to %s\n", Path );
      printf( "Exiting.\n" );
      return -1;
   }

/* Make a directory to contain temporary copies of heartbeat
   files.  The directory is a subdirectory of "Path".
   *********************************************************/
   if ( mkdir_ew( tempDirName ) == -1 )
   {
      printf( "Error. Can't create temporary directory %s\n", tempDirName );
      printf( "Exiting.\n" );
      return -1;
   }
 
/* Change working directory to the temporary directory
   ***************************************************/
   if ( chdir_ew( tempDirName ) == -1 )
   {
      printf( "Error. Can't change working directory to %s\n", tempDirName );
      printf( "Exiting.\n" );
      return -1;
   }


/* Open the file for writing only.
   If an error occurs, sleep a while and try again.
   ***********************************************/
   while ( 1 )
   {
      extern char HbName[80];    /* Name of heartbeat files */
      extern int Interval;

      FILE *fp = fopen( HbName, "wb" );

      if ( fp == NULL )
      {
         printf( "Error opening heartbeat file %s\n", HbName );
         printf( "Exiting.\n" );
         return -1;
      }

/* Write current time to the heartbeat file
   ****************************************/
      if ( fprintf( fp, "%d\n", time(0) ) < 0 )
         printf( "Error writing heartbeat file.\n" );
      fclose( fp );

/* Move the heartbeat file to the parent directory (ie "Path")
   ***********************************************************/
      if ( rename_ew( HbName, ".." ) == -1 )
         printf( "Error. Can't move heartbeat file.\n" );

      sleep( Interval );
   }
}
