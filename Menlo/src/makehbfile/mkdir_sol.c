/*
 *   mkdir_sol.c  Solaris version
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>


/******************************************************************
 *  mkdir_ew( )  Create a new directory                           *
 *  permissions are "777"                                         *
 ******************************************************************/

int mkdir_ew( char *path )
{
   mode_t mode = S_IRWXU | S_IRWXG | S_IRWXO;
   int    rc   = mkdir( path, mode );

   return ((rc == -1) && (errno != EEXIST)) ? -1 : 0;
}

