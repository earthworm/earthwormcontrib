/*
 *   mkdir_win.c  Windows version
 */
#include <stdio.h>
#include <windows.h>


/******************************************************************
 *  mkdir_ew( )  Create a new directory                           *
 *  Returns -1 if the directory can't be created; 0 otherwise     *
 ******************************************************************/

int mkdir_ew( char *path )
{
   DWORD lastError;

   if ( CreateDirectory( path, NULL ) ) return 0;   /* Success */

   lastError = GetLastError();                      /* Failure. Get error number. */

   if ( lastError == ERROR_ALREADY_EXISTS ) return 0;

   printf( "CreateDirectory error: %d\n", lastError );
   return -1;
}

