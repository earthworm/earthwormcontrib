/*
 *   rename_win.c
 */
#include <windows.h>
#include <string.h>


/**********************************************************************
 *  rename_ew( )  Moves a file                   Windows version      *
 *  path1 = file to be moved.  path2 = destination directory          *
 *  Returns -1 if an error occurred; 0 otherwise                      *
 **********************************************************************/

int rename_ew( char *path1, char *path2 )
{
   char path3[80];

   strcpy( path3, path2 );
   strcat( path3, "\\" );
   strcat( path3, path1 );
   if ( MoveFileEx( path1, path3, MOVEFILE_REPLACE_EXISTING ) == 0 )
      return -1;

   return 0;
}

