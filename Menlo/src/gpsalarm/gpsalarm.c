
   /****************************************************************
    *                       GpsAlarm Program                       *
    *                                                              *
    *  The Truetime XL-AK GPS receiver continuously calculates     *
    *  the worst-case time error (wcte) of its internal clock,     *
    *  even if satellite reception is interrupted.  In this case,  *
    *  the wcte will increase at the rate of about 0.8             *
    *  milliseconds per day.  This program periodically checks     *
    *  the wcte and reports to statmgr, if the wcte exceeds some   *
    *  threshold value.  GPS reception may be poor at microwave    *
    *  transmitter sites and locations with excessive radio        *
    *  interference.                                               *
    *                                                              *
    *  GpsAlarm sends TYPE_SNW messages to the shared-memory ring. *
    *  These can be passed to Seisnet Watch.                       *
    ****************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>

#define ERR_WCTE_TOO_HIGH   0
#define ERR_CANT_GET_WCTE   1
#define NOTESIZE          120
#define GET_WCTE_SUCCESS    0

/* Global variables
   ****************/
static long          RingKey;          /* Transport ring key */
static unsigned char MyModId;          /* gpsalarm's module id */
static int           LogSwitch;        /* If 0, no disk logging */
static double        MaxWCTE;          /* Report when worst-case time */
                                       /*    error exceeds this value. */
static int           CheckInterval;    /* Check worst-case time error this often */
static int           ComPort;          /* Com port of GPS receiver */
static int           BaudRate = 9600;  /* Baud rate of GPS receiver */
static int           HeartBeatInterval;
static char          StaName[6];       /* Station name */
static char          NetCode[3];       /* Network code */
static pid_t         myPid;            /* For restarts by startstop */
static unsigned char InstId;           /* Local installation id  */
static unsigned char TypeSNW;          /* Msg type for Seisnet Watch msgs */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static SHM_INFO      Region;

int InitPort( int ComPort, int BaudRate );
int DisableCTR( void );
int GetWCTE( double *wcte );

static int SendSNW( char note[] );


         /*************************************************
          *                 ReportStatus()                *
          *  Builds a heartbeat or error message and      *
          *  puts it in shared memory.                    *
          *************************************************/


int ReportStatus( unsigned char type,
                  short         ierr,
                  char          *note )
{
   MSG_LOGO logo;
   char     msg[256];
   int      res;
   long     size;
   time_t   t;

   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if ( type == TypeHeartBeat )
           sprintf ( msg, "%ld %d\n", (long)t, myPid);
   else if( type == TypeError )
   {
           sprintf ( msg, "%ld %d %s\n", (long)t, ierr, note);
   }

   size = strlen( msg );  /* don't include null byte in message */
   res  = tport_putmsg( &Region, &logo, size, msg );

   return res;
}


       /***************************************************
        *                   GetConfig()                   *
        *  Reads the configuration file.                  *
        *  Exits if any errors are encountered.           *
        ***************************************************/

void GetConfig( char *configfile )
{
   const int ncommand = 7;  /* Number of commands to process */
   char      init[10];      /* Flags, one for each command */
   int       nmiss;         /* Number of commands that were missed */
   char      *com;
   char      *str;
   int       nfiles;
   int       success;
   int       i;

/* Set Default Values
   ******************/
   strcpy( StaName, "--" );
   strcpy( NetCode, "--" );

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
        printf( "Error opening gpsalarm command file <%s>. Exiting.\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )      /* While there are command files open */
   {
        while ( k_rd() )     /* Read next line from active file  */
        {
           com = k_str();   /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
           if ( !com )          continue;
           if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
           if ( com[0] == '@' )
           {
              success = nfiles + 1;
              nfiles  = k_open( &com[1] );
              if ( nfiles != success )
              {
                 printf( "Error opening gpsalarm command file <%s>. Exiting.\n",
                     &com[1] );
                 exit( -1 );
              }
              continue;
           }

           if ( k_its( "MyModuleId" ) )
           {
              str = k_str();
              if ( str )
              {
                 if ( GetModId( str, &MyModId ) < 0 )
                 {
                    printf( "gpsalarm: Invalid MyModuleId <%s> in <%s>",
                             str, configfile );
                    printf( ". Exiting.\n" );
                    exit( -1 );
                 }
              }
              init[0] = 1;
           }

           else if( k_its( "RingName" ) )
           {
              str = k_str();
              if (str)
              {
                 if ( ( RingKey = GetKey(str) ) == -1 )
                 {
                    printf( "gpsalarm: Invalid RingName <%s> in <%s>",
                             str, configfile );
                    printf( ". Exiting.\n" );
                    exit( -1 );
                 }
              }
              init[1] = 1;
           }

           else if( k_its( "MaxWCTE" ) )
           {
              MaxWCTE = k_val();
              init[2] = 1;
           }

           else if( k_its( "LogFile" ) )
           {
              LogSwitch = k_int();
              init[3] = 1;
           }

           else if( k_its( "HeartBeatInterval" ) )
           {
              HeartBeatInterval = k_int();
              init[4] = 1;
           }

           else if( k_its( "CheckInterval" ) )
           {
              CheckInterval = k_int();
              init[5] = 1;
           }

           else if( k_its( "ComPort" ) )
           {
              ComPort = k_int();
              init[6] = 1;
           }

           else if( k_its( "BaudRate" ) )     /* Optional */
           {
              BaudRate = k_int();
           }

           else if( k_its( "StaName" ) )      /* Optional */
           {
              str = k_str();
              if (str)
              {
                 if ( strlen(str) > 5 )
                 {
                    printf( "gpsalarm: Error. StaName is more than five characters long.\n" );
                    printf( "Length: %d   Exiting.\n", strlen(str) );
                    exit( -1 );
                 }
                 strcpy( StaName, str );
              }
           }

           else if( k_its( "NetCode" ) )      /* Optional */
           {
              str = k_str();
              if (str)
              {
                 if ( strlen(str) > 2 )
                 {
                    printf( "gpsalarm: Error. NetCode is more than two characters long.\n" );
                    printf( "Length: %d   Exiting.\n", strlen(str) );
                    exit( -1 );
                 }
                 strcpy( NetCode, str );
              }
           }

           else
           {
              printf( "gpsalarm: <%s> unknown command in <%s>.\n",
                       com, configfile );
              continue;
           }

/* See if there were any errors processing the command
   ***************************************************/
           if ( k_err() )
           {
              printf( "gpsalarm: Bad <%s> command in <%s>; \n",
                       com, configfile );
              exit( -1 );
           }
        }
        nfiles = k_close();
   }

/* Check flags for missed commands
   *******************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if( !init[i] ) nmiss++;

   if ( nmiss )
   {
      printf( "gpsalarm: ERROR, no " );
      if ( !init[0] ) printf( "<MyModuleId> "        );
      if ( !init[1] ) printf( "<RingName> "          );
      if ( !init[2] ) printf( "<MaxWCTE> "           );
      if ( !init[3] ) printf( "<LogFile> "           );
      if ( !init[4] ) printf( "<HeartBeatInterval> " );
      if ( !init[5] ) printf( "<CheckInterval>     " );
      if ( !init[6] ) printf( "<ComPort> "           );
      printf( "command(s) in <%s>. Exiting.\n", configfile );
      exit( -1 );
   }
   return;
}


       /***************************************************
        *                   LogConfig()                   *
        *  Log the configuration file parameters.         *
        ***************************************************/

void LogConfig( void )
{
   logit( "", "\n" );
   logit( "", "MyModId:           %u\n",    MyModId );
   logit( "", "RingKey:           %ld\n",   RingKey );
   logit( "", "HeartBeatInterval: %d\n",    HeartBeatInterval );
   logit( "", "LogSwitch:         %d\n",    LogSwitch );
   logit( "", "MaxWCTE:           %.2lf\n", MaxWCTE );
   logit( "", "CheckInterval:     %d\n",    CheckInterval );
   logit( "", "ComPort:           %d\n",    ComPort );
   logit( "", "BaudRate:          %d\n",    BaudRate );
   logit( "", "StaName:           %s\n",    StaName );
   logit( "", "NetCode:           %s\n",    NetCode );
   logit( "", "\n" );
   return;
}



           /****************************************
            *       Main program starts here       *
            ****************************************/

int main( int argc, char *argv[] )
{

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
        printf( "Usage: gpsalarm <configfile>\n" );
        return 0;
   }

/* Read configuration file
   ***********************/
   GetConfig( argv[1] );

/* Look up installation id
   ***********************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "gpsalarm: Error getting installation id. Exiting.\n" );
      return -1;
   }

/* Look up message types from earthworm.h tables
   *********************************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "gpsalarm: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "gpsalarm: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_SNW", &TypeSNW ) != 0 )
   {
      printf( "gpsalarm: Invalid message type <TYPE_SNW>. Exiting.\n" );
      return -1;
   }


/* Initialize log file and log configuration parameters
   ****************************************************/
   logit_init( argv[1], (short)MyModId, 256, LogSwitch );
   logit( "" , "Read configuration file <%s>\n", argv[1] );
   LogConfig();

/* Get process ID for heartbeat messages
   *************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "gpsalarm: Cannot get pid. Exiting.\n" );
      return -1;
   }
   logit( "", "My process id: %u\n", myPid );

/* Attach to shared memory ring
   ****************************/
   tport_attach( &Region, RingKey );

/* Initialize the serial port.
   Exit on error.
   **************************/
   if ( InitPort( ComPort, BaudRate ) == -1 )
   {
      logit( "e", "InitPort() error. Exiting.\n" );
      return -1;
   }
   logit( "et", "Serial port %d initialized.\n", ComPort );

/* Disable continuous time reporting from the GPS receiver.
   By default, continuous time reporting is enabled.
   *******************************************************/
   if ( DisableCTR() == -1 )
   {
      logit( "e", "DisableCTR() error. Exiting.\n" );
      return -1;
   }
   logit( "et", "Continuous GPS time reporting disabled.\n" );

/* Loop until kill flag is set
   ***************************/
   while ( 1 )
   {
      static time_t tHeart = 0;  // When the last heartbeat was sent
      static time_t tCheck = 0;  // When the worst-case time error was last checked
      static int    gpsOk  = 1;  // Assume GPS is working at program startup
      time_t now = time(0);      // The current time
      double wcte;               // Worst-case time error, in seconds
      char   note[120];          // For statmgr messages

/* Check kill flag
   ***************/
      if ( tport_getflag( &Region ) == TERMINATE ||
           tport_getflag( &Region ) == myPid )
      {
         tport_detach( &Region );
         logit( "t", "gpsalarm termination requested. Exiting.\n" );
         return 0;
      }

/* Send heartbeat every HeartBeatInterval seconds
   **********************************************/
      if ( (now - tHeart) >= (time_t)HeartBeatInterval )
      {
         tHeart = now;
         if ( ReportStatus( TypeHeartBeat, 0, "" ) != PUT_OK )
            logit( "t", "Error sending heartbeat to ring.\n");
      }

/* Get worst-case time error from GPS every CheckInterval seconds
   **************************************************************/
      if ( (now - tCheck) >= (time_t)CheckInterval )
      {
         tCheck = now;

         if ( GetWCTE(&wcte) == GET_WCTE_SUCCESS )
         {
            logit( "et", "Worst-case time error: %.8lf seconds\n", wcte );

/* Create a TYPE_SNW message and send it to the ring
   *************************************************/
            if ( strcmp(StaName,"--") != 0 && strcmp(NetCode,"--") != 0 )
            {
               int src;
               double wcteMicsec = 1.0E6 * wcte;

               src = _snprintf( note, NOTESIZE, "%s-%s:1:GPS Worst-case Time Error (microsec)=%.0lf\n",
                        NetCode, StaName, wcteMicsec );
               if ( src < 0 )
                  logit( "et", "_snprintf() buffer too small (%d characters).\n", NOTESIZE );
               if ( SendSNW( note ) != PUT_OK )
                  logit( "et", "Error sending SNW message to ring.\n" );
            }

/* Report to statmgr if the wcte is too high or if the GPS recovers from an outage
   *******************************************************************************/
            if ( (wcte > MaxWCTE) && gpsOk )
            {
               sprintf( note, "ALARM: GPS worst-case time error > %.8lf seconds", MaxWCTE );
               logit( "et", "%s\n", note );
               if ( ReportStatus( TypeError, ERR_WCTE_TOO_HIGH, note ) != PUT_OK )
                  logit( "et", "Error sending error message to ring.\n");
               gpsOk = 0;
            }

            if ( (wcte <= MaxWCTE) && !gpsOk )
            {
               sprintf( note, "Alarm cleared. GPS worst-case time error < %.8lf seconds", MaxWCTE );
               logit( "et", "%s\n", note );
               if ( ReportStatus( TypeError, ERR_WCTE_TOO_HIGH, note ) != PUT_OK )
                  logit( "et", "Error sending error message to ring.\n");
               gpsOk = 1;
            }
         }

/* Report to statmgr if GetWCTE() returned an error
   ************************************************/
         else
         {
            sprintf( note, "Error. Can't get worst-case time error from GPS" );
            if ( ReportStatus( TypeError, ERR_CANT_GET_WCTE, note ) != PUT_OK )
               logit( "et", "Error sending error message to ring.\n");
         }
      }
      sleep_ew( 1000 );
   }
}


            /**********************************************
             *                 SendSNW()                  *
             *  Send a SeisnetWatch message to the ring.  *
             **********************************************/

int SendSNW( char note[] )
{
   MSG_LOGO logo;
   int      res;
   long     size;

   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = TypeSNW;

   size = strlen( note );
   res  = tport_putmsg( &Region, &logo, size, note );
   return res;
}
