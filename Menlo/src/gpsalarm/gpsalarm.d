#
#                GpsAlarm's Configuration File
#
MyModuleId   MOD_GPSALARM   # Module id for this program,

RingName        HYPO_RING   # Transport ring to write to,

HeartBeatInterval      30   # Interval between heartbeats sent to statmgr,
                            #   in seconds.

LogFile                 1   # If 0, don't write logfile at all, if 1, do
                            # if 2, write module log but not to stderr/stdout

ComPort                 2   # Com port used by GPS receiver
                            # 1 = com1, 2 = com2, etc

BaudRate             9600   # Baud rate of serial port used by GPS receiver.
                            # Optional command.  Default is 9600.

MaxWCTE             0.001   # Report when worst-case time error exceeds
                            # this value, in seconds

CheckInterval         600   # Check worst-case time error this often,
                            # in seconds

StaName             B11TR   # Station name for Seisnet Watch; max 5 chars (optional)
NetCode                NC   # Network code for Seisnet Watch; max 2 chars (optional)

