
#include <stdio.h>
#include <math.h>
#include <earthworm.h>

#define BUFSIZE 40    /* Max response string is 20 chars */

int WritePort( char * );
int ReadPort( char *, int bufsize );
int PurgeCommInputBuffer( void );



           /********************************************************
            *                       GetWCTE()                      *
            *  Get the worst-case time error of the GPS receiver.  *
            *                                                      *
            *  Returns  0 if all ok                                *
            *          -1 if an error occured                      *
            ********************************************************/


int GetWCTE( double *wcte )
{
   static char buf[BUFSIZE];

/* Clear the comm port input buffer.
   Returns zero on failure.
   ********************************/
   if ( PurgeCommInputBuffer() == 0 )
   {
      logit( "et", "Error purging the comm port input buffer.\n" );
      return -1;
   }

/* Request worst-case time error from GPS receiver.
   \x0D is a carriage return character.
   ***********************************************/
// rc = WritePort( "F13\x00D" );
   if ( WritePort( "F13\r" ) == -1 )
   {
      logit( "et", "Error requesting worst-case time error from GPS receiver.\n" );
      return -1;
   }
   logit( "et", "Requested worst-case time error from GPS.\n" );

/* Read worst-case time error from GPS receiver as a string
   ********************************************************/
   if ( ReadPort( buf, BUFSIZE ) == -1 )
      return -1;

/* Decode worst-case time error
   ****************************/
   if ( strncmp(buf, "F13", 3) == 0 )
   {
      if ( sscanf( &buf[4], "%lf", wcte ) < 1 )
      {
         logit( "et", "Error. Can't decode worst-case time error: %s", &buf[4] );
         return -1;
      }
   }
   else
   {
      logit( "et", "Error. Bad response string from GPS receiver.\n" );
      return -1;
   }
   return 0;
}
