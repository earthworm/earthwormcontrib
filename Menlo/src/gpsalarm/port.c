
       /*******************************************************
        *                        port.c                       *
        *                                                     *
        *  Initialize a serial port.                          *
        *                                                     *
        *  This is Windows-specific code!!!                   *
        *******************************************************/


#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <earthworm.h>

static HANDLE handle;


int InitPort( int ComPort, int BaudRate )
{
   COMMTIMEOUTS timeouts;
   DCB          dcb;                /* Data control block */
   BOOL         success;
   char         portName[20];

/* Open the com port
   *****************/
   if ( ComPort < 1 || ComPort > 18 )
   {
      logit( "et", "Invalid value of ComPort (%d). Exiting.\n", ComPort );
      exit( -1 );
   }
   sprintf( portName, "COM%d", ComPort );
   handle = CreateFile( portName,
                        GENERIC_READ|GENERIC_WRITE,
                        0, 0, OPEN_EXISTING,
                        FILE_ATTRIBUTE_NORMAL, 0 );

   if ( handle == INVALID_HANDLE_VALUE )
   {
      int rc = GetLastError();

      if ( rc == ERROR_ACCESS_DENIED )
         logit( "et", "CreateFile() error. Access denied to port: %s\n", portName );
      else
         logit( "et", "Error %d in CreateFile.\n", rc );
      return -1;
   }
   logit( "t", "CreateFile succeeded for comm port.\n" );

/* Get the current settings of the comm port
   *****************************************/
   success = GetCommState( handle, &dcb );
   if ( !success )
   {
      logit( "et", "Error %d in GetCommState.\n", GetLastError() );
      return -1;
   }

/* Modify the baud rate, flow control, etc
   ***************************************/
   dcb.BaudRate        = BaudRate;
   dcb.ByteSize        = 7;            // Hardwired
   dcb.Parity          = EVENPARITY;   // Hardwired
   dcb.StopBits        = ONESTOPBIT;   // Hardwired

   dcb.fOutxCtsFlow    = FALSE;        // Disable RTS/CTS flow control
   dcb.fOutxDsrFlow    = FALSE;        // Disable DTR/DSR flow control
   dcb.fDsrSensitivity = FALSE;        // Driver ignores DSR
   dcb.fOutX           = FALSE;        // Disable XON/XOFF out flow control
   dcb.fInX            = FALSE;        // Disable XON/XOFF in flow control
   dcb.fNull           = FALSE;        // Don't discard null bytes
   dcb.fAbortOnError   = TRUE;         // Abort rd/wr on error

/* Apply the new comm port settings
   ********************************/
   success = SetCommState( handle, &dcb );
   if ( !success )
   {
      logit( "et", "Error %d in SetCommState.\n", GetLastError() );
      return -1;
   }
   logit( "t", "Comm parameters set.\n" );

/* Change the ReadIntervalTimeout so that ReadFile
   will return immediately.
   ***********************************************/
// timeouts.ReadIntervalTimeout         = 0;
   timeouts.ReadIntervalTimeout         = MAXDWORD;
   timeouts.ReadTotalTimeoutMultiplier  = 0;
   timeouts.ReadTotalTimeoutConstant    = 0;
   timeouts.WriteTotalTimeoutMultiplier = 0;
   timeouts.WriteTotalTimeoutConstant   = 0;
   SetCommTimeouts( handle, &timeouts );
   logit( "t", "Timeout parameters set.\n" );

/* Purge the input comm port buffer
   ********************************/
   success = PurgeComm( handle, PURGE_RXCLEAR );
   if ( !success )
   {
      logit( "et", "Error %d in PurgeComm.\n", GetLastError() );
      return -1;
   }
   logit( "t", "Receive buffer purged.\n" );

   return 0;
}


        /*******************************************************
         *                    WritePort()                      *
         *                                                     *
         *  Write message to GPS receiver.                     *
         *                                                     *
         *  Returns  0 if all ok,                              *
         *          -1 if a write error occurred               *
         *******************************************************/

int WritePort( char str[] )
{
   BOOL  success;
   DWORD numWritten;
   unsigned int nchar = strlen( str );

   success = WriteFile( handle, str, nchar, &numWritten, 0 );
   if ( !success )
   {
      logit( "et", "WriteFile error %d\n", GetLastError() );
      return -1;
   }
   if ( numWritten < nchar )
   {
      logit( "et", "WriteFile error. nchar: %u  numWritten: %u\n", nchar, numWritten );
      return -1;
   }
   return 0;
}


        /*******************************************************
         *                    ReadPort()                       *
         *                                                     *
         *  Read message from GPS receiver.                    *
         *                                                     *
         *  Returns  0 if all ok,                              *
         *          -1 if an error occurred                    *
         *******************************************************/

int ReadPort( char str[], int bufsize )
{
   DWORD  numRead;
   BOOL   success;
   char   chr;
   int    i = 0;
   int    j;
   const  int nSleep   = 150;
   const  int sleepLen = 100;    // Milliseconds
   double totalSleep   = 0.001 * nSleep * sleepLen;

/* Read message from GPS receiver, one character at a time.
   Each message ends with line feed (0x0A).
   *******************************************************/
   for ( j = 0; j < nSleep; j++ )
   {
      success = ReadFile( handle, &chr, 1, &numRead, 0 );
      if ( !success )
      {
         logit( "et", "Error %d returned by ReadFile.\n", GetLastError() );
         return -1;
      }

      if ( numRead == 0 )
      {
         sleep_ew( sleepLen );
         continue;
      }

      str[i++] = chr;
      if ( chr == 0x0A )
      {
         str[i] = 0;
         return 0;
      }

      if ( i >= bufsize )
      {
         logit( "et", "ReadPort input buffer full. bufsize = %d\n" );
         return -1;
      }
   }
   logit( "et", "Error. No response from GPS in %.1lf seconds.\n", totalSleep );
   return -1;
}


        /********************************************************
         *                     DisableCTR()                     *
         *  Disable continuous time reporting.  By default,     *
         *  continuous time reporting is enabled.               *
         ********************************************************/

void DisableCTR( void )
{
   char buf[2];
   char chr;
   int  numRead;
   BOOL success;

/* Wait for characters to show up
   ******************************/
   sleep_ew( 1500 );

/* ReadFile is non-blocking
   ************************/
   success = ReadFile( handle, &chr, 1, &numRead, 0 );
   if ( !success )
      logit( "et", "Error %d in ReadFile.\n", GetLastError() );

/* Turn off continuous reporting mode
   **********************************/
   if ( numRead > 0 )
   {
      buf[0] = 0x03;      // Control-C character
      buf[1] = '\0';
      WritePort( buf );
   }

   return;
}


/* Purge the input comm port buffer.
   Returns zero on failure.
   ********************************/

int PurgeCommInputBuffer( void )
{
   return PurgeComm( handle, PURGE_RXCLEAR );
}
