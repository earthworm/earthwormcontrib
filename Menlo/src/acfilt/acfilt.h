/*****************************************************************************/
/*
 * acfilt.h: Definitions for the acfilt Earthworm Module.
 */
/*****************************************************************************/

/*******                                                        *********/
/*      Redefinition Exclusion                                          */
/*******                                                        *********/
#ifndef __ACF_H__
#define __ACF_H__

#include <transport.h>  
#include <trace_buf.h>
#include <mem_circ_queue.h>

/*******                                                         *********/
/*      Constant Definitions                                             */
/*******                                                         *********/

/*    StatusReport Error Codes - Must coincide with acfilt.desc          */
#define  ERR_MISSMSG       0   /* message missed in transport ring       */
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer     */
#define  ERR_NOTRACK       2   /* msg retrieved; tracking limit exceeded */
#define  ERR_FILTER        3   /* call to the ACFilter routine failed    */
#define  ERR_QUEUE         4   /* trouble with the MsgQueue operation    */

/*    ACFilter Error Codes, returned from FiltOneSCN                     */
#define FILT_NOROOM       -1    /* No room in output buffer              */ 

/*    Buffer Lengths                                                     */
#define MAXFILT         100     /* Maximum number of filters             */
#define MAXFILTLEN      300     /* Maximum length of a filter.           */
#define MAXFILENAMELEN   80     /* Maximum length of a file name.        */
#define MAXLINELEN      240     /* Maximum length of a line read from    */
                                /*   or written to a file.               */
#define MAXMESSAGELEN   160     /* Maximum length of a status or error   */
                                /*   message.                            */
#define BUFFSIZE MAX_TRACEBUF_SIZ * 2 /* Size of a channel trace buffer  */
#define QUEUE_SIZE       500    /* How many msgs can we queue            */
#define THREAD_STACK    8192    /* How big is our thread stack           */

/*******                                                         *********/
/*      Structure Definitions                                            */
/*******                                                         *********/

  /*    Information Retrieved from Earthworm*.h                          */
typedef struct _ACFEWH
{
    unsigned char myInstId;       /* Installation running this module.    */
    unsigned char myModId;        /* ID of this module.                   */
    unsigned char readInstId;     /* Retrieve trace messages from         */
                                  /*   specified installation.            */
    unsigned char readModId;      /* Retrieve trace messages from         */
                                  /*   specified module.                  */
    unsigned char typeError;      /* Error message type.                  */
    unsigned char typeHeartbeat;  /* Heartbeat message type.              */
    unsigned char typeWaveform;   /* Waveform message type.               */
    long  ringInKey;              /* Key to input shared memory region.   */
    long  ringOutKey;             /* Key to output shared memory region.  */
} ACFEWH;

typedef struct _BUFF
{
    double buffer[BUFFSIZE];
    double starttime;              /* time of first datapoint in buffer   */
    double samplerate;
    int    read;                   /* Index to start reading from         */
    int    write;                  /* Index to start writing to           */
} BUFFER; 

typedef struct _FILTER
{
    int    Length;                /* Length of the symmetric filter       */
    double coef[MAXFILTLEN];      /* Array of coefficients for this filter*/
    double TimeCorr;              /* Time correction for this filter      */
} FILTER;

typedef struct _FILFILE
{
    char   filtname[MAXFILENAMELEN]; /* Local name of filter                */
    char   filename[MAXFILENAMELEN]; /* Name of file comntaining filter     */       
    FILTER ThisFilt;                 /* Filter specs                        */
} FILFILE;

  /*    Filter Parameter Structure (read from *.d files)              */
typedef struct _ACFPARAM
{
    char  myModName[MAX_MOD_STR];      /* Name of this instance of the module */
                                       /*   REQUIRED.                         */
    char  readInstName[MAX_INST_STR];  /* Name of installation that is        */
                                       /*  producing trace data messages.     */
    char  readModName[MAX_MOD_STR];    /* Name of module at the above  */
                                       /*   installation that is       */
                                       /*   producing the trace data   */
                                       /*   messages.                  */
    char  ringIn[MAX_RING_STR];        /* Name of ring from which      */
                                       /*   trace data will be read -  */
                                       /* REQUIRED.                    */
    char  ringOut[MAX_RING_STR];       /* Name of ring to which        */
                                       /*   traces will be written -   */
                                       /* REQUIRED.                    */
    int   heartbeatInt;                /* Heartbeat Interval(seconds). */
    int   reportInt;                   /* Report Interval(seconds).    */
    int   logSwitch;                   /* Write logs?                  */
                                       /*   ( 0 = No, 1 = Yes )        */
    int   debug;                       /* Write out debug messages?    */
                                       /*   ( 0 = No, 1 = Yes )        */
    double maxGap;                     /* Gap between tracedata points */
    FILFILE   filtfile[MAXFILT];       /* Filter coefficient structure */
    int       nFilts;                  /* How many filters do we have  */  
} ACFPARAM;

typedef struct _STATION
{
  BUFFER     inBuff;                   /* Filter input buffer            */
  BUFFER     outBuff;                  /* Filter output buffer           */
  double     inEndtime;                /* end time of last sample in     */
  char       inSta[TRACE2_STA_LEN];    /* input Station code (name).     */
  char       inChan[TRACE2_CHAN_LEN];  /* input Channel code.            */
  char       inNet[TRACE2_NET_LEN];    /* input Network code.            */
  char       inLoc[TRACE2_LOC_LEN];    /* input Location code.           */
  char       outSta[TRACE2_STA_LEN];   /* output Station code (name).    */
  char       outChan[TRACE2_CHAN_LEN]; /* output Channel code.           */
  char       outNet[TRACE2_NET_LEN];   /* output Network code.           */
  char       outLoc[TRACE2_LOC_LEN];   /* output Location code.          */
  char       FiltNam[MAXFILENAMELEN];  /* output Location code.          */
  int        FiltInd;                  /* Filter index for this station  */
} STATION;

  /*    Filter World structure                                           */
typedef struct _WORLD
{
    ACFEWH    acfEWH;             /* Structure for Earthworm parameters.  */
    ACFPARAM  acfParam;           /* Network parameters.                  */
    SHM_INFO  regionOut;          /* Output shared memory region info     */
    MSG_LOGO  hrtLogo;            /* Logo of outgoing heartbeat message.  */
    MSG_LOGO  errLogo;            /* Logo of outgoing error message.      */
    MSG_LOGO  trcLogo;            /* Logo of outgoing tracebuf message.   */
    QUEUE     MsgQueue;           /* The message queue                    */
    pid_t     MyPid;              /* My pid for restart from startstop    */
    int       FiltStatus;         /* 0=> Filt thread ok. <0 => dead       */
    int       queue_ind;          /* Number of tracebufs in queue         */
    int       queue_max;          /* Number of tracebufs in queue         */
    int       gap_tot;            /* Number of gaps in stat interval      */
    int       olap_tot;           /* Number of overlaps in stat interval  */
    STATION*  stations;           /* Array of stations in the network     */
    int       nSta;               /* How many stations do we have         */  
    FILTER    filter;             /* Filter coefficient structure         */
    int       pass_through;       /* Flag to pass along unfiltered data   */  
} WORLD;

/* Filter function prototypes                                             */
void    InitializeParameters(WORLD *);
void    InitSta(WORLD *);
int     Configure(WORLD *, char **);
int     ReadConfig(char *, WORLD *);
int     ReadEWH(WORLD *);
int     SetStations(WORLD *);
void    StatusReport(WORLD *, unsigned char, short, char *);
int     matchSCNL(TRACE2_HEADER*, WORLD *);
thr_ret FiltThread(void *);
int     ACFilter(WORLD *, char *, int, char *);
int     FiltOneSCNL(WORLD *, STATION *);
void    ResetStation(STATION *);
int ReadFilt (int j, WORLD* pDcm);


#endif  /*  __ACF_H__                                                  */
