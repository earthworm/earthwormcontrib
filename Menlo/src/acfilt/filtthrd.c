#ifdef _WINNT
#include <windows.h>
#define mutex_t HANDLE
#else
#ifdef _SOLARIS
#ifndef _LINUX    /* synch.h not posix threads */
#include <synch.h>      /* mutex's                                      */
#endif
#endif
#endif

#include <earthworm.h>  /* logit, threads                               */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "acfilt.h"

/*****************************************************************************
 *                                                                           *
 *      Function: FiltThread                                                 *
 *              1) Allocates memory for input and output TRACE_BUF messages  *
 *              2) Retrieves TRACE_BUF messages from their queues            *
 *              3) Determines station index from the queue message           *
 *              4) Dispatches messages to ACFilter                           *
 *                                                                           *
 *      Inputs:         Pointer to World Structure                           *
 *                                                                           *
 *      Outputs:        Message sent to the output ring                      *
 *                                                                           *
 *      Returns:        nothing                                              *
 *                                                                           *
 *****************************************************************************/

thr_ret FiltThread (void* filt)
{
    char  whoami[50];
    WORLD         *pFilt;
    int            ret, jSta;
    MSG_LOGO       reclogo;       /* logo of retrieved message     */
    char          *WaveBuf;       /* string to hold wave message   */
    long           WaveBufLen;    /* length of WaveBuf             */
    char          *outBuf;        /* string to hold comp message   */
    long           outBufLen;     /* length of outBuf              */
    
    pFilt = ( WORLD *) filt;
    
    strcpy(whoami, "acfilt: FiltThread:");
  
  /* Allocate the waveform buffer */
    WaveBufLen = (MAX_TRACEBUF_SIZ + sizeof (int));
    WaveBuf    = (char *) malloc ((size_t) WaveBufLen);
    
    if (WaveBuf == NULL) {
        logit ("e", "%s Cannot allocate waveform buffer\n", whoami);
        pFilt->FiltStatus = -1;
        KillSelfThread();
    }

  /* Allocate the data buffer */
    outBufLen = MAX_TRACEBUF_SIZ;
    outBuf    = (char *) malloc ((size_t) outBufLen);
    
    if (outBuf == NULL) {
        logit ("e", "%s Cannot allocate data buffer\n", whoami);
        pFilt->FiltStatus = -1;
        KillSelfThread();
    }

  /* Tell the main thread we're feeling ok */
      pFilt->FiltStatus = 0;

    while (1) {        /* Get top message from the MsgQueue */
        RequestMutex ();
        ret = dequeue (&(pFilt->MsgQueue), WaveBuf, &WaveBufLen, &reclogo);
        ReleaseMutex_ew ();
        
        if (ret < 0) {                                 /* empty queue */
            sleep_ew (100);
            continue;
        }
        
        if(pFilt->queue_ind > pFilt->queue_max) pFilt->queue_max = pFilt->queue_ind;
        pFilt->queue_ind--; /*decrement queue counter*/
        
        /* Extract the SCN number; recall, it was pasted as an int on the front 
         * of the message by the main thread */
        jSta = *((int*) WaveBuf);
        
        if (ACFilter( pFilt, WaveBuf + sizeof(int), jSta, outBuf) != EW_SUCCESS) {
            logit("et", "%s error from ACFilter; exiting\n", whoami);
            pFilt->FiltStatus = -1;
            KillSelfThread();
        }
    
    } /* while (1) - message dequeuing process */

}


/***************************************************************************** 
 *                                                                           *
 * acfilt.c: Applies acausal filter to trace data                            *
 *              1) Filters TRACE2_BUF messages                               *
 *              2) Fills in outgoing TRACE2_BUF messages                     *
 *              3) Puts outgoing messages on transport ring                  *
 *                                                                           *
 *      Function: ACFilter                                                   *
 *                                                                           *
 *      Inputs:         Pointer to World Structure                           *
 *                      Pointer to incoming TRACE2_BUF message               *
 *                      SCN index of incoming message                        *
 *                      Pointer to outgoing TRACE2_BUF buffer                *
 *                                                                           *
 *      Outputs:        Message sent to the output ring                      *
 *                                                                           *
 *      Returns:        EW_SUCCESS on success, else EW_FAILURE               *
 *****************************************************************************/

int ACFilter (WORLD* pFilt, char *inBuf, int jSta, char *outBuf)
{
    char  whoami[50];
    STATION *this;
    TRACE2_HEADER *waveHead, *outHead;
    short *waveShort, *outShort;
    long  *waveLong, *outLong;
    int   i, ret, datasize, outBufLen;
    
    strcpy(whoami, "acfilt: ACFilter:");
  
  /* Set some useful pointers */
    waveHead  = (TRACE2_HEADER*) inBuf;
    waveShort = (short*)(inBuf + sizeof(TRACE2_HEADER));
    waveLong  = (long*)(inBuf + sizeof(TRACE2_HEADER));
    outHead   = (TRACE2_HEADER*)outBuf;
    outShort  = (short*)(outBuf + sizeof(TRACE2_HEADER));
    outLong   = (long*)(outBuf + sizeof(TRACE2_HEADER));
    this      = &(pFilt->stations[jSta]);
    
    if (pFilt->acfParam.debug)
        logit("t", "enter acfilt with <%s.%s.%s.%s> start: %lf samprate: %lf nsamp: %d  \n", 
            waveHead->sta, waveHead->chan, waveHead->net, waveHead->loc, 
            waveHead->starttime, waveHead->samprate, waveHead->nsamp );
  
  /* If we have previous data, check for data gap */
    if ( this->inEndtime != 0.0 ) {
        if ( (waveHead->starttime - this->inEndtime) * waveHead->samprate > pFilt->acfParam.maxGap ) {
            if (pFilt->acfParam.debug)
            logit("et", "%s gap in data for <%s.%s.%s.%s>:\n"
                "\tlast end: %lf this start: %lf; resetting\n", whoami,
                this->inSta, this->inChan, this->inNet, this->inLoc, 
                this->inEndtime, waveHead->starttime);
            pFilt->gap_tot++;
            ResetStation( this );
        }
        else if (waveHead->starttime < this->inEndtime) {
            if (pFilt->acfParam.debug)
            logit("et", "%s overlapping times for <%s.%s.%s.%s>:\n"
                "\tlast end: %lf this start: %lf; resetting\n", whoami,
                this->inSta, this->inChan, this->inNet, this->inLoc,
                this->inEndtime, waveHead->starttime);
            pFilt->olap_tot++;
            ResetStation( this );
        }
    }
    
  /* Check for sufficient space for incoming data */
    if ( waveHead->nsamp > BUFFSIZE - this->inBuff.write ) {
        logit("et", "%s no room for data <%s.%s.%s.%s>; exiting.\n", whoami,
                    this->inSta, this->inChan, this->inNet, this->inLoc);
        return EW_FAILURE;
    }
  
  /* Check for useable data types: we only handle shorts and longs for now */
    if ( waveHead->datatype[0] != 's' && waveHead->datatype[0] != 'i' ) {
        logit("et", "%s unusable datatype <%s> from SCNL <%s.%s.%s.%s>; skipping\n", whoami,
              waveHead->datatype, waveHead->sta, waveHead->chan, waveHead->net, waveHead->loc);
        return EW_SUCCESS;
    }
  
  /* If stage buffer is empty, set its initial values */
    if ( this->inBuff.write == 0 ) {
        this->inBuff.starttime  = waveHead->starttime;
        this->inBuff.samplerate = waveHead->samprate;
    } else {  /* Update its start time based on new data */
        this->inBuff.starttime = waveHead->starttime - this->inBuff.write / waveHead->samprate;
    }
      
  /* Copy in new data */
    if ( waveHead->datatype[1] == '2' ) {
        datasize = 2;
        for (i = 0; i < waveHead->nsamp; i++)
            this->inBuff.buffer[this->inBuff.write++] = (double) waveShort[i];
    }
    else if (waveHead->datatype[1] == '4' ) {
        datasize = 4;
        for (i = 0; i < waveHead->nsamp; i++)
            this->inBuff.buffer[this->inBuff.write++] = (double) waveLong[i];
    } else {
        logit("et", "%s unusable datatype <%s> from SCNL <%s.%s.%s.%s>; skipping\n", whoami,
               waveHead->datatype, waveHead->sta, waveHead->chan, waveHead->net, waveHead->loc);
        return EW_SUCCESS;
    }
    
    this->inEndtime = waveHead->endtime;
    
    pFilt->filter = pFilt->acfParam.filtfile[this->FiltInd].ThisFilt;
  
  /* Do the actual filtering */
    ret = FiltOneSCNL( pFilt, this );
  
    if ( ret > 0 ) {  /* Something is coming out the far end of the pipe! */
        while (this->outBuff.write - this->outBuff.read >= waveHead->nsamp) {
     /* Make the output packets the same length as the input ones. */

      /* Copy data into the outgoing TRACE_BUF packet */      
            outHead->pinno = waveHead->pinno;
            outHead->nsamp = waveHead->nsamp;
            outHead->starttime = this->outBuff.starttime + this->outBuff.read / waveHead->samprate;
            outHead->endtime = (waveHead->nsamp - 1) / waveHead->samprate + outHead->starttime;
            outHead->samprate = waveHead->samprate;
            strcpy(outHead->sta, this->outSta);
            strcpy(outHead->chan, this->outChan);
            strcpy(outHead->net, this->outNet);
            strcpy(outHead->loc, this->outLoc);
            strncpy(outHead->version, waveHead->version, 2);
            strncpy(outHead->datatype, waveHead->datatype, 3);
            strncpy(outHead->quality, waveHead->quality, 2);
      
            if (datasize == 2)
                for (i = 0; i < outHead->nsamp; i++)
                    outShort[i] = (short) this->outBuff.buffer[i + this->outBuff.read];
            else if (datasize == 4)
                for (i = 0; i < outHead->nsamp; i++)
                    outLong[i] = (long) this->outBuff.buffer[i + this->outBuff.read];

            this->outBuff.read += outHead->nsamp;
      
            if (pFilt->acfParam.debug)
                logit("","%s shipping <%s.%s.%s.%s>, start %lf, end %lf\n", whoami,
                    outHead->sta, outHead->chan, outHead->net, outHead->loc,
                    outHead->starttime, outHead->endtime );
      
      /* Ship the packet out to the transport ring */
            outBufLen = outHead->nsamp * datasize + sizeof(TRACE2_HEADER);
            if (tport_putmsg (&(pFilt->regionOut), &(pFilt->trcLogo), outBufLen, outBuf) != PUT_OK) {
                logit ("et", "%s  Error sending TRACE_BUF message.\n", whoami );
                return EW_FAILURE;
            }
        } /* While... */

        if (this->outBuff.read > 0) {  /* Slide the old data out of the output buffer */
            memmove(this->outBuff.buffer, 
                &(this->outBuff.buffer[this->outBuff.read]),
                (this->outBuff.write - this->outBuff.read) * sizeof(double));
            this->outBuff.write -= this->outBuff.read;
            this->outBuff.starttime += this->outBuff.read / outHead->samprate;
            this->outBuff.read = 0;
        }
    } else if (ret < 0) {  /* An error occured in FiltOneSCN */
        switch (ret) {
            case FILT_NOROOM:
                logit("et", "%s no room for data <%s.%s.%s.%s>; exiting.\n", whoami,
                            this->inSta, this->inChan, this->inNet, this->inLoc);
                return EW_FAILURE;
                break;
            default:
                logit("et", "%s unknown error return from FiltOneSCN: %d; exiting\n", whoami, ret);
                return EW_FAILURE;
        }
    }
    return EW_SUCCESS;
}


/*****************************************************************************
 *                                                                           *
 * filt1scn.c: Filter one SCNL's buffer of data                              *
 *              1) Report number of data points in output buffer             *
 *                                                                           *
 *      Function: FiltOneSCNL                                                *
 *                                                                           *
 *      Inputs:         Pointer to stage ready to be processed               *
 *                                                                           *
 *      Outputs:        Filtered data                                        *
 *                                                                           *
 *      Returns:        On success, number of datapoints in output           *
 *                         buffer.                                           *
 *                      On failure, error status defined in acfilt.h         *
 *                                                                           *
 *****************************************************************************/

int FiltOneSCNL ( WORLD *pFilt, STATION *this )
{
    char   whoami[50];
    double num;
    double sum;           /* Holder for the filter result                 */  
    int    need;          /* number of datapoints needed in output buffer */
    int    half_l;        /* number of duplicated coefficients in filter  */
    int    read;          /* Local copy of the buffer read point          */
    int    nodd;          /* =1 if filter length is odd; 0 otherwise      */
    int    i;
    static int ret;
    
    strcpy(whoami, "acfilt: FiltOneSCNL:");
    
    if (pFilt->acfParam.debug)
        logit("t", "enter FiltOneSCNL with <%s.%s.%s.%s> end: %lf samprate: %lf \n", 
            this->inSta, this->inChan, this->inNet, this->inLoc, 
            this->inEndtime, this->inBuff.samplerate );
  
  
    half_l = pFilt->filter.Length / 2;
    nodd = pFilt->filter.Length - half_l * 2;
    read = this->inBuff.read;
  
  /* Check for sufficient space in the output buffer.  */
    need = this->inBuff.write - read - pFilt->filter.Length;
    if (need > BUFFSIZE - this->outBuff.write)
        return FILT_NOROOM;
  
  /* It the next buffer is empty, update its information */
    if (this->outBuff.write == 0) {
    /* Start time is time of newest sample that we'll read first        */
        this->outBuff.starttime = this->inBuff.starttime + 
            (read + (pFilt->filter.Length - 1) / 2.0) / this->inBuff.samplerate;
    }
#ifdef DEBUG
    logit("", "%s read %d, write %d\n", whoami, this->inBuff.read, this->inBuff.write);
#endif
  
  /* Process as much data as we can */
    while (read + pFilt->filter.Length < this->inBuff.write) {
        if (nodd == 1)
            sum = pFilt->filter.coef[half_l] * this->inBuff.buffer[read + half_l];
        else
            sum = 0.0;
  
    /* Filter is symmetric, so we make things a little more efficient */
        for (i = 0; i < half_l; i++)
            sum += pFilt->filter.coef[i] 
            * (this->inBuff.buffer[read + i] + 
            this->inBuff.buffer[read + pFilt->filter.Length - i - 1]);
        
        this->outBuff.buffer[this->outBuff.write++] = sum;
        read ++;
    }
  
  /* Update the current buffer: slide the old data out, update pointers */
    if (read > 0) {
        memmove(this->inBuff.buffer, &(this->inBuff.buffer[read]), 
               (this->inBuff.write - read) * sizeof(double));
        this->inBuff.write -= read;
        this->inBuff.starttime += read / this->inBuff.samplerate;
        read = 0;
    }
    this->inBuff.read = read;
    
    return (this->outBuff.write - this->outBuff.read);
}

/*****************************************************************************
 *                                                                           *
 * resetsta.c: Reset station buffers for one STATION structure               *
 *              1) Reset station parameters and buffer pointers              *
 *                                                                           *
 *      Function: ResetStation                                               *
 *                                                                           *
 *      Inputs:         Pointer to Station Structure                         *
 *                                                                           *
 *      Outputs:        Updated Station structures                           *
 *                                                                           *
 *      Returns:        nothing                                              *
 *****************************************************************************/

void ResetStation( STATION *sta )
{
    sta->inEndtime         = 0.0;
    sta->inBuff.starttime  = 0.0;
    sta->inBuff.read       = 0;
    sta->inBuff.write      = 0;
    sta->outBuff.starttime = 0.0;
    sta->outBuff.read      = 0;
    sta->outBuff.write     = 0;
    
    return;
}
    
