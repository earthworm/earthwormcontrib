#
#                     Configuration File for acfilt
#
MyModId          MOD_ACFILT
InRing           WAVE_RING2      # Transport ring to find waveform data on,
OutRing          ACFILT_RING     # Transport ring to write output to,
                                 # InRing and OutRing may be the same.
HeartBeatInterval     30         # Heartbeat interval, in seconds,
LogFile            1             # 1 -> Keep log, 0 -> no log file
                                 # 2 -> write to module log but not to stderr or
                                 #   stdout
#Debug		 		 # Write out debug messages (optional)
  
#PassThrough                    # Tracebufs not requested for filtering
                                # will be passed along to OutRing unchanged. (optional)

MaxGap              1.5         # Maximum gap, in sample periods, allowed
                                # between trace data points.
                                # When exceeded, channel is restarted.

#
# Specify logos of the messages to grab from the InRing.
# TYPE_TRACEBUF2 is assumed, therefore only module ID and 
# installation ID need to be specified
#
GetWavesFrom    INST_WILDCARD MOD_WILDCARD  # TYPE_TRACEBUF2 (assumed)

FiltFile HRD24     /home/ncss/run/params/acfilters/HRD_50
FiltFile Trident   /home/ncss/run/params/acfilters/HRD_50
FiltFile Lynx      /home/ncss/run/params/acfilters/HRD_50
FiltFile Nano      /home/ncss/run/params/acfilters/HRD_100
FiltFile NanoD1    /home/ncss/run/params/acfilters/HRD_200
FiltFile NanoD2    /home/ncss/run/params/acfilters/HRD_500
FiltFile RefTek    /home/ncss/run/params/acfilters/Reftek_130
FiltFile RefTek72a /home/ncss/run/params/acfilters/RefTek72a
FiltFile Q_80      /home/ncss/run/params/acfilters/Quanterra_80
FiltFile Q_40      /home/ncss/run/params/acfilters/Quanterra_40

#
# List SCNL codes of trace messages to filter and their output SCN codes
# Wildcard characters are not allowed here.
#
#         IN-SCNL        OUT-SCNL         FILT
#GetSCNL GDXB HHZ NC --  GDXB HHZ NC 01    Filt3
#

@/home/ncss/run/params/acfilters/acfiltscnl_hrd.inc
@/home/ncss/run/params/acfilters/acfiltscnl_lynx.inc
@/home/ncss/run/params/acfilters/acfiltscnl_nano.inc
@/home/ncss/run/params/acfilters/acfiltscnl_tri.inc
@/home/ncss/run/params/acfilters/acfiltscnl_reftek.inc
@/home/ncss/run/params/acfilters/acfiltscnl_BK.inc
#
@/home/ncss/run/params/acfilters/acfiltscnl_nevada.inc
@/home/ncss/run/params/acfilters/acfiltscnl_nanodh.inc
#
GetSCNL PHL  HHZ CI --  PHL  HHZ CI --    Q_80
GetSCNL WVOR BHZ US --  WVOR BHZ US --    Q_40
