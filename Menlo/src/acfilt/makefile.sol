CFLAGS = -O ${GLOBALFLAGS}

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib


SRCS = acfilt.c
OBJS = acfilt.o

LIBS = -lm -lposix4 -lthread

OBJS = acfilt.o \
       filtthrd.o \
       $L/logit_mt.o \
       $L/kom.o \
       $L/getutil.o \
       $L/sleep_ew.o \
       $L/time_ew.o \
       $L/transport.o \
       $L/swap.o \
       $L/mem_circ_queue.o \
       $L/threads_ew.o \
       $L/sema_ew.o 

acfilt: $(OBJS)
	cc ${CFLAGS} -o $B/acfilt $(OBJS) $(LIBS)

lint:
	lint acfilt.c filtthrd.c $(GLOBALFLAGS)

# Clean-up rules
clean:
	rm -f a.out core *.o *% *~

clean_bin:
	rm -f $B/acfilt


.c.o:
	cc -c ${CFLAGS} $<

