/***************************************************************************** 
 *                                                                           * 
 * acfilt.c:                                                                 * 
 *                                                                           * 
 * Acfilt filters out the pre-arrival ripple introduced by acausal           *
 * anti-alias filters used in many dataloggers.  If this is not done, the    *
 * picker will either produce a pick time that is too early or will not pick *
 * at all.                                                                   * 
 *                                                                           * 
 *                                      Jim Luetgert 5 Oct 2006              * 
 *****************************************************************************/

#include <earthworm.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <kom.h>
#include <swap.h>
#include "acfilt.h"

static int Debug = 1;

main (int argc, char **argv)
{
    char  whoami[50];
    WORLD ACFilt;                    /* Our main data structure              */
    long  timeNow;                   /* current time                         */ 
    long  timeLastBeat;              /* time last heartbeat was sent         */
    long  timeLastReport;            /* time last report was written         */
    long  sizeMsg;                   /* size of retrieved message            */
    SHM_INFO  regionIn;              /* Input shared memory region info.     */
    MSG_LOGO  logoWave;              /* Logo of requested waveforms.         */
    MSG_LOGO  logoMsg;               /* logo of retrieved message            */
    unsigned  tidFilter;             /* Filter thread id                     */
    char *inBuf;                     /* Pointer to the input message buffer. */
    int inBufLen;                    /* Size of input message buffer         */
    TRACE2_HEADER  *WaveHeader;
    int   ret;
    char  msgText[MAXMESSAGELEN];    /* string for log/error messages        */
    
    strcpy(whoami, "acfilt:");

  /* Check command line arguments 
   ******************************/
    if (argc != 2) {
        fprintf (stderr, "Usage: acfilt <configfile>\n");
        exit (EW_FAILURE);
    }

  /* Read config file and configure the decimator */
    if (Configure(&ACFilt, argv) != EW_SUCCESS) {
        fprintf (stderr, "%s configure() failed \n", whoami);
        exit (EW_FAILURE);
    }
    
  /* We will put the station index in front of the trace message, so we   *
   * don't have to look up the SCNL again at the other end of the queue. */
    inBufLen = MAX_TRACEBUF_SIZ + sizeof( int );
    if ( ! ( inBuf = (char *) malloc( (size_t) inBufLen ) ) ) {
        logit( "et", "%s: Memory allocation failed - initial message buffer!\n", argv[0] );
        exit( EW_FAILURE );
    }
    WaveHeader = (TRACE2_HEADER *) (inBuf + sizeof(int));
  
  /* Attach to Input shared memory ring 
   *******************************************/
    tport_attach (&regionIn, ACFilt.acfEWH.ringInKey);
    if (ACFilt.acfParam.debug)
        logit ("e", "Attached to public memory region %s: %d\n", 
            ACFilt.acfParam.ringIn, ACFilt.acfEWH.ringInKey);

  /* Attach to Output shared memory ring 
   *******************************************/
    if (ACFilt.acfEWH.ringOutKey == ACFilt.acfEWH.ringInKey)
        ACFilt.regionOut = regionIn;
    else
        tport_attach (&(ACFilt.regionOut), ACFilt.acfEWH.ringOutKey);

    if (ACFilt.acfParam.debug)
        logit ("e", "Attached to public memory region %s: %d\n", 
            ACFilt.acfParam.ringOut, ACFilt.acfEWH.ringOutKey);

 /*    Specify logos of incoming waveforms                             */
    logoWave.instid = ACFilt.acfEWH.readInstId;
    logoWave.mod    = ACFilt.acfEWH.readModId;
    logoWave.type   = ACFilt.acfEWH.typeWaveform;

 /*    Specify logos of outgoing messages                              */
    ACFilt.hrtLogo.instid = ACFilt.acfEWH.myInstId;
    ACFilt.hrtLogo.mod    = ACFilt.acfEWH.myModId;
    ACFilt.hrtLogo.type   = ACFilt.acfEWH.typeHeartbeat;
    
    ACFilt.errLogo.instid = ACFilt.acfEWH.myInstId;
    ACFilt.errLogo.mod    = ACFilt.acfEWH.myModId;
    ACFilt.errLogo.type   = ACFilt.acfEWH.typeError;
    
    ACFilt.trcLogo.instid = ACFilt.acfEWH.myInstId;
    ACFilt.trcLogo.mod    = ACFilt.acfEWH.myModId;
    ACFilt.trcLogo.type   = ACFilt.acfEWH.typeWaveform;

  /* Force a heartbeat to be issued in first pass thru main loop  */
    timeLastBeat = time (&timeNow) - ACFilt.acfParam.heartbeatInt - 1;

  /* Flush the incoming transport ring */
    while (tport_getmsg (&regionIn, &logoWave, 1, &logoMsg,
                         &sizeMsg, inBuf, inBufLen) != GET_NONE);

    /* Create MsgQueue mutex */
    CreateMutex_ew();

  /* Allocate the message Queue */
    initqueue (&(ACFilt.MsgQueue), QUEUE_SIZE, inBufLen);

  /* Start decimator thread which will read messages from   *
   * the Queue, filter them and write them to the OutRing */
    if (StartThreadWithArg (FiltThread, (void *) &ACFilt, (unsigned) THREAD_STACK, &tidFilter) == -1) {
        logit( "et", "%s Error starting ACFilt thread.  Exiting.\n", whoami);
        tport_detach (&regionIn);
        tport_detach (&(ACFilt.regionOut));
        exit (EW_FAILURE);
    }
    
    ACFilt.FiltStatus = 0; /*assume the best*/

    ACFilt.queue_ind  = 0;    /*initialize queue counter*/
    ACFilt.queue_max  = 0;    /*Max queue usage in last interval*/
    ACFilt.gap_tot    = 0;    /*Number of gaps in last interval*/
    ACFilt.olap_tot   = 0;    /*Number of overlaps in last interval*/

    timeLastReport = time (&timeNow);
 
/*--------------------- setup done; start main loop -------------------------*/

    while (tport_getflag (&regionIn) != TERMINATE  &&
           tport_getflag (&regionIn) != ACFilt.MyPid ) {
    /* send filter' heartbeat */
        if (time (&timeNow) - timeLastBeat >= ACFilt.acfParam.heartbeatInt) {
            timeLastBeat = timeNow;
            if (ACFilt.acfParam.debug)
                logit ("et", "%s Queue count: %d\n", whoami, ACFilt.queue_ind);
            StatusReport (&ACFilt, ACFilt.acfEWH.typeHeartbeat, 0, ""); 
        }
        
        if (time (&timeNow) - timeLastReport >= ACFilt.acfParam.reportInt) {
            timeLastReport = timeNow;
            logit ("et", "%s Statistic report for last %d seconds. Turn on debug for more info.\n", 
                             whoami, ACFilt.acfParam.reportInt);
            logit ("e", "%s Maximum Queue count: %d\n", whoami, ACFilt.queue_max);
            logit ("e", "%s Total gap count:     %d\n", whoami, ACFilt.gap_tot);
            logit ("e", "%s Total Overlap count: %d\n", whoami, ACFilt.olap_tot);
            ACFilt.queue_max = ACFilt.gap_tot = ACFilt.olap_tot = 0;
        }
        
        if (ACFilt.queue_ind > QUEUE_SIZE/2) {
            logit ("et", "%s ******* Error: Queue count getting too high: %d *******\n", whoami, ACFilt.queue_ind);
        }
        
        if (ACFilt.FiltStatus < 0) {
            logit ("et", "%s Filter thread died. Exiting\n", whoami);
            exit (EW_FAILURE);
        }
        
        ret = tport_getmsg (&regionIn, &logoWave, 1, &logoMsg,
                            &sizeMsg, inBuf + sizeof(int), inBufLen - sizeof(int));

    /* Check return code; report errors */
        if (ret != GET_OK) {
            if (ret == GET_TOOBIG) {
                sprintf (msgText, "msg[%ld] i%d m%d t%d too long for target",
                         sizeMsg, (int) logoMsg.instid,
                         (int) logoMsg.mod, (int)logoMsg.type);
                StatusReport (&ACFilt, ACFilt.acfEWH.typeError, ERR_TOOBIG, msgText);
                if(Debug) logit ("et", "%s %s.\n", whoami, msgText);
                continue;
            }
            else if (ret == GET_MISS) {
                sprintf (msgText, "missed msg(s) i%d m%d t%d in %s",
                         (int) logoMsg.instid, (int) logoMsg.mod, 
                         (int)logoMsg.type, ACFilt.acfParam.ringIn);
                StatusReport (&ACFilt, ACFilt.acfEWH.typeError, ERR_MISSMSG, msgText);
                if(Debug) logit ("et", "%s %s.\n", whoami, msgText);
            }
            else if (ret == GET_NOTRACK) {
                sprintf (msgText, "no tracking for logo i%d m%d t%d in %s",
                         (int) logoMsg.instid, (int) logoMsg.mod, 
                         (int)logoMsg.type, ACFilt.acfParam.ringIn);
                StatusReport (&ACFilt, ACFilt.acfEWH.typeError, ERR_NOTRACK, msgText);
                if(Debug) logit ("et", "%s %s.\n", whoami, msgText);
            }
            else if (ret == GET_NONE) {
                sleep_ew(100);
                continue;
            }
        
        }

    /* Check to see if msg's SCNL code is desired. Note that we don't need *
     * to do byte-swapping before we can read the SCNL.                    */
        if ((ret = matchSCNL(WaveHeader, &ACFilt )) < -1 ) {
            logit ("et", "%s Call to matchSCNL failed; Exiting.\n", whoami);
            exit (EW_FAILURE);
        }
        else if ( ret == -1 ) {
      /* Not an SCNL we want */
            if(ACFilt.pass_through) {
				if (tport_putmsg (&(ACFilt.regionOut), &logoMsg, sizeMsg, inBuf + sizeof(int)) != PUT_OK) {
					logit ("et", "%s  Error sending TRACE_BUF message.\n", whoami );
					return EW_FAILURE;
				}
            }
            continue;
         }

    /* stick the SCNL number as an int at the front of the message */
        *((int*)inBuf) = ret; 

    /* If necessary, swap bytes in the wave message before we look at any binary info */
        if (WaveMsg2MakeLocal (WaveHeader) < 0) {
            logit ("et", "%s Unknown waveform type.\n", whoami);
            continue;
        }
         
         if(WaveHeader->nsamp <= 0) {
            logit ("et", "%s Zero length tracebuf. %s_%s_%s_%s (%d)\n", whoami, 
                WaveHeader->sta, WaveHeader->chan, WaveHeader->net, WaveHeader->loc, WaveHeader->nsamp);
            continue;
         
         }

    /* Queue retrieved msg */
        RequestMutex ();
        ret = enqueue (&(ACFilt.MsgQueue), inBuf, sizeMsg + sizeof(int), logoMsg); 
        ReleaseMutex_ew ();
        
        if (ret != 0) {
            if (ret == -1) {
                sprintf (msgText, "Message too large for queue; Lost message.");
                StatusReport (&ACFilt, ACFilt.acfEWH.typeError, ERR_QUEUE, msgText);
                continue;
            }
            if (ret == -3) {
                sprintf (msgText, "Queue full. Old messages lost.");
                StatusReport (&ACFilt, ACFilt.acfEWH.typeError, ERR_QUEUE, msgText);
                continue;
            }
        } /* problem from enqueue */
        ACFilt.queue_ind++; /*increment queue counter*/

    } /* wait until TERMINATE is raised  */  

  /* Termination has been requested */
    tport_detach (&regionIn);
    tport_detach (&(ACFilt.regionOut));
    logit ("et", "Termination requested; Exiting!\n" );
    exit (EW_SUCCESS);

}

/*****************************************************************************
 *                                                                           *
 * statrpt.c: Produce an error or heartbeat message on the output ring.      *
 *              1) Construct the correct type of message.                    *
 *              2) Send the message to the output ring.                      *
 *                                                                           *
 *      Function: StatusReport                                               * 
 *                                                                           * 
 *      Inputs:         Pointer to the Network structure                     * 
 *                      Message type                                         * 
 *                      Message id(code)                                     * 
 *                      Pointer to a string(message)                         * 
 *                                                                           * 
 *      Outputs:        Message structure sent to output ring                * 
 *                                                                           * 
 *      Returns:        Nothing                                              * 
 *****************************************************************************/

void StatusReport( WORLD* pFilt, unsigned char type, short code, char* message )
{
    char          outMsg[MAXMESSAGELEN];  /* The outgoing message.        */
    time_t        msgTime;                /* Time of the message.         */

  /*  Get the time of the message                                     */
    time( &msgTime );

  /*  Build & process the message based on the type                   */
    if ( pFilt->acfEWH.typeHeartbeat == type ) {
        sprintf( outMsg, "%ld %ld\n\0", msgTime, pFilt->MyPid );
      
      /*Write the message to the output region                          */
        if ( tport_putmsg( &(pFilt->regionOut), &(pFilt->hrtLogo), 
                         (long) strlen( outMsg ), outMsg ) != PUT_OK ) {
        /*     Log an error message                                    */
            logit( "et", "acfilt: StatusReport: Failed to send a heartbeat message (%d).\n", code );
        }
    } else {
        if ( message ) {
            sprintf( outMsg, "%ld %hd %s\n\0", msgTime, code, message );
            logit("t","Error:%d (%s)\n", code, message );
        } else {
            sprintf( outMsg, "%ld %hd\n\0", msgTime, code );
            logit("t","Error:%d (No description)\n", code );
        }

      /*Write the message to the output region                         */
        if ( tport_putmsg( &(pFilt->regionOut), &(pFilt->errLogo), 
                         (long) strlen( outMsg ), outMsg ) != PUT_OK ) {
      /*     Log an error message                                    */
            logit( "et", "acfilt: StatusReport: Failed to send an error message (%d).\n", code );
        }
    }
}


/*****************************************************************************
 * matchSCNL () - Returns the index of the SCNL matching sta.cha.net.loc     *
 *               found in the SCNLlist array.                                *
 *               Otherwise, returns -1 (or -2 if an error occured            *
 *                                                                           *
 *****************************************************************************/

int matchSCNL (TRACE2_HEADER* WaveHead, WORLD* pDcm )
{
  int i;
  
  if ((WaveHead->sta == NULL) || (WaveHead->chan == NULL) || 
      (WaveHead->net == NULL) || (WaveHead->loc == NULL)) {
    logit ("et",  "acfilt: matchSCNL: invalid parameters to matchSCNL\n");
    return (-2);
  }
  
  for (i = 0; i < pDcm->nSta; i++ ) {
    /* try to match explicitly */
    if ((strcmp (WaveHead->sta, pDcm->stations[i].inSta) == 0) &&
        (strcmp (WaveHead->chan, pDcm->stations[i].inChan) == 0) &&
        (strcmp (WaveHead->net, pDcm->stations[i].inNet) == 0) &&
        (strcmp (WaveHead->loc, pDcm->stations[i].inLoc) == 0))
      return (i);
  }
  /* No match */
  return -1;
}

/*****************************************************************************/
/*                                                                           */
/* Configure the acfilt module:                                              */
/*    read the config file and earthworm*.h parameters                       */
/*    set up logging.                                                        */
/*    set up the station and filter structures                               */
/*                                                                           */
/*      Function: Configure                                                  */
/*                                                                           */
/*      Inputs:         Pointer to a string(input filename)                  */
/*                      Pointer to the ACFilt World structure                */
/*                                                                           */
/*      Outputs:        Updated ACFilt parameter structures(above)           */
/*                      Error messages to stderr                             */
/*                                                                           */
/*      Returns:        0 on success                                         */
/*                      Error code as defined in decimate.h on               */
/*                      failure                                              */
/*****************************************************************************/

int Configure(WORLD *pFilt, char **argv)
{
    char  whoami[50];
    
    strcpy(whoami, "acfilt: Configure:");

  /* Set initial values of WORLD structure */
    InitializeParameters( pFilt );

  /* Initialize name of log-file & open it 
     ***************************************/
    logit_init (argv[1], 0, MAXMESSAGELEN, 1);

  /* Read config file and configure the Acausal filter */
    if (ReadConfig(argv[1], pFilt) != EW_SUCCESS)
    {
        fprintf (stderr, "%s failed reading config file <%s>\n", argv[1], whoami);
        return EW_FAILURE;
    }
    
    logit ("" , "Read command file <%s>\n", argv[1]);

  /* Look up important info from earthworm.h tables
     ************************************************/
    if (ReadEWH(pFilt) != EW_SUCCESS)
    {
        fprintf (stderr, "%s Call to ReadEWH failed \n", whoami );
        return EW_FAILURE;
    }

  /* Reinitialize logit to desired logging level 
     *******************************************/
    logit_init (argv[1], 0, MAXMESSAGELEN, pFilt->acfParam.logSwitch);
  
  /* Get our process ID
     **********************/
    if ((pFilt->MyPid = getpid ()) == -1)
    {
        logit ("e", "%s Call to getpid failed. Exiting.\n", whoami);
        return (EW_FAILURE);
    }

  /* Initialize the filter buffers for each station */
    InitSta( pFilt );
    
    return EW_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*      Function: InitializeParameters                                       */
/* initpars.c: Initialize parameter structures used by ACFilt.               */
/*              1) Initialize members of the ACFEWH structure.               */
/*              2) Initialize members of the WORLD structure.                */
/*              3) Allocates memory for the trigger message buffer.          */
/*                                                                           */
/*      Inputs:         Pointer to a ACFilt WORLD parameter                  */
/*                        structure                                          */
/*                                                                           */
/*      Outputs:        Updated inputs(above)                                */
/*                                                                           */
/*      Returns:        0 on success                                         */
/*                      Error code as defined in acfilt.h on                 */
/*                      failure                                              */
/*****************************************************************************/

void InitializeParameters( WORLD* pFilt )
{
    int  i;
  /*    Initialize members of the ACFEWH structure                      */
    pFilt->acfEWH.myInstId = 0;
    pFilt->acfEWH.myModId = 0;
    pFilt->acfEWH.readInstId = 0;
    pFilt->acfEWH.readModId = 0;
    pFilt->acfEWH.typeWaveform = 0;
    pFilt->acfEWH.typeError = 0;
    pFilt->acfEWH.typeHeartbeat = 0;
    pFilt->acfEWH.ringInKey = 0l;
    pFilt->acfEWH.ringOutKey = 0l;

  /*    Initialize members of the ACFPARAM structure                    */
    sprintf( pFilt->acfParam.readInstName, "INST_WILDCARD" );
    sprintf( pFilt->acfParam.readModName, "MOD_WILDCARD" );
    pFilt->acfParam.ringIn[0] = '\0';
    pFilt->acfParam.ringOut[0] = '\0';
    pFilt->acfParam.heartbeatInt = 15;
    pFilt->acfParam.reportInt = 3600;
    pFilt->acfParam.debug = 0;
    pFilt->acfParam.logSwitch = 1;
    pFilt->acfParam.maxGap = 1.5;
    
    pFilt->FiltStatus = 0;
    pFilt->stations = NULL;
    pFilt->nSta = 0;
    pFilt->filter.Length = 0;
    for(i=0;i<MAXFILTLEN;i++)
        pFilt->filter.coef[i] = 0.0;
    
    return;
}

#define NUMREQ          9       /* Number of parameters that MUST be    */
                                /*   set from the config file.          */
#define STATION_INC     10      /* how many more are allocated each     */
                                /*   time we run out                    */
/*****************************************************************************
 *                                                                           *
 *             Read the Filter parameters from a file.                       *
 *              1) Set members of the WORLD structures.                      *
 *                                                                           *
 *      Function: ReadConfig                                                 *
 *                                                                           *
 *      Inputs:         Pointer to a string(input filename)                  *
 *                      Pointer to the ACFilt World structure                *
 *                                                                           *
 *      Outputs:        Updated ACFilt parameter structures(above)           *
 *                      Error messages to stderr                             *
 *                                                                           *
 *      Returns:        0 on success                                         *
 *                      Error code as defined in acfilt.h on                 *
 *                      failure                                              *
 *                                                                           *
 *****************************************************************************/

int ReadConfig (char *configfile, WORLD* pDcm)
{
  /* init flags, one byte for each required command */
    char            init[NUMREQ];     
    char            whoami[50], *com, *str;
    int             i, j, nfiles, success, nmiss;
    STATION         *stations = NULL;  /* Array of STATION structures */
    int             maxSta = 0;     /* number of STATIONs allocated   */
    int             nSta = 0;       /* number of STATIONs used        */
    
    strcpy(whoami, "acfilt: ReadConfig:");
    
    pDcm->acfParam.nFilts = 0;
    pDcm->pass_through = 0;

  /* Set to zero one init flag for each required command */

    for (i = 0; i < NUMREQ; i++) init[i] = 0;

  /* Open the main configuration file 
**********************************/
    nfiles = k_open (configfile); 
    if (nfiles == 0) {
        logit ("e", "%s Error opening command file <%s>; exiting!\n", whoami, configfile);
        return EW_FAILURE;
    }

  /* Process all command files
***************************/
    while (nfiles > 0) {   /* While there are command files open */
    
        while (k_rd ()) {        /* Read next line from active file  */
        
            com = k_str ();         /* Get the first token from line */

      /* Ignore blank lines & comments
*******************************/
            if (!com)
                continue;
            if (com[0] == '#')
                continue;

      /* Open a nested configuration file */
            if (com[0] == '@') {
                success = nfiles + 1;
                nfiles  = k_open (&com[1]);
                if (nfiles != success) {
                    logit ("e", "%s Error opening command file <%s>; exiting!\n", whoami, &com[1]);
                    return EW_FAILURE;
                }
                continue;
            }

      /* Process anything else as a command */
/*0*/       if (k_its ("MyModId")) {
                if ((str = k_str ()) != NULL) {
                    strcpy (pDcm->acfParam.myModName, str);
                    init[0] = 1;
                }
            }
/*1*/          else if (k_its ("InRing")) {
                if ((str = k_str ()) != NULL) {
                    strcpy (pDcm->acfParam.ringIn, str);
                    init[1] = 1;
                }
            }
/*2*/        else if (k_its ("OutRing")) {
                if ((str = k_str ()) != NULL) {
                    strcpy (pDcm->acfParam.ringOut, str);
                    init[2] = 1;
                }
            }
/*3*/          else if (k_its ("HeartBeatInterval")) {
                pDcm->acfParam.heartbeatInt = k_long ();
                init[3] = 1;
            }

/*4*/       else if (k_its ("LogFile")) {
                pDcm->acfParam.logSwitch = k_int();
                init[4] = 1;
            }

/* 5 */     else if (k_its ("MaxGap")) {
                pDcm->acfParam.maxGap = k_val();
                init[5] = 1;
            }
          
      /* Enter installation & module types to get */
/* 6 */     else if (k_its ("GetWavesFrom")) {
                if ((str = k_str()) != NULL) strcpy(pDcm->acfParam.readInstName, str);
                if ((str = k_str()) != NULL) strcpy(pDcm->acfParam.readModName, str);
                
                init[6] = 1;
            }

      /* Enter SCNLs of traces to process */
/* 7 */     else if (k_its ("GetSCNL")) {
                if (nSta >= maxSta) {
          /* Need to allocate more */
                    maxSta += STATION_INC;
                    if ((stations = (STATION *) realloc (stations, 
                                     (maxSta * sizeof (STATION)))) == NULL) {
                    logit ("e", "%s Call to realloc failed; exiting!\n", whoami);
                    return EW_FAILURE;
                    }
                }

                str = k_str ();
                if ( str != NULL) strncpy (stations[nSta].inSta, str, TRACE2_STA_LEN);
                
                str = k_str ();
                if ( str != NULL) strncpy (stations[nSta].inChan, str, TRACE2_CHAN_LEN);
                
                str = k_str ();
                if ( str != NULL) strncpy (stations[nSta].inNet, str, TRACE2_NET_LEN);
                
                str = k_str ();
                if ( str != NULL) strncpy (stations[nSta].inLoc, str, TRACE2_LOC_LEN);
                
                str = k_str ();
                if ( str != NULL) strncpy (stations[nSta].outSta, str, TRACE2_STA_LEN);
                
                str = k_str ();
                if ( str != NULL) strncpy (stations[nSta].outChan, str, TRACE2_CHAN_LEN);
                
                str = k_str ();
                if ( str != NULL) strncpy (stations[nSta].outNet, str, TRACE2_NET_LEN);
                
                str = k_str ();
                if ( str != NULL) strncpy (stations[nSta].outLoc, str, TRACE2_LOC_LEN);

                str = k_str ();
                if ( str != NULL) strncpy (stations[nSta].FiltNam, str, MAXFILENAMELEN);

    /* Make sure that InSCNL and OutSCNL are different if they are returning to the same ring */
                if ( (strcmp(stations[nSta].inSta,  stations[nSta].outSta)  == 0) && 
                     (strcmp(stations[nSta].inChan, stations[nSta].outChan) == 0) && 
                     (strcmp(stations[nSta].inNet,  stations[nSta].outNet)  == 0) &&
                     (strcmp(stations[nSta].inLoc,  stations[nSta].outLoc)  == 0) &&
                     (strcmp(pDcm->acfParam.ringIn,  pDcm->acfParam.ringOut)  == 0)) {
          /** This we should not do ! **/
                    fprintf(stderr, 
                          "%s WARNING - %s.%s.%s.%s will not be renamed after filtering!\n", whoami,
                          stations[nSta].inSta, stations[nSta].inChan, stations[nSta].inNet, stations[nSta].inLoc);
                }
    
    /* Also make sure that none of the components are wildcards */
                if ( (strcmp(stations[nSta].inSta,  "*") == 0) ||
                     (strcmp(stations[nSta].inChan, "*") == 0) ||
                     (strcmp(stations[nSta].inNet,  "*") == 0) ||
                     (strcmp(stations[nSta].inLoc,  "*") == 0) ||
                     (strcmp(stations[nSta].outSta, "*") == 0) ||
                     (strcmp(stations[nSta].outChan,"*") == 0) ||
                     (strcmp(stations[nSta].outNet, "*") == 0) ||
                     (strcmp(stations[nSta].outLoc, "*") == 0) ) {

          /** This we cannot do !!!! **/
                    logit ("e", "%s Wildcards not valid in GetSCNL command; exiting!\n", whoami);
                    return EW_FAILURE;
                }
            
                nSta++;
                init[7] = 1;
            }
      
      /* Enter names of files containing filter parameters */
/* 8 */     else if (k_its ("FiltFile")) {
                if (pDcm->acfParam.nFilts < MAXFILT) {
                    if ((str = k_str()) != NULL) strcpy(pDcm->acfParam.filtfile[pDcm->acfParam.nFilts].filtname, str);
                    if ((str = k_str()) != NULL) strcpy(pDcm->acfParam.filtfile[pDcm->acfParam.nFilts].filename, str);
                    
                    pDcm->acfParam.nFilts++;
                    logit ("e", "FiltFile: %d %s %s \n", pDcm->acfParam.nFilts, 
                    pDcm->acfParam.filtfile[pDcm->acfParam.nFilts-1].filtname, 
                    pDcm->acfParam.filtfile[pDcm->acfParam.nFilts-1].filename);
                    init[8] = 1;
                }
                else logit ("e", "FiltFile: Too many filters. Must be <= %d \n", MAXFILT);
            }

      /* Optional: Statistic report interval (secs) */
            else if (k_its ("ReportInterval")) {
                pDcm->acfParam.reportInt = k_long ();
            }

      /* Optional: pass_through command */
            else if (k_its( "PassThrough") ) pDcm->pass_through = 1;
      
      /* Optional: debug command */
            else if (k_its( "Debug") ) pDcm->acfParam.debug = 1;
      
      /* Unknown command */ 
            else  {
                logit ("e", "%s <%s> Unknown command in <%s>.\n", whoami, com, configfile);
                continue;
            }

      /* See if there were any errors processing the command */
            if (k_err ()) {
                logit ("e", "%s Bad <%s> command in <%s>; exiting!\n", whoami, com, configfile);
                return EW_FAILURE;
            }

        } /** while k_rd() **/

        nfiles = k_close();

    } /** while nfiles **/

    pDcm->stations = stations;
    pDcm->nSta = nSta;
  
  /* After all files are closed, check init flags for missed commands */
    nmiss = 0;
    for (i = 0; i < NUMREQ; i++)  
        if (!init[i]) 
            nmiss++;

    if (nmiss) {
        logit ("e", "%s ERROR, no ", whoami);
        if (!init[0])  logit ("e", "<MyModId> "           );
        if (!init[1])  logit ("e", "<InRing> "            );
        if (!init[2])  logit ("e", "<OutRing> "           );
        if (!init[3])  logit ("e", "<HeartBeatInterval> " );
        if (!init[4])  logit ("e", "<LogFIle> "           );
        if (!init[5])  logit ("e", "<MaxGap> "            );
        if (!init[6])  logit ("e", "<GetWavesFrom> "      );
        if (!init[7])  logit ("e", "<GetSCNL> "           );
        if (!init[8])  logit ("e", "<FiltFile> "          );
        
        logit ("e", "command(s) in <%s>; exiting!\n", configfile);
        return EW_FAILURE;
    }
    
    /* All OK. Sort out the filters.   */
    for(i=0;i<nSta;i++) {
        stations[i].FiltInd = -1;
        for(j=0;j<pDcm->acfParam.nFilts;j++) {
            if( strcmp(stations[i].FiltNam, pDcm->acfParam.filtfile[j].filtname) == 0) {
                stations[i].FiltInd = j;
                break;
            }
        }
        if(stations[i].FiltInd == -1) 
            logit("e", "%s Invalid filter spec for %s %s %s %s\n", 
                whoami, stations[i].inSta, stations[i].inChan, stations[i].inNet, stations[i].inLoc);
    }
    
    for(j=0;j<pDcm->acfParam.nFilts;j++) {
        ReadFilt(j, pDcm);
    }
    
    return EW_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * ReadEWH: Read the Earthworm parameters.                                   *
 *              1) Set members of the ACFEWH structure.                      *
 *                                                                           *
 *      Function: ReadEWH                                                    *
 *                                                                           *
 *      Inputs:         Pointer to a ACFilt Earthworm parameter              *
 *                      structure                                            *
 *                                                                           *
 *      Outputs:        Updated input structure(above)                       *
 *                      Error messages to stderr                             *
 *                                                                           *
 *      Returns:        0 on success                                         *
 *                      Error code as defined in carlstatrig.h on            *
 *                      failure                                              *
 *****************************************************************************/
int ReadEWH( WORLD* pFilt )
{
    char  whoami[50];
    
    strcpy(whoami, "acfilt: ReadEWH:");

    if ( GetLocalInst( &(pFilt->acfEWH.myInstId)) != 0 ) {
        fprintf(stderr, "%s Error getting myInstId.\n", whoami );
        return EW_FAILURE;
    }
    
    if ( GetModId( pFilt->acfParam.myModName, &(pFilt->acfEWH.myModId)) != 0 ) {
        fprintf( stderr, "%s Error getting myModId.\n", whoami );
        return EW_FAILURE;
    }
    
    if ( GetInst( pFilt->acfParam.readInstName, &(pFilt->acfEWH.readInstId)) != 0) {
        fprintf( stderr, "%s Error getting readInstId.\n", whoami );
        return EW_FAILURE;
    }
    
    if ( GetModId( pFilt->acfParam.readModName, &(pFilt->acfEWH.readModId)) != 0 ) {
        fprintf( stderr, "%s Error getting readModId.\n", whoami );
        return EW_FAILURE;
    }
    
    /* Look up keys to shared memory regions */
    if ((pFilt->acfEWH.ringInKey = GetKey (pFilt->acfParam.ringIn)) == -1) {
        fprintf (stderr, "%s  Invalid ring name <%s>; exiting!\n", whoami, pFilt->acfParam.ringIn);
        return EW_FAILURE;
    }
    
    if ((pFilt->acfEWH.ringOutKey = GetKey (pFilt->acfParam.ringOut) ) == -1) {
        fprintf (stderr, "%s  Invalid ring name <%s>; exiting!\n", whoami, pFilt->acfParam.ringOut);
        return EW_FAILURE;
    }

  /* Look up message types of interest */
    if (GetType ("TYPE_HEARTBEAT", &(pFilt->acfEWH.typeHeartbeat)) != 0) {
        fprintf (stderr, "%s Invalid message type <TYPE_HEARTBEAT>; exiting!\n", whoami);
        return EW_FAILURE;
    }
    
    if (GetType ("TYPE_ERROR", &(pFilt->acfEWH.typeError)) != 0) {
        fprintf (stderr, "%s Invalid message type <TYPE_ERROR>; exiting!\n", whoami);
        return EW_FAILURE;
    }
    
    if (GetType ("TYPE_TRACEBUF2", &(pFilt->acfEWH.typeWaveform)) != 0) {
        fprintf (stderr, "%s Invalid message type <TYPE_TRACEBUF2>; exiting!\n", whoami);
        return EW_FAILURE;
    }
    
    return EW_SUCCESS;
} 


/*****************************************************************************
 *                                                                           *
 * readfilt.c: Read the filter parameters from a file.                       *
 *                                                                           *
 *      Function: ReadFilt                                                   *
 *                                                                           *
 *      Inputs:         Index of this filter file structure                  *
 *                      Pointer to the ACFilt World structure                *
 *                                                                           *
 *      Outputs:        Updated ACFilt parameter structures(above)           *
 *                      Error messages to stderr                             *
 *                                                                           *
 *      Returns:        0 on success                                         *
 *                      Error code as defined in acfilt.h on                 *
 *                      failure                                              *
 *****************************************************************************/

int ReadFilt (int j, WORLD* pDcm)
{
    char   whoami[50], *com, *str;
    int    i, index, nfiles, success;
    
    strcpy(whoami, "acfilt: ReadFilt:");

  /* Open the main configuration file 
   **********************************/
    nfiles = k_open (pDcm->acfParam.filtfile[j].filename); 
    if (nfiles == 0) {
        logit ("e", "%s Error opening command file %d <%s>; exiting!\n", whoami, j, pDcm->acfParam.filtfile[j].filename);
        return EW_FAILURE;
    } 
    else 
        logit ("e", "%s Opening command file %d: <%s>\n", whoami, j, pDcm->acfParam.filtfile[j].filename);

  /* Process all command files
   ***************************/
    while (nfiles > 0) {   /* While there are command files open */
        while (k_rd ()) {        /* Read next line from active file  */
            com = k_str ();         /* Get the first token from line */

      /* Ignore blank lines & comments
       *******************************/
            if (!com)
                continue;
            if (com[0] == '#') {
                if ((str = k_str()) != NULL) {
                    if(strcmp(str, "Group") == 0) {
                        str = k_str ();
                        pDcm->acfParam.filtfile[j].ThisFilt.TimeCorr = k_val();
                    }
                }
                continue;
            }
            
      /* Open a nested configuration file */
            if (com[0] == '@') {
                success = nfiles + 1;
                nfiles  = k_open (&com[1]);
                if (nfiles != success) {
                    logit ("e", "%s Error opening command file <%s>; exiting!\n", whoami, &com[1]);
                    return EW_FAILURE;
                }
                continue;
            }

      /* Process anything else as a command */
/*0*/       if (k_its ("B041F08")) {   /* Numerator coefficients */
                str = k_str ();
                str = k_str ();
                str = k_str ();
                pDcm->acfParam.filtfile[j].ThisFilt.Length = k_int();
            }
/*1*/          else if (k_its ("B041F09")) {   /* index coefficient */
                index = k_int();
                pDcm->acfParam.filtfile[j].ThisFilt.coef[index] = k_val();
            }
             else if (k_its ("B041F05")) {   /* Symmetry type */
                str = k_str ();
            }
             else if (k_its ("B041F06")) {   /* Symmetry type */
                str = k_str ();
            }
             else if (k_its ("B041F07")) {   /* Symmetry type */
                str = k_str ();
            }
      
      /* Optional: debug command
            else if (k_its( "Debug") ) pDcm->acfParam.debug = 1; */
      
      /* Unknown command */ 
            else  {
                logit ("e", "%s <%s> Unknown command in <%s>.\n", whoami, com, pDcm->acfParam.filtfile[j].filename);
                continue;
            }

      /* See if there were any errors processing the command */
            if (k_err ()) {
                logit ("e", "%s Bad <%s> command in <%s>; exiting!\n", whoami, com, pDcm->acfParam.filtfile[j].filename);
                return EW_FAILURE;
            }

        } /** while k_rd() **/

        nfiles = k_close();

    } /** while nfiles **/
    
    if(pDcm->acfParam.debug) {
        logit ("e", "%s Filter coeffs from: %s %d\n", whoami, pDcm->acfParam.filtfile[j].filename, pDcm->acfParam.debug);
        logit ("e", "Group Delay %f Number of Coeffs %d \n", pDcm->acfParam.filtfile[j].ThisFilt.TimeCorr, pDcm->acfParam.filtfile[j].ThisFilt.Length);
        for(i=0;i<pDcm->acfParam.filtfile[j].ThisFilt.Length; i++) 
            logit ("e", " %d  %f \n", i, pDcm->acfParam.filtfile[j].ThisFilt.coef[i]);
    }
    return EW_SUCCESS;
}


/*****************************************************************************/
/*                                                                           */
/* InitSta: Initialize filter buffers for all stations                       */
/*                                                                           */
/*      Function: InitSta                                                    */
/*                                                                           */
/*      Inputs:         Pointer to World Structure                           */
/*                                                                           */
/*      Outputs:        Updated Station structures                           */
/*                                                                           */
/*      Returns:        UW_SUCCESS on success, else UW_FAILURE               */
/*****************************************************************************/

void InitSta( WORLD *pFilt )
{
    int iSta;
    STATION *sta;             /* pointer to array of stations             */
  
  /* Set up filter buffer structures for all stations we know about     */
    for (iSta = 0; iSta < pFilt->nSta; iSta++) {
        sta = &(pFilt->stations[iSta]);
        ResetStation( sta );
    }
    
    return;
}
