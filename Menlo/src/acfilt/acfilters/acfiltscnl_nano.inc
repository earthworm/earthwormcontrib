#
#                     Configuration File for Acfilt
#
#
# List SCNL codes of trace messages to fir and their output SCN codes
# Wildcard characters are not allowed here.
#
#         IN-SCNL        OUT-SCNL         FILT
 GetSCNL JSGB EHZ NC --  JSGB EHZ NC --    Nano
 GetSCNL KBO  HHZ NC --  KBO  HHZ NC --    Nano
 GetSCNL KCPB HHZ NC --  KCPB HHZ NC --    Nano
 GetSCNL KCT  HHZ NC --  KCT  HHZ NC --    Nano
 GetSCNL KEB  HHZ NC --  KEB  HHZ NC --    Nano

 GetSCNL KHBB HHZ NC --  KHBB HHZ NC --    Nano
 GetSCNL KHMB HHZ NC --  KHMB HHZ NC --    Nano
 GetSCNL KMPB HHZ NC --  KMPB HHZ NC --    Nano
 GetSCNL KMR  HHZ NC --  KMR  HHZ NC --    Nano
 GetSCNL KRMB HHZ NC --  KRMB HHZ NC --    Nano

 GetSCNL KRP  HHZ NC --  KRP  HHZ NC --    Nano
 GetSCNL KSXB HHZ NC --  KSXB HHZ NC --    Nano
 GetSCNL LDH  HHZ NC --  LDH  HHZ NC --    Nano
 GetSCNL MCB  HHZ NC --  MCB  HHZ NC --    Nano
 GetSCNL MMLB HHZ NC --  MMLB HHZ NC --    Nano

 GetSCNL PMPB HHZ NC --  PMPB HHZ NC --    Nano  
 GetSCNL PSM  EHZ NC --  PSM  EHZ NC --    Nano


