#
#                     Configuration File for Acfilt
#
#
# List SCNL codes of trace messages to fir and their output SCN codes
# Wildcard characters are not allowed here.
#
#         IN-SCNL        OUT-SCNL         FILT
#
GetSCNL BKS  BHZ BK --  BKS  BHZ BK --    Q_40
GetSCNL CMB  BHZ BK --  CMB  BHZ BK --    Q_40
GetSCNL HOPS BHZ BK --  HOPS BHZ BK --    Q_40
GetSCNL JCC  BHZ BK --  JCC  BHZ BK --    Q_40
GetSCNL KCC  BHZ BK --  KCC  BHZ BK --    Q_40
GetSCNL MOD  BHZ BK --  MOD  BHZ BK --    Q_40
GetSCNL ORV  BHZ BK --  ORV  BHZ BK --    Q_40
GetSCNL PKD  BHZ BK --  PKD  BHZ BK --    Q_40
GetSCNL SAO  BHZ BK --  SAO  BHZ BK --    Q_40
GetSCNL WDC  BHZ BK --  WDC  BHZ BK --    Q_40
GetSCNL YBH  BHZ BK --  YBG  BHZ BK --    Q_40


