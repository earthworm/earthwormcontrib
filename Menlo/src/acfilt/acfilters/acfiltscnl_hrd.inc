#
#                     Configuration File for Acfilt
#
#
# List SCNL codes of trace messages to fir and their output SCN codes
# Wildcard characters are not allowed here.
#
#         IN-SCNL        OUT-SCNL         FILT
GetSCNL LAC  SHZ NC --  LAC  SHZ NC --    HRD24
GetSCNL LBK  SHZ NC --  LBK  SHZ NC --    HRD24
GetSCNL LBP  SHZ NC --  LBP  SHZ NC --    HRD24
GetSCNL LCM  SHZ NC --  LCM  SHZ NC --    HRD24
GetSCNL LCSB SHZ NC --  LCSB SHZ NC --    HRD24
#
GetSCNL LDB  SHZ NC --  LDB  SHZ NC --    HRD24
GetSCNL LEL  SHZ NC --  LEL  SHZ NC --    HRD24
GetSCNL LGP  SHZ NC --  LGP  SHZ NC --    HRD24
GetSCNL LME  SHZ NC --  LME  SHZ NC --    HRD24
GetSCNL LPG  SHZ NC --  LPG  SHZ NC --    HRD24
#
GetSCNL LRB  SHZ NC --  LRB  SHZ NC --    HRD24
GetSCNL LRD  SHZ NC --  LRD  SHZ NC --    HRD24
GetSCNL LRR  SHZ NC --  LRR  SHZ NC --    HRD24
GetSCNL LSF  SHZ NC --  LSF  SHZ NC --    HRD24
GetSCNL LSH  SHZ NC --  LSH  SHZ NC --    HRD24
#
GetSCNL LSI  SHZ NC --  LSI  SHZ NC --    HRD24
GetSCNL LSR  SHZ NC --  LSR  SHZ NC --    HRD24
GetSCNL LTC  SHZ NC --  LTC  SHZ NC --    HRD24
GetSCNL LVR  SHZ NC --  LVR  SHZ NC --    HRD24
GetSCNL LWH  SHZ NC --  LWH  SHZ NC --    HRD24
#

