#
#                     Configuration File for Acfilt
#
#
# List SCNL codes of trace messages to fir and their output SCN codes
# Wildcard characters are not allowed here.
#
#         IN-SCNL        OUT-SCNL         FILT
GetSCNL KCR  SHZ NC --  KCR  SHZ NC --    Lynx
GetSCNL KCS  SHZ NC --  KCS  SHZ NC --    Lynx
GetSCNL KJJ  SHZ NC --  KJJ  SHZ NC --    Lynx
GetSCNL KKP  SHZ NC --  KKP  SHZ NC --    Lynx
GetSCNL KOM  SHZ NC --  KOM  SHZ NC --    Lynx
#
GetSCNL KPP  SHZ NC --  KPP  SHZ NC --    Lynx
GetSCNL KSM  SHZ NC --  KSM  SHZ NC --    Lynx
GetSCNL KTR  SHZ NC --  KTR  SHZ NC --    Lynx
GetSCNL LAS  SHZ NC --  LAS  SHZ NC --    Lynx
GetSCNL LBC  SHZ NC --  LBC  SHZ NC --    Lynx
#
GetSCNL LBF  SHZ NC --  LBF  SHZ NC --    Lynx
GetSCNL LGB  SHZ NC --  LGB  SHZ NC --    Lynx
GetSCNL LMH  SHZ NC --  LMH  SHZ NC --    Lynx
GetSCNL LTI  SHZ NC --  LTI  SHZ NC --    Lynx
#

