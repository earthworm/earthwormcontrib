#
#                     Configuration File for Acfilt
#
#
# List SCNL codes of trace messages to fir and their output SCN codes
# Wildcard characters are not allowed here.
#
#         IN-SCNL        OUT-SCNL         FILT
 GetSCNL CCH1 HN1 NC --  CCH1 HN1 NC --    NanoD1
 GetSCNL CGP1 HN1 NC --  CGP1 HN1 NC --    NanoD1
 GetSCNL CMW1 HN1 NC --  CMW1 HN1 NC --    NanoD1
 GetSCNL CSU1 HN1 NC --  CSU1 HN1 NC --    NanoD1
 GetSCNL CYD1 HN1 NC --  CYD1 HN1 NC --    NanoD1
 GetSCNL MDP1 EP1 NC --  MDP1 EP1 NC --    NanoD1
 GetSCNL MMX1 EP1 NC --  MMX1 EP1 NC --    NanoD1
 GetSCNL MBS1 EP1 NC --  MBS1 EP1 NC --    NanoD1

 GetSCNL MDH1 DP1 NC --  MDH1 DP1 NC --    NanoD2
 GetSCNL MDH1 DP2 NC --  MDH1 DP2 NC --    NanoD2
 GetSCNL MDH1 DP3 NC --  MDH1 DP3 NC --    NanoD2



