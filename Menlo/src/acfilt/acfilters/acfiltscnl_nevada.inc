#
#                     Configuration File for Acfilt
#
#
# List SCNL codes of trace messages to fir and their output SCN codes
# Wildcard characters are not allowed here.
#
#         IN-SCNL        OUT-SCNL         FILT
 GetSCNL DON  EHZ NN --  DON  EHZ NN --    RefTek72a
 GetSCNL PEA  EHZ NN --  PEA  EHZ NN --    RefTek72a
 GetSCNL RUB  EHZ NN --  RUB  EHZ NN --    RefTek72a
 GetSCNL WAK  EHZ NN --  WAK  EHZ NN --    RefTek72a
 GetSCNL WIL  EHZ NN --  WIL  EHZ NN --    RefTek72a
 GetSCNL OMM  HHZ NN --  OMM  HHZ NN --    RefTek72a


