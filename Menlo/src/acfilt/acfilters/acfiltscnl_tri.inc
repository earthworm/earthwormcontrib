#
#                     Configuration File for Acfilt
#
#
# List SCNL codes of trace messages to fir and their output SCN codes
# Wildcard characters are not allowed here.
#
#         IN-SCNL        OUT-SCNL         FILT
GetSCNL BAP  SHZ NC --  BAP  SHZ NC --    Trident
GetSCNL BPO  SHZ NC --  BPO  SHZ NC --    Trident
GetSCNL GTC  SHZ NC --  GTC  SHZ NC --    Trident
GetSCNL KBN  SHZ NC --  KBN  SHZ NC --    Trident
GetSCNL KBS  SHZ NC --  KBS  SHZ NC --    Trident
#
GetSCNL KFP  SHZ NC --  KFP  SHZ NC --    Trident
GetSCNL KIP  SHZ NC --  KIP  SHZ NC --    Trident
GetSCNL KRK  SHZ NC --  KRK  SHZ NC --    Trident
GetSCNL LAM  SHZ NC --  LAM  SHZ NC --    Trident
GetSCNL LBO  SHZ NC --  LBO  SHZ NC --    Trident
#
GetSCNL LGM  SHZ NC --  LGM  SHZ NC --    Trident
GetSCNL LHE  SHZ NC --  LHE  SHZ NC --    Trident
GetSCNL LMP  SHZ NC --  LMP  SHZ NC --    Trident
GetSCNL LPK  SHZ NC --  LPK  SHZ NC --    Trident
GetSCNL PABB SHZ NC --  PABB SHZ NC --    Trident
#
GetSCNL PADB SHZ NC --  PADB SHZ NC --    Trident
GetSCNL PAN  SHZ NC --  PAN  SHZ NC --    Trident
GetSCNL PAP  SHZ NC --  PAP  SHZ NC --    Trident
GetSCNL PBI  SHZ NC --  PBI  SHZ NC --    Trident
GetSCNL PBM  SHZ NC --  PBM  SHZ NC --    Trident
#
GetSCNL PBP  SHZ NC --  PBP  SHZ NC --    Trident
GetSCNL PBW  SHZ NC --  PBW  SHZ NC --    Trident
GetSCNL PCB  SHZ NC --  PCB  SHZ NC --    Trident
GetSCNL PCC  SHZ NC --  PCC  SHZ NC --    Trident
GetSCNL PHC  SHZ NC --  PHC  SHZ NC --    Trident
#
GetSCNL PHP  SHZ NC --  PHP  SHZ NC --    Trident
GetSCNL PHR  SHZ NC --  PHR  SHZ NC --    Trident
GetSCNL PHS  SHZ NC --  PHS  SHZ NC --    Trident
GetSCNL PIR  SHZ NC --  PIR  SHZ NC --    Trident
GetSCNL PKY  SHZ NC --  PKY  SHZ NC --    Trident
#
GetSCNL PLO  SHZ NC --  PLO  SHZ NC --    Trident
GetSCNL PMC  SHZ NC --  PMC  SHZ NC --    Trident
GetSCNL PML  SHZ NC --  PML  SHZ NC --    Trident
GetSCNL PPB  SHZ NC --  PPB  SHZ NC --    Trident
GetSCNL PPO  SHZ NC --  PPO  SHZ NC --    Trident
#
GetSCNL PPT  SHZ NC --  PPT  SHZ NC --    Trident
GetSCNL PSA  SHZ NC --  PSA  SHZ NC --    Trident
GetSCNL PSC  SHZ NC --  PSC  SHZ NC --    Trident
GetSCNL PTA  SHZ NC --  PTA  SHZ NC --    Trident
GetSCNL PTQ  SHZ NC --  PTQ  SHZ NC --    Trident
#
GetSCNL PTR  SHZ NC --  PTR  SHZ NC --    Trident
#

