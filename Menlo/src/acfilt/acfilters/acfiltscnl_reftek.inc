#
#                     Configuration File for Acfilt
#
#
# List SCNL codes of trace messages to fir and their output SCN codes
# Wildcard characters are not allowed here.
#
#         IN-SCNL        OUT-SCNL         FILT
GetSCNL GDXB HHZ NC --  GDXB HHZ NC --    RefTek
GetSCNL JELB EHZ NC --  JELB EHZ NC --    RefTek
GetSCNL NHS  EHZ NC --  NHS  EHZ NC --    RefTek 
GetSCNL NSM  EHZ NC --  NSM  EHZ NC --    RefTek 
GetSCNL PAGB HHZ NC --  PAGB HHZ NC --    RefTek
GetSCNL PHOB EHZ NC --  PHOB EHZ NC --    RefTek
#
#

