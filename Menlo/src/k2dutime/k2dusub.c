/******************************************************************************
 *  k2dusub.c                                                                 *
 *  Read a K2-format .evt data file (from the NSMP system),                   *
 *  create SNW Collection Agent messages, and place them in a directory       *
 *  for the snwclient.  Returns number of TYPE_STRONGMOTION messages written  *
 *  to ring  from file.                                                       *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include <chron3.h>
#include <rw_strongmotion.h>
#include "sm_file2ew.h"

#include "nkwhdrs_jhl.h"              /* Kinemetrics header definitions */

#ifdef _INTEL
/*  macro returns byte-swapped version of given 16-bit unsigned integer */
#define BYTESWAP_UINT16(var) \
               ((unsigned short)(((unsigned short)(var)>>(unsigned char)8) + \
               ((unsigned short)(var)<<(unsigned char)8)))

/*  macro returns byte-swapped version of given 32-bit unsigned integer */
#define BYTESWAP_UINT32(var) \
                 ((unsigned long)(((unsigned long)(var)>>(unsigned char)24) + \
        (((unsigned long)(var)>>(unsigned char)8)&(unsigned long)0x0000FF00)+ \
        (((unsigned long)(var)<<(unsigned char)8)&(unsigned long)0x00FF0000)+ \
                                 ((unsigned long)(var)<<(unsigned char)24)))
#endif

#ifdef _SPARC
#define BYTESWAP_UINT16(var) (var)
#define BYTESWAP_UINT32(var) (var)
#endif

#define MAX_REC  4096      
#define PACKET_MAX_SIZE  3003      /* 2992 + 8 + 3, same as QT */
#define MAXTRACELTH 20000
#define GRAVITY 981.0      /* Gravity in cm/sec/sec */
#define SWAP(a,b) temp=(a);(a)=(b);(b)=temp
#define DECADE   315532800  /* Number of seconds betweeen 1/1/1970 and 1/1/1980 */
#define NAM_LEN 100	          /* length of full directory name          */
#define BMSG_LEN 15000

/* Functions in this source file 
 *******************************/
int nsmp2ew( FILE *fp, char *fname );
int build_snw( char *msg );
int Secs2DateTime(int nsecs, char *string2);
int read_tag( FILE *fp );
int read_head( FILE *fp );
int Bcheck_head( char* );
int read_frame( FILE *fp, unsigned long *channels );

void logit( char *, char *, ... );   /* logit.c      sys-independent  */

/* Globals from sm_file2ew.c
 ***************************/
extern CHANNELNAME ChannelName[MAXCHAN];  /* table for box/chan to SCNL conversion */
extern int     NchanNames;          /* number of channel names in table      */
/*extern char         GetFromDir[NAM_LEN];  directory to monitor for data     */
extern char    ArchiveDir[NAM_LEN]; /* directory to store known headers      */
extern char    OutputDir[NAM_LEN];  /* directory to put SNW messages     */
extern int     Debug;

char  station[12];

static int   print_tag = 1;
static int   print_head = 1;
static int   print_frame = 1;
KFF_TAG      tag;          /* received header tag */
MW_HEADER    head, ohead;
K2_HEADER    khead;
FRAME_HEADER frame;
static unsigned char g_k2mi_buff[PACKET_MAX_SIZE-9]; /* received data buffer   */

/******************************************************************************
 * nsmp2ew()  Read and process the .evt file.                                 *
 ******************************************************************************/
int nsmp2ew( FILE *fp, char *fname )
{
    char     whoami[50], serial[10], stid[10], fnew[90], aname[100], msg[200], Bmsg[BMSG_LEN];
    FILE     *fx;      
    int      i, j, k, ii, nscans, nmsg, unknown;  
    int      tlen, flen, nchans, maxchans, npts, stat;
    unsigned long  channels, minute, decade;
    double   dt, secs, sex;
    double   sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's        */
                                        /* time 0 and 1970-01-01 00:00:00.0 GMT    */
    struct Greg  g;

/* Initialize variables 
 **********************/
    sprintf(whoami, " %s: %s: ", "k2duagent", "nsmp2ew");
    memset( &frame,  0, sizeof(FRAME_HEADER) );
    memset( &tag,    0, sizeof(KFF_TAG) );
    memset( &ohead,  0, sizeof(K2_HEADER) );
    memset( &head,   0, sizeof(K2_HEADER) );

    decade = (365*10+2)*24*60*60;     /*  Kinemetric time starts 10 years after EW time  */

/* Close/reopen file in binary read
 **********************************/
    fclose( fp );
    fp = fopen( fname, "rb" );
    if( fp == NULL )               return 0;
    if(strstr(fname, ".txt") != 0) return 0;
    if(strstr(fname, ".evt") == 0) return 0;

    nmsg = 0;
        
/* First, just read the header & first frame to get some basic info. 
 ******************************************************************/
    rewind(fp);
    
    tlen = read_tag(fp);
	if ( tlen != EW_SUCCESS ) return -1;
    stat = read_head(fp);
    
    tlen = read_tag(fp);
	if ( tlen != EW_SUCCESS ) return -1;
    flen = tag.dataLength;
    read_frame(fp, &channels);
/*  logit("e", "%s   \n", fname);   */
    
    nscans = head.roParms.stream.duration;
    nchans = head.rwParms.misc.nchannels;
    dt  = 1.0/(10.0*(flen/(3.0*nchans)));
    maxchans = head.roParms.misc.maxChannels;
    
    if(head.roParms.stream.flags != 0) {
        logit("e", "%s %s not data. flags = %d \n", 
                    whoami, fname, head.roParms.stream.flags);
/*        return -1;     */
        if(head.roParms.stream.flags != 1) return 0;
    }
    
    sprintf(serial, "%d", head.rwParms.misc.serialNumber);
    unknown = 1;
    for(ii=0;ii<NchanNames;ii++) {
        if(strcmp(serial, ChannelName[ii].box) == 0) {
        strcpy(stid, ChannelName[ii].sta);
        sprintf(station, "%s-%s:", ChannelName[ii].net, ChannelName[ii].sta);
        if(strcmp(head.rwParms.misc.stnID, stid) == 0) unknown = 0;
       }
    }
    if(unknown) {
        sprintf(Bmsg, "NSMP site %s (%s) unknown to database. (%s) \n", 
        		head.rwParms.misc.stnID, head.rwParms.misc.comment, fname);
        logit("e", "%s %s  \n", whoami, Bmsg);
    } 
    
    sprintf(aname, "%s%s.%d", ArchiveDir, head.rwParms.misc.stnID, head.rwParms.misc.serialNumber);
    if(Debug) logit("e", "%s archive file: %s   \n", whoami, aname);
    fx = fopen( aname, "rb" );
    if( fx == NULL ) {
    	fx = fopen( aname, "w" );
		if( fx == NULL ) {
			logit( "e", "%s trouble opening archive file: %s \n", whoami, aname ); 
		} else {
	    	if(Debug) logit("e", "%s Write data to archive file: %s   \n", whoami, aname);
	    	fwrite(&head, 1, sizeof(MW_HEADER), fx);
	    	fclose(fx);
			logit( "e", "%s Created new archive file: %s \n", whoami, aname ); 
	        sprintf(Bmsg, "First data from NSMP site %s (%s). (%s) \nNew archive file created.\n", 
	        	head.rwParms.misc.stnID, head.rwParms.misc.comment, fname);
	        logit("e", "%s %s  \n", whoami, Bmsg);
		    
    	}
		return( 1 );
    } else {
    	fread(&ohead, 1, sizeof(MW_HEADER), fx);
    	fclose( fx );

		sprintf(Bmsg, "%s1:UsageLevel=3  \n", station );
	    /**/
	    secs = ohead.roParms.stream.startTime;
	    secs += decade;
        secs += sec1970;
        minute = (long) (secs / 60.0);
        sex = secs - 60.0 * minute;
        i = sex;
        j = (sex - i)*1000;
        grg(minute, &g);
	    sprintf(msg, "Previous header written: %2d/%2d/%4d at %2d:%2d.\n\n", 
	                  g.month, g.day, g.year, g.hour, g.minute);
	    
	    ii = build_snw(Bmsg);
	    
	    if(ii) {
	    	if(Debug) logit("e", "k2duagent: Messages for file: %s  site %s  \n %s  \n", 
	    				fname, head.rwParms.misc.stnID, Bmsg);
	    }
	    else {
	    	if(Debug) logit("e", "k2duagent: No changes for file: %s   \n", fname);
	    }
	    
    	fx = fopen( aname, "w" );
		if( fx == NULL ) {
			logit( "e", "%s: trouble opening archive file: %s \n",
			      whoami, aname ); 
		} else {
	    	fwrite(&head, 1, sizeof(MW_HEADER), fx);
	    	fclose(fx);
    	}
		
		sprintf(aname, "%s%s.%d", OutputDir, head.rwParms.misc.stnID, head.rwParms.misc.serialNumber);
		if(Debug) logit("e", "%s Output file: %s   \n", whoami, aname);
    	fx = fopen( aname, "w" );
		if( fx == NULL ) {
			logit( "e", "%s: trouble opening output file: %s \n",
			      whoami, aname ); 
		} else {
	    	fwrite(Bmsg, 1, strlen(Bmsg), fx);
	    	fclose(fx);
    	}
    }
        /*
        */
        
    return( 1 );
}

/******************************************************************************
 * build_snw(fp)  Build the SNW message from the header and other stuff.      *
 ******************************************************************************/
int build_snw( char *msg )
{
	char	string[150], string2[30];
	double	tolerance = 0.00002;
	double	elev_err = 10.0;
	double  d1;
	int     i, nchans, stat;
   
	stat = 0;
    
    nchans = head.roParms.misc.maxChannels;
    
    /*
    logit("e", "MISC_RO_PARMS: %d %d %d %hu %hu %hu %hu %hu %hu %d %hu %hu %d \n", 
        (int)head.roParms.misc.a2dBits, 
        (int)head.roParms.misc.sampleBytes, 
        (int)head.roParms.misc.restartSource, 
            head.roParms.misc.installedChan,
            head.roParms.misc.maxChannels,
            head.roParms.misc.sysBlkVersion,
            head.roParms.misc.bootBlkVersion,
            head.roParms.misc.appBlkVersion,
            head.roParms.misc.dspBlkVersion,
            head.roParms.misc.batteryVoltage,
            head.roParms.misc.crc,
            head.roParms.misc.flags,
            head.roParms.misc.temperature );
    */
	
	d1 = fabs(head.roParms.misc.batteryVoltage/10.0);
	sprintf(string, "%s1:K2 Internal Battery Voltage=%f  \n", station, d1 );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	
	d1 = fabs(head.roParms.misc.temperature/10.0);
	sprintf(string, "%s1:K2 Temperature in Celsius=%f  \n", station, d1 );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
    
    
    /*
    logit("e", "TIMING_RO_PARMS: %d %d %d %hu %hu %d %d %d %d %hu %d %d %lu %lu %lu %lu %lu %lu \n", 
        (int)head.roParms.timing.clockSource, 
        (int)head.roParms.timing.gpsStatus, 
        (int)head.roParms.timing.gpsSOH, 
            head.roParms.timing.gpsLockFailCount, 
            head.roParms.timing.gpsUpdateRTCCount, 
            head.roParms.timing.acqDelay, 
            head.roParms.timing.gpsLatitude, 
            head.roParms.timing.gpsLongitude, 
            head.roParms.timing.gpsAltitude, 
            head.roParms.timing.dacCount,
            head.roParms.timing.gpsLastDrift[0], 
            head.roParms.timing.gpsLastDrift[1], 
            
            head.roParms.timing.gpsLastTurnOnTime[0], 
            head.roParms.timing.gpsLastTurnOnTime[1], 
            head.roParms.timing.gpsLastUpdateTime[0], 
            head.roParms.timing.gpsLastUpdateTime[1], 
            head.roParms.timing.gpsLastLockTime[0], 
            head.roParms.timing.gpsLastLockTime[1] );
    */
	
  /* Clock Source
     0 = RTC from cold start
	 1 = keyboard
	 2 = Sync w/ ext. ref. pulse
	 3 = Internal GPS
	 */
	sprintf(string, "%s1:K2 Clock Source=%d  \n", station, head.roParms.timing.clockSource );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	
	/* GPS Status
	   Bit 0=1 if currently checking for presence of GPS board
	   Bit 1=1 if GPS board present
	   Bit 2=1 if error communicating with GPS
	   Bit 3=1 if failed to lock within an allotted time (gpsMaxTurnOnTime)
	   Bit 4=1 if not locked
	   Bit 5=1 when GPS power is ON
	   Bits 6,7=undefined
	 */
	sprintf(string, "%s1:K2 GPS Status=%d  \n", station, head.roParms.timing.gpsStatus );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
  /* GPS State of Health; same as Acutime SOH code 
     0 = Normal
	 1 = No GPS signal
	 3 = PDOP (Position Dilution of Precision) is too high
	 9 = Only 1 usable satellite
	 10 = Only 2 usable satellites
	 11 = Only 3 usable satellites
	 12 = Chosen satellite is unusable
	 */
	sprintf(string, "%s1:K2 GPS State of Health=%d  \n", station, head.roParms.timing.gpsSOH );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
	sprintf(string, "%s1:K2 GPS Lock Failures=%d  \n", station, head.roParms.timing.gpsLockFailCount );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
	sprintf(string, "%s1:K2 GPS RTC Update=%d  \n", station, head.roParms.timing.gpsUpdateRTCCount );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
	sprintf(string, "%s1:K2 GPS RTC Drift (msec)=%d  \n", station, head.roParms.timing.gpsLastDrift[0] );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	
	Secs2DateTime(head.roParms.timing.gpsLastTurnOnTime[0], string2);
	sprintf(string, "%s1:K2 GPS Last Turn On=%s  \n", station, string2 );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
	Secs2DateTime(head.roParms.timing.gpsLastUpdateTime[0], string2);
	sprintf(string, "%s1:K2 GPS Last Update=%s  \n", station, string2 );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
	Secs2DateTime(head.roParms.timing.gpsLastLockTime[0], string2);
	sprintf(string, "%s1:K2 GPS Last Lock=%s  \n", station, string2 );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
    
    for(i=0;i<nchans;i++) {
        /*
        logit("e", "CHANNEL_RO_PARMS: %d %lu %d %lu %d %d \n", 
            head.roParms.channel[i].maxPeak,  
            head.roParms.channel[i].maxPeakOffset,  
            head.roParms.channel[i].minPeak,  
            head.roParms.channel[i].minPeakOffset,  
            head.roParms.channel[i].mean,  
            head.roParms.channel[i].aqOffset );
        */
    }
    
        
    /*
    logit("e", "STREAM_RO_PARMS: %d %d %d %d %d %d %d %d \n", 
            head.roParms.stream.startTime,  
            head.roParms.stream.triggerTime,  
            head.roParms.stream.duration,  
            head.roParms.stream.errors,  
            head.roParms.stream.flags,  
            head.roParms.stream.startTimeMsec,  
            head.roParms.stream.triggerTimeMsec,  
            head.roParms.stream.nscans  );
    */
    /*
    logit("e", "MISC_RW_PARMS: %hu %hu %.5s %.33s %d %f %f %d %d %d %d %d %lo %d %.17s %d %d\n", 
            head.rwParms.misc.serialNumber,
            head.rwParms.misc.nchannels,
            head.rwParms.misc.stnID,  
            head.rwParms.misc.comment, 
            head.rwParms.misc.elevation,
            head.rwParms.misc.latitude, 
            head.rwParms.misc.longitude,
            
        (int)head.rwParms.misc.cutlerCode, 
        (int)head.rwParms.misc.minBatteryVoltage, 
        (int)head.rwParms.misc.cutler_decimation, 
        (int)head.rwParms.misc.cutler_irig_type, 
            head.rwParms.misc.cutler_bitmap,
            head.rwParms.misc.channel_bitmap,
        (int)head.rwParms.misc.cutler_protocol, 
            head.rwParms.misc.siteID, 
        (int)head.rwParms.misc.externalTrigger, 
        (int)head.rwParms.misc.networkFlag );
    */
    
    /*
    logit("e", "TIMING_RW_PARMS: %d %d %hu \n", 
        (int)head.rwParms.timing.gpsTurnOnInterval,  
        (int)head.rwParms.timing.gpsMaxTurnOnTime, 
            head.rwParms.timing.localOffset  );
    	*/
        
    for(i=0;i<nchans;i++) {
        /*
        logit("e", "CHANNEL_RW_PARMS: %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f %f %f %f %f %f %f %f %c %c %s %s \n", 
            head.rwParms.channel[i].id,  
            head.rwParms.channel[i].sensorSerialNumberExt,  
            head.rwParms.channel[i].north,  
            head.rwParms.channel[i].east,  
            head.rwParms.channel[i].up,  
            head.rwParms.channel[i].altitude,
            head.rwParms.channel[i].azimuth,
            head.rwParms.channel[i].sensorType,
            head.rwParms.channel[i].sensorSerialNumber,
            head.rwParms.channel[i].gain,
        (int)head.rwParms.channel[i].triggerType,  
        (int)head.rwParms.channel[i].iirTriggerFilter,  
        (int)head.rwParms.channel[i].StaSeconds,  
        (int)head.rwParms.channel[i].LtaSeconds,  
            head.rwParms.channel[i].StaLtaRatio,
        (int)head.rwParms.channel[i].StaLtaPercent,
          
            head.rwParms.channel[i].fullscale,
            head.rwParms.channel[i].sensitivity,
            head.rwParms.channel[i].damping,
            head.rwParms.channel[i].naturalFrequency,
            head.rwParms.channel[i].triggerThreshold,
            head.rwParms.channel[i].detriggerThreshold,
            head.rwParms.channel[i].alarmTriggerThreshold
            head.rwParms.channel[i].calCoil,
            head.rwParms.channel[i].range,
            head.rwParms.channel[i].sensorgain,
            head.rwParms.channel[i].networkcode,
            head.rwParms.channel[i].locationcode
        );
    	*/
    }
    
    
    /*
    logit("e", "STREAM_K2_RW_PARMS: |%d| |%d| |%d| %d %d %d %d %d %d %d %d %d %d %d %d %d %d %lu\n", 
        (int)head.rwParms.stream.filterFlag,  
        (int)head.rwParms.stream.primaryStorage,  
        (int)head.rwParms.stream.secondaryStorage,  
            head.rwParms.stream.eventNumber,  
            head.rwParms.stream.sps,  
            head.rwParms.stream.apw,  
            head.rwParms.stream.preEvent,  
            head.rwParms.stream.postEvent,  
            head.rwParms.stream.minRunTime,  
            head.rwParms.stream.VotesToTrigger,
            head.rwParms.stream.VotesToDetrigger,
        (int)head.rwParms.stream.FilterType,  
        (int)head.rwParms.stream.DataFmt,  
            head.rwParms.stream.Timeout,
            head.rwParms.stream.TxBlkSize,
            head.rwParms.stream.BufferSize,
            head.rwParms.stream.SampleRate,
            head.rwParms.stream.TxChanMap );
		*/
    
    
    return stat;
}


/******************************************************************************
 * Secs2DateTime(fp)  Put epoch seconds into date/time string.                *
 ******************************************************************************/
int Secs2DateTime(int nsecs, char *string2)
{
    int        i, j;
    double   sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's        */
                                        /* time 0 and 1970-01-01 00:00:00.0 GMT    */
    struct Greg  g;
    double   secs, sex;
    unsigned long  minute, decade;
    
    decade = (365*10+2)*24*60*60;     /*  Kinemetric time starts 10 years after EW time  */
    secs = nsecs;
    secs += decade;
    secs += sec1970;
    minute = (long) (secs / 60.0);
    /**/
    sex = secs - 60.0 * minute;
    i = sex;
    j = (sex - i)*1000;
    
    grg(minute, &g);
    sprintf(string2, "%.2d/%.2d/%4d %.2d:%.2d:%05.2f", 
                  g.month, g.day, g.year, g.hour, g.minute, sex);

    return 0;
}



/******************************************************************************
 * read_tag(fp)  Read the 16 byte tag, swap bytes if necessary, and print.    *
 ******************************************************************************/
int read_tag( FILE *fp )
{
    int        stat;
    
    stat = fread(&tag, 1, 16, fp);
    if (stat != 16) {
		logit ("e", "read_tag: read of file failed.\n");
		return EW_FAILURE;
	}
    tag.type       = BYTESWAP_UINT32(tag.type);
    tag.length     = BYTESWAP_UINT16(tag.length);
    tag.dataLength = BYTESWAP_UINT16(tag.dataLength);
    tag.id         = BYTESWAP_UINT16(tag.id);
    tag.checksum   = BYTESWAP_UINT16(tag.checksum);
    
    if(Debug && print_tag) {
        logit("e", "nsmp2ew: TAG: %c %d %d %d %d %d %d %d %d  \n", 
                tag.sync, 
                (int)tag.byteOrder,
                (int)tag.version, 
                (int)tag.instrumentType,
                tag.type, tag.length, tag.dataLength,
                tag.id, tag.checksum);
    }
    
	/* look ahead, and check on the upcoming record for sanity
	**********************************************************/
	{
		long fpos;
		char checkBuffer[MAX_REC];
		unsigned short checksum=0;
		int bytesToCheck;
		int i;

		fpos = ftell (fp); /* remember where we were */
		bytesToCheck = tag.length + tag.dataLength;
		if (bytesToCheck > MAX_REC) {
			logit ("e", "read_tag: record too long. (%d)\n", bytesToCheck);
			logit ("e", "record + data length > MAX_REC. (%d) \n", MAX_REC);
			return EW_FAILURE;
		}
		if (fread(checkBuffer, 1, bytesToCheck, fp) != bytesToCheck) {
			logit ("e", "read_tag: read of file failed.\n");
			return EW_FAILURE;
		}
		/* look at the synch character */
		if( tag.sync != 'K') {
			logit ("e", "read_tag: bad synch character. (%c)\n", tag.sync);
			return EW_FAILURE;
		}
		for ( i=0; i<bytesToCheck; i++)
			checksum = checksum + (unsigned char) checkBuffer[i];
		if (checksum != tag.checksum) {
			logit("","read_tag: checksum error %d %d\n", checksum, tag.checksum);
			return EW_FAILURE;
		}

		/* now put things back the way they were */
		fseek(fp, fpos, SEEK_SET );
	}

    return EW_SUCCESS;;
}


/******************************************************************************
 * read_head(fp)  Read the file header, swap bytes if necessary, and print.   *
 ******************************************************************************/
int read_head( FILE *fp )
{
   long       la;
   int        i, nchans, stat;
   
/* Read in the file header.
   If a K2, there will be 2040 bytes,
   otherwise assume a Mt Whitney.
 ************************************/
    /*
    stat = fread(&head, tag.length, 1, fp);
    */
    nchans = tag.length==2040? MAX_K2_CHANNELS:MAX_MW_CHANNELS;
    stat = fread(&head, 1, 8, fp);
    stat = fread(&head.roParms.misc,       1, sizeof(struct MISC_RO_PARMS)+sizeof(struct TIMING_RO_PARMS), fp);
    stat = fread(&head.roParms.channel[0], 1, sizeof(struct CHANNEL_RO_PARMS)*nchans, fp);
    stat = fread(&head.roParms.stream,     1, sizeof(struct STREAM_RO_PARMS), fp);
    
    stat = fread(&head.rwParms.misc,       1, sizeof(struct MISC_RW_PARMS)+sizeof(struct TIMING_RW_PARMS), fp);
    stat = fread(&head.rwParms.channel[0], 1, sizeof(struct CHANNEL_RW_PARMS)*nchans, fp);
    if(tag.length==2040) {
        stat = fread(&head.rwParms.stream, 1, sizeof(struct STREAM_K2_RW_PARMS), fp);
    } else {
        stat = fread(&head.rwParms.stream, 1, sizeof(struct STREAM_MW_RW_PARMS), fp);
    }
    stat = fread(&head.rwParms.modem,      1, sizeof(struct MODEM_RW_PARMS), fp);
    
    head.roParms.headerVersion = BYTESWAP_UINT16(head.roParms.headerVersion);
    head.roParms.headerBytes   = BYTESWAP_UINT16(head.roParms.headerBytes);
    
    if(Debug && print_head)
    logit("e", "HEADER: %c%c%c %d %hu %hu \n", 
            head.roParms.id[0], head.roParms.id[1], head.roParms.id[2], 
       (int)head.roParms.instrumentCode, 
            head.roParms.headerVersion, 
            head.roParms.headerBytes);
    
    head.roParms.misc.installedChan  = BYTESWAP_UINT16(head.roParms.misc.installedChan);
    head.roParms.misc.maxChannels    = BYTESWAP_UINT16(head.roParms.misc.maxChannels);
    head.roParms.misc.sysBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.sysBlkVersion);
    head.roParms.misc.bootBlkVersion = BYTESWAP_UINT16(head.roParms.misc.bootBlkVersion);
    head.roParms.misc.appBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.appBlkVersion);
    head.roParms.misc.dspBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.dspBlkVersion);
    head.roParms.misc.batteryVoltage = BYTESWAP_UINT16(head.roParms.misc.batteryVoltage);
    head.roParms.misc.crc            = BYTESWAP_UINT16(head.roParms.misc.crc);
    head.roParms.misc.flags          = BYTESWAP_UINT16(head.roParms.misc.flags);
    head.roParms.misc.temperature    = BYTESWAP_UINT16(head.roParms.misc.temperature);
    nchans = head.roParms.misc.maxChannels;
    
    if(Debug && print_head)
    logit("e", "MISC_RO_PARMS: %d %d %d %hu %hu %hu %hu %hu %hu %d %hu %hu %d \n", 
        (int)head.roParms.misc.a2dBits, 
        (int)head.roParms.misc.sampleBytes, 
        (int)head.roParms.misc.restartSource, 
            head.roParms.misc.installedChan,
            head.roParms.misc.maxChannels,
            head.roParms.misc.sysBlkVersion,
            head.roParms.misc.bootBlkVersion,
            head.roParms.misc.appBlkVersion,
            head.roParms.misc.dspBlkVersion,
            head.roParms.misc.batteryVoltage,
            head.roParms.misc.crc,
            head.roParms.misc.flags,
            head.roParms.misc.temperature );
    
    head.roParms.timing.gpsLockFailCount  = BYTESWAP_UINT16(head.roParms.timing.gpsLockFailCount);
    head.roParms.timing.gpsUpdateRTCCount = BYTESWAP_UINT16(head.roParms.timing.gpsUpdateRTCCount);
    head.roParms.timing.acqDelay          = BYTESWAP_UINT16(head.roParms.timing.acqDelay);
    head.roParms.timing.gpsLatitude       = BYTESWAP_UINT16(head.roParms.timing.gpsLatitude);
    head.roParms.timing.gpsLongitude      = BYTESWAP_UINT16(head.roParms.timing.gpsLongitude);
    head.roParms.timing.gpsAltitude       = BYTESWAP_UINT16(head.roParms.timing.gpsAltitude);
    head.roParms.timing.dacCount          = BYTESWAP_UINT16(head.roParms.timing.dacCount);
    head.roParms.timing.gpsLastDrift[0]   = BYTESWAP_UINT16(head.roParms.timing.gpsLastDrift[0]);
    head.roParms.timing.gpsLastDrift[1]   = BYTESWAP_UINT16(head.roParms.timing.gpsLastDrift[1]);
    
    head.roParms.timing.gpsLastTurnOnTime[0] = BYTESWAP_UINT32(head.roParms.timing.gpsLastTurnOnTime[0]);
    head.roParms.timing.gpsLastTurnOnTime[1] = BYTESWAP_UINT32(head.roParms.timing.gpsLastTurnOnTime[1]);
    head.roParms.timing.gpsLastUpdateTime[0] = BYTESWAP_UINT32(head.roParms.timing.gpsLastUpdateTime[0]);
    head.roParms.timing.gpsLastUpdateTime[1] = BYTESWAP_UINT32(head.roParms.timing.gpsLastUpdateTime[1]);
    head.roParms.timing.gpsLastLockTime[0]   = BYTESWAP_UINT32(head.roParms.timing.gpsLastLockTime[0]);
    head.roParms.timing.gpsLastLockTime[1]   = BYTESWAP_UINT32(head.roParms.timing.gpsLastLockTime[1]);
    
    if(Debug && print_head)
    logit("e", "TIMING_RO_PARMS: %d %d %d %hu %hu %d %d %d %d %hu %d %d %lu %lu %lu %lu %lu %lu \n", 
        (int)head.roParms.timing.clockSource, 
        (int)head.roParms.timing.gpsStatus, 
        (int)head.roParms.timing.gpsSOH, 
            head.roParms.timing.gpsLockFailCount, 
            head.roParms.timing.gpsUpdateRTCCount, 
            head.roParms.timing.acqDelay, 
            head.roParms.timing.gpsLatitude, 
            head.roParms.timing.gpsLongitude, 
            head.roParms.timing.gpsAltitude, 
            head.roParms.timing.dacCount,
            head.roParms.timing.gpsLastDrift[0], 
            head.roParms.timing.gpsLastDrift[1], 
            
            head.roParms.timing.gpsLastTurnOnTime[0], 
            head.roParms.timing.gpsLastTurnOnTime[1], 
            head.roParms.timing.gpsLastUpdateTime[0], 
            head.roParms.timing.gpsLastUpdateTime[1], 
            head.roParms.timing.gpsLastLockTime[0], 
            head.roParms.timing.gpsLastLockTime[1] );
           
    
    
    for(i=0;i<nchans;i++) {
        head.roParms.channel[i].maxPeak       = BYTESWAP_UINT32(head.roParms.channel[i].maxPeak);
        head.roParms.channel[i].maxPeakOffset = BYTESWAP_UINT32(head.roParms.channel[i].maxPeakOffset);
        head.roParms.channel[i].minPeak       = BYTESWAP_UINT32(head.roParms.channel[i].minPeak);
        head.roParms.channel[i].minPeakOffset = BYTESWAP_UINT32(head.roParms.channel[i].minPeakOffset);
        head.roParms.channel[i].mean          = BYTESWAP_UINT32(head.roParms.channel[i].mean);
        head.roParms.channel[i].aqOffset      = BYTESWAP_UINT32(head.roParms.channel[i].aqOffset);
        
        if(Debug && print_head)
        logit("e", "CHANNEL_RO_PARMS: %d %lu %d %lu %d %d \n", 
            head.roParms.channel[i].maxPeak,  
            head.roParms.channel[i].maxPeakOffset,  
            head.roParms.channel[i].minPeak,  
            head.roParms.channel[i].minPeakOffset,  
            head.roParms.channel[i].mean,  
            head.roParms.channel[i].aqOffset );
        
    }
    
    
    head.roParms.stream.startTime       = BYTESWAP_UINT32(head.roParms.stream.startTime);
    head.roParms.stream.triggerTime     = BYTESWAP_UINT32(head.roParms.stream.triggerTime);
    head.roParms.stream.duration        = BYTESWAP_UINT32(head.roParms.stream.duration);
    head.roParms.stream.errors          = BYTESWAP_UINT16(head.roParms.stream.errors);
    head.roParms.stream.flags           = BYTESWAP_UINT16(head.roParms.stream.flags);
    head.roParms.stream.nscans          = BYTESWAP_UINT32(head.roParms.stream.nscans);
    head.roParms.stream.startTimeMsec   = BYTESWAP_UINT16(head.roParms.stream.startTimeMsec);
    head.roParms.stream.triggerTimeMsec = BYTESWAP_UINT16(head.roParms.stream.triggerTimeMsec);
    
    if(Debug && print_head)
    logit("e", "STREAM_RO_PARMS: %d %d %d %d %d %d %d %d \n", 
            head.roParms.stream.startTime,  
            head.roParms.stream.triggerTime,  
            head.roParms.stream.duration,  
            head.roParms.stream.errors,  
            head.roParms.stream.flags,  
            head.roParms.stream.startTimeMsec,  
            head.roParms.stream.triggerTimeMsec,  
            head.roParms.stream.nscans  );
    
    head.rwParms.misc.serialNumber   = BYTESWAP_UINT16(head.rwParms.misc.serialNumber);
    head.rwParms.misc.nchannels      = BYTESWAP_UINT16(head.rwParms.misc.nchannels);
    head.rwParms.misc.elevation      = BYTESWAP_UINT16(head.rwParms.misc.elevation);
                                  la = BYTESWAP_UINT32(*(long *)(&head.rwParms.misc.latitude));
    head.rwParms.misc.latitude       = *(float *)(&(la));
                                  la = BYTESWAP_UINT32(*(long *)(&head.rwParms.misc.longitude));
    head.rwParms.misc.longitude      = *(float *)(&la);
    head.rwParms.misc.cutler_bitmap  = BYTESWAP_UINT32(head.rwParms.misc.cutler_bitmap);
    head.rwParms.misc.channel_bitmap = BYTESWAP_UINT32(head.rwParms.misc.channel_bitmap);
    
    if(Debug && print_head)
    logit("e", "MISC_RW_PARMS: %hu %hu %.5s %.33s %d %f %f %d %d %d %d %d %lo %d %.17s %d %d\n", 
            head.rwParms.misc.serialNumber,
            head.rwParms.misc.nchannels,
            head.rwParms.misc.stnID,  
            head.rwParms.misc.comment, 
            head.rwParms.misc.elevation,
            head.rwParms.misc.latitude, 
            head.rwParms.misc.longitude,
            
        (int)head.rwParms.misc.cutlerCode, 
        (int)head.rwParms.misc.minBatteryVoltage, 
        (int)head.rwParms.misc.cutler_decimation, 
        (int)head.rwParms.misc.cutler_irig_type, 
            head.rwParms.misc.cutler_bitmap,
            head.rwParms.misc.channel_bitmap,
        (int)head.rwParms.misc.cutler_protocol, 
            head.rwParms.misc.siteID, 
        (int)head.rwParms.misc.externalTrigger, 
        (int)head.rwParms.misc.networkFlag );
    
    head.rwParms.timing.localOffset = BYTESWAP_UINT16(head.rwParms.timing.localOffset);
    
    if(Debug && print_head)
    logit("e", "TIMING_RW_PARMS: %d %d %hu \n", 
        (int)head.rwParms.timing.gpsTurnOnInterval,  
        (int)head.rwParms.timing.gpsMaxTurnOnTime, 
            head.rwParms.timing.localOffset  );
    
    for(i=0;i<nchans;i++) {
        head.rwParms.channel[i].north      = BYTESWAP_UINT16(head.rwParms.channel[i].north);
        head.rwParms.channel[i].east       = BYTESWAP_UINT16(head.rwParms.channel[i].east);
        head.rwParms.channel[i].up         = BYTESWAP_UINT16(head.rwParms.channel[i].up);
        head.rwParms.channel[i].altitude   = BYTESWAP_UINT16(head.rwParms.channel[i].altitude);
        head.rwParms.channel[i].azimuth    = BYTESWAP_UINT16(head.rwParms.channel[i].azimuth);
        head.rwParms.channel[i].sensorType = BYTESWAP_UINT16(head.rwParms.channel[i].sensorType);
        head.rwParms.channel[i].sensorSerialNumber    = BYTESWAP_UINT16(head.rwParms.channel[i].sensorSerialNumber);
        head.rwParms.channel[i].sensorSerialNumberExt = BYTESWAP_UINT16(head.rwParms.channel[i].sensorSerialNumberExt);
        head.rwParms.channel[i].gain                  = BYTESWAP_UINT16(head.rwParms.channel[i].gain);
        head.rwParms.channel[i].StaLtaRatio           = BYTESWAP_UINT16(head.rwParms.channel[i].StaLtaRatio);
        
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].fullscale));
        head.rwParms.channel[i].fullscale             = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].sensitivity));
        head.rwParms.channel[i].sensitivity           = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].damping));
        head.rwParms.channel[i].damping               = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].naturalFrequency));
        head.rwParms.channel[i].naturalFrequency      = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].triggerThreshold));
        head.rwParms.channel[i].triggerThreshold      = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].detriggerThreshold));
        head.rwParms.channel[i].detriggerThreshold    = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].alarmTriggerThreshold));
        head.rwParms.channel[i].alarmTriggerThreshold = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].calCoil));
        head.rwParms.channel[i].calCoil               = *(float *)(&la);
        if(head.rwParms.channel[i].range != ' ' && (head.rwParms.channel[i].range < '0' || head.rwParms.channel[i].range > '9') )
        	head.rwParms.channel[i].range = ' ';
        if(head.rwParms.channel[i].sensorgain != ' ' && (head.rwParms.channel[i].sensorgain < '0' || head.rwParms.channel[i].sensorgain > '9') )
        	head.rwParms.channel[i].sensorgain = ' '; 
        	
        if(Debug && print_head)
        logit("e", "CHANNEL_RW_PARMS: %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f %f %f %f %f %f %f %f %c %c %s %s \n", 
            head.rwParms.channel[i].id,  
            head.rwParms.channel[i].sensorSerialNumberExt,  
            head.rwParms.channel[i].north,  
            head.rwParms.channel[i].east,  
            head.rwParms.channel[i].up,  
            head.rwParms.channel[i].altitude,
            head.rwParms.channel[i].azimuth,
            head.rwParms.channel[i].sensorType,
            head.rwParms.channel[i].sensorSerialNumber,
            head.rwParms.channel[i].gain,
        (int)head.rwParms.channel[i].triggerType,  
        (int)head.rwParms.channel[i].iirTriggerFilter,  
        (int)head.rwParms.channel[i].StaSeconds,  
        (int)head.rwParms.channel[i].LtaSeconds,  
            head.rwParms.channel[i].StaLtaRatio,
        (int)head.rwParms.channel[i].StaLtaPercent,  
            head.rwParms.channel[i].fullscale,
            head.rwParms.channel[i].sensitivity,
            head.rwParms.channel[i].damping,
            head.rwParms.channel[i].naturalFrequency,
            head.rwParms.channel[i].triggerThreshold,
            head.rwParms.channel[i].detriggerThreshold,
            head.rwParms.channel[i].alarmTriggerThreshold,
            head.rwParms.channel[i].calCoil,
            head.rwParms.channel[i].range,
            head.rwParms.channel[i].sensorgain,
            head.rwParms.channel[i].networkcode,
            head.rwParms.channel[i].locationcode
        );
        
    }
    
    
    head.rwParms.stream.eventNumber = BYTESWAP_UINT16(head.rwParms.stream.eventNumber);
    head.rwParms.stream.sps         = BYTESWAP_UINT16(head.rwParms.stream.sps);
    head.rwParms.stream.apw         = BYTESWAP_UINT16(head.rwParms.stream.apw);
    head.rwParms.stream.preEvent    = BYTESWAP_UINT16(head.rwParms.stream.preEvent);
    head.rwParms.stream.postEvent   = BYTESWAP_UINT16(head.rwParms.stream.postEvent);
    head.rwParms.stream.minRunTime  = BYTESWAP_UINT16(head.rwParms.stream.minRunTime);
    head.rwParms.stream.Timeout     = BYTESWAP_UINT16(head.rwParms.stream.Timeout);
    head.rwParms.stream.TxBlkSize   = BYTESWAP_UINT16(head.rwParms.stream.TxBlkSize);
    head.rwParms.stream.BufferSize  = BYTESWAP_UINT16(head.rwParms.stream.BufferSize);
    head.rwParms.stream.SampleRate  = BYTESWAP_UINT16(head.rwParms.stream.SampleRate);
    head.rwParms.stream.TxChanMap   = BYTESWAP_UINT32(head.rwParms.stream.TxChanMap);
    head.rwParms.stream.VotesToTrigger   = BYTESWAP_UINT16(head.rwParms.stream.VotesToTrigger);
    head.rwParms.stream.VotesToDetrigger = BYTESWAP_UINT16(head.rwParms.stream.VotesToDetrigger);
    
    
    if(Debug && print_head)
    logit("e", "STREAM_K2_RW_PARMS: |%d| |%d| |%d| %d %d %d %d %d %d %d %d %d %d %d %d %d %d %lu\n", 
        (int)head.rwParms.stream.filterFlag,  
        (int)head.rwParms.stream.primaryStorage,  
        (int)head.rwParms.stream.secondaryStorage,  
            head.rwParms.stream.eventNumber,  
            head.rwParms.stream.sps,  
            head.rwParms.stream.apw,  
            head.rwParms.stream.preEvent,  
            head.rwParms.stream.postEvent,  
            head.rwParms.stream.minRunTime,  
            head.rwParms.stream.VotesToTrigger,
            head.rwParms.stream.VotesToDetrigger,
        (int)head.rwParms.stream.FilterType,  
        (int)head.rwParms.stream.DataFmt,  
            head.rwParms.stream.Timeout,
            head.rwParms.stream.TxBlkSize,
            head.rwParms.stream.BufferSize,
            head.rwParms.stream.SampleRate,
            head.rwParms.stream.TxChanMap
              );
            
    return stat;
}


/******************************************************************************
 * Bcheck_head(fp)  Check the file header against the historic header.        *
 ******************************************************************************/
int Bcheck_head( char *msg )
{
	char	string[150];
	double	tolerance = 0.00002;
	double	elev_err = 10.0;
	int     i, nchans, stat;
   
	stat = 0;
    /*
    logit("e", "HEADER: %c%c%c %d %hu %hu \n", 
            head.roParms.id[0], head.roParms.id[1], head.roParms.id[2], 
       (int)head.roParms.instrumentCode, 
            head.roParms.headerVersion, 
            head.roParms.headerBytes);
    */
    if(ohead.roParms.instrumentCode != head.roParms.instrumentCode) {
	    stat = 1;
	    sprintf(string, "HEADER: instrumentCode %d has changed to: %d  \n", 
	            ohead.roParms.instrumentCode, head.roParms.instrumentCode );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.headerVersion != head.roParms.headerVersion) {
	    stat = 1;
	    sprintf(string, "HEADER: headerVersion %hu has changed to: %hu  \n", 
	            ohead.roParms.headerVersion, head.roParms.headerVersion );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.headerBytes != head.roParms.headerBytes) {
	    stat = 1;
	    sprintf(string, "HEADER: headerBytes %hu has changed to: %hu  \n", 
	            ohead.roParms.headerBytes, head.roParms.headerBytes );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    nchans = head.roParms.misc.maxChannels;
    
    /*
    logit("e", "MISC_RO_PARMS: %d %d %d %hu %hu %hu %hu %hu %hu %d %hu %hu %d \n", 
        (int)head.roParms.misc.a2dBits, 
        (int)head.roParms.misc.sampleBytes, 
        (int)head.roParms.misc.restartSource, 
            head.roParms.misc.installedChan,
            head.roParms.misc.maxChannels,
            head.roParms.misc.sysBlkVersion,
            head.roParms.misc.bootBlkVersion,
            head.roParms.misc.appBlkVersion,
            head.roParms.misc.dspBlkVersion,
            head.roParms.misc.batteryVoltage,
            head.roParms.misc.crc,
            head.roParms.misc.flags,
            head.roParms.misc.temperature );
    */
    
    if(ohead.roParms.misc.a2dBits != head.roParms.misc.a2dBits) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: a2dBits %d has changed to: %d  \n", 
	            ohead.roParms.misc.a2dBits, head.roParms.misc.a2dBits );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.sampleBytes != head.roParms.misc.sampleBytes) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: sampleBytes %d has changed to: %d  \n", 
	            ohead.roParms.misc.sampleBytes, head.roParms.misc.sampleBytes );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.restartSource != head.roParms.misc.restartSource) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: restartSource %d has changed to: %d  \n", 
	            ohead.roParms.misc.restartSource, head.roParms.misc.restartSource );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.installedChan != head.roParms.misc.installedChan) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: installedChan %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.installedChan, head.roParms.misc.installedChan );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.maxChannels != head.roParms.misc.maxChannels) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: maxChannels %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.maxChannels, head.roParms.misc.maxChannels );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.sysBlkVersion != head.roParms.misc.sysBlkVersion) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: sysBlkVersion %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.sysBlkVersion, head.roParms.misc.sysBlkVersion );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.bootBlkVersion != head.roParms.misc.bootBlkVersion) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: bootBlkVersion %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.bootBlkVersion, head.roParms.misc.bootBlkVersion );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.appBlkVersion != head.roParms.misc.appBlkVersion) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: appBlkVersion %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.appBlkVersion, head.roParms.misc.appBlkVersion );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.dspBlkVersion != head.roParms.misc.dspBlkVersion) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: dspBlkVersion %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.dspBlkVersion, head.roParms.misc.dspBlkVersion );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
            /*
    if(ohead.roParms.misc.batteryVoltage != head.roParms.misc.batteryVoltage) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: batteryVoltage %d has changed to: %d  \n", 
	            ohead.roParms.misc.batteryVoltage, head.roParms.misc.batteryVoltage );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
            */
    if(ohead.roParms.misc.crc != head.roParms.misc.crc) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: crc %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.crc, head.roParms.misc.crc );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    if(ohead.roParms.misc.flags != head.roParms.misc.flags) {
	    stat = 1;
	    sprintf(string, "MISC_RO_PARMS: flags %hu has changed to: %hu  \n", 
	            ohead.roParms.misc.flags, head.roParms.misc.flags );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    
    /*
    logit("e", "TIMING_RO_PARMS: %d %d %d %hu %hu %d %d %d %d %hu %d %d %lu %lu %lu %lu %lu %lu \n", 
        (int)head.roParms.timing.clockSource, 
        (int)head.roParms.timing.gpsStatus, 
        (int)head.roParms.timing.gpsSOH, 
            head.roParms.timing.gpsLockFailCount, 
            head.roParms.timing.gpsUpdateRTCCount, 
            head.roParms.timing.acqDelay, 
            head.roParms.timing.gpsLatitude, 
            head.roParms.timing.gpsLongitude, 
            head.roParms.timing.gpsAltitude, 
            head.roParms.timing.dacCount,
            head.roParms.timing.gpsLastDrift[0], 
            head.roParms.timing.gpsLastDrift[1], 
            
            head.roParms.timing.gpsLastTurnOnTime[0], 
            head.roParms.timing.gpsLastTurnOnTime[1], 
            head.roParms.timing.gpsLastUpdateTime[0], 
            head.roParms.timing.gpsLastUpdateTime[1], 
            head.roParms.timing.gpsLastLockTime[0], 
            head.roParms.timing.gpsLastLockTime[1] );
    */
    
    if(ohead.roParms.timing.clockSource != head.roParms.timing.clockSource) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: clockSource %d has changed to: %d  \n", 
	            ohead.roParms.timing.clockSource, head.roParms.timing.clockSource );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    /*
    if(ohead.roParms.timing.gpsStatus != head.roParms.timing.gpsStatus) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: gpsStatus %d has changed to: %d  \n", 
	            ohead.roParms.timing.gpsStatus, head.roParms.timing.gpsStatus );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.timing.gpsSOH != head.roParms.timing.gpsSOH) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: gpsSOH %d has changed to: %d  \n", 
	            ohead.roParms.timing.gpsSOH, head.roParms.timing.gpsSOH );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    */
    if(ohead.roParms.timing.acqDelay != head.roParms.timing.acqDelay) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: acqDelay %d has changed to: %d  \n", 
	            ohead.roParms.timing.acqDelay, head.roParms.timing.acqDelay );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    /*
    if(ohead.roParms.timing.gpsLatitude != head.roParms.timing.gpsLatitude) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: gpsLatitude %d has changed to: %d  \n", 
	            ohead.roParms.timing.gpsLatitude, head.roParms.timing.gpsLatitude );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.timing.gpsLongitude != head.roParms.timing.gpsLongitude) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: gpsLongitude %d has changed to: %d  \n", 
	            ohead.roParms.timing.gpsLongitude, head.roParms.timing.gpsLongitude );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.timing.gpsAltitude != head.roParms.timing.gpsAltitude) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: gpsAltitude %d has changed to: %d  \n", 
		            ohead.roParms.timing.gpsAltitude, head.roParms.timing.gpsAltitude ); } 
    
    if(ohead.roParms.timing.dacCount != head.roParms.timing.dacCount) {
	    stat = 1;
	    sprintf(string, "TIMING_RO_PARMS: dacCount %d has changed to: %d  \n", 
	            ohead.roParms.timing.dacCount, head.roParms.timing.dacCount );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    */
    
    for(i=0;i<nchans;i++) {
        /*
        logit("e", "CHANNEL_RO_PARMS: %d %lu %d %lu %d %d \n", 
            head.roParms.channel[i].maxPeak,  
            head.roParms.channel[i].maxPeakOffset,  
            head.roParms.channel[i].minPeak,  
            head.roParms.channel[i].minPeakOffset,  
            head.roParms.channel[i].mean,  
            head.roParms.channel[i].aqOffset );
        */
    }
    
        
    /*
    logit("e", "STREAM_RO_PARMS: %d %d %d %d %d %d %d %d \n", 
            head.roParms.stream.startTime,  
            head.roParms.stream.triggerTime,  
            head.roParms.stream.duration,  
            head.roParms.stream.errors,  
            head.roParms.stream.flags,  
            head.roParms.stream.startTimeMsec,  
            head.roParms.stream.triggerTimeMsec,  
            head.roParms.stream.nscans  );
    */
    /*
    if(ohead.roParms.stream.startTime != head.roParms.stream.startTime) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: startTime %d has changed to: %d  \n", 
	            ohead.roParms.stream.startTime, head.roParms.stream.startTime );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.triggerTime != head.roParms.stream.triggerTime) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: triggerTime %d has changed to: %d  \n", 
	            ohead.roParms.stream.triggerTime, head.roParms.stream.triggerTime );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.duration != head.roParms.stream.duration) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: duration %d has changed to: %d  \n", 
	            ohead.roParms.stream.duration, head.roParms.stream.duration );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.errors != head.roParms.stream.errors) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: errors %d has changed to: %d  \n", 
	            ohead.roParms.stream.errors, head.roParms.stream.errors );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.flags != head.roParms.stream.flags) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: flags %d has changed to: %d  \n", 
	            ohead.roParms.stream.flags, head.roParms.stream.flags );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.startTimeMsec != head.roParms.stream.startTimeMsec) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: startTimeMsec %d has changed to: %d  \n", 
	            ohead.roParms.stream.startTimeMsec, head.roParms.stream.startTimeMsec );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.triggerTimeMsec != head.roParms.stream.triggerTimeMsec) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: triggerTimeMsec %d has changed to: %d  \n", 
	            ohead.roParms.stream.triggerTimeMsec, head.roParms.stream.triggerTimeMsec );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.roParms.stream.nscans != head.roParms.stream.nscans) {
	    stat = 1;
	    sprintf(string, "STREAM_RO_PARMS: nscans %d has changed to: %d  \n", 
	            ohead.roParms.stream.nscans, head.roParms.stream.nscans );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    */
    /*
    logit("e", "MISC_RW_PARMS: %hu %hu %.5s %.33s %d %f %f %d %d %d %d %d %lo %d %.17s %d %d\n", 
            head.rwParms.misc.serialNumber,
            head.rwParms.misc.nchannels,
            head.rwParms.misc.stnID,  
            head.rwParms.misc.comment, 
            head.rwParms.misc.elevation,
            head.rwParms.misc.latitude, 
            head.rwParms.misc.longitude,
            
        (int)head.rwParms.misc.cutlerCode, 
        (int)head.rwParms.misc.minBatteryVoltage, 
        (int)head.rwParms.misc.cutler_decimation, 
        (int)head.rwParms.misc.cutler_irig_type, 
            head.rwParms.misc.cutler_bitmap,
            head.rwParms.misc.channel_bitmap,
        (int)head.rwParms.misc.cutler_protocol, 
            head.rwParms.misc.siteID, 
        (int)head.rwParms.misc.externalTrigger, 
        (int)head.rwParms.misc.networkFlag );
    */
    if(ohead.rwParms.misc.serialNumber != head.rwParms.misc.serialNumber) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: serialNumber %hu has changed to: %hu  \n", 
	            ohead.rwParms.misc.serialNumber, head.rwParms.misc.serialNumber );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.nchannels != head.rwParms.misc.nchannels) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: nchannels %hu has changed to: %hu  \n", 
	            ohead.rwParms.misc.nchannels, head.rwParms.misc.nchannels );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
	if(strcmp(ohead.rwParms.misc.stnID, head.rwParms.misc.stnID) != 0) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: stnID %s has changed to: %s  \n", 
	            ohead.rwParms.misc.stnID, head.rwParms.misc.stnID );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(abs(ohead.rwParms.misc.elevation - head.rwParms.misc.elevation) > elev_err) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: elevation %d has changed to: %d  \n", 
	            ohead.rwParms.misc.elevation, head.rwParms.misc.elevation );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(fabs(ohead.rwParms.misc.latitude - head.rwParms.misc.latitude) > tolerance) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: latitude %f has changed to: %f  \n", 
	            ohead.rwParms.misc.latitude, head.rwParms.misc.latitude );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(fabs(ohead.rwParms.misc.longitude - head.rwParms.misc.longitude) > tolerance) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: longitude %f has changed to: %f  \n", 
	            ohead.rwParms.misc.longitude, head.rwParms.misc.longitude );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.cutlerCode != head.rwParms.misc.cutlerCode) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: cutlerCode %d has changed to: %d  \n", 
	            ohead.rwParms.misc.cutlerCode, head.rwParms.misc.cutlerCode );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.minBatteryVoltage != head.rwParms.misc.minBatteryVoltage) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: minBatteryVoltage %d has changed to: %d  \n", 
	            ohead.rwParms.misc.minBatteryVoltage, head.rwParms.misc.minBatteryVoltage );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.cutler_decimation != head.rwParms.misc.cutler_decimation) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: cutler_decimation %d has changed to: %d  \n", 
	            ohead.rwParms.misc.cutler_decimation, head.rwParms.misc.cutler_decimation );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.cutler_irig_type != head.rwParms.misc.cutler_irig_type) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: cutler_irig_type %d has changed to: %d  \n", 
	            ohead.rwParms.misc.cutler_irig_type, head.rwParms.misc.cutler_irig_type );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.cutler_bitmap != head.rwParms.misc.cutler_bitmap) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: cutler_bitmap %d has changed to: %d  \n", 
	            ohead.rwParms.misc.cutler_bitmap, head.rwParms.misc.cutler_bitmap );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.channel_bitmap != head.rwParms.misc.channel_bitmap) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: channel_bitmap %lo has changed to: %lo  \n", 
	            ohead.rwParms.misc.channel_bitmap, head.rwParms.misc.channel_bitmap );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.cutler_protocol != head.rwParms.misc.cutler_protocol) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: cutler_protocol %d has changed to: %d  \n", 
	            ohead.rwParms.misc.cutler_protocol, head.rwParms.misc.cutler_protocol );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
	if(strcmp(ohead.rwParms.misc.siteID, head.rwParms.misc.siteID) != 0) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: siteID %s has changed to: %s  \n", 
	            ohead.rwParms.misc.siteID, head.rwParms.misc.siteID );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.externalTrigger != head.rwParms.misc.externalTrigger) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: externalTrigger %d has changed to: %d  \n", 
	            ohead.rwParms.misc.externalTrigger, head.rwParms.misc.externalTrigger );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.misc.networkFlag != head.rwParms.misc.networkFlag) {
	    stat = 1;
	    sprintf(string, "MISC_RW_PARMS: networkFlag %d has changed to: %d  \n", 
	            ohead.rwParms.misc.networkFlag, head.rwParms.misc.networkFlag );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    /*
    logit("e", "TIMING_RW_PARMS: %d %d %hu \n", 
        (int)head.rwParms.timing.gpsTurnOnInterval,  
        (int)head.rwParms.timing.gpsMaxTurnOnTime, 
            head.rwParms.timing.localOffset  );
    	*/
    
    if(ohead.rwParms.timing.gpsTurnOnInterval != head.rwParms.timing.gpsTurnOnInterval) {
	    stat = 1;
	    sprintf(string, "TIMING_RW_PARMS: gpsTurnOnInterval %d has changed to: %d  \n", 
	            ohead.rwParms.timing.gpsTurnOnInterval, head.rwParms.timing.gpsTurnOnInterval );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}

    if(ohead.rwParms.timing.gpsMaxTurnOnTime != head.rwParms.timing.gpsMaxTurnOnTime) {
	    stat = 1;
	    sprintf(string, "TIMING_RW_PARMS: gpsMaxTurnOnTime %d has changed to: %d  \n", 
	            ohead.rwParms.timing.gpsMaxTurnOnTime, head.rwParms.timing.gpsMaxTurnOnTime );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}

    if(ohead.rwParms.timing.localOffset != head.rwParms.timing.localOffset) {
	    stat = 1;
	    sprintf(string, "TIMING_RW_PARMS: localOffset %hu has changed to: %hu  \n", 
	            ohead.rwParms.timing.localOffset, head.rwParms.timing.localOffset );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    for(i=0;i<nchans;i++) {
        /*
        logit("e", "CHANNEL_RW_PARMS: %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f %f %f %f %f %f %f %f %c %c %s %s \n", 
            head.rwParms.channel[i].id,  
            head.rwParms.channel[i].sensorSerialNumberExt,  
            head.rwParms.channel[i].north,  
            head.rwParms.channel[i].east,  
            head.rwParms.channel[i].up,  
            head.rwParms.channel[i].altitude,
            head.rwParms.channel[i].azimuth,
            head.rwParms.channel[i].sensorType,
            head.rwParms.channel[i].sensorSerialNumber,
            head.rwParms.channel[i].gain,
        (int)head.rwParms.channel[i].triggerType,  
        (int)head.rwParms.channel[i].iirTriggerFilter,  
        (int)head.rwParms.channel[i].StaSeconds,  
        (int)head.rwParms.channel[i].LtaSeconds,  
            head.rwParms.channel[i].StaLtaRatio,
        (int)head.rwParms.channel[i].StaLtaPercent,
          
            head.rwParms.channel[i].fullscale,
            head.rwParms.channel[i].sensitivity,
            head.rwParms.channel[i].damping,
            head.rwParms.channel[i].naturalFrequency,
            head.rwParms.channel[i].triggerThreshold,
            head.rwParms.channel[i].detriggerThreshold,
            head.rwParms.channel[i].alarmTriggerThreshold
            head.rwParms.channel[i].calCoil,
            head.rwParms.channel[i].range,
            head.rwParms.channel[i].sensorgain,
            head.rwParms.channel[i].networkcode,
            head.rwParms.channel[i].locationcode
        );
    	*/
    
	    if(strcmp(ohead.rwParms.channel[i].id, head.rwParms.channel[i].id) != 0) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: id %s has changed to: %s  \n", 
		            i, ohead.rwParms.channel[i].id, head.rwParms.channel[i].id );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
  
	    if(strcmp(ohead.rwParms.channel[i].networkcode, head.rwParms.channel[i].networkcode) != 0) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: networkcode %s has changed to: %s  \n", 
		            i, ohead.rwParms.channel[i].networkcode, head.rwParms.channel[i].networkcode );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(strcmp(ohead.rwParms.channel[i].locationcode, head.rwParms.channel[i].locationcode) != 0) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: locationcode %s has changed to: %s  \n", 
		            i, ohead.rwParms.channel[i].locationcode, head.rwParms.channel[i].locationcode );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].sensorSerialNumberExt != head.rwParms.channel[i].sensorSerialNumberExt) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: sensorSerialNumberExt %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].sensorSerialNumberExt, head.rwParms.channel[i].sensorSerialNumberExt );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].north != head.rwParms.channel[i].north) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: north %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].north, head.rwParms.channel[i].north );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].east != head.rwParms.channel[i].east) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: east %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].east, head.rwParms.channel[i].east );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].up != head.rwParms.channel[i].up) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: up %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].up, head.rwParms.channel[i].up );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].altitude != head.rwParms.channel[i].altitude) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: altitude %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].altitude, head.rwParms.channel[i].altitude );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].azimuth != head.rwParms.channel[i].azimuth) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: azimuth %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].azimuth, head.rwParms.channel[i].azimuth );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].sensorType != head.rwParms.channel[i].sensorType) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: sensorType %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].sensorType, head.rwParms.channel[i].sensorType );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].sensorSerialNumber != head.rwParms.channel[i].sensorSerialNumber) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: sensorSerialNumber %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].sensorSerialNumber, head.rwParms.channel[i].sensorSerialNumber );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].gain != head.rwParms.channel[i].gain) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: gain %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].gain, head.rwParms.channel[i].gain );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].triggerType != head.rwParms.channel[i].triggerType) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: triggerType %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].triggerType, head.rwParms.channel[i].triggerType );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].iirTriggerFilter != head.rwParms.channel[i].iirTriggerFilter) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: iirTriggerFilter %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].iirTriggerFilter, head.rwParms.channel[i].iirTriggerFilter );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].StaSeconds != head.rwParms.channel[i].StaSeconds) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: StaSeconds %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].StaSeconds, head.rwParms.channel[i].StaSeconds );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].LtaSeconds != head.rwParms.channel[i].LtaSeconds) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: LtaSeconds %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].LtaSeconds, head.rwParms.channel[i].LtaSeconds );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].StaLtaRatio != head.rwParms.channel[i].StaLtaRatio) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: StaLtaRatio %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].StaLtaRatio, head.rwParms.channel[i].StaLtaRatio );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].StaLtaPercent != head.rwParms.channel[i].StaLtaPercent) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: StaLtaPercent %d has changed to: %d  \n", 
		            i, ohead.rwParms.channel[i].StaLtaPercent, head.rwParms.channel[i].StaLtaPercent );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].fullscale != head.rwParms.channel[i].fullscale) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: fullscale %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].fullscale, head.rwParms.channel[i].fullscale );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].sensitivity != head.rwParms.channel[i].sensitivity) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: sensitivity %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].sensitivity, head.rwParms.channel[i].sensitivity );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].damping != head.rwParms.channel[i].damping) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: damping %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].damping, head.rwParms.channel[i].damping );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
	    
	    if(ohead.rwParms.channel[i].naturalFrequency != head.rwParms.channel[i].naturalFrequency) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: naturalFrequency %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].naturalFrequency, head.rwParms.channel[i].naturalFrequency );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].triggerThreshold != head.rwParms.channel[i].triggerThreshold) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: triggerThreshold %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].triggerThreshold, head.rwParms.channel[i].triggerThreshold );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].detriggerThreshold != head.rwParms.channel[i].detriggerThreshold) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: detriggerThreshold %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].detriggerThreshold, head.rwParms.channel[i].detriggerThreshold );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].alarmTriggerThreshold != head.rwParms.channel[i].alarmTriggerThreshold) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: alarmTriggerThreshold %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].alarmTriggerThreshold, head.rwParms.channel[i].alarmTriggerThreshold );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
    
	    if(ohead.rwParms.channel[i].calCoil != head.rwParms.channel[i].calCoil) {
	    stat = 1;
		    sprintf(string, "CHANNEL_RW_PARMS: channel %d: calCoil %f has changed to: %f  \n", 
		            i, ohead.rwParms.channel[i].calCoil, head.rwParms.channel[i].calCoil );
		    logit("e", "%s", string);
		    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		}
        
    }
    
    
    /*
    logit("e", "STREAM_K2_RW_PARMS: |%d| |%d| |%d| %d %d %d %d %d %d %d %d %d %d %d %d %d %d %lu\n", 
        (int)head.rwParms.stream.filterFlag,  
        (int)head.rwParms.stream.primaryStorage,  
        (int)head.rwParms.stream.secondaryStorage,  
            head.rwParms.stream.eventNumber,  
            head.rwParms.stream.sps,  
            head.rwParms.stream.apw,  
            head.rwParms.stream.preEvent,  
            head.rwParms.stream.postEvent,  
            head.rwParms.stream.minRunTime,  
            head.rwParms.stream.VotesToTrigger,
            head.rwParms.stream.VotesToDetrigger,
        (int)head.rwParms.stream.FilterType,  
        (int)head.rwParms.stream.DataFmt,  
            head.rwParms.stream.Timeout,
            head.rwParms.stream.TxBlkSize,
            head.rwParms.stream.BufferSize,
            head.rwParms.stream.SampleRate,
            head.rwParms.stream.TxChanMap );
		*/
    
    if(ohead.rwParms.stream.filterFlag != head.rwParms.stream.filterFlag) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: filterFlag %d has changed to: %d  \n", 
	            ohead.rwParms.stream.filterFlag, head.rwParms.stream.filterFlag );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.primaryStorage != head.rwParms.stream.primaryStorage) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: primaryStorage %d has changed to: %d  \n", 
	            ohead.rwParms.stream.primaryStorage, head.rwParms.stream.primaryStorage );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.secondaryStorage != head.rwParms.stream.secondaryStorage) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: secondaryStorage %d has changed to: %d  \n", 
	            ohead.rwParms.stream.secondaryStorage, head.rwParms.stream.secondaryStorage );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.eventNumber != head.rwParms.stream.eventNumber) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: eventNumber %d has changed to: %d  \n", 
	            ohead.rwParms.stream.eventNumber, head.rwParms.stream.eventNumber );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.sps != head.rwParms.stream.sps) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: sps %d has changed to: %d  \n", 
	            ohead.rwParms.stream.sps, head.rwParms.stream.sps );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.apw != head.rwParms.stream.apw) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: apw %d has changed to: %d  \n", 
	            ohead.rwParms.stream.apw, head.rwParms.stream.apw );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.preEvent != head.rwParms.stream.preEvent) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: preEvent %d has changed to: %d  \n", 
	            ohead.rwParms.stream.preEvent, head.rwParms.stream.preEvent );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.postEvent != head.rwParms.stream.postEvent) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: postEvent %d has changed to: %d  \n", 
	            ohead.rwParms.stream.postEvent, head.rwParms.stream.postEvent );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.minRunTime != head.rwParms.stream.minRunTime) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: minRunTime %d has changed to: %d  \n", 
	            ohead.rwParms.stream.minRunTime, head.rwParms.stream.minRunTime );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.VotesToTrigger != head.rwParms.stream.VotesToTrigger) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: VotesToTrigger %d has changed to: %d  \n", 
	            ohead.rwParms.stream.VotesToTrigger, head.rwParms.stream.VotesToTrigger );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.VotesToDetrigger != head.rwParms.stream.VotesToDetrigger) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: VotesToDetrigger %d has changed to: %d  \n", 
	            ohead.rwParms.stream.VotesToDetrigger, head.rwParms.stream.VotesToDetrigger );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.FilterType != head.rwParms.stream.FilterType) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: FilterType %d has changed to: %d  \n", 
	            ohead.rwParms.stream.FilterType, head.rwParms.stream.FilterType );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.DataFmt != head.rwParms.stream.DataFmt) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: DataFmt %d has changed to: %d  \n", 
	            ohead.rwParms.stream.DataFmt, head.rwParms.stream.DataFmt );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.Timeout != head.rwParms.stream.Timeout) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: Timeout %d has changed to: %d  \n", 
	            ohead.rwParms.stream.Timeout, head.rwParms.stream.Timeout );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.TxBlkSize != head.rwParms.stream.TxBlkSize) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: TxBlkSize %d has changed to: %d  \n", 
	            ohead.rwParms.stream.TxBlkSize, head.rwParms.stream.TxBlkSize );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.BufferSize != head.rwParms.stream.BufferSize) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: BufferSize %d has changed to: %d  \n", 
	            ohead.rwParms.stream.BufferSize, head.rwParms.stream.BufferSize );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.SampleRate != head.rwParms.stream.SampleRate) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: SampleRate %d has changed to: %d  \n", 
	            ohead.rwParms.stream.SampleRate, head.rwParms.stream.SampleRate );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    if(ohead.rwParms.stream.TxChanMap != head.rwParms.stream.TxChanMap) {
    stat = 1;
	    sprintf(string, "STREAM_K2_RW_PARMS: TxChanMap %lu has changed to: %lu  \n", 
	            ohead.rwParms.stream.TxChanMap, head.rwParms.stream.TxChanMap );
	    logit("e", "%s", string);
	    if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    return stat;
}



/******************************************************************************
 * read_frame(fp)  Read the frame header, swap bytes if necessary.            *
 ******************************************************************************/
int read_frame( FILE *fp, unsigned long *channels )
{
    unsigned short   frameStatus, frameStatus2, samprate, streamnumber;
    unsigned char    BitMap[4];
    unsigned long    blockTime, bmap;
    int              stat;
    
    stat = fread(&frame, 32, 1, fp);
    frame.recorderID = BYTESWAP_UINT16(frame.recorderID);
    frame.frameSize  = BYTESWAP_UINT16(frame.frameSize);
    memcpy(&blockTime, &frame.blockTime, 4);
    blockTime  = BYTESWAP_UINT32(blockTime);

    frame.channelBitMap = BYTESWAP_UINT16(frame.channelBitMap);
    
    BitMap[0] = frame.channelBitMap & 255;
    BitMap[1] = frame.channelBitMap >> 8;
    BitMap[2] = frame.channelBitMap1;
    BitMap[3] = 0;
/*  memcpy( &bmap, BitMap, 4 ); */
    bmap = *((long *)(BitMap));
    frame.streamPar  = BYTESWAP_UINT16(frame.streamPar);
    frame.msec       = BYTESWAP_UINT16(frame.msec);
    frameStatus      = frame.frameStatus;
    frameStatus2     = frame.frameStatus2;
    samprate         = frame.streamPar & 4095;
    streamnumber     = frame.streamPar >> 12;
    /**/
    if(Debug && print_frame)
    logit("e", "nsmp2ew: FRAME: %d %d %d %d   %lu X%ho   %hu X%ho %hu %hu     X%ho X%ho %hu X%ho \n", 
            (int)frame.frameType, 
            (int)frame.instrumentCode, 
            frame.recorderID, 
            frame.frameSize, 
             
            blockTime, 
            frame.channelBitMap, 
            
            frame.streamPar, streamnumber, samprate, samprate>>8,  
            frameStatus, 
            frameStatus2, 
            frame.msec, 
            (int)frame.channelBitMap1);
    
    *channels = bmap;
    return stat;
}




