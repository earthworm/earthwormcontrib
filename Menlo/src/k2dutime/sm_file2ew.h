
#include <rw_strongmotion.h>

/* Define Strong Motion file types
 *********************************/
#define SM_TERRA   1   /* Terra data from PG&E system         */
#define SM_REDI    2   /* REDI data from UCB                  */
#define SM_TREMOR  3   /* John Evans' Tremor hardware         */
#define SM_CSMIP   4   /* California Strong Motion Program    */
#define SM_JFLB    5   /* Joe Fletcher/Larry Baker K2 system  */
#define SM_NSMP    6   /* Nat'l Strong Motion Program K2 data */

#define MAXCHAN     4000      /*largest number of channels we can handle */
#define MAX_PAGERS    30      /* Maximum number of Pager addresses        */
#define SM_STA_LEN     6
#define SM_COMP_LEN    8
#define SM_NET_LEN     8
#define SM_LOC_LEN     5
#define SM_MAX_CHAN   18      /* max # channels in a TYPE_SM message       */
#define GRP_LEN       25      /* length of page group name              */
#define NAM_LEN 	  100     /* length of full directory name          */
#define BUFLEN      65000     /* define maximum size for an event msg   */

#define GRAVITY_CGS 981.0  /* Gravity in cm/sec/sec */

/* Structure Definitions
 ***********************/
typedef struct _CHANNELNAME_ {
   char	box[SM_VENDOR_LEN+1]; /* Installation-assigned box name              */
   int	chan;		      /* Channel number on this box                  */	
   char sta[SM_STA_LEN+1];    /* NTS: Site code as per IRIS SEED             */ 
   char comp[SM_COMP_LEN+1];  /* NTS: Channel/Component code as per IRIS SEED*/ 
   char net[SM_NET_LEN+1];    /* NTS: Network code as per IRIS SEED          */
   char loc[SM_LOC_LEN+1];    /* NTS: Location code as per IRIS SEED         */
} CHANNELNAME;

/* Function prototypes
 *********************/
int sm_file2ew_ship( SM_DATA *psm );
int terra2ew ( FILE *fp, char *fname );
int terra_init( void );
int redi2ew  ( FILE *fp, char *fname );
int redi_init( void );
int tremor2ew( FILE *fp, char *fname );
int tremor_init( void );
int csmip2ew ( FILE *fp, char *fname );
int csmip_init( void );
int jflb2ew  ( FILE *fp, char *fname );
int jflb_init( void );
int nsmp2ew  ( FILE *fp, char *fname );
int nsmp_init( void );

