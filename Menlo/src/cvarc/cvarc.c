
        /******************************************************************
         *                            cvarc.c                             *
         *                                                                *
         *   This program converts Y2K format archive messages to         *
         *   pre-Y2K format.                                              *
         *                                                                *
         *   This is a filter program, ie the input arc message is read   *
         *   from stdin, and the output arc message is written to stdout. *
         *                                                                *
         *   If the length of an input line exceeds 255 characters, the   *
         *   program will print a message to stderr and then exit.        *
         *   If either a summary line or an archive line, is too short,   *
         *   the program will print a message to stderr and then exit.    *
         *                                                                *
         *   Otherwise, the program rearranges the fields of each line    *
         *   without checking for errors. (garbage in/garbage out)        *
         *                                                                *
         ******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAXCHR 256                  /* Max number of characters per line */


void cvarc( char *line_in, char *line_out )
{
   int  len       = strlen( line_in );
   char firstChar = line_in[0];

/* If the line begins with $, it is a shadow line.
   Copy it untouched.
   **********************************************/
   if ( firstChar == '$' )
      strcpy( line_out, line_in );

/* Summary lines always begin with a digit
   ***************************************/
   else if ( firstChar>47 && firstChar<58 )
   {
      char ts[10];
      double f;

      if ( len < 164 )                /* Summary line is too short */
      {
         fprintf( stderr, "Summary line is too short.\n" );
         exit( -1 );
      }

      memset( line_out, ' ', 154 );
      memcpy( line_out, line_in+2, 34 );

      memcpy( ts, line_in+36, 3 );       /* Mag from max S amplitude */
      ts[3] = '\0';
      f = atof( ts ) / 10.;
      sprintf( ts, "%2.lf", f );
      memcpy( line_out+34, ts, 2 );

      memcpy( line_out+36, line_in+39, 31 );

      memcpy( ts, line_in+70, 3 );       /* Coda duration magnitude */
      ts[3] = '\0';
      f = atof( ts ) / 10.;
      sprintf( ts, "%2.lf", f );
      memcpy( line_out+67, ts, 2 );

      memcpy( line_out+69,  line_in+73,   9 );
      memcpy( line_out+78,  line_in+83,  10 );
      memcpy( line_out+88,  line_in+94,   2 );   /* Number of P first-motions */
      memcpy( line_out+90,  line_in+97,   3 );   /* Number of S-amp mag weights */
      memcpy( line_out+93,  line_in+101,  3 );   /* Number of duration mag weights */
      memcpy( line_out+96,  line_in+104, 46 );
      memcpy( line_out+142, line_in+151,  3 );   /* Total of preferred mag weights */
      memcpy( line_out+145, line_in+154,  4 );
      memcpy( line_out+149, line_in+159,  3 );   /* Total of alt coda dur mag weights */
      memcpy( line_out+152, line_in+162,  2 );
      line_out[152] = ' ';                       /* WARNING! Kludge for earlybird to work properly; */
                                                 /*          overrides the previous line!           */
      line_out[154] = '\0';
   }

/* Terminator lines begin with four blanks and don't change
   ********************************************************/
   else if ( strncmp( line_in, "    ", 4 ) == 0 )
      strcpy( line_out, line_in );

/* Otherwise, the line is a regular archive line
   *********************************************/
   else
   {
      char ts[10];
      double f;

      if ( len < 111 )
      {
         fprintf( stderr, "Archive line is too short. Exiting.\n" );
         exit( -1 );
      }

      memset( line_out, ' ', 101 );
      memcpy( line_out,   line_in,    4 ); /* first 4 letters of station code */
      memcpy( line_out+4, line_in+13, 4 );
      memcpy( line_out+8, line_in+8,  1 );
      memcpy( line_out+9, line_in+19, 35 );

      memcpy( ts, line_in+54, 7 );       /* Peak-to-peak amplitude */
      ts[7] = '\0';
      f = atof( ts ) / 100.;
      sprintf( ts, "%3.lf", f );
      memcpy( line_out+44, ts, 3 );

      memcpy( line_out+47, line_in+63, 31 );

      memcpy( ts, line_in+94, 3 );       /* Duration magnitude */
      ts[3] = '\0';
      f = atof( ts ) / 10.;
      sprintf( ts, "%2.lf", f );
      memcpy( line_out+78, ts, 2 );

      memcpy( ts, line_in+97, 3 );       /* Amplitude magnitude */
      ts[3] = '\0';
      f = atof( ts ) / 10.;
      sprintf( ts, "%2.lf", f );
      memcpy( line_out+80, ts, 2 );

      memcpy( line_out+82, line_in+100, 8 );
      memcpy( line_out+91, line_in+108, 3 );

      memcpy( line_out+94, line_in+4, 1 );   /* 5th letter of station code */
      memcpy( line_out+95, line_in+9, 3 );   /* 3-letter component code */
      memcpy( line_out+98, line_in+5, 2 );   /* 2-letter network code */
      line_out[100] = '\0';
   }
   return;
}


    /**********************************************************************
     *  The following is a filter program that calls the cvarc function.  *
     **********************************************************************/

int main( int argc, char *argv[] )
{
   char line_in[MAXCHR];      /* Unconverted line */
   char line_out[MAXCHR];     /* Converted line */

/* Read one line from the arc message into line_in
   ***********************************************/
   while ( fgets( line_in, MAXCHR, stdin ) != NULL )
   {
      int len = strlen( line_in );

/* Make sure we didn't overflow the input line
   *******************************************/
      if ( (len == MAXCHR-1) && (line_in[len-1] != '\n') )
      {
         fprintf( stderr, "Input line length exceeds max value of %d. Exiting.\n",
                 MAXCHR-1 );
         return -1;
      }

/* Remove the trailing newline character
   *************************************/
      line_in[len-1] = '\0';

/* Convert the line to pre-Y2K format
   **********************************/
      cvarc( line_in, line_out );

/* Write the convert line to standard output
   *****************************************/
      puts( line_out );
   }
   return 0;
}
