#
#                TempAlarm's Configuration File
#
MyModuleId  MOD_TEMPALARM   # Module id for this program,

RingName        HYPO_RING   # Transport ring to write to,

HeartBeatInterval      30   # Interval between heartbeats sent to statmgr,
                            #   in seconds.

LogFile                 1   # If 0, don't write logfile at all, if 1, do
                            # if 2, write module log but not to stderr/stdout

ComPort                 1   # Com port used by temperature sensor
                            # 1 = com1, 2 = com2...

MaxTemperature      85.00   # Send first alarm when temperature exceeds this value
MaxTemperature2     95.00   # Send second alarm when temperature exceeds this value (optional)

CheckInterval         600   # Check temperature this often, in seconds

StaName             B11TR   # Station name for Seisnet Watch; max 5 chars (optional)
NetCode                NC   # Network code for Seisnet Watch; max 2 chars (optional)
UsageLevel              3   # For Seisnet Watch

