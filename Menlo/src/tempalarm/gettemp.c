
           /*******************************************************
            *                  GetTemperature()                   *
            *  Get the current temperature in degrees.            *
            *                                                     *
            *  Returns  0 if all ok                               *
            *          -1 if error                                *
            *******************************************************/

#include <stdio.h>
#include <math.h>
#include <earthworm.h>

#define BUFSIZE 40    /* Max response string is 20 chars */

int WritePort( char * );
int ReadPort( char * );


int GetTemperature( double *temperature )
{
   static char buf[BUFSIZE];
   int rc;

/* Request temperature from sensor.
   \x0D is a carriage return character.
   ***********************************/
   rc = WritePort( "$1\x00D" );
   if ( rc == -1 )
   {
      logit( "et", "Error. WritePort() returns: %d\n", rc );
      return -1;
   }

/* Read temperature from sensor as a string
   ****************************************/
   rc = ReadPort( buf );
   if ( rc < 0 )
   {
      logit( "et", "Error. ReadPort() returns: %d\n", rc );
      return -1;
   }

   if ( buf[0] == '?' )           /* Error return */
   {
      logit( "et", "Sensor error: %s", &buf[1] );
      return -1;
   }
   else if ( buf[0] == '*' )      /* Normal return */
   {
      if ( sscanf( &buf[1], "%lf", temperature ) < 1 )
      {
         logit( "et", "Error. Can't decode temperature: %s", &buf[1] );
         return -1;
      }

   }
   else                           /* Unknown sensor response code */
   {
      logit( "et", "Error. Sensor response code: %c\n", buf[0] );
      return -1;
   }
   return 0;
}
