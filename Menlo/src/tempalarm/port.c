
       /*******************************************************
        *                        port.c                       *
        *                                                     *
        *  Initialize a serial port.                          *
        *                                                     *
        *  This is Windows-specific code!!!                   *
        *******************************************************/


#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <earthworm.h>

static HANDLE handle;


int InitPort( int ComPort )
{
   COMMTIMEOUTS timeouts;
   DCB          dcb;                /* Data control block */
   BOOL         success;
   char         portName[20];

/* Open the com port
   *****************/
   if ( ComPort < 1 || ComPort > 9 )
   {
      logit( "et", "Invalid value of ComPort (%d). Exiting.\n", ComPort );
      exit( -1 );
   }
   sprintf( portName, "COM%d", ComPort );
   handle = CreateFile( portName,
                        GENERIC_READ|GENERIC_WRITE,
                        0, 0, OPEN_EXISTING,
                        FILE_ATTRIBUTE_NORMAL, 0 );

   if ( handle == INVALID_HANDLE_VALUE )
   {
      int rc = GetLastError();

      if ( rc == ERROR_ACCESS_DENIED )
         logit( "et", "CreateFile() error. Access denied to port: %s\n", portName );
      else
         logit( "et", "Error %d in CreateFile.\n", rc );
      return -1;
   }
   logit( "t", "CreateFile succeeded for comm port.\n" );

/* Get the current settings of the comm port
   *****************************************/
   success = GetCommState( handle, &dcb );
   if ( !success )
   {
      logit( "et", "Error %d in GetCommState.\n", GetLastError() );
      return -1;
   }

/* Modify the baud rate, flow control, etc
   ***************************************/
   dcb.BaudRate        = 9600;         // Hardwired
   dcb.ByteSize        = 8;            // Hardwired
   dcb.Parity          = NOPARITY;     // Hardwired
   dcb.StopBits        = ONESTOPBIT;   // Hardwired

   dcb.fOutxCtsFlow    = FALSE;        // Disable RTS/CTS flow control
   dcb.fOutxDsrFlow    = FALSE;        // Disable DTR/DSR flow control
   dcb.fDsrSensitivity = FALSE;        // Driver ignores DSR
   dcb.fOutX           = FALSE;        // Disable XON/XOFF out flow control
   dcb.fInX            = FALSE;        // Disable XON/XOFF in flow control
   dcb.fNull           = FALSE;        // Don't discard null bytes
   dcb.fAbortOnError   = TRUE;         // Abort rd/wr on error

/* Apply the new comm port settings
   ********************************/
   success = SetCommState( handle, &dcb );
   if ( !success )
   {
      logit( "et", "Error %d in SetCommState.\n", GetLastError() );
      return -1;
   }
   logit( "t", "Comm parameters set.\n" );

/* Change the ReadIntervalTimeout so that ReadFile
   will return immediately.
   ***********************************************/
   timeouts.ReadIntervalTimeout         = 0;
// timeouts.ReadIntervalTimeout         = MAXDWORD;
   timeouts.ReadTotalTimeoutMultiplier  = 0;
   timeouts.ReadTotalTimeoutConstant    = 0;
   timeouts.WriteTotalTimeoutMultiplier = 0;
   timeouts.WriteTotalTimeoutConstant   = 0;
   SetCommTimeouts( handle, &timeouts );
   logit( "t", "Timeout parameters set.\n" );

/* Purge the input comm port buffer
   ********************************/
   success = PurgeComm( handle, PURGE_RXCLEAR );
   if ( !success )
   {
      logit( "et", "Error %d in PurgeComm.\n", GetLastError() );
      return -1;
   }
   logit( "t", "Receive buffer purged.\n" );

   return 0;
}


        /*******************************************************
         *                    WritePort()                      *
         *                                                     *
         *  Write message to temperature sensor.               *
         *                                                     *
         *  Returns  0 if all ok,                              *
         *          -1 if a write error occurred               *
         *******************************************************/

int WritePort( char str[] )
{
   BOOL  success;
   DWORD numWritten;
   int   nchar = strlen( str );

/* Write the command to the port
   *****************************/
   success = WriteFile( handle, str, nchar, &numWritten, 0 );
   if ( !success )
   {
      logit( "et", "Error %d in WriteFile.\n", GetLastError() );
      return -1;
   }
   return 0;
}


        /*******************************************************
         *                    ReadPort()                       *
         *                                                     *
         *  Read message from temperature sensor.              *
         *                                                     *
         *  Returns  0 if all ok,                              *
         *          -1 if a read error occurred                *
         *          -2 if the buffer overflowed                *
         *******************************************************/

int ReadPort( char str[] )
{
   DWORD numRead;
   BOOL  success;
   char  chr;
   int   i = 0;

/* Read the command back from the port.
   Each command ends with line feed (0x0A).
   ***************************************/
   while ( 1 )
   {
      success = ReadFile( handle, &chr, 1, &numRead, 0 );
      if ( !success )
      {
         logit( "et", "Error %d in ReadFile.\n", GetLastError() );
         return -1;
      }

      if ( numRead == 0 )
      {
         sleep_ew( 100 );
         continue;
      }
      if ( chr == 0x0A ) break;
   }

/* Read the response from the sensor
   Each response ends with line feed (0x0A).
   ****************************************/
   while ( 1 )
   {
      success = ReadFile( handle, &chr, 1, &numRead, 0 );
      if ( !success )
      {
         logit( "et", "Error %d in ReadFile.\n", GetLastError() );
         return -1;
      }

      if ( numRead == 0 )
      {
         sleep_ew( 100 );
         continue;
      }
      str[i++] = chr;
      if ( chr == 0x0A ) break;
   }

   str[i] = 0;
   return 0;
}

