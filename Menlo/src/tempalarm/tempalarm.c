
   /****************************************************************
    *                       TempAlarm Program                      *
    *                                                              *
    *  This program communicates with an OmegaBus D1451            *
    *  temperature sensor, via a serial communication link.        *
    *  It reports to statmgr, if the room temperature is exceeds   *
    *  some threshold value.  Before running tempalarm, the        *
    *  sensor must be initialized using these commands:            *
    *  $1WE                                                        *
    *  $1SU31828D82                                                *
    *                                                              *
    *  This sets the baud rate to 9600 and the units to degrees    *
    *  fahrenheit.                                                 *
    *                                                              *
    *  If the serial link from the computer to the temperature     *
    *  sensor is broken, or the temperature sensor malfunctions,   *
    *  no heartbeats are sent to statmgr.  Statmgr may then        *
    *  report that tempalarm is dead.                              *
    *                                                              *
    *  TempAlarm now sends TYPE_SNW messages to the shared-memory  *
    *  ring.  These can be passed to Seisnet Watch.                *
    ****************************************************************/

// Added a second alarm level, so alarms can be sent at, say, 85 and 95 degrees
// WMK 1/19/2010

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>

#define ERR_TOOHOT   0
#define NOTESIZE   120

/* Global variables
   ****************/
static long          RingKey;               /* Transport ring key */
static unsigned char MyModId;               /* Tempalarm's module id */
static int           LogSwitch;             /* If 0, no disk logging */
static double        MaxTemperature;        /* Send first alarm when temperature */
                                            /*    exceeds this value. */
static double        MaxTemperature2=9999.; /* Send second alarm when temperature */
                                            /*    exceeds this value. */
static int           CheckInterval;         /* Check temperature this often */
static int           ComPort;               /* Com port of temperature sensor */
static int           HeartBeatInterval;
static char          StaName[6];            /* Station name */
static char          NetCode[3];            /* Network code */
static int           UsageLevel;            /* For SNW */
static pid_t         myPid;                 /* For restarts by startstop */
static unsigned char InstId;                /* Local installation id  */
static unsigned char TypeSNW;               /* Msg type for Seisnet Watch msgs */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static SHM_INFO      Region;

int InitPort( int );
int GetTemperature( double * );

static int  SendSNW( char note[] );
static int  ReportStatus( unsigned char type, short ierr, char *note );
static void GetConfig( char *configfile );
static void LogConfig( void );


           /****************************************
            *       Main program starts here       *
            ****************************************/

int main( int argc, char *argv[] )
{
   time_t tHeart = 0;       /* When the last heartbeat was sent */
   time_t tCheck = 0;       /* When the temperature was last checked */

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
        printf( "Usage: tempalarm <configfile>\n" );
        return 0;
   }

/* Read configuration file
   ***********************/
   GetConfig( argv[1] );

/* Look up local installation id
   *****************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "tempalarm: Error getting installation id. Exiting.\n" );
      return -1;
   }

/* Look up message types from earthworm.h tables
   *********************************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "tempalarm: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "tempalarm: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_SNW", &TypeSNW ) != 0 )
   {
      printf( "tempalarm: Invalid message type <TYPE_SNW>. Exiting.\n" );
      return -1;
   }


/* Initialize log file and log configuration parameters
   ****************************************************/
   logit_init( argv[1], (short)MyModId, 256, LogSwitch );
   logit( "" , "tempalarm: Read command file <%s>\n", argv[1] );
   LogConfig();

/* Get process ID for heartbeat messages
   *************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "tempalarm: Cannot get pid. Exiting.\n" );
      return -1;
   }

/* Attach to shared memory ring
   ****************************/
   tport_attach( &Region, RingKey );

/* Initialize the serial port.
   Exit on error.
   **************************/
   if ( InitPort( ComPort ) == -1 )
   {
      logit( "e", "InitPort() error. Exiting.\n" );
      return -1;
   }

/* Loop until kill flag is set
   ***************************/
   while ( 1 )
   {
      int rc;
      time_t now = time(0);           /* Current time */

/* Check kill flag
   ***************/
      if ( tport_getflag( &Region ) == TERMINATE )
      {
         tport_detach( &Region );
         logit( "t", "tempalarm: Termination requested. Exiting.\n" );
         return 0;
      }

/* Send heartbeat every HeartBeatInterval seconds
   **********************************************/
      if ( (now - tHeart) >= (time_t)HeartBeatInterval )
      {
         if ( ReportStatus( TypeHeartBeat, 0, "" ) != PUT_OK )
            logit( "t", "tempalarm: Error sending heartbeat to ring.\n");
         tHeart = now;
      }

/* Get the temperature every CheckInterval seconds
   ***********************************************/
      if ( (now - tCheck) < (time_t)CheckInterval )
      {
         sleep_ew( 1000 );
         continue;
      }

/* Keep trying to get the temperature until successful.
   No heartbeats are sent to statmgr, while trying to get the temperature.
   If the link to the temperature sensor is broken, or the temperature
   sensor breaks, statmgr will report that this program is dead.
   **********************************************************************/
      do
      {
         double tempF;                // Current temperature, in degrees F

         rc = GetTemperature( &tempF );
         if ( rc == 0 )
         {
            static int tempLevel = 0;  // Temperature acceptably low
            char   note[NOTESIZE];
            double tempC = 5./9. * (tempF - 32.0);
            logit( "et", "tempC= %.1lf  tempF= %.1lf\n", tempC, tempF );

/* Create a TYPE_SNW message and send it to the ring
   *************************************************/
            if ( strcmp(StaName,"--") != 0 && strcmp(NetCode,"--") != 0 )
            {
               int src;
               src = _snprintf( note, NOTESIZE, "%s-%s:3:Room Temperature (Deg F)=%.1lf;Room Temperature (Deg C)=%.1lf;UsageLevel=%d\n",
                        NetCode, StaName, tempF, tempC, UsageLevel );
               if ( src < 0 )
                  logit( "et", "tempalarm: _snprintf() buffer too small (%d characters).\n", NOTESIZE );
               if ( SendSNW( note ) != PUT_OK )
                  logit( "et", "tempalarm: Error sending SNW message to ring.\n");
            }

/* Report to statmgr if temperature is higher than alarm threshold
   ***************************************************************/
            if ( tempF < MaxTemperature )
            {
               if ( tempLevel > 0 )
               {
                  sprintf( note, "Alarm cancelled. Temperature no longer exceeds %.1lf deg F", MaxTemperature );
                  logit( "et", "%s\n", note );
                  if ( ReportStatus( TypeError, ERR_TOOHOT, note ) != PUT_OK )
                     logit( "et", "tempalarm: Error sending error message to ring.\n");
               }
               tempLevel = 0;
            }

            else    // Temperature exceeds one or both alarm levels
            {
               if ( tempF < MaxTemperature2 )
               {
                  if ( tempLevel == 0 )
                  {
                     sprintf( note, "Warning: Temperature exceeds %.1lf deg F", MaxTemperature );
                     logit( "et", "%s\n", note );
                    if ( ReportStatus( TypeError, ERR_TOOHOT, note ) != PUT_OK )
                        logit( "et", "tempalarm: Error sending error message to ring.\n");
                  }
                  if ( tempLevel == 2 )
                  {
                     sprintf( note, "Temperature no longer exceeds %.1lf deg F", MaxTemperature2 );
                     logit( "et", "%s\n", note );
                     if ( ReportStatus( TypeError, ERR_TOOHOT, note ) != PUT_OK )
                        logit( "et", "tempalarm: Error sending error message to ring.\n");
                  }
                  tempLevel = 1;
               }

               else   // Temperature exceeds the second (higher) alarm level
               {
                  if ( tempLevel < 2 )
                  {
                     sprintf( note, "Warning: Temperature exceeds %.1lf deg F", MaxTemperature2 );
                     logit( "et", "%s\n", note );
                     if ( ReportStatus( TypeError, ERR_TOOHOT, note ) != PUT_OK )
                        logit( "et", "tempalarm: Error sending error message to ring.\n");
                  }
                  tempLevel = 2;
               }
            }
            tCheck = now;    // Only if tempF successfully received
         }
         else      // GetTemperature() returned an error
         {
            sleep_ew( 10000 );
         }
      } while ( rc != 0 );
      sleep_ew( 1000 );
   }
}


         /*************************************************
          *                   SendSNW()                   *
          *  Send a SeisnetWatch message to the ring.     *
          *************************************************/

int SendSNW( char note[] )
{
   MSG_LOGO logo;
   int      res;
   long     size;

   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = TypeSNW;

   size = strlen( note );
   res  = tport_putmsg( &Region, &logo, size, note );
   return res;
}


         /*************************************************
          *                 ReportStatus()                *
          *  Builds a heartbeat or error message and      *
          *  puts it in shared memory.                    *
          *************************************************/

int ReportStatus( unsigned char type,
                  short         ierr,
                  char          *note )
{
   MSG_LOGO logo;
   char     msg[256];
   int      res;
   long     size;
   time_t   t;

   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
           sprintf ( msg, "%ld %d\n", (long)t, myPid);
   }
   else if( type == TypeError )
   {
           sprintf ( msg, "%ld %d %s\n", (long)t, ierr, note);
   }

   size = strlen( msg );
   res  = tport_putmsg( &Region, &logo, size, msg );
   return res;
}


       /***************************************************
        *                   GetConfig()                   *
        *  Reads the configuration file.                  *
        *  Exits if any errors are encountered.           *
        ***************************************************/

void GetConfig( char *configfile )
{
   const int ncommand = 7;  /* Number of commands to process */
   char      init[10];      /* Flags, one for each command */
   int       nmiss;         /* Number of commands that were missed */
   char      *com;
   char      *str;
   int       nfiles;
   int       success;
   int       i;

/* Set Default Values
   ******************/
   strcpy( StaName, "--" );
   strcpy( NetCode, "--" );
   UsageLevel = 3;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
        printf( "tempalarm: Error opening command file <%s>. Exiting.\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )      /* While there are command files open */
   {
        while ( k_rd() )     /* Read next line from active file  */
        {
           com = k_str();   /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
           if ( !com )          continue;
           if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
           if ( com[0] == '@' )
           {
              success = nfiles + 1;
              nfiles  = k_open( &com[1] );
              if ( nfiles != success )
              {
                 printf( "tempalarm: Error opening command file <%s>. Exiting.\n",
                     &com[1] );
                 exit( -1 );
              }
              continue;
           }

           if ( k_its( "MyModuleId" ) )
           {
              str = k_str();
              if ( str )
              {
                 if ( GetModId( str, &MyModId ) < 0 )
                 {
                    printf( "tempalarm: Invalid MyModuleId <%s> in <%s>",
                             str, configfile );
                    printf( ". Exiting.\n" );
                    exit( -1 );
                 }
              }
              init[0] = 1;
           }

           else if( k_its( "RingName" ) )
           {
              str = k_str();
              if (str)
              {
                 if ( ( RingKey = GetKey(str) ) == -1 )
                 {
                    printf( "tempalarm: Invalid RingName <%s> in <%s>",
                             str, configfile );
                    printf( ". Exiting.\n" );
                    exit( -1 );
                 }
              }
              init[1] = 1;
           }

           else if( k_its( "MaxTemperature" ) )
           {
              MaxTemperature = k_val();
              init[2] = 1;
           }

           else if( k_its( "LogFile" ) )
           {
              LogSwitch = k_int();
              init[3] = 1;
           }

           else if( k_its( "HeartBeatInterval" ) )
           {
              HeartBeatInterval = k_int();
              init[4] = 1;
           }

           else if( k_its( "CheckInterval" ) )
           {
              CheckInterval = k_int();
              init[5] = 1;
           }

           else if( k_its( "ComPort" ) )
           {
              ComPort = k_int();
              init[6] = 1;
           }

           else if( k_its( "StaName" ) )     /* Optional */
           {
              str = k_str();
              if (str)
              {
                 if ( strlen(str) > 5 )
                 {
                    printf( "tempalarm: Error. StaName is more than five characters long.\n" );
                    printf( "Length: %d   Exiting.\n", strlen(str) );
                    exit( -1 );
                 }
                 strcpy( StaName, str );
              }
           }

           else if( k_its( "NetCode" ) )     /* Optional */
           {
              str = k_str();
              if (str)
              {
                 if ( strlen(str) > 2 )
                 {
                    printf( "tempalarm: Error. NetCode is more than two characters long.\n" );
                    printf( "Length: %d   Exiting.\n", strlen(str) );
                    exit( -1 );
                 }
                 strcpy( NetCode, str );
              }
           }

           else if( k_its( "UsageLevel" ) )  /* Optional */
           {
              UsageLevel = k_int();
           }

           else if( k_its( "MaxTemperature2" ) )  /* Optional */
           {
              MaxTemperature2 = k_val();
           }

           else
           {
              printf( "tempalarm: <%s> unknown command in <%s>.\n",
                       com, configfile );
              continue;
           }

/* See if there were any errors processing the command
   ***************************************************/
           if ( k_err() )
           {
              printf( "tempalarm: Bad <%s> command in <%s>; \n",
                       com, configfile );
              exit( -1 );
           }
        }
        nfiles = k_close();
   }

/* Check flags for missed commands
   *******************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if( !init[i] ) nmiss++;

   if ( nmiss )
   {
      printf( "tempalarm: ERROR, no " );
      if ( !init[0] ) printf( "<MyModuleId> "        );
      if ( !init[1] ) printf( "<RingName> "          );
      if ( !init[2] ) printf( "<MaxTemperature> "    );
      if ( !init[3] ) printf( "<LogFile> "           );
      if ( !init[4] ) printf( "<HeartBeatInterval> " );
      if ( !init[5] ) printf( "<CheckInterval>     " );
      if ( !init[6] ) printf( "<ComPort> "           );
      printf( "command(s) in <%s>. Exiting.\n", configfile );
      exit( -1 );
   }
   return;
}


       /***************************************************
        *                   LogConfig()                   *
        *  Log the configuration file parameters.         *
        ***************************************************/

void LogConfig( void )
{
   logit( "", "\n" );
   logit( "", "MyModId:           %u\n",    MyModId );
   logit( "", "RingKey:           %ld\n",   RingKey );
   logit( "", "HeartBeatInterval: %d\n",    HeartBeatInterval );
   logit( "", "LogSwitch:         %d\n",    LogSwitch );
   logit( "", "MaxTemperature:    %.2lf\n", MaxTemperature );
   logit( "", "MaxTemperature2:   %.2lf\n", MaxTemperature2 );
   logit( "", "CheckInterval:     %d\n",    CheckInterval );
   logit( "", "ComPort:           %d\n",    ComPort );
   logit( "", "StaName:           %s\n",    StaName );
   logit( "", "NetCode:           %s\n",    NetCode );
   logit( "", "UsageLevel:        %d\n",    UsageLevel );
   logit( "", "\n" );
   return;
}

