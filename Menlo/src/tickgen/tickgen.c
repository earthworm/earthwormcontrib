/******************************************************
*
* Program:
*    tickgen.c
*
* Description:
*    Continuously outputs time ticks on one pin of a
*    National Instruments PCI 6703 board.
*
*******************************************************/

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <earthworm.h>
#include <transport.h>
#include "nidaq.h"

int      GetConfig( char * );
void     LogConfig( void );
void     SendHeartbeat( void );
void     NI_Error( char *, unsigned short );
int      IsTick( LARGE_INTEGER );

pid_t         myPid;                  // For restarts by startstop
SHM_INFO      region;                 // Structure for shared-memory region
unsigned char LocalInstId;
unsigned char TypeHeartBeat;
unsigned char TypeError;


// Main program starts here
// ************************
int main( int argc, char *argv[] )
{
   HANDLE        timerHandle;          // Handle of waitable timer
   short         niStatus;             // Return status of NI-DAQ functions
   long          timeNow;              // Current time
   long          timeLastBeat;         // Time last heartbeat was sent
   LARGE_INTEGER dueTime;              // When the waitable time expires
   const int     UpdateInterval = 100; // A/O update interval in milliseconds
   LONGLONG      llUpdateInterval;     // Update interval in 100 ns units

   extern long     RingKey;            // Key to transport ring
   extern unsigned char ModuleId;      // Module id of this process
   extern short    NI_Device;          // NI device number
   extern short    Chan;               // A/O channel number (0-15)
   extern int      HeartbeatInt;       // Heartbeat interval in seconds

// Read parameters from configuration file
// ***************************************
   if ( GetConfig( argv[1] ) == -1 ) return -1;

// Initialize log file and log configuration parameters
// ****************************************************
   {
      const LogSwitch = 1;       // If 1, log; if 0, don't log
      logit_init( argv[1], ModuleId, 256, LogSwitch );
   }
   LogConfig();

// Get process ID for heartbeat messages
// *************************************
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "Cannot get myPid. Exiting.\n" );
      return -1;
   }

// Attach to existing transport ring
// *********************************
   tport_attach( &region, RingKey );

// Create an unnamed waitable timer object.
// Timer will reset when a wait function succeeds.
// **********************************************
   {
      const BOOL manualReset = FALSE;    // Synchronization timer

      timerHandle = CreateWaitableTimer( NULL, manualReset, NULL );
      if ( timerHandle == NULL )
      {
         int lastError = GetLastError();
         logit( "e", "CreateWaitableTimer error: %d  Exiting.\n", lastError  );
         return -1;
      }
   }

// Get logos we will need later
// ****************************
   if ( GetLocalInst( &LocalInstId ) != 0 )
   {
      logit( "e", "Error getting local installation id. Exiting.\n" );
      return -1;
   }
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      logit( "e", "Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      return -1;
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      logit( "e", "Invalid message type <TYPE_ERROR>. Exiting.\n" );
      return -1;
   }

// Send heartbeat in first pass through main loop
// **********************************************/
   timeLastBeat = time( &timeNow ) - HeartbeatInt - 1;

// Get current time as a FILETIME structure.
// This will be the time that the waitable timer expires.
// *****************************************************
   {
      FILETIME ft;                        // 64-bit time
      GetSystemTimeAsFileTime( &ft );
      memcpy( &dueTime, &ft, sizeof(FILETIME) );
   }

// Convert update interval from milliseconds
// to 100 nanosecond units
// *****************************************
   llUpdateInterval = Int32x32To64( 10000, UpdateInterval );

// Set analog output to 0.0 volts.
// No tick mark at start up.
// ******************************
   niStatus = AO_VWrite( NI_Device, Chan, 0.0 );
   NI_Error( "AO_VWrite", niStatus );

// Main loop
// *********
   while ( 1 )
   {
      int           isTick;             // 0 or 1
      int           termFlag;
      DWORD         rc;                 // Windows return code
      extern double TimeTickSize;       // Size of time tick marks, in volts
      const DWORD   timeout = INFINITE; // Used by WaitForSingleObject
                                        //   Units are milliseconds or INFINITE
      static int    isTickPrev = 0;     // No tick at start up

// Has termination been requested?
// *******************************
      termFlag = tport_getflag( &region );
      if ( (termFlag == TERMINATE) || (termFlag == myPid) ) break;

// Send heartbeat message to statmgr
// *********************************
      if  ( time(&timeNow) - timeLastBeat >=  HeartbeatInt )
      {
         timeLastBeat = timeNow;
         SendHeartbeat();
      }

// Compute time of next update, in 100 ns units
// ********************************************
      dueTime.QuadPart += llUpdateInterval;

// Activate waitable timer and wait for timer event to occur
// *********************************************************
      if ( SetWaitableTimer( timerHandle, &dueTime, 0, NULL, NULL, FALSE  ) == 0 )
      {
         int errNum = GetLastError();
         logit( "et", "SetWaitableTimer error: %d.\n", errNum  );
         logit( "e", "Exiting program.\n" );
         return -1;
      }

      rc = WaitForSingleObject( timerHandle, timeout );
      if ( rc == WAIT_FAILED )
      {
         int errNum = GetLastError();
         logit( "et", "WaitForSingleObject error: %d\n", errNum );
         logit( "e", "Exiting program.\n" );
         return -1;
      }
      else if ( rc == WAIT_TIMEOUT )
      {
         logit( "et", "WaitForSingleObject timed out after %u msec\n", timeout );
         logit( "e", "Exiting program.\n" );
         return -1;
      }

// If we are in a tick mark, set the output to TimeTickSize
// ********************************************************
      isTick = IsTick( dueTime );
      if ( isTick == -1 )
      {
         logit( "et", "IsTick() error.\n" );
         logit( "e", "Exiting program.\n" );
         return -1;
      }
      if ( isTick && !isTickPrev )           // Turn on tick mark
      {
         niStatus = AO_VWrite( NI_Device, Chan, TimeTickSize );
         NI_Error( "AO_VWrite", niStatus );
      }
      else if ( !isTick && isTickPrev )      // Turn off tick mark
      {
         niStatus = AO_VWrite( NI_Device, Chan, 0.0 );
         NI_Error( "AO_VWrite", niStatus );
      }
      isTickPrev = isTick;
   }

// Cancel the waitable timer
// *************************
   if ( CancelWaitableTimer( timerHandle ) == 0 )
      logit( "et", "CancelWaitableTimer failed. Last error: %u\n", GetLastError() );

// Set analog output to 0.0 volts.
// No tick mark at shut down.
// ******************************
   niStatus = AO_VWrite( NI_Device, Chan, 0.0 );
   NI_Error( "AO_VWrite", niStatus );

// End program
// ***********
   logit( "t", "Terminate request received. Exiting program.\n");
   return 0;
}

