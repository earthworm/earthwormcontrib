#
#                    CONFIGURATION FILE FOR TICKGEN
#                    ------------------------------
#
ModuleId         MOD_TICKGEN      # Module id of this instance of tickgen
RingName         WAVE_RING        # Transport ring where waveforms are found
HeartbeatInt     15               # Heartbeat interval in seconds
NI_Device        1                # National Instruments device number
Chan             15               # A/O channel (0-15) for tick mark signal
UpdateDelay      60               # Delay A/O by this many seconds
TimeTickSize     1.0              # Size of time tick marks, in volts

