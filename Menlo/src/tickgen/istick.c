//
//  File istick.c
//

#include <stdio.h>
#include <earthworm.h>
#include <windows.h>

//******************************************************
//                       IsTick()
//
// Returns 1 if current time lands on a tick mark
//         0 if current time doesn't land on a tick mark
//        -1 if an error occured.
//******************************************************

int IsTick( LARGE_INTEGER liTime )
{
   extern long   UpdateDelay;         // Delay A/O this many seconds
   LARGE_INTEGER lTime = liTime;
   FILETIME      *fTime = (FILETIME *)&lTime;
   SYSTEMTIME    st;
   WORD          sec, min, hour;

// Apply the delay time
// ********************
   lTime.QuadPart -= Int32x32To64( 10000000, UpdateDelay );

// Convert time to structure
// *************************
   if ( FileTimeToSystemTime( fTime, &st ) == 0 )
   {
      logit( "et", "FileTimeToSystemTime() failed.\n" );
      return -1;
   }

// Decide if we're on a tick mark
// ******************************
   sec = st.wSecond;
   if ( (sec == 0) || (sec == 1) )
      return 1;                           // Minute mark

   min = st.wMinute;
   if ( min == 0 )
      if ( (sec == 2) || (sec == 3) )
         return 1;                        // Hour mark

   hour = st.wHour;
   if ( hour == 0 )
      if ( min == 0 )
         if ( (sec == 4) || (sec == 5) )
            return 1;                     // Day mark

   return 0;   // No tick mark
}
