#
#                     Make file for cta2sac
#                         Solaris Version
#

CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib


OBJ = cta2sac.o gettrig.o getscnlist.o makelocal.o \
      writefile.o parse_trig.o \
      $L/kom.o $L/chron3.o $L/time_ew.o $L/swap.o \
      $L/putaway.o $L/logit.o $L/sacputaway.o \
      $L/ahputaway.o $L/sudsputaway.o $L/tankputaway.o \
      $L/seiputaway.o $L/gseputaway.o $L/geo_to_km.o \
      $L/dirops_ew.o $L/seiutils.o

cta2sac: $(OBJ)
	cc -o $B/cta2sac $(OBJ) -mt -lm -lposix4 -lthread -lc -lnsl


# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/cta2sac*
