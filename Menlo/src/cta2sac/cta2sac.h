
/*   cta2sac.h    */

#ifndef CTA2SAC_H
#define CTA2SAC_H

#include <stdio.h>

#define MAXCHARFILENAME 80
#define MAXFORMATLEN    80

typedef struct
{
   char   sta[6];           /* Station */
   char   cmp[4];           /* Component */
   char   net[3];           /* Network */
   char   loc[3];           /* Location code */
   int    nbyte;            /* Number of bytes for this SCNL */
   double stime;            /* Trigger start time */
   double etime;            /* Trigger end time */
   int    fdTemp;           /* Descriptor of temporary file */
} SCNL;

typedef struct
{
   char   sta[6];           /* Station */
   char   cmp[4];           /* Component */
   char   net[3];           /* Network */
   char   loc[3];           /* Location code */
   int    ssYear;           /* Year of start save time (last two digits) */
   int    ssMonth;          /* Month of start save time (January = 1) */
   int    ssDay;            /* Day of start save time */
   int    ssHour;           /* Hour of start save time */
   int    ssMinute;         /* Minute of start save time */
   double ssSecond;         /* Second of start save time */
   double stime;            /* Start save time in sec since Jan 1, 1970 */
   int    duration;         /* Duration in seconds */
   char   eventId[EVENTID_SIZE];  /* from earthworm.h */
} TRIGGER;

#endif
