/*
 * makelocal.c
 *
 *  Byte swapping functions
 */

#include <string.h>
#include <swap.h>


   /***************************************************************
   *                      TraceHeadMakeLocal()                    *
   *          Byte-swap the header of a tracebuf message.         *
   *                                                              *
   *  If this is a TRACE_BUF message, convert it to TRACE2_BUF.   *
   *  Changes the 'datatype' field in the message header          *
   *  Returns -1 if unknown data type, 0 otherwise                *
   ****************************************************************/

int TraceHeadMakeLocal( TRACE2_HEADER* wvmsg )
{
   int dataSize;  /* flag telling us how many bytes in the data */
   char  byteOrder;
   long* longPtr;
   short* shortPtr;
   int i;

/* Convert TRACE_BUF to TRACE2_BUF
   *******************************/
   if ( (wvmsg->version[0] != TRACE2_VERSION0) ||
        (wvmsg->version[1] != TRACE2_VERSION1) )
   {
      wvmsg->version[0] = TRACE2_VERSION0;
      wvmsg->version[1] = TRACE2_VERSION1;
      strcpy( wvmsg->loc, "--" );
   }

/* See what sort of data it carries
   ********************************/
/* printf( "datatype: %s\n", wvmsg->datatype ); */
   dataSize=0;
   if ( strcmp(wvmsg->datatype, "s4")==0)
   {
      dataSize=4; byteOrder='s';
   }
   else if ( strcmp(wvmsg->datatype, "i4")==0)
   {
      dataSize=4; byteOrder='i';
   }
   else if ( strcmp(wvmsg->datatype, "s2")==0)
   {
      dataSize=2; byteOrder='s';
   }
   else if ( strcmp(wvmsg->datatype, "i2")==0)
   {
      dataSize=2; byteOrder='i';
   }
   else
      return(-1); /* We don't know this message type*/

#if defined( _SPARC )
   if (byteOrder =='i')
   {
        /* Swap the header
           ***************/
        SwapInt( &(wvmsg->pinno) );
        SwapInt( &(wvmsg->nsamp) );
        SwapDouble( &(wvmsg->starttime) );
        SwapDouble( &(wvmsg->endtime) );
        SwapDouble( &(wvmsg->samprate) );

        /* Re-write the data type field in the message
           *******************************************/
        if (dataSize==2) strcpy(wvmsg->datatype,"s2");
        if (dataSize==4) strcpy(wvmsg->datatype,"s4");
   }

#elif defined( _INTEL )
   if (byteOrder =='s')
   {
        /* Swap the header
           ***************/
        SwapInt( &(wvmsg->pinno) );
        SwapInt( &(wvmsg->nsamp) );
        SwapDouble( &(wvmsg->starttime) );
        SwapDouble( &(wvmsg->endtime) );
        SwapDouble( &(wvmsg->samprate) );

        /* Re-write the data type field in the message
           *******************************************/
        if(dataSize==2) strcpy(wvmsg->datatype,"i2");
        if(dataSize==4) strcpy(wvmsg->datatype,"i4");
   }
#else
        printf( "TraceHeadMakeLocal warning: _INTEL and _SPARC are both undefined." );
#endif
   return 0;
}

