
    /**************************************************************
     *                           cta2sac                          *
     *                                                            *
     *  Program to read continuous tape archive (cta) files and   *
     *  convert using the Earthworm putaway routines.             *
     **************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <kom.h>
#include <trace_buf.h>
#include <time_ew.h>
#include "cta2sac.h"

/* Function prototypes
   *******************/
void GetConfig( char * );
int  GetTrigList( char *, TRIGGER **, int * );
int  GetScnList( SCNL **, int *, char *, TRIGGER *, int );
void SortScnList( SCNL *, int );
int  OpenTempFiles( SCNL *, int );
int  WriteTempFiles( SCNL *, int, char * );
int  WritePutawayFiles( SCNL *, int, char *, char [], char [], char [] );
void RewindTempFiles( SCNL *, int );
void CloseTempFiles( SCNL *, int );

/* Read these from the config file
   *******************************/
int  nCtaFile  = 0;                /* Number of cta files */
char **CtaFile = NULL;             /* Array of names of the cta files */
char TriggerFile[MAXCHARFILENAME]; /* Name of trigger file */
char TempDir[MAXCHARFILENAME];     /* Name of temporary directory */
char OutDir[MAXCHARFILENAME];      /* Directory to contain putaway files */
char OutputFormat[40];             /* sparc or intel */
int  PA_debug;                     /* Putaway debug flag */
char DataFormat[MAXFORMATLEN];     /* sac, suds, ah, tank */
static int LogFile;                /* If 1, log to file */


int main( int argc, char **argv )
{
   static char defaultConfigFile[] = "cta2sac.d";

   int     i;
   long    recsize;              /* Size of retrieved message */
   char    *tracebuf;            /* Buffer to hold event message */
   char    *configFile;
   int     rc;
   int     numread;
   TRIGGER *trigs;               /* List of triggers */
   int     nTrigger;
   SCNL    *scnls;               /* List of SCNLs */
   int     nScnl;
   char    eventDate[40];
   char    eventTime[40];

/* Get the config file name from the command line.
   If not entered in command line, use default name.
   ************************************************/
   configFile = ( argc < 2 ) ? defaultConfigFile : argv[1];

/* Read the configuration file
   ***************************/
   GetConfig( configFile );

/* Set up earthworm logging
   ************************/
   {
      int bufSize = 256;       /* Logit buffer size, in bytes */
      int unused  = 0;
      logit_init( configFile, unused, bufSize, LogFile );
   }

/* Log configuration parameters
   ****************************/
   logit( "e", "Output format: %s\n", OutputFormat );
   logit( "e", "Data format:   %s\n", DataFormat );
   logit( "e", "Trigger file:  %s\n", TriggerFile );
   logit( "e", "TempDir:       %s\n", TempDir );
   logit( "e", "OutDir:        %s\n", OutDir );

/* Does the temporary directory exist?
   ***********************************/
   {
      DIR *dp = opendir( TempDir );
      if ( dp == NULL )
      {
         logit( "e", "Error. Temporary directory does not exist:\n%" );
         logit( "e", "%s\n", TempDir );
         logit( "e", "Exiting.\n" );
         return -1;
      }
      closedir( dp );
   }

/* Allocate buffer to hold one tracebuf message
   ********************************************/
   tracebuf = (char *) malloc( (size_t)(MAX_TRACEBUF_SIZ+1) );
   if ( tracebuf == NULL )
   {
      logit( "e", "Error allocating the trace buffer.\n" );
      logit( "e", "Exiting.\n" );
      return -1;
   }

/* Read the trigger file.
   GetTrigList() allocates the trigger list and a buffer to contain
   the entire triglist message.  Neither of these buffers is freed.
   ****************************************************************/
   logit( "e", "\nReading the trigger file...\n" );

   if ( GetTrigList( TriggerFile, &trigs, &nTrigger ) != EW_SUCCESS )
   {
      logit( "e", "Error getting the trigger file.\n" );
      logit( "e", "Exiting.\n" );
      return -1;
   }

   if ( nTrigger < 1 )
   {
      logit( "e", "There are no triggers in the trigger file.\n" );
      logit( "e", "Exiting.\n" );
      return -1;
   }

/* Log the triggers we found
   *************************/
   logit( "e", "Event id:   %s\n", trigs[0].eventId );
   sprintf( eventDate, "%04d%02d%02d", trigs[0].ssYear,
            trigs[0].ssMonth, trigs[0].ssDay );
   sprintf( eventTime, "%02d%02d%5.2lf", trigs[0].ssHour,
            trigs[0].ssMinute, trigs[0].ssSecond );
   if ( eventTime[4] == ' ' ) eventTime[4] = '0';
   logit( "e", "Event time: %s %s\n", eventDate, eventTime );

   for ( i = 0; i < nTrigger; i++ )
   {
      int  j;
      char scnl[40];

      sprintf( scnl, "%s.%s.%s.%s", trigs[i].sta, trigs[i].cmp,
               trigs[i].net, trigs[i].loc );
      logit( "e", "%s", scnl );
      for ( j = 0; j < (15-strlen(scnl)); j++ )
         logit( "e", " " );
      logit( "e", " %2d",    trigs[i].ssYear );
      logit( "e", "%02d",    trigs[i].ssMonth );
      logit( "e", "%02d",    trigs[i].ssDay );
      logit( "e", " %02d",   trigs[i].ssHour );
      logit( "e", ":%02d",   trigs[i].ssMinute );
      logit( "e", ":%.3lf",  trigs[i].ssSecond );
      logit( "e", "  %3d",   trigs[i].duration );
      logit( "e", "\n" );
   }

/* Search all cta files for SCNLs which appear in the triglist file.
   GetScnList() allocates the SCNL list.  This buffer is not freed.
   ****************************************************************/
   logit( "e", "\nSearching cta files to get list of SCNLs...\n" );
 
   if ( GetScnList( &scnls, &nScnl, tracebuf, trigs, nTrigger ) != EW_SUCCESS )
   {
      logit( "e", "Error searching cta files.\n" );
      logit( "e", "Exiting.\n" );
      return -1;
   }

   if ( nScnl < 1 )
   {
      logit( "e", "The cta files contain no data for this event.\n" );
      logit( "e", "Exiting.\n" );
      return -1;
   }

/* Sort the SCNL list alphabetically by SCNL
   *****************************************/
   SortScnList( scnls, nScnl );
   logit( "e", "\nHere are the SCNLs we will convert:\n" );
   for ( i = 0; i < nScnl; i++ )
   {
      logit( "e", "%s.%s.%s.%s\n", scnls[i].sta, scnls[i].cmp,
             scnls[i].net, scnls[i].loc );
   }

/* Open a set of temporary files, one per channel
   **********************************************/
   if ( OpenTempFiles( scnls, nScnl ) != EW_SUCCESS )
   {
      logit( "e", "OpenTempFiles() error.\n" );
      logit( "e", "Exiting.\n" );
      return -1;
   }

/* Write a set of temporary files, using data from cta files
   *********************************************************/
   logit( "e", "\nReading cta files and writing to temporary files...\n" );
   if ( WriteTempFiles( scnls, nScnl, tracebuf ) != EW_SUCCESS )
   {
      logit( "e", "WriteTempFiles() error.\n" );
      logit( "e", "Exiting.\n" );
      return -1;
   }

/* Set file pointers of temporary files to BOF
   *******************************************/
   RewindTempFiles( scnls, nScnl );

/* Write putaway (SAC) files, using data from temporary files
   **********************************************************/
   logit( "e", "\nReading temporary files and writing to putaway files...\n" );
   if ( WritePutawayFiles( scnls, nScnl, tracebuf, trigs[0].eventId,
                           eventDate, eventTime )
            != EW_SUCCESS )
   {
      logit( "e", "WritePutawayFiles() error.\n" );
      logit( "e", "Exiting.\n" );
      return -1;
   }

/* Close and erase temporary files
   *******************************/
   CloseTempFiles( scnls, nScnl );
   logit( "e", "Exiting.\n" );
   return 0;
}


  /************************************************************************
   *  GetConfig() processes command file(s) using kom.c functions;        *
   *                    exits if any errors are encountered.              *
   ************************************************************************/

#define NCOMMAND 8

void GetConfig( char *configfile )
{
   const int ncommand = NCOMMAND;    /* Process this many required commands */
   char      init[NCOMMAND];         /* Init flags, one for each command */
   int       nmiss;                  /* Number of missing commands */
   char      *com;
   char      *str;
   int       nfiles;
   int       success;
   int       i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      printf( "Error opening command file <%s>.\n", configfile );
      printf( "Exiting.\n" );
      exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )             /* Read next line from active file  */
      {
         com = k_str();            /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )          continue;
         if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "Can't open command file <%s>.\n", &com[1] );
               printf( "Exiting.\n" );
               exit( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         if ( k_its("CtaFile") )
         {
            char **ptemp;

            ptemp = (char **)realloc( CtaFile, (nCtaFile+1)*sizeof(char *) );
            if ( ptemp == NULL )
            {
               printf( "Can't realloc CtaFile.\n" );
               printf( "Exiting.\n" );
               exit( -1 );
            }
            CtaFile = ptemp;

            CtaFile[nCtaFile] = (char *)malloc( MAXCHARFILENAME );
            if ( CtaFile[nCtaFile] == NULL )
            {
               printf( "Can't allocate CtaFile[i]." );
               printf( "Exiting.\n" );
               exit( -1 );
            }

            if ( str=k_str() )
               strcpy( CtaFile[nCtaFile], str );
            nCtaFile ++;
            init[0] = 1;
         }

         else if ( k_its("TriggerFile") )
         {
            if ( str=k_str() )
               strcpy( TriggerFile, str );
            init[1] = 1;
         }
         else if ( k_its("TempDir") )
         {
            if ( str=k_str() )
               strcpy( TempDir, str );
            init[2] = 1;
         }
         else if ( k_its("OutputFormat") )
         {
            if ( str=k_str() )
               strcpy( OutputFormat, str );
            init[3] = 1;
         }
         else if ( k_its("PA_debug") )
         {
            PA_debug = k_int();
            init[4] = 1;
         }
         else if ( k_its("LogFile") )
         {
            LogFile = k_int();
            init[5] = 1;
         }
         else if ( k_its("OutDir") )
         {
            if ( str=k_str() )
               strcpy( OutDir, str );
            init[6] = 1;
         }
         else if ( k_its("DataFormat") )
         {
            if ( str=k_str() )
               strcpy( DataFormat, str );
            init[7] = 1;
         }

/* Unknown command
   ***************/
         else
         {
             printf( "<%s> Unknown command in <%s>.\n", com,
                     configfile );
             continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "Bad <%s> command in <%s>.\n", com, configfile );
            printf( "Exiting.\n" );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
      printf( "ERROR, no " );
      if ( !init[0] ) printf( "<CtaFile> " );
      if ( !init[1] ) printf( "<TriggerFile> " );
      if ( !init[2] ) printf( "<TempDir> " );
      if ( !init[3] ) printf( "<OutputFormat> " );
      if ( !init[4] ) printf( "<PA_debug> " );
      if ( !init[5] ) printf( "<LogFile> " );
      if ( !init[6] ) printf( "<OutDir> " );
      if ( !init[7] ) printf( "<DataFormat> " );
      printf( "command(s) in <%s>.\n", configfile );
      printf( "Exiting.\n" );
      exit( -1 );
   }
   return;
}
