
                         /**********************
                          *     gettrig.c      *
                          **********************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <chron3.h>
#include <earthworm.h>
#include <parse_trig.h>
#include "cta2sac.h"


     /***************************************************************
      *                       ParseTrigbuf()                        *
      *                                                             *
      *           Function to decode one trigger line.              *
      *                                                             *
      *  Returns EW_FAILURE or EW_SUCCESS                           *
      ***************************************************************/

int ParseTrigbuf( char *trigbuf, char **nxtLine, TRIGGER *trig )
{
   char    token[5];
   int     seconds;
   SNIPPET snip;

   if ( parseSnippet( trigbuf , &snip , nxtLine ) == EW_FAILURE )
      return EW_FAILURE;

   strcpy( trig->eventId, snip.eventId );

   strcpy( trig->sta, snip.sta );
   strcpy( trig->cmp, snip.chan );
   strcpy( trig->net, snip.net );

/* Get a valid location code
   *************************/
   {
      int validLoc;
      if ( strlen(snip.loc) == 0 )
      {
         validLoc = 0;
      }
      else if ( strcmp(snip.loc, "--") == 0 )
      {
         validLoc = 1;
      }
      else if ( strcmp(snip.loc, "*") == 0 )
      {
         validLoc = 1;
      }
      else
      {
         int i;
         validLoc = 1;
         for ( i = 0; i < strlen(snip.loc); i++ )
            if ( !isdigit(snip.loc[i]) ) validLoc = 0;
      }
      strcpy( trig->loc, validLoc ? snip.loc : "--" );
   }

   strncpy( token, snip.startYYYYMMDD, 4 );
   token[4] = '\0';
   trig->ssYear = atoi( token );

   strncpy( token, snip.startYYYYMMDD+4, 2 );
   token[2] = '\0';
   trig->ssMonth = atoi( token );

   strncpy( token, snip.startYYYYMMDD+6, 2 );
   token[2] = '\0';
   trig->ssDay = atoi( token );

/* WARNING: snip.startHHMMSS is apparently unterminated!
            Looks like a bug in parsetrig.
   ****************************************************/
/* printf( "snip.startHHMMSS: %s\n", snip.startHHMMSS ); */

   strncpy( token, snip.startHHMMSS, 2 );
   token[2] = '\0';
   trig->ssHour = atoi( token );

   strncpy( token, snip.startHHMMSS+3, 2 );
   token[2] = '\0';
   trig->ssMinute = atoi( token );

   strncpy( token, snip.startHHMMSS+6, 2 );
   token[2] = '\0';
   seconds = atoi( token );
   trig->ssSecond = seconds + snip.starttime - floor(snip.starttime);

   trig->stime    = snip.starttime;
   trig->duration = snip.duration;
   return EW_SUCCESS;
}


     /********************************************************************
      *                          GetTrigList()                           *
      *                                                                  *
      *  Read everything from the trigger file into the trigger list.    *
      *                                                                  *
      *  Do not call this function more than once. Possible memory       *
      *  leaks.                                                          *
      *                                                                  *
      *  Accepts:                                                        *
      *    trigFileName = Name of trigger file                           *
      *  Returns:                                                        *
      *    EW_SUCCESS if all went well                                   *
      *    EW_FAILURE if we ran into problems                            *
      *    ptrig    = Array of trigger structures, to be filled with     *
      *               info from the trigger file                         *
      *    nTrigger = Number of triggers read                            *
      ********************************************************************/

int GetTrigList( char *trigFileName, TRIGGER **pptrig, int *nTrigger )
{
   char    *trigbuf;
   FILE    *fp;
   int     i;
   int     msgsize;
   int     ntrig = 0;
   TRIGGER trig;
   TRIGGER *ptrig;
   char    *nxtLine;
   int     trigFileSize;      /* Size of trigger file, in bytes */

/* Open the trigger file
   *********************/
   fp = fopen( trigFileName, "r" );
   if ( fp == NULL )
   {
      logit( "e", "Can't open trigger file: %s\n", trigFileName );
      return EW_FAILURE;
   }

/* Get size of trigger file
   ************************/
   {
      struct stat statBuf;

      if ( stat( trigFileName, &statBuf ) == -1 )
      {
         logit( "e", "Error getting size of trigger file.\n" );
         return EW_FAILURE;
      }
      trigFileSize = statBuf.st_size;
   }

/* Allocate the triglist message buffer
   ************************************/
   trigbuf = (char *) malloc( trigFileSize + 1 );
   if ( trigbuf == NULL )
   {
      logit( "e", "Error. Can't allocate triglist message buffer.\n" );
      return EW_FAILURE;
   }

/* Read the entire trigger list into trigbuf
   *****************************************/
   msgsize = fread( trigbuf, sizeof(char), trigFileSize, fp );

   if ( ferror( fp ) )
   {
      logit( "e", "Error reading trigger file: %s\n", trigFileName );
      fclose( fp );
      return EW_FAILURE;
   }
   fclose( fp );

/* Null-terminate the triglist message
   ***********************************/
   trigbuf[msgsize] = '\0';

/* Get event id and first trigger from trigger message
   ***************************************************/
   nxtLine = trigbuf;
   if ( ParseTrigbuf( trigbuf , &nxtLine, &trig ) == EW_FAILURE )
   {
      logit( "e", "Can't get event id from trigger message.\n" );
      return EW_FAILURE;
   }

/* Copy the first trigger to the trigger list
   ******************************************/
   ptrig = (TRIGGER *) malloc( sizeof(TRIGGER) );
   if ( ptrig == NULL )
   {
      logit( "e", "Can't allocate the first trigger.\n" );
      return EW_FAILURE;
   }
   ptrig[ntrig++] = trig;

/* Get the next trigger from the trigger message.
   The event id returned here is bogus.
   *********************************************/
   while ( ParseTrigbuf( trigbuf , &nxtLine, &trig ) == EW_SUCCESS )
   {
      TRIGGER *ptemp;
      int duplicate = FALSE;

/* If the trigger is a duplicate, discard it
   *****************************************/
      for ( i = 0; i < ntrig; i++ )
         if ( strcmp(trig.sta, ptrig[i].sta) == 0 &&
              strcmp(trig.cmp, ptrig[i].cmp) == 0 &&
              strcmp(trig.net, ptrig[i].net) == 0 &&
              strcmp(trig.loc, ptrig[i].loc) == 0 )
            duplicate = TRUE;
      if ( duplicate )
         continue;

/* Otherwise, append the trigger to the trigger list
   *************************************************/
      ptemp = (TRIGGER *) realloc( ptrig, (ntrig+1)*sizeof(TRIGGER) );
      if ( ptemp == NULL )
      {
         logit( "e", "Can't realloc the trigger list.\n" );
         return EW_FAILURE;
      }
      ptrig = ptemp;
      ptrig[ntrig++] = trig;
   }

/* If any one of the SCNLs in the trigger list is a wildcard,
   ie * * *, discard all other SCNLs from the list.
   *********************************************************/
   for ( i = 0; i < ntrig; i++ )
   {
      if ( strcmp(ptrig[i].sta, "*") == 0 &&
           strcmp(ptrig[i].cmp, "*") == 0 &&
           strcmp(ptrig[i].net, "*") == 0 &&
           strcmp(ptrig[i].loc, "*") == 0 )
      {
         ptrig[0] = ptrig[i];
         ntrig = 1;
         break;
      }
   }

   *pptrig   = ptrig;  /* Return address of trigger structures */
   *nTrigger = ntrig;  /* Return number of triggers found */
   return EW_SUCCESS;
}
