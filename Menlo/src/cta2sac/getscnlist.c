
       /**********************************************************
        *                     GetScnList()                       *
        *  Returns EW_SUCCESS or EW_FAILURE.                     *
        **********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <trace_buf.h>
#include "cta2sac.h"

/* From the config file
   ********************/
extern int  nCtaFile;        /* Maximum number of cta files */
extern char **CtaFile;       /* Array of names of the cta files */

/* Function declaration
   ********************/
int TraceHeadMakeLocal( TRACE2_HEADER * );


int GetScnList( SCNL **ppscn, int *nScnl, char *tracebuf,
                TRIGGER *trigs, int nTrigger )
{
   int i;
   int nscn  = 0;            /* Number of desired SCNLs found in the cta */
   SCNL *pscn = NULL;
   TRACE2_HEADER *tracehead = (TRACE2_HEADER *)tracebuf;
   const int maxDataBytes = MAX_TRACEBUF_SIZ - sizeof(TRACE2_HEADER);

/* Loop through all cta disk files
   *******************************/
   for ( i = 0; i < nCtaFile; i++ )
   {
      FILE *fp;
      char *dataPtr = tracebuf + sizeof(TRACE2_HEADER);
      logit( "e", "%s\n", CtaFile[i] );

/* Open a cta file
   ***************/
      if ( (fp = fopen( CtaFile[i], "rb" )) == NULL )
      {  
         logit( "e", "Error opening cta disk file: %s\n", CtaFile[i] );
         logit( "e", "Exiting.\n" );
         return EW_FAILURE;
      }
       
/* Read a trace header from the cta file
   *************************************/
      while ( fread( tracehead, sizeof(TRACE2_HEADER), 1, fp ) == 1 )
      {
         int    j;
         int    wantIt  = 0;  /* Non-zero if we want this SCNL */
         int    inScnls = 0;  /* Non-zero if have already seen this SCNL */
         SCNL   *ptemp;
         int    nDataBytes;
         int    nMsgBytes;
         double trigStartTime;
         double trigEndTime;

/* Convert tracebuf header to local byte order
   *******************************************/
         if ( TraceHeadMakeLocal( tracehead ) == -1 )
         {
            logit( "e", "Unknown data type: %s\n", tracehead->datatype );
            logit( "e", "Exiting.\n" );
            exit( -1 );
         }

/* Compute nDataBytes = number of data bytes in tracebuf message
   *************************************************************/
         nDataBytes = tracehead->nsamp * (tracehead->datatype[1] - 48);
         if ( nDataBytes > (MAX_TRACEBUF_SIZ - sizeof(TRACE2_HEADER)) )
         {
            logit( "e", "\nERROR. Bad tracebuf message:\n" );
            logit( "e", "nsamp:      %d\n",    tracehead->nsamp );
            logit( "e", "Start time: %.3lf\n", tracehead->starttime );
            logit( "e", "End time:   %.3lf\n", tracehead->endtime );
            logit( "e", "SCNL:       %5s %3s %2s\n",
                    tracehead->sta, tracehead->chan,
                    tracehead->net, tracehead->loc );
            logit( "e", "Data type:  %2s\n",   tracehead->datatype );
            logit( "e", "nDataBytes(%d) exceeds the max(%d).\n",
                     nDataBytes, maxDataBytes );
            logit( "e", "Exiting.\n" );
            exit( -1 );
         }

/* Compute nMsgBytes = number of bytes in entire tracebuf
   message, padded to a multiple of four bytes.
   ******************************************************/
         nMsgBytes = nDataBytes + sizeof(TRACE2_HEADER);
         {
            int remainder = nMsgBytes % 4;
            if ( remainder > 0 )
               nMsgBytes += (4 - remainder);
         }
 
/* Read the data samples for this message from the cta file
   ********************************************************/
         if ( fread( dataPtr, sizeof(char), nDataBytes, fp ) < nDataBytes )
         {
            logit( "e", "Error reading cta file: %s\n", CtaFile[i] );
            logit( "e", "Exiting.\n" );
            return EW_FAILURE;
         }

/* If the SCNL strings are too long, ignore the message.
   Some Reftek test data was given bogus, long SCNL names.
   ******************************************************/
         if ( strlen( tracehead->sta  ) > 5 ) continue;
         if ( strlen( tracehead->chan ) > 3 ) continue;
         if ( strlen( tracehead->net  ) > 2 ) continue;
         if ( strlen( tracehead->loc  ) > 2 ) continue;

/* Do we want this SCNL?  We want it if the SCNL matches an SCNL
   in the trigger list.  Wildcards are allowed in trigger list
   SCNLs.  The index of the matching trigger (j) is used below.
   *************************************************************/
         for ( j = 0; j < nTrigger; j++ )
         { 
            int sta_ok = strcmp( trigs[j].sta, "*"            ) == 0 ||
                         strcmp( trigs[j].sta, tracehead->sta  ) == 0;
            int cmp_ok = strcmp( trigs[j].cmp, "*"            ) == 0 ||
                         strcmp( trigs[j].cmp, tracehead->chan ) == 0;
            int net_ok = strcmp( trigs[j].net, "*"            ) == 0 ||
                         strcmp( trigs[j].net, tracehead->net  ) == 0;
            int loc_ok = strcmp( trigs[j].loc, "*"            ) == 0 ||
                         strcmp( trigs[j].loc, tracehead->loc  ) == 0;

            if ( wantIt = sta_ok && cmp_ok && net_ok && loc_ok )
               break;
         }
         if ( !wantIt ) continue;      /* Get next tracebuf message */

/* Is the message in the desired time window?
   If not, get the next message.
   *****************************************/
         trigStartTime = trigs[j].stime;
         trigEndTime   = trigStartTime + (double)trigs[j].duration;

         {
            int dontWantIt = ( trigEndTime < tracehead->starttime ) ||
                             ( tracehead->endtime < trigStartTime );
            wantIt = !dontWantIt;
         }
         if ( !wantIt ) continue;      /* Get next tracebuf message */

/*       printf( "%d",      tracehead->nsamp );
         printf( "  %.3lf", tracehead->starttime );
         printf( "  %.3lf", tracehead->endtime );
         printf( "  %5s",   tracehead->sta );
         printf( " %3s",    tracehead->chan );
         printf( " %2s",    tracehead->net );
         printf( " %2s",    tracehead->loc );
         printf( "  %2s\n", tracehead->datatype ); */

/* We want this message.
   Is the SCNL already in the list of SCNLs obtained?
   *************************************************/
         for ( j = 0; j < nscn; j++ )
         {
            if ( strcmp( tracehead->sta,  pscn[j].sta ) == 0 &&
                 strcmp( tracehead->chan, pscn[j].cmp ) == 0 &&
                 strcmp( tracehead->net,  pscn[j].net ) == 0 &&
                 strcmp( tracehead->loc,  pscn[j].loc ) == 0 )
            {
               inScnls = 1;
               break;
            }
         }

/* We have already seen this SCNL.  Increment the byte count.
   *********************************************************/
         if ( inScnls )
         {
            pscn[j].nbyte += nMsgBytes;
            continue;
         }

/* This is a new SCNL.  Add it to the scnl list.
   ********************************************/
         ptemp = (SCNL *) realloc( pscn, (nscn+1)*sizeof(SCNL) );
         if ( ptemp == NULL )
         {
            logit( "e", "Can't realloc the SCNL list.\n" );
            logit( "e", "Exiting.\n" );
            return EW_FAILURE;
         }
         pscn = ptemp;
         strcpy( pscn[nscn].sta, tracehead->sta );
         strcpy( pscn[nscn].cmp, tracehead->chan );
         strcpy( pscn[nscn].net, tracehead->net );
         strcpy( pscn[nscn].loc, tracehead->loc );
         pscn[nscn].nbyte = nMsgBytes;
         pscn[nscn].stime = trigStartTime;
         pscn[nscn].etime = trigEndTime;
         nscn++;
      }                    /* End of loop that processes one tracebuf msg */
      fclose( fp );
   }
   *ppscn = pscn;          /* Return address of SCNL list */
   *nScnl = nscn;          /* Return the number of unique SCNLs found */
   return EW_SUCCESS;
}


void SortScnList( SCNL *scnls, int nScnl )
{
   int  i;
   int  j;
   SCNL temp;

   if ( nScnl < 2 ) return;       /* Nothing to sort */

   for ( i = 0; i < nScnl-1; i++ )
      for ( j = i+1; j < nScnl; j++ )
      {
         if ( strcmp( scnls[i].sta, scnls[j].sta ) < 0 ) continue;
         if ( strcmp( scnls[i].sta, scnls[j].sta ) == 0 )
         {
            if ( strcmp( scnls[i].cmp, scnls[j].cmp ) < 0 ) continue;
            if ( strcmp( scnls[i].cmp, scnls[j].cmp ) == 0 )
               if ( strcmp( scnls[i].net, scnls[j].net ) < 0 ) continue;
         }
         temp     = scnls[i];
         scnls[i] = scnls[j];
         scnls[j] = temp;
      }
}
