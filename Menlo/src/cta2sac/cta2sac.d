#
# This is cta2sac's parameter file
#
TriggerFile    trigtest.2
#
# Logging flag.  If 1, log to a file. If 0, don't.
LogFile        0
#
# Debug flag for the "putaway" routines.
# If 1, debug; if 0, don't debug.
PA_debug       0
#                          
# Name of directory to contain temporary files.
# This directory must exist prior to running this program.
TempDir /home/earthworm/run-will/cta2sac.tmp
#
# Name of directory to contain output (SAC) files.
# This directory must exist prior to running this program.
OutDir  /home/earthworm/run-will/cta2sac.out
#
# Data format must be one of the following:
# ah, sac, suds, gse_int, seisan, or tank.
DataFormat sac
#
# Output files may have "sparc" or "intel" byte order.
# Specify which output format you want.
OutputFormat     sparc
#
# Enter the names of the continuous tape archive files below.
# File names may begin with "wt" or "wu".
# The files must be listed in chronological order.
  CtaFile wu20040511221616
# CtaFile wu20040511221716
# CtaFile wu20040511221816
# CtaFile wu20040511221956
# CtaFile wu20040511222056
# CtaFile wu20040511222156
# CtaFile wu20040511222256
# CtaFile wu20040511222356
