#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <earthworm.h>
#include <swap.h>
#include <trace_buf.h>
#include <ws_clientII.h>
#include <putaway.h>
#include "cta2sac.h"

/* From the config file
   ********************/
extern int  nCtaFile;                  /* Maximum number of cta files */
extern char **CtaFile;                 /* Array of names of the cta files */
extern char TempDir[MAXCHARFILENAME];  /* Name of temporary directory */
extern char OutDir[MAXCHARFILENAME];   /* Directory to contain putaway files */
extern char OutputFormat[40];          /* sparc or intel */
extern int  PA_debug;                  /* for putaway routines */
extern char DataFormat[MAXFORMATLEN];  /* sac, suds, ah, tank */

/* Function declaration
   ********************/
int TraceHeadMakeLocal( TRACE2_HEADER* wvmsg );

/* Constant
   ********/
const size_t sizeHeader = sizeof(TRACE_HEADER);



       /**********************************************************
        *                     OpenTempFiles()                    *
        *  Open temporary files and save file descriptors in     *
        *  the scnls array.                                      *
        *  Returns EW_SUCCESS or EW_FAILURE.                     *
        **********************************************************/

int OpenTempFiles( SCNL *scnls, int nScnl )
{
   int  i;

   for ( i = 0; i < nScnl; i++ )
   {
      char TempFileName[MAXCHARFILENAME];

//    snprintf( TempFileName, MAXCHARFILENAME, "%s/%s.%s.%s.%s.tmp",
//             TempDir, scnls[i].sta, scnls[i].cmp,
//             scnls[i].net, scnls[i].loc );

      snprintf( TempFileName, MAXCHARFILENAME, "%s/tempFile", TempDir );

      scnls[i].fdTemp = open( TempFileName, O_RDWR | O_CREAT | O_TRUNC, 0666 );
      if ( scnls[i].fdTemp == -1 )
      {  
         logit( "e", "Error opening temporary file: %d\n", i );
         logit( "e", "Exiting.\n" );
         return EW_FAILURE;
      }

/* Unlink the file.  The file will be removed by the
   file system, when the file is closed.
   *************************************************/
      if ( unlink( TempFileName ) == -1 )
      {
         logit( "e", "Error unlinking temporary file %d\n", i );
         logit( "e", "Exiting.\n" );
         return EW_FAILURE;
      }
   }
   return EW_SUCCESS;
}


       /**********************************************************
        *                    WriteTempFiles()                    *
        *  Write the temporary files.                            *
        *  Returns EW_SUCCESS or EW_FAILURE.                     *
        **********************************************************/

int WriteTempFiles( SCNL *scnls, int nScnl, char *tracebuf )
{
   int  i;
   TRACE_HEADER *WaveHead = (TRACE_HEADER *) tracebuf;
   const int maxDataBytes = MAX_TRACEBUF_SIZ - sizeHeader;

/* Loop over all cta files
   ***********************/
   for ( i = 0; i < nCtaFile; i++ )
   {
      char  *dataPtr = tracebuf + sizeHeader;
      FILE  *fpCta;
      logit( "e", "%s\n", CtaFile[i] );

/* Open a cta disk file
   ********************/
      if ( (fpCta = fopen( CtaFile[i], "rb" )) == NULL )
      {  
         logit( "e", "Error opening cta disk file: %s\n", CtaFile[i] );
         logit( "e", "Exiting.\n" );
         return EW_FAILURE;
      }
       
/* Read tracebuf messages from the cta file
   ****************************************/
      while ( fread( WaveHead, sizeHeader, 1, fpCta ) == 1 )
      {
         int j;
         int wantIt = 0;          /* Non-zero if we want this SCNL */
         int nDataBytes;
         int nMsgBytes;
         TRACE2_HEADER thdr;

/* Make a local copy of the trace header
   *************************************/
         memcpy( &thdr, WaveHead, sizeHeader );

/* Convert tracebuf header to local byte order
   *******************************************/
         if ( TraceHeadMakeLocal( &thdr ) == -1 )
         {
            logit( "e", "Unknown data type: %s\n", thdr.datatype );
            logit( "e", "Exiting.\n" );
            exit( -1 );
         }

/* Read the data samples for this message from the cta file
   ********************************************************/
         nDataBytes = thdr.nsamp * (thdr.datatype[1] - 48);
         nMsgBytes  = nDataBytes + sizeHeader;
 
         if ( fread( dataPtr, sizeof(char), nDataBytes, fpCta ) < nDataBytes )
         {
            logit( "e", "Error reading cta file: %s\n", CtaFile[i] );
            logit( "e", "Exiting.\n" );
            return -1;
         }

/* If the SCNL strings are too long, ignore the message.
   Some Reftek test data was give bogus, long SCNL names.
   *****************************************************/
         if ( strlen( thdr.sta  ) > 5 ) continue;
         if ( strlen( thdr.chan ) > 3 ) continue;
         if ( strlen( thdr.net  ) > 2 ) continue;
         if ( strlen( thdr.loc  ) > 2 ) continue;

/* We want this message if its SCNL is in the SCNL list.
   The index of the matching SCNL (j) is used below.
   ****************************************************/
         for ( j = 0; j < nScnl; j++ )
         { 
            int sta_ok = strcmp( scnls[j].sta, thdr.sta  ) == 0;
            int cmp_ok = strcmp( scnls[j].cmp, thdr.chan ) == 0;
            int net_ok = strcmp( scnls[j].net, thdr.net  ) == 0;
            int loc_ok = strcmp( scnls[j].loc, thdr.loc  ) == 0;

            if ( wantIt = sta_ok && cmp_ok && net_ok && loc_ok )
               break;
         }
         if ( !wantIt ) continue;      /* Get next tracebuf message */

/* Is the message in the desired time window?
   If not, get the next message.
   *****************************************/
         {
            double trigStartTime = scnls[j].stime;
            double trigEndTime   = scnls[j].etime;
            int    dontWantIt    = ( trigEndTime < thdr.starttime ) ||
                                   ( thdr.endtime < trigStartTime );
            wantIt = !dontWantIt;
         }
         if ( !wantIt ) continue;      /* Get next tracebuf message */

/* Convert message header and waveform to local byte order
   *******************************************************/
         WaveMsgMakeLocal( (TRACE_HEADER *)tracebuf );

/* Write message to temporary file
   *******************************/
         {
            int nwritten;
            nwritten = write( scnls[j].fdTemp, tracebuf, nMsgBytes );
            if ( nwritten != nMsgBytes )
            {
               logit( "e", "Error writing temporary file.\n" );
               return EW_FAILURE;
            }
         }
      }                    /* End of loop that processes one tracebuf msg */
      fclose( fpCta );
   }
   return EW_SUCCESS;      /* Leave the temporary files open */
}


       /**********************************************************
        *                   WritePutawayFiles()                  *
        *  Write the putaway files.                              *
        *  Returns EW_SUCCESS or EW_FAILURE.                     *
        **********************************************************/

int WritePutawayFiles( SCNL *scnls, int nScnl, char *tracebuf,
                       char eventId[], char eventDate[],
                       char eventTime[] )
{
   const int maxDataBytes = MAX_TRACEBUF_SIZ - sizeHeader;
   TRACE_HEADER *WaveHead = (TRACE_HEADER *) tracebuf;
   char *dataPtr = tracebuf + sizeHeader;
   int  i;
   int  rc;
   long outBufferLen;
   int  formatInd;                   /* from PA_init() */
   TRACE_REQ trace_req;
   char *pBuf;                       /* Pointer to snippet buffer */
   unsigned long snipBufLen = 0;

/* Allocate buffer to contain one snippet, which
   contains all tracebuf messages for one channel.
   Length of buffer is maximum snippet length.
   **********************************************/
   for ( i = 0; i < nScnl; i++ )
   {
      if ( snipBufLen < scnls[i].nbyte )
         snipBufLen = scnls[i].nbyte;
   }
   pBuf = malloc( snipBufLen );
   if ( pBuf == NULL )
   {
      logit( "e", "malloc() error allocating snippet buffer.\n" );
      return -1;
   }
   trace_req.bufLen = snipBufLen;
   trace_req.pBuf   = pBuf;         /* Fill in trace-request structure */

/* Initialize the putaway routines
   *******************************/
   rc = PA_init( DataFormat, snipBufLen, &outBufferLen,
                 &formatInd, OutDir, OutputFormat, PA_debug );
   if ( rc == EW_FAILURE )
   {
      logit( "e", "PA_init() error.\n" );
      return EW_FAILURE;
   }

/* Declare that we are starting a new event
   ****************************************/
   {
      static char eventSubnet[] = "unused";
      int   num_req;  /* unused */

      rc = PA_next_ev( eventId, &trace_req, num_req,
                       formatInd, OutDir, eventDate, eventTime,
                       eventSubnet, PA_debug );
      if ( rc == EW_FAILURE )
      {
         logit( "e", "PA_next_ev() error.\n" );
         return EW_FAILURE;
      }
   }

/* Loop over all temporary files
   *****************************/
   for ( i = 0; i < nScnl; i++ )
   {
      double gapThresh = 100;
      char *snipPtr = pBuf;
      int  rc_PA_next;        /* Return code from PA_next() */

/* Fill in the trace request structure
   ***********************************/
      strcpy( trace_req.sta,  scnls[i].sta );
      strcpy( trace_req.chan, scnls[i].cmp );
      strcpy( trace_req.net,  scnls[i].net );
      strcpy( trace_req.loc,  scnls[i].loc );
      trace_req.reqStarttime = scnls[i].stime;
      trace_req.reqEndtime   = scnls[i].etime;
      trace_req.actStarttime = DBL_MAX;
      trace_req.actEndtime   = DBL_MIN;
      trace_req.actLen       = 0;

/* Read tracebuf messages from the temporary file
   **********************************************/
      while ( read( scnls[i].fdTemp, WaveHead, sizeHeader ) == sizeHeader )
      {
         int nDataBytes;
         int nTraceBytes;

/* Read the data samples for this message
   **************************************/
         nDataBytes = WaveHead->nsamp * (WaveHead->datatype[1] - 48);
 
         if ( read( scnls[i].fdTemp, dataPtr, nDataBytes ) < nDataBytes )
         {
            logit( "e", "Error reading temporary file.\n" );
            free( pBuf );
            return EW_FAILURE;
         }

/* Copy this tracebuf message to the snippet buffer.
   Increment snippet pointer by multiple of four bytes.
   ***************************************************/
         nTraceBytes = sizeHeader + nDataBytes;
         memcpy( snipPtr, WaveHead, nTraceBytes );
         snipPtr += nTraceBytes;
         trace_req.actLen += nTraceBytes;
         {
            int remainder = nTraceBytes % 4;
            if ( remainder > 0 )
            {
               int offset = 4 - remainder;
               snipPtr += offset;
               trace_req.actLen += offset;
            }
         }

/* Fill in more fields in the trace_req buffer
   *******************************************/
         trace_req.samprate = WaveHead->samprate;
         if ( trace_req.actStarttime > WaveHead->starttime )
            trace_req.actStarttime   = WaveHead->starttime;
         if ( trace_req.actEndtime   < WaveHead->endtime )
            trace_req.actEndtime     = WaveHead->endtime;
      }        /* End of loop that processes one tracebuf message */

/*  Convert this channel to output format
    *************************************/
      rc_PA_next = PA_next( &trace_req, formatInd, gapThresh,
                            outBufferLen, PA_debug );
      if ( rc_PA_next == EW_FAILURE )
      {
         logit( "e", "PA_next() error.\n" );
         free( pBuf );
         return EW_FAILURE;
      }
   }           /* End of loop that processes one channel */

/* Terminate the putaway system
   ****************************/
   if ( PA_end_ev( formatInd, PA_debug ) == EW_FAILURE )
   {
      logit( "e", "PA_end_ev() error.\n" );
      free( pBuf );
      return EW_FAILURE;
   }
   if ( PA_close( formatInd, PA_debug ) == EW_FAILURE )
   {
      logit( "e", "PA_close() error.\n" );
      free( pBuf );
      return EW_FAILURE;
   }
   free( pBuf );
   return EW_SUCCESS;
}


/*************************************************************
 *                     RewindTempFiles()                     *
 *                Rewind the temporary files.                *
 *************************************************************/

void RewindTempFiles( SCNL *scnls, int nScnl )
{
   int  i;
   for ( i = 0; i < nScnl; i++ )
      lseek( scnls[i].fdTemp, 0, SEEK_SET );
   return;
}


/*************************************************************
 *                      CloseTempFiles()                     *
 *                 Close the temporary files.                *
 *************************************************************/

void CloseTempFiles( SCNL *scnls, int nScnl )
{
   int  i;
   for ( i = 0; i < nScnl; i++ )
      close( scnls[i].fdTemp );
   return;
}
