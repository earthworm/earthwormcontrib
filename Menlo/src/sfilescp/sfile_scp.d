# sfile_rcp.d
#
# Picks up files from specified directory(s) and copies them via rcp
# to specific directories on other computers. 
#

# Basic Module information
#-------------------------
MyModuleId        MOD_SFILERCP     # module id 
RingName          HYPO_RING        # shared memory ring for output
HeartBeatInterval 30               # seconds between heartbeats to statmgr

LogFile           1                # 0 log to stderr/stdout only; 
                                   # 1 log to stderr/stdout and disk;
                                   # 2 log to disk module log only.

Debug             0                # 1=> debug output. 0=> no debug output

# Data file manipulation
#-----------------------
CheckPeriod     1                  # sleep this many seconds between looks
OpenTries       5                  # How many times we'll try to open a file 
OpenWait        200                # Milliseconds to wait between open tries
SaveDataFiles   0                  # 0 = remove files after processing
                                   # non-zero = move files to save subdir
                                   #            after processing  
# Data file directory specification
#----------------------------------
# For each file to be forwarded, we specify the number of destinations, 
# a save flag and the directory in which to look.
# This is followed by a list of the destinations for the files.
#
GetFromDir   4  0   /home/picker/sendfiler/heli  # look for files in this directory
# List of target computers/directories to place output.
#       UserName  IP Address            Directory
        luetgert@ehzmenlo.wr.usgs.gov:/webd1/ncweb/waveforms/helicorder/
        luetgert@ehznorth.wr.usgs.gov:/webd1/ncweb/waveforms/helicorder/
        luetgert@ehzsouth.wr.usgs.gov:/webd1/ncweb/waveforms/helicorder/
        luetgert@ehzeast.wr.usgs.gov:/webd1/ncweb/waveforms/helicorder/

GetFromDir   4  0   /home/picker/sendfiler/sgram1  # look for files in this directory
# List of target computers/directories to place output.
#       UserName  IP Address            Directory
        luetgert@ehzmenlo.wr.usgs.gov:/webd1/ncweb/waveforms/sgram/
        luetgert@ehznorth.wr.usgs.gov:/webd1/ncweb/waveforms/sgram/
        luetgert@ehzsouth.wr.usgs.gov:/webd1/ncweb/waveforms/sgram/
        luetgert@ehzeast.wr.usgs.gov:/webd1/ncweb/waveforms/sgram/

GetFromDir   4  0   /home/picker/sendfiler/sgram2  # look for files in this directory
# List of target computers/directories to place output.
#       UserName  IP Address            Directory
        luetgert@ehzmenlo.wr.usgs.gov:/webd1/ncweb/waveforms/sgramhr/
        luetgert@ehznorth.wr.usgs.gov:/webd1/ncweb/waveforms/sgramhr/
        luetgert@ehzsouth.wr.usgs.gov:/webd1/ncweb/waveforms/sgramhr/
        luetgert@ehzeast.wr.usgs.gov:/webd1/ncweb/waveforms/sgramhr/


GetFromDir   5  0   /home/picker/sendfiler/stalist  # look for files in this directory
# List of target computers/directories to place output.
#       UserName  IP Address            Directory
        luetgert@ehzmenlo.wr.usgs.gov:/webd1/ehzweb/waveforms/wavesall/
        luetgert@ehzmenlo.wr.usgs.gov:/webd1/ncweb/waveforms/wavesall/
        luetgert@ehznorth.wr.usgs.gov:/webd1/ncweb/waveforms/wavesall/
        luetgert@ehzsouth.wr.usgs.gov:/webd1/ncweb/waveforms/wavesall/
        luetgert@ehzeast.wr.usgs.gov:/webd1/ncweb/waveforms/wavesall/

  


