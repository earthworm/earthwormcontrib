CFLAGS = -D_REENTRANT $(GLOBALFLAGS)

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

BINARIES = sfilescp.o  \
          $L/chron3.o $L/logit.o $L/kom.o $L/getutil.o \
          $L/sleep_ew.o $L/time_ew.o $L/transport.o $L/dirops_ew.o  $L/remote_copy.o   

sfilescp: $(BINARIES)
	cc -o $B/sfilescp $(BINARIES)  -lm -lposix4

.c.o:
	$(CC) $(CFLAGS) -g $(CPPFLAGS) -c  $(OUTPUT_OPTION) $<



# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/sfilescp*

# THE END
