#
#   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
#   CHECKED IT OUT USING THE COMMAND CHECKOUT.
#
#    $Id: makefile.sol 2 2004-04-28 23:15:44Z dietz $
#
#    Revision history:
#     $Log$
#     Revision 1.1  2004/04/28 23:15:44  dietz
#     Initial revision
#
#     Revision 1.3  2000/08/08 18:00:30  lucky
#     Added lint directive
#
#     Revision 1.2  2000/02/14 21:26:54  lucky
#     *** empty log message ***
#
#     Revision 1.1  2000/02/14 16:04:49  lucky
#     Initial revision
#
#
#

CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
SRCS = cube2trig.c writetrig.c

B = $(EW_HOME)/$(EW_VERSION)/bin 
L = $(EW_HOME)/$(EW_VERSION)/lib

ARC = cube2trig.o writetrig.o $L/chron3.o $L/logit.o $L/getutil.o \
     $L/read_arc.o $L/transport.o $L/kom.o $L/sleep_ew.o $L/time_ew.o

cube2trig: $(ARC)
	cc -o $(B)/cube2trig $(ARC) -lm -lposix4

.c.o:
	cc -c ${CFLAGS} $<

lint:
	lint cube2trig.c writetrig.c $(GLOBALFLAGS)

depend:
	makedepend -fmakefile.sol -- $(CFLAGS) -- $(SRCS)

# DO NOT DELETE THIS LINE -- make depend depends on it.


# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/cube2trig*
