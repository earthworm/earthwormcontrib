# This is cube2trig's parameter file

#  Basic Earthworm setup:
#
MyModuleId        MOD_CUBE2TRIG  # module id for this instance of cube2trig 
InRingName        HYPO_RING      # shared memory ring for input
OutRingName       HYPO_RING      # shared memory ring for output
LogFile              1           # 0 to turn off disk log file; 1 to turn it on
                                 # 2 to log to module log but not to stderr/stdout
HeartBeatInterval    30          # seconds between heartbeats

# List the message logos to grab from transport ring
#              Installation       Module          Message Types
GetEventsFrom  INST_WILDCARD    MOD_WILDCARD    # hyp2000arc - no choice.

# Set up output directory and prefix for trigger files.
# Daily files will be written with a suffix of .trg_yyyymmdd
# Set either to "none" or "NONE" to stop writing disk files
OutputDir  /home/picker/arc2trig/  # directory to write trigger files in
BaseName   cube2trig               # prefix of trigger file name
                                   # suffix will be .trg_yyyymmdd
 
 Authority US     # Data source

# Optional parameters:

 Debug 1
 
# MinSize 3.5
  MinSize 5.5
 
# Select 38.0 -122.0 5.0
  Select 19.5 -155.0 120.0
 
 