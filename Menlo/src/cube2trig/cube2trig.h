/*
 * cube2trig.h : Include file for cube2trig.c;
 */

#define MAX_STR		     255
#define SHORT_STR             64
#define MEDIUM_STR  (SHORT_STR*2)

/* Things read from config file
 ******************************/
char    OutputDir[SHORT_STR]; /* directory to write "triggers" to  */ 
char    TrigFileBase[SHORT_STR/2]; /* prefix of trigger file name  */

/* Function prototypes
 *********************/
int writetrig_init( void );   /*writetrig.c*/
int writetrig( char * );      /*writetrig.c*/ 
void writetrig_close( );      /*writetrig.c*/
