/*************************************************************************
 * cube2trig.c:                                                          *
 *   Takes a cube message from QDDS as input and produces a .trg file.   *
 *   Based on arc2trig.                                                  *
 *                                                                       *
 * Jim 7/20/01                                                           * 
 *************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <chron3.h>
#include <earthworm.h>
#include <read_arc.h>
#include <kom.h>
#include <transport.h>
#include "cube2trig.h"

/* Functions in this source file
 *******************************/
void   cube2trig_config  ( char * );
void   cube2trig_lookup  ( void );
void   cube2trig_status  ( unsigned char, short, char * );
int    cube2trig_hinvarc ( char*, char*, MSG_LOGO );
void Dstaz( double, double, double, double, double * );
char  *make_datestr( char *, char * );

static  SHM_INFO  InRegion;      /* shared memory region to use for input  */
static  SHM_INFO  OutRegion;     /* shared memory region to use for output */

#define   MAXLOGO   2
MSG_LOGO  GetLogo[MAXLOGO];    /* array for requesting module,type,instid */
short     nLogo;

static char CubeMsgBuf[MAX_BYTES_PER_EQ]; /* character string to hold event message
                                        MAX_BYTES_PER_EQ is from earthworm.h */

static char TrigMsgBuf[MAX_TRIG_BYTES]; /* to hold the trigger message / file */

/* Things to read or derive from configuration file
 **************************************************/
static char    InRingName[MAX_RING_STR];       /* name of transport ring for input  */
static char    OutRingName[MAX_RING_STR];      /* name of transport ring for output */
static char    MyModName[MAX_MOD_STR];        /* speak as this module name/id      */
static int     LogSwitch;            /* 0 if no logfile should be written */
static int     Flush;                /* 0 if ring should not be flushed */
static long    HeartBeatInterval;    /* seconds between heartbeats        */
static int     LocCode;              /* 0 if no no location codes are used */
static double  MinSize;              /* Minimum magnitude to consider */
static int     Debug;                /* 0 if no debug msgs should be written */
static double  LatCen, LonCen, MaxDist;
static int     nAuth;
static char    Auth[5][5];

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          InRingKey;      /* key of transport ring for input   */
static long          OutRingKey;     /* key of transport ring for output  */
static unsigned char InstId;         /* local installation id             */
static unsigned char MyModId;        /* Module Id for this program        */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeCubic;
static unsigned char TypeTrigList;
static unsigned char TypeTrigList2;

/* Error messages used by cube2trig
 *********************************/
#define  ERR_MISSMSG       0   /* message missed in transport ring       */
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer     */
#define  ERR_NOTRACK       2   /* msg retreived; tracking limit exceeded */
#define  ERR_FILEIO        3   /* error opening trigger file             */
#define  ERR_DECODEARC     4   /* error reading input archive message    */
static char  Text[150];        /* string for log/error messages          */

pid_t	MyPid; 	/** Our process id is sent with heartbeat **/

/** length of string required by make_datestr  **/
#define	DATESTR_LEN		22	

int main( int argc, char **argv )
{
   long      timeNow;          /* current time                  */
   long      timeLastBeat;     /* time last heartbeat was sent  */
   long      recsize;          /* size of retrieved message     */
   MSG_LOGO  reclogo;          /* logo of retrieved message     */
   int       res;


/* Check command line arguments
 ******************************/
	if ( argc != 2 ) {
		fprintf( stderr, "Usage: cube2trig <configfile>\n" );
		exit( 0 );
	}

/* Read the configuration file(s)
 ********************************/
	cube2trig_config( argv[1] );

/* Look up important info from earthworm.h tables
 ************************************************/
	cube2trig_lookup();

/* Initialize name of log-file & open it
 ***************************************/
	logit_init( argv[1], (short) MyModId, 256, LogSwitch );
	logit( "" , "%s: Read command file <%s>\n", argv[0], argv[1] );

/* Get our own process ID for restart purposes
 **********************************************/

	if ((MyPid = getpid ()) == -1) {
		logit ("e", "cube2trig: Call to getpid failed. Exitting.\n");
		exit (-1);
	}

/* Initialize the trigger file
 *****************************/
	if( writetrig_init() != 0 ) {
		logit("", "arctrig: error opening file in OutputDir <%s>",
		    OutputDir);
		logit("", "                          with BaseName  <%s>\n",
		    TrigFileBase );
		exit( -1 );
	}
	logit( "", "cube2trig: Writing trigger files in %s\n", OutputDir );

/* Attach to Input shared memory ring
 ************************************/
	tport_attach( &InRegion, InRingKey );
	logit( "", "cube2trig: Attached to public memory region %s: %d\n",
	      InRingName, InRingKey );

/* Attach to Output shared memory ring
 *************************************/
	tport_attach( &OutRegion, OutRingKey );
	logit( "", "cube2trig: Attached to public memory region %s: %d\n",
	      OutRingName, OutRingKey );

/* Force a heartbeat to be issued in first pass thru main loop
 *************************************************************/
	timeLastBeat = time(&timeNow) - HeartBeatInterval - 1;

/* Flush the incoming transport ring on startup
 **************************************************/

	if(Flush) {
	while (tport_getmsg (&InRegion, GetLogo, nLogo,  &reclogo,
				&recsize, CubeMsgBuf, sizeof(CubeMsgBuf)- 1) != GET_NONE)

		;
	}

/*----------------------- setup done; start main loop -------------------------*/

	while(1) {
     /* send cube2trig's heartbeat
      ***************************/
		if  ( time(&timeNow) - timeLastBeat  >=  HeartBeatInterval ) {
			timeLastBeat = timeNow;
			cube2trig_status( TypeHeartBeat, 0, "" );
		}

     /* Process all new messages
      **************************/
		do {
        /* see if a termination has been requested
         *****************************************/
			if ( tport_getflag( &InRegion ) == TERMINATE ) {
				writetrig_close();
			/* detach from shared memory */
				tport_detach( &InRegion );
			/* write a termination msg to log file */
				logit( "t", "cube2trig: Termination requested; exiting!\n" );
				fflush( stdout );
				exit( 0 );
			}

        /* Get msg & check the return code from transport
         ************************************************/
			res = tport_getmsg( &InRegion, GetLogo, nLogo,
			                   &reclogo, &recsize, CubeMsgBuf, sizeof(CubeMsgBuf)-1 );

			if( res == GET_NONE ) {         /* no more new messages     */
				break;
			}
			else if( res == GET_TOOBIG ) {  /* next message was too big */
				                            /* complain and try again   */
				sprintf(Text,
				        "Retrieved msg[%ld] (i%u m%u t%u) too big for CubeMsgBuf[%d]",
				        recsize, reclogo.instid, reclogo.mod, reclogo.type,
				        sizeof(CubeMsgBuf)-1 );
				cube2trig_status( TypeError, ERR_TOOBIG, Text );
				continue;
			}
			else if( res == GET_MISS ) {    /* got a msg, but missed some */
				sprintf( Text,
				        "Missed msg(s)  i%u m%u t%u  %s.",
				         reclogo.instid, reclogo.mod, reclogo.type, InRingName );
				cube2trig_status( TypeError, ERR_MISSMSG, Text );
			}
			else if( res == GET_NOTRACK ) { /* got a msg, but can't tell */
				                            /* if any were missed        */
				sprintf( Text,
				         "Msg received (i%u m%u t%u); transport.h NTRACK_GET exceeded",
				          reclogo.instid, reclogo.mod, reclogo.type );
				cube2trig_status( TypeError, ERR_NOTRACK, Text );
			}

        /* Process the message
         *********************/
           CubeMsgBuf[recsize] = '\0';      /*null terminate the message*/

           if(Debug>1) logit( "et", "Msg received: %s\n", CubeMsgBuf );
           
           if( reclogo.type == TypeCubic ) {
               int ret;
               ret   = cube2trig_hinvarc( CubeMsgBuf, TrigMsgBuf, reclogo );
               if(ret) cube2trig_status( TypeError, ERR_DECODEARC, "" );
           }

        } while( res != GET_NONE );  /*end of message-processing-loop */

        sleep_ew( 1000 );  /* no more messages; wait for new ones to arrive */

   }
}

/******************************************************************************
 *  cube2trig_config() processes command file(s) using kom.c functions;        *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
void cube2trig_config( char *configfile )
{
   int      ncommand;     /* # of required commands you expect to process   */
   char     init[10];     /* init flags, one byte for each required command */
   int      nmiss;        /* number of required commands that were missed   */
   char    *com;
   char    *str;
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command
 *****************************************************/
   ncommand = 9;
   for( i=0; i<ncommand; i++ )  init[i] = 0;
   nLogo = 0;
   MinSize = 0;
   Debug = 0;
   LatCen = LonCen = 0.0;
   MaxDist = 360.;
   Flush = 1;
   nAuth = 0;
	LocCode = 1;

/* Open the main configuration file
 **********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 ) {
        fprintf( stderr,
                "cube2trig: Error opening command file <%s>; exiting!\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr,
                          "cube2trig: Error opening command file <%s>; exiting!\n",
                           &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("InRingName") ) {
                str = k_str();
                if(str) strcpy( InRingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("OutRingName") ) {
                str = k_str();
                if(str) strcpy( OutRingName, str );
                init[3] = 1;
            }
  /*4*/     else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_long();
                init[4] = 1;
            }


         /* Enter installation & module to get event messages from
          ********************************************************/
  /*5*/     else if( k_its("GetEventsFrom") ) {
                if ( nLogo+1 >= MAXLOGO ) {
                    fprintf( stderr,
                            "cube2trig: Too many <GetEventsFrom> commands in <%s>",
                             configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAXLOGO/2 );
                    exit( -1 );
                }
                if( ( str=k_str() ) ) {
                   if( GetInst( str, &GetLogo[nLogo].instid ) != 0 ) {
                       fprintf( stderr,
                               "cube2trig: Invalid installation name <%s>", str );
                       fprintf( stderr, " in <GetEventsFrom> cmd; exiting!\n" );
                       exit( -1 );
                   }
                   GetLogo[nLogo+1].instid = GetLogo[nLogo].instid;
                }
                if( ( str=k_str() ) ) {
                   if( GetModId( str, &GetLogo[nLogo].mod ) != 0 ) {
                       fprintf( stderr,
                               "cube2trig: Invalid module name <%s>", str );
                       fprintf( stderr, " in <GetEventsFrom> cmd; exiting!\n" );
                       exit( -1 );
                   }
                   GetLogo[nLogo+1].mod = GetLogo[nLogo].mod;
                }
                if( GetType( "TYPE_CUBIC", &GetLogo[nLogo].type ) != 0 ) {
                    fprintf( stderr,
                               "cube2trig: Invalid message type <TYPE_CUBIC>" );
                    fprintf( stderr, "; exiting!\n" );
                    exit( -1 );
                }
                   GetLogo[nLogo+1].type = GetLogo[nLogo].type;
                nLogo  += 2;
                init[5] = 1;
            /*    printf("GetLogo[%d] inst:%d module:%d type:%d\n",
                        nLogo, (int) GetLogo[nLogo].instid,
                               (int) GetLogo[nLogo].mod,
                               (int) GetLogo[nLogo].type ); */  /*DEBUG*/
            /*    printf("GetLogo[%d] inst:%d module:%d type:%d\n",
                        nLogo+1, (int) GetLogo[nLogo+1].instid,
                               (int) GetLogo[nLogo+1].mod,
                               (int) GetLogo[nLogo+1].type ); */  /*DEBUG*/
            }
  /*6*/     else if( k_its("OutputDir") ) {
                str = k_str();
                if(str) strcpy( OutputDir, str );
                init[6] = 1;
            }
  /*7*/     else if( k_its("BaseName") ) {
                str = k_str();
                if(str) strcpy( TrigFileBase, str );
                init[7] = 1;
            }
  /*8*/     else if( k_its("Authority") ) {
                str = k_str();
                if(nAuth < 5) {
					if(str) strcpy( Auth[nAuth++], str );
					init[8] = 1;
                }
            }

  /*optional*/
			else if( k_its("NoFlush") ) {
                Flush = 0;
            }

  /*optional*/
			else if( k_its("MinSize") ) {
                MinSize = k_val();
            }

  /*optional*/
	    else if( k_its("Debug") ) {
                Debug = k_int();
            }
  /*optional*/
            else if( k_its("SCN") ) {  
                LocCode = 0;
            }
  /*optional*/
            else if( k_its("SCNL") ) {  
                LocCode = 1;
            }

  /*optional*/
	    else if( k_its("Select") ) {
                LatCen = k_val();
                LonCen = k_val();
                MaxDist = k_val();
            }

         /* Unknown command
          *****************/
            else {
                fprintf( stderr, "cube2trig: <%s> Unknown command in <%s>.\n",
                         com, configfile );
                continue;
            }

        /* See if there were any errors processing the command
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr,
                       "cube2trig: Bad <%s> command in <%s>; exiting!\n",
                        com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "cube2trig: ERROR, no " );
       if ( !init[0] )  fprintf( stderr, "<LogFile> "           );
       if ( !init[1] )  fprintf( stderr, "<MyModuleId> "        );
       if ( !init[2] )  fprintf( stderr, "<InRingName> "        );
       if ( !init[3] )  fprintf( stderr, "<OutRingName> "       );
       if ( !init[4] )  fprintf( stderr, "<HeartBeatInterval> " );
       if ( !init[5] )  fprintf( stderr, "<GetEventsFrom> "     );
       if ( !init[6] )  fprintf( stderr, "<OutputDir>"          );
       if ( !init[7] )  fprintf( stderr, "<BaseName>"           );
       if ( !init[8] )  fprintf( stderr, "<Authority>"          );
       fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }

   return;
}

/******************************************************************************
 *  cube2trig_lookup( )   Look up important info from earthworm.h tables       *
 ******************************************************************************/
void cube2trig_lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( InRingKey = GetKey(InRingName) ) == -1 ) {
        fprintf( stderr,
                "cube2trig:  Invalid ring name <%s>; exiting!\n", InRingName);
        exit( -1 );
   }

/* Look up keys to shared memory regions
   *************************************/
   if( ( OutRingKey = GetKey(OutRingName) ) == -1 ) {
        fprintf( stderr,
                "cube2trig:  Invalid ring name <%s>; exiting!\n", OutRingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr,
              "cube2trig: error getting local installation id; exiting!\n" );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr,
              "cube2trig: Invalid module name <%s>; exiting!\n", MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr,
              "cube2trig: Invalid message type <TYPE_HEARTBEAT>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr,
              "cube2trig: Invalid message type <TYPE_ERROR>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_CUBIC", &TypeCubic ) != 0 ) {
      fprintf( stderr,
              "cube2trig: Invalid message type <TYPE_CUBIC>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRIGLIST2K", &TypeTrigList ) != 0 ) {
      fprintf( stderr,
              "cube2trig: Invalid message type <TYPE_TRIGLIST2K>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRIGLIST_SCNL", &TypeTrigList2 ) != 0 ) {
      fprintf( stderr,
              "arc2trig2: Invalid message type <TYPE_TRIGLIST_SCNL>; Use SCNs only!\n" );
      LocCode = 0;
   }

   return;
}

/******************************************************************************
 * cube2trig_status() builds a heartbeat or error message & puts it into       *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/
void cube2trig_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size;
   long        t;

/* Build the message
 *******************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
        sprintf( msg, "%ld %ld\n\0", t, MyPid );
   }
   else if( type == TypeError )
   {
        sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
        logit( "et", "cube2trig: %s\n", note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &OutRegion, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","cube2trig:  Error sending heartbeat.\n" );
        }
        else if( type == TypeError ) {
           logit("et","cube2trig:  Error sending error:%d.\n", ierr );
        }
   }

   return;
}

/*******************************************************************
 *  cube2trig_hinvarc( )                                           *
 *  read a Cube message and build a trigger message                *
 *******************************************************************/
			   /*
E snaz    US6200107200509392 457732  265772125748 51 515437 096  74  0011B28   Y
          1         2         3         4         5         6         7          
01234567890123456789012345678901234567890123456789012345678901234567890123456789
			   */
int cube2trig_hinvarc( char* cubemsg, char* trigMsg, MSG_LOGO incoming_logo )
{
	MSG_LOGO  outgoing_logo;  /* outgoing logo                         */
	char      line[MAX_STR];  /* to store lines from msg               */
	char      shdw[MAX_STR];  /* to store shadow cards from msg        */
	char      str[120];
	char datestr[DATESTR_LEN];
	int       i, mag, dep, latd, lond, ignore;
	double    rmag, dist_deg, latEvent, lonEvent, depthEvent;

   /* load outgoing logo
   *********************/
	outgoing_logo.instid = InstId;
	outgoing_logo.mod    = MyModId;
	outgoing_logo.type   = TypeTrigList;
   outgoing_logo.type   = LocCode? TypeTrigList2:TypeTrigList;
	
			/* If the file is not from approved source.  Ignore it.
			   ****************************************************/
	ignore = 1;
	for(i=0;i<nAuth;i++) {
		if ( strncmp( cubemsg+10, Auth[i], 2 ) == 0 ) ignore = 0;
	}
	if ( ignore ) {
        if(Debug) logit( "et", "Event ignored: %s\n", cubemsg );
        return 0;
	} 

	memcpy( str, cubemsg+28, 7 );
	str[7] = '\0';
	if ( sscanf( str, "%d", &latd ) < 1 ) return -1;
	latEvent = (double)latd / 10000.;

	memcpy( str, cubemsg+35, 8 );
	str[8] = '\0';
	if ( sscanf( str, "%d", &lond ) < 1 ) return -1;
	lonEvent = (double)lond / 10000.;

	memcpy( str, cubemsg+43, 4 );
	str[4] = '\0';
	if ( sscanf( str, "%d", &dep  ) < 1 ) return -1;
	depthEvent = (double)dep / 10.;

	memcpy( str, cubemsg+47, 2 );
	str[2] = '\0';
	if(strcmp(str, "  ") == 0) {
		mag = 0.0;
	}
	else {
		if ( sscanf( str, "%d", &mag ) < 1 ) return -1;
	}
	rmag = mag/10.;

	memcpy( str, cubemsg+13, 12 );
	str[12] = '\0';

	if(rmag < MinSize) {
		if(Debug) logit("e", "cube2trig: Event %12s too small (%5.2f)\n", str, rmag);
		return 0;
	}

	Dstaz( LatCen, LonCen, latEvent, lonEvent, &dist_deg );

	if ( dist_deg > MaxDist ) {
		if(Debug) logit("e", "cube2trig: Event %12s too far away. \n", str);
		return 0;
	}
	
/* Sample EVENT line for trigger message:
EVENT DETECTED     970729 03:01:13.22 UTC EVENT ID:123456 AUTHOR: asdf:asdf\n
0123456789 123456789 123456789 123456789 123456789 123456789
************************************************************/
	make_datestr( &cubemsg[13], datestr );
   if(LocCode) {
	sprintf( trigMsg, "v2.0 EVENT DETECTED     %s UTC EVENT ID: %12s AUTHOR: %03d%03d%03d:%03d%03d%03d\n\n", 
			datestr, str,
			incoming_logo.type, incoming_logo.mod, incoming_logo.instid,
			outgoing_logo.type, outgoing_logo.mod, outgoing_logo.instid );
   strcat ( trigMsg, "Sta/Cmp/Net/Loc   Date   Time                       start save       duration in sec.\n" );
   strcat ( trigMsg, "---------------   ------ ---------------    ------------------------------------------\n");
   } else {
	sprintf( trigMsg, "EVENT DETECTED     %s UTC EVENT ID: %12s AUTHOR: %03d%03d%03d:%03d%03d%03d\n\n", 
			datestr, str,
			incoming_logo.type, incoming_logo.mod, incoming_logo.instid,
			outgoing_logo.type, outgoing_logo.mod, outgoing_logo.instid );
   strcat ( trigMsg, "Sta/Cmp/Net   Date   Time                       start save       duration in sec.\n" );
   strcat ( trigMsg, "-----------   ------ ---------------    ------------------------------------------\n");
   }

/****************************************************************
 * Build a dummy "phase" line of a trigger message.             *
 * This is needed because trig2disk needs at least one pick     *
 * time.                                                        *
 ****************************************************************/

   if(LocCode) {
	sprintf(str, " STA CMP NET LOC N %s UTC    save: %s 120\n", datestr, datestr );
   } else {
	sprintf(str, " STA CMP NET N %s UTC    save: %s 120\n", datestr, datestr );
   }
	strcat ( trigMsg, str );

	if(Debug) {
		logit("et", "Cube msg accepted and passed as trig msg.\n" ); 
		i = 0;
		while(i < strlen(trigMsg)) {
			logit("e", "%.100s", &trigMsg[i] );
			i += 100; 
		}
		logit("e", "\n\n" ); 
	}



  /* Write trigger message to output ring
   **************************************/
   if( tport_putmsg( &OutRegion, &outgoing_logo, strlen(trigMsg), trigMsg ) != PUT_OK )
   {
      logit("et","cube2trig: Error writing trigger message to ring.\n" );
   }

   /* Write trigger message to trigger file
    ***************************************/
   if( writetrig( "\n" ) != 0 )  /* add a blank line before trigger list */
   {
      cube2trig_status( TypeError, ERR_FILEIO, "Error opening trigger file" );
   }
   if( writetrig( trigMsg ) != 0 )
   {
      cube2trig_status( TypeError, ERR_FILEIO, "Error opening trigger file" );
   }
   if( writetrig( "\n" ) != 0 )  /* add a blank line after trigger list */
   {
      cube2trig_status( TypeError, ERR_FILEIO, "Error opening trigger file" );
   }

   return(0);
}

/*********************************************************************
 * make_datestr()  takes a time and converts it into a character     *
 *                 string in the form of:                            *
 *                   "19880123 12:34:12.21"                          *
 *                 It returns a pointer to the new character string  *
 *                                                                   *
 *********************************************************************/

char *make_datestr( char *str17, char *datestr )
{
    strncpy( datestr, str17,    8 );    /*yyyymmdd*/
    datestr[8] = '\0';
    strcat ( datestr, " " );
    strncat( datestr, str17+8,  2 );    /*hr*/
    strcat ( datestr, ":" );
    strncat( datestr, str17+10,  2 );    /*min*/
    strcat ( datestr, ":" );
    strncat( datestr, str17+12, 2 );    /*seconds*/
    strcat ( datestr, "." );
    strncat( datestr, str17+14, 1 );    /*seconds*/

    /*printf( "str17 <%s>  newstr<%s>\n", str17, datestr );*/ /*DEBUG*/

    return( datestr );
}

 /******************************************************************
  *                             Dstaz()                            *
  *                                                                *
  *  Compute distance, in degrees, from point 1 to 2 on the        *
  *  surface of the Earth.  East longitudes are positive.          *
  *  From "Seismology" by K.E. Bullen.  Azimuth calculation not    *
  *  implemented.                                                  *
  ******************************************************************/

void Dstaz( double lat1d, double lon1d, double lat2d, double lon2d,
            double *dist_deg )
{
	const double pi = 3.14159265359;
	const double degrad = 180. / pi;

	double colat1 = (90. - lat1d) / degrad;
	double colat2 = (90. - lat2d) / degrad;
	double lon1   = lon1d / degrad;
	double lon2   = lon2d / degrad;

	double a1 = sin(colat1) * cos(lon1);
	double b1 = sin(colat1) * sin(lon1);
	double c1 = cos(colat1);
	double a2 = sin(colat2) * cos(lon2);
	double b2 = sin(colat2) * sin(lon2);
	double c2 = cos(colat2);
	double d  = (a1 * a2) + (b1 * b2) + (c1 * c2);

	if ( d <= -1. )
		*dist_deg = 180.;

	else if ( d >=  1. )
		*dist_deg = 0.;

	else
		*dist_deg = degrad * acos( d );
	return;
}


