

  /*****************************************************************
   *                          SwapAdbuf()                          *
   *                                                               *
   *             Swap bytes in adsend waveform buffer              *
   *                          SPARC only                           *
   *****************************************************************/

void SwapAdbuf( char *msg, int length )
{
   short *sh;
   short temp;

/* Swap bytes in the header, pin numbers, and data samples
   *******************************************************/
   swab( msg, msg, length );

/* The first three entries are longs.  Swap words.
   ***********************************************/
   sh    = (short *) msg;
   temp  = sh[0];
   sh[0] = sh[1];
   sh[1] = temp;
   temp  = sh[2];
   sh[2] = sh[3];
   sh[3] = temp;
   temp  = sh[4];
   sh[4] = sh[5];
   sh[5] = temp;

/* Byte 18 is a char.  Swap it back.
   *********************************/
   msg[18] = msg[19];
   msg[19] = '\0';
   return;
}
