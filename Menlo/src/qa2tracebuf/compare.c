#include "qa2tracebuf.h"


     /*************************************************************
      *                       ComparePins()                       *
      *                                                           *
      *  This function is passed to qsort() and bsearch().        *
      *  We use qsort() to sort the station list by pin numbers,  *
      *  and we use bsearch to look up a pin in the list.         *
      *************************************************************/

int ComparePins( const void *s1, const void *s2 )
{
   STATION *t1, *t2;

   t1 = (STATION *) s1;
   t2 = (STATION *) s2;

   if ( t1->pin < t2->pin )
      return -1;
   if ( t1->pin > t2->pin )
      return 1;
   return 0;
}
