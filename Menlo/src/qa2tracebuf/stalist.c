#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <earthworm.h>
#include "qa2tracebuf.h"
#include <malloc.h>

/* Global variables
   ****************/
extern char    StationFile[50];    /* Name of file with station info */
extern int     NumPins;            /* Number of pins in A/D messages */


  /***************************************************************
   *                         GetStaList()                        *
   *                                                             *
   *                     Read the station list                   *
   *                                                             *
   *  Returns -1 if an error is encountered.                     *
   ***************************************************************/

int GetStaList( STATION **Station, int *Nsta )
{
   char    string[130];
   FILE    *fp;
   STATION *ptr;
   STATION *station;
   size_t  size;
   int     nsta;                      /* Number of stations found in file */

/* Open the station list file
   **************************/
   if ( ( fp = fopen( StationFile, "r") ) == NULL )
   {
      logit( "et", "ad_demux: Error opening station list file <%s>.\n",
             StationFile );
      return -1;
   }

/* Read stations from the station list file into the station
   array, including parameters used by the picking algorithm
   *********************************************************/
   nsta = 0;
   ptr  = NULL;
   size = 0;
   
   while ( fgets( string, 130, fp ) != NULL )
   {
      if ( strncmp( string, "//", 2 ) == 0 ) continue;

      size += sizeof(STATION);

      station = (STATION *) realloc( (void *)ptr, size );
       
      if ( station == NULL )
      {
         logit( "et", "ad_demux: Error allocating station array.\n" );
         return -1;
      }

      if ( sscanf( string, "%hd%s%s%s",
           &station[nsta].pin, station[nsta].sta,
            station[nsta].net, station[nsta].chan ) < 4 )
      {
         logit( "et", "ad_demux: Error decoding station file.\n" );
         logit( "e", "Offending station:\n" );
         logit( "e", "%s\n", string );
         return -1;
      }

      nsta++;
      ptr = station;
   }
   
   fclose( fp );
   *Station = station;
   *Nsta = nsta;
   return 0;
}


      /*************************************************************
       *                        LogStaList()                       *
       *                                                           *
       *                    Log the station list                   *
       *************************************************************/

void LogStaList( STATION *Station, int Nsta )
{
   int i;

   logit( "", "\nStation List:\n" );
   for ( i = 0; i < Nsta; i++ )
   {
      logit( "", "%4hd",    Station[i].pin );
      logit( "", "   %-4s", Station[i].sta );
      logit( "", " %-2s",   Station[i].net );
      logit( "", " %-3s",   Station[i].chan );
      logit( "", "\n" );
   }
   logit( "", "\n" );
}
