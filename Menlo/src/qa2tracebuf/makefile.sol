
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = qa2tracebuf.o stalist.o compare.o adswap.o \
    $L/kom.o $L/sleep_ew.o $L/logit.o \
    $L/time_ew.o $L/swap.o

qa2tracebuf: $O
	cc -o $B/qa2tracebuf $O -lm -lposix4

lint:
	lint $(CFLAGS) qa2tracebuf.c

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/qa2tracebuf*
