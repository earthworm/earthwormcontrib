
        /**********************************************************
         *                        qa2tracebuf.c                   *
         *                                                        *
         *   Demultiplexer program for pre-pin adbuf messages     *
         *   the Earthworm A/D converter (Ad data).               *
         *                                                        *
         *   Link to files:                                       *
         *   qa2tracebuf.c, adswap.c, alloc.c, stalist.c,         *
         *   and compare.c                                        *
         *                                                        *
         *   Command line argument:                               *
         *      arg[1] = Name of configuration file               *
         **********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <time.h>
#include <earthworm.h>
#include <data_buf.h>
#include <trace_buf.h>
#include <kom.h>
#include <swap.h>
#include "qa2tracebuf.h"

/* Function prototypes
   *******************/
int  ComparePins( const void *, const void * );
int  GetConfig( char * );
void LogConfig( void );
int  AllocateBuffers( int *, int * );
int  GetStaList( STATION **, int * );
void LogStaList( STATION *, int );
void SwapAdbuf( char *, int );

#define MAX_WF  50    /* max # waveform files to reformat */
#define MAX_LEN 256   /* max length of a path or file name */

/* Parameters from the configuration file
   **************************************/
char   StationFile[50];          /* Name of file with station/pin info        */
int    BasePin;                  /* Pin# to assign to first channel in QAdata */
int    NumPins;                  /* Number of pins in A/D messages            */
int    SamplesPerPin;            /* Number of samples per A/D pin             */
char   InputDir[MAX_LEN];        /* directory containing input files          */
char   OutputDir[MAX_LEN];       /* directory in which to write output        */
char   QAfile[MAX_WF][MAX_LEN];  /* list of waveform files to reformat        */
int    nQAfile = 0;              /* # waveform files to reformat              */

char  *AdBuf;
char  *WaveBuf;


             /*****************************************
              *      The main program starts here     *
              *****************************************/

int main( int argc, char *argv[] )
{
   int          i;            /* Channel index */
   int          j;            /* Scan index */
   int          Nsta;         /* Number of stations found in file */
   STATION      *Station;     /* Station list */
   int          size;         /* Length of A/D message */
   WF_HEADER    *AdHead;      /* Pointer to A/D header */
   short        *AdDat;       /* Pointer to data region of QA A/D buffer */
   TRACE_HEADER *WaveHead;    /* Pointer to header of waveform buffer */
   int          AdBufSize;    /* In bytes */
   int          WaveBufSize;  /* In bytes */
   short       *WaveDat;      /* Pointer to data region of waveform buffer */
   FILE        *fp;           /* input original QA file */
   FILE        *fpout;        /* output file containing tracebuf messages */
   char         filename[MAX_LEN*2+4];  /* full path to file  */
   int          iw;

/* Check argument list
   *******************/
   if ( argc != 2 )
   {
      printf( "Usage: qa2tracebuf <configuration file>\n" );
      exit( 1 );
   }

/* Get parameters from the configuration file
   ******************************************/
   if ( GetConfig( argv[1] ) == -1 )
   {
      printf( "qa2tracebuf: Error getting configuration file. Exitting.\n" );
      exit( 1 );
   }

/* Initialize name of log-file & open it
   *************************************/
   logit_init( "qa2tracebuf", 0, 512, 1 );

/* Log parameters from the configuration file
   ******************************************/
   LogConfig();

/* Allocate buffers
   ****************/
   if ( AllocateBuffers( &AdBufSize, &WaveBufSize ) == -1 )
   {
      logit( "et", "qa2tracebuf: AllocateBuffers() error. Exitting.\n" );
      return -1;
   }

/* Get the station list
   ********************/
   if ( GetStaList( &Station, &Nsta ) == -1 )
   {
      logit( "et", "qa2tracebuf: GetStaList() error. Exitting.\n" );
      return -1;
   }
   logit( "", "\n" );
   logit( "t", "Number of stations in station list: %d\n", Nsta );

/* Sort the station list by pin number
   ***********************************/
   qsort( Station, Nsta, sizeof(STATION), ComparePins );

/* Log the station list
   ********************/
   LogStaList( Station, Nsta );

/* Allocate buffers
   ****************/
   if ( AllocateBuffers( &AdBufSize, &WaveBufSize ) == -1 )
   {
      logit( "et", "qa2tracebuf: AllocateBuffers() error. Exitting.\n" );
      return -1;
   }

/* Set up pointers to headers
   **************************/
   AdHead    = (WF_HEADER *)AdBuf;
   AdDat     = (short *)(AdBuf + sizeof(WF_HEADER));
   WaveHead  = (TRACE_HEADER *)WaveBuf;
   WaveDat   = (short *)(WaveBuf + sizeof(TRACE_HEADER));

/* Loop over all files to process
 ********************************/
   for( iw=0; iw<nQAfile; iw++ )
   {

   /* Open input/output files
      ***********************/
      strcpy( filename, InputDir   );
      strcat( filename, QAfile[iw] );
      fp = fopen( filename, "rb"   );
      if ( fp == NULL )
      {
         logit( "et", "qa2tracebuf: Cannot open QA file  %s\n", filename );
         continue;
      }
      logit( "et", "qa2tracebuf: Reformatting  %s ", filename );

      strcpy( filename, OutputDir  );
      strcat( filename, QAfile[iw] );
      strcat( filename, ".tbuf"    );
      fpout = fopen( filename, "wb" );
      if ( fpout == NULL )
      {
         logit( "e", "\nqa2tracebuf: Cannot open output file  %s\n", filename );
         continue;
      }
      logit( "e", "...writing to  %s  ", filename );

  /* Reformat a single file -
     read one QA adbuf message, demux it, write to output file
     *********************************************************/
      while( 1 )
      {
         short  nscan;          /* Number of samples for each channel in ADBUF */
         short  nchan;          /* Number of channels in ADBUF */
         short  *AdOffset;
         short  *AdBase;
         double samprate;       /* Sample rate for one ADBUF packet */
         double starttime;      /* Start time for one ADBUF packet */
         double endtime;        /* End time for one ADBUF packet */

      /* Read the header
       *****************/
         size = sizeof( WF_HEADER );
         if( fread( AdBuf, sizeof(char), (size_t)size, fp ) < (size_t)size )  break;
         nchan  = AdHead->nchan;
         nscan  = AdHead->nscan;
        #ifdef _SPARC
          SwapShort( &nscan );  /* Note: By definition, TYPE_ADBUF msgs */
          SwapShort( &nchan );  /*       are always in INTEL byte order */
        #endif

      /* Read data samples from file (NO PINS IN QA DATA!!!)
       *****************************************************/
         size = sizeof(short) * nchan * nscan;
         if( size + sizeof(WF_HEADER) > AdBufSize ) {
            logit( "e",
                  "\nqa2tracebuf: qamsg[%d] overflows internal AdBuf[%d]\n",
                   size+sizeof(WF_HEADER), AdBufSize );
            break;
         }
         if( fread( &AdBuf[sizeof(WF_HEADER)], sizeof(char), size, fp ) < (size_t)size )
         {
            break;
         }
         size += sizeof( WF_HEADER );

      /* Swap bytes when running on a SPARC
         **********************************/
        #ifdef _SPARC
            SwapAdbuf( AdBuf, size );
        #endif

      /* Calculate values common to all traces in A/D message
         ****************************************************/
         starttime = (double)AdHead->tssec + (0.000001*AdHead->tsmic);
         samprate  = 100.00;  /* hard code this cause it was bogus in the QA data */
         endtime   = starttime + ((double)(nscan - 1)/samprate);
         AdBase    = AdDat;

      /* Loop through all channels in the QA AdBuf message
         *************************************************/
         for ( i = 0; i < nchan; i++ )
         {
            int     rc;
            STATION key;
            STATION *Sta;

         /* Look up pin number in the station list
            **************************************/
            key.pin = BasePin + i;
            Sta = (STATION *)bsearch( &key, Station, Nsta, sizeof(STATION),
                                   ComparePins );

            if ( Sta == NULL )
            {
               logit( "e", "\nqa2tracebuf: Error. Pin number in A/D msg doesn't"
                           " match station list.  Exitting.\n" );
               logit( "e", "qa2tracebuf: pin in QA msg:         %d\n", BasePin+i );
               logit( "e", "qa2tracebuf: Pin from station list: %d\n", Sta->pin  );
               return -1;
            }

         /* Fill in Wavehead header values
            ******************************/
            WaveHead->pinno = BasePin + i;
            strcpy( WaveHead->sta,  Sta->sta );
            strcpy( WaveHead->net,  Sta->net );
            strcpy( WaveHead->chan, Sta->chan );
            WaveHead->starttime = starttime;
            WaveHead->endtime   = endtime;
            WaveHead->nsamp     = nscan;
            WaveHead->samprate  = samprate;

         /* Set datatype to the local byte order
            ************************************/
           #if defined( _INTEL )
              strcpy( WaveHead->datatype, "i2" );

           #elif defined( _SPARC )
              strcpy( WaveHead->datatype, "s2" );
           #else
              logit( "et", "\nError: _INTEL and _SPARC are both undefined. Exitting." );
              return -1;
           #endif

         /* Copy data samples to WaveDat
            ****************************/
            AdOffset = AdBase++;

            for ( j = 0; j < nscan; j++ )
            {
               WaveDat[j] = *AdOffset;
               AdOffset += nchan;
            }

         /* Write the new message to the output file
            ****************************************/
            size = sizeof(TRACE_HEADER) + nscan*sizeof(short);
            rc = fwrite( WaveBuf, sizeof(char), (size_t)size, fpout );
            if( rc != size ) {
               logit("e", "\nError writing to output file; exiting!!\n" );
               exit( -1 );
            }
         } /* end for over all channels in a QA AdBuf message */
      } /* end while over a single file */

      fclose( fp );
      fclose( fpout );
      logit("e", "...finished!\n" );

   } /*end for over all files */

   logit( "et", "qa2tracebuf: Finished processing all files. Exitting.\n" );
   free( AdBuf );
   free( WaveBuf );
   return 0;
}

     /***************************************************************
      *                          GetConfig()                        *
      *         Processes command file using kom.c functions.       *
      *           Returns -1 if any errors are encountered.         *
      ***************************************************************/
#define ncommand 7              /* Number of commands in the config file */

int GetConfig( char *configfile )
{
   char     init[ncommand];     /* Flags, one for each command */
   int      nmiss;              /* Number of commands that were missed */
   int      nfiles;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      fprintf( stderr, "qa2tracebuf: Error opening configuration file <%s>\n",
               configfile );
      return -1;
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          /* Get the first token from line */

         if ( !com ) continue;             /* Ignore blank lines */
         if ( com[0] == '#' ) continue;    /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               fprintf( stderr, "qa2tracebuf: Error opening command file <%s>.\n",
                        &com[1] );
               return -1;
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "StationFile" ) )
         {
            if ( str = k_str() )
               strcpy( StationFile, str );
            init[0] = 1;
         }

         else if ( k_its( "SamplesPerPin" ) )
         {
            SamplesPerPin = k_int();
            init[1] = 1;
         }

         else if ( k_its( "NumPins" ) )
         {
            NumPins = k_int();
            init[2] = 1;
         }

         else if ( k_its( "BasePin" ) )
         {
            BasePin = k_int();
            init[3] = 1;
         }

      /* Read a list of QA files to reformat
       *************************************/
/*4*/    else if( k_its("QAfile") )
         {
             if( nQAfile+1 >= MAX_WF ) {
                 fprintf( stderr,
                         "qa2tracebuf: Too many <QAfile> commands in <%s>",
                          configfile );
                 fprintf( stderr, "; max=%d; exitting!\n", MAX_WF );
                 exit( -1 );
             }
             if( ( str=k_str() ) ) {
                if( strlen(str) > (size_t)MAX_LEN-1 ) {
                    fprintf( stderr,
                            "qa2tracebuf: Filename <%s> too long in <QAfile>",
                             str );
                    fprintf( stderr, " cmd; max=%d; exitting!\n", MAX_LEN-1 );
                    exit( -1 );
                }
                strcpy( QAfile[nQAfile], str );
             }
             nQAfile++;
             init[4] = 1;
         }

         else if ( k_its( "InputDir" ) )
         {
             if( ( str=k_str() ) ) {
                if( strlen(str) > (size_t)MAX_LEN-1 ) {
                    fprintf( stderr,
                            "qa2tracebuf: directory name <%s> too long in <InputDir>",
                             str );
                    fprintf( stderr, " cmd; max=%d; exitting!\n", MAX_LEN-1 );
                    exit( -1 );
                }
                strcpy( InputDir, str );
             }
             init[5] = 1;
         }

         else if ( k_its( "OutputDir" ) )
         {
             if( ( str=k_str() ) ) {
                if( strlen(str) > (size_t)MAX_LEN-1 ) {
                    fprintf( stderr,
                            "qa2tracebuf: directory name <%s> too long in <OutputDir>",
                             str );
                    fprintf( stderr, " cmd; max=%d; exitting!\n", MAX_LEN-1 );
                    exit( -1 );
                }
                strcpy( OutputDir, str );
             }
             init[6] = 1;
         }


/* An unknown parameter was encountered
   ************************************/
         else
         {
            fprintf( stderr, "qa2tracebuf: <%s> unknown parameter in <%s>\n",
                    com, configfile );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            fprintf( stderr, "qa2tracebuf: Bad <%s> command in <%s>.\n", com,
                     configfile );
            return -1;
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss > 0 )
   {
      fprintf( stderr, "qa2tracebuf: ERROR, no " );
      if ( !init[0] ) fprintf( stderr, "<StationFile> " );
      if ( !init[1] ) fprintf( stderr, "<SamplesPerPin> " );
      if ( !init[2] ) fprintf( stderr, "<NumPins> "   );
      if ( !init[3] ) fprintf( stderr, "<BasePin> "   );
      if ( !init[4] ) fprintf( stderr, "<QAfile> "    );
      if ( !init[4] ) fprintf( stderr, "<InputDir>"   );
      if ( !init[4] ) fprintf( stderr, "<OutputDir> " );
      fprintf( stderr, "command(s) in <%s>. Exitting.\n", configfile );
      return -1;
   }
   return 0;
}


 /***********************************************************************
  *                              LogConfig()                            *
  *                                                                     *
  *                   Log the configuration parameters                  *
  ***********************************************************************/

void LogConfig( void )
{
   logit( "", "\n" );
   logit( "", "StationFile:     %s\n",    StationFile );
   logit( "", "SamplesPerPin:   %6d\n",   SamplesPerPin );
   logit( "", "NumPins:         %6d\n",   NumPins );
   logit( "", "BasePin:         %6d\n",   BasePin );
}


/*********************************************
 *              AllocateBuffers()            *
 *                                           *
 *  Returns  0 if all ok                     *
 *          -1 if error                      *
 *********************************************/

int AllocateBuffers( int *AdBufSize, int *WaveBufSize )
{
   int rc = 0;
   size_t as, ws;

   as = sizeof(WF_HEADER) + (NumPins * SamplesPerPin * sizeof(short));
   AdBuf = (char *) malloc( as );

   if ( AdBuf == NULL )
   {
      logit( "e","qa2tracebuf: Error allocating AdBuf.\n");
      rc = -1;
   }

   ws = sizeof(TRACE_HEADER) + (SamplesPerPin * sizeof(short));
   WaveBuf = (char *) malloc( ws );

   if ( WaveBuf == NULL )
   {
      logit( "e","qa2tracebuf: Error allocating WaveBuf.\n" );
      rc = -1;
   }

   *AdBufSize   = as;
   *WaveBufSize = ws;
   return rc;
}
