#
#     qa2tracebuf Configuration File
#         for .ew1 files
#
StationFile   qa2tracebuf.qsta    # File containing station name/pin# info
SamplesPerPin      100            # Number of samples per pin per A/D message
NumPins            256            # Number of pins in the A/D message
BasePin            0              # = 0 for .ew1 files; = 256 for .ew2 files!
#
InputDir  d:\ew1\    # make sure this path ends with '\' or '/'
OutputDir e:\ew1\    # make sure this path ends with '\' or '/'
#
QAfile  840424a.ew1
QAfile  850804a.ew1
QAfile  860126a.ew1
QAfile  860708a.ew1
QAfile  880610a.ew1
QAfile  890808a.ew1
QAfile  891018b.ew1
QAfile  900228a.ew1
QAfile  900418a.ew1
QAfile  900819a.ew1
QAfile  901024a.ew1
QAfile  910102a.ew1
QAfile  910817a.ew1
QAfile  910817b.ew1
QAfile  910917a.ew1
QAfile  920423a.ew1
QAfile  920425a.ew1
QAfile  920426a.ew1
QAfile  920429a.ew1
QAfile  920628a.ew1
QAfile  930111a.ew1
QAfile  930126a.ew1
QAfile  930528a.ew1
QAfile  930822a.ew1
QAfile  930823a.ew1
QAfile  931011a.ew1
QAfile  931114a.ew1
QAfile  931204b.ew1
QAfile  940103a.ew1
QAfile  940117a.ew1
QAfile  940318a.ew1
QAfile  940403a.ew1
QAfile  940428a.ew1
QAfile  940602a.ew1
QAfile  940607a.ew1
QAfile  940608a.ew1
QAfile  940609a.ew1
QAfile  940701a.ew1
