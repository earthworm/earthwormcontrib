#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Build a page of barcode labels. */

#include "barcode.h"

int main(int argc, char **argv)
{
    char *text[20];
    char line[80];
    FILE *in;
    int width, height, yoffset, xoffset, xincr;
    double yincr;
    int i, n;
    int ps = 1, pcl = 0, oflags;

    width = 140;
    height = 34;

    yoffset = 20;
    yincr = 49.3;

    xoffset = 37;
    xincr = 191;

    if (argc == 2 && !strcmp(argv[1],"-P")) {
	ps = 0; pcl = 1; argc=1;
    }
    if (argc>2) {
	fprintf(stderr, "%s: use \"%s\" for postscript or \"%s -P\" for PCL\n",
		argv[0], argv[0], argv[0]);
	exit(1);
    }
    if (pcl) {
	oflags = BARCODE_OUT_PCL;
    } else {
	oflags = BARCODE_OUT_PS | BARCODE_OUT_NOHEADERS | BARCODE_TAPE;
	printf("%%!PS-Adobe-2.0\n");
	printf("%%%%Creator: barcode sample program\n");
	printf("%%%%EndComments\n");
	printf("%%%%EndProlog\n\n");
	printf("%%%%Page: 1 1\n\n");
    }

    if ((in=fopen("aitlabel.txt","r")) == NULL) {
	fprintf (stderr, "Error - opening text file.\n");
	exit(1);
    }
    n = 0;
    while (fgets (line,80,in) != NULL) {
	line[79] = '\0';
	if (line[0] == '#') continue;
	line[strlen(line)-1] = '\0';
	if (strlen(line) > 8) {
	    fprintf (stderr, "Error: text too long\n");
	    exit(1);
	}
	if ((text[n] = malloc(9 * sizeof(char))) == NULL) {
	    fprintf (stderr, "Error: mallocing text string\n");
	    exit(1);
	}
	strcpy (text[n],line);
	if (strlen(text[n]) == 6) strcat(text[n], "AT");
	++n;
	if (n >= 45) break;
    }

#define BASESIZE 188

    for (i=0; i<n; i++) {
	int j = i/3;
	Barcode_Encode_and_Print(text[i],stdout, width, height, xoffset, (int)(yoffset + j*yincr),
				 BARCODE_39 | oflags);
	if (++i >= n) break;
	
	Barcode_Encode_and_Print(text[i],stdout, width, height, xoffset+xincr, (int)(yoffset + j*yincr),
				 BARCODE_39 | oflags);
	if (++i >= n) break;

	Barcode_Encode_and_Print(text[i],stdout, width, height, xoffset+2*xincr, (int)(yoffset + j*yincr),
				 BARCODE_39 | oflags);
    }
	
    if (pcl) {
	printf("\f");
    } else {
	printf("\nshowpage\n");
	printf("%%%%Trailer\n\n");
    }
    return 0;
}
