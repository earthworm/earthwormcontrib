/*
 *   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT USING THE COMMAND CHECKOUT.
 *
 *    $Id: sniff_trace.c 334 2007-05-04 23:36:26Z dietz $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.11  2007/05/04 23:36:26  dietz
 *     Fixed indexing bug in looping over a Module's associated groups.
 *     Added some debug logging (invoked with recompilation) and fixed
 *     a few casts in comparisons.
 *
 *     Revision 1.10  2007/04/06 18:13:09  dietz
 *     cast time_t to long for print %ld statements
 *
 *     Revision 1.9  2006/06/01 21:38:26  dietz
 *     Modified to handle wildcard in instid and modid correctly.
 *     No wildcards are allowed in the station identifiers!
 *
 *     Revision 1.8  2006/06/01 20:47:35  dietz
 *     Modified to allow comment lines between "Group" and "EndGroup" commands
 *
 *     Revision 1.7  2006/05/31 22:25:41  dietz
 *     Reorganized Group and Module structures and modified logic for tracking
 *     times of last-seen data for each Group. Changed "Member" command to
 *     "MemberModule" and added "MemberStation" command to allow tracking of
 *     individual stations in the group setting.
 *
 *     Revision 1.6  2004/08/24 22:48:57  dietz
 *     added check for null-member Groups
 *
 *     Revision 1.5  2004/08/24 22:27:32  dietz
 *     Changed "struct GROUP" to "struct MODGROUP" to avoid conflict with Windows
 *
 *     Revision 1.4  2004/08/23 23:57:37  dietz
 *     Modified to alarm on groups of modules in addition
 *     to single modules. Also changes lots of printf to logit.
 *
 *     Revision 1.3  2004/04/30 20:15:29  dietz
 *     added type conversion in tport_getmsg
 *
 *     Revision 1.2  2004/04/29 23:46:18  dietz
 *     added TYPE_TRACEBUF2 and TYPE_TRACE2_COMP_UA as acceptable msg types from modules
 *
 */

      /**********************************************************************
       *                        sniff_trace program                         *
       *                                                                    *
       *  Report to the status manager if no tracebuf or compressed trace   *
       *  msgs for a particular module id are found for some time interval. *
       **********************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <trace_buf.h>
#include <transport.h>

#define MAX_LEN    MAX_INST_STR  /* Max size of installation and module names */
#define FALSE      0
#define TRUE       1
#define FORCEBREAK 5000          /* #msgs to read before checking on group status */

/* Error messages used by sniff_trace
   **********************************/
#define ERR_MISSMSG        0     /* Message missed in transport ring */
#define ERR_TOOBIG         1     /* Retrieved message too large for buffer */
#define ERR_NOTRACK        2     /* Message retrieved; tracking limit exceeded */
#define ERR_WFGROUP_DEAD   3     /* Waveform Group is Dead */
#define ERR_WFGROUP_ALIVE  4     /* Waveform Group is Alive */
 
#define MAX_WFGROUP 10           /* max # waveform groups any mod or station can belong to */

typedef struct                   /* Structure containing info about each module */
{
   unsigned char instid;
   unsigned char mod;
   char          inst_name[MAX_LEN];
   char          mod_name[MAX_LEN];
   int           ngrp;               /* number of groups this modid belongs to          */
   int           igrp[MAX_WFGROUP];  /* index into WFGROUP list of group it belongs to  */
   time_t        t_prev;             /* When we saw the previous msg from this module   */
} MOD; 

typedef struct                       /* Structure containing info about stations */
{
   char          sta[TRACE2_STA_LEN];
   char          net[TRACE2_NET_LEN];
   int           ngrp;               /* number of groups this station belongs to      */
   int           igrp[MAX_WFGROUP];  /* index into WFGROUP list of group it belongs to  */
   time_t        t_prev;             /* When we saw the previous msg from this module */
} STA;

typedef struct
{
   char   grp_name[MAX_LEN*2];     /* name of group to use in notifications */
   int    nmember;                 /* number of members in this group */
   int    grp_alive;               /* 1 if the group is alive; 0 if dead */
   int    grp_timeout;             /* complain if entire group is dead for this many sec */
   time_t grp_t_prev;              /* When we saw the previous msg from any group member */
} WFGROUP;

/* Function prototypes
 *********************/
void GetConfig( char * );
void LogConfig( void );
int  SendStatus( unsigned char, short, char * );
int  FindModInList( MOD * );
int  FindStaInList( STA * );
int  AddGroup( void );
int  compareSTA( const void *, const void * );

/* Things to read or derive from config file
   *****************************************/
static char          RingName[MAX_LEN];  /* Name of transport ring */
static long          RingKey;            /* Key of transport ring */
static unsigned char MyModId;            /* sniff_trace's module id               */
static int           LogSwitch;          /* If 0, no logging should be done to disk */
static int           HeartBeatInterval;  /* Send heartbeats to statmgr this often (sec) */
static int           nMod   = 0;         /* Number of modules we know about */
static MOD          *Mod;                /* Module name, group list, rcv time of last data */
static int           nSta   = 0;         /* Number of stations we know about */
static STA          *Sta;                /* Station code, group list, and rcv time of last data */
static int           nGroup = 0;         /* Number of groups we know about */
static WFGROUP      *Group;              /* group name and timeout interval */
static int           Debug = 0;          /* debug flag */

/* Things to look up in the earthworm.h tables
   *******************************************/
static unsigned char LocalInstId;
static unsigned char InstWild;
static unsigned char ModWild;
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeTracebuf;
static unsigned char TypeTraceComp;
static unsigned char TypeTracebuf2;
static unsigned char TypeTraceComp2;

/* Other global variables
   **********************/
SHM_INFO region;
static pid_t myPid;


                      /**************************************
                       *          Main starts here          *
                       **************************************/

int main( int argc, char **argv )
{
   time_t    t_heart_prev;
   int       nGet;              /* number of logos in GetLogo */
   MSG_LOGO *GetLogo;           /* Array for requesting module,type,instid */
   MSG_LOGO  reclogo;           /* Logo of the message we got   */
   long      recsize;           /* Size of retrieved message    */
   static TracePacket buffer;   /* Buffer to hold event message */
   time_t    now;
   char      errmsg[80];
   int       msgcount;          /* looping count of messages read from ring */
   int       i,im;
   int       loglogo = 0;

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
        printf( "Usage: sniff_trace <configFile>\n" );
        return 0;
   }

/* Open log file
   *************/
   logit_init( argv[1], 0, 512, 1 );

/* Read configuration file
   ***********************/
   GetConfig( argv[1] );

/* Look up local installation id
   *****************************/
   if ( GetLocalInst( &LocalInstId ) != 0 )
   {
      logit( "e", "sniff_trace: Error getting local installation id. Exiting.\n" );
      return -1;
   }

/* Look up wildcards from earthworm tables
   ***************************************/
   if ( GetInst( "INST_WILDCARD", &InstWild ) != 0 )
   {
      logit( "e", "sniff_trace: Invalid installation id <INST_WILDCARD>. Exiting.\n" );
      return -1;
   }

   if ( GetModId( "MOD_WILDCARD", &ModWild ) != 0 )
   {
      logit( "e", "sniff_trace: Invalid module id <MOD_WILDCARD>. Exiting.\n" );
      return -1;
   }

/* Look up message types from earthworm tables
   *******************************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      logit( "e", "sniff_trace: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      logit( "e", "sniff_trace: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_TRACEBUF2", &TypeTracebuf2 ) != 0 )
   {
      logit( "e", "sniff_trace: Invalid message type <TYPE_TRACEBUF2>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_TRACE2_COMP_UA", &TypeTraceComp2 ) != 0 )
   {
      logit( "e", "sniff_trace: Invalid message type <TYPE_TRACE2_COMP_UA>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_TRACEBUF", &TypeTracebuf ) != 0 )
   {
      logit( "e", "sniff_trace: Invalid message type <TYPE_TRACEBUF>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_TRACE_COMP_UA", &TypeTraceComp ) != 0 )
   {
      logit( "e", "sniff_trace: Invalid message type <TYPE_TRACE_COMP_UA>. Exiting.\n" );
      return -1;
   }

/* Re-initialize log-file with configured logging level
   ****************************************************/
   logit_init( argv[1], (short) MyModId, 512, LogSwitch );
   logit( "" , "sniff_trace: Read command file <%s>\n", argv[1] );

/* Log the config file
   *******************/
   LogConfig();
 
/* Get our own pid for restart purposes
   ************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "sniff_trace: Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Attach to shared memory ring
   ****************************/
   tport_attach( &region, RingKey );

/* Allocate and fill in the GetLogo array; flush input ring
   ********************************************************/
   nGet = 4;
   GetLogo = (MSG_LOGO *)malloc( nGet*sizeof(MSG_LOGO) );
   if ( GetLogo == NULL )
   {
      logit( "e", "sniff_trace: Can't allocate GetLogo. Exiting.\n" );
      return -1;
   }
   GetLogo[0].instid = InstWild;
   GetLogo[0].mod    = ModWild;
   GetLogo[0].type   = TypeTracebuf2;

   GetLogo[1].instid = InstWild;
   GetLogo[1].mod    = ModWild;
   GetLogo[1].type   = TypeTraceComp2; 

   GetLogo[2].instid = InstWild;
   GetLogo[2].mod    = ModWild;
   GetLogo[2].type   = TypeTracebuf;

   GetLogo[3].instid = InstWild;
   GetLogo[3].mod    = ModWild;
   GetLogo[3].type   = TypeTraceComp; 

   while( tport_getmsg( &region, GetLogo, (short)nGet, &reclogo, &recsize, 
                        buffer.msg, MAX_TRACEBUF_SIZ ) != GET_NONE );

/* Send the first heartbeat
   ************************/
   time( &t_heart_prev );

   if ( SendStatus( TypeHeartBeat, 0, "" ) != PUT_OK )
   {
      logit( "t", "sniff_trace:  Error sending heartbeat to ring. Exiting.\n");
      return -1;
   }

/* Loop until kill flag is set
   ***************************/
   while ( 1 )
   {
      int    res;
      time_t t_heart;

/* Check kill flag
   ***************/
      if ( tport_getflag( &region ) == TERMINATE  ||
           tport_getflag( &region ) == myPid )
      {
         tport_detach( &region );
         logit( "et", "sniff_trace: Termination requested. Exiting.\n" );
         free( GetLogo );
         return 0;
      }

/* Send heartbeat to statmgr
   *************************/
      time( &t_heart );
      if ( t_heart - t_heart_prev > (time_t)HeartBeatInterval )
      {
         t_heart_prev = t_heart;

         if ( SendStatus( TypeHeartBeat, 0, "" ) != PUT_OK ) {
            logit( "et", "sniff_trace:  Error sending heartbeat to ring.\n");
         }
      }

/* Get trace data messages from the ring
   *************************************/
      msgcount = 0;
      do
      {
         STA                keysta;      /* Key for looking up stations  */
         STA               *sta;         /* pointer to station in list   */  

         res = tport_getmsg( &region, GetLogo, (short)nGet, &reclogo, &recsize, 
                             buffer.msg, MAX_TRACEBUF_SIZ );

         if ( res == GET_NONE )                /* No more new messages */ 
              break;   

         else if ( res == GET_TOOBIG )         /* Message was too big */
         {                                     /* Complain and try again */
            sprintf( errmsg, "Retrieved msg[%ld] (i%u m%u t%u) too big for buffer[%d]",
                    recsize, reclogo.instid, reclogo.mod, reclogo.type,
                    MAX_TRACEBUF_SIZ );
            logit( "et", "sniff_trace: %s\n", errmsg );
            SendStatus( TypeError, ERR_TOOBIG, errmsg );
            continue;
         }

         else if ( res == GET_MISS )           /* Got a message, but missed some */
         {
            sprintf( errmsg, "Missed msg(s)  i%u m%u t%u  %s.",
                     reclogo.instid, reclogo.mod, reclogo.type, RingName );
            logit( "et", "sniff_trace: %s\n", errmsg );
            SendStatus( TypeError, ERR_MISSMSG, errmsg );
         }

         else if ( res == GET_NOTRACK )       /* Got a message, but can't tell if */
         {                                    /* any were missed */ 
            sprintf( errmsg, "Msg received (i%u m%u t%u); NTRACK_GET exceeded",
                      reclogo.instid, reclogo.mod, reclogo.type );
            logit( "et", "sniff_trace: %s\n", errmsg );
            SendStatus( TypeError, ERR_NOTRACK, errmsg );
         }  
         if( Debug && loglogo<2000 ) {  
            logit("","\nrcvd i%d m%d t%d :",  
                  (int)reclogo.instid, (int)reclogo.mod, (int)reclogo.type ); 
         }    
         msgcount++; 

      /* Update t_prev for all Mod,Sta,Group this message belongs to
       *************************************************************/
         time(&now);

      /* Module list (wildcards allowed) and associated Groups */
         for( im=0; im<nMod; im++ ) 
         {
            if( Mod[im].instid != InstWild   && 
                Mod[im].instid != reclogo.instid ) continue;
            if( Mod[im].mod    != ModWild    && 
                Mod[im].mod    != reclogo.mod    ) continue;
         /* found a match in Mod list*/
            Mod[im].t_prev = now;
            for( i=0; i<Mod[im].ngrp; i++ ) 
            {   
               Group[Mod[im].igrp[i]].grp_t_prev = now;
               if( Debug )  {
                  if( loglogo<2000 ) logit(""," ig%d", Mod[im].igrp[i] ); 
                  loglogo++;
               }
            }
         }

      /* Station list (no wildcards allowed) and associated Groups */
         strcpy( keysta.sta, buffer.trh2.sta );
         strcpy( keysta.net, buffer.trh2.net );    
         sta = (STA *) bsearch( &keysta, Sta, nSta, sizeof(STA), compareSTA );
         if( sta != NULL ) /* found a match in Sta list*/
         {
            sta->t_prev = now;
            for( i=0; i<sta->ngrp; i++ ) Group[sta->igrp[i]].grp_t_prev = now;
         }
      
      } while( msgcount < FORCEBREAK );   /* force a break after so many messages */
                                          /* if a GET_NONE hasn't been returned   */


   /* See if any groups have died or been reborn
      ******************************************/
      for( i=0; i<nGroup; i++ )
      {
         int isalive;  /* is this group currently alive? */

         if( time(&now)-Group[i].grp_t_prev < (time_t)Group[i].grp_timeout ) isalive = TRUE;
         else                                                                isalive = FALSE;
 
         if( Group[i].grp_alive == TRUE )  /* Group was alive last time around */
         {
            if ( isalive == FALSE )        /* Complain if the group is now dead! */
            {
               if( Group[i].grp_timeout >= 60 ) {
                  sprintf( errmsg, "No data from %s in %.0f min",
                          Group[i].grp_name, (float)Group[i].grp_timeout/60. );
               } else {
                  sprintf( errmsg, "No data from %s in %d sec",
                          Group[i].grp_name, Group[i].grp_timeout );
               }
               logit( "et", "sniff_trace: %s\n", errmsg );
               if ( SendStatus( TypeError, ERR_WFGROUP_DEAD, errmsg ) != PUT_OK ) {
                  logit( "et", "sniff_trace: Error sending error to ring.\n");
               }
               Group[i].grp_alive = FALSE;
            }
         }
         else                              /* Group was dead last time around */
         {
            if ( isalive == TRUE )         /* Announce if the group is reborn */
            {
               sprintf( errmsg, "Data received from %s", Group[i].grp_name );
               logit( "et", "sniff_trace: %s\n", errmsg );
               if ( SendStatus( TypeError, ERR_WFGROUP_ALIVE, errmsg ) != PUT_OK ) {
                  logit( "et", "sniff_trace: Error sending error to ring.\n");
               }
               Group[i].grp_alive = TRUE;
            }
         }
      }
      if( msgcount != FORCEBREAK ) sleep_ew( 200 );
   }
}


         /**************************************************
          *                   SendStatus                   *
          *  Builds heartbeat or error msg and puts it in  *
          *  shared memory.                                *
          **************************************************/

int SendStatus( unsigned char msg_type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   int         res;
   long        size;
   time_t      t;

   logo.instid = LocalInstId;
   logo.mod    = MyModId;
   logo.type   = msg_type;

   time( &t );

   if( msg_type == TypeHeartBeat )
      sprintf ( msg, "%ld %d\n", (long)t, myPid );
   else if( msg_type == TypeError )
      sprintf ( msg, "%ld %d %s\n", (long)t, ierr, note);

   size = strlen( msg );           /* don't include null byte in message */
   res = tport_putmsg( &region, &logo, size, msg );

   return res;
}


/***********************************************************************
 *     GetConfig()  Processes command file using kom.c functions.      *
 *                  Exits if any errors are encountered.               *
 ***********************************************************************/

void GetConfig( char *configFile )
{
   int    ncommand;     /* # of required commands you expect to process */
   char   init[10];     /* Flags, one for each required command */
   int    nmiss;        /* Number of required commands that were missed */
   char  *com;
   char  *str;
   int    nfiles;
   int    success;
   int    i;
   time_t now = time(&now);

/* Set to zero one init flag for each required command
 *****************************************************/
   ncommand = 5;
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configFile );
   if ( nfiles == 0 )
   {
      logit("e", "sniff_trace: Error opening command file <%s>. Exiting.\n",
               configFile );
      exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )         /* While there are command files open */
   {
      while ( k_rd() )          /* Read next line from active file  */
      {
         com = k_str();         /* Get the first token from line */

      /* Ignore blank lines & comments
       *******************************/
         if ( !com )          continue;
         if ( com[0] == '#' ) continue;

      /* Open a nested configuration file
       **********************************/
         if ( com[0] == '@' )
         {
            success = nfiles+1;
            nfiles  = k_open(&com[1]);
            if ( nfiles != success )
            {
               logit( "e", "sniff_trace: Error opening command file <%s>."
                      " Exiting.\n", &com[1] );
               exit( -1 );
            }
            continue;
         }

      /* Read module id for this program
       *********************************/
/*0*/    if ( k_its( "MyModuleId" ) )
         {
             str = k_str();
             if ( str )
             {
                if ( GetModId( str, &MyModId ) < 0 )
                {
                   logit( "e", "sniff_trace: Invalid MyModuleId <%s> in <%s>."
                          " Exiting.\n", str, configFile );
                   exit( -1 );
                }
             }
             init[0] = 1;
         }

      /* Name of transport ring to write to
       ************************************/
/*1*/    else if( k_its( "RingName" ) )
         {
             str = k_str();
             if ( str )
             {
                strcpy( RingName, str );
                if ( ( RingKey = GetKey(str) ) == -1 )
                {
                   logit( "e", "sniff_trace: Invalid RingName <%s> in <%s>."
                          " Exiting.\n", RingName, configFile );
                   exit( -1 );
                }
             }
             init[1] = 1;
         }

      /* Set Logfile switch
       ********************/
/*2*/    else if( k_its( "LogFile" ) )
         {
             LogSwitch = k_int();
             init[2] = 1;
         }

      /* Set heartbeat interval
       ************************/
/*3*/    else if( k_its( "HeartBeatInterval" ) )
         {
             HeartBeatInterval = k_int();
             init[3] = 1;
         }

      /* Get the names of the modules to test
       *************************************/
/*4*/    else if ( k_its("Module") )
         {
            MOD  this;  /* local copy of Module inst/mod */
            int  im;    /* index into module list */
            int  ig;    /* index into group list  */
            memset( &this, 0, sizeof(MOD) );

         /* Read installation id */
            str = k_str();   
            if ( str && strlen(str)<MAX_LEN )
               strcpy( (char *)this.inst_name, str );
            else  
            {   
               logit( "e", "sniff_trace: Module: Can't get installation id."
                      " Exiting.\n" );
               exit( -1 );
            }

         /* Read module id */
            str = k_str();  
            if ( str && strlen(str)<MAX_LEN  )
               strcpy( (char *)this.mod_name, str );
            else  
            {   
               logit( "e", "sniff_trace: Module: Can't get module name. Exiting.\n" );
               exit( -1 );
            }

         /* Find/add this module in module list */
            im = FindModInList( &this );

         /* Set up a Group with one member */
            ig = AddGroup();
            sprintf( Group[ig].grp_name, "%s %s", 
                     Mod[im].inst_name, Mod[im].mod_name );
            Group[ig].grp_timeout = k_int();    /* read from command line */
            Group[ig].grp_t_prev  = now;        /* assume we just saw data from it */
            Group[ig].grp_alive   = TRUE;       /* assume it's alive */
            Group[ig].nmember     = 1;

            Mod[im].igrp[Mod[im].ngrp] = ig;
            Mod[im].ngrp++;
            Mod[im].t_prev = now;               /* assume we just saw data from it */

            init[4] = 1;
         }

      /* Get the names of the groups to test
       *************************************/
/*4*/    else if ( k_its("Group") )
         {
            int ig = AddGroup();

            str = k_str();
            if ( str  &&  strlen(str)<MAX_LEN*2 )
               strcpy( (char *)Group[ig].grp_name, str );
            else  
            {   
               logit( "e", "sniff_trace: Group: Can't get group name. Exiting.\n" );
               exit( -1 );
            }
            Group[ig].grp_timeout = k_int();    /* read from command line */
            Group[ig].grp_t_prev  = now;        /* assume we just saw data from it */
            Group[ig].grp_alive   = TRUE;       /* assume it's alive */
            
            while( k_rd() )
            {
               com = k_str();
               if( !com )          continue;    /* ignore blank lines */
               if( com[0] == '#' ) continue;    /* ignore comments    */

            /* Set up an Instid/Modid Member 
             *******************************/
               if( k_its( "MemberModule" ) )   
               {
                  MOD this;
                  int im;

                  memset( &this, 0, sizeof(MOD) );

               /* Read installation id */
                  str = k_str();   
                  if ( str && strlen(str)<MAX_LEN )
                     strcpy( (char *)this.inst_name, str );
                  else  
                  {   
                     logit( "e", "sniff_trace: MemberModule: Can't get installation id."
                            " Exiting.\n" );
                     exit( -1 );
                  }

               /* Read module id */
                  str = k_str();  
                  if ( str && strlen(str)<MAX_LEN  )
                     strcpy( (char *)this.mod_name, str );
                  else  
                  {   
                     logit( "e", "sniff_trace: MemberModule: Can't get module name."
                            " Exiting.\n" );
                     exit( -1 );
                  }

               /* Find/add this module in module list and current group */
                  im = FindModInList( &this );
                  Mod[im].igrp[Mod[im].ngrp] = ig;
                  Mod[im].ngrp++;
                  Mod[im].t_prev = now;            /* assume we just saw data from it */
                  Group[ig].nmember++;
               }

            /* Set up a Site/Net (seismic station) Member 
             ********************************************/
               else if( k_its( "MemberStation" ) )
               {
                  STA this;
                  int is;

                  memset( &this, 0, sizeof(STA) );

               /* Read station code */
                  str = k_str();   
                  if ( str && strlen(str)<TRACE2_STA_LEN )
                     strcpy( (char *)this.sta, str );
                  else  
                  {   
                     logit( "e", "sniff_trace: MemberStation: Can't get station code."
                            " Exiting.\n" );
                     exit( -1 );
                  }

               /* Read network code */
                  str = k_str();  
                  if ( str && strlen(str)<TRACE2_NET_LEN  )
                     strcpy( (char *)this.net, str );
                  else  
                  {   
                     logit( "e", "sniff_trace: MemberStation: Can't get network code."
                            " Exiting.\n" );
                     exit( -1 );
                  }

               /* Find/add this station in station list and note the current group */
                  is = FindStaInList( &this );
                  Sta[is].igrp[Sta[is].ngrp] = ig;
                  Sta[is].ngrp++;
                  Sta[is].t_prev = now;            /* assume we just saw data from it */
                  Group[ig].nmember++;
               }

            /* Note the end of the group
             ***************************/
               else if( k_its( "EndGroup" ) ) break;
               else
               {
                  logit( "e", "sniff_trace: Unexpected command <%s> after <Group> "
                          "command. Exiting.\n", com );
                  exit( -1 );
               }
            }           
            init[4] = 1;
         }

         else
         {
             logit( "e", "sniff_trace: <%s> unknown command in <%s>.\n",
                     com, configFile );
             continue;
         }

      /* See if there were any errors processing the command
       *****************************************************/
         if ( k_err() )
         {
            logit( "e", "sniff_trace: Bad <%s> command in <%s>; \n",
                     com, configFile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] ) nmiss++;

   if ( nmiss )
   {
       logit( "e", "sniff_trace: ERROR, no " );
       if ( !init[0] ) logit( "e", "<MyModuleId> " );
       if ( !init[1] ) logit( "e", "<RingName> "   );
       if ( !init[2] ) logit( "e", "<LogFile> "    );
       if ( !init[3] ) logit( "e", "<HeartBeatInterval> " );
       if ( !init[4] ) logit( "e", "<Module/Group> " );
       logit( "e", "command(s) in <%s>. Exiting.\n", configFile );
       exit( -1 );
   }

/* Further Initialize the Mod structures
 ***************************************/
   for ( i = 0; i < nMod; i++ )
   {
      if ( GetInst( Mod[i].inst_name, &Mod[i].instid ) != 0 )
      {
         logit( "e", "sniff_trace: Error getting installation id: %s. Exiting.\n",
                 Mod[i].inst_name );
         exit( -1 );
      }  
      if ( GetModId( Mod[i].mod_name, &Mod[i].mod ) != 0 )
      {
         logit( "e", "sniff_trace: Error getting module id: %s. Exiting.\n",
                 Mod[i].mod_name );
         exit( -1 );
      }
   }

/* Do sanity check on group settings
 ***********************************/
   for ( i = 0; i < nGroup; i++ )
   {
      if( Group[i].nmember == 0 )
      {
         logit( "e", "sniff_trace: Error: Group %s has no Members. Exiting.\n",
                Group[i].grp_name );
         exit( -1 );
      }
      if( Group[i].grp_timeout <= 0 )
      {
         logit( "e", "sniff_trace: Invalid timeout (%d) for Group %s. Exiting.\n",
                Group[i].grp_timeout, Group[i].grp_name );
         exit( -1 );
      }
   }

/* qsort the Sta list for quicker lookup 
 ***************************************/
   qsort( Sta, nSta, sizeof(STA), compareSTA );

   return;
}


/***********************************************************************
 *        LogConfig()  Log the configuration file parameters.          *
 ***********************************************************************/

void LogConfig( void )
{
   int i, ig, im, is;

   logit( "", "MyModId:           %u\n", MyModId );
   logit( "", "RingName:          %s\n", RingName );
   logit( "", "HeartBeatInterval: %d\n", HeartBeatInterval );

   logit( "", "\nError messages will be sent to statmgr for these groups of channels\n" );
   logit( "", "if no trace data is received in their configured timeout interval:\n" );
   for( ig = 0; ig < nGroup; ig++ )
   {
      logit( "", "\nGroupName:   %s", Group[ig].grp_name );
      if(Debug) logit("", "  (ig%d)", ig );
      logit( "", "\n  timeout:   %d sec", Group[ig].grp_timeout );

      for( im=0; im<nMod; im++ )  /* loop thru Mods; list ones belonging to this group */
      {
         for( i=0; i<Mod[im].ngrp; i++ ) {
            if( Mod[im].igrp[i] == ig ) {
               logit( "", "\n   module:   %s  %s",
                     Mod[im].inst_name, Mod[im].mod_name );
               if(Debug) logit("","  (i%d m%d)",
                              (int)Mod[im].instid, (int)Mod[im].mod );
            }
         }
      }
      for( is=0; is<nSta; is++ )  /* loop thru Stas; list ones belonging to this group */
      {
         for( i=0; i<Sta[is].ngrp; i++ ) {
            if( Sta[is].igrp[i] == ig ) {
               logit( "", "\n  station:   %s %s",
                     Sta[is].sta, Sta[is].net );
            }
         }
      }
      logit("","\n");
   }

   if( Debug ) 
   {
      for( im=0; im<nMod; im++ )  /* loop thru Mods; list groups it belongs to */
      {
         logit( "", "\n Module: %s  %s (i%d m%d)  ngrp:%d ",
                Mod[im].inst_name, Mod[im].mod_name, 
                (int)Mod[im].instid, (int)Mod[im].mod, Mod[im].ngrp );
         for( i=0; i<Mod[im].ngrp; i++ ) {
            logit( "", " ig%d", Mod[im].igrp[i] );
         }
      }
   }

   logit( "", "\n" );
   return;
}

/***********************************************************************
 * FindModInList() return index on module in list; add to list if new  *
 ***********************************************************************/
int FindModInList( MOD *this )
{
   int    i;
   size_t size;
   MOD   *tmp;

/* See if this module is already in the list */
   for( i=0; i<nMod; i++ )
   {
      if( strcmp(Mod[i].inst_name, this->inst_name)!=0 ) continue;
      if( strcmp(Mod[i].mod_name,  this->mod_name) !=0 ) continue;
      return( i );  /* found in list */
   }

/* It's new module; realloc and add it to the list */
   size = (nMod+1)*sizeof( MOD );
   tmp  = (MOD *) realloc( Mod, size );
   if( tmp == NULL )
   {
      logit("et",
            "sniff_trace: Error reallocing module list to %d bytes. Exiting.\n",
             (int)size );
      exit( -1 );
   }
   Mod = tmp;
   memcpy( &(Mod[nMod]), this, sizeof(MOD) );
   nMod++;

   return( i );
}

/***********************************************************************
 * FindStaInList() return index on station in list; add to list if new *
 ***********************************************************************/
int FindStaInList( STA *this )
{
   int    i;
   size_t size;
   STA   *tmp;

/* See if this module is already in the list */
   for( i=0; i<nSta; i++ )
   {
      if( strcmp(Sta[i].sta, this->sta) !=0 ) continue;
      if( strcmp(Sta[i].net, this->net) !=0 ) continue;
      return( i );  /* found in list */
   }

/* It's new module; realloc and add it to the list */
   size = (nSta+1)*sizeof( STA );
   tmp  = (STA *) realloc( Sta, size );
   if( tmp == NULL )
   {
      logit("et",
            "sniff_trace: Error reallocing station list to %d bytes. Exiting.\n",
             (int)size );
      exit( -1 );
   }
   Sta = tmp;
   memcpy( &(Sta[nSta]), this, sizeof(STA) );
   nSta++;

   return( i );
}


/***********************************************************************
 * AddGroup() realloc group list and return index of new element       *
 ***********************************************************************/
int AddGroup( void )
{
   int      ig;
   size_t   size;
   WFGROUP *tmp;

   ig   = nGroup;
   size = (nGroup+1)*sizeof( WFGROUP );
   tmp  = (WFGROUP *) realloc( Group, size );
   if( tmp == NULL )
   {
      logit("et",
            "sniff_trace: Error reallocing group list to %d bytes. Exiting.\n",
             (int)size );
      exit( -1 );
   }
   Group = tmp;
   memset( &(Group[nGroup]), 0, sizeof(WFGROUP) );
   nGroup++;

   return( ig );
}

/*************************************************************
 *  compareSTA()                                             *
 *  This function is passed to qsort() and bsearch().        *
 *  We use qsort() to sort a station list by sta/net codes,  *
 *  and we use bsearch to look up a sta/net in the list.     *
 *************************************************************/
int compareSTA( const void *s1, const void *s2 )
{
   int rc;
   STA *t1 = (STA *) s1;
   STA *t2 = (STA *) s2;

   rc = strcmp( t1->sta, t2->sta );
   if ( rc != 0 ) return rc;
   rc = strcmp( t1->net, t2->net );
   return rc;
}

