#
#                  sniff_trace's Configuration File
#
MyModuleId  MOD_SNIFF_TRACE   # module id for this program,
RingName          WAVE_RING   # transport ring to write to,
LogFile                   1   # If 0, don't write logfile at all,
HeartBeatInterval        30   # Send heartbeats to statmgr this often (sec)

# Watch for any of these message types:
# TYPE_TRACEBUF2, TYPE_TRACE2_COMP_UA, TYPE_TRACEBUF, TYPE_TRACE_COMP_UA
# Complain if none of these messages are found for the following modules
# or groups of stations/modules within the configured timeout periods.
#
# An individual module can be listed with the command:
#   Module <installation_id> <module_id> <timeout(sec)>
#
# Modules and/or Stations can be grouped together for alarm with 
# the following series of commands. 
#   Group <group name> <timeout(sec)>
#    MemberModule   <inst_id1>  <module_id1>
#    MemberModule   <inst_id2>  <module_id2>
#    MemberModule   <inst_id2>  <module_id3>
#    MemberStation  <sta_code1> <net_code1>
#    MemberStation  <sta_code2> <net_code1>
#    MemberStation  <sta_code3> <net_code2>
#   EndGroup
# The "group name" may contain spaces if enclosed in double quotes.
# Any number of MemberModule and/or MemberStation commands may be 
# sandwiched between each Group and EndGroup pair.  Wildcards are
# allowed in MemberModule commands, but NOT in MemberStation commands.
# A "dead message" will be issued for the Group only after ALL members 
# of the Group have been silent for timeout sec.
# An "alive message" will be issued for the Group after data is
# received from ANY SINGLE member of the Group.
#
# The "Module" command sets up a Group with 1 MemberModule.
#
# Each module or station can belong to up to 10 groups.
#
#      Installation     Module       Timeout
#          name          name         (sec)
#      ------------     ------       -------
Module  INST_MENLO   MOD_ADSEND_A      300
Module  INST_MENLO   MOD_ADSEND_B      300

Group   "FrameRelay Nodes"             600
 MemberModule  INST_MENLO  MOD_ADSEND_MMTH
 MemberModule  INST_MENLO  MOD_ADSEND_CH
EndGroup

Group    Digital_Microwave_Nodes      3600
 MemberModule  INST_MENLO  MOD_ADSEND_GP
 MemberModule  INST_MENLO  MOD_ADSEND_VOLLMER
 MemberModule  INST_MENLO  MOD_ADSEND_TAM
EndGroup

Group  "DSL_stas on ATM1"             900
 MemberStation  1776 NP
 MemberStation  1777 NP
 MemberStation  1778 NP
 MemberStation  1780 NP
 MemberStation   CDO NC
EndGroup
