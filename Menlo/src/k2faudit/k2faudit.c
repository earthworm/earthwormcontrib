/* THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT.
 *
 *    $Id: k2faudit.c 275 2007-01-16 21:56:04Z dietz $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.2  2007/01/16 21:56:04  dietz
 *     got rid of Windows-esque end-of-lines.
 *
 */

/*
 * k2faudit.c:
 *
 * Periodically check a specified directory for .evt files.
 * Reads the files, checks them for header changes,
 * and e-mails notifications.
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <errno.h>
#include "sm_file2ew.h"

/* Functions in this source file 
 *******************************/
void config_me ( char * );
void sm_file2ew_lookup ( void );
void sm_file2ew_status ( unsigned char, short, char * );

SHM_INFO  Region;             /* shared memory region to use for i/o    */
MSG_LOGO  SMlogo;              /* outgoing msg logo: module,type,instid  */
pid_t     myPid;              /* for restarts by startstop              */

static char MsgBuf[BUFLEN];   /* char string to hold output message     */

int NchanNames=0;             /* number of names in this config file     */
CHANNELNAME ChannelName[MAXCHAN];  /* table of box/channel to SCNL       */

static char *TroubleSubdir = "trouble";   /* subdir for problem files    */
static char *SaveSubdir = "save";         /* subdir for processed files  */

/* Things to read or derive from configuration file
 **************************************************/
static char     RingName[20];        /* name of transport ring for i/o    */
static char     MyModName[50];       /* speak as this module name/id      */
static int      LogSwitch;           /* 0 if no logfile should be written */
static int      HeartBeatInterval;   /* seconds betweeen beats to statmgr */
static char     GetFromDir[NAM_LEN]; /* directory to monitor for data     */
       char     ArchiveDir[NAM_LEN]; /* directory to store known headers  */
       char     HeaderDir[NAM_LEN]; /* directory to store known headers  */
static unsigned CheckPeriod;         /* secs between looking for new files*/
static int      OpenTries;
static int      OpenWait;
static int      SaveDataFiles;       /* if non-zero, move to SaveSubdir,  */ 
                                     /*           0, remove files         */
static char     PeerHeartBeatFile[NAM_LEN]; /* name of heartbeat file     */
static int      PeerHeartBeatInterval; /* seconds between heartbeat files */
       int      Debug;               /* non-zero -> debug logging         */
       int      nPager;
       char     person[MAX_PAGERS][50];
       char     manager[50];

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o     */
static unsigned char InstId;        /* local installation id             */
static unsigned char MyModId;       /* Module Id for this program        */
static unsigned char TypeHeartBeat; 
static unsigned char TypeError;
static unsigned char TypeSM;
static unsigned char TypePage;


/* Error messages used by sm_file2ew 
 ***********************************/
#define ERR_CONVERT       0   /* trouble converting a file  */
#define ERR_PEER_LOST     1   /* no peer heartbeat file for a while */
#define ERR_PEER_RESUME   2   /* got a peer heartbeat file again    */

#define TEXT_LEN NAM_LEN*3
static char Text[TEXT_LEN];     /* string for log/error messages      */
static char ProgName[NAM_LEN];  /* program name for logging purposes  */

/* File handling stuff
**********************/
int main( int argc, char **argv )
{
   int         i, ret, read_error = 0;
   char      fname[100], fnew[155];
   char     *c;
   FILE     *fp;
   time_t    tnextbeat;    /* next time for local heartbeat  */
   time_t    tnextpeer;    /* next time for peer's heartbeat */
   time_t    tnow;         /* current time */
   int       peerstatus;   /* current status of peer */


/* Check command line arguments 
 ******************************/
   if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", argv[0] );
        exit( 0 );
   }
   strcpy( ProgName, argv[1] );
   c = strchr( ProgName, '.' );
   if( c ) *c = '\0';
           
/* Read the configuration file(s)
 ********************************/
   config_me( argv[1] );
   
/* Look up important info from earthworm.h tables
 ************************************************/
   sm_file2ew_lookup();
   
/* Set the outgoing logo fields
 ******************************/
   SMlogo.instid = InstId;
   SMlogo.mod    = MyModId;
   SMlogo.type   = TypeSM;
 
/* Initialize name of log-file & open it 
 ***************************************/
   logit_init( argv[1], (short) MyModId, TEXT_LEN*2, LogSwitch );
   logit( "" , "%s: Read command file <%s>\n", argv[0], argv[1] );

/* Get process ID for heartbeat messages 
 ***************************************/
   myPid = getpid();
   if( myPid == -1 ) {
     logit("e","%s: Cannot get pid; exiting!\n", ProgName);
     exit (-1);
   }

/* Change to the directory with the input files
 ***********************************************/
   if( chdir_ew( GetFromDir ) == -1 ) {
      logit( "e", "%s: GetFromDir directory <%s> not found; "
                 "exiting!\n", ProgName, GetFromDir );
      exit(-1);
   }
   if(Debug)logit("et","%s: changed to directory <%s>\n", ProgName,GetFromDir);

/* Make sure trouble subdirectory exists
 ***************************************/
   if( CreateDir( TroubleSubdir ) != EW_SUCCESS ) {
      logit( "e", "%s: trouble creating trouble directory: %s/%s\n",
              ProgName, GetFromDir, TroubleSubdir ); 
      return( -1 );
   }

/* Make sure save subdirectory exists (if it will be used)
 *********************************************************/
   if( SaveDataFiles ) {
      if( CreateDir( SaveSubdir ) != EW_SUCCESS ) {
         logit( "e", "%s: trouble creating save directory: %s/%s\n",
                ProgName, GetFromDir, SaveSubdir ); 
      return( -1 );
      }
   }

/* Attach to Output shared memory ring 
 *************************************/
   tport_attach( &Region, RingKey );
   logit( "", "%s: Attached to public memory region %s: %d\n", 
          ProgName, RingName, RingKey );

/* Force local heartbeat first time thru main loop
   but give peers the full interval before we expect a file
 **********************************************************/
   tnextbeat  = time(NULL) - 1;
   tnextpeer  = time(NULL) + PeerHeartBeatInterval;
   peerstatus = ERR_PEER_RESUME; 


/****************  top of working loop ********************************/
   while(1)  {
     /* Check on heartbeats
      *********************/
        tnow = time(NULL);
        if( tnow >= tnextbeat ) {  /* time to beat local heart */
           sm_file2ew_status( TypeHeartBeat, 0, "" );
           tnextbeat = tnow + HeartBeatInterval;
        }
        if( PeerHeartBeatInterval &&  tnow > tnextpeer ) {  /* peer is late! */
           if( peerstatus == ERR_PEER_RESUME ) {  /* complain! */
              sprintf(Text,"No PeerHeartBeatFile in %s for over %d sec!",
                      GetFromDir, PeerHeartBeatInterval );
              /*
              sm_file2ew_status( TypeError, ERR_PEER_LOST, Text );
              */
              peerstatus = ERR_PEER_LOST;            
           }
        }

     /* See if termination has been requested 
      ****************************************/
    if( tport_getflag( &Region ) == TERMINATE ) {
           logit( "t", "%s: Termination requested; exiting!\n", ProgName );
           break;
        }

     /* Get a file name
      ******************/    
    ret = GetFileName( fname );
    
    sleep_ew( 1*1000 );
    /**/
    if( ret == 1 ) {  /* No files found; wait for one to appear */
           sleep_ew( CheckPeriod*1000 ); 
       continue;
    }
    /**/
        if(Debug)logit("e","\n%s: got file name <%s>\n",ProgName,fname);

     /* Open the file.
      ******************/
     /* We open for write, as that will hopefully get us an exclusive open. 
      * We don't ever want to look at a file that's being written to. 
      */
    for(i=0;i<OpenTries;i++) {
           fp = fopen( fname, "rb" );
           if ( fp != NULL ) goto itopend;
           sleep_ew(OpenWait);
    }
    logit("et","%s: Error: Could not open %s after %d*%d ms\n",
               ProgName, OpenTries,OpenWait);
    itopend:    
    if(i>0) logit("t","Warning: %d attempts required to open file %s\n",
                i,fname);

     /* If it's a heartbeat file, reset tnextpeer and delete the file
      ****************************************************************/
     /* We're not decoding the contents of the file.
      * The idea is that we don't care when the heartbeat was created, 
      * only when we saw it. This prevents 'heartbeats from the past' 
      * from confusing things, but may not be wise... 
      */
        if( strcmp(fname,PeerHeartBeatFile)==0 ) {
            tnextpeer = time(NULL) + PeerHeartBeatInterval;

            if( peerstatus == ERR_PEER_LOST ) {  /* announce resumption */
                sprintf(Text,"Received PeerHeartBeatFile in %s (Peer is alive)",
                        GetFromDir );
                sm_file2ew_status( TypeError, ERR_PEER_RESUME, Text );
                peerstatus = ERR_PEER_RESUME;            
            }
            fclose( fp );
            if( remove( fname ) != 0) {
                logit("et",
                    "%s: Cannot delete heartbeat file <%s>;"
                    " exiting!", ProgName, fname );
                break;
            }
            continue;
        }

        else if( strcmp(fname,"core")==0 ) {
            fclose( fp );
            if( remove( fname ) != 0) {
                logit("et",
                    "%s: Cannot delete core file <%s>;"
                    " exiting!", ProgName, fname );
                break;
            }
            continue;
        }

     /* Pass files to the auditor 
        *************************/
        if(Debug)logit("e","%s: Reading %s \n", ProgName, fname );
        
        ret = nsmp2ew( fp, fname );
        fclose( fp );

     /* Everything went fine...
      *************************/
        if( ret >= 0 ) {  
               
            /* Keep file around */
            if( ret > 0 && SaveDataFiles ) { 
                sprintf(fnew,"%s/%s",SaveSubdir,fname );
                if(Debug)logit("et","%s: Saving %s as \n %s \n", ProgName, fname, fnew );
                if( rename( fname, fnew ) != 0 ) {
                    logit( "e", "error moving file to ./%s\n; exiting!", 
                        fnew );
                    break;
                } else {
                /*  if(Debug)logit("e","moved to ./%s\n", SaveSubdir );   */
                }
            }

            /* Delete the file */
            else { 
                if(Debug)logit("et","%s: Removing %s \n", ProgName, fname );
                if( remove( fname ) != 0 ) {
                    logit("e","error deleting file %s; exiting!\n", fname);
                    break;
                } else  {
                /*  logit("e","deleted file.\n");   */
                }
            }
        }

    /* ...or there was trouble! 
     **************************/
        else { 
            logit("e","\n");
            if(Debug)logit("et","%s: Trouble processing: %s \n", ProgName, fname );
            sprintf( Text,"Trouble processing: %s ;", fname );
            sm_file2ew_status( TypeError, ERR_CONVERT, Text );
            sprintf(fnew,"%s/%s",TroubleSubdir,fname );
            if( rename( fname, fnew ) != 0 ) {
                logit( "e", " error moving file to ./%s ; exiting!\n", fnew );
                break;
            } else {
                logit( "e", " moved to ./%s\n", fnew );
            }
        }

   } /* end of while */

/************************ end of working loop ****************************/
    
/* detach from shared memory */
   tport_detach( &Region ); 

/* write a termination msg to log file */
   fflush( stdout );
   return( 0 );

}  
/************************* end of main ***********************************/


/******************************************************************************
 *  config_me() processes command file(s) using kom.c functions;              *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
#define ncommand 14
void config_me( char *configfile )
{
   char     init[ncommand]; /* init flags, one byte for each required command */
   int      nmiss;          /* number of required commands that were missed   */
   char    *com;
   char    *str;
   int      i, n, nfiles, success;
   
/* Set to zero one init flag for each required command 
 *****************************************************/   
   for( i=0; i<ncommand; i++ )  init[i] = 0;
   nPager = 0;

/* Open the main configuration file 
 **********************************/
   nfiles = k_open( configfile ); 
   if ( nfiles == 0 ) {
        fprintf( stderr,
                "%s: Error opening command file <%s>; exiting!\n", 
                 ProgName, configfile );
        exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0) {  /* While there are command files open */
        while(k_rd()) {       /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr, 
                          "%s: Error opening command file <%s>; exiting!\n",
                           ProgName, &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command 
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("GetFromDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( GetFromDir, str, NAM_LEN );
                init[3] = 1;
            }
  /*4*/     else if( k_its("CheckPeriod") ) {
                CheckPeriod = k_int();
                init[4] = 1;
            }
  /*5*/     else if( k_its("Debug") ) {
                Debug = k_int();
                init[5] = 1;
            }
  /*6*/     else if( k_its("OpenTries") ) {
                OpenTries = k_int();
                init[6] = 1;
            }
  /*7*/     else if( k_its("OpenWait") ) {
                OpenWait = k_int();
                init[7] = 1;
            }
  /*8*/     else if( k_its("PeerHeartBeatFile") ) {
                str = k_str();
                if(str) strncpy( PeerHeartBeatFile, str, NAM_LEN);
                init[8] = 1;
            }
  /*9*/     else if( k_its("ArchiveDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( ArchiveDir, str, NAM_LEN );
                init[9] = 1;
            }
  /*10*/     else if( k_its("HeaderDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( HeaderDir, str, NAM_LEN );
                init[10] = 1;
            }

         /* Get the mappings from box id to SCNL name
          ********************************************/ 
  /*opt*/   else if( k_its("ChannelName") ) {
                if( NchanNames >= MAXCHAN ) {
                    fprintf( stderr, 
                       "sm_reftek2ew: Too many <ChannelName> commands "
                       "in <%s>; max=%d; exiting!\n", 
                        configfile,(int) MAXCHAN );
                    exit( -1 );
                }
         /* Get the box name */
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_VENDOR_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "sm_reftek2ew: box name <%s> too long " 
                                         "in <ChannelName> cmd; exiting!\n", str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].box, str);
                }
         /* Get the channel number */
                ChannelName[NchanNames].chan = k_int();
                if(ChannelName[NchanNames].chan > SM_MAX_CHAN){
                    fprintf(stderr, "sm_reftek2ew: Channel number %d greater "
                                    "than %d in <ChannelName> cmd; exiting\n",
                                     ChannelName[NchanNames].chan,SM_MAX_CHAN);
                    exit(-1);
                }
         /* Get the SCNL name */
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_STA_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "sm_reftek2ew: station name <%s> too long " 
                                         "in <ChannelName> cmd; exiting!\n", str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].sta, str);
                } 
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_COMP_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "sm_reftek2ew: component name <%s> too long "
                                         "in <ChannelName> cmd; exiting!\n", str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].comp, str);
                } 
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_NET_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "sm_reftek2ew: network name <%s> too long "
                                         "in <ChannelName> cmd; exiting!\n", str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].net, str);
                } 
                if( ( str=k_str() ) ) { 
                    if(strlen(str)>SM_LOC_LEN ) { /* from rw_strongmotion.h */
                        fprintf( stderr, "sm_reftek2ew: location name <%s> too long "
                                         "in <ChannelName> cmd; exiting!\n", str );
                        exit( -1 );
                    }
                    strcpy(ChannelName[NchanNames].loc, str);
                }
                NchanNames++;
            }         
  /*11*/    else if( k_its("SaveDataFiles") ) {
                SaveDataFiles = k_int();
                init[11] = 1;
            }
  /*12*/    else if( k_its("PeerHeartBeatInterval") ) {
                PeerHeartBeatInterval = k_int();
                init[12] = 1;
            }
  /*13*/    else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_int();
                init[13] = 1;
            }

               /* optional commands */
            else if( k_its("Pager") ) {
                if (nPager >= MAX_PAGERS ) {
                    fprintf(stderr, "%s Too many <Pager> commands in <%s>", 
                             "k2faudit: ", configfile );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAX_PAGERS );
                    exit( -1 );
                }
                if( (long)(str=k_str()) != 0 )  {
                    strcpy(person[nPager++], str);
                }
            }

            else if( k_its("Manager") ) {
                if( (long)(str=k_str()) != 0 )  {
                    strcpy(manager, str);
                }
            }

         /* Unknown command
          *****************/ 
            else {
                fprintf( stderr, "%s: <%s> Unknown command in <%s>.\n", 
                         ProgName, com, configfile );
                continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr, 
                       "%s: Bad <%s> command in <%s>; exiting!\n",
                        ProgName, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }
   
/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "%s: ERROR, no ", ProgName );
       if ( !init[0] )  fprintf( stderr, "<LogFile> "       );
       if ( !init[1] )  fprintf( stderr, "<MyModuleId> "    );
       if ( !init[2] )  fprintf( stderr, "<GetFromDir> "    );
       if ( !init[3] )  fprintf( stderr, "<CheckPeriod> "   );
       if ( !init[4] )  fprintf( stderr, "<Debug> "         );
       if ( !init[5] )  fprintf( stderr, "<OpenTries> "     );
       if ( !init[6] )  fprintf( stderr, "<CheckPeriod> "   );
       if ( !init[7] )  fprintf( stderr, "<OpenWait> "      );
       if ( !init[8] )  fprintf( stderr, "<PeerHeartBeatFile> " );
       if ( !init[9] )  fprintf( stderr, "<ArchiveDir> "        );
       if ( !init[10])  fprintf( stderr, "<HeaderDir> "         );
       if ( !init[11])  fprintf( stderr, "<SaveDataFiles> "     );
       if ( !init[12])  fprintf( stderr, "<PeerHeartBeatInterval> " );
       if ( !init[13])  fprintf( stderr, "<HeartBeatInterval> " );
       fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }
   
   return;
}

/******************************************************************************
 *  sm_file2ew_lookup( )   Look up important info from earthworm.h tables     *
 ******************************************************************************/
void sm_file2ew_lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 ) {
        fprintf( stderr,
                "%s:  Invalid ring name <%s>; exiting!\n",
                 ProgName, RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr, 
              "%s: error getting local installation id; exiting!\n",ProgName );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid module name <%s>; exiting!\n", ProgName, MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n",ProgName );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_ERROR>; exiting!\n",ProgName );
      exit( -1 );
   }
   if ( GetType( "TYPE_STRONGMOTION", &TypeSM ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_STRONGMOTION>; exiting!\n",ProgName );
      exit( -1 );
   }
   return;
} 

/******************************************************************************
 * sm_file2ew_status() builds a heartbeat or error message & puts it into     *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/
void sm_file2ew_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size;
   long        t;
 
/* Build the message
 *******************/ 
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
        sprintf( msg, "%ld %ld\n\0", t, myPid);
   }
   else if( type == TypeError )
   {
        sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
        logit( "et", "%s: %s\n", ProgName, note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */     

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","%s:  Error sending heartbeat.\n", ProgName );
        }
        else if( type == TypeError ) {
           logit("et","%s:  Error sending error:%d.\n", ProgName, ierr );
        }
   }

   return;
}



