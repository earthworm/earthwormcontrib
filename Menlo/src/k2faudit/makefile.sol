CFLAGS = -D_REENTRANT $(GLOBALFLAGS)

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

BINARIES = k2faudit.o k2aud.o \
          $L/chron3.o $L/logit.o $L/kom.o $L/getutil.o $L/rw_strongmotion.o \
          $L/sleep_ew.o $L/time_ew.o $L/transport.o $L/dirops_ew.o   

k2faudit: $(BINARIES)
	cc -o $B/k2faudit $(BINARIES)  -lm -lposix4

.c.o:
	$(CC) $(CFLAGS) -g $(CPPFLAGS) -c  $(OUTPUT_OPTION) $<



# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/k2faudit*

# THE END

