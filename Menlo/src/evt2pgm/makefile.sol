
CFLAGS = ${GLOBALFLAGS}

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

BINARIES = file2ew.o $L/chron3.o $L/logit.o $L/kom.o $L/getutil.o \
	   $L/sleep_ew.o $L/swap.o $L/time_ew.o $L/transport.o $L/dirops_ew.o $L/k2evt2ew.o 

SMBIN = $(BINARIES) $L/rw_strongmotionII.o 


all:
	make -f makefile.sol sm_nsmp2ew 

sm_nsmp2ew: $(SMBIN) nsmp2ew.o
	cc -o $B/sm_nsmp2ew $(SMBIN) nsmp2ew.o  -lm -lposix4



# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/sm_*2ew* 
