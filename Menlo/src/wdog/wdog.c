#include "stdio.h"
#include "stdlib.h"
#include "conio.h"
#include "string.h"
#include "windows.h"
#include "winbase.h"
#include "pci_wdog.h"

// Macro to convert degF to degC (rounded to whole number)
// *******************************************************
#define tmpc(degf) ((int)(5.0 / 9.0 * ((degf) - 32.0) + 0.5))

//
//===========================================================================
//
//                               wdog
//      A Win32 Program to print status and enable or disable a
//      Berkshire PCI PC Watchdog board.
//
//===========================================================================
//
//      Date  Ver.  Description
// ---------- ----- ---------------------------------------------
// 2002 06-19 1.00  First version started
//
//


int main( int argc, char *argv[] )
{
   int              ok = 1;          // Board status flag
   UINT             i;
   int              tmpF;
   char             s[64];
   WD_CR_DATA       wdcr;            // Command/response data structure
   PCI_WDOG_LIB_VER *libver;         // Library version

   printf( "\nThis program prints the status of a Berkshire PCI PC\n" );
   printf( "Watchdog Card and optionally enables/disables the card.\n" );
   printf( "Usage: 'wdog sta', 'wdog ena', or 'wdog dis'\n\n" );

// Check program arguments
// ***********************
   if ( argc != 2 ) return 1;
   if ( (strcmp(argv[1], "sta") != 0) &&             // Status
        (strcmp(argv[1], "ena") != 0) &&             // Enable
        (strcmp(argv[1], "dis") != 0) ) return 1;    // Disable

// Initialize watchdog board
// *************************
   if ( !WDogInit() )
   {
      printf( &WDogErrorString[0] );
      printf( "WDogInit() error. Exiting.\n" );
      WDogClose();
      return 2;
   }

// Read firmware and library version of watchdog
// *********************************************
   if ( !WDogGetVersion( &s[0]) )
   {
      printf( &WDogErrorString[0] );
      printf( "WDogGetVersion() error. Exiting.\n" );
      WDogClose();
      return 3;
   }
   printf( "PCI PC Watchdog Firmware Version is: %s\n", s );

   libver = WDogLibVersion();
   if ( WDogErrorString[0] )
   {
      printf( &WDogErrorString[0] );
      printf( "WDogLibVersion() error. Exiting.\n" );
      WDogClose();
      return 4;
   }
   printf( "Watchdog API library version: %s\n", libver->strLibVersion );

// Get temperature in degrees F and tickle watchdog
// ************************************************
   tmpF = WDogReadTemp( 1 );
   if ( WDogErrorString[0] )
   {
      printf( &WDogErrorString[0] );
      printf( "WDogReadTemp() error. Exiting.\n" );
      WDogClose();
      return 5;
   }
   printf( "Temperature = %d deg F = %d deg C\n", tmpF, tmpc(tmpF) );

// See if the watchdog has tripped.
// Status of trip flag and trip LED are not changed.
// ************************************************
   if ( WDogTestWDogtrip(FALSE) )
      printf( "Watchdog has tripped.\n" );
   else
      printf( "Watchdog has not tripped.\n" );

// Send command to get status of watchdog board
// (shown in Section 8.2 of manual)
// ********************************************
   wdcr.bCmmdTo  = WDOG_GetStat;
   wdcr.bCmmdLsb = wdcr.bCmmdMsb = 0;

   if ( !WDogSendCommand(&wdcr) )
   {
      printf( &WDogErrorString[0] );
      printf( "WDogSendCommand() error. Exiting.\n" );
      WDogClose();
      return 6;
   }

   if ( wdcr.bRespLsb & 0x01 )
      printf( "Non-volatile (EEPROM) memory is ok.\n" );
   else
   {
      printf( "Non-volatile (EEPROM) memory is NOT ok\n" );
      ok = 0;
   }

   if ( wdcr.bRespLsb & 0x02 )
      printf( "Temperature sensor is ok.\n" );
   else
   {
      printf( "Temperature sensor is NOT ok.\n" );
      ok = 0;
   }

   if ( wdcr.bRespLsb & 0x10 )
      printf( "Watchdog card is: Disabled.\n" );
   else
      printf( "Watchdog card is: Enabled.\n" );

   if ( wdcr.bRespLsb & 0x80 )
      printf( "Watchdog has completed its power-up delay.\n" );
   else
      printf( "Watchdog has NOT completed its power-up delay.\n" );

// Send command to get a one-byte A/D sample
// (shown in Section 8.12 of manual)
// AC power is on if the two high bits are set
// *******************************************
   wdcr.bCmmdTo  = 0x60;
   wdcr.bCmmdLsb = wdcr.bCmmdMsb = 0;

   if ( !WDogSendCommand(&wdcr) )
   {
      printf( &WDogErrorString[0] );
      printf( "WDogSendCommand() error. Exiting.\n" );
      WDogClose();
      return 7;
   }
   printf( "A/D sample byte: %02X hex\n", wdcr.bRespLsb );

   if ( (wdcr.bRespLsb & 0xC0) == 0xC0 )
      printf( "AC power is ON.\n" );
   else
      printf( "AC power is OFF.\n" );

// Read settings of switch SW1
// ***************************
   i = (UINT)WDogReadSwitch();
   printf( "Settings of switch S1: %02X hex\n", i );

// Enable or disable watchdog card
// *******************************
   if ( strcmp(argv[1],"dis") == 0 )
   {
      if ( WDogEnableDisable(FALSE) == TRUE )
         printf( "Watchdog card is now set to: Disabled.\n" );
      else
      {
         printf( "Failure on PC Watchdog Disable\n" );
         ok = 0;
      }
   }

   if ( strcmp(argv[1],"ena") == 0 )
   {
      if ( WDogEnableDisable(TRUE) == TRUE )
         printf( "Watchdog card is now set to: Enabled.\n" );
      else
      {
         printf( "Failure on PC Watchdog Enable\n" );
         ok = 0;
      }
   }

// The bottom line
// ***************
   if ( ok )
      printf( "Watchdog card is functioning correctly.\n" );
   else
      printf( "Watchdog card is NOT functioning correctly.\n" );

   WDogClose();
   printf( "Program exiting.\n" );
   return 0;
}
