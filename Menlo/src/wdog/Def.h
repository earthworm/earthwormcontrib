/* ------------------- Global Definitions -------------------------------*/

#define TRUE    1
#define FALSE   0


/* ------------------- TYPEDEFS -----------------------------------------*/

typedef unsigned int  UINT  ;
typedef unsigned int  UWORD  ;
typedef unsigned long ULONG  ;

typedef unsigned char UBYTE ;


typedef union {         // deal with intel backwards words and longs 
        UINT  i    ;
        UBYTE b[2] ;
        }BYTE2  ;

typedef union {
        ULONG l    ;
        UINT  i[2] ;
        UBYTE b[4] ;
        }BYTE4  ;

