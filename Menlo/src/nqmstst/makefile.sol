#
# nqmstst - NetQuakes mSEED testing
#

CC     = cc
CFLAGS = -D_REENTRANT ${GLOBALFLAGS}

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

QLIB_DIR = $(EW_HOME)/$(EW_VERSION)/src/libsrc/qlib2/

CFLAGS +=  -g -I$(QLIB_DIR) 

all: qlib2 nqmstst

qlib2:  FORCE
	@echo "Making qlib2"
	cd $(QLIB_DIR); make

SRCS = main.c   

OBJS = main.o  
 
EW_LIBS = \
	$L/getutil.o \
	$L/kom.o \
	$L/logit_mt.o \
	$L/sema_ew.o \
	$L/threads_ew.o \
	$L/time_ew.o \
	$L/sleep_ew.o \
	$L/dirops_ew.o \
	$L/transport.o

# IGD 2006/11/16 Note that we use qlib2nl: no-leap-seconds version of Qlib2
nqmstst: $(OBJS); \
        $(CC) -o $(B)/nqmstst $(OBJS) $(EW_LIBS) -L$(QLIB_DIR) -lqlib2nl -lsocket -lnsl -lposix4 -lthread -lm

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~
	(cd $(QLIB_DIR); make clean)

clean_bin:
	rm -f $B/nqmstst*

FORCE:
