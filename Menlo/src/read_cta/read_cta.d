#
# This is read_cta's parameter file
#
#  Extrapolation search is much faster, but it succeeds only if
#  there are no time gaps in the tape data.  If the write_cta
#  program is stopped and then restarted, this will produce a
#  time gap.  If the write_cta program is paused and resumed,
#  the tape data will be continuous.
#
TapeName         /dev/rmt/1n    # Should end in "n"
ymd              19990521       # Year, month, day of event
hms              160000         # Hour, minute, second of event
Duration         60             # Event duration in seconds
SearchType       2              # 1 = Extrapolation search
                                # 2 = Linear search
