
    /*******************************************************************
     *                             read_cta                            *
     *                                                                 *
     *  Program to restore files from a continuous tape archive (cta)  *
     *  This is a Solaris-only program.                                *
     *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Function prototypes
   *******************/
void   GetConfig( char * );
int    GetTarFileName( char *, char * );
time_t GetFileStartTime( char * );
void   MtRewind( char * );
time_t GetEventStart( char *, char * );
void   MtFs( char *, int );
void   TarX( char * );
 
/* Read these from the config file
   *******************************/
extern char TapeName[80];           /* Name of tape device */
extern char ymd[40];                /* Event year, month, day */
extern char hms[40];                /* Event hour, min, sec */
extern int  Duration;               /* Event duration in sec */
extern int  SearchType;             /* Extrapolation or linear */


int main( int argc, char *argv[] )
{
   char   *configFile;
   char   defaultConfig[] = "read_cta.d";
   char   tarFileName[80];
   time_t eventStart;
   int    nfiles = 0;
   int    i;
   int    ts, te;
   int    fst0;
   int    nskip;

/* Read the configuration file
   ***************************/
   configFile = (argc < 2) ? &defaultConfig[0] : argv[1];
   GetConfig( configFile );

/* Print configuration file parameters
   ***********************************/
   putchar( '\n' );
   printf( "Tape device name: %s\n", TapeName );
   printf( "Event start time: %s %s\n", ymd, hms );
   printf( "Event duration:   %d seconds\n", Duration );
   printf( "Search type:      %d", SearchType );
   if ( SearchType == 1 )
      printf( " (Extrapolation search)" );
   else if ( SearchType == 2 )
      printf( " (Linear search)" );
   putchar( '\n' );

/* Calculate event start time in time_t units
   ******************************************/
   eventStart = GetEventStart( ymd, hms );

/* Rewind the tape
   ***************/
   puts( "\nRewinding the tape..." );
   MtRewind( TapeName );

/* Get name of first file on tape
   ******************************/
   puts( "Getting name of first tape file..." );
   if ( GetTarFileName( TapeName, tarFileName ) != 0 )
   {
      puts( "No tar files on tape. Exiting." );
      return -1;
   }
   printf( "%s", tarFileName );
   fst0 = GetFileStartTime( tarFileName );

   ts = eventStart - fst0;
   te = ts + Duration;

   if ( SearchType == 2 )
      printf( "  ts:%6d  te:%6d", ts, te );
   putchar( '\n' );

   if ( te <= 0 )
   {
      puts( "Event is before first tape file. Exiting." );
      return -1;
   }

/* Do an extrapolation search for the event.
   This method is much faster that a linear search, but the
   method will fail if there are gaps in the file sequence.
   ********************************************************/
   if ( SearchType == 1 )
   {
      int fst1;
      int fileSize;         /* In seconds */
      int startFile;        /* First file containing event */
      int stopFile;         /* Last file containing event */

/* Get name of second file on tape
   *******************************/
      puts( "Getting name of second tape file..." );
      if ( GetTarFileName( TapeName, tarFileName ) != 0 )
      {
         puts( "There is only one tar file on the tape. Exiting." );
         return -1;
      }
      puts( tarFileName );
      fst1 = GetFileStartTime( tarFileName );

/* Compute file size, in seconds.
   Compute startFile and stopFile.
   ******************************/
      fileSize = fst1 - fst0;
      printf( "File size: %d seconds\n", fileSize );
      if ( fileSize <= 0 )
      {
         puts( "Error. fileSize is le 0. Exiting." );
         return -1;
      }

      startFile = (eventStart - fst0) / fileSize;
      if ( startFile < 0 ) startFile = 0;
      stopFile = (eventStart + Duration - fst0) / fileSize;
      if ( stopFile < 0 ) stopFile = 0;

/* Compute the number of files to skip and
   the number of files to read.
   The current file is file number 2.
   ***************************************/
      nskip  = startFile - 2;
      nfiles = stopFile - startFile + 1;
   }
            /****** End of extrapolation search ******/

/* Do a linear search for the event.
   This method is slow, but reliable.
   *********************************/
   if ( SearchType == 2 )
   {
      puts( "Searching for tape files containing the event..." );
      while ( GetTarFileName( TapeName, tarFileName ) == 0 )
      {
         printf( "%s  ", tarFileName );

         ts = eventStart - GetFileStartTime( tarFileName );
         te = ts + Duration;
         printf( "ts:%6d  te:%6d\n", ts, te );

         if ( nfiles == 0 )
         {
            if ( ts < 0 )
               nfiles = 1;
            if ( te <= 0 )
            {
               puts( "End of event found." );
               nskip = -nfiles - 1;
               goto SKIP;
            }
         }
         else
         {
            nfiles++;
            if ( te <= 0 )
            {
               puts( "End of event found." );
               nskip = -nfiles - 1;
               goto SKIP;
            }
         }
      }

/* Reached the end of data on the tape.
   Grab last file on tape, just in case.
   ************************************/
      puts( "End of tape detected." );
      nskip = -1 * (++nfiles);
   }
            /****** End of linear search ******/

/* Skip to the first tape file which contains the event
   ****************************************************/
SKIP:
   if ( nskip < 0 )
   {
      printf( "Skipping backwards %d tape file", -nskip );
      if ( nskip < -1 ) putchar( 's' );
      puts( "..." );
   }

   if ( nskip > 0 )
   {
      printf( "Skipping forwards %d tape file", nskip );
      if ( nskip > 1 ) putchar( 's' );
      puts( "..." );
   }
   MtFs( TapeName, nskip );

/* Restore the event from tape to disk
   ***********************************/
   printf( "Restoring %d file", nfiles );
   if ( nfiles > 1 ) putchar( 's' );
   puts( " from tape to disk..." );

   for ( i = 0; i < nfiles; i++ )
      TarX( TapeName );

   return 0;
}

