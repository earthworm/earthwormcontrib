
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <time_ew.h>

time_t GetFileStartTime( char *tarFileName )
{
   struct tm time_str;
   char   s[8];
   time_t tt;

/* wt files contain TYPE_TRACEBUF messages.
   wu files contain TYPE_TRACEBUF2 messages.
   ****************************************/
   if ( (strncmp(tarFileName, "wt", 2) != 0) &&
        (strncmp(tarFileName, "wu", 2) != 0) )
   {
      printf( "Bad tar file name: %s\n", tarFileName );
      printf( "Exiting.\n" );
      exit( -1 );
   }

   if ( strlen( tarFileName ) < 16 )
   {
      printf( "Bad tar file name: %s\n", tarFileName );
      printf( "Exiting.\n" );
      exit( -1 );
   }

   memcpy( s, tarFileName+2, 4 );
   s[4] = '\0';
   time_str.tm_year = atoi(s) - 1900;

   memcpy( s, tarFileName+6, 2 );
   s[2] = '\0';
   time_str.tm_mon = atoi(s) - 1;

   memcpy( s, tarFileName+8, 2 );
   s[2] = '\0';
   time_str.tm_mday = atoi(s);

   memcpy( s, tarFileName+10, 2 );
   s[2] = '\0';
   time_str.tm_hour = atoi(s);

   memcpy( s, tarFileName+12, 2 );
   s[2] = '\0';
   time_str.tm_min = atoi(s);

   memcpy( s, tarFileName+14, 2 );
   s[2] = '\0';
   time_str.tm_sec = atoi(s);

   time_str.tm_isdst = 0;     /* This parameter doesn't seem to matter */

   tt = timegm_ew( &time_str );
   if ( tt == (time_t)-1 )
   {  
      printf( "timegm_ew() error. Exiting.\n" );
      exit( -1 );
   }  
   return tt;
}


time_t GetEventStart( char *ymd, char *hms )
{
   struct tm time_str;
   char   s[8];
   time_t tt;

   if ( strlen( ymd ) != 8 )
   {
      printf( "Error. Bad ymd string: %s\n", ymd );
      printf( "Exiting.\n" );
      exit( -1 );
   }
   if ( strlen( hms ) != 6 )
   {
      printf( "Error. Bad hms string: %s\n", hms );
      printf( "Exiting.\n" );
      exit( -1 );
   }

   memcpy( s, ymd, 4 );
   s[4] = '\0';
   time_str.tm_year = atoi(s) - 1900;

   memcpy( s, ymd+4, 2 );
   s[2] = '\0';
   time_str.tm_mon = atoi(s) - 1;

   memcpy( s, ymd+6, 2 );
   s[2] = '\0';
   time_str.tm_mday = atoi(s);

   memcpy( s, hms, 2 );
   s[2] = '\0';
   time_str.tm_hour = atoi(s);

   memcpy( s, hms+2, 2 );
   s[2] = '\0';
   time_str.tm_min = atoi(s);

   memcpy( s, hms+4, 2 );
   s[2] = '\0';
   time_str.tm_sec = atoi(s);

   time_str.tm_isdst = 0;     /* This parameter doesn't seem to matter */

   tt = timegm_ew( &time_str );
   if ( tt == (time_t)-1 )
   {  
      printf( "timegm_ew() error. Exiting.\n" );
      exit( -1 );
   }  
   return tt;
}


