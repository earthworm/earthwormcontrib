
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


     /*************************************************************
      *                           TarX()                          *
      *                                                           *
      *  Extract a tar file from tape.                            *
      *                                                           *
      *  Returns -1 if error.                                     *
      *           0 if all is ok                                  *
      *************************************************************/

void TarX( char *tapeName )
{
   char cmd[80];

   sprintf( cmd, "tar xvf %s", tapeName );
   if ( system( cmd ) == -1 )
   {
      printf( "Can't extract tar file from tape. Exiting.\n" );
      exit( -1 );
   }
   return;
}


     /*************************************************************
      *                         MtRewind()                        *
      *                                                           *
      *  Rewind the tape.                                         *
      *                                                           *
      *  Returns -1 if error.                                     *
      *           0 if all is ok                                  *
      *************************************************************/

void MtRewind( char *tapeName )
{
   char cmd[80];

   sprintf( cmd, "mt -f %s rewind", tapeName );
   if ( system( cmd ) == -1 )
   {
      printf( "Can't rewind tape. Exiting.\n" );
      exit( -1 );
   }
   return;
}


     /*************************************************************
      *                           MtFs()                          *
      *                                                           *
      *  Skip format or backwards nskip files.                    *
      *                                                           *
      *  Returns -1 if error.                                     *
      *           0 if all is ok                                  *
      *************************************************************/

void MtFs( char *tapeName, int nskip )
{
   char cmd[80];

   if ( nskip == 0 )
      return;

   if ( nskip > 0 )
      sprintf( cmd, "mt -f %s fsf %d", tapeName, nskip );
   else
      sprintf( cmd, "mt -f %s nbsf %d", tapeName, -nskip );

   if ( system( cmd ) == -1 )
   {
      printf( "Can't skip tape files. Exiting.\n" );
      exit( -1 );
   }
   return;
}


     /*************************************************************
      *                       GetTarFileName()                    *
      *                                                           *
      *  Get the name of a tar file on tape.                      *
      *                                                           *
      *  Returns -1 if no tar file at current tape position.      *
      *           0 if all is ok                                  *
      *************************************************************/

int GetTarFileName( char *tapeName, char *tarFileName )
{
   char cmd[80];
   char *tempFile = tempnam( ".", "temp" );
   FILE *fp;
   int  rc;
   char *ptr;

   sprintf( cmd, "tar tf %s > %s", tapeName, tempFile );
   if ( system( cmd ) == -1 )
   {
      printf( "Can't invoke tar. Exiting.\n" );
      exit( -1 );
   }

   fp = fopen( tempFile, "r" );
   if ( fp == NULL )
   {
      printf( "Can't open temp file. Exiting.\n" );
      exit( -1 );
   }

   rc = ( fgets( tarFileName, 80, fp ) == NULL ) ? -1 : 0;

   fclose( fp );

   if ( remove( tempFile ) == -1 )
   {
      printf( "remove() error. Exiting.\n" );
      exit( -1 );
   }

/* Truncate string at the first newline character
   **********************************************/
   ptr = strchr( tarFileName, '\n' );
   if ( ptr != NULL )
      *ptr = '\0';

   return rc;
}
