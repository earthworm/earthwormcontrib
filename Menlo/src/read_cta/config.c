
#include <stdio.h>
#include <string.h>
#include <kom.h>

char TapeName[80];
char ymd[40];                /* Event year, month, day */
char hms[40];                /* Event hour, min, sec */
int  Duration;               /* Event duration in sec */ 
int  SearchType;             /* Extrapolation or linear */ 


  /************************************************************************
   *  GetConfig() processes command file(s) using kom.c functions;        *
   *                    exits if any errors are encountered.              *
   ************************************************************************/

#define NCOMMAND 5


void GetConfig( char *configfile )
{
   const    ncommand = NCOMMAND;    /* Process this many required commands */
   char     init[NCOMMAND];         /* Init flags, one for each command */
   int      nmiss;                  /* Number of missing commands */
   char     *com;
   char     *str;
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      printf( "Error opening command file <%s>. Exiting.\n",
               configfile );
      exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )             /* Read next line from active file  */
      {  
         com = k_str();            /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )          continue;
         if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
         if ( com[0] == '@' )
         {
            success = nfiles+1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "Can't open command file <%s>. Exiting.\n",
                        &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         else if ( k_its("TapeName") )
         {
             str = k_str();
             if (str) strcpy( TapeName, str );
             init[0] = 1;
         }
         else if ( k_its("ymd") )
         {
             str = k_str();
             if (str) strcpy( ymd, str );
             init[1] = 1;
         }
         else if ( k_its("hms") )
         {
             str = k_str();
             if (str) strcpy( hms, str );
             init[2] = 1;
         }
         else if ( k_its("Duration") )
         {
             Duration = k_int();
             init[3] = 1;
         }
         else if ( k_its("SearchType") )
         {
             SearchType = k_int();
             init[4] = 1;
         }

/* Unknown command
   ***************/
         else
         {
             printf( "<%s> Unknown command in <%s>.\n",
                      com, configfile );
             continue;
         }
 
/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "Bad <%s> command in <%s>. Exiting.\n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }
 
/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;
 
   if ( nmiss )
   {
       printf( "ERROR, no " );
       if ( !init[0] )  printf( "<TapeName> " );  
       if ( !init[1] )  printf( "<ymd> " );  
       if ( !init[2] )  printf( "<hms> " );  
       if ( !init[3] )  printf( "<Duration> " );  
       if ( !init[4] )  printf( "<SearchType> " );  
       printf( "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }
   return;
}
