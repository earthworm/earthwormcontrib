#
#                      Make file for read_cta
#                         Solaris Version
#
# -lposix4 is required for time_ew calls
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib


WRITETAPE = read_cta.o config.o tapeio.o filetime.o $L/kom.o $L/time_ew.o

read_cta: $(WRITETAPE)
	cc -o $B/read_cta $(WRITETAPE) -mt -lm -lposix4 -lthread -lc

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/read_cta*

