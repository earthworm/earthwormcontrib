# pathcheck configuration File

# Basic Earthworm Setup
# ---------------------
 MyModuleId    MOD_PATHCHECK   # module id for this program,
 RingName          HYPO_RING   # transport ring to write to,
 HeartBeatInterval        30   # Send heartbeats to statmgr this often (sec)
 LogFile                   1   # If 0, don't write logfile at all,
 Verbose                   1   # If non-zero, log each path check result

# Task-specific Configuration
#----------------------------
 CheckInterval           600   # Seconds between Path checks

 ReportBadAfterNmin       60   # Send Earthworm error message only after
                               # path has been "bad" for this many minutes

 SendStatusToSNW           1   # Flag controlling whether TYPE_SNW messages
                               #   are created to report path status.
                               #   0=no, non-zero=create TYPE_SNW msgs

 StaName               MENLO   # station name (up to 5 characters) to 
                               #   write in TYPE_SNW messages
 NetCode                  NC   # Network code (up to 2 characters) to
                               #   write in TYPE_SNW messages
 
# Paths to monitor
#----------------------------------------------------------------
# Specify as many "Path" commands as desired.
#  <PathDescription> is a char string used in log/error messages.
#  <DestinationHost> is the hostname you want to reach.
#  <GoodHops> is the number of hops you expect when everything is OK.
#  <MaxHops>  is the maximum number of hops to attempt for this path.
#
# When first configuring pathcheck, run a command-line trace to 
# each DestinationHost to verify:
# a. that the host is described by the same character string 
#    in the trace output as in your configuration file, and
# b. the number of hops to your DestinationHost under normal 
#    conditions matches that in your configutation file.
#
# Here are the O/S-specific tracing commands:
#   Windows:  tracert your-destination-host
#   Solaris:  traceroute your-destination-host
#
#      PathDescription       DestinationHost  GoodHops  MaxHops
#      -------------------   ---------------  --------  -------
 Path  Menlo2Vollmer-uWave   ehz-vollmer-lb0     3        10
 Path  Menlo2Fremont-uWave   ehz-fremont-lb0     2        10 

# THE END
