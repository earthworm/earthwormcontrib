/* THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU CHECKED IT OUT!
 *
 *  $Id: tracepath.c 418 2008-07-16 21:25:24Z dietz $
 * 
 *  Revision history:
 *   $Log$
 *   Revision 1.5  2008/07/16 21:25:24  dietz
 *   Modified to count "timeouts" independently from "hops"
 *
 *   Revision 1.4  2008/04/18 21:11:56  dietz
 *   Modified to use unique trace output file name for each path.
 *
 *   Revision 1.3  2008/04/17 22:21:51  dietz
 *   Maximum #hops for each trace command is now configurable for each path
 *
 *   Revision 1.2  2008/04/17 21:36:19  dietz
 *   Modified to remove traceroute output file at the end of function
 *
 *   Revision 1.1  2008/01/03 20:09:29  dietz
 *   Merged OS-specific trace code back into one file: tracepath.c.
 *   Added code to recognize "unreachable" paths in trace output.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pathcheck.h"

/* Definitions specific to Solaris trace 
 ***************************************/
#if defined(_SOLARIS)
  #define OS_DEFINED            1
  #define START_PARSING_LINE    1  /* line# in trace output to begin parsing hops */
  #define START_HOST_POSITION   4  /* array position of first char in each hostname */
  static char *TraceCmd    = "traceroute -m";
  static char *Unreachable = "unreachable";
  static char *Timeout     = "* * *";

/*-----------Sample Solaris traceroute stdout: -------------
 1  salt5-l3-vlan43.wr.usgs.gov (130.118.43.215)  1.796 ms  0.508 ms  0.531 ms
 2  * ehz-menlo1-f000.wr.usgs.gov (130.118.43.248)  1.296 ms  1.171 ms
 3  192.168.112.131 (192.168.112.131)  4.001 ms  4.132 ms  3.987 ms
 4  ad1gp (192.168.111.2)  3.383 ms  3.428 ms  3.620 ms
------------------------------------------------------------*/

/* Definitions specific to Windows trace 
 ***************************************/
#elif defined(_WINNT)
  #define OS_DEFINED            1
  #define START_PARSING_LINE    5  /* line# in trace output to begin parsing hops */
  #define START_HOST_POSITION  32  /* array position of first char in each hostname */
  static char *TraceCmd    = "tracert -h";
  static char *Unreachable = "unreachable";
  static char *Timeout     = "Request timed out";


/*--------------Sample Windows tracert stdout ---------------

Tracing route to ehz-vollmer-lb0 [192.168.112.251]
over a maximum of 30 hops:

  1   <10 ms   <10 ms   <10 ms  ehz-tam [192.168.111.129] 
  2    20 ms    20 ms     *     ehz-vollmer-lb0 [192.168.112.251] 
  3    20 ms    10 ms    30 ms  ehz-vollmer-lb0 [192.168.112.251] 

Trace complete.

------------------------------------------------------------*/

/* Undefined operating system 
 ****************************/
#else
  #define OS_DEFINED            0
#endif

/**********************************************************************
 *  TracePath()  Find out number of hops to given destination.        *
 *               Return: number of hops (positive) if successful or   *
 *                       a negative value in case of error            *
 **********************************************************************/

int TracePath( int ipath, char *desthost, int maxhops, int *destreached, int *timeout  )
{
   char   cmd[256];
   char   prevhop[MAX_LEN]; /* string describing previous hop */
   char   thishop[MAX_LEN]; /* string describing current hop  */
   char   outfiletrace[16];
   size_t destlen;
   int    atdesthost   = 0;  
   int    nhops        = 0;
   int    nhops_old    = 0;
   int    line         = 0;
   int    ntimeout     = 0;
   FILE  *ofp;
   int    rc;

   if( !OS_DEFINED ) {
      logit( "et", 
             "tracepath: cannot trace for undefined operating system!\n" );
      return( -1 );
   }

   *timeout     = 0;
   *destreached = DEST_NOTREACHED;
   destlen = strlen( desthost );

/* Build outputfile name; unique per ipath.
 ******************************************/
   sprintf( outfiletrace, "trace%d.out", ipath );
   remove( outfiletrace );  /* remove leftover old copy */

/* Run tracing command.
   Store output in a file to be parsed to determine #hops. 
 *********************************************************/
   sprintf( cmd, "%s %d %s > %s",
            TraceCmd, maxhops, desthost, outfiletrace );

   if( Verbose ) logit( "ot", "Starting command: %s\n", cmd );
   rc = system( cmd );

/* Parse the result file to find number of hops
 **********************************************/
   ofp = fopen( outfiletrace, "r" );  
   if( ofp == NULL ) {
      logit( "et", "tracepath: error opening output file:%s\n",
              outfiletrace );
      return( -2 );
   }
   atdesthost = 0;
   prevhop[0] = 0; /* set prevhop to null */
   while( fgets(cmd, 256, ofp) )
   {
      line++;

      if( Verbose    ) logit( "o", "%s", cmd );  /* log trace output */
      if( atdesthost ) continue;                 /* keep logging, but don't count hops */

      if( strstr( cmd, Unreachable ) )           /* quit now if destination was unreachable */
      {
         logit( "et", "tracepath: destination %s unreachable!\n", desthost );
        *destreached = DEST_UNREACHABLE;
         break;
      }
      if( strstr( cmd, Timeout ) )               /* don't count timeouts as hops */
      {
         ntimeout++;
         continue;
      }

      if( line >= START_PARSING_LINE    &&
          strlen(cmd) > START_HOST_POSITION )      /* start parsing hops */
      {
         char *phost = &cmd[START_HOST_POSITION];  /* point to start of host */
         while( phost[0] == '*' ) phost += 2;      /* for Solaris, jump over partial timeouts */
      
         if( sscanf( phost, "%s", thishop ) != 1 ) 
         {
            logit( "et", "tracepath: error reading trace output\n" );
            return( -3 );
         }
         if( strcmp ( prevhop,  thishop ) == 0 ) continue; /* identical hops, don't increment */
         if( strncmp( desthost, thishop, destlen ) == 0 ) atdesthost = 1;  /* at destination! */
         strcpy( prevhop, thishop );                         /* store thishop for comparison  */
         nhops++;                                            /* new hop; increment hop count  */
      }
   }
   fclose( ofp );
   remove( outfiletrace );
 
/* Set flag to denote whether we reached our destination
 *******************************************************/
   if( atdesthost ) *destreached = DEST_REACHED;
   *timeout = ntimeout;

   return( nhops );
}
