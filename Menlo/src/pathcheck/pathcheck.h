/* THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU CHECKED IT OUT!
 *
 *  $Id: pathcheck.h 508 2010-01-20 18:28:29Z dietz $
 * 
 *  Revision history:
 *   $Log$
 *   Revision 1.6  2010/01/20 18:28:29  dietz
 *   Modified to send TYPE_ERROR for a path after it's been "BAD" for a
 *   configured length of time. Also added the ability to create TYPE_SNW msgs.
 *
 *   Revision 1.5  2008/07/16 21:25:24  dietz
 *   Modified to count "timeouts" independently from "hops"
 *
 *   Revision 1.4  2008/04/17 22:21:51  dietz
 *   Maximum #hops for each trace command is now configurable for each path
 *
 *   Revision 1.3  2008/01/03 20:09:29  dietz
 *   Merged OS-specific trace code back into one file: tracepath.c.
 *   Added code to recognize "unreachable" paths in trace output.
 *
 *   Revision 1.2  2007/12/21 01:15:32  dietz
 *   Minor tweaks relating to creation of tracepath_OS.c files
 *
 *   Revision 1.1  2007/12/21 00:23:44  dietz
 *   Moved system-dependent code into a separate source file instead of using
 *   ifdefs. Testing file-parsing to count hops instead of simple line counts.
 *   Hopefully can avoid duplicate hops this way. Further changes still needed.
 *
 */

#ifndef _PATHCHECK_H
#define _PATHCHECK_H

#include <earthworm.h>
#include <kom.h>
#include <transport.h>

#define MAX_LEN    MAX_INST_STR   /* Max size of installation and module names */
#define MAX_STANAME       5
#define MAX_NETCODE       2       

#define DEST_REACHED      0
#define DEST_NOTREACHED   1
#define DEST_UNREACHABLE  2

typedef struct                   /* Structure containing info about each path */
{
   char   pathdesc[MAX_LEN];     /* character string describing the path   */
   char   desthost[MAX_LEN];     /* destination to count hops to for test  */
   int    goodhops;              /* number of hops if everything is good   */
   int    maxhops;               /* maximum #hops to attempt for this path */
   int    pathstatus;            /* current status of path                 */
   time_t tpathbad;              /* time pathstatus became not PATH_OK     */
   int    notifiedbad;           /* =1 if error message has been sent      */
} PATH; 

/* Things to read or derive from config file
   *****************************************/
char          RingName[MAX_LEN];  /* Name of transport ring */
long          RingKey;            /* Key of transport ring  */
unsigned char MyModId;            /* pathcheck's module id  */
int           LogSwitch;          /* If 0, no logging should be done to disk      */
int           HeartBeatInterval;  /* Send heartbts to statmgr this often (sec)    */
int           CheckInterval;      /* #seconds between path check                  */
int           ReportBadAfterNsec; /* #seconds before sending error about bad path */
int           nPath;              /* Number of paths we know about                */
PATH         *Path;               /* details of Paths being monitored             */
int           Verbose;            /* verbose flag                                 */
int           SendStatusToSNW;    /* flag for creating SeisNetWatch messages      */
char          StaName[MAX_STANAME+1]; /* station name to use in msgs to SNW       */
char          NetCode[MAX_NETCODE+1]; /* network code to use in msgs to SNW       */

#endif
