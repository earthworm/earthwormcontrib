/* THIS FILE IS UNDER CVS - DO NOT MODIFY UNLESS YOU CHECKED IT OUT!
 *
 *  $Id: pathcheck.c 606 2011-03-22 17:42:47Z dietz $
 * 
 *  Revision history:
 *   $Log$
 *   Revision 1.15  2010/01/26 19:19:19  dietz
 *   Added NetCode.StaName to all log and error messages
 *
 *   Revision 1.14  2010/01/20 23:04:07  dietz
 *   Expanded "path OK again" message to include destination host
 *
 *   Revision 1.13  2010/01/20 18:28:29  dietz
 *   Modified to send TYPE_ERROR for a path after it's been "BAD" for a
 *   configured length of time. Also added the ability to create TYPE_SNW msgs.
 *
 *   Revision 1.12  2008/07/16 21:25:24  dietz
 *   Modified to count "timeouts" independently from "hops"
 *
 *   Revision 1.11  2008/04/18 21:11:56  dietz
 *   Modified to use unique trace output file name for each path.
 *
 *   Revision 1.10  2008/04/17 22:21:51  dietz
 *   Maximum #hops for each trace command is now configurable for each path
 *
 *   Revision 1.9  2008/01/03 21:06:52  dietz
 *   removed original tracepath function
 *
 *   Revision 1.8  2008/01/03 20:09:29  dietz
 *   Merged OS-specific trace code back into one file: tracepath.c.
 *   Added code to recognize "unreachable" paths in trace output.
 *
 *   Revision 1.7  2007/12/21 22:05:47  dietz
 *   Added code to monitor/log whether the trace reached the destination host.
 *
 *   Revision 1.6  2007/12/21 00:23:44  dietz
 *   Moved system-dependent code into a separate source file instead of using
 *   ifdefs. Testing file-parsing to count hops instead of simple line counts.
 *   Hopefully can avoid duplicate hops this way. Further changes still needed.
 *
 *   Revision 1.5  2007/12/18 00:01:13  dietz
 *   Modified Windows command that creates the #hops file from trace output
 *
 *   Revision 1.4  2007/12/17 23:41:20  dietz
 *   Modified to log traceroute output.
 *
 *   Revision 1.3  2007/12/11 22:31:10  dietz
 *   Modified to look at environment variable "windir" to find the path
 *   to DOS executables (different for Win2K and WinXP)
 *
 *   Revision 1.2  2007/12/11 19:35:36  dietz
 *   Fixed minor typos
 *
 *   Revision 1.1  2007/12/11 19:15:41  dietz
 *   New program to test paths between hosts by counting #hops.
 *   Works on Solaris and Windows only.
 *
 */

/**********************************************************************
 *                          pathcheck                                 *
 *                                                                    *
 *  Check number of hops to a destination against expected number;    *
 *  complain to statmgr if there's a change in status                 *
 **********************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include "pathcheck.h"

/* Error messages used by pathcheck
 **********************************/
#define ERR_STATUSCHANGE   0     /* Path Status changed */
#define PATH_OK            0
#define PATH_BAD           1
#define PATH_UNREACHABLE   2
#define PATH_NOTREACHED    3

/* Function prototypes
 *********************/
int  TracePath( int, char *, int, int *, int * );      
int  SendEWMessage( unsigned char, short, char * );
void GetConfig( char * );
void LogConfig( void   );

/* Things to look up in the earthworm.h tables
   *******************************************/
static unsigned char LocalInstId;
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeSNW;

/* Other global variables
   **********************/
SHM_INFO     region;
static pid_t myPid;

/**************************************
 *          Main starts here          *
 **************************************/

int main( int argc, char **argv )
{
   time_t  t_heart_next = 0;  /* time of next heartbeat to statmgr */
   time_t  t_check_next = 0;  /* time of next path check */
   char    note[MAX_LEN*4];
   int     i;

/* Check command line arguments
   ****************************/
   if( argc != 2 )
   {
        printf( "Usage:pathcheck <configFile>\n" );
        return 0;
   }

/* Open log file
   *************/
   logit_init( argv[1], 0, 512, 1 );

/* Read configuration file
   ***********************/
   GetConfig( argv[1] );

/* Look up local installation id
   *****************************/
   if( GetLocalInst( &LocalInstId ) != 0 )
   {
      logit( "e", "pathcheck: Error getting local installation id. Exiting.\n" );
      return -1;
   }

/* Look up message types from earthworm tables
   *******************************************/
   if( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      logit( "e", "pathcheck: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      return -1;
   }
   if( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      logit( "e", "pathcheck: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      return -1;
   }
   if( GetType( "TYPE_SNW", &TypeSNW ) != 0 )
   {
      logit( "e", "pathcheck: Invalid message type <TYPE_SNW>. Exiting.\n" );
      return -1;
   }

/* Re-initialize log-file with configured logging level
   ****************************************************/
   logit_init( argv[1], (short) MyModId, 512, LogSwitch );
   logit( "" , "pathcheck: Read command file <%s>\n", argv[1] );

/* Log the config file
   *******************/
   LogConfig();
 
/* Get our own pid for restart purposes
   ************************************/
   myPid = getpid();
   if( myPid == -1 )
   {
      logit( "e", "pathcheck: Can't get my pid; exiting!\n" );
      return( -1 );
   }

/* Attach to shared memory ring
   ****************************/
   tport_attach( &region, RingKey );

/* Loop until kill flag is set
   ***************************/
   while( 1 )
   {
      time_t t_now;

   /* Check kill flag
    *****************/
      if( tport_getflag( &region )==TERMINATE ||
          tport_getflag( &region )==myPid       ) break;

   /* Send heartbeat to statmgr
    ***************************/
      time( &t_now );
      if( t_now >= t_heart_next )
      {
         if ( SendEWMessage( TypeHeartBeat, 0, "" ) != PUT_OK ) {
            logit( "et", "pathcheck: Error sending heartbeat to ring.\n");
         }
         t_heart_next = t_now + (time_t)HeartBeatInterval;
      }

   /* Check Paths if it's time to do so
    ***********************************/
      if( t_now >= t_check_next )
      { 
         for( i=0; i<nPath; i++ ) 
         {
            int    nhops;
            int    ntimeouts;
            int    destreached;
            time_t now;
            time_t tbad;
  
            PATH *ppath = &Path[i];
            nhops = TracePath( i, ppath->desthost, ppath->maxhops, 
                               &destreached, &ntimeouts );
            if( nhops <= 0 ) {
               logit( "et","pathcheck: error checking path to %s\n",
                       ppath->desthost );
               continue;
            }
            time( &now );

         /* Bad Path: destination "unreachable" */           
            if( destreached == DEST_UNREACHABLE )    
            {
               logit( "et", "%s.%s %s status:BAD %s declared unreachable after %d hops,"
                      " %d timeouts (expected %d hops)\n", NetCode, StaName,
                       ppath->pathdesc, ppath->desthost, nhops, ntimeouts, ppath->goodhops );
               if( ppath->tpathbad == 0 ) ppath->tpathbad = now;
               tbad = now - ppath->tpathbad;
               if( !ppath->notifiedbad  &&  tbad >= ReportBadAfterNsec ) {
                  sprintf( note, 
                          "%s.%s %s may be down (%s unreachable after %d hops);"
                          " tbad=%dmin", NetCode, StaName,
                           ppath->pathdesc, ppath->desthost, nhops, tbad/60 );
                  SendEWMessage( TypeError, (short)ERR_STATUSCHANGE, note );
                  ppath->notifiedbad = 1;
               }
               ppath->pathstatus = PATH_UNREACHABLE;
            }

         /* Bad Path: destination not reached */
            else if( destreached == DEST_NOTREACHED ) 
            {
               logit( "et", "%s.%s %s status:BAD %s not reached after %d hops,"
                      " %d timeouts (expected %d hops)\n", NetCode, StaName,
                      ppath->pathdesc, ppath->desthost, nhops, ntimeouts, 
                      ppath->goodhops );
               if( ppath->tpathbad == 0 ) ppath->tpathbad = now;
               tbad = now - ppath->tpathbad;
               if( !ppath->notifiedbad  &&  tbad >= ReportBadAfterNsec ) {
                  sprintf( note, 
                          "%s.%s %s may be down (%s not reached after %d hops);" 
                          " tbad=%dmin", 
                           NetCode, StaName, ppath->pathdesc, ppath->desthost, 
                           nhops, tbad/60 );
                  SendEWMessage( TypeError, (short)ERR_STATUSCHANGE, note );
                  ppath->notifiedbad = 1;
               }
               ppath->pathstatus = PATH_NOTREACHED;
            }

         /* Good Path: destination reached via short path! */
            else if( destreached == DEST_REACHED  &&
                     nhops <= ppath->goodhops )     
            {
               logit( "et",
                      "%s.%s %s status:OK  %d hops, %d timeouts to %s (expected %d hops)\n",
                       NetCode, StaName, ppath->pathdesc, nhops, ntimeouts, 
                       ppath->desthost, ppath->goodhops );
               if( ppath->pathstatus != PATH_OK  &&  ppath->notifiedbad ) {
                 sprintf( note, "%s.%s %s OK again (%d hops to %s);"
                                " was bad %dmin", 
                          NetCode, StaName, ppath->pathdesc, nhops, ppath->desthost, 
                          (now-ppath->tpathbad)/60 );
                 SendEWMessage( TypeError, (short)ERR_STATUSCHANGE, note );
               }
               ppath->notifiedbad = 0;
               ppath->tpathbad    = 0;
               ppath->pathstatus  = PATH_OK;
            } 

         /* Bad Path: destination reached, but via long path! */
            else if( destreached == DEST_REACHED  &&
                     nhops > ppath->goodhops )       
            {                          
               logit( "et",
                      "%s.%s %s status:BAD %d hops, %d timeouts to %s (expected %d hops)\n",
                       NetCode, StaName, ppath->pathdesc, nhops, ntimeouts,
                       ppath->desthost, ppath->goodhops );
               if( ppath->tpathbad == 0 ) ppath->tpathbad = now;
               tbad = now - ppath->tpathbad;
               if( !ppath->notifiedbad  &&  tbad >= ReportBadAfterNsec ) {
                  sprintf( note, 
                          "%s.%s %s may be down (%d=too many hops to %s);"
                          " tbad=%dmin", NetCode, StaName, ppath->pathdesc, nhops,
                           ppath->desthost, tbad/60 );
                  SendEWMessage( TypeError, (short)ERR_STATUSCHANGE, note );
                  ppath->notifiedbad = 1;
               }
               ppath->pathstatus = PATH_BAD;
            }  

         /* Unexpected return combination */
            else
            {
               logit( "et", "WARNING: unexpected TracePath return values: "
                      "destreached=%d nhops=%d ntimeouts=%d\n", 
                       destreached, nhops, ntimeouts );
               continue;
            }

         /* Build SNW msg with current status */
            if( SendStatusToSNW )
            {
               sprintf( note, "%s-%s:1:%s=%d\n",
                        NetCode, StaName, ppath->pathdesc, ppath->pathstatus );
               SendEWMessage( TypeSNW, 0, note );
            }

         } /*end for over all paths*/

         t_check_next = time(&t_now) + (time_t)CheckInterval;
      }
      sleep_ew( 1000 );
   }
   tport_detach( &region );
   logit( "et", "pathcheck: Termination requested; exiting.\n" );
   free( Path );
   return 0;
}


/********************************************************************** 
 * SendEWMessage()                                                    *
 * Builds heartbeat and error msgs and puts them in shared memory.    * 
 * Puts any other message types in shared memory without reformatting *
 **********************************************************************/

int SendEWMessage( unsigned char msg_type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   int         res;
   long        size;
   time_t      t;

   logo.instid = LocalInstId;
   logo.mod    = MyModId;
   logo.type   = msg_type;

   time( &t );

   if( msg_type == TypeHeartBeat ) {
      sprintf( msg, "%ld %d\n", (long)t, myPid );
   }
   else if( msg_type == TypeError ) {
      sprintf( msg, "%ld %d %s\n", (long)t, ierr, note);
      logit("et", "%s\n", note );
   }
   else { /* just pass the note without reformatting */
      sprintf( msg, "%s", note );
   }
   size = strlen( msg );  /* don't include null byte in message */
   res = tport_putmsg( &region, &logo, size, msg );

   return res;
}


/***********************************************************************
 *     GetConfig()  Processes command file using kom.c functions.      *
 *                  Exits if any errors are encountered.               *
 ***********************************************************************/

void GetConfig( char *configFile )
{
   int    ncommand;     /* # of required commands you expect to process */
   char   init[11];     /* Flags, one for each required command */
   int    nmiss;        /* Number of required commands that were missed */
   char  *com;
   char  *str;
   int    nfiles;
   int    success;
   int    i;

/* Set to zero one init flag for each required command
 *****************************************************/
   ncommand = 11;
   for ( i = 0; i < ncommand; i++ ) init[i] = 0;
   nPath   = 0;
   Path    = NULL;
   Verbose = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configFile );
   if ( nfiles == 0 )
   {
      logit("e", "pathcheck: Error opening command file <%s>. Exiting.\n",
               configFile );
      exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )         /* While there are command files open */
   {
      while ( k_rd() )          /* Read next line from active file  */
      {
         com = k_str();         /* Get the first token from line */

      /* Ignore blank lines & comments
       *******************************/
         if ( !com )          continue;
         if ( com[0] == '#' ) continue;

      /* Open a nested configuration file
       **********************************/
         if ( com[0] == '@' )
         {
            success = nfiles+1;
            nfiles  = k_open(&com[1]);
            if ( nfiles != success )
            {
               logit( "e", "pathcheck: Error opening command file <%s>."
                      " Exiting.\n", &com[1] );
               exit( -1 );
            }
            continue;
         }

      /* Read module id for this program
       *********************************/
/*0*/    if( k_its( "MyModuleId" ) )
         {
             str = k_str();
             if( str )
             {
                if( GetModId( str, &MyModId ) < 0 )
                {
                   logit( "e", "pathcheck: Invalid MyModuleId <%s> in <%s>."
                          " Exiting.\n", str, configFile );
                   exit( -1 );
                }
             }
             init[0] = 1;
         }

      /* Name of transport ring to write to
       ************************************/
/*1*/    else if( k_its( "RingName" ) )
         {
             str = k_str();
             if( str )
             {
                strcpy( RingName, str );
                if( ( RingKey = GetKey(str) ) == -1 )
                {
                   logit( "e", "pathcheck: Invalid RingName <%s> in <%s>."
                          " Exiting.\n", RingName, configFile );
                   exit( -1 );
                }
             }
             init[1] = 1;
         }

      /* Set Logfile switch
       ********************/
/*2*/    else if( k_its( "LogFile" ) )
         {
             LogSwitch = k_int();
             init[2] = 1;
         }

      /* Set heartbeat interval
       ************************/
/*3*/    else if( k_its( "HeartBeatInterval" ) )
         {
             HeartBeatInterval = k_int();
             init[3] = 1;
         }

      /* Get info on paths to test
       ***************************/
/*4*/    else if( k_its("Path") )
         {
            PATH *tpath = (PATH *) NULL;  /* temporary pointer */
            tpath = (PATH *)realloc( Path, (nPath+1)*sizeof(PATH) );
            if( tpath == (PATH *)NULL )
            {
               logit( "et", "pathcheck: error reallocing space for"
                      " %d paths\n", nPath+1  );
               exit( -1 );   
            }
            Path = tpath;  /* store pointer new reallocated space */

         /* Initialize new space */
            tpath = &Path[nPath];
            memset( tpath, 0, sizeof(PATH) );
            tpath->pathstatus = PATH_OK;

         /* Read path description */
            str = k_str();   
            if( str && strlen(str)<MAX_LEN ) strcpy( tpath->pathdesc, str );
            else {   
               logit( "e", "pathcheck: Bad PathDescription in <Path> cmd; exiting!\n" );
               exit( -1 );
            }

         /* Read destination host */
            str = k_str();  
            if( str && strlen(str)<MAX_LEN ) strcpy( tpath->desthost, str );
            else {   
               logit( "e", "pathcheck: Bad DestinationHost in <Path> cmd; exiting!\n" );
               exit( -1 );
            }

         /* Read goodhops */
            tpath->goodhops = k_int();

         /* Read maxhops */
            tpath->maxhops = k_int();
            nPath++;
            init[4] = 1;
         }

      /* Set check interval (seconds)
       ******************************/
/*5*/    else if( k_its( "CheckInterval" ) )
         {
             CheckInterval = k_int();
             init[5] = 1;
         }

      /* Set check interval (seconds)
       ******************************/
/*6*/    else if( k_its( "Verbose" ) )
         {
            Verbose = k_int();
            init[6] = 1;
         }

      /* Set error reporting interval (minutes)
       *****************************************/
/*7*/    else if( k_its( "ReportBadAfterNmin" ) )
         {
            ReportBadAfterNsec = 60*k_int();
            init[7] = 1;
         }

      /* Set flag for creating SNW messages
       ************************************/
/*8*/    else if( k_its( "SendStatusToSNW" ) )
         {
            SendStatusToSNW = k_int();
            init[8] = 1;
         }

      /* Set station/network codes for SNW messages
       ********************************************/
/*9*/    else if( k_its( "StaName" ) )
         {
            str = k_str();   
            if( str && strlen(str)<=MAX_STANAME ) strcpy( StaName, str );
            else {   
               logit( "e", 
                      "pathcheck: Bad <StaName> argument (must be 1-%d chars); exiting!\n", 
                      MAX_STANAME );
               exit( -1 );
            }
            init[9] = 1;
         }
/*10*/   else if( k_its( "NetCode" ) )
         {
            str = k_str();   
            if( str && strlen(str)<=MAX_NETCODE ) strcpy( NetCode, str );
            else {   
               logit( "e", 
                      "pathcheck: Bad <NetCode> argument (must be 1-%d chars); exiting!\n", 
                      MAX_NETCODE );
               exit( -1 );
            }
            init[10] = 1;
         }

         else
         {
             logit( "e", "pathcheck: <%s> unknown command in <%s>.\n",
                     com, configFile );
             continue;
         }

      /* See if there were any errors processing the command
       *****************************************************/
         if ( k_err() )
         {
            logit( "e", "pathcheck: Bad <%s> command in <%s>; \n",
                     com, configFile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] ) nmiss++;

   if ( nmiss )
   {
       logit( "e", "pathcheck: ERROR, no " );
       if ( !init[0] ) logit( "e", "<MyModuleId> "         );
       if ( !init[1] ) logit( "e", "<RingName> "           );
       if ( !init[2] ) logit( "e", "<LogFile> "            );
       if ( !init[3] ) logit( "e", "<HeartBeatInterval> "  );
       if ( !init[4] ) logit( "e", "<Path> "               );
       if ( !init[5] ) logit( "e", "<CheckInterval> "      );
       if ( !init[6] ) logit( "e", "<Verbose> "            );
       if ( !init[7] ) logit( "e", "<ReportBadAfterNmin> " );
       if ( !init[8] ) logit( "e", "<SendStatusToSNW> "    );
       if ( !init[9] ) logit( "e", "<StaName> "            );
       if ( !init[10]) logit( "e", "<NetCode> "            );
       logit( "e", "command(s) in <%s>. Exiting.\n", configFile );
       exit( -1 );
   }
   return;
}


/***********************************************************************
 *        LogConfig()  Log the configuration file parameters.          *
 ***********************************************************************/

void LogConfig( void )
{
   int i;
   logit( "", "MyModId:            %u\n",     MyModId  );
   logit( "", "RingName:           %s\n",     RingName );
   logit( "", "HeartBeatInterval:  %d sec\n", HeartBeatInterval );
   logit( "", "CheckInterval:      %d sec\n", CheckInterval     );
   logit( "", "ReportBadAfterNmin: %d min\n", ReportBadAfterNsec/60 );
   logit( "", "SendStatusToSNW:    %d\n",     SendStatusToSNW );
   logit( "", "StaName:            %s\n",     StaName );
   logit( "", "NetCode:            %s\n",     NetCode );
   for( i=0; i<nPath; i++ ) {
     logit( "", "Path: %s  Destination: %s  GoodHops: %d  MaxHops: %d\n",
                 Path[i].pathdesc, Path[i].desthost, 
                 Path[i].goodhops, Path[i].maxhops  ); 
   }
   return;
}

