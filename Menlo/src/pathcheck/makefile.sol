# Makefile for pathcheck, Solaris version

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

pathcheck: pathcheck.o tracepath.o $L/getutil.o $L/kom.o \
         $L/logit.o $L/sleep_ew.o $L/time_ew.o $L/transport.o 
	cc -o $B/pathcheck pathcheck.o tracepath.o $L/getutil.o $L/kom.o \
	$L/logit.o $L/sleep_ew.o $L/time_ew.o $L/transport.o \
	-lm -lnsl -lposix4

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/pathcheck*
