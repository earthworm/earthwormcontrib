
        /******************************************************************
         *                            cvarc.c                             *
         *                                                                *
         *   Convert Archive Format Year 2000 Messages to Pre Y2K Format  *
         *   The buffer is overwritten with the converted arc message.    *
         *                                                                *
         *   Accepts: bufsize = the size of buf in characters             *
         *   Returns 0 if all went well, -1 if error                      *
         ******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int cvarc( char *buf, int bufsize )
{
   char *workbuf;
   char *inptr = buf;
   char line1[256];      /* Input line */
   char line2[256];      /* Output line */
   int  len1;
   char firstChar;

/* Allocate a work buffer
   **********************/
   workbuf = (char *)malloc( bufsize*sizeof(char) );
   if ( workbuf == NULL ) return -1;
   workbuf[0] = '\0';  /*start with a null-string*/

/* Read the input buffer, one line at a time
   into a line buffer.
   *****************************************/
   while ( inptr < (buf + strlen(buf)) )
   {
      if ( sscanf( inptr, "%[^\n]", line1 ) != 1 ) break;
      len1 = strlen( line1 );
      firstChar = line1[0];

/* If the line begins with $, it is a shadow line.
   Copy it untouched.
   **********************************************/
      if ( firstChar == '$' )
         strcpy( line2, line1 );

/* Summary lines always begin with a digit
   ***************************************/
      else if ( firstChar>47 && firstChar<58 )
      {
         char ts[10];
         double f;

         if ( len1 < 164 )                /* Summary line is too short */
         {
            free( workbuf );
            return -1;
         }

         memset( line2, ' ', 154 );
         memcpy( line2, line1+2, 34 );

         memcpy( ts, line1+36, 3 );       /* Mag from max S amplitude */
         ts[3] = '\0';
         f = atof( ts ) / 10.;
         sprintf( ts, "%2.lf", f );
         memcpy( line2+34, ts, 2 );

         memcpy( line2+36, line1+39, 31 );

         memcpy( ts, line1+70, 3 );       /* Coda duration magnitude */
         ts[3] = '\0';
         f = atof( ts ) / 10.;
         sprintf( ts, "%2.lf", f );
         memcpy( line2+67, ts, 2 );

         memcpy( line2+69,  line1+73,   9 );
         memcpy( line2+78,  line1+83,  10 );
         memcpy( line2+88,  line1+94,   2 );   /* Number of P first-motions */
         memcpy( line2+90,  line1+97,   3 );   /* Number of S-amp mag weights */
         memcpy( line2+93,  line1+101,  3 );   /* Number of duration mag weights */
         memcpy( line2+96,  line1+104, 46 );
         memcpy( line2+142, line1+151,  3 );   /* Total of preferred mag weights */
         memcpy( line2+145, line1+154,  4 );
         memcpy( line2+149, line1+159,  3 );   /* Total of alt coda dur mag weights */
         memcpy( line2+152, line1+162,  2 );
         line2[152] = ' ';  /* kludge for earlybird to work properly; */
                            /* overrides the previous line!           */

         line2[154] = '\0';
      }

/* Terminator lines begin with four blanks and don't change
   ********************************************************/
      else if ( strncmp( line1, "    ", 4 ) == 0 )
         strcpy( line2, line1 );

/* Otherwise, the line is a regular archive line
   *********************************************/
      else
      {
         char ts[10];
         double f;

         if ( len1 < 111 )                /* Archive line is too short */
         {
            free( workbuf );
            return -1;
         }

         memset( line2, ' ', 101 );
         memcpy( line2,   line1,    4 ); /* first 4 letters of station code */
         memcpy( line2+4, line1+13, 4 );
         memcpy( line2+8, line1+8,  1 );
         memcpy( line2+9, line1+19, 35 );

         memcpy( ts, line1+54, 7 );       /* Peak-to-peak amplitude */
         ts[7] = '\0';
         f = atof( ts ) / 100.;
         sprintf( ts, "%3.lf", f );
         memcpy( line2+44, ts, 3 );

         memcpy( line2+47, line1+63, 31 );

         memcpy( ts, line1+94, 3 );       /* Duration magnitude */
         ts[3] = '\0';
         f = atof( ts ) / 10.;
         sprintf( ts, "%2.lf", f );
         memcpy( line2+78, ts, 2 );

         memcpy( ts, line1+97, 3 );       /* Amplitude magnitude */
         ts[3] = '\0';
         f = atof( ts ) / 10.;
         sprintf( ts, "%2.lf", f );
         memcpy( line2+80, ts, 2 );

         memcpy( line2+82, line1+100, 8 );
         memcpy( line2+91, line1+108, 3 );

         memcpy( line2+94, line1+4, 1 );   /* 5th letter of station code */
         memcpy( line2+95, line1+9, 3 );   /* 3-letter component code */
         memcpy( line2+98, line1+5, 2 );   /* 2-letter network code */
         line2[100] = '\0';
      }

/* Save the modified line in workbuf and prepare to get
   the next line from buf
   ****************************************************/
      strcat( workbuf, line2 );
      strcat( workbuf, "\n" );

      inptr += len1 + 1;         /* Prepare to get the next line */
   }

/* Overwrite the original buffer with the work buffer
   **************************************************/
   strcpy( buf, workbuf );
   free( workbuf );
   return 0;
}

#ifdef DONT_COMPILE_THIS_CODE
            /*************************************************
             *  The following is a test program for cvarc()  *
             *************************************************/
#define MAXCHR 100000

int main( int argc, char *argv[] )
{
   static char buf[MAXCHR];
   int insize;

   printf( "Running cvarc test program\n" );

/* Read an arc message from standard input to buf
   **********************************************/
   insize = fread( buf, sizeof(char), MAXCHR, stdin );
   if ( insize >= MAXCHR )
   {
      printf( "Input arc message too large for buffer. Exiting.\n" );
      return -1;
   }
   buf[insize] = '\0';

/* Convert the arc message to pre-Y2K format.
   The input buffer is modified.
   *****************************************/
   if ( cvarc( buf, MAXCHR ) == -1 )
   {
      printf( "Can't convert the arc message. Exiting.\n" );
      return -1;
   }

/* Write the buffer to standard output
   ***********************************/
   fwrite( buf, sizeof(char), strlen(buf), stdout );

   return 0;
}
#endif
