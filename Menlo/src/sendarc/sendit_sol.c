
      /*************************************************************
       *                        sendit_sol.c                       *
       *************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <earthworm.h>
#include "sendarc.h"

#define BUFLEN 1000

/* Declared elsewhere
   ******************/
extern int  errno;               /* Contains socket error numbers */
extern char RemoteIpAddress[80]; /* IP address of system to receive arc msg's */
extern int  WellKnownPort;       /* The well-known port number */

static int  sd;                  /* Socket descriptor */


      /************************************************************
       *                    ConnectToEarlybird()                  *
       *                                                          *
       *          Get a connection to the remote system.          *
       ************************************************************/

int ConnectToEarlybird( void )
{
   struct sockaddr_in server;         /* Server socket address structure */
   const int optVal = 1;
   int rc;

/* Get a new socket descriptor
   ***************************/
   sd = socket( AF_INET, SOCK_STREAM, 0 );
   if ( sd == -1 )
   {
      logit( "et", "sendarc: socket() error: %s\n", strerror(errno) );
      return SENDARC_FAILURE;
   }

/* Allow reuse of socket addresses
   *******************************/
   if ( setsockopt( sd, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
                    sizeof(int) ) == -1 )
   {
      logit( "et", "sendarc: setsockopt() error: %s\n", strerror(errno) );
      SocketClose( sd );
      return SENDARC_FAILURE;
   }

/* Fill in socket address structure
   ********************************/
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family      = AF_INET;
   server.sin_port        = htons((unsigned short)WellKnownPort);
   server.sin_addr.s_addr = inet_addr( RemoteIpAddress );

/* Connect to the server.
   The connect call blocks for a while if Earlybird is not available.
   *****************************************************************/
   logit( "et", "sendarc: Connecting to %s", RemoteIpAddress );
   logit( "e", " port %d\n", WellKnownPort );
   if ( connect( sd, (struct sockaddr *)&server, sizeof(server) ) == -1 )
   {
      logit( "et", "sendarc: connect() error.\n" );
      SocketClose( sd );
      return SENDARC_FAILURE;
   }
   return SENDARC_SUCCESS;
}


       /************************************************************
        *                          ArcToEb                         *
        *                                                          *
        *   Send BUFLEN bytes at a time to the Earlybird system.   *
        ************************************************************/

int ArcToEb( char *arcFileName, char *arcbuf, int recsize )
{
   const  char stx = (char)2;     /* Ascii STX character */
   const  char etx = (char)3;     /* Ascii ETX character */
   static char buf[BUFLEN];
   int    i;
   int    nbuf  = recsize / BUFLEN;
   int    nleft = recsize % BUFLEN;
   char   *ptr = &arcbuf[0];

/* Write an STX character, followed by the arc file name to socket
   ***************************************************************/
   sprintf( buf, "%c%s\n", stx, arcFileName );
   if ( send( sd, buf, strlen(buf), NULL ) == -1 )
   {
      logit( "et", "sendarc: Error writing file name to socket: %s\n",
             strerror(errno) );
      SocketClose( sd );
      return SENDARC_FAILURE;
   }

/* Send the arc message, BUFLEN bytes at a time
   ********************************************/
   for ( i = 0; i < nbuf; i++ )
   {
      if ( send( sd, ptr, BUFLEN, NULL ) == -1 )
      {
         logit( "et", "sendarc: send() error: %s\n", strerror(errno) );
         SocketClose( sd );
         return SENDARC_FAILURE;
      }
      ptr += BUFLEN;
   }

/* Send any leftover bytes
   ***********************/
   if ( send( sd, ptr, nleft, NULL ) == -1 )
   {
      logit( "et", "sendarc: send() error: %s\n", strerror(errno) );
      SocketClose( sd );
      return SENDARC_FAILURE;
   }

/* Write an ETX character to the socket
   ************************************/
   if ( send( sd, &etx, sizeof(char), NULL ) == -1 )
   {
      logit( "et", "sendarc: Error sending ETX character: %s\n",
             strerror(errno) );
      SocketClose( sd );
      return SENDARC_FAILURE;
   }

/* Life is wonderful
   *****************/
   SocketClose( sd );
   return SENDARC_SUCCESS;
}
