#
# This is sendarc's parameter file

MyModuleId         MOD_SENDARC           # Module id for this instance of sendtrace
RingName           HYPO_RING             # Shared memory ring for input/output
LogFile            1                     # 0 to completely turn off disk log file
HeartBeatInterval  30                    # Seconds between heartbeats
LocalArcDir     /home/earthworm/run/arc  # For temporary storage of arc files
RemoteIpAddress    130.118.49.219        # IP address of system to receive arc msg's
WellKnownPort      1234                  # Well-known port number
RetryInterval      60                    # Retry send to ebird this often (sec)
MaxArcSize         50000                 # Max size of an arc message, in bytes
PreY2kArcFormat    0                     # If non-zero, convert arc messages from
                                         # Y2K to pre-Y2K format

#
# Get arc messages with this installation and module id.
# Only messages of type TYPE_HYP2000ARC are read.
# Attach the suffix "ArcSuffix" to the arc file names.
# Suffixes can be no more than five characters long.
#
#              Installation       Module       ArcSuffix
GetEventsFrom  INST_WILDCARD    MOD_EQPROC        arc
GetEventsFrom  INST_WILDCARD    MOD_EQPRELIM      ar1

