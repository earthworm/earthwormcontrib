
       /***************************************************************
        *                      getfilename_nt.c                       *
        *                                                             *
        *  Function to get the name of an arc file in the current     *
        *  directory.                                                 *
        *                                                             *
        *  Returns 0 if all ok                                        *
        *          1 if no arc files are found                        *
        ***************************************************************/

#include <stdio.h>
#include <string.h>
#include <windows.h>
#include "sendarc.h"

extern char  ArcSuffix[MAX_LOGO][MAX_SUFFIX];  /* Arc file suffix for each GetLogo */
extern short nLogo;                            /* Number of logo types to get from ring */
 
void logit( char *, char *, ... );             /* logit.c      sys-independent  */

int GetArcFileName( char fname[] )
{
   HANDLE          fileHandle;
   char            fileMask[80];
   WIN32_FIND_DATA findData;
   int             i;

   for ( i = 0; i < nLogo; i++ )
   {
      strcpy( fileMask, "*." );
      strcat( fileMask, ArcSuffix[i] );

      fileHandle = FindFirstFile( fileMask, &findData );

      if ( fileHandle != INVALID_HANDLE_VALUE )
      {
         strcpy( fname, findData.cFileName );
         FindClose( fileHandle );
         return SENDARC_SUCCESS;
      }

      FindClose( fileHandle );
   }
   return SENDARC_FAILURE;
}

