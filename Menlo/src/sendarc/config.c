
        /***************************************************************
         *                         config.c                            *
         *                                                             *
         *  Functions to read and log the configuration files.         *
         ***************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <transport.h>
#include <kom.h>
#include "sendarc.h"

#define MAX_LOGO    5            /* Max number of arc message logos */
#define MAX_SUFFIX  6            /* Max number of characters in arc file suffixes */

/* Functions in this source file
   *****************************/
void GetConfig( char * );
void LogConfig( void );
void Lookup( void );

/* Share these with the sender thread
   **********************************/
volatile int  MaxArcSize;             /* Max size of arc msg, in bytes */
volatile char LocalArcDir[80];        /* For temporary storage of arc files */
volatile char RemoteIpAddress[80];    /* IP address of system to receive trigger */
volatile int  WellKnownPort;          /* The well-known port number */
volatile int  RetryInterval;          /* If send fails, retry this often (msec) */
volatile int  PreY2kArcFormat;        /* If 1, convert arc messages to pre-Y2K format */

char     RingName[20];                /* Name of transport ring for i/o */
char     MyModName[20];               /* Speak as this module name/id */
int      LogFile;                     /* 0 if no logfile should be written */
long     HeartBeatInterval;           /* Seconds between heartbeats */
short    nLogo = 0;                   /* Number of logo types to get from ring */
MSG_LOGO GetLogo[MAX_LOGO];           /* Array for module,type,instid */
char     ArcSuffix[MAX_LOGO][MAX_SUFFIX];  /* Arc file suffix for each GetLogo */

/* Look these up in the earthworm.h tables with getutil functions
   **************************************************************/
long          RingKey;                /* Key of transport ring for i/o */
unsigned char InstId;                 /* Local installation id */
unsigned char MyModId;                /* Module Id for this program */
unsigned char TypeHeartBeat;
volatile unsigned char TypeErr;
unsigned char TypeArc;


/****************************************************************************
 *  GetConfig() processes command file(s) using kom.c functions;            *
 *                    exits if any errors are encountered.                  *
 ****************************************************************************/

#define NCOMMAND 11

void GetConfig( char *configfile )
{
   const    ncommand = NCOMMAND;    /* Process this many required commands */
   char     init[NCOMMAND];         /* Init flags, one for each command */
   int      nmiss;                  /* Number of missing commands */
   char     *com;
   char     *str;
   int      nfiles;
   int      success;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
        printf( "sendarc: Error opening command file <%s>. Exiting.\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         com = k_str();         /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )           continue;
         if ( com[0] == '#' )  continue;

/* Open a nested configuration file
   ********************************/
         if( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "sendarc: Error opening command file <%s>. Exiting.\n",
                        &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
  /*0*/  if ( k_its("LogFile") )
         {
            LogFile = k_int();
            init[0] = 1;
         }
  /*1*/  else if ( k_its("MyModuleId") )
         {
            str = k_str();
            if (str) strcpy( MyModName, str );
            init[1] = 1;
         }
  /*2*/  else if ( k_its("RingName") )
         {
            str = k_str();
            if (str) strcpy( RingName, str );
            init[2] = 1;
         }
  /*3*/  else if ( k_its("HeartBeatInterval") )
         {
            HeartBeatInterval = k_long();
            init[3] = 1;
         }
  /*4*/  else if ( k_its("MaxArcSize") )
         {
            MaxArcSize = k_int();
            init[4] = 1;
         }
  /*5*/  else if ( k_its("GetEventsFrom") )
         {
            if ( nLogo+1 > MAX_LOGO )
            {
               printf( "sendarc: Too many <GetEventsFrom> commands in <%s>",
                        configfile );
               printf( "; max=%d. Exiting.\n", MAX_LOGO );
               exit( -1 );
            }
            if ( ( str=k_str() ) )
            {
               if ( GetInst( str, &GetLogo[nLogo].instid ) != 0 )
               {
                  printf( "sendarc: Invalid installation name <%s>", str );
                  printf( " in <GetEventsFrom> cmd. Exiting.\n" );
                  exit( -1 );
               }
            }
            if ( ( str=k_str() ) )
            {
               if ( GetModId( str, &GetLogo[nLogo].mod ) != 0 )
               {
                  printf( "sendarc: Invalid module name <%s>", str );
                  printf( " in <GetEventsFrom> cmd. Exiting.\n" );
                  exit( -1 );
               }
            }
            if ( ( str=k_str() ) )       /* Truncate suffix to MAX_SUFFIX chars */
            {
               strncpy( ArcSuffix[nLogo], str, MAX_SUFFIX );
               ArcSuffix[nLogo][MAX_SUFFIX-1] = '\0';
            }
            if ( GetType( "TYPE_HYP2000ARC", &GetLogo[nLogo].type ) != 0 )
            {
               printf( "sendarc: Invalid message type <TYPE_HYP2000ARC>" );
               printf( ". Exiting.\n" );
               exit( -1 );
            }
            nLogo++;
            init[5] = 1;
         }
  /*6*/  else if ( k_its("LocalArcDir") )
         {
            str = k_str();
            if (str) strcpy( (char *)LocalArcDir, str );
            init[6] = 1;
         }
  /*7*/  else if ( k_its("RemoteIpAddress") )
         {
            str = k_str();
            if (str) strcpy( (char *)RemoteIpAddress, str );
            init[7] = 1;
         }
  /*8*/  else if ( k_its("WellKnownPort") )
         {
            WellKnownPort = k_int();
            init[8] = 1;
         }
  /*9*/  else if ( k_its("RetryInterval") )
         {
            RetryInterval = k_int();
            init[9] = 1;
         }
 /*10*/  else if ( k_its("PreY2kArcFormat") )
         {
            PreY2kArcFormat = k_int();
            init[10] = 1;
         }

/* Unknown command
   ***************/
         else
         {
            printf( "sendarc: <%s> Unknown command in <%s>.\n",
                     com, configfile );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "sendarc: Bad <%s> command in <%s>. Exiting.\n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
       printf( "sendarc: ERROR, no " );
       if ( !init[0]  ) printf( "<LogFile> " );
       if ( !init[1]  ) printf( "<MyModuleId> " );
       if ( !init[2]  ) printf( "<RingName> " );
       if ( !init[3]  ) printf( "<HeartBeatInterval> " );
       if ( !init[4]  ) printf( "<MaxArcSize> " );
       if ( !init[5]  ) printf( "<GetEventsFrom> " );
       if ( !init[6]  ) printf( "<LocalArcDir> " );
       if ( !init[7]  ) printf( "<RemoteIpAddress> " );
       if ( !init[8]  ) printf( "<WellKnownPort> " );
       if ( !init[9]  ) printf( "<RetryInterval> " );
       if ( !init[10] ) printf( "<PreY2kArcFormat> " );
       printf( "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }

   return;
}


/***************************************************************************
 *  LogConfig()   Print the configuration file parameters in the log file. *
 ***************************************************************************/

void LogConfig( void )
{
   int i;

   logit( "", "\n" );
   logit( "", "LogFile:           %d\n", LogFile );
   logit( "", "MyModuleId:        %s\n", MyModName );
   logit( "", "RingName:          %s\n", RingName );
   logit( "", "HeartBeatInterval: %d\n", HeartBeatInterval );
   logit( "", "MaxArcSize:        %d\n", MaxArcSize );
   logit( "", "LocalArcDir:       %s\n", LocalArcDir );
   logit( "", "RemoteIpAddress:   %s\n", RemoteIpAddress );
   logit( "", "WellKnownPort:     %d\n", WellKnownPort );
   logit( "", "RetryInterval:     %d\n", RetryInterval );
   logit( "", "PreY2kArcFormat:   %d\n", PreY2kArcFormat );
   for ( i = 0; i < nLogo; i++ )
   {
      logit( "", "GetEventsFrom:     instid:%u  mod:%u",
             GetLogo[i].instid, GetLogo[i].mod );
      logit( "", "  ArcSuffix: %s\n", ArcSuffix[i] );
   }
   logit( "", "\n" );
   return;
}


     /*****************************************************************
      *  Lookup()   Look up important info from earthworm.h tables    *
      *****************************************************************/

void Lookup( void )
{
   unsigned char type_error;

/* Look up keys to shared memory regions
   *************************************/
   if ( ( RingKey = GetKey(RingName) ) == -1 )
   {
        printf( "sendarc:  Invalid ring name <%s>. Exiting.\n", RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "sendarc: Error getting local installation id. Exiting.\n" );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 )
   {
      printf( "sendarc: Invalid module name <%s>. Exiting.\n", MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "sendarc: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      exit( -1 );
   }

   if ( GetType( "TYPE_ERROR", &type_error ) != 0 )
   {
      printf( "sendarc: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   TypeErr = type_error;

   if ( GetType( "TYPE_HYP2000ARC", &TypeArc ) != 0 )
   {
      printf( "sendarc: Invalid message type <TYPE_HYP2000ARC>. Exiting.\n" );
      exit( -1 );
   }
   return;
}
