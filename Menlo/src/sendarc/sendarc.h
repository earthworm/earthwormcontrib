
/*   sendarc.h    */
 
#define ERR_MISSMSG      0    /* Message missed in transport ring */
#define ERR_TOOBIG       1    /* Retrieved msg too large for buffer */
#define ERR_NOTRACK      2    /* Msg retrieved; tracking limit exceeded */
#define ERR_NOCONNECT    3    /* Can't connect to Earlybird */

#define SENDARC_FAILURE -1
#define SENDARC_SUCCESS  0

#define MAX_LOGO         5    /* Max arc message logos */
#define MAX_SUFFIX       6    /* Max characters in arc file suffixes */

