
           /********************************************************
            *                    sender_thread.c                   *
            ********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <ws_clientII.h>
#include "sendarc.h"

/* Declared volatile in config.c
   *****************************/
extern int  MaxArcSize;                /* Max size of arc msg, in bytes */
extern char LocalArcDir[80];           /* For storage of temporary arc files */
extern int  RetryInterval;             /* If send fails, retry this often (msec) */
extern unsigned char TypeErr;
extern int  PreY2kArcFormat;           /* If 1, convert arc messages to pre-Y2K format */

/* Function declarations
   *********************/
int cvarc( char *, int );              /* For converting arc messages to pre-Y2K format */
int GetArcFileName( char * );
int GetArcFile( char *, char *, int * );
int ConnectToEarlybird( void );
int ArcToEb( char *, char *, int );
void SendStatus( unsigned char, short, char * );



      /********************************************************************
       *                           SenderThread()                         *
       *                Thread function invoked by sendarc.               *
       ********************************************************************/

thr_ret SenderThread( void *junk )
{
   char *arcbuf;

/* Allocate the arc message buffer
   *******************************/
   arcbuf = (char *) malloc( (size_t)MaxArcSize );
   if ( arcbuf == NULL )
   {
      logit( "e", "SenderThread: Can't allocate arc message buffer. Exiting.\n" );
      exit( -1 );
   }

/* Change the working directory to LocalArcDir
   *******************************************/
   if ( chdir_ew( LocalArcDir ) < 0 )
   {
      logit( "et", "SenderThread: chdir_ew error.\n" );
      exit( -1 );
   }

/* Initialize the socket system.  This is just a stub
   in Solaris, but it does something in Windows NT.
   **************************************************/
   SocketSysInit();

/* Wait for arc files to show up and send them to Earlybird.
   If the file can't be sent, keep trying at regular intervals.
   ***********************************************************/
   while ( 1 )
   {
      char arcFileName[80];
      int  recsize;

      while ( GetArcFile( arcFileName, arcbuf, &recsize ) == SENDARC_SUCCESS )
      {

/* Convert the arc message to pre-Y2K format.
   The input buffer is modified.
   *****************************************/
         if ( PreY2kArcFormat )
         {
            arcbuf[recsize] = '\0';            /* Null-terminate the arc message */

            if ( cvarc( arcbuf, MaxArcSize ) == -1 )
            {
               logit( "et", "sendarc: Error. Can't convert arc message.\n" );
               logit( "", "%s", arcbuf );
               if ( remove( arcFileName ) == -1 )
                  logit( "et", "sendarc: Error erasing arc file: %s\n", arcFileName );
               continue;
            }
            recsize = strlen( arcbuf );
         }

         while ( 1 )
         {
            while ( ConnectToEarlybird() == SENDARC_FAILURE )
            {
               char errtxt[80];

               sprintf( errtxt, "Can't connect to Earlybird.  Will retry in %d sec.",
                  RetryInterval );
               SendStatus( TypeErr, ERR_NOCONNECT, errtxt );
               sleep_ew( 1000 * RetryInterval );
            }

            if ( ArcToEb( arcFileName, arcbuf, recsize ) == SENDARC_SUCCESS )
               break;

            logit( "et", "Can't send the arc file.  Will retry in %d sec...\n",
                   RetryInterval );
            sleep_ew( 1000 * RetryInterval );
         }

         logit( "et", "File %s sent to Earlybird (%u bytes)\n", arcFileName, recsize );

/* Erase the arc file
   ******************/
         if ( remove( arcFileName ) == -1 )
            logit( "et", "sendarc: Error erasing arc file: %s\n", arcFileName );
      }

      sleep_ew( 1000 );     /* Wait for more arc files to show up */
   }
}


     /******************************************************************
      *                         GetArcFile()                           *
      *                                                                *
      *  See if an arc file has shown up.  If so, read the contents    *
      *  into arcbuf and return the file size.                         *
      *  Return SENDARC_SUCCESS or SENDARC_FAILURE                     *
      ******************************************************************/

int GetArcFile( char *arcFileName, char *arcbuf, int *recsize )
{
   FILE *fp;
   int  rc;

/* Get the name of an arc
   **********************/
   if ( GetArcFileName( arcFileName ) == SENDARC_FAILURE )
      return SENDARC_FAILURE;

/* Read the contents of the file into arcbuf
   *****************************************/
   fp = fopen( arcFileName, "r" );
   if ( fp == NULL )
   {
      logit( "e", "sendevent: Can't open arc file <%s>\n", arcFileName );
      return SENDARC_FAILURE;
   }

   rc = fread( arcbuf, 1, MaxArcSize, fp );
   if ( rc == 0 )
   {
      logit( "e", "sendevent: Error reading arc file: <%s>\n", arcFileName );
      fclose( fp );
      return SENDARC_FAILURE;
   }

   *recsize = rc;
   fclose( fp );
   return SENDARC_SUCCESS;
}

