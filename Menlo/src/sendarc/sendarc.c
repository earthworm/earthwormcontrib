
  /***************************************************************************
   *                                sendarc.c                                *
   *                                                                         *
   *                This program feeds arc messages to Earlybird.            *
   *                                                                         *
   *  The main thread gets arc messages from a transport ring and writes     *
   *  them to disk files.  A second thread reads the arc messages from disk  *
   *  and sends them to Earlybird via a socket connection.                   *
   *                                                                         *
   *  WMK 8/10/98                                                            *
   ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include <time_ew.h>
#include "sendarc.h"

/* Function declarations
   *********************/
void GetConfig( char * );
void LogConfig( void );
void Lookup( void );
void SendStatus( unsigned char, short, char * );
void WriteArcMsg( char [], MSG_LOGO, int );
thr_ret SenderThread( void * );

/* Global variable
   ***************/
static SHM_INFO Region;              /* Shared memory region to use for i/o */

/* Things to read or derive from configuration file
   ************************************************/
extern char  RingName[20];           /* Name of transport ring for i/o */
extern char  MyModName[20];          /* Speak as this module name/id */
extern int   LogFile;                /* 0 if no logfile should be written */
extern long  HeartBeatInterval;      /* Seconds between heartbeats */
extern int   MaxArcSize;             /* Max size of arc msg, in bytes */
extern char  LocalArcDir[80];        /* For temporary storage of arc files */
extern short nLogo;                  /* Number of logo types to get from ring */
extern MSG_LOGO GetLogo[MAX_LOGO];   /* Array for module,type,instid */
extern char  ArcSuffix[MAX_LOGO][MAX_SUFFIX];  /* Arc file suffix for each GetLogo */

/* Look these up in the earthworm.h tables with getutil functions
   **************************************************************/
extern long          RingKey;        /* Key of transport ring for i/o */
extern unsigned char InstId;         /* Local installation id */
extern unsigned char MyModId;        /* Module Id for this program */
extern unsigned char TypeHeartBeat;
extern unsigned char TypeErr;
extern unsigned char TypeArc;

/* Share these with the thread function
   ************************************/
volatile char RemoteHost[40];        /* Name of the remote host */
volatile char RemoteUserId[40];      /* User name on remote host */

static   pid_t myPid;               /* Sent with heartbeat, for restart */


int main( int argc, char **argv )
{
   long         timeNow;             /* Current time */
   long         timeLastBeat;        /* Time last heartbeat was sent */
   int          res;
   char         *arcbuf;             /* Buffer to hold arc msg */
   char         *configFileName;
   unsigned     thread_id;           /* Id number of server thread */
   MSG_LOGO     reclogo;             /* Logo of retrieved message */
   long         recsize;             /* Size of retrieved message */

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      printf( "Usage: sendarc <configfile>\n" );
      return -1;
   }
   configFileName = argv[1];

/* Read the configuration file
   ***************************/
   GetConfig( configFileName );

/* Look up important info in the earthworm.h tables
   ************************************************/
   Lookup();

/* Create a new log file
   *********************/
   logit_init( configFileName, (short) MyModId, 60000, LogFile );

/* Log the configuration file parameters
   *************************************/
   LogConfig();

/* Allocate the arc message buffer
   *******************************/
   arcbuf = (char *) malloc( (size_t)MaxArcSize );
   if ( arcbuf == NULL )
   {
      logit( "e", "sendarc: Can't allocate arc message buffer. Exiting.\n" );
      return -1;
   }

/* Attach to Input/Output shared memory ring
   *****************************************/
   tport_attach( &Region, RingKey );
   logit( "", "sendarc: Attached to public memory region %s: %d\n",
          RingName, RingKey );

/* Flush the input ring
   ********************/
   while ( tport_getmsg( &Region, GetLogo, nLogo, &reclogo, &recsize,
                         arcbuf, MaxArcSize ) != GET_NONE );

/* Change the working directory to LocalArcDir
   *******************************************/
   if ( chdir_ew( LocalArcDir ) < 0 )
   {
      logit( "et", "sendarc: chdir_ew error.\n" );
      return -1;
   }

/* Start a new thread which will send arc files
   to a remote system using a socket connection
   ********************************************/
   if ( StartThread( SenderThread, (unsigned)0, &thread_id ) < 0 )
   {
      logit( "et", "sendarc: Can't start server thread.\n" );
      return -1;
   }

/* Force a heartbeat to be issued in first pass thru main loop
   ***********************************************************/
   timeLastBeat = time(&timeNow) - HeartBeatInterval - 1;

/* Main loop
   *********/
   while ( 1 )
   {

/* Beat the heart
   **************/
      if  ( (time(&timeNow) - timeLastBeat) >= HeartBeatInterval )
      {
          timeLastBeat = timeNow;
          SendStatus( TypeHeartBeat, 0, "" );
      }

/* See if termination has been requested
   *************************************/
      if ( tport_getflag( &Region ) == TERMINATE  ||
           tport_getflag( &Region ) == myPid )
         break;

/* Process all new messages before sleeping again
   **********************************************/
      do
      {
         char     Text[100];                /* String for log/error messages */

/* Get an arc message and check the getmsg() return code
   *****************************************************/
         res = tport_getmsg( &Region, GetLogo, nLogo, &reclogo, &recsize,
                             arcbuf, MaxArcSize );

         if ( res == GET_NONE )                  /* No more new messages */
            break;
         else if ( res == GET_TOOBIG )           /* Next message was too big */
         {
            sprintf( Text, "Retrieved msg[%ld] (i%u m%u t%u) too big for buffer[%d]",
                    recsize, reclogo.instid, reclogo.mod, reclogo.type,
                    MaxArcSize );
            SendStatus( TypeErr, ERR_TOOBIG, Text );
            continue;
         }
         else if ( res == GET_MISS )                   /* Got a msg, but missed some */
         {
            sprintf( Text, "Missed msg(s)  i%u m%u t%u  %s.",
                     reclogo.instid, reclogo.mod, reclogo.type, RingName );
            SendStatus( TypeErr, ERR_MISSMSG, Text );
         }
         else if ( res == GET_NOTRACK )                /* Got a message, but can't */
         {                                             /* tell if any were missed */
            sprintf( Text, "Msg received (i%u m%u t%u); NTRACK_GET exceeded",
                      reclogo.instid, reclogo.mod, reclogo.type );
            SendStatus( TypeErr, ERR_NOTRACK, Text );
         }

/* Process the arc message
   ***********************/
         WriteArcMsg( arcbuf, reclogo, (int)recsize );

      } while ( res != GET_NONE );              /* End of message-processing loop */

      sleep_ew( 250 );                          /* Wait for new messages to arrive */
   }

/* Terminate program
   *****************/
   tport_detach( &Region );
   logit( "t", "The termination flag was set.  Program stopping.\n" );
   return 0;
}


/*************************************************************************
 * SendStatus() builds a heartbeat or error message & puts it into       *
 *              shared memory.  Writes errors to log file & screen.      *
 *************************************************************************/

void SendStatus( unsigned char type, short ierr, char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   long     t;

   static int   first = 1;

/* The first time SendStatus is invoked, get the process id
   ********************************************************/
   if ( first )
   {

#ifdef _SOLARIS
      myPid = getpid();
      if ( myPid == -1 )
      {
         logit( "e", "sendarc: Cannot get my process id. Exiting.\n" );
         exit( 0 );
      }
#endif

#ifdef _WINNT
      myPid = _getpid();
#endif

      first = 0;
   }

/* Build the message
   *****************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if ( type == TypeHeartBeat )
      sprintf( msg, "%ld %ld\n", t, myPid );
   else if ( type == TypeErr )
   {
      sprintf( msg, "%ld %hd %s\n", t, ierr, note);
      logit( "et", "sendarc: %s\n", note );
   }
   else return;

   size = strlen( msg );

/* Write the message to shared memory
   **********************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
      if ( type == TypeHeartBeat )
         logit( "et", "sendarc: Error sending heartbeat.\n" );
      if ( type == TypeErr )
         logit( "et", "sendarc: Error sending error:%d.\n", ierr );
   }
   return;
}


/***************************************************************************
 *  WriteArcMsg()  Write an arc message to a disk file.                    *
 *                      The sender thread will read this file and send     *
 *                      it to a remote system.                             *
 ***************************************************************************/

void WriteArcMsg( char *arcbuf, MSG_LOGO reclogo, int recsize )
{
   char       fname[32];
   int        i;
   FILE       *fp;

/* Write the arc message to a temporary file
   *****************************************/
   fp = fopen( "temp", "w" );
   if ( fp  == NULL )
   {
      logit( "et", "sendarc: Error opening temporary arc file\n" );
      return;
   }

   if ( fwrite( arcbuf, sizeof(char), recsize, fp ) < (size_t)recsize )
   {
      logit( "et", "sendarc: Error writing arc message to temp file\n" );
      fclose( fp );
      return;
   }
   fclose( fp );

/* Figure out which suffix to use for the real file name
   *****************************************************/
   for ( i = 0; i < nLogo; i++ )
      if ( (GetLogo[i].instid == 0) || (GetLogo[i].instid == reclogo.instid) )
         if ( (GetLogo[i].mod == 0) || (GetLogo[i].mod == reclogo.mod) )
            break;

/* Create name for local file using the event origin time,
   last 2 digits of the event id, and SUFFIX read from the
   configuration file command ArcSuffix.
   Example:  199601211324_98.arc   yyyymmddhhmm_xx.SUFFIX
   *******************************************************/
   strncpy( fname,    arcbuf,     12 );
   fname[12] = '_';
   strncpy( fname+13, arcbuf+recsize-3,  2 );
   fname[15] = '.';
   strcpy ( fname+16, ArcSuffix[i]   );

/* Replace blanks in the file name with zeros
   ******************************************/
   for ( i = 0; i < (int)strlen(fname); i++ )
       if ( fname[i] == ' ' )
          fname[i] = '0';

/* Rename the temporary file to it's real name
   *******************************************/
   rename( "temp", fname );
   return;
}
