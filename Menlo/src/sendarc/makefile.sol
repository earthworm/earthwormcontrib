#
#                      Make file for sendarc
#                         Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = sendarc.o config.o sender_thread.o getfilename_sol.o cvarc.o \
    sendit_sol.o $L/logit_mt.o $L/kom.o $L/getutil.o $L/sleep_ew.o \
    $L/time_ew.o $L/transport.o $L/dirops_ew.o $L/threads_ew.o \
   $L/sema_ew.o $L/socket_ew.o $L/socket_ew_common.o $L/chron3.o


sendarc: $O
	cc -o $B/sendarc $O -mt -lm -lsocket -lnsl -lposix4 -lthread -lc

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/sendarc*
