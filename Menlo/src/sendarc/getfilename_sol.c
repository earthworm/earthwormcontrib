
       /***************************************************************
        *                      getfilename_sol.c                      *
        *                                                             *
        *  Function to get the name of an arc file in the current     *
        *  directory.                                                 *
        *                                                             *
        *  Returns 0 if all ok                                        *
        *          1 if no arc files are found                        *
        ***************************************************************/

#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include "sendarc.h"

extern char  LocalArcDir[80];
extern char  ArcSuffix[MAX_LOGO][MAX_SUFFIX];  /* Arc file suffix for each GetLogo */
extern short nLogo;                            /* Number of logo types to get from ring */

void logit( char *, char *, ... );             /* logit.c      sys-independent  */

int GetArcFileName( char fname[] )
{
   DIR           *dp;
   struct dirent *dentp;

   dp = opendir( LocalArcDir );
   if ( dp == NULL )
   {
      logit( "et", "sendarc: Can't open the arc directory.\n" );
      return SENDARC_FAILURE;
   }

   while ( dentp = readdir(dp) )
   {
      int i;

      for ( i = strlen(dentp->d_name)-1; i >= 0; i-- )   /* Look for the last "." */
      {
         if ( dentp->d_name[i] == '.' )
         {
            char *p1 = &dentp->d_name[i+1];             /* Get the suffix address */
            int  j;

            for ( j = 0; j < nLogo; j++ )     /* Do we want files with this suffix? */
            {
               char *p2 = &ArcSuffix[j][0];

               if ( strcmp(p1, p2) == 0 )
               {
                  strcpy( fname, dentp->d_name );
                  closedir( dp );
                  return SENDARC_SUCCESS;
               }
            }
         }
      }
   }
   closedir( dp );
   return SENDARC_FAILURE;
}
