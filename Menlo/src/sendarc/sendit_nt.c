
   /*************************************************************
    *                        sendit_nt.c                        *
    *************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <windows.h>
#include "sendarc.h"

#define BUFLEN 1000

static char buf[BUFLEN];

/* Declared elsewhere
   ******************/
extern char RemoteIpAddress[80];       /* IP address of system to receive arc msg */
extern int  WellKnownPort;             /* The well-known port number */

static SOCKET sd;                      /* Socket identifier */


         /************************************************************
          *                    ConnectToEarlybird()                  *
          *                                                          *
          *          Get a connection to the remote system.          *
          ************************************************************/

int ConnectToEarlybird( void )
{
   struct sockaddr_in server;         /* Server socket address structure */
   const int optVal = 1;

/* Get a new socket descriptor
   ***************************/
   sd = socket( AF_INET, SOCK_STREAM, 0 );
   if ( sd == INVALID_SOCKET )
   {
      logit( "et", "sendarc: socket() error: %d\n", WSAGetLastError() );
      return SENDARC_FAILURE;
   }

/* Allow reuse of socket addresses
   *******************************/
   if ( setsockopt( sd, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal,
                    sizeof(int) ) == SOCKET_ERROR )
   {
      logit( "et", "sendarc: setsockopt() error: %d\n", WSAGetLastError() );
      SocketClose( sd );
      return SENDARC_FAILURE;
   }

/* Fill in socket address structure
   ********************************/
   memset( (char *)&server, '\0', sizeof(server) );
   server.sin_family      = AF_INET;
   server.sin_port        = htons( (unsigned short)WellKnownPort );
   server.sin_addr.s_addr = inet_addr( RemoteIpAddress );

/* Connect to the server.  The connect call blocks if Earlybird is not available.
   The "connect" variable is monitored by the other thread.
   If "connect" is set for 1 for a long time, an error is reported to statmgr.
   *****************************************************************************/
   logit( "et", "Connecting to %s port %d\n", RemoteIpAddress, WellKnownPort );
   if ( connect( sd, (struct sockaddr *)&server, sizeof(server) )
        == SOCKET_ERROR )
   {
      int rc = WSAGetLastError();
      if ( rc == WSAETIMEDOUT )
         logit( "et", "sendarc: connect() returned WSAETIMEDOUT\n" );
      else
         logit( "et", "sendarc: connect() error: %d\n", rc );
      SocketClose( sd );
      return SENDARC_FAILURE;
   }
   return SENDARC_SUCCESS;
}


       /************************************************************
        *                          ArcToEb                         *
        *                                                          *
        *   Send BUFLEN bytes at a time to the Earlybird system.   *
        ************************************************************/

int ArcToEb( char *arcFileName, char *arcbuf, int recsize )
{
   const  char stx = (char)2;     /* Ascii STX character */
   const  char etx = (char)3;     /* Ascii ETX character */
   static char buf[BUFLEN];
   int    i;
   int    nbuf  = recsize / BUFLEN;
   int    nleft = recsize % BUFLEN;
   char   *ptr = &arcbuf[0];

/* Write an STX character, followed by the arc file name to socket
   ***************************************************************/
   sprintf( buf, "%c%s\n", stx, arcFileName );
   if ( send( sd, buf, strlen(buf), 0 ) == SOCKET_ERROR )
   {
      logit( "et", "sendarc: Error writing file name to socket: %s\n",
             WSAGetLastError() );
      SocketClose( sd );
      return SENDARC_FAILURE;
   }

/* Send the arc message, BUFLEN bytes at a time
   ********************************************/
   for ( i = 0; i < nbuf; i++ )
   {
      if ( send( sd, ptr, BUFLEN, 0 ) == SOCKET_ERROR )
      {
         logit( "et", "sendarc: send() error: %s\n", WSAGetLastError() );
         SocketClose( sd );
         return SENDARC_FAILURE;
      }
      ptr += BUFLEN;
   }

/* Send any leftover bytes
   ***********************/
   if ( send( sd, ptr, nleft, 0 ) == SOCKET_ERROR )
   {
      logit( "et", "sendarc: send() error: %s\n", WSAGetLastError() );
      SocketClose( sd );
      return SENDARC_FAILURE;
   }

/* Write an ETX character to the socket
   ************************************/
   if ( send( sd, &etx, sizeof(char), 0 ) == SOCKET_ERROR )
   {
      logit( "et", "sendarc: Error sending ETX character: %s\n",
             WSAGetLastError() );
      SocketClose( sd );
      return SENDARC_FAILURE;
   }

/* Life is wonderful
   *****************/
   SocketClose( sd );
   return SENDARC_SUCCESS;
}

