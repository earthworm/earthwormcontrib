/******************************************************************************
 *  k2dusub.c                                                                 *
 *  Read a K2-format .evt data file (from the NSMP system),                   *
 *  create SNW Collection Agent messages, and place them in a directory       *
 *  for the snwclient.  Returns number of TYPE_STRONGMOTION messages written  *
 *  to ring  from file.                                                       *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include <chron3.h>
#include <rw_strongmotion.h>
#include "sm_file2ew.h"

#include "nkwhdrs_jhl.h"              /* Kinemetrics header definitions */

#ifdef _INTEL
/*  macro returns byte-swapped version of given 16-bit unsigned integer */
#define BYTESWAP_UINT16(var) \
               ((unsigned short)(((unsigned short)(var)>>(unsigned char)8) + \
               ((unsigned short)(var)<<(unsigned char)8)))

/*  macro returns byte-swapped version of given 32-bit unsigned integer */
#define BYTESWAP_UINT32(var) \
                 ((unsigned long)(((unsigned long)(var)>>(unsigned char)24) + \
        (((unsigned long)(var)>>(unsigned char)8)&(unsigned long)0x0000FF00)+ \
        (((unsigned long)(var)<<(unsigned char)8)&(unsigned long)0x00FF0000)+ \
                                 ((unsigned long)(var)<<(unsigned char)24)))
#endif

#ifdef _SPARC
#define BYTESWAP_UINT16(var) (var)
#define BYTESWAP_UINT32(var) (var)
#endif

#define MAX_REC  4096      
#define PACKET_MAX_SIZE  3003      /* 2992 + 8 + 3, same as QT */
#define MAXTRACELTH 20000
#define SWAP(a,b) temp=(a);(a)=(b);(b)=temp
#define DECADE   315532800  /* Number of seconds betweeen 1/1/1970 and 1/1/1980 */
#define NAM_LEN 100	          /* length of full directory name          */
#define BMSG_LEN 15000

/* Functions in this source file 
 *******************************/
int nsmp2ew( FILE *fp, char *fname, char *msg );
int build_snw( char *msg );
int Secs2DateTime(int nsecs, char *string2);
int read_head( FILE *fp );

void logit( char *, char *, ... );   /* logit.c      sys-independent  */

/* Globals from sm_file2ew.c
 ***************************/
extern CHANNELNAME ChannelName[MAXCHAN];  /* table for box/chan to SCNL conversion */
extern int     NchanNames;          /* number of channel names in table      */
extern char    OutputDir[NAM_LEN];  /* directory to put SNW messages     */
extern int     LabelSwitch;         /* 0: net-site 1: site-s/n           */
extern int     Debug;
extern char    sitehist[50];

char  station[50];

static int   print_head = 0;
KFF_TAG      tag;          /* received header tag */
MW_HEADER    head;
K2_HEADER    khead;
FRAME_HEADER frame;
static unsigned char g_k2mi_buff[PACKET_MAX_SIZE-9]; /* received data buffer   */

/******************************************************************************
 * nsmp2ew()  Read and process the .evt file.                                 *
 ******************************************************************************/
int nsmp2ew( FILE *fp, char *fname, char *Bmsg )
{
    char     whoami[50], serial[10], stid[10], fnew[90], aname[100], msg[200];
    FILE     *fx;      
    int      i, j, k, ii, nscans, nmsg, unknown;  
    int      tlen, flen, nchans, maxchans, npts, stat;
    unsigned long  channels, minute, decade;
    double   dt, secs, sex;
    double   sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's        */
                                        /* time 0 and 1970-01-01 00:00:00.0 GMT    */
    struct Greg  g;

/* Initialize variables 
 **********************/
    sprintf(whoami, " %s: %s: ", "k2rtagent", "nsmp2ew");
    memset( &frame,  0, sizeof(FRAME_HEADER) );
    memset( &tag,    0, sizeof(KFF_TAG) );
    memset( &head,   0, sizeof(K2_HEADER) );

    decade = (365*10+2)*24*60*60;     /*  Kinemetric time starts 10 years after EW time  */

/* Close/reopen file in binary read
 **********************************/
    fclose( fp );
    fp = fopen( fname, "rb" );
    if( fp == NULL )               return 0;
    stat = 1;
    if(strstr(fname, ".evt") != 0) stat = 0;
    if(strstr(fname, ".EVT") != 0) stat = 0;
    stat = 0;
    if(stat) return 0;

    nmsg = 0;
        
/* First, just read the header & first frame to get some basic info. 
 ******************************************************************/
    rewind(fp);
    
    stat = read_head(fp);
    
    if(head.roParms.stream.flags != 0) {
        logit("e", "%s %s not data. flags = %d \n", 
                    whoami, fname, head.roParms.stream.flags);
    }
    
	strcpy(sitehist, "unknown");
    sprintf(serial, "%d", head.rwParms.misc.serialNumber);
	if(Debug)logit( "e", "%s: %s <%s> <%s>  %d\n", 
			 whoami, fname, head.rwParms.misc.stnID, serial,  NchanNames);
    unknown = 1;
    for(ii=0;ii<NchanNames;ii++) {
        if(strcmp(serial, ChannelName[ii].box) == 0 && 
           strcmp(head.rwParms.misc.stnID, ChannelName[ii].sta) == 0) {
			if(Debug)logit( "e", "%s: Match found %d %s %s %s.\n",  
			   whoami, ii, serial, ChannelName[ii].box, ChannelName[ii].sta );
			if(LabelSwitch == 0) 
				sprintf(station, "%s-%s:", ChannelName[ii].net, ChannelName[ii].sta);
			else if(LabelSwitch == 1)
				sprintf(station, "%s-%s:", ChannelName[ii].sta, ChannelName[ii].box);
			else if(LabelSwitch == 2)
				sprintf(station, "%s-%s.%s:", ChannelName[ii].net, ChannelName[ii].sta, ChannelName[ii].box);
			
			strcpy(sitehist, station);
			unknown = 0;
       }
    }
    if(unknown) {
        sprintf(Bmsg, "NSMP site %s (%s) unknown to database. (%s) \n", 
        		head.rwParms.misc.stnID, head.rwParms.misc.comment, fname);
        logit("e", "%s %s  \n", whoami, Bmsg);
		return( -1 );
    } 
    
	sprintf(Bmsg, "%s1:UsageLevel=3\n", station );
	
	ii = build_snw(Bmsg);
	if(Debug) {
		if(ii) {
			logit("e", "k2rtagent: Messages for file: %s  site %s  \n ", 
						fname, head.rwParms.misc.stnID);
			fprintf( stdout,  "%s  \n", Bmsg);
		}
		else {
			logit("e", "k2rtagent: No changes for file: %s   \n", fname);
		}
	}
	
	sprintf(aname, "%s%s.%d", OutputDir, head.rwParms.misc.stnID, head.rwParms.misc.serialNumber);
	if(Debug) logit("e", "%s Output file: %s   \n", whoami, aname);
	fx = fopen( aname, "w" );
	if( fx == NULL ) {
		logit( "e", "%s: trouble opening output file: %s \n",
			  whoami, aname ); 
	} else {
		fwrite(Bmsg, 1, strlen(Bmsg), fx);
		fclose(fx);
	}
    
        
    return( 1 );
}

/******************************************************************************
 * build_snw(fp)  Build the SNW message from the header and other stuff.      *
 ******************************************************************************/
int build_snw( char *msg )
{
	char	string[150], string2[30], aname[100];
	double	tolerance = 0.00002;
	double	elev_err = 10.0;
	double  d1;
	int     i, nchans, stat, bit[10];
   
	stat = 0;
    
    nchans = head.roParms.misc.maxChannels;
    
    /*
    logit("e", "MISC_RO_PARMS: %d %d %d %hu %hu %hu %hu %hu %hu %d %hu %hu %d \n", 
        (int)head.roParms.misc.a2dBits, 
        (int)head.roParms.misc.sampleBytes, 
        (int)head.roParms.misc.restartSource, 
            head.roParms.misc.installedChan,
            head.roParms.misc.maxChannels,
            head.roParms.misc.sysBlkVersion,
            head.roParms.misc.bootBlkVersion,
            head.roParms.misc.appBlkVersion,
            head.roParms.misc.dspBlkVersion,
            head.roParms.misc.batteryVoltage,
            head.roParms.misc.crc,
            head.roParms.misc.flags,
            head.roParms.misc.temperature );
    */
    /*
	
	d1 = fabs(head.roParms.misc.batteryVoltage/10.0);
	sprintf(string, "%s2:K2 Internal Battery Voltage=%.2f;UsageLevel=3\n", station, d1 );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	
	d1 = fabs(head.roParms.misc.temperature/10.0);
	sprintf(string, "%s1:K2 Temperature in Celsius=%.2f\n", station, d1 );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    */
	
	d1 = fabs(head.roParms.misc.sysBlkVersion/100.0);
	sprintf(string, "%s1:Software Version-System=%.2f\n", station, d1 );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	
	d1 = fabs(head.roParms.misc.bootBlkVersion/100.0);
	sprintf(string, "%s1:Software Version-Boot=%.2f\n", station, d1 );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	
	d1 = fabs(head.roParms.misc.appBlkVersion/100.0);
	sprintf(string, "%s1:Software Version-Application=%.2f\n", station, d1 );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	
	d1 = fabs(head.roParms.misc.dspBlkVersion/100.0);
	sprintf(string, "%s1:Software Version-DSP=%.2f\n", station, d1 );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
    /*
    logit("e", "TIMING_RO_PARMS: %d %d %d %hu %hu %d %d %d %d %hu %d %d %lu %lu %lu %lu %lu %lu \n", 
        (int)head.roParms.timing.clockSource, 
        (int)head.roParms.timing.gpsStatus, 
        (int)head.roParms.timing.gpsSOH, 
            head.roParms.timing.gpsLockFailCount, 
            head.roParms.timing.gpsUpdateRTCCount, 
            head.roParms.timing.acqDelay, 
            head.roParms.timing.gpsLatitude, 
            head.roParms.timing.gpsLongitude, 
            head.roParms.timing.gpsAltitude, 
            head.roParms.timing.dacCount,
            head.roParms.timing.gpsLastDrift[0], 
            head.roParms.timing.gpsLastDrift[1], 
            
            head.roParms.timing.gpsLastTurnOnTime[0], 
            head.roParms.timing.gpsLastTurnOnTime[1], 
            head.roParms.timing.gpsLastUpdateTime[0], 
            head.roParms.timing.gpsLastUpdateTime[1], 
            head.roParms.timing.gpsLastLockTime[0], 
            head.roParms.timing.gpsLastLockTime[1] );
    */
	
  /* Clock Source
     0 = RTC from cold start
	 1 = keyboard
	 2 = Sync w/ ext. ref. pulse
	 3 = Internal GPS
	 */
	sprintf(string, "%s1:Clock Source=%d\n", station, head.roParms.timing.clockSource );
	logit("e", "%s", string);
	if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	
	/* GPS Status
	   Bit 0=1 if currently checking for presence of GPS board
	   Bit 1=1 if GPS board present
	   Bit 2=1 if error communicating with GPS
	   Bit 3=1 if failed to lock within an allotted time (gpsMaxTurnOnTime)
	   Bit 4=1 if not locked
	   Bit 5=1 when GPS power is ON
	   Bits 6,7=undefined
	 */
	for(i=0;i<8;i++) {
		bit[i] = (head.roParms.timing.gpsStatus>>i) & 1;
	}
	logit("e", "%d  %d  %d  %d  %d  %d  %d  ", head.roParms.timing.gpsStatus, bit[0], bit[1], bit[2], bit[3], bit[4], bit[5]);
	if(bit[1]==1) {
		stat = 0;
		if(bit[0]==1) stat += 1;
		if(bit[5]==0) stat += 2;
		if(bit[2]==1) stat += 4;
		if(bit[3]==1) stat += 8;
		if(bit[4]==1) stat += 16;
		sprintf(string, "%s1:GPS Status=%d\n", station, stat );
		logit("e", "%s", string);
		if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
    
	  /* GPS State of Health; same as Acutime SOH code 
		 0 = Normal
		 1 = No GPS signal
		 3 = PDOP (Position Dilution of Precision) is too high
		 9 = Only 1 usable satellite
		 10 = Only 2 usable satellites
		 11 = Only 3 usable satellites
		 12 = Chosen satellite is unusable
		 */
		sprintf(string, "%s1:GPS State of Health=%d\n", station, head.roParms.timing.gpsSOH );
		logit("e", "%s", string);
		if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		
		sprintf(string, "%s1:GPS Lock Failures=%d\n", station, head.roParms.timing.gpsLockFailCount );
		logit("e", "%s", string);
		if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		
		sprintf(string, "%s1:GPS RTC Update=%d\n", station, head.roParms.timing.gpsUpdateRTCCount );
		logit("e", "%s", string);
		if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		
		sprintf(string, "%s1:GPS RTC Drift (msec)=%d\n", station, head.roParms.timing.gpsLastDrift[0] );
		logit("e", "%s", string);
		if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		
		Secs2DateTime(head.roParms.timing.gpsLastTurnOnTime[0], string2);
		sprintf(string, "%s1:GPS Last Turn On=%s\n", station, string2 );
		logit("e", "%s", string);
		if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		
		Secs2DateTime(head.roParms.timing.gpsLastUpdateTime[0], string2);
		sprintf(string, "%s1:GPS Last Update=%s\n", station, string2 );
		logit("e", "%s", string);
		if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
		
		Secs2DateTime(head.roParms.timing.gpsLastLockTime[0], string2);
		sprintf(string, "%s1:GPS Last Lock=%s\n", station, string2 );
		logit("e", "%s", string);
		if((strlen(msg)+strlen(string)+10)<BMSG_LEN) strcat(msg, string);
	}
    
    
    for(i=0;i<nchans;i++) {
        /*
        logit("e", "CHANNEL_RO_PARMS: %d %lu %d %lu %d %d \n", 
            head.roParms.channel[i].maxPeak,  
            head.roParms.channel[i].maxPeakOffset,  
            head.roParms.channel[i].minPeak,  
            head.roParms.channel[i].minPeakOffset,  
            head.roParms.channel[i].mean,  
            head.roParms.channel[i].aqOffset );
        */
    }
    
        
    /*
    logit("e", "STREAM_RO_PARMS: %d %d %d %d %d %d %d %d \n", 
            head.roParms.stream.startTime,  
            head.roParms.stream.triggerTime,  
            head.roParms.stream.duration,  
            head.roParms.stream.errors,  
            head.roParms.stream.flags,  
            head.roParms.stream.startTimeMsec,  
            head.roParms.stream.triggerTimeMsec,  
            head.roParms.stream.nscans  );
    */
    /*
    logit("e", "MISC_RW_PARMS: %hu %hu %.5s %.33s %d %f %f %d %d %d %d %d %lo %d %.17s %d %d\n", 
            head.rwParms.misc.serialNumber,
            head.rwParms.misc.nchannels,
            head.rwParms.misc.stnID,  
            head.rwParms.misc.comment, 
            head.rwParms.misc.elevation,
            head.rwParms.misc.latitude, 
            head.rwParms.misc.longitude,
            
        (int)head.rwParms.misc.cutlerCode, 
        (int)head.rwParms.misc.minBatteryVoltage, 
        (int)head.rwParms.misc.cutler_decimation, 
        (int)head.rwParms.misc.cutler_irig_type, 
            head.rwParms.misc.cutler_bitmap,
            head.rwParms.misc.channel_bitmap,
        (int)head.rwParms.misc.cutler_protocol, 
            head.rwParms.misc.siteID, 
        (int)head.rwParms.misc.externalTrigger, 
        (int)head.rwParms.misc.networkFlag );
    */
    
    /*
    logit("e", "TIMING_RW_PARMS: %d %d %hu \n", 
        (int)head.rwParms.timing.gpsTurnOnInterval,  
        (int)head.rwParms.timing.gpsMaxTurnOnTime, 
            head.rwParms.timing.localOffset  );
    	*/
        
    for(i=0;i<nchans;i++) {
        /*
        logit("e", "CHANNEL_RW_PARMS: %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f %f %f %f %f %f %f %f %c %c %s %s \n", 
            head.rwParms.channel[i].id,  
            head.rwParms.channel[i].sensorSerialNumberExt,  
            head.rwParms.channel[i].north,  
            head.rwParms.channel[i].east,  
            head.rwParms.channel[i].up,  
            head.rwParms.channel[i].altitude,
            head.rwParms.channel[i].azimuth,
            head.rwParms.channel[i].sensorType,
            head.rwParms.channel[i].sensorSerialNumber,
            head.rwParms.channel[i].gain,
        (int)head.rwParms.channel[i].triggerType,  
        (int)head.rwParms.channel[i].iirTriggerFilter,  
        (int)head.rwParms.channel[i].StaSeconds,  
        (int)head.rwParms.channel[i].LtaSeconds,  
            head.rwParms.channel[i].StaLtaRatio,
        (int)head.rwParms.channel[i].StaLtaPercent,
          
            head.rwParms.channel[i].fullscale,
            head.rwParms.channel[i].sensitivity,
            head.rwParms.channel[i].damping,
            head.rwParms.channel[i].naturalFrequency,
            head.rwParms.channel[i].triggerThreshold,
            head.rwParms.channel[i].detriggerThreshold,
            head.rwParms.channel[i].alarmTriggerThreshold
            head.rwParms.channel[i].calCoil,
            head.rwParms.channel[i].range,
            head.rwParms.channel[i].sensorgain,
            head.rwParms.channel[i].networkcode,
            head.rwParms.channel[i].locationcode
        );
    	*/
    }
    
    
    /*
    logit("e", "STREAM_K2_RW_PARMS: |%d| |%d| |%d| %d %d %d %d %d %d %d %d %d %d %d %d %d %d %lu\n", 
        (int)head.rwParms.stream.filterFlag,  
        (int)head.rwParms.stream.primaryStorage,  
        (int)head.rwParms.stream.secondaryStorage,  
            head.rwParms.stream.eventNumber,  
            head.rwParms.stream.sps,  
            head.rwParms.stream.apw,  
            head.rwParms.stream.preEvent,  
            head.rwParms.stream.postEvent,  
            head.rwParms.stream.minRunTime,  
            head.rwParms.stream.VotesToTrigger,
            head.rwParms.stream.VotesToDetrigger,
        (int)head.rwParms.stream.FilterType,  
        (int)head.rwParms.stream.DataFmt,  
            head.rwParms.stream.Timeout,
            head.rwParms.stream.TxBlkSize,
            head.rwParms.stream.BufferSize,
            head.rwParms.stream.SampleRate,
            head.rwParms.stream.TxChanMap );
		*/
    
    
    return stat;
}


/******************************************************************************
 * Secs2DateTime(fp)  Put epoch seconds into date/time string.                *
 ******************************************************************************/
int Secs2DateTime(int nsecs, char *string2)
{
    int        i, j;
    double   sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's        */
                                        /* time 0 and 1970-01-01 00:00:00.0 GMT    */
    struct Greg  g;
    double   secs, sex;
    unsigned long  minute, decade;
    
    decade = (365*10+2)*24*60*60;     /*  Kinemetric time starts 10 years after EW time  */
    secs = nsecs;
    secs += decade;
    secs += sec1970;
    minute = (long) (secs / 60.0);
    /**/
    sex = secs - 60.0 * minute;
    i = sex;
    j = (sex - i)*1000;
    
    grg(minute, &g);
    sprintf(string2, "%.2d/%.2d/%4d %.2d:%.2d:%05.2f", 
                  g.month, g.day, g.year, g.hour, g.minute, sex);

    return 0;
}



/******************************************************************************
 * read_head(fp)  Read the file header, swap bytes if necessary, and print.   *
 ******************************************************************************/
int read_head( FILE *fp )
{
   long       la;
   int        i, nchans, stat;
   
/* Read in the file header.
   If a K2, there will be 2040 bytes.
 ************************************/
    /*
    stat = fread(&head, MAX_K2_CHANNELS, 1, fp);
    */
    nchans = MAX_K2_CHANNELS;
    stat = fread(&head, 1, 8, fp);
    stat = fread(&head.roParms.misc,       1, sizeof(struct MISC_RO_PARMS)+sizeof(struct TIMING_RO_PARMS), fp);
    stat = fread(&head.roParms.channel[0], 1, sizeof(struct CHANNEL_RO_PARMS)*nchans, fp);
    stat = fread(&head.roParms.stream,     1, sizeof(struct STREAM_RO_PARMS), fp);
    
    stat = fread(&head.rwParms.misc,       1, sizeof(struct MISC_RW_PARMS)+sizeof(struct TIMING_RW_PARMS), fp);
    stat = fread(&head.rwParms.channel[0], 1, sizeof(struct CHANNEL_RW_PARMS)*nchans, fp);
    
    stat = fread(&head.rwParms.stream,     1, sizeof(struct STREAM_K2_RW_PARMS), fp);
    
    stat = fread(&head.rwParms.modem,      1, sizeof(struct MODEM_RW_PARMS), fp);
    
    head.roParms.headerVersion = BYTESWAP_UINT16(head.roParms.headerVersion);
    head.roParms.headerBytes   = BYTESWAP_UINT16(head.roParms.headerBytes);
    
    if(Debug && print_head)
    logit("e", "HEADER: %c%c%c %d %hu %hu \n", 
            head.roParms.id[0], head.roParms.id[1], head.roParms.id[2], 
       (int)head.roParms.instrumentCode, 
            head.roParms.headerVersion, 
            head.roParms.headerBytes);
    
    head.roParms.misc.installedChan  = BYTESWAP_UINT16(head.roParms.misc.installedChan);
    head.roParms.misc.maxChannels    = BYTESWAP_UINT16(head.roParms.misc.maxChannels);
    head.roParms.misc.sysBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.sysBlkVersion);
    head.roParms.misc.bootBlkVersion = BYTESWAP_UINT16(head.roParms.misc.bootBlkVersion);
    head.roParms.misc.appBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.appBlkVersion);
    head.roParms.misc.dspBlkVersion  = BYTESWAP_UINT16(head.roParms.misc.dspBlkVersion);
    head.roParms.misc.batteryVoltage = BYTESWAP_UINT16(head.roParms.misc.batteryVoltage);
    head.roParms.misc.crc            = BYTESWAP_UINT16(head.roParms.misc.crc);
    head.roParms.misc.flags          = BYTESWAP_UINT16(head.roParms.misc.flags);
    head.roParms.misc.temperature    = BYTESWAP_UINT16(head.roParms.misc.temperature);
    nchans = head.roParms.misc.maxChannels;
    
    if(Debug && print_head)
    logit("e", "MISC_RO_PARMS: %d %d %d %hu %hu %hu %hu %hu %hu %d %hu %hu %d \n", 
        (int)head.roParms.misc.a2dBits, 
        (int)head.roParms.misc.sampleBytes, 
        (int)head.roParms.misc.restartSource, 
            head.roParms.misc.installedChan,
            head.roParms.misc.maxChannels,
            head.roParms.misc.sysBlkVersion,
            head.roParms.misc.bootBlkVersion,
            head.roParms.misc.appBlkVersion,
            head.roParms.misc.dspBlkVersion,
            head.roParms.misc.batteryVoltage,
            head.roParms.misc.crc,
            head.roParms.misc.flags,
            head.roParms.misc.temperature );
    
    head.roParms.timing.gpsLockFailCount  = BYTESWAP_UINT16(head.roParms.timing.gpsLockFailCount);
    head.roParms.timing.gpsUpdateRTCCount = BYTESWAP_UINT16(head.roParms.timing.gpsUpdateRTCCount);
    head.roParms.timing.acqDelay          = BYTESWAP_UINT16(head.roParms.timing.acqDelay);
    head.roParms.timing.gpsLatitude       = BYTESWAP_UINT16(head.roParms.timing.gpsLatitude);
    head.roParms.timing.gpsLongitude      = BYTESWAP_UINT16(head.roParms.timing.gpsLongitude);
    head.roParms.timing.gpsAltitude       = BYTESWAP_UINT16(head.roParms.timing.gpsAltitude);
    head.roParms.timing.dacCount          = BYTESWAP_UINT16(head.roParms.timing.dacCount);
    head.roParms.timing.gpsLastDrift[0]   = BYTESWAP_UINT16(head.roParms.timing.gpsLastDrift[0]);
    head.roParms.timing.gpsLastDrift[1]   = BYTESWAP_UINT16(head.roParms.timing.gpsLastDrift[1]);
    
    head.roParms.timing.gpsLastTurnOnTime[0] = BYTESWAP_UINT32(head.roParms.timing.gpsLastTurnOnTime[0]);
    head.roParms.timing.gpsLastTurnOnTime[1] = BYTESWAP_UINT32(head.roParms.timing.gpsLastTurnOnTime[1]);
    head.roParms.timing.gpsLastUpdateTime[0] = BYTESWAP_UINT32(head.roParms.timing.gpsLastUpdateTime[0]);
    head.roParms.timing.gpsLastUpdateTime[1] = BYTESWAP_UINT32(head.roParms.timing.gpsLastUpdateTime[1]);
    head.roParms.timing.gpsLastLockTime[0]   = BYTESWAP_UINT32(head.roParms.timing.gpsLastLockTime[0]);
    head.roParms.timing.gpsLastLockTime[1]   = BYTESWAP_UINT32(head.roParms.timing.gpsLastLockTime[1]);
    
    if(Debug && print_head)
    logit("e", "TIMING_RO_PARMS: %d %d %d %hu %hu %d %d %d %d %hu %d %d %lu %lu %lu %lu %lu %lu \n", 
        (int)head.roParms.timing.clockSource, 
        (int)head.roParms.timing.gpsStatus, 
        (int)head.roParms.timing.gpsSOH, 
            head.roParms.timing.gpsLockFailCount, 
            head.roParms.timing.gpsUpdateRTCCount, 
            head.roParms.timing.acqDelay, 
            head.roParms.timing.gpsLatitude, 
            head.roParms.timing.gpsLongitude, 
            head.roParms.timing.gpsAltitude, 
            head.roParms.timing.dacCount,
            head.roParms.timing.gpsLastDrift[0], 
            head.roParms.timing.gpsLastDrift[1], 
            
            head.roParms.timing.gpsLastTurnOnTime[0], 
            head.roParms.timing.gpsLastTurnOnTime[1], 
            head.roParms.timing.gpsLastUpdateTime[0], 
            head.roParms.timing.gpsLastUpdateTime[1], 
            head.roParms.timing.gpsLastLockTime[0], 
            head.roParms.timing.gpsLastLockTime[1] );
           
    
    
    for(i=0;i<nchans;i++) {
        head.roParms.channel[i].maxPeak       = BYTESWAP_UINT32(head.roParms.channel[i].maxPeak);
        head.roParms.channel[i].maxPeakOffset = BYTESWAP_UINT32(head.roParms.channel[i].maxPeakOffset);
        head.roParms.channel[i].minPeak       = BYTESWAP_UINT32(head.roParms.channel[i].minPeak);
        head.roParms.channel[i].minPeakOffset = BYTESWAP_UINT32(head.roParms.channel[i].minPeakOffset);
        head.roParms.channel[i].mean          = BYTESWAP_UINT32(head.roParms.channel[i].mean);
        head.roParms.channel[i].aqOffset      = BYTESWAP_UINT32(head.roParms.channel[i].aqOffset);
        
        if(Debug && print_head)
        logit("e", "CHANNEL_RO_PARMS: %d %lu %d %lu %d %d \n", 
            head.roParms.channel[i].maxPeak,  
            head.roParms.channel[i].maxPeakOffset,  
            head.roParms.channel[i].minPeak,  
            head.roParms.channel[i].minPeakOffset,  
            head.roParms.channel[i].mean,  
            head.roParms.channel[i].aqOffset );
        
    }
    
    
    head.roParms.stream.startTime       = BYTESWAP_UINT32(head.roParms.stream.startTime);
    head.roParms.stream.triggerTime     = BYTESWAP_UINT32(head.roParms.stream.triggerTime);
    head.roParms.stream.duration        = BYTESWAP_UINT32(head.roParms.stream.duration);
    head.roParms.stream.errors          = BYTESWAP_UINT16(head.roParms.stream.errors);
    head.roParms.stream.flags           = BYTESWAP_UINT16(head.roParms.stream.flags);
    head.roParms.stream.nscans          = BYTESWAP_UINT32(head.roParms.stream.nscans);
    head.roParms.stream.startTimeMsec   = BYTESWAP_UINT16(head.roParms.stream.startTimeMsec);
    head.roParms.stream.triggerTimeMsec = BYTESWAP_UINT16(head.roParms.stream.triggerTimeMsec);
    
    if(Debug && print_head)
    logit("e", "STREAM_RO_PARMS: %d %d %d %d %d %d %d %d \n", 
            head.roParms.stream.startTime,  
            head.roParms.stream.triggerTime,  
            head.roParms.stream.duration,  
            head.roParms.stream.errors,  
            head.roParms.stream.flags,  
            head.roParms.stream.startTimeMsec,  
            head.roParms.stream.triggerTimeMsec,  
            head.roParms.stream.nscans  );
    
    head.rwParms.misc.serialNumber   = BYTESWAP_UINT16(head.rwParms.misc.serialNumber);
    head.rwParms.misc.nchannels      = BYTESWAP_UINT16(head.rwParms.misc.nchannels);
    head.rwParms.misc.elevation      = BYTESWAP_UINT16(head.rwParms.misc.elevation);
                                  la = BYTESWAP_UINT32(*(long *)(&head.rwParms.misc.latitude));
    head.rwParms.misc.latitude       = *(float *)(&(la));
                                  la = BYTESWAP_UINT32(*(long *)(&head.rwParms.misc.longitude));
    head.rwParms.misc.longitude      = *(float *)(&la);
    head.rwParms.misc.cutler_bitmap  = BYTESWAP_UINT32(head.rwParms.misc.cutler_bitmap);
    head.rwParms.misc.channel_bitmap = BYTESWAP_UINT32(head.rwParms.misc.channel_bitmap);
    
    if(Debug && print_head)
    logit("e", "MISC_RW_PARMS: %hu %hu %.5s %.33s %d %f %f %d %d %d %d %d %lo %d %.17s %d %d\n", 
            head.rwParms.misc.serialNumber,
            head.rwParms.misc.nchannels,
            head.rwParms.misc.stnID,  
            head.rwParms.misc.comment, 
            head.rwParms.misc.elevation,
            head.rwParms.misc.latitude, 
            head.rwParms.misc.longitude,
            
        (int)head.rwParms.misc.cutlerCode, 
        (int)head.rwParms.misc.minBatteryVoltage, 
        (int)head.rwParms.misc.cutler_decimation, 
        (int)head.rwParms.misc.cutler_irig_type, 
            head.rwParms.misc.cutler_bitmap,
            head.rwParms.misc.channel_bitmap,
        (int)head.rwParms.misc.cutler_protocol, 
            head.rwParms.misc.siteID, 
        (int)head.rwParms.misc.externalTrigger, 
        (int)head.rwParms.misc.networkFlag );
    
    head.rwParms.timing.localOffset = BYTESWAP_UINT16(head.rwParms.timing.localOffset);
    
    if(Debug && print_head)
    logit("e", "TIMING_RW_PARMS: %d %d %hu \n", 
        (int)head.rwParms.timing.gpsTurnOnInterval,  
        (int)head.rwParms.timing.gpsMaxTurnOnTime, 
            head.rwParms.timing.localOffset  );
    
    for(i=0;i<nchans;i++) {
        head.rwParms.channel[i].north      = BYTESWAP_UINT16(head.rwParms.channel[i].north);
        head.rwParms.channel[i].east       = BYTESWAP_UINT16(head.rwParms.channel[i].east);
        head.rwParms.channel[i].up         = BYTESWAP_UINT16(head.rwParms.channel[i].up);
        head.rwParms.channel[i].altitude   = BYTESWAP_UINT16(head.rwParms.channel[i].altitude);
        head.rwParms.channel[i].azimuth    = BYTESWAP_UINT16(head.rwParms.channel[i].azimuth);
        head.rwParms.channel[i].sensorType = BYTESWAP_UINT16(head.rwParms.channel[i].sensorType);
        head.rwParms.channel[i].sensorSerialNumber    = BYTESWAP_UINT16(head.rwParms.channel[i].sensorSerialNumber);
        head.rwParms.channel[i].sensorSerialNumberExt = BYTESWAP_UINT16(head.rwParms.channel[i].sensorSerialNumberExt);
        head.rwParms.channel[i].gain                  = BYTESWAP_UINT16(head.rwParms.channel[i].gain);
        head.rwParms.channel[i].StaLtaRatio           = BYTESWAP_UINT16(head.rwParms.channel[i].StaLtaRatio);
        
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].fullscale));
        head.rwParms.channel[i].fullscale             = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].sensitivity));
        head.rwParms.channel[i].sensitivity           = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].damping));
        head.rwParms.channel[i].damping               = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].naturalFrequency));
        head.rwParms.channel[i].naturalFrequency      = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].triggerThreshold));
        head.rwParms.channel[i].triggerThreshold      = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].detriggerThreshold));
        head.rwParms.channel[i].detriggerThreshold    = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].alarmTriggerThreshold));
        head.rwParms.channel[i].alarmTriggerThreshold = *(float *)(&la);
                                                   la = BYTESWAP_UINT32(*(long *)(&head.rwParms.channel[i].calCoil));
        head.rwParms.channel[i].calCoil               = *(float *)(&la);
        if(head.rwParms.channel[i].range != ' ' && (head.rwParms.channel[i].range < '0' || head.rwParms.channel[i].range > '9') )
        	head.rwParms.channel[i].range = ' ';
        if(head.rwParms.channel[i].sensorgain != ' ' && (head.rwParms.channel[i].sensorgain < '0' || head.rwParms.channel[i].sensorgain > '9') )
        	head.rwParms.channel[i].sensorgain = ' '; 
        
        if(Debug && print_head)
        logit("e", "CHANNEL_RW_PARMS: %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f %f %f %f %f %f %f %f %c %c %s %s \n", 
            head.rwParms.channel[i].id,  
            head.rwParms.channel[i].sensorSerialNumberExt,  
            head.rwParms.channel[i].north,  
            head.rwParms.channel[i].east,  
            head.rwParms.channel[i].up,  
            head.rwParms.channel[i].altitude,
            head.rwParms.channel[i].azimuth,
            head.rwParms.channel[i].sensorType,
            head.rwParms.channel[i].sensorSerialNumber,
            head.rwParms.channel[i].gain,
        (int)head.rwParms.channel[i].triggerType,  
        (int)head.rwParms.channel[i].iirTriggerFilter,  
        (int)head.rwParms.channel[i].StaSeconds,  
        (int)head.rwParms.channel[i].LtaSeconds,  
            head.rwParms.channel[i].StaLtaRatio,
        (int)head.rwParms.channel[i].StaLtaPercent,  
            head.rwParms.channel[i].fullscale,
            head.rwParms.channel[i].sensitivity,
            head.rwParms.channel[i].damping,
            head.rwParms.channel[i].naturalFrequency,
            head.rwParms.channel[i].triggerThreshold,
            head.rwParms.channel[i].detriggerThreshold,
            head.rwParms.channel[i].alarmTriggerThreshold,
            head.rwParms.channel[i].calCoil,
            head.rwParms.channel[i].range,
            head.rwParms.channel[i].sensorgain,
            head.rwParms.channel[i].networkcode,
            head.rwParms.channel[i].locationcode
        );
        
    }
    
    
    head.rwParms.stream.eventNumber = BYTESWAP_UINT16(head.rwParms.stream.eventNumber);
    head.rwParms.stream.sps         = BYTESWAP_UINT16(head.rwParms.stream.sps);
    head.rwParms.stream.apw         = BYTESWAP_UINT16(head.rwParms.stream.apw);
    head.rwParms.stream.preEvent    = BYTESWAP_UINT16(head.rwParms.stream.preEvent);
    head.rwParms.stream.postEvent   = BYTESWAP_UINT16(head.rwParms.stream.postEvent);
    head.rwParms.stream.minRunTime  = BYTESWAP_UINT16(head.rwParms.stream.minRunTime);
    head.rwParms.stream.Timeout     = BYTESWAP_UINT16(head.rwParms.stream.Timeout);
    head.rwParms.stream.TxBlkSize   = BYTESWAP_UINT16(head.rwParms.stream.TxBlkSize);
    head.rwParms.stream.BufferSize  = BYTESWAP_UINT16(head.rwParms.stream.BufferSize);
    head.rwParms.stream.SampleRate  = BYTESWAP_UINT16(head.rwParms.stream.SampleRate);
    head.rwParms.stream.TxChanMap   = BYTESWAP_UINT32(head.rwParms.stream.TxChanMap);
    head.rwParms.stream.VotesToTrigger   = BYTESWAP_UINT16(head.rwParms.stream.VotesToTrigger);
    head.rwParms.stream.VotesToDetrigger = BYTESWAP_UINT16(head.rwParms.stream.VotesToDetrigger);
    
    
    if(Debug && print_head)
    logit("e", "STREAM_K2_RW_PARMS: |%d| |%d| |%d| %d %d %d %d %d %d %d %d %d %d %d %d %d %d %lu\n", 
        (int)head.rwParms.stream.filterFlag,  
        (int)head.rwParms.stream.primaryStorage,  
        (int)head.rwParms.stream.secondaryStorage,  
            head.rwParms.stream.eventNumber,  
            head.rwParms.stream.sps,  
            head.rwParms.stream.apw,  
            head.rwParms.stream.preEvent,  
            head.rwParms.stream.postEvent,  
            head.rwParms.stream.minRunTime,  
            head.rwParms.stream.VotesToTrigger,
            head.rwParms.stream.VotesToDetrigger,
        (int)head.rwParms.stream.FilterType,  
        (int)head.rwParms.stream.DataFmt,  
            head.rwParms.stream.Timeout,
            head.rwParms.stream.TxBlkSize,
            head.rwParms.stream.BufferSize,
            head.rwParms.stream.SampleRate,
            head.rwParms.stream.TxChanMap
              );
            
    return stat;
}




