# k2rtagent.d
#
# Picks up files from a specified directory and reads the header. 
# If the header for this site is known, we compare to look for changes.
# Changes, if any, are reported and the current header is stored.
#
# Option to save (to subdir ./save) or delete the file afterwards.
# If it has trouble converting as file, it saves it to subdir ./trouble. 
# Maintains its own local heartbeat and also monitors the peer's
# heartbeat via a file. Complains if the peer's expected heartbeat
# interval is exceeded; announces resumption of peer's heartbeat.

# Basic Module information
#-------------------------
MyModuleId        MOD_K2RTAGENT    # module id 
RingName          HYPO_RING	       # shared memory ring for output
HeartBeatInterval 30               # seconds between heartbeats to statmgr

LogFile           1                # 0 log to stderr/stdout only; 
                                   # 1 log to stderr/stdout and disk;
                                   # 2 log to disk module log only.

Debug             0                # 1=> debug output. 0=> no debug output

# Data file manipulation
#-----------------------
ArchiveDir      /home/picker/nsmp/save     # store old headers in this directory
HeaderDir       /home/picker/k2hdrs/dialup # store old headers in this directory
GetFromDir      /home/picker/nsmp/indir    # look for files in this directory
OutputDir       /home/picker/gifs/snw      # put SNW messages in this directory
CheckPeriod     1                  # sleep this many seconds between looks
OpenTries       5                  # How many times we'll try to open a file 
OpenWait        200                # Milliseconds to wait between open tries
SaveDataFiles   1                  # 0 = remove files after processing
                                   # non-zero = move files to save subdir
                                   #            after processing  
  

# Peer (remote partner) heartbeat manipulation
#---------------------------------------------
PeerHeartBeatFile  terra1  HEARTBT.TXT  600 
                                   # PeerHeartBeatFile takes 3 arguments:
                                   # 1st: Name of remote system that is 
                                   #   sending the heartbeat files.
                                   # 2nd: Name of the heartbeat file. 
                                   # 3rd: maximum #seconds between heartbeat 
                                   #   files. If no new PeerHeartBeatFile arrives
                                   #   in this many seconds, an error message will
                                   #   be sent.  An "unerror message" will be
                                   #   sent after next heartbeat file arrives
                                   #   If 0, expect no heartbeat files.
                                   # Some remote systems may have multiple 
                                   # heartbeat files; list each one in a
                                   # seperate PeerHeartBeatFile command
                                   # (up to 5 allowed).
PeerHeartBeatInterval 30               # seconds between heartbeats to statmgr

#LogHeartBeatFile 1                 # If non-zero, write contents of each
                                   #   heartbeat file to the daily log.

Manager   luetgert@usgs.gov

#Pager   luetgert@andreas.wr.usgs.gov


# The entries below assign an IRIS-style SCNL name to each active channel
# of each instrument we may hear from. Each instrument has a 'box' identifier
# which is sent in its message (could be a name or a serial number).
# Use "" to enter a NULL string for any of the SCNL fields.
#
# NOTE: legal channel numbers (as of Nov 00) are 0-17
#
#            box  chan   S     C    N    L
#
#ChannelName  436   0     1002  HN2  NP   ""
#
#ChannelName  1990  0     1023  HN2  NP   ""
#
#ChannelName  1031  0     1103  HN2  NP   ""
#
#ChannelName  556   0     1446  HN2  NP   ""

@NSMP_Chans.db

