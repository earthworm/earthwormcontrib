

   /***********************************************************************
    *                            PutStatus()                              *
    *                                                                     *
    *  Builds a heartbeat or error message & puts it into shared memory.  *
    *  Also, writes errors to log file.                                   *
    ***********************************************************************/

#include <stdio.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>


void PutStatus( UCHAR type, short ierr, char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   long     t;

   extern UCHAR    TypeHeartBeat;    // Heartbeat message type
   extern UCHAR    TypeError;        // Error message type
   extern UCHAR    InstId;           // Local installation id
   extern UCHAR    MyModId;          // Module Id for this program
   extern SHM_INFO region;           // Transport ring

// Build the message
// *****************
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if ( type == TypeHeartBeat ) sprintf( msg, "%ld\n", t );

   if ( type == TypeError ) sprintf( msg, "%ld %hd %s", t, ierr, note );

   size = strlen( msg );        // Don't include the null byte in the message

// Write the message to shared memory
// **********************************
   if ( tport_putmsg( &region, &logo, size, msg ) != PUT_OK )
   {
        if ( type == TypeHeartBeat )
           logit( "et", "Error sending heartbeat.\n" );

        if ( type == TypeError )
           logit( "et", "Error sending error: %d.\n", ierr );
   }
   return;
}
