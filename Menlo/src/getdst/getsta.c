#include <stdio.h>
#include <string.h>
#include <earthworm.h>
#include "getdst.h"


        /********************************************************
         *                     GetStaFile()                     *
         *                                                      *
         *  Get pin numbers and station codes from the station  *
         *  file and load them into the port structures.        *
         ********************************************************/

void GetStaFile( PORT port[], int NPort )
{
   FILE   *fpStaFile;
   int    p, c;                    // Port/channel index
   int    Port;                    // Port number
   int    Chan;                    // from 0 to NCHAN-1
   int    Pin;                     // Pin number
   static char   string[120];
   char   sta[6];                  // Site code
   char   comp[4];                 // Component/channel code
   char   net[3];                  // Network code

   extern char StaFileName[40];    // Name of dst station file from config.c

// Initialize pin numbers
// **********************
   for ( p = 0; p < NPort; p++ )
      for ( c = 0; c < NCHAN; c++ )
         port[p].ch[c].pin = -1;

// Open station file and read it's contents
// ****************************************
   if ( ( fpStaFile = fopen( StaFileName, "r") ) == NULL )
   {
      logit( "et", "Error opening station file <%s>. Exiting.\n", StaFileName );
      exit( -1 );
   }

   while ( fgets( string, 120, fpStaFile ) != NULL )
   {
      if ( strlen( string ) < 2 )              // Skip empty lines
         continue;

      if ( strncmp( string, "//", 2 ) == 0 )   // Skip comment lines
         continue;

      if ( sscanf( string, "%d%d%d%s%s%s", &Port, &Chan, &Pin,
                   sta, comp, net ) < 6 )
      {
         logit( "et", "Fatal error decoding station file. Offending line:\n" );
         logit( "", "%s", string );
         exit( -1 );
      }

      if ( (Port < 1) || (Port > NPort) )      /* Invalid port number */
         continue;

      if ( (Chan < 0) || (Chan >= NCHAN) )     /* Invalid channel number */
         continue;

      if ( (Pin < 0) || (Pin > 32767) )        /* Invalid pin number */
         continue;

      p = Port - 1;
      c = Chan;

      port[p].ch[c].pin = Pin;
      strcpy( port[p].ch[c].sta,  sta );
      strcpy( port[p].ch[c].comp, comp );
      strcpy( port[p].ch[c].net,  net );
   }

   fclose( fpStaFile );

/* Write pin numbers and SCNs to log file
   **************************************/
   logit( "", "\n" );
   logit( "", " Port   Pin      SCN\n" );
   logit( "", " ----   ---      ---\n" );

   for ( p = 0; p < NPort; p++ )
   {
      Port = p + 1;

      for ( c = 0; c < NCHAN; c++ )
      {
         logit( "", "  %2d",  Port );
         logit( "", "   %4d", port[p].ch[c].pin );
         logit( "", "  %-5s", port[p].ch[c].sta );
         logit( "", " %-3s",  port[p].ch[c].comp );
         logit( "", " %-2s",  port[p].ch[c].net );
         logit( "", "\n" );
      }
      logit( "", "\n" );
   }

/* Make sure all pin numbers have been initialized
   ***********************************************/
   for ( p = 0; p < NPort; p++ )
   {
      Port = p + 1;

      for ( c = 0; c < NCHAN; c++ )
      {
         if ( port[p].ch[c].pin == -1 )
         {
            logit( "et","Pin not initialized.  Port %2d. Chan %d. Exiting.\n",
                   Port, c );
            exit( -1 );
         }
      }
   }
   return;
}
