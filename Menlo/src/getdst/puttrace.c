
#include <stdio.h>
#include <string.h>
#include <transport.h>
#include <trace_buf.h>
#include "getdst.h"


     /*****************************************************************
      *                           GetIndex()                          *
      *                                                               *
      *  Get the index numbers of contiguous packets within this      *
      *  frame.                                                       *
      *                                                               *
      *  If first = 1, get the first contiguous data chunk.           *
      *  If first = 0, get the next contiguous data chunk.            *
      *                                                               *
      *  i1 = index number of beginning of contiguous data chunk.     *
      *  i2 = index number of end of contiguous data chunk.           *
      *                                                               *
      *  Returns 1 if a contiguous data chunk was found.              *
      *  Returns 0 if there are no more contiguous data chunks.       *
      *****************************************************************/

int GetIndex( PORT *port, int first, int *i1, int *i2 )
{
   int s;                         // Scan number
   int s1;                        // Index of beginning of chunk
   static int s2;                 // Index of end of chunk

// Start at beginning of frame buffer
// **********************************
   if ( first ) s2 = -1;

// Get the first index
// *******************
   for ( s = s2 + 1; s < NSCAN; s++ )
      if ( port->fr.stat[s] == 1 )
         break;
   if ( s == NSCAN ) return 0;    // No more packets found
   *i1 = s1 = s;                  // Packet found. Return the index

// Get the second index.
// Save s2 as a static variable.
// ****************************
   for ( s = s1; s < NSCAN; s++ )
      if ( port->fr.stat[s] == 0 )
         break;
   *i2 = s2 = s - 1;              // Return the index
   return 1;
}


     /*****************************************************************
      *                           PutTrace()                          *
      *                                                               *
      *  For a particular channel, create a tracebuf message by       *
      *  filling in the header and data samples from the frame        *
      *  buffer.  Then, write the tracebuf message to the transport   *
      *  ring.                                                        *
      *                                                               *
      *  Accepts c  = Channel number (0 to NCHAN-1)                   *
      *          s1 = Beginning scan number of contiguous data chunk  *
      *          s2 = Ending scan number of contiguous data chunk     *
      *                                                               *
      *  Returns 0 if all is ok.                                      *
      *  Returns -1 if there was an error writing to transport ring.  *
      *****************************************************************/

int PutTrace( PORT *port, int c, int s1, int s2 )
{
   long               waveMsgLen;   // Tracebuf msg length in bytes
   int                s;            // Scan number
   extern SHM_INFO    region;       // Transport ring
   extern MSG_LOGO    WaveLogo;     // Logo of waveform messages
   static TracePacket tp;           // Defined in trace_buf.h
                                    //    called the first time
   int  nsamp = s2 - s1 + 1;
   long *samp = &tp.i + sizeof(TRACE_HEADER)/sizeof(long);

// Fill in the trace header
// ************************
   strcpy( tp.trh.sta,  port->ch[c].sta );
   strcpy( tp.trh.chan, port->ch[c].comp );
   strcpy( tp.trh.net,  port->ch[c].net );
   memset( tp.trh.quality, '\0', 2 );
   strcpy( tp.trh.datatype, DATATYPE );
   tp.trh.samprate  = SAMPRATE;
   tp.trh.pinno     = port->ch[c].pin;
   tp.trh.nsamp     = nsamp;
   tp.trh.starttime = (double)port->fr.tstart + s1/tp.trh.samprate;
   tp.trh.endtime   = (double)port->fr.tstart + s2/tp.trh.samprate;

// Display some stuff for debugging
// ********************************
// if ( c == 0 )
// {
//    printf( "%s",  tp.trh.sta );
//    printf( " %s", tp.trh.chan );
//    printf( " %s", tp.trh.net );
//    printf( "  %.2lf", tp.trh.starttime );
//    printf( "  %.2lf\n", tp.trh.endtime );
// }

// Copy data samples from frame buffer to tracebuf msg
// ***************************************************
   for ( s = 0; s < nsamp; s++ )
      samp[s] = port->fr.pb[s+s1].samp[c];

// Write tracebuf message to transport ring
// ****************************************
   waveMsgLen = sizeof(TRACE_HEADER) + nsamp*sizeof(long);
   if ( tport_putmsg( &region, &WaveLogo, waveMsgLen, tp.msg ) != PUT_OK )
      return -1;

   return 0;
}
