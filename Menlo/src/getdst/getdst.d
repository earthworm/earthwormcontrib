#
#                     getdst configuration file (getdst.d)
#                     ------------------------------------
#
#
MyModuleId        MOD_GETDST    # Module id for this instance of getdst
RingName          WAVE_RING     # Shared memory ring for output
HeartBeatInterval 30            # Seconds between heartbeats
StaFileName       sta_list.dst  # Name of file containing DST station list


# NPort: The number of comm ports which are receiving DST data.  The ports
# are named com3, com4, com5,...  getdst monitors only contiguous comm
# ports, eg there is no mechanism for monitoring com3 and com5, without
# monitoring com4.
NPort  12

# InputQueueSize: Windows NT maintains an input queue for each comm port,
# the size of which may be changed.  InputQueueSize is the user-recommended
# input queue size of each comm port.  Windows NT may or may not actually
# reset the queue size, according to its own whims.  If InputQueueSize is increased, the actual queue size seems to increase, up to
# a maximum of about 520000.  The input queue size seems to never decrease until
# the computer is rebooted.
InputQueueSize  20000

# RbSize: Size of receive buffer, in bytes per port.
# getdst reads bytes from the comm port input queues to the receive buffer.
# Increasing RbSize has a similar effect to increasing InputQueueSize.
RbSize  10000

# DrinkInterval: Sleep this many milliseconds between reading from the
# Windows NT input queue to the receive buffer.
DrinkInterval  500

# PbSize: Bytes from the receive buffer are divided into "packets", which are
# stored in a packet buffer for each comm port.  A packet contains one "scan"
# of all four channels for one comm port.  PbSize is the size of the packet
# buffer, in packets per port.
PbSize 1000

# UnitSec: If some bytes in a one-second data frame are missing, getdst will
# extrapolate time from the previous frame.  The extrapolated time is converted
# to time bits and compared with the actual time bits of the current frame.
# If the extrapolated time bits don't match the actual time bits (some of which
# are missing), the current frame is dropped.
# If UnitSec = 0, the program behaves as just described.
# If UnitSec = 1, there is an additional requirement that the units-of-seconds
# bits must be present in the current frame.  This provides greater assurance
# that data is correctly time stamped.  The downside is that some good data may
# get dropped.
UnitSec  0

