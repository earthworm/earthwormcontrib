
       /*******************************************************
        *                        port.c                       *
        *                                                     *
        *  Functions to initialize the Digi ports for read    *
        *  DST data.                                          *
        *******************************************************/


#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <earthworm.h>
#include "getdst.h"


void InitPort( PORT *port,          // Array of comm port parameters
               int  p )             // Index of comm port
{
   COMMTIMEOUTS timeouts;
   COMMPROP     prop;
   DCB          dcb;                // Data control block
   BOOL         success;
   extern int   InputQueueSize;     // Size of comm input queue
   char         portName[12];       // Port device name, eg COM3

// Name the ports COM3, COM4, COM5, etc.
// Ports above COM9 require a prefix of "\\.\"
// *******************************************
   sprintf( portName, "\\\\.\\COM%d", p+3 );
   logit( "", "\n" );
   logit( "t", "Port name:        %s\n", portName );

// Open a Digi port
// ****************
   port[p].handle = CreateFile( portName,
                                GENERIC_READ|GENERIC_WRITE,
                                0, 0, OPEN_EXISTING,
                                FILE_ATTRIBUTE_NORMAL, 0 );

   if ( port[p].handle == INVALID_HANDLE_VALUE )
   {
      logit( "et", "Error %d in CreateFile. Exiting.\n",
             GetLastError() );
      exit( -1 );
   }
   logit( "t", "CreateFile succeeded for comm port.\n" );

// Print current size of input queue
// *********************************
   success = GetCommProperties( port[p].handle, &prop );
   if ( !success )
   {
      logit( "et", "Error %d in GetCommProperties. Exiting.\n",
             GetLastError() );
      exit( -1 );
   }
   logit( "t", "dwCurrentRxQueue: %d\n", prop.dwCurrentRxQueue );

// Reset size of input queue
// *************************
   logit( "t", "Attempting to reset size of input queue.\n" );

   success = SetupComm( port[p].handle, InputQueueSize, 0 );
   if ( !success )
   {
      logit( "et", "Error %d in SetupComm. Exiting.\n",
             GetLastError() );
      exit( -1 );
   }

// Print new size of input queue
// *****************************
   success = GetCommProperties( port[p].handle, &prop );
   if ( !success )
   {
      logit( "et", "Error %d in GetCommProperties. Exiting.\n",
             GetLastError() );
      exit( -1 );
   }
   logit( "t", "dwCurrentRxQueue: %d\n", prop.dwCurrentRxQueue );

// Get the current settings of the comm port
// *****************************************
   success = GetCommState( port[p].handle, &dcb );
   if ( !success )
   {
      logit( "et", "Error %d in GetCommState. Exiting.\n",
             GetLastError() );
      exit( -1 );
   }

// Modify the baud rate, flow control, etc
// ***************************************
   dcb.BaudRate        = 19200;        // Hardwired
   dcb.ByteSize        = 8;            // Hardwired
   dcb.Parity          = NOPARITY;     // Hardwired
   dcb.StopBits        = ONESTOPBIT;   // Hardwired

   dcb.fOutxCtsFlow    = FALSE;        // Disable RTS/CTS flow control
   dcb.fOutxDsrFlow    = FALSE;        // Disable DTR/DSR flow control
   dcb.fDsrSensitivity = FALSE;        // Driver ignores DSR
   dcb.fOutX           = FALSE;        // Disable XON/XOFF out flow control
   dcb.fInX            = FALSE;        // Disable XON/XOFF in flow control
   dcb.fNull           = FALSE;        // Don't discard null bytes
   dcb.fAbortOnError   = TRUE;         // Abort rd/wr on error

// Apply the new comm port settings
// ********************************
   success = SetCommState( port[p].handle, &dcb );
   if ( !success )
   {
      logit( "et", "Error %d in SetCommState. Exiting.\n",
             GetLastError() );
      exit( -1 );
   }
   logit( "t", "Comm parameters set.\n" );

// Change the ReadIntervalTimeout so that ReadFile
// will return immediately.
// ***********************************************
   timeouts.ReadIntervalTimeout         = MAXDWORD;
   timeouts.ReadTotalTimeoutMultiplier  = 0;
   timeouts.ReadTotalTimeoutConstant    = 0;
   timeouts.WriteTotalTimeoutMultiplier = 0;
   timeouts.WriteTotalTimeoutConstant   = 0;
   SetCommTimeouts( port[p].handle, &timeouts );
   logit( "t", "Timeout parameters set.\n" );

// Purge the input comm port buffer
// ********************************
   success = PurgeComm( port[p].handle, PURGE_RXCLEAR );
   if ( !success )
   {
      logit( "et", "Error %d in PurgeComm. Exiting.\n",
             GetLastError() );
      exit( -1 );
   }
   logit( "t", "Receive buffer purged.\n" );

// Initialize the synch flag to "not synched"
// ******************************************
   port[p].synch = 0;

   return;
}
