
                       /***************************
                        *        getdst.h         *
                        ***************************/

#ifndef GETDST_H
#define GETDST_H

#include <time.h>              // Defines time_t
#include <windows.h>           // Defines UCHAR, HANDLE, etc
#include <trace_buf.h>         // Defines TRACE_HEADER

// These constants are hardwired into the DST data format
// ******************************************************
#define PACKETSIZE       14    // Number of bytes per packet
#define NCHAN             4    // Number of channels per port
#define NSCAN           100    // Number of samples per output tracebuf message
#define NTIMEBITS        52    // Number of time bits per frame
#define MAGIC     194601600    // Jan 1, 1970 minus Jan 1, 1600, in minutes
#define SAMPRATE      100.0    // Sampling rate
#define DATATYPE       "i4"    // i4 = Intel byte order, 4-byte integer
#define STARTBYTE      0x0d
#define STOPBYTE       0x0a

// Status manager error codes
// **************************
#define ERR_RBUFFERFULL  1     // Receive buffer is full
#define ERR_PBUFFERFULL  2     // Packet buffer is full
#define ERR_LOSTSYNCH    3     // Lost time synch, data discarded
#define ERR_PUTMSG       4     // Can't write tracebuf message to transport ring

// Channel and port structure definitions
// **************************************
typedef struct
{
   int        pin;             // Pin number
   char       sta[6];          // Site code
   char       comp[4];         // Component/channel code
   char       net[3];          // Network code
} CHAN;

typedef struct
{
   UCHAR      index;           // Packet index
   UCHAR      timebit;         // Time bit of packet, 0 or 1
   long       samp[NCHAN];     // 22-bit samples
} PACKET;

typedef struct
{
   PACKET     pb[NSCAN];       // Array of packets
   UCHAR      stat[NSCAN];     // Non-zero if packet contains data
   time_t     tstart;          // Start time of current frame
   time_t     tprev;           // Start time of previous frame
} FRAME;

typedef struct
{
   HANDLE     handle;          // File handle of Digi port
   CHAN       ch[NCHAN];       // Channel-specific information
   UCHAR      *rb;             // Receive buffer
   int        nbyte;           // Number of bytes in buffer rb
   PACKET     *pb;             // Packet buffer
   int        npack;           // Number of packets in buffer pb
   FRAME      fr;              // One-second data frame
   int        synch;           // 1 if time in synch; 0 otherwise
} PORT;

#endif

