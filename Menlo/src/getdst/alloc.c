
// alloc.c
// *******

#include <stdlib.h>
#include <malloc.h>
#include <earthworm.h>
#include "getdst.h"


      /***********************************************************
       *                    AllocPortStruct()                    *
       *                                                         *
       *  Allocate the Digi port structures, one for each port.  *
       ***********************************************************/

PORT *AllocPortStruct( int NPort )
{
   PORT *port = (PORT *) calloc( NPort, sizeof(PORT) );
   if ( port == NULL )
   {
      logit( "et", "Cannot allocate the Digi port structures. Exiting.\n" );
      exit( -1 );
   }
   return port;
}


     /****************************************************************
      *                         AllocWorkBuf()                       *
      *                                                              *
      *  Allocate work buffers for each port                         *
      ****************************************************************/

void AllocWorkBuf( PORT port[], int NPort )
{
   int p;
   extern int RbSize;            // Receive buffer size, from getdst.d
   extern int PbSize;            // Packet buffer size, from getdst.d

// Allocate receive buffers
// ************************
   for ( p = 0; p < NPort; p++ )
   {
      port[p].rb = (UCHAR *)malloc( RbSize );
      if ( port[p].rb == NULL )
      {
         logit( "t", "Error allocating rb buffers. Exiting.\n" );
         exit( -1 );
      }
      port[p].nbyte = 0;          // rb is empty

// Allocate packet buffers
// ***********************
      port[p].pb = (PACKET *)malloc( PbSize * sizeof(PACKET) );
      if ( port[p].pb == NULL )
      {
         logit( "t", "Error allocating pb buffers. Exiting.\n" );
         exit( -1 );
      }
      port[p].npack = 0;          // pb buffer is empty
   }
   return;
}
