B = $(EW_HOME)/$(EW_VERSION)/bin

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/getdst*
