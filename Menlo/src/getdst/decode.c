/*
   decode.c
*/

#include <time.h>
#include <time_ew.h>
#include <string.h>                    // for memset()
#include <chron3.h>
#include "getdst.h"


       /***********************************************************
        *                      DecodeTime()                       *
        *                                                         *
        *  Accepts a pointer to a one second data frame and       *
        *  extracts the timebits, which are converted to ymdhms.  *
        *  The result is converted to a double and stored in the  *
        *  frame structure.                                       *
        *  Returns 0 on success                                   *
        *  Returns -1 if some of the time bits aren't set         *
        *                                                         *
        *  fr = One second frame structure                        *
        ***********************************************************/

int DecodeTime( FRAME *fr )
{
   PACKET *pb = fr->pb;
   int year, yday, hour, min, sec;
   struct Greg g;
   int i;

// Make sure all time bits are set
// *******************************
   for ( i = 0; i < NTIMEBITS; i++ )
      if ( fr->stat[i] == 0 ) return -1;

// Decode the time bits
// ********************
   year = 1000 * ((8 * pb[0].timebit ) + (4 * pb[1].timebit )  +
                  (2 * pb[2].timebit ) +      pb[3].timebit  ) +
           100 * ((8 * pb[4].timebit ) + (4 * pb[5].timebit )  +
                  (2 * pb[6].timebit ) +      pb[7].timebit  ) +
            10 * ((8 * pb[8].timebit ) + (4 * pb[9].timebit )  +
                  (2 * pb[10].timebit) +      pb[11].timebit ) +
                 ((8 * pb[12].timebit) + (4 * pb[13].timebit)  +
                  (2 * pb[14].timebit) +      pb[15].timebit );
   yday =  100 * ((8 * pb[16].timebit) + (4 * pb[17].timebit)  +
                  (2 * pb[18].timebit) +      pb[19].timebit ) +
            10 * ((8 * pb[20].timebit )+ (4 * pb[21].timebit)  +
                  (2 * pb[22].timebit) +      pb[23].timebit ) +
                 ((8 * pb[24].timebit) + (4 * pb[25].timebit)  +
                  (2 * pb[26].timebit) +      pb[27].timebit );
   hour =   10 * ((8 * pb[28].timebit )+ (4 * pb[29].timebit)  +
                  (2 * pb[30].timebit) +      pb[31].timebit ) +
                 ((8 * pb[32].timebit) + (4 * pb[33].timebit)  +
                  (2 * pb[34].timebit) +      pb[35].timebit );
   min  =   10 * ((8 * pb[36].timebit )+ (4 * pb[37].timebit)  +
                  (2 * pb[38].timebit) +      pb[39].timebit ) +
                 ((8 * pb[40].timebit) + (4 * pb[41].timebit)  +
                  (2 * pb[42].timebit) +      pb[43].timebit );
   sec  =   10 * ((8 * pb[44].timebit )+ (4 * pb[45].timebit)  +
                  (2 * pb[46].timebit) +      pb[47].timebit ) +
                 ((8 * pb[48].timebit) + (4 * pb[49].timebit)  +
                  (2 * pb[50].timebit) +      pb[51].timebit );

// printf( "%4d %03d", year, yday );
// printf( " %02d:%02d:%02d\n", hour, min, sec );

// Convert to seconds since midnight Jan 1, 1970
// *********************************************/
   g.year   = year;
   g.month  = g.day    = 1;
   g.hour   = g.minute = 0;
   g.second = 0.0;

   fr->tstart = 60 * (julmin(&g) - MAGIC) + (86400 * (yday - 1)) +
               (3600 * hour) + (60 * min) + sec;

   return 0;
}


       /***********************************************************
        *                      DigitToBit()                       *
        *                                                         *
        *  Convert a single BCD digit to bit values.              *
        *  This function is called by ExtrapTime().               *
        *                                                         *
        *  digit = The decimal digit to convert.                  *
        *  bit   = Pointer to four-element array of bit values.   *
        ***********************************************************/

void DigitToBit( UCHAR digit, UCHAR *bit )
{
   bit[0] = (digit & 8) >> 3;
   bit[1] = (digit & 4) >> 2;
   bit[2] = (digit & 2) >> 1;
   bit[3] = digit & 1;
   return;
}


       /***********************************************************
        *                      ExtrapTime()                       *
        *                                                         *
        *  Extrapolate time from the previous frame.  See if the  *
        *  extrapolated time matches all available bits from the  *
        *  current time.                                          *
        *  Also check the "User Info" bits, which are set to      *
        *  the same values in each frame.                         *
        *  Returns  0 if time was extrapolated.                   *
        *  Returns -1 if current time bits don't extrapolate      *
        *             from the previous frame, possibly because   *
        *             of an input queue overflow.
        *  Returns -2 if UnitSec option is being used and the     *
        *             units of seconds bits aren't set in the     *
        *             current frame.                              *
        *                                                         *
        *  fr = One second frame structure. fr->ts is updated.    *
        ***********************************************************/

int ExtrapTime( FRAME *fr )
{
   struct tm tstr;
   int year, yday, hour, min, sec;
   UCHAR th_year, hun_year, ten_year, unit_year;
   UCHAR hun_day, ten_day, unit_day;
   UCHAR ten_hour, unit_hour;
   UCHAR ten_min, unit_min;
   UCHAR ten_sec, unit_sec;
   static UCHAR timebit[NSCAN];
   int   i;
   extern int UnitSec;         // If 1, require unitsec for extrapolation

// te is extrapolated time, which may be used for time of current frame
// ********************************************************************
   time_t te = fr->tprev + 1;

// If UnitSec is set, require that the units of seconds
// bits are set for extrapolation to take place.
// ****************************************************
// fr->stat[49] = 0;           // Clobber a units of seconds time bit, for debugging

   if ( UnitSec == 1 )
      for ( i = 48; i < 52; i++ )
         if ( fr->stat[i] == 0 ) return -2;

// Convert te to year-day-hour-minute-second
// *****************************************
   gmtime_ew( &te, &tstr );

   year = tstr.tm_year + 1900;     // Four-digit year
   yday = tstr.tm_yday + 1;        // Day of year (1 - 366)
   hour = tstr.tm_hour;
   min  = tstr.tm_min;
   sec  = tstr.tm_sec;

// printf( "%d",      year );
// printf( " %03d",   yday );
// printf( " %02d",   hour );
// printf( ":%02d",   min );
// printf( ":%02d\n", sec );

// Convert ydhms to BCD digits
// ***************************
   th_year  = year / 1000;
   year -= 1000 * th_year;
   hun_year = year / 100;
   year -= 100 * hun_year;
   ten_year = year / 10;
   year -= 10 * ten_year;
   unit_year = year;

   hun_day  = yday / 100;
   yday -= 100 * hun_day;
   ten_day  = yday / 10;
   yday -= 10 * ten_day;
   unit_day = yday;

   ten_hour  = hour / 10;
   hour -= 10 * ten_hour;
   unit_hour = hour;

   ten_min  = min / 10;
   min -= 10 * ten_min;
   unit_min = min;

   ten_sec  = sec / 10;
   sec -= 10 * ten_sec;
   unit_sec = sec;

// printf( "%d%d%d%d", th_year, hun_year, ten_year, unit_year );
// printf( " %d%d%d",  hun_day, ten_day, unit_day );
// printf( " %d%d",    ten_hour, unit_hour );
// printf( ":%d%d",    ten_min, unit_min );
// printf( ":%d%d\n",  ten_sec, unit_sec );

// Convert BCD digits to bits
// **************************
   DigitToBit( th_year,   &timebit[0]  );
   DigitToBit( hun_year,  &timebit[4]  );
   DigitToBit( ten_year,  &timebit[8]  );
   DigitToBit( unit_year, &timebit[12] );
   DigitToBit( hun_day,   &timebit[16] );
   DigitToBit( ten_day,   &timebit[20] );
   DigitToBit( unit_day,  &timebit[24] );
   DigitToBit( ten_hour,  &timebit[28] );
   DigitToBit( unit_hour, &timebit[32] );
   DigitToBit( ten_min,   &timebit[36] );
   DigitToBit( unit_min,  &timebit[40] );
   DigitToBit( ten_sec,   &timebit[44] );
   DigitToBit( unit_sec,  &timebit[48] );

// Set the "User Info" time bits, which are currently unused.
// Bits 52-95 are always 0; bits 96-99 are always 1.
// *********************************************************
   memset( &timebit[52], 0, 44 );
   memset( &timebit[96], 1, 4 );

// timebit[99] = 0;

// Compare expected time bits with actual time bits
// ************************************************
   for ( i = 0; i < NSCAN; i++ )
   {
      if ( fr->stat[i] == 1 )
         if ( fr->pb[i].timebit != timebit[i] ) return -1;
   }

// Set start time of current frame to extrapolated time
// ****************************************************
   fr->tstart = te;
   return 0;
}

