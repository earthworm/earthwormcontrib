
       /*************************************************************
        *                       Program getdst                      *
        *                                                           *
        *  This program acquires 22-bit DST data from a Digi C/X    *
        *  Serial Port system connected to a Windows NT PC, and     *
        *  sends them to an Earthworm transport ring.               *
        *************************************************************/

/*
                             Programmers Notes

Design Goals:
The DST system is designed to be low cost, and to provide high dynamic
range (22-bit) data.  Data are available in near real time, limited only
by propogation delays of a few milliseconds through radio and microwave
links.  Precision time stamping of data is assured by the use of GPS
receivers at each field site.  However, if data are lost between the
field site and the home office, there is no way for the home office to
rerequest missing data.  In practice, up to few hundred small (one sample)
gaps may occur each day per station.

Configuring the Digi Ports:
Use the Digi pop-up menu to set up the comm ports on Windows NT, by
clicking on Start, Settings, Control Panels, Network, Adapters, Digi C/X
(PCI) Adapter.  Click Help for instructions on how to set up the ports.
The ports must be named COM3, COM4, COM5, etc.  COM3 gets attached to
Digi port 1 on the Digi concentrator, etc.  The program gets data from
consecutive Digi ports, starting at port 1 and counting up.  The program
is not designed to access a random group of ports.

Input Queues and Buffering:
Windows NT maintains an input queue for each com port.  The default size
of the queues is 20000 bytes, but the queue size can be increased using
a configuration file parameter.  If the input queue for a port ever fills
data on that port will be lost.  The getdst program maintains a receive
buffer and a packet buffer for each port.  These buffers may fill
completely, yet no data will be lost unless the input queue is also filled.
If getdst produces a lot of "receive buffer full" and "packet buffer full"
messages, the buffer sizes specified in the configuration file should be
increased.

Detection of Data Packets:
A "packet" is a 14-byte sequence of bytes containing one scan (four components)
of data from a serial port.  A packet also contains a start byte, a stop byte,
a packet index, and a time bit.  The program associates bytes into packets
according to the locations of start and stop bytes in the data stream.  Note
that if the data contain gaps, the packet boundaries may placed incorrectly,
because the data bytes stream may also contain start and stop byte characters.

Detection of Data Frames:
A "frame" is a sequence of 100 consecutive packets with packet index numbers
from 0 to 99.  This program assumes that a new frame is starting, whenever
the packet index number decreases from one packet to the next.  For data with
no gaps, the packet index number will decrease from 99 to 0 at the frame
boundaries.  Note that for large gaps (one second or greater), packets may
be incorrectly assigned to frames.

Time Stamping of Data:
To avoid timing errors due to propogation delays from the field sites to the
home office, each field site is equipped with its own GPS receiver.  Each
complete data frame recorded at the field site is encoded the the UTC time of
the first scan of the frame.  If a frame contains no gaps, the getdst program
decodes the time of the frame and assigns that time to the corresponding
Earthworm messages.  If some time bytes are missing, due to gaps in the data
stream", getdst will extrapolate time from the last complete frame.  If the
extrapolated time matches all available bits from the current frame, the
extrapolated time is assigned to the current frame.  If the extraplated time
bits do not match the current frame, the port is declared "out of time synch",
until the next complete frame is received.

Tracebuf Message Output:
The getdst program writes Earthworm tracebuf message to a ring buffer in
shared memory.  If the data contain no gaps, they are split up into one-second
messages.  Whenever a gap is detected, getdst terminates the current tracebuf
message and starts a new one.  Therefore, each tracebuf message contains
continuous data.

Error Reporting:
Getdst reports four types of messages to statmgr:

  Error 1. "Receive buffer is full."  This is usually not a serious condition.
  Recommended action: Increase receive buffer size in the getdst configuration file.

  Error 2. "Packet buffer is full."  This is usually not a serious condition.
  Recommended action: Increase packet buffer size in the getdst configuration file.

  Error 3. "Lost time synch. Data discarded."  This message may indicate that data
  has been dropped or garbled by the telemetry.  If this message is seen in
  conjunction with errors 1 and/or 2, the comm port input queues may be full.
  Recommended actions: Diagnose and repair bad telemetry.  Increase size of input
  queues in the getdst configuration file.  Buy a faster computer.

  Error 4. "Can't write tracebuf message to transport ring."  This message could
  indicate that another process is not releasing the mutex semaphore for the
  transport ring.  This message is unlikely to ever occur.
*/

#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include "getdst.h"

// Share this with other source files
// **********************************
SHM_INFO region;                      // Transport ring

// Function prototypes
// *******************
void GetConfig( char * );
void GetUtilLookup( void );
void LogConfig( void );
PORT *AllocPortStruct( int );
void AllocWorkBuf( PORT [], int );
void GetStaFile( PORT [], int );
void InitPort( PORT *, int );
void PutStatus( UCHAR, short, char * );
int  GetSynch( PORT * );
int  CopyPacket( PORT * );
int  GetFrame( PORT * );
int  DecodeTime( FRAME * );
int  ExtrapTime( FRAME * );
int  PutTrace( PORT *, int, int, int );
int  GetIndex( PORT *, int, int *, int * );


int main( int argc, char *argv[] )
{
   int             p;                 // Port number
   char            *configFile;
   PORT            *port;             // Array of port parameters
   time_t          timeNow;           // Current time
   time_t          timeLastBeat;      // Time last heartbeat was sent

// Declared in config.c
// ********************
   extern UCHAR    MyModId;           // Module Id for this program
   extern long     RingKey;           // Key of shared memory ring
   extern int      NPort;             // Number of Digi ports
   extern int      DrinkInterval;     // Milliseconds between port reads
   extern MSG_LOGO WaveLogo;          // Logo of waveform messages
   extern long     HeartBeatInterval; // Seconds between heartbeats
   extern UCHAR    TypeHeartBeat;     // Heartbeat message type
   extern UCHAR    TypeError;         // Error message type
   extern int      RbSize;            // Receive buffer size

// Check command line arguments
// ****************************
   if ( argc < 2 )
   {
      printf( "Usage: getdst getdst.d\n" );
      return -1;
   }
   configFile = argv[1];

// Read the configuration file
// ***************************
   GetConfig( configFile );

// Look up important info from earthworm.h tables
// **********************************************
   GetUtilLookup();

// Open the log file
// *****************
   logit_init( "getdst", (short)MyModId, 256, 1 );

// Write the config file parameters to the log file
// ************************************************
   LogConfig();

// Allocate the port structures
// ****************************
   port = AllocPortStruct( NPort );
   logit( "t", "Port structures allocated.\n" );

// Allocate work buffers
// *********************
   AllocWorkBuf( port, NPort );
   logit( "t", "Work buffers allocated.\n" );

// Get pin numbers and station codes from the
// station file and write to the log file
// ******************************************
   GetStaFile( port, NPort );
   logit( "t", "Pin numbers and station codes read from file.\n" );

// Attach to shared memory region
// ******************************
   tport_attach( &region, RingKey );
   logit( "t", "Attached to the shared memory region.\n" );

// Initialize the Digi ports
// *************************
   for ( p = 0; p < NPort; p++ )
      InitPort( port, p );
   logit( "t", "Digi ports opened.\n" );

// Beat the heart in first pass through main loop
// **********************************************
   timeLastBeat = time(&timeNow) - HeartBeatInterval - 1;

// Check termination flag
// **********************
   while ( tport_getflag( &region ) != TERMINATE )
   {

// Send heartbeat to statmgr
// *************************
      if  ( (time(&timeNow) - timeLastBeat) >= HeartBeatInterval )
      {
         timeLastBeat = timeNow;
         PutStatus( TypeHeartBeat, 0, "" );
      }

// Read data from comm ports into receive buffers.
// ReadFile() reads all available characters, up to
// the amount of available space in the receive buffer.
// ReadFile() never seems to fail (happily).
// ***************************************************
      for ( p = 0; p < NPort; p++ )
      {
         char  text[80];               // To contain error messages
         int   rc;
         UCHAR *rbptr = port[p].rb + port[p].nbyte;
         int   maxChar = RbSize - port[p].nbyte;
         DWORD nBytesRead;

         if ( !ReadFile( port[p].handle, rbptr, maxChar, &nBytesRead, 0 ) )
         {
            logit( "et", "Error %d in ReadFile.\n", GetLastError() );
            logit( "et", "I will sleep 60 seconds and read from another port.\n" );
            sleep_ew( 60000 );
            continue;
         }
         port[p].nbyte += nBytesRead;

//       printf( "Port %d. nBytesRead: %d", p+1, nBytesRead );
//       printf( "  nbyte: %d\n", port[p].nbyte );

// Throw away one byte, for debugging
// **********************************
//       if ( port[p].nbyte > 100 )
//       {
//          int i;
//          port[p].nbyte--;
//          for ( i = 50; i < port[p].nbyte; i++ )
//             port[p].rb[i] = port[p].rb[i+1];
//       }

// If we are falling behind reading the Digi port, complain to
// statmgr.  This is not necessarily a serious error, because
// Windows NT will also buffer data in its input queue.
// Data will be lost if the NT input queue ever fills up.
// ***********************************************************
         if ( port[p].nbyte == RbSize )
         {
            sprintf( text, "Port %d. Receive buffer is full.", p+1 );
            PutStatus( TypeError, ERR_RBUFFERFULL, text );
            logit( "et", "%s\n", text );
         }

// Each time through this loop, copy a single packet from
// receive buffer to packet buffer
// ******************************************************
         while ( 1 )
         {

// If the port is out of synch, try to attain synch.
// Returns 0 if synch was achieved
//        -1 if synch not achievable with available bytes
// ******************************************************
GETSYNCH:
            if ( !port[p].synch )
            {
               if ( GetSynch( &port[p] ) == -1 ) break;
            }

// Copy a single packet from receive buffer to packet buffer.
// Returns 0 if a packet was successfully copied.
//        -1 not enough bytes in receive buffer to make a packet.
//        -2 if time synch was lost.
//        -3 if packet buffer is full
// *************************************************************
            rc = CopyPacket( &port[p] );

            if ( rc == -1 ) break;            // Not enough bytes
            if ( rc == -2 ) goto GETSYNCH;    // Time synch lost
            if ( rc == -3 )                   // Packet buffer is full
            {
               sprintf( text, "Port %d. Packet buffer is full.", p+1 );
               PutStatus( TypeError, ERR_PBUFFERFULL, text );
               logit( "et", "%s\n", text );
               break;
            }
         }

//       printf( "npack: %d\n", port[p].npack );

// Copy packets to frame structure
// *******************************
         while ( GetFrame( &port[p] ) == 1 )
         {
            int s1, s2;                 // Index numbers of data chunk
            int first = 1;              // Set to 1 before GetIndex is
                                        //   called the first time

// Print data samples for this frame for debugging
// ***********************************************
/*          int i;
            for ( i = 0; i < NSCAN; i++ )
               if ( port[p].fr.stat[i] == 1 )
               {
                  int j;
                  printf( "%2d", i );
                  for ( j = 0; j < NCHAN; j++ )
                     printf( " %d", port[p].fr.pb[i].samp[j] );
                  putchar( '\n' );
               } */

// Create gaps for debugging
// *************************
/*          {
               static int i = 0;
               if ( (++i % 5) == 0 )
               {
                  int s;
                  for ( s = 10; s < 21; s++ )
                     port->fr.stat[s] = 0;
                  i = 0;
               }
            } */

// Decode time of this frame.  Returns -1 if time can't be decoded.
// This occurs if any bytes containing time bits have been dropped
// from the frame.
// ***************************************************************
            if ( DecodeTime( &port[p].fr ) == -1 )
            {
               int rc;
               if ( port[p].fr.tprev == 0 ) continue;

// We can't decode the time of the current frame.
// Try to extrapolate time from previous frame.
// Returns 0 if time was successfully extrapolated.
//        -1 if current time doesn't extrapolate from time of
//              previous frame, possibly because of an input
//              queue overflow.
//        -2 if UnitSec option is being used and the units of
//              seconds bits aren't set in the current frame.
// **********************************************************
               rc = ExtrapTime( &port[p].fr );
               if ( rc < 0 )
               {
                  sprintf( text, "Port %d. Lost time synch. ", p+1 );
                  if ( rc == -1 )
                     strcat( text, "Can't extrapolate time." );
                  if ( rc == -2 )
                     strcat( text, "Units of seconds bits not set." );
                  PutStatus( TypeError, ERR_LOSTSYNCH, text );
                  logit( "et", "%s\n", text );
                  port[p].fr.tprev = 0;
                  continue;
               }
            }
            port[p].fr.tprev = port[p].fr.tstart;     // Save start time of frame

//          printf( "Port %d. %s", p+1, asctime( gmtime( &port[p].fr.ts ) ) );

// GetIndex() gets the index numbers of contiguous
// packets within this frame.
// If first = 1, get the first contiguous data chunk.
// If first = 0, get the next contiguous data chunk.
// Returns 1 if a contiguous data chunk was found.
// Returns 0 if there are no more contiguous data chunks.
// *****************************************************
            while ( GetIndex( port, first, &s1, &s2 ) == 1 )
            {
               int c;                      // Channel number
               first = 0;
//             printf( "s1: %d  s2: %d\n", s1, s2 );

// Copy channels of frame to tracebuf messages,
// and write tracebuf messages to transport ring
// **********************************************
               for ( c = 0; c < NCHAN; c++ )
                  if ( PutTrace( &port[p], c, s1, s2 ) == -1 )
                  {
                     sprintf( text, "Error sending tracebuf message to transport ring." );
                     PutStatus( TypeError, ERR_PUTMSG, text );
                     logit( "et", "%s\n", text );
                  }
            }
         }
      }

      sleep_ew( DrinkInterval );
   }

   for ( p = 0; p < NPort; p++ )
      CloseHandle( port[p].handle );

   return 0;
}
