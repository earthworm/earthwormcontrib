
//                            File config.c
//                            *************

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <earthworm.h>
#include <transport.h>
#include <kom.h>

static char MyModuleId[20];    // Name of this module
static char RingName[20];      // Name of transport ring

long     HeartBeatInterval;    // Seconds between heartbeats
int      DrinkInterval;        // Milliseconds between port reads
int      NPort;                // Number of Digi ports
int      InputQueueSize;       // Size of comm input queue
int      UnitSec;              // If 1, require unitsec for extrapolation
char     StaFileName[40];      // Name of dst station file
int      RbSize;               // Size of receive buffer, in bytes
int      PbSize;               // Size of packet buffer, in packets per port

// Things to look up in the earthworm.h tables
// *******************************************
long     RingKey;              // Key of shared memory ring
UCHAR    InstId;               // Local installation id
UCHAR    MyModId;              // Module Id for this program
UCHAR    TypeHeartBeat;        // Heartbeat message type
UCHAR    TypeError;            // Error message type
MSG_LOGO WaveLogo;             // Logo of waveform messages


     /***********************************************************
      *                       GetConfig()                       *
      *                                                         *
      *    Processes command file(s) using kom.c functions.     *
      *    Exits if any errors are encountered.                 *
      ***********************************************************/

#define NCOMMAND 10                // Number of commands to process

void GetConfig( char *configfile )
{
   char  init[NCOMMAND];           // Init flags, one for each command
   int   nmiss;                    // Number of commands that were missed
   char  *com;
   char  *str;
   int   nfiles;
   int   success;
   int   i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < NCOMMAND; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      printf( "Error opening command file <%s>. Exiting.\n", configfile );
      exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
        while ( k_rd() )           /* Read next line from active file */
        {
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
           *****************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file
           ********************************/
            if( com[0] == '@' )
            {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success )
               {
                  printf( "Error opening command file <%s>. Exiting.\n",
                          &com[1] );
                  exit( -1 );
               }
               continue;
            }

/* Process anything else as a command
   **********************************/
            else if ( k_its( "MyModuleId" ) )
            {
                str = k_str();
                if (str) strcpy( (char *)MyModuleId, str );
                init[0] = 1;
            }
            else if ( k_its( "RingName" ) )
            {
                str = k_str();
                if (str) strcpy( RingName, str );
                init[1] = 1;
            }
            else if ( k_its( "HeartBeatInterval" ) )
            {
                HeartBeatInterval = k_long();
                init[2] = 1;
            }
            else if ( k_its( "DrinkInterval" ) )
            {
                DrinkInterval = k_int();
                init[3] = 1;
            }
            else if ( k_its( "NPort" ) )
            {
                NPort = k_int();
                init[4] = 1;
            }
            else if ( k_its( "InputQueueSize" ) )
            {
                InputQueueSize = k_int();
                init[5] = 1;
            }
            else if ( k_its( "UnitSec" ) )
            {
                UnitSec = k_int();
                init[6] = 1;
            }
            else if ( k_its( "StaFileName" ) )
            {
                str = k_str();
                if (str) strcpy( StaFileName, str );
                init[7] = 1;
            }
            else if ( k_its( "RbSize" ) )
            {
                RbSize = k_int();
                init[8] = 1;
            }
            else if ( k_its( "PbSize" ) )
            {
                PbSize = k_int();
                init[9] = 1;
            }

/* Unknown command
   ***************/
            else
            {
                printf( "<%s> Unknown command in <%s>.\n", com, configfile );
                continue;
            }

/* See if there were any errors processing the command
   ***************************************************/
            if( k_err() )
            {
               printf( "Bad <%s> command in <%s>. Exiting.\n", com,
                        configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < NCOMMAND; i++ )
      if ( !init[i] ) nmiss++;
   if ( nmiss )
   {
      printf( "ERROR, no " );
      if ( !init[0] ) printf( "<MyModuleId> " );
      if ( !init[1] ) printf( "<RingName> " );
      if ( !init[2] ) printf( "<HeartBeatInterval> " );
      if ( !init[3] ) printf( "<DrinkInterval> " );
      if ( !init[4] ) printf( "<NPort> " );
      if ( !init[5] ) printf( "<InputQueueSize> " );
      if ( !init[6] ) printf( "<UnitSec> " );
      if ( !init[7] ) printf( "<StaFileName> " );
      if ( !init[8] ) printf( "<RbSize> " );
      if ( !init[9] ) printf( "<PbSize> " );
      printf( "command(s) in <%s>.  Exiting.\n", configfile );
      exit( -1 );
   }
   return;
}


     /**************************************************************
      *                         LogConfig()                        *
      *                                                            *
      *  Print the configuration file parameters to the log file.  *
      **************************************************************/

void LogConfig( void )
{
   logit( "", "\n" );
   logit( "", "            Configuration File\n" );
   logit( "", "            ------------------\n" );
   logit( "", "       MyModuleId:        %s\n", MyModuleId );
   logit( "", "       RingName:          %s\n", RingName );
   logit( "", "       NPort:             %d\n", NPort );
   logit( "", "       DrinkInterval:     %d millisec\n", DrinkInterval );
   logit( "", "       HeartBeatInterval: %ld seconds\n", HeartBeatInterval );
   logit( "", "       InputQueueSize:    %d bytes per port\n", InputQueueSize );
   logit( "", "       UnitSec:           %d\n", UnitSec );
   logit( "", "       StaFileName:       %s\n", StaFileName );
   logit( "", "       RbSize:            %d bytes per port\n", RbSize );
   logit( "", "       PbSize:            %d packets per port\n", PbSize );
   logit( "",  "\n" );
   return;
}


    /***************************************************************
     *                       GetUtilLookup()                       *
     *                                                             *
     *       Look up important info from earthworm.h tables        *
     ***************************************************************/

void GetUtilLookup( void )
{
   UCHAR TypeTracebuf;

/* Look up key to shared memory region
   ***********************************/
   if ( ( RingKey = GetKey( RingName ) ) == -1 )
   {
      printf( "Invalid ring name <%s>. Exiting.\n", RingName );
      exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "Error getting local installation id. Exiting.\n" );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModuleId, &MyModId ) != 0 )
   {
      printf( "Invalid module name <%s>. Exiting.\n", MyModuleId );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      exit( -1 );
   }

   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "Invalid message type <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }

   if ( GetType( "TYPE_TRACEBUF", &TypeTracebuf ) != 0 )
   {
      printf( "Invalid message type <TYPE_TRACEBUF>. Exiting.\n" );
      exit( -1 );
   }

// Initialize logo of waveform messages
// ************************************
   WaveLogo.type   = TypeTracebuf;
   WaveLogo.mod    = MyModId;
   WaveLogo.instid = InstId;
   return;
}

