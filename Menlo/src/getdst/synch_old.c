/*
   synch.c
*/

#include <earthworm.h>
#include "getdst.h"

static const UCHAR StartByte = STARTBYTE;
static const UCHAR StopByte  = STOPBYTE;


     /****************************************************************
      *                        DecodePacket()                        *
      *                                                              *
      *  Extract packet index and 22-bit samples from the packet     *
      *  bytes.                                                      *
      *                                                              *
      *  p = Pointer to packet structure                             *
      *  b = Pointer to 12 packet bytes                              *
      ****************************************************************/

void DecodePacket( PACKET *p, UCHAR *b )
{

// Get the packet index
// ********************
   p->index = ((b[2]  & 0x03) << 5) |
              ((b[5]  & 0x03) << 3) |
              ((b[8]  & 0x03) << 1) |
              ((b[11] & 0x02) >> 1);

// Get 22-bit samples for channels 1-4
// ***********************************
   p->samp[0] = ( b[0]          << 14) |
                ( b[1]          <<  6) |
                ((b[2]  & 0xfc) >>  2);

   p->samp[1] = ( b[3]          << 14) |
                ( b[4]          <<  6) |
                ((b[5]  & 0xfc) >>  2);

   p->samp[2] = ( b[6]          << 14) |
                ( b[7]          <<  6) |
                ((b[8]  & 0xfc) >>  2);

   p->samp[3] = ( b[9]          << 14) |
                ( b[10]         <<  6) |
                ((b[11] & 0xfc) >>  2);

// Sign extend from 22 to 32 bits
// ******************************
   p->samp[0] |= (b[0] & 0x80) ? 0xffc00000 : 0x00000000;
   p->samp[1] |= (b[3] & 0x80) ? 0xffc00000 : 0x00000000;
   p->samp[2] |= (b[6] & 0x80) ? 0xffc00000 : 0x00000000;
   p->samp[3] |= (b[9] & 0x80) ? 0xffc00000 : 0x00000000;

// Extract the time bit for this packet
// ************************************
   p->timebit = (b[11] & 0x01) ? 1 : 0;

   return;
}


     /****************************************************************
      *                         CopyPacket()                         *
      *                                                              *
      *  Copy one packet from the receive buffer (rb) to the packet  *
      *  buffer (pb).                                                *
      *                                                              *
      *  Returns 0 if a packet was successfully copied               *
      *         -1 if the receive buffer doesn't contain enough      *
      *            bytes                                             *
      *         -2 if the packet buffer is full                      *
      ****************************************************************/

int CopyPacket( PORT *port )
{
   int npack = port->npack;
   extern int PbSize;            // Packet buffer size, from getdst.d

// We can't copy any more packets if the
// packet buffer is full
// *************************************
   if ( npack >= PbSize ) return -2;

// If we aren't synched, strip off any unexpected bytes
// until two matching pairs of start and stop bytes are found.
// We will never broadcast one-sample messages.
// ***********************************************************
LOSTSYNCH:
   if ( !port->synch )
   {
      do
      {
         int i;              // Loop index

// Are there enough bytes to contain two complete packets?
// If not, wait for more bytes to show up in the receive buffer.
// ************************************************************
         if ( port->nbyte < (2 * PACKETSIZE) ) return -1;

// Find the next start byte in the receive buffer, if any
// ******************************************************
         for ( i = 1; i < port->nbyte; i++ )
            if ( port->rb[i] == StartByte ) break;

// Get rid of all bytes that precede the next start byte.
// If there are no more start bytes, clear the receive buffer.
// **********************************************************
         port->nbyte -= i;
         if ( port->nbyte > 0 )
            memmove( port->rb, &port->rb[i], port->nbyte );

// Does the receive buffer start with two matching pairs
// of start and stop bytes?
// *****************************************************
      } while ( (port->rb[13] != StopByte  ) ||
                (port->rb[14] != StartByte ) ||
                (port->rb[27] != StopByte  ) );

      port->synch = 1;    // Synch achieved
   }

// We are in synch.  Are there are enough bytes in
// the receive buffer to make a single packet?
// ***********************************************
   if ( port->nbyte < PACKETSIZE ) return -1;

// If there aren't start and stop bytes in the correct
// positions, we lost synch.
// Search the receive buffer for start and stop bytes.
// ***************************************************
   if ( (port->rb[0]  != StartByte ) ||
        (port->rb[13] != StopByte  ) )
   {
      port->synch = 0;
      goto LOSTSYNCH;
   }

// Decode the packet and copy to the packet buffer.
// ***********************************************
   DecodePacket( &port->pb[npack], &port->rb[1] );
   port->npack++;

// Discard the packet from the receive buffer
// ******************************************
   port->nbyte -= 14;
   memmove( port->rb, &port->rb[14], port->nbyte );
   return 0;         // Success!
}


     /*****************************************************************
      *                           GetFrame()                          *
      *                                                               *
      *  Copy one frame from packet buffer to frame buffer.           *
      *                                                               *
      *  Returns 1 if a frame was successfully copied                 *
      *          0 if packet buffer doesn't contain a complete frame  *
      *****************************************************************/

int GetFrame( PORT *port )
{
   int i;
   int i2 = -1;
   int npacket;

// Find the first frame boundary.
// i2 is index of last packet in current frame.
// *******************************************
   for ( i = 0; i < port->npack-1; i++ )
   {
      if ( port->pb[i].index >= port->pb[i+1].index )
      {
         i2 = i;
         break;
      }
   }
   if ( i2 == -1 ) return 0;     // No frame boundaries found

// Compute number of packets in current frame
// ******************************************
   npacket = i2 + 1;
// if ( npacket != NSCAN )
//    logit( "et", "%s %d packets in frame. Should be %d.\n", port->ch[0].sta,
//       npacket, NSCAN );

// Clear the frame buffer
// **********************
   for ( i = 0; i < NSCAN; i++ )
      port->fr.stat[i] = 0;

// Copy packets from packet buffer to frame buffer
// ***********************************************
   for ( i = 0; i < npacket; i++ )
   {
      int index = port->pb[i].index;

      if ( (index < 0) || (index > 99) )
         logit( "et", "Warning. Bad index: %d\n", index );

      port->fr.pb[index]   = port->pb[i];
      port->fr.stat[index] = 1;
   }

// Remove packets for this frame from packet buffer
// ************************************************
// printf( "npacket: %d npack: %d\n", npacket, port->npack );
   port->npack -= npacket;

   for ( i = 0; i < port->npack; i++ )
      port->pb[i] = port->pb[i + npacket];

   return 1;                  // Success
}
