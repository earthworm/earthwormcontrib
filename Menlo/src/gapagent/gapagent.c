
  /*****************************************************************
   *                           gapagent.c                          *
   *                                                               *
   *  Program to search for gaps in tracebuf messages and print    *
   *  tables.                                                      *
   *****************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <transport.h>
#include <time_ew.h>
#include <earthworm.h>
#include <trace_buf.h>
#include <chron3.h>
#include <kom.h>
#include <swap.h>
#include <trheadconv.h>
#include "gapagent.h"

/* Windows provides a non-ANSI version of snprintf,
   name _snprintf.  The Windows version has a
   different return value than ANSI snprintf.
   ***********************************************/
#ifdef _WINNT
#define snprintf _snprintf
#endif

/* Function prototypes
   *******************/
void GetConfig( char *, PARM * );
void LogConfig( PARM * );
void GetEwd( void );
void GetStationList( char StationList[] );
void ChangeEpoch( SNCL *sncl );

/* Global variables
   ****************/
extern int           nSncl;           /* # of sncl's to monitor */
extern SNCL          *sncl;           /* Array of sncl parameters */
static char          *TraceBuf;       /* The tracebuf buffer */
static TRACE_HEADER  *TraceHead;      /* The tracebuf header */
static TRACE2_HEADER *Trace2Head;     /* The tracebuf2 header */
static PARM          parm;            /* Configuration file parameters */
pid_t                myPid;           /* for restarts by startstop */
static SHM_INFO      region;          /* Shared memory region */
static MSG_LOGO      errlogo;         /* Logo of outgoing errors */
static FILE          *fpctemp;        /* File pointer */

/* Values set by GetEwd(), in config.c
   ***********************************/
unsigned char        inst_wildcard;   /* From earthworm.d */
unsigned char        inst_local;      /* From earthworm.d */
unsigned char        type_tracebuf;   /* From earthworm.d */
unsigned char        type_tracebuf2;  /* From earthworm.d */
unsigned char        type_tracecomp;  /* From earthworm.d */
unsigned char        type_trace2comp; /* From earthworm.d */
unsigned char        type_heartbeat;  /* From earthworm.d */
unsigned char        type_error;      /* From earthworm.d */
unsigned char        mod_wildcard;    /* From earthworm.d */
unsigned char        mod_gapagent;    /* From earthworm.d */


         /*************************************************
          *        The main program starts here.          *
          *************************************************/

int main( int argc, char *argv[] )
{
   int      i;                    /* Index value */
   MSG_LOGO logo;
   MSG_LOGO hrtlogo;              /* Logo of outgoing heartbeats */
   long     gotsize;
   int      res;
   time_t   prevPrintTime;
   time_t   prevHeartTime;
   unsigned char seq;
   time_t   now;                  /* Current time */
   div_t    d;
   char     tempname[FNLEN];
   unsigned thread_id;

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      printf( "Usage: gapagent <configfile>\n" );
      return -1;
   }

/* Open log file
   *************/
   logit_init( argv[1], 0, 256, 1 );

/* Read things from earthworm*d files.
   GetEwd() exits on error.
   **********************************/
   GetEwd();

/* Get parameters from the configuration files.
   GetConfig() exits on error.
   *******************************************/
   GetConfig( argv[1], &parm );

/* Read station list from file
   ***************************/
   GetStationList( parm.StationList );

/* Get the module id for this process
   **********************************/
   if ( GetModId( parm.MyModName, &mod_gapagent ) != 0 )
   {
      logit("", "gapagent: Error getting %s. Exiting.\n", parm.MyModName );
      return -1;
   }

/* Specify logos of incoming waveforms, outgoing heartbeats, errors
   ****************************************************************/
   if ( parm.nGetLogo == 0 )
   {
      parm.nGetLogo = 2;
      parm.GetLogo  = (MSG_LOGO *) calloc( parm.nGetLogo, sizeof(MSG_LOGO) );

      if ( parm.GetLogo == NULL )
      {
         logit( "e", "gapagent: Error allocating space for GetLogo. Exiting\n" );
         return -1;
      }
      parm.GetLogo[0].instid = inst_wildcard;
      parm.GetLogo[0].mod    = mod_wildcard;
      parm.GetLogo[0].type   = type_tracebuf2;

      parm.GetLogo[1].instid = inst_wildcard;
      parm.GetLogo[1].mod    = mod_wildcard;
      parm.GetLogo[1].type   = type_tracebuf;
   }

   hrtlogo.instid = inst_local;
   hrtlogo.type   = type_heartbeat;
   hrtlogo.mod    = mod_gapagent;

   errlogo.instid = inst_local;
   errlogo.type   = type_error;
   errlogo.mod    = mod_gapagent;

/* Log the configuration parameters
   ********************************/
   LogConfig( &parm );

/* Get process ID for heartbeat messages
   *************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
     logit( "e","gapagent: Cannot get pid. Exiting.\n" );
     return -1;
   }

/* Allocate the trace buffer
   *************************/
   TraceBuf = (char *) malloc( MAX_TRACEBUF_SIZ );
   if ( TraceBuf == NULL )
   {
      logit( "e", "Error allocating trace buffer.  Exiting.\n" );
      return -1;
   }
   TraceHead  = (TRACE_HEADER *)TraceBuf;
   Trace2Head = (TRACE2_HEADER *)TraceBuf;

/* Attach to transport ring
   ************************/
   tport_attach( &region, parm.InKey );

/* Flush transport ring on startup
   *******************************/
   while ( tport_copyfrom( &region, parm.GetLogo, parm.nGetLogo,
                           &logo, &gotsize,
                           TraceBuf, MAX_TRACEBUF_SIZ, &seq ) != GET_NONE );

/* Get the time we start reading messages
   **************************************/
   time( &prevPrintTime );
   time( &prevHeartTime );

/* Determine epoch of data to collect
   **********************************/
   time( &now );
   d = div( (int)now, parm.EpochLen );
   for ( i = 0; i < nSncl; i++ )
   {
      sncl[i].epoch = ( d.rem < (parm.EpochLen - parm.WarmUp) ) ? d.quot+1 : d.quot+2;
      sncl[i].epochdatalen = 0.0;
   }

/* Open temporary file to contain completeness values
   **************************************************/
   if ( chdir_ew( parm.TempDir ) == -1 )
   {
      logit( "e", "Error changing working directory to %s\n", parm.TempDir );
      logit( "e", "Exiting.\n" );
      free( parm.GetLogo );
      return -1;
   }
   sprintf( tempname, "Completeness_10min_%d", sncl[0].epoch );
   fpctemp = fopen( tempname, "w" );
   if ( fpctemp == NULL )
   {
      logit( "e", "Can't open file %s\n", tempname );
      logit( "e", "Exiting.\n" );
      free( parm.GetLogo );
      return -1;
   }
// logit( "e", "Temporary file: %s\n", tempname );

/* See if termination flag has been set
   ************************************/
   while ( 1 )
   {
      int epochChanged;

//    logit( "e", "Got to here\n" );
      sleep_ew( 100 );

      if ( tport_getflag( &region ) == TERMINATE ||
           tport_getflag( &region ) == myPid )
         break;

/* Send a heartbeat to the transport ring
   **************************************/
      time( &now );
      if ( parm.HeartbeatInt > 0 )
      {
         if ( (now - prevHeartTime) >= (time_t)parm.HeartbeatInt )
         {
            int  lineLen;
            char line[40];

            prevHeartTime = now;

            snprintf( line, sizeof(line), "%ld %ld\n", (long)now, myPid );
            line[sizeof(line)-1] = 0;
            lineLen = strlen( line );

            if ( tport_putmsg( &region, &hrtlogo, lineLen, line ) !=
                 PUT_OK )
            {
               logit( "et", "gapagent: Error sending heartbeat. Exiting." );
               free( parm.GetLogo );
               return -1;
            }
         }
      }

/* For each station, if no data have arrived after
   maxlatency seconds into the new epoch, change the epoch.
   *******************************************************/
      time( &now );
      for ( i = 0; i < nSncl; i++ )
      {
         if ( now > (time_t)((sncl[i].epoch + 1)*parm.EpochLen + parm.MaxLatency) )
         {
            ChangeEpoch( &sncl[i] );
            sncl[i].epochdatalen = 0.0;
         }
      }

/* See if the epoch has changed for all channels
   *********************************************/
      epochChanged = 1;
      for ( i = 0; i < nSncl; i++ )
      {
         if ( sncl[i].epochChanged == 0 )
         {
            epochChanged = 0;
            break;
         }
      }
      if ( epochChanged )
      {
         char path1[FNLEN];
         char path2[FNLEN];

/* Close the temporary file and move it
   to its final destination
   ************************************/
         fclose( fpctemp );
         strcpy( path1, parm.TempDir );
#ifdef _WINNT
         strcat( path1, "\\" );
#endif
#ifdef _SOLARIS
         strcat( path1, "/" );
#endif
         strcat( path1, tempname );

         strcpy( path2, parm.GapWriteDir );

#ifdef _WINNT
         strcat( path2, "\\" );
#endif
#ifdef _SOLARIS
         strcat( path2, "/" );
#endif
         strcat( path2, tempname );

         logit( "e", "Renaming %s to %s\n", path1, path2 );
         if ( rename_ew( path1, path2 ) == -1 )
         {
            logit( "e", "Error moving temporary file. Exiting.\n" );
            return -1;
         }

/* Open a new temporary file
   *************************/
         sprintf( tempname, "Completeness_10min_%d", sncl[0].epoch );
         fpctemp = fopen( tempname, "w" );
         if ( fpctemp == NULL )
         {
            logit( "e", "Can't open file %s\n", tempname );
            logit( "e", "Exiting.\n" );
            free( parm.GetLogo );
            return -1;
         }
         logit( "e", "Temporary file: %s\n", tempname );
         for ( i = 0; i < nSncl; i++ )
            sncl[i].epochChanged = 0;
      }

/* Get all available tracebuf/tracebuf2 messages
   *********************************************/
      while ( 1 )
      {
         int    i;                     /* sncl index */
         double tstart, tend;          /* start/end times of tracebuf msg */
         double epochstart, epochend;  /* start/end times of epoch */
         double hsint;                 /* half sample interval */

         res = tport_copyfrom( &region, parm.GetLogo, parm.nGetLogo,
                               &logo, &gotsize,
                               TraceBuf, MAX_TRACEBUF_SIZ, &seq );
         if ( res == GET_NONE )
            break;
         if ( res == GET_TOOBIG )
         {
            logit( "et", "gapagent: Retrieved message is too big (%d)\n", gotsize );
            break;
         }
         if ( res == GET_NOTRACK )
            logit( "et", "gapagent: NTRACK_GET exceeded.\n" );
         if ( res == GET_MISS_LAPPED )
            logit( "et", "gapagent: GET_MISS_LAPPED error.\n" );
         if ( res == GET_MISS_SEQGAP );     /* Do nothing */

/* If necessary, swap bytes in tracebuf message.
/* Convert TYPE_TRACEBUF messages to TYPE_TRACEBUF2.
   ************************************************/
         if ( logo.type == type_tracebuf || logo.type == type_tracecomp )
         {
            if ( WaveMsgMakeLocal( TraceHead ) < 0 )
            {
               logit( "et", "gapagent: WaveMsgMakeLocal() error.\n" );
               continue;
            }
            Trace2Head = TrHeadConv( TraceHead );
         }
         else
            if ( WaveMsg2MakeLocal( Trace2Head ) < 0 )
            {
               logit( "et", "gapagent: WaveMsg2MakeLocal() error.\n" );
               continue;
            }

/* If this sncl is in our channel list, get its
   index: i.  Old-style tracebuf messages are
   assumed to have location code --.
   ********************************************/
         for ( i = 0; i < nSncl; i++ )
         {
            if ( strcmp(sncl[i].sta,  Trace2Head->sta  ) == 0 &&
                 strcmp(sncl[i].net,  Trace2Head->net  ) == 0 &&
                 strcmp(sncl[i].chan, Trace2Head->chan ) == 0 )
            {
               if ( logo.type == type_tracebuf || logo.type == type_tracecomp )
                  strcpy( Trace2Head->loc, sncl[i].loc );
               if ( strcmp( sncl[i].loc, Trace2Head->loc ) == 0 )
                  break;
            }
         }
         if ( i == nSncl ) continue;    /* sncl not in list */

/* Add data length from this message to the total for this epoch.
   If the message contains data later than the current epoch,
   change epochs.
   *************************************************************/
         hsint      = 0.5 / Trace2Head->samprate;
         tstart     = Trace2Head->starttime - hsint;
         tend       = Trace2Head->endtime  + hsint;
         epochstart = sncl[i].epoch * parm.EpochLen;
         epochend   = epochstart + parm.EpochLen;

         if ( (tstart < epochstart) && (tend > epochstart) )
            sncl[i].epochdatalen += (tend - epochstart);

         if ( (tstart > epochstart) && (tend < epochend) )
            sncl[i].epochdatalen += (tend - tstart);

         if ( (tstart < epochend) && (tend > epochend) )
         {
            sncl[i].epochdatalen += (epochend - tstart);
            ChangeEpoch( &sncl[i] );
            sncl[i].epochdatalen = tend - epochend;
         }

         if ( tstart > epochend )
         {
            ChangeEpoch( &sncl[i] );
            sncl[i].epochdatalen = tend - tstart;
         }
      }
   }

/* Shut down program
   *****************/
   logit( "t", "Termination flag detected. Program stopping.\n" );
   tport_detach( &region );
   fclose( fpctemp );
   remove( tempname );
   return 0;
}


   /**************************************************************
    *                        ChangeEpoch()                       *
    *    Log the percent data received in the previous epoch     *
    *              and increment the epoch counter               *
    **************************************************************/

void ChangeEpoch( SNCL *sncl )
{
   double Completeness_10min;

   if ( sncl->epochChanged ) return;
   Completeness_10min = 100.0 * sncl->epochdatalen / parm.EpochLen;
   printf( "%s-%-5s", sncl->net, sncl->sta );
   printf( " Completeness_10min: %6.2lf\n", Completeness_10min );
   fprintf( fpctemp, "%s-%s:1:Completeness_10min (percent)=%.2lf\n",
            sncl->net, sncl->sta, Completeness_10min );
   sncl->epoch++;
   sncl->epochChanged = 1;
   return;
}

