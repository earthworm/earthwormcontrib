
/******************************************************************
 *                       File dailyreport.h                       *
 ******************************************************************/

#include <time.h>
#include <earthworm.h>
#include <trace_buf.h>

#define LABEL_LEN 32              /* Must be a multiple of 4 */
#define FNLEN     80              /* Max length of station list file name */

/* Structure definitions
   *********************/
typedef struct
{
   char   StationList[FNLEN];     /* Name of station list file */
   int    EpochLen;               /* Length of epoch, in seconds */
   char   GapDir[FNLEN];          /* Completeness files are read here */
   int    MinEpoch;               /* Min number of epochs for daily calc */
   char   TempDir[FNLEN];         /* Name of temp directory */
   char   OutDir[FNLEN];          /* Daily gap files are written here */
} PARM;

typedef struct
{
   char   sta[TRACE2_STA_LEN];    /* Station code */
   char   net[TRACE2_NET_LEN];    /* Network code */
   char   chan[TRACE2_CHAN_LEN];  /* Component code */
   char   loc[TRACE2_LOC_LEN];    /* Location code */
   char   staname[50];            /* Station name */
   int    nEpoch;                 /* Num epochs recorded in previous day */
   double dailyAve;               /* Ave completeness for previous day */
} SNCL;

typedef struct
{
   int  year;      /* Year minus 1900 */
   int  yday;      /* Day of year (0-365) */
} GAPDATE;

