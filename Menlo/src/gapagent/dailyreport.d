#
#                     DailyReport's Configuration File
#
# Length of epoch, in seconds.  An epoch is the time period
# over which data completeness is calculated.
EpochLen  600     # Length of epoch, in seconds

# Name of file containing station list
StationList /home/earthworm/run/params/Station_List

# Completeness files are read from this directory.
GapDir /home/earthworm/run/comp10

# Minimum number of epochs required for calculation of
# daily percent completeness.
MinEpoch   10

# Specify directory to contain temporary files, which are erased
# after use.
TempDir /home/earthworm/run-will/temp

# Daily completeness files are written to this directory.
OutDir /home/earthworm/run-will/out
