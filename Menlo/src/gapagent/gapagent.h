
/******************************************************************
 *                        File gapagent.h                         *
 ******************************************************************/

#include <time.h>
#include <earthworm.h>
#include <trace_buf.h>

#define LABEL_LEN 32              /* Must be a multiple of 4 */
#define FNLEN     80              /* Max length of station list file name */

/* Structure definitions
   *********************/
typedef struct
{
   char   InRing[MAX_RING_STR];   /* Name of ring containing tracebuf messages */
   char   MyModName[MAX_MOD_STR]; /* Module name */
   long   InKey;                  /* Key to ring where waveforms live */
   int    HeartbeatInt;           /* Heartbeat interval in seconds */
   char   StationList[FNLEN];     /* Name of station list file */
   short     nGetLogo;            /* Number of logos in GetLogo   */
   MSG_LOGO *GetLogo;             /* Logos of requested waveforms */
   int    EpochLen;               /* Length of epoch, in seconds */
   int    WarmUp;
   int    MaxLatency;
   char   TempDir[FNLEN];         /* Name of temp directory */
   char   GapWriteDir[FNLEN];     /* Completeness files are written here */
   char   GapDir[FNLEN];          /* Completeness files are read here */
} PARM;

typedef struct
{
   char   sta[TRACE2_STA_LEN];    /* Station code */
   char   net[TRACE2_NET_LEN];    /* Network code */
   char   chan[TRACE2_CHAN_LEN];  /* Component code */
   char   loc[TRACE2_LOC_LEN];    /* Location code */
   char   staname[50];            /* Station name */
   int    epoch;                  /* Epoch of data being collected */
   double epochdatalen;           /* Seconds */
   int    epochChanged;           /* 0 or 1 */
} SNCL;

typedef struct
{
   int  year;      /* Year minus 1900 */
   int  yday;      /* Day of year (0-365) */
} GAPDATE;

