#
#                     Gapagent's Configuration File
#
InRing          WAVE_RING    # Transport ring to find waveform data on,
MyModuleId   MOD_GAPAGENT    # Module id for this instance of gapagent,
HeartbeatInt           15    # Heartbeat interval in secs (if 0, send no heartbeats)

# An epoch is the time period over which data completeness
# is calculated.
EpochLen             600     # Length of epoch, in seconds

# At startup, gapagent must be running this many seconds before
# the beginning of an epoch.  Set this parameter to the maximum
# expected drift of the PC system clock.
WarmUp 30

# Gapagent will report the data completeness of a channel this
# many seconds after the end of an epoch, even if no data have
# been received for the succeeding epoch.
MaxLatency 120

# Name of file containing station list
StationList c:\earthworm\run\params\Station_List

# Specify which messages to look at with Getlogo commands.
#   GetLogo <installation_id> <module_id> <message_type>
# The message_type must be one of these four:
#   TYPE_TRACEBUF,      TYPE_TRACEBUF2,
#   TYPE_TRACE_COMP_UA, TYPE_TRACE2_COMP_UA
# Use as many GetLogo commands as you need.
# If no GetLogo commands are given, gapagent will look at all
# TYPE_TRACEBUF and TYPE_TRACEBUF2 messages in InRing.
#------------------------------------------------------------
GetLogo  INST_WILDCARD  MOD_WILDCARD  TYPE_TRACEBUF
GetLogo  INST_WILDCARD  MOD_WILDCARD  TYPE_TRACEBUF2

# Specify directory to contain temporary files, which are erased
# after use.
TempDir /home/earthworm/run/temp

# Completeness files are written to this directory.
GapWriteDir C:\earthworm\run\gap

# Completeness files are read from this directory.
GapDir /home/earthworm/run/gap
