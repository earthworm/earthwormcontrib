
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>
#include <trace_buf.h>
#include "gapagent.h"

/* Values set by GetEwd(), below
   *****************************/
extern unsigned char inst_wildcard;
extern unsigned char inst_local;
extern unsigned char type_tracebuf;
extern unsigned char type_tracebuf2;
extern unsigned char type_tracecomp;
extern unsigned char type_trace2comp;
extern unsigned char type_heartbeat;
extern unsigned char type_error;
extern unsigned char mod_wildcard;
extern unsigned char mod_gapagent;

#define ncommand 10     /* Number of commands in the config file */

int  nSncl = 0;         /* Number of sncl's to monitor */
SNCL *sncl = NULL;      /* Array of sncl structures */


   /*****************************************************************
    *                          GetConfig()                          *
    *         Processes command file using kom.c functions.         *
    *****************************************************************/

void GetConfig( char *config_file, PARM *parm )
{
   char init[ncommand];     /* Flags, one for each command */
   int  nmiss;              /* Number of commands that were missed */
   int  nfiles;
   int  i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ ) init[i] = 0;

/* Initialize Configuration parameters
   ***********************************/
   parm->nGetLogo = 0;
   parm->GetLogo  = NULL;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( config_file );
   if ( nfiles == 0 )
   {
      logit( "e", "gapagent: Error opening configuration file <%s> Exiting.\n",
              config_file );
      exit( -1 );
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          /* Get the first token from line */

         if ( !com ) continue;             /* Ignore blank lines */
         if ( com[0] == '#' ) continue;    /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               logit( "e", "gapagent: Error opening command file <%s>. Exiting.\n",
                       &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "InRing" ) )
         {
            if ( str = k_str() )
            {
               strncpy( parm->InRing, str, MAX_RING_STR );
               parm->InRing[MAX_RING_STR-1] = '\0';

               if( (parm->InKey = GetKey(str)) == -1 )
               {
                  logit( "e", "gapagent: Invalid InRing name <%s>. Exiting.\n", str );
                  exit( -1 );
               }
            }
            init[0] = 1;
         }

         else if ( k_its( "HeartbeatInt" ) )
         {
            parm->HeartbeatInt = k_int();
            init[1] = 1;
         }

         else if ( k_its( "MyModuleId" ) )
         {
            parm->MyModName[0] = '\0';
            if ( str=k_str() )
            {
               strncpy( parm->MyModName, str, MAX_MOD_STR );
               parm->MyModName[MAX_MOD_STR-1] = '\0';
            }
            init[2] = 1;
         }

         else if ( k_its( "StationList" ) )
         {
            parm->StationList[0] = '\0';
            if ( str=k_str() )
            {
               strncpy( parm->StationList, str, FNLEN );
               parm->StationList[FNLEN-1] = '\0';
            }
            init[3] = 1;
         }

         else if ( k_its( "EpochLen" ) )
         {
            parm->EpochLen = k_int();
            init[4] = 1;
         }

         else if ( k_its( "WarmUp" ) )
         {
            parm->WarmUp = k_int();
            init[5] = 1;
         }

         else if ( k_its( "MaxLatency" ) )
         {
            parm->MaxLatency = k_int();
            init[6] = 1;
         }

         else if ( k_its( "TempDir" ) )
         {
            parm->TempDir[0] = '\0';
            if ( str=k_str() )
            {
               strncpy( parm->TempDir, str, FNLEN );
               parm->TempDir[FNLEN-1] = '\0';
            }
            init[7] = 1;
         }

         else if ( k_its( "GapWriteDir" ) )
         {
            parm->GapWriteDir[0] = '\0';
            if ( str=k_str() )
            {
               strncpy( parm->GapWriteDir, str, FNLEN );
               parm->GapWriteDir[FNLEN-1] = '\0';
            }
            init[8] = 1;
         }

         else if ( k_its( "GapDir" ) )
         {
            parm->GapDir[0] = '\0';
            if ( str=k_str() )
            {
               strncpy( parm->GapDir, str, FNLEN );
               parm->GapDir[FNLEN-1] = '\0';
            }
            init[9] = 1;
         }

         else if ( k_its( "GetLogo" ) )      /* Optional */
         {
            MSG_LOGO *tlogo = NULL;
            int       nlogo = parm->nGetLogo;
            tlogo = (MSG_LOGO *)realloc( parm->GetLogo,
                                         (nlogo+1)*sizeof(MSG_LOGO) );
            if( tlogo == NULL )
            {
               logit( "e", "gapagent: GetLogo: error reallocing %d bytes."
                      " Exiting.\n", (nlogo+1)*sizeof(MSG_LOGO) );
               exit( -1 );
            }
            parm->GetLogo = tlogo;

            if( str=k_str() )       /* read instid */
            {
               if( GetInst( str, &(parm->GetLogo[nlogo].instid) ) != 0 )
               {
                  logit( "e", "gapagent: Invalid installation name <%s>"
                         " in <GetLogo> cmd. Exiting.\n", str );
                  exit( -1 );
               }
               if( str=k_str() )    /* read module id */
               {
                  if( GetModId( str, &(parm->GetLogo[nlogo].mod) ) != 0 )
                  {
                     logit( "e", "gapagent: Invalid module name <%s>"
                            " in <GetLogo> cmd. Exiting.\n", str );
                     exit( -1 );

                  }
                  if( str=k_str() ) /* read message type */
                  {
                     if( strcmp(str,"TYPE_TRACEBUF")      !=0 &&
                         strcmp(str,"TYPE_TRACEBUF2")     !=0 &&
                         strcmp(str,"TYPE_TRACE_COMP_UA") !=0 &&
                         strcmp(str,"TYPE_TRACE2_COMP_UA")!=0    )
                     {
                        logit( "e","gapagent: Invalid message type <%s> in <GetLogo>"
                               " cmd; must be TYPE_TRACEBUF, TYPE_TRACEBUF2,"
                               " TYPE_TRACE_COMP_UA, or TYPE_TRACE2_COMP_UA;"
                               " Exiting.\n", str );
                        exit( -1 );
                     }
                     if( GetType( str, &(parm->GetLogo[nlogo].type) ) != 0 ) {
                        logit( "e", "gapagent: Invalid message type <%s>"
                               " in <GetLogo> cmd. Exiting.\n", str );
                        exit( -1 );
                     }
                     parm->nGetLogo++;
                  } /* end if msgtype */
               } /* end if modid */
            } /* end if instid */
         } /* end GetLogo cmd */

/* An unknown parameter was encountered
   ************************************/
         else
         {
            logit( "e", "gapagent: <%s> unknown parameter in <%s>\n",
                    com, config_file );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            logit( "e", "gapagent: Bad <%s> command in <%s>. Exiting.\n", com,
                    config_file );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss > 0 )
   {
      logit( "e", "gapagent: ERROR, no " );
      if ( !init[0] ) logit( "e", "<InRing> " );
      if ( !init[1] ) logit( "e", "<HeartbeatInt> " );
      if ( !init[2] ) logit( "e", "<MyModuleId> " );
      if ( !init[3] ) logit( "e", "<StationList> " );
      if ( !init[4] ) logit( "e", "<EpochLen> " );
      if ( !init[5] ) logit( "e", "<WarmUp> " );
      if ( !init[6] ) logit( "e", "<MaxLatency> " );
      if ( !init[7] ) logit( "e", "<TempDir> " );
      if ( !init[8] ) logit( "e", "<GapWriteDir> " );
      if ( !init[9] ) logit( "e", "<GapDir> " );
      logit( "e", "command(s) in <%s>. Exiting.\n", config_file );
      exit( -1 );
   }
   return;
}


   /**************************************************************
    *                      GetStationList()                      *
    *                Read the station list file.                 *
    **************************************************************/

void GetStationList( char StationList[] )
{
   int  snclSize = 0;       /* Num bytes in sncl array */
   SNCL *snclPtr;
   FILE *fp;                /* File pointer */
   char line[120];
   int  i;

/* Open the station list file
   **************************/
   fp = fopen( StationList, "r" );
   if ( fp == NULL )
   {
      logit( "e", "gapagent: Error open station list file. Exiting.\n" );
      exit( -1 );
   }

/* Skip the first three lines in the file
   **************************************/
   for ( i = 0; i < 3; i++ )
   {
      if ( fgets( line, 120, fp ) == NULL )
      {
         logit( "e", "gapagent: Error skipping 3 lines in station list file. Exiting.\n" );
         exit( -1 );
      }
   }

/* Read lines until a blank line is encountered.
   Skip comment lines.
   ********************************************/
   while ( fgets( line, 120, fp ) != NULL )
   {
      char *token;
      char sta[TRACE2_STA_LEN];
      char chan[TRACE2_CHAN_LEN];
      char net[TRACE2_NET_LEN];
      char loc[TRACE2_LOC_LEN];
      char staname[50];
      int  seenIt = 0;

      if ( strlen( line ) < 2 )         /* Found a blank line */
         break;
      if ( line[0] == '#' )             /* Found a comment line */
         continue;

/* Decode the station name
   ***********************/
      token = strtok( line, " " );
      if ( token == NULL )
      {
         logit( "e", "gapagent: Error decoding station name. Exiting.\n" );
         exit( -1 );
      }
      strncpy( sta, token, TRACE2_STA_LEN );
      sta[TRACE2_STA_LEN-1] = '\0';

/* Decode the component code
   *************************/
      token = strtok( NULL, " " );
      if ( token == NULL )
      {
         logit( "e", "gapagent: Error decoding component code. Exiting.\n" );
         exit( -1 );
      }
      strncpy( chan, token, TRACE2_CHAN_LEN );
      chan[TRACE2_CHAN_LEN-1] = '\0';

/* Decode the network code
   ***********************/
      token = strtok( NULL, " " );
      if ( token == NULL )
      {
         logit( "e", "gapagent: Error decoding network code. Exiting.\n" );
         exit( -1 );
      }
      strncpy( net, token, TRACE2_NET_LEN );
      net[TRACE2_NET_LEN-1] = '\0';

/* Next field is either location code or latitude.
   Use location code if available.
   Otherwise set location code to --.
   Skip latitude.
   **********************************************/
      strcpy( loc, "--" );                     /* Set to default value */
      token = strtok( NULL, " " );
      if ( token == NULL )
      {
         logit( "e", "gapagent: Error decoding location code. Exiting.\n" );
         exit( -1 );
      }
      if ( strlen( token ) == 2 )              /* Got location code */
      {
         strcpy( loc, token );
         token = strtok( NULL, " " );          /* Skip latitude */
         if ( token == NULL )
         {
            logit( "e", "gapagent: Error decoding latitude. Exiting.\n" );
            exit( -1 );
         }
      }

/* Skip the longitude field
   ************************/
      token = strtok( NULL, " " );
      if ( token == NULL )
      {
         logit( "e", "gapagent: Error decoding longitude. Exiting.\n" );
         exit( -1 );
      }

/* Skip the elevation field
   ************************/
      token = strtok( NULL, " " );
      if ( token == NULL )
      {
         logit( "e", "gapagent: Error decoding elevation. Exiting.\n" );
         exit( -1 );
      }

/* Get the station name description
   ********************************/
      token = strtok( NULL, "\n" );
      if ( token == NULL )
      {
         logit( "e", "gapagent: Error decoding station name. Exiting.\n" );
         exit( -1 );
      }
      strncpy( staname, token, 50 );
      staname[49] = '\0';

/* Skip this station if we've seen it before
   *****************************************/
      for ( i = 0; i < nSncl; i++ )
      {
         if ( strcmp( sta, sncl[i].sta ) == 0 &&
              strcmp( net, sncl[i].net ) == 0 )
         {
            seenIt = 1;
            break;
         }
      }
      if ( seenIt ) continue;

/* Save this sncl in our list
   **************************/
      snclSize += sizeof( SNCL );
      snclPtr = (SNCL *)realloc( sncl, snclSize );
      if ( snclPtr ==  NULL )
      {
         logit( "e", "gapagent: realloc() error in GetStationList(). Exiting.\n" );
         exit( -1 );
      }
      sncl = snclPtr;
      strcpy( sncl[nSncl].sta,  sta );
      strcpy( sncl[nSncl].chan, chan );
      strcpy( sncl[nSncl].net,  net );
      strcpy( sncl[nSncl].loc,  loc );
      strcpy( sncl[nSncl].staname, staname );
      sncl[nSncl].epochChanged = 0;
      nSncl++;
   }

   fclose( fp );
   return;
}


   /**************************************************************
    *                         LogConfig()                        *
    *              Log the configuration parameters              *
    **************************************************************/

void LogConfig( PARM *parm )
{
   int i;

   logit( "", "\n" );
   logit( "", "InRing:               %s\n", parm->InRing );
   logit( "", "MyModName:            %s\n", parm->MyModName );
   logit( "", "HeartbeatInt:        %6d\n", parm->HeartbeatInt );
   logit( "", "StationList:          %s\n", parm->StationList );
   logit( "", "EpochLen:             %d\n", parm->EpochLen );
   logit( "", "WarmUp:               %d\n", parm->WarmUp );
   logit( "", "MaxLatency:           %d\n", parm->MaxLatency );
   logit( "", "TempDir:              %s\n", parm->TempDir );
   logit( "", "GapWriteDir:          %s\n", parm->GapWriteDir );
   logit( "", "GapDir:               %s\n", parm->GapDir );
   logit( "", "nGetLogo:            %6d\n", parm->nGetLogo );
   for( i=0; i<parm->nGetLogo; i++ )
   {
      logit( "", "GetLogo[%d]:   i%u m%u t%u\n", i,
            parm->GetLogo[i].instid, parm->GetLogo[i].mod,
            parm->GetLogo[i].type );
   }
   logit( "", "nSncl:               %6d\n", nSncl );

   logit( "", "\nChannels monitored by gapagent:\n" );
   for ( i = 0; i < nSncl; i++ )
   {
      logit( "", "Sncl     %5s %3s %2s %2s  %s\n",
             sncl[i].sta,  sncl[i].net, sncl[i].chan, sncl[i].loc,
             sncl[i].staname );
   }
   logit( "", "\n" );
   return;
}


             /********************************************
              *                 GetEwd()                 *
              *    Read things from earthworm*d files    *
              ********************************************/
void GetEwd( void )
{
   if ( GetInst( "INST_WILDCARD", &inst_wildcard ) != 0 )
   {
      logit( "et", "gapagent: Error getting INST_WILDCARD. Exiting.\n" );
      exit( -1 );
   }
   if ( GetLocalInst( &inst_local ) != 0 )
   {
      logit( "et", "gapagent: Error getting MyInstId.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF", &type_tracebuf ) != 0 )
   {
      logit( "et", "gapagent: Error getting <TYPE_TRACEBUF>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF2", &type_tracebuf2 ) != 0 )
   {
      logit( "et", "gapagent: Error getting <TYPE_TRACEBUF2>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACE_COMP_UA", &type_tracecomp ) != 0 )
   {
      logit( "et", "gapagent: Error getting <TYPE_TRACE_COMP_UA>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACE2_COMP_UA", &type_trace2comp ) != 0 )
   {
      logit( "et", "gapagent: Error getting <TYPE_TRACE2_COMP_UA>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_HEARTBEAT", &type_heartbeat ) != 0 )
   {
      logit( "et", "gapagent: Error getting <TYPE_HEARTBEAT>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &type_error ) != 0 )
   {
      logit( "et", "gapagent: Error getting <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetModId( "MOD_WILDCARD", &mod_wildcard ) != 0 )
   {
      logit( "et", "gapagent: Error getting MOD_WILDCARD. Exiting.\n" );
      exit( -1 );
   }
   return;
}

