
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <time_ew.h>
#include <transport.h>
#include <earthworm.h>
#include <kom.h>
#include "dailyreport.h"

int  OpenDir( char gapdir[] );
int  GetFname( char fname[], int *isDir );
void CloseDir( void );

/* Function prototypes
   *******************/
void GetDrConfig( char *, PARM * );
void LogConfig( PARM * );
void GetStationList( char StationList[] );

/* Global variables
   ****************/
extern int  nSncl;           /* # of sncl's to monitor */
extern SNCL *sncl;           /* Array of sncl parameters */



int main( int argc, char *argv[] )
{
   int     i;
   PARM    parm;               /* Configuration file parameters */
   char    configname[FNLEN];
   int     epoch;
   char    tempname[FNLEN];
   char    fname[FNLEN];
   char    path[FNLEN];
   char    path1[FNLEN];
   char    path2[FNLEN];
   char    line[120];
   FILE    *fp;
   int     isDirectory;
   time_t  t;
   struct  tm date;
   GAPDATE yesterday;
   int     nchan = 0;          /* Number of channels to report */
   FILE    *fpctemp;           /* File pointer */

/* Check command line arguments
   ****************************/
   if ( argc == 1 )
      strcpy( configname, "dailyreport.d" );
   else if ( argc == 2 )
      strcpy( configname, argv[1] );
   else
   {
      printf( "Usage: dailyreport <configfile>\n" );
      return -1;
   }

/* Open log file
   *************/
   logit_init( configname, 0, 256, 1 );

/* Get parameters from the configuration files.
   GetDrConfig() exits on error.
   *******************************************/
   GetDrConfig( configname, &parm );

/* Read station list from file
   ***************************/
   GetStationList( parm.StationList );

/* Log the configuration parameters
   ********************************/
   LogConfig( &parm );

/* Get yesterday's year and day-of-year
   ************************************/
   time( &t );
   gmtime_ew( &t, &date );
   printf( "Current year: %4d     Day of year: %03d\n",
          date.tm_year+1900, date.tm_yday );
   yesterday.year = date.tm_year;
   yesterday.yday = date.tm_yday - 1;
   if ( yesterday.yday < 0 )
   {
      yesterday.year--;
      yesterday.yday = (yesterday.year % 4) ? 364 : 365;
   }

   printf( "yesterday: %4d %03d\n",
          yesterday.year+1900, yesterday.yday );

/* Open temporary file to contain 1-day completeness values
   ********************************************************/
   if ( chdir_ew( parm.TempDir ) == -1 )
   {
      logit( "e", "Error changing working directory to %s\n", parm.TempDir );
      logit( "e", "Exiting.\n" );
      return -1;
   }
   sprintf( tempname, "Completeness_%d_%d", yesterday.year+1900,
            yesterday.yday );
   fpctemp = fopen( tempname, "w" );
   if ( fpctemp == NULL )
   {
      logit( "e", "Can't open file %s\n", tempname );
      logit( "e", "Exiting.\n" );
      return -1;
   }

/* Get names of 10-minute completeness files in gap directory
   **********************************************************/
   if ( OpenDir( parm.GapDir ) == 1 )
   {
      logit( "et", "Error opening directory: %s\n", parm.GapDir );
      return 0;
   }

   while ( GetFname( fname, &isDirectory ) == 0 )
   {
      if ( isDirectory ||
           strncmp( fname, "Completeness_10min_", 19 ) )
        continue;

/* Open up a file with yesterday's date
   ************************************/
      if ( !sscanf( &fname[0]+19, "%d", &epoch ) ) continue;
      t = epoch * parm.EpochLen;
      gmtime_ew( &t, &date );
      if ( date.tm_year != yesterday.year ||
           date.tm_yday != yesterday.yday ) continue;
      strcpy( path, parm.GapDir );
      strcat( path, "/" );
      strcat( path, fname );
      fp = fopen( path, "r" );
      if ( fp == NULL )
      {
         logit( "et", "Can't open file %d\n", path );
         continue;
      }

/* Read one line from the file and decode it
   *****************************************/
      while ( fgets(line, 120, fp) != NULL )
      {
         int  j;
         char net[3];
         char sta[6];
         char *token;

         memcpy( net, line, 2 );
         net[2] = '\0';
         token = strtok( &line[3], ":" );
         strcpy( sta, token );

/* Look up this net/station in the station list
   ********************************************/
         for ( j = 0; j < nSncl; j++ )
         {
            if ( strcmp(sta, sncl[j].sta) == 0 &&
                 strcmp(net, sncl[j].net) == 0 )
            {
               double percent;
               token = strtok( NULL, "=" );
               token = strtok( NULL, "\n" );
               percent = atof( token );
               sncl[j].nEpoch++;
               sncl[j].dailyAve += percent;
//             printf( "%s %s %.2lf\n", net, sta, percent );
               break;
            }
         }
      }
      fclose( fp );
   }
   CloseDir();

/* Make sure some channels have enough epochs to report
   ****************************************************/
   for ( i = 0; i < nSncl; i++ )
   {
      if ( sncl[i].nEpoch >= parm.MinEpoch )
         nchan++;
   }
   if ( nchan == 0 )
   {
      logit( "et", "Can't report daily completeness for any channels. Exiting.\n" );
      return 0;
   }

/* Compute daily averages of percent completeness
   **********************************************/
   for ( i = 0; i < nSncl; i++ )
      sncl[i].dailyAve = (sncl[i].nEpoch == 0 ) ? 0.0 :
                          sncl[i].dailyAve / sncl[i].nEpoch;

/* Print daily averages of percent completeness
   ********************************************/
   for ( i = 0; i < nSncl; i++ )
   {
      if ( sncl[i].nEpoch >= parm.MinEpoch )
         fprintf( fpctemp, "%s-%s:1:Completeness_1day (percent)=%.2lf\n",
                  sncl[i].net, sncl[i].sta, sncl[i].dailyAve );
   }

/* Close the temporary file and move it
   to its final destination
   ************************************/
   fclose( fpctemp );
   strcpy( path1, parm.TempDir );
#ifdef _WINNT
   strcat( path1, "\\" );
#endif
#ifdef _SOLARIS
   strcat( path1, "/" );
#endif
   strcat( path1, tempname );

   strcpy( path2, parm.OutDir );

#ifdef _WINNT
   strcat( path2, "\\" );
#endif
#ifdef _SOLARIS
   strcat( path2, "/" );
#endif
   strcat( path2, tempname );

   logit( "e", "Renaming %s to %s\n", path1, path2 );
   if ( rename_ew( path1, path2 ) == -1 )
   {
      logit( "e", "Error moving temporary file. Exiting.\n" );
      return -1;
   }
   return 0;
}
