/*
   drconfig.c

   Functions for getting configuration parameters for the dailyreport program.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>
#include <trace_buf.h>
#include "gapreport.h"

#define ncommand 6      /* Number of commands in the config file */

int  nSncl = 0;         /* Number of sncl's to monitor */
SNCL *sncl = NULL;      /* Array of sncl structures */


   /*****************************************************************
    *                         GetDrConfig()                         *
    *         Processes command file using kom.c functions.         *
    *****************************************************************/

void GetDrConfig( char *config_file, PARM *parm )
{
   char init[ncommand];     /* Flags, one for each command */
   int  nmiss;              /* Number of commands that were missed */
   int  nfiles;
   int  i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ ) init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( config_file );
   if ( nfiles == 0 )
   {
      logit( "e", "gapreport: Error opening configuration file <%s> Exiting.\n",
              config_file );
      exit( -1 );
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          /* Get the first token from line */

         if ( !com ) continue;             /* Ignore blank lines */
         if ( com[0] == '#' ) continue;    /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               logit( "e", "gapreport: Error opening command file <%s>. Exiting.\n",
                       &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "StationList" ) )
         {
            parm->StationList[0] = '\0';
            if ( str=k_str() )
            {
               strncpy( parm->StationList, str, FNLEN );
               parm->StationList[FNLEN-1] = '\0';
            }
            init[0] = 1;
         }

         else if ( k_its( "EpochLen" ) )
         {
            parm->EpochLen = k_int();
            init[1] = 1;
         }

         else if ( k_its( "InDir" ) )
         {
            parm->InDir[0] = '\0';
            if ( str=k_str() )
            {
               strncpy( parm->InDir, str, FNLEN );
               parm->InDir[FNLEN-1] = '\0';
            }
            init[2] = 1;
         }

         else if ( k_its( "MinEpoch" ) )
         {
            parm->MinEpoch = k_int();
            init[3] = 1;
         }

         else if ( k_its( "TempDir" ) )
         {
            parm->TempDir[0] = '\0';
            if ( str=k_str() )
            {
               strncpy( parm->TempDir, str, FNLEN );
               parm->TempDir[FNLEN-1] = '\0';
            }
            init[4] = 1;
         }

         else if ( k_its( "OutDir" ) )
         {
            parm->OutDir[0] = '\0';
            if ( str=k_str() )
            {
               strncpy( parm->OutDir, str, FNLEN );
               parm->OutDir[FNLEN-1] = '\0';
            }
            init[5] = 1;
         }

/* An unknown parameter was encountered
   ************************************/
         else
         {
            logit( "e", "gapreport: <%s> unknown parameter in <%s>\n",
                    com, config_file );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            logit( "e", "gapreport: Bad <%s> command in <%s>. Exiting.\n", com,
                    config_file );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss > 0 )
   {
      logit( "e", "gapreport: ERROR, no " );
      if ( !init[0] ) logit( "e", "<StationList> " );
      if ( !init[1] ) logit( "e", "<EpochLen> " );
      if ( !init[2] ) logit( "e", "<InDir> " );
      if ( !init[3] ) logit( "e", "<MinEpoch> " );
      if ( !init[4] ) logit( "e", "<TempDir> " );
      if ( !init[5] ) logit( "e", "<OutDir> " );
      logit( "e", "command(s) in <%s>. Exiting.\n", config_file );
      exit( -1 );
   }
   return;
}


   /**************************************************************
    *                      GetStationList()                      *
    *                Read the station list file.                 *
    **************************************************************/

void GetStationList( char StationList[] )
{
   int  snclSize = 0;       /* Num bytes in sncl array */
   SNCL *snclPtr;
   FILE *fp;                /* File pointer */
   char line[120];
   int  i;

/* Open the station list file
   **************************/
   fp = fopen( StationList, "r" );
   if ( fp == NULL )
   {
      logit( "e", "Error opening station list file: %s. Exiting.\n",
             StationList );
      exit( -1 );
   }

/* Skip the first three lines in the file
   **************************************/
   for ( i = 0; i < 3; i++ )
   {
      if ( fgets( line, 120, fp ) == NULL )
      {
         logit( "e", "gapreport: Error skipping 3 lines in station list file. Exiting.\n" );
         exit( -1 );
      }
   }

/* Read lines until a blank line is encountered.
   Skip comment lines.
   ********************************************/
   while ( fgets( line, 120, fp ) != NULL )
   {
      char *token;
      char sta[TRACE2_STA_LEN];
      char chan[TRACE2_CHAN_LEN];
      char net[TRACE2_NET_LEN];
      char loc[TRACE2_LOC_LEN];
      char staname[50];
      int  seenIt = 0;

      if ( strlen( line ) < 2 )         /* Found a blank line */
         break;
      if ( line[0] == '#' )             /* Found a comment line */
         continue;

/* Decode the station name
   ***********************/
      token = strtok( line, " " );
      if ( token == NULL )
      {
         logit( "e", "gapreport: Error decoding station name. Exiting.\n" );
         exit( -1 );
      }
      strncpy( sta, token, TRACE2_STA_LEN );
      sta[TRACE2_STA_LEN-1] = '\0';

/* Decode the component code
   *************************/
      token = strtok( NULL, " " );
      if ( token == NULL )
      {
         logit( "e", "gapreport: Error decoding component code. Exiting.\n" );
         exit( -1 );
      }
      strncpy( chan, token, TRACE2_CHAN_LEN );
      chan[TRACE2_CHAN_LEN-1] = '\0';

/* Decode the network code
   ***********************/
      token = strtok( NULL, " " );
      if ( token == NULL )
      {
         logit( "e", "gapreport: Error decoding network code. Exiting.\n" );
         exit( -1 );
      }
      strncpy( net, token, TRACE2_NET_LEN );
      net[TRACE2_NET_LEN-1] = '\0';

/* Next field is either location code or latitude.
   Use location code if available.
   Otherwise set location code to --.
   Skip latitude.
   **********************************************/
      strcpy( loc, "--" );                     /* Set to default value */
      token = strtok( NULL, " " );
      if ( token == NULL )
      {
         logit( "e", "gapreport: Error decoding location code. Exiting.\n" );
         exit( -1 );
      }
      if ( strlen( token ) == 2 )              /* Got location code */
      {
         strcpy( loc, token );
         token = strtok( NULL, " " );          /* Skip latitude */
         if ( token == NULL )
         {
            logit( "e", "gapreport: Error decoding latitude. Exiting.\n" );
            exit( -1 );
         }
      }

/* Skip the longitude field
   ************************/
      token = strtok( NULL, " " );
      if ( token == NULL )
      {
         logit( "e", "gapreport: Error decoding longitude. Exiting.\n" );
         exit( -1 );
      }

/* Skip the elevation field
   ************************/
      token = strtok( NULL, " " );
      if ( token == NULL )
      {
         logit( "e", "gapreport: Error decoding elevation. Exiting.\n" );
         exit( -1 );
      }

/* Get the station name description
   ********************************/
      token = strtok( NULL, "\n" );
      if ( token == NULL )
      {
         logit( "e", "gapreport: Error decoding station name. Exiting.\n" );
         exit( -1 );
      }
      strncpy( staname, token, 50 );
      staname[49] = '\0';

/* Skip this station if we've seen it before
   *****************************************/
      for ( i = 0; i < nSncl; i++ )
      {
         if ( strcmp( sta, sncl[i].sta ) == 0 &&
              strcmp( net, sncl[i].net ) == 0 )
         {
            seenIt = 1;
            break;
         }
      }
      if ( seenIt ) continue;

/* Save this sncl in our list
   **************************/
      snclSize += sizeof( SNCL );
      snclPtr = (SNCL *)realloc( sncl, snclSize );
      if ( snclPtr ==  NULL )
      {
         logit( "e", "gapreport: realloc() error in GetStationList(). Exiting.\n" );
         exit( -1 );
      }
      sncl = snclPtr;
      strcpy( sncl[nSncl].sta,  sta );
      strcpy( sncl[nSncl].chan, chan );
      strcpy( sncl[nSncl].net,  net );
      strcpy( sncl[nSncl].loc,  loc );
      strcpy( sncl[nSncl].staname, staname );
      sncl[nSncl].nEpoch   = 0;
      sncl[nSncl].dailyAve = 0.0;
      nSncl++;
   }

   fclose( fp );
   return;
}


   /**************************************************************
    *                         LogConfig()                        *
    *              Log the configuration parameters              *
    **************************************************************/

void LogConfig( PARM *parm )
{
   int i;

   logit( "", "\n" );
   logit( "", "StationList:          %s\n", parm->StationList );
   logit( "", "EpochLen:             %d\n", parm->EpochLen );
   logit( "", "InDir:                %s\n", parm->InDir );
   logit( "", "TempDir:              %s\n", parm->TempDir );
   logit( "", "OutDir:               %s\n", parm->OutDir );
   logit( "", "MinEpoch:             %d\n", parm->MinEpoch );
   logit( "", "nSncl:                %d\n", nSncl );

   logit( "", "\nChannels monitored by gapreport:\n" );
   for ( i = 0; i < nSncl; i++ )
   {
      logit( "", "Sncl     %5s %3s %2s %2s  %s\n",
             sncl[i].sta,  sncl[i].net, sncl[i].chan, sncl[i].loc,
             sncl[i].staname );
   }
   logit( "", "\n" );
   return;
}

