CFLAGS = ${GLOBALFLAGS}

B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

all:
	make -f makefile.sol gapagent
	make -f makefile.sol dailyreport

GAOBJ = gapagent.o gaconfig.o $L/chron3.o $L/getutil.o \
        $L/kom.o $L/logit.o $L/time_ew.o $L/transport.o \
        $L/sleep_ew.o $L/swap.o $L/dirops_ew.o \
        $L/trheadconv.o

gapagent: $(GAOBJ)
	cc -o $B/gapagent $(GAOBJ) -lm -lposix4

DROBJ = dailyreport.o readdir_sol.o drconfig.o \
        $L/getutil.o $L/kom.o $L/logit.o $L/time_ew.o \
        $L/dirops_ew.o

dailyreport: $(DROBJ)
	cc -o $B/dailyreport $(DROBJ) -lm -lposix4

lint:
	lint gapagent.c gaconfig.c $(GLOBALFLAGS)

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/gapagent
