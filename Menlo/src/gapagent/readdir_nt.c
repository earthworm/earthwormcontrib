
#include <windows.h>
#include <sys/stat.h>
#include <errno.h>
#include <earthworm.h>
#include <stdio.h>
#include <direct.h>

#define FNLEN 80

static int dirOpen = 0;
static HANDLE fileHandle;
static WIN32_FIND_DATA findData;
static char fileName[FNLEN];
static int  isDir;


/***************************************************************
 *                           OpenDir                           *
 *                                                             *
 *  Returns 0 if all ok                                        *
 *          1 if an error was detected                         *
 ***************************************************************/

int OpenDir( char dirName[] )
{
   char fn[FNLEN];

   strcpy( fn, dirName );
   strcat( fn, "\\*" );
   fileHandle = FindFirstFile( fn, &findData );
   if ( fileHandle == INVALID_HANDLE_VALUE )
      return 1;

   strcpy( fileName, findData.cFileName );
   isDir = (findData.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY)
           ? 1 : 0;
   dirOpen = 1;
   return 0;
}


/***************************************************************
 *                           GetFname                          *
 *                                                             *
 *  Function to get the name of a file in directory dirName.   *
 *                                                             *
 *  Returns 0 if all ok                                        *
 *          1 if no files were found                           *
 ***************************************************************/

int GetFname( char fname[], int *isDirectory )
{
   if ( dirOpen == 0 )
      return 1;

/* We already got the first name when calling OpenDir
   **************************************************/
   if ( dirOpen == 1 )
   {
      strcpy( fname, fileName );
      *isDirectory = isDir;
      dirOpen = 2;
      return 0;
   }

/* Get another name
   ****************/
   if ( !FindNextFile( fileHandle, &findData ) )
      return 1;                                   /* No more files */

   isDir = (findData.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY)
           ? 1 : 0;
   strcpy( fname, findData.cFileName );
   *isDirectory = isDir;
   return 0;
}


void CloseDir( void )
{
   FindClose( fileHandle );
   dirOpen = 0;
   return;
}

