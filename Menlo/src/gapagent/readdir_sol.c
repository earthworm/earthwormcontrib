
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include <earthworm.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>

#define FNLEN 80

static DIR  *dp;
static int  dirOpen = 0;
static char dn[FNLEN];


/***************************************************************
 *                           OpenDir                           *
 *                                                             *
 *  Returns 0 if all ok                                        *
 *          1 if an error was detected                         *
 ***************************************************************/

int OpenDir( char dirName[] )
{
   dp = opendir( dirName );
   if ( dp == NULL ) return 1;
   strcpy( dn, dirName );
   dirOpen = 1;
   return 0;
}



/***************************************************************
 *                           GetFname                          *
 *                                                             *
 *  Function to get the name of a file in directory dirName.   *
 *                                                             *
 *  Returns 0 if all ok                                        *
 *          1 if no files were found                           *
 ***************************************************************/

int GetFname( char fname[], int *isDirectory )
{
   struct dirent *dentp;
   struct stat ss;
   char path[FNLEN];

   if ( dirOpen == 0 )
      return 1;

   dentp = readdir( dp );
   if ( dentp == NULL ) return 1;

   strcpy( path, dn );
   strcat( path, "/" );
   strcat( path, dentp->d_name );

   if ( stat( path, &ss ) == -1 )
   {
      printf( "path: %s\n", path );
      printf( "stat error in GetFname(). Exiting.\n" );
      exit( 0 );
   }

   strcpy( fname, dentp->d_name );
   *isDirectory = !S_ISREG(ss.st_mode);
   return 0;
}



void CloseDir( void )
{
   closedir( dp );
   dirOpen = 0;
   return;
}

