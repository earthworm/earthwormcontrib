/******************************************************************************/
/*                                                                            */
/* dupfile.c:                                                                 */
/*                                                                            */
/* Periodically check a specified directory for files.                        */
/* Reads the files, and copies to the appropriate directory(s).               */
/*                                                                            */
/* JHL July 2002                                                              */
/*                                                                            */
/******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <errno.h>

/* Define Array sizes
 ********************/
#define NAM_LEN	  100         /* length of full directory name          */
#define BUFLEN  65000         /* define maximum size for an event msg   */
#define MAX_DIR    20         /* maximum number of output directories   */
#define MAX_RJCT   10         /* max number of substrings to reject     */

/* Function prototypes
 *********************/
void config_me ( char * );
void ew_lookup ( void );
void ew_status ( unsigned char, short, char * );

void logit( char *, char *, ... );   /* logit.c      sys-independent  */

SHM_INFO  Region;                   /* shared memory region to use for i/o    */
pid_t     myPid;                    /* for restarts by startstop              */

static char MsgBuf[BUFLEN];         /* char string to hold output message     */

char      OutDir[MAX_DIR][NAM_LEN];
int       numInst;
char      Substr[MAX_RJCT][NAM_LEN];
int       numRjct;

static char *TempSubdir    = "temp";       /* subdir for temporary files      */

/* Things to read or derive from configuration file
 **************************************************/
static char     RingName[20];        /* name of transport ring for i/o        */
static char     MyModName[50];       /* speak as this module name/id          */
static int      LogSwitch;           /* 0 if no logfile should be written     */
static int      HeartBeatInterval;   /* seconds betweeen beats to statmgr     */
static char     GetFromDir[NAM_LEN]; /* directory to monitor for data         */
static unsigned CheckPeriod;         /* secs between looking for new files    */
static int      OpenTries;
static int      OpenWait;
       int      Debug;               /* non-zero -> debug logging             */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o          */
static unsigned char InstId;        /* local installation id                  */
static unsigned char MyModId;       /* Module Id for this program             */
static unsigned char TypeHeartBeat; 
static unsigned char TypeError;

/* Error messages used by sm_file2ew 
 ***********************************/
#define ERR_CONVERT       0     /* trouble converting a file                  */

#define TEXT_LEN NAM_LEN*3
static char Text[TEXT_LEN];     /* string for log/error messages              */
static char ProgName[NAM_LEN];  /* program name for logging purposes          */

/* File handling stuff
**********************/
int main( int argc, char **argv )
{
	int	      i, ierr, jerr, rejected, ret, read_error = 0;
	char      fname[100], ftemp[155], fnew[155], buf[BUFLEN];
	char     *c;
	FILE     *fp, *fout;
	time_t    tnextbeat;    /* next time for local heartbeat  */
	time_t    tnow;         /* current time */

/* Check command line arguments 
 ******************************/
	if ( argc != 2 ) {
		fprintf( stderr, "Usage: %s <configfile>\n", argv[0] );
		exit( 0 );
	}
	strcpy( ProgName, argv[1] );
	c = strchr( ProgName, '.' );
	if( c ) *c = '\0';
           
/* Read the configuration file(s)
 ********************************/
	config_me( argv[1] );
   
/* Look up important info from earthworm.h tables
 ************************************************/
	ew_lookup();
   
/* Initialize name of log-file & open it 
 ***************************************/
	logit_init( argv[1], (short) MyModId, TEXT_LEN*2, LogSwitch );
	logit( "" , "%s: Read command file <%s>\n", argv[0], argv[1] );

/* Get process ID for heartbeat messages 
 ***************************************/
	myPid = getpid();
	if( myPid == -1 ) {
		logit("e","%s: Cannot get pid; exiting!\n", ProgName);
		exit (-1);
	}

/* Change to the directory with the input files
 ***********************************************/
	if( chdir_ew( GetFromDir ) == -1 ) {
		logit( "e", "%s: GetFromDir directory <%s> not found; "
		         "exiting!\n", ProgName, GetFromDir );
		exit(-1);
	}
	if(Debug)logit("et","%s: changed to directory <%s>\n", ProgName,GetFromDir);

/* Make sure temp subdirectory exists
 ***************************************/
   if( CreateDir( TempSubdir ) != EW_SUCCESS ) {
      logit( "e", "%s: trouble creating temp directory: %s/%s\n",
              ProgName, GetFromDir, TempSubdir ); 
      return( -1 );
   }

/* Attach to Output shared memory ring 
 *************************************/
   tport_attach( &Region, RingKey );
   logit( "", "%s: Attached to public memory region %s: %d\n", 
          ProgName, RingName, RingKey );

/* Force local heartbeat first time thru main loop
 **********************************************************/
   tnextbeat  = time(NULL) - 1;


/****************  top of working loop ********************************/
	while(1)  {
     /* Check on heartbeats
      *********************/
        tnow = time(NULL);
        if( tnow >= tnextbeat ) {  /* time to beat local heart */
           ew_status( TypeHeartBeat, 0, "" );
           tnextbeat = tnow + (time_t)HeartBeatInterval;
        }

     /* See if termination has been requested 
      ****************************************/
		if( tport_getflag( &Region ) == TERMINATE ) {
		   logit( "t", "%s: Termination requested; exiting!\n", ProgName );
		   break;
		}

     /* Get a file name
      ******************/    
		ret = GetFileName( fname );
    
		sleep_ew( 1*1000 );
    
		if( ret == 1 ) {  /* No files found; wait for one to appear */
			sleep_ew( CheckPeriod*1000 ); 
			continue;
		}
   
        if(Debug)logit("et","%s: got file name <%s>\n",ProgName,fname);

     /* Open the file.
      ******************/
     /* We open for write, as that will hopefully get us an exclusive open. 
      * We don't ever want to look at a file that's being written to. 
      */
		for(i=0;i<OpenTries;i++) {
			fp = fopen( fname, "rb" );
			if ( fp != NULL ) goto itopend;
			sleep_ew(OpenWait);
		}
		logit("et","%s: Error: Could not open %s after %d*%d ms\n",
		           ProgName, OpenTries,OpenWait);
		itopend:    
		if(i>0) logit("t","Warning: %d attempts required to open file %s\n", i,fname);

     /* If it's a core file, delete the file
      **************************************/
		if ( strcmp(fname,"core")==0 ) {
            fclose( fp );
            if( remove( fname ) != 0) {
                logit("et", "%s: Cannot delete core file <%s>\n!", 
                    ProgName, fname );
                break;
            }
            continue;
		}

     /* If it's an unneeded file (e.g. heartbeat file), delete the file
      **************************************/
		rejected = 0;
		for(i=0;i<numRjct;i++) {
			if ( strstr(fname, Substr[i]) != NULL ) {
	            fclose( fp );
	            if( remove( fname ) != 0) {
	                logit("et", "%s: Cannot delete file <%s>!\n", ProgName, fname );
	                break;
	            } else if(Debug) logit("et", "%s: Delete file <%s>\n", ProgName, fname );
	            rejected = 1;
	            break;
			}
		}
		if(rejected) continue;

        fclose( fp );

     /* Everything went fine...
      *************************/
        /* Copy file to distribution directories */
        for(i=0;i<numInst;i++) {
            sprintf(ftemp,"%s/%s",TempSubdir,fname );
			fp = fopen( fname, "rb" );
			if ( fp == NULL ) {
				logit( "et", "Error. Can't open file %s\n", fname );
				continue;
			}
			fout = fopen( ftemp, "wb" );
			if ( fout == NULL ) {
				logit( "et", "Error. Can't open new file %s\n", ftemp );
				fclose( fp );
				continue;
			}

		/* Read BUFLEN bytes at a time and write them to fout
		   ****************************************************/
			while ( 1 ) {
				int nbytes = fread( buf, sizeof(char), BUFLEN, fp );
				if ( nbytes > 0 ) {
					if ( fwrite( buf, sizeof(char), nbytes, fout ) == 0 ) {
						logit( "et", "Error writing new file.\n" );
						sprintf(Text, "Error writing new file: %s ", ftemp );
						ew_status( TypeError, ERR_CONVERT, Text );
						break;
					}
				}
				if ( feof( fp ) ) break;

		/* If an fread error occurs, exit loop.  
		   ************************************/
				if ( (nbytes == 0) && ferror( fp ) ) {
					logit( "et", "fread() error on %s File partially copied. \n", fname );
					break;
				}
			}

		/* Finish up with this file
		   ************************/
			fclose( fp );
			fclose( fout );

            sprintf(fnew,"%s%s", OutDir[i], fname );
            ierr = rename( ftemp, fnew );
            /* The following silliness is necessary to be Windows compatible */ 
            if( ierr != 0 ) {
                if(Debug) logit( "e", "Error moving file %s to %s; ierr = %d\n", ftemp, fnew, ierr );
                if( remove( fnew ) != 0 ) {
                    logit("e","error deleting file %s\n", fnew);
                } else  {
                    if(Debug) logit("e","deleted file %s.\n", fnew);
                    jerr = rename( ftemp, fnew );
                    if( jerr != 0 ) {
                        logit( "e", "error moving file %s to %s; ierr = %d\n", ftemp, fnew, ierr );
                    } else {
                        if(Debug) logit("e","%s moved to %s\n", ftemp, fnew );
                    }
                }
            } else {
                if(Debug) logit("e","%s moved to %s\n", fname, OutDir[i] );
            }
        }

        /* Delete the file */
        if( remove( fname ) != 0 ) {
            logit("et","%s: error deleting file: %s\n", ProgName, fname);
        } else  {
            if(Debug)logit("e","%s: Removed %s \n \n", ProgName, fname );
        }
	}
    
/* detach from shared memory */
   tport_detach( &Region ); 

/* write a termination msg to log file */
   fflush( stdout );
   return( 0 );

}  


/******************************************************************************
 *  config_me() processes command file(s) using kom.c functions;              *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
#define ncommand 9
void config_me( char *configfile )
{
	char     init[ncommand]; /* init flags, one byte for each required command */
	char    *com, *str;
	int      i, n, nfiles, nmiss, success;
   
/* Set to zero one init flag for each required command 
 *****************************************************/   
	for( i=0; i<ncommand; i++ )  init[i] = 0;

/* Open the main configuration file 
 **********************************/
	nfiles = k_open( configfile ); 
	if ( nfiles == 0 ) {
		fprintf( stderr, "%s: Error opening command file <%s>; exiting!\n", 
		         ProgName, configfile );
		exit( -1 );
	}

/* Process all command files
 ***************************/
   while(nfiles > 0) {  /* While there are command files open */
        while(k_rd()) {       /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr, 
                          "%s: Error opening command file <%s>; exiting!\n",
                           ProgName, &com[1] );
                  exit( -1 );
               }
               continue;
			}

        /* Process anything else as a command 
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("InDir") ) {
                str = k_str();
                n = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[n-1] != '/' ) strcat(str, "/");
                if(str) strncpy( GetFromDir, str, NAM_LEN );
                init[3] = 1;
            }
  /*4*/     else if( k_its("CheckPeriod") ) {
                CheckPeriod = k_int();
                init[4] = 1;
            }
  /*5*/     else if( k_its("Debug") ) {
                Debug = k_int();
                init[5] = 1;
            }
  /*6*/     else if( k_its("OpenTries") ) {
                OpenTries = k_int();
                init[6] = 1;
            }
  /*7*/     else if( k_its("OpenWait") ) {
                OpenWait = k_int();
                init[7] = 1;
            }

  /*10*/    else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_int();
                init[8] = 1;
			}

               /* optional commands */

/*NR*/
			else if (k_its ("OutDir")) {
				i = numInst++;
				if(numInst>MAX_DIR) {
					fprintf( stderr, "%s: Attempting to define too many (%d) output directories.\n", ProgName, numInst);
					exit( -1 );
				}
                
				if ((str = k_str())) {
	                n = strlen(str);   /* Make sure directory name has proper ending! */
	                if( str[n-1] != '/' ) strcat(str, "/");
	                if(str) strncpy( OutDir[i], str, NAM_LEN );

				} else {
					fprintf( stderr, "%s: Syntax error for OutDir.\n", ProgName);
					exit( -1 );
				}
				fprintf( stderr, "%s: SaveType: OutDir: %s.\n", ProgName, OutDir[i]);
			}

/*NR*/
			else if (k_its ("Reject")) {
				i = numRjct++;
				if(numInst>MAX_RJCT) {
					fprintf( stderr, "%s: Attempting to define too many (%d) reject substrings.\n", ProgName, numInst);
					exit( -1 );
				}
                
				if ((str = k_str())) {
	                if(str) strncpy( Substr[i], str, NAM_LEN );

				} else {
					fprintf( stderr, "%s: Syntax error for Reject.\n", ProgName);
					exit( -1 );
				}
				fprintf( stderr, "%s: Reject: %s.\n", ProgName, Substr[i]);
			}


         /* Unknown command
          *****************/ 
            else {
                fprintf( stderr, "%s: <%s> Unknown command in <%s>.\n", 
                         ProgName, com, configfile );
                continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr, 
                       "%s: Bad <%s> command in <%s>; exiting!\n",
                        ProgName, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }
   
/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "%s: ERROR, no ", ProgName );
       if ( !init[0] )  fprintf( stderr, "<LogFile> "       );
       if ( !init[1] )  fprintf( stderr, "<MyModuleId> "    );
       if ( !init[2] )  fprintf( stderr, "<RingName> "      );
       if ( !init[3] )  fprintf( stderr, "<InDir> "         );
       if ( !init[4] )  fprintf( stderr, "<CheckPeriod> "   );
       if ( !init[5] )  fprintf( stderr, "<Debug> "         );
       if ( !init[6] )  fprintf( stderr, "<OpenTries> "     );
       if ( !init[7] )  fprintf( stderr, "<OpenWait> "      );
       if ( !init[8] )  fprintf( stderr, "<HeartBeatInterval> "     );
       fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }
   
   return;
}


/******************************************************************************/
/*                                                                            */
/*  ew_lookup( )   Look up important info from earthworm.h tables             */
/*                                                                            */
/******************************************************************************/
void ew_lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 ) {
        fprintf( stderr,
                "%s:  Invalid ring name <%s>; exiting!\n",
                 ProgName, RingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr, 
              "%s: error getting local installation id; exiting!\n",ProgName );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid module name <%s>; exiting!\n", ProgName, MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n",ProgName );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_ERROR>; exiting!\n",ProgName );
      exit( -1 );
   }
   return;
} 

/******************************************************************************/
/*                                                                            */
/* ew_status() builds a heartbeat or error message & puts it into             */
/*                   shared memory.  Writes errors to log file & screen.      */
/*                                                                            */
/******************************************************************************/
void ew_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size;
   time_t      t;
 
/* Build the message
 *******************/ 
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat ) {
        sprintf( msg, "%ld %ld\n\0", (long)t, myPid);
   }
   else if( type == TypeError ) {
        sprintf( msg, "%ld %hd %s\n\0", (long)t, ierr, note);
        logit( "et", "%s: %s\n", ProgName, note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */     

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK ) {
        if( type == TypeHeartBeat ) {
           logit("et","%s:  Error sending heartbeat.\n", ProgName );
        }
        else if( type == TypeError ) {
           logit("et","%s:  Error sending error:%d.\n", ProgName, ierr );
        }
   }
   return;
}



