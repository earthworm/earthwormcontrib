# dupfile.d
#
# Picks up files from a specified directory. 
# Copy the file to other directory(s).
#

# Basic Module information
#-------------------------
MyModuleId        MOD_DUPFILE      # module id 
RingName          HYPO_RING	       # shared memory ring for output
HeartBeatInterval 30               # seconds between heartbeats to statmgr

LogFile           1                # 0 log to stderr/stdout only; 
                                   # 1 log to stderr/stdout and disk;
                                   # 2 log to disk module log only.

Debug             1                # 1=> debug output. 0=> no debug output

# Data file manipulation
#-----------------------
InDir           /picker/gifs/temp  # look for files in this directory
CheckPeriod     10                 # sleep this many seconds between looks
OpenTries       5                  # How many times we'll try to open a file 
OpenWait        200                # Milliseconds to wait between open tries

#
#Keyword     Destination directory
#
OutDir       /picker/gifs/tempa     
OutDir       /picker/gifs/tempb     

#
# Optionally reject files containing the following substrings.
#
Reject       heartbeat             # Reject files containing "heartbeat"
Reject       txt                   # Reject files containing "txt"
