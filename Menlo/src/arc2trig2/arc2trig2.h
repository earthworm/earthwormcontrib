/*
 * arc2trig2.h : Include file for arc2trig2.c;
 */

#define MAX_STR		     255
#define MAX_STADBS        20  /* Maximum number of Station database files    */

#define MAXCHANNELS     1600  /* Maximum number of channels                  */
#define MAX_STATIONS    1600  /* Maximum number of channels                  */
#define SHORT_STR             64
#define MEDIUM_STR  (SHORT_STR*2)

/**************************************************************************
    Define the structure for Channel information.
    This is an abbreviated structure.
 *****************************************************************************/

typedef struct ChanInfo {  /* A channel information structure                */
    char    Site[6];       /* Site                                           */
    char    Comp[5];       /* Component                                      */
    char    Net[5];        /* Net                                            */
    char    Loc[5];        /* Loc                                            */
    char    SCN[18];       /* SCNL                                           */
    char    SCNtxt[20];    /* S C N L                                        */
    char    SCNnam[20];    /* S_C_N_L                                        */
    char    SiteName[50];  /* Common Name of Site                            */
    char    Descript[200]; /* Common Name of Site                            */
    double  Lat;           /* Latitude                                       */
    double  Lon;           /* Longitude                                      */
    double  Elev;          /* Elevation                                      */
    
    int     Inst_type;     /* Type of instrument                             */
    double  Inst_gain;     /* Gain of instrument (microv/count)              */
    int     Sens_type;     /* Type of sensor                                 */
    double  Sens_gain;     /* Gain of sensor (volts/unit)                    */
    int     Sens_unit;     /* Sensor units d=1; v=2; a=3                     */
    double  GainFudge;     /* Additional gain factor.                        */
    double  SiteCorr;      /* Site correction factor.                        */
    double  sensitivity;   /* Channel sensitivity  counts/units              */
    int		ShkQual;       /* Station (Chan) type                            */
} ChanInfo;

/* Things read from config file
 ******************************/

int     nStaDB;              /* number of station databases we know about          */
char    stationList[MAX_STADBS][SHORT_STR];
int     stationListType[MAX_STADBS];

int     arcfileflg;                /* Flag set if we are accepting arc files */
char    ArcInputDir[SHORT_STR];    /* Directory for manual-input arc files   */ 
char    OutputDir[SHORT_STR];      /* Directory to write "triggers" to  */ 
char    TrigFileBase[SHORT_STR/2]; /* Prefix of trigger file name  */

/* Other globals
 ***************/
char    progname[SHORT_STR];
int     Debug;

int     NSCN;              /* Number of SCNs we know about               */
ChanInfo    Chan[MAXCHANNELS];

/* Function prototypes
 *********************/
int writetrig_init( void );   /*writetrig.c*/
int writetrig( char * );      /*writetrig.c*/ 
void writetrig_close( );      /*writetrig.c*/
