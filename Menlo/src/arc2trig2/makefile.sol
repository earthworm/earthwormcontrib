
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
SRCS = arc2trig2.c writetrig.c

B = $(EW_HOME)/$(EW_VERSION)/bin 
L = $(EW_HOME)/$(EW_VERSION)/lib

ARC = arc2trig2.o writetrig.o \
     $L/logit.o $L/getutil.o $L/transport.o $L/kom.o  \
     $L/sleep_ew.o $L/time_ew.o \
     $L/read_arc.o $L/chron3.o $L/dirops_ew.o

arc2trig2: $(ARC)
	cc -o $(B)/arc2trig2 $(ARC) -lm -lposix4

.c.o:
	cc -c ${CFLAGS} $<

lint:
	lint arc2trig2.c writetrig.c $(GLOBALFLAGS)

depend:
	makedepend -fmakefile.sol -- $(CFLAGS) -- $(SRCS)

# DO NOT DELETE THIS LINE -- make depend depends on it.


# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/arc2trig2*
