/*
 * arc2trig2.c:
 * 
 * Modified for location codes. Jim 01/20/05
 * This is a perversion of arc2trig specifically written to handle packaging
 * of trace snippets for NSMP.
 * When an arc message appears on the ring or an arc file is dropped into 
 * the arc_indir, trigger file messages are created for all requested channels. 
 *
Takes a hypo arc message as input and produces a .trg file
alex 8/5/97

So it says, but I think Lynn wrote this module. I (Alex) am modifying it to do
the author field, 6/16/98:

Story: The grand idea is that messages which relate to an event shall include
an author field. In particular, the trigger message shall have include, after
the "EVENT ID:", an "AUTHOR:", which is followed by the author id. The author
id is to be a series of logos, separated by :'s. Each logo is represented as
three sets of three ascii digits. The first logo is the logo of the module
which originated this event, followed by the logos of the modules which
processed it.

So hypoinverse (eqproc) created the event by emitting an arc message. It
should have stuck its logo in the author field. It doesn't, because we don't
have the stomach to change eqproc. So we make up for it here: we know the logo
of the message as it came to us, and we know our own module id, so we write
eqproc's logo and our own into the trigger message which we produce.

Mon Nov  9 13:21:16 MST 1998 lucky:

      Log file name will be built from the name of the configuration
      file -- argv[1] passed to logit_init().

      Incoming transport ring is flushed at start time to prevent 
      confusion if the module is restarted.

      Process id is sent with the heartbeat for restart purposes.

      ===================== Y2K compliance =====================
      Formats in read_hyp() and read_phs() changed (among other
      things)to include date in the form YYYYMMDD. 

      make_datestr() changed to create date string YYYYMMDD...
      
      calls to julsec15() and date15() replaced by julsec17() and
      date17() respectively (as the Y2K equivalents of old calls)

      Message type names changed to their Y2K equivalents.  */
/* More changes: 12/9/1998, Pete Lombard
   Yanked read_hyp() and read_ph() out to use new versions in 
   libsrc/util/read_arc.c

   Alex Nov 23 99:
    The amount of pre-pick data to be saved is now an optional
       configuration file parameter. Also, default time changed from 
    5 seconds to 15 seconds.
   */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <chron3.h>
#include <earthworm.h>
#include <read_arc.h>
#include <kom.h>
#include <transport.h>
#include "arc2trig2.h"

/* Function prototypes */
int read_hyp (char *, char *, struct Hsum *);
int read_phs (char *, char *, struct Hpck *);

/* Time constants
*****************/
/* These variables are set here, but can be changed via
   optional coniguration file commands */
static double PrePickTime = 15; /* seconds to save before pick time */
static double PostCodaTime = 10; /* seconds past coda or tau time */


/* Functions in this source file
 *******************************/
void   config_me  ( char * );
void   arc2trig_lookup  ( void );
void   arc2trig_status  ( unsigned char, short, char * );
int    arc2trig_hinvarc ( char*, char*, MSG_LOGO );
char  *make_datestr( double, char * );
double tau( float );
void   bldtrig_hyp( char *, MSG_LOGO, MSG_LOGO );
void   bldtrig_phs( char * );
int  distaz (double lat1, double lon1, double lat2, double lon2, 
                double *rngkm, double *faz, double *baz);
void Get_Sta_Info(void);

static  SHM_INFO  InRegion;      /* shared memory region to use for input  */
static  SHM_INFO  OutRegion;     /* shared memory region to use for output */

#define   MAXLOGO   2
MSG_LOGO  GetLogo[MAXLOGO];     /* array for requesting module,type,instid */
short     nLogo;

static char ArcMsgBuf[MAX_BYTES_PER_EQ]; /* character string to hold event message
                                            MAX_BYTES_PER_EQ is from earthworm.h */

static char TrigMsgBuf[MAX_TRIG_BYTES]; /* to hold the trigger message / file */

/* Things to read or derive from configuration file
 **************************************************/
static char    InRingName[MAX_RING_STR];       /* name of transport ring for input  */
static char    OutRingName[MAX_RING_STR];      /* name of transport ring for output */
static char    MyModName[MAX_MOD_STR];        /* speak as this module name/id      */
static int     LogSwitch;            /* 0 if no logfile should be written */
static long    HeartBeatInterval;    /* seconds between heartbeats        */
static int     LocCode;              /* 0 if no no location codes are used */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          InRingKey;      /* key of transport ring for input   */
static long          OutRingKey;     /* key of transport ring for output  */
static unsigned char InstId;         /* local installation id             */
static unsigned char MyModId;        /* Module Id for this program        */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeHyp2000Arc;
static unsigned char TypeH71Sum2k;
static unsigned char TypeTrigList;
static unsigned char TypeTrigList2;

/* Error messages used by arc2trig2
 *********************************/
#define  ERR_MISSMSG       0   /* message missed in transport ring       */
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer     */
#define  ERR_NOTRACK       2   /* msg retreived; tracking limit exceeded */
#define  ERR_FILEIO        3   /* error opening trigger file             */
#define  ERR_DECODEARC     4   /* error reading input archive message    */
static char  Text[150];        /* string for log/error messages          */

/* Structures for hyp2000 data */
struct Hsum Sum;               /* Hyp2000 summary data                   */
struct Hpck Pick;              /* Hyp2000 pick structure                 */

pid_t    MyPid;     /** Out process id is sent with heartbeat **/

/** length of string required by make_datestr  **/
#define    DATESTR_LEN        22    

int main( int argc, char **argv )
{
    char      whoami[50], fname[100], string[500], subname[] = "Main";
    time_t    timeNow;          /* current time                  */
    time_t    timeLastBeat;     /* time last heartbeat was sent  */
    long      recsize;          /* size of retrieved message     */
    MSG_LOGO  reclogo;          /* logo of retrieved message     */
    int       res, ret;
    FILE     *fp;


/* Check command line arguments
 ******************************/
    if ( argc != 2 ) {
        fprintf( stderr, "Usage: arc2trig2 <configfile>\n" );
        exit( 0 );
    }
    strcpy(progname, "arc2trig2");
    sprintf(whoami, "%s: %s: ", progname, "Main");

/* Read the configuration file(s)
 ********************************/
	config_me( argv[1] );

/* Look up important info from earthworm.h tables
 ************************************************/
	arc2trig_lookup();

/* Initialize name of log-file & open it
 ***************************************/
	logit_init( argv[1], (short) MyModId, 256, LogSwitch );
	logit( "" , "%s: Read command file <%s>\n", argv[0], argv[1] );

/* Get our own process ID for restart purposes
 **********************************************/

	if ((MyPid = getpid ()) == -1) {
		logit ("e", "%s Call to getpid failed. Exiting.\n", whoami);
		exit (-1);
	}

/* Initialize the trigger file
 *****************************/
	if( writetrig_init() != 0 ) {
		logit("", "%s error opening file in OutputDir <%s>", whoami,
		    OutputDir);
		logit("", "                          with BaseName  <%s>\n",
		    TrigFileBase );
		exit( -1 );
	}
	logit( "", "%s Writing trigger files in %s\n", whoami, OutputDir );

/* Attach to Input shared memory ring
 ************************************/
	tport_attach( &InRegion, InRingKey );
	logit( "", "%s Attached to public memory region %s: %d\n",
	      whoami, InRingName, InRingKey );

/* Attach to Output shared memory ring
 *************************************/
	tport_attach( &OutRegion, OutRingKey );
	logit( "", "%s Attached to public memory region %s: %d\n",
	      whoami, OutRingName, OutRingKey );

/* Find out about all the channels
 *************************************/
	Get_Sta_Info();
	
	if  ( arcfileflg ) {
    /* Change to the directory with the input files
     ***********************************************/
		if( chdir_ew( ArcInputDir ) == -1 ) {
		    logit( "e", "%s GetFromDir directory <%s> not found; "
		             "exiting!\n", whoami, ArcInputDir );
		    exit(-1);
		}
		if(Debug)logit("et","%s changed to directory <%s>\n", whoami, ArcInputDir);
	}

/* Force a heartbeat to be issued in first pass thru main loop
 *************************************************************/
	timeLastBeat = time(&timeNow) - (time_t)HeartBeatInterval - 1;

/* Flush the incoming transport ring on startup
 **********************************************/

	while (tport_getmsg (&InRegion, GetLogo, nLogo,  &reclogo,
	        &recsize, ArcMsgBuf, sizeof(ArcMsgBuf)- 1) != GET_NONE)

	;

/*----------------------- setup done; start main loop -------------------------*/

	while(1) {
     /* send arc2trig2's heartbeat
      ***************************/
		if  ( time(&timeNow) - timeLastBeat  >=  (time_t)HeartBeatInterval ) {
			timeLastBeat = timeNow;
			arc2trig_status( TypeHeartBeat, 0, "" );
		}

     /* Process all new messages
      **************************/
		do {
        /* see if a termination has been requested
         *****************************************/
			if ( tport_getflag( &InRegion ) == TERMINATE ||
			     tport_getflag( &InRegion ) == MyPid ) {
				writetrig_close();
           /* detach from shared memory */
                tport_detach( &InRegion );
           /* write a termination msg to log file */
                logit( "t", "%s Termination requested; exiting!\n", whoami );
                fflush( stdout );
                exit( 0 );
			}

        /* Get msg & check the return code from transport
         ************************************************/
			res = tport_getmsg( &InRegion, GetLogo, nLogo, &reclogo, 
			                    &recsize, ArcMsgBuf, sizeof(ArcMsgBuf)-1 );

           if( res == GET_NONE ) {         /* no more new messages     */
                break;
           }
           else if( res == GET_TOOBIG ) {  /* next message was too big */
                                           /* complain and try again   */
                sprintf(Text,
                        "Retrieved msg[%ld] (i%u m%u t%u) too big for ArcMsgBuf[%d]",
                        recsize, reclogo.instid, reclogo.mod, reclogo.type,
                        sizeof(ArcMsgBuf)-1 );
                arc2trig_status( TypeError, ERR_TOOBIG, Text );
                continue;
           }
           else if( res == GET_MISS )     /* got a msg, but missed some */
           {
                sprintf( Text,
                        "Missed msg(s)  i%u m%u t%u  %s.",
                         reclogo.instid, reclogo.mod, reclogo.type, InRingName );
                arc2trig_status( TypeError, ERR_MISSMSG, Text );
           }
           else if( res == GET_NOTRACK ) /* got a msg, but can't tell */
           {                             /* if any were missed        */
                sprintf( Text,
                         "Msg received (i%u m%u t%u); transport.h NTRACK_GET exceeded",
                          reclogo.instid, reclogo.mod, reclogo.type );
                arc2trig_status( TypeError, ERR_NOTRACK, Text );
           }

        /* Process the message
         *********************/
			ArcMsgBuf[recsize] = '\0';      /*null terminate the message*/

			if( reclogo.type == TypeHyp2000Arc ) {
				int ret;
				ret   = arc2trig_hinvarc( ArcMsgBuf, TrigMsgBuf, reclogo );
				if(ret) arc2trig_status( TypeError, ERR_DECODEARC, "" );
			}
			else if( reclogo.type == TypeH71Sum2k ) {
				/* not doing anything with these message yet LDD:970814 */
				/* arc2trig_h71sum( ); */
			}

		} while( res != GET_NONE );  /*end of message-processing-loop */
        
        /* ------------- See if there is an arc file to process --------------*/
        if  ( arcfileflg ) {

         /* Get a file name
          ******************/    
            ret = GetFileName( fname );
        
            if( ret != 1 ) {  /* Files found; Process */
                if(Debug)logit("et","%s: %s: got file name <%s>\n",progname, subname,fname);
                sleep_ew( 1*1000 );
                fp = fopen( fname, "rb" );
                if ( fp != NULL ) {
                    if(strstr(fname, ".arc") != 0) {
                        ArcMsgBuf[0] = 0L;
                        while(fgets(string, 480, fp)!=0L){
                            strcat(ArcMsgBuf, string);
                        }
                        recsize = strlen(ArcMsgBuf);
                        if(Debug) {
                             printf("%s Retrieved msg[%ld] (i%u m%u t%u)\n      ArcMsgBuf: %.200s\n",
                                whoami, recsize, reclogo.instid, reclogo.mod, reclogo.type, ArcMsgBuf );
                        }
                        reclogo.instid = InstId;
                        reclogo.mod    = MyModId;
                        reclogo.type   = TypeHyp2000Arc;
                        ret   = arc2trig_hinvarc( ArcMsgBuf, TrigMsgBuf, reclogo );
                        if(ret) arc2trig_status( TypeError, ERR_DECODEARC, "" );
                        
                    }
                    fclose( fp );
                }
                if( remove( fname ) != 0 ) {
                    logit("et","%s error deleting file: %s\n", whoami, fname);
                } else  {
                    if(Debug)logit("e","%s Removed %s \n \n", whoami, fname );
                }
            }
        }

		sleep_ew( 1000 );  /* no more messages; wait for new ones to arrive */

   }
/*-----------------------------end of main loop-------------------------------*/
}

/******************************************************************************
 *  config_me() processes command file(s) using kom.c functions;              *
 *                    exits if any errors are encountered.                    *
 ******************************************************************************/
void config_me( char *configfile )
{
    char    whoami[50], *com, *str;
	int     ncommand;     /* # of required commands you expect to process   */
	char    init[15];     /* init flags, one byte for each required command */
	int     nmiss;        /* number of required commands that were missed   */
	int     i, j, nfiles, success;

    sprintf(whoami, "%s: %s: ", progname, "config_me");
/* Set to zero one init flag for each required command
 *****************************************************/
	ncommand = 9;
	for( i=0; i<ncommand; i++ )  init[i] = 0;
	nLogo = 0;
	Debug = 0;
	nStaDB = 0;
	LocCode = 1;

/* Open the main configuration file
 **********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 ) {
        fprintf( stderr,
                "%s Error opening command file <%s>; exiting!\n",
                 whoami, configfile );
        exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {
            com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr,
                          "%s Error opening command file <%s>; exiting!\n",
                           whoami, &com[1] );
                  exit( -1 );
               }
               continue;
            }

        /* Process anything else as a command
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("InRingName") ) {
                str = k_str();
                if(str) strcpy( InRingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("OutRingName") ) {
                str = k_str();
                if(str) strcpy( OutRingName, str );
                init[3] = 1;
            }
  /*4*/     else if( k_its("HeartBeatInterval") ) {
                HeartBeatInterval = k_long();
                init[4] = 1;
            }


         /* Enter installation & module to get event messages from
          ********************************************************/
  /*5*/     else if( k_its("GetEventsFrom") ) {
                if ( nLogo+1 >= MAXLOGO ) {
                    fprintf( stderr,
                            "%s Too many <GetEventsFrom> commands in <%s>",
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAXLOGO/2 );
                    exit( -1 );
                }
                if( ( str=k_str() ) ) {
                   if( GetInst( str, &GetLogo[nLogo].instid ) != 0 ) {
                       fprintf( stderr,
                               "%s Invalid installation name <%s>", whoami, str );
                       fprintf( stderr, " in <GetEventsFrom> cmd; exiting!\n" );
                       exit( -1 );
                   }
                   GetLogo[nLogo+1].instid = GetLogo[nLogo].instid;
                }
                if( ( str=k_str() ) ) {
                   if( GetModId( str, &GetLogo[nLogo].mod ) != 0 ) {
                       fprintf( stderr,
                               "%s Invalid module name <%s>", whoami, str );
                       fprintf( stderr, " in <GetEventsFrom> cmd; exiting!\n" );
                       exit( -1 );
                   }
                   GetLogo[nLogo+1].mod = GetLogo[nLogo].mod;
                }
                if( GetType( "TYPE_HYP2000ARC", &GetLogo[nLogo].type ) != 0 ) {
                    fprintf( stderr,
                               "%s Invalid message type <TYPE_HYP2000ARC>", whoami );
                    fprintf( stderr, "; exiting!\n" );
                    exit( -1 );
                }
                if( GetType( "TYPE_H71SUM2K", &GetLogo[nLogo+1].type ) != 0 ) {
                    fprintf( stderr,
                               "%s Invalid message type <TYPE_H71SUM2K>", whoami );
                    fprintf( stderr, "; exiting!\n" );
                    exit( -1 );
                }
                nLogo  += 2;
                init[5] = 1;
            /*    printf("GetLogo[%d] inst:%d module:%d type:%d\n",
                        nLogo, (int) GetLogo[nLogo].instid,
                               (int) GetLogo[nLogo].mod,
                               (int) GetLogo[nLogo].type ); */  /*DEBUG*/
            /*    printf("GetLogo[%d] inst:%d module:%d type:%d\n",
                        nLogo+1, (int) GetLogo[nLogo+1].instid,
                               (int) GetLogo[nLogo+1].mod,
                               (int) GetLogo[nLogo+1].type ); */  /*DEBUG*/
            }
  /*6*/     else if( k_its("OutputDir") ) {
                str = k_str();
                if(str) strcpy( OutputDir, str );
                init[6] = 1;
            }
  /*7*/     else if( k_its("BaseName") ) {
                str = k_str();
                if(str) strcpy( TrigFileBase, str );
                init[7] = 1;
            }

      
/*8*/          /* get station list path/name
                *****************************/
            else if( k_its("StationList") ) {
                if ( nStaDB >= MAX_STADBS ) {
                    fprintf( stderr, "%s Too many <StationList> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_STADBS );
                    return;
                }
                str = k_str();
                if( (int)strlen(str) >= SHORT_STR) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            str, SHORT_STR);
                    exit(-1);
                }
                if(str) strcpy( stationList[nStaDB] , str );
                stationListType[nStaDB] = 0;
                nStaDB++;
                init[8] = 1;
            }
            else if( k_its("StationList1") ) {
                if ( nStaDB >= MAX_STADBS ) {
                    fprintf( stderr, "%s Too many <StationList> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_STADBS );
                    return;
                }
                str = k_str();
                if( (int)strlen(str) >= SHORT_STR) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, SHORT_STR);
                    exit(-1);
                }
                if(str) strcpy( stationList[nStaDB] , str );
                stationListType[nStaDB] = 1;
                nStaDB++;
                init[8] = 1;
            }

 /*optional*/
            else if( k_its("ArcDir") ) {
                str = k_str();
                if( (int)strlen(str) >= SHORT_STR) {
                    fprintf(stderr,"%s Fatal error. Arc directory name %s greater than %d char.\n",
                        whoami, str, SHORT_STR);
                    exit( -1 );
                }
                j = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[j-1] != '/' ) strcat(str, "/");
                if(str) strcpy( ArcInputDir , str );
                arcfileflg = 1;
            }
  /*optional*/
            else if( k_its("PrePickTime") ) {
                PrePickTime = k_val();
            }
  /*optional*/
            else if( k_its("PostCodaTime") ) {
                PostCodaTime = k_val();
            }
  /*optional*/
            else if( k_its("Debug") ) {  
                Debug = k_int();
                if( k_err() ) Debug = 99;
            }
  /*optional*/
            else if( k_its("SCN") ) {  
                LocCode = 0;
            }
  /*optional*/
            else if( k_its("SCNL") ) {  
                LocCode = 1;
            }

         /* Unknown command
          *****************/
            else {
                fprintf( stderr, "arc2trig2: <%s> Unknown command in <%s>.\n",
                         com, configfile );
                continue;
            }

        /* See if there were any errors processing the command
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr,
                       "arc2trig2: Bad <%s> command in <%s>; exiting!\n",
                        com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "arc2trig2: ERROR, no " );
       if ( !init[0] )  fprintf( stderr, "<LogFile> "           );
       if ( !init[1] )  fprintf( stderr, "<MyModuleId> "        );
       if ( !init[2] )  fprintf( stderr, "<InRingName> "        );
       if ( !init[3] )  fprintf( stderr, "<OutRingName> "       );
       if ( !init[4] )  fprintf( stderr, "<HeartBeatInterval> " );
       if ( !init[5] )  fprintf( stderr, "<GetEventsFrom> "     );
       if ( !init[6] )  fprintf( stderr, "<OutputDir>"          );
       if ( !init[7] )  fprintf( stderr, "<BaseName>"           );
       if ( !init[8] )  fprintf( stderr, "<StationList>"        );
       fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }

   return;
}

/******************************************************************************
 *  arc2trig_lookup( )   Look up important info from earthworm.h tables       *
 ******************************************************************************/
void arc2trig_lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( InRingKey = GetKey(InRingName) ) == -1 ) {
        fprintf( stderr,
                "arc2trig2:  Invalid ring name <%s>; exiting!\n", InRingName);
        exit( -1 );
   }

/* Look up keys to shared memory regions
   *************************************/
   if( ( OutRingKey = GetKey(OutRingName) ) == -1 ) {
        fprintf( stderr,
                "arc2trig2:  Invalid ring name <%s>; exiting!\n", OutRingName);
        exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr,
              "arc2trig2: error getting local installation id; exiting!\n" );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr,
              "arc2trig2: Invalid module name <%s>; exiting!\n", MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr,
              "arc2trig2: Invalid message type <TYPE_HEARTBEAT>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr,
              "arc2trig2: Invalid message type <TYPE_ERROR>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_HYP2000ARC", &TypeHyp2000Arc ) != 0 ) {
      fprintf( stderr,
              "arc2trig2: Invalid message type <TYPE_HYP2000ARC>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_H71SUM2K", &TypeH71Sum2k ) != 0 ) {
      fprintf( stderr,
              "arc2trig2: Invalid message type <TYPE_H71SUM2K>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRIGLIST2K", &TypeTrigList ) != 0 ) {
      fprintf( stderr,
              "arc2trig2: Invalid message type <TYPE_TRIGLIST2K>; exiting!\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRIGLIST_SCNL", &TypeTrigList2 ) != 0 ) {
      fprintf( stderr,
              "arc2trig2: Invalid message type <TYPE_TRIGLIST_SCNL>; Use SCNs only!\n" );
      LocCode = 0;
   }

   return;
}

/******************************************************************************
 * arc2trig_status() builds a heartbeat or error message & puts it into       *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/
void arc2trig_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size;
   time_t      t;

/* Build the message
 *******************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
        sprintf( msg, "%ld %ld\n\0", (long)t, MyPid );
   }
   else if( type == TypeError )
   {
        sprintf( msg, "%ld %hd %s\n\0", (long)t, ierr, note);
        logit( "et", "arc2trig2: %s\n", note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &OutRegion, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","arc2trig2:  Error sending heartbeat.\n" );
        }
        else if( type == TypeError ) {
           logit("et","arc2trig2:  Error sending error:%d.\n", ierr );
        }
   }

   return;
}

/*******************************************************************
 *  arc2trig_hinvarc( )  read a Hypoinverse archive message        *
 *                       and build a trigger message               *
 *******************************************************************/
int arc2trig_hinvarc( char* arcmsg, char* trigMsg, MSG_LOGO incoming_logo )
{
    MSG_LOGO  outgoing_logo;  /* outgoing logo                         */
    char     *in;             /* working pointer to archive message    */
    char      line[MAX_STR];  /* to store lines from msg               */
    char      shdw[MAX_STR];  /* to store shadow cards from msg        */
    int       msglen;         /* length of input archive message       */
    int       nline;          /* number of lines (not shadows) so far  */
    int       i, j;
    double    dist, faz, baz;
    char    whoami[50];


    sprintf(whoami, "%s: %s: ", progname, "arc2trig_hinvarc");
   /* Initialize some stuff
   ***********************/
   nline  = 0;
   msglen = strlen( arcmsg );
   in     = arcmsg;

   /* load outgoing logo
   *********************/
   outgoing_logo.instid = InstId;
   outgoing_logo.mod    = MyModId;
   outgoing_logo.type   = LocCode? TypeTrigList2:TypeTrigList;


  /* Read one data line and its shadow at a time from arcmsg; process them
   ***********************************************************************/
   while( in < arcmsg+msglen )
   {
      if ( sscanf( in, "%[^\n]", line ) != 1 )  return( -1 );
      in += strlen( line ) + 1;
      if ( sscanf( in, "%[^\n]", shdw ) != 1 )  return( -1 );
      in += strlen( shdw ) + 1;
      nline++;
      /*logit( "e", "%s\n", line );*/  /*DEBUG*/
      /*logit( "e", "%s\n", shdw );*/  /*DEBUG*/

      /* Process the hypocenter card (1st line of msg) & its shadow
      ************************************************************/
      if( nline == 1 ) {                /* hypocenter is 1st line in msg  */
         read_hyp( line, shdw, &Sum );
         bldtrig_hyp( trigMsg, incoming_logo, outgoing_logo);        /* write 1st part of trigger msg  */
         continue;
      }

      /* Process all the phase cards & their shadows
      *********************************************/
      if( strlen(line) < (size_t) 75 )  /* found the terminator line      */
         break;
      /* read_phs( line, shdw, &Pick );    load info into Pick structure   */
      /* bldtrig_phs( trigMsg );           write "phase" lines of trigger msg  */

   } /*end while over reading message*/
   
    /* Build faux phase cards for K2s & Refteks
    *********************************************/
if(Debug)logit("et","%s NSCN: %d\n", whoami, NSCN);
    for(i=0;i<NSCN;i++) {
        if(Chan[i].Inst_type==6 || Chan[i].Inst_type==9) {
            strncpy(Pick.site,Chan[i].Site,6);
            strncpy(Pick.comp,Chan[i].Comp,4);
            strncpy(Pick.net, Chan[i].Net, 3);
            strncpy(Pick.loc, Chan[i].Loc, 3);
            Pick.Plabel = 'P';
            Pick.codalen = 0;
if(Debug)logit("et","%s SCNL: %s %s %s %s %d %d\n", whoami, Pick.site, Pick.comp, Pick.net, Pick.loc, i, Chan[i].Inst_type);
            distaz((double)Sum.lat, (double)Sum.lon, Chan[i].Lat, Chan[i].Lon, &dist, &faz, &baz);
            Pick.Pat = Sum.ot + dist/6.0;
            
            bldtrig_phs( trigMsg );           /* write "phase" lines of trigger msg  */
        }
    }

  /* Write trigger message to output ring
   **************************************/
   if( tport_putmsg( &OutRegion, &outgoing_logo, strlen(trigMsg), trigMsg ) != PUT_OK )
   {
      logit("et","arc2trig2: Error writing trigger message to ring.\n" );
   }

   /* Write trigger message to trigger file
    ***************************************/
   if( writetrig( "\n" ) != 0 )  /* add a blank line before trigger list */
   {
      arc2trig_status( TypeError, ERR_FILEIO, "Error opening trigger file" );
   }
   if( writetrig( trigMsg ) != 0 )
   {
      arc2trig_status( TypeError, ERR_FILEIO, "Error opening trigger file" );
   }
   if( writetrig( "\n" ) != 0 )  /* add a blank line after trigger list */
   {
      arc2trig_status( TypeError, ERR_FILEIO, "Error opening trigger file" );
   }

   return(0);
}

/**************************************************************
 * bldtrig_hyp() builds the EVENT line of a trigger message   *
 * Modified for author id by alex 7/10/98                     *
 **************************************************************/
void bldtrig_hyp( char *trigmsg, MSG_LOGO incoming, MSG_LOGO outgoing)
{
   char datestr[DATESTR_LEN];

/* Sample EVENT line for trigger message:
EVENT DETECTED     970729 03:01:13.22 UTC EVENT ID:123456 AUTHOR: asdf:asdf\n
0123456789 123456789 123456789 123456789 123456789 123456789
************************************************************/
   make_datestr( Sum.ot, datestr );
   if(LocCode) {
   sprintf( trigmsg, "v2.0 EVENT DETECTED     %s UTC EVENT ID: %u AUTHOR: %03d%03d%03d:%03d%03d%03d\n\n", 
            datestr, (int) Sum.qid,
            incoming.type, incoming.mod, incoming.instid,
            outgoing.type, outgoing.mod, outgoing.instid );
   strcat ( trigmsg, "Sta/Cmp/Net/Loc   Date   Time                       start save       duration in sec.\n" );
   strcat ( trigmsg, "---------------   ------ ---------------    ------------------------------------------\n");
   } else {
   sprintf( trigmsg, "EVENT DETECTED     %s UTC EVENT ID: %u AUTHOR: %03d%03d%03d:%03d%03d%03d\n\n", 
            datestr, (int) Sum.qid,
            incoming.type, incoming.mod, incoming.instid,
            outgoing.type, outgoing.mod, outgoing.instid );
   strcat ( trigmsg, "Sta/Cmp/Net   Date   Time                       start save       duration in sec.\n" );
   strcat ( trigmsg, "-----------   ------ ---------------    ------------------------------------------\n");
   }
   printf( "\n%s", trigmsg ); /*DEBUG*/

   return;
}

/****************************************************************
 * bldtrig_phs() builds the "phase" lines of a trigger message  *
 ****************************************************************/
void bldtrig_phs( char *trigmsg )
{
   char str[MEDIUM_STR];
   char pckt_str[DATESTR_LEN];
   char savet_str[DATESTR_LEN];
   double event_duration;  /* coda duration from tau()      */
   double save_duration;   /* total duration to save        */
   long codalen;

/* Convert times in seconds since 1600 to character strings
 **********************************************************/
   make_datestr( Pick.Pat,        pckt_str  );
   make_datestr( Pick.Pat-PrePickTime, savet_str );

/* Calculate how much to save based on the longer of
   calculated tau or picker-measured coda length
 ***************************************************/
   event_duration = tau( Sum.Md );
   codalen = Pick.codalen;
   if( codalen < 0 ) codalen = -codalen;

   /* Include PrePickTime in duration time, since we are
      starting at time PickTime-PrePickTime 
      davidk 032400
   ****************************************************/
   if( event_duration > codalen ) 
   {
      save_duration = event_duration + PostCodaTime + PrePickTime;
   }
   else 
   {
      save_duration = codalen + PostCodaTime + PrePickTime;
   }


/* Build the "phase" line!  Here's a sample:
alex 11/1/97: changed format to be variable lenth <station> <comp> <net>
separated by spaces:
 MCM VHZ NC N 19970729 03:01:13.34 UTC    save: yyyymmdd 03:00:12.34      120\n
0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789
********************************************************************************/
   if(LocCode) {
   sprintf( str, " %s %s %s %s %c %s UTC    save: %s %8ld\n",
            Pick.site, Pick.comp, Pick.net, Pick.loc, Pick.Plabel,
            pckt_str, savet_str, (long)save_duration );
   } else {
   sprintf( str, " %s %s %s %c %s UTC    save: %s %8ld\n",
            Pick.site, Pick.comp, Pick.net, Pick.Plabel,
            pckt_str, savet_str, (long)save_duration );
   }
   printf( "%s",str ); /*DEBUG*/

   strcat( trigmsg, str );

   return;
}

/*********************************************************************
 * make_datestr()  takes a time in seconds since 1600 and converts   *
 *                 it into a character string in the form of:        *
 *                   "19880123 12:34:12.21"                          *
 *                 It returns a pointer to the new character string  *
 *                                                                   *
 *    NOTE: this requires an output buffer >=21 characters long      *
 *                                                                   *
 *  Y2K compliance:                                                  *
 *     date format changed to YYYYMMDD                               *
 *     date15() changed to date17()                                  *
 *                                                                   *
 *********************************************************************/

char *make_datestr( double t, char *datestr )
{
    char str17[18];   /* temporary date string */

/* Convert time to a pick-format character string */
    date17( t, str17 );

/* Convert a date character string in the form of:
   "19880123123412.21"        to one in the form of:
   "19880123 12:34:12.21"
    0123456789 123456789
   Requires a character string at least 21 characters long
*/
    strncpy( datestr, str17,    8 );    /*yyyymmdd*/
    datestr[8] = '\0';
    strcat ( datestr, " " );
    strncat( datestr, str17+8,  2 );    /*hr*/
    strcat ( datestr, ":" );
    strncat( datestr, str17+10,  2 );    /*min*/
    strcat ( datestr, ":" );
    strncat( datestr, str17+12, 5 );    /*seconds*/

    /*printf( "str17 <%s>  newstr<%s>\n", str17, datestr );*/ /*DEBUG*/

    return( datestr );
}

/******************************************************
 * tau()  Calculate tau (duration) from magnitude     *
 ******************************************************/
double tau( float xmag )
{
/* From Dave Oppenheimer:
 *    TAU = 10.0**((XMAG - FMA)/FMB)
 *  where TAU  is in seconds,
 *        XMAG is the magnitude,
 *        FMA  is the coda magnitude intercept coefficient
 *        FMB  is the coda magnitude duration coefficient
 */

   float fma   = (float) -0.87;  /* value from a paper by Bakun */
   float fmb   = (float)  2.0;   /* value from a paper by Bakun */

   return( pow( (double)10.0, (double) (xmag-fma)/fmb ) );
}


/************************************************************************

    Subroutine distaz (lat1,lon1,lat2,lon2,RNGKM,FAZ,BAZ)
    
c--  COMPUTES RANGE AND AZIMUTHS (FORWARD AND BACK) BETWEEN TWO POINTS.
c--  OPERATOR CHOOSES BETWEEN 3 FIRST ORDER ELLIPSOIDAL MODELS OF THE
c--  EARTH AS DEFINED BY THE MAJOR RADIUS AND FLATTENING.
c--  THE PROGRAM UTILIZES THE SODANO AND ROBINSON (1963) DIRECT SOLUTION
c--  OF GEODESICS (ARMY MAP SERVICE, TECH REP #7, SECTION IV).
c--  (TERMS ARE GIVEN TO ORDER ECCENTRICITY TO THE FOURTH POWER.)
c--  ACCURACY FOR VERY LONG GEODESICS:
c--          DISTANCE < +/-  1 METER
c--          AZIMUTH  < +/- .01 SEC
c
    Ellipsoid            Major Radius    Minor Radius    Flattening
1  Fischer 1960            6378166.0        6356784.28      298.30
2  Clarke1866              6378206.4        6356583.8       294.98
3  S. Am 1967              6378160.0        6356774.72      298.25
4  Hayford Intl 1910       6378388.0        6356911.94613   297.00
5  WGS 1972                6378135.0        6356750.519915  298.26
6  Bessel 1841             6377397.155      6356078.96284   299.1528
7  Everest 1830            6377276.3452     6356075.4133    300.8017
8  Airy                    6377563.396      6356256.81      299.325
9  Hough 1960              6378270.0        6356794.343479  297.00
10 Fischer 1968            6378150.0        6356768.337303  298.30
11 Clarke1880              6378249.145      6356514.86955   293.465
12 Fischer 1960            6378155.0        6356773.32      298.30
13 Intl Astr Union         6378160.0        6378160.0       298.25
14 Krasovsky               6378245.0        6356863.0188    298.30
15 WGS 1984                6378137.0        6356752.31      298.257223563
16 Aust Natl               6378160.0        6356774.719     298.25
17 GRS80                   6378137.0        63567552.31414  298.2572
18 Helmert                 6378200.0        6356818.17      298.30
19 Mod. Airy               6377341.89       6356036.143     299.325
20 Mod. Everest            6377304.063      6356103.039     300.8017
21 Mercury 1960            6378166.0        6356784.283666  298.30
22 S.E. Asia               6378155.0        6356773.3205    298.2305
23 Sphere                  6370997.0        6370997.0         0.0
24 Walbeck                 6376896.0        6355834.8467    302.78
25 WGS 1966                6378145.0        6356759.769356  298.25

************************************************************************/

int distaz (double lat1, double lon1, double lat2, double lon2, 
                double *rngkm, double *faz, double *baz)
{
    double    pi, rd, f, fsq;
    double    a3, a5, a6, a10, a16, a17, a18, a19, a20;
    double    a21, a22, a22t1, a22t2, a22t3, a23, a24, a25, a27, a28, a30;
    double    a31, a32, a33, a34, a35, a36, a38, a39, a40;
    double    a41, a43;
    double    a50, a51, a52, a54, a55, a57, a58, a62;
    double    dlon, alph[2];
    double    p11, p12, p13, p14, p15, p16, p17, p18;
    double    p21, p22, p23, p24, p25, p26, p27, p28, pd1, pd2;
    double    ang45;
    short     k;
    double    RAD, MRAD, FINV;
    
    RAD  = 6378206.4;
    MRAD = 6356583.8;
    FINV = 294.98;
    ang45 = atan(1.0);
    
    pi = 4.0*atan(1.0);
    rd = pi/180.0;

    a5 = RAD;
    a3 = FINV;
    
    if(a3==0.0) {
        a6 = MRAD;                    /*    minor radius of ellipsoid    */
        f = fsq = a10 = 0.0;
    }
    else {
        a6 = (a3-1.0)*a5/a3;          /*    minor radius of ellipsoid    */
        f = 1.0/a3;                   /*    flattening    */
        fsq = 1.0/(a3*a3);
        a10 = (a5*a5-a6*a6)/(a5*a5);  /*    eccentricity squared    */
    }
    
    /*    Following definitions are from Sodano algorithm for long geodesics    */
    a50 = 1.0 + f + fsq;
    a51 = f + fsq;
    a52 = fsq/ 2.0;
    a54 = fsq/16.0;
    a55 = fsq/ 8.0;
    a57 = fsq* 1.25;
    a58 = fsq/ 4.0;
    
/*     THIS IS THE CALCULATION LOOP. */
    
    p11 = lat1*rd;
    p12 = lon1*rd;
    p21 = lat2*rd;
    p22 = lon2*rd;
    *rngkm = *faz = *baz = 0.0;
    if( (lat1==lat2) && (lon1==lon2))    return(0);
    
    /*    Make sure points are not exactly on the equator    */
    if(p11 == 0.0) p11 = 0.000001;
    if(p21 == 0.0) p21 = 0.000001;
    
    /*    Make sure points are not exactly on the same meridian    */
    if(p12 == p22)             p22 += 0.0000000001;
    if(fabs(p12-p22) == pi)    p22 += 0.0000000001;
    
    /*    Correct latitudes for flattening    */
    p13 = sin(p11);
    p14 = cos(p11);
    p15 = p13/p14;
    p18 = p15*(1.0-f);
    a62 = atan(p18);
    p16 = sin(a62);
    p17 = cos(a62);
    
    p23 = sin(p21);
    p24 = cos(p21);
    p25 = p23/p24;
    p28 = p25*(1.0-f);
    a62 = atan(p28);
    p26 = sin(a62);
    p27 = cos(a62);
    
    dlon = p22-p12;
    a16 = fabs(dlon);
    
    /*    Difference in longitude to minimum (<pi)    */
    if(a16 >= pi) a16 = 2.0*pi - a16;

    /*    Compute range (a35)    */
    if(a16==0.0)     {a17 = 0.0;         a18 = 1.0;}
    else             {a17 = sin(a16);    a18 = cos(a16);}
    a19 = p16*p26;
    a20 = p17*p27;
    a21 = a19 + a20*a18;
    
    a40 = a41 = a43 = 0.0;
    
    for(k=0; k<2; k++) {
        a22t1 = (a17*p27)*(a17*p27);
        a22t2 = p26*p17 - p16*p27*a18;
        a22   = sqrt(a22t1 + (a22t2*a22t2));
        if(a22 == 0.0) return(0);
        a22t3 = a22*a21;
        a23 = (a20*a17)/a22;
        a24 = 1.0 - a23*a23;
        a25 = asin(a22);
        if(a21 < 0.0) a25 = pi-a25;
        a27 = (a25*a25)/a22;
        a28 = a21*a27;
        if(k==0) {
            a30 = (a50*a25) + a19*(a51*a22-a52*a27);
            a31 = 0.5*a24*(fsq*a28 - a51*(a25 + a22t3));
            a32 = a19*a19*a52*a22t3;
            a33 = a24*a24*(a54*(a25 + a22t3) - a52*a28 - a55*a22t3*(a21*a21));
            a34 = a19*a24*a52*(a27 + a22t3*a21);
            a35 = (a30 + a31 - a32 + a33 + a34)*a6;
            *rngkm = a35/1000.0;
        }

    /*    Compute azimuths    */
        a36 = (a51*a25) - a19*(a52*a22 + fsq*a27) + a24*(a58*a22t3 - a57*a25 + fsq*a28);
        a38 = a36*a23 + a16;
        a39 = sin(a38);
        a40 = cos(a38);

        if(a39*p27 == 0.0) a43 = 0.0;
        else {
          a41 = (p26*p17 - a40*p16*p27)/(a39*p27);
          if(a41 == 0.0)    a43 = pi/2.0;
          else                a43 = atan(1.0/a41);
        }

        alph[k] = a43;
        if((dlon <= -pi) || ((dlon >= 0.0) && (dlon < pi))) {
          if(a41 >= 0.0)       alph[k] = alph[k]-pi;
        }
        else {
          if(a41 >= 0.0)       alph[k] = pi - alph[k];
          else                 alph[k] = 2.0*pi - alph[k];
        }
        if(alph[k] >= pi)      alph[k] = alph[k] - pi;
        if(alph[k] <  pi)      alph[k] = alph[k] + pi;
        if(alph[k] <  0.0)     alph[k] = alph[k] + 2.0*pi;
        if(alph[k] >= 2.0*pi)  alph[k] = alph[k] - 2.0*pi;
        alph[k] = alph[k]/rd;
        pd1 = p16;
        pd2 = p17;
        p16 = p26;
        p17 = p27;
        p26 = pd1;
        p27 = pd2;
        dlon = -dlon;
    }

    *faz = alph[0];
    *baz = alph[1];
    while (*faz >= 360.0) *faz = *faz - 360.0;
    while (*baz >= 360.0) *baz = *baz - 360.0;
    
    return(0);
}


/****************************************************************************
 *  Get_Sta_Info();                                                         *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Sta_Info(void)
{
    char    whoami[50], *com, *str, ns, ew;
    int     i, j, k, nfiles, success, type, sensor, units;
    double  dlat, mlat, dlon, mlon, gain, sens, ssens;

    sprintf(whoami, "%s: %s: ", progname, "Get_Sta_Info");
    
    ns = 'N';
    ew = 'W';
    NSCN = 0;
    for(k=0;k<nStaDB;k++) {
            /* Open the main station file
             ****************************/
        nfiles = k_open( stationList[k] );
        if(nfiles == 0) {
            fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, stationList[k] );
            exit( -1 );
        }

            /* Process all command files
             ***************************/
        while(nfiles > 0) {  /* While there are command files open */
            while(k_rd())  {      /* Read next line from active file  */
                com = k_str();         /* Get the first token from line */

                    /* Ignore blank lines & comments
                     *******************************/
                if( !com )           continue;
                if( com[0] == '#' )  continue;

                    /* Open a nested configuration file
                     **********************************/
                if( com[0] == '@' ) {
                    success = nfiles+1;
                    nfiles  = k_open(&com[1]);
                    if ( nfiles != success ) {
                        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                        exit( -1 );
                    }
                    continue;
                }

                /* Process anything else as a channel descriptor
                 ***********************************************/

                if( NSCN >= MAXCHANNELS ) {
                    fprintf(stderr, "%s Too many channel entries in <%s>", 
                             whoami, stationList[k] );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAXCHANNELS );
                    exit( -1 );
                }
                j = NSCN;
                
                    /* S C N */
                strncpy( Chan[j].Site, com,  6);
                str = k_str();
                if(str) strncpy( Chan[j].Net,  str,  2);
                str = k_str();
                if(str) strncpy( Chan[j].Comp, str, 3);
                for(i=0;i<6;i++) if(Chan[j].Site[i]==' ') Chan[j].Site[i] = 0;
                for(i=0;i<2;i++) if(Chan[j].Net[i]==' ')  Chan[j].Net[i]  = 0;
                for(i=0;i<3;i++) if(Chan[j].Comp[i]==' ') Chan[j].Comp[i] = 0;
                Chan[j].Comp[3] = Chan[j].Net[2] = Chan[j].Site[5] = 0;
                strcpy(Chan[j].Loc, "--");
                if(stationListType[k] == 1) {
                    str = k_str();
                    if(str) strncpy( Chan[j].Loc, str, 2);
                    for(i=0;i<2;i++) if(Chan[j].Loc[i]==' ')  Chan[j].Loc[i]  = 0;
                    Chan[j].Loc[2] = 0;
                }
                sprintf(Chan[j].SCNtxt, "%s %s %s %s", Chan[j].Site, Chan[j].Comp, Chan[j].Net, Chan[j].Loc);


                    /* Lat Lon Elev */
                if(stationListType[k] == 0) {
	                dlat = k_int();
	                mlat = k_val();
	                
	                dlon = k_int();
	                mlon = k_val();
	                
	                Chan[j].Elev = k_val();
	                
	                    /* convert to decimal degrees */
	                if ( dlat < 0 ) dlat = -dlat;
	                if ( dlon < 0 ) dlon = -dlon;
	                Chan[j].Lat = dlat + (mlat/60.0);
	                Chan[j].Lon = dlon + (mlon/60.0);
	                    /* make south-latitudes and west-longitudes negative */
	                if ( ns=='s' || ns=='S' )               Chan[j].Lat = -Chan[j].Lat;
	                if ( ew=='w' || ew=='W' || ew==' ' )    Chan[j].Lon = -Chan[j].Lon;
                } else if(stationListType[k] == 1) {
                    Chan[j].Lat  = k_val();
                    Chan[j].Lon  = k_val();
                    Chan[j].Elev = k_val();
                }

         /*       str = k_str();      Blow past the subnet */
                
                type   = k_int();
                sens   = k_val();
                gain   = k_val();
                sensor = k_int();
                units  = k_int();
                ssens  = k_val();
                Chan[j].SiteCorr = k_val();
                        
                if(units == 3) ssens /= 981.0;
                Chan[j].Inst_type = type;
                Chan[j].Inst_gain = sens;
                Chan[j].GainFudge = gain;
                Chan[j].Sens_type = sensor;
                Chan[j].Sens_gain = ssens;
                Chan[j].Sens_unit = units;
               
                Chan[j].sensitivity = (1000000.0*ssens/sens)*gain*Chan[j].SiteCorr;    /*    sensitivity counts/units        */
                
                if(stationListType[k] == 1) Chan[j].ShkQual = k_int();
                
                if (k_err()) {
                    logit("e", "%s Error decoding line in station file\n%s\n  exiting!\n", whoami, k_get() );
                    exit( -1 );
                }
         /*>Comment<*/
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( Chan[j].SiteName, str );
                    
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( Chan[j].Descript, str );
                    
                NSCN++;
            }
            nfiles = k_close();
        }
    }
}



