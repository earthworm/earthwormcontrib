#
#                   Make file for ew2cta
#                        Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib
I = $(EW_HOME)/$(EW_VERSION)/include


EW2CTA = ew2cta.o $L/logit.o $L/kom.o \
         $L/getutil.o $L/sleep_ew.o $L/time_ew.o \
         $L/transport.o $L/swap.o $L/trheadconv.o

ew2cta: $(EW2CTA)
	cc -o $B/ew2cta $(EW2CTA) -lm -lposix4 -lc

# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/ew2cta*

lint:
	lint -I $I -Nlevel=4 ew2cta.c

