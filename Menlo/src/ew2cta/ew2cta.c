
  /********************************************************************
   *                             ew2cta.c                             *
   ********************************************************************/
/*
   This program writes tracebuf messages to disk files in cta format.
   Cta format is just a bunch of TRACE_BUF and TRACE2_BUF messages
   concatenated together.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
/*#include <unistd.h> */ /* Not found on Windows */
#include <kom.h>
#include <transport.h>
#include <trace_buf.h>
#include <swap.h>
#include <trheadconv.h>
#include <time_ew.h>
#include "ew2cta.h"

/* Function prototypes
   *******************/
void   GetConfig( char * );
void   LogConfig( void );
void   LookupMore( void );
void   LookupMsgType( void );
void   WriteTracebuf2Msg( char *, long, int );

/* Global variables
   ****************/
static SHM_INFO Region;              /* Shared memory region to use for i/o */
static pid_t    myPid;               /* Process id of this process */
static FILE     *fp;
static char     Text[150];           /* String for log/error messages */

/* Get these from the config file
   ******************************/
static MSG_LOGO *GetLogo = NULL;     /* Array for module,type,instid */
static size_t   GetLogoSize = 0;     /* In bytes */
static short    nLogo = 0;
static char     RingName[20];        /* Name of transport ring for i/o */
static char     MyModName[20];       /* Speak as this module name/id */
static long     HeartBeatInt;        /* Seconds between heartbeats */
static char     OutDir[100];         /* Name of output directory */
static char     TmpDir[100];         /* Name of temporary directory */
static int      CtaFileLen;          /* Write to cta file this often */

/* Look these up in the earthworm.h tables with getutil functions
   **************************************************************/
static long          RingKey;        /* Transport ring key */
static unsigned char InstId;         /* Local installation id */
static unsigned char MyModId;        /* Module Id for this program */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeTracebuf;
static unsigned char TypeTracebuf2;


int main( int argc, char **argv )
{
   long          timeNow;           /* Current time */
   long          timeLastBeat;      /* Time last heartbeat was sent */
   long          recsize;           /* Size of retrieved message */
   MSG_LOGO      reclogo;           /* Logo of retrieved message */
   int           res;
   TRACE_HEADER  *TraceHead;        /* Pointer to tracebuf header */
   TRACE2_HEADER *Trace2Head;       /* Pointer to tracebuf2 header */
   char          *TraceBuf;         /* Buffer to hold event msg */
   char          *configFile;

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      printf( "Usage: ew2cta <configfile>\n" );
      return 0;
   }
   configFile = argv[1];

/* Look up message types from earthworm.h tables
   *********************************************/
   LookupMsgType();

/* Read the configuration file
   ***************************/
   GetConfig( configFile );

/* Look up more info from earthworm.h tables
   *****************************************/
   LookupMore();

/* Initialize name of log-file & open it
   *************************************/
   logit_init( "ew2cta", (short) MyModId, 256, 1 );
   logit( "" , "Read command file <%s>\n", argv[1] );

/* Get the pid of this process for restart purposes
   ************************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "ew2cta: Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Log the configuration file
   **************************/
   LogConfig();

/* Allocate buffer to hold one tracebuf message
   ********************************************/
   TraceBuf = (char *) malloc( (size_t)(MAX_TRACEBUF_SIZ+1) );
   if ( TraceBuf == NULL )
   {
      logit( "e", "Couldn't allocate the message buffer. Exiting.\n" );
      return -1;
   }
   logit( "", "Message buffer allocated.\n" );

/* Pointer to the tracebuf or tracebuf2 message
   ********************************************/
   TraceHead  = (TRACE_HEADER *) TraceBuf;
   Trace2Head = (TRACE2_HEADER *) TraceBuf;

/* Attach to shared memory ring
   ****************************/
   tport_attach( &Region, RingKey );
   logit( "", "Attached to public memory region: %s\n", RingName );

/* Force a heartbeat to be issued in first pass thru main loop
   ***********************************************************/
   timeLastBeat = time(&timeNow) - HeartBeatInt - 1;

/* Main loop
   *********/
   while ( tport_getflag( &Region ) != TERMINATE &&
           tport_getflag( &Region ) != myPid )
   {

/* Send a heartbeat to statmgr
   ***************************/
      if  ( (time(&timeNow) - timeLastBeat) >= HeartBeatInt )
      {
         timeLastBeat = timeNow;
         SendStatus( TypeHeartBeat, 0, "" );
      }

/* Process all new messages
   ************************/
      do
      {

/* Get type cta, tracebuf, and tracebuf2 messages from transport ring
   ******************************************************************/
         res = tport_getmsg( &Region, GetLogo, nLogo,
                             &reclogo, &recsize, TraceBuf, MAX_TRACEBUF_SIZ );

         if ( res == GET_NONE )              /* No more new messages */
            break;
         if ( res == GET_TOOBIG )            /* Next message was too big */
         {                                   /* Try again */
            sprintf( Text,
               "Retrieved msg[%ld] (i%u m%u t%u) too big for buffer[%d]",
               recsize, reclogo.instid, reclogo.mod, reclogo.type,
               MAX_TRACEBUF_SIZ );
            SendStatus( TypeError, ERR_TOOBIG, Text );
            continue;
         }
         if ( res == GET_MISS )         /* Got a msg, but missed some */
         {
            sprintf( Text, "Missed msg(s)  i%u m%u t%u  %s.",
               reclogo.instid, reclogo.mod, reclogo.type, RingName );
            SendStatus( TypeError, ERR_MISSMSG, Text );
         }
         if ( res == GET_NOTRACK )      /* Got a msg, but can't tell */
         {                                   /* if any were missed */
            sprintf( Text, "Msg received; transport.h NTRACK_GET exceeded" );
            SendStatus( TypeError, ERR_NOTRACK, Text );
         }

/* This is a TYPE_TRACEBUF or TYPE_TRACEBUF2 message.
   Convert it to local byte order.
   *************************************************/
         if ( reclogo.type == TypeTracebuf )
         {
            if ( WaveMsgMakeLocal( TraceHead ) < 0 )
            {
               logit( "et", "ew2cta: WaveMsgMakeLocal() error.\n" );
               continue;
            }
         }
         else
            if ( WaveMsg2MakeLocal( Trace2Head ) < 0 )
            {
               logit( "et", "ew2cta: WaveMsg2MakeLocal error.\n" );
               continue;
            }

/* If this is a TYPE_TRACEBUF message, convert it to TYPE_TRACEBUF2
   ****************************************************************/
         if ( reclogo.type == TypeTracebuf )
            Trace2Head = TrHeadConv( TraceHead );

/* The message is now TYPE_TRACEBUF2.  Write it to a disk file
   ***********************************************************/
         WriteTracebuf2Msg( TraceBuf, recsize, 0 );

      } while ( res != GET_NONE );    /* End of message-processing loop */

      sleep_ew( 250 );                /* Wait for new messages to arrive */
   }

/* Get ready to exit
   *****************/
   tport_detach( &Region );
   WriteTracebuf2Msg( TraceBuf, recsize, 1 );
   logit( "et", "ew2cta: Exiting.\n" );
   return 0;
}


  /************************************************************************
   *  GetConfig() processes command file(s) using kom.c functions;        *
   *                    exits if any errors are encountered.              *
   ************************************************************************/

#define NCOMMAND 6

void GetConfig( char *configfile )
{
   const int ncommand = NCOMMAND;    /* Process this many required commands */
   char      init[NCOMMAND];         /* Init flags, one for each command */
   int       nmiss;                  /* Number of missing commands */
   char      *com;
   char      *str;
   int       nfiles;
   int       success;
   int       i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ ) init[i] = 0;
   strcpy( TmpDir, "" );

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      printf( "ew2cta: Error opening command file <%s>. Exiting.\n",
               configfile );
      exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )            /* While there are command files open */
   {
      while ( k_rd() )             /* Read next line from active file  */
      {
         com = k_str();            /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
         if ( !com )          continue;
         if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
         if ( com[0] == '@' )
         {
            success = nfiles+1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "ew2cta: Can't open command file <%s>. Exiting.\n",
                        &com[1] );
               exit( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         else if ( k_its("MyModuleId") )
         {
             str = k_str();
             if (str) strcpy( MyModName, str );
             init[0] = 1;
         }
         else if ( k_its("RingName") )
         {
             str = k_str();
             if (str) strcpy( RingName, str );
             init[1] = 1;
         }
         else if ( k_its("HeartBeatInt") )
         {
             HeartBeatInt = k_long();
             init[2] = 1;
         }
         else if ( k_its("GetEventsFrom") )
         {
             unsigned char instId;
             unsigned char modId;
             MSG_LOGO      *logoPtr;

             if ( ( str=k_str() ) )
             {
                if ( GetInst( str, &instId ) != 0 )
                {
                   printf( "ew2cta: Invalid installation name <%s>", str );
                   printf( " in <GetEventsFrom> cmd. Exiting.\n" );
                   exit( -1 );
                }
             }
             if ( ( str=k_str() ) )
             {
                if ( GetModId( str, &modId ) != 0 )
                {
                   printf( "ew2cta: Invalid module name <%s>", str );
                   printf( " in <GetEventsFrom> cmd. Exiting.\n" );
                   exit( -1 );
                }
             }
             GetLogoSize += sizeof( MSG_LOGO );
             logoPtr = (MSG_LOGO *)realloc( GetLogo, GetLogoSize );
             if ( logoPtr == NULL )
             {
                printf( "ew2cta: realloc() error 1 in GetConfig(). Exiting.\n" );
                exit( -1 );
             }
             GetLogo = logoPtr;
             GetLogo[nLogo].instid = instId;
             GetLogo[nLogo].mod    = modId;
             GetLogo[nLogo].type   = TypeTracebuf;
             nLogo ++;

             GetLogoSize += sizeof( MSG_LOGO );
             logoPtr = (MSG_LOGO *)realloc( GetLogo, GetLogoSize );
             if ( logoPtr == NULL )
             {
                printf( "ew2cta: realloc() error 2 in GetConfig(). Exiting.\n" );
                exit( -1 );
             }
             GetLogo = logoPtr;
             GetLogo[nLogo].instid = instId;
             GetLogo[nLogo].mod    = modId;
             GetLogo[nLogo].type   = TypeTracebuf2;
             nLogo ++;
             init[3] = 1;
         }
         else if ( k_its("OutDir") )
         {
             str = k_str();
             if (str) strcpy( OutDir, str );
             init[4] = 1;
         }
         else if ( k_its("CtaFileLen") )
         {
             CtaFileLen = k_int();
             init[5] = 1;
         }
 /*opt*/ else if ( k_its("TmpDir") )
         {
             str = k_str();
             if (str) strcpy( TmpDir, str );
         }

/* Unknown command
   ***************/
         else
         {
             printf( "ew2cta: <%s> Unknown command in <%s>.\n",
                      com, configfile );
             continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "ew2cta: Bad <%s> command in <%s>. Exiting.\n",
                     com, configfile );
            exit( -1 );
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss )
   {
       printf( "ew2cta: ERROR, no " );
       if ( !init[0] )  printf( "<MyModuleId> "    );
       if ( !init[1] )  printf( "<RingName> "      );
       if ( !init[2] )  printf( "<HeartBeatInt> "  );
       if ( !init[3] )  printf( "<GetEventsFrom> " );
       if ( !init[4] )  printf( "<OutDir> "        );
       if ( !init[5] )  printf( "<CtaFileLen> "    );
       printf( "command(s) in <%s>. Exiting.\n", configfile );
       exit( -1 );
   }

   if( strlen(TmpDir)==0 ) strcpy( TmpDir, OutDir );
   return;
}


  /************************************************************************
   *  LogConfig() prints the config parameters in the log file.           *
   ************************************************************************/

void LogConfig( void )
{
   int i;

   logit( "", "\nConfig File Parameters:\n" );
   logit( "", "   MyModuleId:      %s\n", MyModName );
   logit( "", "   RingName:        %s\n", RingName );
   logit( "", "   HeartBeatInt:    %d\n", HeartBeatInt );
   logit( "", "   OutDir:          %s\n", OutDir );
   logit( "", "   CtaFileLen:      %d\n", CtaFileLen );

   for ( i = 0; i < nLogo; i++ )
   {
      logit( "", "   GetEventsFrom:  inst:%u mod:%u type:%u\n",
         GetLogo[i].instid, GetLogo[i].mod, GetLogo[i].type );
   }
   return;
}


  /**********************************************************************
   *  LookupMsgType( )   Look up message types from earthworm.h tables  *
   **********************************************************************/

void LookupMsgType( void )
{
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "ew2cta: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "ew2cta: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF", &TypeTracebuf ) != 0 )
   {
      printf( "ew2cta: Invalid message type <TYPE_TRACEBUF>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF2", &TypeTracebuf2 ) != 0 )
   {
      printf( "ew2cta: Invalid message type <TYPE_TRACEBUF2>. Exiting.\n" );
      exit( -1 );
   }
}


  /**********************************************************************
   *  LookupMore( )   Look up more from earthworm.h tables              *
   **********************************************************************/

void LookupMore( void )
{
   if ( ( RingKey = GetKey(RingName) ) == -1 )
   {
        printf( "ew2cta:  Invalid ring name <%s>. Exiting.\n", RingName);
        exit( -1 );
   }
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "ew2cta: Error geting local installation id. Exiting.\n" );
      exit( -1 );
   }
   if ( GetModId( MyModName, &MyModId ) != 0 )
   {
      printf( "ew2cta: Invalid module name <%s>; Exiting.\n", MyModName );
      exit( -1 );
   }
   return;
}


/******************************************************************************
 * SendStatus() builds a heartbeat or error message & puts it into            *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/

void SendStatus( unsigned char type, short ierr, char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   long     t;

/* Build the message
   *****************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if ( type == TypeHeartBeat )
      sprintf( msg, "%ld %d\n", t, myPid );
   else if ( type == TypeError )
   {
      sprintf( msg, "%ld %hd %s\n", t, ierr, note );
      logit( "et", "ew2cta: %s\n", note );
   }
   else
      return;

   size = strlen( msg );           /* Don't include null byte in message */

/* Write the message to shared memory
   **********************************/
   if ( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if ( type == TypeHeartBeat )
           logit("et","ew2cta:  Error sending heartbeat.\n" );
        else
           if ( type == TypeError )
              logit("et","ew2cta:  Error sending error:%d.\n", ierr );
   }
   return;
}


   /**********************************************************************
    *                         RenameEventFile()                          *
    *  The names of old tracebuf files begin with "wt".                  *
    *  The names of new tracebuf2 files beging with "wu".                *
    *  That's how we keep track of the format of data with the files.    *
    **********************************************************************/

void RenameEventFile( char tempFileName[], time_t *tempFileStartTime )
{
   struct tm strTime;
   char      filename[17];                 /* Allow for the null byte */
   char      eventFileName[80];

   gmtime_ew( tempFileStartTime, &strTime );
   sprintf( filename, "wu%04d%02d%02d%02d%02d%02d",
            strTime.tm_year+1900,
            strTime.tm_mon+1,
            strTime.tm_mday,
            strTime.tm_hour,
            strTime.tm_min,
            strTime.tm_sec );
   strcpy( eventFileName, (char *)OutDir );
   strcat( eventFileName, "/" );
   strcat( eventFileName, filename );

   if ( rename( tempFileName, eventFileName ) == -1 )
      logit( "et", "ew2cta: Can't rename the temporary file %s to %s\n",
              tempFileName, eventFileName );
   else
      logit( "et", "ew2cta: File %s created.\n", filename );
   return;
}


   /*****************************************************************
    *                      WriteTracebuf2Msg()                      *
    *****************************************************************/

void WriteTracebuf2Msg( char *TraceBuf, long recsize, int termFlag )
{
   time_t        currentTime;
   int           numwritten;
   static time_t tempFileStartTime;
   static int    fileOpen = 0;
   static int    first = 1;
   static char   tempFileName[80];

/* Build the whole path name of the temporary file
   ***********************************************/
   if ( first )
   {
      strcpy( tempFileName, (char *)TmpDir );
      strcat( tempFileName, "/ew2cta.temp" );
      first = 0;
   }

/* We're shutting down
   *******************/
   if ( termFlag && fileOpen )
   {
      fclose( fp );
      RenameEventFile( tempFileName, &tempFileStartTime );
      return;
   }

/* Get the current time
   ********************/
   time( &currentTime );

/* If there is no open file, let's create
   a temporary file in the TmpDir directory
   ****************************************/
   if ( fileOpen == 0 )
   {
      tempFileStartTime = currentTime;

      if ( (fp = fopen( tempFileName, "wb" )) == NULL )
      {
         logit( "et", "ew2cta: Error creating the temporary file: %s\n",
                tempFileName );
         return;
      }
      fileOpen = 1;                         /* Flag the file as open */
   }

/* A temporary file is already open
   ********************************/
   else
   {

/* The message doesn't belong to the current temporary file.
   Close the file and open a new one.
   ********************************************************/
      if ( (currentTime - tempFileStartTime) >= CtaFileLen )
      {
         fclose( fp );
         fileOpen = 0;                      /* Flag the file as closed */

/* Rename the temporary file to its real name
   ******************************************/
         RenameEventFile( tempFileName, &tempFileStartTime );

/* Open a new temporary file
   *************************/
         tempFileStartTime = currentTime;
         if ( (fp = fopen( tempFileName, "wb" )) == NULL )
         {
            logit( "et", "ew2cta: Error creating the temporary file: %s\n",
                   tempFileName );
            return;
         }
         fileOpen = 1;                     /* Flag the file as open */
      }
   }

/* Write the tracebuf2 message to the output file
   **********************************************/
   numwritten = fwrite( TraceBuf, sizeof(char), recsize, fp );
   if ( numwritten < recsize )
      logit( "et", "ew2cta: Error writing to output file: %s\n",
             tempFileName );
   return;
}
