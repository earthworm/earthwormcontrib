#
# This is ew2cta's parameter file
#
MyModuleId   MOD_EW2CTA      # Module id for this instance of ew2cta
RingName     WAVE_RING       # Shared memory ring for input/output
HeartBeatInt 30              # Seconds between heartbeats
CtaFileLen   600             # Cta file length, in seconds

OutDir /home/earthworm/run-will/ew2cta     # completed cta files are moved here
TmpDir /home/earthworm/run-will/ew2cta/tmp # working cta files are written here

#
# Logos of messages to grab from transport ring
# (TYPE_TRACEBUF and TYPE_TRACEBUF2 only)
#
#               Installation       Module
GetEventsFrom   INST_MENLO       MOD_ADSEND_A
GetEventsFrom   INST_MENLO       MOD_ADSEND_B
GetEventsFrom   INST_MENLO       MOD_IMPORT1
GetEventsFrom   INST_MENLO       MOD_IMPORT2
GetEventsFrom   INST_MENLO       MOD_GETDST2
GetEventsFrom   INST_MENLO       MOD_NAQS2EW
GetEventsFrom   INST_MENLO       MOD_NAQS2EW_SAT
GetEventsFrom   INST_MENLO       MOD_CONDENSEK2
GetEventsFrom   INST_MENLO       MOD_CONDENSEK2B
GetEventsFrom   INST_MENLO       MOD_REFTEK2EW
GetEventsFrom   INST_MENLO       MOD_REFTEK2EW_B
GetEventsFrom   INST_UNR         MOD_WILDCARD
GetEventsFrom   INST_CIT         MOD_WILDCARD
GetEventsFrom   INST_CDWR        MOD_WILDCARD
GetEventsFrom   INST_PGE         MOD_WILDCARD
GetEventsFrom   INST_USNSN       MOD_WILDCARD
