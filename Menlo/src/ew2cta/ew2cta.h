
/* Error messages used by ew2cta
   *****************************/
#define  ERR_MISSMSG    0           /* Message missed in transport ring */
#define  ERR_TOOBIG     1           /* Retrieved msg too large for buffer */
#define  ERR_NOTRACK    2           /* Msg retrieved; tracking limit exceeded */

void    SendStatus( unsigned char, short, char * );

