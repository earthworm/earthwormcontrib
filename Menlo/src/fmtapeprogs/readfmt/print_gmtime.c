/*
 *   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT USING THE COMMAND CHECKOUT.
 *
 *    $Id: print_gmtime.c 888 2002-03-06 01:31:24Z kohler $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.2  2002/03/06 01:31:24  kohler
 *     Removed unused variable line[].
 *     Added type cast (time_t)time.
 *
 *     Revision 1.1  2000/02/14 16:00:43  lucky
 *     Initial revision
 *
 *
 */

#include <stdio.h>
#include <time.h>
#include <math.h>

void PrintGmtime( double time )
{
   int  hsec;
   struct tm *gmt;
   time_t ltime = (time_t)time;

   gmt = gmtime( &ltime );
   printf( "%-3d",  gmt->tm_yday+1 );
// printf( " %-2d", gmt->tm_mon+1 );
// printf( "/%2d",  gmt->tm_mday );
// printf( "/%4d",  gmt->tm_year+1900 );
   printf( " %02d", gmt->tm_hour );
   printf( ":%02d", gmt->tm_min );
   printf( ":%02d", gmt->tm_sec );
   hsec = (int)(100. * (time - floor(time)));
   printf( ".%02d", hsec );
   return;
}
