
      /*********************************************************************
       *                             readfmt.c                             *
       *                                                                   *
       *         Read an fmt file created by the fmtape program.           *
       *********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <time.h>
#include <time_ew.h>
#include "irige.h"

#define N_DIO_PORTS 12

typedef struct
{
   long sec;
   long min;
   long hour;
   long day;
   long hun;       // Hundredths of a second
   long footage;
   long tapeid;
   long syncError;
} DIO;

void   DecodeDIO( long pattern[], DIO *dioPtr );
void   irige_init( struct TIME_BUFF * );
int    Irige( struct TIME_BUFF *, long, int, int, short *, int, int );
void   PrintGmtime( double time );
double TcgToDouble( DIO dio, int Year );
void   YdayToMday( int Year, int yday, int *mon, int *mday );


int main( int argc, char *argv[] )
{
   int    numRead;
   char   fname[100];
   FILE   *fpfmt;
   long   nport;
   long   nchan;
   long   nscan;
   long   scan;
   long   pattern[N_DIO_PORTS]; // Data from PCI-DIO-96
   short  *daqBuf;              // Pointer to DAQ buffer
   int    daqBufSize;           // Size of the DAQ buffer in samples

   const double sint = 0.01;    // Sample interval (sec)
// const int tcChan = 56;       // Pin number of time-code
   const int tcChan = 62;       // Pin number of AGC'd time-code
   const int Year = 1985;

/* Get name of fmt file
   ********************/
   if ( argc < 2 )
      strncpy( fname, "C:\\earthworm\\working-will\\Contrib\\Menlo\\src\\readfmt\\CNET85NO01_20100623_203417.fmt",
               sizeof(fname) );
   else
      strncpy( fname, argv[1], sizeof(fname) );
   printf( "fmt file: %s\n", fname );

/* Open the fmt file
   *****************/
   fpfmt = fopen( fname, "rb" );
   if ( fpfmt == NULL )
   {
      printf( "Error. Can't open file %s\n", fname );
      return 0;
   }

/* Get nport, nchan, and nscan from file header
   ********************************************/
   if ( fread( &nport, sizeof(long), 1, fpfmt ) < 1 )
   {
      printf( "Error reading nport from file header. Exiting.\n" );
      return 0;
   }
   printf( "nport: %d", nport );

   if ( fread( &nchan, sizeof(long), 1, fpfmt ) < 1 )
   {
      printf( "Error reading nchan from file header. Exiting.\n" );
      return 0;
   }
   printf( "   nchan: %d", nchan );

   if ( fread( &nscan, sizeof(long), 1, fpfmt ) < 1 )
   {
      printf( "Error reading nscan from file header. Exiting.\n" );
      return 0;
   }
   printf( "   nscan: %d\n", nscan );

/* Allocate buffer to contain DAQ data
   ***********************************/
   daqBufSize = nchan * nscan;
   daqBuf = (short *) calloc( daqBufSize, sizeof(short) );
   if ( daqBuf == NULL )
   {
      printf( "Error: Cannot allocate the DAQ buffer. Exiting.\n" );
      return -1;
   }

/* Get DIO port data from block header
   ***********************************/
   while ( 1 )
   {
      DIO dio;
      static long syncErrorPrev = 1;

      numRead = fread( pattern, sizeof(long), N_DIO_PORTS, fpfmt );
      if ( numRead < N_DIO_PORTS )
      {
         if ( feof(fpfmt) )
            printf( "End of fmt file encountered. Exiting.\n" );
         else
            printf( "Error reading DIO pattern from fmt file. Exiting.\n" );
         break;
      }

// Print for debugging
//    {
//       int port;
//       for ( port = 0; port < nport; port++ )
//          printf( " %02x", pattern[port] );
//       printf( "\n" );
//    }

/* Get scan count from block header
   ********************************/
      if ( fread( &scan, sizeof(long), 1, fpfmt ) < 1 )
      {
         printf( "Error reading scan count from fmt file. Exiting.\n" );
         return 0;
      }
//    printf( "  scan:%d\n", scan );

/* Get block of muxed DAQ data
   ***************************/
      numRead = fread( daqBuf, sizeof(short), daqBufSize, fpfmt );
      if ( numRead < daqBufSize )
      {
         printf( "Error reading DAQ buffer.\n" );
         printf( "numRead=%d  daqBufSize=%d\n", numRead, daqBufSize );
         printf( "Exiting.\n" );
         return 0;
      }

/* Decode data from the DIO
   ************************/
      DecodeDIO( pattern, &dio );

/* Print decoded DIO stuff.  footage and syncError values
   are likely ok because they do not come from the tape.
   DIO time is likely corrupt unless the TCT is synched.
   ******************************************************/
      printf( " %04x",     dio.footage );
      printf( "  err:%d",  dio.syncError );

      if ( dio.syncError == 0 )               // No synch error detected
      {
         printf( "  id:%x", dio.tapeid );
         printf( "  tcg:%03ld %02ld:%02ld:%02ld.%02ld", dio.day, dio.hour, dio.min, dio.sec, dio.hun );
      }

/* Print some IRIGE DAQ samples, for debugging
   *******************************************/
//    if ( dio.syncError == 0 )               // No synch error detected
//    {
//       int i;
//       short *sptr = daqBuf + tcChan;
//
//       for ( i = 0; i < nscan; i++ )
//       {
//          printf( "%5hd", *sptr );
//          if ( i % 10 == 0 ) printf( "\n" );
//          sptr += nchan;
//       }
//    }

/* The Irige function decodes the digitized IRIGE signal.
   Don't bother unless unless the TCT is synched,
   because IRIGE is likely corrupted.
   *****************************************************/
      if ( dio.syncError == 0 )             // No synch error detected
      {
         struct TIME_BUFF Tbuf;             // Irige()'s read-write structure
         static int       IrigeStatusPrev = TIME_UNKNOWN;
         int              IrigeStatus;

/* If the TCG error light is no longer lit (TCG synch is now ok),
   calculate IRIGE bias the next time the Irige function is called.
   Calling irige_init will cause the bias to be recalculated, and
   then the Irige function will then go into Searching mode.
   I'm not sure if reinitializing is helpful or not.
   ***************************************************************/
//       if ( syncErrorPrev == 1 )
//          irige_init( &Tbuf );

         IrigeStatus = Irige( &Tbuf, scan, nchan, nscan, daqBuf, tcChan, Year );

         printf( "  irg:" );
         if      ( IrigeStatus == TIME_NOSYNC )
            printf( "NoSync" );
         else if ( IrigeStatus == TIME_FLAT )
            printf( "Flat" );
         else if ( IrigeStatus == TIME_NOISE )
            printf( "Noisy" );
         else if ( IrigeStatus == TIME_WAIT )
            printf( "Searching" );

/* We got some good time code.  Estimate the difference
   between TCG time and software decoded IRIGE.
   Time from software decoded IRIGE is probably more accurate.
   **********************************************************/
         else if ( IrigeStatus == TIME_OK )
         {
            double tIrige = Tbuf.t - 11676096000.0;
            double tcgTime;   // Time from time code generator
            double tcgDelay;  // TCG is this many seconds behind IRIGE
            PrintGmtime( tIrige );
            tcgTime  = TcgToDouble( dio, Year );
            tcgDelay = tcgTime - tIrige - (nscan - 1)*sint;
            printf( "  tcgDelay:%.2lf", tcgDelay );
            if ( fabs(tcgDelay) > 1.0 )
               printf( "\nWARNING.  Large time delay." );
         }
         else
            printf( "Unknown return code from Irige(): %d\n", IrigeStatus );

/* The Irige function quit searching.
   Call irige_init to start searching again.
   Maybe the bias level changed in the next dub event.
   I'm not sure if reinitializing is helpful or not.
   **************************************************/
//       if ( IrigeStatusPrev == TIME_WAIT )
//          if ( IrigeStatus == TIME_NOSYNC ||
//               IrigeStatus == TIME_FLAT   ||
//               IrigeStatus == TIME_NOISE )
//          {
//             printf( "\nRestarting search mode." );
//             irige_init( &Tbuf );
//          }
         IrigeStatusPrev = IrigeStatus;
      }
      printf( "\n" );
      syncErrorPrev = dio.syncError;
   }

/* Close fmt file and exit
   ***********************/
   fclose( fpfmt );
   return 0;
}


void DecodeDIO( long pattern[], DIO *dioPtr )
{
   long   us, ts;
   long   um, tm;
   long   uh, th;
   long   ud, td, hd;
   long   tenths, hundredths;

   us = pattern[0] & 0xf;
   ts = (pattern[0] >> 4) & 0x7;
   dioPtr->sec = 10*ts + us;

   um = pattern[1] & 0xf;
   tm = (pattern[1] >> 4) & 0x7;
   dioPtr->min = 10*tm + um;

   uh = pattern[2] & 0xf;
   th = (pattern[2] >> 4) & 0x3;
   dioPtr->hour = 10*th + uh;

   ud = pattern[3] & 0xf;
   td = (pattern[3] >> 4) & 0xf;
   hd = pattern[4] & 0x3;
   dioPtr->day = 100*hd + 10*td + ud;

   dioPtr->syncError = pattern[5] & 0x1;

   dioPtr->footage = (pattern[7] << 8) & 0xff00;
   dioPtr->footage |= pattern[6] & 0xff;

   dioPtr->tapeid = pattern[8] & 0xf;

   hundredths = pattern[9] & 0xf;
   tenths = (pattern[9] >> 4) & 0xf;
   dioPtr->hun = 10*tenths + hundredths;
   return;
}


double TcgToDouble( DIO dio, int Year )
{
   struct tm stm;
   double tcgTime;
   time_t ltime;
   int    month, dayOfMonth;

/* Convert Day-of-Year to Month and Day-of-Month.
   *********************************************/
   YdayToMday( Year, dio.day, &month, &dayOfMonth );
// printf( "  month:%d  mday:%d", month, dayOfMonth );

/* Fill in the stm structure
   *************************/
   stm.tm_sec   = dio.sec;
   stm.tm_min   = dio.min;
   stm.tm_hour  = dio.hour;
   stm.tm_mday  = dayOfMonth;
   stm.tm_mon   = month - 1;
   stm.tm_year  = Year - 1900;
   stm.tm_isdst = 0;           // No daylight savings

// printf( "  %d/%d/%d %02d:%02d:%02d", stm.tm_year, stm.tm_mon, stm.tm_mday, stm.tm_hour, stm.tm_min, stm.tm_sec );
   ltime = timegm_ew( &stm );
// printf( "  ltime: %d", ltime );
   tcgTime = (double)ltime + (0.01 * dio.hun);
// printf( "  tcgTime:%.2lf", tcgTime );
   return tcgTime;
}


/* Convert Day-of-Year to Month and Day-of-Month
   *********************************************/

void YdayToMday( int Year, int yday, int *mon, int *mday )
{
   *mday = yday;
   *mon  = 1;                         // January = month 1

   if ( *mday <= 31 ) return;         // January
   (*mon)++, *mday-=31;
   if ( (Year % 4) == 0 )             // February
   {
      if ( *mday <= 29 ) return;
      *mday -= 29;
   }
   else
   {
      if ( *mday <= 28 ) return;
      *mday -= 28;
   }
   (*mon)++;
   if ( *mday <= 31 ) return;         // March
   (*mon)++, *mday-=31;
   if ( *mday <= 30 ) return;         // April
   (*mon)++, *mday-=30;
   if ( *mday <= 31 ) return;         // May
   (*mon)++, *mday-=31;
   if ( *mday <= 30 ) return;         // June
   (*mon)++, *mday-=30;
   if ( *mday <= 31 ) return;         // July
   (*mon)++, *mday-=31;
   if ( *mday <= 31 ) return;         // August
   (*mon)++, *mday-=31;
   if ( *mday <= 30 ) return;         // September
   (*mon)++, *mday-=30;
   if ( *mday <= 31 ) return;         // October
   (*mon)++, *mday-=31;
   if ( *mday <= 30 ) return;         // November
   (*mon)++, *mday-=30;
   if ( *mday <= 31 ) return;         // December
   printf( "Error.  Day of year too big: %d\n", yday );
   exit( 0 );
}
