#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <time_ew.h>
#include "getontime.h"

/* Declare functions
   *****************/
int    CompareStartTime( const void *a, const void *b );
time_t StringDateToTimet( char date_string[] );


void GetOnTime( char ssname[], RECORDER recorder[] )
{
   FILE *fp;
   char line[200];
   int  i, j, k;

   const int nrecorder = NRECORDER;
   const int ntrack    = NTRACK;
   const int nfreq     = NFREQ;

/* Initialize structures
   *********************/
   for ( i = 0; i < nrecorder; i++ )
   {
//    recorder[i].name = 'A' + i;
//    printf( " %c", recorder[i].name );
      for ( j = 0; j < ntrack; j++ )
      {
         for ( k = 0; k < nfreq; k++ )
         {
            recorder[i].track[j].freq[k].nscnl = 0;
            recorder[i].track[j].freq[k].scnlt = NULL;
         }
      }
   }

/* Open the csv file
   *****************/
   fp = fopen( ssname, "r" );
   if ( fp == NULL )
   {
      printf( "Error opening file %s\n", ssname );
      printf( "Exiting.\n" );
      exit( 0 );
   }

/* Get header line from csv file
   *****************************/
   if ( fgets(line, 200, fp) == NULL )
   {
      printf( "Can't get header line from csv file.\n" );
      printf( "Exiting.\n" );
      exit( 0 );
   }

/* Get all other lines from csv file
   *********************************/
   while ( fgets(line, 200, fp) != NULL )
   {
      struct TOKS
      {
        SCNL scnl;
        char usgs_comp[4];
        char start_date[18], end_date[18];
        char freq[5];
        char track[5];
        char recorder[2];
        char remarks[20];
      } toks;

      int  itrack;       // Index of track number
      int  ifreq;        // Index of frequency channel
      int  irecorder;    // Index of recorder code
      char *token;
      int  nscnl;
      SCNLT *scnlt;
      time_t start_time;
      time_t end_time;

//    printf( "%s", line );

/* Break up the line into tokens
   *****************************/
      token = strtok( line, "," );
      strcpy( toks.scnl.net, token );
      token = strtok( NULL, "," );
      strcpy( toks.scnl.sta, token );
      token = strtok( NULL, "," );
      strcpy( toks.scnl.loc, token );
      token = strtok( NULL, "," );
      strcpy( toks.usgs_comp, token );
      token = strtok( NULL, "," );
      strcpy( toks.scnl.comp, token );
      token = strtok( NULL, "," );
      strcpy( toks.start_date, token );
      token = strtok( NULL, "," );
      strcpy( toks.end_date, token );
      token = strtok( NULL, "," );
      strcpy( toks.freq, token );
      token = strtok( NULL, "," );
      strcpy( toks.track, token );
      token = strtok( NULL, "," );
      strcpy( toks.recorder, token );
      token = strtok( NULL, "\n" );
      strcpy( toks.remarks, token );

/* Reject lines without a valid track, frequency channel, or recorder.
   Frequency channels are from 0 to 8, but channel 0 isn't used.
   ******************************************************************/
      if ( toks.track[0] == '-' ) continue;   // Skip line. Track not entered in spreadsheet.

      if ( sscanf( toks.track, "%d", &itrack ) < 1 )
         { printf( "ERROR. Invalid track number: %s\n", toks.track ); break; }
      itrack--;                                          // Track numbers are from 1 to 14
      if ( (itrack < 0) || (itrack >= ntrack) )
         { printf( "ERROR. Track number is out of range: %5s\n", toks.track ); break; }

      if ( sscanf( toks.freq, "%d", &ifreq ) < 1 )
         { printf( "ERROR. Invalid frequency channel: %s\n", toks.freq ); break; }
      if ( (ifreq < 0) || (ifreq >= nfreq) )
         { printf( "ERROR. Frequency channel is out of range: %s\n", toks.freq ); break; }

      if ( strlen(toks.recorder) > 1 )
         { printf( "ERROR. More than one character in recorder code.\n" ); break; }
      irecorder = toks.recorder[0] - 'A';                 // Indexes are from 0 to 4
      if ( (irecorder < 0) || irecorder >= nrecorder )
         { printf( "ERROR. Recorder code not A-E: %c\n", toks.recorder[0] ); break; };

//    printf( "%s",  toks.scnl.net );
//    printf( " %s", toks.scnl.sta );
//    printf( " %s", toks.scnl.loc );
//    printf( " %s", toks.usgs_comp );
//    printf( " %s", toks.scnl.comp );
//    printf( " %s", toks.start_date );
//    printf( " %s", toks.end_date );
//    printf( " %s", toks.freq );
//    printf( " %s", toks.track );
//    printf( " %s", toks.recorder );
//    printf( " %s", toks.remarks );
//    printf( "\n" );

/* Convert start/end dates from strings to time_t
   **********************************************/
      start_time = StringDateToTimet( toks.start_date );
      end_time   = StringDateToTimet( toks.end_date );

/* Fill the scnlt structure
   ************************/
      scnlt = recorder[irecorder].track[itrack].freq[ifreq].scnlt;
      nscnl = recorder[irecorder].track[itrack].freq[ifreq].nscnl + 1;

      scnlt = (SCNLT *)realloc( scnlt, nscnl * sizeof(SCNLT) );
      if ( scnlt == NULL )
      {
         printf( "Error allocating new scnlt. Exiting.\n" );
         exit( 0 );
      }

      memcpy( &scnlt[nscnl-1].scnl, &toks.scnl, sizeof(SCNL) );
      strcpy( scnlt[nscnl-1].usgs_comp,  toks.usgs_comp );
      strcpy( scnlt[nscnl-1].start_date, toks.start_date );
      strcpy( scnlt[nscnl-1].end_date,   toks.end_date );
      strcpy( scnlt[nscnl-1].remarks,    toks.remarks );

      scnlt[nscnl-1].start_time = start_time;
      scnlt[nscnl-1].end_time   = end_time;

      recorder[irecorder].track[itrack].freq[ifreq].scnlt = scnlt;
      recorder[irecorder].track[itrack].freq[ifreq].nscnl = nscnl;
//    break;
   }
   fclose( fp );
   return;
}


time_t StringDateToTimet( char start_date[] )
{
   char datestr[18];
   char year[5], mon[3], mday[3], hour[3], min[3];
   struct tm tmstr;

   strcpy( datestr, start_date );
   strcpy( mon,  strtok(datestr, "/") );
   strcpy( mday, strtok(NULL, "/") );
   strcpy( year, strtok(NULL, " ") );
   strcpy( hour, strtok(NULL, ":") );
   strcpy( min,  strtok(NULL, "\0") );
   tmstr.tm_sec  = 0;
   tmstr.tm_min  = atoi( min );
   tmstr.tm_hour = atoi( hour );
   tmstr.tm_mday = atoi( mday );
   tmstr.tm_mon  = atoi( mon ) - 1;
   tmstr.tm_year = atoi( year ) - 1900;
   tmstr.tm_isdst = 0;
   return timegm_ew( &tmstr );
}


void SortByStartTime( RECORDER recorder[] )
{
   int  i, j, k;

   const int nrecorder = NRECORDER;
   const int ntrack    = NTRACK;
   const int nfreq     = NFREQ;

/* Loop through all recorders/tracks/frequencies
   *********************************************/
   for ( i = 0; i < nrecorder; i++ )
   {
      char rec = i + 'A';         // Recorder (A-E)

      for ( j = 0; j < ntrack; j++ )
      {
         int track = j + 1;       // Track number (1-14)

         for ( k = 0; k < nfreq; k++ )
         {
            int freq = k;         // Frequency index (0-8)
            SCNLT *scnlt = recorder[i].track[j].freq[k].scnlt;
            int   nscnl  = recorder[i].track[j].freq[k].nscnl;

            if ( nscnl > 1 )
               qsort( scnlt, nscnl, sizeof(SCNLT), CompareStartTime );
         }
      }
   }
   return;
}


/* Compare spreadsheet start times for qsort
   *****************************************/
int CompareStartTime( const void *a, const void *b )
{
   const SCNLT *sa = (const SCNLT *)a;
   const SCNLT *sb = (const SCNLT *)b;
   time_t at = sa->start_time;
   time_t bt = sb->start_time;
   return (int)(at - bt);
}


void PrintOnTime( RECORDER recorder[] )
{
   int  i, j, k, m;

   const int nrecorder = NRECORDER;
   const int ntrack    = NTRACK;
   const int nfreq     = NFREQ;

/* Loop through all recorders/tracks/frequencies
   *********************************************/
   for ( i = 0; i < nrecorder; i++ )
   {
      char rec = i + 'A';         // Recorder (A-E)

      for ( j = 0; j < ntrack; j++ )
      {
         int track = j + 1;       // Track number

         for ( k = 0; k < nfreq; k++ )
         {
            int freq = k;         // Frequency index (0-8)
            SCNLT *scnlt = recorder[i].track[j].freq[k].scnlt;
            int   nscnl  = recorder[i].track[j].freq[k].nscnl;

            if ( nscnl == 0 ) continue;
//          printf( "freq chan:%d  track:%d  recorder:%c", freq, track, rec );
//          printf( "\n" );

            for ( m = 0; m < nscnl; m++ )
            {
               time_t endtimeprev;
               printf( "%s",  scnlt[m].scnl.net );
               printf( ",%s", scnlt[m].scnl.sta );
               printf( ",%s", scnlt[m].scnl.loc );
               printf( ",%s", scnlt[m].usgs_comp );
               printf( ",%s", scnlt[m].scnl.comp );
               printf( ",%s", scnlt[m].start_date );
               printf( ",%s", scnlt[m].end_date );
               printf( ",%d", freq );
               printf( ",%d", track );
               printf( ",%c", rec );
               printf( ",%s", scnlt[m].remarks );

               if ( m > 0 )
               {
                  int gap = scnlt[m].start_time - endtimeprev;
                  printf( ",%d", gap );
                  if ( gap < 0 )
                     printf( ",overlap" );
               }
               endtimeprev = scnlt[m].end_time;

               printf( "\n" );
            }
         }
      }
   }
   return;
}
