#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "getontime.h"

/* Function declarations
   *********************/
void GetOnTime( char ssname[], RECORDER recorder[] );
void SortByStartTime( RECORDER recorder[] );
void PrintOnTime( RECORDER recorder[] );
void ReadDubCat( char dubcatname[], RECORDER recorder[] );


/* Main program starts here
   ************************/

int main( int argc, char *argv[] )
{
   char ssname[80]     = "ncsn20100624-fmt.csv";
   char dubcatname[80] = "dubcat.all";
   RECORDER *recorder;

/* Get name of spreadsheet file, in csv format
   *******************************************/
   if ( argc > 1 )
      strncpy( ssname, argv[1], 80 );

/* Allocate recorder structures.
   These will be filled from the spreadsheet.
   *****************************************/
   recorder = (RECORDER *)malloc( NRECORDER * sizeof(RECORDER) );
   if ( recorder == NULL )
   {
      printf( "malloc error for recorder struct array. Exiting.\n" );
      return 0;
   }

/* Fill in recorder struct array from spreadsheet.  No output.
   **********************************************************/
   GetOnTime( ssname, recorder );

/* Sort spreadsheet lines by start time.  No output.
   ************************************************/
   SortByStartTime( recorder );

/* Print the recorder struct array.
   Also, check for overlapping records in spreadsheet.
   May want to comment this line out.
   **************************************************/
// PrintOnTime( recorder );

/* Read the dubcat files and print overlapping spreadsheet
   records that contain dubbed events.
   *******************************************************/
   ReadDubCat( dubcatname, recorder );
   return 0;
}
