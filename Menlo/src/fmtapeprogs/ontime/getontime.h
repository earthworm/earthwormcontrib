#include <time.h>

#define NRECORDER  5
#define NTRACK    14
#define NFREQ      9

typedef struct
{
   char sta[6];
   char comp[4];
   char net[3];
   char loc[3];
} SCNL;

typedef struct
{
   SCNL scnl;
   char usgs_comp[4];
   char start_date[17];
   char end_date[17];
   time_t start_time;
   time_t end_time;
   char remarks[10];
} SCNLT;

typedef struct
{
   int   nscnl;     // Number of scnls in scnlt array
   SCNLT *scnlt;    // Array of start/end times for this recorder/track/freq
} FREQ;             // 680 to 3060

typedef struct
{
   FREQ freq[NFREQ];
} TRACK;            // 1 - 14

typedef struct
{
   TRACK track[NTRACK];
} RECORDER;         // A - E

