#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <time_ew.h>
#include "getontime.h"


void ReadDubCat( char dubcatname[], RECORDER recorder[] )
{
   FILE *fp;
   char line[200];

/* Open the dubcat file
   ********************/
   fp = fopen( dubcatname, "r" );
   if ( fp == NULL )
   {
      printf( "Error opening dubcat file %s\n", dubcatname );
      printf( "Exiting.\n" );
      exit( 0 );
   }

/* Get each event line from dubcat file
   ************************************/
   while ( fgets(line, 200, fp) != NULL )
   {
      const int nrecorder = NRECORDER;
      const int ntrack    = NTRACK;
      const int nfreq     = NFREQ;
      int  i, j, k, m;
      int  year1, mon1, day1, hour1, min1, sec1;  // Start dub time
      int  year2, mon2, day2, hour2, min2, sec2;  // Stop dub time
      int  ndropouts;                             // Number of dropouts in dub event
      char footage1[5], footage2[5];              // Start/end footage of dub event
      char tapename[8];                           // Name of dubbed tape
      char token[20];
      char rec;                                   // Recorder code (A-E)
      struct tm tmstr;
      time_t start_time;                          // Start time as integer
      time_t end_time;                            // End time as integer

      line[59] = 0;                               // Truncate the dub event lime

/* Decode start and stop dub time into integers
   ********************************************/
      strncpy( token, line,    2 ); token[2] = 0; year1 = atoi( token );
      strncpy( token, line+2,  2 ); token[2] = 0; mon1  = atoi( token );
      strncpy( token, line+4,  2 ); token[2] = 0; day1  = atoi( token );
      strncpy( token, line+7,  2 ); token[2] = 0; hour1 = atoi( token );
      strncpy( token, line+9,  2 ); token[2] = 0; min1  = atoi( token );
      strncpy( token, line+12, 2 ); token[2] = 0; sec1  = atoi( token );
      strncpy( token, line+24, 2 ); token[2] = 0; year2 = atoi( token );
      strncpy( token, line+26, 2 ); token[2] = 0; mon2  = atoi( token );
      strncpy( token, line+28, 2 ); token[2] = 0; day2  = atoi( token );
      strncpy( token, line+31, 2 ); token[2] = 0; hour2 = atoi( token );
      strncpy( token, line+33, 2 ); token[2] = 0; min2  = atoi( token );
      strncpy( token, line+36, 2 ); token[2] = 0; sec2  = atoi( token );

/* Uh oh.  In the dubcat, 62 dub events have stop time seconds = 60
   Fake it, for now.
   ****************************************************************/
      if ( sec2 == 60 ) sec2 = 59;

/* Make sure the dub start and stop times are valid
   ************************************************/
      if (year1 <74 || year1 >89)
         { printf("Invalid year1: %d  Exiting.\n", year1); exit(0); }
      if (mon1  <1  || mon1  >12)
         { printf("Invalid mon1: %d  Exiting.\n",  mon1 ); exit(0); }
      if (day1  <1)
         { printf("Invalid day1: %d  Exiting.\n",  day1 ); exit(0); }
      if ((mon1==1 || mon1==3 || mon1==5 || mon1==7 || mon1==8 || mon1==10 || mon1==12) && day1>31)
         { printf("Invalid day1: %d  Exiting.\n",  day1 ); exit(0); }
      if ((mon1==4 || mon1==6 || mon1==9 || mon1==11) && day1  >30)
         { printf("Invalid day1: %d  Exiting.\n",  day1 ); exit(0); }
      if (year1%4 ==0 && mon1==2 && day1 >29)
         { printf("Invalid day1: %d  Exiting.\n",  day1 ); exit(0); }
      if (year1%4 !=0 && mon1==2 && day1 >28)
         { printf("Invalid day1: %d  Exiting.\n",  day1 ); exit(0); }
      if (hour1 <0  || hour1 >23)
         { printf("Invalid hour1: %d  Exiting.\n", hour1); exit(0); }
      if (min1  <0  || min1  >59)
         { printf("Invalid min1: %d  Exiting.\n",  min1 ); exit(0); }
      if (sec1  <0  || sec1  >59)
         { printf("Invalid sec1: %d  Exiting.\n",  sec1 ); exit(0); }

      if (year2 <74 || year2 >89)
         { printf("Invalid year2: %d  Exiting.\n", year2); exit(0); }
      if (mon2  <1  || mon2  >12)
         { printf("Invalid mon2: %d  Exiting.\n",  mon2 ); exit(0); }
      if (day2  <1 )
         { printf("Invalid day2: %d  Exiting.\n",  day2 ); exit(0); }
      if ((mon2==1 || mon2==3 || mon2==5 || mon2==7 || mon2==8 || mon2==10 || mon2==12) && day2>31)
         { printf("Invalid day2: %d  Exiting.\n",  day2 ); exit(0); }
      if ((mon2==4 || mon2==6 || mon2==9 || mon2==11) && day2  >30)
         { printf("Invalid day2: %d  Exiting.\n",  day2 ); exit(0); }
      if (year2%4 ==0 && mon2==2 && day2 >29)
         { printf("Invalid day2: %d  Exiting.\n",  day2 ); exit(0); }
      if (year2%4 !=0 && mon2==2 && day2 >28)
         { printf("Invalid day2: %d  Exiting.\n",  day2 ); exit(0); }
      if (hour2 <0  || hour2 >23)
         { printf("Invalid hour2: %d  Exiting.\n", hour2); exit(0); }
      if (min2  <0  || min2  >59)
         { printf("Invalid min2: %d  Exiting.\n",  min2 ); exit(0); }
      if (sec2  <0  || sec2  >59)
         { printf("Invalid sec2: %d  Exiting.\n",  sec2 ); exit(0); }

/* Convert dub start and stop times to seconds since midnite Jan 1, 1970
   *********************************************************************/
      tmstr.tm_sec  = sec1;
      tmstr.tm_min  = min1;
      tmstr.tm_hour = hour1;
      tmstr.tm_mday = day1;
      tmstr.tm_mon  = mon1 - 1;
      tmstr.tm_year = year1;
      tmstr.tm_isdst = 0;
      start_time = timegm_ew( &tmstr );

      tmstr.tm_sec  = sec2;
      tmstr.tm_min  = min2;
      tmstr.tm_hour = hour2;
      tmstr.tm_mday = day2;
      tmstr.tm_mon  = mon2 - 1;
      tmstr.tm_year = year2;
      tmstr.tm_isdst = 0;
      end_time = timegm_ew( &tmstr );

/* Uh oh.  In the dubcat, 3 events have end_time <= start time.
   Ignore them, for now.
   ***********************************************************/
//    if ( (end_time - start_time) <= 0 )
//       printf( "ERROR: dub end_time <= dub start_time.\n" );

/* Get footage at start/end of dub event
   *************************************/
      strncpy( footage1, line + 16, 4 ); footage1[4] = 0;
      strncpy( footage2, line + 40, 4 ); footage2[4] = 0;

/* Get number of dropouts in dub event
   ***********************************/
      strncpy( token, line+46, 3 ); ndropouts = atoi( token );
      if ( ndropouts < 0 )
         { printf( "ERROR. Invalid number of dropouts: %d\n", ndropouts ); exit( 0 ); }

/* Get the recorder and make sure it is valid
   ******************************************/
      rec = line[50];
      if ( (rec < 'A') || (rec > 'E') )
         { printf( "ERROR. Invalid recorder: %c\n", rec ); exit( 0 ); }

/* Get name of dubbed tape
   ***********************/
      strncpy( tapename, line+55, 7 ); tapename[7] = 0;

/* Print the decoded dub event
   ***************************/
//    printf( "%02d%02d%02d %02d%02d %02d", year1, mon1, day1, hour1, min1, sec1 );
//    printf( " (%4s)", footage1 );
//    printf( " - %02d%02d%02d %02d%02d %02d", year2, mon2, day2, hour2, min2, sec2 );
//    printf( " (%4s)", footage2 );
//    printf( ":%3d", ndropouts );
//    printf( " %c", rec );
//    printf( "    %s", tapename );
//    printf( "\n" );

/* Look up recorder and dub time in Fred's spreadsheet
   ***************************************************/
      i = rec - 'A';

      for ( j = 0; j < ntrack; j++ )
      {
         int track = j + 1;       // Track number

         for ( k = 0; k < nfreq; k++ )
         {
            int nlines = 0;
            int freq = k;         // Frequency index (0-8)
            SCNLT *scnlt = recorder[i].track[j].freq[k].scnlt;
            int   nscnl  = recorder[i].track[j].freq[k].nscnl;

            if ( nscnl == 0 ) continue;

/* Count number of relevant spreadsheet lines
   ******************************************/
            for ( m = 0; m < nscnl; m++ )
               if ( (start_time >= scnlt[m].start_time) && (start_time <= scnlt[m].end_time) )
                  nlines++;

            if ( nlines > 1 )
            {
               printf( "\n%s\n", line );

               for ( m = 0; m < nscnl; m++ )
               {
                  if ( (start_time >= scnlt[m].start_time) && (start_time <= scnlt[m].end_time) )
                  {
                     printf( "%s",  scnlt[m].scnl.net );
                     printf( ",%s", scnlt[m].scnl.sta );
                     printf( ",%s", scnlt[m].scnl.loc );
                     printf( ",%s", scnlt[m].usgs_comp );
                     printf( ",%s", scnlt[m].scnl.comp );
                     printf( ",%s", scnlt[m].start_date );
                     printf( ",%s", scnlt[m].end_date );
                     printf( ",%d", freq );
                     printf( ",%d", track );
                     printf( ",%c", rec );
                     printf( ",%s", scnlt[m].remarks );
                     printf( "\n" );
                  }
               }
            }
         }
      }
//    break;
   }

   fclose( fp );
   return;
}
