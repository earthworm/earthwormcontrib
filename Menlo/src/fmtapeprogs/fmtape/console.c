
     /***************************************************************
      *                  Control the console display                *
      ***************************************************************/

#include <stdio.h>
#include <time.h>
#include <earthworm.h>

extern int    ExtTrig;             // Trigger flag, from config file
static HANDLE outHandle;           // The console file handle

void PrintGmtime( double, int );   // Function prototype


void SetCurPos( int x, int y )
{
   COORD     coord;

   coord.X = x;
   coord.Y = y;
   SetConsoleCursorPosition( outHandle, coord );
   return;
}


void InitCon( void )
{
   WORD   color;
   COORD  coord;
   DWORD  numWritten;

/* Get the console handle
   **********************/
   outHandle = GetStdHandle( STD_OUTPUT_HANDLE );

/* Set foreground and background colors
   ************************************/
   color = BACKGROUND_BLUE | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
   coord.X= coord.Y= 0;
   FillConsoleOutputAttribute( outHandle, color, 2000, coord, &numWritten );
   SetConsoleTextAttribute( outHandle, color );

/* Fill in the labels
   ******************/
   SetCurPos( 31, 1 );
   printf( "FM TAPE DIGITIZER" );

   SetCurPos( 4, 4 );
   if ( ExtTrig )
      printf( "Scans are externally triggered." );
   else
      printf( "Scans are triggered by DAQ clock." );

   SetCurPos( 4, 5 );
   printf( "Scan:" );

   SetCurPos( 4, 7 );
   printf( "TraceBuf Start Time:" );

   SetCurPos( 4, 10 );
   printf( "FMT File Name:" );

   SetCurPos( 4, 13 );
   printf( "Footage:" );

   SetCurPos( 0, 0 );
   return;
}
