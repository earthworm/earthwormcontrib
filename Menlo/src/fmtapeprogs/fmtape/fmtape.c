
  /*********************************************************************
   *                              fmtape.c                             *
   *                                                                   *
   *               Digitizing program for 1-inch FM tapes.             *
   *********************************************************************/

/* This program digitizes 128 channels at 16 times real time, equivalent to
   100 samples per second as originally recorded.  After each buffer of DAQ
   data is obtained, digital data is obtained from 12 8-bit DIO ports.  The
   DIO ports contain:
   1. Time from the TrueTime time-code generator, in day-of-year, hour,
      minute and second (no year).
   2. Synch-error bit, equivalent to the error light on the TCG,
   3. Tape footage
   4. Tape ID code.  These were originally labeled A, B, C, D, E.
   If the tape footage doesn't change, no data are output.  If the tape
   footage changes, the program generates Earthworm tracebuf messages,
   time-stamped by the PC clock.  Also, the program writes muxed data to
   disk files, known as "fmt files".  The fmt file contains a file header,
   followed by a sequence of muxed DAQ blocks.  The file header contains:

           bytes
           -----
   nports    4    Number of DIO ports.  Should always be 12.
   nchan     4    Number of DAQ channels.  Should always be 128.
   nscan     4    Number of scans per muxed data buffer, which
                  is the same as the Earthworm tracebuf msg size.

   The block header contains:

           bytes
           -----
   pattern  48    12 element array, containing data from the DIO ports,
   scan      4    scan number of DAQ data, from start of this program.

   The block header is followed by the muxed DAQ data, consisting of
   nchan x nscan 16-bit samples.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/timeb.h>
#include <ctype.h>
#include <conio.h>
#include <fcntl.h>
#include <sys\types.h>
#include <sys\stat.h>
#include <string.h>
#include <process.h>    /* for _getpid() */

#include <earthworm.h>
#include <transport.h>
#include <trace_buf.h>
#include "fmtape.h"
#include "irige.h"

#define N_DIO_PORTS 12

/* Functions prototypes
   ********************/
int  GetArgs( int, char **, char ** );
int  GetConfig( char * );
int  GetDAQ( short *, unsigned long * );
void Heartbeat( void );
void InitCon( void );
void InitDAQ( int );
void irige_init( struct TIME_BUFF * );
int  Irige( struct TIME_BUFF *, long, int, int, short *, int, int );
void LogAndReportError( int, char * );
void LogConfig( void );
void LogSta( int, SCNL * );
void PrintGmtime( double, int );
void LogitGmtime( double, int );
int  ReadSta( char *, unsigned char, int, SCNL * );
void ReportError( int, char * );
void SetCurPos( int, int );
void StopDAQ( void );
FILE *OpenFmt( int *fmtFileSize );
void CloseFmt( FILE *fpfmt, int fmtFileSize );
void InitDIOPorts( void );
void GetDIOPorts( long pattern[] );

/* Global configuration parameters
   *******************************/
extern unsigned char ModuleId;   // Data source id placed in the trace header
extern int    OnboardChans;      // The number of channels on the DAQ board
extern int    NumMuxBoards;      // Number of mux cards
extern double ChanRate;          // Rate in samples per second per channel
extern int    ChanMsgSize;       // Message size in samples per channel
extern int    ExtTrig;           // External trigger flag
extern double LowValue;          // The low hysteresis voltage for external triggering
extern double HighValue;         // The high hysteresis voltage for external triggering
extern long   OutKey;            // Key to ring where traces will live
extern SCNL   *ChanList;         // Array to fill with SCNL values
extern int    Nchan;             // Number of channels in ChanList
extern char   FmtDir[80];        // Directory to contain fmt files
extern int    MaxFileSize;       // In millions of bytes
extern char   FmtapeName[80];    // Name of FM tape, printed on tape box

/* Other global variables
   **********************/
SHM_INFO      OutRegion;         // Info structure for output region
pid_t         MyPid;             // process id, sent with heartbeat



int main( int argc, char *argv[] )
{
   FILE      *fpfmt;           // File pointer
   int       traceBufSize;     // Size of the trace buffer, in bytes
   short     *halfBuf,         // Pointer to half buffer of A/D data
             *traceDat;        // Where the data points are stored in the trace msg
   char      *traceBuf;        // Where the trace message is assembled
   unsigned  halfBufSize,      // Size of the half buffer in number of samples
             scan = 0;         // Scan number of first scan in a message
   unsigned  char InstId;      // Installation id placed in the trace header
   MSG_LOGO  logo;             // Logo of message to put out
   TRACE2_HEADER *traceHead;   // Where the trace header is stored
   struct TIME_BUFF Tbuf;      // Irige()'s read-write structure
   int       fmtFileSize;      // In bytes
   int       maxFileSize;      // In bytes
   long      footagePrev;      // Previous footage, from DIO ports 6-7
   long      pattern[N_DIO_PORTS]; // Data from PCI-DIO-96
   const long nports = N_DIO_PORTS;

/* Get command line arguments
   **************************/
   if ( argc < 2 )
   {
      printf( "Usage: fmtape <config file>\n" );
      return -1;
   }

/* Initialize name of log-file & open it
   *************************************/
   logit_init( argv[1], 0, 256, 1 );

/* Read configuration parameters
   *****************************/
   if ( GetConfig( argv[1] ) < 0 )
   {
      logit( "", "Error reading configuration file. Exiting.\n" );
      return -1;
   }

/* Log the configuration file
   **************************/
   LogConfig();

/* Get our Process ID for restart purposes
   ***************************************/
   MyPid = _getpid();
   if( MyPid == -1 )
   {
      logit("", "Error. Cannot get PID. Exiting.\n" );
      return -1;
   }

/* Set up the logo of outgoing waveform messages
   *********************************************/
   if ( GetLocalInst( &InstId ) < 0 )
   {
      logit( "", "Error getting local installation id. Exiting.\n" );
      return -1;
   }
   logit( "t", "Local InstId: %u\n", InstId );

   logo.instid = InstId;
   logo.mod    = ModuleId;
   if ( GetType( "TYPE_TRACEBUF2", &logo.type ) == -1 )
   {
      logit( "t", "Error getting type code TYPE_TRACEBUF2. Exiting.\n" );
      return -1;
   }

/* Make sure that trigger parameters LowValue and HighValue are ok
   ***************************************************************/
   if ( ExtTrig )
   {
      if ( HighValue - LowValue < 0.1)
      {
         logit( "et", "Error: HighValue - LowValue < 0.1 volts. Exiting.\n" );
         return -1;
      }
      if ( LowValue < -10.0 )
      {
         logit( "et", "Error: LowValue < -10.0 volts. Exiting.\n" );
         return -1;
      }
      if ( HighValue > 10.0 )
      {
         logit( "et", "Error: HighValue > 10.0 volts. Exiting.\n" );
         return -1;
      }
   }

/* Allocate some array space
   *************************/
   halfBufSize = (unsigned)(ChanMsgSize * Nchan);
   logit( "t", "Half buffer size: %u samples\n", halfBufSize );

   halfBuf = (short *) calloc( halfBufSize, sizeof(short) );
   if ( halfBuf == NULL )
   {
      logit( "t", "Error: Cannot allocate the A/D buffer. Exiting.\n" );
      return -1;
   }
   traceBufSize = sizeof(TRACE2_HEADER) + (ChanMsgSize * sizeof(short));
   logit( "t", "Trace buffer size: %d bytes\n", traceBufSize );
   traceBuf = (char *) malloc( traceBufSize );
   if ( traceBuf == NULL )
   {
      logit( "t", "Error: Cannot allocate the trace buffer. Exiting.\n" );
      return -1;
   }
   traceHead = (TRACE2_HEADER *) &traceBuf[0];
   traceDat  = (short *) &traceBuf[sizeof(TRACE2_HEADER)];

/* Attach to existing transport ring and send first heartbeat
   **********************************************************/
   tport_attach( &OutRegion, OutKey );
   logit( "t", "Attached to transport ring: %d\n", OutKey );
   Heartbeat();

/* Initialize the console display
   ******************************/
   InitCon();

/* Open fmt file for output and set file size to zero.
   Write nports, Nchan, ChanMsgSize to the fmt file.
   **************************************************/
   fpfmt = OpenFmt( &fmtFileSize );
   if ( fpfmt == NULL )
   {
      logit( "et", "Error opening fmt file for output. Exiting.\n" );
      return 0;
   }
   maxFileSize = 1000000 * MaxFileSize;

/* Initialize the National Instruments DAQ board, PCI-MIO-16E-4
   ************************************************************/
   InitDAQ( ExtTrig );

/* Initialize the DIO ports on the PCI-DIO-96.
   Decode initial footage from DIO ports 6 and 7.
   *********************************************/
   InitDIOPorts();
   GetDIOPorts( pattern );
   footagePrev = (pattern[7] << 8) & 0xff00;
   footagePrev |= pattern[6] & 0xff;
   SetCurPos( 13, 13 );
   printf( "%04x  %5d", footagePrev, footagePrev );

   /************************* The main program loop   *********************/

   while ( tport_getflag( &OutRegion ) != TERMINATE  &&
           tport_getflag( &OutRegion ) != MyPid         )
   {
      int           i, j;             // Loop indexes
      int           port;             // DIO port number
      int           numWritten;       // fwrite returns this
      int           rc;               // Function return code
      short         *tracePtr;        // Pointer into traceBuf for demuxing
      short         *halfPtr;         // Pointer into halfBuf for demuxing
      unsigned long ptsTfr;           // Number of points transferred to AdDat
      struct _timeb timebuffer;
      double        tbufTime;         // Tracebuf message time
      int           IrigeStatus;
      static int    firstTbuf = 1;
      const int     DaqDataReady = 0;
      long          footage;              // Footage, from DIO ports 6-7

   /* Beat the heart
      **************/
      Heartbeat();

   /* Get a half buffer of data from the DAQ system.
      GetDAQ returns 0 if data are available.
      Check DAQ data ready every sleepLen msec.
      sleepLen is 0.1 of the message length, in msec.
      ***********************************************/
      if ( GetDAQ( halfBuf, &ptsTfr ) != DaqDataReady )
      {
         int sleepLen = (int)(100. * ChanMsgSize / ChanRate + 0.5);
         sleep_ew( sleepLen );
         continue;
      }

   /* Update scan count and write to console window
      *********************************************/
      scan += (unsigned)ChanMsgSize;
      SetCurPos( 10, 5 );
      printf( "%u", scan );

   /* Read the 12 8-bit ports from the PCI-DIO-96 board.
      Decode the footage.  If the footage doesn't change,
      don't write the current DAQ and DIO data to the fmt file.
      ********************************************************/
      GetDIOPorts( pattern );
//    for ( port = 0; port < N_DIO_PORTS; port++ )
//       logit( "", "port %2d  %08x\n", port, pattern[port] );
      footage = (pattern[7] << 8) & 0xff00;
      footage |= pattern[6] & 0xff;
      if ( footage == footagePrev ) continue;
      footagePrev = footage;
      SetCurPos( 13, 13 );
      printf( "%04x  %5d", footagePrev, footagePrev );

   /* Write the DIO ports to the fmt file
      ***********************************/
      numWritten = fwrite( pattern, sizeof(long), N_DIO_PORTS, fpfmt );
      if ( numWritten < N_DIO_PORTS )
      {
         logit( "et", "Error writing DIO pattern to fmt file. Exiting.\n" );
         break;
      }
//    logit( "", "pattern nbytes: %d\n", numWritten * sizeof(long) );
      fmtFileSize += numWritten * sizeof(long);   // In bytes

   /* Write scan count to fmt file
      ****************************/
      if ( fwrite( &scan, sizeof(unsigned), 1, fpfmt ) < 1 )
      {
         logit( "et", "Error writing scan to fmt file. Exiting.\n" );
         break;
      }
//    logit( "", "scan: %d  nbytes: %d\n", scan, sizeof(unsigned) );
      fmtFileSize += sizeof(unsigned);

   /* Write the half buffer to the fmt file.
      halfBufSize = size of half buffer in number of samples.
      ******************************************************/
      numWritten = fwrite( halfBuf, sizeof(short), halfBufSize, fpfmt );
      if ( numWritten < halfBufSize )
      {
         logit( "et", "Error writing DAQ samples to fmt file. Exiting.\n" );
         break;
      }
      fmtFileSize += numWritten * sizeof(short);   // In bytes
      if ( fmtFileSize >= maxFileSize )
      {
         CloseFmt( fpfmt, fmtFileSize );
         fpfmt = OpenFmt( &fmtFileSize );
         if ( fpfmt == NULL )
         {
            logit( "et", "Error opening fmt file for output. Exiting.\n" );
            break;
         }
      }

   /* From here on down, construct Earthworm tracebuf
      messages and write them to the transport ring
      ***********************************************/

   /* Get first tracebuf message time from system clock.
      After that, add size of previous message.
      Times are in seconds since midnight 1/1/1970
      *************************************************/
      if ( firstTbuf )
      {
         _ftime( &timebuffer );
         tbufTime = (double)timebuffer.time +
                   ((double)timebuffer.millitm)/1000.;
         firstTbuf = 0;
      }
      else
         tbufTime += ChanMsgSize / ChanRate;
      SetCurPos( 4, 8 );
      PrintGmtime( tbufTime, 2 );

   /* Loop through the tracebuf messages to be sent out
      *************************************************/
      for ( i = 0; i < Nchan; i++ )
      {

      /* Do not send tracebuf messages for channels
         not specified in the config file
         ******************************************/
         if ( strlen( ChanList[i].sta ) == 0 ) continue;

      /* Fill the trace buffer header
         ****************************/
         traceHead->nsamp      = ChanMsgSize;             // Number of samples in message
         traceHead->samprate   = ChanRate;                // Sample rate; nominal
         traceHead->version[0] = TRACE2_VERSION0;         // Header version number
         traceHead->version[1] = TRACE2_VERSION1;         // Header version number
         traceHead->quality[0] = '\0';                    // One bit per condition
         traceHead->quality[1] = '\0';                    // One bit per condition
         traceHead->pinno      = i;                       // Pin number
         traceHead->starttime  = tbufTime;
         traceHead->endtime    = traceHead->starttime + (ChanMsgSize - 1)/ChanRate;

         strcpy( traceHead->datatype, "i2" );             // Data format code
         strcpy( traceHead->sta,  ChanList[i].sta );      // Site name
         strcpy( traceHead->net,  ChanList[i].net );      // Network name
         strcpy( traceHead->chan, ChanList[i].comp );     // Component/channel code
         strcpy( traceHead->loc,  ChanList[i].loc );      // Location code

      /* Transfer ChanMsgSize samples from halfBuf to traceBuf
         *****************************************************/
         tracePtr = &traceDat[0];
         halfPtr  = &halfBuf[i];
         for ( j = 0; j < ChanMsgSize; j++ )
         {
            *tracePtr = *halfPtr;
            tracePtr++;
            halfPtr += Nchan;
         }

      /* Send message to transport ring
         ******************************/
         rc = tport_putmsg( &OutRegion, &logo, traceBufSize, traceBuf );

         SetCurPos( 4, 22 );
         if ( rc == PUT_TOOBIG )
                 printf( "Trace message for channel %d too big\n", i );

         if ( rc == PUT_NOTRACK )
                 printf( "Tracking error while sending channel %d\n", i );
      }
   }

/* Clean up and exit program
   *************************/
   StopDAQ();
   free( halfBuf );
   free( ChanList );
   CloseFmt( fpfmt, fmtFileSize );
   logit( "t", "Fmtape exiting.\n" );
   return 0;
}



   /******************************************************
    *                      OpenFmt()                     *
    *  Open fmt file for output.                         *
    *  Construct file name from FM tape name and the     *
    *  current date/time.                                *
    *  Write nports, Nchan, ChanMsgSize to the fmt file. *
    ******************************************************/

FILE *OpenFmt( int *fmtFileSize )
{
   FILE   *fpfmt;
   time_t ltime;
   struct tm *tmt;
   char   fname[80];
   int    fileSize = 0;
   const  long nports = N_DIO_PORTS;

/* Open the fmt file
   *****************/
   time( &ltime );            // Get current time
   tmt = gmtime( &ltime );    // Convert to a struct

   sprintf( fname, "%s\\%s_%4d%02d%02d_%02d%02d%02d.fmt",
      FmtDir, FmtapeName,
      tmt->tm_year + 1900, tmt->tm_mon + 1, tmt->tm_mday,
      tmt->tm_hour, tmt->tm_min, tmt->tm_sec );

   SetCurPos( 4, 11 );
   printf( "%s", fname );

   logit( "t", "Opening new fmt file: %s\n", fname );
   fpfmt = fopen( fname, "wb" );

/* Write nports, Nchan, ChanMsgSize to the fmt file.
   ************************************************/
   if ( fpfmt != NULL )
   {
      if ( fwrite( &nports, sizeof(long), 1, fpfmt ) < 1 )
      {
         logit( "et", "Error writing nports to fmt file. Exiting.\n" );
         exit(0);
      }
//    logit( "", "nports: %d  nbytes: %d\n", nports, sizeof(long) );
      fileSize += sizeof(long);
      if ( fwrite( &Nchan, sizeof(int), 1, fpfmt ) < 1 )
      {
         logit( "et", "Error writing Nchan to fmt file. Exiting.\n" );
         exit(0);
      }
//    logit( "", "Nchan: %d  nbytes: %d\n", Nchan, sizeof(int) );
      fileSize += sizeof(int);
      if ( fwrite( &ChanMsgSize, sizeof(int), 1, fpfmt ) < 1 )
      {
         logit( "et", "Error writing ChanMsgSize to fmt file. Exiting.\n" );
         exit(0);
      }
//    logit( "", "ChanMsgSize: %d  nbytes: %d\n", ChanMsgSize, sizeof(int) );
      fileSize += sizeof(int);
   }
   *fmtFileSize = fileSize;
   return fpfmt;
}



   /*****************************************************
    *                     CloseFmt()                    *
    *  Close fmt file for output.                       *
    *****************************************************/

void CloseFmt( FILE *fpfmt, int fmtFileSize )
{
   logit( "t", "Closing fmt file.  Size: %d\n", fmtFileSize );
   fclose( fpfmt );
   return;
}
