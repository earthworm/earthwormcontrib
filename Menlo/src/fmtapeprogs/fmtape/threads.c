/*
 *   THIS FILE IS UNDER RCS - DO NOT MODIFY UNLESS YOU HAVE
 *   CHECKED IT OUT USING THE COMMAND CHECKOUT.
 *
 *    $Id: threads.c 476 2009-08-03 22:37:53Z kohler $
 *
 *    Revision history:
 *     $Log$
 *     Revision 1.1  2009/08/03 22:37:53  kohler
 *     This is an Earthworm module to digitize FM tapes and write to disk.
 *     First version.
 *
 *     Revision 1.1  2000/02/14 16:00:43  lucky
 *     Initial revision
 *
 *
 */

   /******************************************************************
    *              Dummy thread functions for adsend.                *
    *  adsend is single-threaded so it must link to these functions. *
    ******************************************************************/


int _beginthread()
{
   return 0;
}


void _endthread()
{
   return;
}
