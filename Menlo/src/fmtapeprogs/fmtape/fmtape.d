#
#                    CONFIGURATION FILE FOR FMTAPE
#                    -----------------------------
#
#  If there are no mux boards in the system, OnboardChans (16) channels are
#  digitized.  If there are mux boards in the system,
#  (4 * OnboardChans * NumMuxBoards) channels are digitized.
#  The station file must contain an entry for each channel digitized.
#
#  The names of output files include the FM tape name (FmtapeName), specified
#  below.  Be sure to change FmtapeName each time a new tape is played.
#
#
ModuleId         MOD_FMTAPE     # Module id of this instance of fmtape
OutRing          SCNL_RING      # Transport ring to write waveforms to
HeartbeatInt     15             # Heartbeat interval in seconds
#
#
#                        CHANNEL CONFIGURATION
OnboardChans     16             # Number of channels on the DAQ card
NumMuxBoards     2              # Number of 64-channel mux's (0,1,2, or 4)
ChanRate         1600.0         # Sampling rate in samples/second per channel
ChanMsgSize      400            # TraceBuf message size, in samples
Gain             2              # Channel gains (-1: +/-10V)(1: +/-5V)(2: +/-2.5V)
SampRate         250000.0       # Max = 250000. for the PC-MIO-16E-4
#
#
#                           OUTPUT FILES
FmtDir    C:\earthworm\run\fmt  # Directory to contain fmt files
MaxFileSize      500            # In millions of bytes
FmtapeName       CNETMA8105     # From label on FM tape
#
#
#                         EXTERNAL TRIGGERING
# This program uses "High-Hysteresis Analog Triggering Mode".
# Triggering occurs when the trigger voltage becomes greater than HighValue.
# Detriggering occurs when the trigger voltage becomes less than LowValue.
# HighValue minus LowValue must be >= 0.1 volt.
# For more information on triggering, see the National Instruments "PCI-MIO
# E Series User Manual", Jan 1997 edition, chapter 3.
#
ExtTrig          1              # 0 for internal triggering; 1 for external
LowValue         2.75           # In volts (-10V to 10V) (Highvaluemust be < HighValue)
HighValue        3.25           # In volts (-10V to 10V) (must be > LowValue)
#
#
#                   SCN VALUES FOR EACH DAQ CHANNEL
# Chan lines must follow the Channel Configuration lines in this file.
# Unused channels may be omitted from the list.  Pin numbers are optional.
# If a pin number is not specified for a channel, the pin number is set to
# the DAQ channel number.
#
#      DAQ
#     Chan    Sta/Comp/Net/Loc
#    -----    ----------------
 Chan    0    000   EHZ NC --
 Chan    1    001   EHZ NC --
 Chan    2    002   EHZ NC --
 Chan    3    003   EHZ NC --
 Chan    4    004   EHZ NC --
 Chan    5    005   EHZ NC --
 Chan    6    006   EHZ NC --
 Chan    7    007   EHZ NC --
 Chan    8    008   EHZ NC --
 Chan    9    009   EHZ NC --
 Chan   10    010   EHZ NC --
 Chan   11    011   EHZ NC --
 Chan   12    012   EHZ NC --
 Chan   13    013   EHZ NC --
 Chan   14    014   EHZ NC --
 Chan   15    015   EHZ NC --
 Chan   16    016   EHZ NC --
 Chan   17    017   EHZ NC --
 Chan   18    018   EHZ NC --
 Chan   19    019   EHZ NC --
 Chan   20    020   EHZ NC --
 Chan   21    021   EHZ NC --
 Chan   22    022   EHZ NC --
 Chan   23    023   EHZ NC --
 Chan   24    024   EHZ NC --
 Chan   25    025   EHZ NC --
 Chan   26    026   EHZ NC --
 Chan   27    027   EHZ NC --
 Chan   28    028   EHZ NC --
 Chan   29    029   EHZ NC --
 Chan   30    030   EHZ NC --
 Chan   31    031   EHZ NC --
 Chan   32    032   EHZ NC --
 Chan   33    033   EHZ NC --
 Chan   34    034   EHZ NC --
 Chan   35    035   EHZ NC --
 Chan   36    036   EHZ NC --
 Chan   37    037   EHZ NC --
 Chan   38    038   EHZ NC --
 Chan   39    039   EHZ NC --
 Chan   40    040   EHZ NC --
 Chan   41    041   EHZ NC --
 Chan   42    042   EHZ NC --
 Chan   43    043   EHZ NC --
 Chan   44    044   EHZ NC --
 Chan   45    045   EHZ NC --
 Chan   46    046   EHZ NC --
 Chan   47    047   EHZ NC --
 Chan   48    048   EHZ NC --
 Chan   49    049   EHZ NC --
 Chan   50    050   EHZ NC --
 Chan   51    051   EHZ NC --
 Chan   52    052   EHZ NC --
 Chan   53    053   EHZ NC --
 Chan   54    054   EHZ NC --
 Chan   55    055   EHZ NC --
 Chan   56    056   EHZ NC --
 Chan   57    057   EHZ NC --
 Chan   58    058   EHZ NC --
 Chan   59    059   EHZ NC --
 Chan   60    060   EHZ NC --
 Chan   61    061   EHZ NC --
 Chan   62    062   EHZ NC --
 Chan   63    063   EHZ NC --
 Chan   64    064   EHZ NC --
 Chan   65    065   EHZ NC --
 Chan   66    066   EHZ NC --
 Chan   67    067   EHZ NC --
 Chan   68    068   EHZ NC --
 Chan   69    069   EHZ NC --
 Chan   70    070   EHZ NC --
 Chan   71    071   EHZ NC --
 Chan   72    072   EHZ NC --
 Chan   73    073   EHZ NC --
 Chan   74    074   EHZ NC --
 Chan   75    075   EHZ NC --
 Chan   76    076   EHZ NC --
 Chan   77    077   EHZ NC --
 Chan   78    078   EHZ NC --
 Chan   79    079   EHZ NC --
 Chan   80    080   EHZ NC --
 Chan   81    081   EHZ NC --
 Chan   82    082   EHZ NC --
 Chan   83    083   EHZ NC --
 Chan   84    084   EHZ NC --
 Chan   85    085   EHZ NC --
 Chan   86    086   EHZ NC --
 Chan   87    087   EHZ NC --
 Chan   88    088   EHZ NC --
 Chan   89    089   EHZ NC --
 Chan   90    090   EHZ NC --
 Chan   91    091   EHZ NC --
 Chan   92    092   EHZ NC --
 Chan   93    093   EHZ NC --
 Chan   94    094   EHZ NC --
 Chan   95    095   EHZ NC --
 Chan   96    096   EHZ NC --
 Chan   97    097   EHZ NC --
 Chan   98    098   EHZ NC --
 Chan   99    099   EHZ NC --
 Chan  100    100   EHZ NC --
 Chan  101    101   EHZ NC --
 Chan  102    102   EHZ NC --
 Chan  103    103   EHZ NC --
 Chan  104    104   EHZ NC --
 Chan  105    105   EHZ NC --
 Chan  106    106   EHZ NC --
 Chan  107    107   EHZ NC --
 Chan  108    108   EHZ NC --
 Chan  109    109   EHZ NC --
 Chan  110    110   EHZ NC --
 Chan  111    111   EHZ NC --
 Chan  112    112   EHZ NC --
 Chan  113    113   EHZ NC --
 Chan  114    114   EHZ NC --
 Chan  115    115   EHZ NC --
 Chan  116    116   EHZ NC --
 Chan  117    117   EHZ NC --
 Chan  118    118   EHZ NC --
 Chan  119    119   EHZ NC --
 Chan  120    120   EHZ NC --
 Chan  121    121   EHZ NC --
 Chan  122    122   EHZ NC --
 Chan  123    123   EHZ NC --
 Chan  124    124   EHZ NC --
 Chan  125    125   EHZ NC --
 Chan  126    126   EHZ NC --
 Chan  127    127   EHZ NC --
