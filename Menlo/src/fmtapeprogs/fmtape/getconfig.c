
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <kom.h>
#include <earthworm.h>
#include "fmtape.h"


/* Function declaration
   ********************/
int IsComment( char [] );    // Defined below

/* The configuration file parameters
   *********************************/
unsigned char ModuleId;      // Module id of this program
int      OnboardChans;       // The number of channels on the DAQ board
int      NumMuxBoards;       // Number of mux cards
double   ChanRate;           // Rate in samples per second per channel
int      ChanMsgSize;        // Message size in samples per channel
double   SampRate=250000.0;  // Sample rate per scan (in samp/s)
int      Gain;               // Gain of amp in front of A/D converter.
int      ExtTrig;            // External trigger flag
long     OutKey;             // Key to ring where traces will live
int      HeartbeatInt;       // Heartbeat interval in seconds
double   LowValue;           // The low hysteresis voltage for external triggering
double   HighValue;          // The high hysteresis voltage for external triggering
SCNL     *ChanList;          // Array to fill with SCNL values
int      Nchan;              // Number of channels (64 * (# of mux's))
char     FmtDir[80];         // Directory to contain fmt files
int      MaxFileSize;        // In millions of bytes
char     FmtapeName[80];     // Name of FM tape, printed on tape box


     /***************************************************************
      *                          GetConfig()                        *
      *         Processes command file using kom.c functions.       *
      *           Returns -1 if any errors are encountered.         *
      ***************************************************************/

#define NCOMMAND 14             // Number of mandatory commands in the config file

int GetConfig( char *configfile )
{
   const int ncommand = NCOMMAND;

   char     init[NCOMMAND];     /* Flags, one for each command */
   int      nmiss;              /* Number of commands that were missed */
   int      nfiles;
   int      i;
   int      nChanLines = 0;     /* number of 'chan' specifiers. Must == Nchan */

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
      logit("", "fmtape: Error opening configuration file <%s>\n", configfile );
      return -1;
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          /* Get the first token from line */

         if ( !com ) continue;             /* Ignore blank lines */
         if ( com[0] == '#' ) continue;    /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               logit("", "fmtape: Error opening command file <%s>.\n", &com[1] );
               return -1;
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "ModuleId" ) )
         {
            if ( str = k_str() )
            {
               if ( GetModId(str, &ModuleId) == -1 )
               {
                  logit("", "fmtape: Invalid ModuleId <%s>. Exiting.\n", str );
                  return -1;
               }
            }
            init[0] = 1;
         }

         else if ( k_its( "OnboardChans" ) )
         {
            OnboardChans = k_int();
            init[1] = 1;
         }

         else if ( k_its( "NumMuxBoards" ) )
         {
            NumMuxBoards = k_int();
            init[2] = 1;
         }

         else if ( k_its( "ChanRate" ) )
         {
            ChanRate = k_val();
            init[3] = 1;
         }

         else if ( k_its( "ChanMsgSize" ) )
         {
            ChanMsgSize = k_int();
            init[4] = 1;
         }

         else if ( k_its( "SampRate" ) )
         {
            SampRate = k_val();
         }

         else if ( k_its( "Gain" ) )
         {
            Gain = k_int();
            init[5] = 1;
         }

         else if ( k_its( "ExtTrig" ) )
         {
            ExtTrig = k_int();
            init[6] = 1;
         }

         else if ( k_its( "OutRing" ) )
         {
            if ( str = k_str() )
            {
               if ( (OutKey = GetKey(str)) == -1 )
               {
                  logit("", "fmtape: Invalid OutRing <%s>. Exiting.\n", str );
                  return -1;
               }
            }
            init[7] = 1;
         }

         else if ( k_its( "HeartbeatInt" ) )
         {
            HeartbeatInt = k_int();
            init[8] = 1;
         }

         else if ( k_its( "LowValue" ) )
         {
            LowValue = k_val();
            init[9] = 1;
         }

         else if ( k_its( "HighValue" ) )
         {
            HighValue = k_val();
            init[10] = 1;
         }

         else if ( k_its( "FmtDir" ) )
         {
            if ( str = k_str() )
               strcpy( FmtDir, str );
            init[11] = 1;
         }

         else if ( k_its( "MaxFileSize" ) )
         {
            MaxFileSize = k_int();
            init[12] = 1;
         }

         else if ( k_its( "FmtapeName" ) )
         {
            if ( str = k_str() )
               strcpy( FmtapeName, str );
            init[13] = 1;
         }

/* Get the channel list
   ********************/
         else if ( k_its( "Chan" ) )                // Scnl value for each channel
         {
            static int first = 1;
            int        chan;
            int        rc;                          // kom function return code

            if ( first )                            // First time a Chan line was found
            {
               if ( init[1]==0 || init[2]==0)
               {
                  logit("", "Error. In the config file, the NumMuxBoards and OnboardChans\n" );
                  logit("", "       specification must appear before any Chan lines.\n" );
                  return -1;
               }
               Nchan    = (NumMuxBoards == 0) ? OnboardChans : (4 * OnboardChans * NumMuxBoards );
               ChanList = (SCNL *) calloc( Nchan, sizeof(SCNL) );
               if ( ChanList == NULL )
               {
                  logit("", "Error. Cannot allocate the channel list.\n" );
                  return -1;
               }
               first = 0;
            }

            chan = k_int();                          // Get channel number
            if ( chan>=Nchan || chan<0 )
            {
               logit("", "Error. Bad channel number (%d) in config file.\n", chan );
               return -1;
            }

            strcpy( ChanList[chan].sta,  k_str() );  // Save Scnl value in chan list
            strcpy( ChanList[chan].comp, k_str() );
            strcpy( ChanList[chan].net,  k_str() );
            strcpy( ChanList[chan].loc,  k_str() );
            nChanLines++;
         }

/* An unknown parameter was encountered
   ************************************/
         else
         {
            logit("", "fmtape: <%s> unknown parameter in <%s>\n", com, configfile );
            return -1;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            logit("", "fmtape: Bad <%s> command in <%s>.\n", com, configfile );
            return -1;
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss > 0 )
   {
      logit("", "fmtape: ERROR, no " );
      if ( !init[0]  ) logit("", "<ModuleId> " );
      if ( !init[1]  ) logit("", "<OnboardChans> " );
      if ( !init[2]  ) logit("", "<NumMuxBoards> " );
      if ( !init[3]  ) logit("", "<ChanRate> " );
      if ( !init[4]  ) logit("", "<ChanMsgSize> " );
      if ( !init[5]  ) logit("", "<Gain> " );
      if ( !init[6]  ) logit("", "<ExtTrig> " );
      if ( !init[7]  ) logit("", "<OutRing> " );
      if ( !init[8]  ) logit("", "<HeartbeatInt> " );
      if ( !init[9]  ) logit("", "<LowValue> " );
      if ( !init[10] ) logit("", "<HighValue> " );
      if ( !init[11] ) logit("", "<FmtDir> " );
      if ( !init[12] ) logit("", "<MaxFileSize> " );
      if ( !init[13] ) logit("", "<FmtapeName> " );
      logit("", "command(s) in <%s>.\n", configfile );
      return -1;
   }

   return 0;
}


 /***********************************************************************
  *                              LogConfig()                            *
  *                                                                     *
  *                   Log the configuration parameters                  *
  ***********************************************************************/

void LogConfig( void )
{
   int i;

   logit( "", "ModuleId:             %8u\n",    ModuleId );
   logit( "", "OnboardChans:         %8d\n",    OnboardChans );
   logit( "", "NumMuxBoards:         %8d\n",    NumMuxBoards );
   logit( "", "Nchan:                %8d\n",    Nchan );
   logit( "", "ChanRate:             %8.3lf\n", ChanRate );
   logit( "", "ChanMsgSize:          %8d\n",    ChanMsgSize );
   logit( "", "SampRate:             %10.2lf\n",SampRate );
   logit( "", "Gain:                 %8d\n",    Gain );
   logit( "", "ExtTrig:              %8d\n",    ExtTrig );
   logit( "", "OutKey:               %8d\n",    OutKey );
   logit( "", "HeartbeatInt:         %8d\n",    HeartbeatInt );
   logit( "", "LowValue:             %8.3lf\n", LowValue );
   logit( "", "HighValue:            %8.3lf\n", HighValue );
   logit( "", "FmtDir:               %s\n",     FmtDir );
   logit( "", "MaxFileSize:          %8d\n",    MaxFileSize );
   logit( "", "FmtapeName:           %s\n",     FmtapeName );

/* Log the channel list
   ********************/
   logit( "", "\n\n" );
   logit( "", "  DAQ\n" );
   logit( "", "channel   Sta Comp Net Loc\n" );
   logit( "", "-------   --- ---- --- ---\n" );

   for ( i = 0; i < Nchan; i++ )
   {
      if ( strlen( ChanList[i].sta  ) > 0 )          /* This channel is used */
      {
         logit( "", "  %4d   %-5s %-3s %-2s %-2s\n", i,
            ChanList[i].sta, ChanList[i].comp,
            ChanList[i].net, ChanList[i].loc );
      }
      else                                           /* This channel is unused */
         logit( "", "  %4d     Unused\n", i );
   }

   logit( "", "\n" );
   return;
}


    /*********************************************************************
     *                             IsComment()                           *
     *                                                                   *
     *  Accepts: String containing one line from a config file.          *
     *  Returns: 1 if it's a comment line                                *
     *           0 if it's not a comment line                            *
     *********************************************************************/

int IsComment( char string[] )
{
   int i;

   for ( i = 0; i < (int)strlen( string ); i++ )
   {
      char test = string[i];

      if ( test!=' ' && test!='\t' )
      {
         if ( test == '#'  )
            return 1;          /* It's a comment line */
         else
            return 0;          /* It's not a comment line */
      }
   }
   return 1;                   /* It contains only whitespace */
}
