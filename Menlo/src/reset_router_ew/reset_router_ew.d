#
#                    reset_router_ew's Configuration File
#
InRing           WAVE_RING        # Transport ring to find waveform data on,
MyModuleId   MOD_RESET_ROUTER_EW  # Module id for this instance of reset_router_ew,
HeartbeatInt          15          # Heartbeat interval in secs (if 0, send no heartbeats)
ResetGap             600          # Gaps longer than this many seconds cause a router reset
ProgName          reset_router    # Name of stand-alone program to reset the router
#
#   These are the SCN's to monitor
#
#                                    Router
#     Sta Comp Net      IP Address     Password1  Password2
#     --- ---- ---     -------------------------------------
Scn   1783 HNE  NP     192.168.56.11    RealTime  QuakeWatch
