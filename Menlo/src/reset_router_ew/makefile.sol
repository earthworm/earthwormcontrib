#
#              Make file for reset_router_ew
#                      Solaris Version
#
#  The posix4 library is required for nanaosleep.
#
O = reset_router_ew.o rs_sol.o config.o
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

reset_router_ew: $O
	cc -o $B/reset_router_ew $O $L/sleep_ew.o $L/kom.o $L/getutil.o $L/logit.o $L/time_ew.o $L/transport.o $L/swap.o -lm -lsocket -lnsl -lposix4
