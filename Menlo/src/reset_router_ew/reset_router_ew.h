
/******************************************************************
 *                     File reset_router_ew.h                     *
 ******************************************************************/

#include <time.h>

typedef struct
{
   char   sta[6];               /* Station name */
   char   comp[4];              /* Component */
   char   net[3];               /* Network */
   char   router_ip[16];        /* IP address of router */
   char   router_pwd1[16];      /* Router password 1 */
   char   router_pwd2[16];      /* Router password 2 */
   time_t gapStart;             /* When we received the previous message for this SCN */
   int    reset_active;         /* 1 if router reset active; 0 otherwise */
#ifdef _WINNT
   HANDLE hProcess;             /* Process handle */
#endif
#ifdef _SOLARIS
   pid_t  pid;                  /* Process id */
#endif
} SCN;

typedef struct {
   char InRing[MAX_RING_STR];   /* Name of ring containing tracebuf messages */
   char MyModName[MAX_MOD_STR]; /* module name */
   long InKey;                  /* Key to ring where waveforms live */
   int  nScn;                   /* Number of Scn's in the config file */
   int  HeartbeatInt;           /* Heartbeat interval in seconds */
   int  ResetGap;               /* Gaps longer than this value (sec) cause a router reset */
   char ProgName[80];           /* Name of stand-alone router reset program */
} GPARM;

