
#include <stdio.h>
#include <string.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>
#include "reset_router_ew.h"

#define ncommand 5           /* Number of commands in the config file */


int CountScn( char *config_file )
{
   int nScn = 0;
   int nfiles;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( config_file );
   if ( nfiles == 0 )
   {
      printf( "reset_router_ew: Error opening configuration file <%s>\n",
              config_file );
      return -1;
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         int  success;
         char *com;

         com = k_str();          /* Get the first token from line */

         if ( !com ) continue;             /* Ignore blank lines */
         if ( com[0] == '#' ) continue;    /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "reset_router_ew: Error opening command file <%s>.\n",
                       &com[1] );
               return -1;
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "Scn" ) ) nScn++;

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "reset_router_ew: Bad <%s> command in <%s>.\n", com,
                    config_file );
            return -1;
         }
      }
      nfiles = k_close();
   }
   return nScn;
}


 /*******************************************************************
  *                            GetConfig()                          *
  *           Processes command file using kom.c functions.         *
  *             Returns -1 if any errors are encountered.           *
  *******************************************************************/

int GetConfig( char *config_file, GPARM *Gparm, SCN **pscn )
{
   char     init[ncommand];     /* Flags, one for each command */
   int      nmiss;              /* Number of commands that were missed */
   int      nfiles;
   int      i;
   int      nScn = 0;
   SCN      *scn;

/* Count the number of SCN lines in the config file
   ************************************************/
   Gparm->nScn = CountScn( config_file );
   if ( Gparm->nScn < 0 )
   {
      printf( "reset_router_ew: Error counting Scn lines in config file\n" );
      return -1;
   }
   else if ( Gparm->nScn == 0 )
   {
      printf( "reset_router_ew: No Scn lines in the config file.\n" );
      return -1;
   }

/* Allocate the Scn buffers
   ************************/
   scn = (SCN *) malloc( Gparm->nScn * sizeof(SCN) );
   if ( scn == NULL )
   {
      printf( "reset_router_ew: Can't allocate the scn buffers.\n" );
      return -1;
   }

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ ) init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( config_file );
   if ( nfiles == 0 )
   {
      printf( "reset_router_ew: Error opening configuration file <%s>\n",
              config_file );
      return -1;
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          /* Get the first token from line */

         if ( !com ) continue;             /* Ignore blank lines */
         if ( com[0] == '#' ) continue;    /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               printf( "reset_router_ew: Error opening command file <%s>.\n",
                        &com[1] );
               return -1;
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "InRing" ) )
         {
            if ( str = k_str() )
            {
               strcpy( Gparm->InRing, str );

               if( (Gparm->InKey = GetKey(str)) == -1 )
               {
                  printf( "reset_router_ew: Invalid InRing name <%s>. Exiting.\n", str );
                  return -1;
               }
            }
            init[0] = 1;
         }

         else if ( k_its( "HeartbeatInt" ) )
         {
            Gparm->HeartbeatInt = k_int();
            init[1] = 1;
         }

         else if ( k_its( "ResetGap" ) )
         {
            Gparm->ResetGap = k_int();
            init[2] = 1;
         }

         else if ( k_its( "MyModuleId" ) )
         {
            if ( str=k_str() )
               strcpy( Gparm->MyModName, str );
            init[3] = 1;
         }

         else if ( k_its( "ProgName" ) )
         {
            if ( str=k_str() )
               strcpy( Gparm->ProgName, str );
            init[4] = 1;
         }

         else if ( k_its( "Scn" ) )
         {
            strcpy( scn[nScn].sta,         k_str() );
            strcpy( scn[nScn].comp,        k_str() );
            strcpy( scn[nScn].net,         k_str() );
            strcpy( scn[nScn].router_ip,   k_str() );
            strcpy( scn[nScn].router_pwd1, k_str() );
            strcpy( scn[nScn].router_pwd2, k_str() );
            nScn++;
         }

/* An unknown parameter was encountered
   ************************************/
         else
         {
            printf( "reset_router_ew: <%s> unknown parameter in <%s>\n",
                    com, config_file );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            printf( "reset_router_ew: Bad <%s> command in <%s>.\n", com,
                    config_file );
            return -1;
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss > 0 )
   {
      printf( "reset_router_ew: ERROR, no " );
      if ( !init[0] ) printf( "<InRing> " );
      if ( !init[1] ) printf( "<HeartbeatInt> " );
      if ( !init[2] ) printf( "<ResetGap> " );
      if ( !init[3] ) printf( "<MyModuleId> " );
      if ( !init[4] ) printf( "<ProgName> " );
      printf( "command(s) in <%s>. Exiting.\n", config_file );
      return -1;
   }
   *pscn = scn;  /* Return address of scn array */
   return 0;
}


 /***********************************************************************
  *                              LogConfig()                            *
  *                                                                     *
  *                   Log the configuration parameters                  *
  ***********************************************************************/

void LogConfig( GPARM *Gparm, SCN *scn )
{
   int i;

   logit( "", "\n" );
   logit( "", "InRing:        %s\n",   Gparm->InRing );
   logit( "", "MyModName:     %s\n",   Gparm->MyModName );
   logit( "", "ProgName:      %s\n",   Gparm->ProgName );
   logit( "", "HeartbeatInt:  %-6d\n", Gparm->HeartbeatInt );
   logit( "", "ResetGap:      %-6d\n", Gparm->ResetGap );
   logit( "", "\n" );

   logit( "", "Sta Comp Net     Router IP     Password1  Password2\n" );
   logit( "", "------------     ---------     ---------  ---------\n" );

   for ( i = 0; i < Gparm->nScn; i++ )
   {
      logit( "", "%-5s %-3s %-2s   %-15s  %s  %s\n", scn[i].sta, scn[i].comp,
                               scn[i].net, scn[i].router_ip,
                               scn[i].router_pwd1, scn[i].router_pwd2 );
   }
   logit( "", "\n" );
   return;
}

