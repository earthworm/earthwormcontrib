
       /****************************************************
        *                     rs_sol.c                     *
        *                                                  *
        *  This file contains the SpawnResetRouter and     *
        *  TestChild functions                             *
        ****************************************************/


#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <earthworm.h>
#include "reset_router_ew.h"


      /*****************************************************
       *                 SpawnResetRouter()                *
       *          Spawn the reset_router command.          *
       *                                                   *
       *  Returns  0 if successful                         *
       *          -1 if the process couldn't be spawned    *
       *****************************************************/

int SpawnResetRouter( char ProgName[], SCN *scnp )
{
   pid_t pid = fork();

   switch( pid )
   {
      case -1:    /* fork failed */
         logit( "t", "Error spawning reboot process: %s\n", strerror(errno) );
         return -1;

      case 0:     /* in new child process */
         execlp( ProgName, ProgName, scnp->router_ip, scnp->router_pwd1, scnp->router_pwd2,
                 "-q", (char *)0 );
         logit( "t", "execlp() failed: %s\n", strerror(errno) );
         return -1;

      default:    /* in parent, pid is PID of child */
         break;
   }

   scnp->pid = pid;       /* Save process id */
   return 0;              /* Reboot succeeded */
}


      /**********************************************************
       *                       TestChild()                      *
       *          See if a child process has completed          *
       *                                                        *
       *  Returns  2 if child completed and exitCode is set.    *
       *           1 if child completed and exitCode not set.   *
       *           0 if child process hasn't completed          *
       *          -1 if an error occured.                       *
       **********************************************************/

int TestChild( SCN *scnp, int *exCode )
{
   int   status;
   pid_t rc;

   rc = waitpid( scnp->pid, &status, WNOHANG  );

   if ( rc == -1 )              /* We shouldn't see this */
   {
      logit( "t", "reboot_mss_ew: waitpid() error: %s\n", strerror(errno) );
      return -1;
   }
   else if ( rc == 0 )          /* Child process is still alive */
      return 0;

   if ( WIFEXITED( status ) )
   {
      *exCode = WEXITSTATUS( status );
      return 2;
   }
   else
      return 1;
}

