
       /****************************************************
        *                 reset_router_ew                  *
        *                                                  *
        *          Program to reset a Cisco router         *
        *    using the <clear ip ospf process> command.    *
        *                                                  *
        *  If no data is received from a K2 for a while,   *
        *  we assume the router is hung.                   *
        ****************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <transport.h>
#include <earthworm.h>
#include <trace_buf.h>
#include <kom.h>
#include <swap.h>
#include "reset_router_ew.h"

/* Function prototypes
   *******************/
int  GetConfig( char *, GPARM *, SCN ** );
void LogConfig( GPARM *, SCN * );
int  SpawnResetRouter( char [], SCN * );
int  TestChild( SCN *, int * );

/* Global variables
   ****************/
static SCN          *scn;          /* Array of scn parameters */
static char         *TraceBuf;     /* The tracebuf buffer */
static TRACE_HEADER *TraceHeader;  /* The tracebuf header */
static GPARM        Gparm;         /* Configuration file parameters */
pid_t               myPid;         /* for restarts by startstop */
static SHM_INFO     region;        /* Shared memory region */
static MSG_LOGO     errlogo;       /* Logo of outgoing errors */


int main( int argc, char *argv[] )
{
   MSG_LOGO getlogo;
   MSG_LOGO logo;
   MSG_LOGO hrtlogo;              /* Logo of outgoing heartbeats */
   long     gotsize;
   time_t   startTime;
   time_t   prevHeartTime;
   unsigned char inst_wildcard;
   unsigned char inst_local;
   unsigned char type_tracebuf;
   unsigned char type_heartbeat;
   unsigned char type_error;
   unsigned char mod_wildcard;
   unsigned char mod_reset_router_ew;
   unsigned char seq;

/* Get command line arguments
   **************************/
   if ( argc != 2 )
   {
      printf( "Usage: reset_router_ew <configfile>\n" );
      return -1;
   }

/* Get parameters from the configuration file
   ******************************************/
   if ( GetConfig( argv[1], &Gparm, &scn ) == -1 )
   {
      printf( "reset_router_ew: GetConfig() failed. Exiting.\n" );
      return -1;
   }

/* Open the log file
   *****************/
   if ( strlen( Gparm.MyModName ) != 0 )     /* use config file MyModuleId */
   {
      if ( GetModId( Gparm.MyModName, &mod_reset_router_ew ) != 0 )
      {
         printf( "reset_router_ew: Error getting %s. Exiting.\n", Gparm.MyModName );
         return -1;
      }
   }
   else                                      /* not configured, use default */
   {
      if ( GetModId( "MOD_RESET_ROUTER_EW", &mod_reset_router_ew ) != 0 )
      {
         printf( "reset_router_ew: Error getting MOD_RESET_ROUTER_EW. Exiting.\n" );
         return -1;
      }
   }
   logit_init( argv[1], (short)mod_reset_router_ew, 256, 1 );

/* Log the configuration parameters
   ********************************/
   LogConfig( &Gparm, scn );

/* Initialize some parameters in the Scn structures
   ************************************************/
   {
      int p;
      for ( p = 0; p < Gparm.nScn; p++ )
      {
         scn[p].gapStart = time( 0 );      /* Current system time */
         scn[p].reset_active = 0;          /* No active router resets at startup */
      }
   }

/* Get process ID for heartbeat messages
   *************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
     logit( "e","reset_router_ew: Cannot get pid. Exiting.\n" );
     return -1;
   }

/* Initialize the message buffer
   *****************************/
   TraceBuf = (char *) malloc( (size_t)MAX_TRACEBUF_SIZ );
   if ( TraceBuf == NULL )
   {
      logit( "e", "reset_router_ew: Error allocating trace buffer. Exiting.\n" );
      return -1;
   }
   TraceHeader = (TRACE_HEADER *) TraceBuf;

/* Attach to a transport ring
   **************************/
   tport_attach( &region, Gparm.InKey );

/* Specify logos to get
   ********************/
   if ( GetInst( "INST_WILDCARD", &inst_wildcard ) != 0 )
   {
      logit( "e", "reset_router_ew: Error getting INST_WILDCARD. Exiting.\n" );
      return -1;
   }

   if ( GetLocalInst( &inst_local ) != 0 )
   {
      logit( "e", "reset_router_ew: Error getting MyInstId.\n" );
      return -1;
   }

   if ( GetType( "TYPE_TRACEBUF", &type_tracebuf ) != 0 )
   {
      logit( "e", "reset_router_ew: Error getting <TYPE_TRACEBUF>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_HEARTBEAT", &type_heartbeat ) != 0 )
   {
      logit( "e", "reset_router_ew: Error getting <TYPE_HEARTBEAT>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_ERROR", &type_error ) != 0 )
   {
      logit( "e", "reset_router_ew: Error getting <TYPE_ERROR>. Exiting.\n" );
      return -1;
   }

   if ( GetModId( "MOD_WILDCARD", &mod_wildcard ) != 0 )
   {
      logit( "e", "reset_router_ew: Error getting MOD_WILDCARD. Exiting.\n" );
      return -1;
   }

/* Specify logos of incoming tracebufs and outgoing heartbeats
   ***********************************************************/
   getlogo.instid = inst_wildcard;
   getlogo.type   = type_tracebuf;
   getlogo.mod    = mod_wildcard;

   hrtlogo.instid = inst_local;
   hrtlogo.type   = type_heartbeat;
   hrtlogo.mod    = mod_reset_router_ew;

   errlogo.instid = inst_local;
   errlogo.type   = type_error;
   errlogo.mod    = mod_reset_router_ew;

/* Flush the transport ring on startup
   ***********************************/
   while (tport_copyfrom( &region, &getlogo, (short)1, &logo, &gotsize,
                          (char *)TraceBuf, MAX_TRACEBUF_SIZ, &seq ) != GET_NONE);

/* Get the time when we start reading messages
   *******************************************/
   time( &startTime );
   prevHeartTime = startTime;

/* Master loop
   ***********/
   while ( 1 )
   {
      int    i;
      time_t now;

      sleep_ew( 1000 );

/* See if the termination flag has been set
   ****************************************/
      if ( tport_getflag( &region ) == TERMINATE )
      {
         tport_detach( &region );
         logit( "t", "Termination flag detected. Exiting.\n" );
         return 0;
      }

/* Send a heartbeat to the transport ring
   **************************************/
      now = time( 0 );
      if ( Gparm.HeartbeatInt > 0 )
      {
         if ( (now - prevHeartTime) >= Gparm.HeartbeatInt )
         {
            int  lineLen;
            char line[40];

            prevHeartTime = now;

            sprintf( line, "%ld %ld\n", now, myPid );
            lineLen = strlen( line );

            if ( tport_putmsg( &region, &hrtlogo, lineLen, line ) !=
                 PUT_OK )
            {
               logit( "t", "Error sending heartbeat. Exiting." );
               return -1;
            }
         }
      }

/* See if any router-reset processes have completed.
   TestChild() returns:
      2 if child completed and exitCode is set.
      1 if child completed and exitCode not set.
      0 if child process hasn't completed.
     -1 if an error occured.
   ************************************************/
      for ( i = 0; i < Gparm.nScn; i++ )
      {
         if ( scn[i].reset_active )
         {
            int exitCode;
            int rc = TestChild( &scn[i], &exitCode );

            if ( rc == 1 )
            {
               logit( "t", "%s Can't tell if router reset succeeded.", scn[i].router_ip );
               scn[i].reset_active = 0;
            }
            if ( rc == 2 )
            {
               if ( exitCode == 0 )
                  logit( "t", "%s Router reset succeeded\n", scn[i].router_ip );
               else
                  logit( "t", "%s Router reset failed\n", scn[i].router_ip );

               scn[i].reset_active = 0;
            }
         }
      }

/* Get all available tracebuf messages.
   Update gapStart for the scn's we are monitoring.
   ***********************************************/
      while ( 1 )
      {
         int res = tport_copyfrom( &region, &getlogo, (short)1, &logo, &gotsize,
                                 (char *)TraceBuf, MAX_TRACEBUF_SIZ, &seq );

         if ( res == GET_NONE )
            break;

         if ( res == GET_TOOBIG )
         {
            logit( "t", "Retrieved message is too big (%d)\n", gotsize );
            break;
         }

         if ( res == GET_NOTRACK )
            logit( "t", "NTRACK_GET exceeded.\n" );

         if ( res == GET_MISS_LAPPED )
            logit( "t", "GET_MISS_LAPPED error.\n" );

/* If we're monitoring this SCN, save the current time
   ***************************************************/
         for ( i = 0; i < Gparm.nScn; i++ )
         {
            if ( !strcmp(scn[i].sta,  TraceHeader->sta)  &&
                 !strcmp(scn[i].comp, TraceHeader->chan) &&
                 !strcmp(scn[i].net,  TraceHeader->net) )
            {
               scn[i].gapStart = now;
               break;
            }
         }
      }

/* See how much time has elapsed since
   we saw data from each monitored SCN
   ***********************************/
      for ( i = 0; i < Gparm.nScn; i++ )
      {
         int GapSize = now - scn[i].gapStart;


/* If the gap is too big and we aren't already resetting,
   invoke the reset function
   *****************************************************/
         if ( GapSize >= Gparm.ResetGap )
         {
            if ( scn[i].reset_active == 0 )
            {
               if ( SpawnResetRouter( Gparm.ProgName, &scn[i] ) < 0 )
                  logit( "t", "%s Error spawning process to reset router\n",
                         scn[i].router_ip );
               else
               {
                  logit( "t", "%s Resetting the router\n", scn[i].router_ip );
                  scn[i].reset_active = 1;
               }
            }
            scn[i].gapStart = now;
         }
      }
   }
}
