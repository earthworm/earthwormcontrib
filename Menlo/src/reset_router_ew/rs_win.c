
       /****************************************************
        *                     rs_win.c                     *
        *                                                  *
        *  This file contains the SpawnResetRouter and     *
        *  TestChild functions                             *
        ****************************************************/


#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <earthworm.h>
#include "reset_router_ew.h"


      /*****************************************************
       *                 SpawnResetRouter()                *
       *          Spawn the reset_router command.          *
       *                                                   *
       *  Returns  0 if successful                         *
       *          -1 if the process couldn't be spawned    *
       *****************************************************/

int SpawnResetRouter( char ProgName[], SCN *scnp )
{
   char cmdStr[100];
   int  success;

   STARTUPINFO         startUpInfo;
   PROCESS_INFORMATION procInfo;

   strcpy( cmdStr, ProgName );
   strcat( cmdStr, " " );
   strcat( cmdStr, scnp->router_ip );
   strcat( cmdStr, " " );
   strcat( cmdStr, scnp->router_pwd1 );
   strcat( cmdStr, " " );
   strcat( cmdStr, scnp->router_pwd2 );
   strcat( cmdStr, " -q" );              /* Quiet mode */

/* Get STARTUPINFO structure for current process
   *********************************************/
   GetStartupInfo( &startUpInfo );

/* Create the child process
   ************************/
   success = CreateProcess( 0, cmdStr, 0, 0, FALSE,
                            DETACHED_PROCESS, 0, 0,
                            &startUpInfo, &procInfo );
   if ( !success )
   {
      logit( "t", "Error spawning reboot process: %d\n",
                   GetLastError() );
      return -1;
   }
   scnp->hProcess = procInfo.hProcess;   /* Save process handle */
   return 0;                             /* Reboot succeeded */
}


      /****************************************************
       *                    TestChild()                   *
       *       See if a child process has completed       *
       *                                                  *
       *  Returns  2 if child process completed.          *
       *             In this case, exitCode is set.       *
       *           0 if child process hasn't completed    *
       ****************************************************/

int TestChild( SCN *scnp, int *exCode )
{
   int exitCode;

   GetExitCodeProcess( scnp->hProcess, &exitCode );
   if ( exitCode == STILL_ACTIVE ) return 0;
   *exCode = exitCode;
   return 2;
}
