
/* File apsalarm.h
   ***************/

#define ERR_AC_OFF   0
#define NOTESIZE   160
#define TEXTLEN     80

#define COMPLETED_OK       0
#define CANT_OPEN_SESSION -1
#define ERROR_IN_PACKET   -2
#define SNMP_TIMEOUT      -3
#define UNKNOWN_ERROR     -4

typedef struct {
       char netSta[10];            // Network-station code used by SNW
       char apsName[TEXTLEN];      // IP name or IP address of the APS
       int  powerOn;               // Power on=1; Power off=0
} APS;

typedef struct
{
   int acVoltage;
   int busVoltage;        // Hundredths of a volt
   int systemPower;
   int loadPower;         // Hundredths of a kilowatt
   int loadCurrent;       // amps
   int batteryCurrent;    // amps
   int rectifierCurrent;  // amps
} APS_VALS;

int GetAps( char *peer, APS_VALS *vals );

