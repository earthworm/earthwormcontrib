
   /********************************************************************
    *                           apssoh2snw.c                           *
    *                                                                  *
    *  Command-line program to read a Eaton Powerware APS file which   *
    *  has been converted from html to simple text and to produce a    *
    *  file in SeisNetWatch input format.                              *
    *                                                                  *
    *  Will Kohler  Mar 4, 2010                                        *
    ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Define individual APS params to find in file
   ********************************************/
#define N_APS_PARAM  4    /* number of APS params in SOH file */

#define BUSVOLTAGE   0
#define ACVOLTAGE    1
#define LOADPOWER    2
#define SYSTEMPOWER  3

static char *APSparam[] = { "Bus Voltage:",
                            "AC Voltage:",
                            "Load Power:",
                            "System Power:" };

int main( int argc, char *argv[] )
{
   char  *prog;               /* arvg[0] */
   char  *station;            /* argv[1] */
   char  *network;            /* argv[2] */
   char  *infile;             /* argv[3] */
   char  *outfile;            /* argv[4] */
   FILE  *ifp;                /* file to read from       */
   FILE  *ofp;                /* file to write to        */
   char   line[4096];         /* single line from infile */
   double bus_voltage;        /* bus voltage             */
   int    ac_voltage;         /* AC voltage              */
   double load_power;         /* kilowatt                */
   int    system_power;       /* percent                 */
   int    found[N_APS_PARAM]; /* flag for each SOH param */
   int    nmiss;              /* number of missed params */
   int    nparam;             /* #params to write to SNW */
   int    i;

/* Check arguments; assign them to coder-friendly variables
 **********************************************************/
   prog = argv[0];

   if( argc != 5 )
   {
      fprintf( stderr,
              "  Usage: %s <station> <network> <infile> <outfile>\n"
              "Example: %s B11TR NC aps_NC-B11TR.txt aps_NC-B11TR.snw\n",
               prog, prog );
      return( 1 );
   }
   station = argv[1];
   network = argv[2];
   infile  = argv[3];
   outfile = argv[4];

/* Validate arguments
 ********************/
   if( strlen(station) > 5 )
   {
     fprintf( stderr,
             "%s: station <%s> too long (must be <= 5 chars); exiting!\n",
              prog, station );
     return( 1 );
   }
   if( strlen(network) > 2 )
   {
     fprintf( stderr,
             "%s: network <%s> too long (must be <= 2 chars); exiting!\n",
              prog, network );
     return( 2 );
   }

/* Open the input/output files
 *****************************/
   ifp = fopen( infile, "r" );
   if( ifp == NULL )
   {
     fprintf( stderr, "%s: Cannot open input file <%s>\n", prog, infile );
     return( 4 );
   }

   ofp = fopen( outfile, "w" );
   if( ofp == NULL )
   {
     fprintf( stderr, "%s: Cannot open output <%s>\n",  prog, outfile );
     fclose( ifp );
     return( 5 );
   }

/* Read thru file for interesting SOH params.
/* Different APS firmware versions may use different keywords.
 ************************************************************/
   for ( i = 0; i < N_APS_PARAM; i++ ) found[i] = 0;

   while ( fgets( line, 4096, ifp ) != NULL )
   {
      char *token;

      if ( strncmp( line, " Bus Voltage: ", 14 ) == 0 )
      {
         token = strtok( line+14, " V" );
         sscanf( token, "%lf", &bus_voltage );
         found[BUSVOLTAGE] = 1;
      }

      else if ( strncmp( line, " AC Voltage: ", 13 ) == 0 )
      {
         token = strtok( line+13, " V" );
         sscanf( token, "%d", &ac_voltage  );
         found[ACVOLTAGE] = 1;
      }

      else if ( strncmp( line, " Load Power: ", 13 ) == 0 )
      {
         token = strtok( line+13, " kW" );
         sscanf( token, "%lf", &load_power  );
         found[LOADPOWER] = 1;
      }

      else if ( strncmp( line, " System Power: ", 15 ) == 0 )
      {
         token = strtok( line+15, " %" );
         sscanf( token, "%d", &system_power  );
         found[SYSTEMPOWER] = 1;
      }

//    else if( sscanf( line, " System Power: %d",   &system_power ) == 1 ) found[SYSTEMPOWER] = 1;
   }

/* Verify that all expected parameters were read from file
 *********************************************************/
   nmiss = 0;
   for ( i = 0; i < N_APS_PARAM; i++ )
   {
      if ( !found[i] )
      {
         fprintf( stderr,
                 "%s: error reading <%s> from '%s'; Exiting.\n",
                  prog, APSparam[i], infile );
         nmiss++;
      }
   }
   if ( nmiss )
   {
      printf( "nmiss: %d\n", nmiss );
      fclose( ifp );
      fclose( ofp );
      return( -1*nmiss );  /* exit, returning # of missed params */
   }

/* Write SeisNetWatch-formatted file (all on one line!)
 ******************************************************/
   nparam = N_APS_PARAM;

   fprintf( ofp, "%s-%s:%d:", network, station, nparam );
   fprintf( ofp, "APS Bus Voltage=%.2lf;",          bus_voltage );
   fprintf( ofp, "APS AC Voltage=%d;",              ac_voltage );
   fprintf( ofp, "APS Load Power (kW)=%.2f;",       load_power );
   fprintf( ofp, "APS System Power (percent)=%d\n", system_power );

// printf( "%s-%s:%d:", network, station, nparam );
// printf( "APS Bus Voltage=%.2lf;",          bus_voltage );
// printf( "APS AC Voltage=%d;",              ac_voltage );
// printf( "APS Load Power (kW)=%.2f;",       load_power );
// printf( "APS System Power (percent)=%d\n", system_power );

/* All done!
 ***********/
   fclose( ifp );
   fclose( ofp );
   return( 0 );
}

