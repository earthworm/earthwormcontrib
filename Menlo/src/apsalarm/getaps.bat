@rem snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv -t2000 uwave-aps-gp ac-Voltage.0
@rem snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv -t2000 uwave-aps-sm ac-Voltage.0
@rem snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv -t2000 uwave-aps-tam ac-Voltage.0
@rem snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv -t2000 uwave-aps-vp ac-Voltage.0
@rem snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv -t2000 uwave-aps-mp ac-Voltage.0
@rem snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv -t2000 uwave-aps-cp ac-Voltage.0
@rem snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv -t2000 uwave-aps-fp ac-Voltage.0

@rem Default timeout is 300 ms.  Default maximum number of retries is 6.
@rem Timeout increases after each unsuccessful request until max retries is reached.
snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv uwave-aps-gp ac-Voltage.0
snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv uwave-aps-sm ac-Voltage.0
snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv uwave-aps-tam ac-Voltage.0
snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv uwave-aps-vp ac-Voltage.0
snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv uwave-aps-mp ac-Voltage.0
snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv uwave-aps-cp ac-Voltage.0
snmpget -m RPS-SMX5-MIB -v1 -c public -OUqv uwave-aps-fp ac-Voltage.0
