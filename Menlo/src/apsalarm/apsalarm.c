
   /****************************************************************
    *                       apsalarm Program                       *
    *                                                              *
    *  This program communicates with an Eaton APS PowerWare       *
    *  device, using SNMP.  It reports to statmgr, if the AC       *
    *  input voltage is too low.                                   *
    *                                                              *
    *  The snmpget program must be in the search path.             *
    *  snmpget is available as part of the NetSNMP open-source     *
    *  software package.                                           *
    *                                                              *
    *  Temporary files are created in the Earthworm params         *
    *  directory.  The environmental variable EW_PARAMS must       *
    *  exist.                                                      *
    ****************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <time_ew.h>
#include "apsalarm.h"


/* From the configuration file
   ***************************/
extern long           RingKey;               // Transport ring key
extern unsigned char  MyModId;               // gpsalarm's module id
extern int            LogSwitch;             // If 0, no disk logging
extern int            LogVoltage;            // If 0, no voltage logging
extern int            MinInputVoltage;       // Send alarm when input voltage falls
                                             //    below this value.
extern int            CheckInterval;         // Check the APS this often
extern int            HeartBeatInterval;
extern char           SnwServer[80];         // Machine where the SNW agent is running
extern unsigned short SnwPort;               // Port number of SNW agent on SNW server
extern APS            *aps;
extern int            nAPS;                  // Number of APS sites

/* Function prototypes
   *******************/
int  ReportStatus( unsigned char type, short ierr, char *note );
void GetConfig( char *configfile );
void LogConfig( void );
int  SendSensorReadingToSnw( char snwStr[], char SnwServer[], unsigned short SnwPort, int SnwTimeout );

/* Global variables shared with status.c
   *************************************/
pid_t         myPid;           // For restarts by startstop
unsigned char InstId;          // Local installation id/
unsigned char TypeHeartBeat;
unsigned char TypeError;
SHM_INFO      Region;


           /****************************************
            *       Main program starts here       *
            ****************************************/

int main( int argc, char *argv[] )
{

/* Check command line arguments and get configuration file
   *******************************************************/
   if ( argc != 2 )
   {
        printf( "Usage: apsalarm <configfile>\n" );
        return 0;
   }
   GetConfig( argv[1] );

/* Look up local installation id
   *****************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "apsalarm: Error getting installation id. Exiting.\n" );
      return -1;
   }

/* Look up message types from earthworm.h tables
   *********************************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "apsalarm: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "apsalarm: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      return -1;
   }

/* Log the configuration file
   **************************/
   logit_init( argv[1], (short)MyModId, 256, LogSwitch );
   LogConfig();

/* Get process ID for heartbeat messages
   *************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "apsalarm: Cannot get pid. Exiting.\n" );
      return -1;
   }

/* Attach to shared memory ring
   ****************************/
   tport_attach( &Region, RingKey );

/* Initialize the socket system for sending to SNW
   ***********************************************/
   SocketSysInit();

/* Loop until kill flag is set
   ***************************/
   while ( 1 )
   {
      time_t now = time(0);           // Current time

      static time_t tHeart = 0;       // When the last heartbeat was sent
      static time_t tCheck = 0;       // When the APS was last checked

/* Check kill flag
   ***************/
      if ( tport_getflag( &Region ) == TERMINATE ||
           tport_getflag( &Region ) == myPid )
      {
         tport_detach( &Region );
         logit( "t", "Termination requested. Exiting.\n" );
         return 0;
      }

/* Send heartbeat every HeartBeatInterval seconds
   **********************************************/
      if ( (now - tHeart) >= (time_t)HeartBeatInterval )
      {
         if ( ReportStatus( TypeHeartBeat, 0, "" ) != PUT_OK )
            logit( "t", "apsalarm: Error sending heartbeat to ring.\n");
         tHeart = now;
      }

/* Get status from all APS sites every CheckInterval seconds
   *********************************************************/
      if ( (now - tCheck) >= (time_t)CheckInterval )
      {
         int i;
         tCheck = now;

/* Get APS parameters from all sites
   *********************************/
         for ( i = 0; i < nAPS; i++ )
         {
            APS_VALS vals;
            int rc = GetAps( aps[i].apsName, &vals );

/* Got parameters from the APS server
   **********************************/
            if ( rc == COMPLETED_OK )
            {
               char      note[NOTESIZE];
               char      timestr[30];
               time_t    ltime;
               const int nparam = 8;          // Number of params to write to SNW
               const int snwTimeout = -1;     // Connection timeout in msec; if -1, do not time out
               char      snwStr[200];         // Entire string to send to SNW

               int    acVoltage        = vals.acVoltage;         // AC volts input
               double busVoltage       = 0.01 * vals.busVoltage; // DC volts output
               int    systemPower      = vals.systemPower;       // Percent of capacity
               double loadPower        = 0.01 * vals.loadPower;  // Kilowatts
               int    loadCurrent      = vals.loadCurrent;       // Not sent to SNW
               int    batteryCurrent   = vals.batteryCurrent;    // Not sent to SNW
               int    rectifierCurrent = vals.rectifierCurrent;  // Not sent to SNW

/* Print APS voltage on console.  And to log file, if desired.
   **********************************************************/
               if ( (i == 0) && (nAPS > 1) ) printf( "\n" );
               time( &ltime );
               ctime_ew( &ltime, timestr, 30 );          // Get UTC time as a string
               timestr[ strlen(timestr) - 1] = '\0';     // Strip off trailing CR
               printf( "%s", timestr );
               printf( "  APS AC voltage= %d at %s\n", acVoltage, aps[i].netSta );
               if ( LogVoltage )
                  logit( "t", "APS AC voltage= %d at %s\n", acVoltage, aps[i].netSta );

/* Send string to SeisnetWatch
   ***************************/
               sprintf( snwStr, "%s:%d:APS Bus Voltage=%.2lf;APS AC Voltage=%d;APS Load Power (kW)=%.2lf;APS System Power (percent)=%d;APS Load Current (amps)=%d;APS Battery Current (amps)=%d;APS Rectifier Current (amps)=%d",
                        aps[i].netSta, nparam, busVoltage, acVoltage, loadPower, systemPower, loadCurrent, batteryCurrent, rectifierCurrent );
               strcat( snwStr, ";UsageLevel=3\n" );
//             printf( "%s", snwStr );

               if ( SendSensorReadingToSnw( snwStr, SnwServer, SnwPort, snwTimeout ) != 0 )
                  logit( "et", "Error sending sensor reading to SeisnetWatch\n" );

/* Report to statmgr if the AC power goes off
   ******************************************/
               if ( aps[i].powerOn && (acVoltage < MinInputVoltage) )
               {
                  sprintf( note, "WARNING: No AC power to the PowerWare APS at %s.",
                      aps[i].netSta );
                  logit( "et", "%s\n", note );
                  if ( ReportStatus( TypeError, ERR_AC_OFF, note ) != PUT_OK )
                     logit( "et", "apsalarm: Error sending error message to ring.\n");
                  aps[i].powerOn = 0;
               }

/* Report to statmgr if the AC power is restored
   *********************************************/
               if ( !aps[i].powerOn && (acVoltage >= MinInputVoltage) )
               {
                  sprintf( note, "AC power restored to the PowerWare APS at %s.",
                      aps[i].netSta );
                  logit( "et", "%s\n", note );
                  if ( ReportStatus( TypeError, ERR_AC_OFF, note ) != PUT_OK )
                     logit( "et", "apsalarm: Error sending error message to ring.\n");
                  aps[i].powerOn = 1;
               }
            }

/* Error getting parameters from the APS
   *************************************/
            else if ( rc == CANT_OPEN_SESSION )
               logit( "et", "GetAps can't open session with device at %s\n", aps[i].netSta );

            else if ( rc == ERROR_IN_PACKET )
               logit( "et", "Packet error detected by GetAps at %s\n", aps[i].netSta );

            else if ( rc == SNMP_TIMEOUT )
               logit( "et", "Timeout detected by GetAps at %s\n", aps[i].netSta );

            else if ( rc == UNKNOWN_ERROR )
               logit( "et", "Unknown error detected by GetAps at %s\n", aps[i].netSta );
         }
      }
      sleep_ew( 1000 );
   }
}
