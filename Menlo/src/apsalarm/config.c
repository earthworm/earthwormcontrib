#include <stdio.h>
#include <stdlib.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include "apsalarm.h"


/* Global variables
   ****************/
long           RingKey;               // Transport ring key
unsigned char  MyModId;               // gpsalarm's module id
int            LogSwitch;             // If 0, no disk logging
int            LogVoltage;            // If 0, no voltage logging
int            MinInputVoltage;       // Send alarm when input voltage falls
                                      //    below this value.
int            CheckInterval;         // Check the APS this often
int            HeartBeatInterval;
char           SnwServer[80];         // Machine where the SNW agent is running
unsigned short SnwPort;               // Port number of SNW agent on SNW server

APS            *aps = NULL;
int            nAPS = 0;              // Number of APS sites


       /***************************************************
        *                   GetConfig()                   *
        *  Reads the configuration file.                  *
        *  Exits if any errors are encountered.           *
        ***************************************************/

void GetConfig( char *configfile )
{
   const int ncommand = 10; /* Number of commands to process */
   char      init[10];      /* Flags, one for each command */
   int       nmiss;         /* Number of commands that were missed */
   char      *com;
   char      *str;
   int       nfiles;
   int       success;
   int       i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
        printf( "apsalarm: Error opening command file <%s>. Exiting.\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )      // While there are command files open
   {
        while ( k_rd() )     // Read next line from active file
        {
           com = k_str();    // Get the first token from line

/* Ignore blank lines & comments
   *****************************/
           if ( !com )          continue;
           if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
           if ( com[0] == '@' )
           {
              success = nfiles + 1;
              nfiles  = k_open( &com[1] );
              if ( nfiles != success )
              {
                 printf( "apsalarm: Error opening command file <%s>. Exiting.\n",
                     &com[1] );
                 exit( -1 );
              }
              continue;
           }

           if ( k_its( "MyModuleId" ) )
           {
              str = k_str();
              if ( str )
              {
                 if ( GetModId( str, &MyModId ) < 0 )
                 {
                    printf( "apsalarm: Invalid MyModuleId <%s> in <%s>",
                             str, configfile );
                    printf( ". Exiting.\n" );
                    exit( -1 );
                 }
              }
              init[0] = 1;
           }

           else if( k_its( "RingName" ) )
           {
              str = k_str();
              if (str)
              {
                 if ( ( RingKey = GetKey(str) ) == -1 )
                 {
                    printf( "apsalarm: Invalid RingName <%s> in <%s>",
                             str, configfile );
                    printf( ". Exiting.\n" );
                    exit( -1 );
                 }
              }
              init[1] = 1;
           }

           else if( k_its( "MinInputVoltage" ) )
           {
              MinInputVoltage = k_int();
              init[2] = 1;
           }

           else if( k_its( "LogFile" ) )
           {
              LogSwitch = k_int();
              init[3] = 1;
           }

           else if( k_its( "HeartBeatInterval" ) )
           {
              HeartBeatInterval = k_int();
              init[4] = 1;
           }

           else if( k_its( "CheckInterval" ) )
           {
              CheckInterval = k_int();
              init[5] = 1;
           }

           else if( k_its( "SnwServer" ) )
           {
              str = k_str();
              if (str) strcpy( SnwServer, str );
              init[6] = 1;
           }

           else if( k_its( "SnwPort" ) )
           {
              SnwPort = k_int();
              init[7] = 1;
           }

           else if ( k_its( "APS" ) )
           {
              APS *anotherAPS;
              nAPS++;
              anotherAPS = (APS *) realloc( aps, nAPS * sizeof(APS) );
              if ( anotherAPS != NULL )
              {
                 aps = anotherAPS;
                 str = k_str();
                 if (str) strcpy( aps[nAPS-1].netSta, str );       // eg "NC-PBHRY"
                 str = k_str();
                 if (str) strcpy( aps[nAPS-1].apsName, str );
                 aps[nAPS-1].powerOn = 1;         // Assume AC power is on at start-up
                 init[8] = 1;
              }
              else
              {
                 free( aps );
                 printf( "Error reallocating memory.\n" );
                 exit( -1 );
              }
           }

           else if( k_its( "LogVoltage" ) )
           {
              LogVoltage = k_int();
              init[9] = 1;
           }

           else
           {
              printf( "apsalarm: <%s> unknown command in <%s>.\n",
                       com, configfile );
              continue;
           }

/* See if there were any errors processing the command
   ***************************************************/
           if ( k_err() )
           {
              printf( "apsalarm: Bad <%s> command in <%s>; \n",
                       com, configfile );
              exit( -1 );
           }
        }
        nfiles = k_close();
   }

/* Check flags for missed commands
   *******************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if( !init[i] ) nmiss++;

   if ( nmiss )
   {
      printf( "apsalarm: ERROR, no " );
      if ( !init[0] ) printf( "<MyModuleId> "        );
      if ( !init[1] ) printf( "<RingName> "          );
      if ( !init[2] ) printf( "<MinInputVoltage> "   );
      if ( !init[3] ) printf( "<LogFile> "           );
      if ( !init[4] ) printf( "<HeartBeatInterval> " );
      if ( !init[5] ) printf( "<CheckInterval> "     );
      if ( !init[6] ) printf( "<SnwServer> "         );
      if ( !init[7] ) printf( "<SnwPort> "           );
      if ( !init[8] ) printf( "<APS> "               );
      if ( !init[9] ) printf( "<LogVoltage> "        );
      printf( "command(s) in <%s>. Exiting.\n", configfile );
      exit( -1 );
   }
   return;
}


       /***************************************************
        *                   LogConfig()                   *
        *  Log the configuration file parameters.         *
        ***************************************************/

void LogConfig( void )
{
   int i;

   logit( "", "\n" );
   logit( "", "MyModId:           %u\n",  MyModId );
   logit( "", "RingKey:           %ld\n", RingKey );
   logit( "", "HeartBeatInterval: %d\n",  HeartBeatInterval );
   logit( "", "LogSwitch:         %d\n",  LogSwitch );
   logit( "", "LogVoltage:        %d\n",  LogVoltage );
   logit( "", "MinInputVoltage:   %d\n",  MinInputVoltage );
   logit( "", "CheckInterval:     %d\n",  CheckInterval );
   logit( "", "SnwServer:         %s\n",  SnwServer );
   logit( "", "SnwPort:           %d\n",  SnwPort );
   logit( "", "\n" );
   for ( i = 0; i < nAPS; i++ )
   {
      logit( "", "APS:  %s  %s\n", aps[i].netSta, aps[i].apsName );
   }
   logit( "", "\n" );
   return;
}
