
#include <time.h>
#include <earthworm.h>
#include <transport.h>

/* Global Variables
   ****************/
extern pid_t         myPid;            // For restarts by startstop
extern char          InstId;           // Local installation id
extern unsigned char TypeHeartBeat;
extern unsigned char TypeError;
extern SHM_INFO      Region;
extern unsigned char MyModId;          // This program's module id


         /*************************************************
          *                 ReportStatus()                *
          *  Builds a heartbeat or error message and      *
          *  puts it in shared memory.                    *
          *************************************************/


int ReportStatus( unsigned char type,
                  short         ierr,
                  char          *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   time_t   now = time(0);    // Current time

   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   if( type == TypeHeartBeat )
   {
      sprintf ( msg, "%ld %d\n", (long)now, myPid);
   }
   else if( type == TypeError )
   {
      sprintf ( msg, "%ld %d %s\n", (long)now, ierr, note);
   }
   else
   {
      logit( "et", "Error.  Msg type %u. This is not a heartbeat or error message.\n", type );
      return -99;
   }

   size = strlen( msg );     // Don't include null byte in message
   return tport_putmsg( &Region, &logo, size, msg );
}
