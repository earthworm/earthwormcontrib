#
#                apsalarm's Configuration File
#
MyModuleId   MOD_APSALARM   # Module id for this program,

RingName        STAT_RING   # Transport ring to write to,

HeartBeatInterval      30   # Interval between heartbeats sent to statmgr,
                            #   in seconds.

LogFile                 1   # If 0, don't write logfile at all, if 1, do
                            # if 2, write module log but not to stderr/stdout

LogVoltage              0   # If 0, don't log voltages
                            # If non-zero, log all voltages

MinInputVoltage       100   # Send alarm when APS AC input voltage drops below this value

CheckInterval         120   # Check the APS this often, in seconds

SnwServer        campbell   # Machine where the SNW agent is running.
                            # The name must be in the hosts file or DNS (No IPs)

SnwPort              6666   # Port number of SNW agent on the SNW server.


# These are the APS units to check:
#
#                Network name
#     Net-Sta    or IP address
#     --------   -------------
APS   NC-GGPRY   uwave-aps-gp
APS   NC-NSMRY   uwave-aps-sm
APS   NC-NTARY   uwave-aps-tam
APS   NC-CVPRY   uwave-aps-vp
APS   NC-JMPRY   uwave-aps-mp
APS   NC-JXPRY   uwave-aps-cp
APS   NC-HFPRY   uwave-aps-fp
APS   NC-PWHRY   uwave-aps-wh
APS   NC-PBMRY   uwave-aps-bm

