/***********************************************************************
 *                           wdog-AU912.c
 *
 *  These functions control the watchdog hardware built into the
 *  system board of a Logic Supply AU912 computer. They require a
 *  PortTalk 2.2 or later device driver.  Follow these steps on your
 *  development computer:
 *
 *  1.  Download file porttalk22.zip from http://drivers.softpedia.com.
 *  2.  Unzip the file to any convenient location on your hard drive.
 *  3.  Copy file pt_ioctl.c to the source directory of your program.
 *  4.  Include pt_ioctl.c and windows.h in your program.
 *  5.  Before compiling, run batch file "vcvars32.bat".
 *
 *  To install the porttalk driver, copy file porttalk.sys to
 *  C:\Windows\System32\drivers and reboot your computer.
 *  The driver must be installed on all computers that will run
 *  your program.
 ***********************************************************************/

#include <stdio.h>
#include <windows.h>
#include "pt_ioctl.c"


/* EnableWatchdog():
   ResetSec is the time until reboot, limited to 255 seconds.
   *********************************************************/

void EnableWatchdog( unsigned char resetSec )
{
   OpenPortTalk();
   outportb( 0x2e,   0x87 );     // Initialize IO port
   outportb( 0x2e,   0x87 );     // twice

   outportb( 0x2e,   0x07 );     // Point to logical device
   outportb( 0x2e+1, 0x07 );     // Select logical device 7

   outportb( 0x2e,   0xf5 );     // Select offset f5h
   outportb( 0x2e+1, 0x40 );     // Set bit 5 = 1 to clear bit 5

   outportb( 0x2e,   0xf0 );     // Select offset f0h
   outportb( 0x2e+1, 0x81 );     // Set bit 7 = 1 to enable WDTRST#

   outportb( 0x2e,   0xf6 );     // Select offset f6h
   outportb( 0x2e+1, resetSec ); // Set watchdog timeout in seconds (0-255)

   outportb( 0x2e,   0xf5 );     // Select offset f5h
   outportb( 0x2e+1, 0x20 );     // Set bit5 = 1 enable watch dog time

   outportb( 0x2e,   0xaa );     // Close IO port
   ClosePortTalk();
   return;
}


/* DisableWatchdog():
   Calling this function disables the watchdog
   *******************************************/

void DisableWatchdog( void )
{
   OpenPortTalk();
   outportb( 0x2e,   0x87 );     // Initialize IO port
   outportb( 0x2e,   0x87 );     // twice

   outportb( 0x2e,   0xf5 );     // Select offset f5h
   outportb( 0x2e+1, 0x00 );     // Set bit5 = 0 disable watch dog

   outportb( 0x2e,   0xaa );     // Close IO port
   ClosePortTalk();
   return;
}

