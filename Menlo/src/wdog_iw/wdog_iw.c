/***********************************************************************
 *                             wdog_iw.c
 *
 *  This program reboots your Inchworm if no Earthworm tracebuf data
 *  are being collected.  The porttalk driver must be installed by
 *  copying file porttalk.sys to directory C:\Windows\System32\drivers
 *  and rebooting the computer.
 ***********************************************************************/

#include <stdio.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include "wdog_iw.h"

/* Function declarations
   *********************/
void EnableWatchdog( unsigned char CountdownSec );
void DisableWatchdog( void );
void GetConfig( char *configfile );
void LogConfig( void );
void wdog_lookup( void );
void wdog_status( unsigned char type, short ierr, char *text );

/* Global variables
 ******************/
pid_t         myPid;          // For restarts by startstop
unsigned char InstId;         // Local installation id
SHM_INFO      Region;         // Shared memory region
unsigned char TypeHeartBeat;
unsigned char TypeError;
unsigned char TypeTracebuf2;
unsigned char InstWildcard;
unsigned char ModWildcard;
unsigned char TypeWildcard;
long          RingKey;        // Transport ring key
unsigned char MyModId;        // Module id of this program

/* Variables from the wdog_iw.d configuration file
   ***********************************************/
extern char     RingName[MAX_RING_STR]; // Name of transport ring
extern char     MyModName[MAX_MOD_STR]; // Speak as this module name/id
extern int      HeartBeatInterval;      // Seconds between statmgr heartbeats
extern int      CountdownSec;           // Must be less than 256 seconds.
extern short    nLogo;                  // Num logos to read from transport ring
extern MSG_LOGO GetLogo[MAXLOGO];       // Array for requesting module,type,instid
extern int      PreSleep;               // Delay startup this many seconds
extern int      ReenableInterval;       // Interval between watchdog reenables


int main( int argc, char *argv[] )
{
   time_t timeNow;               // Current time
   time_t timeLastBeat;          // Time last heartbeat was sent
   time_t timeLastEnabled;       // Time watchdog was last reenabled
   int gotmsg = FALSE;           // No data received yet
   int wdogInstalled = TRUE;     // TRUE or FALSE
                                 // Set to FALSE for testing on a
                                 // machine without a watchdog.
   int  debug = TRUE;            // TRUE or FALSE

#define BUF_SIZE 60000           // Maximum size for an event msg
   static char Buffer[BUF_SIZE]; // Buffer to hold event message
   long        recsize;          // Size of retrieved message
   MSG_LOGO    reclogo;          // Logo of retrieved message

/* Check program arguments
   ***********************/
   if ( argc != 2 )
   {
      printf( "\nUsage: wdog_iw <config_file>\n" );
      return -1;
   }

/* Initialize log file
   *******************/
   logit_init( argv[1], 0, 256, 1 );

/* Read and log the configuration file
 * ***********************************/
   GetConfig( argv[1] );
   LogConfig();

/* Look up important info from earthworm.h tables
   **********************************************/
   wdog_lookup();

/* Get process ID for heartbeat messages
   *************************************/
   myPid = getpid();
   if( myPid == -1 )
   {
     logit("e","Cannot get my process id. Exiting.\n");
     return 0;
   }

/* Attach to shared memory ring
   ****************************/

   tport_attach( &Region, RingKey );
   logit( "", "Attached to shared-memory region %s: %d\n",
          RingName, RingKey );

/* Sleep to allow dataloggers to begin producing data
   **************************************************/
   logit( "et", "Sleeping %d seconds before enabling watchdog.\n", PreSleep );
   sleep_ew( 1000 * PreSleep );

/* Enable watchdog and wait for machine to reboot.
   After calling EnableWatchdog(), you must call
   either SetWatchdog() or DisableWatchdog() within
   CountdownSec seconds or else the computer will reboot.
   *****************************************************/
   if ( wdogInstalled )
      EnableWatchdog( (unsigned char)CountdownSec );
   logit( "et", "Watchdog enabled.\n" );
   timeLastEnabled = time( &timeNow );

/* Flush the shared-memory region
   ******************************/
   while( tport_getmsg( &Region, GetLogo, nLogo, &reclogo, &recsize, Buffer,
                        sizeof(Buffer)-1 ) != GET_NONE );

/* Force a heartbeat to be issued in first pass thru main loop
   ***********************************************************/
   timeLastBeat = time(&timeNow) - HeartBeatInterval - 1;

//------------------- Setup done; start main loop ---------------------
   while ( 1 )
   {

/* Send heartbeat to statmgr
   *************************/
      if  ( time(&timeNow) - timeLastBeat >= HeartBeatInterval )
      {
         timeLastBeat = timeNow;
         wdog_status( TypeHeartBeat, 0, "" );
      }

/* Get all available messages from transport ring
   **********************************************/
      while ( 1 )
      {
         int  i;
         int  res;
         char text[150];  // String for log messages

/* If a termination has been requested,
   disable watchdog and exit program.
   ***********************************/
         if ( tport_getflag( &Region ) == TERMINATE ||
              tport_getflag( &Region ) == myPid )
         {
            logit( "et", "Termination requested.\n" );
            tport_detach( &Region );
            if ( wdogInstalled )
               DisableWatchdog();
            logit( "et", "Watchdog disabled.\n" );
            logit( "et", "Exiting wdog_iw.\n" );
            return 0;
         }

/* Get message and check return code from transport
   ************************************************/
         res = tport_getmsg( &Region, GetLogo, nLogo,
               &reclogo, &recsize, Buffer, sizeof(Buffer)-1 );

         if( res == GET_NONE )                // No more new messages
         {
              break;
         }
         else if( res == GET_TOOBIG )         // Message was too big
         {                                    // Complain and try again
              sprintf( text,
                      "Retrieved msg[%ld] (i%u m%u t%u) too big for Buffer[%d]",
                      recsize, reclogo.instid, reclogo.mod, reclogo.type,
                      sizeof(Buffer)-1 );
              wdog_status( TypeError, ERR_TOOBIG, text );
         }
         else if( res == GET_MISS )           // Got a msg but missed some
         {
              sprintf( text,
                      "Missed msg(s)  i%u m%u t%u  %s.",
                       reclogo.instid, reclogo.mod, reclogo.type, RingName );
              wdog_status( TypeError, ERR_MISSMSG, text );
         }
         else if( res == GET_NOTRACK )        // Got a msg but can't tell
         {                                    // if any were missed.
              sprintf( text,
                       "Msg received (i%u m%u t%u); transport.h NTRACK_GET exceeded",
                        reclogo.instid, reclogo.mod, reclogo.type );
              wdog_status( TypeError, ERR_NOTRACK, text );
         }

/* Does this message have a logo we are looking for?
   *************************************************/
         for ( i = 0; i < nLogo; i++ )
         {
            if ( (GetLogo[i].instid == reclogo.instid) ||
                 (GetLogo[i].instid == InstWildcard) )
               if ( (GetLogo[i].mod = reclogo.mod) ||
                    (GetLogo[i].mod == ModWildcard) )
                  if ( (GetLogo[i].type == reclogo.type) ||
                       (GetLogo[i].type == TypeWildcard) )
                     gotmsg = TRUE;
         }
      }                       // End of message-processing loop

/* If we got an interesting message and it's been
   a long enough time, reenable watchdog
   **********************************************/
      if ( gotmsg )
      {
         if  ( time(&timeNow) - timeLastEnabled >= ReenableInterval )
         {
            if ( wdogInstalled )
               EnableWatchdog( (unsigned char)CountdownSec );
            if ( debug )
               logit( "et", "Watchdog enabled.\n" );
            timeLastEnabled = timeNow;
            gotmsg = FALSE;
         }
      }
      sleep_ew( 1000 );  // Wait for new messages to arrive
   }
//-------------------------End of main loop----------------------------
}


/**************************************************************************
 *  wdog_lookup( )   Look up important info from earthworm.h tables       *
 **************************************************************************/
void wdog_lookup( void )
{

/* Look up my module id
   ********************/
   if ( GetModId( MyModName, &MyModId ) != 0 )
   {
      printf( "wdogiw: Invalid module name <%s>. Exiting.\n", MyModName );
      exit( -1 );
   }
/* Look up key to shared memory region
   ***********************************/
   if( ( RingKey = GetKey(RingName) ) == -1 )
   {
      printf( "wdogiw:  Invalid ring name <%s>. Exiting.\n", RingName);
      exit( -1 );
   }

/* Look up local installation id
   *****************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "wdogiw: Error getting local installation id. Exiting.\n" );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "wdogiw: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "wdogiw: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_TRACEBUF2", &TypeTracebuf2 ) != 0 )
   {
      printf( "wdogiw: Invalid message type <TYPE_TRACEBUF2>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetInst( "INST_WILDCARD", &InstWildcard ) != 0 )
   {
      printf( "wdogiw: Invalid installation id <INST_WILDCARD>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetModId( "MOD_WILDCARD", &ModWildcard ) != 0 )
   {
      printf( "wdogiw: Invalid module id <MOD_WILDCARD>. Exiting.\n" );
      exit( -1 );
   }
   if ( GetType( "TYPE_WILDCARD", &TypeWildcard ) != 0 )
   {
      printf( "wdogiw: Invalid message type <TYPE_WILDCARD>. Exiting.\n" );
      exit( -1 );
   }
   return;
}

/**************************************************************************
 * wdog_status() builds a heartbeat or error message & puts it into       *
 *               shared memory.  Writes errors to log file & console.     *
 **************************************************************************/
void wdog_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size;
   time_t        t;

/* Build the message
   *****************/
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
        sprintf( msg, "%ld %ld\n\0", (long)t, (long)myPid);
   }
   else if( type == TypeError )
   {
        sprintf( msg, "%ld %hd %s\n\0", (long)t, ierr, note);
        logit( "et", "wdog_iw: %s\n", note );
   }

   size = strlen( msg );   // Don't include null byte in message

/* Write the message to shared memory
   **********************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","wdog_iw:  Error sending heartbeat.\n" );
        }
        else if( type == TypeError ) {
           logit("et","wdog_iw:  Error sending error:%d.\n", ierr );
        }
   }
   return;
}
