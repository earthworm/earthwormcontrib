
#define   MAXLOGO     10       // Max logos to read from transport ring

/* Error messages
   **************/
#define  ERR_MISSMSG   0       // Message missed in transport ring
#define  ERR_TOOBIG    1       // Retreived msg too large for buffer
#define  ERR_NOTRACK   2       // Msg retrieved; tracking limit exceeded
