#
#                Configuration File for Program wdog_iw
#
MyModName      MOD_WDOGIW   # Module id for this program.

RingName        SCNL_RING   # Transport ring to read tracebuf messages from
                            # and write heartbeats and errors to.

HeartBeatInterval      15   # Interval between heartbeats sent to statmgr,
                            #   in seconds.

CountdownSec          255   # Reboot after this many seconds after the
                            # watchdog has been enabled but not reenabled.
                            # Must be less than 256 seconds.

ReenableInterval       30   # Interval in seconds between watchdog
                            # reenables, provided that a desired logo
                            # has been detected.

PreSleep               30   # Wait this many seconds before enabling
                            # watchdog, to allow data to arrive in
                            # the transport ring.

# Up to 10 GetEventsFrom commands are allowed.
# If any of these logos are found, the watchdog will be reenabled.
# INST_WILDCARD, MOD_WILDCARD, and TYPE_WILCARD are permitted vales.

#              Installation ID    Module ID      Message Type
#              ---------------    ---------      ------------
GetEventsFrom    INST_MENLO     MOD_ADSENDXS    TYPE_TRACEBUF2
