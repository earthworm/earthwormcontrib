
#include <stdio.h>
#include <string.h>
#include <earthworm.h>
#include <transport.h>
#include <kom.h>
#include "wdog_iw.h"

// Global variables, from configuration file
// *****************************************
char      MyModName[MAX_MOD_STR];  // Speak as this module name/id
char      RingName[MAX_RING_STR];  // Name of transport ring
int       HeartBeatInterval;       // Interval between statmgr heartbeats
int       CountdownSec;
short     nLogo;                   // Num logos to read from transport ring
MSG_LOGO  GetLogo[MAXLOGO];        // Array for requesting inst, mod, type
int       PreSleep;                // Delay startup this many seconds
int       ReenableInterval;        // Interval between watchdog reenables



       /***************************************************
        *                   GetConfig()                   *
        *  Reads the configuration file.                  *
        *  Exits if any errors are encountered.           *
        ***************************************************/

void GetConfig( char *configfile )
{
   const int ncommand = 7;       // Number of commands to process
   char      init[10];           // Flags, one for each command
   int       nmiss;              // Number of commands that were missed
   char      *com;
   char      *str;
   int       nfiles;
   int       success;
   int       i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;
   nLogo = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
        printf( "wdog_iw: Error opening command file <%s>. Exiting.\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )      // While there are command files open
   {
        while ( k_rd() )     // Read next line from active file
        {
           com = k_str();    // Get the first token from line

/* Ignore blank lines & comments
   *****************************/
           if ( !com )          continue;
           if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
           if ( com[0] == '@' )
           {
              success = nfiles + 1;
              nfiles  = k_open( &com[1] );
              if ( nfiles != success )
              {
                 printf( "wdog_iw: Error opening configuration file <%s>. Exiting.\n",
                     &com[1] );
                 exit( -1 );
              }
              continue;
           }

           if ( k_its( "MyModName" ) )
           {
              strcpy( MyModName, k_str() );
              init[0] = 1;
           }

           else if( k_its( "RingName" ) )
           {
              strcpy( RingName, k_str() );
              init[1] = 1;
           }

           else if( k_its( "HeartBeatInterval" ) )
           {
              HeartBeatInterval = k_int();
              init[2] = 1;
           }

           else if( k_its( "CountdownSec" ) )
           {
              CountdownSec = k_int();
              init[3] = 1;
           }

           else if( k_its("GetEventsFrom") )
           {
              if ( nLogo+1 > MAXLOGO ) {
                  logit( "e", "Too many <GetEventsFrom> commands in <%s>",
                           configfile );
                  logit( "e", "; max=%d. Exiting.\n", MAXLOGO );
                  exit( -1 );
              }
              if( ( str=k_str() ) ) {
                 if( GetInst( str, &GetLogo[nLogo].instid ) != 0 ) {
                     logit( "e", "Invalid installation name <%s>", str );
                     logit( "e", " in <GetEventsFrom> cmd. Exiting.\n" );
                     exit( -1 );
                 }
              }
              if( ( str=k_str() ) ) {
                 if( GetModId( str, &GetLogo[nLogo].mod ) != 0 ) {
                     logit( "e", "Invalid module name <%s>", str );
                     logit( "e", " in <GetEventsFrom> cmd. Exiting.\n" );
                     exit( -1 );
                 }
              }
              if( ( str=k_str() ) ) {
                  if( GetType( str, &GetLogo[nLogo].type ) != 0 ) {
                  logit( "e", "Invalid message type <%s>", str );
                  logit( "e", " in <GetEventsFrom> cmd. Exiting.\n" );
                  exit( -1 );
                 }
              }
              nLogo++;
              init[4] = 1;
           }

           else if( k_its( "PreSleep" ) )
           {
              PreSleep = k_int();
              init[5] = 1;
           }

           else if( k_its( "ReenableInterval" ) )
           {
              ReenableInterval = k_int();
              init[6] = 1;
           }

           else
           {
              printf( "<%s> unknown command in <%s>.\n",
                       com, configfile );
              printf( "Exiting.\n" );
              exit( -1 );
           }

/* See if there were any errors processing the command
   ***************************************************/
           if ( k_err() )
           {
              printf( "wdog_iw: Bad <%s> command in <%s>; \n",
                       com, configfile );
              exit( -1 );
           }
        }
        nfiles = k_close();
   }

/* Check flags for missed commands
   *******************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if( !init[i] ) nmiss++;

   if ( nmiss )
   {
      printf( "wdog_iw: ERROR, no " );
      if ( !init[0] ) printf( "<MyModName> "         );
      if ( !init[1] ) printf( "<RingName> "          );
      if ( !init[2] ) printf( "<HeartBeatInterval> " );
      if ( !init[3] ) printf( "<CountdownSec> "      );
      if ( !init[4] ) printf( "<GetEventsFrom> "     );
      if ( !init[5] ) printf( "<PreSleep> "          );
      if ( !init[6] ) printf( "<ReenableInterval> "  );
      printf( "command(s) in <%s>. Exiting.\n", configfile );
      exit( -1 );
   }
   return;
}


       /***************************************************
        *                   LogConfig()                   *
        *  Log the configuration file parameters.         *
        ***************************************************/

void LogConfig( void )
{
   int i;

   logit( "e", "\n" );
   logit( "e", "MyModName:         %s\n", MyModName );
   logit( "e", "RingName:          %s\n", RingName );
   logit( "e", "HeartBeatInterval: %d\n", HeartBeatInterval );
   logit( "e", "CountdownSec;      %d\n", CountdownSec );
   logit( "e", "PreSleep:          %d\n", PreSleep );
   logit( "e", "ReenableInterval:  %d\n", ReenableInterval );

   for ( i = 0; i < nLogo; i++ )
   logit( "e", "GetLogo[%d] inst:%d module:%d type:%d\n",
               nLogo-1,
               (int) GetLogo[i].instid,
               (int) GetLogo[i].mod,
               (int) GetLogo[i].type );
   logit( "e", "\n" );
   return;
}
