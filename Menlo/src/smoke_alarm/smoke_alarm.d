#
#                smoke_alarm's Configuration File
#
MyModuleId  MOD_SMOKE_ALARM # Module id for this program,

RingName        HYPO_RING   # Transport ring to write to,

HeartBeatInterval      30   # Interval between heartbeats sent to statmgr,
                            #   in seconds.

LogFile                 1   # If 0, don't write logfile at all, if 1, do
                            # if 2, write module log but not to stderr/stdout

ComPort                 2   # Com port used by smoke alarm
                            # 1 = com1, 2 = com2, etc

CheckInterval           5   # Check smoke alarm status this often,
                            # in seconds

