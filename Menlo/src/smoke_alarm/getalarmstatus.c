
#include <stdio.h>
#include <math.h>
#include <earthworm.h>
#include "smoke_alarm.h"

#define BUFSIZE 40   /* Size of the read buffer */

int WritePort( char * );
int ReadPort( char *, int nchar );



           /***************************************************************
            *                       GetAlarmStatus()                      *
            *  See if the smoke alarm is currently triggered.             *
            *  If the smoke alarm is triggered, alarmStatus is set to     *
            *  ALARM_TRIGGERED.                                           *
            *  Otherwise, alarmStatus is set to ALARM_NOT_TRIGGERED.      *
            *                                                             *
            *  Returns  0 if we got the alarm status                      *
            *          -1 if an error occured                             *
            ***************************************************************/


int GetAlarmStatus( int *alarmStatus )
{
   static char buf[BUFSIZE];
   int rc;

/* Send character string to the smoke detector.
   If the relay is closed, the string will be

   returned to the serial port via loopback.
   *******************************************/
   rc = WritePort( "LoopString" );
   if ( rc == -1 )
   {
      logit( "et", "Error sending loopback string. WritePort() returns: %d\n", rc );
      return -1;
   }

/* See if the loopback string shows up on
   the input line of the serial port.
   **************************************/
   rc = ReadPort( buf, 10 );
   if ( rc < 0 )
   {
      *alarmStatus = ALARM_NOT_TRIGGERED;
      return 0;
   }

/* We received a bad loop string (unlikely)
   ****************************************/
   if ( strcmp(buf, "LoopString") != 0 )
   {
      logit( "et", "Error. Bad loop string: %s\n", buf );
      return -1;
   }

/* We received the loopback message
   ********************************/
   *alarmStatus = ALARM_TRIGGERED;
   return 0;
}
