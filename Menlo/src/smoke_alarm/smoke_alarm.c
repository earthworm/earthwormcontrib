
   /****************************************************************
    *                     smoke_alarm Program                      *
    ****************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include "smoke_alarm.h"

#define ERR_REPORTSMOKE     0

/* Global variables
   ****************/
static long          RingKey;          /* Transport ring key */
static unsigned char MyModId;          /* smoke_alarm's module id */
static int           LogSwitch;        /* If 0, no disk logging */
static int           CheckInterval;    /* Check worst-case time error this often */
static int           ComPort;          /* Com port of smoke alarm */
static int           HeartBeatInterval;
static pid_t         myPid;            /* For restarts by startstop */
static unsigned char InstId;           /* Local installation id  */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static SHM_INFO      Region;

int InitPort( int );
int GetAlarmStatus( int *currentAlarmStatus );


         /*************************************************
          *                 ReportStatus()                *
          *  Builds a heartbeat or error message and      *
          *  puts it in shared memory.                    *
          *************************************************/


int ReportStatus( unsigned char type,
                  short         ierr,
                  char          *note )
{
   MSG_LOGO logo;
   char     msg[256];
   int      res;
   long     size;
   time_t   t;

   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
           sprintf ( msg, "%ld %d\n", (long)t, myPid);
   }
   else if( type == TypeError )
   {
           sprintf ( msg, "%ld %d %s\n", (long)t, ierr, note);
   }

   size = strlen( msg );  /* don't include null byte in message */
   res  = tport_putmsg( &Region, &logo, size, msg );

   return res;
}


       /***************************************************
        *                   GetConfig()                   *
        *  Reads the configuration file.                  *
        *  Exits if any errors are encountered.           *
        ***************************************************/

void GetConfig( char *configfile )
{
   const int ncommand = 6;  /* Number of commands to process */
   char      init[10];      /* Flags, one for each command */
   int       nmiss;         /* Number of commands that were missed */
   char      *com;
   char      *str;
   int       nfiles;
   int       success;
   int       i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
        printf( "smoke_alarm: Error opening command file <%s>. Exiting.\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )      /* While there are command files open */
   {
        while ( k_rd() )     /* Read next line from active file  */
        {
           com = k_str();   /* Get the first token from line */

/* Ignore blank lines & comments
   *****************************/
           if ( !com )          continue;
           if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
           if ( com[0] == '@' )
           {
              success = nfiles + 1;
              nfiles  = k_open( &com[1] );
              if ( nfiles != success )
              {
                 printf( "smoke_alarm: Error opening command file <%s>. Exiting.\n",
                     &com[1] );
                 exit( -1 );
              }
              continue;
           }

           if ( k_its( "MyModuleId" ) )
           {
              str = k_str();
              if ( str )
              {
                 if ( GetModId( str, &MyModId ) < 0 )
                 {
                    printf( "smoke_alarm: Invalid MyModuleId <%s> in <%s>",
                             str, configfile );
                    printf( ". Exiting.\n" );
                    exit( -1 );
                 }
              }
              init[0] = 1;
           }

           else if( k_its( "RingName" ) )
           {
              str = k_str();
              if (str)
              {
                 if ( ( RingKey = GetKey(str) ) == -1 )
                 {
                    printf( "smoke_alarm: Invalid RingName <%s> in <%s>",
                             str, configfile );
                    printf( ". Exiting.\n" );
                    exit( -1 );
                 }
              }
              init[1] = 1;
           }

           else if( k_its( "LogFile" ) )
           {
              LogSwitch = k_int();
              init[2] = 1;
           }

           else if( k_its( "HeartBeatInterval" ) )
           {
              HeartBeatInterval = k_int();
              init[3] = 1;
           }

           else if( k_its( "CheckInterval" ) )
           {
              CheckInterval = k_int();
              init[4] = 1;
           }

           else if( k_its( "ComPort" ) )
           {
              ComPort = k_int();
              init[5] = 1;
           }

           else
           {
              printf( "smoke_alarm: <%s> unknown command in <%s>.\n",
                       com, configfile );
              continue;
           }

/* See if there were any errors processing the command
   ***************************************************/
           if ( k_err() )
           {
              printf( "smoke_alarm: Bad <%s> command in <%s>; \n",
                       com, configfile );
              exit( -1 );
           }
        }
        nfiles = k_close();
   }

/* Check flags for missed commands
   *******************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if( !init[i] ) nmiss++;

   if ( nmiss )
   {
      printf( "smoke_alarm: ERROR, no " );
      if ( !init[0] ) printf( "<MyModuleId> "        );
      if ( !init[1] ) printf( "<RingName> "          );
      if ( !init[2] ) printf( "<LogFile> "           );
      if ( !init[3] ) printf( "<HeartBeatInterval> " );
      if ( !init[4] ) printf( "<CheckInterval>     " );
      if ( !init[5] ) printf( "<ComPort> "           );
      printf( "command(s) in <%s>. Exiting.\n", configfile );
      exit( -1 );
   }
   return;
}


       /***************************************************
        *                   LogConfig()                   *
        *  Log the configuration file parameters.         *
        ***************************************************/

void LogConfig( void )
{
   logit( "", "\n" );
   logit( "", "MyModId:           %u\n",    MyModId );
   logit( "", "RingKey:           %ld\n",   RingKey );
   logit( "", "HeartBeatInterval: %d\n",    HeartBeatInterval );
   logit( "", "LogSwitch:         %d\n",    LogSwitch );
   logit( "", "CheckInterval:     %d\n",    CheckInterval );
   logit( "", "ComPort:           %d\n",    ComPort );
   logit( "", "\n" );
   return;
}



           /****************************************
            *       Main program starts here       *
            ****************************************/

int main( int argc, char *argv[] )
{
   time_t tHeart = 0;       /* When the last heartbeat was sent */
   time_t tCheck = 0;       /* When the worst-case time error was last checked */

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
        printf( "Usage: smoke_alarm <configfile>\n" );
        return 0;
   }

/* Read configuration file
   ***********************/
   GetConfig( argv[1] );

/* Look up installation id
   ***********************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      printf( "smoke_alarm: Error getting installation id. Exiting.\n" );
      return -1;
   }

/* Look up message types from earthworm.h tables
   *********************************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      printf( "smoke_alarm: Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      printf( "smoke_alarm: Invalid message type <TYPE_ERROR>. Exiting.\n" );
      return -1;
   }

/* Initialize log file and log configuration parameters
   ****************************************************/
   logit_init( argv[1], (short)MyModId, 256, LogSwitch );
   logit( "" , "smoke_alarm: Read command file <%s>\n", argv[1] );
   LogConfig();

/* Get process ID for heartbeat messages
   *************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "smoke_alarm: Cannot get pid. Exiting.\n" );
      return -1;
   }

/* Attach to shared memory ring
   ****************************/
   tport_attach( &Region, RingKey );

/* Initialize the serial port.
   Exit on error.
   **************************/
   if ( InitPort( ComPort ) == -1 )
   {
      logit( "e", "Error initializing com port %d. Exiting.\n", ComPort );
      return -1;
   }

/* Loop until kill flag is set
   ***************************/
   while ( 1 )
   {
      int    rc;
      time_t now = time(0);           /* Current time */
      int    currentAlarmStatus;
      char   note[120];
      static int previousAlarmStatus = ALARM_NOT_TRIGGERED;

/* Check kill flag
   ***************/
      if ( tport_getflag( &Region ) == TERMINATE )
      {
         tport_detach( &Region );
         logit( "t", "Termination requested. Exiting.\n" );
         return 0;
      }

/* Send heartbeat every HeartBeatInterval seconds
   **********************************************/
      if ( (now - tHeart) >= (time_t)HeartBeatInterval )
      {
         if ( ReportStatus( TypeHeartBeat, 0, "" ) != PUT_OK )
            logit( "t", "smoke_alarm: Error sending heartbeat to ring.\n");
         tHeart = now;
      }

/* Get the smoke-alarm status every CheckInterval seconds
   ******************************************************/
      if ( (now - tCheck) < (time_t)CheckInterval )
      {
         sleep_ew( 1000 );
         continue;
      }

/* Keep trying to get the smoke-alarm status until successful
   **********************************************************/

      rc = GetAlarmStatus( &currentAlarmStatus );

      if ( rc == 0 )   // Alarm status successfully obtained
      {
         time_t ltime;
         struct tm *timestruct;
         char   timestring[26];

/* Get the current GMT time
   ************************/
         time( &ltime );
         timestruct = gmtime( &ltime );
         strcpy( timestring, asctime(timestruct) );
         timestring[24] = 0;   // Strip off newline character

/* Report to statmgr when the smoke alarm goes on or off
   *****************************************************/
         if ( currentAlarmStatus == ALARM_TRIGGERED )
         {
            printf( "%s  Smoke Alarm Status: TRIGGERED\n", timestring );
            if ( previousAlarmStatus == ALARM_NOT_TRIGGERED )
            {
               sprintf( note, "ALARM. Smoke alarm triggered." );
               logit( "et", "%s\n", note );
               if ( ReportStatus( TypeError, ERR_REPORTSMOKE, note ) != PUT_OK )
                  logit( "et", "smoke_alarm: Error sending error message to ring.\n");
            }
            previousAlarmStatus = ALARM_TRIGGERED;
         }
         else      // Alarm currently not triggered
         {
            printf( "%s  Smoke Alarm Status: NOT TRIGGERED\n", timestring );
            if ( previousAlarmStatus == ALARM_TRIGGERED )
            {
               sprintf( note, "ALARM CLEARED. Smoke alarm no longer triggered." );
               logit( "et", "%s\n", note );
               if ( ReportStatus( TypeError, ERR_REPORTSMOKE, note ) != PUT_OK )
                  logit( "et", "smoke_alarm: Error sending error message to ring.\n");
            }
            previousAlarmStatus = ALARM_NOT_TRIGGERED;
         }
         tCheck = now;
      }
      else
      {
         logit( "et", "Error getting smoke alarm status.\n" );
         sleep_ew( 10000 );
      }
   }
}

