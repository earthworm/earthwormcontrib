/*
   smoke_alarm.h

   Constants used by smoke_alarm modules.
*/

#define ALARM_NOT_TRIGGERED 0
#define ALARM_TRIGGERED     1
