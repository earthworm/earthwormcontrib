
       /*******************************************************
        *                        port.c                       *
        *                                                     *
        *  Initialize a serial port.                          *
        *                                                     *
        *  This is Windows-specific code!!!                   *
        *******************************************************/


#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <earthworm.h>

static HANDLE handle;


int InitPort( int ComPort )
{
   COMMTIMEOUTS timeouts;
   DCB          dcb;                /* Data control block */
   BOOL         success;
   char         portName[20];

/* Open the com port
   *****************/
   if ( ComPort < 1 || ComPort > 9 )
   {
      logit( "et", "Invalid value of ComPort (%d). Exiting.\n", ComPort );
      exit( -1 );
   }
   sprintf( portName, "COM%d", ComPort );
   handle = CreateFile( portName,
                        GENERIC_READ|GENERIC_WRITE,
                        0, 0, OPEN_EXISTING,
                        FILE_ATTRIBUTE_NORMAL, 0 );

   if ( handle == INVALID_HANDLE_VALUE )
   {
      int rc = GetLastError();

      if ( rc == ERROR_ACCESS_DENIED )
         logit( "et", "CreateFile() error. Access denied to port: %s\n", portName );
      else
         logit( "et", "Error %d in CreateFile.\n", rc );
      return -1;
   }
   logit( "t", "CreateFile succeeded for comm port.\n" );

/* Get the current settings of the com port
   ****************************************/
   success = GetCommState( handle, &dcb );
   if ( !success )
   {
      logit( "et", "Error %d in GetCommState.\n", GetLastError() );
      return -1;
   }

/* Modify the baud rate, flow control, etc
   ***************************************/
   dcb.BaudRate        = 9600;         // Hardwired
   dcb.ByteSize        = 7;            // Hardwired
   dcb.Parity          = EVENPARITY;   // Hardwired
   dcb.StopBits        = ONESTOPBIT;   // Hardwired

   dcb.fOutxCtsFlow    = FALSE;        // Disable RTS/CTS flow control
   dcb.fOutxDsrFlow    = FALSE;        // Disable DTR/DSR flow control
   dcb.fDsrSensitivity = FALSE;        // Driver ignores DSR
   dcb.fOutX           = FALSE;        // Disable XON/XOFF out flow control
   dcb.fInX            = FALSE;        // Disable XON/XOFF in flow control
   dcb.fNull           = FALSE;        // Don't discard null bytes
   dcb.fAbortOnError   = TRUE;         // Abort rd/wr on error

/* Apply the new com port settings
   *******************************/
   success = SetCommState( handle, &dcb );
   if ( !success )
   {
      logit( "et", "Error %d in SetCommState.\n", GetLastError() );
      return -1;
   }
   logit( "t", "Comm parameters set.\n" );

/* Set ReadTotalTimeout to one second (hardwired)
   **********************************************/
   timeouts.ReadIntervalTimeout         = 0;
   timeouts.ReadTotalTimeoutMultiplier  = 0;
   timeouts.ReadTotalTimeoutConstant    = 1000;
   timeouts.WriteTotalTimeoutMultiplier = 0;
   timeouts.WriteTotalTimeoutConstant   = 0;
   SetCommTimeouts( handle, &timeouts );
   logit( "t", "Timeout parameters set.\n" );

   return 0;
}


        /*******************************************************
         *                    WritePort()                      *
         *                                                     *
         *  Write character string to the serial port.         *
         *                                                     *
         *  Returns  0 if all ok,                              *
         *          -1 if a write error occurred               *
         *******************************************************/

int WritePort( char str[] )
{
   BOOL  success;
   DWORD numWritten;
   int   nchar = strlen( str );

/* Purge the input com port buffer
   *******************************/
   success = PurgeComm( handle, PURGE_RXCLEAR );
   if ( !success )
   {
      logit( "et", "Error %d in PurgeComm.\n", GetLastError() );
      return -1;
   }

/* Write the string to the port
   ****************************/
   success = WriteFile( handle, str, nchar, &numWritten, 0 );
   if ( !success )
   {
      logit( "et", "Error %d in WriteFile.\n", GetLastError() );
      return -1;
   }
   return 0;
}


        /********************************************************
         *                     ReadPort()                       *
         *                                                      *
         *  Read nchar characters from the serial port and put  *
         *  them in string str.  If successful, null-terminate  *
         *  the string.                                         *
         *                                                      *
         *  Returns  0 if all characters were read.             *
         *          -1 if a read error or timeout occurred.     *
         ********************************************************/

int ReadPort( char str[], int nchar )
{
   DWORD numRead;

   ReadFile( handle, &str[0], nchar, &numRead, 0 );
   if ( numRead == (DWORD)nchar )
   {
      str[nchar] = 0;
      return 0;
   }
   return -1;        // Error or timeout
}
