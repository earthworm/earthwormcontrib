# copyevt.d
#
# Picks up files from a specified directory and reads the header. 
# Copy the file to other directory(s) based on type of file (data vs FT).
#
# If it has trouble interpreting the file, it saves it to subdir ./trouble. 

# Basic Module information
#-------------------------
MyModuleId        MOD_COPYEVT      # module id 
RingName          HYPO_RING	       # shared memory ring for output
HeartBeatInterval 30               # seconds between heartbeats to statmgr

LogFile           1                # 0 log to stderr/stdout only; 
                                   # 1 log to stderr/stdout and disk;
                                   # 2 log to disk module log only.

Debug             1                # 1=> debug output. 0=> no debug output

# Data file manipulation
#-----------------------
InDir      /home/picker/evtin/indir  # look for files in this directory
CheckPeriod     10                 # sleep this many seconds between looks
OpenTries       5                  # How many times we'll try to open a file 
OpenWait        200                # Milliseconds to wait between open tries

#
#            SaveTypeColumn =1 (data only); =2 (ft only); =3 (non-evt)
#Keyword     SaveType    Destination directory
#
OutDir       1 1 0    /home/picker/nsmp/indir    # Pass data and ft to k2faudit
OutDir       1 0 0    /home/picker/evt/indir     # Pass only data to sortevtfile
OutDir       0 1 0    /home/picker/evtin/ft      # Save the function tests
OutDir       0 0 1    /home/picker/evtin/notevt  # Save the non-evt files

#
# Peer (remote partner) heartbeat manipulation
#---------------------------------------------
PeerHeartBeatFile  terra1  HEARTBT.TXT  600 
                                   # PeerHeartBeatFile takes 3 arguments:
                                   # 1st: Name of remote system that is 
                                   #   sending the heartbeat files.
                                   # 2nd: Name of the heartbeat file. 
                                   # 3rd: maximum #seconds between heartbeat 
                                   #   files. If no new PeerHeartBeatFile arrives
                                   #   in this many seconds, an error message will
                                   #   be sent.  An "unerror message" will be
                                   #   sent after next heartbeat file arrives
                                   #   If 0, expect no heartbeat files.
                                   # Some remote systems may have multiple 
                                   # heartbeat files; list each one in a
                                   # seperate PeerHeartBeatFile command
                                   # (up to 5 allowed).
PeerHeartBeatInterval 30           # seconds between heartbeats to statmgr

#LogHeartBeatFile 1                # If non-zero, write contents of each
                                   #   heartbeat file to the daily log.


