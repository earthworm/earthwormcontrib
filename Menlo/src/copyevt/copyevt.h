/******************************************************************************/
/* copyevt.h:                                                                 */
/*                                                                            */
/* JHL May 2002                                                               */
/*                                                                            */
/******************************************************************************/

/* Define Strong Motion file types
 *********************************/
#define MAXTRACELTH 18000
#define NAM_LEN 100	          /* length of full directory name          */
#define BUFLEN  65000         /* define maximum size for an event msg   */

/* Region structs
 ****************/
#define MAX_INST   20
 
typedef struct _authreg
{
	char    RegionID[NAM_LEN];
	int     dataf;
	int     ftf;
	int     nonevtf;
	int     in_region;
	char    OutDir[NAM_LEN];
} AUTHREG;

/* Function prototypes
 *********************/
void config_me ( char * );
void ew_lookup ( void );
void ew_status ( unsigned char, short, char * );

int filter( FILE *fp, char *fname );
int read_tag( FILE *fp );
int read_head( FILE *fp );
int read_frame( FILE *fp, unsigned long *channels );

void logit( char *, char *, ... );   /* logit.c      sys-independent  */

