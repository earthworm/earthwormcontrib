/***************************************************************************
 * Subroutines for doing various trace calculations.                       *
 *                                                                         *
 ***************************************************************************/

#include <platform.h>
#include <errno.h>
#include "math.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>

#include <ws_clientII.h>
/*
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
*/
#include "shaker.h"

/* Functions in this source file 
 *******************************/
int  est_ground(Global *But, double *Data, Arkive *Ark);
void rdrvaa(double *, int, double, double, double, double *, double *, double *);
int  est_wood  (Global *But, double *Data, Arkive *Ark);
int  Get1ML(Global *But, Arkive *Ark, short method);
void Get_Average_Ml(Arkive *Ark);
void Get_Median_Ml (Arkive *Ark);
void hpsort(int n, double ra[]);

void DefinePZ(Arkive *Ark, Resp *PZ);
int WA_Amp(int npts, double dt, double *c1, cdouble *cw, Resp *PZ);
cdouble wood(double w);
int VelHi(int npts, double dt, double *c1, cdouble *cw, double fl, double fh, Resp *PZ);
void demean(double *A, int N);
void taper(double *a, int len, double percent);
void cooln(int nn, double *DATAI, int signi);
cdouble response(double w, int itype, Resp *PZ);
cdouble hibwth(double WP, double W0);
cdouble bpbwth(double WP, double W1, double W2);
void amaxper(int npts, double dt, double *fc, double *amaxmm, double *pmax, int *imax);
int  MaxAmp(Global *But, Arkive *Ark, double *Data);
int  ipow(int N);

cdouble Cadd(cdouble a, cdouble b);
cdouble Csub(cdouble a, cdouble b);
cdouble Cmul(cdouble a, cdouble b);
cdouble Complex(double re, double im);
cdouble Conjg(cdouble z);
cdouble Cdiv(cdouble a, cdouble b);
double Cabs(cdouble z);
cdouble RCmul(double x, cdouble a);
cdouble Cexp(cdouble z);
cdouble Csqrt(cdouble z);

cdouble   dw[MAXTRACELTH];
double    d2[MAXTRACELTH];

/*********************************************************************
     subroutine for estimation of ground motion

     input:  
             npts   - number of points in timeseries
             dt     - sample spacing in sec
             Data   - data array
     return:  
             iere   - error return code

    cdouble    dw[MAXTRACELTH];
*********************************************************************/
int est_ground(Global *But, double *Data, Arkive *Ark)
{
    char    whoami[50];
    int     k, iere, ind, ind2, npts, imax_vel, imax_acc, imax_dsp, imax_raw;
    int     ii, kk, kpts, id[4], npd[4][2], first, last;
    double  grav, fl, fh, dt, hstep, totint, a, tpi, omega, damp, rd, rv, aa;
    double  amax_vel, pmax_vel, amax_acc, pmax_acc, amax_dsp, pmax_dsp;
    double  amax_raw, pmax_raw, gd[4], sd[4], period;
    Resp    PZ;
    
    sprintf(whoami, " %s: %s: ", But->mod, "est_ground");

    DefinePZ(Ark, &PZ);        /*    Set up the response parameters for this trace    */

    gd[0] = 0.05;
    gd[1] = 0.10;
    gd[2] = 0.20;
    gd[3] = 0.50;
    
/*    acceleration of gravity in cm/s/s    */

    grav = 980;

/*    set variables for ground motion parameters     */

    ind = 21;
    ind2 = 6;
    fl = 0.1;
    fh = 25.0;
    tpi = 8.0*atan(1.0);

    k = Ark->Current;
    npts  = But->Npts;
    dt  = 1.0/But->samp_sec;
    first = (int)(Ark->A[k].PPick - 2.0 - Ark->EvntTime)*But->samp_sec;
    if(first<0 || first>=npts) first = 0;
    last = (int)(Ark->A[k].SPick + 10.0 - Ark->EvntTime)*But->samp_sec;
    if(last<first || last>=npts) last = npts;

/*    convert to cgs ground motion    */

    for(ii=0;ii<npts;ii++) Data[ii] = Data[ii]/PZ.sensitivity;

/*    find the raw maximum and its period    */

    amaxper(last-first, dt, &Data[first], &amax_raw, &pmax_raw, &imax_raw);

/*    remove instrument to velocity and hipass with a phaseless Butterworth    */

    iere = VelHi(npts, dt, Data, dw, fl, fh, &PZ);
    if (iere != 0)  return iere;
/*    find the velocity maximum (cm/s) and its period    */

    amaxper(last-first, dt, &Data[first], &amax_vel, &pmax_vel, &imax_vel);
    
/*    differentiate velocity to acceleration    */

    hstep = dt;
    for(kk=1;kk<npts-1;kk++) {
        d2[kk] = (Data[kk] - Data[kk-1])/hstep;
    }
    d2[0] = 0.0;

/*    find the spectral response    */

    damp = 0.05;
    kk = 0;
    period = 0.3;
    Ark->A[k].freq[kk] = 1.0/period;
    omega = tpi/period;
    rdrvaa(&d2[first], last-first-1, omega, damp, dt, &rd, &rv, &aa);
    Ark->A[k].RSA[kk] = Ark->A[k].SP1 = aa;
    kk += 1;

    period = 1.0;
    Ark->A[k].freq[kk] = 1.0/period;
    omega = tpi/period;
    rdrvaa(&d2[first], last-first-1, omega, damp, dt, &rd, &rv, &aa);
    Ark->A[k].RSA[kk] = Ark->A[k].SP2 = aa;
    kk += 1;

    period = 3.0;
    Ark->A[k].freq[kk] = 1.0/period;
    omega = tpi/period;
    rdrvaa(&d2[first], last-first-1, omega, damp, dt, &rd, &rv, &aa);
    Ark->A[k].RSA[kk] = Ark->A[k].SP3 = aa;
    kk += 1;

    Ark->A[k].nRSA = kk;

/*    find the acceleration maximum (cm/s/s) and its period    */

    amaxper(last-first-1, dt, &d2[first], &amax_acc, &pmax_acc, &imax_acc);

/*    since we are here, determine the duration of strong shaking    */

    for(kk=0;kk<=4;kk++) {
        id[kk] = npd[kk][1] = npd[kk][2] = 0;
        for(ii=1;ii<=npts-1;ii++) {
            a = fabs(d2[ii]/grav);
            if (a >= gd[kk]) {
                id[kk] = id[kk] + 1;
                if (id[kk] == 1) npd[kk][1] = ii;
                npd[kk][2] = ii;
            }
        }
        if (id[kk] != 0) {
            kpts = npd[kk][2] - npd[kk][1] + 1;
            sd[kk] = kpts*dt;
        } else {
            sd[kk] = 0.0;
        }
    }

/*    integrate velocity to displacement    */

    hstep = 0.5*dt;
    totint = 0.0;
    for(kk=1;kk<=npts-1;kk++) {
        totint = totint + (Data[kk] + Data[kk+1])*hstep;
        d2[kk] = totint;
    }

/*    find the displacement maximum (m) and its period    */

    amaxper(npts-first-1, dt, &d2[first], &amax_dsp, &pmax_dsp, &imax_dsp);

    if(But->Debug) {
    logit("e", "%s: amaxperR:. amaxmm: %f  pmax: %f  imax: %d \n", "est_ground", amax_raw, pmax_raw, imax_raw);
    logit("e", "%s: amaxperA:. amaxmm: %f  pmax: %f  imax: %d \n", "est_ground", amax_acc, pmax_acc, imax_acc);
    logit("e", "%s: amaxperV:. amaxmm: %f  pmax: %f  imax: %d \n", "est_ground", amax_vel, pmax_vel, imax_vel);
    logit("e", "%s: amaxperD:. amaxmm: %f  pmax: %f  imax: %d \n", "est_ground", amax_dsp, pmax_dsp, imax_dsp);
    }

    Ark->A[k].Pgd = amax_dsp;
    Ark->A[k].Pgv = amax_vel;
    Ark->A[k].Pga = amax_acc;
    
    Ark->A[k].tpgd = Ark->A[k].Stime + imax_dsp*dt;
    Ark->A[k].tpgv = Ark->A[k].Stime + imax_vel*dt;
    Ark->A[k].tpga = Ark->A[k].Stime + imax_acc*dt;

    if(Ark->A[k].Pgd < 0.0 || Ark->A[k].Pgd >  1000.0) {
    	Ark->A[k].Pgd = -1.0;
    	Ark->A[k].tpgd = 0.0;
    }
    if(Ark->A[k].Pgv < 0.0 || Ark->A[k].Pgv > 10000.0) {
    	Ark->A[k].Pgv = -1.0;
    	Ark->A[k].tpgv = 0.0;
    }
    if(Ark->A[k].Pga < 0.0 || Ark->A[k].Pga > 10000.0) {
    	Ark->A[k].Pga = -1.0;
    	Ark->A[k].tpga = 0.0;
    }
        
    return iere;
}    
    
      
void rdrvaa(double *acc, int na, double omega, double damp, double dt, 
            double *rd, double *rv, double *aa)
/*----------------- BEGIN RDRVAA -----------------------------
      subroutine rdrvaa(acc,na,omega,damp,dt,rd,rv,aa)
* This is a modified version of "Quake.For", originally
* written by J.M. Roesset in 1971 and modified by
* Stavros A. Anagnostopoulos, Oct. 1986.  The formulation is that of
* Nigam and Jennings (BSSA, v. 59, 909-922, 1969).  
* Dates: 02/11/00 - Modified by David M. Boore, based on RD_CALC

*   acc = acceleration time series
*    na = length of time series
* omega = 2*pi/per
*  damp = fractional damping (e.g., 0.05)
*    dt = time spacing of input
*    rd = relative displacement of oscillator
*    rv = relative velocity of oscillator
*    aa = absolute acceleration of oscillator

      dimension acc(*)
*/
{
    double    omt, d2, bom, d3, omd, om2, omdt, c1, c2, c3, c4, cc, ee;
    double    s1, s2, s3, s4, s5, a11, a12, a21, a22, b11, b12, b21, b22;
    double    y, ydot, y1, z, z1, z2, ra;
    int        i;
    
    omt = omega*dt;
    d2 = sqrt(1.0-damp*damp);
    bom = damp*omega;
    d3 = 2.0*bom;
    omd = omega*d2;
    om2 = omega*omega;
    omdt = omd*dt;
    c1 = 1.0/om2;
    c2 = 2.0*damp/(om2*omt);
    c3 = c1+c2;
    c4 = 1.0/(omega*omt);
    ee = exp(-damp*omt);
    cc = cos(omdt)*ee;
    s1 = sin(omdt)*ee/omd;
    s2 = s1*bom;
    s3 = s2 + cc;
    s4 = c4*(1.0-s3);
    s5 = s1*c4 + c2;
    
    a11 =  s3;          a12 = s1;
    a21 = -om2*s1;      a22 = cc - s2;
    
    b11 =  s3*c3 - s5;  b12 = -c2*s3 + s5 - c1;
    b21 = -s1+s4;       b22 = -s4;
    
    y = ydot = *rd = *rv = *aa = 0.0;
    for(i=0;i<na-1;i++) {
        y1   = a11*y + a12*ydot + b11*acc[i] + b12*acc[i+1];
        ydot = a21*y + a22*ydot + b21*acc[i] + b22*acc[i+1];
        y = y1;    /* y is the oscillator output at time corresponding to index i   */
        z = fabs(y);
        if (z > *rd) *rd = z;
        z1 = fabs(ydot);
        if (z1 > *rv) *rv = z1;
        ra = -d3*ydot -om2*y1;
        z2 = fabs(ra);
        if (z2 > *aa) *aa = z2;
    }
}

/*********************************************************************
      subroutine for estimation of wood-anderson amplitudes
 
*********************************************************************/
int est_wood(Global *But, double *Data, Arkive *Ark)
{
    double  dt, amaxmm, pmax;
    int     ii, k, npts, ierr, imax, first, last;
    Resp    PZ;

    k = Ark->Current;
    npts  = But->Npts;
    dt  = 1.0/But->samp_sec;
    first = 0;
    first = (int)(Ark->A[k].PPick - 2.0 - Ark->EvntTime)*But->samp_sec;
    if(first<0 || first>=npts) first = 0;
    last = (int)(Ark->A[k].SPick + 10.0 - Ark->EvntTime)*But->samp_sec;

    DefinePZ(Ark, &PZ);        /*    Set up the response parameters for this trace    */

/*    convert to cgs ground motion    */

    for(ii=0;ii<npts;ii++) Data[ii] = Data[ii]/PZ.sensitivity;

/*     remove instrument and reconvolve with Wood Anderson    */

    ierr = WA_Amp(npts, dt, Data, dw, &PZ);
    if (ierr != 0) return ierr;

/*     amaxmm is the WA amplitude in centimeters    */
/*     pmax is the period of the maximum    */

    amaxper(npts-first, dt, &Data[first], &amaxmm, &pmax, &imax);

    if(pmax < 0.0) pmax = -pmax;
    Ark->A[k].WA_Amp = amaxmm*10.0;    /*    cm -> mm    */
    Ark->A[k].xmag = log10(amaxmm/pmax);
    
    if(But->Debug) {
        logit("et", "%s: %d amaxper:  amaxmm: %f  pmax: %f  imax: %d xmag: %f\n", "est_wood", 
            PZ.iunit, amaxmm, pmax, imax, Ark->A[k].xmag);
    }
    
    Get1ML(But, Ark, 3);
    
    return 0;
}

/*********************************************************************
    Get1ML determines the ML for one channel.
    
*********************************************************************/
int Get1ML(Global *But, Arkive *Ark, short method)
{
    char    whoami[50];
    double  offset1, offset2, offset3, offset4, offset5, offset6;
    double  Amp, logA0, logA;
    int     k, error;
    
    sprintf(whoami, " %s: %s: ", But->mod, "Get1ML");
    error = 0;    
    k = Ark->Current;
    Amp = Ark->A[k].WA_Amp;
    Ark->A[k].Ml100 = 0.0;
    if(method==0) {
            /*    6th order polynomial fit to Richter's 
                tabulated values for -logA0.    */
        offset1 = Ark->A[k].Dist;
        offset2 = offset1*offset1;
        offset3 = offset1*offset2;
        offset4 = offset1*offset3;
        offset5 = offset1*offset4;
        offset6 = offset1*offset5;
        logA0 = 1.197561977861021 + 
                4.0338766324244707e-2 * offset1 - 
                3.45672100609e-4 * offset2 + 
                1.627064449e-6 * offset3 - 
                3.944965e-9 * offset4 + 
                4.729e-12 * offset5 - 
                2.0e-15 * offset6;
        logA  = log10(Amp/2.0);
        Ark->A[k].Ml100 = logA - logA0;
    }
    
    if(method==1) {
            /*    Kanamori's exponential fit to Richter's 
                tabulated values for -logA0.    
                
                q(r) = c*r^^-n*exp(-kr),    r = sqrt(dist^2+href^2)
                
                c = 0.49710, n = 1.2178, k = 0.0053km^-1, href = 8km    
                
                Kanamori et al., 1993, BSSA, 83, 2, 330-346*/
        offset1 = sqrt(Ark->A[k].Dist*Ark->A[k].Dist + Ark->EvntDepth*Ark->EvntDepth);
        logA0 = log10(0.49710*pow(offset1,-1.2178)*exp(-0.0053*offset1));
        
        logA  = log10(Amp/2.0);
        Ark->A[k].Ml100 = logA - logA0;
    }
        
    if(method==2) {
            /*    Hutton&Boore, 1987, BSSA, 77, 6, 2074-2094.    
                (for Southern California)
                -logA = 1.110*log(r/100) + 0.00189*(r-100) + 3.0;    10<= r <=700    
                        r = sqrt(dist^2+depth^2)
                */
        offset1 = sqrt(Ark->A[k].Dist*Ark->A[k].Dist + Ark->EvntDepth*Ark->EvntDepth);
        logA0 = 1.110*log10(offset1/100.0) + 0.00189*(offset1-100.0) + 3.0;
        logA0 = -logA0;
        
        logA  = log10(Amp/2.0);
        Ark->A[k].Ml100 = logA - logA0;
    }
        
    if(method==3) {
            /*    Bakun&Joyner, 1984, BSSA, 74, 1827-1843.    
                (for Central California)
                -logA = 1.000*log(r/100) + 0.00301*(r-100) + 3.0;    0<= r <=400    
                        r = sqrt(dist^2+depth^2)
                */
        offset1 = sqrt(Ark->A[k].Dist*Ark->A[k].Dist+Ark->EvntDepth*Ark->EvntDepth);
        logA0 = 1.000*log10(offset1/100.0) + 0.00301*(offset1-100.0) + 3.0;
        logA0 = -logA0;
        
        logA  = log10(Amp/2.0);
        logA  = log10(Amp);
        Ark->A[k].Ml100 = logA - logA0;
    }
        
    if(method==4) {
            /*    Chavez&Priestley, 1985, BSSA, 75, 1583-1598.    
                (for Great Basin, Western U.S.)
                -logA = 1.000*log(r/100) + 0.0069*(r-100) + 3.0;     0<= r <= 90
                -logA = 0.830*log(r/100) + 0.0026*(r-100) + 3.0;    90<= r <=600
                        r = sqrt(dist^2+depth^2)
                */
        offset1 = sqrt(Ark->A[k].Dist*Ark->A[k].Dist+Ark->EvntDepth*Ark->EvntDepth);
        if(offset1<90.0)
            logA0 = 1.000*log10(offset1/100.0) + 0.0069*(offset1-100.0) + 3.0;
        else
            logA0 = 0.830*log10(offset1/100.0) + 0.0026*(offset1-100.0) + 3.0;
        logA0 = -logA0;
        
        logA  = log10(Amp/2.0);
        Ark->A[k].Ml100 = logA - logA0;
    }
        
    if(method==5) {
            /*    Greenhalgh&Singh, 1986, BSSA, 76, 757-769.    
                (for Western Australia)
                -logA = 1.100*log(d/100) + 0.00013*(d-100) + 3.03;    40<= d <=600    
                        
                */
        offset1 = Ark->A[k].Dist;
        logA0 = 1.100*log10(offset1/100.0) + 0.00013*(offset1-100.0) + 3.03;
        logA0 = -logA0;
        
        logA  = log10(Amp/2.0);
        Ark->A[k].Ml100 = logA - logA0;
    }
        
    if(method==6) {
            /*    Fujino&Inoue, 1985, written communication to Hutton&Boore.    
                (for Japan)
                -logA = 1.098*log(d/100) + 0.0003*(d-100) + 3.0;        
                        
                */
        offset1 = Ark->A[k].Dist;
        logA0 = 1.098*log10(offset1/100.0) + 0.0003*(offset1-100.0) + 3.0;
        logA0 = -logA0;
        
        logA  = log10(Amp/2.0);
        Ark->A[k].Ml100 = logA - logA0;
    }
    
    if(But->Debug) {
logit("e", "%s %s %s Offset: %6.2f Amp:%f -logA0:%lf logA:%lf ML:%f\n", 
        whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Dist, Amp, -logA0, logA, Ark->A[k].Ml100);
    }
        
    return error;
}


/*********************************************************************
    Get_Average_Ml does the final ML determination.
        Note: Using median rather than mean would deselect errors.
    
*********************************************************************/
void Get_Average_Ml(Arkive *Ark)
{
    double    sumML;
    int       i, j, nval;

    Ark->Ml = 0.0;
    sumML = 0.0;
    nval = 0;
    for(i=0;i<Ark->InList;i++) {
        j = Ark->index[i];
        if(Ark->A[j].Ml100 > 0.0 && Ark->A[j].reject==0) {
    /*        if(Ark->A[j].StaList == '*') {    */
                sumML += Ark->A[j].Ml100;
                nval += 1;
    /*        }    */
        }
    }
    Ark->Ml_sta = nval;
    Ark->Ml = sumML/nval;
}


/*******************************************************************************
 *    Get_Median_Ml finds the median of the Mls of the individual channels.    *
 *                                                                             *
 *******************************************************************************/

void Get_Median_Ml(Arkive *Ark)
{
    double  ds[2000];
    int     i, j;

    Ark->Ml_Med = 0.0;
    i = 0;
    for(j=0;j<Ark->InList;j++) {
        if(Ark->A[j].Ml100 > 0.0 && Ark->A[j].reject==0) {
    /*        if(Ark->A[j].StaList == '*') {    */
                ds[i++] = Ark->A[j].Ml100;
    /*        }    */
        }
    }
    
    hpsort(i, ds);
    
    Ark->Ml_Med = ds[i/2];
    logit("e", " %d %f\n", i, Ark->Ml_Med);
}


/*******************************************************************************
 *    hpsort performs a sifting sort in place of the n members of the double   *
 *    array ra.  This is modified from the version in Recipes in C to make it  *
 *    0-based as in C rather than 1-based as in fortran!!                      *
 *******************************************************************************/

void hpsort(int n, double ra[])
{
    int   i, j, k, ir;
    double rra;
    
    if(n<2) return;
    k = n >> 1;
    ir = n-1;
        
    for(;;) {
        if(k > 0) rra = ra[--k];
        else {
            rra = ra[ir];
            ra[ir] = ra[0];
            if(--ir == 0) {
                ra[0] = rra;
                break;
            }
        }
        i = k; j = k + k + 1;
        while(j<=ir) {
            if(j<ir && ra[j]<ra[j+1]) j++;
            if(rra<ra[j]) {
                ra[i] = ra[j];
                i = j;
                j = j + j + 1;
            } else break;
        }
        ra[i] = rra;
    }
}


/********************************************************************
     DefinePZ sets up the poles&zeros for this channel.

 ********************************************************************/
void DefinePZ(Arkive *Ark, Resp *PZ)
{
    int        i, k;

    k = Ark->Current;
    switch(Ark->A[k].Inst_type) {
        case 0:   /* _UNKNOWN */
                PZ->a0 = 1.9059e13;
                PZ->a0 = 1.0;
                PZ->a0 = 4.0e6;
                PZ->np = 10;    PZ->nz = 10;
                PZ->Pole[0] = Complex(-0.01818,  0.013636);
                PZ->Pole[1] = Complex(-0.01818, -0.013636);
                PZ->Pole[2] = Complex(-0.002159,  0.0);
                PZ->Pole[3] = Complex(-0.002159,  0.0);
                PZ->Pole[4] = Complex(-1.0,  0.0);
                PZ->Pole[5] = Complex(-1.0,  0.0);
                PZ->Pole[6] = Complex(-0.173955,  0.419942);
                PZ->Pole[7] = Complex(-0.173955, -0.419942);
                PZ->Pole[8] = Complex(-0.419955,  0.173925);
                PZ->Pole[9] = Complex(-0.419955, -0.173925);    /**/
                for(i=0;i<PZ->nz;i++) 
                    PZ->Zero[i] = Complex( 0.00000000e+00,  0.00000000e+00);
            break;
        case 1:    /* CalNet analog w/ L4-c  */
                PZ->a0 = 1.9059e13;
                PZ->np = 10;    PZ->nz = 5;
                
                PZ->a0 /= 12.56628;    /*    L4-c empirically determined    */
                
                PZ->Pole[0] = Complex(-5.02650,  3.7699);
                PZ->Pole[1] = Complex(-5.02650, -3.7699);
                PZ->Pole[2] = Complex(-0.59690,  0.0);
                PZ->Pole[3] = Complex(-0.59690,  0.0);
                PZ->Pole[4] = Complex(-276.460,  0.0);
                PZ->Pole[5] = Complex(-276.460,  0.0);
                PZ->Pole[6] = Complex(-48.0915,  116.0973);
                PZ->Pole[7] = Complex(-48.0915, -116.0973);
                PZ->Pole[8] = Complex(-116.1007,  48.0832);
                PZ->Pole[9] = Complex(-116.1007, -48.0832);    /**/
                for(i=0;i<PZ->nz;i++) 
                    PZ->Zero[i] = Complex( 0.00000000e+00,  0.00000000e+00);
            break;
        case 2:  /* _DST  */
                PZ->a0 = 2.359642709e15;    /*    amplitude constant (7*157.08)    */
                PZ->np = 9;    PZ->nz = 3;
                PZ->np = 7;    PZ->nz = 0;
                
                PZ->Pole[0] = Complex(-157.08,  0.0);
                PZ->Pole[1] = Complex(-141.51,  68.18);
                PZ->Pole[2] = Complex(-141.51, -68.18);
                PZ->Pole[3] = Complex(-97.938,  122.81);
                PZ->Pole[4] = Complex(-97.938, -122.81);
                PZ->Pole[5] = Complex(-34.953,  153.14);
                PZ->Pole[6] = Complex(-34.953, -153.14);
                PZ->Pole[7] = Complex(-5.02650,  3.7699);    /*    L4-c    */
                PZ->Pole[8] = Complex(-5.02650, -3.7699);
                for(i=0;i<PZ->nz;i++) 
                    PZ->Zero[i] = Complex( 0.00000000e+00,  0.00000000e+00);
            break;
        case 3:  /* RefTek */
                PZ->a0 = 1.0;
                PZ->np = 0;    PZ->nz = 0;
                
                PZ->Pole[0] = Complex(-157.08,  0.0);
                PZ->Pole[1] = Complex(-141.51,  68.18);
                PZ->Pole[2] = Complex(-141.51, -68.18);
                PZ->Pole[3] = Complex(-97.938,  122.81);
                PZ->Pole[4] = Complex(-97.938, -122.81);
                PZ->Pole[5] = Complex(-34.953,  153.14);
                PZ->Pole[6] = Complex(-34.953, -153.14);
                for(i=0;i<PZ->nz;i++) 
                    PZ->Zero[i] = Complex( 0.00000000e+00,  0.00000000e+00);
            break;
        case 4:    /* _NANOMETRICS */
                PZ->a0 = 1.0;
                PZ->np = 0;    PZ->nz = 0;
            break;
        case 5:    /*    Quanterra    */
                PZ->a0 = 1.0;
                PZ->np = 0;    PZ->nz = 0;
            break;
        case 6:    /*    K2    */
                PZ->a0 = 1.0;
                PZ->np = 0;    PZ->nz = 0;
            break;
        default:
                PZ->a0 = 1.0;
                PZ->np = 0;    PZ->nz = 0;
            break;
    }
    
    PZ->sensitivity = Ark->A[k].sensitivity;
    PZ->iunit = Ark->A[k].Sens_unit;
}


/********************************************************************
     subroutine for Fourier Domain operations in process_waveform

     input:  
             npts   - number of points in timeseries
             dt     - sample spacing in sec
             c1    - input and output timeseries
             cw    - input and output dummy array for storing spectra
             fl    - input low  freq corner of Butterworth filters (input in Hz)
             fh    - input high freq corner of Butterworth filters (input in Hz)
 ********************************************************************/
int VelHi(int npts, double dt, double *c1, cdouble *cw, double fl, double fh, Resp *PZ)
{
    char     *whoami = " VelHi: ";
    cdouble   caw, crw;
    double    percent, pi, tpi, dw, f, wl, wh;
    int       ii, jj, n2, itype;
    
    pi  = 4.0*atan(1.0);
    tpi = 2.0*pi;
/*     detrend/deman    */
/*    detrend(c1, npts, dt, c1)    */
    demean(c1, npts);

/*     taper    */
    percent =  5.0;
    taper(c1, npts, percent);

/*     compute the number of points    */
/*     n2 is the next power of 2 greater than npts    */

    n2 = ipow(npts);
    
/*     zero excess points    */
/*     form complex array    */
/*     forward transform    */

    for(ii=npts;ii<n2;ii++) c1[ii] = 0.0;
    for(ii=0;ii<n2;ii++) {cw[ii].r = c1[ii]; cw[ii].i = 0.0;}
    cooln(n2, (double *)cw, -1);

/*    for(ii=0;ii<n2;ii++) cw[ii] = RCmul(0.5, cw[ii]);    */

/*      remove instrument to velocity        */

    dw = tpi / (2.0*n2 * dt);
    itype = 2;
    
    for(ii=1;ii<n2/2;ii++) {
        f = 2.0*ii*dw;
        crw = response(f, itype, PZ);
        jj = n2 - ii;
        cw[ii] = Cdiv(cw[ii], crw);
        cw[jj] = Conjg(cw[ii]);
    }
    cw[0] = Complex(0.0,0.0);
    cw[n2/2-1].r = sqrt(cw[n2/2-1].r*cw[n2/2-1].r + cw[n2/2-1].i*cw[n2/2-1].i);
    cw[n2/2-1].i = 0.0;
    
/*     filter operations    */

    fl = 1.0;    fh = 25.0;
    wl = fl*tpi;    wh = fh*tpi;
    for(ii=0;ii<n2/2;ii++) {
        f = 2.0*ii*dw;
    /*  caw = hibwth (f, wl);   */
        caw = bpbwth (f, wl, wh);
        caw = Cmul(caw, Conjg(caw));
        caw = Csqrt(caw);
        jj = n2 - ii;
        cw[ii] = Cmul(caw, cw[ii]);
        cw[jj] = Conjg(cw[ii]);
    }
    cw[n2/2-1].r = sqrt(cw[n2/2-1].r*cw[n2/2-1].r + cw[n2/2-1].i*cw[n2/2-1].i);
    cw[n2/2-1].i = 0.0;

/*     inverse transform filtered function    */

    cooln(n2, (double *)cw, 1);
    
    for(ii=0;ii<n2;ii++) cw[ii] = RCmul(1.0/n2, cw[ii]);
    
    for(ii=0;ii<npts;ii++) c1[ii] = cw[ii].r;  
    
    return 0;
}


/********************************************************************
     subroutine for Fourier Domain operations in process_waveform

     input:  
             npts   - number of points in timeseries
             dt     - sample spacing in sec
             c1    - input and output timeseries
             cw    - input and output dummy array for storing spectra
 ********************************************************************/
int WA_Amp(int npts, double dt, double *c1, cdouble *cw, Resp *PZ)
{
    char     *whoami = " WA_Amp: ";
    cdouble   caw, crw;
    double    percent, pi, tpi, dw, f, x1, y1, x0, y0;
    int       ii, jj, j, n2, itype;
    
    pi  = 4.0*atan(1.0);
    tpi = 2.0*pi;
/*     detrend/deman    */
/*    detrend(c1, npts, dt, c1)    */
    demean(c1, npts);

/*     taper    */
    percent =  5.0;
    taper(c1, npts, percent);

/*     compute the number of points    */

    n2 = ipow(npts);

/*     zero excess points    */
/*     form complex array    */
/*     forward transform    */

    for(ii=npts;ii<n2;ii++) c1[ii] = 0.0;
    for(ii=0;ii<n2;ii++) {cw[ii].r = c1[ii]; cw[ii].i = 0.0;}
    cooln(n2, (double *)cw, -1);

     for(ii=0;ii<n2;ii++) cw[ii] = RCmul(0.5, cw[ii]);     

/*     Perform instrument operations    */

/*     frequency increment & number of frequency samples    */

    dw = tpi / (2.0*n2 * dt);

    itype = 1;
    for(ii=1;ii<n2/2;ii++) {
        f = 2.0*ii*dw;
        crw = response(f, itype, PZ);
        caw = wood(f);        /*    convolve with the Wood Anderson response    */
        jj = n2 - ii;
        cw[ii] = Cmul(cw[ii], Cdiv(caw, crw));
        cw[jj] = Conjg(cw[ii]);
    }
    cw[0] = Complex(0.0,0.0);
    cw[n2/2-1].r = sqrt(cw[n2/2-1].r*cw[n2/2-1].r + cw[n2/2-1].i*cw[n2/2-1].i);
    cw[n2/2-1].i = 0.0;

    
/*     filter operations    */

    for(ii=0;ii<n2/2;ii++) {
        f = 2.0*ii*dw;
        caw = hibwth (f, 0.5*tpi);
        caw = Cmul(caw, Conjg(caw));
        caw = Csqrt(caw);
        jj = n2 - ii;
        cw[ii] = Cmul(caw, cw[ii]);
        cw[jj] = Conjg(cw[ii]);
    }
    cw[n2/2-1].r = sqrt(cw[n2/2-1].r*cw[n2/2-1].r + cw[n2/2-1].i*cw[n2/2-1].i);
    cw[n2/2-1].i = 0.0;
    
/*     inverse transform filtered function    */

    cooln(n2, (double *)cw, 1);
    
    for(ii=0;ii<n2;ii++) cw[ii] = RCmul(1.0/n2, cw[ii]);
    
    for(ii=0;ii<npts;ii++) c1[ii] = cw[ii].r; 
    
/*  one-pole Butterworth   */ 
    x1 = y1 = 0.0;
    for(j=0;j<npts;j++) {
        x0 = x1;
        x1 = c1[j]/1.003141603;
        y0 = y1;
        c1[j] = y1 = (x1 - x0) + 0.9937364715*y0;
    }

    return 0;
}


/********************************************************************
     compute nominal Wood Anderson response
 
     w is angular frequency in rad/s

      complex*8 cz(2),cp(2), ck, s, t

     poles and zero of Wood Anderson response
     --from the file seisp.stf
 ********************************************************************/
cdouble wood(double w)
{
    int        ii;
    cdouble    cz[2], cp[2], ck, s;
    
    for(ii=0;ii<2;ii++) cz[ii] = Complex(0.0,0.0);
    cp[0] = Complex(-0.541925E+01, -0.568479E+01);
    cp[1] = Complex(-0.541925E+01,  0.568479E+01);

    s = Complex(0.0, w);

    ck = Complex(2.08000E+03, 0.0);    /*    let's make no assumptions about units    */

/*    loop over zeros and poles    */
    for(ii=0;ii<2;ii++) {
    /*    ck = ck * ((s - cz[ii])/(s - cp[ii]))    */
        ck = Cmul(ck,Cdiv(Csub(s,cz[ii]),Csub(s,cp[ii])));
    }

    return ck;
}


/********************************************************************
  $$$$$ CALLS NO OTHER ROUTINE $$$$$
 
    DEMEAN REMOVES THE MEAN FROM THE N POINT SERIES STORED IN
    ARRAY A.

 ********************************************************************/
void demean(double *A, int N)
{
    int        i;
    double    xm;
    
    xm = 0.0;
    for(i=0;i<N;i++) xm = xm + A[i];
    xm = xm/N;
    for(i=0;i<N;i++) A[i] = A[i] - xm;
}


/********************************************************************
   taper INPUT SERIES WITH A percent% COS**2 TAPER
   percent=100 IS A FULL TAPER
 ********************************************************************/
void taper(double *a, int len, double percent)
{
    double    fls, flm, pi, arg, result;
    int        i;

    pi = 3.1415926535898;
    fls = 0.005*percent*len;
    flm = len - fls;
    for(i=0;i<len;i++) {
        result = 1.0;
        if(i < fls) {
            arg = pi*i/fls;
            result = 0.5*(1.0 - cos(arg));
        }
        else if(i > flm) {
            arg = pi*(len-i)/(len-flm);
            result = 0.5*(1.0 - cos(arg));
        }
        
        a[i] = a[i]*result;
    }
    a[0] = a[len-1] = 0.0;
}


/********************************************************************
 *  COOL fft                                                        *
 *                                                                  *
 *   nn - number of data points                                     *
 *   DATAI - array of 2*nn values. (real, imag)                     *
 ********************************************************************/
void cooln(int nn, double *DATAI, int signi)
{
    int        i, j, m, n, mmax, istep;
    double    temp, tempr, tempi, theta, sinth, wstpr, wstpi, wr, wi;
    
    n = nn*2;
    j = 0;

    for(i=0;i<n;i+=2) {
        if(i < j) {
            SWAP(DATAI[j], DATAI[i]);
            SWAP(DATAI[j+1], DATAI[i+1]);
        }
        m = n/2;
        do {
            if(j < m) break;
            j = j-m;
            m = m/2;
        } while(m >= 2);
        j += m;
    }

    mmax = 2;
    while(1) {
        if(mmax >= n) break;
        istep = 2*mmax;
        theta = signi*6.28318531/(float)(mmax);
        sinth = sin(theta/2.0);
        wstpr = -2.0  *sinth*sinth;
        wstpi =  sin(theta);
        wr = 1.0;
        wi = 0.0;
        for(m=0;m<mmax;m+=2) {
            for(i=m;i<n;i+=istep) {
                j = i + mmax;
                tempr = wr*DATAI[j]-wi*DATAI[j+1];
                tempi = wr*DATAI[j+1]+wi*DATAI[j];
                DATAI[j] = DATAI[i]-tempr;
                DATAI[j+1] = DATAI[i+1]-tempi;
                DATAI[i]   = DATAI[i]+tempr;
                DATAI[i+1] = DATAI[i+1]+tempi;
            }
            tempr = wr;
            wr = wr*wstpr - wi*wstpi    + wr;
            wi = wi*wstpr + tempr*wstpi + wi;
        }
        mmax = istep;
    }
}


/********************************************************************
     resp calculates the actual response curve for a
       given set of poles and zeros

     w is circular frequency in rad/sec
     itype specifies the desired response

     itype = 0: default units
             1: displacement response
             2: velocity response
             3: acceleration response

     the poles and zeros are expected to be stored in the
       Resp structure PZ

    a0 is the digital sensitivity * gain
    
     assumes the exp(-iwt) transform convention

 ********************************************************************/
cdouble response(double w, int itype, Resp *PZ)
{
      int        ii, jj, iaction, icom;
      cdouble    s, t, sp;

/*     decide what to do    */

    if (itype == 0)    iaction = 0;                    /*    don't do anything    */
    else            iaction = PZ->iunit - itype;    /*    figure it out    */

/*     check for zero divide    */

    if ((w == 0.0) && (iaction != 0)) w = 0.0001;
    s = Complex(0.0,w);

/*     scale the instrument response by the digitial sensitivity and gain    */

    t = Complex(PZ->a0, 0.0);

    icom = PZ->np<PZ->nz? PZ->np:PZ->nz;

/*     loop over common zeros and poles    */

    for(ii=0;ii<icom;ii++) {
        t = Cmul(t, Cdiv(Csub(s, PZ->Zero[ii]), Csub(s, PZ->Pole[ii])));
    }

/*     loop over extra poles or zeros    */

    if (PZ->np > icom) {
        for(ii=icom;ii<PZ->np;ii++) t = Cdiv(t, Csub(s, PZ->Pole[ii]));
    } else {
        for(ii=icom;ii<PZ->nz;ii++) t = Cmul(t, Csub(s, PZ->Zero[ii]));
    }

/*     determine type of response    */

    if (iaction != 0) {
        sp = Complex(1.0, 0.0);
        s = Complex(0.0,w);
        if(iaction<0) s = Cdiv(sp, s);
        jj = abs(iaction);
        if(jj == 1) sp = s;
            else 
        if(jj == 2) sp = Cmul(s, s);
        t = Cmul(t, sp);
    }
    return t;
}

/********************************************************************

   hibwth RETURNS THE COMPLEX FORWARD TRANSFER FUNCTION OF A SIX POLE
   BUTTERWORTH FILTER AS A FUNCTION OF ANGULAR FREQUENCY W.  THIS
   FILTER IS A HIGH PASS WITH A CORNER AT W0 AND A ROLLOFF OF ABOUT
   36 DB/OCTAVE.  A FOURIER TRANSFORM CONVENTION OF
   F(W) = INT F(T)*EXP(-IWT)*DT IS ASSUMED.

                                                     -ALL
 ********************************************************************/
cdouble hibwth(double WP, double W0)
{
    cdouble    a1, a2, a3, s, t;
    double    w;
    
    a1 = Complex(3.863703305,0.0);
    a2 = Complex(7.464101615,0.0);
    a3 = Complex(9.141620173,0.0);
    
    w = (1.E-2>WP)? 1.E-2:WP;
    s = Complex(0.0,w/W0);
    s = Cdiv(Complex(1.0,0.0),s);
    /*
    s = s*(a1+s*(a2+s*(a3+s*(a2+s*(a1+s))))) + 1
    */
    s = Cadd(Cmul(s,Cadd(a1,Cmul(s,Cadd(a2,Cmul(s,Cadd(a3,Cmul(s,Cadd(a2,Cmul(s,Cadd(a1,s)))))))))),Complex(1.0,0.0));

    t = Cdiv(Complex(1.0,0.0),s);
    return t;
}

/********************************************************************

   BPBWTH RETURNS THE COMPLEX FORWARD TRANSFER FUNCTION OF A SIX POLE
   BUTTERWORTH FILTER AS A FUNCTION OF ANGULAR FREQUENCY WP.  THIS
   FILTER IS A BAND PASS WITH CORNERS AT W1 AND W2 AND A ROLLOFF OF
   36 DB/OCTAVE.  A FOURIER TRANSFORM CONVENTION OF
   F(W) = INT F(T)*EXP(-IWT)*DT IS ASSUMED.

                                                     -ALL
 ********************************************************************/
cdouble bpbwth(double WP, double W1, double W2)
{
    cdouble    a1, a2, a3, s, t;
    double    w, w0;
    
    a1 = Complex(3.863703305,0.0);
    a2 = Complex(7.464101615,0.0);
    a3 = Complex(9.141620173,0.0);
    
    w = (1.E-2>WP)? 1.E-2:WP;
    w0 = (w*w-W1*W2)/(w*(W2-W1));
    s = Complex(0.0,w0);
    /*
    s = s*(a1+s*(a2+s*(a3+s*(a2+s*(a1+s))))) + 1
    */
    s = Cadd(Cmul(s,Cadd(a1,Cmul(s,Cadd(a2,Cmul(s,Cadd(a3,Cmul(s,Cadd(a2,Cmul(s,Cadd(a1,s)))))))))),Complex(1.0,0.0));

    t = Cdiv(Complex(1.0,0.0),s);
    return t;
}

/********************************************************************
     compute maximum amplitude and its associated period

     input:
             npts   - number of points in timeseries
             dt     - sample spacing in sec
             fc     - input timeseries
     output:
             amaxmm - raw maximum
             pmax   - period of maximum
             imax   - index of maxmimum point

 ********************************************************************/
void amaxper(int npts, double dt, double *fc, double *amaxmm, double *pmax, int *imax)
{
    double    amax, x, pp, pm, mean, frac;
    int       i, j, jmax;
    
    *imax = jmax = 1;
    amax = *amaxmm = *pmax = mean = 0.0;
    amax = *amaxmm = *pmax = fabs(fc[0]);
    if(npts < 5) {
    	logit("e", "%s: Too few pts in time series %d\n", "amaxper", npts);
    	return;
    }
    if(dt < 0.0001 || dt > 10) {
    	logit("e", "%s: Bad dt %f\n", "amaxper", dt);
    	return;
    }
    for(i=0;i<npts;i++) {
        mean = mean + fc[i]/npts;
        x = fabs(fc[i]);
        if (x > amax) { jmax = i; amax = x; }
    }

/*     compute period of maximum    */

    pp = pm = 0.0;
    if (fc[jmax] > mean) {
        j = jmax+1;
        while(fc[j] > mean && j < npts) {
            pp += dt;
            j  += 1;
        }
     /*   frac = dt*(mean-fc[j-1])/(fc[j]-fc[j-1]);
        frac = 0.0;
        pp = pp + frac;  */
        j = jmax-1;
        while(fc[j] > mean && j > 0) {
            pm += dt;
            j  -= 1;
        }
     /*   frac = dt*(mean-fc[j+1])/(fc[j]-fc[j+1]);
        frac = 0.0;
        pm = pm + frac;  */
    } else {
        j = jmax+1;
        if(fc[j] < mean && j < npts) {
            pp += dt;
            j  += 1;
        }
      /*  frac = dt*(mean-fc[j-1])/(fc[j]-fc[j-1]);
        frac = 0.0;
        pp = pp + frac;  */
        j = jmax-1;
        if(fc[j] < mean && j > 0) {
            pm += dt;
            j  -= 1;
        }
      /*  frac = dt*(mean-fc[j+1])/(fc[j]-fc[j+1]);
        frac = 0.0;
        pm = pm + frac;  */
    }

    *imax = jmax;
    *pmax = 2.0*(pm+pp);
    *amaxmm = amax;

    return;
}

/*********************************************************************
    MaxAmp determines the maximum deflection.
    
*********************************************************************/
int MaxAmp(Global *But, Arkive *Ark, double *Data)
{
    char    whoami[50];
    double  min, max;
    int     i, k, error;
    
    sprintf(whoami, " %s: %s: ", But->mod, "MaxAmp");
    error = 0;    
    k = Ark->Current;
    min = max = Data[0];
    for(i=1;i<But->Npts;i++) {
        if(Data[i]<min) min = Data[i];
        if(Data[i]>max) max = Data[i];
    }
    Ark->A[k].Amp = max - min;
    Ark->A[k].Amp = fabs(max)>fabs(min)? fabs(max):fabs(min);
    
    if(But->Debug) {
logit("e", "%s npts:%d min:%f max:%f Amp:%f\n", whoami,
        But->Npts, min, max, Ark->A[k].Amp);
    }
    
    return error;
}


/*********************************************************************
  FINDS POWER OF 2 GREATER THAN N
*********************************************************************/
int ipow(int N)
{
    int    in;
    
    in = 2;
    while(in < N) in = in*2;
        
    return in;
}


/*********************************************************************
   Package of complex subroutines for C.
   
   Implemented in C on 14 June 1999 by J. Luetgert.

*********************************************************************/
cdouble Csqrt(cdouble z)
{
    double        x, y, w, r;
    cdouble    c;
    
    if((z.r == 0.0) && (z.i == 0.0)) {
        c = Complex(0.0, 0.0);
    } else {
        x = fabs(z.r);
        y = fabs(z.i);
        if(x >= y) {
            r = y/x;
            w = sqrt(x)*sqrt(0.5*(1.0+sqrt(1.0+r*r)));
        } else {
            r = x/y;
            w = sqrt(y)*sqrt(0.5*(r+sqrt(1.0+r*r)));
        }
        if(z.r >= 0.0) {
            c = Complex(w, z.i/(2.0*w));
        } else {
            c.i = (z.i >= 0) ? w : -w;
            c.r = z.i/(2.0*(c.i));
        }
    }
    return c;
}

cdouble Cadd(cdouble a, cdouble b)
{
    cdouble    c;
    c.r = a.r + b.r;
    c.i = a.i + b.i;
    return c;
}


cdouble Csub(cdouble a, cdouble b)
{
    cdouble    c;
    c.r = a.r - b.r;
    c.i = a.i - b.i;
    return c;
}


cdouble Cmul(cdouble a, cdouble b)
{
    cdouble    c;
    c.r = a.r*b.r - a.i*b.i;
    c.i = a.i*b.r + a.r*b.i;
    return c;
}


cdouble Complex(double re, double im)
{
    cdouble    c;
    c.r = re;
    c.i = im;
    return c;
}



cdouble Conjg(cdouble z)
{
    cdouble    c;
    c.r = z.r;
    c.i = -z.i;
    return c;
}


cdouble Cdiv(cdouble a, cdouble b)
{
    cdouble    c;
    double    r, den;
    if(fabs(b.r) >= fabs(b.i)) {
        r = b.i/b.r;
        den = b.r + r*b.i;
        c.r = (a.r + r*a.i)/den;
        c.i = (a.i - r*a.r)/den;
    } else {
        r = b.r/b.i;
        den = b.i + r*b.r;
        c.r = (a.r*r + a.i)/den;
        c.i = (a.i*r - a.r)/den;
    }
    return c;
}


double Cabs(cdouble z)
{
    double    x, y, ans, temp;
    
    x = fabs(z.r);
    y = fabs(z.i);
    if(x == 0.0)
        ans = y;
    else if(y == 0.0)
        ans = x;
    else if(x > y) {
        temp = y/x;
        ans = x*sqrt(1.0+temp*temp);
    } else {
        temp = x/y;
        ans = y*sqrt(1.0+temp*temp);
    }
    return ans;
}

cdouble RCmul(double x, cdouble a)
{
    cdouble    c;
    c.r = x*a.r;
    c.i = x*a.i;
    return c;
}


cdouble Cexp(cdouble z)
{
    cdouble    c;
    c.r = exp(z.r)*cos(z.i);
    c.i = exp(z.r)*sin(z.i);
    return c;
}


