/*************************************************************************
 *      shaker.c:
 * Takes arkive messages, buffers them in an in-memory fifo; 
 * pulls them out, and starts parametric processing thread for each message.
 *
Thread structure:
 * Main:
 * Starts the ProcessorMgr thread. Sets up the in-memory queue buffer. 
 * Loops reading hyosum messages, and stuffs them into the queue.
 * 
 * ProcessorMgr:
 * Pulls messages from the queue.  
 * It gets a pointer to some structure, containing the arkive record.
 * It calls processing routines to do all the seismology.
 * Jim 12/17/97
**************************************************************************/

#include <platform.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include <decode.h>
#include <kom.h>
#include <transport.h>
#include "mem_circ_queue.h" 
#include <rw_strongmotion.h>
#include <rw_strongmotionII.h>

#include <ws_clientII.h>

#include "shaker.h" 

/* Functions in this source file
 *******************************/
thr_ret ProcessorMgr( void*);
int ProcessorThread( char *);
void config_me(Global *But,  char * ); /* reads configuration (.d) file via Carl's routines  */
void ewmod_status( unsigned char, short, char *); /* sends heartbeats and errors into ring   */
void lookup_ew ( void );     /* Goes from symbolic names to numeric values, via earthworm.h  */
void HeartBeat( void );
int pager1( char  *note, char  *person );
void Write_MsgII(Arkive *Ark);

/* Shared memory
 ***************/
static  SHM_INFO  InRegion;              /* public shared memory for receiving arkive messages */
static  SHM_INFO  OutRegion;             /* shared memory region to use for output */

/* Things to lookup in the earthworm.h table with getutil.c functions
 ********************************************************************/
static long          InRingKey;         /* key of transport ring for input   */
static long          OutRingKey;        /* key of transport ring for output  */
static unsigned char InstId;            /* local installation id             */
static unsigned char MyModId;           /* our module id                     */
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeStrongMotion;
static unsigned char TypeStrongMotion2;

/* Things to read from configuration file
 ****************************************/
static char InRingName[20];             /* name of transport ring for i/o          */
static char OutRingName[20];            /* name of transport ring for output       */
static char MyModuleId[20];             /* module id for this module               */
static int  LogSwitch;                  /* 0 if no logging should be done to disk  */
static long MaxMsgSize = MAX_BYTES_PER_EQ; /* max size for input/output msgs       */
static int  MaxMessages;                /* max messages in output circular buffer  */
static time_t heartBeatInterval = 5;    /* seconds between heartbeats              */
static int  Debugger;                   /* debug flag                              */
static int  Flush;                      /* Flush flag                              */
static int  howlong = -1;
Global      Btlr;                       /* Private area for the threads            */
Arkive      Ark;

#define  MAXLOGO   2
static MSG_LOGO  GetLogo[MAXLOGO];      /* array for requesting module,type,instid */
short     nLogo;                        /* number of logos read from ring          */

pid_t MyPid;              /* Our own pid, sent with heartbeat for restart purposes */

/* Variables for talking to statmgr
 **********************************/
time_t    timeNow;
time_t    timeLastBeat;                 /* time last heartbeat was sent            */
char      Text[150];

/* Error words used by eqwaves
 *****************************/
#define   ERR_MISSMSG       0
#define   ERR_TOOBIG        1
#define   ERR_NOTRACK       2
#define   ERR_INTERNAL      3
#define   ERR_QUEUE         4

/* The in-memory fifo buffer 
****************************/
QUEUE OutQueue;               /* from queue.h, queue.c; sets up linked list via malloc and free */
static long num_in_Q;         /* Number of msgs currently in the queue */ 
static char OutMsg[MAX_BYTES_PER_EQ]; 
static char ArcMsgBufA[MAX_BYTES_PER_EQ]; /* character string to hold event message */ 
                                          /* MAX_BYTES_PER_EQ is from earthworm.h */
/* Server thread stuff
**********************/
static unsigned int tidProcessorMgr;              /* id of dispatcher of socket-serving threads */
#define THREAD_STACK  8096

int Jeeves(Global *But, int, char *);

/* Other globals
 ***************/
char      progname[] = "shaker";

/**********************************************************************************
 *  main( int argc, char **argv )                                                 *
 **********************************************************************************/

int main( int argc, char **argv )
{
    char        fname[100], string[500], subname[] = "Main";
    int         resA, waitA, ret;
    long        recsizeA;   /* size of retrieved message */
    MSG_LOGO    reclogoA;   /* logo of retrieved message */
    float       mag;
	FILE        *fp;

        /* Check command line arguments
         ******************************/
    if ( argc != 2 ) {
        fprintf( stderr, "Usage: %s <configfile>\n", progname );
        exit( 0 );
    }
    Debugger = num_in_Q = 0;
    Flush = 1;

    /* Get our own Pid for restart purposes
    ***************************************/
    MyPid = getpid();
    if( MyPid == -1 ) {
      fprintf( stderr,"%s: Cannot get pid. Exiting.\n", progname);
      return -1;
    }

        /* Read the configuration file(s)
         ********************************/
    config_me(&Btlr, argv[1] );

        /* Look up important info from earthworm.h tables
         ************************************************/
    lookup_ew();

        /* Initialize name of log-file & open it
         ***************************************/
    logit_init( argv[1], (short) MyModId, 512, LogSwitch );
    logit( "" , "%s: %s: Read command file <%s>\n",  progname, subname, argv[1] );
    
        /* DEBUG: dump variables to stdout
        **********************************/
    if(LogSwitch) {
        logit("","%s: %s: MyModuleId:  %s \n",   progname, subname, MyModuleId);
        logit("","%s: %s: InRingName:  %s \n",   progname, subname, InRingName);
        logit("","%s: %s: GetLogo: %u %u %u\n",  progname, subname, GetLogo[0].instid, GetLogo[0].mod, GetLogo[0].type);
        logit("","%s: %s: MaxMsgSizeo: %d \n",   progname, subname, MaxMsgSize);
        logit("","%s: %s: MaxMessages: %d \n",   progname, subname, MaxMessages);
    }

        /* Create a Mutex to control access to queue
        ********************************************/
    CreateMutex_ew();
    if(Debugger) logit("","%s: %s: Created Mutex\n",   progname, subname);

        /* Allocate space for  messages
        *********************************/
    if(LogSwitch) {
        logit("","%s: %s: ArcMsgSize: %d \n",  progname, subname, sizeof(ArcMsgBufA));
        logit("","%s: %s: OutMsgSize: %d \n",  progname, subname, sizeof(OutMsg));
    }

        /* Initialize the message queue
        *******************************/
    initqueue ( &OutQueue, (unsigned long)MaxMessages, (unsigned long)MaxMsgSize);   

        /* Start the Dispatcher thread which pulls 
         * messages from fifo, and starts processing threads
         ***************************************************/
    if ( StartThread( ProcessorMgr, THREAD_STACK, &tidProcessorMgr ) == -1 ) {
        logit( "e", "%s: %s: Error starting ProcessorMgr thread. Exiting.\n",  progname, subname );
        return( -1 );
    }
    logit("","%s: %s: ProcessorMgr started.\n",  progname, subname);

        /* Attach to public HYPO shared memory ring
        ******************************************/
    tport_attach( &InRegion, InRingKey );
    if (LogSwitch) logit( "", "%s: %s: Attached to public memory region <%s>: %ld.\n", 
                              progname, subname, InRingName, InRegion.key );
#ifdef _SOLARIS                    /* SOLARIS ONLY:                         */
    if (Debugger) logit( "e", "%s: %s: Attached to public memory region <%s> key: %ld mid: %ld sid: %ld.\n", 
                              progname, subname, InRingName, InRegion.key, InRegion.mid, InRegion.sid );
    if (Debugger) logit( "e", "%s: %s: nbytes: %ld keymax: %ld keyin: %ld keyold: %ld flag: %d.\n", 
                              progname, subname, InRegion.addr->nbytes, 
                              InRegion.addr->keymax, InRegion.addr->keyin, 
                              InRegion.addr->keyold, InRegion.addr->flag );
#endif                             /*                                       */

        /* ------------- See if there is an arc file to process --------------*/
    if  ( Btlr.arcfileflg ) {
	/* Change to the directory with the input files
	 ***********************************************/
		if( chdir_ew( Btlr.indir ) == -1 ) {
			logit( "e", "%s: %s: GetFromDir directory <%s> not found; "
			         "exiting!\n", progname, subname, Btlr.indir );
			exit(-1);
		}
		if(Debugger)logit("et","%s: %s: changed to directory <%s>\n", progname, subname,Btlr.indir);
    }

        /* Send first heartbeat
        **********************/
    time(&timeLastBeat);
    HeartBeat();


        /* ------------------------ flush the ring -------------------------*/
        /* -------------- to prevent re-processing of old events -----------*/
        /* ------------------------ after a restart ------------------------*/
    if(Flush) {
        do {
            resA = tport_getmsg( &InRegion, GetLogo, nLogo, 
                                &reclogoA, &recsizeA, ArcMsgBufA, sizeof(ArcMsgBufA)-1 );
        
        } while(resA != GET_NONE);
    }

        /* ------------------------ start working loop -------------------------*/
    waitA = 0;
    while(1) {
        do {    /* ------ start of message processing loop: take all messages, 
                            'till transport says "no more" ---*/
                       
        /* -------------- see if a termination has been requested ----------*/
            if ( tport_getflag( &InRegion ) == TERMINATE ||
            	 tport_getflag( &InRegion ) == MyPid ) {      /* detach from shared memory regions*/
                sleep_ew( 500 );       /* wait around while butlers finish */
                tport_detach( &InRegion );
                logit("et", "%s: %s: Termination requested; exiting.\n",  progname, subname );
                fflush(stdout);
                exit( 0 );
            }

        /* --------------------- send prog's heartbeat ---------------------*/
            HeartBeat();

                        /* Get and process the next arkive message from shared memory */
                        /***************************************************************/
            resA = tport_getmsg( &InRegion, GetLogo, nLogo, 
                                &reclogoA, &recsizeA, ArcMsgBufA, sizeof(ArcMsgBufA)-1 );
            switch(resA) {
                int ret;

                case GET_NONE:          /* no more new messages     */
                    if (LogSwitch && waitA>=howlong && howlong>0) {
                       logit("et","%s: %s: No new messages from %s in %ld passes.\n",  
                                  progname, subname, InRingName, waitA);
                       waitA = 0;
                    }
                    waitA += 1;
                    break;

                case GET_TOOBIG:   /* next message was too big. Complain and try again */
                    sprintf( Text, "Retrieved msg[%ld] (i%u m%u t%u) too big for ArcMsgBufA[%d]",
                             recsizeA, reclogoA.instid, reclogoA.mod, reclogoA.type, sizeof(ArcMsgBufA)-1 );
                    ewmod_status( TypeError, ERR_TOOBIG, Text );
                    break;

                case GET_MISS:     /* got a msg, but missed some */
                    sprintf( Text,"Missed msg(s)  i%u m%u t%u  region:%ld.",
                            reclogoA.instid, reclogoA.mod, reclogoA.type, InRegion.key);
                    ewmod_status( TypeError, ERR_MISSMSG, Text );
                    goto PutInQueue;

                case GET_NOTRACK: /* got a msg, but can't tell if any were missed  */
                    sprintf( Text,"Msg received (i%u m%u t%u); transport.h NTRACK_GET exceeded",
                            reclogoA.instid, reclogoA.mod, reclogoA.type );
                    ewmod_status( TypeError, ERR_NOTRACK, Text );
                    goto PutInQueue;

                case GET_OK:
                /* Stuff the message into the fifo
                **********************************/
  PutInQueue:              
                    waitA = 0;
                        if(Debugger) {
                             logit("et", "%s: %s: Retrieved msg[%ld] (i%u m%u t%u)\n      ArcMsgBufA: %.200s\n",
                                progname, subname, recsizeA, reclogoA.instid, reclogoA.mod, reclogoA.type, ArcMsgBufA );
                        }
                        mag = (float)((DECODE( ArcMsgBufA+67, 2, atof ))/10.0);
                        if((num_in_Q < 20 || mag > 2.5) &&
                           (num_in_Q < 10 || mag > 2.0)) {
                            if(num_in_Q <  5 || mag > 1.0) {
                                RequestMutex();
                                ret=enqueue( &OutQueue, ArcMsgBufA, recsizeA, reclogoA ); /* put it into the queue */
                                if(Debugger) 
                                    logit("et","%s: %s: Retrieved and Queued message.\n",  progname, subname); /*debug */
                                num_in_Q += 1;
                                ReleaseMutex_ew();
                                if ( ret!= 0 ) {       
                                    if (ret==-2) { /* Serious: quit */
                                        sprintf(Text,"internal queue error %d. Terminating/n", ret);
                                                    ewmod_status( TypeError, ERR_QUEUE, Text );
                                        exit(-1);
                                    }
                                    if (ret==-1) sprintf(Text,"queue cant allocate memory. Lost message\n");
                                    if (ret==-3) sprintf(Text,"Circular queue lapped. Message lost\n");
                                    ewmod_status( TypeError, ERR_QUEUE, Text );
                                    continue;
                                }
                            }
                        }
                    
                    break;
            }
        } while (resA !=GET_NONE );      /* ------- end of message processing loop for A ------------- */

        /* --------------------- send prog's heartbeat ---------------------*/
        HeartBeat();

        /* ------------- See if there is an arc file to process --------------*/
        if  ( Btlr.arcfileflg ) {

	     /* Get a file name
	      ******************/    
			ret = GetFileName( fname );
	    
			if( ret != 1 ) {  /* Files found; Process */
		        if(Debugger)logit("et","%s: %s: got file name <%s>\n",progname, subname,fname);
				sleep_ew( 1*1000 );
				fp = fopen( fname, "rb" );
				if ( fp != NULL ) {
				    if(strstr(fname, ".arc") != 0) {
				    	ArcMsgBufA[0] = 0L;
				    	while(fgets(string, 480, fp)!=0L){
				    		strcat(ArcMsgBufA, string);
				    	}
				    	recsizeA = strlen(ArcMsgBufA);
                        if(Debugger) {
                             printf("%s: %s: Retrieved msg[%ld] (i%u m%u t%u)\n      ArcMsgBufA: %.200s\n",
                                progname, subname, recsizeA, reclogoA.instid, reclogoA.mod, reclogoA.type, ArcMsgBufA );
                        }
                        mag = (float)((DECODE( ArcMsgBufA+67, 2, atof ))/10.0);
                        if((num_in_Q < 20 || mag > 2.5) &&
                           (num_in_Q < 10 || mag > 2.0)) {
                            if(num_in_Q <  5 || mag > 1.0) {
                                RequestMutex();
                                ret=enqueue( &OutQueue, ArcMsgBufA, recsizeA, reclogoA ); /* put it into the queue */
                                if(Debugger > 1) 
                                    logit("","%s: %s: Retrieved and Queued message.\n",  progname, subname); /*debug */
                                num_in_Q += 1;
                                ReleaseMutex_ew();
                                if ( ret!= 0 ) {       
                                    if (ret==-2) { /* Serious: quit */
                                        sprintf(Text,"internal queue error %d. Terminating/n", ret);
                                                    ewmod_status( TypeError, ERR_QUEUE, Text );
                                        exit(-1);
                                    }
                                    if (ret==-1) sprintf(Text,"queue cant allocate memory. Lost message\n");
                                    if (ret==-3) sprintf(Text,"Circular queue lapped. Message lost\n");
                                    ewmod_status( TypeError, ERR_QUEUE, Text );
                                    continue;
                                }
                            }
                        }
				    }
					fclose( fp );
				}
	            if( remove( fname ) != 0 ) {
	                logit("et","%s: %s: error deleting file: %s\n", progname, subname, fname);
	            } else  {
	                if(Debugger)logit("e","%s: %s: Removed %s \n \n", progname, subname, fname );
	            }
			}
	   

        }
        sleep_ew( 500 );       /* wait around for more summary lines   */

    }   /*------------------------------end of working loop------------------------------*/
}



/************************** ProcessorMgr Thread *********************************
 * Waits for for a hypo message to appear in the fifo buffer. When it           *
 * does, starts a processing thread, gives it a pointer to station list         *
 * and hypo message, and wishes them luck. Keeps track of active threads,       * 
 * and does not start too many threads.                                         *
 ********************************************************************************/

thr_ret ProcessorMgr( void *dummy )
{
    char        subname[] = "ProcessorMgr";
    long        ret, recsize, num_processed;
    MSG_LOGO    reclogo;                /* logo of retrieved message  */

    if (LogSwitch) logit("", "%s: %s: Beginning ProcessorMgr.\n",  progname, subname);

    num_processed = 0;      
    while (1) {   /* Pull a message from the queue *
                   *********************************/
        RequestMutex();
        ret=dequeue( &OutQueue, OutMsg, &recsize, &reclogo);
        ReleaseMutex_ew();
        if(ret == 0 && Debugger ) { /*  0 means got a msg */
   /*       logit("et","%s: %s: Processor Thread Retrieved msg[%ld] (i%u m%u t%u) OutMsg: %.100s\n",
                     progname, subname, recsize, reclogo.instid, reclogo.mod, reclogo.type, OutMsg );
                     */
        }
        if(ret < 0 ) {  /* -1 means empty queue sleep a while */
            sleep_ew(100);
            continue;
        }
        num_in_Q -= 1;
        if( recsize >= MAX_BYTES_PER_EQ) {
            logit("et","%s: %s: Fatal error. Retrieved message is too long: %.200s\n",  progname, subname, OutMsg);
            recsize = MAX_BYTES_PER_EQ;
        }
        OutMsg[recsize] = 0;        /* Make sure the message is null terminated */
   
           /* Start the Processing *
            ************************/
        if(num_in_Q>0 && LogSwitch) {
            logit("e","%s: %s: Starting Processing. There are %ld Msgs in Queue. \n",  progname, subname,num_in_Q);
        }
        
        ProcessorThread(OutMsg);
    }    
}


/********************************************************************
 *  The Processor thread starts up the actual process.              *
 *  Rsec4 is unthreaded, so this is just a pass-through.            *
 *                                                                  *
 ********************************************************************/

int ProcessorThread( char *OutMsg )
{
    char    subname[] = "ProcessorThread";
    int     myThrdId;    
    
    myThrdId = 0;
    Jeeves(&Btlr, myThrdId, OutMsg);  /*  Insert processing code here */

    if(Debugger) logit("et","%s: %s: terminating\n\n", progname, subname);
    return 0;
}

/****************************************************************************
 *      config_me() process command file using kom.c functions              *
 *                       exits if any errors are encountered                *
 ****************************************************************************/

void config_me(Global *But,   char* configfile )
{
    char    whoami[50], *com, *str;
    char    init[20];       /* init flags, one byte for each required command */
    int     ncommand;       /* # of required commands you expect */
    int     nmiss;          /* number of required commands that were missed   */
    int     i, j, n, nfiles, success;

    sprintf(whoami, "%s: %s: ", progname, "config_me");
        /* Set to zero one init flag for each required command
         *****************************************************/
    nLogo    = 0;
    ncommand = 13;
    for(i=0; i<ncommand; i++ )  init[i] = 0;
    
    strcpy(But->PGADir , "" );
    But->TimeDomain = 0;
    
    But->MinSize    =   0.0;
    But->MaxDist    = 100;
    But->wsTimeout  = WSTIMEOUT*1000;
    But->RetryCount =  10;
    But->Debug      = 0;
    But->WSDebug    = 0;
    But->ncomp      = 0;
    But->nStaDB     = 0;
    But->nServer    = 0;
    But->arcfileflg = 0;

    But->ntargets   = 0;
    But->nnet       = 0;
    But->nPager     = 0;
    
        /* Open the main configuration file
         **********************************/
    nfiles = k_open( configfile );
    if(nfiles == 0) {
        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, configfile );
        exit( -1 );
    }

        /* Process all command files
         ***************************/
    while(nfiles > 0) {  /* While there are command files open */
        while(k_rd())  {      /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

                /* Ignore blank lines & comments
                 *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

                /* Open a nested configuration file
                 **********************************/
            if( com[0] == '@' ) {
                success = nfiles+1;
                nfiles  = k_open(&com[1]);
                if ( nfiles != success ) {
                    fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                    exit( -1 );
                }
                continue;
            }

                /* Process anything else as a command
                 ************************************/
  /*0*/
            if( k_its("LogSwitch") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/
            else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModuleId , str );
                init[1] = 1;
            }
  /*2*/
            else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( InRingName, str );
                init[2] = 1;
            }

  /*3*/
            else if( k_its("HeartBeatInt") ) {
                heartBeatInterval = k_long();
                init[3] = 1;
            }

                /* Retrieve installation and module to get messages from  */

  /*4*/
            else if( k_its("GetSumFrom") ) {
                if ( nLogo >= MAXLOGO ) {
                    fprintf(stderr, "%s Too many <GetSumFrom> commands in <%s>", whoami, configfile );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAXLOGO );
                    exit( -1 );
                }
                if( ( str=k_str() ) ) {
                    if( GetInst( str, &GetLogo[nLogo].instid ) != 0 ) {
                        fprintf( stderr, "%s Invalid installation name <%s>", whoami, str );
                        fprintf( stderr, " in <GetSumFrom> cmd; exiting!\n" );
                        exit( -1 );
                    }
                }
                if( ( str=k_str() ) ) {
                    if( GetModId( str, &GetLogo[nLogo].mod ) != 0 ) {
                        fprintf( stderr, "%s Invalid module name <%s>", whoami, str );
                        fprintf( stderr, " in <GetSumFrom> cmd; exiting!\n" );
                        exit( -1 );
                    }
                }
                if( ( str=k_str() ) ) {
                    if( GetType( str, &GetLogo[nLogo].type ) != 0 ) {
                        fprintf( stderr, "%s Invalid message type <%s>", whoami, str );
                        fprintf( stderr, "; exiting!\n" );
                        exit( -1 );
                    }
                }
                nLogo++;
                init[4] = 1;
            }

                /* get memory fifo buffer parameters
                ************************************/
  /*5*/
            else if( k_its("MaxMsgSize") ) {
                MaxMsgSize = k_int();
                init[5] = 1;
            }

  /*6*/
            else if( k_its("MaxMessages") ) {
                MaxMessages = k_int();
                init[6] = 1;
            }

         /* wave server addresses and port numbers to get trace snippets from
          *******************************************************************/
/*7*/
            else if( k_its("WaveServer") ) {
                if ( But->nServer >= MAX_WAVESERVERS ) {
                    fprintf(stderr, "%s Too many <WaveServer> commands in <%s>", 
                             whoami, configfile );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAX_WAVESERVERS );
                    exit( -1 );
                }
                if( (long)(str=k_str()) != 0 )  strcpy(But->wsIp[But->nServer],str);
                if( (long)(str=k_str()) != 0 )  strcpy(But->wsPort[But->nServer],str);
                if( (long)(str=k_str()) != 0 )  strcpy(But->wsComment[But->nServer],str);
                But->nServer++;
                init[7]=1;
            }

          /* get the target directory(s)
        ************************************/
/*8*/
            else if( k_its("Target") ) {
                if ( But->ntargets >= MAX_TARGETS ) {
                    fprintf(stderr, "%s Too many <Target> commands in <%s>", 
                             whoami, configfile );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAX_TARGETS );
                    exit( -1 );
                }
                if( (long)(str=k_str()) != 0 )  {
                    j = strlen(str);   /* Make sure directory name has proper ending! */
                    if( str[j-1] != '/' ) strcat(str, "/");
                    strcpy(But->target[But->ntargets], str);
                    /**/
                    i = 0;
                    while(str[i] != '@') {
                        But->UserID[But->ntargets][i] = str[i];
                        i++;
                    }
                    But->UserID[But->ntargets][i] = 0;
                    But->Passwd[But->ntargets][0] = 0;
                    j = 0;
                    i++;
                    while(str[i] != '@' && str[i] != ':') {
                        But->Host[But->ntargets][j++] = str[i++];
                    }
                    But->Host[But->ntargets][j] = 0;
                    strcpy(But->Directory[But->ntargets], &str[i+1]);
                    
                }
                But->ntargets += 1;
                init[8] = 1;
            }

        /* get Gif directory path/name
        *****************************/
/*9*/
            else if( k_its("GifDir") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf(stderr,"%s Fatal error. Gif directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    exit( -1 );
                }
                j = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[j-1] != '/' ) strcat(str, "/");
                if(str) strcpy( But->GifDir , str );
                init[9] = 1;
            }


          /* get the component types
        ****************************/
/*10*/
            else if( k_its("Component") ) {
                if ( But->ncomp >= MAX_COMPS ) {
                    fprintf(stderr, "%s Too many <Component> commands in <%s>", 
                             whoami, configfile );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAX_COMPS );
                    exit( -1 );
                }
                if( (long)(str=k_str()) != 0 )  {
                    strcpy(But->Comp[But->ncomp], str);
                }
                But->ncomp += 1;
                init[10] = 1;
            }


          /* get the network codes
        **************************/
/*11*/
            else if( k_its("Network") ) {
                if ( But->nnet >= MAX_NETS ) {
                    fprintf(stderr, "%s Too many <Network> commands in <%s>", 
                             whoami, configfile );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAX_NETS );
                    exit( -1 );
                }
                if( (long)(str=k_str()) != 0 )  {
                    strcpy(But->Net[But->nnet], str);
                }
                But->nnet += 1;
                init[11] = 1;
            }


                /* get station list path/name
                *****************************/
/*12*/
            else if( k_its("StationList") ) {
                if ( But->nStaDB >= MAX_STADBS ) {
                    fprintf( stderr, "%s Too many <StationList> commands in <%s>", 
                             whoami, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAX_STADBS );
                    return;
                }
                str = k_str();
                if( (int)strlen(str) >= STALIST_SIZ) {
                    fprintf( stderr, "%s Fatal error. Station list name %s greater than %d char.\n", 
                            whoami, str, STALIST_SIZ);
                    exit(-1);
                }
                if(str) strcpy( But->stationList[But->nStaDB] , str );
                But->nStaDB++;
                init[12] = 1;
            }


        /* get Prefix for deletable files on the target
        *****************************/
            else if( k_its("Prefix") ) {
                str = k_str();
                if( (int)strlen(str) >= PRFXSZ) {
                    fprintf( stderr, "%s Fatal error. File prefix %s greater than %d char.\n",
                        whoami, str, PRFXSZ);
                    exit( -1 );
                }
                if(str) strcpy(But->Prefix, str );
            }



               /* optional commands */

        /* get Arcfile input directory path/name
        ****************************************/
            else if( k_its("ArcDir") ) {
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf(stderr,"%s Fatal error. Arc directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    exit( -1 );
                }
                j = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[j-1] != '/' ) strcat(str, "/");
                if(str) strcpy( But->indir , str );
                But->arcfileflg = 1;
            }
            

               /* optional commands */
            else if( k_its("Pager") ) {
                if ( But->nPager >= MAX_PAGERS ) {
                    fprintf(stderr, "%s Too many <Pager> commands in <%s>", 
                             whoami, configfile );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAX_PAGERS );
                    exit( -1 );
                }
                if( (long)(str=k_str()) != 0 )  {
                    strcpy(But->person[But->nPager], str);
                }
                But->nPager += 1;
            }

            else if( k_its("TimeDomain") ) But->TimeDomain = 1;   /* optional commands */

            else if( k_its("MaxDist") ) {  /*optional command*/
                But->MaxDist = k_int();
                if(But->MaxDist>1000) But->MaxDist = 1000; 
                if(But->MaxDist<  10) But->MaxDist =   10; 
            }

            else if( k_its("RetryCount") ) {  /*optional command*/
                But->RetryCount = k_int();
                if(But->RetryCount>100) But->RetryCount = 100; 
                if(But->RetryCount<  1) But->RetryCount =   1; 
            }

            else if( k_its("MinSize") ) {  /*optional command*/
                But->MinSize = k_val();
                if(But->MinSize>10.0) But->MinSize = 10.0; 
                if(But->MinSize< 0.0) But->MinSize =  0.0; 
            }

            else if( k_its("Passes") ) {
                howlong = k_int();
            }

        /* get the timeout interval in seconds
        **************************************/
            else if( k_its("wsTimeout") ) {
                But->wsTimeout = k_int()*1000;
            }
            

            else if( k_its("UseDST") )         But->UseDST = 1;       /* optional commands */

            else if( k_its("Debug") ) {  /*optional command*/
                But->Debug = k_int();
                if( k_err() ) But->Debug = 99;
                Debugger = But->Debug;
            }
            
            else if( k_its("WSDebug") ) But->WSDebug = 1;   /* optional commands */

            else if( k_its("NoFlush") ) {  /*optional command*/
                Flush = 0;
            }

            else if( k_its("PGADir") ) {        /* get PGA directory path/name */
                str = k_str();
                if( (int)strlen(str) >= GDIRSZ) {
                    fprintf(stderr,"%s Fatal error. PGA directory name %s greater than %d char.\n",
                        whoami, str, GDIRSZ);
                    exit( -1 );
                }
                j = strlen(str);   /* Make sure directory name has proper ending! */
                if( str[j-1] != '/' ) strcat(str, "/");
                if(str) strcpy( But->PGADir , str );
            }


                /* At this point we give up. Unknown thing.
                *******************************************/
            else {
                fprintf(stderr, "%s <%s> Unknown command in <%s>.\n",
                         whoami, com, configfile );
                continue;
            }

                /* See if there were any errors processing the command
                 *****************************************************/
            if( k_err() ) {
               fprintf( stderr, "%s Bad <%s> command  in <%s>; exiting!\n",
                             whoami, com, configfile );
               exit( -1 );
            }
        }
        nfiles = k_close();
    }

        /* After all files are closed, check init flags for missed commands
         ******************************************************************/
    nmiss = 0;
    for(i=0;i<ncommand;i++)  if( !init[i] ) nmiss++;
    if ( nmiss ) {
        fprintf( stderr, "%s ERROR, no ",  whoami);
        if ( !init[0] )  fprintf( stderr, "<LogSwitch> "       );
        if ( !init[1] )  fprintf( stderr, "<MyModuleId> "      );
        if ( !init[2] )  fprintf( stderr, "<RingName> "        );
        if ( !init[3] )  fprintf( stderr, "<HeartBeatInt> "    );
        if ( !init[4] )  fprintf( stderr, "<GetSumFrom> "      );
        if ( !init[5] )  fprintf( stderr, "<MaxMsgSize> "      );
        if ( !init[6] )  fprintf( stderr, "<MaxMessages> "     );
        if ( !init[7] )  fprintf( stderr, "<WaveServer> "      );
        if ( !init[8] )  fprintf( stderr, "<Target> "          );
        if ( !init[9] )  fprintf( stderr, "<GifDir> "          );
        if ( !init[10])  fprintf( stderr, "<Component> "       );
        if ( !init[11])  fprintf( stderr, "<Network> "         );
        if ( !init[12])  fprintf( stderr, "<StationList> "     );
        fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
        exit( -1 );
    }
}


/**********************************************************************************
 * ewmod_status() builds a heartbeat or error msg & puts it into shared memory    *
 **********************************************************************************/
void ewmod_status( unsigned char type,  short ierr,  char *note )
{
    char        subname[] = "ewmod_status";
    MSG_LOGO    logo;
    char        msg[512];
    long        size;
    time_t      t;

    logo.instid = InstId;
    logo.mod    = MyModId;
    logo.type   = type;

    time( &t );
    if( type == TypeHeartBeat ) {
        sprintf( msg, "%ld %ld\n\0", t,MyPid);
    } else
    if( type == TypeError ) {
        sprintf( msg, "%ld %hd %s\n", t, ierr, note );
        logit( "t", "%s: %s:  %s\n",  progname, subname, note );
    }
    /**/
    else if( type == TypeStrongMotion ) {
        strcpy( msg, note );
    }
    else if( type == TypeStrongMotion2 ) {
        strcpy( msg, note );
    }
    

    size = strlen( msg );   /* don't include the null byte in the message */
    if( tport_putmsg( &InRegion, &logo, size, msg ) != PUT_OK ) {
        if( type == TypeHeartBeat ) {
            logit("et","%s: %s:  Error sending heartbeat.\n",  progname, subname );
        } else
        if( type == TypeError ) {
            logit("et","%s: %s:  Error sending error:%d.\n",  progname, subname, ierr );
        }
        /**/
        else if( type == TypeStrongMotion ) {
            logit("et","%s: %s:  Error sending strong motion msg:%d.\n",  progname, subname, ierr );
        }
        else if( type == TypeStrongMotion2 ) {
            logit("et","%s: %s:  Error sending strong motion msg2:%d.\n",  progname, subname, ierr );
        }
        
    }
}


/****************************************************************************
 *  lookup_ew( ) Look up important info from earthworm.h tables           *
 ****************************************************************************/
void lookup_ew( )
{
    char        subname[] = "lookup_ew";
    
        /* Look up keys to shared memory regions
         ***************************************/
    if( (InRingKey = GetKey(InRingName)) == -1 ) {
        fprintf( stderr,
                "%s: %s: Invalid ring name <%s>; exiting!\n",  progname, subname, InRingName );
        exit( -1 );
    }

        /* Look up installation Id
         *************************/
    if ( GetLocalInst( &InstId ) != 0 ) {
        fprintf( stderr,
                "%s: %s: error getting local installation id; exiting!\n",  progname, subname );
        exit( -1 );
    }

        /* Look up modules of interest
         *****************************/
    if ( GetModId( MyModuleId, &MyModId ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid module name <%s>; exiting!\n",  progname, subname, MyModuleId );
        exit( -1 );
    }

        /* Look up message types of interest
         ***********************************/
    if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n",  progname, subname );
        exit( -1 );
    }
    if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid message type <TYPE_ERROR>; exiting!\n",  progname, subname );
        exit( -1 );
    }
    /**/
    if ( GetType( "TYPE_STRONGMOTION", &TypeStrongMotion ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid message type <TYPE_STRONGMOTION>; exiting!\n",  progname, subname );
        exit( -1 );
    }
    if ( GetType( "TYPE_STRONGMOTIONII", &TypeStrongMotion2 ) != 0 ) {
        fprintf( stderr,
                "%s: %s: Invalid message type <TYPE_STRONGMOTIONII>; exiting!\n",  progname, subname );
        exit( -1 );
    }
}


/**********************************************************************************
 * HeartBeat() sends a heartbeat to shared memory                                 *
 **********************************************************************************/
void HeartBeat( void )
{
    if  ( time(&timeNow) - timeLastBeat  >=  heartBeatInterval ) {
        timeLastBeat = timeNow;
        ewmod_status( TypeHeartBeat, 0, "" );
    }

}


/*****************************************************************************
 *  pager1( )  Sends a message to somebodies pager                           *
            note        * Message to be sent to pagers                       *
            person      * Person to receive page                             *
    ierr = pager1(note, person); 
 *****************************************************************************/   
int pager1( char  *note, char  *person)
{
    FILE    *out;
    char   subname[] = "pager1";
    char   msgfilename[100], systemtext[200], sendmsg[200];

/* Build temporary path & final path to write file to
 ****************************************************/
    sprintf( sendmsg,     "/home/earthworm/v4.1/bin/sendmsg" );
    sprintf( msgfilename, "/home/earthworm/run/params/pagermsg" );
    
    out = fopen(msgfilename, "wb");
    if(out == 0L) 
        logit("e", "%s: Unable to send page: %s to %s\n", 
                subname, msgfilename, person);
    else {        
        fprintf(out, "%s", note);
    } 
    fclose(out);
    
    sprintf( systemtext, "%s %s %s >/dev/null 2>&1", sendmsg, msgfilename, person );

    system (systemtext);
    
/* Everything went smoothly
 **************************/
    
    remove(msgfilename);      
    return( 0 );
}


/**********************************************************************************
 *  Write_MsgII assembles a type II strong motion msg and writes it to ring.      *
 **********************************************************************************/
void Write_MsgII(Arkive *Ark)
{
    SM_INFO    sm;
    time_t     current_time;
    char       buf[512];
    int        i, k;

    k = Ark->Current;

    strcpy( sm.sta,  Ark->A[k].Site ); 
    strcpy( sm.comp, Ark->A[k].Comp ); 
    strcpy( sm.net,  Ark->A[k].Net ); 
    strcpy( sm.loc, "" ); 
    strcpy( sm.loc,  Ark->A[k].Loc ); 
    strcpy( sm.qid, Ark->EvntIDS ); 
    strcpy( sm.qauthor, "014024003" );   
    sm.t  = Ark->A[k].PPick;
    sm.talt    = time(&current_time);
    sm.altcode = 0;
    sm.pga  = Ark->A[k].Pga;
    sm.tpga = Ark->A[k].tpga;
    sm.pgv  = Ark->A[k].Pgv;
    sm.tpgv = Ark->A[k].tpgv;
    sm.pgd = Ark->A[k].Pgd;
    sm.tpgd = Ark->A[k].tpgd;
    sm.nrsa = Ark->A[k].nRSA;
    for(i=0;i<sm.nrsa;i++) sm.pdrsa[i] = 1.0/Ark->A[k].freq[i];
    for(i=0;i<sm.nrsa;i++) sm.rsa[i]  = Ark->A[k].RSA[i];
    
    if( wr_strongmotionII( &sm, buf, 512 ) ) logit("", "strappend error. \n");
    log_strongmotionII( &sm );
    ewmod_status( TypeStrongMotion2, 0, buf );

}


