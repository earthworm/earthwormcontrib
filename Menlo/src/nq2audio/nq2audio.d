#
# nq2audio configuration file
#
# This code receives MiniSEED records as files, converts them into
# audio files in .au format.
#
#
ModuleId	 MOD_NQ2AUDIO        # module id for this import,
InRingName      HYPO_RING        # shared memory ring for input

 HeartBeatInt   10      # Heartbeat interval in seconds
                        # this should match the nq2ring.desc heartbeat!

 LogFile         1      # If 0, don't write logfile;; if 1, do
                        # if 2, log to module log but not stderr/stdout

# Directory where we should look for the NetQuakes miniSEED files
NQFilesInDir    /home/luetgert/getfiles/netquakes

# Directory where the successfully processed files are put
NQFilesOutDir /home/luetgert/getfiles/netquakes/save

# Directory where the problem files are put
NQFilesErrorDir /home/luetgert/getfiles/netquakes/trouble

# Directory where the audio files are put
NQFilesAudioDir /home/luetgert/getfiles/netquakes/audio

# Debug switch: the token "Debug" (without the quotes) can be stated.
# If it is, lots of weird debug messages will be produced 
# Debug

