#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <termio.h>
#include <fcntl.h>
#include <time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/time.h>
#include <stdlib.h>
#include <signal.h>

#include "earthworm.h"
#include <kom.h>
#include <transport.h>
#include "trace_buf.h"
		
#include "qlib2.h"
 
#include "msdatatypes.h"

#include "seedstrc.h"

#define   DEBUG
#define   FILE_NAM_LEN 500

/* the defines below map into the q2ew.desc error file */
#define Q2EW_DEATH_SIG_TRAP   2
#define Q2EW_DEATH_EW_PUTMSG  3
#define Q2EW_DEATH_EW_TERM    4
#define Q2EW_DEATH_EW_CONFIG  5

#define VER_NO "1.0.2 -  1999.172"
/* keep track of version notes here */

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/*****************************************************************************
 *  defines                                                                  *
 *****************************************************************************/

#define MAXCHANS        3  /* Max number of channels in a mSEED file         */
#define MAXBUFF      1025  /* Max buffer (>2 x a mSEED packet)               */

/*****************************************************************************
 *  Define the structure for keeping track of a buffer of trace data.        *
 *****************************************************************************/

typedef struct _DATABUF {
  char sncl[20];     /* SNCL for this data channel                           */
  int buf[MAXBUFF];  /* The raw trace data; native byte order                */
  int bufptr;        /* The nominal time between sample points               */
  double starttime;  /* time of first sample in raw data buffer              */
  double endtime;    /* time of last sample in raw data buffer               */
  FILE   *fp;
} DATABUF;

/*****************************************************************************
 *  Define the structure for the audio file header.                          *
 *****************************************************************************/

typedef struct _Audio_filehdr {
  unsigned long magic;        /* Magic number                                */
  unsigned long hdr_size;     /* Byte offset to start of audio data          */
  unsigned long data_size;    /* Data length in bytes (optional)             */
  unsigned long encoding;     /* Data encoding code                          */
  unsigned long sample_rate;  /* Samples per second                          */
  unsigned long channels;     /* Number of interleaved channels              */
} Audio_filehdr;

