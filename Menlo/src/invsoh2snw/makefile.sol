#
#                     Make file for invsoh2snw
#                         Solaris Version
#
CFLAGS = -D_REENTRANT $(GLOBALFLAGS)
B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib

O = invsoh2snw.o

invsoh2snw: $O
	cc -o $B/invsoh2snw $O -lm -lsocket -lnsl -lposix4

clean:
	/bin/rm -f invsoh2snw *.o

clean_bin:
	/bin/rm -f invsoh2snw
