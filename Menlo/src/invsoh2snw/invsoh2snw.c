
   /********************************************************************
    *                           invsoh2snw.c                           *
    *                                                                  *
    *  Command-line program to read a Majorsine Power Inverter which   *
    *  has been converted from html to simple text and to produce a    *
    *  file in SeisNetWatch input format.                              *
    *                                                                  *
    *  Will Kohler  Mar 19, 2010                                       *
    ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Define individual INV params to find in file
   ********************************************/
#define N_INV_PARAM  2    /* number of INV params in SOH file */

#define OUTPUTVOLTAGE   0
#define OUTPUTLOAD      1

static char *INVparam[] = { "Output Voltage (Volt)",
                            "Output Load (%)" };

int main( int argc, char *argv[] )
{
   char  *prog;               /* arvg[0] */
   char  *station;            /* argv[1] */
   char  *network;            /* argv[2] */
   char  *infile;             /* argv[3] */
   char  *outfile;            /* argv[4] */
   FILE  *ifp;                /* file to read from       */
   FILE  *ofp;                /* file to write to        */
   char   line[4096];         /* single line from infile */
   double output_voltage;     /* output voltage          */
   int    output_load;        /* output load (%)         */
   int    found[N_INV_PARAM]; /* flag for each SOH param */
   int    nmiss;              /* number of missed params */
   int    nparam;             /* #params to write to SNW */
   int    i;

/* Check arguments; assign them to coder-friendly variables
 **********************************************************/
   prog = argv[0];

   if( argc != 5 )
   {
      fprintf( stderr,
              "  Usage: %s <station> <network> <infile> <outfile>\n"
              "Example: %s VOLPK NC inv_uwave-inv-vp.txt inv_uwave-inv-vp.snw\n",
               prog, prog );
      return( 1 );
   }
   station = argv[1];
   network = argv[2];
   infile  = argv[3];
   outfile = argv[4];

/* Validate arguments
 ********************/
   if( strlen(station) > 5 )
   {
     fprintf( stderr,
             "%s: station <%s> too long (must be <= 5 chars); exiting!\n",
              prog, station );
     return( 1 );
   }
   if( strlen(network) > 2 )
   {
     fprintf( stderr,
             "%s: network <%s> too long (must be <= 2 chars); exiting!\n",
              prog, network );
     return( 2 );
   }

/* Open the input file
 *********************/
   ifp = fopen( infile, "r" );
   if( ifp == NULL )
   {
     fprintf( stderr, "%s: Cannot open input file <%s>\n", prog, infile );
     return( 4 );
   }

/* Read thru file for interesting SOH params.
/* Different INV firmware versions may use different keywords.
 ************************************************************/
   for ( i = 0; i < N_INV_PARAM; i++ ) found[i] = 0;

   while ( fgets( line, 4096, ifp ) != NULL )
   {
      char *token;

      line[4095] = 0;

      if ( strncmp( line, " Output Voltage (Volt)", 22 ) == 0 )
      {
         token = strtok( line+22, " \n" );
         if ( sscanf( token, "%lf", &output_voltage ) != 1 )
         {
            fprintf( stderr, "%s: Cannot decode output_voltage from line:\n%s", prog, line );
            continue;
         }
//       printf( "output_voltage: %.1lf\n", output_voltage );
         found[OUTPUTVOLTAGE] = 1;
      }

      else if ( strncmp( line, " Output Load (%)", 16 ) == 0 )
      {
         token = strtok( line+16, " v\n" );
         if ( sscanf( token, "%d", &output_load ) != 1 )
         {
            fprintf( stderr, "%s: Cannot decode output_load from line:\n%s", prog, line );
            continue;
         }
//       printf( "output_load: %d\n", output_load );
         found[OUTPUTLOAD] = 1;
      }
   }

/* Verify that all expected parameters were read from file
 *********************************************************/
   nmiss = 0;
   for ( i = 0; i < N_INV_PARAM; i++ )
   {
      if ( !found[i] )
      {
         fprintf( stderr,
                 "%s: error reading <%s> from '%s'; Exiting.\n",
                  prog, INVparam[i], infile );
         nmiss++;
      }
   }
   if ( nmiss )
   {
      printf( "nmiss: %d\n", nmiss );
      fclose( ifp );
      return( -1*nmiss );  /* exit, returning # of missed params */
   }

/* Open the output file
 **********************/
   ofp = fopen( outfile, "w" );
   if( ofp == NULL )
   {
     fprintf( stderr, "%s: Cannot open output <%s>\n",  prog, outfile );
     fclose( ifp );
     return( 5 );
   }

/* Write SeisNetWatch-formatted file (all on one line!)
 ******************************************************/
   nparam = N_INV_PARAM;

   fprintf( ofp, "%s-%s:%d:", network, station, nparam );
   fprintf( ofp, "Inverter Output Voltage=%.1lf;",       output_voltage );
   fprintf( ofp, "Inverter Output Load (percent)=%d;\n", output_load );

// printf( "%s-%s:%d:", network, station, nparam );
// printf( "Inverter Output Voltage=%.1lf;",           output_voltage );
// printf( "Inverter Output Load (percent)=%d;\n",     output_load );

/* All done!
 ***********/
   fclose( ifp );
   fclose( ofp );
   return( 0 );
}

