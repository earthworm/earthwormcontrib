#
#                Configuration File for Program wdog_ew
#
MyModuleId    MOD_WDOG_EW   # Module id for this program.

RingName        WAVE_RING   # Transport ring to read tracebuf messages from
                            # and write heartbeats and errors to.

MaxTemperature         40   # Report when temperature exceeds this value
                            # in degrees centrigrade

HeartBeatInterval      30   # Interval between heartbeats sent to statmgr,
                            #   in seconds.

TickleInterval        600   # Tickle watchdog and check watchdog temperature
                            # at this interval, in seconds.

AcCheckInterval       600   # Check if AC power is on this often, in seconds.
                            # If 0; don't check if AC power is on.

WdogCheckInterval    3600   # Check eeprom memory and temperature sensor at
                            # this interval, in seconds

RequireWaveform         1   # If 0, tickle the watchdog every TickleInterval
                            # seconds, regardless of the presence of 
                            # TYPE_TRACEBUF2 messages.
                            # If 1, don't tickle the watchdog unless we have
                            # seen tracebuf messages in the previous
                            # TickleInterval seconds.

