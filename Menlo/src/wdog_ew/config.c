
#include <stdio.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>

// Global variables, from configuration file
// *****************************************
long          RingKey;           // Transport ring key
unsigned char MyModuleId;        // wdog_ew's module id
int           TickleInterval;    // Tickle the watchdog at this interval
int           AcCheckInterval;   // Check AC power at this interval
int           HeartBeatInterval; // Interval between statmgr heartbeats
int           MaxTemperature;    // Status message is sent at higher temps
int           RequireWaveform;   // Flag
int           WdogCheckInterval; // Watchdog status check interval



       /***************************************************
        *                   GetConfig()                   *
        *  Reads the configuration file.                  *
        *  Exits if any errors are encountered.           *
        ***************************************************/

void GetConfig( char *configfile )
{
   const int ncommand = 8;       // Number of commands to process
   char      init[10];           // Flags, one for each command
   int       nmiss;              // Number of commands that were missed
   char      *com;
   char      *str;
   int       nfiles;
   int       success;
   int       i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i = 0; i < ncommand; i++ )
      init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( configfile );
   if ( nfiles == 0 )
   {
        printf( "wdog_ew: Error opening command file <%s>. Exiting.\n",
                 configfile );
        exit( -1 );
   }

/* Process all command files
   *************************/
   while ( nfiles > 0 )      // While there are command files open
   {
        while ( k_rd() )     // Read next line from active file
        {
           com = k_str();    // Get the first token from line

/* Ignore blank lines & comments
   *****************************/
           if ( !com )          continue;
           if ( com[0] == '#' ) continue;

/* Open a nested configuration file
   ********************************/
           if ( com[0] == '@' )
           {
              success = nfiles + 1;
              nfiles  = k_open( &com[1] );
              if ( nfiles != success )
              {
                 printf( "wdog_ew: Error opening configuration file <%s>. Exiting.\n",
                     &com[1] );
                 exit( -1 );
              }
              continue;
           }

           if ( k_its( "MyModuleId" ) )
           {
              str = k_str();
              if ( str )
              {
                 if ( GetModId( str, &MyModuleId ) < 0 )
                 {
                    printf( "wdog_ew: Invalid MyModuleId <%s> in <%s>",
                             str, configfile );
                    printf( ". Exiting.\n" );
                    exit( -1 );
                 }
              }
              init[0] = 1;
           }

           else if( k_its( "RingName" ) )
           {
              str = k_str();
              if (str)
              {
                 if ( ( RingKey = GetKey(str) ) == -1 )
                 {
                    printf( "wdog_ew: Invalid RingName <%s> in <%s>",
                             str, configfile );
                    printf( ". Exiting.\n" );
                    exit( -1 );
                 }
              }
              init[1] = 1;
           }

           else if( k_its( "MaxTemperature" ) )
           {
              MaxTemperature = k_int();
              init[2] = 1;
           }

           else if( k_its( "HeartBeatInterval" ) )
           {
              HeartBeatInterval = k_int();
              init[3] = 1;
           }

           else if( k_its( "TickleInterval" ) )
           {
              TickleInterval = k_int();
              init[4] = 1;
           }

           else if( k_its( "AcCheckInterval" ) )
           {
              AcCheckInterval = k_int();
              init[5] = 1;
           }

           else if( k_its( "RequireWaveform" ) )
           {
              RequireWaveform = k_int();
              init[6] = 1;
           }

           else if( k_its( "WdogCheckInterval" ) )
           {
              WdogCheckInterval = k_int();
              init[7] = 1;
           }

           else
           {
              printf( "wdog_ew: <%s> unknown command in <%s>.\n",
                       com, configfile );
              continue;
           }

/* See if there were any errors processing the command
   ***************************************************/
           if ( k_err() )
           {
              printf( "wdog_ew: Bad <%s> command in <%s>; \n",
                       com, configfile );
              exit( -1 );
           }
        }
        nfiles = k_close();
   }

/* Check flags for missed commands
   *******************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if( !init[i] ) nmiss++;

   if ( nmiss )
   {
      printf( "wdog_ew: ERROR, no " );
      if ( !init[0] ) printf( "<MyModuleId> "        );
      if ( !init[1] ) printf( "<RingName> "          );
      if ( !init[2] ) printf( "<MaxTemperature> "    );
      if ( !init[3] ) printf( "<HeartBeatInterval> " );
      if ( !init[4] ) printf( "<TickleInterval> "    );
      if ( !init[5] ) printf( "<AcCheckInterval> "   );
      if ( !init[6] ) printf( "<RequireWaveform> "   );
      if ( !init[7] ) printf( "<WdogCheckInterval> " );
      printf( "command(s) in <%s>. Exiting.\n", configfile );
      exit( -1 );
   }
   return;
}


       /***************************************************
        *                   LogConfig()                   *
        *  Log the configuration file parameters.         *
        ***************************************************/

void LogConfig( void )
{
   logit( "", "\n" );
   logit( "", "MyModuleId:        %u\n",  MyModuleId );
   logit( "", "RingKey:           %ld\n", RingKey );
   logit( "", "HeartBeatInterval: %d\n",  HeartBeatInterval );
   logit( "", "MaxTemperature:    %d\n",  MaxTemperature );
   logit( "", "TickleInterval:    %d\n",  TickleInterval );
   logit( "", "AcCheckInterval:   %d\n",  AcCheckInterval );
   logit( "", "RequireWaveform:   %d\n",  RequireWaveform );
   logit( "", "WdogCheckInterval: %d\n",  WdogCheckInterval );
   logit( "", "\n" );
   return;
}
