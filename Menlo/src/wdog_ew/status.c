#include <stdio.h>
#include <time.h>
#include <string.h>
#include <transport.h>
#include <earthworm.h>

/* declared in wdog_ew.c */
extern pid_t         myPid;             // For restarts by startstop
extern unsigned char InstId;            // Local installation id
extern unsigned char MyModuleId;        // wdog_ew's module id
extern SHM_INFO      region;            // Shared memory region
extern unsigned char TypeHeartBeat;
extern unsigned char TypeError;


/*************************************************************************
 * SendStatus() builds a heartbeat or error message & puts it into       *
 *              shared memory.  Writes errors to log file & screen.      *
 *************************************************************************/

void SendStatus( unsigned char type, short ierr, char *note )
{
   MSG_LOGO logo;
   char     msg[256];
   long     size;
   time_t   t;

/* Build message to be sent to statmgr
   ***********************************/
   logo.instid = InstId;
   logo.mod    = MyModuleId;
   logo.type   = type;

   time( &t );

   if ( type == TypeHeartBeat )
   {
      sprintf( msg, "%ld %d\n", (long) t, myPid );
   }
   else if ( type == TypeError )
   {
      sprintf( msg, "%ld %hd %s\n", (long) t, ierr, note );
      logit( "et", "%s\n", note );
   }
   else return;

   size = strlen( msg );

/* Write the message to shared memory
   **********************************/
   if ( tport_putmsg( &region, &logo, size, msg ) != PUT_OK )
   {
      if ( type == TypeHeartBeat )
         logit( "et", "Error sending heartbeat.\n" );
      if ( type == TypeError )
         logit( "et", "Error sending error:%d.\n", ierr );
   }
   return;
}

