//////////////////////////////////////////////////////////////////////
//      File - PCI_WDog.h
//
//      (c) 2000 - Berkshire Products, Inc.
//
//      www.pcwatchdog.com              www.berkprod.com
//
//      Library for accessing the Berkshire Products PCI PC Watchdog card,
//      See section 7 of the manual for documentation of the I/O ports
//  on the board. See section 8 for description of commands.
//
//  Version 1.00a   03/20/00
//
//////////////////////////////////////////////////////////////////////

#ifndef _PCI_WDOG_LIB_H_
#define _PCI_WDOG_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
    int iLibVersion ;
    char *strLibVersion ;
} PCI_WDOG_LIB_VER ;


#ifndef UBYTE
typedef unsigned char UBYTE ;
#endif

typedef enum                    // Offsets for the eight (8) I/O ports on the board
{
    WDOG_Port0 = 0,
    WDOG_Port1 = 1,
    WDOG_Port2 = 2,
    WDOG_Port3 = 3,
    WDOG_Port4 = 4,
    WDOG_Port5 = 5,
    WDOG_Port6 = 6,
    WDOG_Port7 = 7
} WDOG_REGS;

typedef enum            // Commands defined in Section 8 of manual
{
    WDOG_GetStat   = 0x04,      // get status
    WDOG_GetVer    = 0x08,      // get firmware version
    WDOG_ReadArm   = 0x10,      // read current ARM time
    WDOG_WriteArm  = 0x11,      // write current ARM time
    WDOG_RdNvArm   = 0x14,      // read non-volatile ARM time
    WDOG_WrNvArm   = 0x15,      // write non-volatile ARM time
    WDOG_ReadWdTm  = 0x18,      // read current watchdog time
    WDOG_WriteWdTm = 0x19,      // write current watchdog time
    WDOG_RdNvWdTm  = 0x1c,      // read non-volatile watchdog time
    WDOG_WrNvWdTm  = 0x1d,      // write non-volatile watchdog time
    WDOG_ReadRly   = 0x50,      // read relay control
    WDOG_WriteRly  = 0x51,      // write relay control
    WDOG_ResetPC   = 0x80,      // reset the PC
    WDOG_GetRstCnt = 0x84       // get the reset count
} WDOG_CMDS;

typedef struct
{
    UBYTE bCmmdTo ;     // command to send to watchdog
    UBYTE bCmmdLsb ;    // lsb of command data
    UBYTE bCmmdMsb ;    // msb of command data
    UBYTE bRespFrom ;   // response byte from watchdog
    UBYTE bRespLsb ;    // lsb of response data
    UBYTE bRespMsb ;    // msb of response data
} WD_CR_DATA ;

typedef struct          // returned by status check function
{
        unsigned Relay1Stat :1 ; // RL1A = 1 - Relay #1 on
        unsigned Relay2Stat :1 ; // RL2A = 1 - Relay #2 on
        unsigned Relay2Excl :1 ; // R2DS = 1 - Relay #2 exclusive control
        unsigned TempTrip   :1 ; // TTRP = 1 - Temp is above trip point.
        unsigned HrtBtLed   :1 ; // HRBT = 1 - Heart Beat LED is on
        unsigned WDogTrip   :1 ; // WTRP = 1 - WDog trip is on
        unsigned spare1     :1 ; // not used - keeps bit placement same as ISA WDog lib
        unsigned EnTTrip    :1 ; // ENTP = 1 - WDog can reset on overtemp (along w/Switch 4 on SW1)
} WDOG_STATUS ;

// this string is set to an error message, if one occurs
extern CHAR WDogErrorString[] ;

// ***************************************************************
// Start Function Definitions
// ***************************************************************
//
// NOTE: Some of these functions return ints, bools etc as a result
//              of a test.
//      If you fail to call the WDogInit() function at startup
//              then these functions will return 0 or FALSE.
//              Always check WDogErrorString[] to test for errors. If it is
//              not a null string then an error occurred.

// ---------------------------------------------------------------
// Library Version Function - version of library.
//
//  Calling:
//
//  Return:
//          *PCI_WDOG_LIB_VER - structure pointer
//      member int - bits 31-24 - always = 0
//                   bits 23-16 - integer portion of version
//                   bits 15- 8 - hundreds portion
//                   bits  7- 0 - ascii minor
//      member char* - string version of revision
// ---------------------------------------------------------------
PCI_WDOG_LIB_VER* WDogLibVersion(void) ;

// ---------------------------------------------------------------
// Init Function - This MUST be called first to setup the
//              Library functions.
//      This routine will first check to makre sure the
//      driver (windrvr.vxd for 95/98 or windrvr.sys for NT/2K)
//      is installed and running. Then it will scan the PCI bus
//      to find the watchdog board.
//      If this function is not done first then all read functions
//      will return 0x00 and writes will do nothing.
//              The ErrorString will be written.
//
//  Calling:
//              WDOG_ADDR - USER I/O address range set on DIP Switch #2
//  Return:
//              BOOL - TRUE if open sucessful
//                   - FALSE will also write message to WdogErrorString
//
// ---------------------------------------------------------------
BOOL WDogInit(void) ;

// ---------------------------------------------------------------
// Close Function - This MUST be called before your application
//              exits. It cleans up any allocated memory variables.
//
//  Calling:
//
//  Return:
//
// ---------------------------------------------------------------
void WDogClose(void) ;

// ---------------------------------------------------------------
// Read Temp Function - Returns the temp from the board.
//              Calling this function also "tickles" the PC Watchdog to make
//              it reset the countdown timer. The "Watchdog Trip" LED on the
//              board should flash every time you call this function.
//
//  Calling:
//              BOOL - TRUE returns temp in Fahrenhite
//  Return:
//              int - temperature (signed)
//
// ---------------------------------------------------------------
int WDogReadTemp(BOOL)  ;

// ---------------------------------------------------------------
// Clear Trip Function - Test (and clear) the WTRP bit. If you
//              clear the bit then you will turn off the lower trip LED
//              on the back of the board.
//
//  Calling:
//              BOOL - TRUE will also clear the bit.
//  Return:
//              BOOL - TRUE if WTRP was set. ie: WDog generated a reset
//                   - FALSE WTRP was clear.
//
// ---------------------------------------------------------------
BOOL WDogTestWDogtrip(BOOL)  ;

// ---------------------------------------------------------------
// Test Temptrip Function - Checks to see if the temperature has
//              exceeded the lower trip point.
//
//  Calling:
//
//  Return:
//              BOOL - TRUE if bit is set.
//                   - FALSE bit is clear.
//
// ---------------------------------------------------------------
BOOL WDogTestTemptrip(void)  ;

// ---------------------------------------------------------------
// WDog Status Function - Returns status of the PC Watchdog.
//
//  Calling:
//
//  Return:
//              int - bit field WDogStatusBits.
//
// ---------------------------------------------------------------
WDOG_STATUS WDogStatus(void)  ;

// ---------------------------------------------------------------
// Relay2 Control Function - Turn relay #2 On/Off. Also set for
//              exclusive control of Relay#2.
//
//  Calling:
//              BOOL - First Parm - TRUE for Relay #2 On.
//              BOOL - Second Parm - TRUE for exclusive Relay #2 control.
//  Return:
//
//      NOTE: This function will also clear the WTRP bit and the Trip
//              LED if they were active. Call WDogTestWDogtrip() first if
//              you need to check the status.
// ---------------------------------------------------------------
void WDogRelay2Control(BOOL, BOOL)  ;

// ---------------------------------------------------------------
// Get Version Function - Returns the firmware version of the code
//              running in the sinlge chip CPU on the board.
//
//  Calling:
//              char* - buffer pointer for 5 character string (including
//                      null terminator). Will be of form x.xx.
//  Return:
//              BOOL - TRUE if no errors.
//                   - FALSE will also write message to WdogErrorString
//
// ---------------------------------------------------------------
BOOL WDogGetVersion(char*)  ;

// ---------------------------------------------------------------
// Send Command Function - Sends one of the special commands to
//              the board. This function will call the Sleep() function
//              while it waits for board to respond.
//
//  Calling:
//              WD_CR_DATA* - pointer to a command/response structure that must
//                      have the first three bytes set to valid data.
//  Return:
//              BOOL - TRUE if no errors.
//                   - FALSE will also write message to WdogErrorString
//
//  This routine will fill in the last three bytes in the structure.
// ---------------------------------------------------------------
BOOL WDogSendCommand(WD_CR_DATA*)  ;

// ---------------------------------------------------------------
// Set Temp Offset Function - Sets the temperature offset on the
//              board and enables the PC Watchdog to reset the PC if the
//              second trip point is hit and Switch #4 on SW1 is on.
//
//  Calling:
//              UBYTE - Temperature offset: 0x00 to 0x0f.
//              BOOL - TRUE will enable the temperature trip.
//  Return:
//              BOOL - TRUE if no errors.
//
// ---------------------------------------------------------------
BOOL WDogSetTempOffset(UBYTE, BOOL)  ;

// ---------------------------------------------------------------
// Get Temp Offest Function - Returns the current temperature
//              offset stored in the board in the range 0x00 to 0x0f. If
//              the temp trip bit is set then the value returned will have
//              the upper bit set to one (1).
//
//  Calling:
//
//  Return:
//              UBYTE - Temperature offset: 0x00 to 0x0f or 0x80 to 0x8f.
//
// ---------------------------------------------------------------
UBYTE WDogGetTempOffset(void)  ;

// ---------------------------------------------------------------
// Enable Disable Function - Performs the enable or disable opera-
//              tion. Then check disable bit (WDIS) to see if sucessful
//              and return status.
//
//  Calling:
//              BOOL - TRUE will enable the PC Watchdog
//  Return:
//              BOOl - TRUE if enable/disable operation worked.
//
// ---------------------------------------------------------------
BOOL WDogEnableDisable(BOOL)  ;

// ---------------------------------------------------------------
// Read Dip Switch Function - Will read the settings of SW1.
//
//  Calling:
//
//  Return:
//              UBYTE - hex value of switch values.
//
// ---------------------------------------------------------------
UBYTE WDogReadSwitch(void)  ;

// ---------------------------------------------------------------
// Write Digital Outputs - Writes the lower four bits of the
//      byte to the digital outputs: DO-3 to DO-0.
//
//  Calling:
//              UBYTE - lower four (4) bits written to digital outputs
//  Return:
//              UBYTE - reads the data in port 7 after the write.
//
// ---------------------------------------------------------------
UBYTE WDogWriteDigitalIO(UBYTE)  ;

// ---------------------------------------------------------------
// Read Digital I/O - Reads the state of port 7 for the state of
//      the four inputs and four outpts.
//
//  Calling:
//
//  Return:
//              UBYTE - reads the data in port 7.
//
// ---------------------------------------------------------------
UBYTE WDogReadDigitalIO(void)  ;

// ---------------------------------------------------------------
// Read Port Function - This low level function will read a
//              UBYTE (unsigned char) from the specified I/O port on
//              the board.
//
//  Calling:
//              WDOG_REGS - I/O port offset from base address
//  Return:
//              UBYTE - Data from I/O port on board.
//
// ---------------------------------------------------------------
UBYTE WDogReadIO(WDOG_REGS) ;

// ---------------------------------------------------------------
// Write Port Function - This low level function will write a
//              UBYTE (unsigned char) to the sepcified I/O port on
//              the board.
//
//  Calling:
//              WDOG_REGS - I/O port offset from base address
//              UBYTE - Data to I/O port on board.
//  Return:
//
// ---------------------------------------------------------------
void WDogWriteIO(WDOG_REGS, UBYTE)  ;

// ***************************************************************
// End Function Definitions
// ***************************************************************

#ifdef __cplusplus
}
#endif

#endif
