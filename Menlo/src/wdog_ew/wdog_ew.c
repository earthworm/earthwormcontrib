
/*===========================================================================
 *                               wdog_ew
 *   Earthworm program to monitor a Berkshire PCI PC Watchdog board.
 *
 *   Temperature is logged.  If temperature exceeds a threshold value,
 *   a message is sent to statmgr.
 *
 *   When non-UPS, AC power is lost, a message is sent to statmgr.
 *
 *   Sends heartbeats to the watchdog card, as long as TRACE_BUF messages
 *   from the digitizer are detected.  If the watchdog doesn't receive
 *   heartbeats for a specified time interval, it will reboot the computer.
 *
 *===========================================================================
 *
 *      Date  Ver.  Description
 * ---------- ----- ---------------------------------------------
 * 2002 06-19 1.00  First version started
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <windows.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <trace_buf.h>
#include "pci_wdog.h"

/* Define errors
 ****************/
#define ERR_TOO_HOT  0
#define ERR_AC_POWER 1

/* Function declarations
 ***********************/
void GetConfig( char * );
void LogConfig( void );
void SendStatus( unsigned char, short, char * );
int  WdogEnabled( void );
int  WdogOk( void );
int  AcPower( int * );
int  WaveformAvailable( MSG_LOGO * );

/* Global variables
 ******************/
pid_t          myPid;             /* For restarts by startstop */
unsigned char  InstId;            /* Local installation id     */
SHM_INFO       region;
unsigned char  TypeHeartBeat;
unsigned char  TypeError;


int main( int argc, char *argv[] )
{
   extern long RingKey;           /* Transport ring key */
   extern int  HeartBeatInterval; /* Interval between statmgr heartbeats */
   extern int  TickleInterval;    /* Tickle watchdog at this interval */
   extern int  AcCheckInterval;   /* Check AC power at this interval */
   extern int  WdogCheckInterval; /* Watchdog status check interval */
   time_t      timeLastBeat;      /* Time last heartbeat was sent */
   time_t      timeLastTickle;    /* Last time watchdog was tickled */
   time_t      timeLastAcCheck;   /* Last time the AC power was checked */
   time_t      timeLastWdogCheck; /* Last time the watchdog status was checked */
   int         gotWaveform = 0;   /* 1 if we got waveforms in last */
                                  /*    TickleInterval seconds */
   MSG_LOGO    getlogo;           /* Logo of tracebuf msgs to get from shared memory */

/* Check program arguments
 * ***********************/
   if ( argc != 2 )
   {
      printf( "\nUsage: wdog_ew <config_file>\n" );
      return -1;
   }

/* Read the wdog_ew configuration file
 * ***********************************/
   GetConfig( argv[1] );

/* Initialize log file and log configuration parameters
 ******************************************************/
   {
      extern unsigned char MyModuleId;   /* Module id of this process */
      const LogSwitch = 1;               /* Enable logging */

      logit_init( argv[1], MyModuleId, 256, LogSwitch );
      logit( "" , "Read command file <%s>\n", argv[1] );
   }
   LogConfig();

/* Look up installation id
  ************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      logit( "e", "Error getting installation id. Exiting.\n" );
      return -1;
   }

/* Look up message types from earthworm.h tables
 ***********************************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      logit( "e", "Invalid message type <TYPE_HEARTBEAT>. Exiting.\n" );
      return -1;
   }

   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 )
   {
      logit( "e", "Invalid message type <TYPE_ERROR>. Exiting.\n" );
      return -1;
   }

/* Set up logo for getting tracebuf messages
 *******************************************/
   if ( GetInst( "INST_WILDCARD", &getlogo.instid ) != 0 )
   {
       logit( "e", "Invalid installation name <INST_WILDCARD>. Exiting." );
       return -1;
   }
   if ( GetModId( "MOD_WILDCARD", &getlogo.mod ) != 0 )
   {
       logit( "e", "Invalid module id <MOD_WILDCARD>. Exiting." );
       return -1;
   }
   if ( GetType( "TYPE_TRACEBUF2", &getlogo.type ) != 0 )
   {
       logit( "e", "Invalid message type <TYPE_TRACEBUF2>. Exiting." );
       return -1;
   }

/* Get process ID for heartbeat messages
 ***************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "wdog_ew: Cannot get my PID. Exiting.\n" );
      return -1;
   }
   logit( "", "My process id: %u\n", myPid );

/* Attach to shared memory ring
 ******************************/
   tport_attach( &region, RingKey );
   logit( "", "Attached to shared-memory ring with key: %d\n", RingKey );

/* Initialize watchdog board
 ***************************/
   if ( !WDogInit() )
   {
      logit( "", &WDogErrorString[0] );
      logit( "", "WDogInit() error. Exiting.\n" );
      WDogClose();
      return -1;
   }

/* Read firmware version of watchdog
  **********************************/
   {
      char s[64];
      if ( !WDogGetVersion( &s[0]) )
      {
         logit( "", &WDogErrorString[0] );
         logit( "", "WDogGetVersion() error. Exiting.\n" );
         WDogClose();
         return -1;
      }
      logit( "e", "Watchdog firmware version is: %s\n", s );
   }

/* Read library version of watchdog
 **********************************/
   {
      PCI_WDOG_LIB_VER *libver = WDogLibVersion();
      if ( WDogErrorString[0] )
      {
         logit( "", &WDogErrorString[0] );
         logit( "", "WDogLibVersion() error. Exiting.\n" );
         WDogClose();
         return -1;
      }
      logit( "e", "Watchdog API library version: %s\n", libver->strLibVersion );
   }

/* See if the watchdog has tripped.
 * Status of trip flag and trip LED are not changed.
 ***************************************************/
   if ( WDogTestWDogtrip(FALSE) )
      logit( "e", "Watchdog has tripped.\n" );
   else
      logit( "e", "Watchdog has not tripped.\n" );

/* Read settings of switch SW1
 *****************************/
   {
      UINT i = WDogReadSwitch();
      logit( "e", "Settings of switch S1: %02X hex\n", i );
   }

/* See if the watchdog is enabled?
 * Even if it's disabled, it should be able to
 * return temperature and AC power status.
 *********************************************/
   {
      int rc = WdogEnabled();
      if ( rc == -1 )
      {
         logit( "t", "WdogEnabled() failed. Exiting.\n" );
         WDogClose();
         return -1;
      }
      if ( rc )
         logit( "e", "Watchdog is ENABLED.\n" );
      else
         logit( "e", "Watchdog is DISABLED.\n" );
   }

/* In first pass through main loop, send a heartbeat
 * and tickle the watchdog. Also, check AC power and
 * make sure watchdog board is functioning correctly.
 ***************************************************/
   {
      time_t now        = time(0);
      timeLastBeat      = now - (time_t)HeartBeatInterval - 1;
      timeLastTickle    = now - (time_t)TickleInterval    - 1;
      timeLastAcCheck   = now - (time_t)AcCheckInterval   - 1;
      timeLastWdogCheck = now - (time_t)WdogCheckInterval - 1;
   }

/* Loop until termination is requested
 *************************************/
   while ( tport_getflag( &region ) != TERMINATE  &&
           tport_getflag( &region ) != myPid )
   {
      extern int MaxTemperature;    /* Status message is sent at higher temps */
      extern int RequireWaveform;   /* Flag */
      time_t now = time(0);         /* Get current time */

/* Send a heartbeat to statmgr
 *****************************/
      if ( (now - timeLastBeat) >= (time_t)HeartBeatInterval )
      {
         timeLastBeat = now;
         SendStatus( TypeHeartBeat, 0, "" );
      }

/* Check watchdog non-volatile memory and temperature sensor
 ***********************************************************/
      if ( (now - timeLastWdogCheck) >= (time_t)WdogCheckInterval )
      {
         timeLastWdogCheck = now;
         if ( WdogOk() )
            logit( "et", "Watchdog memory and temperature sensor are OK.\n" );
         else
         {
            logit( "et", "Fatal watchdog error. Exiting.\n" );
            WDogClose();
            return -1;
         }
      }

/* See if any tracebuf messages have arrived
 *******************************************/
      if ( RequireWaveform && WaveformAvailable(&getlogo) )
         gotWaveform = 1;

/* Tickle watchdog and get its temperature in degrees C
 ******************************************************/
      if ( !RequireWaveform || gotWaveform )
      {
         if ( (now - timeLastTickle) >= (time_t)TickleInterval )
         {
            static int first = 1;
            char   msg[80];
            int    tmpC;
            int    tooHotPrev;
            int    tooHot;

            tmpC = WDogReadTemp( FALSE );  /* Tickle watchdog and get temperature */
            if ( WDogErrorString[0] )
            {
               logit( "et", &WDogErrorString[0] );
               logit( "et", "WDogReadTemp() error. Exiting.\n" );
               WDogClose();
               return -1;
            }
            logit( "et", "Watchdog temperature: %d deg C\n", tmpC );

            tooHot = (tmpC > MaxTemperature);

            if ( first )                  /* First time temperature is checked */
            {
               if ( tooHot )
                  logit( "et", "WARNING: Watchdog temperature exceeds %d deg C.\n",
                         MaxTemperature );
               tooHotPrev = tooHot;
               first = 0;
            }

            if ( !tooHotPrev && tooHot )
            {
               sprintf( msg, "WARNING: Watchdog temperature exceeds %d deg C.", MaxTemperature );
               SendStatus( TypeError, ERR_TOO_HOT, msg );
            }

            if ( tooHotPrev && !tooHot )
            {
               sprintf( msg, "Watchdog temperature is now <= %d deg C.", MaxTemperature );
               SendStatus( TypeError, ERR_TOO_HOT, msg );
            }

            gotWaveform    = 0;
            tooHotPrev     = tooHot;
            timeLastTickle = now;
         }
      }

/* Are we running on AC power or backup power?
 *********************************************/
      if ( AcCheckInterval > 0 )
         if ( (now - timeLastAcCheck) >= (time_t)AcCheckInterval )
         {
            static int first = 1;
            int    acOnPrev;
            int    acOn;

            if ( AcPower( &acOn ) == -1 )
            {
               logit( "et", "AcPower() error. Exiting.\n" );
               WDogClose();
               return -1;
            }

            if ( first )
            {
               if ( acOn )
                  logit( "et", "AC power is ON.\n" );
               else
                  logit( "et", "AC power is OFF.\n" );

               acOnPrev = acOn;
               first = 0;
            }

            if ( acOnPrev && !acOn )
               SendStatus( TypeError, ERR_AC_POWER, "AC power is OFF." );

            if ( !acOnPrev && acOn )
               SendStatus( TypeError, ERR_AC_POWER, "AC power is ON." );

            acOnPrev        = acOn;
            timeLastAcCheck = now;
         }

      sleep_ew( 500 );     /* Sleep a while */
   }

   WDogClose();
   logit( "et", "Termination request received. Exiting.\n" );
   return 0;
}


     /****************************************************
      *                   WdogEnabled()                  *
      *                                                  *
      *  Is the watchdog enabled?                        *
      *  Even if it's disabled, it should be able to     *
      *  return temperature and AC power status.         *
      *                                                  *
      *  If enabled, return 1,                           *
      *  If disabled, return 0.                          *
      *  If an error occured, return -1.                 *
      ****************************************************/

int WdogEnabled( void )
{
   WD_CR_DATA wdcr;            /* Command/response data structure */

   wdcr.bCmmdTo  = WDOG_GetStat;
   wdcr.bCmmdLsb = wdcr.bCmmdMsb = 0;

   if ( !WDogSendCommand(&wdcr) )
   {
      logit( "", &WDogErrorString[0] );
      logit( "", "WDogSendCommand() error.\n" );
      return -1;
   }
   return ( wdcr.bRespLsb & 0x10 ) ? 0 : 1;
}



     /****************************************************
      *                     WdogOk()                     *
      *                                                  *
      *  Is the watchdog board working ok?               *
      *  If so return 1, else return 0.                  *
      ****************************************************/

int WdogOk( void )
{
   int        ok_flag = 1;    /* Board status flag */
   WD_CR_DATA wdcr;           /* Command/response data structure */

   wdcr.bCmmdTo  = WDOG_GetStat;
   wdcr.bCmmdLsb = wdcr.bCmmdMsb = 0;

   if ( !WDogSendCommand(&wdcr) )
   {
      logit( "", &WDogErrorString[0] );
      logit( "", "WDogSendCommand() error.\n" );
      return 0;
   }

   if ( !(wdcr.bRespLsb & 0x01) )
   {
      logit( "et", "Non-volatile (EEPROM) memory is NOT ok.\n" );
      ok_flag = 0;
   }

   if ( !(wdcr.bRespLsb & 0x02) )
   {
      logit( "et", "Temperature sensor is NOT ok.\n" );
      ok_flag = 0;
   }
   return ok_flag;
}


     /****************************************************
      *                    AcPower()                     *
      *                                                  *
      *  acOn is set to 1 if the AC power is on.         *
      *  Otherwise, acOn is set to 0;                    *
      *                                                  *
      *  Returns 0 if check was successful               *
      *          -1 if a fatal error occured             *
      ****************************************************/

int AcPower( int *acOn )
{
   WD_CR_DATA wdcr;           /* Command/response data structure */

   wdcr.bCmmdTo  = 0x60;
   wdcr.bCmmdLsb = wdcr.bCmmdMsb = 0;

   if ( !WDogSendCommand(&wdcr) )
   {
      logit( "t", &WDogErrorString[0] );
      logit( "t", "WDogSendCommand() error in AcPower().\n" );
      return -1;
   }

   *acOn = ((wdcr.bRespLsb & 0xC0) == 0xC0 ) ? 1 : 0;
   return 0;
}


     /*****************************************************
      *                WaveformAvailable()                *
      *                                                   *
      *  Returns 0 if no tracebuf messages are available  *
      *          1 if tracebuf messages are available.    *
      *****************************************************/

int WaveformAvailable( MSG_LOGO *getlogo )
{
   int         res;
   const short nget = 1;                  /* Number of logos in getlogo */
   MSG_LOGO    reclogo;                   /* Logo of received message */
   long        recsize;                   /* Size of received message */
   static char msgbuf[MAX_TRACEBUF_SIZ];  /* The received message */
   int         avail = 0;

/* Get all tracebuf messages from transport
 ******************************************/
   do
   {
      res = tport_getmsg( &region, getlogo, nget, &reclogo,
                          &recsize, msgbuf, MAX_TRACEBUF_SIZ );
      if ( res != GET_NONE ) avail = 1;
   } while ( res != GET_NONE );
   return avail;
}
