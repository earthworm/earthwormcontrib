@echo off
rem # safecopy.bat                                    2003/11/20:LDD
rem # Usage: safecopy filename target-directory

rem # Safecopy makes an interim copy of the file in temporary workspace, 
rem # then moves it into the target directory.  

if "%1"=="" goto Usage
if "%2"=="" goto Usage

copy /y %1 safecopy.tmp
move /y safecopy.tmp %2\%1
goto Done

:Usage
@echo Usage: safecopy filename target-directory

:Done