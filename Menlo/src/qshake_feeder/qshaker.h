/*****************************************************************************
    qshaker.h
    
    header file for qshaker.c
 *****************************************************************************/

/*****************************************************************************
    defines
 *****************************************************************************/

#define MAXCHANNELS     1400  /* Maximum number of channels                  */
#define MAXSAMPRATE      550  /* Maximum # of samples/sec.                   */
#define MAXMINUTES         5  /* Maximum # of minutes of trace.              */
#define MAXTRACELTH MAXMINUTES*60*MAXSAMPRATE /* Max. data trace length      */
#define MAXTRACEBUF MAXTRACELTH*10  /* This should work for 24-bit data      */

#define MAXLIST         1000  /* Maximum number of events in list            */

#define TIMEOUT         1800  /* Time(sec) after event to wait for data.     */

#define MAX_STR          255  /* Size of strings for arkive records          */
#define MAX_WAVESERVERS   50  /* Maximum number of Waveservers               */
#define MAX_ADRLEN        20  /* Size of waveserver address arrays           */
#define MAX_COMPS         30  /* Number of acceptable component classes      */
#define MAX_NETS          30  /* Number of acceptable network codes          */
#define MAX_PAGERS        30  /* Maximum number of Pager addresses           */

#define MAXLOGO            2  /* Maximum number of Logos                     */
#define MAXPLOTS           8  /* Maximum number of Plots/Event               */
#define MAX_STADBS        20  /* Maximum number of Station database files    */
#define MAX_SERV_THRDS     1  /* Maximum number of processor threads         */
#define MAX_TARGETS        5  /* Maximum number of targets                   */

#define GDIRSZ           132  /* Size of string for GIF target directory     */
#define STALIST_SIZ      100  /* Size of string for station list file        */

#define twopi    6.283185307179586


/*****************************************************************************
 *  Define the structure for specifying gaps in the data.                    *
 * Note: if a gap would be declared at end of data, the data must be         *
 * truncated instead of adding another GAP structure. A gap may be           *
 * declared at the start of the data, however.                               *
 *****************************************************************************/

typedef struct _GAP *PGAP;
typedef struct _GAP {
  double starttime;  /* time of first sample in the gap                      */
  double gapLen;     /* time from first gap sample to first sample after gap */
  long firstSamp;    /* index of first gap sample in data buffer             */
  long lastSamp;     /* index of last gap sample in data buffer              */
  PGAP next;         /* The next gap structure in the list                   */
} GAP;

/*****************************************************************************
 *  Define the structure for keeping track of buffer of trace data.          *
 *****************************************************************************/

typedef struct _DATABUF {
  double rawData[MAXTRACELTH*5];   /* The raw trace data; native byte order  */
  double delta;      /* The nominal time between sample points               */
  double starttime;  /* time of first sample in raw data buffer              */
  double endtime;    /* time of last sample in raw data buffer               */
  long nRaw;         /* number of samples in raw data buffer, including gaps */
  long lenRaw;       /* length to the rawData array                          */
  GAP *gapList;      /* linked list of gaps in raw data                      */
  int nGaps;         /* number of gaps found in raw data                     */
} DATABUF;

/*****************************************************************************
 *  Define the structure for Channel information.                            *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/

typedef struct ChanInfo {  /* A channel information structure                */
    char    Site[6];       /* Site                                           */
    char    Comp[5];       /* Component                                      */
    char    Net[5];        /* Net                                            */
    char    Loc[5];        /* Loc                                            */
    char    SCN[15];       /* SCN                                            */
    char    SCNtxt[17];    /* S C N                                          */
    char    SCNnam[17];    /* S_C_N                                          */
    char    SiteName[50];  /* Common Name of Site                            */
    char    Descript[200]; /* Common Name of Site                            */
    double  Lat;           /* Latitude                                       */
    double  Lon;           /* Longitude                                      */
    double  Elev;          /* Elevation                                      */
    
    int     Inst_type;     /* Type of instrument                             */
    double  Inst_gain;     /* Gain of instrument (microv/count)              */
    int     Sens_type;     /* Type of sensor                                 */
    double  Sens_gain;     /* Gain of sensor (volts/unit)                    */
    int     Sens_unit;     /* Sensor units d=1; v=2; a=3                     */
    double  GainFudge;     /* Additional gain factor.                        */
    double  SiteCorr;      /* Site correction factor.                        */
    double  sensitivity;   /* Channel sensitivity  counts/units              */
    int		ShkQual;       /* Station (Chan) type                            */
} ChanInfo;

/*****************************************************************************
 *  Define the structure for Arkive Phase cards.                             *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/

typedef struct ArkInfo {  /* A phase card structure */
    char    Site[6];      /*  0- 3  0- 4 Site                                */
    char    Comp[5];      /* 95-97  9-11 Component                           */
    char    Net[5];       /* 98-99  5- 6 Net                                 */
    char    Loc[5];       /* Loc                                             */
    char    Prem[5];      /*  4- 5 13-14 P remark                            */
    char    fm;           /*     6    15 P first motion                      */
    int     Pwgt;         /*     7    16 weight                              */
    double  PPick;        /*  9-18+19-23 17-29+29-33 P pick time             */
    double  SPick;        /*  9-18+31-35 17-29+41-45 S pick time             */
    char    Srem[5];      /* 36-37 46-47 S remark                            */
    float   SttResid;     /* 40-43 50-53 S travel-time residual              */
    float   PttResid;     /* 24-27 34-37 P travel-time residual              */
    float   Pdelay;       /* 50-53 66-69 P delay time                        */
    float   Sdelay;       /* 54-57 70-73 S delay time                        */
    float   Dist;         /* 58-61 74-77 Epicentral distance (km)            */
    float   Emerg;        /* 62-64 78-80 Emergence angle at source           */
    float   CodaDur;      /* 71-74 87-90 Coda duration (sec)                 */
    float   Azim;         /* 75-77 91-93 Azimuth to station in deg. E of N.  */
    char    DataSrc;      /* 91    108                                       */
    char    StaList;      /* Flag (*) to indicate info came from StaList     */
    
    int     Inst_type;    /* Type of instrument                              */
    double  Inst_gain;    /* Gain of instrument (microv/count)               */
    int     Sens_type;    /* Type of sensor                                  */
    double  Sens_gain;    /* Gain of sensor (volts/unit)                     */
    int     Sens_unit;    /* Sensor units disp=1; vel=2; acc=3               */
    double  GainFudge;    /* Additional gain factor.                         */
    double  SiteCorr;     /* Site correction factor.                         */
    double  sensitivity;  /* Channel sensitivity  counts/units               */
    int     Ignore;       /* Flag to ignore this phase                       */
    float   Duration;     /* Seconds to acquire                              */
    double  Stime;        /* Requested start time                            */
    int     PgaAvail;     /* Flag to say that trace has been measured        */
    double  Amp;          /* Raw Maximum Amplitude                           */
    double  Pga;          /* peak acceleration (cm/sec/sec)                  */
    double  Pgv;          /* peak velocity (cm/sec)                          */
    double  Pgd;          /* peak displacement (cm)                          */
    double  tpga;         /* time of peak acceleration                       */
    double  tpgv;         /* time of peak velocity                           */
    double  tpgd;         /* time of peak displacement                       */
    int     nRSA;         /* # pts describing response spectrum accel (RSA)  */
    double  freq[25];     /* frequencies at which RSA values are given       */
    double  period[25];   /* periods at which RSA values are given           */
    double  RSA[25];      /* RSA value for this channel at given frequency   */
    
    int     reject;        /*                   */
} ArkInfo;

/*****************************************************************************
 *  Define the structure for the Arkive.                                     *
 *  This is an abbreviated structure.                                        *
 *****************************************************************************/
    
typedef struct Arkive {   /* An Arkive List */
    char    ArkivMsg[MAX_BYTES_PER_EQ];
    long    InList;       /* Number of archive records in list            */
    double  EvntTime;     /* Time of Event (Julian Sec)                   */
    short   EvntYear;     /*   0-  1   0-  3 Event Year                   */
    short   EvntMonth;    /*   2-  3   4-  5 Event Month                  */
    short   EvntDay;      /*   4-  5   6-  7 Event Day                    */
    short   EvntHour;     /*   6-  7   8-  9 Event Hour                   */
    short   EvntMin;      /*   8-  9  10- 11 Event Minute                 */
    float   EvntSec;      /*  10- 13  12- 15 Event Second*100             */
    float   EvntLat;      /*  14- 20  16- 22 Geog coord of Event          */
    float   EvntLon;      /*  21- 28  23- 30 Geog coord of Event          */
    float   EvntDepth;    /*  29- 33  31- 35 Depth of Event*100           */
    long    NumPhs;       /*  36- 38  39- 41 # phases used in solution (final weights > 0.1) */
    long    Gap;          /*  39- 41  42- 44 Maximum azimuthal gap        */
    float   Dmin;         /*  42- 44  45- 47 Minimum distance to site     */
    float   rms;          /*  45- 48  48- 51 RMS travel time residual*100 */
    char    EvntIDS[10];  /* 128-137 136-145 Event ID                     */
    long    EvntID;       /* Event ID                                     */

    long    Current;      /* Index of Currently active channel */
    long    index[MAXCHANNELS];
    ArkInfo A[MAXCHANNELS];
    double  Pa;           /* Intercept of the PPick Vapp       */
    double  Pb;           /* PPick slope                       */
    double  VpApp;        /* PPick Vapp                        */
    double  Psiga;        /* PPick Intercept std dev           */
    double  Psigb;        /* PPick slope std dev               */
    int     nPGs;         /* # channels yielding peak motions  */
} Arkive;

/*****************************************************************************
 *  Define the structure for the individual Global thread.                   *
 *  This is the private area the thread needs to keep track                  *
 *  of all those variables unique to itself.                                 *
 *****************************************************************************/

struct Global {
    int     Debug;             /*                                            */
    int     WSDebug;           /*                                            */
    int     ButStatus;         /* -1 = idle; 0 = initiating; 1 = running     */
    int     readyflag;         /* ready for data                             */
    int     got_a_menu;
 
    int     ncomp;               /* Number of Acceptable component types               */
    char    Comp[MAX_COMPS][5];  /* Acceptable component types                         */
    int     nnet;                /* Number of Acceptable networks                      */
    char    Net[MAX_NETS][5];    /* Acceptable network codes                           */
    
    char    TraceBuf[MAXTRACEBUF]; /* This should work for 24-bit digitizers */
    char    mod[20];

    int     ntargets;                    /* Number of target directories               */
    char    target[MAX_TARGETS][100];    /* Target in form-> UserId@IPname:/directory/ */
    char    UserID[MAX_TARGETS][100];    /* Target in form-> UserId                    */
    char    Passwd[MAX_TARGETS][100];    /* Passwd (for OS2)                           */
    char    Host[MAX_TARGETS][100];      /* Target in form-> IPname                    */
    char    Directory[MAX_TARGETS][100]; /* Target in form-> /directory/               */

    char    person[MAX_PAGERS][50];
    int     nPager;              /* number of defined pager recipients                 */
    pid_t   pid;
    WS_MENU_QUEUE_REC menu_queue[MAX_WAVESERVERS];
    
    int      NSCN;               /* Number of SCNs we know about                       */
    ChanInfo Chan[MAXCHANNELS];
/* Globals to set from configuration file
 ****************************************/
    long    wsTimeout;           /* seconds to wait for reply from ws                  */
    int     nServer;             /* number of wave servers we know about               */
    long    RetryCount;          /* Retry count for waveserver errors.                 */
                                 /* list of available waveServers, from config. file   */
    int     inmenu[MAX_WAVESERVERS];
    int     wsLoc[MAX_WAVESERVERS];
    char    wsIp[MAX_WAVESERVERS][MAX_ADRLEN];
    char    wsPort[MAX_WAVESERVERS][MAX_ADRLEN];
    char    wsComment[MAX_WAVESERVERS][50];

    long    samp_sec;            /* samples/sec for current trace                      */
    long    Npts;                /* Number of points in trace                          */
    double  Mean;                /* Mean value of the last data gulp                   */
    
    double  Secs_Screen;         /* Maximum Number of secs of data to retrieve         */
    double  Pre_Event;           /* Maximum Number of secs before event to start plot  */

    int     nentries;            /* Number of menu entries for this SCN                */
    double  TStime[MAX_WAVESERVERS*2]; /* Tank start for this entry                    */
    double  TEtime[MAX_WAVESERVERS*2]; /* Tank end for this entry                      */
    int     index[MAX_WAVESERVERS*2];  /* WaveServer for this entry                    */
    
    int     nStaDB;              /* number of station databases we know about          */
    char    stationList[MAX_STADBS][STALIST_SIZ];
    
    char    indir[GDIRSZ];       /* Directory for manual-input arc files               */
    int     arcfileflg;          /* Flag set if we are accepting arc files             */
    int     UseDST;              /* Daylight Savings Time used when needed             */
    
    long    MaxDist;             /* Maximum offset distance for traces to plot.        */

};
typedef struct Global Global;


