#
# This is the qshake_feeder parameter file. This module gets "trigger" messages
# from the hypo_rings, and initiates threads to measure parameters.

#  Basic Earthworm setup:
#
LogSwitch        1              # 0 to completely turn off disk log file
MyModuleId       MOD_QSHAKER    # module id for this instance of qshake_feeder 

RingName         HYPO_RING      # ring to get input from
HeartBeatInt     5              # seconds between heartbeats

# List the message logos to grab from transport ring
#            Installation    Module        Message Type
 GetSumFrom  INST_WILDCARD   MOD_WILDCARD  TYPE_HYP2000ARC

#	others
MaxMsgSize  60000   # length of largest message we'll ever handle - in bytes
MaxMessages   100   # limit of number of message to buffer

# Directory for manual input of arc files
 ArcDir   /home/earthworm/run/gifs/qshaker/arc_indir

# We accept a command "Passes" which issues a heartbeat-type logit
# to let us know qshake_feeder is alive (debugging)
 Passes   800

# We accept a command "NoFlush" which prevents the ring from being
# flushed on restart.
# NoFlush
#
# A list of wave servers is supplied.
# We interrogate all wave servers as to what they have, and do the best we can
# to get as many traces as possible.

# List of wave servers (ip port) to contact to retrieve trace data.
# WaveServer     130.118.43.34   16020     " wsv3 dwr"
  WaveServer     130.118.43.34   16021     " wsv3 nano3"
  WaveServer     130.118.43.34   16022     " wsv3 reftek"
# WaveServer     130.118.43.34   16023     " wsv3 unr"
  WaveServer     130.118.43.34   16024     " wsv3 nano2"
  WaveServer     130.118.43.34   16025     " wsv3 nano1"
  WaveServer     130.118.43.34   16026     " wsv3 dst"
# WaveServer     130.118.43.34   16027     " wsv3 ucb"
# WaveServer     130.118.43.34   16028     " wsv3 cit"
  WaveServer     130.118.43.34   16029     " wsv3 k2nc1"
  WaveServer     130.118.43.34   16030     " wsv3 k2np"
# WaveServer     130.118.43.34   16031     " wsv3 ad1"
# WaveServer     130.118.43.34   16032     " wsv3 ad2"
# WaveServer     130.118.43.34   16033     " wsv3 ad3"
# WaveServer     130.118.43.34   16034     " wsv3 ad4"
# WaveServer     130.118.43.34   16035     " wsv3 ad5"
# WaveServer     130.118.43.34   16036     " wsv3 ad6"
  WaveServer     130.118.43.34   16039     " wsv2 k2nc2"

# WaveServer     130.118.43.4    16020     " wsv2 dwr"
  WaveServer     130.118.43.4    16021     " wsv2 nano3"
  WaveServer     130.118.43.4    16022     " wsv2 reftek"
# WaveServer     130.118.43.4    16023     " wsv2 unr"
  WaveServer     130.118.43.4    16024     " wsv2 nano2"
  WaveServer     130.118.43.4    16025     " wsv2 nano1"
  WaveServer     130.118.43.4    16026     " wsv2 dst"
# WaveServer     130.118.43.4    16027     " wsv2 ucb"
# WaveServer     130.118.43.4    16028     " wsv2 cit"
  WaveServer     130.118.43.4    16029     " wsv2 k2nc1"
  WaveServer     130.118.43.4    16030     " wsv2 k2np"
# WaveServer     130.118.43.4    16031     " wsv2 ad1"
# WaveServer     130.118.43.4    16032     " wsv2 ad2"
# WaveServer     130.118.43.4    16033     " wsv2 ad3"
# WaveServer     130.118.43.4    16034     " wsv2 ad4"
# WaveServer     130.118.43.4    16035     " wsv2 ad5"
# WaveServer     130.118.43.4    16036     " wsv2 ad6"
  WaveServer     130.118.43.4    16039     " wsv2 k2nc2"

 StationList     /home/earthworm/run/params/calsta.db1

#Pager   luetgert

# Network selection.
  Network  NC
  Network  NP
  Network  NN
#  Network  BK
#  Network  BP

# Data type selection.
  Component  HNN
  Component  HNE
  Component  HHN
  Component  HHE
  Component  BNN
  Component  BNE
  Component  BHN
  Component  BHE
#  Component  VLN
#  Component  VLE
  Component  ADN
  Component  ADE

    # *** Optional Commands ***
 
 MaxDist    100  # Max distance of traces to plot; default=100km
 RetryCount   2  # Number of attempts to get a trace from server; default=10

# We accept a command "Debug" which turns on a bunch of log messages
# Debug
# WSDebug

