/***************************************************************************
 * This thread is started by the processor thread manager. We're given     *
 * an integer which identifies who we are.                                 *
 *                                                                         *
 *     - This is the process controller.                                   *
 *       Read config file                                                  *
 *       Read Arkive msg and stuff info in ark structure.                  *
 *       Build table of data of interest                                   *
 *       Retrieve data traces                                              *
 *       Process traces                                                    *
 *       Publish                                                           *
 ***************************************************************************/

#include <platform.h>
#include <errno.h>
#include "math.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <decode.h>
#include <kom.h>
#include <chron3.h>
#include <time.h>
#include <time_ew.h>
#include <transport.h>
#include <swap.h>
#include <trace_buf.h>

#include <ws_clientII.h>
/*
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
*/
#include "qshaker.h"


DATABUF  pTrace;

/* Functions in this source file 
 *******************************/
int  MainProcess(Global *, Arkive *Ark);
void BitBuster(Global *But, double *Data, Arkive *Ark);
void DeSpike(Global *But, double *Data, Arkive *Ark);
void dspint(double *, double *, int *, int, int, int);
void medint(double *, double *, int arrlen, int winlen);
void Sort_Servers (Global *, double StartTime);
int  RequestWave(Global *, int, double *, char *, char *, char *, char *, double, double);
int  WSReqBin(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt);
int  WSReqBin2(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt);
int  Get_Ark(Arkive *);
void read_hypy2k(Arkive *, char *);
void read_phsy2k(ArkInfo *, char *, char *);
int  Build_Table(Global *, Arkive *, long *);
int  Chan_Info (Global *But, char *sta, char *comp, char *net, char *loc);
int  No_Dups (Arkive *Arkp, char *sta, char *comp, char *net, char *loc);
int  Build_Menu (Global *);
int  In_Menu (Global *, char *sta, char *comp, char *net, char *loc, double *, double *);
int  In_Menu_list (Global *, char *Site, char *Comp, char *Net, char *Loc);
void Sort_Ark (long, double *, long *);

void Build_SM_file(Global *, Arkive *);
void date22( double, char *);
void Get_Sta_Info(Global *);
void old_Get_Sta_Info(Global *);
int  distaz (double lat1, double lon1, double lat2, double lon2, 
                double *rngkm, double *faz, double *baz);
                
/* External Functions  
 ********************/
void HeartBeat( void );
void Write_MsgII(Arkive *Ark);
int pager1( char  *note, char  *person );

int peak_ground(double *Data, int npts, Arkive *Ark, int itype, double dt);
  
extern  Arkive      Ark;

char    module[20];           /* Global area for the module name   */
double  Data[MAXTRACELTH*2];    /* Trace arrays */
double  PltData[MAXTRACELTH*2]; /* Trace arrays */
double  Work[MAXTRACELTH*2];    /* Trace arrays */
int     spkflg[MAXTRACELTH*2];  /* Trace arrays */
int     Debug;
#define SWAP(a,b) temp=(a);(a)=(b);(b)=temp

/********************************************************************
 *  MainProcess is responsible for shepherding one event through    *
 *  the seismogram plotting string.                                 *
 *                                                                  *
 ********************************************************************/

int MainProcess(Global *But, Arkive *Ark)
{
    char        whoami[50], Text[200];
    char        Site[5], Net[3], Comp[5], Loc[5];
    time_t      current_time, start_time, end_time, proc_time;
    long        j, TooLate, next, InListToDo, We_Need[MAXCHANNELS];
    int         i, jj, jjj, k, error, successful, Traces_done, waitflg;
    double      Amplitude, Min, Max, oldlth, newlth;
    
    start_time = time(&current_time);
    /* Build the modname for error messages *
     ****************************************/
    sprintf(But->mod,"QShake_feeder");
    sprintf(module,"QShake_feeder");
    sprintf(whoami, " %s: %s: ", But->mod, "MainProcess");
    
    /* Let's see the message * 
     *************************/
    Debug = But->Debug;
    if (But->Debug ==1) {
        logit("et", "%s Begin trace processing for event:\n%.80s...\n", 
                whoami, Ark->ArkivMsg);
    }
    
    Get_Sta_Info(But);
    
    /* Go get all available information from the arkive msg. 
    ********************************************************/
    error = Get_Ark(Ark);
    if(error) {
        logit("e", "%s Error <%hd> reading arkive msg.\n", whoami, error);
        logit("e", "        There are %d entries in table.\n", Ark->InList);
        if(Ark->InList < 1) {
            logit("e", "exiting!\n");
            return 1;
        }
    }
    if(But->Debug) logit("e", "%s Got the arkive msg for %s.\n", whoami, Ark->EvntIDS);
    
    /* Set up the plotting parameters for each plot. 
     ************************************************/
    But->Secs_Screen = 60.0; /* Maximum Number of secs of data to retrieve        */
    But->Pre_Event   =  0.0; /* Maximum Number of secs before event to start plot */

    Build_Menu(But);  /*  Build the current wave server menus */
    
    if(!But->got_a_menu) {
        logit("e", "%s No Menu from %d servers! Just quit.\n", whoami, But->nServer);
        return 1;
    }
    /* Build the To-Do list from all available information. 
     ******************************************************/
    error = Build_Table(But, Ark, &InListToDo);
    if(error) {
        if(error>0) logit("e", "%s Error <%hd> building ToDo list.\n", whoami, error);
        logit("e", "        There are %d entries in table.\n", InListToDo);
        if(InListToDo < 1) {
            logit("e", "exiting!\n");
            for(j=0;j<But->nServer;j++) wsKillMenu(&(But->menu_queue[j]));
            return 1;
        }
    }
    if(But->Debug) logit("e", "%s Got the ToDo list.\n", whoami);
 
    next = 0;  /* First Initialize a bunch of stuff. */
    time(&current_time);
    TooLate = current_time + TIMEOUT;
    for(i=0;i<InListToDo;i++) We_Need[i] = 1;
    for(i=InListToDo;i<MAXCHANNELS;i++) We_Need[i] = 0;
    Traces_done = 0;
    
    /*  Process the traces by looping thru the To-Do list. 
    ******************************************************/
    while(current_time < TooLate && next < InListToDo) {
        if(We_Need[next]) {
            waitflg = 0;
try_again:
            k = Ark->Current = Ark->index[next];
            strcpy(Site, Ark->A[k].Site);
            strcpy(Comp, Ark->A[k].Comp);
            strcpy(Net, Ark->A[k].Net);
            strcpy(Loc, Ark->A[k].Loc);
            if(In_Menu_list(But, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, Ark->A[k].Loc )) {  
                successful = 0;
                Sort_Servers (But, Ark->A[k].Stime);
                if(But->nentries > 0) {
	                for(jj=0;jj<But->nentries;jj++) {
	                    jjj = But->index[jj];
	                
	                    Ark->A[k].Duration = (float)(But->Secs_Screen + 5.0 + Ark->A[k].Dist/6);
	                    Ark->A[k].Duration = (float)(15.0 + Ark->A[k].Dist/3);
	                    if(Ark->A[k].Stime + Ark->A[k].Duration > But->TEtime[jjj]) {
	                        oldlth = Ark->A[k].Duration;
	                        Ark->A[k].Duration = (float)(But->TEtime[jjj] - Ark->A[k].Stime);
	                        newlth = Ark->A[k].Duration;
	                
		                    if(But->Debug) 
		                        logit("et", "%s %s %s %s Trace length -> %f.  Available -> %f. Check the Menu.\n", 
		                            whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, oldlth, newlth);
					        sleep_ew(5000);
					        HeartBeat();
				            waitflg += 1;
		                    if(waitflg<10) {
							    for(j=0;j<But->nServer;j++) wsKillMenu(&(But->menu_queue[j]));
							    Build_Menu(But);  /*  Build the current wave server menus */
			                    goto try_again;
		                    }
	                    } else if(waitflg) {
	                        newlth = But->TEtime[jjj] - Ark->A[k].Stime;
		                    if(But->Debug) 
		                        logit("et", "%s %s %s %s %s Trace length -> %f.  Available -> %f.\n", 
		                            whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, Ark->A[k].Loc, Ark->A[k].Duration, newlth);
	                    }
	                    successful = RequestWave(But, jjj, Data, Site, Comp, Net, Loc, Ark->A[k].Stime, Ark->A[k].Duration);
	                        
	                    if(successful == 1) {   /*    Process this trace. */
	                        break;
	                    }
	                    else if(successful == 2) {
	                        if(But->Debug) 
	                            logit("e", "%s RequestWave error 2. %s %s %s %s\n", 
	                                whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, Ark->A[k].Loc);
	                        continue;
	                    }
	                    else if(successful == 3) {   /* Gap in data */
	                        if(But->Debug) 
	                            logit("e", "%s RequestWave error 3.\n", whoami);
	                        continue;
	                    }
	                    else if(successful == 4) {   /* Gap in data */
	                        if(But->Debug) 
	                            logit("e", "%s RequestWave error 4. Too many gaps.\n", whoami);
	                        continue;
	                    }
	                }
                }
                else {
                    if(But->Debug) 
                        logit("et", "%s %s %s %s No data available within window.\n", 
                            whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net);
                
                }
                if(successful == 1) {   /*    Process this trace.    
                                         ******************************/
    
                    if(Ark->A[k].Inst_type==2) DeSpike(But, Data, Ark);
             /*       if(strcmp(Ark->A[k].Site, "CBR")==0 || 
                       strcmp(Ark->A[k].Site, "NLH")==0 || 
                       strcmp(Ark->A[k].Site, "CPI")==0 || 
                       strcmp(Ark->A[k].Site, "JUM")==0 || 
                       strcmp(Ark->A[k].Site, "JCH")==0 ) BitBuster(But, Data, Ark);
*/
                    if(But->Debug) logit("e", "%s %s %s %s %s acquired. \n", 
                    		whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, Ark->A[k].Loc);

				    Min = Max = Data[0];
				    for(i=0;i<But->Npts;i++) {
				        if(Data[i] != 919191) {
				            if(Data[i]<Min) Min = Data[i];
				            if(Data[i]>Max) Max = Data[i];
				        }
				    }
				    Amplitude = Max - Min;
                    
                    Ark->A[k].reject = 0;
                    if(Amplitude < 15) {
                        Ark->A[k].reject = 1; 
	                    if(But->Debug) 
	                        logit("e", "%s %s %s %s rejected for low amplitude (%f).\n", 
	                            whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, Amplitude);
                    } 
                    
                    Ark->A[k].PgaAvail = Ark->A[k].reject==0? 1:0;
                    
                    if(Ark->A[k].PgaAvail) {
	                    for(j=0;j<But->Npts;j++) PltData[j] = Data[j]/Ark->A[k].sensitivity;
	            		peak_ground(PltData, But->Npts, Ark, Ark->A[k].Sens_unit, 1.0/But->samp_sec);
	                    
	                    /* Filter out potentially clipped lo-gain analogs  */
	                    if(strcmp(Ark->A[k].Comp, "VLZ")==0 || 
	                       strcmp(Ark->A[k].Comp, "VLE")==0 || 
	                       strcmp(Ark->A[k].Comp, "VLN")==0 ) {
	                    	if(Ark->A[k].Pgv <= 0.014) Write_MsgII(Ark);
	                    	else {
	                    		logit("e", "\n%s %s %s %s Probably clipped.\n", 
	                        		whoami, Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net);
	                    		Ark->A[k].PgaAvail = 0;
	                    	}
	                    } else {
	                    	Write_MsgII(Ark);
	                    }
	                    Traces_done   += 1;
                    }
                    
                    We_Need[next] = 0;
                }
            }  
        }
        next += 1;
        if(next >= InListToDo) break;
        time(&current_time);
    }
    
        /*  Report the results of processing.
        ***********************************************/
    if(Traces_done > 0) {
        
        Build_SM_file(But, Ark);
        
        proc_time = time(&current_time) - start_time;
        end_time  = (long)(time(&current_time) - Ark->EvntTime);
        logit("et", "%s Processing time: %d:%.2d Time since event: %d:%.2d\n", 
                    whoami, div(proc_time,60).quot, div(proc_time,60).rem, 
                            div(end_time,60).quot, div(end_time,60).rem); 
    
        sprintf(Text,"Event: %s  Sent to ShakeMap    Peak_values:%d Time_since_eq: %d:%.2d Vapp:%5.2f", 
            Ark->EvntIDS, Ark->nPGs, div(end_time,60).quot, div(end_time,60).rem, Ark->VpApp); 
        logit("et", "%s %s\n", whoami, Text); 
	    
        for(i=0;i<But->nPager;i++) pager1(Text, &But->person[i][0]); 
    }
    
    for(j=0;j<But->nServer;j++) wsKillMenu(&(But->menu_queue[j]));
    
    return 0;
}


/*******************************************************************************
 *    BitBuster removes erroneous bits which show up on some DST channels      *
 *                                                                             *
 *******************************************************************************/

void BitBuster(Global *But, double *Data, Arkive *Ark)
{
    char    SCNtxt[20];
    double  d;
    int     j, k, mask, sign, ii, i1, i2, width, sample, noise;
    int     a, b;

    noise = 15;
    for(j=0;j<But->Npts;j++) {
        d = Data[j];
        width = 6;
        i1 = j-width;  i2 = j+width;
        if(i1<0) i1 = 0;
        if(i2>But->Npts) i2 = But->Npts;
        sign = Data[j]<0.0? -1:1;
        mask = noise;
        for(ii=i1;ii<i2;ii++) {
            sample = (int)fabs(Data[ii]);
            if(ii != j && sample != 919191) mask = mask | sample;
        }
        sample = mask >> 1;
        while(sample>1) {
            mask = mask | sample;
            sample = sample >> 1;
        }
        mask = mask << 1;
        mask = mask | noise;
            
        sample = (int)fabs(Data[j]);
        if(sample != 919191) {
            Data[j] = sign*(sample & mask);
            if(Data[j] != d) {
                a = b = sample & (mask ^ 0xFFFF);
                ii = 0;
                for(i1=0;i1<32;i1++) {if((a&1) == 1) ii++; a=a>>1;}
                if(But->Debug) {
                    k = Ark->Current;
                      sprintf(SCNtxt, "%s %s %s", Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net);
                    logit("e", "%s %s. %x %7.0f %7.0f %7.0f %7.0f %5x %3d bit(s) at pt: %6d of %6d\n", 
                        "BitBuster:", SCNtxt, mask, 
                        Data[j-1], Data[j], Data[j+1], d, b, ii, j, But->Npts);
                }   
            }
        }
    }
}


/*******************************************************************************
 *    DeSpike removes erroneous bits which show up on some DST channels        *
 *                                                                             *
 *******************************************************************************/

void DeSpike(Global *But, double *Data, Arkive *Ark)
{
    int     i, k, npts, maxdur, minsiz, diff;
    double  spiketime;
    char    time1[30];

  /* Do the actual filtering */
	npts = But->Npts;
	maxdur = 3;
	minsiz = 100;
	dspint(Data, Work, spkflg, npts, maxdur, minsiz);

    k = Ark->Current;
    for(i=0;i<But->Npts;i++) {
		if(!spkflg[i]) {
			spiketime = Ark->A[k].Stime + i/But->samp_sec;
        	date22 (spiketime, time1);
        	diff = (int)(fabs(Data[i]-Work[i]));
			logit ("e", "%s_%s_%s  Spike found at %s. (%d)\n", 
					Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, time1, diff);
		}
        Data[i] = Work[i];
    }
}


/***************************************************************************/
/*      Function: dspint                                                   */
/*                                                                         */
/* A spike identification and removal routine for double data using        */
/* a running median filter technique to identify the spikes.               */
/*                                                                         */
/* After J.R. Evans, USGS, 02/13/90.                                       */
/***************************************************************************/

void dspint(double arrin[], double arrout[], int spkflg[], int npts, 
            int maxdur,  int minsiz)
{
	double x, y, d, dd;
	int    i, ii, iii, winlen, j;

	/* Check validity of parameters */

	if(maxdur < 1) {
		fprintf(stderr, "\nSelected spike duration (%7d) must be positive.\n", maxdur);
		fprintf(stderr, "NO FILTERING DONE.\n");
		return;
	}

	if(minsiz <= 0) {
		fprintf(stderr, "\nSelected spike hight (%7d) must be positive.\n", minsiz);
		fprintf(stderr, "STOPPING RUN.\n");
		exit(-1);
	}

	if(npts < ((2 * maxdur) + 1)) {
/*        fprintf(stderr, "Too few points (%d) in routine DSPINT\n", npts);
		fprintf(stderr, "for selected spike duration (%7d)\n", maxdur);
		fprintf(stderr, "NO FILTERING DONE.\n\n");*/
		return;
	}

	/* Each point is innocent until proven guilty */
	for(i=0;i<npts;i++) spkflg[i] = 1;

	/* Find window length for running median filter */
	winlen = 2 * maxdur + 1;

	/* Do RMF */
	medint(arrin, arrout, npts, winlen);

	/* Find spikes.  Mark all known null-data points for
	   later interpolation if asked. */

	for(i=0;i<npts;i++) {
		if(((int)(fabs(arrin[i] - arrout[i])) >= minsiz) ) spkflg[i] = 0;
	}

	/* Copy input to output prior to interpolation */
	for(i=0;i<npts;i++) arrout[i] = arrin[i];

	/* Do linear interpolation
	   (first and last points are guaranteed to be non-spike) */
	for(i=1;i<npts-1;i++) {
		if(!spkflg[i]) {
            ii = iii = i;        /* points to start of spike */
			while (++i < npts) {
				if(spkflg[i]) {
                    iii = i - 1;    /* points to end of spike */
                    break;        /* ... out of iii search */
				}
			}

			/* Interpolate it.  */

			d = (double)arrin[ii - 1];
			y = (double)(arrin[iii + 1] - arrin[ii - 1]);
			x = (double)(iii - ii + 2);
			for(j = ii ; j <= iii ; j++) {
				dd = d + y * (double)(j - (ii - 1)) / x;
				if(dd < 0.)
					arrout[j] = (int)(dd - 0.5);
				else
					arrout[j] = (int)(dd + 0.5);
			}
		}
	}
	return;
}


/***************************************************************************/
/*      Function: medint                                                   */
/*                                                                         */
/* An odd-length running median filter for integer data.                   */
/*                                                                         */
/***************************************************************************/

void medint(double arrin[], double arrout[], int arrlen, int winlen)
{
	int sort[10], subs[10], temp;
	int hww, iold, inew, jj, iout, i, key;

    hww = (winlen - 1) / 2;    /* "Half-window width", which in a zero-offset
				   array also points to middle-point of window */

	/* Test for errors */
	if (winlen < 3) {
		fprintf(stderr, " Window length too small.  winlen=%5i\n", winlen);
		winlen = 3;
	}
	if (winlen > arrlen) {
		fprintf(stderr, " Window length greater than trace length.\n");
		winlen = arrlen;
	}

	if (((2 * hww) + 1) != winlen) {
		fprintf(stderr, " Window must be odd length.  winlen=%5i\n", winlen);
		winlen = 2*hww + 1;
	}

	/* "Copy-on" ends of arrin where RMF is undefined */
	for(jj=0;jj<hww;jj++) arrout[jj] = arrin[jj];
	for(jj=arrlen - hww;jj<arrlen;jj++) arrout[jj] = arrin[jj];

	/* Fill sorting array for first time and bubble sort */
	for(i = 0 ; i < winlen ; i++) {
		subs[i] = i;
		sort[i] = (int)arrin[i];
	}

	key = 1;
    while(key) {    /* Ascending-order bubble sort */
		key = 0;
		for(i=1;i<winlen;i++) {
			if (sort[i-1] > sort[i]) {
				key = 1;
				SWAP(sort[i],sort[i-1]);
				SWAP(subs[i],subs[i-1]);
			}
		}
	}

	/* Put result (middle point of "sort") into "arrout" */
	arrout[hww] = sort[hww];

	/* Thereafter, drop in only the new point from "arrin" to save sorting time */
	for(iout = hww + 1 ; iout < arrlen - hww ; iout++) {
        iold = iout - (hww + 1);    /* points to oldest element still in sort window */
        inew = iout + hww;            /* points to element to be added to sort window */

        for(i=0;subs[i]!=iold;i++);    /* find old point in sort array */

        sort[i] = (int)arrin[inew];        /* replace old point in sort array */
		subs[i] = inew;

        while(i < winlen - 1) {        /* move point up list */
			if(sort[i] > sort[i + 1]) {
				SWAP(sort[i],sort[i+1]);
				SWAP(subs[i],subs[i+1]);
				i++;
			}
			else {
				break;
			}
		}
        while(i > 0) {            /* move point down list */
			if(sort[i - 1] > sort[i]) {
				SWAP(sort[i],sort[i-1]);
				SWAP(subs[i],subs[i-1]);
				i--;
			}
			else {
				break;
			}
		}

		/* Put result (middle point of "sort") into "arrout" */
		arrout[iout] = sort[hww];
	}
}

/*************************************************************************
 *   Sort_Servers                                                        *
 *      From the table of waveservers containing data for the current    *
 *      SCN, the table is re-sorted to provide an intelligent order of   *
 *      search for the data.                                             *
 *                                                                       *
 *      The strategy is to start by dividing the possible waveservers    *
 *      into those which contain the requested StartTime and those which *
 *      don't.  Those which do are retained in the order specified in    *
 *      the config file allowing us to specify a preference for certain  *
 *      waveservers.  Those waveservers which do not contain the         *
 *      requested StartTime are sorted such that the possible data       *
 *      retrieved is maximized.                                          *
 *************************************************************************/

void Sort_Servers (Global *But, double StartTime)
{
    char    whoami[50], c22[25];
    double  tdiff[MAX_WAVESERVERS*2];
    int     j, k, jj, kk, hold, index[MAX_WAVESERVERS*2];
    
    sprintf(whoami, " %s: %s: ", But->mod, "Sort_Servers");
        /* Throw out servers with data too old. */
    j = 0;
    while(j<But->nentries) {
        k = But->index[j];
        if(StartTime > But->TEtime[k]) {
            if(But->Debug) {
                date22( StartTime, c22);
                logit("e","%s %d %d  %s", whoami, j, k, c22);
                    logit("e", " %s %s <%s>\n", 
                          But->wsIp[k], But->wsPort[k], But->wsComment[k]);
                date22( But->TEtime[k], c22);
                logit("e","ends at: %s rejected.\n", c22);
            }
     /*       But->inmenu[k] = 0;    */
            But->nentries -= 1;
            for(jj=j;jj<But->nentries;jj++) {
                But->index[jj] = But->index[jj+1];
            }
        } else j++;
    }
    if(But->nentries <= 1) return;  /* nothing to sort */
            
    /* Calculate time differences between StartTime needed and tankStartTime */
    /* And copy positive values to the top of the list in the order given    */
    jj = 0;
    for(j=0;j<But->nentries;j++) {
        k = index[j] = But->index[j];
        tdiff[k] = StartTime - But->TStime[k];
        if(tdiff[k]>=0) {
            But->index[jj++] = index[j];
            tdiff[k] = -65000000; /* two years should be enough of a flag */
        }
    }
    
    /* Sort the index list copy in descending order */
    j = 0;
    do {
        k = index[j];
        for(jj=j+1;jj<But->nentries;jj++) {
            kk = index[jj];
            if(tdiff[kk]>tdiff[k]) {
                hold = index[j];
                index[j] = index[jj];
                index[jj] = hold;
            }
            k = index[j];
        }
        j += 1;
    } while(j < But->nentries);
    
    /* Then transfer the negatives */
    for(j=jj,k=0;j<But->nentries;j++,k++) {
        But->index[j] = index[k];
    }
}    


/********************************************************************
 *  RequestWave                                                     *
 *   This is the binary version                                     *
 *   k - waveserver index                                           *
 ********************************************************************/
int RequestWave(Global *But, int k, double *Data,  
            char *Site, char *Comp, char *Net, char *Loc, double Stime, double Duration)
{
    char     whoami[50], SCNtxt[17];
    int      i, ret, iEnd, npoints, gap0;
    double   mean;
    TRACE_REQ   request;
    GAP *pGap;
    
    sprintf(whoami, " %s: %s: ", But->mod, "RequestWave");
    strcpy(request.sta,  Site);
    strcpy(request.chan, Comp);
    strcpy(request.net,  Net );
    strcpy(request.loc,  Loc );   
    request.waitSec = 0;
    request.pinno = 0;
    request.reqStarttime = Stime;
    request.reqEndtime   = request.reqStarttime + Duration;
    request.partial = 1;
    request.pBuf    = But->TraceBuf;
    request.bufLen  = MAXTRACELTH*9;
    request.timeout = But->wsTimeout;
    request.fill    = 919191;
    sprintf(SCNtxt, "%s %s %s %s", Site, Comp, Net, Loc);

    /* Clear out the gap list */
    if(pTrace.nGaps != 0) {
        while ( (pGap = pTrace.gapList) != (GAP *)NULL) {
            pTrace.gapList = pGap->next;
            free(pGap);
        }
    }
    pTrace.nGaps = 0;

    if(But->wsLoc[k]==0) {
	    ret = WSReqBin(But, k, &request, &pTrace, SCNtxt);
    } else {
	    ret = WSReqBin2(But, k, &request, &pTrace, SCNtxt);
    }
    
  /* Find mean value of non-gap data */
    pGap = pTrace.gapList;
    i = npoints = 0L;
    mean    = 0.0;
  /*
   * Loop over all the data, skipping any gaps. Note that a `gap' will not be declared
   * at the end of the data, so the counter `i' will always get to pTrace.nRaw.
   */
    do {
        iEnd = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->firstSamp - 1;
        if (pGap != (GAP *)NULL) { /* Test for gap within peak-search window */
            gap0 = pGap->lastSamp - pGap->firstSamp + 1;
            if(Debug) logit("t", "trace from <%s> has %d point gap in window at %d\n", SCNtxt, gap0, pGap->firstSamp);
        }
        for (; i < iEnd; i++) {
            mean += pTrace.rawData[i];
            npoints++;
        }
        if (pGap != (GAP *)NULL) {     /* Move the counter over this gap */    
            i = pGap->lastSamp + 1;
            pGap = pGap->next;
        }
    } while (i < pTrace.nRaw );
  
    mean /= (double)npoints;
  
  /* Now remove the mean, and set points inside gaps to zero */
    i = 0;
    do {
        iEnd = (pGap == (GAP *)NULL)? pTrace.nRaw:pGap->firstSamp - 1;
        for (; i < iEnd; i++) pTrace.rawData[i] -= mean;

        if (pGap != (GAP *)NULL) {    /* Fill in the gap with zeros */    

            for ( ;i < pGap->lastSamp + 1; i++) pTrace.rawData[i] = 0.0;

            pGap = pGap->next;
        }
    } while (i < pTrace.nRaw );

    But->Npts = pTrace.nRaw;
    if(But->Npts>MAXTRACELTH) {
        logit("e","%s Trace: %s Too many points: %d\n", whoami, SCNtxt, But->Npts);
        But->Npts = MAXTRACELTH;
    }
    for(i=0;i<But->Npts;i++) Data[i] = pTrace.rawData[i];
    But->Mean = 0.0;
    if(pTrace.nGaps > 500) ret = 4;

    return ret;
}

/********************************************************************
 *  WSReqBin                                                        *
 *                                                                  *
 *   k - waveserver index                                           *
 ********************************************************************/
int WSReqBin(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt)
{
    char     server[wsADRLEN*3], whoami[50];
    int      i, kk, io, retry;
    int      isamp, nsamp, gap0, success, ret, WSDebug = 0;          
    long     iEnd, npoints;
    WS_MENU  menu = NULL;
    WS_PSCNL  pscn = NULL;
    double   mean, traceEnd, samprate;
    long    *longPtr;
    short   *shortPtr;
    
    TRACE_HEADER *pTH;
    TRACE_HEADER *pTH4;
    char tbuf[MAX_TRACEBUF_SIZ];
    GAP *pGap, *newGap;
    
    sprintf(whoami, " %s: %s: ", But->mod, "WSReqBin");
    WSDebug = But->WSDebug;
    success = retry = 0;
    
gettrace:
    menu = NULL;
    /*    Put out WaveServer request here and wait for response */
    /* Get the trace
     ***************/
    /* rummage through all the servers we've been told about */
    if ( (wsSearchSCN( request, &menu, &pscn, &But->menu_queue[k]  )) == WS_ERR_NONE ) {
        strcpy(server, menu->addr);
        strcat(server, "  ");
        strcat(server, menu->port);
    } else {
        strcpy(server, "unknown");
    }
    
/* initialize the global trace buffer, freeing old GAP structures. */
    pTrace->nRaw  = 0L;
    pTrace->delta     = 0.0;
    pTrace->starttime = 0.0;
    pTrace->endtime   = 0.0;
    pTrace->nGaps = 0;

    /* Clear out the gap list */
    pTrace->nGaps = 0;
    while ( (pGap = pTrace->gapList) != (GAP *)NULL) {
        pTrace->gapList = pGap->next;
        free(pGap);
    }
 
    if(WSDebug) {     
        logit("e","\n%s Issuing request to wsGetTraceBin: server: %s Socket: %d.\n", 
             whoami, server, menu->sock);
        logit("e","    %s %f %f %d\n", SCNtxt,
             request->reqStarttime, request->reqEndtime, request->timeout);
    }

    io = wsGetTraceBin(request, &But->menu_queue[k], But->wsTimeout);
    
    if (io == WS_ERR_NONE ) {
        if(WSDebug) {
            logit("e"," %s server: %s trace %s: went ok first time. Got %ld bytes\n",
                whoami, server, SCNtxt, request->actLen); 
            logit("e","        actStarttime=%lf, actEndtime=%lf, actLen=%ld, samprate=%lf\n",
                  request->actStarttime, request->actEndtime, request->actLen, request->samprate);
        }
    }
    else {
        switch(io) {
        case WS_ERR_EMPTY_MENU:
        case WS_ERR_SCNL_NOT_IN_MENU:
        case WS_ERR_BUFFER_OVERFLOW:
            if (io == WS_ERR_EMPTY_MENU ) 
                logit("e"," %s server: %s No menu found.  We might as well quit.\n", whoami, server); 
            if (io == WS_ERR_SCNL_NOT_IN_MENU ) 
                logit("e"," %s server: %s Trace %s not in menu\n", whoami, server, SCNtxt);
            if (io == WS_ERR_BUFFER_OVERFLOW ) 
                logit("e"," %s server: %s Trace %s overflowed buffer. Fatal.\n", whoami, server, SCNtxt); 
            return 2;                /*   We might as well quit */
        
        case WS_ERR_PARSE:
        case WS_ERR_TIMEOUT:
        case WS_ERR_BROKEN_CONNECTION:
        case WS_ERR_NO_CONNECTION:
            sleep_ew(500);
            retry += 1;
            if (io == WS_ERR_PARSE )
                logit("e"," %s server: %s Trace %s: Couldn't parse server's reply. Try again.\n", 
                        whoami, server, SCNtxt); 
            if (io == WS_ERR_TIMEOUT )
                logit("e"," %s server: %s Trace %s: Timeout to wave server. Try again.\n", whoami, server, SCNtxt); 
            if (io == WS_ERR_BROKEN_CONNECTION ) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Broken connection to wave server. Try again.\n", 
                whoami, server, SCNtxt); 
            }
            if (io == WS_ERR_NO_CONNECTION ) {
                if(WSDebug || retry>1) 
                    logit("e"," %s server: %s Trace %s: No connection to wave server.\n", whoami, server, SCNtxt); 
                if(WSDebug) logit("e"," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
            }
            ret = wsAttachServer( menu, But->wsTimeout );
            if(ret == WS_ERR_NONE && retry < But->RetryCount) goto gettrace;
            if(WSDebug) {   
                switch(ret) {
                case WS_ERR_NO_CONNECTION:
                    logit("e"," %s server: %s: No connection to wave server.\n", whoami, server); 
                    break;
                case WS_ERR_SOCKET:
                    logit("e"," %s server: %s: Socket error.\n", whoami, server); 
                    break;
                case WS_ERR_INPUT:
                    logit("e"," %s server: %s: Menu missing.\n", whoami, server); 
                    break;
                default:
                    logit("e"," %s server: %s: wsAttachServer error %d.\n", whoami, server, ret); 
                }
            }
            return 2;                /*   We might as well quit */
        
        case WS_WRN_FLAGGED:
            if((int)strlen(&request->retFlag)>0) {
                if(WSDebug)  
                logit("e","%s server: %s Trace %s: return flag from wsGetTraceBin: <%c>\n %.50s\n", 
                        whoami, server, SCNtxt, request->retFlag, But->TraceBuf);
            }
            if(WSDebug) logit("e"," %s server: %s Trace %s: No trace available. Wave server returned %c\n", 
                whoami, server, SCNtxt, request->retFlag); 
            break;
      /*      return 2;                   We might as well quit */
        
        default:
            logit( "et","%s server: %s Failed.  io = %d\n", whoami, server, io );
        }
    }
    
    /* Transfer trace data from TRACE_BUF packets into Trace buffer */
    traceEnd = (request->actEndtime < request->reqEndtime) ?  request->actEndtime :
                                                              request->reqEndtime;
    pTH  = (TRACE_HEADER *)request->pBuf;
    pTH4 = (TRACE_HEADER *)tbuf;
    /*
    * Swap to local byte-order. Note that we will be calling this function
    * twice for the first TRACE_BUF packet; this is OK as the second call
    * will have no effect.
    */

    memcpy( pTH4, pTH, sizeof(TRACE_HEADER) );
    if(WSDebug) logit( "e","%s server: %s Make Local\n", whoami, server, io );
    if (WaveMsgMakeLocal(pTH4) == -1) {
        logit("et", "%s server: %s %s.%s.%s unknown datatype <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
        return 5;
    }
    if (WaveMsgMakeLocal(pTH4) == -2) {
        logit("et", "%s server: %s %s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
    /*  return 5;  */
    }

    if(WSDebug) logit("e"," %s server: %s Trace %s: Data has samprate %g.\n", 
                     whoami, server, SCNtxt, pTH4->samprate); 
    if (pTH4->samprate < 0.1) {
        logit("et", "%s server: %s %s.%s.%s (%s) has zero samplerate (%g); skipping trace\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, SCNtxt, pTH4->samprate);
        return 5;
    }
    But->samp_sec = pTH4->samprate<=0? 100L:(long)pTH4->samprate;

    pTrace->delta = 1.0/But->samp_sec;
    samprate = But->samp_sec;   /* Save rate of first packet to compare with later packets */
    pTrace->starttime = request->reqStarttime;
    /* Set Trace endtime so it can be used to test for gap at start of data */
    pTrace->endtime = ( (pTH4->starttime < request->reqStarttime) ?
                        pTH4->starttime : request->reqStarttime) - 0.5*pTrace->delta ;
    if(WSDebug) logit("e"," pTH->starttime: %f request->reqStarttime: %f delta: %f endtime: %f.\n", 
                     pTH4->starttime, request->reqStarttime, pTrace->delta, pTrace->endtime); 

  /* Look at all the retrieved TRACE_BUF packets 
   * Note that we must copy each tracebuf from the big character buffer
   * to the TRACE_BUF structure pTH4.  This is because of the occasionally 
   * seen case of a channel putting an odd number of i2 samples into
   * its tracebufs!  */
    kk = 0;
    while( pTH < (TRACE_HEADER *)(request->pBuf + request->actLen) ) {
        memcpy( pTH4, pTH, sizeof(TRACE_HEADER) );
         /* Swap bytes to local order */
	    if (WaveMsgMakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsgMakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
	    /*  return 5;  */
        }
    
        nsamp = pTH4->nsamp;
        memcpy( pTH4, pTH, sizeof(TRACE_HEADER) + nsamp*4 );
         /* Swap bytes to local order */
	    if (WaveMsgMakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsgMakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->datatype);
	    /*  return 5;  */
        }
    
        if ( fabs(pTH4->samprate - samprate) > 1.0) {
            logit("et", "%s <%s.%s.%s samplerate change: %f - %f; discarding trace\n",
                whoami, pTH4->sta, pTH4->chan, pTH4->net, samprate, pTH4->samprate);
            return 5;
        }
    
    /* Check for gap */
        if (pTrace->endtime + 1.5 * pTrace->delta < pTH4->starttime) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Gap detected.\n", 
                        whoami, server, SCNtxt); 
            if ( (newGap = (GAP *)calloc(1, sizeof(GAP))) == (GAP *)NULL) {
                logit("et", "getTraceFromWS: out of memory for GAP struct\n");
                return -1;
            }
            newGap->starttime = pTrace->endtime + pTrace->delta;
            newGap->gapLen = pTH4->starttime - newGap->starttime;
            newGap->firstSamp = pTrace->nRaw;
            newGap->lastSamp  = pTrace->nRaw + (long)( (newGap->gapLen * samprate) - 0.5);
            if(WSDebug) logit("e"," starttime: %f gaplen: %f firstSamp: %d lastSamp: %d.\n", 
                newGap->starttime, newGap->gapLen, newGap->firstSamp, newGap->lastSamp); 
            /* Put GAP struct on list, earliest gap first */
            if (pTrace->gapList == (GAP *)NULL)
                pTrace->gapList = newGap;
            else
                pGap->next = newGap;
            pGap = newGap;  /* leave pGap pointing at the last GAP on the list */
            pTrace->nGaps++;

            /* Advance the Trace pointers past the gap; maybe gap will get filled */
            pTrace->nRaw = newGap->lastSamp + 1;
            pTrace->endtime += newGap->gapLen;
        }
    
        isamp = (pTrace->starttime > pTH4->starttime)?
                (long)( 0.5 + (pTrace->starttime - pTH4->starttime) * samprate):0;

        if (request->reqEndtime < pTH4->endtime) {
            nsamp = pTH4->nsamp - (long)( 0.5 * (pTH4->endtime - request->reqEndtime) * samprate);
            pTrace->endtime = request->reqEndtime;
        } 
        else {
            nsamp = pTH4->nsamp;
            pTrace->endtime = pTH4->endtime;
        }

    /* Assume trace data is integer valued here, long or short */    
        if (pTH4->datatype[1] == '4') {
            longPtr=(long*) ((char*)pTH4 + sizeof(TRACE_HEADER) + isamp * 4);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *longPtr;
                longPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF message */
            pTH = (TRACE_HEADER *)((char *)pTH + sizeof(TRACE_HEADER) + pTH4->nsamp * 4);
        }
        else {   /* pTH->datatype[1] == 2, we assume */
            shortPtr=(short*) ((char*)pTH4 + sizeof(TRACE_HEADER) + isamp * 2);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *shortPtr;
                shortPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF packets */
            pTH = (TRACE_HEADER *)((char *)pTH + sizeof(TRACE_HEADER) + pTH4->nsamp * 2);
        }
    }  /* End of loop over TRACE_BUF packets */
  

    if (io == WS_ERR_NONE ) success = 1;

    return success;
}


/********************************************************************
 *  WSReqBin2                                                       *
 *                                                                  *
 *  Retrieves binary data from location code enabled waveservers.   *
 *   k - waveserver index                                           *
 ********************************************************************/
int WSReqBin2(Global *But, int k, TRACE_REQ *request, DATABUF *pTrace, char *SCNtxt)
{
    char     server[wsADRLEN*3], whoami[50];
    int      i, kk, io, retry;
    int      isamp, nsamp, gap0, success, ret, WSDebug = 0;          
    long     iEnd, npoints;
    WS_MENU  menu = NULL;
    WS_PSCNL  pscn = NULL;
    double   mean, traceEnd, samprate;
    long    *longPtr;
    short   *shortPtr;
    
    TRACE2_HEADER *pTH;
    TRACE2_HEADER *pTH4;
    char tbuf[MAX_TRACEBUF_SIZ];
    GAP *pGap, *newGap;
    
    sprintf(whoami, " %s: %s: ", But->mod, "WSReqBin2");
    WSDebug = But->WSDebug;
    success = retry = 0;
    
gettrace:
    menu = NULL;
    /*    Put out WaveServer request here and wait for response */
    /* Get the trace
     ***************/
    /* rummage through all the servers we've been told about */
    if ( (wsSearchSCNL( request, &menu, &pscn, &But->menu_queue[k]  )) == WS_ERR_NONE ) {
        strcpy(server, menu->addr);
        strcat(server, "  ");
        strcat(server, menu->port);
    } else {
        strcpy(server, "unknown");
    }
    
/* initialize the global trace buffer, freeing old GAP structures. */
    pTrace->nRaw  = 0L;
    pTrace->delta     = 0.0;
    pTrace->starttime = 0.0;
    pTrace->endtime   = 0.0;
    pTrace->nGaps = 0;

    /* Clear out the gap list */
    pTrace->nGaps = 0;
    while ( (pGap = pTrace->gapList) != (GAP *)NULL) {
        pTrace->gapList = pGap->next;
        free(pGap);
    }
 
    if(WSDebug) {     
        logit("e","\n%s Issuing request to wsGetTraceBinL: server: %s Socket: %d.\n", 
             whoami, server, menu->sock);
        logit("e","    %s %f %f %d\n", SCNtxt,
             request->reqStarttime, request->reqEndtime, request->timeout);
    }

    io = wsGetTraceBinL(request, &But->menu_queue[k], But->wsTimeout);
    
    if (io == WS_ERR_NONE ) {
        if(WSDebug) {
            logit("e"," %s server: %s trace %s: went ok first time. Got %ld bytes\n",
                whoami, server, SCNtxt, request->actLen); 
            logit("e","        actStarttime=%lf, actEndtime=%lf, actLen=%ld, samprate=%lf\n",
                  request->actStarttime, request->actEndtime, request->actLen, request->samprate);
        }
    }
    else {
        switch(io) {
        case WS_ERR_EMPTY_MENU:
        case WS_ERR_SCNL_NOT_IN_MENU:
        case WS_ERR_BUFFER_OVERFLOW:
            if (io == WS_ERR_EMPTY_MENU ) 
                logit("e"," %s server: %s No menu found.  We might as well quit.\n", whoami, server); 
            if (io == WS_ERR_SCNL_NOT_IN_MENU ) 
                logit("e"," %s server: %s Trace %s not in menu\n", whoami, server, SCNtxt);
            if (io == WS_ERR_BUFFER_OVERFLOW ) 
                logit("e"," %s server: %s Trace %s overflowed buffer. Fatal.\n", whoami, server, SCNtxt); 
            return 2;                /*   We might as well quit */
        
        case WS_ERR_PARSE:
        case WS_ERR_TIMEOUT:
        case WS_ERR_BROKEN_CONNECTION:
        case WS_ERR_NO_CONNECTION:
            sleep_ew(500);
            retry += 1;
            if (io == WS_ERR_PARSE )
                logit("e"," %s server: %s Trace %s: Couldn't parse server's reply. Try again.\n", 
                        whoami, server, SCNtxt); 
            if (io == WS_ERR_TIMEOUT )
                logit("e"," %s server: %s Trace %s: Timeout to wave server. Try again.\n", whoami, server, SCNtxt); 
            if (io == WS_ERR_BROKEN_CONNECTION ) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Broken connection to wave server. Try again.\n", 
                whoami, server, SCNtxt); 
            }
            if (io == WS_ERR_NO_CONNECTION ) {
                if(WSDebug || retry>1) 
                    logit("e"," %s server: %s Trace %s: No connection to wave server.\n", whoami, server, SCNtxt); 
                if(WSDebug) logit("e"," %s server: %s: Socket: %d.\n", whoami, server, menu->sock); 
            }
            ret = wsAttachServer( menu, But->wsTimeout );
            if(ret == WS_ERR_NONE && retry < But->RetryCount) goto gettrace;
            if(WSDebug) {   
                switch(ret) {
                case WS_ERR_NO_CONNECTION:
                    logit("e"," %s server: %s: No connection to wave server.\n", whoami, server); 
                    break;
                case WS_ERR_SOCKET:
                    logit("e"," %s server: %s: Socket error.\n", whoami, server); 
                    break;
                case WS_ERR_INPUT:
                    logit("e"," %s server: %s: Menu missing.\n", whoami, server); 
                    break;
                default:
                    logit("e"," %s server: %s: wsAttachServer error %d.\n", whoami, server, ret); 
                }
            }
            return 2;                /*   We might as well quit */
        
        case WS_WRN_FLAGGED:
            if((int)strlen(&request->retFlag)>0) {
                if(WSDebug)  
                logit("e","%s server: %s Trace %s: return flag from wsGetTraceBin: <%c>\n %.50s\n", 
                        whoami, server, SCNtxt, request->retFlag, But->TraceBuf);
            }
            if(WSDebug) logit("e"," %s server: %s Trace %s: No trace available. Wave server returned %c\n", 
                whoami, server, SCNtxt, request->retFlag); 
            break;
      /*      return 2;                   We might as well quit */
        
        default:
            logit( "et","%s server: %s Failed.  io = %d\n", whoami, server, io );
        }
    }
    
    /* Transfer trace data from TRACE_BUF packets into Trace buffer */
    traceEnd = (request->actEndtime < request->reqEndtime) ?  request->actEndtime :
                                                              request->reqEndtime;
    pTH  = (TRACE2_HEADER *)request->pBuf;
    pTH4 = (TRACE2_HEADER *)tbuf;
    /*
    * Swap to local byte-order. Note that we will be calling this function
    * twice for the first TRACE_BUF packet; this is OK as the second call
    * will have no effect.
    */

    memcpy( pTH4, pTH, sizeof(TRACE2_HEADER) );
    if(WSDebug) logit( "e","%s server: %s Make Local\n", whoami, server, io );
    if (WaveMsg2MakeLocal(pTH4) == -1) {
        logit("et", "%s server: %s %s.%s.%s.%s unknown datatype <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
        return 5;
    }
    if (WaveMsg2MakeLocal(pTH4) == -2) {
        logit("et", "%s server: %s %s.%s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
    /*  return 5;  */
    }

    if(WSDebug) logit("e"," %s server: %s Trace %s: Data has samprate %g.\n", 
                     whoami, server, SCNtxt, pTH4->samprate); 
    if (pTH4->samprate < 0.1) {
        logit("et", "%s server: %s %s.%s.%s.%s (%s) has zero samplerate (%g); skipping trace\n",
              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, SCNtxt, pTH4->samprate);
        return 5;
    }
    But->samp_sec = pTH4->samprate<=0? 100L:(long)pTH4->samprate;

    pTrace->delta = 1.0/But->samp_sec;
    samprate = But->samp_sec;   /* Save rate of first packet to compare with later packets */
    pTrace->starttime = request->reqStarttime;
    /* Set Trace endtime so it can be used to test for gap at start of data */
    pTrace->endtime = ( (pTH4->starttime < request->reqStarttime) ?
                        pTH4->starttime : request->reqStarttime) - 0.5*pTrace->delta ;
    if(WSDebug) logit("e"," pTH->starttime: %f request->reqStarttime: %f delta: %f endtime: %f.\n", 
                     pTH4->starttime, request->reqStarttime, pTrace->delta, pTrace->endtime); 

  /* Look at all the retrieved TRACE_BUF packets 
   * Note that we must copy each tracebuf from the big character buffer
   * to the TRACE_BUF structure pTH4.  This is because of the occasionally 
   * seen case of a channel putting an odd number of i2 samples into
   * its tracebufs!  */
    kk = 0;
    while( pTH < (TRACE2_HEADER *)(request->pBuf + request->actLen) ) {
        memcpy( pTH4, pTH, sizeof(TRACE2_HEADER) );
         /* Swap bytes to local order */
	    if (WaveMsg2MakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsg2MakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	    /*  return 5;  */
        }
    
        nsamp = pTH4->nsamp;
        memcpy( pTH4, pTH, sizeof(TRACE2_HEADER) + nsamp*4 );
         /* Swap bytes to local order */
	    if (WaveMsg2MakeLocal(pTH4) == -1) {
	        logit("et", "%s server: %s %s.%s.%s.%s unknown datatype <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	        return 5;
	    }
	    if (WaveMsg2MakeLocal(pTH4) == -2) {
	        logit("et", "%s server: %s %s.%s.%s.%s found funky packet with suspect header values!! <%s>; skipping\n",
	              whoami, server, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, pTH4->datatype);
	    /*  return 5;  */
        }
    
        if ( fabs(pTH4->samprate - samprate) > 1.0) {
            logit("et", "%s <%s.%s.%s.%s samplerate change: %f - %f; discarding trace\n",
                whoami, pTH4->sta, pTH4->chan, pTH4->net, pTH4->loc, samprate, pTH4->samprate);
            return 5;
        }
    
    /* Check for gap */
        if (pTrace->endtime + 1.5 * pTrace->delta < pTH4->starttime) {
            if(WSDebug) logit("e"," %s server: %s Trace %s: Gap detected.\n", 
                        whoami, server, SCNtxt); 
            if ( (newGap = (GAP *)calloc(1, sizeof(GAP))) == (GAP *)NULL) {
                logit("et", "getTraceFromWS: out of memory for GAP struct\n");
                return -1;
            }
            newGap->starttime = pTrace->endtime + pTrace->delta;
            newGap->gapLen = pTH4->starttime - newGap->starttime;
            newGap->firstSamp = pTrace->nRaw;
            newGap->lastSamp  = pTrace->nRaw + (long)( (newGap->gapLen * samprate) - 0.5);
            if(WSDebug) logit("e"," starttime: %f gaplen: %f firstSamp: %d lastSamp: %d.\n", 
                newGap->starttime, newGap->gapLen, newGap->firstSamp, newGap->lastSamp); 
            /* Put GAP struct on list, earliest gap first */
            if (pTrace->gapList == (GAP *)NULL)
                pTrace->gapList = newGap;
            else
                pGap->next = newGap;
            pGap = newGap;  /* leave pGap pointing at the last GAP on the list */
            pTrace->nGaps++;

            /* Advance the Trace pointers past the gap; maybe gap will get filled */
            pTrace->nRaw = newGap->lastSamp + 1;
            pTrace->endtime += newGap->gapLen;
        }
    
        isamp = (pTrace->starttime > pTH4->starttime)?
                (long)( 0.5 + (pTrace->starttime - pTH4->starttime) * samprate):0;

        if (request->reqEndtime < pTH4->endtime) {
            nsamp = pTH4->nsamp - (long)( 0.5 * (pTH4->endtime - request->reqEndtime) * samprate);
            pTrace->endtime = request->reqEndtime;
        } 
        else {
            nsamp = pTH4->nsamp;
            pTrace->endtime = pTH4->endtime;
        }

    /* Assume trace data is integer valued here, long or short */    
        if (pTH4->datatype[1] == '4') {
            longPtr=(long*) ((char*)pTH4 + sizeof(TRACE2_HEADER) + isamp * 4);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *longPtr;
                longPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF message */
            pTH = (TRACE2_HEADER *)((char *)pTH + sizeof(TRACE2_HEADER) + pTH4->nsamp * 4);
        }
        else {   /* pTH->datatype[1] == 2, we assume */
            shortPtr=(short*) ((char*)pTH4 + sizeof(TRACE2_HEADER) + isamp * 2);
            for ( ;isamp < nsamp; isamp++) {
                pTrace->rawData[pTrace->nRaw] = (double) *shortPtr;
                shortPtr++;
                pTrace->nRaw++;
                if(pTrace->nRaw >= MAXTRACELTH*5) break;
            }
            /* Advance pTH to the next TRACE_BUF packets */
            pTH = (TRACE2_HEADER *)((char *)pTH + sizeof(TRACE2_HEADER) + pTH4->nsamp * 2);
        }
    }  /* End of loop over TRACE_BUF packets */
  

    if (io == WS_ERR_NONE ) success = 1;

    return success;
}


/********************************************************************
 *    Get_Ark is responsible for reading the archive msg.           *
 *                                                                  *
 ********************************************************************/

int Get_Ark(Arkive *Ark)
{
    char    *in, temp[5];   /* working pointer to archive message    */
    char    line[MAX_STR];  /* to store lines from msg               */
    int     msglen;         /* length of input archive message       */
    int     nline;          /* number of lines (not shadows) so far  */
    int     i;
    
  /* Initialize some stuff
   ***********************/
    nline  = 0;
    msglen = strlen( Ark->ArkivMsg );
    in     = Ark->ArkivMsg;
    
    /* Initialize the ArkList    
    *************************/
    Ark->InList = 0;
    for(i=0;i<MAXCHANNELS;i++) {
        Ark->A[i].PPick    = Ark->A[i].SPick = 0.0;
        Ark->A[i].PgaAvail = 0;
        Ark->A[i].StaList = ' ';
        strcpy(Ark->A[i].Site, " ");
        strcpy(Ark->A[i].Comp, " ");
        strcpy(Ark->A[i].Net,  " ");
        strcpy(Ark->A[i].Loc,  " ");
    }
    
   /* Read one data line at a time from arkive and process it.
    * Ignore the shadows (if any).
    **********************************************************/
    i = 0;
    while( in < Ark->ArkivMsg + msglen ) {
        if ( sscanf( in, "%[^\n]", line ) != 1 )  return( -1 );
        in += strlen( line ) + 1;
        nline++;
        
        if(line[0] != '$') {
        /* Process the hypocenter card (1st line of msg).
         *************************************************/
			if( nline == 1 ) {                      /* hypocenter is first line in msg     */
				read_hypy2k(Ark, line );
			}
			else {
        /* Process all the phase cards.
         *******************************/
				if( (int)strlen(line) < (size_t)75 ) break;       /* found the terminator line           */
				read_phsy2k( &(Ark->A[i]), line, line);   /* load phase info into Ark structure  */
				i += 1;
				if(i >= MAXCHANNELS) break;
				
				Ark->A[i].SPick = (Ark->A[i].SPick==0.0)? 
							   Ark->EvntTime + Ark->A[i].Dist/3.36:
							   Ark->A[i].SPick;         /* SPick */
				
				Ark->A[i].SPick = Ark->EvntTime + Ark->A[i].Dist/3.36;         /* SPick */
			}
        }
    }
    Ark->InList = i; 
    return(0);
}


/**************************************************************
 * read_hypy2k() reads the hypocenter line from an archive msg   *
 **************************************************************/
/*------------------------------------------------------------------------
 Sample hypoinverse archive summary line and its shadow.  The summary line
 may be up to 188 characters long.  Its full format is described in 
 documentation (shadow.doc) by Fred Klein.
           10        20        30        40        50        60        70        80        90        100       110       120       130       140       
0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
9701151845332336  148120 3703  605 0 19104  5  37 8971 15222513 13518SLA  57 * 0 132 14424  0125  0 27PMMTWWWD 26X                 1007557D175125Z114  8
$1
  ------------------------------------------------------------------------*/

void read_hypy2k(Arkive *Ark,  char *hyp)
{
    char   datestr[20], subname[] = "read_hypy2k";
    int    i, j, io;
    float  deg, min;

    Ark->EvntYear  =  (short)DECODE( hyp+0,  4, atof );         /* year of origin time     */
    Ark->EvntMonth =  (short)DECODE( hyp+4,  2, atof );         /* month of origin time    */
    Ark->EvntDay   =  (short)DECODE( hyp+6,  2, atof );         /* day of origin time      */
    Ark->EvntHour  =  (short)DECODE( hyp+8,  2, atof );         /* hour of origin time     */
    Ark->EvntMin   =  (short)DECODE( hyp+10, 2, atof );         /* minute of origin time   */
    Ark->EvntSec   = (float)((DECODE( hyp+12, 4, atof ))/100.); /* seconds of origin time  */
    strncpy( datestr, hyp, 12 );
    datestr[12] = '\0';
    strcat ( datestr, "00.00" );                        /* origin time without seconds */
    if(epochsec17(&(Ark->EvntTime), datestr) ) {
       logit("", "%s: Problem converting origin time: %s\n", subname, datestr );
    }
    Ark->EvntTime += (double)Ark->EvntSec;              /* Time of Event (Julian Sec) */

    deg = (float)DECODE( hyp+16, 2, atof );
    min = (float)((DECODE( hyp+19, 4, atof ))/100.);
    Ark->EvntLat = (float)(deg + min/60.);                       /* Latitude of Event */
    if( hyp[18]=='S' || hyp[18]=='s' ) Ark->EvntLat = -Ark->EvntLat;

    deg = (float)DECODE( hyp+23, 3, atof );
    min = (float)((DECODE( hyp+27, 4, atof ))/100.);
    Ark->EvntLon = (float)(deg + min/60.);                       /* Longitude of Event */
    if( hyp[26]=='W' || hyp[26]=='w' || hyp[26]==' ' ) Ark->EvntLon = -Ark->EvntLon;

    Ark->EvntDepth = (float)((DECODE( hyp+31, 5, atof ))/100.);  /* Depth of Event            */
    Ark->NumPhs    =  DECODE( hyp+39, 3, atoi );        /* # phases used in solution */
    Ark->rms       = (float)((DECODE( hyp+48, 4, atof ))/100.);  /* RMS travel time residual  */

    strcpy(Ark->EvntIDS, "No_ID");
    for(i=136,j=0;i<146;i++) if(hyp[i]!=' ') {j = i;break;}
    if(j) strncpy(Ark->EvntIDS, &hyp[j], 146-j);
    Ark->EvntIDS[146-j] = 0;
    io = sscanf(Ark->EvntIDS, "%d", & Ark->EvntID);
    if(io != 1) {
        logit("e", "%s: io:%d EvntID: <%s> interpreted as: %d\n", 
            subname, io, Ark->EvntIDS, Ark->EvntID);
    }
}


/***************************************************************
 * read_phsy2k() reads a phase line & its shadow from archive msg *
 ***************************************************************/
/*------------------------------------------------------------------------
Sample HYPOINVERSE station archive card (P-arrival) and a shadow card.
   (phase card is 100 chars; shadow is up to 103 chars):
0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1
PPC  PD0V97 1151845 3500 -21129    0   0   0     0 -27   0  8611400  0    916616 0 185   0 WD  VHZNC

0123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1
$   4 3.29 1.80 3.45 1.97 0.08 PSN0    9 PHP2 1334  1 427  3 328  5 108  7  62  9  49  0   0

--------------------------------------------------------------------------*/
void read_phsy2k(ArkInfo *A, char *phs, char *shdw)
{
    char   datestr[20], subname[] = "read_phsy2k";
    float  sec;
    int    i;

    A->StaList = '*';
    strncpy( A->Site, phs,    5  );                     /* Site */
    A->Site[5] = '\0';
    for(i=0; i<5; i++){      /* get rid of trailing blanks in A->Site */
        if(A->Site[i]==' ') {
            A->Site[i]='\0';
            break;
        }
    }
    strncpy( A->Comp, phs+9, 3);                        /* Component */
    A->Comp[3] = '\0';
    strncpy( A->Net,  phs+5, 2);                        /* Net */
    A->Net[2]  = '\0';

    strncpy( A->Loc, "--", 2);
    strncpy( A->Loc,  phs+111, 2);                      /* Loc */
    A->Loc[2]  = '\0';

    strncpy( datestr,  phs+17,  12 );                   /* arrival time without seconds */
    datestr[12]='\0';
    strcat ( datestr, "00.00" );
    if( phs[31] == '.' ) sec = (float)DECODE( phs+29, 5, atof );
    else                 sec = (float)((DECODE( phs+29, 5, atof ))/100.0);
    if(epochsec17(&(A->SPick), datestr ) ) {
      printf( "%s: Problem converting arrival time: %s\n", subname, datestr );
    }
    A->PPick = A->SPick;
    A->PPick = (sec==0.0)? 0.0:A->PPick+sec; /* PPick */
    
    if( phs[43] == '.' ) sec = (float)DECODE( phs+41, 5, atof );
    else                 sec = (float)((DECODE( phs+41, 5, atof ))/100.0);
    A->SPick = (sec==0.0)? 0.0:A->SPick+sec; /* SPick */
    
    A->Dist      = (float)((DECODE( phs+74,  4, atof ))/10.0);   /* Epicentral distance (km)             */
}


/********************************************************************
 *    Build_Table is responsible for setting up the ToDo list.      *
 *                                                                  *
 ********************************************************************/

int Build_Table(Global *But, Arkive *Ark, long *InListToDo)
{
    char    whoami[50];
    double  offset[MAXCHANNELS];
    int     i, j, k, kk, error, stime_flag, CompOK, NetOK;
    double  dist, lat, lon, faz, baz, t0, t1;
    double  sx, sy, ss, sxoss, st2, t;
    
    sprintf(whoami, " %s: %s: ", But->mod, "Build_Table");
    stime_flag = error = *InListToDo = 0;
    
    sx = sy = ss = st2 = t = 0.0;
    ss = Ark->InList;
    Ark->Pa = Ark->Pb = 0.0;
    for(i=0;i<Ark->InList;i++) {
        sx += Ark->A[i].Dist;
        sy += Ark->A[i].PPick - Ark->EvntTime;
    }
    sxoss = sx/ss;
    for(i=0;i<Ark->InList;i++) {
        t = Ark->A[i].Dist - sxoss;
        st2 += t*t;
        Ark->Pb += t*(Ark->A[i].PPick - Ark->EvntTime);
    }
    Ark->Pb /= st2;
    Ark->VpApp = 1.0/Ark->Pb;
    Ark->Pa = (sy - sx*Ark->Pb)/ss;
    Ark->Psiga = sqrt((1.0+sx*sx/(ss*st2))/ss);
    Ark->Psigb = sqrt(1.0/st2);
    
      /* Fill out the Ark list with unpicked entries from the station list */
    
    lat = Ark->EvntLat;    lon = Ark->EvntLon;
    for(i=0;i<But->NSCN;i++) {
        if( Ark->InList >= MAXCHANNELS ) {
            fprintf( stderr, "%s %s %s ", whoami, 
                "Site table full; cannot load entire file\n", 
                "Use <maxsite> command to increase table size; exiting!\n");
            break;
        } 
        else {
            distaz(lat, lon, But->Chan[i].Lat, But->Chan[i].Lon, &dist, &faz, &baz);
            if(dist < But->MaxDist) {
                CompOK = NetOK = 0;
                for(kk=0;kk<But->ncomp;kk++) {
                    if(strcmp(But->Chan[i].Comp, But->Comp[kk]) == 0) CompOK = 1; 
                }
                for(kk=0;kk<But->nnet;kk++) {
                    if(strcmp(But->Chan[i].Net,  But->Net[kk]) == 0)  NetOK = 1; 
                }
                
                if(CompOK && NetOK && No_Dups(Ark, But->Chan[i].Site, But->Chan[i].Comp, But->Chan[i].Net, But->Chan[i].Loc)) {    
                    j = Ark->InList;
                    strcpy(Ark->A[j].Site, But->Chan[i].Site);
                    strcpy(Ark->A[j].Comp, But->Chan[i].Comp);
                    strcpy(Ark->A[j].Net,  But->Chan[i].Net);
                    strcpy(Ark->A[j].Loc,  But->Chan[i].Loc);
                    Ark->A[j].Inst_type = But->Chan[i].Inst_type;
                    Ark->A[j].Dist = (float)dist;
                    Ark->A[j].PPick = Ark->EvntTime + dist/6.00 + Ark->Pa;
                    Ark->A[j].PPick = Ark->EvntTime + dist/Ark->Pb - Ark->Pa;
                    Ark->A[j].PPick = Ark->EvntTime + dist/Ark->VpApp + Ark->Pa;
                    Ark->A[j].SPick = Ark->EvntTime + dist/3.36;
                    Ark->InList += 1;
                } 
            }
        }
    }
        
    
    /* Keep only those which have been blessed *
     ***********************************************/
    for(i=0;i<Ark->InList;i++) {
        Ark->A[i].reject = 3;    /*    we later clear this if trace found and not clipped    */
        j = Chan_Info(But, Ark->A[i].Site, Ark->A[i].Comp, Ark->A[i].Net, Ark->A[i].Loc);
        if(j) {
            strcpy(Ark->A[i].Loc,  But->Chan[j-1].Loc);
            Ark->A[i].Inst_type = But->Chan[j-1].Inst_type;
            Ark->A[i].GainFudge = But->Chan[j-1].GainFudge;
            Ark->A[i].Sens_type = But->Chan[j-1].Sens_type;
            Ark->A[i].Sens_unit = But->Chan[j-1].Sens_unit;
            Ark->A[i].sensitivity  = But->Chan[j-1].sensitivity;
        }
        else {
            Ark->A[i].Dist = 30000.0;
        }
    }
    
    /* Set up the arrays for selecting and sorting *
     ***********************************************/
    for(i=0;i<Ark->InList;i++) {
        offset[i] = (Ark->A[i].Dist>0.0 && Ark->A[i].Dist< But->MaxDist)? Ark->A[i].Dist:30000.0; 
        Ark->index[i] = i;
        
        Ark->A[i].SPick = Ark->EvntTime + Ark->A[i].Dist/3.46;
        Ark->A[i].SPick = Ark->A[i].PPick + Ark->A[i].Dist*(0.900/Ark->VpApp);
        
    }
    
    /* Go get the needed info *
    ***************************/
    if(But->Debug) logit("e", "%s Get the %d requested traces.\n", whoami, Ark->InList);
    j = 0;
    for(i=0;i<Ark->InList;i++)  {
        if( j >= MAXCHANNELS ) {    /* Make sure internal site table has room left */
           logit("e", "%s Site table full <%d>; cannot load entire arkive file\n", whoami, j);
           logit("e", "        Use <MaxSite> command to increase table size; exiting!\n");
           return 1;
        }

        if(offset[i] < But->MaxDist) {
            CompOK = NetOK = 0;
            for(kk=0;kk<But->ncomp;kk++) {
                if(strcmp(Ark->A[i].Comp, But->Comp[kk]) == 0) CompOK = 1; 
            }
            for(kk=0;kk<But->nnet;kk++) {
                if(strcmp(Ark->A[i].Net,  But->Net[kk]) == 0)  NetOK = 1; 
            }
                            
            if(CompOK && NetOK && In_Menu(But, Ark->A[i].Site, Ark->A[i].Comp, Ark->A[i].Net, Ark->A[i].Loc, &t0, &t1)) {    
                Ark->A[i].Stime = Ark->EvntTime - But->Pre_Event;   /* Earliest time needed. */
                if(Ark->A[i].Stime > t0 && Ark->A[i].Stime < t1) {
                    Ark->A[i].Duration = (float)(But->Secs_Screen + 5.0 + Ark->A[i].Dist/6);   /* Time Reduced/6             */
                    if(Ark->A[i].Stime + Ark->A[i].Duration > t1) 
                        Ark->A[i].Duration = (float)(t1 - Ark->A[i].Stime);
                    
                    j += 1;
                    *InListToDo = j;
                }
            } else 
                offset[i] = 30000.0;
        } else 
            offset[i] = 30000.0;
    }
    if(*InListToDo==0) error = -1;
    Sort_Ark (Ark->InList, offset, &Ark->index[0]);
/*
 Site: Comp: Net:  Loc:    Dist:      Picked:
 CADB  HNN   NC    --     69.967354    
 CADB   HNE     NC    --     69.967354    
*/    
    if(But->Debug) {
        logit("e", "%s The following %d traces are available:\n Site: Comp: Net:  Loc:    Dist:      Picked:\n", 
              whoami, *InListToDo);
        for(i=0;i<(*InListToDo);i++)  {
            k = Ark->index[i];
            logit("e", " %-5s %-4s  %-3s   %-3s    %f  %c \n", 
                Ark->A[k].Site, Ark->A[k].Comp, Ark->A[k].Net, Ark->A[k].Loc, 
                Ark->A[k].Dist, Ark->A[k].StaList);
        }
    }
    return error;
}

/*************************************************************************
 *   int Chan_Info (sta,comp,net)                                        *
 *      Determines if the scn is in the Arkive list                      *
 *************************************************************************/

int Chan_Info (Global *But, char *sta, char *comp, char *net, char *loc)
{
    int    i;
      
    for(i=0;i<But->NSCN;i++) {
       if(strcmp(But->Chan[i].Site, sta )==0 && 
          strcmp(But->Chan[i].Comp, comp)==0 && 
          strcmp(But->Chan[i].Net,  net )==0 &&
          strcmp(But->Chan[i].Loc,  loc )==0) {
          sprintf(But->Chan[i].SCNtxt, "%s %s %s %s", But->Chan[i].Site, But->Chan[i].Comp, But->Chan[i].Net, But->Chan[i].Loc);
          return i+1;
       }
    }
    return 0;
}


/*************************************************************************
 *   int No_Dups (sta,comp,net,loc)                                      *
 *      Determines if the scn is in the Arkive list                      *
 *************************************************************************/

int No_Dups (Arkive *Arkp, char *sta, char *comp, char *net, char *loc)
{
    int    i;
      
    for(i=0;i<Arkp->InList;i++) {
       if(strcmp(Arkp->A[i].Site, sta )==0 && 
          strcmp(Arkp->A[i].Comp, comp)==0 && 
          strcmp(Arkp->A[i].Net,  net )==0 &&
          strcmp(Arkp->A[i].Loc,  loc )==0) {
          return 0;
       }
    }
    return 1;
}


/*************************************************************************
 *   int Build_Menu ()                                                   *
 *      Builds the waveserver's menu                                     *
 *************************************************************************/

int Build_Menu (Global *But)
{
    char    whoami[50], server[100];
    int     j, retry, ret, rc;
    WS_PSCNL scnp;
    WS_MENU menu; 
      
    /* Build the current wave server menus *
     ***************************************/

    sprintf(whoami, " %s: %s: ", But->mod, "Build_Menu");
    But->got_a_menu = 0;
    
    for(j=0;j<But->nServer;j++) But->index[j]  = j;
    
    for (j=0;j< But->nServer; j++) {
        retry = 0;
        But->inmenu[j] = 0;
        sprintf(server, " %s:%s <%s>", But->wsIp[j], But->wsPort[j], But->wsComment[j]);
        if ( But->wsIp[j][0] == 0 ) continue;
    Append:
        
        ret = wsAppendMenu(But->wsIp[j], But->wsPort[j], &But->menu_queue[j], But->wsTimeout);
        
        if (ret == WS_ERR_NO_CONNECTION) { 
            if(But->Debug) 
                logit("e","%s Could not get a connection to %s to get menu.\n", whoami, server);
        }
        else if (ret == WS_ERR_SOCKET) 
            logit("e","%s Could not create a socket for %s\n", whoami, server);
        else if (ret == WS_ERR_BROKEN_CONNECTION) {
            logit("e","%s Connection to %s broke during menu\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_TIMEOUT) {
            logit("e","%s Connection to %s timed out during menu.\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_MEMORY) 
            logit("e","%s Waveserver %s out of memory.\n", whoami, server);
        else if (ret == WS_ERR_INPUT) {
            logit("e","%s Connection to %s input error\n", whoami, server);
            if(retry++ < But->RetryCount) goto Append;
        }
        else if (ret == WS_ERR_PARSE) 
            logit("e","%s Parser failed for %s\n", whoami, server);
        else if (ret == WS_ERR_BUFFER_OVERFLOW) 
            logit("e","%s Buffer overflowed for %s\n", whoami, server);
        else if (ret == WS_ERR_EMPTY_MENU) 
            logit("e","%s Unexpected empty menu from %s\n", whoami, server);
        else if (ret == WS_ERR_NONE) {
            But->inmenu[j] = But->got_a_menu = 1;
        }
        else logit("e","%s Connection to %s returns error: %d\n", 
                   whoami, server, ret);
    }
    /* Let's make sure that servers in our server list have really connected.
       **********************************************************************/  
    for(j=0;j<But->nServer;j++) {
        if ( But->inmenu[j] ) {
            rc = wsGetServerPSCNL( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
            if ( rc == WS_ERR_EMPTY_MENU ) {
                logit("e","%s Empty menu.\n", whoami);
                But->inmenu[j] = 0;
            }
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                logit("e","%s %s not in menu.\n", whoami, But->wsIp[j]);
                But->inmenu[j] = But->wsIp[j][0] = 0;
            }
        /* Then, detach 'em and let RequestWave attach only the ones it needs.
           **********************************************************************/  
            menu = But->menu_queue[j].head;
            if ( menu->sock > 0 ) wsDetachServer( menu );
        }
    }
    return 0;
}


/*************************************************************************
 *   short In_Menu (sta,comp,net)                                        *
 *      Determines if the scn is in the waveserver's menu                *
 *************************************************************************/

int In_Menu (Global *But, char *sta, char *comp, char *net, char *loc, 
                double *tankStarttime, double *tankEndtime)
{
    char    whoami[50];
    int     i, rc;
    WS_PSCNL scnp;
      
    sprintf(whoami, " %s: %s: ", But->mod, "In_Menu");
    for(i=0;i<But->nServer;i++) {
        if(But->inmenu[i]) {
            rc = wsGetServerPSCNL( But->wsIp[i], But->wsPort[i], &scnp, &But->menu_queue[i]);    
            if ( rc == WS_ERR_EMPTY_MENU )         continue;
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                if(But->Debug) logit("e","%s  %s:%s not in menu.\n", whoami, But->wsIp[i], But->wsPort[i]);
                But->inmenu[i] = 0;
                continue;
            }

			But->wsLoc[i] = (strlen(scnp->loc))? 1:0;
            while ( 1 ) {
               if(strcmp(scnp->sta,  sta )==0 && 
                  strcmp(scnp->chan, comp)==0 && 
                  strcmp(scnp->net,  net )==0) {
					if(strcmp(scnp->loc,  loc )==0 || But->wsLoc[i]==0)  {  
						*tankStarttime = scnp->tankStarttime;
						*tankEndtime   = scnp->tankEndtime;
						return 1;
					}
               }
                if(comp[0] == 'T' && scnp->chan[0] == 'T') {
                    fprintf(stderr,"%s A Time channel.  <%s> <%s> <%s> :: <%s> <%s> <%s>\n", 
                        whoami, sta, comp, net, scnp->sta, scnp->chan, scnp->net );
                }
               if ( scnp->next == NULL )
                  break;
               else
                  scnp = scnp->next;
            }
        }
    }
    return 0;
}


/*************************************************************************
 *   In_Menu_list                                                        *
 *      Determines if the scn is in the waveservers' menu.               *
 *      If there, the tank starttime and endtime are returned.           *
 *      Also, the Server IP# and port are returned.                      *
 *************************************************************************/

int In_Menu_list (Global *But, char *Site, char *Comp, char *Net, char *Loc)
{
    char    whoami[50], server[100], SCNtxt[25];
    long    j, rc;
    WS_PSCNL scnp;
    
    sprintf(whoami, " %s: %s: ", But->mod, "In_Menu_list");
    sprintf(SCNtxt, " %s %s %s", Site, Comp, Net);
    But->nentries = 0;
    for(j=0;j<But->nServer;j++) {
        if(But->inmenu[j]) {
            sprintf(server, " %s:%s <%s>", But->wsIp[j], But->wsPort[j], But->wsComment[j]);
            rc = wsGetServerPSCNL( But->wsIp[j], But->wsPort[j], &scnp, &But->menu_queue[j]);    
            if ( rc == WS_ERR_EMPTY_MENU ) {
                if(But->Debug) logit("e","%s Empty menu for %s \n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }
            if ( rc == WS_ERR_SERVER_NOT_IN_MENU ) {
                if(But->Debug) logit("e","%s  %s not in menu.\n", whoami, server);
                But->inmenu[j] = 0;    
                continue;
            }
            /*
            if(But->Debug) logit("e","%s Got menu for %s \n", whoami, server);
*/
			But->wsLoc[j] = (strlen(scnp->loc))? 1:0;
            while ( 1 ) {
               if(strcmp(scnp->sta,  Site)==0 && 
                  strcmp(scnp->chan, Comp)==0 && 
                  strcmp(scnp->net,  Net )==0) {
					if(strcmp(scnp->loc,  Loc )==0 || But->wsLoc[j]==0)  {  
						But->TStime[j] = scnp->tankStarttime;
						But->TEtime[j] = scnp->tankEndtime;
						But->index[But->nentries]  = j;
				/* 	if(But->Debug) 
							logit("e","%s  %s Found %s %s %s %s in %s.\n", 
									whoami, server, Site, Comp, Net, Loc, But->wsComment[j]);  */
						But->nentries += 1;
					}
				}
               if ( scnp->next == NULL )
                  break;
               else
                  scnp = scnp->next;
            }
        }
    }
    if(But->nentries>0) return 1;
    return 0;
}


/*************************************************************************
 *   void Sort_Ark (n,dist,indx)                                         *
 *   --THIS is A VERSION OF THE HEAPSORT SUBROUTINE FROM THE NUMERICAL   *
 *     RECIPES BOOK. dist is REARRANGED IN ASCENDING ORDER &             *
 *     indx is PUT IN THE SAME ORDER AS dist.                            *
 *   long        n       !THE NUMBER OF VALUES OF dist TO BE SORTED      *
 *   double      dist(n) !SORT THIS ARRAY IN ASCENDING ORDER             *
 *   long        indx(n) !PASSIVELY REARRANGE THIS ARRAY TOO             *
 *************************************************************************/

void Sort_Ark (long n, double *dist, long *indx)
{
    long    indxh, ii, ir, i, j;
    double  disth;
    
    ii = n/2+1;    ir = n;
    
    while(1) {    
        if (ii > 1) {
            ii -= 1;
            disth      = dist[ii-1];   indxh      = indx[ii-1];
        }
        else {
            disth      = dist[ir-1];   indxh      = indx[ir-1];
            dist[ir-1] = dist[0];      indx[ir-1] = indx[0];
            ir -= 1;
            if(ir == 1) {
                dist[0] = disth;       indx[0]    = indxh;
                return;
            }
        }
        i = ii;   j = ii+ii;
        
        while (j <= ir) {
            if (j < ir && dist[j-1] < dist[j]) j += 1;
            
            if (disth < dist[j-1]) {
                dist[i-1] = dist[j-1]; indx[i-1] = indx[j-1];
                i = j;   j = j+j;
            }
            else    j = ir+1;
        }
        dist[i-1] = disth;   indx[i-1] = indxh;
    }
}


/*********************************************************************
 *   Build_SM_file()                                                 *
 *********************************************************************/

void Build_SM_file(Global *But, Arkive *Ark)
{
    char    whoami[50];
    short   yeargmt;
    double  lat, lon, sex, sec1970 = 11676096000.00;  
    int     j, k, minute;
    struct Greg  g;
    
    sprintf(whoami, " %s: %s: ", But->mod, "Build_SM_file");
    
/* Do the ShakeMap file
 *************************/    
    sex = Ark->EvntTime + sec1970;
    minute = (long) (sex / 60.0);
    grg(minute, &g);
    yeargmt = g.year;
    lat = Ark->EvntLat;
    lon = Ark->EvntLon;
    if(But->Debug)  logit("e", "%s Writing ShakeMap File\n", whoami);   
/* Put the data in the log file in case we need it later
 *******************************************************/    
    logit("e", "------------------------------------------------------------\n");
    logit("e", "                NCSN Ground Motion Amplitude Data           \n");
    logit("e", "                     for Event Number : %s       \n", Ark->EvntIDS);
    logit("e", "------------------------------------------------------------\n");
    logit("e", "Summary of Results :                                      \n\n");
    logit("e", "Origin Time      :  %.2d/%.2d/%.2d     %.2d:%.2d:%05.2f UTC \n",
                  yeargmt, Ark->EvntMonth, Ark->EvntDay, Ark->EvntHour,  Ark->EvntMin, Ark->EvntSec);
    logit("e", "Location         : Lat : %6.2f  Long : %7.2f  Depth : %6.2f  \n", lat, lon, Ark->EvntDepth);
    logit("e", "Phases Used      :   %d   Solution  RMS : %5.2f    \n", Ark->NumPhs, Ark->rms);
    logit("e", "------------------------------------------------------------\n");
    logit("e", "Digital Seismic Network Results (in cgs) :    \n\n");
    logit("e", "Sta   Net Comp Loc :   Accel   Vel     Displ   SP_0.3   SP_1.0   SP_3.0    \n");
    
    for(k=0;k<Ark->InList;k++) {
        j = Ark->index[k];
        if(Ark->A[j].PgaAvail) {
            logit("e", "%-5s %-3s %-4s %-3s : %7.2f %7.3f  %7.4f %7.3f  %7.3f  %7.3f\n", 
            Ark->A[j].Site, Ark->A[j].Net, Ark->A[j].Comp, Ark->A[j].Loc,  Ark->A[j].Pga, Ark->A[j].Pgv,    Ark->A[j].Pgd, 
            Ark->A[j].RSA[0],  Ark->A[j].RSA[1],   Ark->A[j].RSA[2]);
        } 
    }
}


/**********************************************************************
 * date22 : Calculate 22 char date in the form Jan23,1988 12:34 12.21 *
 *          from the julian seconds.  Remember to leave space for the *
 *          string termination (NUL).                                 *
 **********************************************************************/
void date22( double secs, char *c22)
{
    char *cmo[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    struct Greg  g;
    long    minute;
    double  sex;
    double  sec1970 = 11676096000.00;  /* # seconds between Carl Johnson's     */
                                       /* time 0 and 1970-01-01 00:00:00.0 GMT */

    secs += sec1970;
    minute = (long) (secs / 60.0);
    sex = secs - 60.0 * minute;
    grg(minute, &g);
    sprintf(c22, "%3s%2d,%4d %.2d:%.2d:%05.2f",
            cmo[g.month-1], g.day, g.year, g.hour, g.minute, sex);
}


/****************************************************************************
 *  Get_Sta_Info(Global *But);                                              *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void Get_Sta_Info(Global *But)
{
    char    whoami[50], *com, *str;
    int     i, j, k, nfiles, success;

    sprintf(whoami, "%s: %s: ", But->mod, "Get_Sta_Info");
    
    But->NSCN = 0;
    for(k=0;k<But->nStaDB;k++) {
            /* Open the main station file
             ****************************/
        nfiles = k_open( But->stationList[k] );
        if(nfiles == 0) {
            fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, But->stationList[k] );
            exit( -1 );
        }

            /* Process all command files
             ***************************/
        while(nfiles > 0) {  /* While there are command files open */
            while(k_rd())  {      /* Read next line from active file  */
                com = k_str();         /* Get the first token from line */

                    /* Ignore blank lines & comments
                     *******************************/
                if( !com )           continue;
                if( com[0] == '#' )  continue;

                    /* Open a nested configuration file
                     **********************************/
                if( com[0] == '@' ) {
                    success = nfiles+1;
                    nfiles  = k_open(&com[1]);
                    if ( nfiles != success ) {
                        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                        exit( -1 );
                    }
                    continue;
                }

                /* Process anything else as a channel descriptor
                 ***********************************************/

                if( But->NSCN >= MAXCHANNELS ) {
                    fprintf(stderr, "%s Too many channel entries in <%s>", 
                             whoami, But->stationList[k] );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAXCHANNELS );
                    exit( -1 );
                }
                j = But->NSCN;
                
                    /* S C N */
                strncpy( But->Chan[j].Site, com,  6);
                str = k_str();
                if(str) strncpy( But->Chan[j].Net,  str,  2);
                str = k_str();
                if(str) strncpy( But->Chan[j].Comp, str, 3);
                str = k_str();
                if(str) strncpy( But->Chan[j].Loc, str, 3);
                for(i=0;i<6;i++) if(But->Chan[j].Site[i]==' ') But->Chan[j].Site[i] = 0;
                for(i=0;i<2;i++) if(But->Chan[j].Net[i]==' ')  But->Chan[j].Net[i]  = 0;
                for(i=0;i<3;i++) if(But->Chan[j].Comp[i]==' ') But->Chan[j].Comp[i] = 0;
                for(i=0;i<2;i++) if(But->Chan[j].Loc[i]==' ')  But->Chan[j].Loc[i]  = 0;
                But->Chan[j].Comp[3] = But->Chan[j].Net[2] = But->Chan[j].Site[5] = But->Chan[j].Loc[2] = 0;
                sprintf(But->Chan[j].SCNnam, "%s_%s_%s_%s", 
                        But->Chan[j].Site, But->Chan[j].Comp, But->Chan[j].Net, But->Chan[j].Loc);


                    /* Lat Lon Elev */
                But->Chan[j].Lat  = k_val();
                But->Chan[j].Lon  = k_val();
                But->Chan[j].Elev = k_val();
                                
                But->Chan[j].Inst_type  = k_int();
                But->Chan[j].Inst_gain  = k_val();
                But->Chan[j].GainFudge  = k_val();
                But->Chan[j].Sens_type  = k_int();
                But->Chan[j].Sens_unit  = k_int();
                But->Chan[j].Sens_gain  = k_val();
                if(But->Chan[j].Sens_unit == 3) But->Chan[j].Sens_gain /= 981.0;
                But->Chan[j].SiteCorr = k_val();
                But->Chan[j].ShkQual  = k_int();
                        
               
                But->Chan[j].sensitivity = (1000000.0*But->Chan[j].Sens_gain/But->Chan[j].Inst_gain)*But->Chan[j].GainFudge;    /*    sensitivity counts/units        */
                
                if (k_err()) {
                    logit("e", "%s Error decoding line in station file\n%s\n  exiting!\n", whoami, k_get() );
                    exit( -1 );
                }
         /*>Comment<*/
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].SiteName, str );
                    
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].Descript, str );
                    
                But->NSCN++;
            }
            nfiles = k_close();
        }
    }
}


/****************************************************************************
 *  Get_Sta_Info(Global *But);                                              *
 *  Retrieve all the information available about the network stations       *
 *  and put it into an internal structure for reference.                    *
 *  This should eventually be a call to the database; for now we must       *
 *  supply an ascii file with all the info.                                 *
 *     process station file using kom.c functions                           *
 *                       exits if any errors are encountered                *
 ****************************************************************************/
void old_Get_Sta_Info(Global *But)
{
    char    whoami[50], *com, *str, ns, ew;
    int     i, j, k, nfiles, success, type, sensor, units;
    double  dlat, mlat, dlon, mlon, gain, sens, ssens;

    sprintf(whoami, "%s: %s: ", But->mod, "Get_Sta_Info");
    
    ns = 'N';
    ew = 'W';
    But->NSCN = 0;
    for(k=0;k<But->nStaDB;k++) {
            /* Open the main station file
             ****************************/
        nfiles = k_open( But->stationList[k] );
        if(nfiles == 0) {
            fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, But->stationList[k] );
            exit( -1 );
        }

            /* Process all command files
             ***************************/
        while(nfiles > 0) {  /* While there are command files open */
            while(k_rd())  {      /* Read next line from active file  */
                com = k_str();         /* Get the first token from line */

                    /* Ignore blank lines & comments
                     *******************************/
                if( !com )           continue;
                if( com[0] == '#' )  continue;

                    /* Open a nested configuration file
                     **********************************/
                if( com[0] == '@' ) {
                    success = nfiles+1;
                    nfiles  = k_open(&com[1]);
                    if ( nfiles != success ) {
                        fprintf( stderr, "%s Error opening command file <%s>; exiting!\n", whoami, &com[1] );
                        exit( -1 );
                    }
                    continue;
                }

                /* Process anything else as a channel descriptor
                 ***********************************************/

                if( But->NSCN >= MAXCHANNELS ) {
                    fprintf(stderr, "%s Too many channel entries in <%s>", 
                             whoami, But->stationList[k] );
                    fprintf(stderr, "; max=%d; exiting!\n", (int) MAXCHANNELS );
                    exit( -1 );
                }
                j = But->NSCN;
                
                    /* S C N */
                strncpy( But->Chan[j].Site, com,  6);
                str = k_str();
                if(str) strncpy( But->Chan[j].Net,  str,  2);
                str = k_str();
                if(str) strncpy( But->Chan[j].Comp, str, 3);
                for(i=0;i<6;i++) if(But->Chan[j].Site[i]==' ') But->Chan[j].Site[i] = 0;
                for(i=0;i<2;i++) if(But->Chan[j].Net[i]==' ')  But->Chan[j].Net[i]  = 0;
                for(i=0;i<3;i++) if(But->Chan[j].Comp[i]==' ') But->Chan[j].Comp[i] = 0;
                But->Chan[j].Comp[3] = But->Chan[j].Net[2] = But->Chan[j].Site[5] = 0;


                    /* Lat Lon Elev */
                dlat = k_int();
                mlat = k_val();
                
                dlon = k_int();
                mlon = k_val();
                
                But->Chan[j].Elev = k_val();
                
                    /* convert to decimal degrees */
                if ( dlat < 0 ) dlat = -dlat;
                if ( dlon < 0 ) dlon = -dlon;
                But->Chan[j].Lat = dlat + (mlat/60.0);
                But->Chan[j].Lon = dlon + (mlon/60.0);
                    /* make south-latitudes and west-longitudes negative */
                if ( ns=='s' || ns=='S' )               But->Chan[j].Lat = -But->Chan[j].Lat;
                if ( ew=='w' || ew=='W' || ew==' ' )    But->Chan[j].Lon = -But->Chan[j].Lon;

         /*       str = k_str();      Blow past the subnet */
                
                type   = k_int();
                sens   = k_val();
                gain   = k_val();
                sensor = k_int();
                units  = k_int();
                ssens  = k_val();
                But->Chan[j].SiteCorr = k_val();
                        
                if(units == 3) ssens /= 981.0;
                But->Chan[j].Inst_type = type;
                But->Chan[j].Inst_gain = sens;
                But->Chan[j].GainFudge = gain;
                But->Chan[j].Sens_type = sensor;
                But->Chan[j].Sens_gain = ssens;
                But->Chan[j].Sens_unit = units;
               
                But->Chan[j].sensitivity = (1000000.0*ssens/sens)*gain;    /*    sensitivity counts/units        */
                
                if (k_err()) {
                    logit("e", "%s Error decoding line in station file\n%s\n  exiting!\n", whoami, k_get() );
                    exit( -1 );
                }
         /*>Comment<*/
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].SiteName, str );
                    
                str = k_str();
                if( (long)(str) != 0 && str[0]!='#')  strcpy( But->Chan[j].Descript, str );
                    
                But->NSCN++;
            }
            nfiles = k_close();
        }
    }
}


/************************************************************************

    Subroutine distaz (lat1,lon1,lat2,lon2,RNGKM,FAZ,BAZ)
    
c--  COMPUTES RANGE AND AZIMUTHS (FORWARD AND BACK) BETWEEN TWO POINTS.
c--  OPERATOR CHOOSES BETWEEN 3 FIRST ORDER ELLIPSOIDAL MODELS OF THE
c--  EARTH AS DEFINED BY THE MAJOR RADIUS AND FLATTENING.
c--  THE PROGRAM UTILIZES THE SODANO AND ROBINSON (1963) DIRECT SOLUTION
c--  OF GEODESICS (ARMY MAP SERVICE, TECH REP #7, SECTION IV).
c--  (TERMS ARE GIVEN TO ORDER ECCENTRICITY TO THE FOURTH POWER.)
c--  ACCURACY FOR VERY LONG GEODESICS:
c--          DISTANCE < +/-  1 METER
c--          AZIMUTH  < +/- .01 SEC
c
    Ellipsoid            Major Radius    Minor Radius    Flattening
1  Fischer 1960            6378166.0        6356784.28      298.30
2  Clarke1866              6378206.4        6356583.8       294.98
3  S. Am 1967              6378160.0        6356774.72      298.25
4  Hayford Intl 1910       6378388.0        6356911.94613   297.00
5  WGS 1972                6378135.0        6356750.519915  298.26
6  Bessel 1841             6377397.155      6356078.96284   299.1528
7  Everest 1830            6377276.3452     6356075.4133    300.8017
8  Airy                    6377563.396      6356256.81      299.325
9  Hough 1960              6378270.0        6356794.343479  297.00
10 Fischer 1968            6378150.0        6356768.337303  298.30
11 Clarke1880              6378249.145      6356514.86955   293.465
12 Fischer 1960            6378155.0        6356773.32      298.30
13 Intl Astr Union         6378160.0        6378160.0       298.25
14 Krasovsky               6378245.0        6356863.0188    298.30
15 WGS 1984                6378137.0        6356752.31      298.257223563
16 Aust Natl               6378160.0        6356774.719     298.25
17 GRS80                   6378137.0        63567552.31414  298.2572
18 Helmert                 6378200.0        6356818.17      298.30
19 Mod. Airy               6377341.89       6356036.143     299.325
20 Mod. Everest            6377304.063      6356103.039     300.8017
21 Mercury 1960            6378166.0        6356784.283666  298.30
22 S.E. Asia               6378155.0        6356773.3205    298.2305
23 Sphere                  6370997.0        6370997.0         0.0
24 Walbeck                 6376896.0        6355834.8467    302.78
25 WGS 1966                6378145.0        6356759.769356  298.25

************************************************************************/

int distaz (double lat1, double lon1, double lat2, double lon2, 
                double *rngkm, double *faz, double *baz)
{
    double    pi, rd, f, fsq;
    double    a3, a5, a6, a10, a16, a17, a18, a19, a20;
    double    a21, a22, a22t1, a22t2, a22t3, a23, a24, a25, a27, a28, a30;
    double    a31, a32, a33, a34, a35, a36, a38, a39, a40;
    double    a41, a43;
    double    a50, a51, a52, a54, a55, a57, a58, a62;
    double    dlon, alph[2];
    double    p11, p12, p13, p14, p15, p16, p17, p18;
    double    p21, p22, p23, p24, p25, p26, p27, p28, pd1, pd2;
    double    ang45;
    short     k;
    double    RAD, MRAD, FINV;
    
    RAD  = 6378206.4;
    MRAD = 6356583.8;
    FINV = 294.98;
    ang45 = atan(1.0);
    
    pi = 4.0*atan(1.0);
    rd = pi/180.0;

    a5 = RAD;
    a3 = FINV;
    
    if(a3==0.0) {
        a6 = MRAD;                    /*    minor radius of ellipsoid    */
        f = fsq = a10 = 0.0;
    }
    else {
        a6 = (a3-1.0)*a5/a3;          /*    minor radius of ellipsoid    */
        f = 1.0/a3;                   /*    flattening    */
        fsq = 1.0/(a3*a3);
        a10 = (a5*a5-a6*a6)/(a5*a5);  /*    eccentricity squared    */
    }
    
    /*    Following definitions are from Sodano algorithm for long geodesics    */
    a50 = 1.0 + f + fsq;
    a51 = f + fsq;
    a52 = fsq/ 2.0;
    a54 = fsq/16.0;
    a55 = fsq/ 8.0;
    a57 = fsq* 1.25;
    a58 = fsq/ 4.0;
    
/*     THIS IS THE CALCULATION LOOP. */
    
    p11 = lat1*rd;
    p12 = lon1*rd;
    p21 = lat2*rd;
    p22 = lon2*rd;
    *rngkm = *faz = *baz = 0.0;
    if( (lat1==lat2) && (lon1==lon2))    return(0);
    
    /*    Make sure points are not exactly on the equator    */
    if(p11 == 0.0) p11 = 0.000001;
    if(p21 == 0.0) p21 = 0.000001;
    
    /*    Make sure points are not exactly on the same meridian    */
    if(p12 == p22)             p22 += 0.0000000001;
    if(fabs(p12-p22) == pi)    p22 += 0.0000000001;
    
    /*    Correct latitudes for flattening    */
    p13 = sin(p11);
    p14 = cos(p11);
    p15 = p13/p14;
    p18 = p15*(1.0-f);
    a62 = atan(p18);
    p16 = sin(a62);
    p17 = cos(a62);
    
    p23 = sin(p21);
    p24 = cos(p21);
    p25 = p23/p24;
    p28 = p25*(1.0-f);
    a62 = atan(p28);
    p26 = sin(a62);
    p27 = cos(a62);
    
    dlon = p22-p12;
    a16 = fabs(dlon);
    
    /*    Difference in longitude to minimum (<pi)    */
    if(a16 >= pi) a16 = 2.0*pi - a16;

    /*    Compute range (a35)    */
    if(a16==0.0)     {a17 = 0.0;         a18 = 1.0;}
    else             {a17 = sin(a16);    a18 = cos(a16);}
    a19 = p16*p26;
    a20 = p17*p27;
    a21 = a19 + a20*a18;
    
    a40 = a41 = a43 = 0.0;
    
    for(k=0; k<2; k++) {
        a22t1 = (a17*p27)*(a17*p27);
        a22t2 = p26*p17 - p16*p27*a18;
        a22   = sqrt(a22t1 + (a22t2*a22t2));
        if(a22 == 0.0) return(0);
        a22t3 = a22*a21;
        a23 = (a20*a17)/a22;
        a24 = 1.0 - a23*a23;
        a25 = asin(a22);
        if(a21 < 0.0) a25 = pi-a25;
        a27 = (a25*a25)/a22;
        a28 = a21*a27;
        if(k==0) {
            a30 = (a50*a25) + a19*(a51*a22-a52*a27);
            a31 = 0.5*a24*(fsq*a28 - a51*(a25 + a22t3));
            a32 = a19*a19*a52*a22t3;
            a33 = a24*a24*(a54*(a25 + a22t3) - a52*a28 - a55*a22t3*(a21*a21));
            a34 = a19*a24*a52*(a27 + a22t3*a21);
            a35 = (a30 + a31 - a32 + a33 + a34)*a6;
            *rngkm = a35/1000.0;
        }

    /*    Compute azimuths    */
        a36 = (a51*a25) - a19*(a52*a22 + fsq*a27) + a24*(a58*a22t3 - a57*a25 + fsq*a28);
        a38 = a36*a23 + a16;
        a39 = sin(a38);
        a40 = cos(a38);

        if(a39*p27 == 0.0) a43 = 0.0;
        else {
          a41 = (p26*p17 - a40*p16*p27)/(a39*p27);
          if(a41 == 0.0)    a43 = pi/2.0;
          else                a43 = atan(1.0/a41);
        }

        alph[k] = a43;
        if((dlon <= -pi) || ((dlon >= 0.0) && (dlon < pi))) {
          if(a41 >= 0.0)       alph[k] = alph[k]-pi;
        }
        else {
          if(a41 >= 0.0)       alph[k] = pi - alph[k];
          else                 alph[k] = 2.0*pi - alph[k];
        }
        if(alph[k] >= pi)      alph[k] = alph[k] - pi;
        if(alph[k] <  pi)      alph[k] = alph[k] + pi;
        if(alph[k] <  0.0)     alph[k] = alph[k] + 2.0*pi;
        if(alph[k] >= 2.0*pi)  alph[k] = alph[k] - 2.0*pi;
        alph[k] = alph[k]/rd;
        pd1 = p16;
        pd2 = p17;
        p16 = p26;
        p17 = p27;
        p26 = pd1;
        p27 = pd2;
        dlon = -dlon;
    }

    *faz = alph[0];
    *baz = alph[1];
    while (*faz >= 360.0) *faz = *faz - 360.0;
    while (*baz >= 360.0) *baz = *baz - 360.0;
    
    return(0);
}



