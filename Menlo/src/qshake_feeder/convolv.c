/***************************************************************************
 * Subroutines for doing various trace calculations.                       *
 *                                                                         *
 ***************************************************************************/

#include <platform.h>
#include <errno.h>
#include "math.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>

#include <ws_clientII.h>
/*
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
*/
#include <rw_strongmotion.h>

#include "qshaker.h"

#define GRAVITY 981.0      /* Gravity in cm/sec/sec */

/* Functions in this source file 
 *******************************/
int peak_ground(double *Data, int npts, Arkive *Ark, int itype, double dt);
void locut(double *s, int nd, double fcut, double delt, int nroll, int icaus);
void rdrvaa(double *, int, double, double, double, double *, double *, double *);

void demean(double *A, int N);
void amaxper(int npts, double dt, double *fc, double *amaxmm, double *aminmm, double *pmax, int *imin, int *imax);

double    d1[MAXTRACELTH];
double    d2[MAXTRACELTH];
double    d3[MAXTRACELTH];
extern int          Debug;

/******************************************************************************
 *   subroutine for estimation of ground motion                               *
 *                                                                            *
 *   input:                                                                   *
 *           Data   - data array                                              *
 *           npts   - number of points in timeseries                          *
 *           itype  - units for timeseries. 1=disp 2=vel 3=acc                *
 *           dt     - sample spacing in sec                                   *
 *   return:                                                                  *
 *           0      - All OK                                                  *
 *           1      - error                                                   *
 *                                                                            *
 ******************************************************************************/
int peak_ground(double *Data, int npts, Arkive *Ark, int itype, double dt)
{
    char    whoami[50];
    int     imax_acc, imax_vel, imax_dsp, imax_raw;
    int     imin_acc, imin_vel, imin_dsp, imin_raw;
    int     k, ii, kk, kpts, id[4], npd[4][2], icaus;
    double  totint, a, tpi, omega, damp, rd, rv, aa, period;
    double  amax_acc, amin_acc, pmax_acc;
    double  amax_vel, amin_vel, pmax_vel;
    double  amax_dsp, amin_dsp, pmax_dsp;
    double  amax_raw, amin_raw, pmax_raw, gd[4], sd[4];
    
    sprintf(whoami, " %s: %s: ", "sm_file2ew", "peak_ground");

    k = Ark->Current;
    gd[0] = 0.05;
    gd[1] = 0.10;
    gd[2] = 0.20;
    gd[3] = 0.50;
    icaus = 1;

    tpi  = 8.0*atan(1.0);
/*
    if(Debug) 
        logit("e", "%s: npts: %d  itype: %d  dt: %f \n", 
                    whoami, npts, itype, dt);
*/
/* Find the raw maximum and its period    
 *************************************/

    demean(Data, npts);
    amaxper(npts, dt, Data, &amin_raw, &amax_raw, &pmax_raw, &imin_raw, &imax_raw);
    
    if(itype == 1) {  /* input data is displacement  */
        for(kk=0;kk<npts;kk++) d1[kk] = Data[kk];
        locut(d1, npts, 0.17, dt, 2, icaus);
        for(kk=1;kk<npts;kk++) {
            d2[kk] = (d1[kk] - d1[kk-1])/dt;
        }
        d2[0] = d2[1];
        demean(d2, npts);
        for(kk=1;kk<npts;kk++) {
            d3[kk] = (d2[kk] - d2[kk-1])/dt;
        }
        d3[0] = d3[1];
        demean(d3, npts);
    } else
    if(itype == 2) {  /* input data is velocity      */
        for(kk=0;kk<npts;kk++) d2[kk] = Data[kk];
        locut(d2, npts, 0.17, dt, 2, icaus);
        for(kk=1;kk<npts;kk++) {
            d3[kk] = (d2[kk] - d2[kk-1])/dt;
        }
        d3[0] = d3[1];
        demean(d3, npts);
        
        totint = 0.0;
        for(kk=0;kk<npts-1;kk++) {
            totint = totint + (d2[kk] + d2[kk+1])*0.5*dt;
            d1[kk] = totint;
        }
        d1[npts-1] = d1[npts-2];
        demean(d1, npts);
        
    } else
    if(itype == 3) {  /* input data is acceleration  */
        for(kk=0;kk<npts;kk++) d3[kk] = Data[kk];
        locut(d3, npts, 0.17, dt, 2, icaus);
        
        totint = 0.0;
        for(kk=0;kk<npts-1;kk++) {
            totint = totint + (d3[kk] + d3[kk+1])*0.5*dt;
            d2[kk] = totint;
        }
        d2[npts-1] = d2[npts-2];
        demean(d2, npts);
        
        totint = 0.0;
        for(kk=0;kk<npts-1;kk++) {
            totint = totint + (d2[kk] + d2[kk+1])*0.5*dt;
            d1[kk] = totint;
        }
        d1[npts-1] = d1[npts-2];
        demean(d1, npts);
    } else {
        return 1;
    }

/* Find the displacement(cm), velocity(cm/s), & acceleration(cm/s/s) maxima  and their periods    
 *********************************************************************************************/

    amaxper(npts, dt, &d1[0], &amin_dsp, &amax_dsp, &pmax_dsp, &imin_dsp, &imax_dsp);
    amaxper(npts, dt, &d2[0], &amin_vel, &amax_vel, &pmax_vel, &imin_vel, &imax_vel);
    amaxper(npts, dt, &d3[0], &amin_acc, &amax_acc, &pmax_acc, &imin_acc, &imax_acc);

/* Find the spectral response    
 ****************************/

    damp = 0.05;
    kk = 0;
    period = 0.3;
    Ark->A[k].period[kk] = period;
    Ark->A[k].freq[kk] = 1.0/period;
    omega = tpi/period;
    rdrvaa(&d3[0], npts-1, omega, damp, dt, &rd, &rv, &aa);
    Ark->A[k].RSA[kk] = aa;
    kk += 1;

    period = 1.0;
    Ark->A[k].period[kk] = period;
    Ark->A[k].freq[kk] = 1.0/period;
    omega = tpi/period;
    rdrvaa(&d3[0], npts-1, omega, damp, dt, &rd, &rv, &aa);
    Ark->A[k].RSA[kk] = aa;
    kk += 1;

    period = 3.0;
    Ark->A[k].period[kk] = period;
    Ark->A[k].freq[kk] = 1.0/period;
    omega = tpi/period;
    rdrvaa(&d3[0], npts-1, omega, damp, dt, &rd, &rv, &aa);
    Ark->A[k].RSA[kk] = aa;
    kk += 1;

    Ark->A[k].nRSA = kk;

/* Since we are here, determine the duration of strong shaking    
 *************************************************************/

    for(kk=0;kk<4;kk++) {
        id[kk] = npd[kk][1] = npd[kk][2] = 0;
        for(ii=1;ii<=npts-1;ii++) {
            a = fabs(d3[ii]/GRAVITY);
            if (a >= gd[kk]) {
                id[kk] = id[kk] + 1;
                if (id[kk] == 1) npd[kk][1] = ii;
                npd[kk][2] = ii;
            }
        }
        if (id[kk] != 0) {
            kpts = npd[kk][2] - npd[kk][1] + 1;
            sd[kk] = kpts*dt;
        } else {
            sd[kk] = 0.0;
        }
    }

    Ark->A[k].Pgd = fabs(amin_dsp)>fabs(amax_dsp)? fabs(amin_dsp):fabs(amax_dsp);
    Ark->A[k].Pgv = fabs(amin_vel)>fabs(amax_vel)? fabs(amin_vel):fabs(amax_vel);
    Ark->A[k].Pga = fabs(amin_acc)>fabs(amax_acc)? fabs(amin_acc):fabs(amax_acc);
    if(fabs(amin_dsp)>fabs(amax_dsp)) {
    	Ark->A[k].Pgd = fabs(amin_dsp);
    	Ark->A[k].tpgd = Ark->EvntTime + dt*imin_dsp;
    } else {
    	Ark->A[k].Pgd = fabs(amax_dsp);
    	Ark->A[k].tpgd = Ark->EvntTime + dt*imax_dsp;
    }
    if(Ark->A[k].Pgd < 0.0 || Ark->A[k].Pgd > 10000.0) {
    	Ark->A[k].Pgd = -1.0;
    	Ark->A[k].tpgd = 0.0;
    }

    if(fabs(amin_vel)>fabs(amax_vel)) {
    	Ark->A[k].Pgv = fabs(amin_vel);
    	Ark->A[k].tpgv = Ark->EvntTime + dt*imin_vel;
    } else {
    	Ark->A[k].Pgv = fabs(amax_vel);
    	Ark->A[k].tpgv = Ark->EvntTime + dt*imax_vel;
    }
    if(Ark->A[k].Pgv < 0.0 || Ark->A[k].Pgv > 10000.0) {
    	Ark->A[k].Pgv = -1.0;
    	Ark->A[k].tpgv = 0.0;
    }

    if(fabs(amin_acc)>fabs(amax_acc)) {
    	Ark->A[k].Pga = fabs(amin_acc);
    	Ark->A[k].tpga = Ark->EvntTime + dt*imin_acc;
    } else {
    	Ark->A[k].Pga = fabs(amax_acc);
    	Ark->A[k].tpga = Ark->EvntTime + dt*imax_acc;
    }
    if(Ark->A[k].Pga < 0.0 || Ark->A[k].Pga > 10000.0) {
    	Ark->A[k].Pga = -1.0;
    	Ark->A[k].tpga = 0.0;
    }
        
    if(Debug) {
    /*
    logit("e", "%s: amaxperR:. aminmm: %f  amaxmm: %f  pmax: %f  imin: %d  imax: %d \n", 
        whoami, amin_raw, amax_raw, pmax_raw, imin_raw, imax_raw);

    logit("e", "%s: amaxperA:. aminmm: %f  amaxmm: %f  pmax: %f  imin: %d  imax: %d \n", 
        whoami, amin_acc, amax_acc, pmax_acc, imin_acc, imax_acc);
    
    logit("e", "%s: amaxperV:. aminmm: %f  amaxmm: %f  pmax: %f  imin: %d  imax: %d \n", 
        whoami, amin_vel, amax_vel, pmax_vel, imin_vel, imax_vel);
        
    logit("e", "%s: amaxperD:. aminmm: %f  amaxmm: %f  pmax: %f  imin: %d  imax: %d \n", 
        whoami, amin_dsp, amax_dsp, pmax_dsp, imin_dsp, imax_dsp);
        
    logit("e", "%s: maxD: %f  maxV: %f  maxA: %f maxA: %f%%g  \n", 
        whoami, Ark->A[k].Pgd, Ark->A[k].Pgv, Ark->A[k].Pgq, 100.0*Ark->A[k].Pga/GRAVITY);
        */
    }

    return 0;
}    
    
      
/******************************************************************************
 *  Butterworth locut filter order 2*nroll (nroll<=8)                         *
 *   (see Kanasewich, Time Sequence Analysis in Geophysics,                   *
 *   Third Edition, University of Alberta Press, 1981)                        *
 *  written by W. B. Joyner 01/07/97                                          *
 *                                                                            *
 *  s[j] input = the time series to be filtered                               *
 *      output = the filtered series                                          *
 *      dimension of s[j] must be at least as large as                        *
 *        nd+3.0*float(nroll)/(fcut*delt)                                     *
 *  nd    = the number of points in the time series                           *
 *  fcut  = the cutoff frequency                                              *
 *  delt  = the timestep                                                      *
 *  nroll = filter order                                                      *
 *  causal if icaus.eq.1 - zero phase shift otherwise                         *
 *                                                                            *
 * The response is given by eq. 15.8-6 in Kanasewich:                         *
 *  Y = sqrt((f/fcut)**(2*n)/(1+(f/fcut)**(2*n))),                            *
 *                 where n = 2*nroll                                          *
 *                                                                            *
 * Dates: 01/07/97 - Written by Bill Joyner                                   *
 *        12/17/99 - D. Boore added check for fcut = 0.0, in which case       *
 *                   no filter is applied.  He also cleaned up the            *
 *                   appearance of the code (indented statements in           *
 *                   loops, etc.)                                             *
 *        02/04/00 - Changed "n" to "nroll" to eliminate confusion with       *
 *                   Kanesewich, who uses "n" as the order (=2*nroll)         *
 *        03/01/00 - Ported to C by Jim Luetgert                              *
 *                                                                            *
 ******************************************************************************/
void locut(double *s, int nd, double fcut, double delt, int nroll, int icaus)
{
    double    fact[8], b1[8], b2[8];
    double    pi, w0, w1, w2, w3, w4, w5, xp, yp, x1, x2, y1, y2;
    int       j, k, np2, npad;
    
    if (fcut == 0.0) return;       /* Added by DMB  */

    pi = 4.0*atan(1.0);
    w0 = 2.0*pi*fcut;
    w1 = 2.0*tan(w0*delt/2.0);
    w2 = (w1/2.0)*(w1/2.0);
    w3 = (w1*w1)/2.0 - 2.0;
    w4 = 0.25*pi/nroll;

    for(k=0;k<nroll;k++) {
        w5 = w4*(2.0*k + 1.0);
        fact[k] = 1.0/(1.0+sin(w5)*w1 + w2);
        b1[k] = w3*fact[k];
        b2[k] = (1.0-sin(w5)*w1 + w2)*fact[k];
    }

    np2 = nd;

    if(icaus != 1) {
        npad = (int)(3.0*nroll/(fcut*delt));
        np2 = nd+npad;
        for(j=nd;j<np2;j++) s[j] = 0.0;
    }

    for(k=0;k<nroll;k++) {
        x1 = x2 = y1 = y2 = 0.0;
        for(j=0;j<np2;j++) {
            xp = s[j];
            yp = fact[k]*(xp-2.0*x1+x2) - b1[k]*y1 - b2[k]*y2;
            s[j] = yp;
            y2 = y1;
            y1 = yp;
            x2 = x1;
            x1 = xp;
        }
    }

    if(icaus != 1) {
        for(k=0;k<nroll;k++) {
            x1 = x2 = y1 = y2 = 0.0;
            for(j=0;j<np2;j++) {
                xp = s[np2-j-1];
                yp = fact[k]*(xp-2.0*x1+x2) - b1[k]*y1 - b2[k]*y2;
                s[np2-j-1] = yp;
                y2 = y1;
                y1 = yp;
                x2 = x1;
                x1 = xp;
            }
        }
    }

    return;
}


/*----------------- BEGIN RDRVAA -----------------------------
      subroutine rdrvaa(acc,na,omega,damp,dt,rd,rv,aa)
* This is a modified version of "Quake.For", originally
* written by J.M. Roesset in 1971 and modified by
* Stavros A. Anagnostopoulos, Oct. 1986.  The formulation is that of
* Nigam and Jennings (BSSA, v. 59, 909-922, 1969).  
* Dates: 02/11/00 - Modified by David M. Boore, based on RD_CALC

*   acc = acceleration time series
*    na = length of time series
* omega = 2*pi/per
*  damp = fractional damping (e.g., 0.05)
*    dt = time spacing of input
*    rd = relative displacement of oscillator
*    rv = relative velocity of oscillator
*    aa = absolute acceleration of oscillator

      dimension acc(*)
*/
void rdrvaa(double *acc, int na, double omega, double damp, double dt, 
            double *rd, double *rv, double *aa)
{
    double    omt, d2, bom, d3, omd, om2, omdt, c1, c2, c3, c4, cc, ee;
    double    s1, s2, s3, s4, s5, a11, a12, a21, a22, b11, b12, b21, b22;
    double    y, ydot, y1, z, z1, z2, ra;
    int        i;
    
    omt = omega*dt;
    d2 = sqrt(1.0-damp*damp);
    bom = damp*omega;
    d3 = 2.0*bom;
    omd = omega*d2;
    om2 = omega*omega;
    omdt = omd*dt;
    c1 = 1.0/om2;
    c2 = 2.0*damp/(om2*omt);
    c3 = c1+c2;
    c4 = 1.0/(omega*omt);
    ee = exp(-damp*omt);
    cc = cos(omdt)*ee;
    s1 = sin(omdt)*ee/omd;
    s2 = s1*bom;
    s3 = s2 + cc;
    s4 = c4*(1.0-s3);
    s5 = s1*c4 + c2;
    
    a11 =  s3;          a12 = s1;
    a21 = -om2*s1;      a22 = cc - s2;
    
    b11 =  s3*c3 - s5;  b12 = -c2*s3 + s5 - c1;
    b21 = -s1+s4;       b22 = -s4;
    
    y = ydot = *rd = *rv = *aa = 0.0;
    for(i=0;i<na-1;i++) {
        y1   = a11*y + a12*ydot + b11*acc[i] + b12*acc[i+1];
        ydot = a21*y + a22*ydot + b21*acc[i] + b22*acc[i+1];
        y = y1;    /* y is the oscillator output at time corresponding to index i   */
        z = fabs(y);
        if (z > *rd) *rd = z;
        z1 = fabs(ydot);
        if (z1 > *rv) *rv = z1;
        ra = -d3*ydot -om2*y1;
        z2 = fabs(ra);
        if (z2 > *aa) *aa = z2;
    }
}

/********************************************************************
  $$$$$ CALLS NO OTHER ROUTINE $$$$$
 
    DEMEAN REMOVES THE MEAN FROM THE N POINT SERIES STORED IN
    ARRAY A.

 ********************************************************************/
void demean(double *A, int N)
{
    int        i;
    double    xm;
    
    xm = 0.0;
    for(i=0;i<N;i++) xm = xm + A[i];
    xm = xm/N;
    for(i=0;i<N;i++) A[i] = A[i] - xm;
}


/******************************************************************************
 *   compute maximum amplitude and its associated period                      *
 *                                                                            *
 *   input:                                                                   *
 *           npts   - number of points in timeseries                          *
 *           dt     - sample spacing in sec                                   *
 *           fc     - input timeseries                                        *
 *   output:                                                                  *
 *           amaxmm - raw maximum                                             *
 *           pmax   - period of maximum                                       *
 *           imax   - index of maxmimum point                                 *
 *                                                                            *
 ******************************************************************************/
void amaxper(int npts, double dt, double *fc, double *aminmm, double *amaxmm, 
                       double *pmax, int *imin, int *imax)
{
    double    amin, amax, pp, pm, mean, frac;
    int       i, j, jmin, jmax;
    
    *imax = jmax = *imin = jmin = 0;
    amax = amin = *amaxmm = *aminmm = fc[0];
    *aminmm = *pmax = mean = 0.0;
    for(i=0;i<npts;i++) {
        mean = mean + fc[i]/npts;
        if (fc[i] > amax) { jmax = i; amax = fc[i]; }
        if (fc[i] < amin) { jmin = i; amin = fc[i]; }
    }

/*     compute period of maximum    */

    pp = pm = 0.0;
    if (fc[jmax] > mean) {
        j = jmax+1;
        while(fc[j] > mean && j < npts) {
            pp += dt;
            j  += 1;
        }
        frac = dt*(mean-fc[j-1])/(fc[j]-fc[j-1]);
        frac = 0.0;
        pp = pp + frac;
        j = jmax-1;
        while(fc[j] > mean && j >= 0) {
            pm += dt;
            j  -= 1;
        }
        frac = dt*(mean-fc[j+1])/(fc[j]-fc[j+1]);
        frac = 0.0;
        pm = pm + frac;
    } else {
        j = jmax+1;
        if(fc[j] < mean && j < npts) {
            pp += dt;
            j  += 1;
        }
        frac = dt*(mean-fc[j-1])/(fc[j]-fc[j-1]);
        frac = 0.0;
        pp = pp + frac;
        j = jmax-1;
        if(fc[j] < mean && j >= 0) {
            pm += dt;
            j  -= 1;
        }
        frac = dt*(mean-fc[j+1])/(fc[j]-fc[j+1]);
        frac = 0.0;
        pm = pm + frac;
    }

    *imin = jmin;
    *imax = jmax;
    *pmax = 2.0*(pm+pp);
    *aminmm = amin;
    *amaxmm = amax;

    return;
}

