
      /*****************************************************************
       *                        latency_mon.c                          *
       *                                                               *
       *  This program takes waveforms dropped in InRing and           *
       *  logs latency and outages of the data.  Each time a trace     *
       *  arrives, its start time is compared to the end time of the   *
       *  last packet.  When there is a gap the outage is logged to    *
       *  a data file for that station.  Also, the start time is       *
       *  compared to real-time, and latency is determined.  Whenever  *
       *  the latency changes, it is also logged with start and end    *
       *  time to the file.  Latencies are grouped generally: <1' = 0; *
       *  1'-2' = 1; 2'-3' = 2; 3'-5' = 3; >5' = 4.                    *
       *  Here, latency is described as present time minus mid-time    *
       *  of the packet.                                               *
       *                                                               *
       *  The graphical output shows a line for each station in the    *
       *  .sta file for a set length of time.  Colors on the line      *
       *  represent the latency of the station over that time interval.*
       *                                                               *
       *  This will only give accurate outage values if this module is *
       *  running all the time the earthworm system is up.             *
       *                                                               *
       *  This module is strictly for use under Windows.               *
       *                                                               *
       *  Written by Paul Whitmore, (WC/ATWC) August, 2001             *
       *                                                               *
       ****************************************************************/
	   
/* slightly hacked by alex to read FindWave files 6/12/2 */
/* John Patton did:
     Thickened status lines.
	 Changed status line colors to:
	    Green < 1
		Yellow = 1-2 min
		Orange = 2-3 min
		Red = 3 - 5 min
		White > 5 min or out
	 Added network to Station ID.
	 Changed Station ID from red to black.
	 Added Current status indicator between status lines.
	 and Summerary
	 Enabled the program to save a file summery.txt which is
	 identical to the printer output.
	 Made the refresh button redraw summery lines to current time
*/

#include <windows.h>
#include <wingdi.h>
#include <winuser.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include <swap.h>
#include "latency_mon.h"

/* Global Variables 
   ****************/
EWH     Ewh;                   /* Parameters from earthworm.h */
MSG_LOGO getlogoW;             /* Logo of requested waveforms */
GPARM   Gparm;                 /* Configuration file parameters */
HINSTANCE hInstMain;           /* Copy of main program instance (process id) */
MSG_LOGO hrtlogo;              /* Logo of outgoing heartbeats */
HWND    hwndWndProc;           /* Client window handle */
int     iLate[MAX_STATIONS][2];/* Latency flag (see top for desc.) */
int     iRead;                 /* A retrieval has been performed */
LATENCY *Latency;              /* Latency data array */
time_t  lStartTime, lEndTime;  /* Station status time period (1/1/70 sec) */
MSG     msg;                   /* Windows control message variable */
pid_t   myPid;                 /* Process id of this process */
int     Nsta;                  /* Number of stations to display */
STATION *StaArray;             /* Station data array */
char    szProcessName[] = "Latency Monitor";
time_t  then;                  /* Previous heartbeat time */
char    *WaveBuf;              /* Pointer to waveform buffer */
TRACE_HEADER *WaveHead;        /* Pointer to waveform header */

      /***********************************************************
       *              The main program starts here.              *
       *                                                         *
       *  Argument:                                              *
       *     argv[1] = Name of configuration file                *
       ***********************************************************/

int WINAPI WinMain (HINSTANCE hInst, HINSTANCE hPreInst, 
                    LPSTR lpszCmdLine, int iCmdShow)
{
   char          configfile[64];  /* Name of config file */
   FILE         *hFile;           /* File handle */
   int           i;
   long          InBufl;          /* Maximum message size in bytes */
   char          line[40];        /* Heartbeat message */
   int           lineLen;         /* Length of heartbeat message */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */
   char          szFileName[64];  /* Latency log file name */
   static unsigned tidW;          /* Waveform getter Thread */
   static WNDCLASS wc;
   
   hInstMain = hInst;
   iRead = 0;
   
/* Get config file name (format "latency_mon latency_mon.D")
   ***********************************************************/
   if ( strlen( lpszCmdLine ) <= 0 )
   {
      fprintf( stderr, "Need configfile in start line.\n" );
      return -1;
   }
   strcpy( configfile, lpszCmdLine );

/* Get parameters from the configuration files
   *******************************************/
   if ( GetConfig( configfile, &Gparm ) == -1 )
   {
      fprintf( stderr, "GetConfig() failed. file %s.\n", configfile );
      return -1;
   }

/* Look up info in the earthworm.h tables
   **************************************/
   if ( GetEwh( &Ewh ) < 0 )
   {
      fprintf( stderr, "latency_mon: GetEwh() failed. Exiting.\n" );
      return -1;
   }

/* Specify logos of incoming waveforms and outgoing heartbeats
   ***********************************************************/
   getlogoW.instid = Ewh.GetThisInstId;
   getlogoW.mod    = Ewh.GetThisModId;
   getlogoW.type   = Ewh.TypeWaveform;

   hrtlogo.instid = Ewh.MyInstId;
   hrtlogo.mod    = Gparm.MyModId;
   hrtlogo.type   = Ewh.TypeHeartBeat;

/* Initialize name of log-file & open it
   *************************************/
   logit_init( configfile, Gparm.MyModId, 256, 1 );

/* Get our own pid for restart purposes
   ************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "latency_mon: Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Log the configuration parameters
   ********************************/
   LogConfig( &Gparm );

/* Allocate the waveform buffer
   ****************************/
   InBufl = MAX_TRACEBUF_SIZ*2;
   WaveBuf = (char *) malloc( (size_t) InBufl );
   if ( WaveBuf == NULL )
   {
      logit( "et", "latency_mon: Cannot allocate waveform buffer\n" );
      return -1;
   }

/* Point to header and data portions of waveform message
   *****************************************************/
   WaveHead  = (TRACE_HEADER *) WaveBuf;

/* Read the station list and return the number of stations found.
   Allocate the station list array.
   *************************************************************/
   /* We can tolerate two schemes: The original scheme, which reads
   a station list and a station data file. Or, we can read a FindWave
   debug file. */

   /* Read original station files */
   if(Gparm.FindWaveFile[0] == '0' && Gparm.StaFile[0] != '0')
   {
      if ( GetStaList( &StaArray, &Nsta, &Gparm ) == -1 )
      {
         fprintf( stderr, "latency_mon: GetStaList() failed. Exiting.\n" );
         free( WaveBuf );
         return -1;
      }
   }
   /* Read FindWave station list */
   else if ( Gparm.FindWaveFile[0] != '0' && Gparm.StaFile[0] == '0')
   {
      if ( GetFindWaveStaList( &StaArray, &Nsta, &Gparm ) == -1 )
      {
         fprintf( stderr, "latency_mon: GetStaList() failed. Exiting.\n" );
         free( WaveBuf );
         return -1;
      }
   }
   else  /* either both or neither were specified */
   {
      logit( "e", "Bad station list specification: .%s. .%s.\n",
             Gparm.FindWaveFile, Gparm.StaFile );
      return -1;
   }
   if ( Nsta == 0 )
   {
      logit( "et", "latency_mon: Empty station list. Exiting." );
      free( WaveBuf );
      free( StaArray );
      return -1;
   }
   logit( "t", "latency_mon: Displaying %d stations.\n", Nsta );

/* Log the station list
   ********************/
   LogStaList( StaArray, Nsta );

/* Allocate and init the Latency buffer
   ***********************************/
   InBufl = sizeof( LATENCY ) * Nsta;
   Latency = (LATENCY *) malloc( (size_t) InBufl );
   if ( Latency == NULL )
   {
      free( WaveBuf );
      free( StaArray );
      logit( "et", "latency_mon: Cannot allocate latency buffer\n");
      return -1;
   }
   for ( i=0; i<Nsta; i++ )       /* Fill latency structure */
   {
      InitLatency( &Latency[i] );
      strcpy( szFileName, Gparm.LogPath ); /* Make sure Latency file is there */
      strcat( szFileName, StaArray[i].szStation );
      strcat( szFileName, StaArray[i].szChannel );
      strcat( szFileName, ".LAT" );
      hFile = fopen( szFileName, "ab" );
   }

/* If this is the first instance of this program, init window stuff and
   register window (it always is)
   ********************************************************************/
   if ( !hPreInst )
   {  /* Force PAINT when sized and give double click notification */
      wc.style         = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
      wc.lpfnWndProc   = WndProc;         /* Control Window process */
      wc.cbClsExtra    = 0;
      wc.cbWndExtra    = 0;
      wc.hInstance     = hInst;           /* Process id */
      wc.hIcon         = LoadIcon( hInst, "latency_mon" );/* System app icon */
      wc.hCursor       = LoadCursor( NULL, IDC_ARROW );    /* Pointer */
      wc.hbrBackground = (HBRUSH) GetStockObject( WHITE_BRUSH ); /* White */
      wc.lpszMenuName  = "latency_mon_menu";   /* Relates to .RC file */
      wc.lpszClassName = szProcessName;
      if ( !RegisterClass( &wc ) )        /* Window not registered */
      {
         logit( "t", "RegisterClass failed\n" );
         free( WaveBuf );
         free( StaArray );
         return -1;
      }
   }

/* Create the window
   *****************/
   hwndWndProc = CreateWindow(
                 szProcessName,          /* Process name */
                 szProcessName,          /* Initial title bar caption */
                 WS_OVERLAPPEDWINDOW | WS_VSCROLL,
                 CW_USEDEFAULT,          /* top left x starting location */
                 CW_USEDEFAULT,          /* top left y starting location */
                 CW_USEDEFAULT,          /* Initial screen width in pixels */
                 CW_USEDEFAULT,          /* Initial screen height in pixels */
                 NULL,                   /* No parent window */
                 NULL,                   /* Use standard system menu */
                 hInst,                  /* Process id */
                 NULL );                 /* No extra data to pass in */
   if ( hwndWndProc == NULL )            /* Window not created */
   {
      logit( "t", "CreateWindow failed\n" );
      free( WaveBuf );
      free( StaArray );
      return 0;
   }
   
   ShowWindow( hwndWndProc, iCmdShow );  /* Show the Window */
   UpdateWindow( hwndWndProc );          /* Force an initial PAINT call */

/* Attach to existing transport rings
   **********************************/
   tport_attach( &Gparm.InRegion,  Gparm.InKey );

/* Flush the input waveform ring
   *****************************/
   while ( tport_getmsg( &Gparm.InRegion, &getlogoW, 1, &logo, &MsgLen,
                         WaveBuf, MAX_TRACEBUF_SIZ ) != GET_NONE );

/* Send 1st heartbeat to the transport ring
   ****************************************/
   time( &then );
   sprintf( line, "%d %d\n", then, myPid );
   lineLen = strlen( line );
   if ( tport_putmsg( &Gparm.InRegion, &hrtlogo, lineLen, line ) != PUT_OK )
   {
      logit( "et", "latency_mon: Error sending 1st heartbeat. Exiting." );
      tport_detach( &Gparm.InRegion );
      free( WaveBuf );
      free( StaArray );
      return 0;
   }

/* Start the waveform getter tread
   *******************************/
   if ( StartThread( WThread, 8192, &tidW ) == -1 )
   {
      tport_detach( &Gparm.InRegion );
      free( WaveBuf );
      free( StaArray );
      logit( "et", "Error starting W thread; exiting!\n" );
      return -1;
   }
   
/* Main windows thread
   *******************/      
   while ( GetMessage( &msg, NULL, 0, 0 ) )
   {
      TranslateMessage( &msg );
      DispatchMessage( &msg );
   }

/* Detach from the ring buffer
   ***************************/
   tport_detach( &Gparm.InRegion );
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
   free( WaveBuf );
   free( StaArray );
   logit( "t", "Termination requested. Exiting.\n" );
   return (msg.wParam);
}

 /********************************************************************
  *                 DisplayChannelIDLM()                             *
  *                                                                  *
  * This function displays the station and channel name in the       *
  * display window.                                                  *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  Sta               Array of all station data structures          *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  lTitleFHt         Title font height                             *
  *  lTitleFWd         Title font width                              *
  *  cxScreen          Horizontal pixel width of window              *
  *  cyScreen          Vertical pixel height of window               *
  *  iNumTracePerScreen Number of traces to put on screen            *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *                                                                  *
  ********************************************************************/
  
void DisplayChannelIDLM( HDC hdc, STATION Sta[], 
      int iNumStas, long lTitleFHt, long lTitleFWd,
      int cxScreen, int cyScreen, int iNumTracePerScreen, int iVScrollOffset,
      int iTitleOffset )
{
   HFONT   hOFont, hNFont;       /* Old and new font handles */
   int     i;
   long    lOffset;              /* Offset to center of trace from top */
   POINT   pt;                   /* Screen location for trace name */

/* Create font */
   hNFont = CreateFont( lTitleFHt, lTitleFWd, 
             0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
             DEFAULT_PITCH | FF_MODERN, "Elite" );
   hOFont = SelectObject( hdc, hNFont );
   SetTextColor( hdc, RGB( 0, 0, 0 ) );   /* Color trace names red */

/* Display all station/channel names */   
   for ( i=0; i<iNumStas; i++ )
   {
      pt.x = cxScreen / 500;
      lOffset = i*(cyScreen-iTitleOffset)/iNumTracePerScreen +
                iVScrollOffset + iTitleOffset;
      pt.y = lOffset - 3*lTitleFHt/8;
      TextOut( hdc, pt.x, pt.y, Sta[i].szStation, strlen( Sta[i].szStation ) );
      pt.x = 5*cxScreen / 100;
      TextOut( hdc, pt.x, pt.y, Sta[i].szChannel, strlen( Sta[i].szChannel ) );
      pt.x = 9*cxScreen / 100;
      TextOut( hdc, pt.x, pt.y, Sta[i].szNetID, strlen( StaArray[i].szNetID ) );
   }
   DeleteObject( SelectObject( hdc, hOFont ) );  /* Reset font */
}

 /********************************************************************
  *                 DisplayLatencyGraph()                            *
  *                                                                  *
  * This function displays colored lines which indicate latencies for*
  * stations over the time period entered.                           *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  cxScreen          Horizontal pixel width of window              *
  *  cyScreen          Vertical pixel height of window               *
  *  Sta               Array of all station data structures          *
  *  LatencyBuf        Latency buffer                                *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iChanIDOffset     Open space at left of screen for channel names*
  *  iSummaryOffset    Open space at rt. of screen for latencies     *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iNumTracePerScreen Number of traces to put on screen            *
  *                                                                  *
  ********************************************************************/
  
void DisplayLatencyGraph( HDC hdc, int cxScreen, int cyScreen, STATION Sta[], 
      LATENCY LatencyBuf[], int iVScrollOffset, int iChanIDOffset,
      int iSummaryOffset, int iNumStas, int iTitleOffset,
      int iNumTracePerScreen )
{
   double  dTemp; 
   HPEN    hRPen, hYPen, hGPen, hWPen, hOPen, hBBPen, hlRPen, hlYPen, hlGPen, hlWPen, hlOPen; /* Pen handles */
   int     i, j;
   int last_status;
   POINT   pt;                   /* Screen location for trace name */

/* Create pens */
   /*hGPen = CreatePen( PS_SOLID, 8, RGB( 0,222,0 ) );    */  /* green  */
   /*hOPen = CreatePen( PS_SOLID, 8, RGB( 255,128,0 ) );  */  /* Orange */
   /*hYPen = CreatePen( PS_SOLID, 8, RGB( 255,255,0 ) );  */  /* yellow */
   /*hRPen = CreatePen( PS_SOLID, 8, RGB( 222,0,0 ) );    */  /* red    */
   /*hWPen = CreatePen( PS_SOLID, 8, RGB( 255,255,255 ) );*/  /* white  */

   LOGBRUSH lb;
   
   lb.lbStyle = BS_SOLID;
   lb.lbColor = RGB( 0,222,0 );
   lb.lbHatch = HS_BDIAGONAL;
   hGPen = ExtCreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT |
            PS_JOIN_MITER, 8, &lb, 0, NULL );      /* green  */

   lb.lbStyle = BS_SOLID;
   lb.lbColor = RGB( 255,128,0 );
   lb.lbHatch = HS_BDIAGONAL;
   hOPen = ExtCreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT |
            PS_JOIN_MITER, 8, &lb, 0, NULL );      /* Orange */

   lb.lbStyle = BS_SOLID;
   lb.lbColor = RGB( 255,255,0 );
   lb.lbHatch = HS_BDIAGONAL;
   hYPen = ExtCreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT |
            PS_JOIN_MITER, 8, &lb, 0, NULL );     /* yellow */

   lb.lbStyle = BS_SOLID;
   lb.lbColor = RGB( 222,0,0 );
   lb.lbHatch = HS_BDIAGONAL; 
   hRPen = ExtCreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT |
            PS_JOIN_MITER, 8, &lb, 0, NULL );     /* red    */

   lb.lbStyle = BS_SOLID;
   lb.lbColor = RGB( 255,255,255 );
   lb.lbHatch = HS_BDIAGONAL;
   hWPen = ExtCreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT |
            PS_JOIN_MITER, 8, &lb, 0, NULL );     /* white  */

   lb.lbStyle = BS_SOLID;
   lb.lbColor = RGB( 0,0,0 );
   lb.lbHatch = HS_BDIAGONAL;
   hBBPen = ExtCreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_ROUND |
             PS_JOIN_ROUND, 10, &lb, 0, NULL );   /* black box  */

/*   hBBPen = CreatePen( PS_SOLID, 10, RGB( 0,0,0 ) );       black line  */

   hlGPen = CreatePen( PS_SOLID, 6, RGB( 0,222,0 ) );      /* green dot  */
   hlOPen = CreatePen( PS_SOLID, 6, RGB( 255,128,0 ) );    /* Orange dot */
   hlYPen = CreatePen( PS_SOLID, 6, RGB( 255,255,0 ) );    /* yellow dot */
   hlRPen = CreatePen( PS_SOLID, 6, RGB( 222,0,0 ) );      /* red dot    */
   hlWPen = CreatePen( PS_SOLID, 6, RGB( 255,255,255 ) );  /* white dot  */

/* Draw lines for each channel */   
   if ( iRead == 1 )
      for ( i=0; i<iNumStas; i++ )
      {
         last_status = 4;
         pt.y = i*(cyScreen-iTitleOffset)/iNumTracePerScreen +
                iVScrollOffset + iTitleOffset+3;
         for ( j=0; j<LatencyBuf[i].lIntervals; j++ )
         {
            if ( LatencyBuf[i].iLate[j] == 0 )
            {
               SelectObject( hdc, hGPen );          /* Green pen = <1 minute */
               last_status = 0;
            }
            else if ( LatencyBuf[i].iLate[j] == 1 )
            {
               SelectObject( hdc, hYPen );          /* Yellow pen = 1-2 min */
               last_status = 1;
            }
            else if ( LatencyBuf[i].iLate[j] == 2 )
            {
               SelectObject( hdc, hOPen );          /* Orange pen = 2-3 minutes*/
               last_status = 2;
            }
            else if ( LatencyBuf[i].iLate[j] == 3 )
            {
               SelectObject( hdc, hRPen );          /* Red pen = 3-5 minutes */
               last_status = 3;
            }
            else if ( LatencyBuf[i].iLate[j] == 4 )
            {
               SelectObject( hdc, hWPen );          /* White pen = >5 minutes */
               last_status = 4;
            }
            if ( j == 0 )
            {
               dTemp = LatencyBuf[i].dStart[j]-(double) lStartTime;
               if ( dTemp < 0. ) dTemp = 0.;
               pt.x = (long) ((double) (cxScreen-iChanIDOffset-iSummaryOffset) *
                       dTemp / (double) (lEndTime-lStartTime)) + iChanIDOffset;
            }
            else
               pt.x = (long) ((double) (cxScreen-iChanIDOffset-iSummaryOffset) *
                      (LatencyBuf[i].dStart[j]-(double) lStartTime) /
                      (double) (lEndTime-lStartTime)) + iChanIDOffset;
            if ( pt.x < iChanIDOffset ) pt.x = iChanIDOffset;
            MoveToEx( hdc, pt.x, pt.y, NULL );
            if ( j == LatencyBuf[i].lIntervals-1 )
            {
               dTemp = LatencyBuf[i].dEnd[j]-(double) lStartTime;
               if ( dTemp > (double) (lEndTime-lStartTime) )
                  dTemp = (double) (lEndTime-lStartTime);
               pt.x = (long) ((double) (cxScreen-iChanIDOffset-iSummaryOffset) *
                      dTemp / (double) (lEndTime-lStartTime)) + iChanIDOffset;
            }
            else
               pt.x = (long) ((double) (cxScreen-iChanIDOffset-iSummaryOffset) *
                      (LatencyBuf[i].dEnd[j]-(double) lStartTime) /
                      (double) (lEndTime-lStartTime)) + iChanIDOffset;
            if ( pt.x > cxScreen-iSummaryOffset )
               pt.x = cxScreen-iSummaryOffset;
            LineTo( hdc, pt.x, pt.y );
         }  

         pt.x = 68*cxScreen/100;
         SelectObject( hdc, hBBPen );
         MoveToEx( hdc, pt.x, pt.y, NULL );
         pt.x = 69*cxScreen/100;
         LineTo( hdc, pt.x, pt.y );
         if ( last_status == 0 )
            SelectObject( hdc, hlGPen );          /* Green pen = <1 minute */
         else if ( last_status == 1 )
            SelectObject( hdc, hlYPen );          /* Yellow pen = 1-2 min */
         else if ( last_status == 2 )
            SelectObject( hdc, hlOPen );          /* Orange pen = 2-3 minutes*/
         else if ( last_status == 3 )
            SelectObject( hdc, hlRPen );          /* Red pen = 3-5 minutes */
         else if ( last_status == 4 )
            SelectObject( hdc, hlWPen );          /* White pen = >5 minutes */
         pt.x = 68*cxScreen/100;		 
         MoveToEx( hdc, pt.x, pt.y, NULL );
         pt.x = 69*cxScreen/100;
         LineTo( hdc, pt.x, pt.y );
      }
   DeleteObject( hOPen );                           /* Delete Pens */
   DeleteObject( hRPen );   
   DeleteObject( hGPen );   
   DeleteObject( hWPen );   
   DeleteObject( hYPen );   
   DeleteObject( hBBPen );
   DeleteObject( hlOPen );                           
   DeleteObject( hlRPen );   
   DeleteObject( hlGPen );   
   DeleteObject( hlWPen );   
   DeleteObject( hlYPen );   
}

     /**************************************************************
      *                 DisplayTitles()                            *
      *                                                            *
      * This function outputs the title and latencies to the       *
      * display window along with the color legend and start/end   *
      * times.                                                     *
      *                                                            *
      * Arguments:                                                 *
      *  hdc               Device context to use                   *
      *  lTitleFHt         Title font height                       *
      *  lTitleFWd         Title font width                        *
      *  cxScreen          Screen width in pixels                  *
      *  cyScreen          Screen height in pixels                 *
      *  LatencyBuf        Latency buffer                          *
      *  iVScrollOffset    Vertical scroll setting                 *
      *  iNumStas          Number of stations in Sta array         *
      *  iTitleOffset      Title offset to give room at top        *
      *  iNumTracePerScreen Number of traces to put on screen      *
      *                                                            *
      **************************************************************/
      
void DisplayTitles( HDC hdc, long lTitleFHt, long lTitleFWd, int cxScreen,
                    int cyScreen, LATENCY LatencyBuf[], int iVScrollOffset,
                    int iNumStas, int iTitleOffset, int iNumTracePerScreen )
{
   HFONT   hOFont, hNFont;      /* Font handles */
   HPEN    hRPen, hYPen, hGPen, hWPen, hOPen; /* Pen handles */
   int     i, j;
   long    lOffset;             /* Offset to center of trace from top */
   char   *pszTime;             /* ASCII start and end times */
   POINT   pt;
   char    szBuf[64];           /* Latency summary for each station */
   char    szTemp[8];


   LOGBRUSH lb;
   
   lb.lbStyle = BS_SOLID;
   lb.lbColor = RGB( 0,222,0 );
   lb.lbHatch = HS_BDIAGONAL;
   hGPen = ExtCreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT | PS_JOIN_MITER, 8, &lb, 0, NULL );      /* green  */

   lb.lbStyle = BS_SOLID;
   lb.lbColor = RGB( 255,128,0 );
   lb.lbHatch = HS_BDIAGONAL;
   hOPen = ExtCreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT | PS_JOIN_MITER, 8, &lb, 0, NULL );    /* Orange */

   lb.lbStyle = BS_SOLID;
   lb.lbColor = RGB( 255,255,0 );
   lb.lbHatch = HS_BDIAGONAL;
   hYPen = ExtCreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT | PS_JOIN_MITER, 8, &lb, 0, NULL );    /* yellow */

   lb.lbStyle = BS_SOLID;
   lb.lbColor = RGB( 222,0,0 );
   lb.lbHatch = HS_BDIAGONAL; 
   hRPen = ExtCreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT | PS_JOIN_MITER, 8, &lb, 0, NULL );      /* red    */

   lb.lbStyle = BS_SOLID;
   lb.lbColor = RGB( 255,255,255 );
   lb.lbHatch = HS_BDIAGONAL;
   hWPen = ExtCreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT | PS_JOIN_MITER, 8, &lb, 0, NULL );  /* white  */

/* Create font */   
   hNFont = CreateFont( lTitleFHt, lTitleFWd, 
             0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
             FIXED_PITCH | FF_MODERN, "Elite" );

/* Select font and color */
   hOFont = (HFONT) SelectObject( hdc, hNFont );    /* Select the font */
   SetTextColor( hdc, RGB( 0, 0, 0 ) );             /* Black text */
	 
/* Write latency summary title */
   pt.x = 73*cxScreen/100;
   pt.y = iVScrollOffset;
   TextOut( hdc, pt.x, pt.y, "Latency Summary (Minutes)", 25 );
   pt.x = 67*cxScreen/100;
   pt.y = 3*cyScreen/100 + iVScrollOffset;
   TextOut( hdc, pt.x, pt.y, "Now  <1    1-2   2-3   3-5   Off", 32 );
   
/* Show line colors under related latency times */
   pt.y = 6*cyScreen/100 + iVScrollOffset;
   pt.x = 143*cxScreen/200;               /* Green pen = <1 minute */
   SelectObject( hdc, hGPen );
   MoveToEx( hdc, pt.x, pt.y, NULL );
   pt.x = 149*cxScreen/200;
   LineTo( hdc, pt.x, pt.y );
   pt.x = 155*cxScreen/200;               /* Yellow pen = 1-2 minutes */
   SelectObject( hdc, hYPen );
   MoveToEx( hdc, pt.x, pt.y, NULL );
   pt.x = 161*cxScreen/200;
   LineTo( hdc, pt.x, pt.y );
   pt.x = 167*cxScreen/200;               /* Orange pen = 2-3 minutes */
   SelectObject( hdc, hOPen );
   MoveToEx( hdc, pt.x, pt.y, NULL );
   pt.x = 173*cxScreen/200;
   LineTo( hdc, pt.x, pt.y );
   pt.x = 179*cxScreen/200;               /* Red pen = 3-5 minutes */
   SelectObject( hdc, hRPen );
   MoveToEx( hdc, pt.x, pt.y, NULL );
   pt.x = 185*cxScreen/200;
   LineTo( hdc, pt.x, pt.y );
   pt.x = 191*cxScreen/200;               /* White pen = >5 minutes (or out) */
   SelectObject( hdc, hWPen );
   MoveToEx( hdc, pt.x, pt.y, NULL );
   pt.x = 197*cxScreen/200;
   LineTo( hdc, pt.x, pt.y );   
   
/* Write start and end times */   
   if ( iRead == 1 )
   {
      pt.y = iVScrollOffset;
      pt.x = 14*cxScreen/100;
      pszTime = asctime( TWCgmtime( lStartTime ) );
      strcpy( szBuf, "\0" );
      strncpy( szBuf, pszTime, 24 );
      szBuf[24] = '\0';
      strcat( szBuf, " TO " );
      pszTime = asctime( TWCgmtime( lEndTime ) );
      strncat( szBuf, pszTime, 24 );
      TextOut( hdc, pt.x, pt.y, szBuf, strlen( szBuf ) );
   }
   
/* Write latency summary */   
   if ( iRead == 1 )
      for ( i=0; i<iNumStas; i++ )
      {
         pt.x = 70*cxScreen/100;
         strcpy( szBuf, "" );
         for ( j=0; j<NUM_INTERVALS; j++ )
         {
            sprintf( szTemp, "%5.1lf ", LatencyBuf[i].dPct[j] );
            strcat( szBuf, szTemp );
         }
         lOffset = i*(cyScreen-iTitleOffset)/iNumTracePerScreen +
                   iVScrollOffset + iTitleOffset;
         pt.y = lOffset - 3*lTitleFHt/8;
         TextOut( hdc, pt.x, pt.y, szBuf, strlen( szBuf ) );
      }

   DeleteObject( hOPen );                           /* Delete Pens */
   DeleteObject( hRPen );   
   DeleteObject( hGPen );   
   DeleteObject( hWPen );   
   DeleteObject( hYPen );   
   DeleteObject( SelectObject( hdc, hOFont ) );     /* Reset font */
}

      /*******************************************************
       *                      GetEwh()                       *
       *                                                     *
       *      Get parameters from the earthworm.d file.      *
       *******************************************************/

int GetEwh( EWH *Ewh )
{
   if ( GetLocalInst( &Ewh->MyInstId ) != 0 )
   {
      fprintf( stderr, "latency_mon: Error getting MyInstId.\n" );
      return -1;
   }
   if ( GetInst( "INST_WILDCARD", &Ewh->GetThisInstId ) != 0 )
   {
      fprintf( stderr, "latency_mon: Error getting GetThisInstId.\n" );
      return -2;
   }
   if ( GetModId( "MOD_WILDCARD", &Ewh->GetThisModId ) != 0 )
   {
      fprintf( stderr, "latency_mon: Error getting GetThisModId.\n" );
      return -3;
   }
   if ( GetType( "TYPE_HEARTBEAT", &Ewh->TypeHeartBeat ) != 0 )
   {
      fprintf( stderr, "latency_mon: Error getting TypeHeartbeat.\n" );
      return -4;
   }
   if ( GetType( "TYPE_ERROR", &Ewh->TypeError ) != 0 )
   {
      fprintf( stderr, "latency_mon: Error getting TypeError.\n" );
      return -5;
   }
   if ( GetType( "TYPE_TRACEBUF", &Ewh->TypeWaveform ) != 0 )
   {
      fprintf( stderr, "latency_mon: Error getting TYPE_TRACEBUF.\n" );
      return -6;
   }
   return 0;
}

   /*******************************************************************
    *                         InitLatency()                           *
    *                                                                 *
    * This function initializes the latency structure for one station.*
    *                                                                 *
    *  Arguments:                                                     *
    *    pLatency      Pointer to latency structure                   *
    *                                                                 *
    *******************************************************************/
	
void InitLatency( LATENCY *pLatency )
{
   int     i;

   for ( i=0; i<MAX_ONOFF; i++ )
   {
      pLatency->iLate[i]  = 0;
      pLatency->dStart[i] = 0.;
      pLatency->dEnd[i]   = 0.;
   }
   for ( i=0; i<NUM_INTERVALS; i++ )
      pLatency->dPct[i] = 0.;
   pLatency->lIntervals = 0;
}

      /***********************************************************
       *                   StationStatusDlgProc()                *
       *                                                         *
       * This dialog procedure lets the user specify the time    *
       * interval over which to display the latencies and summary*
       *                                                         *
       ***********************************************************/
	   
long WINAPI StationStatusDlgProc( HWND hwnd, UINT msg, UINT wParam, long lParam)
{
   int        iTemp;       /* Dummy variable for GetDlg... */
   int        iTT;         /* Time in hours to retrieve from data files */
   static time_t lTime;    /* Present time (1/1/70 seconds */
   static struct tm *tm;   /* C time structure */

   switch ( msg ) 
   {
      case WM_INITDIALOG:  /* Pre-set entry field to present number */
         time( &lTime );
         tm = TWCgmtime( lTime );
         SetDlgItemInt( hwnd, EF_DISPLAYYEAR, (int) tm->tm_year+1900, TRUE );
         SetDlgItemInt( hwnd, EF_DISPLAYMONTH, (int) tm->tm_mon+1, TRUE );
         SetDlgItemInt( hwnd, EF_DISPLAYDAY, (int) tm->tm_mday, TRUE );
         SetDlgItemInt( hwnd, EF_DISPLAYHOUR, (int) tm->tm_hour, TRUE );
         SetDlgItemInt( hwnd, EF_DISPLAYTOTALTIME, 24, TRUE );
         SetFocus( hwnd );
         break;

      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {
            case IDOK:     /* Accept user input */
               tm->tm_year = GetDlgItemInt( hwnd, EF_DISPLAYYEAR, &iTemp, TRUE)
                             - 1900;
               if ( tm->tm_year < 100 || tm->tm_year > 200  ) /* Check year */
               {  MessageBox( hwnd, "Invalid Year",
                              NULL, MB_OK | MB_ICONEXCLAMATION ); break; }
               tm->tm_mon =
                GetDlgItemInt( hwnd, EF_DISPLAYMONTH, &iTemp, TRUE)-1;
               if ( tm->tm_mon < 0 || tm->tm_mon > 11  )   /* Check month */
               {  MessageBox( hwnd, "Invalid Month",
                              NULL, MB_OK | MB_ICONEXCLAMATION ); break; }
               tm->tm_mday = GetDlgItemInt( hwnd, EF_DISPLAYDAY, &iTemp, TRUE);
               if ( tm->tm_mday < 1 || tm->tm_mday > 31  ) /* Check day */
               {  MessageBox( hwnd, "Invalid Day",
                              NULL, MB_OK | MB_ICONEXCLAMATION ); break; }
               tm->tm_hour = GetDlgItemInt( hwnd, EF_DISPLAYHOUR, &iTemp, TRUE);
               if ( tm->tm_hour < 0 || tm->tm_hour > 23  ) /* Check hour */
               {  MessageBox( hwnd, "Invalid Hour",
                              NULL, MB_OK | MB_ICONEXCLAMATION ); break; }
               tm->tm_min = 0;
               tm->tm_sec = 0;
               tm->tm_isdst = 0;
               iTT = GetDlgItemInt( hwnd, EF_DISPLAYTOTALTIME, &iTemp, TRUE);
               if ( iTT <= 0 )                         /* Check total time */
               {  MessageBox( hwnd, "Invalid Total Time",
                              NULL, MB_OK | MB_ICONEXCLAMATION ); break; }
               lStartTime = mktime( tm );
               if ( lStartTime < 0 )                   /* Check mktime return */
               {  MessageBox( hwnd, "Invalid Time",
                              NULL, MB_OK | MB_ICONEXCLAMATION ); break; }
               lEndTime = lStartTime + iTT*3600;
               EndDialog (hwnd, IDOK);
               break;

            case IDCANCEL: /* Escape - don't accept input */
               EndDialog (hwnd, IDCANCEL);
               break;
         }
         break;
   }
   return 0;
}

      /***********************************************************
       *                 TracePerScreenDlgProc()                 *
       *                                                         *
       * This dialog procedure lets the user set the number of   *
       * traces to be shown on the visible part of the screen    *
       * display.                                                *
       *                                                         *
       ***********************************************************/
	   
long WINAPI TracePerScreenDlgProc( HWND hwnd, UINT msg, UINT wParam,
                                   long lParam )
{
   int     iTemp;

   switch ( msg ) 
   {
      case WM_INITDIALOG:  /* Pre-set entry field to present number */
         SetDlgItemInt( hwnd, EF_NUMSTATODISP, Gparm.NumTracePerScreen, TRUE );
         SetFocus( hwnd );
         break;

      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {
            case IDOK:     /* Accept user input */
               Gparm.NumTracePerScreen =
                GetDlgItemInt( hwnd, EF_NUMSTATODISP, &iTemp, TRUE );
               if ( Gparm.NumTracePerScreen > Nsta )
                  Gparm.NumTracePerScreen = Nsta;
               if ( Gparm.NumTracePerScreen < 2 )
                  Gparm.NumTracePerScreen = 2;
               EndDialog (hwnd, IDOK);
               break;

            case IDCANCEL: /* Escape - don't accept input */
               EndDialog (hwnd, IDCANCEL);
               break;
		
            default:
               break;
         }
   }
   return 0;
}

      /***********************************************************
       *                      WndProc()                          *
       *                                                         *
       *  This dialog procedure processes messages from the      *
       *  windows screen.  All                                   *
       *  display is performed in PAINT.  Menu options (on main  *
       *  menu and through right button clicks) control the      *
       *  display.                                               *
       *                                                         *
       ***********************************************************/
       
long WINAPI WndProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   static  int    cxScreen, cyScreen;        /* Window size in pixels */
   double  dLastEnd;        /* End time of last latency interval */
   double  dStart, dEnd;    /* Latency time period read from file */
   HCURSOR hCursor;         /* Present cursor handle (hourglass or arrow) */
   static  HDC hdc;         /* Device context of screen */
   FILE    *hFile;          /* File handle */
   static  HANDLE  hMenu;   /* Handle to the menu */
   FILE    *hPrinter;       /* Printer handle */
   FILE    *hSaveFile;       /* Printer handle */
   int     i, iCnt, j, k;
   static  int  iChanIDOffset;/* Trace offset at left of screen */
   int     iLateT;          /* Latency value read from file */
   int     iRC;             /* Return code from reads */
   static  int  iScrollVertBuff;      /* Small vertical scroll amount (pxl) */
   static  int  iScrollVertPage;      /* Large vertical scroll amount (pxl) */				
   static  int  iSummaryOffset;       /* Summary offset at right of screen */
   static  int  iVScrollOffset;       /* Vertical scoll bar setting */
   static  long lTitleFHt, lTitleFWd; /* Font height and width */
   static  int  iTitleOffset;         /* Trace offset at top of screen */
   long    lTotalTime;      /* Time (sec) in period of interest */
   static  LATENCY *pLatency; /* Pointer to the latency structure processed */
   PAINTSTRUCT ps;          /* Paint structure used in WM_PAINT command */
   RECT    rct;             /* RECT (rectangle structure) */
   static  STATION *Sta;    /* Pointer to the station being processed */
   char    szFileName[64];  /* Latency log file name */

/* Respond to user input (menu choices, etc.) and system messages */
   switch ( msg )
   {
      case WM_CREATE:         /* Do this the first time through */
         hCursor = LoadCursor( NULL, IDC_ARROW );
         SetCursor( hCursor );
         hMenu = GetMenu( hwnd );
         iVScrollOffset = 0;       /* Start with no vertical scrolling */
         break;
		 
/* Get screen size in pixels, re-paint, and re-proportion screen */
      case WM_SIZE:
         cyScreen = HIWORD (lParam);
         cxScreen = LOWORD (lParam);
		 
/* Compute font size */
         lTitleFHt = cyScreen / 33;
         lTitleFWd = cxScreen / 100;
		 
/* Compute vertical scrolling amounts */		 
         iScrollVertBuff = cyScreen / Gparm.NumTracePerScreen;
         iScrollVertPage = cyScreen / 2;
		 
/* Compute offsets from top and left sides of screen */		 
         iTitleOffset = 8*cyScreen / 100;
         iChanIDOffset = 13*cxScreen / 100;
         iSummaryOffset = 34*cxScreen / 100; /*31*/
	 
/* Set scroll thumb positions */
         SetScrollRange( hwnd, SB_VERT, 0, 100, FALSE );  /* 100/50 arbitrary */
         SetScrollPos( hwnd, SB_VERT, 0, TRUE );
		 
         InvalidateRect( hwnd, NULL, TRUE );    /* Force a re-PAINT */
         break;

/* Respond to menu selections */
      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {
/* This menu option lets the user change the number of traces
   which are squished into the visible (vertically) part of the
   screen display. */
            case IDM_TRACEPERSCREEN:	
               if ( DialogBox( hInstMain, "TracePerScreen", hwndWndProc, 
                   (DLGPROC) TracePerScreenDlgProc ) == IDOK )
               {
                  iScrollVertBuff = cyScreen / Gparm.NumTracePerScreen;
                  iScrollVertPage = cyScreen / 8;
                  InvalidateRect( hwnd, NULL, TRUE );
               }
               break;

/* Print a summary of the latency percentages to the printer */
            case IDM_PRINTSUMMARY:	
               if ( iRead == 1 )
               {
                  hPrinter = fopen( Gparm.PrinterPath, "w" );
                  if ( hPrinter == NULL )
                  {
                     logit( "et", "latency_mon: can't open printer.\n" );
                     break;
                  }
                  fprintf( hPrinter, "STATION DATA TRANSMISSION STATUS\n\n" );
                  fprintf( hPrinter, "Time Period (UTC):\n" );
                  fprintf( hPrinter, "Start: %s",
                           asctime( TWCgmtime( lStartTime ) ) );
                  fprintf( hPrinter, "End:   %s\n",
                           asctime( TWCgmtime( lEndTime ) ) );
                  fprintf( hPrinter, "                       Latency Summary\n");
                  fprintf( hPrinter, "Stn  Chn  Net    <1    1-2   2-3   3-5   Off\n" );
                  fprintf( hPrinter, "--------------------------------------------\n" );
                  for ( i=0; i<Nsta; i++ )
                     fprintf( hPrinter, "%-4s %-4s %-4s %5.1lf %5.1lf %5.1lf %5.1lf %5.1lf\n",
                              StaArray[i].szStation, StaArray[i].szChannel,
                              StaArray[i].szNetID, Latency[i].dPct[0],
                              Latency[i].dPct[1], Latency[i].dPct[2],
                              Latency[i].dPct[3], Latency[i].dPct[4] );
                  fprintf( hPrinter, "%c", '\014');   /* Formfeed */
                  fclose( hPrinter );
               }
               break;

/* Save a summary of the latency percentages to a file */
            case IDM_SAVESUMMARY:	
               if ( iRead == 1 )
               {
                  hSaveFile = fopen( "summery.txt", "w" );
                  if ( hSaveFile == NULL )
                  {
                     logit( "et", "latency_mon: can't open save file.\n" );
                     break;
                  }
                  fprintf( hSaveFile, "STATION DATA TRANSMISSION STATUS\n\n" );
                  fprintf( hSaveFile, "Time Period (UTC):\n" );
                  fprintf( hSaveFile, "Start: %s",
                           asctime( TWCgmtime( lStartTime ) ) );
                  fprintf( hSaveFile, "End:   %s\n",
                           asctime( TWCgmtime( lEndTime ) ) );
                  fprintf( hSaveFile, "                       Latency Summary\n");
                  fprintf( hSaveFile, "Stn  Chn  Net    <1    1-2   2-3   3-5   Off\n" );
                  fprintf( hSaveFile, "--------------------------------------------\n" );
                  for ( i=0; i<Nsta; i++ )
                     fprintf( hSaveFile, "%-4s %-4s %-4s %5.1lf %5.1lf %5.1lf %5.1lf %5.1lf\n",
                              StaArray[i].szStation, StaArray[i].szChannel,
                              StaArray[i].szNetID, Latency[i].dPct[0],
                              Latency[i].dPct[1], Latency[i].dPct[2],
                              Latency[i].dPct[3], Latency[i].dPct[4] );
                  fclose( hSaveFile );
               }
               break;

/* Let user enter latency period of interest, and read in files */
            case IDM_STATION_STATUS:	
               if ( DialogBox( hInstMain, "StationStatus", hwndWndProc, 
                   (DLGPROC) StationStatusDlgProc ) == IDOK )
               {
/* One-by-one, read in the latency files */               
                  for (i=0; i<Nsta; i++ )
                  {
                     Sta = (STATION *) &StaArray[i];
                     pLatency = (LATENCY *) &Latency[i];
                     InitLatency( pLatency );
                     iCnt = 0;                      
                     strcpy( szFileName, Gparm.LogPath ); /* Latency file */
                     strcat( szFileName, Sta->szStation );
                     strcat( szFileName, Sta->szChannel );
                     strcat( szFileName, ".LAT" );
                     hFile = fopen( szFileName, "rb" );
                     if ( hFile != NULL )
                     {
                        for ( ;; )
                        {
                           iRC = fread( &iLateT, sizeof( int ), 1, hFile );
                           if ( iRC < 1 ) break;
                           iRC = fread( &dStart, sizeof( double ), 1, hFile );
                           if ( iRC < 1 ) break;
                           iRC = fread( &dEnd, sizeof( double ), 1, hFile );
                           if ( iRC == 1 )
                           {
/* Compare times to period of interest */                        
                              if ( dEnd   > (double) lStartTime &&
                                   dStart < (double) lEndTime )/* In per.? */
                              {
                                 pLatency->iLate[iCnt]  = iLateT;
                                 pLatency->dStart[iCnt] = dStart;
                                 pLatency->dEnd[iCnt]   = dEnd;
                                 iCnt++;
                                 if ( iCnt == MAX_ONOFF )
                                 {
                                    logit( "", "%s %s Max latency pers. reached\n",
                                           Sta->szStation, Sta->szChannel );
                                    break;
                                 }
/* If we are passed time of interest, start another file */                              
                              }
                              if ( dStart > (double) lEndTime ) break;
                           }
/* If only two are read, we must be up to present time */                           
                           else if ( iRC < 1 )
                           {
                              dEnd = Sta->dEndTime;
                              logit( "", "%s %s iRC<1\n", 
                                     Sta->szStation, Sta->szChannel );
/* Compare times to period of interest */                        
                              if ( dEnd   > (double) lStartTime &&
                                   dStart < (double) lEndTime )/* In per.? */
                              {
                                 pLatency->iLate[iCnt]  = iLateT;
                                 pLatency->dStart[iCnt] = dStart;
                                 pLatency->dEnd[iCnt]   = dEnd;
                                 iCnt++;
                                 if ( iCnt == MAX_ONOFF )
                                 {
                                    logit( "", "%s %s Max latency pers. reached\n",
                                           Sta->szStation, Sta->szChannel );
                                    break;
                                 }
/* If we are passed time of interest, start another file */                              
                              }
                              if ( dStart > (double) lEndTime ) break;
                           }
/* Otherwise, we must be at end of file (or bad file) */                           
                           else break;
                        }
                        fclose( hFile );
                        pLatency->lIntervals = (long) iCnt;
                        
/* Compute percentages of times in each latency interval */                        
                        if ( pLatency->lIntervals == 0 ) /* Always out */
                        {
                           for ( j=0; j<NUM_INTERVALS-1; j++ )
                           pLatency->dPct[j] = 0.;
                           pLatency->dPct[NUM_INTERVALS-1] = 100.;
                        }
                        else
                        {
                           lTotalTime = lEndTime - lStartTime;
                           for ( j=0; j<pLatency->lIntervals; j++ )
                           {
/* If startup time was greater than start time of interest */
                              if ( j == 0 )
                                 if ( pLatency->dStart[j] > (double)lStartTime )
                                    pLatency->dPct[NUM_INTERVALS-1] +=
                                     pLatency->dStart[j]-(double)lStartTime;
/* If last packet end time was less than end time of interest */
                              if ( j == pLatency->lIntervals-1 )
                                 if ( pLatency->dEnd[j] < (double) lEndTime )
                                    pLatency->dPct[NUM_INTERVALS-1] +=
                                     (double)lEndTime-pLatency->dEnd[j];
/* Add the time of this latency to proper index */                                     
                              for ( k=0; k<NUM_INTERVALS; k++ )
                              {
                                 if ( pLatency->iLate[j] == k )
                                 {
                                    if ( pLatency->dStart[j] >
                                        (double) lStartTime &&
                                         pLatency->dEnd[j] < (double) lEndTime )
                                       pLatency->dPct[k] +=                                       
                                        pLatency->dEnd[j]-pLatency->dStart[j]; 
                                    else if ( pLatency->dStart[j] <
                                        (double) lStartTime && pLatency->dEnd[j] 
                                         < (double) lEndTime )
                                       pLatency->dPct[k] +=
                                        pLatency->dEnd[j]-(double) lStartTime; 
                                    else if ( pLatency->dStart[j] >
                                        (double) lStartTime && pLatency->dEnd[j]
                                         > (double) lEndTime )
                                       pLatency->dPct[k] +=
                                        (double) lEndTime-pLatency->dStart[j]; 
                                    else if ( pLatency->dStart[j] <
                                        (double) lStartTime && pLatency->dEnd[j]
                                         > (double) lEndTime )
                                       pLatency->dPct[k] +=
                                        (double) lEndTime-(double) lStartTime; 
                                 }
                              }
                              if ( j > 0 )
                                 if (pLatency->dStart[j]-dLastEnd >
                                     2./Sta->dSampRate )
                                    pLatency->dPct[NUM_INTERVALS-1] +=
                                     pLatency->dStart[j]-dLastEnd;
                              dLastEnd = pLatency->dEnd[j];
                           }
                           for ( k=0; k<NUM_INTERVALS; k++ )
                              pLatency->dPct[k] = 100.*pLatency->dPct[k]/
                                                  (double) lTotalTime;                            
                        }
                     }
                     else logit( "", "File %s not opened\n", szFileName );
                  iRead = 1;
                  InvalidateRect( hwnd, NULL, TRUE );
                  }
               }
               break;

            case IDM_REFRESH:       // redraw the screen
/*               InvalidateRect( hwnd, NULL, TRUE );*/

/* One-by-one, read in the latency files */               
               for (i=0; i<Nsta; i++ )
               {
                  Sta = (STATION *) &StaArray[i];
                  pLatency = (LATENCY *) &Latency[i];
                  InitLatency( pLatency );
                  iCnt = 0;                      
                  strcpy( szFileName, Gparm.LogPath ); /* Latency file */
                  strcat( szFileName, Sta->szStation );
                  strcat( szFileName, Sta->szChannel );
                  strcat( szFileName, ".LAT" );
                  hFile = fopen( szFileName, "rb" );
                  if ( hFile != NULL )
                  {
                     for ( ;; )
                     {
                        iRC = fread( &iLateT, sizeof( int ), 1, hFile );
                        if ( iRC < 1 ) break;
                        iRC = fread( &dStart, sizeof( double ), 1, hFile );
                        if ( iRC < 1 ) break;
                        iRC = fread( &dEnd, sizeof( double ), 1, hFile );
                        if ( iRC == 1 )
                        {
/* Compare times to period of interest */                        
                           if ( dEnd   > (double) lStartTime &&
                                dStart < (double) lEndTime )/* In per.? */
                           {
                              pLatency->iLate[iCnt]  = iLateT;
                              pLatency->dStart[iCnt] = dStart;
                              pLatency->dEnd[iCnt]   = dEnd;
                              iCnt++;
                              if ( iCnt == MAX_ONOFF )
                              {
                                 logit( "", "%s %s Max latency pers. reached\n",
                                        Sta->szStation, Sta->szChannel );
                                 break;
                              }
/* If we are passed time of interest, start another file */                              
                           }
                           if ( dStart > (double) lEndTime ) break;
                        }
/* If only two are read, we must be up to present time */                           
                        else if ( iRC < 1 )
                        {
                           dEnd = Sta->dEndTime;
                           logit( "", "%s %s iRC<1\n", 
                                  Sta->szStation, Sta->szChannel );
/* Compare times to period of interest */                        
                           if ( dEnd   > (double) lStartTime &&
                                dStart < (double) lEndTime )/* In per.? */
                           {
                              pLatency->iLate[iCnt]  = iLateT;
                              pLatency->dStart[iCnt] = dStart;
                              pLatency->dEnd[iCnt]   = dEnd;
                              iCnt++;
                              if ( iCnt == MAX_ONOFF )
                              {
                                 logit( "", "%s %s Max latency pers. reached\n",
                                        Sta->szStation, Sta->szChannel );
                                 break;
                              }
/* If we are passed time of interest, start another file */                              
                           }
                           if ( dStart > (double) lEndTime ) break;
                        }
/* Otherwise, we must be at end of file (or bad file) */                           
                        else break;
                     }
                     fclose( hFile );
                     pLatency->lIntervals = (long) iCnt;
                        
/* Compute percentages of times in each latency interval */                        
                     if ( pLatency->lIntervals == 0 ) /* Always out */
                     {
                        for ( j=0; j<NUM_INTERVALS-1; j++ )
                        pLatency->dPct[j] = 0.;
                        pLatency->dPct[NUM_INTERVALS-1] = 100.;
                     }
                     else
                     {
                        lTotalTime = lEndTime - lStartTime;
                        for ( j=0; j<pLatency->lIntervals; j++ )
                        {
/* If startup time was greater than start time of interest */
                           if ( j == 0 )
                              if ( pLatency->dStart[j] > (double)lStartTime )
                                 pLatency->dPct[NUM_INTERVALS-1] +=
                                  pLatency->dStart[j]-(double)lStartTime;
/* If last packet end time was less than end time of interest */
                           if ( j == pLatency->lIntervals-1 )
                              if ( pLatency->dEnd[j] < (double) lEndTime )
                                 pLatency->dPct[NUM_INTERVALS-1] +=
                                  (double)lEndTime-pLatency->dEnd[j];
/* Add the time of this latency to proper index */                                     
                           for ( k=0; k<NUM_INTERVALS; k++ )
                           {
                              if ( pLatency->iLate[j] == k )
                              {
                                 if ( pLatency->dStart[j] >
                                     (double) lStartTime &&
                                      pLatency->dEnd[j] < (double) lEndTime )
                                    pLatency->dPct[k] +=                                       
                                     pLatency->dEnd[j]-pLatency->dStart[j]; 
                                 else if ( pLatency->dStart[j] <
                                     (double) lStartTime && pLatency->dEnd[j] 
                                      < (double) lEndTime )
                                    pLatency->dPct[k] +=
                                     pLatency->dEnd[j]-(double) lStartTime; 
                                 else if ( pLatency->dStart[j] >
                                     (double) lStartTime && pLatency->dEnd[j]
                                      > (double) lEndTime )
                                    pLatency->dPct[k] +=
                                     (double) lEndTime-pLatency->dStart[j]; 
                                 else if ( pLatency->dStart[j] <
                                     (double) lStartTime && pLatency->dEnd[j]
                                      > (double) lEndTime )
                                    pLatency->dPct[k] +=
                                     (double) lEndTime-(double) lStartTime; 
                              }
                           }
                           if ( j > 0 )
                              if (pLatency->dStart[j]-dLastEnd >
                                  2./Sta->dSampRate )
                                 pLatency->dPct[NUM_INTERVALS-1] +=
                                  pLatency->dStart[j]-dLastEnd;
                           dLastEnd = pLatency->dEnd[j];
                        }
                        for ( k=0; k<NUM_INTERVALS; k++ )
                           pLatency->dPct[k] = 100.*pLatency->dPct[k]/
                                               (double) lTotalTime;                            
                     }
                  }
                  else logit( "", "File %s not opened\n", szFileName );
               iRead = 1;
               InvalidateRect( hwnd, NULL, TRUE );
               }
            }
            break;
/*         }
         break ;
*/
/* Fill in screen display with traces, Ps, mag info, and stn names */
      case WM_PAINT:
         hdc = BeginPaint( hwnd, &ps );         /* Get device context */
         GetClientRect( hwnd, &rct );
         FillRect( hdc, &rct, (HBRUSH) GetStockObject( WHITE_BRUSH ) );
         DisplayTitles( hdc, lTitleFHt, lTitleFWd, cxScreen, cyScreen,
          Latency, iVScrollOffset, Nsta, iTitleOffset, Gparm.NumTracePerScreen);
         DisplayChannelIDLM( hdc, StaArray, Nsta, lTitleFHt,
          lTitleFWd, cxScreen, cyScreen, Gparm.NumTracePerScreen,
          iVScrollOffset, iTitleOffset );
         DisplayLatencyGraph( hdc, cxScreen, cyScreen, StaArray, 
          Latency, iVScrollOffset, iChanIDOffset, iSummaryOffset, Nsta,
          iTitleOffset, Gparm.NumTracePerScreen );
         SetScrollPos( hwnd, SB_VERT, (int) (((double) iVScrollOffset*-1.)/
                      ((cyScreen-iTitleOffset)/(double) Gparm.NumTracePerScreen*
                      (double) Nsta) * 100.), TRUE );
         EndPaint( hwnd, &ps );
         break;
	
/* Vertical scroll message */	
      case WM_VSCROLL:
         if ( LOWORD (wParam) == SB_LINEUP )
            iVScrollOffset += iScrollVertBuff;
         else if ( LOWORD (wParam) == SB_PAGEUP )
            iVScrollOffset += iScrollVertPage;
         else if ( LOWORD (wParam) == SB_LINEDOWN )
            iVScrollOffset -= iScrollVertBuff;
         else if ( LOWORD (wParam) == SB_PAGEDOWN )
            iVScrollOffset -= iScrollVertPage;
         InvalidateRect( hwnd, NULL, TRUE );
         break;

/* Close up shop and return */
      case WM_DESTROY:
         logit( "", "WM_DESTROY posted- log last latency\n" );
		 
/* Log last end time to each station's file */		 
         for ( i=0; i<Nsta; i++ )
            if ( StaArray[i].dEndTime > 0. )
            {
               Sta = (STATION *) &StaArray[i];
               strcpy( szFileName, Gparm.LogPath );
               strcat( szFileName, Sta->szStation );
               strcat( szFileName, Sta->szChannel );
               strcat( szFileName, ".LAT" );
               hFile = fopen( szFileName, "ab" );
               if ( hFile != NULL )
               {
//                  fwrite( &Sta->dEndTime, sizeof( double ), 1, hFile );
                  fwrite( &iLate[i][0], sizeof( int ), 1, hFile );
                  fwrite( &Sta->dTrigTime, sizeof( double ), 1, hFile );
                  fwrite( &Sta->dEndTime, sizeof( double ), 1, hFile );
                  fclose( hFile );
               }
            }   
         PostQuitMessage( 0 );
         break;

      default:
         return ( DefWindowProc( hwnd, msg, wParam, lParam ) );
   }
return 0;
}

      /*********************************************************
       *                     WThread()                         *
       *                                                       *
       *  This thread gets earthworm waveform messages.        *
       *                                                       *
       *********************************************************/
	   
thr_ret WThread( void *dummy )
{
   FILE         *hFile;           /* File handle */
   int           i, j;
   char          line[40];        /* Heartbeat message */
   int           lineLen;         /* Length of heartbeat message */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */
   char          szFileName[64];  /* Latency log file name */

/* Initialize latency structure
   ****************************/
   for ( i=0; i<MAX_STATIONS; i++ )
      for ( j=0; j<2; j++ )
         iLate[i][j] = 0;

/* Loop to read waveform messages
   ******************************/
   while ( tport_getflag( &Gparm.InRegion ) != TERMINATE )
   {
      long    lGapSize;         /* Number of missing samples (integer) */
      time_t  now;              /* Current time */
      int     rc;               /* Return code from tport_getmsg() */
      static  STATION *Sta;     /* Pointer to the station being processed */
	  
/* Send a heartbeat to the transport ring
   **************************************/
      time( &now );
      if ( (now - then) >= Gparm.HeartbeatInt )
      {
         then = now;
         sprintf( line, "%d %d\n", now, myPid );
         lineLen = strlen( line );
         if ( tport_putmsg( &Gparm.InRegion, &hrtlogo, lineLen, line ) !=
              PUT_OK )
         {
            logit( "et", "latency_mon: Error sending heartbeat." );
            break;
         }
      }
      
/* Get a waveform from transport region
   ************************************/
      rc = tport_getmsg( &Gparm.InRegion, &getlogoW, 1, &logo, &MsgLen,
                         WaveBuf, MAX_TRACEBUF_SIZ);

      if ( rc == GET_NONE )
      {
         sleep_ew( 100 );
         continue;
      }

      if ( rc == GET_NOTRACK )
         logit( "et", "latency_mon: Tracking error.\n");

      if ( rc == GET_MISS_LAPPED )
         logit( "et", "latency_mon: Got lapped on the ring.\n");

      if ( rc == GET_MISS_SEQGAP )
         logit( "et", "latency_mon: Gap in sequence numbers.\n");

      if ( rc == GET_MISS )
         logit( "et", "latency_mon: Missed messages.\n");

      if ( rc == GET_TOOBIG )
      {
         logit( "et", "latency_mon: Retrieved message too big (%d) for msg.\n",
                MsgLen );
         continue;
      }

/* If necessary, swap bytes in the message
   ***************************************/
      if ( WaveMsgMakeLocal( WaveHead ) < 0 )
      {
         logit( "et", "latency_mon: Unknown waveform type.\n" );
         continue;
      }
	  
/* If sample rate is 0, get out of here before it kills program
   ************************************************************/
      if ( WaveHead->samprate == 0. )
      {
         logit( "", "Sample rate=0., %s %s\n", WaveHead->sta, WaveHead->chan );
         continue;
      }

/* Look up SCN number in the station list
   **************************************/
      Sta = NULL;								  
      for ( i=0; i<Nsta; i++ )
         if ( !strcmp( WaveHead->sta,  StaArray[i].szStation ) &&
              !strcmp( WaveHead->chan, StaArray[i].szChannel ) &&
              !strcmp( WaveHead->net,  StaArray[i].szNetID ) )
         {
            Sta = (STATION *) &StaArray[i];
            break;
         }

      if ( Sta == NULL )      /* SCN not found */
         continue;
	  
/* Determine data latency (present time - mid-time of packet)
   ***********************************************************/	  
      if ( (double) now-((WaveHead->endtime+WaveHead->starttime)/2.) <= 60. )
                           iLate[i][1] = 0;
      else if ( (double) now-((WaveHead->endtime+WaveHead->starttime)/2.)
                 <= 120. ) iLate[i][1] = 1;
      else if ( (double) now-((WaveHead->endtime+WaveHead->starttime)/2.)
                 <= 180. ) iLate[i][1] = 2;
      else if ( (double) now-((WaveHead->endtime+WaveHead->starttime)/2.)
                 <= 300. ) iLate[i][1] = 3;
      else if ( (double) now-((WaveHead->endtime+WaveHead->starttime)/2.)
                 > 300. ) iLate[i][1] = 4;

/* Do this the first time we get a message with this SCN
   *****************************************************/
      if ( Sta->iFirst == 1 )
      {
         logit( "", "Init %s %s\n", Sta->szStation, Sta->szChannel );	  
         Sta->iFirst = 0;
         Sta->dEndTime = WaveHead->starttime - 1./WaveHead->samprate;
         Sta->dTrigTime = WaveHead->endtime;
         strcpy( szFileName, Gparm.LogPath );   /* Create latency file name */
         strcat( szFileName, Sta->szStation );
         strcat( szFileName, Sta->szChannel );
         strcat( szFileName, ".LAT" );
         hFile = fopen( szFileName, "ab" );
         if ( hFile != NULL )
         {
            fwrite( &iLate[i][1], sizeof( int ), 1, hFile );
            fwrite( &WaveHead->starttime, sizeof( double ), 1, hFile );
            fwrite( &WaveHead->endtime, sizeof( double ), 1, hFile );
            fclose( hFile );
         }
         iLate[i][0] = iLate[i][1];
      }
      
/* If data is not in order, throw it out
   *************************************/
      if ( Sta->dEndTime >= WaveHead->starttime )
      {
         if ( Gparm.Debug ) logit( "e", "%s out of order\n", Sta->szStation );
         Sta->iPickStatus = 0;
         continue;
      }
			
/* Compute the number of samples since the end of the previous message.
   If (lGapSize == 1), no data has been lost between messages.
   If (1 < lGapSize <= 2), go ahead anyway.
   If (lGapSize > 2), call it an outage.
   *******************************************************************/
      lGapSize = (long) (WaveHead->samprate *
                        (WaveHead->starttime-Sta->dEndTime) + 0.5);

/* Log gaps or changes in latency (update file hourly if no changes)
   *****************************************************************/
      if ( lGapSize > 2 || iLate[i][0] != iLate[i][1] ||
           Sta->dEndTime > Sta->dTrigTime+3600. )
      {
         if ( Gparm.Debug == 1 )
            logit( "", "%s %s gap or latency (%ld) \n", Sta->szStation,
                   Sta->szChannel, iLate[i][1] );	  
         strcpy( szFileName, Gparm.LogPath );   /* Create latency file name */
         strcat( szFileName, Sta->szStation );
         strcat( szFileName, Sta->szChannel );
         strcat( szFileName, ".LAT" );
         hFile = fopen( szFileName, "ab" );
         if ( hFile != NULL )
         {
            fwrite( &iLate[i][0], sizeof( int ), 1, hFile );
            fwrite( &Sta->dTrigTime, sizeof( double ), 1, hFile );
            fwrite( &Sta->dEndTime, sizeof( double ), 1, hFile );
//            fwrite( &iLate[i][1], sizeof( int ), 1, hFile );
//            fwrite( &WaveHead->starttime, sizeof( double ), 1, hFile );
            fclose( hFile );
         }
         if ( Sta->dEndTime > Sta->dTrigTime+3600. ) /* If hourly update */
            Sta->dTrigTime = Sta->dEndTime;
         else
            Sta->dTrigTime = WaveHead->starttime;
      }

/* Save time of the end of the current message and station's latency flag
   **********************************************************************/
      Sta->dEndTime = WaveHead->endtime;
      iLate[i][0] = iLate[i][1];
   }   
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
}
