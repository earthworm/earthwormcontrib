/******************************************************************
 *                     File latency_mon.h                         *
 *                                                                *
 *  Include file for latency_monitor module                       *
 ******************************************************************/

#include <trace_buf.h>
#include "\earthworm\atwc\src\libsrc\earlybirdlib.h"

/* Definitions
   ***********/
#define MAX_ONOFF         16384     /* Maximum # latency periods allowed */
#define NUM_INTERVALS         5     /* Number of latency intervals */

/* Menu options
   ************/
#define IDM_STATION_STATUS  110     /* Bring up status display */		
#define IDM_TRACEPERSCREEN  112     /* Control panel options */		
#define IDM_PRINTSUMMARY    114
#define IDM_SAVESUMMARY     116  
#define IDM_REFRESH         140	    /* Refresh screen */

/* Entry fields
   ************/
#define EF_NUMSTATODISP     3000    /* For TracesPerScreen dialog */
#define EF_DISPLAYYEAR      3002    /* For StationStatus dialog */
#define EF_DISPLAYMONTH     3003 
#define EF_DISPLAYDAY       3004
#define EF_DISPLAYHOUR      3005 
#define EF_DISPLAYTOTALTIME 3006

#define ID_NULL            -1       /* Resource file declaration */

/* User defined window messages
   ****************************/
#define WM_NEW_DATA (WM_USER + 10)
#define WM_NEW_PPICKS (WM_USER + 11)

typedef struct {
   int     iLate[MAX_ONOFF];     /* Latency indicator (see code for desc.) */
   double  dStart[MAX_ONOFF];    /* Start time (1/1/70 sec) of this period */
   double  dEnd[MAX_ONOFF];      /* End time (1/1/70 sec) of this period */
   long    lIntervals;           /* Total number of latency periods */
   double  dPct[NUM_INTERVALS];  /* % of time in latency periods */
} LATENCY;

typedef struct {
   char FindWaveFile[64];		  /* Name of FindWave-style file */
   char StaFile[64];              /* Name of file with SCN info */
   char StaDataFile[64];          /* Station information file */
   long InKey;                    /* Key to ring where waveforms live */
   int  HeartbeatInt;             /* Heartbeat interval in seconds */
   int  Debug;                    /* If 1, print debug messages */
   unsigned char MyModId;         /* Module id of this program */
   int  NumTracePerScreen;        /* # traces to show on screen (rest scroll) */
   char LogPath[64];              /* Path to put latency information */
   char PrinterPath[64];          /* Where to open printer */
   SHM_INFO InRegion;             /* Info structure for input region */
} GPARM;

typedef struct {
   unsigned char MyInstId;        /* Local installation */
   unsigned char GetThisInstId;   /* Get messages from this inst id */
   unsigned char GetThisModId;    /* Get messages from this module */
   unsigned char TypeHeartBeat;   /* Heartbeat message id */
   unsigned char TypeError;       /* Error message id */
   unsigned char TypeWaveform;    /* Earthworm waveform messages */
} EWH;

/* Function declarations for latency_mon
   *************************************/
void    DisplayChannelIDLM( HDC, STATION [], int,
                            long, long, int, int, int, int, int );
void    DisplayLatencyGraph( HDC, int, int, STATION [], LATENCY [], int, int,
                             int, int, int, int );
void    DisplayTitles( HDC, long, long, int, int, LATENCY [], int, int, 
                       int, int );
int     GetEwh( EWH * );
void    InitLatency( LATENCY * );
long WINAPI StationStatusDlgProc( HWND, UINT, UINT, long );
long WINAPI TracePerScreenDlgProc( HWND, UINT, UINT, long );
long WINAPI WndProc( HWND, UINT, UINT, long );
thr_ret WThread( void * );

int     GetConfig( char *, GPARM * );                    /* config.c */
void    LogConfig( GPARM * );

int     GetStaList( STATION **, int *, GPARM * );        /* stalist.c */
int     GetFindWaveStaList( STATION **, int *, GPARM * );
int     IsComment( char [] );
int     LoadStationData( STATION *, char * );
void    LogStaList( STATION *, int );

