/******************************************************************
 *                          File lpproc.h                         *
 *                                                                *
 *  Include file for lpproc module used at the West Coast/        *
 *  Alaska Tsunami Warning Center.                                *
 ******************************************************************/

#include <trace_buf.h>
#include "\earthworm\atwc\src\libsrc\earlybirdlib.h"

/* Definitions
   ***********/
#define RESET_INTERVAL 1800000 /* Reset options interval in milliseconds */
#define MAX_HYPO_SIZE    512   /* Maximum size of TYPE_HYPOTWC */
#define RWAVE_WINDOW    1800   /* Rayleigh wave window length (seconds) */
#define MS_BACKGROUND_TIME 120 /* Time to compute background MDF (seconds) */
#define RECENT_ALARM_TIME 900  /* # seconds late data can be and still run thru
                                  alarm */							   

/* Menu options
   ************/
#define IDM_HSCALE          110     /* Control panel options */
#define IDM_VSCALE          111
#define IDM_TRACEPERSCREEN  112		
#define IDM_FILTDISPLAY     113
#define IDM_BBDISPLAY       114                    
#define IDM_EXIT_MAIN       115
#define IDM_SCALE_8X   	    120     /* Individual station menu options */
#define IDM_SCALE_4X        121
#define IDM_SCALE_2X        122
#define IDM_SCALE_HALFX     123
#define IDM_SCALE_QUARTERX  124
#define IDM_SCALE_EIGHTHX   125
#define IDM_SCALE_DEFAULT   126
#define IDM_STADISP_ON      127
#define IDM_STADISP_OFF     128
#define IDM_REFRESH         140	    /* Refresh screen */
#define IDM_CLIPIT          150	    /* Clip-it flag */
#define IDM_DISP_HOLD       160     /* Prevent auto-scrolling */
#define IDM_STOP_LP         170     /* Stop MS computations */
#define IDM_COMPUTE_MS      180     /* Re-compute magnitudes */
#define IDM_ALARM_PAGE      190     /* Alarms sent to pager */
#define IDM_ALARM_SPEAK     191     /* Alarms sent to speaker */

/* Entry fields
   ************/
#define EF_NUMSTATODISP     3000    /* For TracesPerScreen dialog */
#define IDD_SCALE4X         3100    /* For Global Scale dialog */
#define IDD_SCALE2X         3102
#define IDD_SCALEHALF       3103
#define IDD_SCALEQUARTER    3104
#define IDD_SCALEEIGHTH     3105
#define IDD_SCALE_DEFAULT   3106

#define ID_NULL            -1       /* Resource file declaration */

/* User defined window messages
   ****************************/
#define WM_NEW_DATA (WM_USER + 10)

#define ID_TIMER2            2    /* Reset options timer */   

typedef struct {
   char StaFile[64];              /* Name of file with SCN info */
   char StaDataFile[64];          /* Station information file */
   char ATPLineupFileLP[64];      /* Optional command when used with ATPlayer */
   long InKey;                    /* Key to ring where waveforms live */
   long PKey;                     /* Key to ring where picks will live */
   long HKey;                     /* Key to ring where hypocenters will live */
   long AKey;                     /* Key to ring where alarm will live */
   int  HeartbeatInt;             /* Heartbeat interval in seconds */
   int  Debug;                    /* If 1, print debug messages */
   double HighCutFilter;          /* Bandpass high cut in hz */
   double LowCutFilter;           /* Bandpass low cut in hz */
   unsigned char MyModId;         /* Module id of this program */
   double SigNoise;               /* LP signal-to-noise ratio for MS */
   int MinutesInBuff;             /* Number of minutes data to save per trace */
   int NumTracePerScreen;         /* # traces to show on screen (rest scroll) */
   double TimePerScreen;          /* # seconds data to show on screen */
   double AlarmTimeout;           /* Timeout (seconds) alarm re-activated */
   char DummyFile[64];            /* Hypocenter parameter disk file */
   char LPRTFile[64];             /* Long period data file for LOCATE */
   char QuakeFile[64];            /* Previously located quakes from loc_wcatwc*/
   SHM_INFO InRegion;             /* Info structure for input region */
   SHM_INFO PRegion;              /* Info structure for P region */
   SHM_INFO HRegion;              /* Info structure for Hypocenter region */
   SHM_INFO ARegion;              /* Info structure for Alarm region */
} GPARM;

typedef struct {
   unsigned char MyInstId;        /* Local installation */
   unsigned char GetThisInstId;   /* Get messages from this inst id */
   unsigned char GetThisModId;    /* Get messages from this module */
   unsigned char TypeHeartBeat;   /* Heartbeat message id */
   unsigned char TypeError;       /* Error message id */
   unsigned char TypePickTWC;     /* P-pick message - TWC format */
   unsigned char TypeWaveform;    /* Earthworm waveform messages */
   unsigned char TypeHypoTWC;     /* Hypocenter message - TWC format*/
   unsigned char TypeAlarm;       /* Tsunami Ctr alarm message id */
} EWH;

/* Function declarations for lpproc
   ********************************/
thr_ret CheckRTLPThread( void * );
int     GetEwh( EWH * );
void    GetLDC( long, long *, double *, long );
int     GetMs( STATION *, RTIMES *, PPICK *, TRACE_HEADER *, HYPO *,
               GPARM *, int );
void    GetRWaveWindow( HYPO *, RTIMES *, STATION *, PPICK * );
long WINAPI GlobalScaleDlgProc( HWND, UINT, UINT, long );
void    HorizontalScroll( WORD, double *, double, double );
thr_ret HThread( void * );
void    PadBuffer( long, double, long *, long *, long );
int     PatchDummyWithLP( HYPO *, char * );
void    PutDataInBuffer( TRACE_HEADER *, long *, long *, long *, long );
void    ResetOptions( HWND, int, TRACE [], int *, int *, int *, HMENU );
long WINAPI TracePerScreenDlgProc( HWND, UINT, UINT, long );
long WINAPI WndProc( HWND, UINT, UINT, long );
thr_ret WThread( void * );

                                                         /* alarm.c */
void    SeismicAlarm( STATION *, GPARM *, EWH *, TRACE_HEADER *, long * ); 

int     GetConfig( char *, GPARM * );                    /* config.c */
void    LogConfig( GPARM * );

int     GetStaList( STATION **, int *, GPARM * );        /* stalist.c */
int     IsComment( char [] );
int     LoadStationData( STATION *, char * );
void    LogStaList( STATION *, int );

void    ReportPick( PPICK *, GPARM *, EWH * );           /* report.c */
void    ReportAlarm( STATION *, GPARM *, EWH * ); 

