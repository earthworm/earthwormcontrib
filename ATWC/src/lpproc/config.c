#include <stdio.h>
#include <string.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>
#include "lpproc.h"

#define ncommand 19          /* Number of commands in the config file */


 /***********************************************************************
  *                              GetConfig()                            *
  *             Processes command file using kom.c functions.           *
  *               Returns -1 if any errors are encountered.             *
  ***********************************************************************/

int GetConfig( char *config_file, GPARM *Gparm )
{
   char     init[ncommand];     /* Flags, one for each command */
   int      nmiss;              /* Number of commands that were missed */
   int      nfiles;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i=0; i<ncommand; i++ ) init[i] = 0;
   strcpy( Gparm->ATPLineupFileLP, "\0" );

/* Open the main configuration file
   ********************************/
   nfiles = k_open( config_file );
   if ( nfiles == 0 )
   {
      fprintf( stderr, "lpproc: Error opening configuration file <%s>\n",
               config_file );
      return -1;
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          /* Get the first token from line */

         if ( !com ) continue;             /* Ignore blank lines */
         if ( com[0] == '#' ) continue;    /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               fprintf( stderr, "lpproc: Error opening command file <%s>."
			            "\n", &com[1] );
               return -1;
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "StaFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->StaFile, str );
            init[0] = 1;
         }
		 
         else if ( k_its( "StaDataFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->StaDataFile, str );
            init[1] = 1;
         }

         else if ( k_its( "InRing" ) )
         {
            if ( str = k_str() )
            {
               if( (Gparm->InKey = GetKey(str)) == -1 )
               {
                  fprintf( stderr, "lpproc: Invalid InRing name <%s>. "
                                   "Exiting.\n", str );
                  return -1;
               }
            }
            init[2] = 1;
         }

         else if ( k_its( "PRing" ) )
         {
            if ( str = k_str() )
            {
               if ( (Gparm->PKey = GetKey(str)) == -1 )
               {
                  fprintf( stderr, "lpproc: Invalid PRing name <%s>. "
                                   "Exiting.\n", str );
                  return -1;
               }
            }
            init[3] = 1;
         }

         else if ( k_its( "HypoRing" ) )
         {
            if ( str = k_str() )
            {
               if ( (Gparm->HKey = GetKey(str)) == -1 )
               {
                  fprintf( stderr, "lpproc: Invalid HypoRing name <%s>. "
                                   "Exiting.\n", str );
                  return -1;
               }
            }
            init[4] = 1;
         }

         else if ( k_its( "AlarmRing" ) )
         {
            if ( str = k_str() )
            {
               if ( (Gparm->AKey = GetKey(str)) == -1 )
               {
                  fprintf( stderr, "lpproc: Invalid AlarmRing name <%s>. "
                                   "Exiting.\n", str );
                  return -1;
               }
            }
            init[5] = 1;
         }

         else if ( k_its( "HeartbeatInt" ) )
         {
            Gparm->HeartbeatInt = k_int();
            init[6] = 1;
         }

         else if ( k_its( "LowCutFilter" ) )
         {
            Gparm->LowCutFilter = k_val();
            init[7] = 1;
         }

         else if ( k_its( "HighCutFilter" ) )
         {
            Gparm->HighCutFilter = k_val();
            init[8] = 1;
         }

         else if ( k_its( "SigNoise" ) )
         {
            Gparm->SigNoise = k_val();
            init[9] = 1;
         }

         else if ( k_its( "Debug" ) )
         {
            Gparm->Debug = k_int();
            init[10] = 1;
         }

         else if ( k_its( "MyModId" ) )
         {
            if ( str = k_str() )
            {
               if ( GetModId(str, &Gparm->MyModId) == -1 )
               {
                  fprintf( stderr, "lpproc: Invalid MyModId <%s>.\n", str);
                  return -1;
               }
            }
            init[11] = 1;
         }

         else if ( k_its( "MinutesInBuff" ) )
         {
            Gparm->MinutesInBuff = k_int();
            init[12] = 1;
         }

         else if ( k_its( "NumTracePerScreen" ) )
         {
            Gparm->NumTracePerScreen = k_int();
            init[13] = 1;
         }

         else if ( k_its( "TimePerScreen" ) )
         {
            Gparm->TimePerScreen = k_val();
            init[14] = 1;
         }

         else if ( k_its( "AlarmTimeout" ) )
         {
            Gparm->AlarmTimeout = k_val();
            init[15] = 1;
         }
		 
         else if ( k_its( "DummyFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->DummyFile, str );
            init[16] = 1;
         }
		 
         else if ( k_its( "LPRTFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->LPRTFile, str );
            init[17] = 1;
         }
		 
         else if ( k_its( "QuakeFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->QuakeFile, str );
            init[18] = 1;
         }
	 
/* Optional command when run with ATPlayer
  ****************************************/	 
         else if ( k_its( "ATPLineupFileLP" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->ATPLineupFileLP, str );
         }

/* An unknown parameter was encountered
   ************************************/
         else
         {
            fprintf( stderr, "lpproc: <%s> unknown parameter in <%s>\n",
                     com, config_file );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            fprintf( stderr, "lpproc: Bad <%s> command in <%s>.\n", com,
                     config_file );
            return -1;
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss > 0 )
   {
      fprintf( stderr, "lpproc: ERROR, no " );
      if ( !init[0] ) fprintf( stderr, "<StaFile> " );
      if ( !init[1] ) fprintf( stderr, "<StaDataFile> " );
      if ( !init[2] ) fprintf( stderr, "<InRing> " );
      if ( !init[3] ) fprintf( stderr, "<PRing> " );
      if ( !init[4] ) fprintf( stderr, "<HypoRing> " );
      if ( !init[5] ) fprintf( stderr, "<AlarmRing> " );
      if ( !init[6] ) fprintf( stderr, "<HeartbeatInt> " );
      if ( !init[7] ) fprintf( stderr, "<LowCutFilter> " );
      if ( !init[8] ) fprintf( stderr, "<HighCutFilter> " );
      if ( !init[9] ) fprintf( stderr, "<SigNoise> " );
      if ( !init[10] ) fprintf( stderr, "<Debug> " );
      if ( !init[11] ) fprintf( stderr, "<MyModId> " );
      if ( !init[12] ) fprintf( stderr, "<MinutesInBuff> " );
      if ( !init[13] ) fprintf( stderr, "<NumTracePerScreen> " );
      if ( !init[14] ) fprintf( stderr, "<TimePerScreen> " );
      if ( !init[15] ) fprintf( stderr, "<AlarmTimeout> " );
      if ( !init[16] ) fprintf( stderr, "<DummyFile> " );
      if ( !init[17] ) fprintf( stderr, "<LPRTFile> " );
      if ( !init[18] ) fprintf( stderr, "<QuakeFile> " );
      fprintf( stderr, "command(s) in <%s>. Exiting.\n", config_file );
      return -1;
   }
   return 0;
}


 /***********************************************************************
  *                              LogConfig()                            *
  *                                                                     *
  *                   Log the configuration parameters                  *
  ***********************************************************************/

void LogConfig( GPARM *Gparm )
{
   logit( "", "\n" );
   logit( "", "StaFile:          %s\n",    Gparm->StaFile );
   logit( "", "StaDataFile:      %s\n",    Gparm->StaDataFile );
   logit( "", "InKey:            %d\n",    Gparm->InKey );
   logit( "", "PKey:             %d\n",    Gparm->PKey );
   logit( "", "HKey:             %d\n",    Gparm->HKey );
   logit( "", "AKey:             %d\n",    Gparm->AKey );
   logit( "", "HeartbeatInt:     %d\n",    Gparm->HeartbeatInt );
   logit( "", "LowCutFilter:     %lf\n",   Gparm->LowCutFilter );
   logit( "", "HighCutFilter:    %lf\n",   Gparm->HighCutFilter );
   logit( "", "SigNoise:         %lf\n",   Gparm->SigNoise );
   logit( "", "Debug:            %d\n",    Gparm->Debug );
   logit( "", "MyModId:          %u\n",    Gparm->MyModId );
   logit( "", "MinutesInBuff     %ld\n",   Gparm->MinutesInBuff );
   logit( "", "NumTracePerScreen %ld\n",   Gparm->NumTracePerScreen );
   logit( "", "AlarmTimeout      %lf\n",   Gparm->AlarmTimeout );
   logit( "", "DummyFile:        %s\n",    Gparm->DummyFile );
   logit( "", "LPRTFile:         %s\n",    Gparm->LPRTFile );
   logit( "", "QuakeFile:        %s\n",    Gparm->QuakeFile );
   if ( strlen( Gparm->ATPLineupFileLP ) > 2 )
      logit( "", "LPproc to be used with Player - File: %s\n",
       Gparm->ATPLineupFileLP);
   logit( "", "TimePerScreen     %lf\n\n", Gparm->TimePerScreen );
   return;
}
