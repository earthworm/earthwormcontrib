#include <stdio.h>
#include <string.h>            
#include <stdlib.h>
#include <earthworm.h>
#include <transport.h>
#include <trace_buf.h>
#include "lpproc.h"               

  /***************************************************************
   *                         GetStaList()                        *
   *                                                             *
   *                     Read the station list                   *
   *                                                             *
   *  Returns -1 if an error is encountered.                     *
   ***************************************************************/

int GetStaList( STATION **Sta, int *Nsta, GPARM *Gparm )
{
   double  dTemp;                 /* Values in file, not needed here */
   char    string[180];
   int     i;
   int     ndecoded;
   int     nsta;
   STATION *sta;
   STATION StaTemp;
   FILE    *fp;

/* Open the station list file
   **************************/
   if ( ( fp = fopen( Gparm->StaFile, "r") ) == NULL )
   {
      logit( "et", "lpproc: Error opening station list file <%s>.\n",
             Gparm->StaFile );
      return -1;
   }

/* Count channels in the station file.
   Ignore comment lines and lines consisting of all whitespace.
   ************************************************************/
   nsta = 0;
   while ( fgets( string, 160, fp ) != NULL )
   {
      if ( IsComment( string ) ) continue;
      ndecoded = sscanf( string,"%s %s %s %d %d %d %d %lf %lf %lf %d %lf %lf\n",
                 StaTemp.szStation, StaTemp.szChannel, StaTemp.szNetID,
                 &StaTemp.iPickStatus, &StaTemp.iFiltStatus, 
                 &StaTemp.iSignalToNoise, &StaTemp.iAlarmStatus, 
                 &StaTemp.dAlarmAmp, &StaTemp.dAlarmDur,
                 &StaTemp.dAlarmMinFreq, &StaTemp.iComputeMwp, &dTemp, &dTemp );
      if ( ndecoded < 13 )
      {
         logit( "et", "lpproc: Error decoding station file. - 1\n" );
         logit( "e", "ndecoded: %d\n", ndecoded );
         logit( "e", "Offending line:\n" );
         logit( "e", "%s\n", string );
         return -1;
      }
      nsta++;
   }
   rewind( fp );

/* Allocate the station list
   *************************/
   if ( nsta > MAX_STATIONS )
   {
      logit( "et", "Too many stations in Station file\n" );
      return -1;
   }   
//   sta = (STATION *) calloc( nsta, sizeof(STATION) );
   sta = (STATION *) calloc( MAX_STATIONS, sizeof(STATION) );
   if ( sta == NULL )
   {
      logit( "et", "lpproc: Cannot allocate the station array\n" );
      return -1;
   }

/* Read stations from the station list file into the station
   array, including parameters used by the picking algorithm
   *********************************************************/
   i = 0;
   while ( fgets( string, 160, fp ) != NULL )
   {
      if ( IsComment( string ) ) continue;
      ndecoded = sscanf( string,"%s %s %s %d %d %d %d %lf %lf %lf %d %lf %lf\n",
                 sta[i].szStation, sta[i].szChannel, sta[i].szNetID,
                 &sta[i].iPickStatus, &sta[i].iFiltStatus, 
                 &sta[i].iSignalToNoise, &sta[i].iAlarmStatus, 
                 &sta[i].dAlarmAmp, &sta[i].dAlarmDur,
                 &sta[i].dAlarmMinFreq, &sta[i].iComputeMwp, &dTemp,
                 &sta[i].dScaleFactor );
      if ( ndecoded < 13 )
      {
         logit( "et", "lpproc: Error decoding station file.\n" );
         logit( "e", "ndecoded: %d\n", ndecoded );
         logit( "e", "Offending line:\n" );
         logit( "e", "%s\n", string );
         return -1;
      }
      if ( LoadStationData( &sta[i], Gparm->StaDataFile ) == -1 )
      {
         logit( "et", "lpproc: No match for scn in station info file.\n" );
         logit( "e", "file: %d\n", Gparm->StaDataFile );
         logit( "e", "scn = %s %s %s\n", sta[i].szStation, sta[i].szChannel,
		        sta[i].szNetID );
         return -1;
      }
      InitVar( &sta[i] );	  
	  
/* Convert iAlarmStatus	to iAlarmPage and iAlarmSpeak
   **************************************************/
      sta[i].iAlarmSpeak = 0;
      sta[i].iAlarmPage = 0;
      if ( sta[i].iAlarmStatus == 1 )
      {
         sta[i].iAlarmPage = 1;	   
         sta[i].iAlarmSpeak = 0;	   
      }
      if ( sta[i].iAlarmStatus == 2 )
      {
         sta[i].iAlarmPage = 0;	   
         sta[i].iAlarmSpeak = 1;	   
         sta[i].iAlarmStatus = 1;
      }
      if ( sta[i].iAlarmStatus == 3 )
      {
         sta[i].iAlarmPage = 1;	   
         sta[i].iAlarmSpeak = 1;	   
         sta[i].iAlarmStatus = 1;
      }
      sta[i].iFirst = 1;
      i++;
   }
   fclose( fp );
   *Sta  = sta;
   *Nsta = nsta;
   return 0;
}

  /***************************************************************
   *                       LoadStationData()                     *
   *                                                             *
   *       Get data on station from information file             *
   *                                                             *
   *  Returns -1 if an error is encountered or no match is found.*
   ***************************************************************/

int LoadStationData( STATION *Sta, char *pszInfoFile )
{
   char    string[180];
   int     ndecoded;
   FILE    *fp;
   char    szChannel[TRACE_CHAN_LEN], szStation[TRACE_STA_LEN],
           szNetID[TRACE_NET_LEN];

/* Open the station data file
   **************************/
   if ( ( fp = fopen( pszInfoFile, "r") ) == NULL )
   {
      logit( "et", "lpproc: Error opening station data file <%s>.\n",
             pszInfoFile );
      return -1;
   }

/* Read station data from the station data file 
   ********************************************/
   while ( fgets( string, 160, fp ) != NULL )
   {
      ndecoded = sscanf( string, "%s %s %s", szStation, szChannel,
	             szNetID);
      if ( ndecoded < 3 )
      {
         logit( "et", "lpproc: Error decoding station data file.\n" );
         logit( "e", "ndecoded: %d\n", ndecoded );
         logit( "e", "Offending line:\n" );
         logit( "e", "%s\n", string );
         fclose( fp );
         return -1;
      }
	  
/* Compare SCNs
   ************/	  
	  if ( !strcmp (szStation, Sta->szStation ) &&
	       !strcmp (szChannel, Sta->szChannel ) &&
	       !strcmp (szNetID, Sta->szNetID ) )
      {     /* We have a match */
         ndecoded = sscanf( string, "%s %s %s %lf %lf %lf %lf %lf %lf %lf %d",
                    Sta->szStation, Sta->szChannel, Sta->szNetID,
                    &Sta->dSens, &Sta->dGainCalibration, 
                    &Sta->dLat, &Sta->dLon, &Sta->dElevation,
                    &Sta->dClipLevel, &Sta->dTimeCorrection,					
                    &Sta->iStationType);
         if ( ndecoded < 11 )
         {
            logit( "et", "lpproc: Error decoding station data file-2.\n" );
            logit( "e", "ndecoded: %d\n", ndecoded );
            logit( "e", "Offending line:\n" );
            logit( "e", "%s\n", string );
            fclose( fp );
            return -1;
         }
         fclose( fp );
         return 0;
      }
   }
   fclose( fp );
   return -1;
}

 /***********************************************************************
  *                             LogStaList()                            *
  *                                                                     *
  *                         Log the station list                        *
  ***********************************************************************/

void LogStaList( STATION *Sta, int Nsta )
{
   int i;

   logit( "", "\nStation List:\n" );
   for ( i = 0; i < Nsta; i++ )
   {
      logit( "", "%4s",     Sta[i].szStation );
      logit( "", " %3s",    Sta[i].szChannel );
      logit( "", " %2s",    Sta[i].szNetID );
      logit( "", " %1d",    Sta[i].iAlarmStatus );
      logit( "", " %lf",    Sta[i].dScaleFactor );
      logit( "", " %lE",    Sta[i].dAlarmAmp ); 
      logit( "", " %lf",    Sta[i].dAlarmDur );
      logit( "", "\n" );
   }
   logit( "", "\n" );
} 

    /*************************************************************************
     *                             IsComment()                               *
     *                                                                       *
     *  Accepts: String containing one line from a lpproc station list       *
     *  Returns: 1 if it's a comment line                                    *
     *           0 if it's not a comment line                                *
     *************************************************************************/

int IsComment( char string[] )
{
   int i;

   for ( i = 0; i < (int)strlen( string ); i++ )
   {
      char test = string[i];

      if ( test!=' ' && test!='\t' && test!='\n' )
      {
         if ( test == '#'  )
            return 1;          /* It's a comment line */
         else
            return 0;          /* It's not a comment line */
      }
   }
   return 1;                   /* It contains only whitespace */
}
