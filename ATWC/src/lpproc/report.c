  /**********************************************************************
   *                              report.c                              *
   *                                                                    *
   **********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include <transport.h>
#include "lpproc.h"

     /**************************************************************
      *                         ReportPick()                       *
      *                                                            *
      *                 Fill in TYPE_PICKTWC format                *
      **************************************************************/

void ReportPick( PPICK *P, GPARM *Gparm, EWH *Ewh )
{
   MSG_LOGO     logo;      /* Logo of message to send to the output ring */
   int          lineLen;
   char         line[512]; /* TYPE_PICKTWC format message */
   long         lTemp;

/* Create TYPE_PICKTWC message
   ***************************/
   sprintf( line,    "%ld %ld %ld %s %s %s %ld %ld %lf %c %s %lf %ld %lf %lf "
                     "%ld %lf %lf %ld %lf %lE %lf %ld\0",
            (int) Ewh->TypePickTWC, (int) Gparm->MyModId, (int) Ewh->MyInstId,
            P->szStation, P->szChannel, P->szNetID, P->lPickIndex, P->iUseMe, 
            P->dPTime, P->cFirstMotion, P->szPhase, P->dMbAmpGM, P->lMbPer,
            P->dMbTime, P->dMlAmpGM, P->lMlPer, P->dMlTime, 
            P->dMSAmpGM, P->lMSPer, P->dMSTime, 
            P->dMwpIntDisp, P->dMwpTime, 01 );
   lineLen = strlen( line ) + 1;

/* log pick
   ********/
   lTemp = (long) (P->dPTime);
   logit( "e", "%s, P=%lf, MSamp=%lf, MSPer=%ld, MSTime=%lf\n", P->szStation,
    P->dPTime, P->dMSAmpGM, P->lMSPer, P->dMSTime );

/* Send the pick to the output ring
   ********************************/
   logo.type   = Ewh->TypePickTWC;
   logo.mod    = Gparm->MyModId;
   logo.instid = Ewh->MyInstId;

   if ( tport_putmsg( &Gparm->PRegion, &logo, lineLen, line ) != PUT_OK )
      logit( "t", "lpproc: Error sending pick to output ring.\n" );
}

     /**************************************************************
      *                         ReportAlarm()                      *
      *                                                            *
      *                 Fill in TYPE_ALARM format                  *
      **************************************************************/

void ReportAlarm( STATION *Sta, GPARM *Gparm, EWH *Ewh )
{
   MSG_LOGO     logo;      /* Logo of message to send to the output ring */
   int          lineLen;
   char         line[128]; /* TYPE_PICKALARM format message */

/* Create TYPE_ALARM message (1->activate Respond Thread)
   ******************************************************/
   sprintf( line,    "%5ld %5ld %5ld %5ld %5ld     1 %s %s DIGITAL LP ALARM\0",
           (int) Ewh->TypeAlarm, (int) Gparm->MyModId, (int) Ewh->MyInstId,
            Sta->iAlarmSpeak, Sta->iAlarmPage, Sta->szStation, Sta->szChannel );
   lineLen = strlen( line );

/* log alarm
   *********/
   logit( "t", "%s\n", line );

/* Send the alarm to the output ring
   *********************************/
   logo.type   = Ewh->TypeAlarm;
   logo.mod    = Gparm->MyModId;
   logo.instid = Ewh->MyInstId;

   if ( tport_putmsg( &Gparm->ARegion, &logo, lineLen, line ) != PUT_OK )
      logit( "t", "pick_wcatwc: Error sending alarm to output ring.\n" );
}
