#
#                     Lpproc's Configuration File
#
MyModId         MOD_LPPROC     # This instance of develo
InRing        WAVE_RING_LP     # Input waveform ring (input)
PRing            PICK_RING     # P-pick ring (output)
HypoRing         HYPO_RING     # Hypocenter ring (input)
AlarmRing       ALARM_RING     # Alarm ring (output)
StaFile        "pick_wcatwc_lp.sta" # File containing stations to be processed
StaDataFile    "station.dat"   # File with information about stations
HeartbeatInt            30     # Heartbeat interval, in seconds
#    Add file created in ATPlayer if used in conjunction with player (optional)
#    Comment this line if using lpproc in real-time mode
#ATPLineupFileLP "\earthworm\run\params\ATP-LP.sta" # Station config file
#
#    LPPROC can bandpass filter data prior to display. 
#    The following values are used as filter coefficients.
#    The default values (28S, 14S) relate to the
#    response coefficients given within magcomp.  If other values are
#    used, the filter response for those values must be computed and used 
#    instead of the standard response.
#
LowCutFilter     0.0357143     # In hz, low frequency bandpass filter cut (28s)
HighCutFilter    0.0714286     # In hz, high frequency bandpass filter cut (14s)
#
MinutesInBuff          180     # Time (minutes) to allocate for each trace 
NumTracePerScreen       50     # Number of traces to show on visible screen
                               # (the rest will scroll)
TimePerScreen          720.    # Seconds of data to display on screen							   
SigNoise                 4.    # Signal:Noise to exceed for MS computations
                               #  to proceed
#
AlarmTimeout          3600.    # Time (seconds) to reset alarm after triggerring
DummyFile  "d:\seismic\erlybird\dummyX.dat"  # WC/ATWC EarlyBird dummy file
LPRTFile   "d:\seismic\locate\lprtdataX.dat" # WC/ATWC LOCATE real-time LP File
QuakeFile  "d:\seismic\analyze\oldquakeX.dat"# List of located quakes
#
Debug                    1     # If 1, print debugging message
