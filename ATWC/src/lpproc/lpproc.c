
      /*****************************************************************
       *                        lpproc.c                               *
       *                                                               *
       *  This program takes long period waveforms dropped in InRing   *
       *  and displays them on a Windows window.  The display is       *
       *  similar to a develocorder (with no film delay).  Every X     *
       *  seconds the screen scrolls to the left.  The display can be  *
       *  "held" and manually scrolled through previous times (the time*
       *  limit of saved data is specified in the .d file). An RC      *
       *  file goes along with this to describe the window menu,       *
       *  set-up, etc.                                                 *
       *                                                               *
       *  This module is strictly for use under Windows.               *
       *  It is based on the Windows NT real-time seismic processing   *
       *  system known as EarlyBird which was designed at the West     *
       *  Coast/Alaska Tsunami Warning Center.                         *
       *                                                               *
       *  Hypocenters entered to HYPO_RING are obtained by this module.*
       *  If the hypocenter has a magnitude greater than 5.0 Mb (or    *
       *  Ml), this module will compute expected Rayleigh wave         *
       *  times for the quake and will evaluate the data in the window *
       *  for Ms.  When an Ms is computed, the data is sent in the     *
       *  PICK_TWC message format to PICK_RING with P-time= 0.         *
       *                                                               *
       *  As data is input, it can be processed by an LP alarm with    *
       *  parameters specified in the .d file.  If an alarm situation  *
       *  is found, an alarm is reported to ALARM_RING.  Alarms can    *
       *  turned on or off by right clicking on the screen.            *
       *                                                               *
       *  Written by Paul Whitmore, (WC/ATWC) April, 2001              *
       *                                                               *
       *  October, 2005: Added options to run this module in           *
       *                 conjunction with ATPlayer.                    *
       *  October, 2003: If updated location is below processing       *
       *   threshold, stop LP processing.                              *
       *                                                               *
       ****************************************************************/
	   
#include <windows.h>
#include <wingdi.h>
#include <winuser.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>  
#include <swap.h>
#include "lpproc.h"

/* Global Variables 
   ****************/
double  dLastEndTime;          /* Screen end time of last display */
EWH     Ewh;                   /* Parameters from earthworm.h */
MSG_LOGO getlogoW;             /* Logo of requested waveforms */
GPARM   Gparm;                 /* Configuration file parameters */
HANDLE  hEventRTLP;            /* Event shared externally to trigger LP proc. */
HANDLE  hEventRTDoneLP;        /* Event shared externally to signal proc. done*/
HINSTANCE hInstMain;           /* Copy of main program instance (process id) */
MSG_LOGO hrtlogo;              /* Logo of outgoing heartbeats */
HYPO    HStruct;               /* Hypocenter data structure */
HWND    hwndWndProc;           /* Client window handle */
int     iAhead[MAX_STATIONS];  /* Flag indicating if data ahead of present */
int     iHold;                 /* 0->let screen auto-scroll, 1->fix screen */
int     iLPProcessing;         /* 0->no LP processing going on, 1-> LP going */
int     iNewPlayerData;        /* New set of data started from player */
int     iProcLPFromThread;     /* 1-> COMPUTE_MS called from LOCATE */
int     iScale;                /* Global scaling factor from dialogs */
MSG     msg;                   /* Windows control message variable */
mutex_t mutsem1;               /* Semaphore to protect variable adjustments */
mutex_t mutsem2;               /* Semaphore to protect display */
pid_t   myPid;                 /* Process id of this process */
int     Nsta;                  /* Number of stations to display */
PPICK   *PBuf;                 /* Pointer to P-pick buffer */
RTIMES  RTimes[MAX_STATIONS];  /* Array of Rayleigh wave arrival times */
STATION *StaArray;             /* Station data array */
char    szProcessName[] = "LP Processor";
char    szTitle[] = "LP Processor"; /* String to load in Title bar */
time_t  then;                  /* Previous heartbeat time */
char    *WaveBuf;              /* Pointer to waveform buffer */
char    *WaveBuf2;             /* Pointer to spare waveform buffer */
TRACE_HEADER *WaveHead;        /* Pointer to waveform header */
long    *WaveLong;             /* Long pointer to waveform data */
long    *WaveLongF;            /* Waveform data with DC removed */
short   *WaveShort;            /* Short pointer to waveform data */

      /***********************************************************
       *              The main program starts here.              *
       *                                                         *
       *  Argument:                                              *
       *     argv[1] = Name of configuration file                *
       ***********************************************************/

int WINAPI WinMain (HINSTANCE hInst, HINSTANCE hPreInst, 
                    LPSTR lpszCmdLine, int iCmdShow)
{
   char          configfile[64];  /* Name of config file */
   HDC           hIC;	          /* Information context to check for info */
   int           i;
   long          InBufl;          /* Maximum message size in bytes */
   char          line[40];        /* Heartbeat message */
   int           lineLen;         /* Length of heartbeat message */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */
   static unsigned tidC;          /* LP semaphore checker Thread */
   static unsigned tidH;          /* Hypocenter getter Thread */
   static unsigned tidW;          /* Waveform getter Thread */
   static WNDCLASS wc;
   
   iNewPlayerData = 0;   
   hInstMain = hInst;
   dLastEndTime = 0.;
   iHold = 0;                     /* Let screen auto scroll with new data */
   iLPProcessing = 0;
   iProcLPFromThread = 0;
   for ( i=0; i<MAX_STATIONS; i++ ) iAhead[i] = 0;
   
/* Get config file name (format "lpproc lpproc.D")
   ***********************************************************/
   if ( strlen( lpszCmdLine ) <= 0 )
   {
      fprintf( stderr, "Need configfile in start line.\n" );
      return -1;
   }
   strcpy( configfile, lpszCmdLine );

/* Get parameters from the configuration files
   *******************************************/
   if ( GetConfig( configfile, &Gparm ) == -1 )
   {
      fprintf( stderr, "GetConfig() failed. file %s.\n", configfile );
      return -1;
   }

/* Look up info in the earthworm.h tables
   **************************************/
   if ( GetEwh( &Ewh ) < 0 )
   {
      fprintf( stderr, "lpproc: GetEwh() failed. Exiting.\n" );
      return -1;
   }

/* Specify logos of incoming waveforms and outgoing heartbeats
   ***********************************************************/
   getlogoW.instid = Ewh.GetThisInstId;
   getlogoW.mod    = Ewh.GetThisModId;
   getlogoW.type   = Ewh.TypeWaveform;

   hrtlogo.instid = Ewh.MyInstId;
   hrtlogo.mod    = Gparm.MyModId;
   hrtlogo.type   = Ewh.TypeHeartBeat;

/* Initialize name of log-file & open it
   *************************************/
   logit_init( configfile, Gparm.MyModId, 256, 1 );

/* Get our own pid for restart purposes
   ************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "lpproc: Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Log the configuration parameters
   ********************************/
   LogConfig( &Gparm );

/* Allocate the waveform buffers
   *****************************/
   InBufl = MAX_TRACEBUF_SIZ*2;
   WaveBuf = (char *) malloc( (size_t) InBufl );
   if ( WaveBuf == NULL )
   {
      logit( "et", "lpproc: Cannot allocate waveform buffer\n" );
      return -1;
   }
   WaveBuf2 = (char *) malloc( (size_t) InBufl );
   if ( WaveBuf2 == NULL )
   {
      logit( "et", "lpproc: Cannot allocate waveform buffer2\n" );
      return -1;
   }

/* Point to header and data portions of waveform message
   *****************************************************/
   WaveHead  = (TRACE_HEADER *) WaveBuf;
   WaveLong  = (long *) (WaveBuf + sizeof (TRACE_HEADER));
   WaveLongF  = (long *) (WaveBuf2 + sizeof (TRACE_HEADER));
   WaveShort = (short *) (WaveBuf + sizeof (TRACE_HEADER));

/* Read the station list and return the number of stations found.
   Allocate the station list array.
   *************************************************************/
   if ( GetStaList( &StaArray, &Nsta, &Gparm ) == -1 )
   {
      fprintf( stderr, "lpproc: GetStaList() failed. Exiting.\n" );
	  free( WaveBuf );
      free( WaveBuf2 );
      return -1;
   }
   if ( Nsta == 0 )
   {
      logit( "et", "lpproc: Empty station list. Exiting." );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      return -1;
   }
   logit( "t", "lpproc: Displaying %d stations.\n", Nsta );

/* Log the station list
   ********************/
   LogStaList( StaArray, Nsta );
   
/* Create event to share with an external (non-earthworm) program; this event
   triggers LP processing in LPPROC
   **************************************************************************/
   hEventRTLP = CreateEvent( NULL,      /* Default security */
                             FALSE,     /* Auto-reset event */
                             FALSE,     /* Initially not set */
                             "RTLP" );  /* Share with LOCATE */
   if ( hEventRTLP == NULL )            /* If event not created */
   {
      logit( "t", "failed to create RTLP event" );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      return -1;
   }
   
/* Create another event to share with an external (non-earthworm) program;
   this event signals the external program that the LP data file has been
   filled 
   ***********************************************************************/
   hEventRTDoneLP = CreateEvent( NULL,        /* Default security */
                                 FALSE,       /* Auto-reset event */
                                 FALSE,       /* Initially not set */
                                 "RTDoneLP" );/* Share with LOCATE */
   if ( hEventRTDoneLP == NULL )              /* If event not created */
   {
      logit( "t", "failed to create RTDoneLP event" );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      CloseHandle( hEventRTLP );
      return -1;
   }

/* If this is the first instance of this program, init window stuff and
   register window (it always is)
   ********************************************************************/
   if ( !hPreInst )
   {  /* Force PAINT when sized and give double click notification */
      wc.style         = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
      wc.lpfnWndProc   = WndProc;         /* Control Window process */
      wc.cbClsExtra    = 0;
      wc.cbWndExtra    = 0;
      wc.hInstance     = hInst;           /* Process id */
      wc.hIcon         = LoadIcon( hInst, "lpproc" );    /* System app icon */
      wc.hCursor       = LoadCursor( NULL, IDC_ARROW );          /* Pointer */
      wc.hbrBackground = (HBRUSH) GetStockObject( WHITE_BRUSH ); /* White */
      wc.lpszMenuName  = "lpproc_menu";   /* Relates to .RC file */
      wc.lpszClassName = szProcessName;
      if ( !RegisterClass( &wc ) )        /* Window not registered */
      {
         logit( "t", "RegisterClass failed\n" );
         free( WaveBuf );
         free( WaveBuf2 );
         free( StaArray );
         CloseHandle( hEventRTLP );
         CloseHandle( hEventRTDoneLP );
         return -1;
      }
   }

/* Create the window
   *****************/
   hIC = CreateIC ("DISPLAY", NULL, NULL, NULL);
   if (hIC == NULL)
   {
      logit( "t", "CreateIC error\n" );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      CloseHandle( hEventRTLP );
      CloseHandle( hEventRTDoneLP );
      return -1;
   }
   hwndWndProc = CreateWindow(
                 szProcessName,          /* Process name */
                 szProcessName,          /* Initial title bar caption */
                 WS_OVERLAPPEDWINDOW | WS_HSCROLL | WS_VSCROLL,
//                 CW_USEDEFAULT,          /* top left x starting location */
//                 CW_USEDEFAULT,          /* top left y starting location */
//                 CW_USEDEFAULT,          /* Initial screen width in pixels */
//                 CW_USEDEFAULT,          /* Initial screen height in pixels */
                 GetDeviceCaps (hIC, HORZRES)/4, /* top left x starting loc. */
                 0,                      /* top left y starting location */
                 GetDeviceCaps (hIC, HORZRES)/4, /* Window width in pixels */
                 GetDeviceCaps (hIC, VERTRES)/2, /* Window height in pixels */
                 NULL,                   /* No parent window */
                 NULL,                   /* Use standard system menu */
                 hInst,                  /* Process id */
                 NULL );                 /* No extra data to pass in */
   if ( hwndWndProc == NULL )            /* Window not created */
   {
      logit( "t", "CreateWindow failed\n" );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      CloseHandle( hEventRTLP );
      CloseHandle( hEventRTDoneLP );
      return 0;
   }
   
   ShowWindow( hwndWndProc, iCmdShow );  /* Show the Window */
   UpdateWindow( hwndWndProc );          /* Force an initial PAINT call */

/* Attach to existing transport rings
   **********************************/
   tport_attach( &Gparm.InRegion,  Gparm.InKey );
   tport_attach( &Gparm.PRegion,   Gparm.PKey );
   tport_attach( &Gparm.HRegion,   Gparm.HKey );
   tport_attach( &Gparm.ARegion,   Gparm.AKey );

/* Flush the input waveform ring
   *****************************/
   while ( tport_getmsg( &Gparm.InRegion, &getlogoW, 1, &logo, &MsgLen,
                         WaveBuf, MAX_TRACEBUF_SIZ ) != GET_NONE );

/* Allocate and init the P-pick buffer
   ***********************************/
   InBufl = sizeof( PPICK ) * Nsta; 
   PBuf = (PPICK *) malloc( (size_t) InBufl );
   if ( PBuf == NULL )
   {
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.PRegion );
      tport_detach( &Gparm.HRegion );
      tport_detach( &Gparm.ARegion );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      CloseHandle( hEventRTLP );
      CloseHandle( hEventRTDoneLP );
      logit( "et", "lpproc: Cannot allocate p-pick buffer\n");
      return -1;
   }
   for ( i=0; i<Nsta; i++ ) InitP( &PBuf[i] );
   for ( i=0; i<Nsta; i++ ) PBuf[i].iUseMe = 0;

/* Send 1st heartbeat to the transport ring
   ****************************************/
   time( &then );
   sprintf( line, "%d %d\n", then, myPid );
   lineLen = strlen( line );
   if ( tport_putmsg( &Gparm.InRegion, &hrtlogo, lineLen, line ) != PUT_OK )
   {
      logit( "et", "lpproc: Error sending 1st heartbeat. Exiting." );
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.PRegion );
      tport_detach( &Gparm.HRegion );
      tport_detach( &Gparm.ARegion );
      free( PBuf );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      CloseHandle( hEventRTLP );
      CloseHandle( hEventRTDoneLP );
      return 0;
   }
   
/* Create a mutex for protecting adjustments of variables
   ******************************************************/
   CreateSpecificMutex( &mutsem1 );
   
/* Create a mutex for protecting screen display
   ********************************************/
   CreateSpecificMutex( &mutsem2 );

/* Start the LP compute semaphore checker thread
   *********************************************/
   if ( StartThread( CheckRTLPThread, 8192, &tidC ) == -1 )
   {
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.PRegion );
      tport_detach( &Gparm.HRegion );
      tport_detach( &Gparm.ARegion );
      free( PBuf );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      CloseHandle( hEventRTLP );
      CloseHandle( hEventRTDoneLP );
      logit( "et", "Error starting H thread; exiting!\n" );
      return -1;
   }

/* Start the Hypocenter getter thread
   **********************************/
   if ( StartThread( HThread, 8192, &tidH ) == -1 )
   {
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.PRegion );
      tport_detach( &Gparm.HRegion );
      tport_detach( &Gparm.ARegion );
      free( PBuf );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      CloseHandle( hEventRTLP );
      CloseHandle( hEventRTDoneLP );
      logit( "et", "Error starting H thread; exiting!\n" );
      return -1;
   }

/* Start the waveform getter tread
   *******************************/
   if ( StartThread( WThread, 8192, &tidW ) == -1 )
   {
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.PRegion );
      tport_detach( &Gparm.HRegion );
      tport_detach( &Gparm.ARegion );
      free( PBuf );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      CloseHandle( hEventRTLP );
      CloseHandle( hEventRTDoneLP );
      logit( "et", "Error starting W thread; exiting!\n" );
      return -1;
   }
	 
/* Start a timer so that display options are automatically reset 
   *************************************************************/
   SetTimer( hwndWndProc, ID_TIMER2, RESET_INTERVAL, NULL );
   
/* Main windows loop
   *****************/      
   while ( GetMessage( &msg, NULL, 0, 0 ) )
   {
      TranslateMessage( &msg );
      DispatchMessage( &msg );
   }

/* Detach from the ring buffers
   ****************************/
   tport_detach( &Gparm.InRegion );
   tport_detach( &Gparm.PRegion );
   tport_detach( &Gparm.HRegion );
   tport_detach( &Gparm.ARegion );
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
   free( WaveBuf );
   free( WaveBuf2 );
   free( PBuf );
   CloseHandle( hEventRTLP );
   CloseHandle( hEventRTDoneLP );
   for ( i=0; i<Nsta; i++ ) 
   {
      free( StaArray[i].plRawCircBuff );
      free( StaArray[i].plFiltCircBuff );
   }
   free( StaArray );
   logit( "t", "Termination requested. Exiting.\n" );
   return (msg.wParam);
}

      /*********************************************************
       *             CheckRTLPThread()                         *
       *                                                       *
       *  This thread checks for events set in LOCATE to force *
       *  a new ProcessLP (and stops any existing processes).  *
       *                                                       *
       *  Its prupose is to let an external program trigger    *
       *  LP processing.  Windows specific semaphore commands  *
       *  are used throughout as named events are not supported*
       *  in sema_ew.c.                                        *
       *                                                       *
       *********************************************************/
	   
thr_ret CheckRTLPThread( void *dummy )
{
   sleep_ew( 10000 );      /* Let things get going */

/* Loop forever, waiting to send a message to Process LP */
   for( ;; )
   {
      WaitForSingleObject( hEventRTLP, INFINITE );
      iProcLPFromThread = 1;
      PostMessage( hwndWndProc, WM_COMMAND, IDM_COMPUTE_MS, 0 );
   }
}

      /*******************************************************
       *                      GetEwh()                       *
       *                                                     *
       *      Get parameters from the earthworm.d file.      *
       *******************************************************/

int GetEwh( EWH *Ewh )
{
   if ( GetLocalInst( &Ewh->MyInstId ) != 0 )
   {
      fprintf( stderr, "lpproc: Error getting MyInstId.\n" );
      return -1;
   }
   if ( GetInst( "INST_WILDCARD", &Ewh->GetThisInstId ) != 0 )
   {
      fprintf( stderr, "lpproc: Error getting GetThisInstId.\n" );
      return -2;
   }
   if ( GetModId( "MOD_WILDCARD", &Ewh->GetThisModId ) != 0 )
   {
      fprintf( stderr, "lpproc: Error getting GetThisModId.\n" );
      return -3;
   }
   if ( GetType( "TYPE_HEARTBEAT", &Ewh->TypeHeartBeat ) != 0 )
   {
      fprintf( stderr, "lpproc: Error getting TypeHeartbeat.\n" );
      return -4;
   }
   if ( GetType( "TYPE_ERROR", &Ewh->TypeError ) != 0 )
   {
      fprintf( stderr, "lpproc: Error getting TypeError.\n" );
      return -5;
   }
   if ( GetType( "TYPE_PICKTWC", &Ewh->TypePickTWC ) != 0 )
   {
      fprintf( stderr, "lpproc: Error getting TYPE_PICKTWC.\n" );
      return -7;
   }
   if ( GetType( "TYPE_TRACEBUF", &Ewh->TypeWaveform ) != 0 )
   {
      fprintf( stderr, "lpproc: Error getting TYPE_TRACEBUF.\n" );
      return -8;
   }
   if ( GetType( "TYPE_HYPOTWC", &Ewh->TypeHypoTWC ) != 0 )
   {
      fprintf( stderr, "lpproc: Error getting TYPE_HYPOTWC.\n" );
      return -9;
   }
   if ( GetType( "TYPE_ALARM", &Ewh->TypeAlarm ) != 0 )
   {
      fprintf( stderr, "lpproc: Error getting TYPE_ALARM.\n" );
      return -10;
   }
   return 0;
}

  /******************************************************************
   *                           GetLDC()                             *
   *                                                                *
   *  Determine and update moving averages of signal value          *
   *  (called LDC here).                                            *
   *                                                                *
   *  Arguments:                                                    *
   *    lNSamps     Number of samples in this packet                *
   *    WaveLong    Pointer to data array                           *
   *    pdLDC       Pointer to long term DC average                 *
   *    lFiltSamp   # samples through filter                        *
   *                                                                *
   ******************************************************************/

void GetLDC( long lNSamps, long *WaveLong, double *pdLDC, long lFiltSamp )
{
   double  dSumLDC;     /* Summation of all values in this packet */
   int     i;
   
   dSumLDC = 0.;
   
/* Sum up total for this packet */
   for ( i=0; i<lNSamps; i++ )
   {
      if ( lFiltSamp == 0 ) dSumLDC  += ((double) WaveLong[i]);
      else                  dSumLDC  += ((double) WaveLong[i] - *pdLDC);
   }
	  
/* Compute new LTAs (first time through, just take average.  Adjust average
   from there */
   if ( lFiltSamp == 0 ) *pdLDC =  (dSumLDC/(double) lNSamps);
   else                  *pdLDC += (0.5 * dSumLDC/(double) lNSamps);
}

 /***********************************************************************
  *                             GetMs()                                 *
  *                                                                     *
  *      Loop through data to get MS parameters.                        *
  *                                                                     *
  *      NOTE: Only call this function if some R-window within buffer   *                                                               *
  *                                                                     *
  *  Arguments:                                                         *
  *     Sta              Pointer to station being processed             *
  *     RTimes           Rayleigh wave window structure                 *
  *     pPBuf            P-pick buffer                                  *
  *     pWaveHd          Tracebuf header pointer                        *
  *     pHStruct         Hypocenter data structure                      *
  *     Gparm            Configuration parameter structure              *
  *     iInit            0=just process last packet, 1=start anew       *
  *                                                                     *
  *  Returns:                                                           *
  *     1 if new highest amplitude found, 0 otherwise                   *                                                                   *
  *                                                                     *
  ***********************************************************************/
  
int GetMs( STATION *Sta, RTIMES *RTimes, PPICK *pPBuf, TRACE_HEADER *pWaveHd,
           HYPO *pHStruct, GPARM *Gparm, int iInit )
{
   double  dOldestTime;   /* Oldest time (1/1/70 seconds) in buffer */
   double  dPer;          /* Period in seconds */
   double  dTime;         /* 1/1/70 time at i=0 */
   int     iReturn;       /* 0=no new mag params, 1=new mag params */
   long    i, lIndex;
   long    lAmp2;         /* MS Amplitude in counts */
   long    lBIndex;       /* Buffer index to start MDF evaluation */
   long    lBNum;         /* Number of samples to evaluate for MDF */
   long    lNum;          /* Number of samples to evaluate */
   long    lRIndex;       /* Buffer index of R-wave start */

   iReturn = 0;
   dOldestTime = Sta->dEndTime -
    ((double) Sta->lRawCircSize/Sta->dSampRate) + 1./Sta->dSampRate;
   
/* Is this a calibrated station? */
   if ( Sta->dSens <= 0. ) return 0;

/* Initialize some things */
   if ( iInit == 1 )          /* Go through buffer to catch up */
   {            
/* What is starting buffer index of the R-wave start time? */   
      if ( dOldestTime < RTimes->dRStartTime ) 
         lRIndex = Sta->lSampIndexF - (long) ((Sta->dEndTime-
                   RTimes->dRStartTime)*Sta->dSampRate) - 1;
      else 
         lRIndex = Sta->lSampIndexF - Sta->lRawCircSize + 1;
      while ( lRIndex <  0 )                 lRIndex += Sta->lRawCircSize;
      while ( lRIndex >= Sta->lRawCircSize ) lRIndex -= Sta->lRawCircSize; 
	  
      Sta->dAveLDCRawOrig = Sta->dAveLDC;  /* Here RawOrig = filtered */
      if ( lRIndex > 0 ) Sta->lSampOld = Sta->plFiltCircBuff[lRIndex-1] -
                                         (long) Sta->dAveLDCRawOrig;
      else Sta->lSampOld = Sta->plFiltCircBuff[Sta->lRawCircSize-1] -
                                         (long) Sta->dAveLDCRawOrig;
      Sta->lSampsPerCyc = 0;
      Sta->lMagAmp = 0;
      Sta->lPhase1 = 0;        /* Here, lPhase1 is the previous amp */
      Sta->lPhase2 = 0;        /* Here, lPhase2 is the highest amp */
      Sta->lPer = 0;
   
/* How many points to evaluate? */   
      lNum = (long) ((RTimes->dREndTime-RTimes->dRStartTime) * Sta->dSampRate);
      dTime = RTimes->dRStartTime;
      if ( dOldestTime > RTimes->dRStartTime )
      {
         lNum -= (long) ((dOldestTime-RTimes->dRStartTime) * Sta->dSampRate);
         dTime = dOldestTime;
      }
      if ( Sta->dEndTime < RTimes->dREndTime )
         lNum -= (long) ((RTimes->dREndTime-Sta->dEndTime) * Sta->dSampRate);
	  
/* Compute the background MDF; NOTE: The lBIndex could be in newer data than
   expected.  If the buffer is big enough (several hours) this shouldn't
   be a problem. */   
      lBNum = (long) ((double) MS_BACKGROUND_TIME * Sta->dSampRate);
      lBIndex = lRIndex - lBNum;
      while ( lBIndex < 0 ) lBIndex += Sta->lRawCircSize; 
      GetMDFFilt( lBIndex, lBNum, Sta );
   }
   else                   /* Just analyze last packet */
   {
      lRIndex = Sta->lEndData;
      lNum = pWaveHd->nsamp;
      dTime = Sta->dEndTime - (double) lNum*Sta->dSampRate;
   }

/* Set iPickStatus so that MS will be computed on new packets, if needed */
   if ( Sta->dEndTime < RTimes->dREndTime ) Sta->iPickStatus = 1;
      
/* Loop through data to get magnitude */
   for ( i=0; i<lNum; i++ )
   {
      lIndex = i + lRIndex;
      if ( lIndex >= Sta->lRawCircSize ) lIndex -= Sta->lRawCircSize; 
      Sta->lSampNew = Sta->plFiltCircBuff[lIndex] - (long) Sta->dAveLDCRawOrig;
      
/* Check for cycle changes (at the zero crossings) */
      if ( (Sta->lSampNew <  0 && Sta->lSampOld <  0) || 
           (Sta->lSampNew >= 0 && Sta->lSampOld >= 0) )
      {                               /* No zero crossing */
         Sta->lSampsPerCyc += 1;
         if ( labs( Sta->lSampNew ) > Sta->lMagAmp ) 
             Sta->lMagAmp = labs( Sta->lSampNew );
      }
      else                            /* Zero has been crossed */
      {
         Sta->lSampsPerCyc += 1;
         dPer = (double) (Sta->lSampsPerCyc+Sta->lPer) / Sta->dSampRate;
         if ( dPer <= 1.0  ) dPer = 1.0;      /* Filter response constraint */
         if ( dPer >= 29.0 ) dPer = 29.0;
         Sta->lPer = Sta->lSampsPerCyc;
         Sta->lSampsPerCyc = 0;
         lAmp2 = Sta->lMagAmp + Sta->lPhase1;
		 
/* Should we report this cycle to PICK_RING? */		 
         if ( lAmp2 > Sta->lPhase2 &&           /* Bigger than previous? */
              dPer >= 17. && dPer <= 23. &&     /* Right Period Range? */
                                                /* High enough S:N? */
              lAmp2 > (long) (Sta->dAveMDF*Gparm->SigNoise) &&
              pPBuf->iUseMe >= 0 )              /* Manually removed? */
         {       /* Convert to ground motion and save in PBuf */
            pPBuf->lPickIndex = pHStruct->iQuakeID;
            strcpy( pPBuf->szStation, Sta->szStation );
            strcpy( pPBuf->szChannel, Sta->szChannel );
            strcpy( pPBuf->szNetID, Sta->szNetID );
            strcpy( pPBuf->szPhase, "R" );
            pPBuf->cFirstMotion = '?';
            pPBuf->dPTime = 0.;
            pPBuf->iUseMe = 1;
            pPBuf->lMSPer = (long) (dPer + 0.5);
            if ( lAmp2 > Sta->dClipLevel ) pPBuf->iMSClip = 1;
            pPBuf->dMSAmpGM = MsGroundMotion( Sta, pPBuf->lMSPer, lAmp2 );
            pPBuf->dSens = Sta->dSens;	    
            pPBuf->dMSTime = dTime + (double) i*Sta->dSampRate;	    
            pPBuf->dMSMag = ComputeMSMag( pPBuf->szChannel, 0.,
             pPBuf->dMSAmpGM, (double) pPBuf->lMSPer, pPBuf->dDelta );
            Sta->lPhase2 = lAmp2;
            iReturn = 1;
         }
         Sta->lPhase1 = Sta->lMagAmp;     /* Reset for next 1/2 cycle */
         Sta->lMagAmp = Sta->lSampNew;
      }  
      Sta->lSampOld = Sta->lSampNew;
   }
   return iReturn;
}

  /******************************************************************
   *                      GetRWaveWindow()                          *
   *                                                                *
   *  Compute Rayleigh wave travel time and window start and end    *
   *  times.   These times are in 1/1/70 seconds.                   *
   *                                                                *
   *  Arguments:                                                    *
   *    pHypo       Hypocenter data structure                       *
   *    RTimes      Rayleigh wave window sdtructure                 *
   *    Sta         Pointer to station being processed              *
   *    pPBuf       Pointer to pick buffer                          *
   *                                                                *
   ******************************************************************/
   
void GetRWaveWindow (HYPO *pHypo, RTIMES *RTimes, STATION *Sta, PPICK *pPBuf )
{
   AZIDELT az;              /* Epicentral distance, azimuth */
   double  dDist, dOver;    /* Distance interpolation values */
                            /* Rayleigh wave travel time in seconds */
   int     iRTravTime[181] = {0,32,63,95,127,159,190,222,254,285,317,// 0-10
        349,381,412,441,475,507,539,571,603,634,                     // 11-20
        661,688,715,742,769,796,823,850,877,900,                     // 21-30
        930,960,990,1020,1050,1080,1110,1140,1170,1200,              // 31-40
        1230,1260,1290,1320,1350,1380,1410,1440,1470,1500,           // 41-50
        1530,1560,1590,1620,1650,1680,1710,1740,1770,1800,           // 51-60
        1819,1838,1858,1877,1896,1915,1934,1953,1972,1992,           // 61-70
        2021,2049,2078,2106,2135,2163,2192,2220,2248,2277,           // 71-80
        2305,2334,2362,2391,2419,2448,2476,2505,2533,2562,           // 81-90
        2590,2618,2648,2675,2704,2732,2761,2789,2818,2846,           // 91-100
        2875,2903,2932,2960,2988,3017,3045,3074,3102,3131,           // 101-110
        3159,3188,3216,3245,3273,3302,3330,3358,3387,3415,           // 111-120
        3444,3472,3501,3529,3558,3586,3615,3643,3672,3700,           // 121-130
        3728,3757,3785,3814,3842,3871,3899,3928,3956,3985,           // 131-140
        4013,4042,4070,4098,4127,4155,4184,4212,4241,4269,           // 141-150
        4298,4326,4355,4383,4412,4440,4468,4497,4525,4554,           // 151-160
        4582,4611,4639,4668,4696,4725,4753,4782,4810,4838,           // 161-170
        4867,4895,4924,4952,4981,5009,5038,5066,5095,5123 };         // 171-180
   LATLON  ll;              /* Station location */
        
/* Get epicentral distance */	
   ll.dLat = Sta->dLat;
   ll.dLon = Sta->dLon;
   GeoCent( &ll );
   GetLatLonTrig( &ll );
   az = GetDistanceAz( (LATLON *) pHypo, &ll );

/* Save distance for later use */
   pPBuf->dDelta = az.dDelta;
   
/* Get Rayleigh wave travel time in seconds */
   dDist = az.dDelta;
   dOver = dDist - floor( dDist );
   RTimes->dRTravTime = (double) iRTravTime[(int) dDist] +
    ((double) (iRTravTime[(int) dDist+1] - iRTravTime[(int) dDist])*dOver);
	
/* Find beginning and end of Rayleigh window for this station (1/1/70 secs) */
   RTimes->dRStartTime = pHypo->dOriginTime + RTimes->dRTravTime - 10.;
   RTimes->dREndTime = RTimes->dRStartTime + RWAVE_WINDOW;
}
 		 
      /***********************************************************
       *                   GlobalScaleDlgProc()                  *
       *                                                         *
       * This procedure is used to change the overall scale in   *
       * both the vertical and horizontal directions.            *
       *                                                         *
       ***********************************************************/
       
long WINAPI GlobalScaleDlgProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   switch ( msg )
   {
      case WM_INITDIALOG:
         SetFocus( hwnd );
         break;

      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {
            case IDD_SCALE4X:        /* The following scales end the */
               iScale = 4;           /*  dialog when chosen          */
               EndDialog( hwnd, IDOK );
               break;
	       
            case IDD_SCALE2X:
               iScale = 2;
               EndDialog( hwnd, IDOK );
               break;
	       
            case IDD_SCALEHALF:
               iScale = -2;
               EndDialog( hwnd, IDOK );
               break;
	       
            case IDD_SCALEQUARTER:
               iScale = -4;
               EndDialog( hwnd, IDOK );
               break;
		
            case IDD_SCALE_DEFAULT:	// Re-set to default
               iScale = 0;
               EndDialog( hwnd, IDOK );
               break;
        
            case IDCANCEL:        /* Escape and make no changes */
               EndDialog( hwnd, IDCANCEL );
               break;
         }
   }
   return 0;
}

   /*******************************************************************
    *                      HorizontalScroll()                         *
    *                                                                 *
    * This function handles scrolling messages.  How much the screen  *
    * scrolls depends on the amount message.                          *
    *                                                                 *
    *  Arguments:                                                     *
    *    wScroll       Amount to scroll screen                        *
    *    pdLastEndTime New Time at right side of screen (1/1/70 sec)  *
    *    dScrollHBit   Small scrolling amount                         *
    *    dScrollHPage  Large scrolling amount                         *
    *                                                                 *
    *******************************************************************/
	
void HorizontalScroll( WORD wScroll, double *pdLastEndTime,
                       double dScrollHBit, double dScrollHPage )
{
   switch ( wScroll )         /* Respond to scrolling message */
   {
      case SB_LINELEFT:
         *pdLastEndTime += -dScrollHBit;
         break;
		 
      case SB_LINERIGHT:
         *pdLastEndTime += dScrollHBit;
         break;
		
      case SB_PAGELEFT:
         *pdLastEndTime += -dScrollHPage;
         break;
		
      case SB_PAGERIGHT:
         *pdLastEndTime += dScrollHPage;
         break;
		
      default:
         *pdLastEndTime += 0.;
         break;
    }
}

  /******************************************************************
   *                        PadBuffer()                             *
   *                                                                *
   *  Fill in gaps in the data with DC offset.                      *
   *  Update last data value index.                                 *
   *                                                                *
   *  Arguments:                                                    *
   *    lGapSize    Number of values to pad (+1)                    *
   *    dLDC        DC average                                      *
   *    plBuffCtr   Index tracker in main buffer                    *
   *    plBuff      Pointer to main circular buffer                 *
   *    lBuffSize   # samples in main circular buffer               *
   *                                                                *
   ******************************************************************/

void PadBuffer( long lGapSize, double dLDC, long *plBuffCtr, 
                long *plBuff, long lBuffSize )
{
   long    i;
   
/* Pad the data buffer over the gap interval */
   for ( i=0; i<lGapSize-1; i++ )
   {
      plBuff[*plBuffCtr] = (long) dLDC;
      *plBuffCtr += 1;
      if ( *plBuffCtr == lBuffSize ) *plBuffCtr = 0;
   }	  
}

      /*********************************************************
       *                     HThread()                         *
       *                                                       *
       *  This thread gets messages from the hypocenter ring.  *
       *                                                       *
       *********************************************************/
	   
thr_ret HThread( void *dummy )
{
   double        dAvgMS;          /* Average MS for Title */
   double        dOldestTime;     /* 1/1/70 time of oldest data in buffer */
   MSG_LOGO      getlogoH;        /* Logo of requested picks */
   char          HIn[MAX_HYPO_SIZE];/* Pointer to hypocenter from ring */
   HYPO          HStructT;        /* Temporary Hypocenter data structure */
   HYPO          HypoD;           /* Hypocenter structure from dummy file */
   int           i;
   int           iNumMS;          /* Number stns used in average MS (Title) */
   static int    iQuakeID;        /* ID of last quake (processed) */
   static int    iVersion;        /* Version of last quake (processed) */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */
   char          szAvgMS[8], szNumMS[4], szQuake[8]; /* Used in Title */

   iVersion = -1;
   iQuakeID = -1;
/* Set up logos for Hypo ring 
   **************************/
   getlogoH.instid = Ewh.GetThisInstId;
   getlogoH.mod    = Ewh.GetThisModId;
   getlogoH.type   = Ewh.TypeHypoTWC;

/* Flush the input ring
   ********************/
   while ( tport_getmsg( &Gparm.HRegion, &getlogoH, 1, &logo, &MsgLen,
                         HIn, MAX_HYPO_SIZE) != GET_NONE );
						 
/* Loop to read hypocenter messages and load buffer
   ************************************************/
   while ( tport_getflag( &Gparm.HRegion ) != TERMINATE )
   {
      int     rc;               /* Return code from tport_getmsg() */

/* Get a hypocenter from transport region
   **************************************/
      rc = tport_getmsg( &Gparm.HRegion, &getlogoH, 1, &logo, &MsgLen,
                         HIn, MAX_HYPO_SIZE);

      if ( rc == GET_NONE )
      {
         sleep_ew( 1000 );
         continue;
      }

      if ( rc == GET_NOTRACK )
         logit( "et", "lpprocT: Tracking error.\n");

      if ( rc == GET_MISS_LAPPED )
         logit( "et", "lpprocT: Got lapped on the ring.\n");

      if ( rc == GET_MISS_SEQGAP )
         logit( "et", "lpprocT: Gap in sequence numbers.\n");

      if ( rc == GET_MISS )
         logit( "et", "lpprocT: Missed messages.\n");

      if ( rc == GET_TOOBIG )
      {
         logit( "et", "lpprocT: Retrieved message too big (%d) for msg.\n",
                MsgLen );
         continue;
      }
	  
/* Put hypocenter into temp structure (NOTE: No byte-swapping is performed, so 
   there will be trouble getting hypos from an opposite order machine)
   *******************************************************************/   
      if ( HypoStruct( HIn, &HStructT ) < 0 )
      {
         logit( "t", "Problem in HypoStruct function - 1\n" );
         continue;
      }
      	  
/* Process LP data if this is a new or updated location and of sufficient
   magnitude (or shutoff processing if new location indicates small quake)
   **********************************************************************/
      if ( iLPProcessing == 1 && HStructT.iQuakeID == iQuakeID && 
           HStructT.iVersion != iVersion && HStructT.iVersion < 12 &&
          (HStructT.dMbAvg < 5.0 && HStructT.dMlAvg < 5.0 &&
           HStructT.dMwpAvg < 5.5) )
      {	  
         RequestSpecificMutex( &mutsem1 );   /* Sem protect buff writes */
         for ( i=0; i<Nsta; i++ ) StaArray[i].iPickStatus = 0;
         strcpy( szTitle, szProcessName );
         SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
         for ( i=0; i<Nsta; i++ ) PBuf[i].dMSMag = 0.;
         iLPProcessing = 0;
         ReleaseSpecificMutex( &mutsem1 ); /* Let someone else have sem */	  	  
         logit( "t", "LP processing stopped due to small magnitude\n" );   
         iQuakeID = HStruct.iQuakeID;	  
         iVersion = HStruct.iVersion;	  		 		 
      }
      else if ( (iLPProcessing == 1 && HStructT.iQuakeID == iQuakeID && 
            HStructT.iVersion != iVersion && HStructT.iVersion < 9 &&
           (HStructT.dMbAvg > 5.0 || HStructT.dMlAvg > 5.0 ||
            HStructT.dMwpAvg > 5.5)) ||
           (iLPProcessing == 0 && 
           (HStructT.dMbAvg > 5.0 || HStructT.dMlAvg > 5.0 ||
            HStructT.dMwpAvg > 5.5)) )
      {
         logit( "t", "Start LP - %ld %ld\n",
                                   HStructT.iQuakeID, HStructT.iVersion );
         RequestSpecificMutex( &mutsem1 );   /* Sem protect buffer writes */
         iLPProcessing = 0;
         for ( i=0; i<Nsta; i++ )
            if ( PBuf[i].iUseMe >= 0 )
            {
               InitP( &PBuf[i] );
               PBuf[i].iUseMe = 0;
            }
			
/* iPickStatus: 1->waiting on data in the Rayleigh wave window */
         for ( i=0; i<Nsta; i++ )
         {
            StaArray[i].iPickStatus = 0;
            StaArray[i].lHit = 1;   /* lHit = 1 for Init, 0 for no init */
         }
	  
/* Put hypocenter into global structure (NOTE: No byte-swapping is performed, so 
   there will be trouble getting hypos from an opposite order machine)
   *******************************************************************/   
         if ( HypoStruct( HIn, &HStruct ) < 0 )
         {
            logit( "t", "Problem in HypoStruct function - 2\n" );
            continue;
         }
         strcpy( szTitle, szProcessName );
         strcat( szTitle, " - PROCESSING LP" );
         SetWindowText( hwndWndProc, szTitle );  /* Display the title */
         iLPProcessing = 1;
         for ( i=0; i<Nsta; i++ )
         {
            GetRWaveWindow( &HStruct, &RTimes[i], &StaArray[i], &PBuf[i] );
/* Compute time of oldest data in buffer */
            dOldestTime = StaArray[i].dEndTime - ((double)
             StaArray[i].lRawCircSize/StaArray[i].dSampRate) +
             1./StaArray[i].dSampRate;
	     
/* Is there data in this buffer covering the R-wave window? */
            if ( RTimes[i].dRStartTime < StaArray[i].dEndTime &&
                 RTimes[i].dREndTime   > dOldestTime ) 
            {		 
/* Process the data */
               if ( GetMs( &StaArray[i], &RTimes[i], &PBuf[i], WaveHead,
                           &HStruct, &Gparm, StaArray[i].lHit ) == 1 )
               {
                  ReportPick( &PBuf[i], &Gparm, &Ewh );  /* Report MS to ring */
                  WritePickFile( Nsta, PBuf, Gparm.LPRTFile ); /* Update File */
                  dAvgMS = ComputeAvgMS( Nsta, PBuf, &iNumMS );
                  HStruct.iNumMS = iNumMS;
                  HStruct.dMSAvg = dAvgMS;
/* If this is the same hypo as in the dummy file, update the Ms */				  
                  ReadDummyData( &HypoD, Gparm.DummyFile ); 
                  if ( IsItSameQuake( &HStruct, &HypoD ) == 1 )
                     if ( PatchDummyWithLP( &HStruct, Gparm.DummyFile ) == 0 )
                        logit( "t", "Write dummy file error in lpproc-1\n");
                  _gcvt( dAvgMS, 2, szAvgMS );
                  if ( dAvgMS - (int) dAvgMS <= 0.05 || 
                       dAvgMS - (int) dAvgMS >= 0.95 ) szAvgMS[2] = '0';
                  strcpy( szTitle, szProcessName );
                  strcat( szTitle, " - PROCESSING LP - " );
                  strcat( szTitle, szAvgMS );
                  strcat( szTitle, "-" );
                  itoaX( iNumMS, szNumMS );
                  strcat( szTitle, szNumMS );
                  strcat( szTitle, " - " );
                  itoaX( HStruct.iQuakeID, szQuake );
                  PadZeroes( 4, szQuake );
                  strcat( szTitle, szQuake );
                  SetWindowText( hwndWndProc, szTitle );  /* Display title */
               }
               StaArray[i].lHit = 0;   /* lHit = 1 for Init, 0 for no init */
            }
            else if ( RTimes[i].dRStartTime > StaArray[i].dEndTime )
               StaArray[i].iPickStatus = 1;
         }
         for ( i=0; i<Nsta; i++ )
            if ( StaArray[i].iPickStatus == 1 ) goto LoopEnd;
         strcpy( szTitle, szProcessName );
         SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
         for ( i=0; i<Nsta; i++ ) PBuf[i].dMSMag = 0.;
         iLPProcessing = 0;
         logit( "t", "LP processing stopped in HThread\n" );
LoopEnd:
         ReleaseSpecificMutex( &mutsem1 );   /* Let someone else have sem */
         iQuakeID = HStruct.iQuakeID;	  
         iVersion = HStruct.iVersion;	  
      }
   }
}

  /******************************************************************
   *                      PatchDummyWithLP()                        *
   *                                                                *
   *  Update the dummy file with the new average Ms.                *
   *                                                                *
   *  Arguments:                                                    *
   *    pHypo       Structure with hypocenter parameters            *
   *    pszDFile    Dummy File name                                 *
   *                                                                *
   ******************************************************************/
   
int PatchDummyWithLP( HYPO *pHypo, char *pszDFile )
{
   HYPO    HypoT;       /* Temporary hypocenter data structure */

/* Read in hypocenter information from dummy file */
   if ( ReadDummyData( &HypoT, pszDFile ) == 0 ) 
   {
      logit ("t", "Failed to open DummyFile in PatchDummyWithLP\n");
      return 0;
   }
    
/* Update MS data */
   HypoT.dMSAvg = pHypo->dMSAvg;
   HypoT.iNumMS = pHypo->iNumMS;
   
/* Choose (and update) the preferred magnitude */
   GetPreferredMag( &HypoT );
   pHypo->iNumPMags = HypoT.iNumPMags;
   strcpy( pHypo->szPMagType, HypoT.szPMagType );
   pHypo->dPreferredMag = HypoT.dPreferredMag;

/* Update dummy file */
   if ( WriteDummyData( &HypoT, NULL, pszDFile, 0, 0 ) == 0 )
   {
      logit ("t", "Failed to open DummyFile in PatchDummyWithLP (2)\n");
      return 0;
   }
   return 1;
}

  /******************************************************************
   *                         PutDataInBuffer()                      *
   *                                                                *
   *  Load the data from the input buffer to the large circular     *
   *  buffer.  Update last data value index.                        *
   *                                                                *
   *  Arguments:                                                    *
   *    WaveHead    Pointer to data buffer header                   *
   *    WaveLong    Pointer to data array                           *
   *    plBuff      Pointer to main circular buffer                 *
   *    plBuffCtr   Index tracker in main buffer                    *
   *    lBuffSize   # samples in main circular buffer               *
   *                                                                *
   ******************************************************************/

void PutDataInBuffer( TRACE_HEADER *WaveHead, long *WaveLong, 
                      long *plBuff, long *plBuffCtr, long lBuffSize )
{
   int     i;
   
/* Copy the data buffer */
   for ( i=0; i<WaveHead->nsamp; i++ )
   {
      plBuff[*plBuffCtr] = WaveLong[i];
      *plBuffCtr += 1;
      if ( *plBuffCtr == lBuffSize ) *plBuffCtr = 0;
   }	  
}

      /******************************************************************
       *                       ResetOptions()                           *
       *                                                                *
       * Every X minutes, this functions is called to set some variables*
       * to defaults.                                                   *
       *                                                                *
       *  Arguments:                                                    *
       *   hwnd             Window handle of calling program            *
       *   iNumSta          Numberof stations in Sta                    *
       *   Trace            Array of trace control information          *
       *   piFiltDisplay    1->display filtered signal, 0->broadband    *
       *   piClipIt         1->Amplitude limit signal, 0->let it go     *
       *   piVScrollOffset  Vertical scrolling amount                   *
       *   hMenu            Menu handle                                 *
       *                                                                *
       ******************************************************************/
	   
void ResetOptions( HWND hwnd, int iNumSta, TRACE Trace[], int *piFiltDisplay,
                   int *piClipIt, int *piVScrollOffset, HMENU hMenu )
{
   int     i;
		
/* Turn trace toggle to on (Rt. mouse click) */		
   for ( i=0; i<iNumSta; i++ )
      if ( Trace[i].iDisplayStatus == 0 ) Trace[i].iDisplayStatus = 1;

/* Reset vertical scaling factors */		
   for ( i=0; i<iNumSta; i++ )
      Trace[i].dVScale = StaArray[i].dScaleFactor;
		
/* Set trace clipping to on */
   *piClipIt = 1;
   HiliteMenuItem( hwnd, hMenu, IDM_CLIPIT, MF_BYCOMMAND | MFS_HILITE );
		
/* Set data display to filtered data */
   *piFiltDisplay = 1;
   CheckMenuItem( hMenu, IDM_FILTDISPLAY, MF_BYCOMMAND | MFS_CHECKED );
   CheckMenuItem( hMenu, IDM_BBDISPLAY, MF_BYCOMMAND | MFS_UNCHECKED );
   
/* Turn of screen hold option */   
   iHold = 0;
   HiliteMenuItem( hwnd, hMenu, IDM_DISP_HOLD, MF_BYCOMMAND | MFS_UNHILITE );
   
/* Reset vertical offset to 0 position */
   *piVScrollOffset = 0;
}

      /***********************************************************
       *                 TracePerScreenDlgProc()                 *
       *                                                         *
       * This dialog procedure lets the user set the number of   *
       * traces to be shown on the visible part of the screen    *
       * display.                                                *
       *                                                         *
       ***********************************************************/
	   
long WINAPI TracePerScreenDlgProc( HWND hwnd, UINT msg, UINT wParam,
                                   long lParam )
{
   int     iTemp;

   switch ( msg ) 
   {
      case WM_INITDIALOG:  /* Pre-set entry field to present number */
         SetDlgItemInt( hwnd, EF_NUMSTATODISP, Gparm.NumTracePerScreen, TRUE );
         SetFocus( hwnd );
         break;

      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {
            case IDOK:     /* Accept user input */
               Gparm.NumTracePerScreen =
                GetDlgItemInt( hwnd, EF_NUMSTATODISP, &iTemp, TRUE );
               if ( Gparm.NumTracePerScreen > Nsta )
                  Gparm.NumTracePerScreen = Nsta;
               if ( Gparm.NumTracePerScreen < 2 )
                  Gparm.NumTracePerScreen = 2;
               EndDialog (hwnd, IDOK);
               break;

            case IDCANCEL: /* Escape - don't accept input */
               EndDialog (hwnd, IDCANCEL);
               break;
		
            default:
               break;
         }
   }
   return 0;
}

      /***********************************************************
       *                      WndProc()                          *
       *                                                         *
       *  This dialog procedure processes messages from the      *
       *  windows screen and messages passed to it from elsewhere*
       *  (e.g., the message WM_NEW_DATA comes from WinMain when *
       *  a waveform has been picked out of the ring).  All      *
       *  display is performed in PAINT.  Menu options (on main  *
       *  menu and through right button clicks) control the      *
       *  display.  Traces can be erased with the right button on*
       *  the trace.                                             *
       *                                                         *
       ***********************************************************/
       
long WINAPI WndProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   static  int    cxScreen, cyScreen;        /* Window size in pixels */
   double  dAvgMS;            /* Average MS for Title */
   static  double dInc;                      /* Timing line increments */
   double        dOldestTime;     /* 1/1/70 time of oldest data in buffer */
   static  double dScreenTime;               /* # secs. of data on screen */
   static  double dScrollHPage, dScrollHBit; /* Hor. scroll amounts (sec) */
   FILE    *hFile;            /* File handle */
   HCURSOR hCursor;           /* Present cursor handle (hourglass or arrow) */
   static  HDC hdc;           /* Device context of screen */
   static  HANDLE  hMenu;     /* Handle to the menu */
   static  HMENU   hMenu2;    /* Handle to the station popup menu */
   static  HMENU   hMenu3;    /* Handle to the station popup menu */
   HYPO    HypoD;             /* Hypocenter structure from dummy file */
   HYPO    HypoT[MAX_QUAKES]; /* Array of previously located hypocenters */
   int     i, j, iCnt;        /* Loop Counters */
   static  int  iChanIDOffset;/* Trace offset at left of screen */
   static  int  iClipIt;      /* 0->no trace clipping, 1->trace clipping */
   static  int  iDeltaPerLine, iAccumDelta;  /* Mousewheel variables */
   static  int  iFiltDisplay; /* 0->broadband data display; 1->filt disp (def)*/
   static  int  iIndex;       /* Index of station right clicked upon */   
   int     iNumMS;            /* Number stns used in average MS (Title) */
   int     iNumShown;         /* Number of traces displayed in DisplayTraces */
   static  int  iScreenToDisplay;     /* Set of stations to display */
   static  int  iScrollVertBuff;      /* Small vertical scroll amount (pxl) */
   static  int  iScrollVertPage;      /* Large vertical scroll amount (pxl) */				
   static  int  iVScrollOffset;       /* Vertical scoll bar setting */
   static  long lTitleFHt, lTitleFWd; /* Font height and width */
   static  int  iTitleOffset;         /* Trace offset at top of screen */
   PAINTSTRUCT ps;          /* Paint structure used in WM_PAINT command */
   POINT   pt, pt2;         /* Screen locations */
   RECT    rct;             /* RECT (rectangle structure) */
   char    szAvgMS[8], szNumMS[4], szQuake[8]; /* Used in Title */
   static  TRACE *Trace;    /* Array of structures which describe screen */   
   unsigned long ulScrollLines;  /* Mousewheel variables */

/* Respond to user input (menu choices, etc.) and system messages */
   switch ( msg )
   {
      case WM_CREATE:         /* Do this the first time through */
         hCursor = LoadCursor( NULL, IDC_ARROW );
         SetCursor( hCursor );
         hMenu = GetMenu( hwnd );
         hMenu2 = LoadMenu( hInstMain, "Station_Menu" );     /* Popup menu */
         hMenu3 = GetSubMenu( hMenu2, 0 );      /* Load station POPUP menu */
         dScreenTime = Gparm.TimePerScreen;
         iVScrollOffset = 0;       /* Start with no vertical scrolling */
         HiliteMenuItem( hwnd, hMenu, IDM_DISP_HOLD,     /* Let screen auto- */
                         MF_BYCOMMAND | MFS_UNHILITE );  /* scroll           */
         iClipIt = 1;              /* Clip traces */
         HiliteMenuItem( hwnd, hMenu, IDM_CLIPIT, MF_BYCOMMAND | MFS_HILITE );
         iFiltDisplay = 1;         /* Start with filtered data on display */
         CheckMenuItem( hMenu, IDM_FILTDISPLAY, MF_BYCOMMAND | MFS_CHECKED );
         CheckMenuItem( hMenu, IDM_BBDISPLAY, MF_BYCOMMAND | MFS_UNCHECKED );
         dInc = 60.;
         SetWindowText( hwnd, szTitle );       /* Display the title */

/* Allocate the trace array */
         Trace = (TRACE *) calloc( Nsta, sizeof (TRACE) );
         if ( Trace == NULL )
         {
            logit( "et", "lpproc: Cannot allocate the trace array\n" );
            PostMessage( hwnd, WM_DESTROY, 0, 0 );
            break;
         }
		 
/* Fill in Trace array */
         for ( i=0; i<Nsta; i++ )
         {
            Trace[i].iStationDisp[0] = 1;
            Trace[i].iDisplayStatus = 1;
            Trace[i].dVScale = StaArray[i].dScaleFactor;
         }
         iScreenToDisplay = 0;     /* Default is show all stations */
		 		 
/* Set horizontal scrolling amounts */		 
         dScrollHBit  = Gparm.TimePerScreen / 10.;
         dScrollHPage = Gparm.TimePerScreen / 2.;
	 
      case WM_SETTINGCHANGE:
         SystemParametersInfo( SPI_GETWHEELSCROLLLINES, 0, &ulScrollLines, 0 );
/* ulScrolllines usually equals 3 or 0 (for no scrolling)
   WHEEL_DELTA equals 120, so iDeltaPerLine will be 40 */
          iAccumDelta = 0;
          if ( ulScrollLines )
             iDeltaPerLine = WHEEL_DELTA / ulScrollLines;	
          else
             iDeltaPerLine = 0;	
          break;
		 
/* Get screen size in pixels, re-paint, and re-proportion screen */
      case WM_SIZE:
         cyScreen = HIWORD (lParam);
         cxScreen = LOWORD (lParam);
		 
/* Compute font size */
         lTitleFHt = cyScreen / 40;
         lTitleFWd = cxScreen / 140;
		 
/* Compute vertical scrolling amounts */		 
         iScrollVertBuff = cyScreen / Gparm.NumTracePerScreen;
         iScrollVertPage = cyScreen / 8;
		 
/* Compute offsets from top and left sides of screen */		 
         iTitleOffset = cyScreen / 30;
         iChanIDOffset = cxScreen / 18;
	 
/* Set scroll thumb positions */
         SetScrollRange( hwnd, SB_HORZ, 0, 100, FALSE );  /* 100/50 arbitrary */
         SetScrollPos( hwnd, SB_HORZ, 50, TRUE );
         SetScrollRange( hwnd, SB_VERT, 0, 100, FALSE );  /* 100/50 arbitrary */
         SetScrollPos( hwnd, SB_VERT, 0, TRUE );
		 
         InvalidateRect( hwnd, NULL, TRUE );    /* Force a re-PAINT */
         break;

      case WM_TIMER:		
         if ( wParam == ID_TIMER2 )           /* Time to reset options */
            ResetOptions( hwnd, Nsta, Trace, &iFiltDisplay, &iClipIt,
                          &iVScrollOffset, hMenu );   
         break;

/* Respond to menu selections */
      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {
            case IDM_SCALE_8X:      /* 8 times existing trace scale */
               Trace[iIndex].dVScale *= 8.;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
	        
            case IDM_SCALE_4X:      /* 4 times existing trace scale */
               Trace[iIndex].dVScale *= 4.;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
	       
            case IDM_SCALE_2X:      /* 2 times existing trace scale */
               Trace[iIndex].dVScale *= 2.;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
	        
            case IDM_SCALE_HALFX:   /* 0.5 times existing trace scale */
               Trace[iIndex].dVScale *= 0.5;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
	        
            case IDM_SCALE_QUARTERX:/* 0.25 times existing trace scale */
               Trace[iIndex].dVScale *= 0.25;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
	       
            case IDM_SCALE_EIGHTHX: /* 0.125 times existing trace scale */
               Trace[iIndex].dVScale *= 0.125;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
	       
            case IDM_SCALE_DEFAULT: /* Force back to original */
               Trace[iIndex].dVScale = StaArray[iIndex].dScaleFactor;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
		
            case IDM_STADISP_ON:    /* Toggle display to on */
               Trace[iIndex].iDisplayStatus = 1;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
		
            case IDM_STADISP_OFF:   /* Toggle display to always off */
               Trace[iIndex].iDisplayStatus = 2;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
		
            case IDM_ALARM_PAGE:    /* Toggle pager alarm on/off */
               if ( StaArray[iIndex].iAlarmPage == 1 )
                  StaArray[iIndex].iAlarmPage = 0;
               else if ( StaArray[iIndex].iAlarmPage == 0 )
                  StaArray[iIndex].iAlarmPage = 1;
               break;
		
            case IDM_ALARM_SPEAK:   /* Toggle speaker alarm on/off */
               if ( StaArray[iIndex].iAlarmSpeak == 1 )
                  StaArray[iIndex].iAlarmSpeak = 0;
               else if ( StaArray[iIndex].iAlarmSpeak == 0 )
                  StaArray[iIndex].iAlarmSpeak = 1;
               break;

            case IDM_HSCALE:        /* Change the horizontal scale */
               iScale = 1;          /*  on all traces              */
               if ( DialogBox( hInstMain, "HorizontalScale", hwndWndProc,
                   (DLGPROC) GlobalScaleDlgProc ) == IDOK )
               {
                  if ( iScale == 0 )           /* Set scale to original */
                     dScreenTime = Gparm.TimePerScreen;
                  else if ( iScale < 0 )       /* Change scale - increase time*/
                     dScreenTime *= (double) abs( iScale );
                  else if ( iScale > 0 )       /* Change scale - decrease time*/
                     dScreenTime /= (double) iScale; 
                  dScrollHBit = dScreenTime / 10.;
                  dScrollHPage = dScreenTime / 2.;
                  InvalidateRect( hwnd, NULL, TRUE );
               }
               break;

            case IDM_VSCALE:        /* Change the vertical scale */
               iScale = 1;          /*  on all traces            */
               if ( DialogBox( hInstMain, "GlobalVerticalScale", hwndWndProc,
                   (DLGPROC) GlobalScaleDlgProc ) == IDOK )
               {
                  if ( iScale == 0 )           /* Reset to original */
                     for ( i=0; i<Nsta; i++ )
                        Trace[i].dVScale = StaArray[i].dScaleFactor;
                  else if ( iScale > 0 )       /* Change scale on all-increase*/
                     for ( i=0; i<Nsta; i++ )  
                        Trace[i].dVScale *= (double) iScale;
                  else if ( iScale < 0 )       /* Change scale on all-decrease*/
                     for ( i=0; i<Nsta; i++ )  
                        Trace[i].dVScale /= (double) abs( iScale );
                  InvalidateRect( hwnd, NULL, TRUE );
               }
               break;
		
/* This menu option lets the user change the number of traces
   which are squished into the visible (vertically) part of the
   screen display. */
            case IDM_TRACEPERSCREEN:	
               if ( DialogBox( hInstMain, "TracePerScreen", hwndWndProc, 
                   (DLGPROC) TracePerScreenDlgProc ) == IDOK )
               {
                  iScrollVertBuff = cyScreen / Gparm.NumTracePerScreen;
                  iScrollVertPage = cyScreen / 8;
                  InvalidateRect( hwnd, NULL, TRUE );
               }
               break;

            case IDM_FILTDISPLAY:      /* Let trace display filtered data */
               iFiltDisplay = 1;
               CheckMenuItem( hMenu, IDM_FILTDISPLAY,    /* Menu checkmarks */
                              MF_BYCOMMAND | MFS_CHECKED );
               CheckMenuItem( hMenu, IDM_BBDISPLAY, 
                              MF_BYCOMMAND | MFS_UNCHECKED );
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_BBDISPLAY:        /* Let trace display broadband data */
               iFiltDisplay = 0;
               CheckMenuItem( hMenu, IDM_FILTDISPLAY,    /* Menu checkmarks */
                              MF_BYCOMMAND | MFS_UNCHECKED );
               CheckMenuItem( hMenu, IDM_BBDISPLAY, 
                              MF_BYCOMMAND | MFS_CHECKED );
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_CLIPIT:     /* Turn iClipIt flag on or off (this */
               if ( iClipIt )    /*  amplitude limits the display)    */
               {
                  iClipIt = 0;
                  HiliteMenuItem( hwnd, hMenu, IDM_CLIPIT, 
                                  MF_BYCOMMAND | MFS_UNHILITE );
               }
               else 
               {
                  iClipIt = 1;
                  HiliteMenuItem( hwnd, hMenu, IDM_CLIPIT, 
                                  MF_BYCOMMAND | MFS_HILITE );
               }
               break;

            case IDM_DISP_HOLD:   /* Stop screen from updating */
               if ( iHold )
               {
                  iHold = 0;
                  HiliteMenuItem( hwnd, hMenu, IDM_DISP_HOLD, 
                                  MF_BYCOMMAND | MFS_UNHILITE );
               }
               else          
               {
                  iHold = 1;
                  HiliteMenuItem( hwnd, hMenu, IDM_DISP_HOLD, 
                                  MF_BYCOMMAND | MFS_HILITE );
               }
               break;
			   
            case IDM_STOP_LP:
               RequestSpecificMutex( &mutsem1 );   /* Sem protect buff writes */
               for ( i=0; i<Nsta; i++ ) StaArray[i].iPickStatus = 0;
               strcpy( szTitle, szProcessName );
               SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
               for ( i=0; i<Nsta; i++ ) PBuf[i].dMSMag = 0.;
               iLPProcessing = 0;
               ReleaseSpecificMutex( &mutsem1 ); /* Let someone else have sem */
               logit( "t", "LP processing manually stopped\n" );
               break;
			
            case IDM_COMPUTE_MS:
               RequestSpecificMutex( &mutsem1 );   /* Sem protect buff writes */
               if ( iLPProcessing == 1 )
               {
                  for ( i=0; i<Nsta; i++ ) StaArray[i].iPickStatus = 0;
                  strcpy( szTitle, szProcessName );
                  SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
                  for ( i=0; i<Nsta; i++ ) PBuf[i].dMSMag = 0.;
                  iLPProcessing = 0;                  
                  logit( "t", "LP processing stopped in COMPUTE_MS-1\n" );
               }
               for ( i=0; i<Nsta; i++ )
                  if ( PBuf[i].iUseMe >= 0 )
                  {
                     InitP( &PBuf[i] );
                     PBuf[i].iUseMe = 0;
                  }
				  
/* Clear out LP data file */
               hFile = fopen( Gparm.LPRTFile, "w" );
               fclose( hFile );				  				  
				  
/* iPickStatus: 1->waiting on data in the Rayleigh wave window */
               for ( i=0; i<Nsta; i++ )
               {
                  StaArray[i].iPickStatus = 0;
                  StaArray[i].lHit = 1;   /* lHit = 1 for Init, 0 for no init */
               }
			   
/* Get hypocenter parameters from dummy file */			   
               ReadDummyData( &HStruct, Gparm.DummyFile );
               HStruct.iQuakeID = 0;
               HStruct.iVersion = 0;
			   
/* Is this quake in the oldquake file (can MS be updated in HYPO_DISPLAY)? */
               LoadHypo( Gparm.QuakeFile, HypoT );
               for ( i=0; i<MAX_QUAKES; i++ )
                  if ( IsItSameQuake( &HStruct, &HypoT[i] ) == 1 )
                  {
                     HStruct.iQuakeID = HypoT[i].iQuakeID; 
                     HStruct.iVersion = HypoT[i].iVersion; 
                     break;
                  }
               strcpy( szTitle, szProcessName );
               strcat( szTitle, " - PROCESSING LP" );
               SetWindowText( hwndWndProc, szTitle );  /* Display the title */
               logit( "t", "LP processing started in COMPUTE_MS\n" );
               iLPProcessing = 1;
               for ( i=0; i<Nsta; i++ )
               {
                  GetRWaveWindow( &HStruct, &RTimes[i], &StaArray[i], &PBuf[i] );
/* Compute time of oldest data in buffer */
                  dOldestTime = StaArray[i].dEndTime - ((double)
                   StaArray[i].lRawCircSize/StaArray[i].dSampRate) +
                   1./StaArray[i].dSampRate;
	     
/* Is there data in this buffer covering the R-wave window? */
                  if ( RTimes[i].dRStartTime < StaArray[i].dEndTime &&
                       RTimes[i].dREndTime   > dOldestTime ) 
                  {		 	  		
/* Process the data */
                     if ( GetMs( &StaArray[i], &RTimes[i], &PBuf[i], WaveHead,
                                 &HStruct, &Gparm, StaArray[i].lHit ) == 1 )
                     {
                        ReportPick( &PBuf[i], &Gparm, &Ewh );/* Report MS*/
                        WritePickFile( Nsta, PBuf, Gparm.LPRTFile );
                        dAvgMS = ComputeAvgMS( Nsta, PBuf, &iNumMS );
                        HStruct.iNumMS = iNumMS;
                        HStruct.dMSAvg = dAvgMS;
/* If this is the same hypo as in the dummy file, update the Ms */				  
                        ReadDummyData( &HypoD, Gparm.DummyFile ); 
                        if ( IsItSameQuake( &HStruct, &HypoD ) == 1 )
                           if ( PatchDummyWithLP( &HStruct, Gparm.DummyFile ) ==
                                0 )
                              logit( "t", "Write dummy file error in lpproc\n");
                        _gcvt( dAvgMS, 2, szAvgMS );
                        if ( dAvgMS - (int) dAvgMS <= 0.05 || 
                             dAvgMS - (int) dAvgMS >= 0.95 ) szAvgMS[2] = '0';
                        strcpy( szTitle, szProcessName );
                        strcat( szTitle, " - PROCESSING LP - " );
                        strcat( szTitle, szAvgMS );
                        strcat( szTitle, "-" );
                        itoaX( iNumMS, szNumMS );
                        strcat( szTitle, szNumMS );
                        strcat( szTitle, " - " );
                        itoaX( HStruct.iQuakeID, szQuake );
                        PadZeroes( 4, szQuake );
                        strcat( szTitle, szQuake );
                        SetWindowText( hwndWndProc, szTitle );  /* Display title */
                     }   
                     StaArray[i].lHit = 0;   /* lHit = 1 for Init, 0 for no init */
                  }
                  else if ( RTimes[i].dRStartTime > StaArray[i].dEndTime )
                     StaArray[i].iPickStatus = 1;
               }
               for ( i=0; i<Nsta; i++ )
                  if ( StaArray[i].iPickStatus == 1 ) goto LoopEnd;
               strcpy( szTitle, szProcessName );
               SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
               for ( i=0; i<Nsta; i++ ) PBuf[i].dMSMag = 0.;
               iLPProcessing = 0;
               logit( "t", "LP processing stopped in COMPUTE_MS-2\n" );
LoopEnd:
               if ( iProcLPFromThread == 1 ) SetEvent( hEventRTDoneLP );
               iProcLPFromThread = 0;
               ReleaseSpecificMutex( &mutsem1 ); /* Let someone else have sem */
               break;

            case IDM_REFRESH:       // redraw the screen
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_EXIT_MAIN:     /* Send DESTROY message to itself */
               PostMessage( hwnd, WM_DESTROY, 0, 0 );
               break;          
         }
         break ;
       	
/* Have update traces, show them on screen */	
      case WM_NEW_DATA:	
         if ( strlen( Gparm.ATPLineupFileLP ) > 2 )   /* Player mode */
            if ( iNewPlayerData == 1 )	    
            {
               iNewPlayerData = 0;
               for ( i=0; i<Nsta; i++ )
               {
                  Trace[i].iStationDisp[0] = 1;
                  Trace[i].iDisplayStatus = 1;
                  Trace[i].dVScale = StaArray[i].dScaleFactor;
               } 
            }
         InvalidateRect( hwnd, NULL, TRUE );
         break;

/* Remove/Include P from locations or make a pick */
      case WM_LBUTTONDOWN:
         GetCursorPos( &pt );            /* Get the cursor location */
         ScreenToClient( hwnd, &pt );    /* Put in client window coords */
		 
/* Which screen index is this y value */
         j = ((pt.y-iTitleOffset-iVScrollOffset+(cyScreen-iTitleOffset)/
              (Gparm.NumTracePerScreen*2)) * Gparm.NumTracePerScreen) /
              (cyScreen-iTitleOffset);
         iCnt = 0;
         for( i=0; i<Nsta; i++ )            /* Which station was clicked? */
            if( Trace[i].iStationDisp[iScreenToDisplay] )
            {
               if ( iCnt == j ) break;
               iCnt++;
            }
			
/* If click was within station name part of screen, eliminate if from soln */
         if( i >= 0 && i < Nsta && fabs( StaArray[i].dAveLDCRaw ) > 0. )
            if ( pt.x < iChanIDOffset )     /* X out from solution */
/* Set display menu item check */
               if ( PBuf[i].dMSAmpGM > 0. )
               {
                  RequestSpecificMutex( &mutsem1 ); /* Protect buff writes */
                  if      ( PBuf[i].iUseMe >=  0 ) PBuf[i].iUseMe = -1;
                  else if ( PBuf[i].iUseMe == -1 ) PBuf[i].iUseMe = 0;
                  ReportPick( &PBuf[i], &Gparm, &Ewh );
                  WritePickFile( Nsta, PBuf, Gparm.LPRTFile ); /* Update File */
                  InvalidateRect( hwnd, NULL, TRUE );
                  ReleaseSpecificMutex( &mutsem1 ); /* Release sem */
               }
         break;

/* Toggle on/off for trace display, or bring up popup menu */
      case WM_RBUTTONDOWN:
         GetCursorPos( &pt );            /* Get the cursor location */
         pt2.x = pt.x;
         pt2.y = pt.y;
         ScreenToClient( hwnd, &pt );    /* Put in client window coords */
		 
/* Which screen index is this y value */
         j = ((pt.y-iTitleOffset-iVScrollOffset+(cyScreen-iTitleOffset)/
              (Gparm.NumTracePerScreen*2)) * Gparm.NumTracePerScreen) /
              (cyScreen-iTitleOffset);
         iCnt = 0;
         for( i=0; i<Nsta; i++ )            /* Which station was clicked? */
            if( Trace[i].iStationDisp[iScreenToDisplay] )
            {
               if ( iCnt == j ) break;
               iCnt++;
            }
			
/* If click was within station name part of screen, bring up menu; otherwise
   toggle trace on or off */			
//         if( i >= 0 && i < Nsta && fabs( StaArray[i].dAveLDCRaw ) > 0. )
         if ( i >= 0 && i < Nsta )
            if ( pt.x > iChanIDOffset )     /* Toggle trace on/off */
               {
                  if ( Trace[i].iDisplayStatus == 0 )
                     Trace[i].iDisplayStatus = 1;
                  else if ( Trace[i].iDisplayStatus == 1 )
                     Trace[i].iDisplayStatus = 0;
                  InvalidateRect( hwnd, NULL, TRUE );
               }
               else                         /* Bring up station popup menu */
               {
                  iIndex = i;               /* Save for future scaling */
	    
/* Set display menu item check */
                  if( Trace[i].iDisplayStatus == 0 ||
                      Trace[i].iDisplayStatus == 1 ) 
                  {                                       /* On */
                     CheckMenuItem( hMenu3, IDM_STADISP_ON,
                                    MF_BYCOMMAND | MFS_CHECKED );
                     CheckMenuItem( hMenu3, IDM_STADISP_OFF,
                                    MF_BYCOMMAND | MFS_UNCHECKED );
                  }
                  else                                    /* Off */
                  {
                     CheckMenuItem( hMenu3, IDM_STADISP_ON,
                                    MF_BYCOMMAND | MFS_UNCHECKED );
                     CheckMenuItem( hMenu3, IDM_STADISP_OFF,
                                    MF_BYCOMMAND | MFS_CHECKED );
                  }
	    
/* Set alarm menu items check */
                  if ( StaArray[i].iAlarmPage == 1 )      /* On */
                     CheckMenuItem( hMenu3, IDM_ALARM_PAGE,
                                    MF_BYCOMMAND | MFS_CHECKED );
                  else                                    /* Off */
                     CheckMenuItem( hMenu3, IDM_ALARM_PAGE,
                                    MF_BYCOMMAND | MFS_UNCHECKED );
                  if ( StaArray[i].iAlarmSpeak == 1 )     /* On */
                     CheckMenuItem( hMenu3, IDM_ALARM_SPEAK,
                                    MF_BYCOMMAND | MFS_CHECKED );
                  else                                    /* Off */
                     CheckMenuItem( hMenu3, IDM_ALARM_SPEAK,
                                    MF_BYCOMMAND | MFS_UNCHECKED );
                  TrackPopupMenu( hMenu3, 0, pt2.x, pt2.y, 0, hwnd, NULL );
               }
         break;
	 
/* Respond to mouse roller */                          
      case WM_MOUSEWHEEL:
         if ( iDeltaPerLine == 0 ) break;
         iAccumDelta += (short) HIWORD ( wParam );
  	 while ( iAccumDelta >= iDeltaPerLine )
	 {
            SendMessage( hwnd, WM_VSCROLL, SB_LINEUP, 0 );
	    iAccumDelta -= iDeltaPerLine;
	 }
	 while ( iAccumDelta <= -iDeltaPerLine )
	 {
            SendMessage( hwnd, WM_VSCROLL, SB_LINEDOWN, 0 );
	    iAccumDelta += iDeltaPerLine;
	 }
	 break;

/* Fill in screen display with traces, Ps, mag info, and stn names */
      case WM_PAINT:
         hdc = BeginPaint( hwnd, &ps );         /* Get device context */
         GetClientRect( hwnd, &rct );
         FillRect( hdc, &rct, (HBRUSH) GetStockObject( WHITE_BRUSH ) );
         if ( dLastEndTime > 0. )
         {
            RequestSpecificMutex( &mutsem2 );/* Semaphore protect display */
            DisplayTimeLines( hdc, lTitleFHt, lTitleFWd, cxScreen, cyScreen,
                              iChanIDOffset, dLastEndTime, dScreenTime, dInc );
            DisplayTraces( hdc, StaArray, Trace, Gparm.NumTracePerScreen, Nsta,
                           iScreenToDisplay, iFiltDisplay, iClipIt, cxScreen,
                           cyScreen, iVScrollOffset, iTitleOffset,
                           iChanIDOffset, dLastEndTime, dScreenTime,
                           &iNumShown );
            DisplayChannelID( hdc, StaArray, Trace, Nsta, iScreenToDisplay,
                              lTitleFHt, lTitleFWd, cxScreen, cyScreen,
                              Gparm.NumTracePerScreen, iVScrollOffset,
                              iTitleOffset, iAhead );
            DisplayRTimes( hdc, Nsta, Gparm.NumTracePerScreen, cxScreen,
                           cyScreen, iVScrollOffset, iTitleOffset,iChanIDOffset,
                           dLastEndTime, dScreenTime, lTitleFHt, RTimes );
            DisplayMagBox( hdc, Nsta, Gparm.NumTracePerScreen, cxScreen,
                           cyScreen, iVScrollOffset, iTitleOffset,iChanIDOffset,
                           dLastEndTime, dScreenTime, lTitleFHt, PBuf );
            DisplayMs( hdc, Nsta, PBuf, lTitleFHt, lTitleFWd, cxScreen,
                       cyScreen, Gparm.NumTracePerScreen, iVScrollOffset,
                       iTitleOffset, iChanIDOffset );
            DisplayKOPs( hdc, Trace, Nsta, iScreenToDisplay, cxScreen,
                         cyScreen, iVScrollOffset, iTitleOffset, iChanIDOffset,
                         PBuf, lTitleFHt, lTitleFWd, Gparm.NumTracePerScreen );
            ReleaseSpecificMutex( &mutsem2 );/* Let WThread have sem */
            SetScrollPos( hwnd, SB_VERT, (int) (((double) iVScrollOffset*-1.) /
                          ((cyScreen-iTitleOffset) /
                          (double) Gparm.NumTracePerScreen *
                          (double) iNumShown) * 100.), TRUE );
         }
         EndPaint( hwnd, &ps );
         break;

/* Horizontal scroll message */
      case WM_HSCROLL:
         if ( LOWORD (wParam) != SB_ENDSCROLL )
         {
            HorizontalScroll( LOWORD (wParam), &dLastEndTime,
                              dScrollHBit, dScrollHPage );
            InvalidateRect( hwnd, NULL, TRUE );
         }
         break;
	
/* Vertical scroll message */	
      case WM_VSCROLL:
         if ( LOWORD (wParam) == SB_LINEUP )
            iVScrollOffset += iScrollVertBuff;
         else if ( LOWORD (wParam) == SB_PAGEUP )
            iVScrollOffset += iScrollVertPage;
         else if ( LOWORD (wParam) == SB_LINEDOWN )
            iVScrollOffset -= iScrollVertBuff;
         else if ( LOWORD (wParam) == SB_PAGEDOWN )
            iVScrollOffset -= iScrollVertPage;
         InvalidateRect( hwnd, NULL, TRUE );
         break;

/* Close up shop and return */
      case WM_DESTROY:
         logit( "", "WM_DESTROY posted\n" );
         KillTimer( hwndWndProc, ID_TIMER2 );
         PostQuitMessage( 0 );
         break;

      default:
         return ( DefWindowProc( hwnd, msg, wParam, lParam ) );
   }
return 0;
}

      /*********************************************************
       *                     WThread()                         *
       *                                                       *
       *  This thread gets earthworm waveform messages.        *
       *                                                       *
       *********************************************************/
	   
thr_ret WThread( void *dummy )
{
   double        dAvgMS;          /* Average MS for Title */
   double        dOldestTime;     /* 1/1/70 time of oldest data in buffer */
   HYPO          HypoD;           /* Hypocenter structure from dummy file */
   int           i;
   int           iNumMS;          /* Number stns used in average MS (Title) */
   long          InBufl;          /* Memory size */
   char          line[40];        /* Heartbeat message */
   int           lineLen;         /* Length of heartbeat message */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */
   long          RawBufl;         /* Raw data buffer size (for Mwp) */
   char          szAvgMS[8], szNumMS[4], szQuake[8]; /* Used in Title */

/* Loop to read waveform messages
   ******************************/
   while ( tport_getflag( &Gparm.InRegion ) != TERMINATE )
   {
      long    lGapSize;         /* Number of missing samples (integer) */
      static  time_t  now;      /* Current time */
      static  PPICK   *pPBuf;   /* Pointer to the PBuffer being processed */
      int     rc;               /* Return code from tport_getmsg() */
      static  RTIMES  *pRTimes; /* Pointer to the proper RTimes structure */
      static  STATION *Sta;     /* Pointer to the station being processed */
      char    szType[3];        /* Incoming data format type */
	  
/* Send a heartbeat to the transport ring
   **************************************/
      time( &now );
      if ( (now - then) >= Gparm.HeartbeatInt )
      {
         then = now;
         sprintf( line, "%d %d\n", now, myPid );
         lineLen = strlen( line );
         if ( tport_putmsg( &Gparm.InRegion, &hrtlogo, lineLen, line ) !=
              PUT_OK )
         {
            logit( "et", "lpproc: Error sending heartbeat." );
            break;
         }
      }
      
/* Get a waveform from transport region
   ************************************/
      rc = tport_getmsg( &Gparm.InRegion, &getlogoW, 1, &logo, &MsgLen,
                         WaveBuf, MAX_TRACEBUF_SIZ);

      if ( rc == GET_NONE )
      {
         sleep_ew( 100 );
         continue;
      }

      if ( rc == GET_NOTRACK )
         logit( "et", "lpproc: Tracking error.\n");

      if ( rc == GET_MISS_LAPPED )
         logit( "et", "lpproc: Got lapped on the ring.\n");

      if ( rc == GET_MISS_SEQGAP )
         logit( "et", "lpproc: Gap in sequence numbers.\n");

      if ( rc == GET_MISS )
         logit( "et", "lpproc: Missed messages.\n");
                            
      if ( rc == GET_TOOBIG )
      {
         logit( "et", "lpproc: Retrieved message too big (%d) for msg.\n",
                MsgLen );
         continue;
      }                

/* If necessary, swap bytes in the message
   ***************************************/
      if ( WaveMsgMakeLocal( WaveHead ) < 0 )
      {
         logit( "et", "lpproc: Unknown waveform type.\n" );
         continue;
      }
	  
/* If sample rate is 0, get out of here before it kills program
   ************************************************************/
      if ( WaveHead->samprate == 0. )
      {
         logit( "", "Sample rate=0., %s %s\n", WaveHead->sta, WaveHead->chan );
         continue;
      }
      
 /* If we are in the ATPlayer version of lpproc, see if we should re-init
   **********************************************************************/
      if ( strlen( Gparm.ATPLineupFileLP ) > 2 )     /* Then we are */
         if ( WaveHead->starttime-(int) dLastEndTime > 3600) /* Big gap */
         {
            iNewPlayerData = 1;
            Nsta = ReadLineupFile( Gparm.ATPLineupFileLP, StaArray );
            logit( "", "Nsta=%ld\n", Nsta );
            if ( Nsta < 2 )
            {
               logit( "", "Bad Lineup File read %s\n", Gparm.ATPLineupFileLP );
               continue;
            }	    
            for ( i=0; i<Nsta; i++ )
	    {
               StaArray[i].iFirst = 1; 
               InitVar( &StaArray[i] );	  
               free( StaArray[i].plRawCircBuff );
               free( StaArray[i].plFiltCircBuff );
            }
/* Re-Allocate and init the P-pick buffer
   **************************************/
            if ( PBuf != NULL ) free( PBuf );
            InBufl = sizeof( PPICK ) * Nsta; 
            PBuf = (PPICK *) malloc( (size_t) InBufl );
            if ( PBuf == NULL )
            {
               logit( "et", "develo: Cannot re-allocate ppick buffer\n");
               continue;
	    }
            for ( i=0; i<Nsta; i++ ) InitP( &PBuf[i] );
            for ( i=0; i<Nsta; i++ )       /* Fill lat/lon part of structure */
            {
               PBuf[i].iUseMe = 0;
               PBuf[i].dLat = StaArray[i].dLat;
               PBuf[i].dLon = StaArray[i].dLon;
               GeoCent( (LATLON *) &PBuf[i] );
               GetLatLonTrig( (LATLON *) &PBuf[i] );
            }		 
         } 	 

/* Look up SCN number in the station list
   **************************************/
      Sta = NULL;								  
      for ( i=0; i<Nsta; i++ )
         if ( !strcmp( WaveHead->sta,  StaArray[i].szStation ) &&
              !strcmp( WaveHead->chan, StaArray[i].szChannel ) &&
              !strcmp( WaveHead->net,  StaArray[i].szNetID ) )
         {
            Sta = (STATION *) &StaArray[i];
            pPBuf = (PPICK *) &PBuf[i];     
            pRTimes = (RTIMES *) &RTimes[i];
            break;
         }

      if ( Sta == NULL )      /* SCN not found */
         continue;
		 
/* Check if the time stamp is reasonable.  If it is ahead of the present
   1/1/70 time, it is not reasonable. (+1. to account for int).
   *********************************************************************/
      if ( WaveHead->endtime > (double) now+1. )
      {
         iAhead[i] = 1;
         logit( "t", "%s %s endtime (%lf) ahead of present (%ld)\n",
                Sta->szStation, Sta->szChannel, WaveHead->endtime, now );
         continue;
      }
      iAhead[i] = 0;
      

/* Do this the first time we get a message with this SCN
   *****************************************************/
      if ( Sta->iFirst == 1 )
      {
         RequestSpecificMutex( &mutsem2 );   /* Protect display */      
         logit( "", "Init %s %s\n", Sta->szStation, Sta->szChannel );	  
         Sta->iFirst = 0;
         Sta->iPickStatus = 0;
         Sta->dEndTime = WaveHead->starttime - 1./WaveHead->samprate;
         Sta->dSampRate = WaveHead->samprate;
         ResetFilter( Sta );
		 		 
/* Allocate memory for raw circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples)
   ************************************************************************/
         Sta->lRawCircSize =
          (long) (WaveHead->samprate*(double)Gparm.MinutesInBuff*60.+0.1);
         RawBufl = sizeof (long) * Sta->lRawCircSize;
         Sta->plRawCircBuff = (long *) malloc( (size_t) RawBufl );
         if ( Sta->plRawCircBuff == NULL )
         {
            logit( "et", "lpproc: Can't allocate raw circ buffer for %s\n",
			       Sta->szStation );
            PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
            return;
         }
		 		 
/* Allocate memory for filt. circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples)
   **************************************************************************/
         Sta->plFiltCircBuff = (long *) malloc( (size_t) RawBufl );
         if ( Sta->plFiltCircBuff == NULL )
         {
            logit( "et", "lpproc: Can't allocate filt circ buffer for %s\n",
			       Sta->szStation );
            PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
            return;
         }
         ReleaseSpecificMutex( &mutsem2 );   /* Release sem */
      }      
      
/* If data is not in order, throw it out
   *************************************/
      if ( Sta->dEndTime >= WaveHead->starttime )
      {
         if ( Gparm.Debug ) logit( "e", "%s out of order\n", Sta->szStation );
         continue;
      }

/* If the samples are shorts, make them longs
   ******************************************/
      strcpy( szType, WaveHead->datatype );
      if ( (strcmp( szType, "i2" ) == 0) || (strcmp( szType, "s2") == 0) )
         for ( i=WaveHead->nsamp-1; i>-1; i-- )
            WaveLong[i] = (long) WaveShort[i];
			
/* Compute the number of samples since the end of the previous message.
   If (lGapSize == 1), no data has been lost between messages.
   If (1 < lGapSize <= 2), go ahead anyway.
   If (lGapSize > 2), re-initialize filter variables.
   *******************************************************************/
      lGapSize = (long) (WaveHead->samprate *
                        (WaveHead->starttime-Sta->dEndTime) + 0.5);

/* Announce gaps
   *************/
      if ( lGapSize > 2 )
      {
         int      lineLen;
         time_t   errTime;
         char     errmsg[80];
         MSG_LOGO logo;

         time( &errTime );
         sprintf( errmsg,
               "%d 1 Found %4d sample gap. Restarting station %-5s%-2s%-3s\n",
               errTime, lGapSize, Sta->szStation, Sta->szNetID,
               Sta->szChannel );
         lineLen = strlen( errmsg );
         logo.type   = Ewh.TypeError;
         logo.mod    = Gparm.MyModId;
         logo.instid = Ewh.MyInstId;
         tport_putmsg( &Gparm.InRegion, &logo, lineLen, errmsg );
         if ( Gparm.Debug )
            logit( "t", "lpproc: Restarting %-5s%-2s %-3s. lGapSize = %d\n",
                     Sta->szStation, Sta->szNetID, Sta->szChannel, lGapSize ); 

/* For big gaps reset filter 
   *************************/
         RequestSpecificMutex( &mutsem2 );   /* Protect display */      
         ResetFilter( Sta );
         Sta->dSumLDCRaw = 0.;
         Sta->dAveLDCRaw = 0.;
         Sta->dSumLDC = 0.;
         Sta->dAveLDC = 0.;
         ReleaseSpecificMutex( &mutsem2 );   /* Release sem */
      }

/* For gaps less than the size of the buffer, pad buffers with DC
   **************************************************************/
      if ( lGapSize > 1 && lGapSize <= 2 )
      {
         RequestSpecificMutex( &mutsem2 );   /* Protect display */      
         PadBuffer( lGapSize, Sta->dAveLDCRaw, &Sta->lSampIndexR,
                    Sta->plRawCircBuff, Sta->lRawCircSize );
         PadBuffer( lGapSize, Sta->dAveLDC, &Sta->lSampIndexF,
                    Sta->plFiltCircBuff, Sta->lRawCircSize );
         ReleaseSpecificMutex( &mutsem2 );   /* Release sem */
      }
	  
/* For gaps greater than the size of the buffer, re-init
   *****************************************************/
      if ( lGapSize >= Sta->lRawCircSize )
      {
         RequestSpecificMutex( &mutsem2 );   /* Protect display */      
         Sta->dSumLDCRaw = 0.;
         Sta->dAveLDCRaw = 0.;
         Sta->dSumLDC = 0.;
         Sta->dAveLDC = 0.;
         Sta->lSampIndexR = 0;
         Sta->lSampIndexF = 0;
         Sta->iPickStatus = 0;
         ReleaseSpecificMutex( &mutsem2 );   /* Release sem */
      }
      
/* In this module, lEndData is the first buffer index in this packet
   *****************************************************************/      
      RequestSpecificMutex( &mutsem2 );   /* Protect display */      
      Sta->lEndData = Sta->lSampIndexR;
      ReleaseSpecificMutex( &mutsem2 );   /* Release sem */

/* Compute DC offset for raw data
   ******************************/
      GetLDC( WaveHead->nsamp, WaveLong, &Sta->dAveLDCRaw, Sta->lFiltSamps );
			
/* Tuck raw data into proper location in buffer
   ********************************************/			
      RequestSpecificMutex( &mutsem2 );   /* Protect display */      
      PutDataInBuffer( WaveHead, WaveLong, Sta->plRawCircBuff,
                       &Sta->lSampIndexR, Sta->lRawCircSize );
	  
/* Bandpass filter the data if specified in station file (remove DC 1st)
   (This is usually necessary for broadband data, not so for LP data.
    Taper the signal for restarts, but not for continuous signal.).
   ******************************************************************/
      for ( i=0; i<WaveHead->nsamp; i++ )
         WaveLongF[i] = WaveLong[i] - (long) (Sta->dAveLDCRaw+0.5);             
		 		 
      if ( Sta->iFiltStatus == 1 )
         FilterPacket ( WaveLongF, Sta, WaveHead, Gparm.LowCutFilter,
                        Gparm.HighCutFilter, 3.*(1./Gparm.LowCutFilter) );
      ReleaseSpecificMutex( &mutsem2 );   /* Release sem */
		 
/* Process data through alarm function if set in .sta file
   (the broadband data is processed for alarms so that LP
    filter "kicks" don't set off the alarm).
   *******************************************************/
      if ( now-WaveHead->endtime < RECENT_ALARM_TIME )
         if ( Sta->iAlarmStatus >= 1 )
            SeismicAlarm( Sta, &Gparm, &Ewh, WaveHead, WaveLong );
 	   
/* Compute DC offset for filtered data
   ***********************************/
      GetLDC( WaveHead->nsamp, WaveLongF, &Sta->dAveLDC, Sta->lFiltSamps );
			         
/* Tuck filtered data into proper location in buffer
   *************************************************/			
      RequestSpecificMutex( &mutsem2 );   /* Protect display */      
      PutDataInBuffer( WaveHead, WaveLongF, Sta->plFiltCircBuff,
                       &Sta->lSampIndexF, Sta->lRawCircSize );
      ReleaseSpecificMutex( &mutsem2 );   /* Release sem */
	  
/* Send Message to WndProc (update screen every 5 seconds)
   *******************************************************/
      if ( WaveHead->endtime > dLastEndTime+5.0 )
         if ( iHold == 0 ) 
         {                                   
            RequestSpecificMutex( &mutsem2 );   /* Protect display */      
            dLastEndTime = WaveHead->endtime;
            ReleaseSpecificMutex( &mutsem2 );   /* Release sem */
            PostMessage( hwndWndProc, WM_NEW_DATA, 0, 0 );   
         }     

/* Save time of the end of the current message
   *******************************************/
      RequestSpecificMutex( &mutsem2 );   /* Protect display */      
      Sta->dEndTime = WaveHead->endtime;
      ReleaseSpecificMutex( &mutsem2 );   /* Release sem */

/* Process LP data if this station is still waiting on Rayleigh wave data
   **********************************************************************/
      if ( iLPProcessing == 1 )
      {
         RequestSpecificMutex( &mutsem1 );   /* Sem protect buffer writes */
         if ( Sta->iPickStatus == 1 )
         {
/* Compute time of oldest data in buffer */
            dOldestTime = Sta->dEndTime - ((double)
             Sta->lRawCircSize/Sta->dSampRate) + 1./Sta->dSampRate;
	     
/* Is there data in this buffer covering the R-wave window? */
            if ( pRTimes->dRStartTime < Sta->dEndTime &&
                 pRTimes->dREndTime   > dOldestTime ) 
            {		 
/* iPickStatus: 1->waiting on data in the Rayleigh wave window */
               Sta->iPickStatus = 0;

/* Process the data */
               if ( GetMs( Sta, pRTimes, pPBuf, WaveHead, &HStruct, &Gparm,
                    Sta->lHit ) == 1 )
               {
                  ReportPick( pPBuf, &Gparm, &Ewh );/* Report MS*/
                  WritePickFile( Nsta, PBuf, Gparm.LPRTFile ); /* Update File */
                  dAvgMS = ComputeAvgMS( Nsta, PBuf, &iNumMS );
                  HStruct.iNumMS = iNumMS;
                  HStruct.dMSAvg = dAvgMS;
/* If this is the same hypo as in the dummy file, update the Ms */				  
                  ReadDummyData( &HypoD, Gparm.DummyFile ); 
                  if ( IsItSameQuake( &HStruct, &HypoD ) == 1 )
                     if ( PatchDummyWithLP( &HStruct, Gparm.DummyFile ) == 0 )
                        logit( "t", "Write dummy file error in lpproc-3\n");
                  _gcvt( dAvgMS, 2, szAvgMS );
                  if ( dAvgMS - (int) dAvgMS <= 0.05 || 
                       dAvgMS - (int) dAvgMS >= 0.95 ) szAvgMS[2] = '0';
                  strcpy( szTitle, szProcessName );
                  strcat( szTitle, " - PROCESSING LP - " );
                  strcat( szTitle, szAvgMS );
                  strcat( szTitle, "-" );
                  itoaX( iNumMS, szNumMS );
                  strcat( szTitle, szNumMS );
                  strcat( szTitle, " - " );
                  itoaX( HStruct.iQuakeID, szQuake );
                  PadZeroes( 4, szQuake );
                  strcat( szTitle, szQuake );
                  SetWindowText( hwndWndProc, szTitle );  /* Display title */
               }
               Sta->lHit = 0;        /* lHit = 1 for Init, 0 for no init */
            }
         }
		 
/* Is it time to reset title bar (+60. for delay) */		 
         for ( i=0; i<Nsta; i++ )
            if ( (double) now < RTimes[i].dREndTime ) goto LoopEnd;
         for ( i=0; i<Nsta; i++ ) StaArray[i].iPickStatus = 0;
         strcpy( szTitle, szProcessName );
         SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
         for ( i=0; i<Nsta; i++ ) PBuf[i].dMSMag = 0.;
         iLPProcessing = 0;
         logit( "t", "LP processing stopped in WThread\n" );
LoopEnd:
         ReleaseSpecificMutex( &mutsem1 );   /* Let someone else have sem */
      }
   }   
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
}
