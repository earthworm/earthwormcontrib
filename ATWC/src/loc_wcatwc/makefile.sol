B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib
N = $(EW_HOME)/atwc/src/libsrc

O = loc_wcatwc.o config.o stalist.o report.o locate.o \
    $L/kom.o $L/getutil.o $L/time_ew.o $L/chron3.o $L/logit.o \
    $L/transport.o $L/sleep_ew.o $L/swap.o $L/sema_ew.o $N/geotools.o \
    $N/get_pick.o $N/mags.o $N/dummy.o $N/get_hypo.o $N/littoral.o \
    $N/fereg.o $N/mjstime.o $N/logquake.o $N/locquake.o

loc_wcatwc: $O
	cc -o $B/loc_wcatwc $O -lm -lposix4

lint:
	lint loc_wcatwc.c config.c stalist.c locate.c report.c $(GLOBALFLAGS)



# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/loc_wcatwc*
