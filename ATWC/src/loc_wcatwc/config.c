#include <stdio.h>
#include <string.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>
#include "loc_wcatwc.h"

#define ncommand 25          /* Number of commands in the config file */


 /***********************************************************************
  *                              GetConfig()                            *
  *             Processes command file using kom.c functions.           *
  *               Returns -1 if any errors are encountered.             *
  ***********************************************************************/

int GetConfig( char *config_file, GPARM *Gparm )
{
   char     init[ncommand];     /* Flags, one for each command */
   int      nmiss;              /* Number of commands that were missed */
   int      nfiles;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i=0; i<ncommand; i++ ) init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( config_file );
   if ( nfiles == 0 )
   {
      fprintf( stderr, "loc_wcatwc: Error opening configuration file <%s>\n",
               config_file );
      return -1;
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          /* Get the first token from line */

         if ( !com ) continue;             /* Ignore blank lines */
         if ( com[0] == '#' ) continue;    /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               fprintf( stderr, "loc_wcatwc: Error opening command file <%s>."
			            "\n", &com[1] );
               return -1;
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "StaDataFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->StaDataFile, str );
            init[0] = 1;
         }

         else if ( k_its( "InRing" ) )
         {
            if ( str = k_str() )
            {
               if( (Gparm->InKey = GetKey(str)) == -1 )
               {
                  fprintf( stderr, "loc_wcatwc: Invalid InRing name <%s>. "
                                   "Exiting.\n", str );
                  return -1;
               }
            }
            init[1] = 1;
         }

         else if ( k_its( "OutRing" ) )
         {
            if ( str = k_str() )
            {
               if ( (Gparm->OutKey = GetKey(str)) == -1 )
               {
                  fprintf( stderr, "loc_wcatwc: Invalid OutRing name <%s>. "
                                   "Exiting.\n", str );
                  return -1;
               }
            }
            init[2] = 1;
         }

         else if ( k_its( "AlarmRing" ) )
         {
            if ( str = k_str() )
            {
               if ( (Gparm->AlarmKey = GetKey(str)) == -1 )
               {
                  fprintf( stderr, "loc_wcatwc: Invalid AlarmRing name <%s>. "
                                   "Exiting.\n", str );
                  return -1;
               }
            }
            init[3] = 1;
         }

         else if ( k_its( "HeartbeatInt" ) )
         {
            Gparm->HeartbeatInt = k_int();
            init[4] = 1;
         }

         else if ( k_its( "NumPBuffs" ) )
         {
            Gparm->NumPBuffs = k_int();
            init[5] = 1;
			if ( Gparm->NumPBuffs > MAX_PBUFFS )
               {
                  fprintf( stderr, "Too many P buffers specified <%ld>. "
                                   "Exiting.\n", Gparm->NumPBuffs );
                  return -1;
               }
         }

         else if ( k_its( "MaxTimeBetweenPicks" ) )
         {
            Gparm->MaxTimeBetweenPicks = k_val();
            init[6] = 1;
         }

         else if ( k_its( "PsPerBuffer" ) )
         {
            Gparm->PsPerBuffer = k_int();
            init[7] = 1;
            if ( Gparm->PsPerBuffer > MAX_STATION_DATA )
               {
                  fprintf( stderr, "Too many PsPerBuffer specified <%ld>. "
                                   "Exiting.\n", Gparm->PsPerBuffer );
                  return -1;
               }
         }

         else if ( k_its( "MinPs" ) )
         {
            Gparm->MinPs = k_int();
            init[8] = 1;
			if ( Gparm->MinPs < 5 )
               {
                  fprintf( stderr, "MinPs too small <%ld>. Minimum 5. "
                                   "Exiting.\n", Gparm->MinPs );
                  return -1;
               }
         }

         else if ( k_its( "Debug" ) )
         {
            Gparm->Debug = k_int();
            init[9] = 1;
         }

         else if ( k_its( "MyModId" ) )
         {
            if ( str = k_str() )
            {
               if ( GetModId(str, &Gparm->MyModId) == -1 )
               {
                  fprintf( stderr, "loc_wcatwc: Invalid MyModId <%s>.\n", str);
                  return -1;
               }
            }
            init[10] = 1;
         }
		 
         else if ( k_its( "BValFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->szBValFile, str );
            init[11] = 1;
         }
		 
         else if ( k_its( "OldQuakes" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->szOldQuakes, str );
            init[12] = 1;
         }
		 
         else if ( k_its( "AutoLoc" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->szAutoLoc, str );
            init[13] = 1;
         }
		 
         else if ( k_its( "DummyFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->szDummyFile, str );
            init[14] = 1;
         }
		 
         else if ( k_its( "MapFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->szMapFile, str );
            init[15] = 1;
         }
		 
         else if ( k_its( "RTPFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->szRTPFile, str );
            init[16] = 1;
         }
		 
         else if ( k_its( "QLogFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->szQLogFile, str );
            init[17] = 1;
         }
		 
         else if ( k_its( "MwFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->szMwFile, str );
            init[18] = 1;
         }
		 
         else if ( k_its( "LocFilePath" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->szLocFilePath, str );
            init[19] = 1;
         }
		 
         else if ( k_its( "DepthFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->szDepthDataFile, str );
            init[20] = 1;
         }

         else if ( k_its( "SouthernLat" ) )
         {
            Gparm->SouthernLat = k_val();
            init[21] = 1;
         }

         else if ( k_its( "NorthernLat" ) )
         {
            Gparm->NorthernLat = k_val();
            init[22] = 1;
         }

         else if ( k_its( "WesternLon" ) )
         {
            Gparm->WesternLon = k_val();
            init[23] = 1;
         }

         else if ( k_its( "EasternLon" ) )
         {
            Gparm->EasternLon = k_val();
            init[24] = 1;
         }

/* An unknown parameter was encountered
   ************************************/
         else
         {
            fprintf( stderr, "loc_wcatwc: <%s> unknown parameter in <%s>\n",
                    com, config_file );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            fprintf( stderr, "loc_wcatwc: Bad <%s> command in <%s>.\n", com,
                     config_file );
            return -1;
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss > 0 )
   {
      fprintf( stderr, "loc_wcatwc: ERROR, no " );
      if ( !init[0] ) fprintf( stderr, "<StaDataFile> " );
      if ( !init[1] ) fprintf( stderr, "<InRing> " );
      if ( !init[2] ) fprintf( stderr, "<OutRing> " );
      if ( !init[3] ) fprintf( stderr, "<AlarmRing> " );
      if ( !init[4] ) fprintf( stderr, "<HeartbeatInt> " );
      if ( !init[5] ) fprintf( stderr, "<NumPBuffs> " );
      if ( !init[6] ) fprintf( stderr, "<MaxTimeBetweenPicks> " );
      if ( !init[7] ) fprintf( stderr, "<PsPerBuffer> " );
      if ( !init[8] ) fprintf( stderr, "<MinPs> " );
      if ( !init[9] ) fprintf( stderr, "<Debug> " );
      if ( !init[10] ) fprintf( stderr, "<MyModId> " );
      if ( !init[11] ) fprintf( stderr, "<BValFile> " );
      if ( !init[12] ) fprintf( stderr, "<OldQuakes> " );
      if ( !init[13] ) fprintf( stderr, "<AutoLoc> " );
      if ( !init[14] ) fprintf( stderr, "<DummyFile> " );
      if ( !init[15] ) fprintf( stderr, "<MapFile> " );
      if ( !init[16] ) fprintf( stderr, "<RTPFile> " );
      if ( !init[17] ) fprintf( stderr, "<QLogFile> " );
      if ( !init[18] ) fprintf( stderr, "<MwFile> " );
      if ( !init[19] ) fprintf( stderr, "<LocFilePath> " );
      if ( !init[20] ) fprintf( stderr, "<DepthFile> " );
      if ( !init[21] ) fprintf( stderr, "<NorthernLat> " );
      if ( !init[22] ) fprintf( stderr, "<SouthernLat> " );
      if ( !init[23] ) fprintf( stderr, "<WesternLon> " );
      if ( !init[24] ) fprintf( stderr, "<EasternLon> " );
      fprintf( stderr, "command(s) in <%s>. Exiting.\n", config_file );
      return -1;
   }
   return 0;
}


 /***********************************************************************
  *                              LogConfig()                            *
  *                                                                     *
  *                   Log the configuration parameters                  *
  ***********************************************************************/

void LogConfig( GPARM *Gparm )
{
   logit( "", "\n" );
   logit( "", "StaDataFile:        %s\n",    Gparm->StaDataFile );
   logit( "", "InKey:              %d\n",    Gparm->InKey );
   logit( "", "OutKey:             %d\n",    Gparm->OutKey );
   logit( "", "AlarmKey:           %d\n",    Gparm->AlarmKey );
   logit( "", "HeartbeatInt:       %d\n",    Gparm->HeartbeatInt );
   logit( "", "NumPBuffs:          %d\n",    Gparm->NumPBuffs );
   logit( "", "PsPerBuffer:        %d\n",    Gparm->PsPerBuffer );
   logit( "", "MaxTimeBetweenPicks:%lf\n",   Gparm->MaxTimeBetweenPicks );
   logit( "", "MinPs:              %d\n",    Gparm->MinPs );
   logit( "", "Debug:              %d\n",    Gparm->Debug );
   logit( "", "MyModId:            %u\n",    Gparm->MyModId );
   logit( "", "BValFile:           %s\n",    Gparm->szBValFile );
   logit( "", "OldQuakes:          %s\n",    Gparm->szOldQuakes );
   logit( "", "AutoLoc:            %s\n",    Gparm->szAutoLoc );
   logit( "", "DummyFile:          %s\n",    Gparm->szDummyFile );
   logit( "", "MapFile:            %s\n",    Gparm->szMapFile );
   logit( "", "RTPFile:            %s\n",    Gparm->szRTPFile );
   logit( "", "MwFile:             %s\n",    Gparm->szMwFile );
   logit( "", "LocFilePath:        %s\n",    Gparm->szLocFilePath );
   logit( "", "DepthFile  :        %s\n",    Gparm->szDepthDataFile );
   logit( "", "NorthernLat:        %lf\n",   Gparm->NorthernLat );
   logit( "", "SouthernLat:        %lf\n",   Gparm->NorthernLat );
   logit( "", "WesternLon:         %lf\n",   Gparm->WesternLon );
   logit( "", "EasternLon:         %lf\n",   Gparm->EasternLon );
   logit( "", "QLogFile:           %s\n\n",  Gparm->szQLogFile );
   return;
}
