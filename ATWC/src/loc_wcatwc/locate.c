  /***********************************************************************
  *                                                                      *
  * LOCATE.C                                                             *
  *                                                                      *
  * These functions call location functions in libsrc.  The first job of *
  * these functions is to sort the incoming Ps into buffers.  The        *
  * functions attempt to put the Ps into buffers which contain only Ps   *
  * from the same event.  This is not an easy task for some station      *
  * geometries.                                                          *
  *                                                                      *
  * Ps are sorted based on times.  Before any locations are made within  *
  * the buffers, Ps are sorted based on the possibility of the times     *
  * being from the same quake.  If the time difference between the new P *
  * and any other P within the buffer is less than the maximum which     *
  * could be expected for the distance between the two, the P is placed  *
  * in that buffer.  A second check is also made: if the P time is       *
  * greater than a specified number of minutes from the last station in  *
  * the buffer, it will not be included in that buffer.                  *
  *                                                                      *
  * When a set number of P-times are loaded into the buffer, functions   *
  * will be called to locate a quake based on those P-times.  From that  *
  * point on (assuming there was a good solution), Ps will be loaded into*
  * the buffer based on if the P-time fits the location within some      *
  * tolerance.  If a good solution was not arrived at, Ps are continually*
  * added with new locations attempted with every other P.  When over a  *
  * certain number of Ps are in the buffer, the highest residuals are    *
  * thrown out (and set into a different buffer). Also, when a good loc  *
  * is attained in a buffer, the other buffers are checked for Ps which  *
  * may fit that location.  Any Ps which fit are moved in.               *
  *                                                                      *
  * A common problem is that the same quake may be located in two        *
  * different buffers.  This can happen due to phases other than the P   *
  * (such as the pP or sP) being picked up as the P by the P-picker      *
  * instead of the real P.  A routine is called to check for similar     *
  * quakes in different buffers.  Only the buffer with the most Ps is    *
  * sent to the Hypo_Ring.                                               *
  *
  * Made into earthworm module 2/2001.                                   *
  *                                                                      *
  ************************************************************************/
  
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include "loc_wcatwc.h"

/* fPP defined in iaspei91.h.  Must be defined as extern for compilation */
extern float fPP[27436];  

extern char  szStnRem[MAX_PBUFFS][MAX_STN_REM][TRACE_STA_LEN];/* Removed stns */

     /**************************************************************
      *                        AddInMw()                           *
      *                                                            *
      * This function reads the Mw data file, determines if the    *
      * Mws are from the same quake as being located, and adds Mw  *
      * information to arrays.                                     *
      *                                                            *
      * Arguments:                                                 *
      *  pszFile     File with Mw results                          *
      *  pHypo       Computed hypocentral parameters               * 
      *                                                            *
      **************************************************************/
      
void AddInMw( char *pszFile, HYPO *pHypo )
{
   double  dMw[MAX_STATIONS];           /* Mw from data file */
   double  dMwSumMod;                   /* Mw summation */
   FILE    *hFile;                      /* Mw File handle */
   int     i;
   int     iMwCountMod;                 /* # Mws from file(within .6 of avg.) */
   HYPO    HypoT;                       /* Quake params. in Mw file */
   char    szNet[6], szStn[6], szChn[6];/* Stn, Channel from Mw file */

/* Initialize Mw variables */
   pHypo->dMwAvg = 0.;
   pHypo->iNumMw = 0;
   iMwCountMod = 0;
   dMwSumMod = 0.;

/* Try to open the Mw file */
   if ( (hFile = (FILE *) fopen( pszFile, "r" )) == NULL )
   {
      logit( "t", "Couldn't open %s\n", pszFile );
      return;
   }
    
/* Read Mw quake location */
   fscanf( hFile, "%lf\n", &HypoT.dLat );
   fscanf( hFile, "%lf\n", &HypoT.dLon );
   fscanf( hFile, "%lf\n", &HypoT.dOriginTime );  /* O-time in 1/1/70 seconds */

/* Is the quake that produced the Mws the same as that located here? */
   if ( IsItSameQuake( &HypoT, pHypo ) )
   {
      while ( !feof( hFile ) )
      {                                 /* Yes, so add in Mw */
         fscanf( hFile, "%s %s %s %lf\n", szStn, szChn, szNet,
                 &dMw[pHypo->iNumMw] );
         pHypo->dMwAvg += dMw[pHypo->iNumMw];
	 pHypo->iNumMw += 1;
      }
      if ( pHypo->iNumMw ) pHypo->dMwAvg /= (double) pHypo->iNumMw;
      if ( pHypo->dMwAvg > 0. )
         for ( i=0; i<pHypo->iNumMw; i++ )
            if ( dMw[i] > 0. )
               if ( fabs( pHypo->dMwAvg-dMw[i] ) < 0.6 )
               {
                  iMwCountMod++;
                  dMwSumMod += dMw[i];
               }
      if ( iMwCountMod ) pHypo->dMwAvg = dMwSumMod / (double) iMwCountMod;
      if ( iMwCountMod ) pHypo->iNumMw = iMwCountMod;
   }
fclose (hFile);     	/* Close file and return */
return;
}

     /**************************************************************
      *                     CheckPBuffTimes()                      *
      *                                                            *
      * After a solution has been made, see if any other P's in    *
      * other buffers go with this location.  If so, copy them into*
      * the buffer with the latest solution.                       *
      *                                                            *
      * Also, remove Ps that had high residuals and find a new     *
      * buffer for them.                                           *
      *                                                            *
      * Sept., 2004: When picks are dropped due to bad residual    *
      *              (but not too bad - i.e. bad P-pick) don't move*
      *              to another buffer; just eliminate it.         *
      *                                                            *
      * Arguments:                                                 *
      *  pPBuf       P-pick buffers to copy PStruct into           *
      *  iBufCnt     Number of P-picks in each buffer so far       *
      *  Hypo        Computed hypocenters for each buffer          *
      *  iActive     Last buffer to get a hypocenter solution      *
      *  Gparm       Configuration parameter structure             *
      *  iLastCnt    P buffer counter                              *
      *  iNumRem     Number of stations permanently removed        *
      *                                                            *
      **************************************************************/
	  
void CheckPBuffTimes( PPICK **pPBuf, int iPBufCnt[], HYPO Hypo[], int iActive,
                      GPARM *Gparm, int iLastCnt[], int iNumRem[] )
{
   AZIDELT azidelt;                /* Distance/azimuth between 2 points */
   double  dCorr;                  /* Distance correction for P tables */
   double  dMin;                   /* Minimum P-time in all buffers */
   double  dTimeExpected;          /* P-time for this station */
   double  dTTDif;                 /* P-time difference between 2 points */
   double  dTTDifMax;              /* Max P-time diff. given delta for 2 pts. */
   double  dTTDifQ;                /* Expected P time for this point */
   int     i, j, k, kk, iNum, iTemp;/* Index counters */
   int     iAlreadyUsed;           /* 1->This buffer already has been ordered */
   int     iBuff[MAX_PBUFFS];      /* Order of buffers (Starting with buffer
                                      with the most picks, buffers with a good
                                      location are designated -2) */
   static  int  iDLev;             /* Index of 0 deg. in desired depth level */
   int     iMatch, iPCnt, iTDifCnt;/* Flags to indicate correct buffer */
   int     iMin;                   /* Index of buffer with minimum P-time */
   long    lTime;                  /* Preset 1/1/70 time in seconds */   
	    
/* Remove picks with high residual */	    
   if ( iPBufCnt[iActive] > Gparm->MinPs+1 )
      for ( k=0; k<iPBufCnt[iActive]; k++ )
      {
/* First, check to see if this looks like a bad P-pick for this event.
   That is, the pick should be associated with this event, but the P-time
   is off by 5-20 seconds (but the pick was not forced in by manual pick
   in hypo_display).  If it does seem like a bad P, remove it and send it
   nowhere so that it does not join up with other bad Ps to make
   a false location in another buffer. */   	  
         if ( pPBuf[iActive][k].iUseMe < 2  &&
              pPBuf[iActive][k].dPTime > 0. &&
              Hypo[iActive].iGoodSoln >= 2  &&
              Hypo[iActive].iNumPs    >= 10 &&
              fabs( pPBuf[iActive][k].dRes ) > 5. &&
              fabs( pPBuf[iActive][k].dRes ) < 20. )
         {
            logit( "", "Pick %s removed completely\n",
             pPBuf[iActive][k].szStation );
            strcpy( szStnRem[iActive][iNumRem[iActive]],
                    pPBuf[iActive][k].szStation );
            iNumRem[iActive]++;
            logit( "", "%s added to Buffer %ld - %ld removed stations\n",
             szStnRem[iActive][iNumRem[iActive]-1], iActive, iNumRem[iActive] );
            goto Remove;
         }
			  
/* Now check to see if this is way out and not likely just a bad P, but
   probably associated with another quake.  Then, try other buffers. */
         if (((((fabs( pPBuf[iActive][k].dRes ) > 10. &&
             Hypo[iActive].iGoodSoln >= 1) ||
            (Hypo[iActive].iNumPs >= 10 &&
             Hypo[iActive].iGoodSoln >= 2 &&
             fabs( pPBuf[iActive][k].dRes ) > 5.)) &&
             pPBuf[iActive][k].iUseMe < 2) ||
            (fabs( pPBuf[iActive][k].dRes ) > 180.)) &&
             pPBuf[iActive][k].dPTime > 0. )
         {
/* Find where to put pick */	 
/* Check all buffers to see if this fits in an existing solution */		 		 
            for ( i=0; i<Gparm->NumPBuffs; i++ ) iBuff[i] = -1;   
            for ( i=0; i<Gparm->NumPBuffs; i++ )
            {
               iTemp = iActive+i;
               if ( iTemp >= Gparm->NumPBuffs ) iTemp -= Gparm->NumPBuffs;
	  
/* Do we have a good sol'n for this buffer */
               if ( Hypo[iTemp].iNumPs >= Gparm->MinPs+2 &&
                    Hypo[iTemp].iGoodSoln >= 2 && iTemp != iActive )
               {
                  iBuff[iTemp] = -2;

/* Get depth level to use in travel time table */
                  iDLev = (int) ( (Hypo[iTemp].dDepth+0.01) /
                          (double) IASP_DEPTH_INC) * IASP_NUM_PER_DEP;
	  
/* Check the P-time with what would be expected for this quake; if it
   fits keep it.  Otherwise, go to next buffer */	  
                  azidelt = GetDistanceAz( (LATLON *) &Hypo[iTemp],
                                           (LATLON *) &pPBuf[iActive][k] );
                  dTTDif = pPBuf[iActive][k].dPTime -
                           Hypo[iTemp].dOriginTime;
                  dCorr = azidelt.dDelta*(1./IASP_DIST_INC) - 
                   floor( azidelt.dDelta*(1./IASP_DIST_INC) );
                  dTTDifQ = fPP[(int) (azidelt.dDelta*(1./IASP_DIST_INC))+
                   iDLev] + dCorr*(fPP[(int) (azidelt.dDelta*
                   (1./IASP_DIST_INC))+1+iDLev] - fPP[(int)
                   (azidelt.dDelta*(1./IASP_DIST_INC))+iDLev]);				
				   
/* Is this station already in buffer? If it is, go to next buffer*/		 
                  for ( j=0; j<iPBufCnt[iTemp]; j++ )
                     if ( !strcmp( pPBuf[iTemp][j].szStation,
                                   pPBuf[iActive][k].szStation ) &&
                          !strcmp( pPBuf[iTemp][j].szChannel,
                                   pPBuf[iActive][k].szChannel ) )
                        goto NextBuffer;
					
                  if ( fabs( dTTDifQ - dTTDif ) <= 5. )
                  {
                     CopyPBuf( &pPBuf[iActive][k],
                               &pPBuf[iTemp][iPBufCnt[iTemp]] );
                     iPBufCnt[iTemp]++;
                     if ( Gparm->Debug )
                        logit( "", "Pick Added in Check (post7) %s %lf - %ld\n",
                         pPBuf[iTemp][iPBufCnt[iTemp]-1].szStation,
                         pPBuf[iTemp][iPBufCnt[iTemp]-1].dPTime, iTemp );
                     goto Remove;
                  }
               }
NextBuffer:;}
	  
/* If we don't have a sol'n which matches this pick, try buffers to see which
   it could fit in. But first, order the buffers by Number of Ps. */
            for ( i=0; i<Gparm->NumPBuffs; i++ )
               if ( iBuff[i] != -2 ) /* Skip those where there is already a soln */
               {
                  iNum = 0;
                  for ( j=0; j<Gparm->NumPBuffs; j++ )
                     if ( iBuff[j] != -2 )
                     {
                        iAlreadyUsed = 0;
                        for ( kk=0; kk<i; kk++ )
                           if ( iBuff[kk] == j ) iAlreadyUsed = 1;
                        if ( iAlreadyUsed == 0 )
                           if ( iPBufCnt[j] > iNum )
                           {
                              iBuff[i] = j;
                              iNum = iPBufCnt[j];
                           }
                     }
               }
/* It didn't fit an existing solution, so see which buffer it can fit in */	  
            for ( i=0; i<Gparm->NumPBuffs; i++ )
            {
               iTemp = iBuff[i];
			   
/* If we don't have a sol'n yet in buffer or there are not many picks */	  
               if ( iTemp >= 0  && iTemp != iActive )
               {              /* -2->good soln for buffer, -1->Buffer empty */
                  iMatch = 0;
                  iPCnt = 0;
                  iTDifCnt = 0;

/* Get depth level to use in travel time table */
                  iDLev = (int) ( ((double) DEPTHKM+0.01) /
                          (double) IASP_DEPTH_INC) * IASP_NUM_PER_DEP;
			  
/* Here, check buffers by MaxTimeBetweenPicks and P-time differences */	  
                  for ( j=0; j<iPBufCnt[iTemp]; j++ )
                  {
                     azidelt = GetDistanceAz( (LATLON *) &pPBuf[iActive][k],
                                              (LATLON *) &pPBuf[iTemp][j] );
                     dTTDif = fabs( pPBuf[iActive][k].dPTime -
                                    pPBuf[iTemp][j].dPTime );
                     dTTDifMax = fPP[(int) (azidelt.dDelta*
                                 (1./IASP_DIST_INC))+1+iDLev];
                     if ( dTTDif > dTTDifMax && pPBuf[iTemp][j].dPTime > 0. )
                        iPCnt++;
                     if ( dTTDif > Gparm->MaxTimeBetweenPicks*60. ) iTDifCnt++;
                     if ( !strcmp( pPBuf[iActive][k].szStation,
                                   pPBuf[iTemp][j].szStation ) ) iMatch = 1;
                  }
/* Are we within the constraints to add the pick to this buffer */
                  if ( iMatch == 0 && iPCnt == 0 &&
                       iTDifCnt != iPBufCnt[iTemp] )
			
/* We are within the constraints to add the pick to this buffer */
                  {
                     CopyPBuf( &pPBuf[iActive][k],
                               &pPBuf[iTemp][iPBufCnt[iTemp]] );
                     iPBufCnt[iTemp]++;
                     if ( Gparm->Debug )
                        logit( "", "Pick Added in Check %s %lf - %ld\n",
                         pPBuf[iTemp][iPBufCnt[iTemp]-1].szStation,
                         pPBuf[iTemp][iPBufCnt[iTemp]-1].dPTime, iTemp );
                     goto Remove;
                  }
               }
            }

/* If we get here, the pick doesn't fit in any existing buffers.  
   Try to find the first buffer available with no picks and put it there. */
            for ( i=0; i<Gparm->NumPBuffs; i++ )
            {
               iTemp = iActive+i;
               if ( iTemp >= Gparm->NumPBuffs ) iTemp -= Gparm->NumPBuffs;
               if ( iTemp != iActive )
               {
                  if ( iPBufCnt[iTemp] == 0 )
                  {
                     CopyPBuf( &pPBuf[iActive][k],
                               &pPBuf[iTemp][iPBufCnt[iTemp]] );
                     iPBufCnt[iTemp]++;
                     if ( Gparm->Debug )
                        logit( "", "Pick Added in Check (new buf) %s %lf - %ld\n",
                         pPBuf[iTemp][iPBufCnt[iTemp]-1].szStation,
                         pPBuf[iTemp][iPBufCnt[iTemp]-1].dPTime, iTemp );
                     goto Remove;
                  }
               } 
            }
			   
/* If we get here, all P buffers must have some Ps in them, so one buffer must
   be cleared out. Find the one with the oldest P's and re-init that one. */
            iMin = 0;
            dMin = 1.E20;
            time( &lTime );
            for ( i=0; i<Gparm->NumPBuffs; i++ )
               if ( Hypo[i].dMSAvg == 0. ||
                  ((double) lTime-Hypo[i].dOriginTime) > 7200.)
                  if ( i != iActive )
                     for ( j=0; j<iPBufCnt[i]; j++ )
                        if ( pPBuf[i][j].dPTime < dMin &&
                             pPBuf[i][j].dPTime > 0.0)
                        {
                           dMin = pPBuf[i][j].dPTime;
                           iMin = i;
                        }
            iPBufCnt[iMin] = 0;
            iNumRem[iMin] = 0;
            iLastCnt[iMin] = 0;
            Hypo[iMin].iVersion = 1;
            Hypo[iMin].iAlarmIssued = 0;
            Hypo[iMin].iQuakeID += Gparm->NumPBuffs;
            if ( Hypo[iMin].iQuakeID >= 10000 ) Hypo[iMin].iQuakeID -= 10000;
            for ( i=0; i<Gparm->PsPerBuffer; i++ ) InitP( &pPBuf[iMin][i] );
            InitHypo( &Hypo[iMin] );
            CopyPBuf( &pPBuf[iActive][k], &pPBuf[iMin][iPBufCnt[iMin]] );
            iPBufCnt[iMin]++;
            if ( Gparm->Debug )
               logit( "", "Pick Added in Check (zeroed buf) %s %lf - %ld\n",
                pPBuf[iMin][iPBufCnt[iMin]-1].szStation,
                pPBuf[iMin][iPBufCnt[iMin]-1].dPTime, iMin );
	    
/* We've found a home for the bad P, so remove it from the active buffer */
Remove:     RemoveP( pPBuf[iActive], &iPBufCnt[iActive], k, &iLastCnt[iActive]);
            k--;             /* Yes, the loop index is being decremented */
         }
      }

/* Next, bring in other picks if we're pretty sure this is a good location */
   if ( Hypo[iActive].iNumPs >= Gparm->MinPs+1 && Hypo[iActive].iGoodSoln >= 2 )
   {
/* Get depth level to use in travel time table */
      iDLev = (int) ( (Hypo[iActive].dDepth+0.01) /
              (double) IASP_DEPTH_INC) * IASP_NUM_PER_DEP;
   
/* Loop through all other buffers */
      for ( i=0; i<Gparm->NumPBuffs; i++ )
         if ( i != iActive )
            for ( j=0; j<iPBufCnt[i]; j++ )
/* If pick already included in a good sol'n, don't move it around */			
               if ( ((Hypo[i].iGoodSoln >= 2 && pPBuf[i][j].iUseMe < 1) ||
                      Hypo[i].iGoodSoln < 2) && pPBuf[i][j].dPTime > 0. &&
                      Hypo[i].iNumPs < MAX_SCAVENGE )
               {				   
/* Is this station already in buffer? If it is, go to next buffer*/		 
                  for ( k=0; k<iPBufCnt[iActive]; k++ )
                     if ( !strcmp( pPBuf[i][j].szStation,
                                   pPBuf[iActive][k].szStation ) &&
                          !strcmp( pPBuf[i][j].szChannel,
                                   pPBuf[iActive][k].szChannel ) )
                        goto NextP;
						
/* Compute what the P-time should be for this site from the last computed
   quake */			
                  azidelt = GetDistanceAz( (LATLON *) &Hypo[iActive],
                                           (LATLON *) &pPBuf[i][j] );
                  dCorr = azidelt.dDelta*(1./IASP_DIST_INC) - 
                   floor( azidelt.dDelta*(1./IASP_DIST_INC) );
                  dTimeExpected = Hypo[iActive].dOriginTime +
                   fPP[(int) (azidelt.dDelta*(1./IASP_DIST_INC))+iDLev] +
                   dCorr*(fPP[(int) (azidelt.dDelta*(1./IASP_DIST_INC))+1+iDLev]
                   - fPP[(int) (azidelt.dDelta*(1./IASP_DIST_INC))+iDLev]);				
/* If the P-time at this station fits what is expected, it maybe associated
   with this quake.  Add it to the Active buffer and remove from old. */				
                  if ( fabs( dTimeExpected-pPBuf[i][j].dPTime ) <= 5. )
                  {
                     CopyPBuf( &pPBuf[i][j], &pPBuf[iActive][iPBufCnt[iActive]] );
                     iPBufCnt[iActive]++;
                     if ( Gparm->Debug )
                        logit( "o", "Moved %s from %ld to %ld; timedif=%lf\n",
                         pPBuf[i][j].szStation, i, iActive,
                         dTimeExpected-pPBuf[i][j].dPTime );
                     RemoveP( pPBuf[i], &iPBufCnt[i], j, &iLastCnt[i] );
                  }
NextP:;        }
   }
}			

      /******************************************************************
       *                          LocateQuake()                         *
       *                                                                *
       *  This function call the functions necessary to locate an       *
       *  earthquake given a set of P arrival times.  The hypocenter    *
       *  parameters are returned in a structure argument.              *
       *                                                                *
       *  April, 2006: Added region of reliability to locator for local *
       *               versions of the locator.                         *
       *               Also, removed RESPOND due to 24x7 coverage.      *
       *                                                                *
       *  Arguments:                                                    *
       *   pPBuf            One of the P Buffer structures              *
       *   iPBufCnt         Number of Ps in this structure              *
       *   Gparm            Configuration parameter structure           *
       *   pHypo            Computed hypocentral parameters             *
       *   iIndex           pPbuf index within array                    *
       *   Ewh              Earthworm.d defines                         *
       *   pcity            Array of reference city information         *
       *   iLoc             1=locate quake, 0=mags only                 *
       *   HypoStruct       Computed hypocenters for each buffer        *
       *   iBufCntStruct    Number of P-picks in each buffer so far     *
       *   pcityEC          Array of reference eastern city information *
       *   pEqDepth         Array of depth data                         *
       *                                                                *
       *  Return:                                                       *
       *   -1 if problem in location; otherwise ActiveBuffer index      *
       *                                                                *
       ******************************************************************/
	   
int LocateQuake( PPICK *pPBuf, int iPBufCnt, GPARM *Gparm, HYPO *pHypo,
                 int iIndex, EWH *Ewh, CITY *pcity, int iLoc, HYPO HypoStruct[],
                 int iBufCntStruct[], CITY *pcityEC, EQDEPTHDATA pEqDepth[] )
{
   FILE    *hFile;          /* File handle */
   static  HYPO    HypoLast;/* Hypo parameters from last alarmed quake */
   int     i, iCnt;
   int     iBadPIndices[16];/* Indices of stations tossed out by FindBadP */
   int     iBufferMatch;    /* Index of P buffer with hypocenter same as this */
   static int iInRegion;    /* 1->location within proper region; 0->otherwise */
   int     iMatch;          /* index of present quake in OldQuake File */										
   int     iNewAlarm;       /* 1->location/mag different than last alarm */
   int     iNight;          /* 1 if quake occurred during Alaska night (10-7) */
   int     iPCnt;           /* number of useable P-data points */										
   static int iRegion;      /* Quake epicenter region (WC/ATWC AOR) */
   static int iRespond;     /* 1=>this quake was large enough for a response */
   int     iSendIt;         /* 1->Big enough to put on alarm, 0->Don't send */
   static LATLON  ll, ll2;  /* Quake lat/lon in geographic coords */
   char    *pszLogDir;      /* Earthworm log directory */
   long    lTime;           /* 1/1/70 time */
   static  OLDQUAKE OldQuake[MAX_QUAKES]; /* Previous auto-located quakes */
   char    szFileName[64];  /* File to dump P data */
   char    szPageMsg[128];  /* HYPO71SUM2K message for output */
   char    szMessage[256];  /* Pager message */
   char    szTemp[16];      /* Temporary string */
   char    szTWCMsg[MAX_HYPO_SIZE]; /* TYPE_HYPOTWC message for output */
   struct tm *tm;           /* time structure */
   
   if ( iLoc == 1 ) pHypo->iMagOnly = 0;
   else             pHypo->iMagOnly = 1;
   
/* Reset previously automatically eliminated stations */
   if ( iLoc == 1 )
   {
      for ( i=0; i<iPBufCnt; i++ )
         if ( pPBuf[i].iUseMe == 0 ) pPBuf[i].iUseMe = 1;

/* Are there are enough p-times to locate quake? */ 
      iPCnt = 0;
      for ( i=0; i<iPBufCnt; i++ )
         if ( pPBuf[i].iUseMe > 0 && pPBuf[i].dPTime > 0. ) iPCnt++;
      if ( iPCnt < Gparm->MinPs )
      {
         logit( "", "Not enough Ps; need %ld, have %ld; buffer %ld; PCnt %ld\n", 
                     Gparm->MinPs, iPBufCnt, iIndex, iPCnt );
         if ( Gparm->Debug )
            for ( i=0; i<iPBufCnt; i++ )
               logit( "", "i=%ld, iUseMe=%ld, PTime=%lf\n", i, pPBuf[i].iUseMe,
                      pPBuf[i].dPTime );
         return -1;
      }
	  
/* Order Ps within array from first to last */	  
      qsort( (void *) pPBuf, iPBufCnt, sizeof( PPICK ), SortAllByPTime );
	
/* First locate quake letting initial loc. be first P station */
      pHypo->iDepthControl = 3;                           /* Fixed at 40km */
      pHypo->dDepth = DEPTHKM;                       /* Set for 40 kilometers */
      InitHypo( pHypo );                                  /* Init structure */
      InitialLocator( iPBufCnt, 3, 1, pPBuf, pHypo, 0., 0. );/* Get start pt. */
      QuakeSolveIasp( iPBufCnt, pPBuf, pHypo, pEqDepth, 1 ); /* Solve location */
      IsItGoodSoln( iPBufCnt, pPBuf, pHypo, Gparm->MinPs );/* Check Residual */
   
/* See if location was OK */
      if ( pHypo->iGoodSoln != 3 )
   
/* If not, re-locate using tripartite initial locator */
      {
         InitHypo( pHypo );		
         InitialLocator( iPBufCnt, 3, 2, pPBuf, pHypo, 0., 0. );
         QuakeSolveIasp( iPBufCnt, pPBuf, pHypo, pEqDepth, 1 );
         IsItGoodSoln( iPBufCnt, pPBuf, pHypo, Gparm->MinPs );
      }
   
/* If this solution was no good, try the Bad P finder
   (NOTE: NumPicks <= 6 => 1 knock-out; <= 10 => 2 ko; > 10 => 3 */
      if ( pHypo->iGoodSoln != 3 && MAX_TO_KO > 0 )
      {
         InitHypo( pHypo );		
         FindBadPs( iPBufCnt, 3, pPBuf, pHypo, 0., 0., Gparm->MinPs, pEqDepth );
         IsItGoodSoln( iPBufCnt, pPBuf, pHypo, Gparm->MinPs );
      }
   
/* See if location OK, there are many picks, and good azimuthal control;
   if so, let the depth float. */
      if ( pHypo->iGoodSoln > 0 && pHypo->iAzm >= 180 &&
           pHypo->iNumPs >= Gparm->MinPs+2 )
      {
         pHypo->iDepthControl = 4;                       /* Float */
/* Bring eliminated picks back in */
         iCnt = 0;
         for ( i=0; i<iPBufCnt; i++ )
            if ( pPBuf[i].iUseMe == 0 && pPBuf[i].dRes < 20. )
            {
               pPBuf[i].iUseMe = 1;
               iBadPIndices[iCnt] = i;
               iCnt++;
               if ( iCnt > 15 ) break;
            }
         QuakeSolveIasp( iPBufCnt, pPBuf, pHypo, pEqDepth, 1 );
         IsItGoodSoln( iPBufCnt, pPBuf, pHypo, Gparm->MinPs );
/* If this solution bombs, go back to fixed depth */
         if  ( pHypo->iGoodSoln < 2 )
         {
/* Remove originally eliminated picks */
            for ( i=0; i<iCnt; i++ )
               pPBuf[iBadPIndices[i]].iUseMe = 0;
            pHypo->iDepthControl = 3;                 /* Fixed at 40km */
            pHypo->dDepth = DEPTHKM;                  /* Set for 40 km */
            InitialLocator( iPBufCnt, 3, 1, pPBuf, pHypo, 0., 0. );
            QuakeSolveIasp( iPBufCnt, pPBuf, pHypo, pEqDepth, 1 );/* Solve location */
            IsItGoodSoln( iPBufCnt, pPBuf, pHypo, Gparm->MinPs );/* Check Res */
         }
      }
   }
   else    /* Zero res for MS only data */
   {
      for ( i=0; i<iPBufCnt; i++ )
         if ( pPBuf[i].dPTime <= 1. ) pPBuf[i].dRes = 0.;
   }
   
/* We must have a solution now, good or not, so get magnitudes */   
   ZeroMagnitudes( pPBuf, Gparm->PsPerBuffer );
   ComputeMagnitudes( iPBufCnt, pPBuf, pHypo );
   AddInMw( Gparm->szMwFile, pHypo );
   GetPreferredMag( pHypo );
   
/* Is this the same quake as is being located in another buffer? */
   iBufferMatch = -1;
   for ( i=0; i<Gparm->NumPBuffs; i++ )
      if ( i != iIndex && (HypoStruct[i].dLat != 0. || HypoStruct[i].dLon != 0.) )	  
         if ( IsItSameQuake( pHypo, &HypoStruct[i] ) == 1 )
            if ( iBufCntStruct[i] > iPBufCnt )   /* Who has more picks */
               iBufferMatch = i;
   if ( iBufferMatch >= 0 ) logit( "et", "Buffer match - %ld\n", iBufferMatch );

/* Is the quake location within the region that this locator is solving? */
   iInRegion = 0;
   GeoGraphic( &ll, (LATLON *) pHypo );
   iRegion = GetRegion( ll.dLat, ll.dLon );     /* For use later */
   if ( ll.dLon > 180. ) ll.dLon -= 360.;
   if ( Gparm->SouthernLat <= ll.dLat &&
        Gparm->NorthernLat >= ll.dLat &&
        Gparm->WesternLon <= ll.dLon &&
        Gparm->EasternLon >= ll.dLon ) iInRegion = 1;
   
/* Re-write the file monitored by ANALYZE with time of first P */
   if ( iLoc == 1 && pHypo->iVersion == 1 && iBufferMatch < 0 && iInRegion == 1 )
   {
      if ( (hFile = fopen( Gparm->szAutoLoc, "w" )) != NULL )
      {
         fprintf( hFile, "%lf\n", pHypo->dFirstPTime );
         fclose( hFile );
      }
      else
         logit( "t", "Failed to open %s in AutoLoc\n", Gparm->szAutoLoc );
   }
	
/* If good sol'n, more than MinPs-1 picks used in location, < 13 picks, active
   location, in the proper region, and not just an Ms update, update the
   dummy file */
   if ( pHypo->iGoodSoln >= 2 && pHypo->iNumPs >= Gparm->MinPs &&
        iBufferMatch < 0  && iLoc == 1 && pHypo->iNumPs < 15 && iInRegion == 1 )
      WriteDummyData( pHypo, Gparm->szMapFile, Gparm->szDummyFile, iLoc, 1 );
	
/* If good sol'n and more than MinPs-1 picks used in location, fill H71SUM2K
   format, report event to hypo ring, and update map and files */
   if ( pHypo->iGoodSoln >= 2 && pHypo->iNumPs >= Gparm->MinPs &&
        iBufferMatch < 0 && iInRegion == 1 )
   {
      MakeH71Msg( pHypo, szPageMsg );
      MakeTWCMsg( pHypo, szTWCMsg );
      ReportHypo( szPageMsg, szTWCMsg, Gparm, Ewh, iLoc, pHypo->iNumPs );
	  
/* Create file and path for P-data
   *******************************/
      strcpy ( szFileName, Gparm->szLocFilePath );
      strcat( szFileName, "loc" );
      itoaX( pHypo->iQuakeID, szTemp );
      PadZeroes( 4, szTemp );
      strcat( szFileName, szTemp );                    /* QuakeID */
      strcat( szFileName, "." );      
      itoaX( pHypo->iVersion, szTemp );
      PadZeroes( 3, szTemp );            
      strcat( szFileName, szTemp );                    /* Version */
      WritePickFile2( iPBufCnt, pPBuf, szFileName );
   }
	
/* Send a message to the alarm ring if warranted */
   if ( pHypo->iNumPs >= Gparm->MinPs && pHypo->iGoodSoln >= 2 && iLoc == 1 &&
        iBufferMatch < 0 && iInRegion == 1 )
   {
/* Filter out some of the messages if it is night in Alaska */   
      iNight = 0;
      lTime = (long) (pHypo->dOriginTime+0.5);
      tm = TWCgmtime( lTime );
      if ( tm->tm_hour >= 6 && tm->tm_hour <= 15 ) iNight = 1;
	  
/* Is this quake large enough to respond to? */
      iRespond = 0;
      iSendIt = 0;
      if ( ((iRegion < 5 || iRegion == 8)  && pHypo->dPreferredMag >= 3.5) ||
           (iRegion >= 10 && iRegion <= 13 && pHypo->dPreferredMag >  3.4) ||
           ((iRegion == 5 || iRegion == 9) && pHypo->dPreferredMag >= 5.5) ||
           (iRegion >= 14 && pHypo->dPreferredMag >= 5.5) ) iSendIt = 1;
/* Is this a significantly different location/magnitude than the last
   alarm message sent out */
      GeoGraphic( &ll2, (LATLON *) &HypoLast );
      while ( ll2.dLon > 180. ) ll2.dLon -= 360.;
      iNewAlarm = 1; 	  
      if ( fabs( ll2.dLat-ll.dLat ) < 0.5 &&
           fabs( ll2.dLon-ll.dLon ) < 0.5 &&
           fabs( HypoLast.dPreferredMag-pHypo->dPreferredMag ) < 0.2 &&
           fabs( HypoLast.dOriginTime-pHypo->dOriginTime ) < 5.0 &&
           abs( HypoLast.iNumPs-pHypo->iNumPs ) < 5 ) iNewAlarm = 0;
      if ( pHypo->iNumPs < 31 && iSendIt )
      {                      /* Is at night and big? or just day? */
         if ( iNewAlarm == 1 ) 
         {                   /* Send message if it is different, or a respond */
            strcpy( szMessage, "\0" );
            LoadPagerString( pHypo, szMessage, pcity, pcityEC );
            ReportAlarm( iRespond, szMessage, Gparm, Ewh );
            HypoLast = *pHypo;
         }
      }
   }
   
/* Set iAlarmIssued to 2 if no alarm has gone out yet (this will prevent
   "POOR LOC..." alarm from going out */
   if ( pHypo->iNumPs > 11 && pHypo->iGoodSoln >= 2 &&
        pHypo->iAlarmIssued == 0 && iBufferMatch < 0 && iInRegion == 1 )	  
      pHypo->iAlarmIssued = 2;
	    
/* Lots of picks for this event, but not a good solution. If enough
   picks, call in the troops */	  
   if ( pHypo->iNumPs > 12 && pHypo->iGoodSoln < 2 &&
        pHypo->iAlarmIssued == 0 && iLoc == 1 && iBufferMatch < 0 )
   {
/*      if ( pHypo->iAlarmIssued == 0 ) iRespond = 1; */
      strcpy( szMessage, "\0" );
      sprintf( szMessage, "POOR LOCATION; %d STATIONS", pHypo->iNumPs );
      ReportAlarm( iRespond, szMessage, Gparm, Ewh );    /* Activate RESPOND */
      pHypo->iAlarmIssued = 1;
   }
	    
/* Send alarm if respond not yet sent and MS > 6.2 & reasonable number of Ms's*/
   if ( iLoc == 0 && pHypo->iAlarmIssued == 0 &&
        pHypo->dMSAvg > 6.2 && pHypo->iNumMS > 3 && iInRegion == 1 )
   {
      if ( iRegion < 20 )                            /* Pacific & Carib. only */
      {
         strcpy( szMessage, "\0" );
         LoadPagerString( pHypo, szMessage, pcity, pcityEC );
         ReportAlarm( 0, szMessage, Gparm, Ewh );    /* Activate RESPOND */
         pHypo->iAlarmIssued = 1;
      }
   }

/* Only update last quake file if it looks like a good sol'n */
   if ( pHypo->iGoodSoln >= 2 && pHypo->iNumPs >= Gparm->MinPs &&
        iBufferMatch < 0 && iInRegion == 1 )
   {
      if ( (hFile = fopen( Gparm->szOldQuakes, "r" )) != NULL )
/* First read in old quake data */
      {
         for ( i=0; i<MAX_QUAKES; i++ )
            if ( fscanf( hFile, "%lf %lf %lf %lf %ld %s %ld %ld %ld %ld %lf "
                                "%lf %lf %ld %lf %ld %lf %ld %lf %ld %lf %ld "
                                "%lf %ld\n",
                &OldQuake[i].dOTime, &OldQuake[i].dLat, &OldQuake[i].dLon,
                &OldQuake[i].dPreferredMag, &OldQuake[i].iNumPMags,
                &OldQuake[i].szPMagType, &OldQuake[i].iDepth,
                &OldQuake[i].iQuakeID, &OldQuake[i].iVersion,
                &OldQuake[i].iNumPs, &OldQuake[i].dAvgRes,
                &OldQuake[i].dAzm, &OldQuake[i].dMbAvg, &OldQuake[i].iNumMb,
                &OldQuake[i].dMlAvg, &OldQuake[i].iNumMl,
                &OldQuake[i].dMSAvg, &OldQuake[i].iNumMS,
                &OldQuake[i].dMwpAvg, &OldQuake[i].iNumMwp,
                &OldQuake[i].dMwAvg, &OldQuake[i].iNumMw,
                &OldQuake[i].d1stPTime, &OldQuake[i].iGoodSoln ) != 24 ) break;
         fclose( hFile );
			
/* Determine if this is the same quake as one already written to szOldQuake */
         iMatch = -1;
         for ( i=0; i<MAX_QUAKES; i++ )
            if ( pHypo->iQuakeID == OldQuake[i].iQuakeID &&
                 pHypo->iVersion >= OldQuake[i].iVersion &&  /* Probably same */
                 pHypo->dOriginTime < OldQuake[i].dOTime + 1200. &&
                 pHypo->dOriginTime > OldQuake[i].dOTime - 1200. )
               iMatch = i;			
         hFile = fopen( Gparm->szOldQuakes, "w" );
         if ( iMatch == -1 )          /* No Match - Put new one at top */
         {
            fprintf( hFile, "%lf %lf %lf %lf %ld %s %ld %ld %ld %ld %lf "
                            "%lf %lf %ld %lf %ld %lf %ld %lf %ld %lf %ld "
                            "%lf %ld\n",
                     pHypo->dOriginTime, ll.dLat, ll.dLon, pHypo->dPreferredMag,
                     pHypo->iNumPMags, pHypo->szPMagType,
                     (int) (pHypo->dDepth + 0.5), pHypo->iQuakeID, 
                     pHypo->iVersion, pHypo->iNumPs, 
                     pHypo->dAvgRes, (double) pHypo->iAzm, pHypo->dMbAvg,
                     pHypo->iNumMb, pHypo->dMlAvg, pHypo->iNumMl, pHypo->dMSAvg,
                     pHypo->iNumMS, pHypo->dMwpAvg, pHypo->iNumMwp,
                     pHypo->dMwAvg, pHypo->iNumMw,
                     pHypo->dFirstPTime, pHypo->iGoodSoln );
            for ( i=0; i<MAX_QUAKES-1; i++ )
               fprintf( hFile, "%lf %lf %lf %lf %ld %s %ld %ld %ld %ld %lf "
                               "%lf %lf %ld %lf %ld %lf %ld %lf %ld %lf %ld "
                               "%lf %ld\n",
                        OldQuake[i].dOTime, OldQuake[i].dLat, OldQuake[i].dLon,
                        OldQuake[i].dPreferredMag, OldQuake[i].iNumPMags,
                        OldQuake[i].szPMagType, OldQuake[i].iDepth,
                        OldQuake[i].iQuakeID, OldQuake[i].iVersion,
                        OldQuake[i].iNumPs, OldQuake[i].dAvgRes,
                        OldQuake[i].dAzm, OldQuake[i].dMbAvg,OldQuake[i].iNumMb,
                        OldQuake[i].dMlAvg, OldQuake[i].iNumMl,
                        OldQuake[i].dMSAvg, OldQuake[i].iNumMS,
                        OldQuake[i].dMwpAvg, OldQuake[i].iNumMwp,
                        OldQuake[i].dMwAvg, OldQuake[i].iNumMw,
                        OldQuake[i].d1stPTime, OldQuake[i].iGoodSoln );
         }
         else             /* Re-write file patching in updated location */
         {         		 
            for ( i=0; i<MAX_QUAKES; i++ )
               if ( i == iMatch )
                  fprintf( hFile, "%lf %lf %lf %lf %ld %s %ld %ld %ld %ld %lf "
                           "%lf %lf %ld %lf %ld %lf %ld %lf %ld %lf %ld "
                           "%lf %ld\n",
                    pHypo->dOriginTime, ll.dLat, ll.dLon, pHypo->dPreferredMag,
                    pHypo->iNumPMags, pHypo->szPMagType,
                    (int) (pHypo->dDepth + 0.5), pHypo->iQuakeID, 
                    pHypo->iVersion, pHypo->iNumPs, 
                    pHypo->dAvgRes, (double) pHypo->iAzm, pHypo->dMbAvg,
                    pHypo->iNumMb, pHypo->dMlAvg, pHypo->iNumMl, pHypo->dMSAvg,
                    pHypo->iNumMS, pHypo->dMwpAvg, pHypo->iNumMwp,
                    pHypo->dMwAvg, pHypo->iNumMw,
                    pHypo->dFirstPTime, pHypo->iGoodSoln );
               else
                  fprintf( hFile, "%lf %lf %lf %lf %ld %s %ld %ld %ld %ld %lf "
                           "%lf %lf %ld %lf %ld %lf %ld %lf %ld %lf %ld "
                           "%lf %ld\n",
                    OldQuake[i].dOTime, OldQuake[i].dLat, OldQuake[i].dLon,
                    OldQuake[i].dPreferredMag, OldQuake[i].iNumPMags,
                    OldQuake[i].szPMagType, OldQuake[i].iDepth,
                    OldQuake[i].iQuakeID, OldQuake[i].iVersion,
                    OldQuake[i].iNumPs, OldQuake[i].dAvgRes,
                    OldQuake[i].dAzm, OldQuake[i].dMbAvg, OldQuake[i].iNumMb,
                    OldQuake[i].dMlAvg, OldQuake[i].iNumMl,
                    OldQuake[i].dMSAvg, OldQuake[i].iNumMS,
                    OldQuake[i].dMwpAvg, OldQuake[i].iNumMwp,
                    OldQuake[i].dMwAvg, OldQuake[i].iNumMw,
                    OldQuake[i].d1stPTime, OldQuake[i].iGoodSoln );
         }
         fclose( hFile );
      }
      else
         logit( "t", "Failed to open %s file in AutoLoc\n",
		         Gparm->szOldQuakes );
   }
   
/* Log results */
   if ( iLoc == 1 )
   {
      QuakeLog( iPBufCnt, pPBuf, pHypo, pcity, pcityEC );
      QuakeLog2( Gparm->szQLogFile, pHypo );
   }
   
/* Increment version */
   if ( iLoc == 1 ) pHypo->iVersion++;   
   
/* Return P buffer index */   
   if ( iBufferMatch >= 0 ) return iBufferMatch;
   else                     return iIndex;
}

      /******************************************************************
       *                     LoadPagerString()                          *
       *                                                                *
       * This function builds a quake message to send to the alarm ring.*
       *                                                                *
       *  Arguments:                                                    *
       *   pHypo            Computed hypocentral parameters             *
       *   pszMsg           Pointer to string to hold message           *
       *   pcity            Array of reference city information         *
       *   pcityEC          Array of eastern reference city information *
       *                                                                *
       ******************************************************************/
       
void LoadPagerString( HYPO *pHypo, char *pszMsg, CITY *pcity, CITY *pcityEC )
{
   CITYDIS CityDis;           /* Dist/dir from nearest cities (epicenter) */
   char    cNS, cEW;          /* 'N', 'S', etc. indicators. for lat/lon */
   int     iFERegion;         /* Flinn-Engdahl Region number */
   int     iRegion;           /* Procedural Region number */
   int     iTemp;
   LATLON  ll;                /* Geographic epicenter */
   long    lTime;             /* 1/1/70 time */
   char    *psz;              /* General region of epicenter */
   char    szTemp[64];        /* Littoral epicentral location */
   static  struct  tm    *tm; /* Origin time in tm structure */
   
/* Convert o-time from 1/1/70 time to tm structure */
   lTime = (long) (pHypo->dOriginTime+0.5);
   tm = TWCgmtime( lTime );
   
/* Get nearest cities to epicenter */
   GeoGraphic( &ll, (LATLON *) pHypo );
   if ( ll.dLon < 0 ) ll.dLon += 360.;
   iRegion = GetRegion( ll.dLat, ll.dLon );
   if ( ll.dLon > 180. ) ll.dLon -= 360.;
   cNS = 'N';
   if ( ll.dLat < 0. ) cNS = 'S';
   cEW = 'E';
   if ( ll.dLon < 0. ) cEW = 'W';
   
/* Compute distance to nearest cities */   
   if ( iRegion >= 10 )                  /* WC&ATWC Eastern AOR */
      NearestCitiesEC( (LATLON *) pHypo, pcityEC, &CityDis ); 
   else
      NearestCities( (LATLON *) pHypo, pcity, &CityDis );
  
/* Is there a city within 320 miles of epicenter? */
   if ( CityDis.iDis[1] < 320 || CityDis.iDis[0] < 320 )
   {
/* Yes, there is */
      if ( CityDis.iDis[1] < CityDis.iDis[0] ) iTemp = 1;
      else                                     iTemp = 0;
      sprintf( szTemp, "%d %s %s", CityDis.iDis[iTemp],
               CityDis.pszDir[iTemp], CityDis.pszLoc[iTemp] );
   }
   else          /* There was no near-by city, so use general area */
   {
      psz = namnum( ll.dLat, ll.dLon, &iFERegion );
      psz[strlen (psz)-1] = '\0';
      strcpy( szTemp, psz );
   }
   
/* Write the entire alarm message */
   sprintf( pszMsg, "%s  M%s %3.1lf  %ld STN  %.1lf%c %.1lf%c  "
    "%ld:%ld:%ldZ %ld/%ld  RES %.1lf  AZM %ld ",
    szTemp, pHypo->szPMagType,
    pHypo->dPreferredMag, pHypo->iNumPs, fabs( ll.dLat ), cNS, fabs( ll.dLon ),
    cEW, tm->tm_hour, tm->tm_min, tm->tm_sec, tm->tm_mon+1, tm->tm_mday,
    pHypo->dAvgRes, pHypo->iAzm );
}

     /**************************************************************
      *                         LoadUpPBuff()                      *
      *                                                            *
      * Load latest P-pick in the proper buffer.  If the pick is a *
      * repeat of a previous pick, replace the previous one (this  *
      * should have new magnitude information). Start with         *
      * iActiveBuffer and determine if the P-time fits in with the *
      * existing picks in that buffer.  If not, move to next       *
      * buffer. If it doesn't fit with any, move to the first      *
      * with no P-picks and start there.                           *
      *                                                            *
      * Two checks are made on the times before deciding which     *
      * buffer to use.  If the P-time is more than                 *
      * MaxTimeBetweenPicks from the last pick in the buffer, it   *
      * must go to the next.  Also, if it's time difference is     *
      * greater than possible (from the IASPEI91 P-time tables) to *
      * any pick in the buffer, it is moved to the next buffer.    *
      *                                                            *
      * Sept., 2004: Check to see that the station was not         *
      *              permanently removed from this buffer          *
      *                                                            *
      * Arguments:                                                 *
      *  pPStruct    Structure containing input P-pick information *
      *  pPBuf       P-pick buffers to copy PStruct into           *
      *  iBufCnt     Number of P-picks in each buffer so far       *
      *  Hypo        Computed hypocenters for each buffer          *
      *  piActive    Last buffer to put a P-pick into              *
      *  Gparm       Configuration parameter structure             *
      *  Ewh         Earthworm.d defines                           *
      *  city        reference city locations                      *
      *  iNumRem     Number of stations permanently removed        *
      *  cityEC      Eastern reference city locations              *
      *  pEqDepth    Array of depth data                           *
      *                                                            *
      **************************************************************/
	  
void LoadUpPBuff( PPICK *pPStruct, PPICK **pPBuf, int iPBufCnt[], HYPO Hypo[],
                  int *piActive, GPARM *Gparm, EWH *Ewh, CITY city[],
                  int iNumRem[], CITY cityEC[], EQDEPTHDATA pEqDepth[] )
{
   AZIDELT azidelt;                /* Distance/azimuth between 2 points */
   double  dCorr;                  /* Distance correction for P tables */
   double  dMax;                   /* Latest P-time in any buffer */
   double  dTTDif;                 /* P-time difference between 2 points */
   double  dTTDifMax;              /* Max P-time diff. given delta for 2 pts. */
   double  dTTDifQ;                /* Expected P time for this point */
   int     i, j, k, iNum, iTemp;   /* Index counters */
   int     iAlreadyUsed;           /* 1->This buffer already has been ordered */
   int     iBuff[MAX_PBUFFS];      /* Order of buffers (Starting with buffer
                                      with the most picks, buffers with a good
                                      location are designated -2) */
   int     iIndex;                 /* Index of buffer with latest P-pick */
   int     iMatch, iPCnt, iTDifCnt;/* Flags to indicate correct buffer */
   static  int  iDLev;             /* Index of 0 deg. in desired depth level */
   long    lTime;                  /* Preset 1/1/70 time in seconds */   
   
/* Is it an Ms? */
   if ( pPStruct->dPTime == 0. )   
      for ( i=0; i<Gparm->NumPBuffs; i++ )
         if ( Hypo[i].iQuakeID == pPStruct->lPickIndex )  /* Yes */
         {		 
/* Is this station already in buffer? */		 
            for ( j=0; j<iPBufCnt[i]; j++ )
               if ( !strcmp( pPBuf[i][j].szStation, pPStruct->szStation ) &&
                    !strcmp( pPBuf[i][j].szChannel, pPStruct->szChannel ) )
               {
/* If there is a match, copy the new Ms info into the old pick structure */		 
                  pPBuf[i][j].dMSAmpGM = pPStruct->dMSAmpGM;
                  pPBuf[i][j].lMSPer   = pPStruct->lMSPer;
                  pPBuf[i][j].dMSTime  = pPStruct->dMSTime;
				  
/* Just re-compute the magnitudes and dump to TWC hypo ring */			
                  if ( LocateQuake( pPBuf[i], iPBufCnt[i], Gparm, &Hypo[i], i,
                       Ewh, city, 0, Hypo, iPBufCnt, cityEC, pEqDepth ) < 0 )
                     logit( "et", "Problem in LocateQuake\n" );
                  if ( Gparm->Debug )
                     logit( "", "Ms Updated %s - %ld\n",
                      pPBuf[i][j].szStation, i );
                  return;
               }
/* It must not be in buffer, so add it */
            CopyPBuf( pPStruct, &pPBuf[i][iPBufCnt[i]] );
            iPBufCnt[i]++;
            if ( Gparm->Debug )
               logit( "", "Pick Added for MS %s - %ld\n",
                pPBuf[i][iPBufCnt[i]-1].szStation, i );
            return;
         }
		 
   if ( pPStruct->dPTime == 0. )
   {
      logit( "t", "P-time=0., no match for hypo=%ld\n", pPStruct->lPickIndex );
      return;
   }
   
/* Ignore this pick if it is older than PPICK_TIMEOUT seconds */   
   time( &lTime );
   if ( lTime-(long) pPStruct->dPTime > PPICK_TIMEOUT )
   {
      logit( "t", "Old P-time - %s\n", pPStruct->szStation );
      return;
   }
   
/* If this pick came from hypo_display, it should have a QuakeID */
   for ( i=0; i<Gparm->NumPBuffs; i++ )
      if ( Hypo[i].iQuakeID == pPStruct->iHypoID )
      {             /* Then, force it into this buffer */
         for ( j=0; j<iPBufCnt[i]; j++ )
            if ( !strcmp( pPBuf[i][j].szStation, pPStruct->szStation ) &&
                 !strcmp( pPBuf[i][j].szChannel, pPStruct->szChannel ) )
            {
/* If the Station was already in this buffer, update that pick */			
               CopyPBuf( pPStruct, &pPBuf[i][j] );
               if ( Gparm->Debug )
                  logit( "", "Pick Updated (in hypo_display) %s %lf - %ld\n",
                   pPBuf[i][j].szStation, pPBuf[i][j].dPTime, i );
               return;
            }
/* Otherwise, add a new pick to the buffer */			
         CopyPBuf( pPStruct, &pPBuf[i][iPBufCnt[i]] );
         iPBufCnt[i]++;
         if ( Gparm->Debug )
            logit( "", "Pick Added (from hypo_display) %s %lf - %ld\n",
             pPBuf[i][iPBufCnt[i]-1].szStation,
             pPBuf[i][iPBufCnt[i]-1].dPTime, i );
         return;
      }
   
/* Determine if this pick is a repeat of a previous pick */
   for ( i=0; i<Gparm->NumPBuffs; i++ )
      for ( j=0; j<iPBufCnt[i]; j++ )
         if ( (pPBuf[i][j].lPickIndex == pPStruct->lPickIndex &&
               fabs( pPStruct->dPTime-pPBuf[i][j].dPTime ) <
               Gparm->MaxTimeBetweenPicks*60.) ||
              (!strcmp( pPBuf[i][j].szStation, pPStruct->szStation ) &&
               !strcmp( pPBuf[i][j].szChannel, pPStruct->szChannel ) &&
               fabs( pPStruct->dPTime-pPBuf[i][j].dPTime ) <
              (double) BUFFER_TIMEOUT) )
         {
/* If it is, copy the new information into the old pick structure; UNLESS
   the Pick was manually adjusted by hypo_display or develo (i.e., iUseMe=2)
   and this adjustment is from pick_wcatwc. */		 
            if ( pPStruct->iUseMe == 1 && pPBuf[i][j].iUseMe == 2 )
            {
               if ( Gparm->Debug )
                  logit( "", "Pick Update on %s rejected.\n",
                         pPBuf[i][j].szStation);
               return;
            }
            CopyPBuf( pPStruct, &pPBuf[i][j] );
            if ( Gparm->Debug )
               logit( "", "Pick Updated %s %lf - %ld\n",
                pPBuf[i][j].szStation, pPBuf[i][j].dPTime, i );
            return;
         }

   for ( i=0; i<Gparm->NumPBuffs; i++ ) iBuff[i] = -1;   
/* If we get here, no match was found for pick so find a buffer for it */
/* First, check all buffers to see if this fits in an existing solution */		 		 
   for ( i=0; i<Gparm->NumPBuffs; i++ )
   {
      iTemp = *piActive+i;
      if ( iTemp >= Gparm->NumPBuffs ) iTemp -= Gparm->NumPBuffs;
	  
/* Verify that this was not in the permanently removed list */	  
      for ( j=0; j<iNumRem[iTemp]; j++ )
         if ( !strcmp( szStnRem[iTemp][j], pPStruct->szStation ) ) 
         {
            logit( "", "%s was perm. removed from %ld\n", pPStruct->szStation,
                   iTemp );
            return;
         }
	  
/* Do we have a good sol'n for this buffer */
      if ( Hypo[iTemp].iNumPs >= Gparm->MinPs+2 && Hypo[iTemp].iGoodSoln >= 2 )
      {	  

/* Get depth level to use in travel time table */
         iDLev = (int) ( (Hypo[iTemp].dDepth+0.01) /
                 (double) IASP_DEPTH_INC) * IASP_NUM_PER_DEP;
         iBuff[iTemp] = -2;
/* Check the P-time with what would be expected for this quake; if it
   fits keep it.  Otherwise, go to next buffer */	  
         azidelt = GetDistanceAz( (LATLON *) &Hypo[iTemp], (LATLON *) pPStruct);
         dTTDif = pPStruct->dPTime - Hypo[iTemp].dOriginTime;
         dCorr = azidelt.dDelta*(1./IASP_DIST_INC) - 
          floor( azidelt.dDelta*(1./IASP_DIST_INC) );
         dTTDifQ = fPP[(int) (azidelt.dDelta*(1./IASP_DIST_INC))+iDLev] +
          dCorr*(fPP[(int) (azidelt.dDelta*(1./IASP_DIST_INC))+1+iDLev] -
          fPP[(int) (azidelt.dDelta*(1./IASP_DIST_INC))+iDLev]);				
         if ( fabs( dTTDifQ - dTTDif ) <= 10. )
         {
            CopyPBuf( pPStruct, &pPBuf[iTemp][iPBufCnt[iTemp]] );
            iPBufCnt[iTemp]++;
            if ( Gparm->Debug )
               logit( "", "Pick Added (existing) %s %lf - %ld\n",
                pPBuf[iTemp][iPBufCnt[iTemp]-1].szStation,
			    pPBuf[iTemp][iPBufCnt[iTemp]-1].dPTime, iTemp );
            return;
         }
      }
   }
   
/* If we don't have a sol'n which matches this pick, try buffers to see which
   it could fit in. But first, order the buffers by Number of Ps. */
   for ( i=0; i<Gparm->NumPBuffs; i++ )
      if ( iBuff[i] != -2 )      /* Skip those where there is already a soln */
      {
         iNum = 0;
         for ( j=0; j<Gparm->NumPBuffs; j++ )
            if ( iBuff[j] != -2 )
            {
               iAlreadyUsed = 0;
               for ( k=0; k<i; k++ )
                  if ( iBuff[k] == j ) iAlreadyUsed = 1;
               if ( iAlreadyUsed == 0 )
                  if ( iPBufCnt[j] > iNum )
                  {
                     iBuff[i] = j;
                     iNum = iPBufCnt[j];
                  }
            }
      }
   
/* Get depth level to use in travel time table */
   iDLev = (int) ((double) (DEPTHKM+0.01) / IASP_DEPTH_INC)*IASP_NUM_PER_DEP;	    
   
   for ( i=0; i<Gparm->NumPBuffs; i++ )
   {
      iTemp = iBuff[i];
      if ( iTemp >= 0 )      /* -2->good soln for buffer, -1->Buffer empty */
      {	  
/* Here, check buffers by MaxTimeBetweenPicks and P-time differences */	  
         iMatch = 0;
         iPCnt = 0;
         iTDifCnt = 0;
         for ( j=0; j<iPBufCnt[iTemp]; j++ )
         {
            azidelt = GetDistanceAz( (LATLON *) pPStruct,
                                     (LATLON *) &pPBuf[iTemp][j] );
            dTTDif = fabs( pPStruct->dPTime - pPBuf[iTemp][j].dPTime );
            dTTDifMax = fPP[(int) (azidelt.dDelta*(1./IASP_DIST_INC))+1+iDLev];
            if ( dTTDif > dTTDifMax && pPBuf[iTemp][j].dPTime > 0. ) iPCnt++;
            if ( dTTDif > Gparm->MaxTimeBetweenPicks*60. ) iTDifCnt++;
            if ( !strcmp( pPStruct->szStation, pPBuf[iTemp][j].szStation ) )
               iMatch = 1;
         }
		
/* Are we within the constraints to add the pick to this buffer */
         if ( iMatch == 0 && iPCnt == 0 && iTDifCnt != iPBufCnt[iTemp] )
         {
            CopyPBuf( pPStruct, &pPBuf[iTemp][iPBufCnt[iTemp]] );
            iPBufCnt[iTemp]++;
            if ( Gparm->Debug )
               logit( "", "Pick Added %s %lf - %ld\n",
                pPBuf[iTemp][iPBufCnt[iTemp]-1].szStation,
                pPBuf[iTemp][iPBufCnt[iTemp]-1].dPTime, iTemp );
            return;
         }
      }
   }

/* If we get here, the pick doesn't fit in any existing buffers.  First, 
   try to find the first buffer available with no picks and put it there. */
   for ( i=0; i<Gparm->NumPBuffs; i++ )
   {
      iTemp = *piActive+i;
      if ( iTemp >= Gparm->NumPBuffs ) iTemp -= Gparm->NumPBuffs;
	  if ( iPBufCnt[iTemp] == 0 )
      {
         CopyPBuf( pPStruct, &pPBuf[iTemp][iPBufCnt[iTemp]] );
         iPBufCnt[iTemp]++;
         if ( Gparm->Debug )
            logit( "", "Pick Added (new buf) %s %lf - %ld\n",
             pPBuf[iTemp][iPBufCnt[iTemp]-1].szStation,
		     pPBuf[iTemp][iPBufCnt[iTemp]-1].dPTime, iTemp );
         return;
      }
   }

/* If we get here, all buffers have some data, but this pick doesn't
   fit in any.  Find the buffer with the latest data and put P there.
   (We really shouldn't get here.) */
   iIndex = 0;
   dMax = 0.;
   for ( i=0; i<Gparm->NumPBuffs; i++ )
      for ( j=0; j<iPBufCnt[i]; j++ )
         if ( pPBuf[i][j].dPTime > dMax )
         {
            dMax = pPBuf[i][j].dPTime;
            iIndex = i;
         }
   CopyPBuf( pPStruct, &pPBuf[iIndex][iPBufCnt[iIndex]] );
   iPBufCnt[iIndex]++;
   if ( Gparm->Debug )
      logit( "", "Pick Added (latest) %s %lf - %ld\n",
       pPBuf[iIndex][iPBufCnt[iIndex]-1].szStation,
       pPBuf[iIndex][iPBufCnt[iIndex]-1].dPTime, iIndex );
}				  

      /******************************************************************
       *                          MakeH71Msg()                          *
       *                                                                *
       * This function takes hypocenter parameters and puts them into a *
       * H71Sum2K message format.                                       *
       *                                                                *
       *  Arguments:                                                    *
       *   pHypo            Computed hypocentral parameters             *
       *   pszMsg           H71SUM2K message in proper format           *
       *                                                                *
       ******************************************************************/
       
void MakeH71Msg( HYPO *pHypo, char *pszMsg )
{
   double       dRes;                  /* Average residual */
   double       dTemp;                 /* Lat/Lon minutes */
   LATLON       ll;                    /* Lat/lon (geographic) array */
   long         lTime;                 /* 1/1/70 time */
   char         szTemp[20];
   struct tm    *tm;                   /* Origin time in tm structure */
/*
H71SUM2K SUMMARY FORMAT. From Lynn Dietz
1st Col  Length  Type           Description
0        4       int    %4d     Origin time year
4        2       int    %2d     Origin time month
6        2       int    %2d     Origin time day
8        1                      unused
9        2       int    %2d     Origin time hours
11       2       int    %2d     Origin time minutes
13       6       float  %6.2f   Origin time seconds
19       3       int    %3d     Latitude (degrees)
22       1       char   %c      S for south, blank for north
23       5       float  %5.2f   Latitude (decimal minutes)
28       4       int    %4d     Longitude (degrees)
32       1       char   %c      E for east, blank for west
33       5       float  %5.2f   Longitude (decimal minutes)
38       7       float  %7.2f   Depth (km)
45       1                      unused
46       1       char   %c      Magnitude type; D=duration, Z=low gain duration
47       5       float  %5.2f   Magnitude
52       3       int    %3d     # P&S times with weights > 0.1
55       4       int    %4d     Maximum azimuthal gap
59       5       float  %5.1f   Distance to nearest station
64       5       float  %5.2f   RMS travel time residual
69       5       float  %5.2f   Horizontal error (km)
74       5       float  %5.1f   Vertical error (km)
79       1       char   %c      Remark: Q if quarry blast
80       1       char   %c      Remark: Quality A-D
81       1       char   %c      Remark: Data source code
82       1                      unused
83       10      long   %10ld   Event ID number
93       1                      unused
94       1       char   %c      Version; 0=prelim, 1=Final w/ MD, 2=1+ML etc.
95       1       char   %c      newline character
*/

/* Clear message */
   strcpy( pszMsg, "\0" );

/* Convert o-time from 1/1/70 time to tm structure and fill in time fields */
   lTime = (long) (pHypo->dOriginTime);
   tm = TWCgmtime( lTime );
   itoaX( (int) tm->tm_year+1900, szTemp ); /* 0-3 year */
   PadZeroes( 4, szTemp );
   strcpy( pszMsg, szTemp );
   itoaX( (int) tm->tm_mon+1, szTemp );     /* 4-5 month */
   PadZeroes( 2, szTemp );
   strcat( pszMsg, szTemp );
   itoaX( (int) tm->tm_mday, szTemp );      /* 6-7 day */
   PadZeroes( 2, szTemp );             
   strcat( pszMsg, szTemp );
   strcat( pszMsg, " " );                   /* 8 blank */
   itoaX( (int) tm->tm_hour, szTemp );      /* 9-10 hour */
   PadZeroes( 2, szTemp );
   strcat( pszMsg, szTemp );
   itoaX( (int) tm->tm_min, szTemp );       /* 11-12 Minute */
   PadZeroes( 2, szTemp );
   strcat( pszMsg, szTemp );
   itoaX( (int) tm->tm_sec, szTemp );       /* 13-15 Second */
   PadZeroes( 3, szTemp );
   strcat( pszMsg, szTemp );
   strcat( pszMsg, "." );                   /* 16 . */
   itoaX( (int) ((pHypo->dOriginTime-floor( pHypo->dOriginTime ))*100.), szTemp );
   PadZeroes( 2, szTemp );                  /* 17-18 Hundredths Sec */
   strcat( pszMsg, szTemp );
   strcat( pszMsg, " " );                   /* 19 blank */
   
/* Convert lat/long from geocentric to geographic, then fill msg with loc. */
   GeoGraphic( &ll, (LATLON *) pHypo );
   itoaX( abs ((int) ll.dLat), szTemp );    /* 20-21 Latitude degrees */
   PadZeroes( 2, szTemp );
   strcat( pszMsg, szTemp );
   if ( ll.dLat > 0 ) strcpy( szTemp, " " );
   else               strcpy( szTemp, "S" );
   strcat( pszMsg, szTemp );               /* 22 S or blank for north */
   dTemp = 60. * fabs( ll.dLat - (int) ll.dLat );
   itoaX( (int) dTemp, szTemp );           /* 23-24 Latitude minutes */
   PadZeroes( 2, szTemp );
   strcat( pszMsg, szTemp );
   strcat( pszMsg, "." );                  /* 25 . */
   itoaX( (int) ((dTemp-(int)dTemp)*100.), szTemp );
   PadZeroes( 2, szTemp );                 /* 26-27 decimal minutes */
   strcat( pszMsg, szTemp );
   strcat( pszMsg, " " );                  /* 28 blank */
   while ( ll.dLon > 180. ) ll.dLon -= 360.;
   itoaX( abs( (int) ll.dLon ), szTemp );
   PadZeroes( 3, szTemp );                 /* 29-31 Longitude degrees */
   strcat( pszMsg, szTemp );
   if ( ll.dLon > 0 ) strcpy( szTemp, "E" );
   else               strcpy( szTemp, " " );
   strcat( pszMsg, szTemp );               /* 32 E or blank for W */
   dTemp = 60. * fabs( ll.dLon - (int) ll.dLon );
   itoaX( (int) dTemp, szTemp );           /* 33-34 Longitude minutes */
   PadZeroes( 2, szTemp );
   strcat( pszMsg, szTemp );
   strcat( pszMsg, "." );                  /* 35 . */
   itoaX( (int) ((dTemp-(int)dTemp)*100.), szTemp );
   PadZeroes( 2, szTemp );                 /* 36-37 decimal minutes */
   strcat( pszMsg, szTemp );
   strcat( pszMsg, " " );                  /* 38 blank */
   
/* Add depth to message (km) */   
   itoaX( (int) (pHypo->dDepth + 0.5), szTemp );
   PadZeroes( 3, szTemp );                 /* 39-41 Integer depth */
   strcat( pszMsg, szTemp );
   strcat( pszMsg, ".00" );                /* 42-44 Fill in expected format */
   strcat( pszMsg, " " );                  /* 45 blank */
   
/* Fill in magnitude type and magnitude */
   strncat( pszMsg, pHypo->szPMagType, 1 );
   strcat( pszMsg, "\0" );                 /* 46 magnitude type (b, w, etc.) */
   strcat( pszMsg, " " );                  /* 47 blank */
   itoaX( (int) pHypo->dPreferredMag, szTemp );
   strcat( pszMsg, szTemp );               /* 48 Integer mag. */
   strcat( pszMsg, "." );                  /* 49 . */
   itoaX( (int) ((pHypo->dPreferredMag-(int)pHypo->dPreferredMag)*100.),
                  szTemp );                /* 50-51 decimal mag. */
   PadZeroes( 2, szTemp );
   strcat( pszMsg, szTemp );
   
/* Number of P's used in location */
   itoaX( pHypo->iNumPs, szTemp );         /* 52-54 Number Ps used in loc */
   PadZeroes( 3, szTemp );
   strcat( pszMsg, szTemp );
   strcat( pszMsg, " " );                  /* 55 blank */
   
/* Azimuthal coverage of stations about the epicenter (converted to gap) */
   itoaX( (360-pHypo->iAzm), szTemp );
   PadZeroes( 3, szTemp );                 /* 56-58 Azimuthal gap */
   strcat( pszMsg, szTemp );
   
/* Nearest station's distance in degrees */
   itoaX( (int) pHypo->dNearestDist, szTemp );
   PadZeroes( 3, szTemp );                 /* 59-61 integer nearest distance */
   strcat( pszMsg, szTemp );
   strcat( pszMsg, "." );                  /* 62 . */
   itoaX( (int) ((pHypo->dNearestDist-(int)pHypo->dNearestDist)*10.),
                  szTemp );                /* 63 decimal distance */
   PadZeroes( 1, szTemp );
   strcat( pszMsg, szTemp );
   
/* Add Residual */
   dRes = pHypo->dAvgRes;
   if ( dRes > 99.99 ) dRes = 99.99;       /* Fit it in the space given */
   itoaX( (int) dRes, szTemp );            /* 64-65 integer average residual */
   PadZeroes( 2, szTemp );
   strcat( pszMsg, szTemp );
   strcat( pszMsg, "." );                  /* 66 . */
   itoaX( (int) ((dRes-(int)dRes)*100.), szTemp );
   PadZeroes( 2, szTemp );                 /* 67-68 decimal residual */
   strcat( pszMsg, szTemp );
   
/* Errors not computed by loc_wcatwc */
   strcat( pszMsg, "            " );       /* 69-80 blank */
   
/* Flags indicating regional center, quake id, and version */   
   strcat( pszMsg, "P" );                  /* 81 Data Source code (P=Palmer) */
   strcat( pszMsg, "  " );                 /* 82-83 blank */
   itoaX( pHypo->iQuakeID, szTemp);        /* 84-92 Quake ID */
   PadZeroes( 9, szTemp );
   strcat( pszMsg, szTemp );
   itoaX( pHypo->iVersion, szTemp);        /* 93-94 version */
   PadZeroes( 2, szTemp );
   strcat( pszMsg, szTemp );
   pszMsg[95] = '\n';
   pszMsg[96] = '\0';
}

      /******************************************************************
       *                          MakeTWCMsg()                          *
       *                                                                *
       * This function takes hypocenter parameters and puts them into a *
       * TYPE_HYPOTWC message format.                                   *
       *                                                                *
       *  Arguments:                                                    *
       *   pHypo            Computed hypocentral parameters             *
       *   pszMsg           HYPOTWC message in proper format            *
       *                                                                *
       ******************************************************************/
       
void MakeTWCMsg( HYPO *pHypo, char *pszMsg )
{
/*
HYPOTWC SUMMARY FORMAT.
Type     Description
----     -----------
int      Quake ID
int      Version
double   Origin time (1/1/70 seconds)
double   Latitude (geocentric)
double   Longitude (geocentric)
int      Depth (km)
int      # P times used in location
int      Station Azimuthal coverage around epicenter
double   RMS travel time residual (absolute average)
int      Quality: 0=poor, 1=fair, 2=good, 3=very good (based on residual)
double   Preferred magnitude (modified average)
char     Type of preferred magnitude (b, l, S, w)
int      # stations used in preferred magnitude
double   MS magnitude (modified average)
int      # stations used in Ms
double   Mwp magnitude (modified average)
int      # stations used in Mwp
double   Mb magnitude (modified average)
int      # stations used in Mb
double   Ml magnitude (modified average)
int      # stations used in Ml
double   Mw magnitude (modified average)
int      # stations used in Mw
int      0->new location, 1->new magnitude only
*/

/* Clear message */
   strcpy( pszMsg, "\0" );

/* Fill message */
   sprintf( pszMsg, "%ld %ld %lf %lf %lf %lf %ld %ld %lf %ld %lf %s %ld %lf "
                    "%ld %lf %ld %lf %ld %lf %ld %lf %ld %ld  \0",
    pHypo->iQuakeID, pHypo->iVersion, 
    pHypo->dOriginTime, pHypo->dLat, pHypo->dLon,
    pHypo->dDepth, pHypo->iNumPs, pHypo->iAzm,
    pHypo->dAvgRes, pHypo->iGoodSoln, 
	pHypo->dPreferredMag, pHypo->szPMagType, pHypo->iNumPMags,
    pHypo->dMSAvg, pHypo->iNumMS, pHypo->dMwpAvg, pHypo->iNumMwp, 
    pHypo->dMbAvg, pHypo->iNumMb, pHypo->dMlAvg, pHypo->iNumMl, pHypo->dMwAvg,
	pHypo->iNumMw, pHypo->iMagOnly );	
}

     /**************************************************************
      *                         RemoveP()                          *
      *                                                            *
      * Remove a PPICK structure from a P buffer.                  *
      *                                                            *
      * Arguments:                                                 *
      *  pPBuf       P-pick buffer to remove PPICK from            *
      *  iBufCnt     Number of P-picks in the buffer               *
      *  iIndex      Index of P structure to remove from pPBuf     *
      *  iLastCnt    Last buffer counter for this buffer           *
      *                                                            *
      **************************************************************/
	  
void RemoveP( PPICK *pPBuf, int *iPBufCnt, int iIndex, int *iLastCnt )
{
   int     i;
   
   for ( i=0; i<*iPBufCnt; i++ )
      if ( i > iIndex )
         CopyPBuf( &pPBuf[i], &pPBuf[i-1] );
   InitP( &pPBuf[*iPBufCnt-1] );
   *iPBufCnt -= 1;
}
