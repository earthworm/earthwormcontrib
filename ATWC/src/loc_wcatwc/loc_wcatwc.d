
#
#                     Loc_wcatwc's Configuration File
#
MyModId        MOD_LOC_WCATWC  # This instance of Loc_wcatwc
StaDataFile    "station.dat"   # File with information about stations
InRing           PICK_RING     # Transport ring to find P-picks
OutRing          HYPO_RING     # Transport ring to write quake locations
AlarmRing       ALARM_RING     # Transport ring to write alarm messages to
HeartbeatInt            30     # Heartbeat interval, in seconds
#
NumPBuffs               20     # Number of P-time Repositories (max in .h file)
PsPerBuffer            100     # Maximum number of P-picks per buffer
MaxTimeBetweenPicks      4.    # Maximum allowable time between P-picks
                               #  placed in the same buffer (minutes)
MinPs                    6     # Minimum number of P-times used in location
                               #  (5 min)
BValFile        "bvals.dat"    # File containing Richter B-value data for mb.
                               #  Must have 2500 entries.
# The following boundaries define the area over which locations in this 
# routine are trusted.
SouthernLat            -90.    # The whole world  
NorthernLat             90.   
WesternLon            -180.   
EasternLon             180.   
# The following files are used to connect information from loc_wcatwc to
# the WC/ATWC programs ANALYZE and LOCATE.
OldQuakes  "\DoNotCopyToEB\oldquakeX.dat"  # List of located quakes
AutoLoc    "\seismic\analyze\autolocX.dat" # Screen update trigger file
DummyFile  "\DoNotCopyToEB\dummyX.dat"     # WC/ATWC EarlyBird dummy file
MapFile    "\seismic\erlybird\location.dat"# WC/ATWC EarthVu Map Key File
RTPFile    "\seismic\locate\prtimeX.dat"   # WC/ATWC LOCATE real-time P File
QLogFile   "\DoNotCopyToEB\quake.log"      # Ongoing log of all quakes located
MwFile     "\earthworm\atwc\src\mm\mw.dat" # Mws from mm
LocFilePath "\DoNotCopyToEB\LocFiles\"     # Path for disk logs
DepthFile  "\seismic\locate\eqdepth.dat"   # File with average and max depth data
#
Debug                    1     # If 1, print debugging message
