/******************************************************************
 *                          File loc_wcatwc.h                     *
 *                                                                *
 *  Include file for location module used at the West Coast/Alaska*
 *  Tsunami Warning Center.  Made into Earthworm module 2/2001.   *
 ******************************************************************/

#include <trace_buf.h>
#include "\earthworm\atwc\src\libsrc\earlybirdlib.h"

/* Definitions
 *************/
#define MAX_PBUFFS        20  /* Max number of P-pick buffers */
#define MAX_STN_REM       10  /* Max number of Stations to permanently remove */
#define BUFFER_TIMEOUT   150  /* Adjusted P-pick time limit */
#define PPICK_TIMEOUT   1800  /* Don't accept P-picks older than this (sec) */
#define SOLVE_NUM        200  /* Max # stns to use in solution */
#define MAX_SCAVENGE      20  /* Don't scavenge from a buffer if it has more 
                                 than this # of picks */
#define MAX_VERSIONS      50  /* Max number of times to allow quake to be
                                 located. (Needed due to an occasional
                                 infinite loop triggered by scavenging) */

/* List of previously located quakes by loc_wcatwc
   ***********************************************/
typedef struct {
   double  dOTime;         /* 1/1/70 origin time of quake */
   double  dLat;           /* Geographic (+/-) latitude */
   double  dLon;           /* Geographic (+/-) longitude */
   double  d1stPTime;      /* First good P-time in auto-loc (1/1/70 sec.) */
   int     iDepth;         /* Hypocenter depth (km) */
   double  dPreferredMag;  /* Magnitude to use for this event */
   int     iNumPMags;      /* Number of stations used in dPreferredMag */
   int     iGoodSoln;      /* 0-bad, 1-soso, 2-good, 3-very good */
   int     iQuakeID;       /* Event number */
   int     iVersion;       /* Version of location (1, 2, ...) */
   int     iNumPs;         /* Number of P's used in quake solution */
   double  dAvgRes;        /* Sum of absolute values of residuals */
   double  dAzm;           /* Azimuthal coverage in degrees */
   double  dMbAvg;         /* Modified average Mb */
   int     iNumMb;         /* Number of Mb's in modified average */
   double  dMlAvg;         /* Modified average Ml */
   int     iNumMl;         /* Number of Ml's in modified average */
   double  dMSAvg;         /* Modified average MS */
   int     iNumMS;         /* Number of MS's in modified average */
   double  dMwpAvg;        /* Modified average Mwp */
   int     iNumMwp;        /* Number of Mwp's in modified average */
   double  dMwAvg;         /* Modified average Mw */
   int     iNumMw;         /* Number of Mw's in modified average */
   char    szPMagType[4];  /* Magnitude type of dPreferredMag (l, b, etc.) */
} OLDQUAKE;

typedef struct {
   char           StaDataFile[64];/* Station information file */
   long           InKey;          /* Key to ring where waveforms live */
   long           OutKey;         /* Key to ring where picks will live */
   long           AlarmKey;       /* Key to ring where alarms will live */
   int            HeartbeatInt;   /* Heartbeat interval in seconds */
   int            Debug;          /* If 1, print debug messages */
   unsigned char  MyModId;        /* Module id of this program */
   int            NumPBuffs;      /* Number of P-pick buffers */
   int            PsPerBuffer;    /* Max number of P-picks allowed per buffer */
   double     MaxTimeBetweenPicks;/* Time (min.) between Ps to start new buff */
   int            MinPs;          /* Minimum number of P-times/buff to locate */
   double         SouthernLat;    /* Southern latitude of region to solve for */
   double         NorthernLat;    /* Northern latitude of region to solve for */
   double         WesternLon;     /* Western longitude of region to solve for */
   double         EasternLon;     /* Eastern longitude of region to solve for */
   char           szBValFile[128];/* File containing Richter B-values for mb */
   char           szOldQuakes[128];/* List of last MAX_QUAKES quakes */
   char           szAutoLoc[128]; /* ANALZYE Trigger file to update screen */
   char           szDummyFile[128];/* WC/ATWC EarlyBird dummy File */
   char           szMapFile[128]; /* WC/ATWC EarthVu map key file */
   char           szRTPFile[128]; /* File to send P/mag data to LOCATE */
   char           szQLogFile[128];/* Log of all locations made */
   char           szMwFile[128];  /* Mws computed in mm */
   char           szLocFilePath[128]; /* Path to log the LOC files */
   char           szDepthDataFile[128];/* File with depth data */
   SHM_INFO       InRegion;       /* Info structure for input region */
   SHM_INFO       OutRegion;      /* Info structure for output region */
   SHM_INFO       AlarmRegion;    /* Info structure for alarm output region */
} GPARM;

typedef struct {
   unsigned char MyInstId;        /* Local installation */
   unsigned char GetThisInstId;   /* Get messages from this inst id */
   unsigned char GetThisModId;    /* Get messages from this module */
   unsigned char TypeHeartBeat;   /* Heartbeat message id */
   unsigned char TypeError;       /* Error message id */
   unsigned char TypeAlarm;       /* Tsunami Ctr alarm message id */
   unsigned char TypePickTWC;     /* Tsunami Ctr P-picker message id */
   unsigned char TypeH71Sum2K;    /* Hypocenter message */
   unsigned char TypeHypoTWC;     /* Hypocenter message - TWC format*/
} EWH;

/* Function declarations for loc_wcatwc
   ************************************/
void    AddInMw( char *, HYPO * );
int     GetEwh( EWH * );
int     LoadEQData (char *);
int     LocateQuake( PPICK *, int, GPARM *, HYPO *, int, EWH *, CITY *, int,
                     HYPO [], int [], CITY *, EQDEPTHDATA[] );
thr_ret LocateThread( void * );

int     GetConfig( char *, GPARM * );                    /* config.c */
void    LogConfig( GPARM * );

void    CheckPBuffTimes( PPICK **, int [], HYPO [], int, GPARM *, int [],
                         int [] );
void    LoadPagerString( HYPO *, char *, CITY *, CITY * );/* locate.c */
void    LoadUpPBuff( PPICK *, PPICK **, int [], HYPO [], int *, GPARM *,
                     EWH *, CITY *, int [], CITY *, EQDEPTHDATA[] );
void    MakeH71Msg( HYPO *, char * );
void    MakeTWCMsg( HYPO *, char * );
void    RemoveP( PPICK *, int *, int, int * );

void    ReportHypo( char *, char *,GPARM *, EWH *, int, int );/* report.c */
void    ReportAlarm( int, char *, GPARM *, EWH * );

int     IsComment( char [] );                            /* stalist.c */
int     LoadStationData( STATION **, int *, GPARM * );
