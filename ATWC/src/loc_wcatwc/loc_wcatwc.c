      /*****************************************************************
       *                          loc_wcatwc.c                         *
       *                                                               *
       *  This program locates earthquakes given P-picks placed by     *
       *  pick_wcatwc in the InRing.  Some simple logic is used to     *
       *  discriminate P-picks of different quakes.  The P-time        *
       *  between stations is compared to the distance between the two.*
       *  If the time is larger than can be possible given the P travel*
       *  time table values, the latest pick is moved into the next    *
       *  open P buffer.  As more P-picks come in, they are put in a   *
       *  buffer based on their P-time.  If MaxTimeBetweenPicks expires*
       *  between P-picks, the next P's are put into a new buffer.     *
       *  After a buffer has enough P-picks to locate a quake (MinPs), *
       *  the solution is computed.  If a good solution is made, P's   *
       *  from other buffers are compared to this solution and added   *
       *  back into the buffer if they fit (only P's that were thrown  *
       *  out due to the MaxTimeBetweenPicks critieria, not those      *
       *  eliminated due to excessively large P-time differences).     *
       *                                                               *
       *  Quake locations are computed using Geiger's method given an  *
       *  initial location computed by a technique developed at the    *
       *  West Coast/Alaska Tsunami Warning Center.  An initial guess  *
       *  is first assigned to the location of the first P-time in the *
       *  buffer.  If a solution can not be computed from this initial *
       *  location, a routine is called to compute the initial location*
       *  from azimuth and distance determined from a quadrapartite of *
       *  stations.  If a location can still not be determined, a bad  *
       *  P-pick discriminator is called.  This simply throws out      *
       *  stations one-at-a-time (up to three stations at once) and    *
       *  re-computes the location.  Good solutions are verified by    *
       *  total residual.  The quake locator was first introduced in   *
       *  tsunami warning centers by Sokolowski in the 1970's.  I      *
       *  I think it was originally developed at NEIC before that.     *
       *                                                               *
       *  The IASPEI91 travel times are used as the basis for quake    *
       *  locations in this program.  A time/distance/depth table has  *
       *  been created from software provided by the National          *
       *  Earthquake Information Center.  Locations with this set of P *
       *  times have been been compared to those made with the         *
       *  Jefferey's-Bullen set of times and were found to be superior *
       *  in regards to depth discrimination and epicentral location   *
       *  with poor azimuthal control.  The P-table is arranged on 10km*
       *  depth increments and 0.5 degree distance increments.         *
       *                                                               *
       *  After a good location has been computed, magnitude is output *
       *  based on the amplitude/periods reported by the P-picker.  Mb,*
       *  Ml, MS, and Mwp magnitudes are computed depending on         *
       *  epicentral distance.                                         *
       *                                                               *
       *  The locations/magnitudes are sent to the OutRing.  Alarms    *
       *  based on location and magnitude can also be issued to the    *
       *  AlarmRing if desired.  The page_alarm module will send these *
       *  to a pager.                                                  *
       *                                                               *
       *  March, 2006: Nyland's FindDepth function added so that       *
       *               average depth in region is used as fixed depth  *
       *               and depth can't float beyond max + 50km         *
       *  Sept., 2004: Respond to special PPick from hypo_display      *
       *               which causes an immediate relocate here.        *
       *  Sept., 2004: Completely remove picks from all buffers if they*
       *               appear to be Bad picks.                         *
       *  July, 2004: Update Mwps and screen data every x seconds      *
       *              automatically after a new location is made.      *
       *                                                               *
       *  Written by Paul Whitmore, (WC/ATWC) February, 2001           *
       *                                                               *
       ****************************************************************/
	   
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include "loc_wcatwc.h"

/* Global Variables (those needed in threads)
   ******************************************/
CITY   city[NUM_CITIES];       /* Array of reference city locations */
CITY   cityEC[NUM_CITIES_EC];  /* Array of eastern reference city locations */
EQDEPTHDATA EqDepth[EQSIZE];   /* Array of depth structures */
GPARM  Gparm;                  /* Configuration file parameters */
HYPO   Hypo[MAX_PBUFFS];       /* Hypocenter information for each P buffer */
int    iActiveBuffer;          /* Last buffer in which hypocenter computed */
int    iPBufCnt[MAX_PBUFFS];   /* Buffer P-pick Counter */
int    iNumPBufRem[MAX_PBUFFS];/* # of stns (permanently) removed from buffer */
PPICK  *PBuf[MAX_PBUFFS];      /* Pointer to P-pick buffers */
EWH    Ewh;                    /* Parameters from earthworm.h */
mutex_t mutsem1;               /* Semaphore to protect PBuf and iPBufCnt
                                  adjustments */
char   szStnRem[MAX_PBUFFS][MAX_STN_REM][TRACE_STA_LEN];/* Removed stns */

      /***********************************************************
       *              The main program starts here.              *
       *                                                         *
       *  Argument:                                              *
       *     argv[1] = Name of configuration file                *
       ***********************************************************/

int main( int argc, char **argv )
{
   int           i, j;            /* Loop counters */
   int           iRC;
   static STATION  *StaArray;     /* Station array (from StaDataFile) */
   char          PIn[MAX_PICKTWC_SIZE];/* Pointer to P-pick from ring */
   int           lineLen;         /* Length of heartbeat message */
   char          line[40];        /* Heartbeat message */
   long          MsgLen;          /* Size of retrieved message */
   MSG_LOGO      getlogo;         /* Logo of requested picks */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   MSG_LOGO      hrtlogo;         /* Logo of outgoing heartbeats */
   int           Nsta;            /* Number of stations in data file */
   time_t        then;            /* Previous heartbeat time */
   long          InBufl;          /* Maximum buffer size in bytes */
   char          *configfile;     /* Pointer to name of config file */
   pid_t         myPid;           /* Process id of this process */
   static unsigned tidLocate;     /* Quake Location Thread */
   
/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      fprintf( stderr, "Usage: loc_wcatwc <configfile>\n" );
      return -1;
   }
   configfile = argv[1];

/* Get parameters from the configuration files
   *******************************************/
   if ( GetConfig( configfile, &Gparm ) == -1 )
   {
      fprintf( stderr, "loc_wcatwc: GetConfig() failed. Exiting.\n" );
      return -1;
   }

/* Look up info in the earthworm.h tables
   **************************************/
   if ( GetEwh( &Ewh ) < 0 )
   {
      fprintf( stderr, "loc_wcatwc: GetEwh() failed. Exiting.\n" );
      return -1;
   }

/* Specify logos of incoming P-picks and outgoing heartbeats
   *********************************************************/
   getlogo.instid = Ewh.GetThisInstId;
   getlogo.mod    = Ewh.GetThisModId;
   getlogo.type   = Ewh.TypePickTWC;

   hrtlogo.instid = Ewh.MyInstId;
   hrtlogo.mod    = Gparm.MyModId;
   hrtlogo.type   = Ewh.TypeHeartBeat;

/* Initialize name of log-file & open it
   *************************************/
   logit_init( configfile, Gparm.MyModId, 256, 1 );

/* Get our own pid for restart purposes
   ************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "loc_wcatwc: Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Log the configuration parameters
   ********************************/
   LogConfig( &Gparm );

/* Load reference city coordinates used in littoral locations
  ***********************************************************/
   if ( LoadCities( city, 1 ) < 0)
   {
      logit( "t", "LoadCities failed, exiting\n" );
      return -1;
   }

/* Load eastern reference city coordinates used in littoral locations
  *******************************************************************/
   if ( LoadCitiesEC( cityEC, 1 ) < 0)
   {
      logit( "t", "LoadCitiesEC failed, exiting\n" );
      return -1;
   }
	
/* Load Richter B-values for use in Mb magnitudes 
   **********************************************/
   if ( LoadBVals( Gparm.szBValFile ) < 0 )
   {
      logit( "t", "LoadBVals failed, exiting\n" );
      return -1;
   }

/* Load Avg and max depth data
   ***************************/
   if ( !LoadEQData(Gparm.szDepthDataFile ) )
   {
      logit( "t", "LoadEQData failed\n" );
      return -1;
   }

/* Allocate the P-pick buffers
   ***************************/
   for ( i=0; i<Gparm.NumPBuffs; i++ )
   {
      InBufl = sizeof( PPICK ) * Gparm.PsPerBuffer; 
      PBuf[i] = (PPICK *) malloc( (size_t) InBufl );
      if ( PBuf[i] == NULL )
      {
         logit( "et", "loc_wcatwc: Cannot allocate waveform buffer %d\n", i );
         return -1;
      }
   }

/* Read all sites in the station data file and return the number of stations
   found in file. Allocate the station list array.
   *************************************************************************/
   if ( LoadStationData( &StaArray, &Nsta, &Gparm ) == -1 )
   {
      fprintf( stderr, "loc_wcatwc: LoadStationData() failed. Exiting.\n" );
      for ( i=0; i<Gparm.NumPBuffs; i++ ) free( PBuf[i] );
      return -1;
   }
   if ( Nsta == 0 )
   {
      logit( "t", "loc_wcatwc: Empty station data file. Exiting." );
      for ( i=0; i<Gparm.NumPBuffs; i++ ) free( PBuf[i] );
      free( StaArray );
      return -1;
   }
   logit( "", "loc_wcatwc: %d stations in data file.\n", Nsta );
   
/* Initialize P buffers
   ********************/   
   for ( i=0; i<Gparm.NumPBuffs; i++ )
   {
      for ( j=0; j<Gparm.PsPerBuffer; j++ ) InitP( &PBuf[i][j] );
      iPBufCnt[i] = 0;
      iNumPBufRem[i] = 0;
      InitHypo( &Hypo[i] );
      Hypo[i].iQuakeID = i+1;
	  Hypo[i].iVersion = 1;
	  Hypo[i].iAlarmIssued = 0;
   }
   iActiveBuffer = 0;
   
/* Create a mutex for protecting adjustments of PBuf and iPBufCnt
   **************************************************************/
   CreateSpecificMutex( &mutsem1 );

/* Attach to existing transport rings
   **********************************/
   if ( Gparm.OutKey != Gparm.InKey )
   {
      tport_attach( &Gparm.InRegion,  Gparm.InKey );
      tport_attach( &Gparm.OutRegion, Gparm.OutKey );
      tport_attach( &Gparm.AlarmRegion, Gparm.AlarmKey );
   }
   else
   {
      tport_attach( &Gparm.InRegion, Gparm.InKey );
      Gparm.OutRegion = Gparm.InRegion;
   }

/* Flush the input ring
   ********************/
   while ( tport_getmsg( &Gparm.InRegion, &getlogo, 1, &logo, &MsgLen,
                         PIn, MAX_PICKTWC_SIZE) != GET_NONE );

/* Send 1st heartbeat to the transport ring
   ****************************************/
   time( &then );
   sprintf( line, "%d %d\n", then, myPid );
   lineLen = strlen( line );
   if ( tport_putmsg( &Gparm.OutRegion, &hrtlogo, lineLen, line ) != PUT_OK )
   {
      logit( "et", "loc_wcatwc: Error sending 1st heartbeat. Exiting." );
      if ( Gparm.OutKey != Gparm.InKey )
      {
         tport_detach( &Gparm.InRegion );
         tport_detach( &Gparm.OutRegion );
         tport_detach( &Gparm.AlarmRegion );
      }
      else
         tport_detach( &Gparm.InRegion );
      for ( i=0; i<Gparm.NumPBuffs; i++ ) free( PBuf[i] );
      free( StaArray );
      return 0;
   }

/* Start the quake location thread
   *******************************/
   if ( StartThread( LocateThread, 8192, &tidLocate ) == -1 )
   {
      for ( i=0; i<Gparm.NumPBuffs; i++ ) free( PBuf[i] );
      free( StaArray );
      if ( Gparm.OutKey != Gparm.InKey )
      {
         tport_detach( &Gparm.InRegion );
         tport_detach( &Gparm.OutRegion );
         tport_detach( &Gparm.AlarmRegion );
      }
      else
         tport_detach( &Gparm.InRegion );
      logit( "et", "Error starting Locate thread; exiting!\n" );
      return -1;
   }

/* Loop to read picker messages and invoke the locator
   ***************************************************/
   while ( tport_getflag( &Gparm.InRegion ) != TERMINATE )
   {
      int     rc;               /* Return code from tport_getmsg() */
      time_t  now;              /* Current time */
      PPICK   PStruct;          /* P-pick data structure */

/* Send a heartbeat to the transport ring
   **************************************/
      time( &now );
      if ( (now - then) >= Gparm.HeartbeatInt )
      {
         then = now;
         sprintf( line, "%d %d\n", now, myPid );
         lineLen = strlen( line );
         if ( tport_putmsg( &Gparm.OutRegion, &hrtlogo, lineLen, line ) !=
              PUT_OK )
         {
            logit( "et", "loc_wcatwc: Error sending heartbeat." );
            break;
         }
      }

/* Get a waveform buffer from transport region
   *******************************************/
      rc = tport_getmsg( &Gparm.InRegion, &getlogo, 1, &logo, &MsgLen,
                         PIn, MAX_PICKTWC_SIZE);

      if ( rc == GET_NONE )
      {
         sleep_ew( 200 );
         continue;
      }

      if ( rc == GET_NOTRACK )
         logit( "et", "loc_wcatwc: Tracking error.\n");

      if ( rc == GET_MISS_LAPPED )
         logit( "et", "loc_wcatwc: Got lapped on the ring.\n");

      if ( rc == GET_MISS_SEQGAP )
         logit( "et", "loc_wcatwc: Gap in sequence numbers.\n");

      if ( rc == GET_MISS )
         logit( "et", "loc_wcatwc: Missed messages.\n");

      if ( rc == GET_TOOBIG )
      {
         logit( "et", "loc_wcatwc: Retrieved message too big (%d) for msg.\n",
                MsgLen );
         continue;
      }
	  
/* Put P-pick into structure (NOTE: No byte-swapping is performed, so 
   there will be trouble getting picks from an opposite order machine)
   ******************************************************************/
      if ( PPickStruct( PIn, &PStruct, Ewh.TypePickTWC ) < 0 ) continue;
	  
/* See if this "PPick" is actually a trigger to force a location in a 
   specified buffer.  If so, re-locate that buffer.
   ******************************************************************/
      if ( PStruct.iHypoID > 0 && 
           !strcmp( PStruct.szStation, "LOC" ) &&
           !strcmp( PStruct.szChannel, "ATE" ) )    /* Locate now */
      {
         logit( "et", "Force location sent from hypo_display.\n" );
         RequestSpecificMutex( &mutsem1 );   /* Semaphore protect buffer writes */
         for ( i=0; i<Gparm.NumPBuffs; i++ )
            if ( Hypo[i].iQuakeID == PStruct.iHypoID )
            {
               logit( "", "Relocate ID %ld\n", PStruct.iHypoID );
               iRC = LocateQuake( PBuf[i], iPBufCnt[i], &Gparm, &Hypo[i], i,
                                  &Ewh, city, 1, Hypo, iPBufCnt, cityEC, EqDepth );
						
               if ( iRC == -1 )
                  logit( "et", "Problem in LocateQuake-2\n" );
/* Set active buffer to the one which has had the latest good location (or the
   buffer which has the same quake with more picks) */			   
               if ( iRC >= 0 && Hypo[i].iGoodSoln >= 2 ) iActiveBuffer = iRC;
               break;
            }
         ReleaseSpecificMutex( &mutsem1 );   /* Let someone else have sem */
         continue;
      }

/* Add other known data about station to PPick structure
   *****************************************************/
      if ( PPickMatch( &PStruct, StaArray, Nsta ) < 0 )
      {
         int      lineLen;
         time_t   errTime;
         char     errmsg[80];
         MSG_LOGO logo;
		 
         time( &errTime );
         sprintf( errmsg, "%d 1 %s %s %s not found in StaDataFile\n", errTime,
                  PStruct.szStation, PStruct.szNetID, PStruct.szChannel );
         lineLen = strlen( errmsg );
         logo.type   = Ewh.TypeError;
         logo.mod    = Gparm.MyModId;
         logo.instid = Ewh.MyInstId;
         tport_putmsg( &Gparm.InRegion, &logo, lineLen, errmsg );
         continue;
      }
	  
/* Load P-pick into proper buffer
   ******************************/
      RequestSpecificMutex( &mutsem1 );   /* Semaphore protect buffer writes */
      LoadUpPBuff( &PStruct, PBuf, iPBufCnt, Hypo, &iActiveBuffer, &Gparm,
                   &Ewh, city, iNumPBufRem, cityEC, EqDepth );
      ReleaseSpecificMutex( &mutsem1 );   /* Let someone else have sem */
   }

/* Detach from the ring buffers
   ****************************/
   if ( Gparm.OutKey != Gparm.InKey )
   {
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.OutRegion );
      tport_detach( &Gparm.AlarmRegion );
   }
   else
      tport_detach( &Gparm.InRegion );
   for ( i=0; i<Gparm.NumPBuffs; i++ ) free( PBuf[i] );
   free( StaArray );
   logit( "t", "Termination requested. Exiting.\n" );
   return 0;
}

      /*******************************************************
       *                      GetEwh()                       *
       *                                                     *
       *      Get parameters from the earthworm.d file.      *
       *******************************************************/

int GetEwh( EWH *Ewh )
{
   if ( GetLocalInst( &Ewh->MyInstId ) != 0 )
   {
      fprintf( stderr, "loc_wcatwc: Error getting MyInstId.\n" );
      return -1;
   }

   if ( GetInst( "INST_WILDCARD", &Ewh->GetThisInstId ) != 0 )
   {
      fprintf( stderr, "loc_wcatwc: Error getting GetThisInstId.\n" );
      return -2;
   }
   if ( GetModId( "MOD_WILDCARD", &Ewh->GetThisModId ) != 0 )
   {
      fprintf( stderr, "loc_wcatwc: Error getting GetThisModId.\n" );
      return -3;
   }
   if ( GetType( "TYPE_HEARTBEAT", &Ewh->TypeHeartBeat ) != 0 )
   {
      fprintf( stderr, "loc_wcatwc: Error getting TypeHeartbeat.\n" );
      return -4;
   }
   if ( GetType( "TYPE_ERROR", &Ewh->TypeError ) != 0 )
   {
      fprintf( stderr, "loc_wcatwc: Error getting TypeError.\n" );
      return -5;
   }
   if ( GetType( "TYPE_ALARM", &Ewh->TypeAlarm ) != 0 )
   {
      fprintf( stderr, "loc_wcatwc: Error getting TypeAlarm.\n" );
      return -6;
   }
   if ( GetType( "TYPE_PICKTWC", &Ewh->TypePickTWC ) != 0 )
   {
      fprintf( stderr, "loc_wcatwc: Error getting TypePickTWC.\n" );
      return -7;
   }
   if ( GetType( "TYPE_H71SUM2K", &Ewh->TypeH71Sum2K ) != 0 )
   {
      fprintf( stderr, "loc_wcatwc: Error getting TYPE_H71SUM2K.\n" );
      return -8;
   }
   if ( GetType( "TYPE_HYPOTWC", &Ewh->TypeHypoTWC ) != 0 )
   {
      fprintf( stderr, "loc_wcatwc: Error getting TypeHypoTWC.\n" );
      return -9;
   }
   return 0;
}

      /*********************************************************
       *                   LoadEqData()                        *
       *                                                       *
       * The function "LoadEQData" will load EQSIZE lines from *
       * the input depth file into the structure type          *
       * EQDEPTHDATA. There are 4 values from each line in the *
       * file that are loaded into the structure. These are the*
       * latitude, longitude, average depth and the maximum    *
       * depth.                                                *
       *                                                       *
       * Arguments:                                            *
       *  pszFile        Input file with depth data            *
       *                                                       *
       * Return:                                               *
       *  int            -1 if problem; 1 if ok                *
       *                                                       *
       *********************************************************/
       
int LoadEQData ( char *pszFile )
{
   int   i;             /* Counter */
   FILE *hFile;         /* File handle */

/* Open a file for text input */
   i = 0;
   if ( (hFile = fopen( pszFile, "r" )) == NULL )
   {
      logit ( "", "Error opening depth data file %s.\n", pszFile );
      return (-1);
   }
   while ( !feof(hFile) )   /* Read in depth data */
   {
      fscanf( hFile, "%d %d %d %d",  &EqDepth[i].iLat, &EqDepth[i].iLon,
              &EqDepth[i].iAveDepth, &EqDepth[i].iMaxDepth );
      i++; 
      if ( i > EQSIZE )     /* Hit limit */
      {
         logit ( "", "Too many values in %s, %ld values\n", pszFile, i );
         break;
      }
   }
   fclose( hFile );
   return (1);
} 

      /*********************************************************
       *                   LocateThread.()                      *
       *                                                       *
       *  This thread checks each P buffer to see if enough    *
       *  picks have been entered to make a location.  Each    *
       *  new pick added to a buffer triggers another location.*
       *  Location/magnitude are checked to alarm criteria.    *
       *  After location, the thread checks whether any Ps from*
       *  other buffers should be added to this buffer.        *
       *  At the end of each loop, the thread checks time of   *
       *  Ps in each buffer to see if that buffer should be    *
       *  zeroed.                                              *
       *                                                       *
       *  September, 2002: Changed when new locations are made;*
       *   new locations now made whenever # Ps in buffer      *
       *   change and there are more than MinP picks.          *
       *                                                       *
       *********************************************************/
	   
thr_ret LocateThread( void *dummy )
{
   double  dMin;                             /* Oldest P-time in buffers */
   int     i, j;
   int     iFinal;                           /* 1->location made X min after P*/
   static  int     iLastBuffCnt[MAX_PBUFFS]; /* Previous number of Ps/buffer */
   int     iLoc;                             /* 1->full loc; 0->mag only */
   int     iMin;                             /* Index of oldest P buffer */
   int     iRC;                              /* Return from Locate */ 
   static  long    lLastTime[MAX_PBUFFS];    /* 1/1/70 time of last location */
   long    lTime;                            /* Present 1/1/70 time */
   
   //FILE *outputstream;
   //char outFile[] = "billk_loc.txt";

   for ( i=0; i<Gparm.NumPBuffs; i++ ) iLastBuffCnt[i] = 0;

/* Loop every second to check P buffer status */
   for (;;)
   {
   
/* Check each buffer to see if there are enough Ps to locate and/or there is a
   new or changed P */   
      time( &lTime );
      for ( i=0; i<Gparm.NumPBuffs; i++ )
      {
         iFinal = 0;
         if ( Hypo[i].iGoodSoln >= 2 && Hypo[i].iFinalMade == 0 &&
             (lTime-lLastTime[i]) > (long) (Gparm.MaxTimeBetweenPicks*60.) &&
             (lTime-Hypo[i].dOriginTime < 20*60) )
              iFinal = 1;
         if ( (iPBufCnt[i] >= Gparm.MinPs && iPBufCnt[i] != iLastBuffCnt[i]) ||
               iFinal == 1 )
         {			   
/* Semaphore protect buffer writes since this thread and others adjust
   buffers */			   
            RequestSpecificMutex( &mutsem1 );
            iLastBuffCnt[i] = iPBufCnt[i];
			
/* Try to locate a quake with this buffer's picks (if PTime > 0. and
   there are not more than SOLVE_NUM picks in a good soln) */			
            iLoc = 0;			
            if ( PBuf[i][iPBufCnt[i]-1].dPTime > 0. &&
                 Hypo[i].iVersion < MAX_VERSIONS &&
                (Hypo[i].iNumPs < SOLVE_NUM || (Hypo[i].iNumPs >= SOLVE_NUM &&
                 Hypo[i].iGoodSoln < 2)) ) iLoc = 1;
            iRC = LocateQuake( PBuf[i], iPBufCnt[i], &Gparm, &Hypo[i], i, &Ewh,
                               city, iLoc, Hypo, iPBufCnt, cityEC, EqDepth );			
            if ( iRC == -1 )
               logit( "et", "Problem in LocateQuake\n" );
            else
            {
   
/* Check other P buffers to see if any of their Ps should go with this quake */
               if ( iFinal == 0 )
                  CheckPBuffTimes( PBuf, iPBufCnt, Hypo, i, &Gparm,
                                   iLastBuffCnt, iNumPBufRem );
            }

/* Set active buffer to the one which has had the latest good location (or the
   buffer which has the same quake with more picks) */			   
            if ( iRC >= 0 && Hypo[i].iGoodSoln >= 2 ) iActiveBuffer = iRC;			   
			
            ReleaseSpecificMutex( &mutsem1 );  /* Let someone else have sem */
			
/* Is this location the final? */
            if ( iFinal == 1 ) Hypo[i].iFinalMade = 1;			   
            lLastTime[i] = lTime;              /* Save for future comparisons */
         }
      }
		 
/* Reset oldest P Buffer to zero when all are filled */
      for ( i=0; i<Gparm.NumPBuffs; i++ )
         if ( iPBufCnt[i] == 0 ) goto Sleeper;  /* Then, at least 1 is empty */
		 
/* If we get here, all P buffers must have some Ps in them, so one buffer must
   be cleared out. Find the one with the oldest P's and re-init that one. */
      iMin = 0;
      dMin = 1.E20;
      for ( i=0; i<Gparm.NumPBuffs; i++ )
/* Save this if MS could still be updating */
         if ( Hypo[i].dMSAvg == 0. ||
            ((double) lTime-Hypo[i].dOriginTime) > 7200. )
            for ( j=0; j<iPBufCnt[i]; j++ )
               if ( PBuf[i][j].dPTime < dMin && PBuf[i][j].dPTime > 0.0 )
               {
                  dMin = PBuf[i][j].dPTime;
                  iMin = i;
               }
      RequestSpecificMutex( &mutsem1 );   /* Semaphore protect buffer writes */
      iPBufCnt[iMin] = 0;
      iNumPBufRem[iMin] = 0;
      iLastBuffCnt[iMin] = 0;
      for ( i=0; i<Gparm.PsPerBuffer; i++ ) InitP( &PBuf[iMin][i] );
      InitHypo( &Hypo[iMin] );
      Hypo[iMin].iQuakeID += Gparm.NumPBuffs;
      if ( Hypo[iMin].iQuakeID >= 10000 ) Hypo[iMin].iQuakeID -= 10000;
      Hypo[iMin].iVersion = 1;
      Hypo[iMin].iAlarmIssued = 0;
      ReleaseSpecificMutex( &mutsem1 );   /* Let someone else have sem */
			
/* Wait a bit before looping again */	 
Sleeper:
      sleep_ew( 3000 );    
   }
}
