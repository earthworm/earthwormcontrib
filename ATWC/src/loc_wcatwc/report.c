  /**********************************************************************
   *                              report.c                              *
   *                                                                    *
   *                   Hypo, alarm reporting functions                  *
   *                                                                    *
   *  This file contains function ReportHypo() and ReportAlarm().       *
   **********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include <transport.h>
#include "loc_wcatwc.h"

     /**************************************************************
      *                         ReportHypo()                       *
      *                                                            *
      *   Report type H712K and type TWCLOC messages.              *
      **************************************************************/

void ReportHypo( char *pszPageMsg, char *pszTWCMsg, GPARM *Gparm, EWH *Ewh,
                 int iLoc, int iNumPs )
{
   MSG_LOGO     logo;      /* Logo of message to send to the output ring */
   int          lineLen;

/* log hypo
   ********/
   if ( iLoc == 1 ) logit( "et", "HYPO - %s\n", pszPageMsg );
   logit( "et", "HYPO - %s\n", pszTWCMsg );

/* Send the hypo (TWC format) to the output ring
   *********************************************/
   logo.type   = Ewh->TypeHypoTWC;
   logo.mod    = Gparm->MyModId;
   logo.instid = Ewh->MyInstId;
   lineLen     = strlen( pszTWCMsg ) + 1;

   if ( tport_putmsg( &Gparm->OutRegion, &logo, lineLen, pszTWCMsg ) !=PUT_OK )
      logit( "t", "loc_wcatwc: Error sending hypo to output ring - TWC.\n" );
	  
/* Send the hypo (H71 format) to the output ring
   *********************************************/
   if ( iLoc == 1 && iNumPs >= 8 )
   {
      logo.type   = Ewh->TypeH71Sum2K;
      lineLen     = strlen( pszPageMsg );

      if ( tport_putmsg( &Gparm->OutRegion, &logo, lineLen, pszPageMsg ) !=
           PUT_OK )
         logit( "t", "loc_wcatwc: Error sending hypo to output ring - H71.\n" );
   }
}

     /**************************************************************
      *                         ReportAlarm()                      *
      *                                                            *
      *                 Fill in TYPE_ALARM format                  *
      **************************************************************/

void ReportAlarm( int iRespond, char *pszPageMsg, GPARM *Gparm, EWH *Ewh )
{
   MSG_LOGO     logo;      /* Logo of message to send to the output ring */
   int          lineLen;
   char         line[256]; /* TYPE_PICKALARM format message */

/* Create TYPE_ALARM message (1->activate Respond Thread)
   ******************************************************/
   sprintf( line,    "%5ld %5ld %5ld     0     1 %5ld %s\0",
            (int) Ewh->TypeAlarm, (int) Gparm->MyModId, (int) Ewh->MyInstId,
            iRespond, pszPageMsg );
   lineLen = strlen( line ) + 1;

/* log alarm
   *********/
   logit( "et", "ALARM - %s\n", line );

/* Send the alarm to the output ring
   *********************************/
   logo.type   = Ewh->TypeAlarm;
   logo.mod    = Gparm->MyModId;
   logo.instid = Ewh->MyInstId;

   if ( tport_putmsg( &Gparm->AlarmRegion, &logo, lineLen, line ) != PUT_OK )
      logit( "t", "loc_wcatwc: Error sending alarm to output ring.\n" );
}
