#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <earthworm.h>
#include <transport.h>
#include "loc_wcatwc.h"

  /***************************************************************
   *                       LoadStationData()                     *
   *                                                             *
   *       Get data on station from information file             *
   *                                                             *
   *  Arguments:                                                 *
   *     Sta              Pointer to station data array          *
   *     Nsta             Number of stations in StaDataFile      *
   *     GParm            Pointer to configuration parameters    *
   *                                                             *
   *  Returns -1 if an error is encountered or no match is found.*
   ***************************************************************/

int LoadStationData( STATION **Sta, int *Nsta, GPARM *Gparm )
{
   int     i;
   char    string[180];       /* Line from StaDataFile */
   int     ndecoded;          /* Number of fields in string */
   int     nsta;              /* Number of stations counter */
   FILE    *fp;               /* StaDataFile handle */
   STATION *sta;              /* Temp pointer to array of structures */
   STATION StaTemp;

/* Open the station data file
   **************************/
   if ( ( fp = fopen( Gparm->StaDataFile, "r") ) == NULL )
   {
      logit( "et", "loc_wcatwc: Error opening station data file <%s>.\n",
             Gparm->StaDataFile );
      return -1;
   }
   
/* Count channels in the station data file.
   Ignore comment lines and lines consisting of all whitespace.
   ***********************************************************/
   nsta = 0;
   while ( fgets( string, 160, fp ) != NULL )
   {
      if ( IsComment( string ) ) continue;
      ndecoded = sscanf( string, "%s %s %s %lf %lf %lf %lf %lf %lf %lf %d",
                    StaTemp.szStation, StaTemp.szChannel, StaTemp.szNetID,
                    &StaTemp.dSens, &StaTemp.dGainCalibration, 
                    &StaTemp.dLat, &StaTemp.dLon, &StaTemp.dElevation,
                    &StaTemp.dClipLevel, &StaTemp.dTimeCorrection,					
                    &StaTemp.iStationType);
      if ( ndecoded < 11 )
      {
         logit( "et", "loc_wcatwc: Error decoding station data file-1.\n" );
         logit( "e", "ndecoded: %d\n", ndecoded );
         logit( "e", "Offending line:\n" );
         logit( "e", "%s\n", string );
         fclose( fp );
         return -1;
      }
      nsta++;
      if ( nsta == MAX_STATION_DATA )
      {
         logit( "", "Station limit (MAX_STATION_DATA) reached in StaDataFile\n" );
         break;
      }
   }
   rewind( fp );
   
/* Allocate the station list
   *************************/
   sta = (STATION *) calloc( nsta, sizeof(STATION) );
   if ( sta == NULL )
   {
      logit( "et", "loc_wcatwc: Cannot allocate the station data array\n" );
      return -1;
   }

/* Read station data from the station data file 
   ********************************************/
   i = 0;
   while ( fgets( string, 160, fp ) != NULL )
   {
      ndecoded = sscanf( string, "%s %s %s %lf %lf %lf %lf %lf %lf %lf %d",
                    sta[i].szStation, sta[i].szChannel, sta[i].szNetID,
                    &sta[i].dSens, &sta[i].dGainCalibration, 
                    &sta[i].dLat, &sta[i].dLon, &sta[i].dElevation,
                    &sta[i].dClipLevel, &sta[i].dTimeCorrection,					
                    &sta[i].iStationType);
      if ( ndecoded < 11 )
      {
         logit( "et", "loc_wcatwc: Error decoding station data file-2.\n" );
         logit( "e", "ndecoded: %d\n", ndecoded );
         logit( "e", "Offending line:\n" );
         logit( "e", "%s\n", string );
         fclose( fp );
         free( sta );
         return -1;
      }
	  i++;
      if ( i == MAX_STATION_DATA ) break;
   }
   fclose( fp );
   *Sta  = sta;
   *Nsta = i;
   return 0;
}

    /*************************************************************************
     *                             IsComment()                               *
     *                                                                       *
     *  Accepts: String containing one line from a pick_wcatwc station list  *
     *  Returns: 1 if it's a comment line                                    *
     *           0 if it's not a comment line                                *
     *************************************************************************/

int IsComment( char string[] )
{
   int i;

   for ( i = 0; i < (int)strlen( string ); i++ )
   {
      char test = string[i];

      if ( test!=' ' && test!='\t' && test!='\n' )
      {
         if ( test == '#'  )
            return 1;          /* It's a comment line */
         else
            return 0;          /* It's not a comment line */
      }
   }
   return 1;                   /* It contains only whitespace */
}
