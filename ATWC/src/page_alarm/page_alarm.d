# 
#                Configuration File for the page_alarm Program
#
#    page_alarm is a pager notification generator.  This module grabs messages 
#    from a transport ring which are meant to be issued over a pager system.  
#    The pager system used here is a Motorola system.  This was written by Whitmore
#    for use at the West Coast/Alaska Tsunami Warning Center in January, 2001.
#    It is based on eq_alarm.
#    
#
MyModuleId  MOD_PAGE_ALARM      # module id for this instance of page_alarm
RingName    ALARM_RING          # trasport ring for I/0
HeartbeatInt 30                 # Heartbeat interval, in seconds
LogSwitch   1                   # 0 to turn off logging to disk; 1 to turn on
                                # 2 to log to module log but not to stderr/stdout
#
MaxMsgSize  256                 # Max size (bytes) of alarm message
MsgBuffer   16                  # Internal alarm message buffer size 
#
# List the message logos to grab from transport ring
#            Installation      Module            Message Type
GetAlarmFrom INST_ATWC         MOD_WILDCARD      TYPE_ALARM
#
# Specify serial port settings. These work for NT. They have not been tested
# for Solaris.  There may be another set to use for Solaris.
#
CommPortName  COM6              # Serial port to output message
CommPortRate  9600              # Comm port speed (baud rate)
CommPortSize  8                 # Comm port data bit setting
CommPortPrty  0                 # Comm port parity setting:
                                # 0 - No Parity; 1 - Odd Parity
                                # 2 - Even Parity; 3 - Mark Parity
                                # 4 - Space Parity																
CommPortStop  0                 # Comm port number of stop bits
                                # 0 - 1 stop bit; 1 -  1.5 stop bit;
                                # 2 - 2 stop bits
CommPort2Name  COM9             # Serial port to output dialer info
CommPort2Rate  9600             # Comm port speed (baud rate)
CommPort2Size  8                # Comm port data bit setting
CommPort2Prty  0                # Comm port parity setting:
                                # 0 - No Parity; 1 - Odd Parity
                                # 2 - Even Parity; 3 - Mark Parity
                                # 4 - Space Parity																
CommPort2Stop  0                # Comm port number of stop bits
                                # 0 - 1 stop bit; 1 -  1.5 stop bit;
                                # 2 - 2 stop bits
TestSystem 0                    # 1->Testing (different alarm sound); 0->working
PagerFile   "\seismic\messages\PageAlarm.txt" # File to send pager messages
#
# Alarm file for cell phone notification.
#
AlarmFile   "\earthworm\run\params\EBALARM.DAT"# File monitored by email program
AlarmString "EARLYBIRD ALARM"        # String to send on cell phone

