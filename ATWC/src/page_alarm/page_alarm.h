/******************************************************************
 *                         File page_alarm.h                      *
 *                                                                *
 *  Include file pager alarm module.                              *
 *  Made into Earthworm module 1/2001.                            *
 ******************************************************************/

/* Comm port structure
   *******************/
typedef struct {
   char   szName[16];       /* Comm (serial) port name (e.g. COM1) */
   int    iBaudRate;        /* Baud rate (bits/second) */
   int    iByteSize;        /* Data bits / byte (7 or 8) */
   int    iParity;          /* Parity (even, odd, none) */
   int    iStopBits;        /* Stop bits (0, 1, or 2) */
} COMM_INFO;

/* Function declarations
   *********************/
#ifdef _WINNT 
   int     EndTTS (void);         /* Text-to-speech stuff */
   int     InitializeTTS (void);
   int     SendToTTS (char *);
#endif 

thr_ret BeepThread( void * );     /* Thread to issue PC BEEPs                 */
int  GetConfig( char * );         /* read configuration (.d) file             */
int  GetEwh( void );              /* Goes from symbolic names to numeric values,
                                     via earthworm.h                          */
void LogConfig( void );           /* Log configuration parameters             */
int  OpenCommPorts( COMM_INFO *CommInfo ); /* Set rate, etc. for 1 comm port  */
thr_ret RespondThread( void * );  /* Thread to issue "respond" messages       */ 
void SendPageMsg( char *, COMM_INFO *, COMM_INFO *, int );/* Output msgs      */
void LogPageMsgToFile( char *, char * );/* Page message file for EB           */

