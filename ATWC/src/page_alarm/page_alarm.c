/******************************************************************************
 * page_alarm.c: This module takes a message from a transport ring, and if    *
 *               the message is a valid alarm message, it will send it out a  *
 *               serial port.  This serial port output can activate a pager   *
 *               system.  This is loosely based on eq_alarm written by        *
 *               Bittenbinder.                                                *
 *                                                                            *
 *               This module assumes all alarms written to the transport ring *
 *               page_alarm examines are meant to be issued to the serial     *
 *               port.  That is, all filtering of alarm messages must be      *
 *               performed prior to input to this transport ring.  Internal   *
 *               buffering of messages is provided by page_alarm to prevent   *
 *               missed messages.                                             *
 *                                                                            *
 *               This module will also beep the PC (Windows only) if a flag   *
 *               is set in the alarm message.  Another flag in the alarm      *
 *               message will activate a thread which sends a "RESPOND" msg   *
 *               out the serial port if set.                                  *
 *                                                                            *
 *               The format for alarm messages is:                            *
 *                MsgType ModID InstID BeepFlag PageFlag RespondFlag Message  *
 *                                                                            *
 *               Module written by Whitmore, January, 2001, for use at the    *
 *               West Coast/Alaska Tsunami Warning Center.                    *
 *                                                                            *
 *    October, 2005: Added sounds for test systems                            *
 *    January, 2003: Added option to flag a pin on a serial output port (used *
 *                   at Palmer to trigger an automatic phone dialer as backup *
 *                   to beeper system.                                        *
 *    November, 2002: Added AlarmFile and AlarmString to .d file.  These are  *
 *                    used to aide in sending out alarms over cell phones.    *
 *                    A program must monitor AlarmFile for changes and forward*
 *                    AlarmString to the cell phone.                          *
 *    December, 2001: PC beep changed to verbal message, so that user knows   *
 *                    alarm has triggered. This is accomodated in the Windows *
 *                    version with multimedia funcitons.                      *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#ifdef _WINNT 
#include <mmsystem.h>
#endif
#include "page_alarm.h"

#define NCOMMAND  21        /* Number of commands in the config file */
#define  MAXLOGO   1        /* Max number of message logos to get */

static  SHM_INFO  Region1;  /* Public shared memory for receiving summary msgs*/

/* Things to lookup in the earthworm.h table with getutil.c functions
   ******************************************************************/
static long          PublicKey;     /* key to pbulic memory region for i/o  */
static unsigned char InstId;        /* local installation id                */
static unsigned char MyModId;       /* our module id                        */
static unsigned char TypeHeartBeat;
static unsigned char TypeAlarm;

/* Things to read from configuration file
   **************************************/
static char MyModuleId[32];       /* module id for this module                */
static char RingName[32];         /* name of transport ring for i/o           */
static int  LogSwitch;            /* 0 if no logging should be done to disk   */
MSG_LOGO    GetLogo[MAXLOGO];     /* array for requesting module,type,instid  */
short       nLogo;                /* number of logos read from ring           */
int         MaxMsgSize;           /* max laram message size in bytes          */
int         MsgBuffer;            /* Number messages to internally buffer     */
COMM_INFO   CommInfo;             /* Comm port setup info - pager             */
COMM_INFO   CommInfo2;            /* Comm port setup info - dialer            */
int         HeartbeatInt;         /* Heartbeat (for statmgr) in seconds       */
int         iTestSystem;          /* 1->Test; 0->Operational                  */
char        szPageMsgFile[64];    /* File to write pager message              */
char        szEBAlarmFile[64];    /* File to write cell phone alarm string    */
char        szEBAlarmString[128]; /* Phrase to write to the EBAlarmFile       */

/* Variables to share with Threads
   *******************************/
int         iBeepThread;          /* 1->thread operating, 0->done */
int         iSPAlarm;             /* 0->LP Alarm; 1->SP Alarm; 2->multi-stn */

      /***********************************************************
       *              The main program starts here.              *
       *                                                         *
       *  Argument:                                              *
       *     argv[1] = Name of page_alarm configuration file     *
       ***********************************************************/

int main( int argc, char **argv )
{
   int         iLineLen;             /* length of heartbeat message       */
   int         iPage;                /* 1->send page msg, 0->don't send   */
   int         iRespond;             /* 1->activate RespondThread         */
   int         iSpeak;               /* 1->Say msg, 0->don't say          */
   int         iTemp;                /* temp variable                     */
   long        lMsgSize;             /* size of retrieved message         */
   static MSG_LOGO    msgHrtLogo;    /* heartbeat logo                    */
   MSG_LOGO    msgLogo;              /* logo of retrieved message         */
   static pid_t       MyPid;         /* Hold process ID for heartbeats    */
   char        *pszMessage;          /* actual retrieved message          */
   int         rc;                   /* Return code from tport_getmsg     */ 
   char        szHrtMsg[64];         /* heartbeat message                 */
   char        szSta[64];            /* Station name which triggerred alarm */
   static unsigned    tidBeep;       /* Thread to issue verbal alarm      */
   static time_t      timeNow;       /* present time (1/1/70 time in sec) */ 
   static time_t      timeLastBeat;  /* time last heartbeat was sent      */

/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      fprintf( stderr, "Usage: page_alarm <configfile>\n" );
      return -1;
   }

/* Get parameters from the configuration files
   *******************************************/
   if ( GetConfig( argv[1] ) == -1 )
   {
      fprintf( stderr, "page_alarm: GetConfig() failed. Exiting.\n" );
      return -1;
   }

/* Look up info in the earthworm.h tables
   **************************************/
   if ( GetEwh() < 0 )
   {
      fprintf( stderr, "page_alarm: GetEwh() failed. Exiting.\n" );
      return -1;
   }

/* Initialize name of log-file & open it
   *************************************/
   logit_init( argv[1], (short) MyModId, 256, LogSwitch );
   logit( "", "page_alarm: Read command file <%s>\n", argv[1] );

/* Log configuration parameters
   ****************************/
   LogConfig();

/* Attempt to set up comm port 1 with specified parameters
   *******************************************************/
   if ( OpenCommPorts( &CommInfo ) < 0 )
   {
      logit( "", "Commport set failed\n" );
      return -1;
   }

/* Attempt to set up comm port 2 with specified parameters
   *******************************************************/
   if ( OpenCommPorts( &CommInfo2 ) < 0 )
   {
      logit( "", "Commport2 set failed\n" );
      return -1;
   }

/* Get our process ID for statmgr's use
   ************************************/
   if ( ( MyPid = getpid () ) == -1 )
   {
      logit( "e", "page_alarm: Call to getpid failed. Exiting.\n" );
      return -1 ;
   }
   iBeepThread = 0;

/* Allocate the message buffers
   ****************************/
   pszMessage = (char *) malloc( (size_t) MaxMsgSize+1 );
   if ( pszMessage == NULL )
   {
      logit( "et", "page_alarm: Cannot allocate message buffer\n" );
      return -1;
   }

/* Attach to input transport ring
   ******************************/
   tport_attach( &Region1, PublicKey );
   logit( "", "page_alarm: Attached to input ring <%s>: %ld.\n",
               RingName, Region1.key );

/* Set up heartbeat logo and send first heartbeat
   **********************************************/
   msgHrtLogo.instid = InstId;
   msgHrtLogo.mod    = MyModId;
   msgHrtLogo.type   = TypeHeartBeat;
   time( &timeLastBeat );
   sprintf( szHrtMsg, "%ld %ld\n", timeLastBeat, MyPid );
   iLineLen = strlen( szHrtMsg );
   if ( tport_putmsg( &Region1, &msgHrtLogo, iLineLen, szHrtMsg ) != PUT_OK )
   {
      logit( "et", "page_alarm: Error sending 1st heartbeat. Exiting." );
      tport_detach( &Region1 );
      free( pszMessage );
      return -1;
   }

/* Loop to read alarm message
   **************************/
   while ( tport_getflag( &Region1 ) != TERMINATE )
   {

/* Send page_alarm's heartbeat so statmgr knows we're alive
   ********************************************************/
      if  ( time( &timeNow ) - timeLastBeat  >=  HeartbeatInt )
      {
         timeLastBeat = timeNow;
         sprintf( szHrtMsg, "%ld %ld\n", timeLastBeat, MyPid );
         iLineLen = strlen( szHrtMsg );
         if ( tport_putmsg( &Region1, &msgHrtLogo, iLineLen, szHrtMsg ) !=
              PUT_OK )
         {
            logit( "et", "page_alarm: Error sending heartbeat. Exiting." );
            break;
         }
      }

/* Get and process the next alarm message from transport ring
   **********************************************************/
      rc = tport_getmsg( &Region1, GetLogo, nLogo, &msgLogo, &lMsgSize,
                          pszMessage, MaxMsgSize );
      if ( rc == GET_NONE )
      {
         sleep_ew( 500 );
         continue;
      }

      if ( rc == GET_NOTRACK )
         logit( "t", "Msg received (i%u m%u t%u); transport.h NTRACK_GET "
                "exceeded", msgLogo.instid, msgLogo.mod, msgLogo.type );

      if ( rc == GET_MISS )
         logit( "t", "Missed msg(s)  i%u m%u t%u  region:%ld.",
                 msgLogo.instid, msgLogo.mod, msgLogo.type, Region1.key);

      if ( rc == GET_TOOBIG )
      {
         logit( "t", "Retrieved msg[size=%ld] (i%u m%u t%u) too big for message"
                " [max=%d]", lMsgSize, msgLogo.instid, msgLogo.mod,
                msgLogo.type, MaxMsgSize );
         continue;
      }

/* Null terminate the string
   *************************/
      pszMessage[lMsgSize] = '\0';

/* Break string into variables
   ***************************/
      sscanf( pszMessage, "%ld %ld %ld %ld %ld %ld %s ",
              &iTemp, &iTemp, &iTemp, &iSpeak, &iPage, &iRespond, szSta );
      logit( "t", "Got message - Speak=%ld, Page=%ld, Respond=%ld %s\n",
              iSpeak, iPage, iRespond, pszMessage );			  
		   
/* Issue the page message
   **********************/
      if ( iPage && iTestSystem == 0 )      
         SendPageMsg( &pszMessage[36], &CommInfo, &CommInfo2, iRespond );
		   
/* Send to page to file
   ********************/
      if ( iSpeak )      
         LogPageMsgToFile( &pszMessage[36], szPageMsgFile );
		 
/* Start the beeper thread
   ***********************/
#ifdef _WINNT 
      if ( iSpeak == 1 )
         if ( iBeepThread == 0 )
            {
            iBeepThread = 1;
/* Determine if this was from long or short period alarm */			
        	if ( strstr( &pszMessage[0], " LP " ) )      iSPAlarm = 0;
        	else if ( strstr( &pszMessage[0], " SP " ) ) iSPAlarm = 1;
            else                                         iSPAlarm = 2;
            if ( StartThread( BeepThread, 8192, &tidBeep ) == -1 )
               logit( "et", "Error starting Beep thread\n" );
            }
#endif
   }

/* Detach from the ring buffers and exit
   *************************************/
   tport_detach( &Region1 );
   free( pszMessage );
   logit( "t", "Termination requested. Exiting.\n" );
   return 0;
}

      /*********************************************************
       *                     BeepThread()                      *
       *                                                       *
       *  This thread makes a PC Beep repeatedly, after an     *
       *  alarm was declared.                                  *
       *                                                       *
       *  December, 2001: Beep changed to verbal message.      *
       *  January, 2002:  After loop, alarm times out for 20   *
       *                  minutes so that it is not annoying.  *
       *                                                       *
       *********************************************************/
	   
thr_ret BeepThread( void *dummy )
{
#ifdef _WINNT
   int    i;

/* Loop every 4s and beep the PC (12/01: PC says alarm message) */
   for( i=0; i<6; i++ )
   {
      if (iTestSystem == 1)            /* Test system alarm */
         PlaySound ("\\earthworm\\atwc\\src\\lpproc\\ringin.wav", NULL,
                    SND_SYNC);	 
      else if (iSPAlarm == 0)          /* Long period alarm */
         PlaySound ("\\earthworm\\atwc\\src\\lpproc\\lpalarm.wav", NULL,
                    SND_SYNC);	 
      else if (iSPAlarm == 1)          /* Short period alarm */
         PlaySound ("\\earthworm\\atwc\\src\\lpproc\\spalarm.wav", NULL,
                    SND_SYNC);	 
      else if (iSPAlarm == 2)          /* Regional alarm */
         PlaySound ("\\earthworm\\atwc\\src\\lpproc\\regional.wav", NULL,
                    SND_SYNC);	 
   
/* Wait 2s before speaking again */	 
      sleep_ew( 2000 );
   }
   
/* Wait 20 minutes before speaking again */	 
   sleep_ew( 1000*20*60 );
   iBeepThread = 0;
   return;
#endif   
}

 /***********************************************************************
  *                              GetConfig()                            *
  *             Processes command file using kom.c functions.           *
  *                                                                     *
  * Arguments:                                                          *
  *  config_file      Earthworm .d file with initialization info        *
  *                                                                     *
  * Return:           0 normally, -1 if problem                         *
  ***********************************************************************/

int GetConfig( char *config_file )
{
   int  iInit[NCOMMAND];   /* Init flags, 1 if command read in              */
   int  iNumMiss;          /* Number of required commands that were missed  */
   int  iNumFiles;
   int  i;


/* Set init flag to zero for each required command
   ***********************************************/
   for ( i=0; i<NCOMMAND; i++ )  iInit[i] = 0;
   nLogo = 0;

/* Open the main configuration file
 **********************************/
   iNumFiles = k_open( config_file );
   if ( iNumFiles == 0 ) 
   {
      fprintf( stderr, "page_alarm: Error opening command file <%s>;exiting!\n",
               config_file );
      return( -1 );
   }

/* Process all nested configuration files
   **************************************/
   while ( iNumFiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )              /* Read next line from active file  */
      {
         int  success;
         char *com;
         char *str;

         com = k_str();             /* Get the first token from line */
         if ( !com )           continue;     /* Ignore blank lines */
         if ( com[0] == '#' )  continue;     /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = iNumFiles + 1;
            iNumFiles  = k_open( &com[1] );
            if ( iNumFiles != success ) 
            {
               fprintf( stderr, "page_alarm: Error opening command file <%s>\n",
                                 &com[1] );
               return( -1 );
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         else if ( k_its( "LogSwitch" ) ) 
         {
            LogSwitch = k_int();
            iInit[0] = 1;
         }
         else if ( k_its( "MyModuleId" ) ) 
         {
            str = k_str();
            if ( str ) strcpy( MyModuleId , str );
            iInit[1] = 1;
         }
         else if ( k_its( "RingName" ) ) 
         {
            str = k_str();
            if ( str ) strcpy( RingName, str );
            iInit[2] = 1;
         }
         else if ( k_its( "GetAlarmFrom" ) ) 
         {
            if ( nLogo >= MAXLOGO ) 
            {
               fprintf( stderr, "page_alarm: Too many <GetAlarmFrom> commands "
                                "in <%s>", config_file );
               fprintf( stderr, "; max=%d; exiting!\n", (int) MAXLOGO );
               return( -1 );
            }
            if ( ( str=k_str() ) ) 
            {
               if ( GetInst( str, &GetLogo[nLogo].instid ) != 0 ) 
               {
                  fprintf( stderr, "page_alarm: Invalid installation name <%s>",
                           str );
                  fprintf( stderr, " in <GetAlarmFrom> cmd\n" );
                  return( -1 );
               }
            }
            if ( ( str=k_str() ) ) 
            {
               if ( GetModId( str, &GetLogo[nLogo].mod ) != 0 ) 
               {
                  fprintf( stderr, "page_alarm: Invalid module name<%s>", str );
                  fprintf( stderr, " in <GetAlarmFrom> cmd\n" );
                  return( -1 );
               }
            }
            if ( ( str=k_str() ) ) 
            {
               if ( GetType( str, &GetLogo[nLogo].type ) != 0 ) 
               {
                  fprintf( stderr, "page_alarm: Invalid msg type <%s>", str );
                  fprintf( stderr, " in <GetAlarmFrom> cmd\n" );
                  return( -1 );
               }
            nLogo++;
            iInit[3] = 1;
            }
         }
         else if ( k_its( "MaxMsgSize" ) ) 
         {
            MaxMsgSize = k_int();
            iInit[4] = 1;
         }
         else if ( k_its( "MsgBuffer" ) ) 
         {
            MsgBuffer = k_int();
            iInit[5] = 1;
         }
         else if ( k_its( "CommPortName" ) ) 
         {
            str = k_str();
            if ( str ) strcpy( CommInfo.szName, str );
            iInit[6] = 1;
         }
         else if ( k_its( "CommPortRate" ) ) 
         {
            CommInfo.iBaudRate = k_int();
            iInit[7] = 1;
         }
         else if ( k_its( "CommPortSize" ) ) 
         {
            CommInfo.iByteSize = k_int();
            iInit[8] = 1;
         }
         else if ( k_its( "CommPortPrty" ) ) 
         {
            CommInfo.iParity = k_int();
            iInit[9] = 1;
         }
         else if ( k_its( "CommPortStop" ) ) 
         {
            CommInfo.iStopBits = k_int();
            iInit[10] = 1;
         }
         else if ( k_its( "HeartbeatInt" ) )
         {
            HeartbeatInt = k_int();
            iInit[11] = 1;
         }
         else if ( k_its( "AlarmFile" ) ) 
         {
            str = k_str();
            if ( str ) strcpy( szEBAlarmFile, str );
            iInit[12] = 1;
         }
         else if ( k_its( "AlarmString" ) ) 
         {
            str = k_str();
            if ( str ) strcpy( szEBAlarmString, str );
            iInit[13] = 1;
         }
         else if ( k_its( "CommPort2Name" ) ) 
         {
            str = k_str();
            if ( str ) strcpy( CommInfo2.szName, str );
            iInit[14] = 1;
         }
         else if ( k_its( "CommPort2Rate" ) ) 
         {
            CommInfo2.iBaudRate = k_int();
            iInit[15] = 1;
         }
         else if ( k_its( "CommPort2Size" ) ) 
         {
            CommInfo2.iByteSize = k_int();
            iInit[16] = 1;
         }
         else if ( k_its( "CommPort2Prty" ) ) 
         {
            CommInfo2.iParity = k_int();
            iInit[17] = 1;
         }
         else if ( k_its( "CommPort2Stop" ) ) 
         {
            CommInfo2.iStopBits = k_int();
            iInit[18] = 1;
         }
         else if ( k_its( "TestSystem" ) ) 
         {
            iTestSystem = k_int();
            iInit[19] = 1;
         }
         else if ( k_its( "PagerFile" ) ) 
         {
            str = k_str();
            if ( str ) strcpy( szPageMsgFile, str );
            iInit[20] = 1;
         }

/* An unknown parameter was encountered
   ************************************/
         else
         {
            fprintf( stderr, "page_alarm: <%s> Unknown command in <%s>.\n",
                     com, config_file );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() ) 
         {
            fprintf( stderr, "page_alarm: Bad <%s> command  in <%s>.\n",
                     com, config_file );
            return( -1 );
         }
      }
      iNumFiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
   ****************************************************************/
   iNumMiss = 0;
   for ( i = 0; i < NCOMMAND; i++ )  
      if ( !iInit[i] ) iNumMiss++;

   if ( iNumMiss ) 
   {
      fprintf( stderr, "page_alarm: ERROR, no " );
      if ( !iInit[0] )  fprintf( stderr, "<LogSwitch> " );
      if ( !iInit[1] )  fprintf( stderr, "<MyModuleId> " );
      if ( !iInit[2] )  fprintf( stderr, "<RingName> " );
      if ( !iInit[3] )  fprintf( stderr, "<GetAlarmFrom> " );
      if ( !iInit[4] )  fprintf( stderr, "<MaxMsgSize> " );
      if ( !iInit[5] )  fprintf( stderr, "<MsgBuffer> " );
      if ( !iInit[6] )  fprintf( stderr, "<CommPortName> " );
      if ( !iInit[7] )  fprintf( stderr, "<CommPortRate> " );
      if ( !iInit[8] )  fprintf( stderr, "<CommPortSize> " );
      if ( !iInit[9] )  fprintf( stderr, "<CommPortPrty> " );
      if ( !iInit[10])  fprintf( stderr, "<CommPortStop> " );
      if ( !iInit[11])  fprintf( stderr, "<HeartbeatInt> " );
      if ( !iInit[12])  fprintf( stderr, "<AlarmFile> " );
      if ( !iInit[13])  fprintf( stderr, "<AlarmString> " );
      if ( !iInit[14])  fprintf( stderr, "<CommPort2Name> " );
      if ( !iInit[15])  fprintf( stderr, "<CommPort2Rate> " );
      if ( !iInit[16])  fprintf( stderr, "<CommPort2Size> " );
      if ( !iInit[17])  fprintf( stderr, "<CommPort2Prty> " );
      if ( !iInit[18])  fprintf( stderr, "<CommPort2Stop> " );
      if ( !iInit[19])  fprintf( stderr, "<TestSystem> " );
      if ( !iInit[20])  fprintf( stderr, "<PagerFile> " );
      fprintf( stderr, "command(s) in <%s>; exiting!\n", config_file );
      return( -1 );
   }
   return( 0 );
}

 /***********************************************************************
  *                              LogConfig()                            *
  *                                                                     *
  *                   Log the configuration parameters                  *
  ***********************************************************************/

void LogConfig()
{
   logit( "", "\n" );
   logit( "", "LogSwitch:       %6d\n",   LogSwitch );
   logit( "", "MyModId:         %6u\n",   MyModId );
   logit( "", "InRing:          %6d\n",   PublicKey );
   logit( "", "HeartbeatInt:    %6d\n",   HeartbeatInt );
   logit( "", "MaxMsgSize:      %6d\n",   MaxMsgSize );
   logit( "", "MsgBuffer:       %6d\n",   MsgBuffer );
   logit( "", "CommPortName:     %s\n",   CommInfo.szName );
   logit( "", "CommPortRate:    %6d\n",   CommInfo.iBaudRate );
   logit( "", "CommPortSize:    %6d\n",   CommInfo.iByteSize );
   logit( "", "CommPortPrty:    %6d\n",   CommInfo.iParity );
   logit( "", "CommPortStop:    %6d\n",   CommInfo.iStopBits );
   logit( "", "CommPort2Name:    %s\n",   CommInfo2.szName );
   logit( "", "CommPort2Rate:   %6d\n",   CommInfo2.iBaudRate );
   logit( "", "CommPort2Size:   %6d\n",   CommInfo2.iByteSize );
   logit( "", "CommPort2Prty:   %6d\n",   CommInfo2.iParity );
   logit( "", "CommPort2Stop:   %6d\n",   CommInfo2.iStopBits );
   logit( "", "PagerFile:        %s\n",   szPageMsgFile );
   logit( "", "AlarmFile:        %s\n",   szEBAlarmFile );
   logit( "", "AlarmString:      %s\n",   szEBAlarmString );
   logit( "", "TestSystem:       %ld\n",  iTestSystem );
}

      /*******************************************************
       *                      GetEwh()                       *
       *                                                     *
       *      Get parameters from the earthworm.h file.      *
       *******************************************************/

int GetEwh( void )
{
/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 )
   {
      fprintf( stderr, "page_alarmr: Error getting InstId.\n" );
      return( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 )
   {
      fprintf( stderr, "page_alarmr: Error getting TypeHeartbeat.\n" );
      return( -1 );
   }
   if ( GetType( "TYPE_ALARM", &TypeAlarm ) != 0 )
   {
      fprintf( stderr, "page_alarmr: Error getting TypeAlarm.\n" );
      return( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModuleId, &MyModId ) != 0 ) 
   {
      fprintf( stderr, "page_alarmr: Invalid module name <%s>\n", MyModuleId );
      return( -1 );
   }

/* Look up keys to shared memory regions
   *************************************/
   if ( (PublicKey = GetKey( RingName ) ) == -1 ) 
   {
      fprintf( stderr, "page_alarmr: Invalid ring name <%s>\n", RingName );
      return( -1 );
   }
return( 0 );
}

 /***********************************************************************
  *                       LogPageMsgToFile()                            *
  *                                                                     *
  * Log all pager messages to file for monitoring and display in SUMMARY*
  *                                                                     *
  * Arguments:                                                          *
  *  pszMsg           Message to write to string                        *
  *  pszPageFile      File to write pager messages                      *
  *                                                                     *
  ***********************************************************************/

void LogPageMsgToFile( char *pszMsg, char *pszPageFile )
{
   FILE *hFile;                      /* Alarm File handle */

/* Try opening File
   ****************/
   if ( ( hFile = fopen (pszPageFile, "w" ) ) == NULL ) /* Wouldn't open */
      logit( "t", "%s could not be opened\n", pszPageFile );
   else                                                 /* Open OK       */
   {               /* Write alarm message to file */
      fprintf( hFile, "%s\n", pszMsg);
      fclose( hFile );
   }
}

 /***********************************************************************
  *                          OpenCommPorts()                            *
  * Set up comm port for serial write.  This function sets baud rate,   *
  * parity, data bits, and stop bits.                                   *
  *                                                                     *
  * Arguments:                                                          *
  *  CommInfo         Structure containing baud rate, etc. for port     *
  *                                                                     *
  * Return:           0 normally, -1 if problem                         *
  ***********************************************************************/

int OpenCommPorts ( COMM_INFO *CommInfo )
{
DCB     dcbDCB;                       /* Data communications block */
HANDLE  hCommDevice;                  /* Comm port handle          */

/* Open the comm port */
   hCommDevice = CreateFile( CommInfo->szName, GENERIC_READ | GENERIC_WRITE, 0,
                             NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );

/* Could we open port? */
   if ( hCommDevice == INVALID_HANDLE_VALUE ) /* No, we couldn't */
   {
      logit( "t", "Couldn't open Comm port %s\n", CommInfo->szName );
      return( -1 );  
   }
   else                                       /* Yes, port was opened */
   {
/* Get current setings of comm port */
      if ( GetCommState( hCommDevice, &dcbDCB ) == 0 )
      {
         logit( "t", "Couldn't get settings for comm port %s\n",
                 CommInfo->szName );
         return( -1 );
      }
/* Reset port rate, partiry, etc. */
      dcbDCB.BaudRate = CommInfo->iBaudRate;
      dcbDCB.ByteSize = CommInfo->iByteSize;
      dcbDCB.Parity   = CommInfo->iParity;
      dcbDCB.StopBits = CommInfo->iStopBits;

/* Update comm port with new settings */
      if ( SetCommState( hCommDevice, &dcbDCB ) == 0 )
      {
         logit( "t", "Couldn't Set comm port %s\n", CommInfo->szName);
         return( -1 );
      }
      CloseHandle( hCommDevice );
   }
return( 0 );                     /* Port set */
}

      /*********************************************************
       *                   RespondThread()                     *
       *                                                       *
       *  This thread sends a message "RESPOND-EW" over the    *
       *  pager system three times, then stops.                *
       *                                                       *
       *********************************************************/
	   
thr_ret RespondThread( void *dummy )
{
   int    i;
   FILE *hPort;                      /* Comm port handle */

/* Wait 10s before starting loop */
   sleep_ew( 10000 );

/* Loop every 20s and output "RESPOND-EW" message */
   for ( i=0; i<3; i++ )
   {
/* Try opening pager port
   **********************/
      if ( ( hPort = fopen (CommInfo.szName, "w" ) ) == NULL ) /* Wouldn't open */
         logit( "t", "Comm port not opened for RESPOND - %ld\n", i );
      else                                                      /* Open OK       */
      {
         fprintf( hPort, "RESPOND-EW - %ld\n", i);
         fclose( hPort );
         logit( "t", "ALARM SENT - RESPOND-EW - %ld\n", i );	  
      }
   
/* Wait 20s before sending again */	 
      sleep_ew( 20000 );
   }
   return;
}

 /***********************************************************************
  *                            SendPageMsg()                            *
  *                                                                     *
  * Write message to serial port and flag second pin on second serial   *
  * port.                                                               *
  *                                                                     *
  * August, 2003: On second serial port, now flag by sending            *
  *               line instead of open/close.                           *
  *                                                                     *
  * Arguments:                                                          *
  *  pszMsg           Message to write to string                        *
  *  CommInfo         Structures containing baud rate, etc. for port    *
  *  iRespond         1->Activate Respond Thread, 0->don't              *
  *                                                                     *
  ***********************************************************************/

void SendPageMsg( char *pszMsg, COMM_INFO *CommInfo, COMM_INFO *CommInfo2,
                  int iRespond )
{
   FILE *hFile;                      /* Alarm File handle */
   FILE *hPort;                      /* Comm port handle                  */
   static unsigned tidRespond;       /* Thread to issue "respond" messages*/
   static time_t      timeLastAlarm; /* time last respond alarm was sent  */
   static time_t      timeNow;       /* present time (1/1/70 time in sec) */ 

/* Try opening pager port
   **********************/
   if ( ( hPort = fopen (CommInfo->szName, "w" ) ) == NULL ) /* Wouldn't open */
      logit( "t", "Comm port not opened for msg %s\n", pszMsg );
   else                                                      /* Open OK       */
   {               /* Send alarm message over pager port */
      fprintf( hPort, "%s\n", pszMsg);
      fclose( hPort );
      logit( "t", "ALARM SENT - %s\n", pszMsg );	  
   }
   
/* Start the "RESPOND" thread (if it hasn't been started in last X minutes)
   and send alarm message out to cell phones. 
   ***********************************************************************/
   if ( iRespond > 0 )
      if  ( time( &timeNow ) - timeLastAlarm  >=  15*60 )
      {
         timeLastAlarm = timeNow;
/* Start RESPOND thread over pager */		 
         if ( StartThread(  RespondThread, 8192, &tidRespond ) == -1 )
            logit( "et", "Error starting Respond thread\n" );
/* Update digital cell phone messaging file */			
         if ( (hFile = fopen( szEBAlarmFile, "w" )) == NULL )
            logit( "t", "Failed to open EB Alarm File %s\n", szEBAlarmFile );
         else 
         {
            fprintf( hFile, "%ld\n", timeNow );
            fprintf( hFile, "%s\n", szEBAlarmString );
            fclose( hFile );
         }
/* Flag pin on serial port to trigger automatic dialer */		 
         if ( ( hPort = fopen (CommInfo2->szName, "w" ) ) == NULL )/* Wouldn't open */
            logit( "t", "Comm port 2 not opened \n" );
         else                                                      /* Open OK       */		 
         {
            fprintf (hPort, "THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG'S BACK 1234567890 TIMES.\n");
            fprintf (hPort, "THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG'S BACK 1234567890 TIMES.\n");
            fprintf (hPort, "THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG'S BACK 1234567890 TIMES.\n");
            fprintf (hPort, "THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG'S BACK 1234567890 TIMES.\n");
            fprintf (hPort, "THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG'S BACK 1234567890 TIMES.\n");
            fclose( hPort );
            logit( "t", "DIALER MESSAGE ACTIVATED\n" );	  
         }
      }
}
