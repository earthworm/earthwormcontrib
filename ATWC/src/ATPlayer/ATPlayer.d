
#
#                     Latency_mon's Configuration File
#
MyModId       MOD_ATPLAYER     # Module ID
InRing           WAVE_RING     # Output waveform ring
InRingLP         WAVE_RING_LP  # Output LP waveform ring
#
#
DataPath "e:\archivedata"      # Path to find seismic data files
LineupFileBB "\earthworm\run\params\ATP-BB.sta" # Station config file
LineupFileLP "\earthworm\run\params\ATP-LP.sta" # Station config file
FileLength               2     # Length of full data files in minutes (should be evenly
                               #  divisible into 60 (i.e., 1,2,3,4,5,6,10,...)
FileLengthLP             20    # Length of LP data files in minutes (should be evenly
                               #  divisible into 60 (i.e., 1,2,3,4,5,6,10,...)
FileSuffix             ".S"    # File suffix for differentiating different
                               #  sample rates of the same data
FileSuffixLP           ".L"    # LP File suffix for differentiating different
                               #  sample rates of the same data
ReadLength               30    # # minutes to read of full data (assumes 25 sps)
ReadLengthLP            100    # # minutes to read of LP data (assumes 1 sps)
Debug                    1     # If 1, print debugging message; 2->print lots
