
      /*****************************************************************
       *                        ATPlayer.c                             *
       *                                                               *
       *  This module takes data from WC/ATWC format disk files and    *
       *  reformats the data into TRACE_BUF records prior to loading   *
       *  into a WAVE_RING.  The purpose is to simulate old events.    *
       *  Earthworm is normally run with this module turned off.  If   *
       *  this program is started, all other data imports should be    *
       *  turned off.                                                  *
       *                                                               *
       *  The module is run from a Windows window, where the desired   *
       *  data time is specified.  The PC system clock is set to the   *
       *  data time to simulate the real event.  The only graphical    *
       *  output is an elapsed time display.                           *
       *                                                               *
       *  This module does not send heartbeats so should not be listed *
       *  in statmgr.d (and needs no .desc file).                      *
       *                                                               *
       *  This module is strictly for use under Windows.               *
       *                                                               *
       *  Written by Paul Whitmore, (WC/ATWC) August, 2005             *
       *                                                               *
       ****************************************************************/
	   
#include <windows.h>
#include <wingdi.h>
#include <winuser.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include <swap.h>
#include "ATPlayer.h"

/* Global Variables 
   ****************/         
double	dTimeRequested;        /* Requested archived file time */
double  dTimeRequestedLP;      /* Requested LP archived file time */
EWH     Ewh;                   /* Parameters from earthworm.h */
MSG_LOGO getlogoW;             /* Logo of requested waveforms */
GPARM   Gparm;                 /* Configuration file parameters */
HINSTANCE hInstMain;           /* Copy of main program instance (process id) */
HWND    hwndWndProc;           /* Client window handle */
int     iReadBB, iReadLP;      /* Number of files read in */
int     iStartThread;          /* 1->sender thread running; 0-> thread off */
MSG     msg;                   /* Windows control message variable */
int     Nsta;                  /* Number of stations in file */
int     NstaLP;                /* Number of stations in LP file */
STATION StaArray[MAXWVS];      /* Station data array */
STATION StaArrayLP[MAXWVS];    /* LP Station data array */
char	szArchSelect[64];      /* Archived sub-directory to display */
char    szProcessName[] = "AT Player";
time_t  then;                  /* Previous data send time */
char    *WaveBuf;              /* Pointer to waveform buffer */
TRACE_HEADER *WaveHead;        /* Pointer to waveform header */

      /***********************************************************
       *              The main program starts here.              *
       *                                                         *
       *  Argument:                                              *
       *     argv[1] = Name of configuration file                *
       ***********************************************************/

int WINAPI WinMain (HINSTANCE hInst, HINSTANCE hPreInst, 
                    LPSTR lpszCmdLine, int iCmdShow)
{
   char          configfile[64];  /* Name of config file */
   int           i;
   long          InBufl;          /* Maximum message size in bytes */
   long          RawBufl;         /* Memory size to allocate */
   static unsigned tidW;          /* Waveform getter Thread */
   static WNDCLASS wc;
   
   hInstMain = hInst;
   
/* Get config file name (format "ATPlayer ATPlayer.D")
   ***********************************************************/
   if ( strlen( lpszCmdLine ) <= 0 )
   {
      fprintf( stderr, "Need configfile in start line.\n" );
      return -1;
   }
   strcpy( configfile, lpszCmdLine );

/* Get parameters from the configuration files
   *******************************************/
   if ( GetConfig( configfile, &Gparm ) == -1 )
   {
      fprintf( stderr, "GetConfig() failed. file %s.\n", configfile );
      return -1;
   }

/* Look up info in the earthworm.h tables
   **************************************/
   if ( GetEwh( &Ewh ) < 0 )
   {
      fprintf( stderr, "ATPlayer: GetEwh() failed. Exiting.\n" );
      return -1;
   }

/* Specify logos of outgoing waveforms 
   ***********************************/
   getlogoW.instid = Ewh.GetThisInstId;
   getlogoW.mod    = Gparm.MyModId;
   getlogoW.type   = Ewh.TypeWaveform;

/* Initialize name of log-file & open it
   *************************************/
   logit_init( configfile, Gparm.MyModId, 256, 1 );

/* Log the configuration parameters
   ********************************/
   LogConfig( &Gparm );

/* Allocate the waveform buffer
   ****************************/
   InBufl = MAX_TRACEBUF_SIZ*2;
   WaveBuf = (char *) malloc( (size_t) InBufl );
   if ( WaveBuf == NULL )
   {
      logit( "et", "ATPlayer: Cannot allocate waveform buffer\n" );
      return -1;
   }

/* Point to header and data portions of waveform message
   *****************************************************/
   WaveHead  = (TRACE_HEADER *) WaveBuf;
   
/* Allocate the waveform buffers for disk data
   *******************************************/
   for ( i=0; i<MAXWVS; i++ )
   {		 		                  
/* Allocate memory for raw circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples)
   ********************************/
      StaArray[i].lRawCircSize = (long) (25 *  /* Assume 25 sps */
                                 (double)(Gparm.ReadLength+Gparm.FileLength)*
                                          60. + 0.1);
      RawBufl = sizeof (long) * StaArray[i].lRawCircSize;
      StaArray[i].plRawCircBuff = (long *) malloc( (size_t) RawBufl );
      if ( StaArray[i].plRawCircBuff == NULL )
      {
         logit( "et", "ATPlayer: Can't allocate raw circ buffer for %ld\n", i );
         free( WaveBuf );
         return -1;
      }
/* Allocate memory for LP raw circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples)
   ********************************/
      StaArrayLP[i].lRawCircSize = (long) (1 *  /* Assume 1 sps */
                                   (double)(Gparm.ReadLengthLP+Gparm.FileLength)
                                            *60.+0.1);
      RawBufl = sizeof (long) * StaArrayLP[i].lRawCircSize;
      StaArrayLP[i].plRawCircBuff = (long *) malloc( (size_t) RawBufl );
      if ( StaArrayLP[i].plRawCircBuff == NULL )
      {
         logit( "et", "ATPlayer: Can't allocate LP circ buffer for %ld\n", i );
         free( WaveBuf );
         for ( i=0; i<MAXWVS; i++ ) free( StaArray[i].plRawCircBuff );
         return -1;
      }
   }

/* Attach to existing transport rings
   **********************************/
   tport_attach( &Gparm.InRegion,  Gparm.InKey );
   tport_attach( &Gparm.InRegionLP,  Gparm.InKeyLP );

/* Start the waveform sender thread
   ********************************/
   if ( StartThread( WThread, 8192, &tidW ) == -1 )
   {
      logit( "et", "Error starting Wthread; exiting!\n" );
      free( WaveBuf );
      for ( i=0; i<MAXWVS; i++ ) free( StaArray[i].plRawCircBuff );
      for ( i=0; i<MAXWVS; i++ ) free( StaArrayLP[i].plRawCircBuff );
      return -1;
   }

/* If this is the first instance of this program, init window stuff and
   register window (it always is)
   ********************************************************************/
   if ( !hPreInst )
   {  /* Force PAINT when sized and give double click notification */
      wc.style         = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
      wc.lpfnWndProc   = WndProc;         /* Control Window process */
      wc.cbClsExtra    = 0;
      wc.cbWndExtra    = 0;
      wc.hInstance     = hInst;           /* Process id */
      wc.hIcon         = LoadIcon( hInst, "ATPlayer" );/* System app icon */
      wc.hCursor       = LoadCursor( NULL, IDC_ARROW );    /* Pointer */
      wc.hbrBackground = (HBRUSH) GetStockObject( WHITE_BRUSH ); /* White */
      wc.lpszMenuName  = "ATPlayer_menu";   /* Relates to .RC file */
      wc.lpszClassName = szProcessName;
      if ( !RegisterClass( &wc ) )        /* Window not registered */
      {
         logit( "t", "RegisterClass failed\n" );
         free( WaveBuf );
         for ( i=0; i<MAXWVS; i++ ) free( StaArray[i].plRawCircBuff );
         for ( i=0; i<MAXWVS; i++ ) free( StaArrayLP[i].plRawCircBuff );
         return -1;
      }
   }

/* Create the window
   *****************/
   hwndWndProc = CreateWindow(
                 szProcessName,          /* Process name */
                 szProcessName,          /* Initial title bar caption */
                 WS_OVERLAPPEDWINDOW | WS_VSCROLL,
                 CW_USEDEFAULT,          /* top left x starting location */
                 CW_USEDEFAULT,          /* top left y starting location */
                 CW_USEDEFAULT,          /* Initial screen width in pixels */
                 CW_USEDEFAULT,          /* Initial screen height in pixels */
                 NULL,                   /* No parent window */
                 NULL,                   /* Use standard system menu */
                 hInst,                  /* Process id */
                 NULL );                 /* No extra data to pass in */
   if ( hwndWndProc == NULL )            /* Window not created */
   {
      logit( "t", "CreateWindow failed\n" );
      free( WaveBuf );
      for ( i=0; i<MAXWVS; i++ ) free( StaArray[i].plRawCircBuff );
      for ( i=0; i<MAXWVS; i++ ) free( StaArrayLP[i].plRawCircBuff );
      return 0;
   }
   
   ShowWindow( hwndWndProc, iCmdShow );  /* Show the Window */
   UpdateWindow( hwndWndProc );          /* Force an initial PAINT call */
   
/* Main windows thread
   *******************/      
   while ( GetMessage( &msg, NULL, 0, 0 ) )
   {
      TranslateMessage( &msg );
      DispatchMessage( &msg );
   }

/* Detach from the ring buffer
   ***************************/
   iStartThread = 2;
   tport_detach( &Gparm.InRegion );
   tport_detach( &Gparm.InRegionLP );
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
   free( WaveBuf );
   for ( i=0; i<MAXWVS; i++ ) free( StaArray[i].plRawCircBuff );
   for ( i=0; i<MAXWVS; i++ ) free( StaArrayLP[i].plRawCircBuff );
   logit( "t", "Termination requested. Exiting.\n" );
   return (msg.wParam);
}

     /**************************************************************
      *                 CreateLineupFile()                         *
      *                                                            *
      * This function Creates a file which has station information *
      * about the data just read in.                               *
      *                                                            *
      * Arguments:                                                 *
      *  pszFile           File name to create                     *
      *  Sta               Station array                           *
      *  iNumSta           NUmber of station in Sta array          *
      *                                                            *
      * Returns: -1 -> failure; 1->ok                              *
      *                                                            *
      **************************************************************/
      
int CreateLineupFile( char *pszFile, STATION *Sta, int iNumSta  )
{
   FILE   *hFile;               /* File handle */
   int     i;

/* Create file */   
   hFile = fopen( pszFile, "w" );
   if ( hFile == NULL )
   {
      logit( "", "%s could not be created in CreateLineupFile\n", pszFile );
      return (-1);
   }
   
/* Create file */
   for ( i=0; i<iNumSta; i++ )             
   {
/* Compute some of the station array params */
      Sta[i].iPickStatus = 0;  
      if ( !strcmp( Sta[i].szChannel, "BHZ" ) ||
           !strcmp( Sta[i].szChannel, "BHZ00" ) ||
           !strcmp( Sta[i].szChannel, "SHZ" ) ) Sta[i].iPickStatus = 1;
      Sta[i].iFiltStatus = 0;  
      if ( Sta[i].szChannel[0] == 'B' ) Sta[i].iFiltStatus = 1;
      Sta[i].iAlarmStatus = 0;  
      Sta[i].iComputeMwp = 0;  
      if ( Sta[i].dSens > 0. && Sta[i].szChannel[0] == 'B' )
      {
         Sta[i].iAlarmStatus = 2;
         Sta[i].iComputeMwp = 1;
      }
      if ( Sta[i].dSampRate > 3. )    /* Broadband */
      {
         Sta[i].dAlarmAmp = 0.000012;  
         Sta[i].dAlarmDur = 50.0;  
         Sta[i].dAlarmMinFreq = 0.333;  
      }
      else                            /* Long period */
      {
         Sta[i].dAlarmAmp = 0.00005;  
         Sta[i].dAlarmDur = 60.0;  
         Sta[i].dAlarmMinFreq = 0.0333;  
/* Fix the LP scaling for events before 10/10/2000 */
	 if ( Sta[i].dScaleFactor > 0.005 ) Sta[i].dScaleFactor = 0.005;
      }
      fprintf( hFile, "%s %s %s %lf %lf %lf %lf %lf %lf %lf %lf %lf %ld %ld %ld %ld %ld %lf %lf %lf %ld\n",
       Sta[i].szStation,
       Sta[i].szChannel,
       Sta[i].szNetID,
       Sta[i].dLat,
       Sta[i].dLon,
       Sta[i].dElevation,
       Sta[i].dSampRate,
       Sta[i].dSens,
       Sta[i].dGainCalibration,
       Sta[i].dClipLevel,
       Sta[i].dTimeCorrection,
       Sta[i].dScaleFactor,
       Sta[i].iStationType,
       Sta[i].iSignalToNoise,
       Sta[i].iPickStatus,
       Sta[i].iFiltStatus,
       Sta[i].iAlarmStatus,
       Sta[i].dAlarmAmp,
       Sta[i].dAlarmDur,
       Sta[i].dAlarmMinFreq,
       Sta[i].iComputeMwp );
   }       

   fclose( hFile );
   return (1);
}

     /**************************************************************
      *                 DisplayCounter()                           *
      *                                                            *
      * This function outputs a % and completeness graph.          *
      *                                                            *
      * Arguments:                                                 *
      *  hdc               Device context to use                   *
      *  lTitleFHt         Title font height                       *
      *  lTitleFWd         Title font width                        *
      *  cxScreen          Screen width in pixels                  *
      *  cyScreen          Screen height in pixels                 *
      *  Sta               Station array (full)                    *
      *  StaLP             Station array (LP)                      *
      *  GP                Parameters from .d file                 *
      *                                                            *
      **************************************************************/
      
void DisplayCounter( HDC hdc, long lTitleFHt, long lTitleFWd, int cxScreen,
                     int cyScreen, STATION *Sta, STATION *StaLP, GPARM *GP )
{
   double  dTimeSoFar;          /* Time displayed so far */
   double  dTimeTotal;          /* Total time requested (full data) */
   double  dTimeTotalLP;        /* Total time requested (LP data) */
   double  dPercent;            /* Percentage of time shown (full) */
   double  dPercentLP;          /* Percentage of time shown (LP) */
   HFONT   hOFont, hNFont;      /* Font handles */
   HPEN    hRPen, hGPen;        /* Pen handles */
   time_t  now;                 /* Current time */
   POINT   pt;
   char    szBuf[8];            /* Percentage done */
   char    szTemp[8];
   LOGBRUSH lb;

/* Create font */   
   hNFont = CreateFont( lTitleFHt, lTitleFWd, 
             0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
             FIXED_PITCH | FF_MODERN, "Elite" );

/* Select font and color */
   hOFont = (HFONT) SelectObject( hdc, hNFont );    /* Select the font */
   SetTextColor( hdc, RGB( 0, 0, 0 ) );             /* Black text */
   
   lb.lbStyle = BS_SOLID;
   lb.lbColor = RGB( 0,222,0 );
   lb.lbHatch = HS_BDIAGONAL;
   hGPen = ExtCreatePen( PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT |
                         PS_JOIN_MITER, 8, &lb, 0, NULL );      /* green */

   lb.lbStyle = BS_SOLID;
   lb.lbColor = RGB( 200,0,0 );
   lb.lbHatch = HS_BDIAGONAL;
   hRPen = ExtCreatePen( PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT |
                         PS_JOIN_MITER, 8, &lb, 0, NULL );      /* red */

   time( &now );                /* Get present time */
   dTimeSoFar   = (double) now - dTimeRequested;
   dTimeTotal   = (double) iReadBB * 60.;
   dTimeTotalLP = (double) iReadLP * 60.;
   dPercent     = dTimeSoFar / dTimeTotal * 100.;
   if ( dPercent   > 100. ) dPercent   = 100.;
   dPercentLP   = dTimeSoFar / dTimeTotalLP * 100.;
   if ( dPercentLP > 100. ) dPercentLP = 100.;
	 
/* Write titles */
   pt.x = 10*cxScreen/100;
   pt.y = 15*cyScreen/100;
   TextOut( hdc, pt.x, pt.y, "Percentage complete - Broadband data", 36 );
   pt.y = 45*cyScreen/100;
   TextOut( hdc, pt.x, pt.y, "Percentage complete - Long Period data", 38 );
   
/* Draw lines showing percent done */
   SelectObject( hdc, hRPen );            /* Broadband data line */
   pt.x = 10*cxScreen/100;
   pt.y = 20*cyScreen/100;
   MoveToEx( hdc, pt.x, pt.y, NULL );
   pt.x = 90*cxScreen/100;
   LineTo( hdc, pt.x, pt.y );
   SelectObject( hdc, hGPen );
   pt.x = 10*cxScreen/100;
   MoveToEx( hdc, pt.x, pt.y, NULL );
   pt.x = (int) ((80.*dPercent/100.*(double) cxScreen)/100.) + 10*cxScreen/100;
   LineTo( hdc, pt.x, pt.y );

   SelectObject( hdc, hRPen );            /* LP data line */
   pt.x = 10*cxScreen/100;
   pt.y = 50*cyScreen/100;
   MoveToEx( hdc, pt.x, pt.y, NULL );
   pt.x = 90*cxScreen/100;
   LineTo( hdc, pt.x, pt.y );
   SelectObject( hdc, hGPen );
   pt.x = 10*cxScreen/100;
   MoveToEx( hdc, pt.x, pt.y, NULL );
   pt.x = (int) ((80.*dPercentLP/100.*(double) cxScreen)/100.) + 10*cxScreen/100;
   LineTo( hdc, pt.x, pt.y );
   
/* Write percent done below line */   
   pt.x = 10*cxScreen/100;                /* Broadband data percent */
   pt.y = 22*cyScreen/100;
   strcpy( szBuf, "" );
   sprintf( szTemp, "%3.0lf%%", dPercent );
   strcat( szBuf, szTemp );
   TextOut( hdc, pt.x, pt.y, szBuf, strlen( szBuf ) );
   
   pt.x = 10*cxScreen/100;                /* Long Period data percent */
   pt.y = 52*cyScreen/100;
   strcpy( szBuf, "" );
   sprintf( szTemp, "%3.0lf%%", dPercentLP );
   strcat( szBuf, szTemp );
   TextOut( hdc, pt.x, pt.y, szBuf, strlen( szBuf ) );

   DeleteObject( hGPen );                           /* Delete Pens */   
   DeleteObject( hRPen );   
   DeleteObject( SelectObject( hdc, hOFont ) );     /* Reset font */
}

     /**************************************************************
      *                   DisplayPause()                           *
      *                                                            *
      * This function outputs a PAUSE notice.                      *
      *                                                            *
      * Arguments:                                                 *
      *  hdc               Device context to use                   *
      *  lTitleFHt         Title font height                       *
      *  lTitleFWd         Title font width                        *
      *  cxScreen          Screen width in pixels                  *
      *  cyScreen          Screen height in pixels                 *
      *                                                            *
      **************************************************************/
      
void DisplayPause( HDC hdc, long lTitleFHt, long lTitleFWd, int cxScreen,
                   int cyScreen )
{
   HFONT   hOFont, hNFont;      /* Font handles */
   POINT   pt;

/* Create font */   
   hNFont = CreateFont( lTitleFHt*2, lTitleFWd*2, 
             0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
             FIXED_PITCH | FF_MODERN, "Elite" );

/* Select font and color */
   hOFont = (HFONT) SelectObject( hdc, hNFont );    /* Select the font */
   SetTextColor( hdc, RGB( 0, 0, 0 ) );             /* Black text */
   
/* Write titles */
   pt.x = 30*cxScreen/100;
   pt.y = 70*cyScreen/100;
   TextOut( hdc, pt.x, pt.y, "DATA FLOW PAUSE", 15 );
   
   DeleteObject( SelectObject( hdc, hOFont ) );     /* Reset font */
}

      /*******************************************************
       *                      GetEwh()                       *
       *                                                     *
       *      Get parameters from the earthworm.d file.      *
       *******************************************************/

int GetEwh( EWH *Ewh )
{
   if ( GetLocalInst( &Ewh->MyInstId ) != 0 )
   {
      fprintf( stderr, "ATPlayer: Error getting MyInstId.\n" );
      return -1;
   }
   if ( GetInst( "INST_WILDCARD", &Ewh->GetThisInstId ) != 0 )
   {
      fprintf( stderr, "ATPlayer: Error getting GetThisInstId.\n" );
      return -2;
   }
   if ( GetModId( "MOD_WILDCARD", &Ewh->GetThisModId ) != 0 )
   {
      fprintf( stderr, "ATPlayer: Error getting GetThisModId.\n" );
      return -3;
   }
   if ( GetType( "TYPE_ERROR", &Ewh->TypeError ) != 0 )
   {
      fprintf( stderr, "ATPlayer: Error getting TypeError.\n" );
      return -5;
   }
   if ( GetType( "TYPE_TRACEBUF", &Ewh->TypeWaveform ) != 0 )
   {
      fprintf( stderr, "ATPlayer: Error getting TYPE_TRACEBUF.\n" );
      return -6;
   }
   return 0;
}
					      
   /*******************************************************************
    *                         ListArchive1()                          *
    *                                                                 *
    * Display dates of data saved in archive directory.               *
    *                                                                 *
    *  Arguments:                                                     *
    *                                                                 *
    *******************************************************************/

long WINAPI ListArchive1DlgProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   int    iIndex;        /* Index of selected file */
   char   szCurDir[64];  /* Temporary stash for programs directory */

   switch (msg)						    
   {
      case WM_INITDIALOG:
/* Clear list box */
         SendDlgItemMessage( hwnd, IDD_ARCH_FILE, LB_RESETCONTENT, 0, 0 );
/* Save current directory */
         GetCurrentDirectory( sizeof( szCurDir ), szCurDir );
/* Set currect directory to data archive directory */
         if( !SetCurrentDirectory( Gparm.DataPath ) )
         {
            logit( "t", "Archive dir not set, %s\n", Gparm.DataPath );
            EndDialog( hwnd, IDCANCEL );
         }
/* Load the list box with directories */
         SendDlgItemMessage( hwnd, IDD_ARCH_FILE, LB_DIR, DDL_READWRITE | 
                             DDL_DIRECTORY, (LPARAM) "*.*" );
/* Return the directory to what it was */
	 SetCurrentDirectory( szCurDir );
         SetFocus( hwnd );
	 break;

   case WM_COMMAND:
      switch ( LOWORD( wParam ) )
      {
         case IDD_ARCH_FILE:           /* A file has been selected */
            switch ( HIWORD( wParam ) )
            {
               case LBN_DBLCLK:  /*User must double click on selection */
/* Obtain the index of selection */
                  iIndex = SendDlgItemMessage( hwnd, IDD_ARCH_FILE, 
                                               LB_GETCURSEL, 0, 0 );
/* Read the selected text */
                  if ( iIndex != LB_ERR )
                     SendDlgItemMessage( hwnd, IDD_ARCH_FILE, LB_GETTEXT,
                                         iIndex, (LPARAM) szArchSelect );
                  else         /* Nothing selected */
                     EndDialog( hwnd, IDCANCEL );
                  if ( DialogBox( hInstMain, "ListArchive2", hwndWndProc, 
                      (DLGPROC) ListArchive2DlgProc ) == IDCANCEL ) 
                  {
                     EndDialog( hwnd, IDCANCEL );
                     break;
                  }
               EndDialog( hwnd, IDOK );
               break;
            }
            break;

         case IDOK:     /* Entering OK the same as double-click */
/* Obtain the index of selection */
            iIndex = SendDlgItemMessage( hwnd, IDD_ARCH_FILE, 
                                         LB_GETCURSEL, 0, 0 );
/* Read the selected text */
            if( iIndex != LB_ERR )	
               SendDlgItemMessage( hwnd, IDD_ARCH_FILE, LB_GETTEXT,
                                   iIndex, (LPARAM) szArchSelect );
            else         /* Nothing selected */
               EndDialog( hwnd, IDCANCEL );
            if ( DialogBox ( hInstMain, "ListArchive2", hwndWndProc, 
                (DLGPROC) ListArchive2DlgProc ) == IDCANCEL ) 
            {
               EndDialog( hwnd, IDCANCEL );
               break;
            }
            EndDialog( hwnd, IDOK );
            break;

         case IDCANCEL:
            EndDialog( hwnd, IDCANCEL );
            break;
      }
      break;
   }							      
   return ( 0 );
}
					      
   /*******************************************************************
    *                         ListArchive2()                          *
    *                                                                 *
    * Display dates of data saved in archive directory.               *
    *                                                                 *
    *  Arguments:                                                     *
    *                                                                 *
    *******************************************************************/

long WINAPI ListArchive2DlgProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   int   iIndex;         /* Index of selected file */
   static char  szBuffer[64];   /* Directory to show */
   static char  szBuffer2[64];  /* File chosen */
   static char  szBuffer3[64];  /* File chosen with path */
   char  szCurDir[64];   /* Temporary stash for programs dir */

   switch( msg ) 
   {
      case WM_INITDIALOG:
/* Clear list box */
         SendDlgItemMessage( hwnd, IDD_ARCH_FILE, LB_RESETCONTENT, 0, 0 );
/* Save current directory */
         GetCurrentDirectory( sizeof( szCurDir ), szCurDir );
/* Set currect directory to data archive directory */
         strcpy( szBuffer, Gparm.DataPath );
         strcat (szBuffer, "\\");
/* Remove [ ] from chosen directory */
         strncat( szBuffer, &szArchSelect[1], strlen( szArchSelect )-2 );
         strcat( szBuffer, "\0" );
         if ( !SetCurrentDirectory( szBuffer ) )
         {
            logit( "t", "Archive Time Dir not set, %s\n", szBuffer );
            EndDialog( hwnd, IDCANCEL );
         }
/* Load the list box with data files in directory */
         SendDlgItemMessage( hwnd, IDD_ARCH_FILE, LB_DIR, DDL_READWRITE,
                            (LPARAM) "*.S*" );
/* Return the directory to what it was */
         SetCurrentDirectory( szCurDir );
         SetFocus( hwnd );
         break;

      case WM_COMMAND:
         switch ( LOWORD( wParam ) )
         {
            case IDD_ARCH_FILE:     /* A file has been selected */
               switch ( HIWORD( wParam ) )
               {
                  case LBN_DBLCLK:  /* User must double click on selection */
/* Obtain the index of selection */
                     iIndex = SendDlgItemMessage( hwnd, IDD_ARCH_FILE, 
                              LB_GETCURSEL, 0, 0 );
/* Read the selected text */
                     if ( iIndex != LB_ERR )
                        SendDlgItemMessage( hwnd, IDD_ARCH_FILE, LB_GETTEXT,
                                            iIndex, (LPARAM) szBuffer2 );
                     else           /* Nothing selected */
                        EndDialog( hwnd, IDCANCEL );
/* Convert file name into a MJS time */
                     strcpy( szBuffer3, szBuffer );
                     strcat( szBuffer3, "\\" );
                     strcat( szBuffer3, szBuffer2 );
                     dTimeRequested = GetTimeFromFileName( szBuffer3 );
                     EndDialog( hwnd, IDOK );
                     break;
               }
               break;

	    case IDOK:   /* Entering OK the same as double-click */
/* Obtain the index of selection */
               iIndex = SendDlgItemMessage( hwnd, IDD_ARCH_FILE, 
                                            LB_GETCURSEL, 0, 0 );
/* Read the selected text */
               if ( iIndex != LB_ERR )	
                  SendDlgItemMessage( hwnd, IDD_ARCH_FILE, LB_GETTEXT,
                                      iIndex, (LPARAM) szBuffer2 );
               else      /* Nothing selected */
                  EndDialog( hwnd, IDCANCEL );
/* Convert file name into a MJS time */
               strcpy( szBuffer3, szBuffer );
               strcat( szBuffer3, "\\" );
               strcat( szBuffer3, szBuffer2 );
               dTimeRequested = GetTimeFromFileName( szBuffer3 );
               EndDialog( hwnd, IDOK );
               break;

            case IDCANCEL:
               EndDialog( hwnd, IDCANCEL );
               break;
         }
         break;
   }							      
   return ( 0 );
}

      /***********************************************************
       *                      WndProc()                          *
       *                                                         *
       *  This dialog procedure processes messages from the      *
       *  windows screen.  All                                   *
       *  display is performed in PAINT.  Menu options (on main  *
       *  menu and through right button clicks) control the      *
       *  display.                                               *
       *                                                         *
       ***********************************************************/
       
long WINAPI WndProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   static  int    cxScreen, cyScreen;        /* Window size in pixels */
   static  double dTimeRequestedLast;/* Last time requested (must go sequentially) */
   struct tm    *gmt;       /* Time to set system clock */
   HCURSOR hCursor;         /* Present cursor handle (hourglass or arrow) */
   static  HDC hdc;         /* Device context of screen */
   static  HANDLE  hMenu;   /* Handle to the menu */
   int     i;
   static  int iPause;      /* 1->data flow pause; 0->normal */
   static  long lTitleFHt, lTitleFWd; /* Font height and width */
   time_t  ltime;           /* Requested time converted to time_t format */
   PAINTSTRUCT ps;          /* Paint structure used in WM_PAINT command */
   static  char *pszFileToOpen;  /* File Name which should contain archived data */
   static  char *pszFileToOpenLP;/* LP File Name which should contain archived data */
   int     rc;              /* return code */
   RECT    rct;             /* RECT (rectangle structure) */
   SYSTEMTIME    st;        /* UTC time with which to set PC clock */
   struct tm *tm;           /* time structure for the file name time */

/* Respond to user input (menu choices, etc.) and system messages */
   switch ( msg )
   {
      case WM_CREATE:         /* Do this the first time through */
         iStartThread = 0;
         iPause = 0;
         dTimeRequestedLast = 0.;
         hCursor = LoadCursor( NULL, IDC_ARROW );
         SetCursor( hCursor );
         hMenu = GetMenu( hwnd );
         break;
		 
/* Get screen size in pixels, re-paint, and re-proportion screen */
      case WM_SIZE:
         cyScreen = HIWORD (lParam);
         cxScreen = LOWORD (lParam);
		 
/* Compute font size */
         lTitleFHt = cyScreen / 25;
         lTitleFWd = cxScreen / 80;
	 
         InvalidateRect( hwnd, NULL, TRUE );    /* Force a re-PAINT */
         break;

      case WM_TIMER:		
         if ( wParam == ID_TIMER )              /* Update counter */
            InvalidateRect( hwnd, NULL, TRUE ); /* Force a re-PAINT */            
         break;

/* Respond to menu selections */
      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {
/* List and choose from data in archive */
            case IDM_DATAFROMCD:	
               if ( iStartThread == 1 )   /* Is WThread already running? */
               {
                  logit( "et", "Sender thread already running\n" );
                  break;
               }
               else
                  if ( DialogBox (hInstMain, "ListArchive1", hwndWndProc, 
                      (DLGPROC) ListArchive1DlgProc) == IDCANCEL ) break;

/* This time must be after the last quake, since EW modules assume data
   progresses forward in time */
               if ( dTimeRequested <= dTimeRequestedLast )
               {
                  logit( "t", "Time requested (%lf) prior to last (%lf)\n", 
                         dTimeRequested, dTimeRequestedLast );
                  MessageBeep( MB_ICONEXCLAMATION );
                  MessageBox( hwnd, "Archived quakes must be selected in "
                                    "order.  To select an earlier quake, "
                                    "restart Player", NULL,
                                    MB_ICONEXCLAMATION | MB_OK );
                  break;
               }
	 
/* Set SystemTime to dTimeRequested */	 
               GetSystemTime (&st);
//               ltime = (time_t) (dTimeRequested+10.);
               ltime = (time_t) (dTimeRequested);
               gmt = TWCgmtime (ltime);
               st.wYear = gmt->tm_year+1900;
               st.wMonth = gmt->tm_mon+1;             
               st.wDay = gmt->tm_mday;
               st.wHour = gmt->tm_hour;                       
               st.wMinute = gmt->tm_min;
               st.wSecond = gmt->tm_sec;
               st.wMilliseconds = 0;
               SetSystemTime (&st);
               if ( Gparm.Debug > 0 ) logit( "", "System time set\n" );
                
               dTimeRequestedLast = dTimeRequested;
               PostMessage (hwnd, WM_GET_DATA, 0, 0);
               break;

            case IDM_PAUSE:         /* Pause (or unpause) data retrieval */
               if ( iPause == 0 && iStartThread == 1)
               {
                  iStartThread = 0;
                  iPause = 1;
               }
               else if ( iPause == 1 )
               {
                  iStartThread = 1;
                  iPause = 0;
               }
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_STOP:          /* Stop data retrieval */
               iStartThread = 0;
               KillTimer( hwndWndProc, ID_TIMER );
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_REFRESH:       /* redraw the screen */
               InvalidateRect( hwnd, NULL, TRUE );
               break;
         }
         break;

/* Read data */
      case WM_GET_DATA:
         for (i=0; i<MAXWVS; i++)   /* Init some variables */
         {
            memset (StaArray[i].plRawCircBuff, 0,
                     sizeof (long)*StaArray[i].lRawCircSize);
            memset (StaArrayLP[i].plRawCircBuff, 0,
                     sizeof (long)*StaArrayLP[i].lRawCircSize);
            StaArray[i].lSampNew   = 0;   /* Sample counter */
            StaArrayLP[i].lSampNew = 0;
         }

/* First, get the full data station parameters */
         pszFileToOpen = CreateFileName( dTimeRequested, Gparm.FileLength, 
                                         Gparm.DataPath, Gparm.FileSuffix );
         Nsta = ReadDiskHeader( pszFileToOpen, StaArray, MAXWVS );
         if ( Nsta <= 0 ) 
         {
            logit( "et", "NSta (%ld) too small\n", Nsta );
            break;
         }
         logit( "et", "%ld stations to read in full data file (%s)\n", Nsta,
                pszFileToOpen );
         if ( CreateLineupFile( Gparm.LineupFileBB, StaArray, Nsta ) == -1 )
         {
            logit( "et", "BB Lineup file not created\n" );
            break;
         }
		
/* Then, get the LP data station parameters */
         ltime = (time_t) (floor( dTimeRequested ));
         tm = TWCgmtime( ltime );
         dTimeRequestedLP = floor( dTimeRequested - ((double) (tm->tm_min
          % Gparm.FileLengthLP)*60.) - (double) tm->tm_sec );
         pszFileToOpenLP = CreateFileName( dTimeRequestedLP, Gparm.FileLengthLP, 
                                           Gparm.DataPath, Gparm.FileSuffixLP );
         NstaLP = ReadDiskHeader( pszFileToOpenLP, StaArrayLP, MAXWVS );
         if ( NstaLP <= 0 ) 
         {
            logit( "et", "NStaLP (%ld) too small\n", NstaLP );
            break;
         }
         logit( "et", "%ld stations to read in LP data file (%s)\n", NstaLP,
                pszFileToOpenLP );
         if ( CreateLineupFile( Gparm.LineupFileLP, StaArrayLP, NstaLP ) == -1 )
         {
            logit( "et", "LP Lineup file not created\n" );
            break;
         }
		
/* Read full set of data */    
         iReadBB = 0;
         rc = ReadDiskDataForMTSolo( Gparm.FileLength, Gparm.ReadLength,
               Gparm.DataPath, Gparm.FileSuffix, dTimeRequested, Nsta,
               StaArray, &iReadBB );
         if ( rc != 0 )                  
         {
            logit( "t", "Full data file read problem\n" );
            break;
         }
         iReadBB *= Gparm.FileLength;
         if ( Gparm.Debug > 0 ) logit( "", "Past SP read; rc=%ld\n", rc );
         for (i=0; i<Nsta; i++)     /* Init start time variables */ 
         {
/* dTrigTime keeps track of what has been sent (Data start time here) */			      
            StaArray[i].dTrigTime = StaArray[i].dEndTime - 
             ((double) (StaArray[i].lRawCircCtr)/StaArray[i].dSampRate);
            if ( Gparm.Debug > 1 ) logit( "", "i=%ld, %s trig=%lf\n", i, StaArray[i].szStation, StaArray[i].dTrigTime );  
            if ( Gparm.Debug > 1 ) logit( "", "end=%lf ctr=%ld SR=%lf\n", StaArray[i].dEndTime,
                                    StaArray[i].lRawCircCtr, StaArray[i].dSampRate);  
         }
			 
/* Read LP set of data */
         iReadLP = 0;
         rc = ReadDiskDataForMTSolo( Gparm.FileLengthLP, Gparm.ReadLengthLP,
               Gparm.DataPath, Gparm.FileSuffixLP, dTimeRequestedLP, NstaLP,
               StaArrayLP, &iReadLP );
         if ( rc != 0 )
         {
            logit( "t", "LP data file read problem\n" );
            break;
         }
         iReadLP *= Gparm.FileLengthLP;
         if ( Gparm.Debug > 0 ) logit( "", "Past LP read; rc=%ld\n", rc );              
         for (i=0; i<NstaLP; i++)   /* Init start time variables */
/* dTrigTime keeps track of what has been sent (Data start time here) */			      
            StaArrayLP[i].dTrigTime = StaArrayLP[i].dEndTime - 
             ((double) (StaArrayLP[i].lRawCircCtr)/StaArrayLP[i].dSampRate);

/* Start the waveform sender thread */
         iStartThread = 1;
		 
/* Start a timer to control counter updates */
         SetTimer( hwndWndProc, ID_TIMER, RESET_INTERVAL, NULL );		 

         InvalidateRect (hwnd, NULL, TRUE);
         if ( Gparm.Debug > 0 ) logit( "", "Done with GET_DATA\n" );
         break;

/* Fill in screen display with information */
      case WM_PAINT:
         hdc = BeginPaint( hwnd, &ps );         /* Get device context */
         GetClientRect( hwnd, &rct );
         FillRect( hdc, &rct, (HBRUSH) GetStockObject( WHITE_BRUSH ) );
         if ( iStartThread == 1 )
            DisplayCounter( hdc, lTitleFHt, lTitleFWd, cxScreen, cyScreen,
             StaArray, StaArrayLP, &Gparm);
         if ( iPause == 1 )
            DisplayPause( hdc, lTitleFHt, lTitleFWd, cxScreen, cyScreen );
         EndPaint( hwnd, &ps );
         break;

/* Close up shop and return */
      case WM_DESTROY:
         logit( "", "WM_DESTROY posted\n" );
         iStartThread = 2;               /* Close Thread */
         KillTimer( hwndWndProc, ID_TIMER );
         PostQuitMessage( 0 );
         break;

      default:
         return ( DefWindowProc( hwnd, msg, wParam, lParam ) );
   }
return 0;
}

      /*********************************************************
       *                     WThread()                         *
       *                                                       *
       *  This thread sends earthworm waveform messages from   *
       * WC/ATWC format disk files.                            *
       *                                                       *
       *********************************************************/
	   
thr_ret WThread( void *dummy )
{
   int           i, j, iTemp, iNum;
   int           iDoneLP, iDoneFull; /* Flags which show when all data sent */
   int           iPacketSize;     /* Number of samples in this packet to send */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */
   static time_t now;             /* Current time */
   int           rc;              /* Return code from tport_putmsg() */
   char         *traceBuf;        /* Where the trace message is assembled */
   long         *traceDat;        /* Where the data points are stored in the trace msg */
   TRACE_HEADER *traceHead;       /* Where the trace header is stored */
   int           traceBufSize;    /* Size of the trace buffer, in bytes */

   sleep_ew( 5000 );              /* Let everything else start */

   if ( Gparm.Debug > 0 ) logit( "", "In WThread\n" );
   traceBufSize = sizeof (TRACE_HEADER) + (MAX_SAMPLES * sizeof (long));
   logit( "t", "Trace buffer size: %d bytes\n", traceBufSize );
   traceBuf = (char *) malloc( traceBufSize );
   if ( traceBuf == NULL )
   {
      logit( "", "Cannot allocate the trace buffer\n" );
      free( WaveBuf );
      for ( i=0; i<MAXWVS; i++ ) free( StaArray[i].plRawCircBuff );
      for ( i=0; i<MAXWVS; i++ ) free( StaArrayLP[i].plRawCircBuff );
      goto End;
   }
   if ( Gparm.Debug > 0 ) logit( "", "traceBuf allocated\n" );  
   traceHead = (TRACE_HEADER *) &traceBuf[0];
   traceDat  = (long *) &traceBuf[sizeof(TRACE_HEADER)];
   
   for (;;)
   {	  
/* Are we supposed to shut down?
   *****************************/
      if ( tport_getflag( &Gparm.InRegion ) == TERMINATE ) goto End;
		 
/* Loop to send waveform messages
   ******************************/
      while ( iStartThread >= 1 )
      {
/* Program is shutting down
   ************************/
         if ( Gparm.Debug > 1 ) logit( "", "StartThread >= 1\n" );  
         if ( iStartThread == 2 ) goto End;
         iDoneLP   = 0;
         iDoneFull = 0;
	  
/* Are we supposed to shut down?
   *****************************/
         if ( tport_getflag( &Gparm.InRegion ) == TERMINATE ) goto End;
   
/* Flush the output waveform rings
   *******************************/
         while ( tport_getmsg( &Gparm.InRegion, &getlogoW, 1, &logo, &MsgLen,
                                WaveBuf, MAX_TRACEBUF_SIZ ) != GET_NONE );
         while ( tport_getmsg( &Gparm.InRegionLP, &getlogoW, 1, &logo, &MsgLen,
                                WaveBuf, MAX_TRACEBUF_SIZ ) != GET_NONE );
         time( &now );

/* Have we already finished with this data?
   ****************************************/
         if ( ((dTimeRequested + (((double) iReadBB*60.)-(double) PACKET_LENGTH)) -
                (double) now)  > 0. )
         {
/* Loop through the stations
   *************************/
            if ( Gparm.Debug > 1 ) logit( "", "Send some BB data\n" );  
            for ( i=0; i<Nsta; i++ )
            {  
/* Fill the trace buffer header
   ****************************/
               traceHead->samprate   = StaArray[i].dSampRate;
               traceHead->quality[0] = '\0';
               traceHead->quality[1] = '\0';
               strcpy( traceHead->datatype, "i4" );
               strcpy( traceHead->sta,  StaArray[i].szStation );
               strcpy( traceHead->net,  StaArray[i].szNetID );
               strcpy( traceHead->chan, StaArray[i].szChannel );
               traceHead->pinno = i;
               iPacketSize = PACKET_LENGTH * (int) (StaArray[i].dSampRate+0.001);
	 
/* Catch data up to present time - DELAY_TIME (full data)
   ******************************************************/	 
               while ( ((double) now - StaArray[i].dTrigTime) >= DELAY_TIME )
               {
/* Transfer samples from RawCircBuff to traceBuf
   *********************************************/
                  iTemp = StaArray[i].lSampNew;
                  iNum = MAX_SAMPLES;
                  if ( iPacketSize < iNum ) iNum = iPacketSize;
                  for ( j=0; j<iNum; j++ )
                  {
                     traceDat[j] = StaArray[i].plRawCircBuff[iTemp+j];
                     StaArray[i].dTrigTime += (1./StaArray[i].dSampRate);
                     StaArray[i].lSampNew += 1;
                     if ( StaArray[i].lSampNew >= StaArray[i].lRawCircCtr ) break;
                  }

/* Set the trace start and end times.
   Times are in seconds since midnight 1/1/1970
   ********************************************/
                  traceHead->nsamp     = iNum;
                  traceHead->starttime = StaArray[i].dTrigTime -
                                         ((double) iNum/StaArray[i].dSampRate);
                  traceHead->endtime   = StaArray[i].dTrigTime -
                                         1./StaArray[i].dSampRate;
                  traceBufSize = sizeof (TRACE_HEADER) + (iNum*sizeof (long));

/* Send the Tracebuf message to the RING.
   **************************************/
                  if ( traceHead->starttime > dTimeRequested )
                  {
                     rc = tport_putmsg( &Gparm.InRegion, &getlogoW, traceBufSize,
                                         traceBuf );
      
                     if ( rc == PUT_TOOBIG )
                        printf( "Trace message for channel %s too big\n",
                                 StaArray[i].szStation );

                     if ( rc == PUT_NOTRACK )
                        printf( "Tracking error while sending channel %s\n",
                                 StaArray[i].szStation );
                  }
               }
            }			
         }
         else                /* Done with full data */
            iDoneFull = 1;

/* Have we already finished with the LP data?
   ******************************************/
         if ( ((dTimeRequestedLP + (((double) iReadLP*60.)-(double) PACKET_LENGTH)) -
                (double) now) > 0. )
         {
 	 
/* Loop through the LP stations
   ****************************/
            if ( Gparm.Debug > 1 ) logit( "", "Send some LP data\n" );  
            for ( i=0; i<NstaLP; i++ )
            {  
/* Fill the trace buffer header
   ****************************/
               traceHead->samprate   = StaArrayLP[i].dSampRate;
               traceHead->quality[0] = '\0';
               traceHead->quality[1] = '\0';
               strcpy( traceHead->datatype, "i4" );
               strcpy( traceHead->sta,  StaArrayLP[i].szStation );
               strcpy( traceHead->net,  StaArrayLP[i].szNetID );
               strcpy( traceHead->chan, StaArrayLP[i].szChannel );
               traceHead->pinno = i;
               iPacketSize = PACKET_LENGTH * (int) (StaArrayLP[i].dSampRate+0.001);
	 
/* Catch data up to present time - DELAY_TIME (LP data)
   ****************************************************/	 
               while ( (now - StaArrayLP[i].dTrigTime) >= DELAY_TIME )
               {
/* Transfer samples from RawCircBuff to traceBuf
   *********************************************/
                  iTemp = StaArrayLP[i].lSampNew;
                  iNum = MAX_SAMPLES;
                  if ( iPacketSize < iNum ) iNum = iPacketSize;
                  for ( j=0; j<iNum; j++ )
                  {
                     traceDat[j] = StaArrayLP[i].plRawCircBuff[iTemp+j];
                     StaArrayLP[i].dTrigTime += (1./StaArrayLP[i].dSampRate);
                     StaArrayLP[i].lSampNew += 1;
                     if ( StaArrayLP[i].lSampNew >= StaArrayLP[i].lRawCircCtr )
                        break;
                  }

/* Set the trace start and end times.
   Times are in seconds since midnight 1/1/1970
   ********************************************/
                  traceHead->nsamp     = iNum;
                  traceHead->starttime = StaArrayLP[i].dTrigTime -
                                         ((double) iNum/StaArrayLP[i].dSampRate);
                  traceHead->endtime   = StaArrayLP[i].dTrigTime - 
                                         1./StaArrayLP[i].dSampRate;
                  traceBufSize = sizeof (TRACE_HEADER) + (iNum*sizeof (long));

/* Send the Tracebuf message to the RING.
   **************************************/
                  if ( traceHead->starttime > dTimeRequested )
                  {
                     rc = tport_putmsg( &Gparm.InRegionLP, &getlogoW, traceBufSize,
                                         traceBuf );

                     if ( rc == PUT_TOOBIG )
                        printf( "Trace message for channel %s too big\n",
                                 StaArrayLP[i].szStation );

                     if ( rc == PUT_NOTRACK )
                        printf( "Tracking error while sending channel %s\n",
                                 StaArrayLP[i].szStation );
                  }
               }
            }			
         }
         else                /* Done with LP */
            iDoneLP = 1;			
         if ( iDoneFull == 1 && iDoneLP == 1 )
         {
            iStartThread = 0;			
            KillTimer( hwndWndProc, ID_TIMER );
         }			
         sleep_ew( 1000 );
      }
      sleep_ew( 1000 );
   }

/* Time to shutdown
   ****************/   
   End:
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
   KillSelfThread(); 
}
