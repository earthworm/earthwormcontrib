/******************************************************************
 *                     File ATPlayer.h                            *
 *                                                                *
 *  Include file for ATPlayer module                              *
 ******************************************************************/

#include <trace_buf.h>
#include "\earthworm\atwc\src\libsrc\earlybirdlib.h"

/* Definitions
   ***********/
#define DELAY_TIME       15    /* Time (seconds) to delay data send */
#define PACKET_LENGTH     5    /* Data packet length in seconds */
#define MAX_SAMPLES     500    /* Maximum samples to put in traceBuf packet */
#define RESET_INTERVAL 5000    /* Reset counter interval in milliseconds */
   
// User window messages
#define WM_GET_DATA (WM_USER + 34)

#define ID_TIMER  1             /* Reset counter */   

/* Menu options
   ************/
#define IDM_DATAFROMCD      110     /* Read old data from CD   */		
#define IDM_PAUSE           120	    /* Pause playback */
#define IDM_STOP            130	    /* Stop playback */
#define IDM_REFRESH         140	    /* Refresh screen */

/* Entry fields
   ************/
#define EF_NUMSTATODISP     3000    /* For TracesPerScreen dialog */
#define EF_DISPLAYYEAR      3002    /* For StationStatus dialog */
#define EF_DISPLAYMONTH     3003 
#define EF_DISPLAYDAY       3004
#define EF_DISPLAYHOUR      3005 
#define EF_DISPLAYTOTALTIME 3006

#define ID_NULL            -1       /* Resource file declaration */

#define IDD_ARCH_FILE	1040

typedef struct {
   long InKey;                    /* Key to ring where waveforms live */
   long InKeyLP;                  /* Key to LP ring where waveforms live */
   int  FileLength;               /* Full data file length in minutes */
   int  FileLengthLP;             /* LP data file length in minutes */
   int  ReadLength;               /* Number minutes of full data to read */
   int  ReadLengthLP;             /* Number minutes of LP data to read */
   char FileSuffix[8];            /* File suffix for full data */
   char FileSuffixLP[8];          /* File suffix for LP data */
   int  Debug;                    /* If 1, print debug messages */
   unsigned char MyModId;         /* Module id of this program */
   char DataPath[64];             /* Path to put latency information */
   char LineupFileBB[64];         /* File created to show data lineup */
   char LineupFileLP[64];         /* File created to show data lineup */
   SHM_INFO InRegion;             /* Info structure for input region */
   SHM_INFO InRegionLP;           /* Info structure for input LP region */
} GPARM;

typedef struct {
   unsigned char MyInstId;        /* Local installation */
   unsigned char GetThisInstId;   /* Get messages from this inst id */
   unsigned char GetThisModId;    /* Get messages from this module */
   unsigned char TypeHeartBeat;   /* Heartbeat message id */
   unsigned char TypeError;       /* Error message id */
   unsigned char TypeWaveform;    /* Earthworm waveform messages */
} EWH;

/* Function declarations for ATPlayer
   **********************************/
void    DisplayCounter( HDC, long, long, int, int, STATION *, STATION *, 
                        GPARM * );
void    DisplayPause( HDC, long, long, int, int );
int     CreateLineupFile( char *, STATION *, int );
int     GetEwh( EWH * );
long WINAPI ListArchive1DlgProc (HWND, UINT, UINT, long); 
long WINAPI ListArchive2DlgProc (HWND, UINT, UINT, long); 
long WINAPI WndProc( HWND, UINT, UINT, long );
thr_ret WThread( void * );

int     GetConfig( char *, GPARM * );                    /* config.c */
void    LogConfig( GPARM * );
