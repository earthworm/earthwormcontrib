#include <stdio.h>
#include <string.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>
#include "ATPlayer.h"

#define ncommand 13          /* Number of commands in the config file */


 /***********************************************************************
  *                              GetConfig()                            *
  *             Processes command file using kom.c functions.           *
  *               Returns -1 if any errors are encountered.             *
  ***********************************************************************/

int GetConfig( char *config_file, GPARM *Gparm )
{
   char     init[ncommand];     /* Flags, one for each command */
   int      nmiss;              /* Number of commands that were missed */
   int      nfiles;
   int      i;
   
/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i=0; i<ncommand; i++ ) init[i] = 0;

/* Open the main configuration file
   ********************************/
   nfiles = k_open( config_file );
   if ( nfiles == 0 )
   {
      fprintf( stderr, "ATPlayer: Error opening configuration file <%s>\n",
               config_file );
      return -1;
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          /* Get the first token from line */

         if ( !com ) continue;             /* Ignore blank lines */
         if ( com[0] == '#' ) continue;    /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               fprintf( stderr, "ATPlayer: Error opening command file <%s>."
			            "\n", &com[1] );
               return -1;
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         if ( k_its( "InRing" ) )
         {
            if ( str = k_str() )
            {
               if( (Gparm->InKey = GetKey(str)) == -1 )
               {
                  fprintf( stderr, "ATPlayer: Invalid InRing name <%s>. "
                                   "Exiting.\n", str );
                  return -1;
               }
            }
            init[0] = 1;
         }
		 
         if ( k_its( "InRingLP" ) )
         {
            if ( str = k_str() )
            {
               if( (Gparm->InKeyLP = GetKey(str)) == -1 )
               {
                  fprintf( stderr, "ATPlayer: Invalid InRingLP name <%s>. "
                                   "Exiting.\n", str );
                  return -1;
               }
            }
            init[1] = 1;
         }
		 
         else if ( k_its( "Debug" ) )
         {
            Gparm->Debug = k_int();
            init[2] = 1;
         }
		 
         else if ( k_its( "MyModId" ) )
         {
            if ( str = k_str() )
            {
               if ( GetModId(str, &Gparm->MyModId) == -1 )
               {
                  fprintf( stderr, "ATPlayer: Invalid MyModId <%s>.\n", str);
                  return -1;
               }
            }
            init[3] = 1;
         }
		 
         else if ( k_its( "DataPath" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->DataPath, str );
            init[4] = 1;
         }
		                
         else if ( k_its( "LineupFileBB" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->LineupFileBB, str );
            init[5] = 1;
         }
		 
         else if ( k_its( "LineupFileLP" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->LineupFileLP, str );
            init[6] = 1;
         }
		 
         else if ( k_its( "FileLength" ) )
         {
            Gparm->FileLength = k_int();
            if ( Gparm->FileLength <= 0 )
            {
               fprintf( stderr, "ATPlayer: Invalid FileLength <%ld>.\n",
                        Gparm->FileLength);
               return -1;
            }
            init[7] = 1;
         }
		 
         else if ( k_its( "FileLengthLP" ) )
         {
            Gparm->FileLengthLP = k_int();
            if ( Gparm->FileLengthLP <= 0 )
            {
               fprintf( stderr, "ATPlayer: Invalid FileLengthLP <%ld>.\n",
                        Gparm->FileLengthLP);
               return -1;
            }
            init[8] = 1;
         }
		 
         else if ( k_its( "FileSuffix" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->FileSuffix, str );
            init[9] = 1;
         }
		 
         else if ( k_its( "FileSuffixLP" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->FileSuffixLP, str );
            init[10] = 1;
         }
		 
         else if ( k_its( "ReadLength" ) )
         {
            Gparm->ReadLength = k_int();
            if ( Gparm->ReadLength <= 0 )
            {
               fprintf( stderr, "ATPlayer: Invalid ReadLength <%ld>.\n",
                        Gparm->ReadLength);
               return -1;
            }
            init[11] = 1;
         }
		 
         else if ( k_its( "ReadLengthLP" ) )
         {
            Gparm->ReadLengthLP = k_int();
            if ( Gparm->ReadLengthLP <= 0 )
            {
               fprintf( stderr, "ATPlayer: Invalid ReadLengthLP <%ld>.\n",
                        Gparm->ReadLengthLP);
               return -1;
            }
            init[12] = 1;
         }
		 
/* An unknown parameter was encountered
   ************************************/
         else
         {
            fprintf( stderr, "ATPlayer: <%s> unknown parameter in <%s>\n",
                     com, config_file );
            continue;
         }
		 
/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            fprintf( stderr, "ATPlayer: Bad <%s> command in <%s>.\n", com,
                     config_file );
            return -1;
         }
      }
      nfiles = k_close();
   }
   
/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] ) nmiss++;
	   
   if ( nmiss > 0 )
   {
      fprintf( stderr, "ATPlayer: ERROR, no " );
      if ( !init[0] ) fprintf( stderr, "<InRing> " );
      if ( !init[1] ) fprintf( stderr, "<InRingLP> " );
      if ( !init[2] ) fprintf( stderr, "<Debug> " );
      if ( !init[3] ) fprintf( stderr, "<MyModId> " );
      if ( !init[4] ) fprintf( stderr, "<DataPath> " );
      if ( !init[5] ) fprintf( stderr, "<LinupFileBB> " );
      if ( !init[6] ) fprintf( stderr, "<LinupFileLP> " );
      if ( !init[7] ) fprintf( stderr, "<FileLength> " );
      if ( !init[8] ) fprintf( stderr, "<FileLengthLP> " );
      if ( !init[9] ) fprintf( stderr, "<FileSuffix> " );
      if ( !init[10] ) fprintf( stderr, "<FileSuffixLP> " );
      if ( !init[11] ) fprintf( stderr, "<ReadLength> " );
      if ( !init[12]) fprintf( stderr, "<ReadLengthLP> " );
      fprintf( stderr, "command(s) in <%s>. Exiting.\n", config_file );
      return -1;
   }
   return 0;
}

 /***********************************************************************
  *                              LogConfig()                            *
  *                                                                     *
  *                   Log the configuration parameters                  *
  ***********************************************************************/

void LogConfig( GPARM *Gparm )
{
   logit( "", "\n" );
   logit( "", "InKey:            %d\n",    Gparm->InKey );
   logit( "", "InKeyLP:          %d\n",    Gparm->InKeyLP );
   logit( "", "Debug:            %d\n",    Gparm->Debug );
   logit( "", "MyModId:          %u\n",    Gparm->MyModId );
   logit( "", "FileLength:       %ld\n",   Gparm->FileLength );
   logit( "", "FileLengthLP:     %ld\n",   Gparm->FileLengthLP );
   logit( "", "ReadLength:       %ld\n",   Gparm->ReadLength );
   logit( "", "ReadLengthLP:     %ld\n",   Gparm->ReadLengthLP );
   logit( "", "FileSuffix:       %s\n",    Gparm->FileSuffix );
   logit( "", "FileSuffixLP:     %s\n",    Gparm->FileSuffixLP );
   logit( "", "LineupFileBB:     %s\n",    Gparm->LineupFileBB );
   logit( "", "LineupFileLP:     %s\n",    Gparm->LineupFileLP );
   logit( "", "DataPath:         %s\n\n",  Gparm->DataPath );
   return;
}
