	program mnames

c	MNAMES reads the ASCII file NAMES.ASC sequentially, and writes an
c	identical unformatted direct access file.

c	Each of the 729 records has a length of 32 characters and contains the
c	Flinn-Engdahl geographical region name whose F-E number corresponds to
c	that of its record number.

c	Input file: NAMES.ASC
c	Output file: NAMES.FER

c	TO MAINTAIN THE 'sedas$fenames' FILE 'names.fer':
c		1)  Edit names.asc, preserving 32 character record length
c		2)  run MNAMES
c		3)  copy names.fer to <sedas.permfiles>
c		4)  set prot=(gro:RWED,wor:R)

c	Written by G.J. Dunphy  -  April 1983

c       i/p file spec.
	character*128 input
	character*32 name
c       o/p file spec.
	character*128 output

	integer*2 regnum

	open (unit=1,access='sequential',form='formatted',status='old',
     1        file='names.asc')
	open (unit=2, access='direct',form='unformatted',recl=32,
     1        status='new', file='names.fer')

	for (regnum = 1; regnum <= 729; regnum = regnum + 1)
	do 1 regnum = 1, 729
	      read (1, '(a32)') name
	      write (2, rec=regnum) name
1             continue
	close (1)
	close (2)
	end
