/*	program newidx

c	NEWIDX is the first of 3 programs that must be executed to cre-
c       ate the files necessary for efficient retrieval of the F-E geo-
c       graphical region numbers and names.  See also programs FEBNDY
c       and MNAMES.

c	Input: file QUADSIDX.ASC
c	Output: file LLINDX.FER

c	File QUADSIDX.ASC is an ASCII file used primarily to transport
c       these geographical data to other computer systems.

c	File LLINDX.FER is used by program FEBNDY to create file
c       LATTIERS.FER. Both LLINDX.FER & LATTIERS.FER are permanent files
c       used to retrieve the geographical region number.

c	QUADSIDX.ASC consists of 52 56 byte records.  Each record con-
c       tains 7	4-character numbers, each preceded by 4 blanks and right
c       justified with blank fill to the left, i.e. the record format is
c       (7(4x,i4)).

c	There are 13 of these records for each quadrant of the globe,
c       i.e. 91	numbers per quadrant, corresponding to latitude tiers 0
c       to 90, and -0 to -90.  The quadrant order is NE, NW, SE, and SW.

c	These numbers represent the number of longitudinal boundaries
c       employed in each tier of latitude.

c	NEWIDX creates a file similar to QUADSIDX.ASC.  However it is a
c       direct access file and numbers representing pointers into file
c       LATTIERS.FER are paired with each longitudinal boundary count.

c	Programmer  -  G.J. Dunphy  -  April 1980.

Converted to C 10/99 at wc&atwc by Whitmore

*/
#include <windows.h>
#include <winuser.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

void main (void)
{
FILE	*InFile, *OutFile;
int	iBuf[7], iCarry, i, j;

if ((InFile = fopen ("quadsidx.asc", "r")) == NULL)
    {
    printf ("quadsidx.asc file not opened\n");
    exit (0);
    }
if ((OutFile = fopen ("llindx.fer", "w")) == NULL)
    {
    printf ("llindx.fer file not opened\n");
    exit (0);
    }

iCarry = 1;
for (i=1; i<=52; i++)
    {
    for (j=0; j<7; j++) fscanf (InFile, "%ld", &iBuf[j]);
    fscanf (InFile, "\n");
    for (j=0; j<7; j++)
        {
	fprintf (OutFile, "%ld %ld\n", iCarry, iBuf[j]);
	iCarry += iBuf[j];
        }
    }
fclose (InFile);
fclose (OutFile);
}	
