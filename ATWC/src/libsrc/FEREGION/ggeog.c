/*
c	program to test namnum (and gtsnum, if comment characters removed)
	program ggeog

Converted to C 10/99 at wc&atwc by Whitmore

*/
#include <windows.h>
#include <winuser.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

PSZ namnum (double, double, int *);

void main (void)
{
double	dLat, dLon;		// Geographic lat/lon to convert
int	iFENum;			// FE region number
PSZ	pszFEName;		// FE region name

printf ("Latitude (degrees): \n");
scanf ("%lf", &dLat);	
printf ("Longitude (degrees): \n");
scanf ("%lf", &dLon);	
pszFEName = namnum (dLat, dLon, &iFENum);
printf ("\nFE region name: %s\nFE region number: %ld\n", pszFEName, iFENum);
return;	
}
