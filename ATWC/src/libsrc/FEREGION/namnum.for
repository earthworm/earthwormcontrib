	subroutine namnum (geoltd, geolnd, name, number)

c	Given the geographical coordinates in decimal degrees, returns
c	the Flinn - Engdahl geographical region number (I3) and
c	name (character*32).

c	entry point NAMNBR accepts coordinates in radians.

c	ARGUMENTS:
c		1.  geographical latitude in degrees (geoltd)  -   real
c		2.  geographical longitude in degrees (geolnd)  -  real
c		3.  geographical region name			-  character*32
c		4.  F-E geographical region number		-  integer

c	Note:  Subroutines getnum & getnam require logical units 1 & 2.  To
c	       minimize the number of logical units required, these units are
c	       opened & closed with each call.  This could proove grossly
c	       inefficient for certain applications.
c	Note:  Getnum and getnam now call gtunit to get the next available
c	       unit number, to prevent conflicts with programs which may have
c	       already opened other files using units 1 and 2. (BWP 8/29/91)

	integer number

	real deg, geoltd, geolnd, geoltr, geolnr

	character*32 name

	data deg /57.2957795/

	call getnum (geoltd, geolnd, number)
	call getnam (number, name)

	return

c	************
	entry namnbr (geoltr, geolnr, name, number)
c	************

	geoltd = geoltr * deg
	geolnd = geolnr * deg

	call getnum (geoltd, geolnd, number)
	call getnam (number, name)

	return
	end
