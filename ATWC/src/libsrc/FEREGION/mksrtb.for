	program mksrtb

c	Given file SEISR_DEF.ASC as input, MKSRTB creates file SEISRG.FER
c	as a fixed length record, direct access file. Subroutine GTSEIS, given
c	a geographical region number, can then use this file to obtain
c	the seimic region number.

	integer*2 b, e
	integer*4 i, j, k

	open (unit=1, access='sequential', form='formatted',status='old',
     +        file='SEISRDEF.ASC', readonly)
	open (unit=2, access='direct', form='unformatted', status='new',
     +        file='SEISRG.FER', recordtype='fixed', recl=1)
	do 2 i = 1, 50
	     read (1, '(i2,2(1x,i3))', iostat=ios) j, b, e
	     if (ios .ne. 0) write (6, '(a)') ' GASP!!!'
	     if (i .ne. j) write (6, '(a)') ' GAAAASP!!!'
	     do 1 k = b, e
1	        write (2, rec=k) i
2	continue
	end
