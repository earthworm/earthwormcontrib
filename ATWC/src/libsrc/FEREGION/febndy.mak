
#                    Nmake File For FEBNDY - Windows NT version

NODEBUG=1

!include <ntwin32.mak>

all:FEBNDY.exe

FEBNDY.obj: FEBNDY.c FEBNDY.mak
    $(cc) $(cflags) $(cvarsmt) FEBNDY.c

FEBNDY.exe: FEBNDY.obj FEBNDY.mak
    LINK $(conlflags) $(conlibsmt) FEBNDY.obj -out:FEBNDY.exe
