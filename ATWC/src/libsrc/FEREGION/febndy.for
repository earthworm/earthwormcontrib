	program febndy

c	FEBNDY is the second of three programs that must be executed to
c	create the files necessary for efficient retrieval of the F-E
c	geographical regions numbers and names.  Program NEWIDX must be 
c	executed before running FEBNDY.

c	Input files:
c		LLINDX.FER
c		NESECT.ASC
c		NWSECT.ASC
c		SESECT.ASC
c		SWSECT.ASC
c	Output file:
c		LATTIERS.FER

c	The four *.ASC files, representing each quadrant of the globe,
c       are ASCII files used primarily for transporting these data to
c       other computer systems.

c	These four files each consist of records of up to 80 bytes.
c       Each record contains up to 10 pairs of 4-character numbers,
c       right justified	with blank fill to left, i.e. the record format
c       is (20i4).

c	These paired numbers represent, respectively, the included lon-
c       gitudinal boundary and its region number.  Each record contains
c       only those ordered pairs pertaining to the same tier of lati-
c       tude.  Thus short records are used to complete a tier when nec-
c       essary.

c	FEBNDY simply converts these 4 quadrant files into 1 direct ac-
c       cess file.
c	File LLINDX contains pointers to the onset of each tier, paired
c       with the count of boundary-FE number pairs of that tier.

c	PROGRAMMER: G. J. Dunphy - April 1980

	character*10 sect, fmt

	integer*2 ip, op, quad, j, k, m, recnum, size, buf, onset,
     1		fulrec, intlft, kk, kkk, n, beg, fin

	dimension buf(70), beg(4), fin(4)

	data ip /7/, op /8/
	data beg /1, 92, 183, 274/
	data fin /91, 182, 273, 364/

	open (unit=ip, status='old', file='llindx.fer', access='direct',
     1	      form='unformatted', recl=1)

	open (unit=op,status='new', file='lattiers.fer', access='direct',
     1	      form='unformatted', recl=1)

	recnum = 0

	do 3 quad = 1, 4
	     if (quad.eq.1) then
	        sect = 'nesect.asc'
	     else if (quad.eq.2) then
	        sect = 'nwsect.asc'
             else if (quad.eq.3) then
                sect = 'sesect.asc'
             else
                sect = 'swsect.asc'
             end if

	     open (unit=quad,status='old',file=sect,access='sequential',
     1		   form='formatted')

	     rewind quad

	     do 2 j = beg(quad), fin(quad)
		  read (ip, rec=j, err=4) onset, size
		  fulrec = size / 10
		  kk = 0
		  kkk = 0
		  if (fulrec.gt.0) then
		       do 8 n = 1, fulrec
			    kk = (n-1) * 20 + 1
			    kkk = n * 20
			    read (quad,'(20i4)',end=5)(buf(k),k=kk,kkk)
8			    continue
		  end if

		  intlft = mod(size,10) * 2
		  if (intlft.gt.0) then
		       kk = kkk + 1
		       kkk = kkk + intlft
		       write (fmt, '(''('',i2,''i4)'')') intlft
c	write (6, '(3i6,a10)') intlft, kk, kkk, fmt
		       read (quad, fmt, end=5) (buf(k), k = kk, kkk)
		  end if

		  do 1  m = 1, size * 2, 2
		       recnum = recnum + 1
		       write(op, rec=recnum, err=6) buf(m), buf(m+1)
1		       continue
2	          continue
		  close (quad)
3	     continue

	goto 7
4	write (6, '(''end'', i6)') ip
	goto 7
5	write(6, '(''end'', 5i6)') j, n, fulrec, kk, kkk
	goto 7
6	write (6, '(''err'', i6)') op
7	continue
	close (ip)
	close (op)
	end
