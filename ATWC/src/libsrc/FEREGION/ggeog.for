c	program to test namnum (and gtsnum, if comment characters removed)
	program ggeog

	character*32 name
        character*1 ans
        character*17 qlat
	character*18 qlon
	integer number, seinum
	real ltd, lnd
	data qlat,qlon /' latitude (deg): ',' longitude (deg): '/
1	if (.true.) then
 	    write (6, '(a17,$)') qlat
	    read (5, *) ltd
	    write (6, '(a18,$)') qlon
            read (5, *) lnd
	    call namnum (ltd, lnd, name, number)
	    write (6,'(a)') ' F-E Geographic Region name and number:'
	    write (6, '(1x,a32,1x,i3.3)') name, number
c	    to test the seismic region retrieval routine, remove the comment
c	    character from the following 2 lines
c	    call gtsnum (number,seinum)
c	    write (6,'(a,i2.2)') ' Seismic region number: ', seinum
            write (6, '(1x,a,$)') ' Want more? (y or n):'
            read (5, '(a1)') ans
            if (ans.eq.'Y' .or. ans.eq.'y') goto 1
	endif
	end
