/*	program mnames

c	MNAMES reads the ASCII file NAMES.ASC sequentially, and writes an
c	identical unformatted direct access file.

c	Each of the 729 records has a length of 32 characters and contains the
c	Flinn-Engdahl geographical region name whose F-E number corresponds to
c	that of its record number.

c	Input file: NAMES.ASC
c	Output file: NAMES.FER

c	TO MAINTAIN THE 'sedas$fenames' FILE 'names.fer':
c		1)  Edit names.asc, preserving 32 character record length
c		2)  run MNAMES
c		3)  copy names.fer to <sedas.permfiles>
c		4)  set prot=(gro:RWED,wor:R)

c	Written by G.J. Dunphy  -  April 1983

Converted to C 10/99 at wc&atwc by Whitmore

*/
#include <windows.h>
#include <winuser.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

void main (void)
{
FILE	*InFile, *OutFile;
char	szName[33];

if ((InFile = fopen ("names.asc", "r")) == NULL)
    {
    printf ("names.asc file not opened\n");
    exit (0);
    }
if ((OutFile = fopen ("names.fer", "w")) == NULL)
    {
    printf ("names.fer file not opened\n");
    exit (0);
    }

while (!feof (InFile))
    {
    fgets (szName, sizeof (szName), InFile);
    fprintf (OutFile, "%s", szName);	
    }
fclose (InFile);
fclose (OutFile);
}
