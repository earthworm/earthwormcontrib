
#                    Nmake File For NEWIDX - Windows NT version

NODEBUG=1

!include <ntwin32.mak>

all:NEWIDX.exe

NEWIDX.obj: NEWIDX.c NEWIDX.mak
    $(cc) $(cflags) $(cvarsmt) NEWIDX.c

NEWIDX.exe: NEWIDX.obj NEWIDX.mak
    LINK $(conlflags) $(conlibsmt) NEWIDX.obj -out:NEWIDX.exe
