/*	program febndy

c	FEBNDY is the second of three programs that must be executed to
c	create the files necessary for efficient retrieval of the F-E
c	geographical regions numbers and names.  Program NEWIDX must be 
c	executed before running FEBNDY.

c	Input files:
c		LLINDX.FER
c		NESECT.ASC
c		NWSECT.ASC
c		SESECT.ASC
c		SWSECT.ASC
c	Output file:
c		LATTIERS.FER

c	The four *.ASC files, representing each quadrant of the globe,
c       are ASCII files used primarily for transporting these data to
c       other computer systems.

c	These four files each consist of records of up to 80 bytes.
c       Each record contains up to 10 pairs of 4-character numbers,
c       right justified	with blank fill to left, i.e. the record format
c       is (20i4).

c	These paired numbers represent, respectively, the included lon-
c       gitudinal boundary and its region number.  Each record contains
c       only those ordered pairs pertaining to the same tier of lati-
c       tude.  Thus short records are used to complete a tier when nec-
c       essary.

c	FEBNDY simply converts these 4 quadrant files into 1 direct ac-
c       cess file.
c	File LLINDX contains pointers to the onset of each tier, paired
c       with the count of boundary-FE number pairs of that tier.

c	PROGRAMMER: G. J. Dunphy - April 1980

Converted to C 10/99 at wc&atwc by Whitmore

*/
#include <windows.h>
#include <winuser.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

void main (void)
{
FILE	*InFile, *OutFile, *SectFile;		// File handles
char	szSect[12];				// Quadrant file name
int	i, j, k, kk, kkk, m, n;			// Counters
int	iBeg[4] = {0, 91, 182, 273};
int	iBuf[70];
int	iFin[4] = {90, 181, 272, 363};
int	iFulRec;
int	iIntLeft;
int	iOnset;
int	iSize;

if ((InFile = fopen ("llindx.fer", "r")) == NULL)
    {
    printf ("llindx.fer file not opened\n");
    exit (0);
    }
if ((OutFile = fopen ("lattiers.fer", "w")) == NULL)
    {
    printf ("lattiers.fer file not opened\n");
    exit (0);
    }
	
for (i=0; i<4; i++)
    {
    if (i == 0) strcpy (szSect, "nesect.asc");	
    if (i == 1) strcpy (szSect, "nwsect.asc");	
    if (i == 2) strcpy (szSect, "sesect.asc");	
    if (i == 3) strcpy (szSect, "swsect.asc");	
    SectFile = fopen (szSect, "r");
    for (j=iBeg[i]; j<=iFin[i]; j++)
        {
	fscanf (InFile, "%ld %ld\n", &iOnset, &iSize);
	iFulRec = (int) (iSize/10);
	kk = 0;
	kkk = 0;
	if (iFulRec > 0)
	    for (n=1; n<=iFulRec; n++)
	        {
		kk = (n-1)*20;
		kkk = n*20;
		for (k=kk; k<kkk; k++) fscanf (SectFile, "%ld", &iBuf[k]);
		}
	iIntLeft = (iSize%10) * 2;
	if (iIntLeft > 0)
	    {
	    kk = kkk;
	    kkk += iIntLeft;
	    for (k=kk; k<kkk; k++) fscanf (SectFile, "%ld", &iBuf[k]);
	    }
	for (m=0; m<iSize*2; m+=2)
	    fprintf (OutFile, "%ld %ld\n", iBuf[m], iBuf[m+1]);
        }
    fclose (SectFile);	
    }		 
fclose (InFile);
fclose (OutFile);	
}
