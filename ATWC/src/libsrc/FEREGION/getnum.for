	subroutine getnum (lat, lon, nbr)

c	PROCEDURE IS CALLED BY namnum

c	ARGUMENTS:
c		lat - geographic latitude in decimal degrees
c		lon - geographic longitude in decimal degrees
c		nbr - the Flinn - Engdahl geographical region number

c	FILES ACCESSED:
c			llindx.fer
c			lattiers.fer

c	A GEOGRAPHIC REGION CONTAINS ITS LOWER BOUNDARIES BUT NOT
c	ITS UPPER. (HERE UPPER AND LOWER ARE DEFINED IN TERMS
c	OF THEIR ABSOLUTE VALUES).  THE EQUATOR BELONGS TO THE NORTHERN
c	HEMISPHERE & THE GREENWICH MERIDIAN BELONGS TO THE EASTERN HEMISPHERE IN
c	THIS IMPLEMENTATION.  180.000 DEGREES LONGITUDE IS ASSIGNED TO THE
c	EASTERN HEMISPHERE.


c	COORDINATES ARE TRUNCATED TO WHOLE DEGREES BEFORE DETERMINING
c	NUMBER.

c	Note:  There are a maximum of 35 boundary-number-pairs/tier in file
c	       lattiers.fer.  The record length chosen (512 bytes) corresponds
c	       to a page size on the VAX/VMS system.
c	Written by G.J. Dunphy

c	Modified August 29, 1991 by B Presgrave to call library function
c	gtunit to determine the next available unit number for the open
c	statements. This is needed because many programs have long ago opened
c	another file on unit 1 or 2 before this routine is called. The close
c 	at the end then closes the other file, leaving the programmer scratching
c	his head about why adding a subroutine call causes his output to
c 	disappear completely!

	parameter (RECSIZ = 128)     ! Recordsize in longwords
	parameter (RSTMS2 = 256)     ! Twice RECSIZ
	parameter (RSPLS1 = 129)     ! RECSIZ + 1
	parameter (RSMNS1 = 127)     ! RECSIZ - 1
	parameter (RSMNS2 = 126)     ! RECSIZ - 2
	parameter (LASTRC =  46)     ! (TOTREC / RECSIZ) + 1 (TOTLEN = 5796)

c	BIBLIOGRAPHIC REFERENCES

c		Flinn, E.A. and E.R. Engdahl (1964). A proposed basis for geo-
c			graphical and seismic regionalization, Seismic Data
c			Laboratory Report No. 101, Earth Sciences Division,
c			United Electrodynamics, Inc. Alexandria, Va.

c		Flinn, E.A. and E.R. Engdahl (1965). A proposed basis for geo-
c			graphical and seismic regionalization, Rev. Geophys 3,
c			123 - 149.

c		Flinn, E.A., E.R. Engdahl and A.R. Hill (1974). Seismic and geo-
c			graphical regionalization, BSSA 64, 771 - 992.

	real lat, lon, lng, alat, alon

	integer gtunit			! external function in AIDSLIB

	integer begrec, bgtier, endrec, fntier, hit, idx, llindx
	integer	ln, lt, nbr, quadon, recnbr, statll, stattr, tiers

	integer*2 tieron, nbrbdy, frstrc, scndrc, pair

	dimension frstrc(RSTMS2), scndrc(RSTMS2), pair(1:2,1:RSTMS2)

	equivalence (pair(1,1), frstrc), (pair(1,RSPLS1), scndrc)

c	call gtunit to get the next available unit numbers
	llindx = gtunit()

	open (unit=llindx, access='direct', form='unformatted', 
     1	      status='old',file='llindx.fer',mode='read',share='denywr',
     1	      recl=1, iostat=statll)

	tiers = gtunit()

	open (unit=tiers, access='direct', form='unformatted',
     1	      status='old', file='lattiers.fer',mode='read',
	      share='denywr', recl=RECSIZ, iostat=stattr)

	if (statll .ne. 0) write (6, '(a/a,i8)') 
     1	    ' NAMNUM: GETNUM: err opening LLINDX.FER',' I/O err = ', 
     1	    statll
	if (stattr .ne. 0) write (6, '(a/a,i8)') 
     1	    ' NAMNUM: GETNUM: err opening LATTIERS.FER',
     1	    ' I/O err = ', stattr
	nbr = 0
	lng = lon
	if (lng .eq. -180.0) lng = 180.0
	alat = abs(lat)
	alon = abs(lng)
	if (alat .gt. 90.001  .or.  alon .gt. 180.001) return

c	GET ONSET OF QUADRANT INFO IN llindx.fer
	if (lat .lt. 0) then
c	    NOTE: Both +0.0 & -0.0 will belong to the No. Hemisphere
	    if (lng .lt. 0) then
		quadon = 274  ! quadrant onset
	    else
		quadon = 183
	    endif
	else
	    if (lng .lt. 0) then
		quadon = 92
	    else
		quadon = 1
	    endif
	endif

c	TRUNCATE ABSOLUTE VALUES OF COORDINATES
	lt = aint(alat)
	ln = aint(alon)

c	GET INDEX TO ONSET OF LATITUDE TIER INFO IN lattiers.fer
	recnbr = lt + quadon
c	tieron = tier onset     nbrbdy = number of segments in tier
	read (llindx, rec=recnbr, iostat=statll) tieron, nbrbdy
	if (statll .ne. 0) return

c	COMPUTE RECORD IN WHICH LATITUDE TIER BEGINS

	begrec = (tieron + RSMNS1)/RECSIZ

c	COMPUTE RECORD IN WHICH LATITUDE TIER ENDS

	endrec = (tieron + nbrbdy + RSMNS2)/RECSIZ

c	COMPUTE BEGINNING AND ENDING OFFSETS, IN RECORD(S) TO BE READ,
c	OF TARGET LATITUDE TIER.

	bgtier = tieron - (RECSIZ * (begrec - 1))
	fntier = bgtier + nbrbdy - 1

c	READ RECORD IN WHICH LATITUDE TIER BEGINS.
	read (tiers, rec=begrec, iostat=stattr) frstrc
	if (stattr .ne. 0) then
c	    RECORD NO. LASTRC IS THE LAST RECORD IN FILE 'lattiers.fer'
c	    AND IS A SHORT RECORD.
	    if (.not.(stattr.lt.0 .and. begrec.eq.LASTRC)) return
	endif
	if (begrec .ne. endrec) then
c	    READ RECORD IN WHICH LATITUDE TIER ENDS, IF NOT ABOVE.
	    read (tiers, rec=endrec, iostat=stattr) scndrc
	    if (stattr .ne. 0) then
		if (.not.(stattr.lt.0.and.endrec.eq.LASTRC)) return
	    endif
	endif

c	TEST LONGITUDE BOUNDARIES
	do 1 idx = bgtier, fntier
	    hit = idx
	    if (pair(1,hit) .gt. ln) then
		hit = hit - 1
		go to 2
	    endif
 1	continue

c	OUTPUT F.E. NUMBER
 2	nbr = pair(2,hit)

	close (llindx)
	close (tiers)

	return
	end
