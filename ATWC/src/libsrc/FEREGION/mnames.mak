
#                    Nmake File For MNAMES - Windows NT version

NODEBUG=1

!include <ntwin32.mak>

all:MNAMES.exe

MNAMES.obj: MNAMES.c MNAMES.mak
    $(cc) $(cflags) $(cvarsmt) MNAMES.c

MNAMES.exe: MNAMES.obj MNAMES.mak
    LINK $(conlflags) $(conlibsmt) MNAMES.obj -out:MNAMES.exe
