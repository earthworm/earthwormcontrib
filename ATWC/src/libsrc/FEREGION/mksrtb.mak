
#                    Nmake File For MKSRTB - Windows NT version

NODEBUG=1

!include <ntwin32.mak>

all:MKSRTB.exe

MKSRTB.obj: MKSRTB.c MKSRTB.mak
    $(cc) $(cflags) $(cvarsmt) MKSRTB.c

MKSRTB.exe: MKSRTB.obj MKSRTB.mak
    LINK $(conlflags) $(conlibsmt) MKSRTB.obj -out:MKSRTB.exe
