/*	program mksrtb

c	Given file SEISR_DEF.ASC as input, MKSRTB creates file SEISRG.FER
c	as a fixed length record, direct access file. Subroutine GTSEIS, given
c	a geographical region number, can then use this file to obtain
c	the seimic region number.

Converted to C 10/99 at wc&atwc by Whitmore

*/
#include <windows.h>
#include <winuser.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

void main (void)
{
FILE	*InFile, *OutFile;
int	i, j, k, b, e;

if ((InFile = fopen ("seisrdef.asc", "r")) == NULL)
    {
    printf ("seisrdef.asc file not opened\n");
    exit (0);
    }
if ((OutFile = fopen ("seisrg.fer", "w")) == NULL)
    {
    printf ("seisrg.fer file not opened\n");
    exit (0);
    }

for (i=1; i<=50; i++)
    {
    fscanf (InFile, "%ld %ld %ld", &j, &b, &e);
    for (k=b; k<=e; k++) fprintf (OutFile, "%ld\n", i);
    }
    
fclose (InFile);
fclose (OutFile);
}

