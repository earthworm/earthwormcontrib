	program chrecl

c	CHRECL (CHange RECord Length) merely rewrites file LATTIERS.FER
c       at a different record length.  LATTIERS.FER is originally writ-
c       ten by FEBNDY with a record length of 1 longword (4 bytes). How-
c       ever, subroutine GETNUM should read this unformatted, direct
c       access file at a record	length optimal to the system upon which
c       it resides.  Some systems, such as the UNIX allow an unformat-
c       ted, direct access file to be read at a different record length
c       than that at with it was written.  Others, such as the VAX/VMS
c       do not.

c	SUBROUTINE GETNUM REQUIRES A RECORD LENGTH OF AT LEAST 34 LONG-
c       WORDS (136 bytes), as it is structured so as to use no more than
c       2 consecutive reads to scan an entire longitude tier (max. len. 
c       = 35 longwords).

c	record length in 16-bit words
	integer*2 RCLLW
c	record length for open statement (as required by system)
c	(currently 128, for 512 byte records)
	integer*2 RCLOP
	integer*2 TOTREC
	integer*4 EOF
	integer*4 ios

	parameter (RCLLW = 256, RCLOP = 128, TOTREC = 5796, EOF = -1)

	integer*2 buf(RCLLW)
	open (unit=1, access='direct', form='unformatted', status='old',
     1      file='lattiers.fer', recl=1)
	open (unit=2,status='new', file='lattiers.fer', access='direct',
     1      form='unformatted', recl=RCLOP)

	j = 0
	k = 1
	do 1 i=1, TOTREC
	      j = j + 2
	      read (1, rec=i, iostat=ios) buf(j-1), buf(j)
	      if (ios .lt. 0) then
		    write(6,'(a,i5)') ' Unexpected EOF at record no. ',i
	      end if
	      if (j .eq. RCLLW) then
		    write (2, rec=k) buf
		    k = k + 1
		    j = 0
	      end if
1	continue
	j = j - 1
	write (2, rec=k) (buf(m), m = 1, j)
	close (1)
	close (2)
	end
