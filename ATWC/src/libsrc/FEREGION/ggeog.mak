
#                    Nmake File For GGEOG - Windows NT version

NODEBUG=1

!include <ntwin32.mak>

all:GGEOG.exe

GGEOG.obj: GGEOG.c GGEOG.mak
    $(cc) $(cflags) $(cvarsmt) GGEOG.c

GGEOG.exe: GGEOG.obj GGEOG.mak
    LINK $(conlflags) $(conlibsmt)  -out:GGEOG.exe GGEOG.obj \
	c:\earthworm\v4.0\lib\sema_ew.obj c:\earthworm\v4.0\lib\time_ew.obj \
	fereg.obj \seismic\utils\logit_mt.obj 

