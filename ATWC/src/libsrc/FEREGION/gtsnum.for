	subroutine gtsnum (nbr, snum, logun)

c	Returns the Seismic Region Number

c	ARGUMENTS:
c		nbr - the Flinn - Engdahl geographical region number (i3)
c		snum - Seismic Region Number (i2)
c               logun - logical unit to which files are to be connected (i2)

c	FILES ACCESSED:
c		seisrg.fer

c	The Seismic Region number ranges from 1 to 50

c	Written by G.J. Dunphy

	integer ios, nbr, logun, snum

	open (unit=logun, access='direct', form='unformatted', recl=4,
     1       mode='read', status='old', file='seisrg.fer')
	snum = 0
	if (nbr.eq.0) return
	read (logun, rec=nbr, iostat=ios) snum
	close (logun)
	if (ios.ne.0) snum = 0
	return
	end
