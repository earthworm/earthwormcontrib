	subroutine getnam (nbr, name)

c	PROCEDURE IS CALLED BY namnum

c	ARGUMENTS:
c		nbr - the Flinn - Engdahl geographical region number
c		name - the corresponding F-E geographical region name

c	FILES ACCESSED:
c		names.fer

c	A GEOGRAPHICAL REGION NAME IS NO MORE THAN 32 CHARACTERS IN LENGTH.

c	Written by G.J. Dunphy
c	modified August 29, 1991 by BPresgrave to call subroutine gtunit
c	to open the next available unit number. This is needed because many
c	programs have already opened another file as unit 1 long before this
c	subroutine is called.

	integer names, nbr, statna
	integer gtunit			! external function in aidslib

	character*32 name, blank

	data blank /' '/

	names = gtunit()	! get the next available unit number

	open (unit=names, access='direct', form='unformatted', recl=8,
     1	     mode='read',share='denywr',status='old',file='names.fer')
	name = blank
	if (nbr.eq.0) return
	read (names, rec=nbr, iostat=statna) name
	close (names)
	if (statna.ne.0) name = blank
	return
	end
