c  gtunit	- get the next available unit number

c  returns an availible unit number
c  or returns -1 if no units are available

	integer function gtunit()

	integer unum
	logical inuse

	unum = 0
	inuse = .true.

c   Number of units changed from 20 to 100 for the VAX on
c   15 April 1981 by R. Buland.
	do while (unum .lt. 100 .and. inuse)
	    unum = unum + 1
	    inquire(unum, opened = inuse)
	end do
	if (inuse) then
	    gtunit = -1
	    return
	end if
	gtunit = unum
	return
	end
