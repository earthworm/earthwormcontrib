	program newidx

c	NEWIDX is the first of 3 programs that must be executed to cre-
c       ate the files necessary for efficient retrieval of the F-E geo-
c       graphical region numbers and names.  See also programs FEBNDY
c       and MNAMES.

c	Input: file QUADSIDX.ASC
c	Output: file LLINDX.FER

c	File QUADSIDX.ASC is an ASCII file used primarily to transport
c       these geographical data to other computer systems.

c	File LLINDX.FER is used by program FEBNDY to create file
c       LATTIERS.FER. Both LLINDX.FER & LATTIERS.FER are permanent files
c       used to retrieve the geographical region number.

c	QUADSIDX.ASC consists of 52 56 byte records.  Each record con-
c       tains 7	4-character numbers, each preceded by 4 blanks and right
c       justified with blank fill to the left, i.e. the record format is
c       (7(4x,i4)).

c	There are 13 of these records for each quadrant of the globe,
c       i.e. 91	numbers per quadrant, corresponding to latitude tiers 0
c       to 90, and -0 to -90.  The quadrant order is NE, NW, SE, and SW.

c	These numbers represent the number of longitudinal boundaries
c       employed in each tier of latitude.

c	NEWIDX creates a file similar to QUADSIDX.ASC.  However it is a
c       direct access file and numbers representing pointers into file
c       LATTIERS.FER are paired with each longitudinal boundary count.

c	Programmer  -  G.J. Dunphy  -  April 1980.

	integer*2 buf(7), carry, lbcnt, i, ip, op, recnum

	data ip /9/, op /8/

	open (unit=ip, status='old', file='quadsidx.asc',
     1	      access='sequential', form='formatted')
	open (unit=op, file='llindx.fer', status='new',
     1	      access='direct', form='unformatted', recl=1)

	rewind ip

	carry = 1
	recnum = 1

	do 2 i=1, 52
		read(ip, 5, end=3) buf
5		format (7(4x,i4))
		do 1 j = 1, 7
			lbcnt = buf(j)
			write(op, rec=recnum, err=6) carry, lbcnt
			recnum = recnum + 1
			carry = carry + lbcnt
1			continue
2		continue
	goto 4
3	write (6, '(''end'')')
	go to 4
6	write (6, '(''err'')')
4	continue
	close (ip)
	close (op)
	end
