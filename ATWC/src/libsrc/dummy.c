    /******************************************************************
     *                            dummy.c                             * 
     *                                                                *
     * Contains dummy file read/write functions.                      *
     *                                                                *
     *   By:   Whitmore - May, 2001                                   *
     ******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include "earlybirdlib.h"

  /******************************************************************
   *                         GetPFile()                             *
   *                                                                *
   *  Create name of P file containing P/mag data.                  *
   *                                                                *
   *  Arguments:                                                    *
   *    pHypo       HYPO structure to get QuakeID and Version from  *
   *    pszFilePath Path to directory which contains loc files      *
   *                                                                *
   *  Returns:  P file name                                         *
   *                                                                *
   ******************************************************************/

char *GetPFile( HYPO *pHypo, char *pszFilePath )
{
   static  char    szFileName[128];/* Created File name */
   char    szTemp[32];
   
   strcpy ( szFileName, pszFilePath );
   strcat( szFileName, "loc" );
   itoaX( pHypo->iQuakeID, szTemp );      /* P-time file name */
   PadZeroes( 4, szTemp );
   strcat( szFileName, szTemp ); 
   strcat( szFileName, "." );      
   itoaX( pHypo->iVersion, szTemp );
   PadZeroes( 3, szTemp );            
   strcat( szFileName, szTemp ); 
   return( szFileName );
}

      /******************************************************************
       *                       ReadDummyData()                          *
       *                                                                *
       * This function reads hypocenter parameters data from the        *
       * specified dummy file.                                          *
       *                                                                *
       * October, 2003: Read in lat lon as +/- geographic coord.        *
       *                                                                *
       * February, 2002: Added iNumMwp to read/write                    *
       *                                                                *
       * October, 2002: Added num magnitudes for each type              *
       *                                                                *
       *  Arguments:                                                    *
       *   pHypo            Computed hypocentral parameters             *
       *   pszDumFile       Parameter file (dummy file) to update       *
       *                                                                *
       *  Returns: 1->ok read, 0->no read                               *
       *                                                                *
       ******************************************************************/
	   
int ReadDummyData( HYPO *pHypo, char *pszDumFile )
{
   double  dAzm;          /* Azimuthal coverage in degrees */
   double  dDum;    
   FILE    *hFile;        /* File handle */
   int     iCnt;          /* Dummy file open counter */
   int     iDepth;        /* Quake depth from dummy file */
   int     iDum;
   int     iMonth, iYear; /* Origin dates */ 
   LATLON  ll;            /* Epicentral geographic location */
   char    szLat[16], szLon[16];  /* Location from dummy file */
   struct tm tm;          /* Origin time in structure */
    
/* Initialize the hypocenter structure */
   InitHypo( pHypo );		
	
/* Read Dummy File */
   iCnt = 0; 
   Open2:
   if ( (hFile = fopen( pszDumFile, "r" )) != NULL )
      fscanf( hFile, "%ld %lf %lf %ld %lf %s %d %d %d %d %d %d "
                     "%ld %ld %lf %ld %lf %ld %lf %lf %ld %lf %ld %lf %ld %ld "
                     "%lf %lf %lf %ld %ld",
       &iDum, &ll.dLat, &ll.dLon, &iDepth, &pHypo->dPreferredMag,
       &pHypo->szPMagType, &tm.tm_mday, &iMonth, &iYear,
       &tm.tm_hour, &tm.tm_min, &tm.tm_sec, &iDum, &pHypo->iNumPMags,
       &pHypo->dMSAvg, &pHypo->iNumMS, &pHypo->dMwpAvg, &pHypo->iNumMwp,
       &dDum, &pHypo->dMbAvg, &pHypo->iNumMb, &pHypo->dMlAvg, &pHypo->iNumMl,
       &pHypo->dMwAvg, &pHypo->iNumMw, &pHypo->iNumPs, &pHypo->dNearestDist,
       &pHypo->dAvgRes, &dAzm, &iDum, &pHypo->iUpdateMap );
   else                         /* Try again in .1 sec if busy */
   {
      if ( iCnt == 5 )          /* Quit trying */
      {
         logit( "et" , "Dummy file not opened 5 times in ReadDummyData\n" );
         return 0;
      }
      iCnt++;
      sleep_ew( 100 );
      goto Open2;
   }
   fclose( hFile );
   
/* Put lat/lon in geocentric form */
//   ll.dLat = LLConv( szLat );
//   ll.dLon = LLConv( szLon );
   GeoCent( &ll );
   pHypo->dLat = ll.dLat;
   pHypo->dLon = ll.dLon;   
   GetLatLonTrig( (LATLON *) pHypo );
   
/* Convert origin time to 1/1/70 seconds */   
   tm.tm_mon = iMonth - 1;
   tm.tm_year = iYear - 1900;
   tm.tm_isdst = 0;
   pHypo->dOriginTime = (double) mktime( &tm );
   
/* Convert a few other items */   
   pHypo->iAzm = (int) dAzm;
   pHypo->dDepth = (double) iDepth;
   if (pHypo->dDepth > 750.) pHypo->dDepth = 750.;
   if (pHypo->dDepth < 0.)   pHypo->dDepth = 0.;
   return 1;
}

      /******************************************************************
       *                     ReadEBPTimeFile()                          *
       *                                                                *
       * This function reads P-time, distance, and azimuth from a file  *
       * created by LOCATE after a location is made.                    *
       *                                                                *
       *  Arguments:                                                    *
       *   piNumPAuto       Number of P-times read                      *
       *   pPBuf            P-pick buffer                               *
       *   pszPTimeFile     Parameter file from LOCATE                  *
       *   iPMax            Maximum number of P-times to read           *
       *                                                                *
       *  Returns: 1->ok read, 0->no read                               *
       *                                                                *
       ******************************************************************/
	   
int ReadEBPTimeFile( int *piNumPAuto, PPICK *pPBuf, char *pszPTimeFile,
                     int iPMax )
{
   FILE    *hFile;          /* File handle */
   int     iDum;    

   if ((hFile = fopen (pszPTimeFile, "r")) != NULL)
   {
      *piNumPAuto = 0;
      while (!feof (hFile)) /* Read till end of file */
      {
         fscanf (hFile, "%s %ld %s %s %lf %lf %lf %lf\n",
                 &pPBuf[*piNumPAuto].szStation, &iDum,
                 &pPBuf[*piNumPAuto].szChannel, &pPBuf[*piNumPAuto].szNetID,
                 &pPBuf[*piNumPAuto].dPTime, &pPBuf[*piNumPAuto].dDelta,
                 &pPBuf[*piNumPAuto].dAz, &pPBuf[*piNumPAuto].dRes);
         *piNumPAuto += 1;
         if ( *piNumPAuto >= iPMax ) break;
      }
      fclose (hFile);
      return 1;
   }
   else                     /* Couldn't open file */
   {
      logit ("t", "Failed to open %s in ReadEBPTimeFile\n", pszPTimeFile);	  
      return 0;
   }
}

      /******************************************************************
       *                          ReadPickFile2()                       *
       *                                                                *
       * This function reads the P and magnitude data written to a file *
       * in loc_wcatwc.                                                 *
       *                                                                *
       *  Arguments:                                                    *
       *   piPNum           Number of Ps in the file                    *
       *   P                One of the P Buffer structures              *
       *   pszFile          Input data file name                        *
       *   iPMax            Maximum number of P data to read            *
       *                                                                *
       ******************************************************************/
	   
void ReadPickFile2( int *piNumP, PPICK P[], char *pszFile, int iPMax )
{
   FILE    *hFile;                   /* File handle */
   int     iRC;                      /* fscanf return (# fields read) */
   
   *piNumP = 0;

/* Open pick file */        
   if ( (hFile = fopen( pszFile, "r" )) == NULL )
   {
      logit ("t", "Pick file, %s, not opened.\n", pszFile);
      return;
   }

/* Read the picks and magnitude data from disk */
   for ( ;; )  
   {
      iRC = fscanf( hFile, "%s %s %s %ld %lf %s %lf %lf %lf %ld %c %lf %lf %lf "
                           "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
       P[*piNumP].szStation, P[*piNumP].szChannel, P[*piNumP].szNetID,
       &P[*piNumP].lPickIndex, &P[*piNumP].dPTime, P[*piNumP].szPhase,
       &P[*piNumP].dRes, &P[*piNumP].dDelta, &P[*piNumP].dAz,
       &P[*piNumP].iUseMe, &P[*piNumP].cFirstMotion,                 
       &P[*piNumP].dMbAmpGM, &P[*piNumP].dMbPer,
       &P[*piNumP].dMbMag, &P[*piNumP].dMbTime, 
       &P[*piNumP].dMlAmpGM, &P[*piNumP].dMlPer,
       &P[*piNumP].dMlMag, &P[*piNumP].dMlTime, 
       &P[*piNumP].dMSAmpGM, &P[*piNumP].dMSPer,
       &P[*piNumP].dMSMag, &P[*piNumP].dMSTime, 
       &P[*piNumP].dMwpIntDisp, &P[*piNumP].dMwpTime, &P[*piNumP].dMwpMag );
      if ( iRC == 26 ) *piNumP += 1;
      else
      {
         logit( "", "iRC=%ld in readpickfile2, iNumP=%ld\n", iRC, *piNumP );
         break;
      }
      if ( *piNumP >= iPMax ) break;
   }
   fclose( hFile );
}

      /******************************************************************
       *                       WriteDummyData()                         *
       *                                                                *
       * This function writes hypocenter parameters data to the         *
       * specified dummy file.  It also updates the file used by the    *
       * GIS (pszMapFile).                                              *
       *                                                                *
       * October, 2003: Write lat lon as +/- geographic coord.          *
       *                                                                *
       * February, 2002: Added iNumMwp to read/write                    *
       *                                                                *
       * October, 2002: Added num magnitudes for each type              *
       *                                                                *
       *  Arguments:                                                    *
       *   pHypo            Computed hypocentral parameters             *
       *   pszMapFile       Map file to update                          *
       *   pszDumFile       Parameter file (dummy file) to update       *
       *   iLoc             Loc=1 when full location was made           *
       *   iUpdate          iUpdate=1 => force a new map creation in EV *
       *                                                                *
       *  Returns: 1->ok write, 0->no write                             *
       *                                                                *
       ******************************************************************/
	   
int WriteDummyData( HYPO *pHypo, char *pszMapFile, char *pszDumFile, int iLoc,
                    int iUpdate )
{
   double  dAvgRes;     /* Average residual */
   double  dMwpAvg;     /* Mwp avg. to write to dummy file (must have >= 3) */
   double  dDum;
   FILE    *hFile;      /* File handle */
   static  int     iBullNo;     /* Bulletin number from dummy file */
   static  int     iQuakeID;    /* Quake ID from dummy file */
   int     iCnt;        /* Dummy file open counter */
   int     iDum;
   char    cNS, cEW;    /* N, S, E, W indicator */
   LATLON  ll;          /* Epicentral geographic location */
   long    lTime;       /* 1/1/70 time */
   char    szLat[16], szLon[16]; /* Epicentral lat/lon in string form */
   char    szDum[8];
   struct tm *tm;       /* Origin time in structure */

/* Get epicenter lat/lon in geographic coordinates */
   GeoGraphic( &ll, (LATLON *) pHypo );
   if ( ll.dLon > 180. ) ll.dLon -= 360.;
   
/* Update map file if location appears good */
   if ( pHypo->iGoodSoln >= 2 && iLoc == 1 )
      if ( (hFile = fopen( pszMapFile, "w" )) != NULL )
      {
         fprintf( hFile, "%lf %lf %lf", ll.dLat, ll.dLon, pHypo->dPreferredMag);
         fclose( hFile );
      }
      else                           /* File could not be opened */
         logit( "t", "Failed to open %s in WriteDummy\n", pszMapFile );
   
/* Read in bulletin number and quake ID from existing dummy file */   
   iCnt = 0; 
   Open1:
   hFile = fopen( pszDumFile, "r" );     
   if ( hFile != NULL )           /* Read bulletin number */
      fscanf( hFile, "%ld %lf %lf %ld %lf %s %d %d %d %d %d %d "
                     "%ld %ld %lf %ld %lf %ld %lf %lf %ld %lf %ld %lf %ld %ld "
                     "%lf %lf %lf %ld %ld",
       &iBullNo, &dDum, &dDum, &iDum, &dDum, &szDum, &iDum, &iDum, &iDum,
       &iDum, &iDum, &iDum, &iDum, &iDum, &dDum, &iDum, &dDum, &iDum,
       &dDum, &dDum, &iDum, &dDum, &iDum, &dDum, &iDum, &iDum, &dDum,
       &dDum, &dDum, &iQuakeID, &iDum );
   else                           /* Try again in .1 sec if busy */
   {
      if ( iCnt == 5 )            /* Quit trying */
      {
         logit ("t" , "Dummy file not opened 5 times in read\n");
         return 0;
      }
      iCnt++;
      sleep_ew( 100 );
      goto Open1;
   }
   fclose( hFile );
   
/* Only add Mwp to dummy file if more than 2 station average */
   dMwpAvg = 0.;
   if ( pHypo->iNumMwp >= 3 ) dMwpAvg = pHypo->dMwpAvg;
   else                       pHypo->iNumMwp = 0;

/* Compute average residual */
   dAvgRes = pHypo->dAvgRes;
   if ( dAvgRes > 99.99 ) dAvgRes = 99.99;

/* Get time rounded to nearest second */
   lTime = (long) (pHypo->dOriginTime+0.5);
   tm = TWCgmtime( lTime );
    
/* Update Dummy File */
   iCnt = 0; 
   Open2:
   if ( (hFile = fopen( pszDumFile, "w" )) != NULL )
      fprintf( hFile, "%ld %7.3lf %8.3lf %ld %3.1lf %s %d %d %d %d %d %d %ld %ld "
       "%3.1lf %ld %3.1lf %ld %3.1lf %3.1lf %ld %3.1lf %ld %3.1lf %ld %ld "
       "%7.3lf %5.2lf %5.1lf %ld %ld\n",
       iBullNo, ll.dLat, ll.dLon, (int) (pHypo->dDepth + 0.5),
       pHypo->dPreferredMag, pHypo->szPMagType, tm->tm_mday, tm->tm_mon+1,
       tm->tm_year+1900, tm->tm_hour, tm->tm_min, tm->tm_sec, 0,
       pHypo->iNumPMags, pHypo->dMSAvg, pHypo->iNumMS, pHypo->dMwpAvg,
       pHypo->iNumMwp, 0., pHypo->dMbAvg, pHypo->iNumMb, pHypo->dMlAvg,
       pHypo->iNumMl, pHypo->dMwAvg, pHypo->iNumMw, pHypo->iNumPs,
       pHypo->dNearestDist, dAvgRes, (double) pHypo->iAzm, iQuakeID, iUpdate );
   else                         /* Try again in .1 sec if busy */
   {
      if ( iCnt == 5 )          /* Quit trying */
      {
         logit( "et" , "Dummy file not opened 5 times in WriteDummyData\n" );
         return 0;
      }
      iCnt++;
      sleep_ew( 100 );
      goto Open2;
   }
   fclose( hFile );
   return 1;
}

