         
 /************************************************************************
  * LOGQUAKE.C                                                           *
  *                                                                      *
  * These functions write quake locations and magnitudes to a log file.  *
  * Detailed data is logged with all P-picks, magnitudes, etc.           *
  *                                                                      *
  * Made into earthworm module 2/2001.                                   *
  *                                                                      *
  ************************************************************************/
  
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include "earlybirdlib.h"
                  
      /******************************************************************
       *                        MagnitudeLog()                          *
       *                                                                *
       * This function provides magnitude logging.  Magnitudes must have*
       * already been calculated.                                       *
       *                                                                *
       *  Arguments:                                                    *
       *   iPNum          Number of P-data                              *
       *   P              Array of P-data structures                    *
       *   pHypo          Hypocenter parameters                         *
       *                                                                *
       ******************************************************************/
	   
void MagnitudeLog( int iPNum, PPICK P[], HYPO *pHypo )
{
   int     i;

/* If there is no solution, print nothing and return */
   if ( pHypo->dLat == 0. && pHypo->dLon == 0. ) return;

/* Is there any magnitude information to output? */
   if ( !pHypo->iNumMb && !pHypo->iNumMl && !pHypo->iNumMS && !pHypo->iNumMwp )
   {
      logit("", " No Magnitude Information for this event\n");
      logit("", "\n"
       "----------------------------------------------------------------------------\n");
   }
   else                  /* There are some magnitudes to print */
   {
      logit( "",
       "                             MAGNITUDE DATA\n"
       "              ----------------------------------------------\n" );
      logit( "", 
       "               STA  PER     AMP     DIST   Ml   Mb   MS  Mwp\n" );
      logit( "", "              ----  ---  ---------  ----  ---  ---  ---  ---\n" );
      for ( i=0; i<iPNum; i++ )
         if ( (P[i].dMbMag || P[i].dMlMag || P[i].dMSMag || P[i].dMwpMag) &&
	       P[i].iUseMe > 0 )
         {                      /* This station has a magnitude */
            logit( "", "              %4s", P[i].szStation );/* Station Name */
            if ( P[i].dMSMag )                              /* Period */
               logit( "", "%5.1f", (double) P[i].lMSPer );
            else if (P[i].dMlMag )
               logit( "", "%5.1f", (double) P[i].lMlPer/10. );
            else if (P[i].dMbMag )
               logit( "", "%5.1f", (double) P[i].lMbPer/10. );
            else if (P[i].dMwpMag )
	       logit( "", "     " );
            if ( P[i].dMSMag )                              /* Amplitude */
               logit( "", "%9.1f", P[i].dMSAmpGM );
            else if ( P[i].dMlMag )
               logit( "", "%9.1f", P[i].dMlAmpGM );
            else if ( P[i].dMbMag )
               logit( "", "%9.1f", P[i].dMbAmpGM );
            else if ( P[i].dMwpMag )
               logit( "", "         " );
            logit( "", "%8.1f", P[i].dDelta );              /* Distance */
            if ( P[i].dMlMag )                              /* Magnitude */
            {
               logit( "", "%5.1f", P[i].dMlMag );
               if ( P[i].iMlClip ) logit( "", "C      " );
               else                logit( "", "       " );
            }
            else if ( P[i].dMbMag )
            {
               logit( "", "     %5.1f", P[i].dMbMag );
               if ( P[i].iMbClip ) logit( "", "C " );
               else                logit( "", "  " );
            }
            else
               logit( "", "            " );
            if ( P[i].dMSMag )
            {
               logit( "", "%3.1f", P[i].dMSMag );
               if ( P[i].iMSClip ) logit( "", "C " );
               else                logit( "", "  " );
            }
            else
               logit( "", "     " );
            if ( P[i].dMwpMag )
               logit( "", "%3.1f  ", P[i].dMwpMag );
            else
               logit( "", "     " );
            logit( "", "\n" );
         }
	 
/* Output the modified averages (not including those 0.6 away from norm) */
      if ( pHypo->iNumMS || pHypo->iNumMl || pHypo->iNumMb || pHypo->iNumMw ||
           pHypo->iNumMwp )
      {
         logit( "",
                 "              ---------------------------------------------------\n"
                 "                               MODIFIED AVERAGES\n" );
         if ( pHypo->iNumMS )
            logit( "", "                         MS =%4.1f;  %d STATION AVERAGE\n",
                   pHypo->dMSAvg, pHypo->iNumMS );
         if ( pHypo->iNumMwp >= 3 )
            logit( "", "                         Mwp=%4.1f;  %d STATION AVERAGE\n",
                   pHypo->dMwpAvg, pHypo->iNumMwp );
         else if ( pHypo->iNumMwp >= 1 )
            logit( "", "                         Mwp=%4.1f?; %d STATION AVERAGE\n",
                   pHypo->dMwpAvg, pHypo->iNumMwp );
         if ( pHypo->iNumMb )
            logit( "", "                         Mb =%4.1f;  %d STATION AVERAGE\n",
                   pHypo->dMbAvg, pHypo->iNumMb );
         if ( pHypo->iNumMl )
            logit( "", "                         Ml =%4.1f;  %d STATION AVERAGE\n",
                   pHypo->dMlAvg, pHypo->iNumMl );
         if ( pHypo->iNumMw )
            logit( "", "                         Mw =%4.1f;  %d STATION AVERAGE\n",
                   pHypo->dMwAvg, pHypo->iNumMw );
      }
		 
/* End of output */
      logit( "", 
             "------------------------------------------------------------------------------\n\n" );
   }
}

      /******************************************************************
       *                          QuakeLog()                            *
       *                                                                *
       * This function writes the hypocenter parameters to a log file.  *
       *                                                                *
       *  Arguments:                                                    *
       *   iPNum          Number of P-data                              *
       *   P              Array of P-data structures                    *
       *   pHypo          Hypocenter parameters                         *
       *   pcity          Array of reference cities                     *
       *   pcityEC        Array of eastern reference cities             *
       *                                                                *
       ******************************************************************/
       
void QuakeLog( int iPNum, PPICK P[], HYPO *pHypo, CITY *pcity, CITY *pcityEC )
{
   char    cEW, cNS;            /* N, S, E, W character for lat/lon */
   CITYDIS CityDis;             /* Distance, direction from nearest cities */
   char    cTemp;               /* X if necessary for iUseMe */
   int     i;
   int     iDepth;              /* Epicentral depth in km */
   int     iFERegion;           /* Flinn-Engdahl Region number */
   int     iNumUsed;            /* Number of stations used in location */
   int     iRegion;             /* Procedural region number */
   int     iTenths;             /* Tenths of seconds in P-time */
   LATLON  ll;                  /* Geographic epicenter coordinates */
   long    lTime;               /* 1/1/70 time in seconds */
   char    *psz;                /* Epicentral region */
   char    szHour[3], szMin[3], szSec[3];  /* P-time in string form */
   char    szTenth[3], szTenths[3];        /* Character string of tenths */
   struct tm *tm;               /* C Time structure */

/* If there is no solution, print nothing and return */
   if ( pHypo->dLat == 0. && pHypo->dLon == 0. ) return;   

/* Convert epicenter coords from geocentric to geographic */
   GeoGraphic( &ll, (LATLON *) pHypo );
   
/* Put lat/lon in form for output */
   cEW = 'E';
   cNS = 'N';
   if ( ll.dLon > 180.0 )
   {
      while ( ll.dLon > 180.0 ) ll.dLon -= 360.0;
      ll.dLon = fabs( ll.dLon );
      cEW = 'W';
   }
   if ( ll.dLat < 0.0 )
   {
      ll.dLat = fabs( ll.dLat );
      cNS = 'S';
   }
   
/* Convert depth to integer */
   iDepth = (int) (pHypo->dDepth + 0.5);

/* Get current date/time and write on top of output header */
   time( &lTime );
   logit( "", "\n%ld         Quake %5ld-%ld - Solution at: %s", pHypo->iGoodSoln,
          pHypo->iQuakeID, pHypo->iVersion, asctime( TWCgmtime( lTime ) ) );
   logit( "", 
 "------------------------------------------------------------------------------\n"
 "         STA  P-TIME       RES   DIS   AZM    Ml    Mb    MS   Mwp\n"
 "         ---  ------       ---   ---   ---   ---   ---   ---   ---\n");

/* Print out P-times, deltas, etc. */
   iNumUsed = 0;
   for ( i=0; i<iPNum; i++ )   /* Only print those with P-times */
      if ( P[i].dPTime > 0. && P[i].iUseMe > 0 ) iNumUsed++;
   for ( i=0; i<iPNum; i++ )
   {
      lTime = (long) (P[i].dPTime);
      tm = TWCgmtime( lTime );
      itoaX( tm->tm_hour, szHour );
      itoaX( tm->tm_min, szMin );
      itoaX( tm->tm_sec, szSec );
      PadZeroes( 2, szHour );
      PadZeroes( 2, szMin );
      PadZeroes( 2, szSec );
      iTenths = (int) (((P[i].dPTime-floor( P[i].dPTime ))*10.)+0.5);
      itoaX( iTenths, szTenth );
      strncpy( &szTenths[0], szTenth, 1 );
      strcpy( &szTenths[1], "\0" );
      cTemp = ' ';
      if ( P[i].iUseMe <= 0 ) cTemp = 'X';
      logit( "", "        %4s %2s%2s%2s.%1s%c %7.1f%6.1f%7.1f",
             P[i].szStation, szHour, szMin, szSec, szTenths, cTemp,
             P[i].dRes, P[i].dDelta, P[i].dAz );
      if (P[i].dMlMag)
      {
          logit( "", "%5.1f", P[i].dMlMag);
          if (P[i].iMlClip) logit( "", "C        ");
            else logit( "", "         ");
      }
      else if (P[i].dMbMag)
      {
          logit( "", "      %5.1f", P[i].dMbMag);
          if (P[i].iMbClip) logit( "", "C  ");
            else logit( "", "   ");
      }
      else
          logit( "", "              ");
      if (P[i].dMSMag)
      {
          logit( "", "%3.1f", P[i].dMSMag);
          if (P[i].iMSClip) logit( "", "C  ");
            else logit( "", "   ");
      }
      else
          logit( "", "      ");
      if (P[i].dMwpMag)
          logit( "", "%3.1f   ", P[i].dMwpMag);
      else
          logit( "", "      ");
      logit( "", "\n");
   }
   
/* Next print hypocenter parameters summary */
   logit( "", "------------------------------------------------------------------------------\n" );
   lTime = (long) (pHypo->dOriginTime+0.5);
   logit( "", "LOC...=%5.1f%c%8.1f%c   DEP=%3d",
          ll.dLat, cNS, ll.dLon, cEW, iDepth );
   logit( "", "               H(Z)  =%s", asctime( TWCgmtime( lTime ) ) );
		  
/* Compute the average residual in the solution */
   logit( "", "                                           " );
	 
/* Convert the O-time to local time and output */
   if ( IsDayLightSavings2( pHypo->dOriginTime, -9 ) ) /* In AK */
   {                                                   /* Daylight time */               
      lTime = (long) (pHypo->dOriginTime-28800.+0.5);
      tm = TWCgmtime( lTime );                           /* 8 hours UTC diff. */
      logit( "", "    H(ADT)=%s", asctime( tm ) );
   }
   else	                                               /* Standard time */
   {
      lTime = (long) (pHypo->dOriginTime-32400.+0.5);
      tm = TWCgmtime( lTime );                           /* 9 hours UTC diff. */
      logit( "", "    H(AST)=%s", asctime( tm ) );
   }
   
   if ( pHypo->iGoodSoln > 0 )
   {
/* Find the nearest major and minor city to the epicenter */
      GeoGraphic( &ll, (LATLON *) pHypo );
      if ( ll.dLon < 0 ) ll.dLon += 360.;
      iRegion = GetRegion( ll.dLat, ll.dLon );
      if ( iRegion >= 10 )                  /* WC&ATWC Eastern AOR */
         NearestCitiesEC( (LATLON *) pHypo, pcityEC, &CityDis ); 
      else
         NearestCities( (LATLON *) pHypo, pcity, &CityDis );
	  
      if ( CityDis.iDis[1] < 320 || CityDis.iDis[0] < 320 )
/* Then, print nearest cities and azimuthal coverage */
      {
         logit( "", "LOC...= %3d miles %2s of %s",
                CityDis.iDis[0], CityDis.pszDir[0], CityDis.pszLoc[0] );
         for ( i=strlen( CityDis.pszLoc[0] ); i<35; i++ ) logit( "", " " );
         logit( "", "       AZM =    %3d\n", pHypo->iAzm );
      }
      else             /* Otherwise, use General Area of... */
      {
         GeoGraphic( &ll, (LATLON *) pHypo );
         if ( ll.dLon > 180.0 && ll.dLon < 360.0 ) ll.dLon -= 360.0;
         psz = namnum( ll.dLat, ll.dLon, &iFERegion );
         psz[strlen (psz)-1] = '\0';                      
         logit( "", "LOC...= General Area of %s", psz );
         for ( i = strlen (psz); i<35; i++ ) logit( "", " ");
         logit( "", "       AZM =    %3d\n", pHypo->iAzm );
         logit( "", "                                           " );
         logit( "", "                       RES = %6.1f\n", pHypo->dAvgRes );
      }
      
/* List distance from major city, also */
      if ( CityDis.iDis[1] < 320 || CityDis.iDis[0] < 320 )  /* if near */
         if ( strncmp( CityDis.pszLoc[1], CityDis.pszLoc[0], 27 ) )
         {
            logit( "", "LOC...= %3d miles %2s of %s",
                     CityDis.iDis[1], CityDis.pszDir[1], CityDis.pszLoc[1] );
            for ( i=strlen( CityDis.pszLoc[1] ); i<35; i++ ) logit( "", " " );
            logit( "", "       RES = %6.1f\n", pHypo->dAvgRes);
         }
   }
   
/* Print out final line */
   logit( "",
"------------------------------------------------------------------------------"
"\n" );
	 
/* Output the modified averages (not including those 0.6 away from norm) */
   if ( pHypo->iNumMS || pHypo->iNumMl || pHypo->iNumMb || pHypo->iNumMw || 
        pHypo->iNumMwp )
   {
      logit( "",
              "                                MODIFIED AVERAGES\n" );
      if ( pHypo->iNumMS )
         logit( "", "                          MS =%4.1f;  %d STATION AVERAGE\n",
                pHypo->dMSAvg, pHypo->iNumMS );
      if ( pHypo->iNumMwp >= 3 )
         logit( "", "                          Mwp=%4.1f;  %d STATION AVERAGE\n",
                pHypo->dMwpAvg, pHypo->iNumMwp );
      else if ( pHypo->iNumMwp >= 1 )
         logit( "", "                          Mwp=%4.1f?; %d STATION AVERAGE\n",
                pHypo->dMwpAvg, pHypo->iNumMwp );	  
      if ( pHypo->iNumMb )
         logit( "", "                          Mb =%4.1f;  %d STATION AVERAGE\n",
                pHypo->dMbAvg, pHypo->iNumMb );
      if ( pHypo->iNumMl )
         logit( "", "                          Ml =%4.1f;  %d STATION AVERAGE\n",
                pHypo->dMlAvg, pHypo->iNumMl );
      if ( pHypo->iNumMw )
         logit( "", "                          Mw =%4.1f;  %d STATION AVERAGE\n",
                pHypo->dMwAvg, pHypo->iNumMw );
   }
		 
/* End of output */
   logit( "", 
"------------------------------------------------------------------------------"
"\n\n" );
}

      /******************************************************************
       *                          QuakeLog2()                           *
       *                                                                *
       * This function writes the hypocenter parameters to a log file.  *
       * This file is appended and is an ongoing log of automatic       *
       * locations.                                                     *
       *                                                                *
       *  Arguments:                                                    *
       *   pszFile        File to add quake data                        *
       *   pHypo          Hypocenter parameters                         *
       *                                                                *
       ******************************************************************/
       
void QuakeLog2( char *pszFile, HYPO *pHypo )
{
   FILE    *hFile;              /* File handle */
   int     iDepth;              /* Epicentral depth in km */
   LATLON  ll;                  /* Geographic epicenter coordinates */
   long    lTime;               /* 1/1/70 time in seconds */

/* If there is no solution, print nothing and return */
   if ( pHypo->dLat == 0. && pHypo->dLon == 0. ) return;   

/* Convert epicenter coords from geocentric to geographic */
   GeoGraphic( &ll, (LATLON *) pHypo );
   if (ll.dLon > 180.) ll.dLon -= 360.;
   
/* Convert depth to integer */
   iDepth = (int) (pHypo->dDepth + 0.5);
   
/* Open log file for append */   
   hFile = fopen( pszFile, "a" );
   if ( hFile == NULL )
   {
      logit( "t", "%s log file not opened\n", pszFile );
      return;
   }
   
/* Get current date/time and write first */
   time( &lTime );
   fprintf( hFile, "Solution at: %s", asctime( TWCgmtime( lTime ) ) );
   
/* Print hypocenter parameters summary */
   lTime = (long) (pHypo->dOriginTime+0.5);
   fprintf( hFile, "%ld %ld %.24s %lf %lf %ld %ld %lf %ld\n", pHypo->iQuakeID,
          pHypo->iVersion, asctime( TWCgmtime( lTime ) ), ll.dLat, ll.dLon,
          iDepth, pHypo->iNumPs, pHypo->dAvgRes, pHypo->iAzm );
   fprintf( hFile, "      Mw=%lf-%ld, Mwp=%lf-%ld, MS=%lf-%ld, Mb=%lf-%ld, Ml=%lf-%ld\n",
          pHypo->dMwAvg, pHypo->iNumMw, pHypo->dMwpAvg, pHypo->iNumMwp,
		  pHypo->dMSAvg, pHypo->iNumMS, pHypo->dMbAvg, pHypo->iNumMb,
		  pHypo->dMlAvg, pHypo->iNumMl );
   fclose( hFile );
}
