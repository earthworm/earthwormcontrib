 /************************************************************************
  * GEOTOOLS.C                                                           *
  *                                                                      *
  * This is a group of functions which provide tools for converting      *
  * geocentric lat/lons to geographic and vice-versa and for performing  *
  * distance/azimuth computations on a sphere (the earth).               *
  *                                                                      *
  * Made into earthworm module 2/2001.                                   *
  *                                                                      *
  ************************************************************************/
  
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include "earlybirdlib.h"

      /******************************************************************
       *                         ConvertLoc()                           *
       *                                                                *
       *  This function converts a lat/lon pair expressed in +/- form to*
       *  an alphabetic output such as 45.2N 124.2W.                    *
       *                                                                *
       *  Arguments:                                                    *
       *   pll1             Input lat/lon pair                          *
       *   pll2             Converted lat/lon pair                      *
       *   pcLat            S or N for south or north                   *
       *   pcLon            E or W for east or west                     *
       *                                                                *
       ******************************************************************/
       
void ConvertLoc( LATLON *pll1, LATLON *pll2, char *pcLat, char *pcLon )
{
   *pcLat = 'N';
   *pcLon = 'E';
   if ( pll1->dLon > 180. ) pll1->dLon -= 360.;
   if ( pll1->dLat < 0. ) *pcLat = 'S';
   if ( pll1->dLon < 0. ) *pcLon = 'W';
   pll2->dLat = fabs( pll1->dLat );
   pll2->dLon = fabs( pll1->dLon );
}

      /******************************************************************
       *                         DepthCheck()                           *
       *                                                                *
       *  Given an input lat/lon (+/- geographic), a maximum expected   *
       *  depth in km is returned.                                      *
       *                                                                *
       *  Arguments:                                                    *
       *   lat              geographic lat (+/-)                        *
       *   lon              geographic lon (+/-)                        *
       *                                                                *
       *  Return:           Maximum expected depth in that region (km)  *                                                               *
       *                                                                *
       ******************************************************************/

double DepthCheck( double dLat, double dLon, char *pszDepthFile )
{
   double dAbsLon;                           /* East Longitude */
   double dArea;                             /* Square area */
   double dLatLL[MAXREGS], dLatUR[MAXREGS];  /* Latitude from file */
   double dLonLL[MAXREGS], dLonUR[MAXREGS];  /* Longitude from file */
   double dDep[MAXREGS];                     /* Depth from file */
   double dDepth=50.;                        /* Depth to return (50km = default) */
   double dMinArea = 1.E+10;                 /* Take box with smallest area */
   int    i=0, j;                            /* Counters*/
   FILE   *hFile;                            /* Input depth file handle */

/* Read in data */
   if ( (hFile = fopen( pszDepthFile, "r") ) == NULL )
   {
      logit( "", "DepthCheck: Input file (%s) not found\n", pszDepthFile );
      return -1.;
   }
   else
      while ( !feof( hFile ) && i < MAXREGS )
      {
         fscanf( hFile, "%lf %lf %lf %lf %lf", &dLatLL[i], &dLonLL[i],
                 &dLatUR[i], &dLonUR[i], &dDep[i] );
         i++;
      }
   i--;
   fclose( hFile );
   if ( i > (MAXREGS-2) ) logit( "", "DepthCheck: check array size\n" );

/* Convert longitudes to 'absolute' east longitude */
   for ( j=0; j<i; j++ )
   {
      if ( dLonLL[j] < 0.0 ) dLonLL[j] = dLonLL[j] + 360.;
      if ( dLonUR[j] < 0.0 ) dLonUR[j] = dLonUR[j] + 360.;
   }

/* Test for ur / ll errors. i.e. if ur lat smaller than ll lat */
   for ( j=0; j<i; j++ )
   {
      if ( dLatLL[j] >= dLatUR[j] )
      {
         logit( "", "DepthCheck: latitude error in box number %d \n", j+1);
         return -1.;
      }
      if ( dLonLL[j] >= dLonUR[j] )
      {
         logit( "", "DepthCheck: longitude error in box number %d \n", j+1);
         return -1.;
      }
   }

/* Convert input longitude into absolute east longitude */
   if ( dLon < 0. ) dAbsLon = 360. + dLon;
   else             dAbsLon = dLon;

/* Does the test point fall within a depth box? */
   for ( j=0; j<i; j++ )
      if ( dLat >= dLatLL[j]    && dLat < dLatUR[j] &&
           dAbsLon >= dLonLL[j] && dAbsLon < dLonUR[j] )
      {
         dArea = (dLatUR[j]-dLatLL[j]) * (dLonUR[j]-dLonLL[j]);
         if ( dArea < dMinArea )
         {
            dMinArea = dArea;
            dDepth = dDep[j];
         }
      }
   return dDepth;
}

      /******************************************************************
       *                            GeoCent()                           *
       *                                                                *
       *  Converts to geocentric latitude and longitude . Latitude runs *
       *  from 0 degrees at North to 180 degrees.  Longitude runs 0 to  *
       *  360 degrees. West longitudes are negative. Corrected for earth*
       *  shape.  Returned values are in radians.                       *
       *                                                                *
       *  Arguments:                                                    *
       *   pll              On Input: geographic lat/lon                *
       *                    On Output: geocentric lat/lon               *
       *                                                                *
       ******************************************************************/
	   
void GeoCent( LATLON *pll )
{
   double  dTemp;

   while ( pll->dLat < -90.0 ) pll->dLat += 90.0;
   while ( pll->dLat > 90.0 )  pll->dLat -= 90.0;
   if ( pll->dLat < 0. )
   {
      dTemp = 1.;
      pll->dLat = fabs( pll->dLat );
   }
   else dTemp = -1.;
   pll->dLat *= RAD;
   pll->dLat = atan( 0.993277 * tan( pll->dLat ) );
   pll->dLat = PI/2. + pll->dLat*dTemp;

   while ( pll->dLon < 0.0 )    pll->dLon += 360.0;
   while ( pll->dLon >= 360.0 ) pll->dLon -= 360.0;
   pll->dLon *= RAD;
}

      /******************************************************************
       *                          GeoGraphic()                          *
       *                                                                *
       *  This function converts geocentric lat/lon to geographic       *
       *  lat/lon.  Output longitude goes from 0 to 360.                *
       *                                                                *
       *  Arguments:                                                    *
       *   pllOut           Converted geographic lat/lon                *
       *   pllIn            Input geocentric lat/lon                    *
       *                                                                *
       ******************************************************************/
	   
void GeoGraphic( LATLON *pllOut, LATLON *pllIn )
{
   while ( pllIn->dLon > TWOPI) pllIn->dLon -= TWOPI;
   while ( pllIn->dLon < 0.0)   pllIn->dLon += TWOPI;
   pllOut->dLon = pllIn->dLon * DEG;
   pllOut->dLat = DEG * atan( (cos( pllIn->dLat ) / sin( pllIn->dLat )) /
                  0.993277 );
}

      /******************************************************************
       *                          GetDistanceAz()                       *
       *                                                                *
       *  This function calculates the distance and azimuth between two *
       *  points. Spherical trigonometric relations are used.  The      *
       *  azimuth is from ll1 to ll2.  NOTE: An explanation of the math *
       *  used here can be found in "Modern Trigonometry", by Kaj L.    *
       *  Nielsen, 1966, Barnes and Noble (a brief explanation exists in*
       *  the CRC Math Tables).                                         *
       *                                                                *
       *  Arguments:                                                    *
       *   pll1             From point                                  *
       *   pll2             To point                                    *
       *                                                                *
       *  Returns:                                                      *
       *   AZIDELT Structure with an azimuth and distance (180 degrees  *
       *           returned if a problem)                               *     
       ******************************************************************/
       
AZIDELT GetDistanceAz( LATLON *pll1, LATLON *pll2 )
{
   AZIDELT azidelt;     /* Return the distance and azimuth */
   double  dAdd;        /* 1/2 (pll1 azimuth + pll2 azimuth) */
   double  dDelLat;     /* Difference in latitudes */
   double  dDelLon;     /* Angle at north pole in radians */
   double  dSub;        /* 1/2 (pll1 azimuth - pll2 azimuth) */
  
/* Check inputs, if they could cause floating point error, send back zeroes */
   if ( pll1->dLat <= 0. || pll1->dLat > PI ||
        pll2->dLat <= 0. || pll2->dLat > PI ||
       (0.5 * (pll1->dLat+pll2->dLat)) == PI/2. ||
       (0.5 * (pll1->dLat-pll2->dLat)) == PI/2. || pll1->dLon < 0. || 
        pll1->dLon > TWOPI || pll2->dLon < 0. || pll2->dLon > TWOPI )
      logit ("t", "Bad Latlon in GetDistanceAz\nlat1=%lf, lon1=%lf, "
                  "lat2=%lf, lon2=%lf\n", pll1->dLat, pll1->dLon,
                                          pll2->dLat, pll2->dLon);
   dDelLon = fabs( pll1->dLon - pll2->dLon );   /* Polar angle */
   while ( dDelLon >= TWOPI ) dDelLon -= TWOPI; /* Not likely */
   if ( dDelLon >= PI ) dDelLon = TWOPI - dDelLon; /* Force it between 0 and PI */
   if ( dDelLon == PI ) dDelLon -= 0.000000001; /* So tan doesn't explode */
   if ( dDelLon == 0. ) dDelLon += 0.000000001;	/* Prevent divide by zero */
   dDelLat = pll1->dLat - pll2->dLat;
   if ( dDelLat == PI ) dDelLat -= 0.000000001;
   if ( dDelLat == 0. ) dDelLat += 0.000000001;
/* Use Napier analogies to get internal angles (azimuths) at pll1 and 2 */
   dAdd = atan( (cos( 0.5 * dDelLat )) / (tan( 0.5*dDelLon ) * 
                 cos( 0.5 * (pll1->dLat+pll2->dLat) )) );
   dSub = atan( (sin( 0.5 * dDelLat )) / (tan( 0.5*dDelLon ) * 
                 sin( 0.5 * (pll1->dLat+pll2->dLat) )) );
/* Subtract two to get pll1 to pll2 azimuth (add the two together if the
   azimuth from pll2 to pll1 is needed) */
   azidelt.dAzimuth = dAdd - dSub;
/* Get azimuth clockwise from north, account for crossing prime meridian */
   if ( azidelt.dAzimuth < 0. ) /* co-azimuth */
   {
      if ( pll2->dLon < pll1->dLon || pll2->dLon > pll1->dLon+PI ) /* sta. west */
         azidelt.dAzimuth = PI - azidelt.dAzimuth;                 /* of epi. */
      else                      /* Sta. east of epi. */
         azidelt.dAzimuth = PI + azidelt.dAzimuth;
   }
   else                         /* actual azimuth */
   {
      if ( pll2->dLon < pll1->dLon || pll2->dLon > pll1->dLon+PI ) 
           azidelt.dAzimuth = TWOPI - azidelt.dAzimuth;
   }
/* Check to see that computed azimuth makes sense */
   if ( azidelt.dAzimuth < 0. || azidelt.dAzimuth > TWOPI )
      logit( "t", "Bad az in GetDistanceAz: %lf\nlat1=%lf, lon1=%lf, "
             "lat2=%lf, lon2=%lf\n", azidelt.dAzimuth, pll1->dLat, pll1->dLon,
             pll2->dLat, pll2->dLon );
/* Use another one of Napier's analogies to get distance from pll1 to 2 */
   if ( dSub == 0. || dSub == PI ) dSub += 0.0000001; /* Prevent divide by zero */
   azidelt.dDelta = fabs( 2.*atan( (tan( 0.5*dDelLat ) *
                    sin( dAdd )) / sin( dSub ) ) );
/* Check to see that computed distance makes sense */
   if ( azidelt.dDelta < 0. || azidelt.dDelta > PI )
      logit ( "t", "Bad dist in GetDistanceAz: %lf\nlat1=%lf, lon1=%lf, "
              "lat2=%lf, lon2=%lf\n", azidelt.dDelta, pll1->dLat, pll1->dLon,
              pll2->dLat, pll2->dLon );
/* Convert radian angles to degrees */
   azidelt.dDelta *= DEG;
   azidelt.dAzimuth *= DEG;
   return( azidelt );
}

      /******************************************************************
       *                        GetLatLonTrig()                         *
       *                                                                *
       *  This function computes the sine and cos of the geocentric     *
       *  lat/lon.                                                      *
       *                                                                *
       *  Arguments:                                                    *
       *   pll              Input geocentric lat/lon                    *
       *                                                                *
       ******************************************************************/
	   
void GetLatLonTrig( LATLON *pll )
{
   pll->dCoslat = cos( pll->dLat );
   pll->dSinlat = sin( pll->dLat );
   pll->dCoslon = cos( pll->dLon );
   pll->dSinlon = sin( pll->dLon );
}

      /******************************************************************
       *                       GetOpAgency ()                           *
       *                                                                *
       *  This function takes an input Network ID and returns a         *
       *  descriptive string describing the agency.                     *
       *                                                                *
       *  Arguments:                                                    *
       *   pszNetID         Seismometer Network ID                      *
       *                                                                *
       *  Return:                                                       *
       *   String naming the operating agency.                          *
       *                                                                *
       ******************************************************************/
       
char *GetOpAgency( char *pszNetID )
{
   if ( !strcmp( pszNetID, "AT" ) )
      return( "West Coast/Alaska Tsunami Warning Center" );
   else if ( !strcmp( pszNetID, "AK" ) )
      return( "Alaska Earthquake Information Center" );
   else if ( !strcmp( pszNetID, "II" ) )
      return( "IRIS/IDA Project" );
   else if ( !strcmp( pszNetID, "IU" ) )
      return( "IRIS/ASL Project" );
   else if ( !strcmp( pszNetID, "GT" ) )
      return( "Global Telemetry Seismic Network" );
   else if ( !strcmp( pszNetID, "IC" ) )
      return( "China Siesmographic Bureau" );
   else if ( !strcmp( pszNetID, "US" ) )
      return( "US National Seismic Network" );
   else if ( !strcmp( pszNetID, "BK" ) )
      return( "UC - Berkeley" );
   else if ( !strcmp( pszNetID, "CN" ) )
      return( "Pacific Geosciences Center" );
   else if ( !strcmp( pszNetID, "UW" ) )
      return( "University of Washington" );
   else if ( !strcmp( pszNetID, "CI" ) )
      return( "Cal Tech" );
   else if ( !strcmp( pszNetID, "PT" ) )
      return( "Pacific Tsunami Warning Center" );
   else if ( !strcmp( pszNetID, "HV" ) )
      return( "Hawaii Volcanoes Observatory" );
   else if ( !strcmp( pszNetID, "LD" ) )
      return( "Lamont-Doherty Observatory" );
   else if ( !strcmp( pszNetID, "UO" ) )
      return( "University of Oregon" );
   else if ( !strcmp( pszNetID, "UU" ) )
      return( "University of Utah" );
   else if ( !strcmp( pszNetID, "NN" ) )
      return( "Nevada Seismic Network" );
   else if ( !strcmp( pszNetID, "NE" ) )
      return( "Boston College, Weston Obs." );
   else if ( !strcmp( pszNetID, "PR" ) )
      return( "Puerto Rico Seismic Network" );
   else return( "" );
}
	   
      /******************************************************************
       *                           GetRegion()                          *
       *                                                                *
       * This function computes and returns the region which the quake  *
       * is located in.  These regions key specific procedures at the   *
       * West Coast/Alaska Tsunami Warning Center.  Regions are:        *
       *     0 - U.S. west coast                                        *
       *     1 - British Columbia                                       *
       *     2 - Alaska/Alaska Peninsula/Aleutian                       *
       *     3 - Bering Sea-shallow                                     *
       *     4 - Bering Sea-deep                                        *
       *     5 - Pacific Basin (non WC&ATWC AOR)                        *
       *     8 - Arctic Ocean (AOR)                                     *
       *     9 - Arctic Ocean (non-AOR)                                 *
       *     10 - US East Coast                                         *
       *     11 - Gulf of Mexico                                        *
       *     12 - Eastern Canada                                        *
       *     13 - Gulf of St. Lawrence                                  *
       *     14 - Puerto Rico                                           *
       *     15 - Caribbean Sea                                         *
       *     16 - Mid-Atlantic Ridge                                    *
       *     17 - Atlantic Basin	                                *
       *     20 - None of the above                                     *
       *                                                                *
       *  January, 2005: Added Arctic and Bering Sea-shallow region     *
       *  December, 2005: Tightened up west coast and Alaska.           *
       *  November, 2005: Added Atlantic regions.                       *
       *  January, 2005: Added Atlantic regions.                        *
       *  November, 2004: Added a Caribbean region and adjusted Bering  *
       *                  Sea region.                                   *
       *                                                                *
       *  Arguments:                                                    *
       *   dLat             Epicentral latitude (geographic)            *
       *   dLon             Epicentral longitude                        *
       *                                                                *
       *  Return:                                                       *
       *   Region number (see above)                                    *
       *                                                                *
       ******************************************************************/
       
int GetRegion( double dLat, double dLon )
{
   double  dLatT, dLonT;        /* lat/lon with lon from 0-360 */
   int     iMsgRegion;          /* Quake region (see above) */

/* Adjust epicenter in case longitude comes in negative */
   dLatT = dLat;
   dLonT = dLon;
   if ( dLonT < 0. ) dLonT += 360.;

/* Get the region */
if (dLatT >= 53. && dLatT < 53.5 && dLonT >= 174. && dLonT <= 188.)
    iMsgRegion = 4;                 /* Bering Sea-deep */
  else if (dLatT >= 53.5 && dLatT < 54. && dLonT >= 174. && dLonT <= 191.)
    iMsgRegion = 4;                 /* Bering Sea-deep */
  else if (dLatT >= 54. && dLatT < 54.5 && dLonT >= 170. && dLonT <= 192.)
    iMsgRegion = 4;                 /* Bering Sea-deep */
  else if (dLatT >= 54.5 && dLatT < 55. && dLonT >= 170. && dLonT <= 195.)
    iMsgRegion = 4;                 /* Bering Sea-deep */
  else if (dLatT >= 55. && dLatT < 55.5 && dLonT >= 167. && dLonT <= 195.)
    iMsgRegion = 4;                 /* Bering Sea-deep */
  else if (dLatT >= 55.5 && dLatT < 56. && dLonT >= 167. && dLonT <= 193.)
    iMsgRegion = 4;                 /* Bering Sea-deep */
  else if (dLatT >= 56. && dLatT < 57. && dLonT >= 164. && dLonT <= 193.)
    iMsgRegion = 4;                 /* Bering Sea-deep */
  else if (dLatT >= 57. && dLatT < 58. && dLonT >= 162. && dLonT <= 190.)
    iMsgRegion = 4;                 /* Bering Sea-deep */
  else if (dLatT >= 58. && dLatT < 59. && dLonT >= 162. && dLonT <= 188.)
    iMsgRegion = 4;                 /* Bering Sea-deep */
  else if (dLatT >= 59. && dLatT < 60. && dLonT >= 163. && dLonT <= 186.)
    iMsgRegion = 4;                 /* Bering Sea-deep */
  else if (dLatT >= 60. && dLatT < 61. && dLonT >= 165. && dLonT <= 184.)
    iMsgRegion = 4;                 /* Bering Sea-deep */
  else if (dLatT >= 61. && dLatT < 62. && dLonT >= 172. && dLonT <= 183.)
    iMsgRegion = 4;                 /* Bering Sea-deep */
  else if (dLatT >= 62. && dLatT < 63. && dLonT >= 175. && dLonT <= 180.)
    iMsgRegion = 4;                 /* Bering Sea-deep */
  else if (dLatT >= 55. && dLatT < 55.5 && dLonT >= 195. && dLonT <= 197.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 55.5 && dLatT < 56. && dLonT >= 193. && dLonT <= 198.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 56. && dLatT < 56.5 && dLonT >= 193. && dLonT <= 199.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 56.5 && dLatT < 57. && dLonT >= 193. && dLonT <= 201.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 57. && dLatT < 57.5 && dLonT >= 190. && dLonT <= 202.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 57.5 && dLatT < 58. && dLonT >= 190. && dLonT <= 203.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 58. && dLatT < 59. && dLonT >= 188. && dLonT <= 203.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 59. && dLatT < 59.5 && dLonT >= 186. && dLonT <= 203.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 59.5 && dLatT < 60. && dLonT >= 186. && dLonT <= 199.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 60. && dLatT < 60.5 && dLonT >= 184. && dLonT <= 199.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 60.5 && dLatT < 61. && dLonT >= 184. && dLonT <= 196.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 61. && dLatT < 62. && dLonT >= 183. && dLonT <= 196.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 62. && dLatT < 63. && dLonT >= 180. && dLonT <= 196.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 63. && dLatT < 65. && dLonT >= 178. && dLonT <= 200.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 65. && dLatT < 66. && dLonT >= 178. && dLonT <= 195.)
    iMsgRegion = 3;                 /* Bering Sea-shallow */
  else if (dLatT >= 66. && dLatT < 75. && dLonT >= 178. && dLonT <= 200.)
    iMsgRegion = 8;                 /* Arctic O. AOR */
  else if (dLatT >= 69. && dLatT < 79. && dLonT >= 200. && dLonT <= 220.)
    iMsgRegion = 8;                 /* Arctic O. AOR */
  else if (dLatT >= 68. && dLatT < 83. && dLonT >= 220. && dLonT <= 240.)
    iMsgRegion = 8;                 /* Arctic O. AOR */
  else if (dLatT >= 67. && dLatT < 87. && dLonT >= 240. && dLonT <= 276.)
    iMsgRegion = 8;                 /* Arctic O. AOR */
  else if (dLatT >= 81. && dLatT < 87. && dLonT >= 276. && dLonT <= 300.)
    iMsgRegion = 8;                 /* Arctic O. AOR */
  else if (dLatT >= 45. && dLatT < 54.5 && dLonT >= 170. && dLonT < 195.)
    iMsgRegion = 2;                 /* Alaska/AK Pen./Aleutians */
  else if (dLatT >= 49. && dLatT < 69. && dLonT >= 195. && dLonT < 210.)
    iMsgRegion = 2;                 /* Alaska/AK Pen./Aleutians */
  else if (dLatT >= 52. && dLatT < 69.0 && dLonT >= 210. && dLonT < 220.)
    iMsgRegion = 2;                 /* Alaska/AK Pen./Aleutians */
  else if (dLatT >= 54.5 && dLatT < 68. && dLonT >= 220. && dLonT < 235.)
    iMsgRegion = 2;                 /* Alaska/AK Pen./Aleutians */
  else if (dLatT > 48.5 && dLatT <= 50.0 && dLonT >= 223. && dLonT < 244.)
    iMsgRegion = 1;                 /* British Columbia */
  else if (dLatT > 50.0 && dLatT <= 51.0 && dLonT >= 222. && dLonT < 244.)
    iMsgRegion = 1;                 /* British Columbia */
  else if (dLatT > 51.0 && dLatT <= 52.0 && dLonT >= 221. && dLonT < 242.)
    iMsgRegion = 1;                 /* British Columbia */
  else if (dLatT > 52.0 && dLatT <= 54.5 && dLonT >= 220. && dLonT < 240.)
    iMsgRegion = 1;                 /* British Columbia */
  else if (dLatT >= 39.0 && dLatT <= 48.5 && dLonT >= 226. && dLonT < 244.)
    iMsgRegion = 0;                 /* U.S. West Coast */
  else if (dLatT >= 36.0 && dLatT <= 39.0 && dLonT >= 228. && dLonT < 244.)
    iMsgRegion = 0;                 /* U.S. West Coast */
  else if (dLatT >= 34.0 && dLatT <= 36.0 && dLonT >= 230. && dLonT < 246.)
    iMsgRegion = 0;                 /* U.S. West Coast */
  else if (dLatT >= 31.0 && dLatT <= 34.0 && dLonT >= 231. && dLonT < 248.)
    iMsgRegion = 0;                 /* U.S. West Coast */
  else if ( dLatT >= 51.0 && dLatT < 52.0 && dLonT >= 300. && dLonT < 303.5 )
    iMsgRegion = 13;                /* Gulf of St. Lawrence */
  else if ( dLatT >= 50.0 && dLatT < 51.0 && dLonT >= 290. && dLonT < 303. )
    iMsgRegion = 13;                /* Gulf of St. Lawrence */
  else if ( dLatT >= 48.0 && dLatT < 50.0 && dLonT >= 290. && dLonT < 302.5 )
    iMsgRegion = 13;                /* Gulf of St. Lawrence */
  else if ( dLatT >= 47.0 && dLatT < 48.0 && dLonT >= 290. && dLonT < 300. )
    iMsgRegion = 13;                /* Gulf of St. Lawrence */
  else if ( dLatT >= 46.0 && dLatT < 47.0 && dLonT >= 294. && dLonT < 299. )
    iMsgRegion = 13;                /* Gulf of St. Lawrence */
  else if ( dLatT >= 45.5 && dLatT < 46.0 && dLonT >= 296. && dLonT < 298.5 )
    iMsgRegion = 13;                /* Gulf of St. Lawrence */
  else if ( dLatT >= 22.0 && dLatT < 24.0 && dLonT >= 279. && dLonT < 284. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 24.0 && dLatT < 27.0 && dLonT >= 279. && dLonT < 285. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 27.0 && dLatT < 28.0 && dLonT >= 278.5 && dLonT < 285. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 28.0 && dLatT < 30.0 && dLonT >= 278. && dLonT < 285. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 30.0 && dLatT < 31.0 && dLonT >= 277. && dLonT < 286. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 31.0 && dLatT < 32.0 && dLonT >= 277. && dLonT < 287. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 32.0 && dLatT < 33.0 && dLonT >= 276. && dLonT < 288. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 33.0 && dLatT < 34.0 && dLonT >= 276. && dLonT < 289. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 34.0 && dLatT < 36.0 && dLonT >= 276. && dLonT < 290. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 36.0 && dLatT < 38.0 && dLonT >= 278. && dLonT < 291. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 38.0 && dLatT < 39.0 && dLonT >= 278. && dLonT < 294. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 39.0 && dLatT < 40.0 && dLonT >= 278. && dLonT < 295. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 40.0 && dLatT < 41.0 && dLonT >= 280. && dLonT < 297. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 41.0 && dLatT < 42.0 && dLonT >= 280. && dLonT < 300. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 42.0 && dLatT < 45.0 && dLonT >= 280. && dLonT < 294. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 45.0 && dLatT < 48.0 && dLonT >= 288. && dLonT < 293. )
    iMsgRegion = 10;                /* U.S. East Coast */
  else if ( dLatT >= 18.5 && dLatT < 20.0 && dLonT >= 263. && dLonT < 271. )
    iMsgRegion = 11;                /* Gulf of Mexico */
  else if ( dLatT >= 20.0 && dLatT < 21.0 && dLonT >= 261. && dLonT < 271. )
    iMsgRegion = 11;                /* Gulf of Mexico */
  else if ( dLatT >= 21.0 && dLatT < 22.0 && dLonT >= 261. && dLonT < 274. )
    iMsgRegion = 11;                /* Gulf of Mexico */
  else if ( dLatT >= 22.0 && dLatT < 22.5 && dLonT >= 261. && dLonT < 276. )
    iMsgRegion = 11;                /* Gulf of Mexico */
  else if ( dLatT >= 22.5 && dLatT < 23.0 && dLonT >= 261. && dLonT < 277. )
    iMsgRegion = 11;                /* Gulf of Mexico */
  else if ( dLatT >= 23.0 && dLatT < 27.0 && dLonT >= 261. && dLonT < 279. )
    iMsgRegion = 11;                /* Gulf of Mexico */
  else if ( dLatT >= 27.0 && dLatT < 28.0 && dLonT >= 261. && dLonT < 278.5 )
    iMsgRegion = 11;                /* Gulf of Mexico */
  else if ( dLatT >= 28.0 && dLatT < 30.0 && dLonT >= 261. && dLonT < 278. )
    iMsgRegion = 11;                /* Gulf of Mexico */
  else if ( dLatT >= 30.0 && dLatT < 31.0 && dLonT >= 261. && dLonT < 277. )
    iMsgRegion = 11;                /* Gulf of Mexico */
  else if ( dLatT >= 31.0 && dLatT < 32.0 && dLonT >= 263. && dLonT < 277. )
    iMsgRegion = 11;                /* Gulf of Mexico */
  else if ( dLatT >= 32.0 && dLatT < 34.0 && dLonT >= 264. && dLonT < 276. )
    iMsgRegion = 11;                /* Gulf of Mexico */
  else if ( dLatT >= 42.0 && dLatT < 45.0 && dLonT >= 294. && dLonT < 315. )
    iMsgRegion = 12;                /* Eastern Canada */
  else if ( dLatT >= 45.0 && dLatT < 46.0 && dLonT >= 285. && dLonT < 316. )
    iMsgRegion = 12;                /* Eastern Canada */
  else if ( dLatT >= 46.0 && dLatT < 48.0 && dLonT >= 285. && dLonT < 318. )
    iMsgRegion = 12;                /* Eastern Canada */
  else if ( dLatT >= 48.0 && dLatT < 52.0 && dLonT >= 285. && dLonT < 315. )
    iMsgRegion = 12;                /* Eastern Canada */
  else if ( dLatT >= 52.0 && dLatT < 58.0 && dLonT >= 285. && dLonT < 310. )
    iMsgRegion = 12;                /* Eastern Canada */
  else if ( dLatT >= 58.0 && dLatT < 62.0 && dLonT >= 285. && dLonT < 307. )
    iMsgRegion = 12;                /* Eastern Canada */
  else if ( dLatT >= 16.0 && dLatT < 22.0 && dLonT >= 290. && dLonT < 302. )
    iMsgRegion = 14;                /* Puerto Rico */
  else if ( dLatT >= 8.0 && dLatT < 9.2 && dLonT >= 282. && dLonT < 305. )
    iMsgRegion = 15;                /* Caribbean */
  else if ( dLatT >= 8.5 && dLatT < 9.0 && dLonT >= 278. && dLonT < 280. )
    iMsgRegion = 15;                /* Caribbean */
  else if ( dLatT >= 9.0 && dLatT < 9.2 && dLonT >= 276.5 && dLonT < 280. )
    iMsgRegion = 15;                /* Caribbean */
  else if ( dLatT >= 9.2 && dLatT < 10.0 && dLonT >= 276.5 && dLonT < 305. )
    iMsgRegion = 15;                /* Caribbean */
  else if ( dLatT >= 10.0 && dLatT < 11.0 && dLonT >= 276. && dLonT < 305. )
    iMsgRegion = 15;                /* Caribbean */
  else if ( dLatT >= 11.0 && dLatT < 15.0 && dLonT >= 275. && dLonT < 305. )
    iMsgRegion = 15;                /* Caribbean */
  else if ( dLatT >= 15.0 && dLatT < 21.0 && dLonT >= 271. && dLonT < 305. )
    iMsgRegion = 15;                /* Caribbean */
  else if ( dLatT >= 21.0 && dLatT < 22.0 && dLonT >= 274. && dLonT < 305. )
    iMsgRegion = 15;                /* Caribbean */
  else if ( dLatT >= 22.0 && dLatT < 22.5 && dLonT >= 276. && dLonT < 279. )
    iMsgRegion = 15;                /* Caribbean */
  else if ( dLatT >= 22.5 && dLatT < 23.0 && dLonT >= 277. && dLonT < 279. )
    iMsgRegion = 15;                /* Caribbean */
  else if ( dLatT >= 22.0 && dLatT < 24.0 && dLonT >= 284. && dLonT < 305. )
    iMsgRegion = 15;                /* Caribbean */
  else if ( dLatT >= -52.0 && dLatT < -50.0 && dLonT >= 346. && dLonT < 358. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= -50.0 && dLatT < -45.0 && dLonT >= 340. && dLonT < 358. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= -45.0 && dLatT < -6.0 && dLonT >= 340. && dLonT < 352. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= -6.0 && dLatT < -2.0 && dLonT >= 330. && dLonT < 352. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= -2.0 && dLatT < 2.0 && dLonT >= 326. && dLonT < 352. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= 2.0 && dLatT < 3.0 && dLonT >= 326. && dLonT < 345. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= 3.0 && dLatT < 5.0 && dLonT >= 320. && dLonT < 345. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= 5.0 && dLatT < 7.0 && dLonT >= 320. && dLonT < 332. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= 7.0 && dLatT < 10.0 && dLonT >= 314. && dLonT < 332. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= 10.0 && dLatT < 12.0 && dLonT >= 310. && dLonT < 332. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= 12.0 && dLatT < 28.0 && dLonT >= 310. && dLonT < 321. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= 28.0 && dLatT < 30.0 && dLonT >= 310. && dLonT < 327. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= 30.0 && dLatT < 35.0 && dLonT >= 313. && dLonT < 327. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= 35.0 && dLatT < 40.0 && dLonT >= 320. && dLonT < 331. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if ( dLatT >= 40.0 && dLatT < 61.0 && dLonT >= 320. && dLonT < 335. )
    iMsgRegion = 16;                /* Atlantic Ridge */
  else if (dLatT >= 45. && dLatT < 66. && dLonT > 130. && dLonT < 240.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= 25. && dLatT < 45. && dLonT > 110. && dLonT < 255.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= 20. && dLatT < 25. && dLonT > 99. && dLonT < 261.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= 18.5 && dLatT < 20. && dLonT > 99. && dLonT < 263.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= 15. && dLatT < 18.5 && dLonT > 99. && dLonT < 271.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= 11. && dLatT < 15. && dLonT > 99. && dLonT < 275.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= 10. && dLatT < 11. && dLonT > 99. && dLonT < 276.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= 9. && dLatT < 10. && dLonT > 99. && dLonT < 276.5)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= 8.5 && dLatT < 9. && dLonT > 99. && dLonT < 278.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= 8.5 && dLatT < 9.2 && dLonT > 280. && dLonT < 282.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= 8.0 && dLatT < 8.5 && dLonT > 99. && dLonT < 282.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= -4. && dLatT < 8. && dLonT > 102. && dLonT < 285.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= -7. && dLatT < -4. && dLonT > 108. && dLonT < 285.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= -13. && dLatT < -7. && dLonT > 132. && dLonT < 285.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= -30. && dLatT < -13. && dLonT > 132. && dLonT < 295.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= -40. && dLatT < -30. && dLonT > 132. && dLonT < 292.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if (dLatT >= -70. && dLatT < -40. && dLonT > 132. && dLonT < 290.)
    iMsgRegion = 5;                 /* Pacific event non-WC&ATWC AOR */
  else if ( dLatT >= -70.0 && dLatT < -20.0 && dLonT >= 0. && dLonT < 23. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= -20.0 && dLatT < 10.0 && dLonT >= 0. && dLonT < 20. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 41.0 && dLatT < 48.0 && dLonT >= 0. && dLonT < 3. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 48.0 && dLatT < 62.0 && dLonT >= 0. && dLonT < 10. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 62.0 && dLatT < 78.0 && dLonT >= 0. && dLonT < 25. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= -70.0 && dLatT < -60.0 && dLonT >= 300. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= -60.0 && dLatT < -43.0 && dLonT >= 290. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= -43.0 && dLatT < -36.0 && dLonT >= 292. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= -36.0 && dLatT < -28.0 && dLonT >= 298. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= -28.0 && dLatT < -20.0 && dLonT >= 305. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= -20.0 && dLatT < -15.0 && dLonT >= 310. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= -15.0 && dLatT < -5.0 && dLonT >= 315. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= -5.0 && dLatT < 1.0 && dLonT >= 305. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 1.0 && dLatT < 8.0 && dLonT >= 296. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 9.2 && dLatT < 10.0 && dLonT >= 276.5 && dLonT < 282. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 9.0 && dLatT < 9.2 && dLonT >= 276.5 && dLonT < 280. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 8.5 && dLatT < 9.0 && dLonT >= 278. && dLonT < 280. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 8.0 && dLatT < 10.0 && dLonT >= 282. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 10.0 && dLatT < 11.0 && dLonT >= 276. && dLonT < 350. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 11.0 && dLatT < 15.0 && dLonT >= 275. && dLonT < 350. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 15.0 && dLatT < 18.5 && dLonT >= 271. && dLonT < 350. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 18.5 && dLatT < 20.0 && dLonT >= 263. && dLonT < 350. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 20.0 && dLatT < 27.0 && dLonT >= 261. && dLonT < 350. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 27.0 && dLatT < 35.0 && dLonT >= 261. && dLonT < 355. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 35.0 && dLatT < 41.0 && dLonT >= 275. && dLonT < 355. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 41.0 && dLatT < 42.0 && dLonT >= 275. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 42.0 && dLatT < 50.0 && dLonT >= 280. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 50.0 && dLatT < 67.0 && dLonT >= 265. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 67.0 && dLatT < 81.0 && dLonT >= 277. && dLonT <= 360. )
    iMsgRegion = 17;                 /* Atlantic Basin */
  else if ( dLatT >= 66.0 )
    iMsgRegion = 9;                  /* Arctic Ocean */
  else
    iMsgRegion = 20;                 /* Not in any of the above */
   return iMsgRegion;
}

      /******************************************************************
       *                       GetSeisInfo ()                           *
       *                                                                *
       *  This function takes an input seismometer type indicator and   *
       *  returns the type in a string and flat response length.        *
       *                                                                *
       *  Arguments:                                                    *
       *   pszNetID         Seismometer Network ID                      *
       *                                                                *
       *  Return:                                                       *
       *   String naming the operating agency.                          *
       *                                                                *
       ******************************************************************/
       
char *GetSeisInfo( int iSeisType, double *dFlatResp )
{
   *dFlatResp = 1.;
   if ( iSeisType == 103 )
      return( "Short period/Low gain" );
   else if ( iSeisType == 102 )
      return( "Short period/Medium gain" );
   else if ( iSeisType == 101 )
      return( "Short period/High gain" );
   else if ( iSeisType == 100 )
      return( "Short period/Uncalibrated" );
   else if ( iSeisType == 52 )
      return( "Long period/Low gain" );
   else if ( iSeisType == 51 )
      return( "Long period/High gain" );
   else if ( iSeisType == 50 )
      return( "Long period/Uncalibrated" );
   else if ( iSeisType == 30 )
      return( "Accelerometer/FBA ES-T" );
   else if ( iSeisType == 20 )
      return( "Broadband/Unknown Type" );
   else if ( iSeisType == 1 )
   {
      *dFlatResp = 360.;
      return( "Streckheisen STS-1" );
   }
   else if ( iSeisType == 2 )
   {
      *dFlatResp = 130.;
      return( "Streckheisen STS-2" );
   }
   else if ( iSeisType == 3 )
   {
      *dFlatResp = 30.;
      return( "Guralp CMG-3NSN" );
   }
   else if ( iSeisType == 4 )
   {
      *dFlatResp = 100.;
      return( "Guralp CMG-3T" );
   }   
   else if ( iSeisType == 5 )
   {
      *dFlatResp = 360.;
      return( "Kinemetrics KS360i" );
   }     
   else if ( iSeisType == 6 )
   {
      *dFlatResp = 360.;
      return( "Kinemetrics KS54000" );
   }     
   else if ( iSeisType == 7 )
   {
      *dFlatResp = 30.;
      return( "Guralp CMG-3" );
   } 
   else if ( iSeisType == 8 )
   {
      *dFlatResp = 60.;
      return( "Guralp CMG-40T" );
   } 
   else if ( iSeisType == 9 )
   {
      *dFlatResp = 30.;
      return( "Guralp CMG-3TNSN" );
   } 
   else if ( iSeisType == 10 )
   {
      *dFlatResp = 20.;
      return( "Geotech KS-10" );
   }
   else if ( iSeisType == 11 )
   {
      *dFlatResp = 30.;
      return( "Guralp CMG-3ESP_30" );
   }
   else if ( iSeisType == 12 )
   {
      *dFlatResp = 60.;
      return( "Guralp CMG-3ESP_60" );
   } 
   else if ( iSeisType == 13 )
   {
      *dFlatResp = 130.;
      return( "Trillium" );
   } 
   else if ( iSeisType == 14 )
   {
      *dFlatResp = 120.;
      return( "Guralp CMG-3ESP_120" );
   } 
   else if ( iSeisType == 15 )
   {
      *dFlatResp = 20.;
      return( "Guralp CMG40T_20" );
   } 
   else if ( iSeisType == 16 )
   {
      *dFlatResp = 360.;
      return( "Guralp CMG3T_360" );
   } 
   else if ( iSeisType == 17 )
   {
      *dFlatResp = 120.;
      return( "Kinemetrics 2000_120" );
   } 
   else return( "" );
}

      /******************************************************************
       *                            itoaX()                             *
       *                                                                *
       *  Convert integer to character string.                          *
       *                                                                *
       *  From Stu at PTWC. (Added for compatibility with Solaris.)     *
       *                                                                *
       *  Arguments:                                                    *
       *   iNum             Integer to convert                          *
       *   pszNum           String pointer with int converted to char   *
       *                                                                *
       *  Return:                                                       *
       *   Pointer to character string (pszNum)                         *
       *                                                                *
       ******************************************************************/

char *itoaX( int iNum, char *pszNum )
{
   int   i;
   int   iDigit;       /* Values to convert */
   int   iPower;       /* Number of characters necessary in converted string */
 
   if ( pszNum == (char *) NULL )
   {
      logit( "", "No memory allocated for pszNum in itoaX\n");
      return NULL;
   }
 
   iPower = (int) log10( (double) iNum );  
 
   for ( i=0; i<=iPower; i++)
   {
      iDigit = iNum%10;
      pszNum[iPower-i]= iDigit + 48;
      iNum=iNum/10;
   }
 
   pszNum[iPower+1] = '\0';
   return pszNum;
}

      /******************************************************************
       *                          LLConv()                              *
       *                                                                *
       *  This function converts a character string to a double         *
       *  precision floating point value and accounts for south and     *
       *  west lat/lons.                                                *
       *                                                                *
       *  Arguments:                                                    *
       *   psz              Input lat/lon string with n,s,...           *
       *                                                                *	   
       *  Returns:          Converted lat or lon (double)               *
       *                                                                *
       ******************************************************************/
	   
double LLConv( char *psz )
{
   double  dTemp;

   dTemp = atof( psz );
   if ( strchr( psz, 's' ) || strchr( psz, 'S' ) || strchr( psz, 'w' ) ||
        strchr( psz, 'W' ) ) dTemp *= -1.0;
   return dTemp;
}

      /******************************************************************
       *                           PadZeroes()                          *
       *                                                                *
       *  This function adds leading zeroes to a numeric character      *
       *  string. (10 characters maximum in string).                    *
       *                                                                *
       *  Arguments:                                                    *
       *   iNumDigits       # digits desired in output                  *
       *   pszString        String to be padded with zeroes             *
       *                                                                *
       ******************************************************************/
	   
void PadZeroes( int iNumDigits, char *pszString )
{
   int     i, iStringLen;
   char    szTemp1[10], szTemp2[10];

   strcpy( szTemp1, pszString );
   strcpy( szTemp2, szTemp1 );
   iStringLen = strlen( szTemp1 );
   if ( iNumDigits-iStringLen > 0 )     /* Number of zeroes to add */
   {
      for ( i=0; i<iNumDigits-iStringLen; i++ ) szTemp1[i] = '0';
      for ( i=iNumDigits-iStringLen; i<iNumDigits; i++ ) 
         szTemp1[i] = szTemp2[i-(iNumDigits-iStringLen)];
      szTemp1[iNumDigits] = '\0';       /* Add NULL character to end */
      strcpy( pszString, szTemp1 );
   }
}

      /******************************************************************
       *                          PointToEpi()                          *
       *                                                                *
       *  This function returns a latitude and longitude given a point, *
       *  an azimuth and distance from that point.  The calculated lat  *
       *  and lon is delta degrees away from input lat and lon.         *
       *  Spherical trigonometric relations are used in the             *
       *  computations.                                                 *
       *                                                                *
       *  Arguments:                                                    *
       *   pll1             Input geocentric lat/lon (on az, from pt.)  *
       *   pazidelt         Distance/az from point to output point      *
       *                                                                *
       *  Return:                                                       *
       *   LATLON structure geocentric location of new point.           *
       *                                                                *
       ******************************************************************/
	   
LATLON PointToEpi( LATLON *pll1, AZIDELT *pazidelt )
{
   double   dAz;                  /* Azimuth from input point */
   double   dSinamb, dSinapb, dCosamb, dCosapb, dA, dB;
   LATLON   ll2;                  /* Computed lat/lon to return */

   dAz = pazidelt->dAzimuth;      /* Make this local as it may change */
   if ( dAz > PI ) dAz = TWOPI - dAz;
   dSinamb = sin( (pll1->dLat - pazidelt->dDelta) / 2.0 );
   dSinapb = sin( (pll1->dLat + pazidelt->dDelta) / 2.0 );
   dCosamb = cos( (pll1->dLat - pazidelt->dDelta) / 2.0 );
   dCosapb = cos( (pll1->dLat + pazidelt->dDelta) / 2.0 );
   dA = atan( dSinamb / (dSinapb*tan( dAz/2.0 )) ) + 
        atan( dCosamb / (dCosapb*tan( dAz/2.0 )) );
   if (dA < 0.) dA += PI;     
   dB = 2.0 * atan( dCosamb / (dCosapb*tan( dAz / 2.0 )) ) - dA;
   ll2.dLat = acos( (cos( pazidelt->dDelta ) * cos( pll1->dLat )) +
              (sin( pazidelt->dDelta ) * sin( pll1->dLat ) * cos( dAz )) );
   if ( pazidelt->dAzimuth < PI ) ll2.dLon = pll1->dLon + dB;
   else                           ll2.dLon = pll1->dLon - dB;
   while ( ll2.dLon >= TWOPI ) ll2.dLon -= TWOPI;  /* Just a check */
   while ( ll2.dLon < 0.0 )    ll2.dLon += TWOPI;
   while ( ll2.dLat >= PI )    ll2.dLat -= PI;
   while ( ll2.dLat < 0.0 )    ll2.dLat += PI;
   return( ll2 );
}

      /******************************************************************
       *                      StationAziDelt()                          *
       *                                                                *
       *  This function calculates the distance and azimuth between a   *
       *  point and a number of seismic stations' latitude and          *
       *  longitude.  NOTE: This can only be used when the incoming     *
       *  array is in the USGSDATA structure!                           *
       *                                                                *
       *  Arguments:                                                    *
       *   iNumStas    # of sites to get az and delta                   *
       *   pll         geocentric lat/lon of point (epi?)               *
       *   DataArray   Array with site locations (geocent)              *
       *                                                                *
       ******************************************************************/
void StationAziDelt( int iNumStas, LATLON *pll, USGSDATA DataArray[] )
{
/* Note that all input and output must be in geocentric radians */
   double  dTempAzimuth, dDeltaCosine, dDeltaSine;
   double  dSnooze, dCooze;
   int     i;

   for ( i=0; i<iNumStas; i++ )
   {
      dDeltaCosine = pll->dCoslat*DataArray[i].dCoslat + 
                     pll->dSinlat*DataArray[i].dSinlat *
                     (pll->dCoslon*DataArray[i].dCoslon + 
                     pll->dSinlon*DataArray[i].dSinlon);
      dDeltaSine = sqrt( 1. - dDeltaCosine * dDeltaCosine );
      if ( dDeltaSine   == 0.0 ) dDeltaSine = 0.01;
      if ( dDeltaCosine == 0.0 ) dDeltaCosine = 0.01;
      DataArray[i].dDelta =  atan( dDeltaSine/dDeltaCosine );
      dCooze = (DataArray[i].dCoslat - pll->dCoslat*dDeltaCosine) /
               (pll->dSinlat*dDeltaSine);
      if ( dCooze == 0. ) dCooze = 0.01;
      dSnooze = DataArray[i].dSinlat * (pll->dCoslon*DataArray[i].dSinlon - 
                pll->dSinlon*DataArray[i].dCoslon) / dDeltaSine;
      dTempAzimuth = atan( dSnooze/dCooze );
      while ( dTempAzimuth < 0.0 ) dTempAzimuth += PI;
      if ( dSnooze <= 0. ) dTempAzimuth += PI;
      while ( DataArray[i].dDelta < 0.0 ) DataArray[i].dDelta += PI;
      DataArray[i].dAzimuth = dTempAzimuth * DEG;
      DataArray[i].dDelta *= DEG;
   }
}

      /******************************************************************
       *                      StationAziDelt()                          *
       *                                                                *
       *  This function calculates the distance and azimuth between a   *
       *  point and a number of seismic stations' latitude and          *
       *  longitude.  NOTE: This can only be used when the incoming     *
       *  array is in the USGSDATA structure!                           *
       *                                                                *
       *  This is the same as StationAziDelt except that it is passed   *
       *  a USGSDATA2 structure instead of a USGSDATA structure.        * 
       *                                                                *
       *  Arguments:                                                    *
       *   iNumStas    # of sites to get az and delta                   *
       *   pll         geocentric lat/lon of point (epi?)               *
       *   DataArray   Array with site locations (geocent)              *
       *                                                                *
       ******************************************************************/
void StationAziDelt2( int iNumStas, LATLON *pll, USGSDATA2 DataArray[] )
{
/* Note that all input and output must be in geocentric radians */
   double  dTempAzimuth, dDeltaCosine, dDeltaSine;
   double  dSnooze, dCooze;
   int     i;

   for ( i=0; i<iNumStas; i++ )
   {
      dDeltaCosine = pll->dCoslat*DataArray[i].dCoslat + 
                     pll->dSinlat*DataArray[i].dSinlat *
                     (pll->dCoslon*DataArray[i].dCoslon + 
                     pll->dSinlon*DataArray[i].dSinlon);
      dDeltaSine = sqrt( 1. - dDeltaCosine * dDeltaCosine );
      if ( dDeltaSine   == 0.0 ) dDeltaSine = 0.01;
      if ( dDeltaCosine == 0.0 ) dDeltaCosine = 0.01;
      DataArray[i].dDelta =  atan( dDeltaSine/dDeltaCosine );
      dCooze = (DataArray[i].dCoslat - pll->dCoslat*dDeltaCosine) /
               (pll->dSinlat*dDeltaSine);
      if ( dCooze == 0. ) dCooze = 0.01;
      dSnooze = DataArray[i].dSinlat * (pll->dCoslon*DataArray[i].dSinlon - 
                pll->dSinlon*DataArray[i].dCoslon) / dDeltaSine;
      dTempAzimuth = atan( dSnooze / dCooze );
      while ( dTempAzimuth < 0.0 ) dTempAzimuth += PI;
      if ( dSnooze <= 0. ) dTempAzimuth += PI;
      while ( DataArray[i].dDelta < 0.0 ) DataArray[i].dDelta += PI;
      DataArray[i].dAzimuth = dTempAzimuth * DEG;
      DataArray[i].dDelta *= DEG;
   }
}
