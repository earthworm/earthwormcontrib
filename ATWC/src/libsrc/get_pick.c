 /************************************************************************
  * GET_PICK.C                                                           *
  *                                                                      *
  * This is a group of functions which provide tools for                 *
  * reading P-picks from a ring and loading them in a structure.         *
  *                                                                      *
  * Made into earthworm module 3/2001.                                   *
  *                                                                      *
  ************************************************************************/
  
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include "earlybirdlib.h"

     /**************************************************************
      *                         CopyPBuf()                         *
      *                                                            *
      * Copy one PPICK structure to another.                       *
      *                                                            *
      * Arguments:                                                 *
      *  PIn         Input PPICK structure                         *
      *  POut        Output PPICK structure                        *
      *                                                            *
      **************************************************************/

void CopyPBuf( PPICK *PIn, PPICK *POut )
{
   strcpy (POut->szStation, PIn->szStation );
   strcpy (POut->szChannel, PIn->szChannel );
   strcpy (POut->szNetID,   PIn->szNetID );
   strcpy (POut->szPhase,   PIn->szPhase );
   POut->lPickIndex       = PIn->lPickIndex;
   POut->iUseMe           = PIn->iUseMe;
   POut->dPTime           = PIn->dPTime;
   POut->cFirstMotion     = PIn->cFirstMotion;
   POut->dMbAmpGM         = PIn->dMbAmpGM;
   POut->lMbPer           = PIn->lMbPer;
   POut->dMbTime          = PIn->dMbTime;
   POut->dMbMag           = PIn->dMbMag;
   POut->iMbClip          = PIn->iMbClip;
   POut->dMlAmpGM         = PIn->dMlAmpGM;
   POut->lMlPer           = PIn->lMlPer;
   POut->dMlTime          = PIn->dMlTime;
   POut->dMlMag           = PIn->dMlMag;
   POut->iMlClip          = PIn->iMlClip;
   POut->dMSAmpGM         = PIn->dMSAmpGM;
   POut->lMSPer           = PIn->lMSPer;
   POut->dMSTime          = PIn->dMSTime;
   POut->dMSMag           = PIn->dMSMag;
   POut->iMSClip          = PIn->iMSClip;
   POut->dMwpIntDisp      = PIn->dMwpIntDisp;
   POut->dMwpTime         = PIn->dMwpTime;
   POut->dMwpMag          = PIn->dMwpMag;
   POut->dLat             = PIn->dLat;
   POut->dLon             = PIn->dLon;
   POut->dCoslat          = PIn->dCoslat;
   POut->dSinlat          = PIn->dSinlat;
   POut->dCoslon          = PIn->dCoslon;
   POut->dSinlon          = PIn->dSinlon;
   POut->dSens            = PIn->dSens;
   POut->dClipLevel       = PIn->dClipLevel;
   POut->dElevation       = PIn->dElevation;
   POut->dGainCalibration = PIn->dGainCalibration;
   POut->iStationType     = PIn->iStationType;
   POut->dRes             = PIn->dRes;
   POut->dDelta           = PIn->dDelta;
   POut->dAz              = PIn->dAz;
   POut->dFracDelta       = PIn->dFracDelta;
   POut->dSnooze          = PIn->dSnooze;
   POut->dCooze           = PIn->dCooze;
}

      /******************************************************************
       *                            InitP()                             *
       *                                                                *
       *  This function initializes a PPICK structure.                  *
       *                                                                *
       *  Arguments:                                                    *
       *   pP               PPICK structure                             *
       *                                                                *
       ******************************************************************/
	   
void InitP( PPICK *pP )
{
   strcpy (pP->szStation, "     ");
   strcpy (pP->szChannel, "     ");
   strcpy (pP->szNetID, "  ");
   strcpy (pP->szPhase, "     ");
   pP->dLat = 0.;
   pP->dLon = 0.;
   pP->dElevation = 0.;
   pP->dSinlat = 0.;
   pP->dCoslat = 0.;
   pP->dSinlon = 0.;
   pP->dCoslon = 0.;
   pP->dPTime = 0.;
   pP->dClipLevel = 0.;
   pP->cFirstMotion = '?';
   pP->dMbAmpGM = 0.;
   pP->lMbPer = 0;
   pP->dMbMag = 0.;
   pP->dMbTime = 0.;
   pP->iMbClip = 0;
   pP->dMlAmpGM = 0.;
   pP->lMlPer = 0;
   pP->dMlTime = 0.;
   pP->dMlMag = 0.;
   pP->iMlClip = 0;
   pP->dMSAmpGM = 0.;
   pP->lMSPer = 0;
   pP->dMSTime = 0.;
   pP->dMSMag = 0.;
   pP->iMSClip = 0;
   pP->dMwpIntDisp = 0.;
   pP->dMwpTime = 0.;
   pP->dMwpMag = 0.;
   pP->dRes = 0.;
   pP->dDelta = 0.;
   pP->dAz = 0.;
   pP->dFracDelta = 0.;
   pP->dSnooze = 0.;
   pP->dCooze = 0.;
   pP->iUseMe = 1;
   pP->iHypoID = -1;
}

     /**************************************************************
      *                         PPickStruct()                      *
      *                                                            *
      * Fill in PPICK structure from PickTWC message.              *
      *                                                            *
      * Arguments:                                                 *
      *  PIn         PickTWC message from ring                     *
      *  PPick       P-pick data structure                         *
      *  TypePickTWC Earthworm message type expected               *
      *                                                            *
      * Return - 0 if OK, -1 if wrong message type                 *
      **************************************************************/

int PPickStruct( char *PIn, PPICK *PPick, unsigned char TypePickTWC )
{
   int      iMessageType, iModId, iInst;  /* Incoming logo */

/* Break up incoming message
   *************************/
   sscanf( PIn,    "%ld %ld %ld %s %s %s %ld %ld %lf %c %s %lf %ld %lf %lf %ld "
                   "%lf %lf %ld %lf %lE %lf %ld",
           &iMessageType, &iModId, &iInst, PPick->szStation, PPick->szChannel,
		   PPick->szNetID, &PPick->lPickIndex, &PPick->iUseMe, &PPick->dPTime,
		   &PPick->cFirstMotion, PPick->szPhase,
		   &PPick->dMbAmpGM, &PPick->lMbPer, &PPick->dMbTime,
		   &PPick->dMlAmpGM, &PPick->lMlPer, &PPick->dMlTime,
		   &PPick->dMSAmpGM, &PPick->lMSPer, &PPick->dMSTime,
		   &PPick->dMwpIntDisp, &PPick->dMwpTime, &PPick->iHypoID );

   if ( iMessageType == TypePickTWC )
      return 0;
   else
   {
      logit( "te", "Incoming message type %ld; must be PickTWC\n",
	               iMessageType );
      return -1;
   }
}

     /**************************************************************
      *                          PPickMatch()                      *
      *                                                            *
      * Fill in PPICK structure with data from StaDataFile.        *
      *                                                            *
      * Arguments:                                                 *
      *  PPick       P-pick data structure                         *
      *  StaArray    Station data array                            *
      *  Nsta        Number of stations in array                   *
      *                                                            *
      * Return - 0 if OK, -1 if no match                           *
      **************************************************************/

int PPickMatch( PPICK *PPick, STATION *StaArray, int Nsta )
{
   int    i;

/* Search StaArray for SCN which has just arrived, then copy data
   **************************************************************/
   for ( i=0; i<Nsta; i++ )
      if ( !strcmp( PPick->szStation, StaArray[i].szStation ) &&
           !strcmp( PPick->szChannel, StaArray[i].szChannel ) &&
           !strcmp( PPick->szNetID,   StaArray[i].szNetID ) )
      {
         PPick->dLat             = StaArray[i].dLat;
         PPick->dLon             = StaArray[i].dLon;
         GeoCent( (LATLON *) PPick );
         GetLatLonTrig( (LATLON *) PPick );
         PPick->dSens            = StaArray[i].dSens;
         PPick->dClipLevel       = StaArray[i].dClipLevel;
         PPick->dElevation       = StaArray[i].dElevation;
         PPick->dGainCalibration = StaArray[i].dGainCalibration;
         PPick->iStationType     = StaArray[i].iStationType;
         return 0;
      }
   logit( "te", "No match in StaDataFile for %s %s %s\n",
          PPick->szStation, PPick->szChannel, PPick->szNetID );
   return -1;
}

     /**************************************************************
      *                       ShortCopyPBuf()                      *
      *                                                            *
      * Update magnitudes in main PBuffer.                         *
      *                                                            *
      * Arguments:                                                 *
      *  PIn         Input PPICK structure                         *
      *  POut        Output PPICK structure                        *
      *                                                            *
      **************************************************************/

void ShortCopyPBuf( PPICK *PIn, PPICK *POut )
{
   POut->dMbAmpGM         = PIn->dMbAmpGM;
   POut->lMbPer           = PIn->lMbPer;
   POut->dMbTime          = PIn->dMbTime;
   POut->dMbMag           = PIn->dMbMag;
   POut->iMbClip          = PIn->iMbClip;
   POut->dMlAmpGM         = PIn->dMlAmpGM;
   POut->lMlPer           = PIn->lMlPer;
   POut->dMlTime          = PIn->dMlTime;
   POut->dMlMag           = PIn->dMlMag;
   POut->iMlClip          = PIn->iMlClip;
   POut->dMSAmpGM         = PIn->dMSAmpGM;
   POut->lMSPer           = PIn->lMSPer;
   POut->dMSTime          = PIn->dMSTime;
   POut->dMSMag           = PIn->dMSMag;
   POut->iMSClip          = PIn->iMSClip;
   POut->dMwpIntDisp      = PIn->dMwpIntDisp;
   POut->dMwpTime         = PIn->dMwpTime;
   POut->dMwpMag          = PIn->dMwpMag;
   POut->dRes             = PIn->dRes;
   POut->dDelta           = PIn->dDelta;
   POut->dAz              = PIn->dAz;
   POut->dFracDelta       = PIn->dFracDelta;
   POut->dSnooze          = PIn->dSnooze;
   POut->dCooze           = PIn->dCooze;
}

