/****************************************************************************
EMIASP91.C

These functions were taken from the NEIC ftp site in FORTRAN and converted
to C at WC&ATWC.  The variables were left the same for the most part.

They are part of the package of programs which compute seismic travel times
and travel time tables given a modern model of the earth's spherically 
symetric shells. Many phases can be computed given the model.

This set of functions contains routines used by REMODL when computing tables.

        Contributors:       NEIC/ Converted to C by Whitmore
        OS:                 Windows NT v4.0
        Compiler:           Microsoft Visual C++ v6.0
	Link Info:	    Link with any program needing these functions,
	                    include ...emiasp91.obj in the link,
			    or, include this file in the source program.
        Compile Info:       See iasutils.mak
---------------------------------------------------------------------------*/
#include <windows.h>
#include <winuser.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "iasplib.h"
typedef BOOL bool;

/*       set up information on earth model (specified by emiask)
         set dimension of cpr,rd  equal to number of radial
         discontinuities in model                          */
void emdlv (double r, double *vp, double *vs)
{
static	double	rn=1.5696123e-4;
static	double	vn=6.8501006;
double	rho;

emiask (rn*r, &rho, vp, vs);
*vp = vn * *vp;
*vs = vn * *vs;
return;
}

void emdld (int *nn, double cpr[], PSZ pszName)
{
int	i;
static	char	szModnam[20] = "iasp91";
static	double	rd[11] = {1217.1,3482.0,3631.,5611.,5711.,5961.,6161.,
    		  6251.,6336.,6351.,6371.};
static	int	np=11;

*nn = np;
for (i=0; i<np; i++) cpr[i] = rd[i];
strcpy (pszName, szModnam);			     
return;
}

/*
 $$$$$ calls no other routine $$$$$

   Emiask returns model parameters for the IASPEI working model 
   (September 1990.1).  
   Given non-dimensionalized radius x0, emiasp returns
   non-dimensionalized density, ro, compressional velocity, vp, and
   shear velocity, vs.  Non-dimensionalization is according to the
   scheme of Gilbert in program EOS:  x0 by a (the radius of the
   Earth), ro by robar (the mean density of the Earth), and velocity
   by a*sqrt(pi*G*robar) (where G is the universal gravitational
   constant.
*/
void emiask (double x0, double *ro, double *vp, double *vs)
{
double	r[14] = {0.      ,1217.1  ,3482.0  ,3631.  ,5611.   ,5711.   ,
                 5961.   ,6161.   ,6251.   ,6336.   ,6351.    ,6371.    ,
                 6371.   ,6371.};
double	d[4][13] = {13.01219,12.58416, 6.8143 , 6.8143 , 6.8143 ,11.11978,
         7.15855, 7.15855, 7.15855,  2.92  , 2.72   , 0., 0.,
         0.     ,-1.69929,-1.66273,-1.66273,-1.66273,-7.87054,
         -3.85999,-3.85999,-3.85999, 0., 0., 0., 0.,
         -8.45292,-1.94128,-1.18531,-1.18531,-1.18531,8*0.,
         0.     ,-7.11215, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.};
double	p[4][13] = {11.24094,10.03904,14.49470,25.1486 ,25.969838,29.38896,
         30.78765,25.41389, 8.785412, 6.5   , 5.8   , 0., 0., 
         0.   , 3.75665, -1.47089,-41.1538, -16.934118,-21.40656,
         -23.25415,-17.69722,-0.7495294, 0., 0., 0., 0.,
         -4.09689,-13.67046, 0.0  ,51.9932, 0., 0., 0., 0., 0., 0., 0., 0., 0.,
         0.     , 0.      , 0.     ,-26.6083,0., 0., 0., 0., 0., 0., 0., 0., 0.};
double	s[4][13] = {3.56454, 0.      , 8.16616,12.9303 ,20.768902,17.70732,
         15.24213,5.750203, 6.706232, 3.75   , 3.36   , 0., 0., 
         0.     , 0.      ,-1.58206,-21.2590,-16.531471,-13.50652,
         -11.08553,-1.274202,-2.248585, 0., 0., 0., 0.,
         -3.45241, 0.    , 0.0    ,27.8988 , 0., 0., 0., 0., 0., 0., 0., 0., 0.,
         0.     , 0.    , 0.     ,-14.1080, 0., 0., 0., 0., 0., 0., 0., 0., 0.};
int	i;
double	rn = 0.18125793;
double	vn = 0.14598326;
double	x;
double	x1;
double	xn = 6371.;	// Earth radius in km

i = 0;
x = max (x0, 0);
x1 = xn * x;
Repeat1:
if (x1 < r[i])
    {
    i--;
    goto Repeat1;
    }
Repeat2:
if (x1 > r[i+1] && i < 10)
    {
    i++;
    if (i < 10) goto Repeat2;
    }
*ro = rn*(d[0][i] + x*(d[1][i] + x*(d[2][i] + x*d[3][i])));
*vp = vn*(p[0][i] + x*(p[1][i] + x*(p[2][i] + x*p[3][i])));
*vs = vn*(s[0][i] + x*(s[1][i] + x*(s[2][i] + x*s[3][i])));
return;
}      
