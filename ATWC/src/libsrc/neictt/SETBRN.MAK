
#                    Nmake File For SETBRN - Windows NT version

NODEBUG=1

!include <ntwin32.mak>

all:SETBRN.exe

SETBRN.obj: SETBRN.c SETBRN.mak
#    $(cc) $(cflags) $(cvarsmt) SETBRN.c
    cl /c /Op SETBRN.c

SETBRN.exe: SETBRN.obj SETBRN.mak
    LINK $(conlflags) $(conlibsmt) SETBRN.obj taulib.obj -out:SETBRN.exe
