
 /************************************************************************
  * LOCQUAKE.C                                                           *
  *                                                                      *
  * These functions locate earthquakes using the technique used at the   *
  * WC&ATWC for many years (since the late 1970's).  There are two steps *
  * to locating an earthquake with these functions.  First, an initial   *
  * guess at the location is arrived at through InitialLocator.  This    *
  * function returns either: the location of the first station which     *
  * picked up the earthquake (for local or regional quakes), a user input*
  * location, or a location computed from 4 of the total number of sites *
  * which recorded the quake.  The last option scribes 4 tripartites of  *
  * stations within the 4 station array.  For each tripartite, a location*
  * is computed based on the arrival azimuth (from P-time differences)   *
  * and the distance (from incident angles).  This approach was written  *
  * by Whitmore in 1987.  The initial location is then passed to the     *
  * QuakeSolve function which determines a more exact location by using  *
  * all stations and minimizing the residuals (matrix solving).  This    *
  * function was set up by Sokolowski and others in the late 1970's/early*
  * 1980's. The HYPO structure is filled up by the functions.  IASPEI91  *
  * travel time tables are used tables can be used throughout.           *
  *                                                                      *
  * A function is also included which tests the residuals of the location*
  * and relocates if necessary by throwing out different combinations of *
  * stations. This is called FindBadPs.                                  *
  *                                                                      *
  * Made into earthworm module 2/2001.                                   *
  *                                                                      *
  ************************************************************************/
  
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include "earlybirdlib.h"
#include "iaspei91.h"          /* Tau/P travel times (from iaspei91 model) */

      /******************************************************************
       *                         FindBadPs()                            *
       *                                                                *
       * This function is used to search for P-times not associated with*
       * the majority of P-times in the P array.  It does this by       *
       * successively solving for the hypocenter parameters; throwing   *
       * out different combinations of stations and checking what the   *
       * residuals are for the quake without those stations.  The       *
       * solution with the lowest average residual is assumed to be the *
       * correct location.  The function will pick out a maximum of 3   *
       * unassociated P-times. First it tries eliminating just 1        *
       * station.  If that doesn't work, it tries for 2, then 3 if there*
       * are enough stations in the PArray.                             *
       *                                                                *
       * January, 2002: If iUseMe=2, do not try to remove it.           *
       *                                                                *
       *  Arguments:                                                    *
       *   iPNum            Number of Ps in this structure              *
       *   iArea            Starting location for solution              *
       *                    0 = Compute a starting location             *
       *                    1 = Location is station with lowest P-time  *
       *                    2 = User has specified a starting location  *
       *                    3 = The computer will decide between 0 and 1*
       *   P                One of the P Buffer structures              *
       *   pHypo            Computed hypocentral parameters             *
       *   dLatUser         Latitude specified by user (geographic)     *
       *   dLonUser         Longitude specified by user                 *
       *   iMinPs           Minimum Ps needed for soln                  *
       *   pEqDep           Array of depth data structures              *
       *                                                                *
       ******************************************************************/
	   
void FindBadPs( int iPNum, int iArea, PPICK P[], HYPO *pHypo, double dLatUser,
                double dLonUser, int iMinPs, EQDEPTHDATA pEqDep[] ) 
{
   double  dMinResid;     /* Min residual for the suite of locs. just done */
   int     i, j, k, iTemp;
   int     iCnt;          /* # of usable stations when function entered */
   int     iLocator;      /* Auto-initial locator type used */
   int     iMinIndex1, iMinIndex2, iMinIndex3;  /* Indices used for dMinResid */
   
   if ( MAX_TO_KO < 1 ) return;

/* Force the depth control to fixed at 40km */
   pHypo->dDepth = DEPTHKM;         /* Set for 40 kilometers */
   pHypo->iDepthControl = 3;        /* Fixed at 40km */
   iLocator = 1;
   iMinIndex1 = 0;
   iMinIndex2 = 0;
   iMinIndex3 = 0;

/* How many stations do we have to locate with */
   iCnt = 0;
   for ( i=0; i<iPNum; i++ )
      if ( P[i].iUseMe > 0 && P[i].dPTime > 0. ) iCnt++;

/* Try knocking out just one station */
   dMinResid = 1.e20;                     /* Any big number will do */
   for ( i=0; i<iPNum; i++ )
      if ( P[i].iUseMe == 1 && P[i].dPTime > 0. )
      {
         P[i].iUseMe = 0;
         iTemp = 1;
         InitHypo( pHypo );		
         InitialLocator( iPNum, iArea, iTemp, P, pHypo, dLatUser, dLonUser );
         QuakeSolveIasp( iPNum, P, pHypo, pEqDep, 1 );
         IsItGoodSoln( iPNum, P, pHypo, iMinPs );
         if ( pHypo->iGoodSoln != 3 && iArea == 3 )
         {           /* If not, re-locate using tripartite initial locator */
            iTemp = 2;
            InitHypo( pHypo );		
            InitialLocator( iPNum, iArea, iTemp, P, pHypo, dLatUser, dLonUser );
            QuakeSolveIasp( iPNum, P, pHypo, pEqDep, 1 );
         }
         if ( pHypo->dAvgRes < dMinResid ) 
         {           /* Compare residual to minimum so far */
            dMinResid = pHypo->dAvgRes;
            iMinIndex1 = i;
            iLocator = iTemp;
         }
         P[i].iUseMe = 1;
      }
      
/* Fill HYPO structure with hypo params from sol'n with lowest res. */
   P[iMinIndex1].iUseMe = 0;
   InitHypo( pHypo );		
   InitialLocator( iPNum, iArea, iLocator, P, pHypo, dLatUser, dLonUser );
   QuakeSolveIasp( iPNum, P, pHypo, pEqDep, 1 );

/* If 6 P-times or less available, don't try to knock out 2 stations */
   if ( iCnt <= 6 ) return;

/* If the Residuals are reasonable, return */
   IsItGoodSoln( iPNum, P, pHypo, iMinPs );
   if ( pHypo->iGoodSoln == 3 ) return;
   P[iMinIndex1].iUseMe = 1;
   
/* It wasn't a great solution, so try knocking out 2 stations at a time */
   if ( MAX_TO_KO < 2 ) return;
   dMinResid = 1.e20;                     /* Any big number will do */
   for ( i=0; i<iPNum-1; i++ )
      if ( P[i].iUseMe == 1 && P[i].dPTime > 0. )
         for ( j=i+1; j<iPNum; j++ )
    	    if ( P[j].iUseMe == 1 && P[j].dPTime > 0. )
            {
               P[i].iUseMe = 0;          /* Turn off two stations */
               P[j].iUseMe = 0;
               iTemp = 1;
               InitHypo( pHypo );		
               InitialLocator( iPNum, iArea, iTemp, P, pHypo, dLatUser,
                               dLonUser );
               QuakeSolveIasp( iPNum, P, pHypo, pEqDep, 1 );
               IsItGoodSoln( iPNum, P, pHypo, iMinPs );
               if ( pHypo->iGoodSoln != 3 && iArea == 3 )
               {        /* If not, re-locate using tripartite initial locator */
                  iTemp = 2;
                  InitHypo( pHypo );		
                  InitialLocator( iPNum, iArea, iTemp, P, pHypo, dLatUser,
                                  dLonUser );
                  QuakeSolveIasp( iPNum, P, pHypo, pEqDep, 1 );
               }
               if ( pHypo->dAvgRes < dMinResid ) 
               {        /* Compare residual to minimum so far */
                  dMinResid = pHypo->dAvgRes;
                  iMinIndex1 = i;
                  iMinIndex2 = j;
                  iLocator = iTemp;
               }
               P[i].iUseMe = 1;
               P[j].iUseMe = 1;
            }
	    
/* Fill HYPO structure with hypo params from sol'n with lowest res. */
   P[iMinIndex1].iUseMe = 0;
   P[iMinIndex2].iUseMe = 0;
   InitHypo( pHypo );		
   InitialLocator( iPNum, iArea, iLocator, P, pHypo, dLatUser, dLonUser );
   QuakeSolveIasp( iPNum, P, pHypo, pEqDep, 1 );
	
/* If 10 P-times or less available, don't try to knock out 3 stations
   (or if more than 20; it will take to long to compute) */
   if ( iCnt <= 10 || iCnt > 20 ) return;

/* If the Residuals are reasonable, return */
   IsItGoodSoln( iPNum, P, pHypo, iMinPs );
   if ( pHypo->iGoodSoln == 3 ) return;
   P[iMinIndex1].iUseMe = 1;
   P[iMinIndex2].iUseMe = 1;

/* It wasn't a great solution, so try knocking out 3 stations at a time */
   if ( MAX_TO_KO < 3 ) return;
   dMinResid = 1.e20;                     /* Any big number will do */
   for ( i=0; i<iPNum-2; i++ )
      if ( P[i].iUseMe == 1 && P[i].dPTime > 0. )
         for ( j=i+1; j<iPNum-1; j++ )
            if ( P[j].iUseMe == 1 && P[j].dPTime > 0. )
               for ( k=j+1; k<iPNum; k++ )
                  if ( P[k].iUseMe == 1 && P[k].dPTime > 0. )
                  {
                     P[i].iUseMe = 0;       /* Turn off three stations */
                     P[j].iUseMe = 0;
                     P[k].iUseMe = 0;
                     iTemp = 1;
                     InitHypo( pHypo );			
                     InitialLocator( iPNum, iArea, iTemp, P, pHypo, 
                                     dLatUser, dLonUser);
                     QuakeSolveIasp( iPNum, P, pHypo, pEqDep, 1 );
                     IsItGoodSoln( iPNum, P, pHypo, iMinPs );
                     if ( pHypo->iGoodSoln != 3 && iArea == 3 )
                     {  /* If not, re-locate using tripartite initial locator */
                        iTemp = 2;
                        InitHypo( pHypo );		
                        InitialLocator( iPNum, iArea, iTemp, P, pHypo, 
                                        dLatUser, dLonUser );
                        QuakeSolveIasp( iPNum, P, pHypo, pEqDep, 1 );
                     }
                     if ( pHypo->dAvgRes < dMinResid )  
                     {  /* Compare residual to minimum so far */
                        dMinResid = pHypo->dAvgRes;
                        iMinIndex1 = i;
                        iMinIndex2 = j;
                        iMinIndex3 = k;
                        iLocator = iTemp;
                     }
                     P[i].iUseMe = 1;
                     P[j].iUseMe = 1;
                     P[k].iUseMe = 1;
                  }
		  
/* Fill HYPO structure with hypo params from sol'n with lowest res. */
   P[iMinIndex1].iUseMe = 0;
   P[iMinIndex2].iUseMe = 0;
   P[iMinIndex3].iUseMe = 0;
   InitHypo( pHypo );		
   InitialLocator( iPNum, iArea, iLocator, P, pHypo, dLatUser, dLonUser );
   QuakeSolveIasp( iPNum, P, pHypo, pEqDep, 1 );
}


      /******************************************************************
       *                        FindDepth()                             *
       *                                                                *
       * Function "FindDepth" will return an index i for identifying the*
       * proper average and maximium depth for a specific latitude and  *
       * longitude in an EQDEPTHDATA structure. If the latitude and     *
       * longitude passed to this function are not contained in the     *
       * array of structures, the function returns a -1.                *
       *                                                                *
       *  Arguments:                                                    *
       *   dLatpass         Latitude to search (geo +/-)                *
       *   dLonpass         Longitude to search (geo +/-)               *
       *   pEqDep           Array of depth data structures              *
       *                                                                *
       *  Returns:                                                      *
       *   int              Index if location found, -1 otherwise       *
       *                                                                *
       ******************************************************************/
       
int FindDepth( double dLatpass, double dLonpass, EQDEPTHDATA pEqDep[] )
{
   int i, iRndLat, iRndLon;
    
/* Round double to nearest int */
   iRndLon = Round( dLonpass );
   iRndLat = Round( dLatpass );
 
   for ( i=0; i<EQSIZE; i++ ) /* Search the file for matching lat and lon */
      if ( pEqDep[i].iLat == iRndLat && pEqDep[i].iLon == iRndLon )
         return (i);
   return (-1);
}

      /******************************************************************
       *                        GetEpiAzDelta()                         *
       *                                                                *
       * This function calculates distance, azimuth, and other important*
       * location parameters for all stations used in locating an       *
       * earthquake.  Spherical trigonometric relations are used        *
       * throughout.                                                    *
       *                                                                *
       *  Arguments:                                                    *
       *   iPNum            Number of Ps in this structure              *
       *   P                One of the P Buffer structures              *
       *   pHypo            Computed hypocentral parameters             *
       *                                                                *
       ******************************************************************/
       
void GetEpiAzDelta( int iPNum, PPICK *P, HYPO *pHypo )
{
   double  dDeltaCosine, dDeltaSine;
   double  dTempDelta, dTempAz;
   int     i;

   GetLatLonTrig( (LATLON *) pHypo );
   if ( pHypo->dSinlat == 0.0 ) pHypo->dSinlat = 0.01;

   for ( i=0; i<iPNum; i++ )
   {
      dDeltaCosine = pHypo->dCoslat*P[i].dCoslat + pHypo->dSinlat*
                     P[i].dSinlat*(pHypo->dCoslon*P[i].dCoslon +
                     pHypo->dSinlon*P[i].dSinlon);
      dDeltaSine = sqrt( 1. - dDeltaCosine*dDeltaCosine );
      if ( dDeltaSine == 0.0 )   dDeltaSine = 0.01;
      if ( dDeltaCosine == 0.0 ) dDeltaCosine = 0.01;
      dTempDelta =  atan( dDeltaSine / dDeltaCosine );
      P[i].dCooze = (P[i].dCoslat - pHypo->dCoslat * dDeltaCosine) /
                    (pHypo->dSinlat * dDeltaSine);
      if ( P[i].dCooze == 0.0 ) P[i].dCooze = 0.0001;
      P[i].dSnooze = P[i].dSinlat * (pHypo->dCoslon * P[i].dSinlon -
                     pHypo->dSinlon * P[i].dCoslon) / dDeltaSine;
      dTempAz = atan( P[i].dSnooze / P[i].dCooze );
      while ( dTempAz < 0.0 )    dTempAz += PI;
      if ( P[i].dSnooze <= 0. )  dTempAz += PI;
      while ( dTempDelta < 0.0 ) dTempDelta += PI;
      dTempDelta *= DEG;
      P[i].dFracDelta = dTempDelta - floor( dTempDelta );
      P[i].dDelta = dTempDelta;
      P[i].dAz = dTempAz * DEG;
   }
}

      /******************************************************************
       *                        GetPTimes()                             *
       *                                                                *
       * This function computes expected P-wave arrival times at all    *
       * sites in the P array for the hypocenter given in pHypo.  The   *
       * times are loaded into the P array.                             *
       *                                                                *
       *  Arguments:                                                    *
       *   iPNum            Number of Ps in the P structure             *
       *   P                P Buffer structure                          *
       *   pHypo            Computed hypocentral parameters             *
       *                                                                *
       ******************************************************************/
	   
void GetPTimes( int iPNum, PPICK *P, HYPO *pHypo )
{
   AZIDELT azidelt;        /* Distance/azimuth between 2 points */
   double  dCorr;          /* Distance correction for P tables */
   int     i;
   static  int  iDLev;     /* Index of 0 deg. in desired depth level */
   int     iDepth;         /* Temp depth in km */
   
   iDepth = (int) pHypo->dDepth;
/* Get depth level to use in travel time table */
   if (iDepth >= (int) ((DEPTH_LEVELS_IASP-1)*IASP_DEPTH_INC)) 
       iDepth = (int) ((DEPTH_LEVELS_IASP-1)*IASP_DEPTH_INC);
   iDLev = (int) ( (double) iDepth /
                 (double) IASP_DEPTH_INC) * IASP_NUM_PER_DEP;

   for ( i=0; i<iPNum; i++ )
   {
      azidelt = GetDistanceAz( (LATLON *) pHypo, (LATLON *) &P[i] );
      P[i].dDelta = azidelt.dDelta;
      P[i].dAz = azidelt.dAzimuth;
      dCorr = azidelt.dDelta*(1./IASP_DIST_INC) - 
              floor( azidelt.dDelta*(1./IASP_DIST_INC) );
      P[i].dExpectedPTime = fPP[(int) (azidelt.dDelta*(1./IASP_DIST_INC))+
              iDLev] + dCorr*(fPP[(int) (azidelt.dDelta*
              (1./IASP_DIST_INC))+1+iDLev] - fPP[(int)
              (azidelt.dDelta*(1./IASP_DIST_INC))+iDLev]) + pHypo->dOriginTime;				
   }
}
	   

      /******************************************************************
       *                       InitialLocator()                         *
       *                                                                *
       * This function computes an initial epicentral location for a    *
       * group of stations with P-Times.  If the function is to compute *
       * its own starting location, four stations are selected from the *
       * group and an epicenter is computed for each of the four        *
       * inscribed tripartites.  The average lat and lon is then given  *
       * as the initial location.   For each tripartite, the intial     *
       * location is computed first by computing the azimuth from the   *
       * center of the tripartite and then computing the distance away  *
       * from the center.  The azimuth is computed from the difference  *
       * in arrival times between the stations.  The distance is        *
       * obtained from the apparent velocity of the quake which relates *
       * to the angle of incidence of the wave to the tripartite.  The  *
       * angle of incidence then relates to the distance of the quake   *
       * from the tripartite.  If the user has specified a location,    *
       * this is converted to geocentric and then returned.  If the     *
       * nearest station is used as the starting point, that station's  *
       * location is then returned.  The computed value is loaded into  *
       * the HYPO array.                                                *
       *                                                                *
       *  Arguments:                                                    *
       *   iPNum            Number of Ps in this structure              *
       *   iArea            Starting location for solution              *
       *                    0 = Compute a starting location             *
       *                    1 = Location is station with lowest P-time  *
       *                    2 = User has specified a starting location  *
       *                    3 = The computer will decide between 0 and 1*
       *   iTry             1st or 2nd try when iArea = 3               *
       *   P                One of the P Buffer structures              *
       *   pHypo            Computed hypocentral parameters             *
       *   dLatUser         Latitude specified by user (geographic)     *
       *   dLonUser         Longitude specified by user                 *
       *                                                                *
       *  Return:                                                       *
       *   -1 if problem in location; 0 otherwise                       *
       *                                                                *
       ******************************************************************/
       
void InitialLocator( int iPNum, int iArea, int iTry, PPICK *P, HYPO *pHypo,
                     double dLatUser, double dLonUser )
{
   AZIDELT azidelt[MAX_STATION_DATA];  /* Dist. to fixed center pt. for all */
   AZIDELT azidelt01, azidelt02, azidelt12;/* Azidelts within tripartite */
   AZIDELT azideltAB, azideltAC;/* Azimuth and dist. of bisectors of AB, AC */
   AZIDELT azideltB, azideltC;  /* Azimuth and dist. from corner A to B and C */
   AZIDELT azideltTemp;	        /* Temporary structure of distance/azimuth */
   double  dAng[3];             /* Interior angles of chosen tripartite */
   double  dAngIncident;        /* Incident angle of p-wave into tripartite */
   double  dAngle[3];           /* Ordered interior angles of chosen trip. */
   double  dApVel;              /* Apparent velocity over triangle leg */
   double  dAzEpiAB, dAzEpiAC;  /* Angles between epicentral az. and side az. */
   double  dAz;                 /* Calculated azimuth toward the epicenter */
   double  dCorrB, dCorrC;      /* Interpolation corrections for max p-times */
   double  dDelta;              /* Distance to Epicenter from tripartite */
   double  dHyp;                /* Hypotenuse of rt. trngl created with dx,dy */
   double  dTdAB, dTdAC;        /* Time difference between corners A,B & A,C */
   double  dTdABm, dTdACm;      /* Max. time diff. between corners A,B & A,C */
   double  dTemp1, dTemp2, dTempLon;
   double  dx, dy;              /* Legs of trngl pointing direction to quake */
   int     i1, i2, i3, i, j, k, iCnt, l, n, ia, ib, ic; 
         /* This array lists the incident angle (*10) expected
            for each distance (the first entry is 0 degrees,
            the last is 100 degrees). NOTE: the incident angle is
            measured from the normal to the earth's surface. */
   int     iDelt[] = {520,506,506,502,502,492,496,487,487,483,476,469,
            460,451,447,442,434,425,395,365,346,328,322,316,310,305,
            300,295,291,287,287,287,282,277,275,273,271,270,268,267,
            265,263,261,260,258,256,254,253,249,246,242,239,237,236,
            233,229,226,222,220,219,215,212,209,206,204,202,201,199,
            196,193,189,186,184,182,181,179,176,173,171,170,166,163,
            161,160,157,154,152,150,148,147,145,144,142,141,141,141,
            141,141,142,144,142,141,139,138};
   static  int  iDLev;             /* Index of 0 deg. in desired depth level */
   int     iIndices[4] = {0, 0, 0, 0}; /* Index of 4 stas closest to center */
   LATLON  llAKArray;           /* Point used to compare station locations */
   LATLON  llCen1;              /* Center of tripartite */
   LATLON  llCen2;              /* Center of tripartite leg used for distance */
   LATLON  llEpi;               /* Lat/lon of epicenter estimation */
   
/* Initialize some things */
   pHypo->iNumPs = 0;
   for ( i=0; i<iPNum; i++ )    /* Find number of stations to use */
      if ( P[i].iUseMe > 0 && P[i].dPTime > 0. ) pHypo->iNumPs++;
   dTempLon = PI / 2.;
   pHypo->dLat = 0.;
   pHypo->dLon = 0.;
   llAKArray.dLat = 0.47;       /* Near center of south-central Alaska array */
   llAKArray.dLon = 3.65;

/* Get depth level to use in travel time table */
   iDLev = (int) ((double) (DEPTHKM+0.01) / IASP_DEPTH_INC)*IASP_NUM_PER_DEP;
	    
/* If user is to specify location, take what was specified and convert */
   if ( iArea == 2 )  
   {
      pHypo->dLat = dLatUser;
      pHypo->dLon = dLonUser;
      GeoCent ((LATLON *) pHypo);
   }
   
/* Set initial location to station with lowest arrival time in certain cases */
   else if ( iArea == 1 || (iArea == 3 && iTry == 1) )
   {
      dTemp1 = 1.e20;
      l = 0;
      for ( i=0; i<iPNum; i++ )
         if ( P[i].dPTime > 0. && P[i].iUseMe > 0 && P[i].dPTime < dTemp1 )
         {
            dTemp1 = P[i].dPTime;
            l = i;
         }
      pHypo->dLat = P[l].dLat + 0.02;
      pHypo->dLon = P[l].dLon + 0.02;
   }
   
/* Compute initial location */   
   else if ( iArea == 0 || (iArea == 3 && iTry > 1) )
   {
/* Get distance from all stations to defined center point (this could be 
   improved by more intelligently pick the quadrapartite; such as by angles
   and distance between the stations !!!) */
      for ( i=0; i<iPNum; i++ )
         azidelt[i] = GetDistanceAz( (LATLON *) &P[i], &llAKArray );
      
/*  Choose four stations closest to defined center point */
      k = 0;
      for ( i=0; i<iPNum; i++ )
         if ( P[i].iUseMe > 0 && P[i].dPTime > 0. )
         {
            iCnt = 0;
            for ( j=0; j<iPNum; j++ )
               if ( azidelt[i].dDelta < azidelt[j].dDelta &&
                    P[j].iUseMe > 0 && P[j].dPTime > 0. ) iCnt++;
            if ( iCnt >= (pHypo->iNumPs-4) )
            {
               iIndices[k] = i;
               k++;
            }
         }
      if ( k < 3 )                 /* Not enough stations to do anything with */
      {
         pHypo->dLat = 63.0;       /* Use a spot in south-central Alaska */
         pHypo->dLon = -152.0;
         GeoCent( (LATLON *) pHypo );
         GetLatLonTrig( (LATLON *) pHypo );
         return;
      }
   
/* Set up 4 station tripartite loop */
      for ( i3=0; i3<4; i3++ )
      {
         j = iIndices[i3];
         i1 = i3 + 1;
         if ( i3 == 3 ) i1 = 0;
         k = iIndices[i1];
         i2 = i3 + 2;
         if ( i3 == 2 )      i2 = 0;
         else if ( i3 == 3 ) i2 = 1;
         l = iIndices[i2];
/* Compute azimuths and distances between stations */
         azidelt01 = GetDistanceAz( (LATLON *) &P[j], (LATLON *) &P[k] );
         azidelt02 = GetDistanceAz( (LATLON *) &P[j], (LATLON *) &P[l] );
         azidelt12 = GetDistanceAz( (LATLON *) &P[k], (LATLON *) &P[l] );
/* Compute interior angles of tripartite (use half-angle formulas from
   spherical trig - !!! This is changed from original; was planar) */
         dTemp1 = 0.5*(azidelt01.dDelta + azidelt02.dDelta + azidelt12.dDelta);
         dTemp2 = sqrt( (sin ((dTemp1 - azidelt01.dDelta)*RAD) *
                         sin ((dTemp1 - azidelt02.dDelta)*RAD) *
                         sin ((dTemp1 - azidelt12.dDelta)*RAD)) / 
                         sin (dTemp1*RAD) );
         dAng[0] = 2.*atan( dTemp2/sin( (dTemp1-azidelt01.dDelta)*RAD ) )*DEG;
         dAng[1] = 2.*atan( dTemp2/sin( (dTemp1-azidelt02.dDelta)*RAD ) )*DEG;
         dAng[2] = 2.*atan( dTemp2/sin( (dTemp1-azidelt12.dDelta)*RAD ) )*DEG;
/* Compute geocentric center of tripartite */
         llCen1.dLat = (min( P[j].dLat, min (P[k].dLat, P[l].dLat) ) + 
                        max( P[j].dLat, max (P[k].dLat, P[l].dLat) )) / 2.0;
         llCen1.dLon = (min( P[j].dLon, min (P[k].dLon, P[l].dLon) ) + 
                        max( P[j].dLon, max (P[k].dLon, P[l].dLon) )) / 2.0;
/* Rename corners A,B,C (A closest to 60 deg, B so azimuth AB < az. AC */
         for ( i=0; i<3; i++ ) dAngle[i] = fabs( dAng[i] - 60.0 );
         dTemp1 = 10000;
         for ( i=0; i<3; i++ )
            if ( dAngle[i] < dTemp1 )     /* Get closest to 60 degrees (ia) */
            {
               dTemp1 = dAngle[i];
               if ( i == 0 ) { ia=j; ib=k; ic=l; }
               if ( i == 1 ) { ia=k; ib=l; ic=j; }
               if ( i == 2 ) { ia=l; ib=j; ic=k; }
            }
         azideltB = GetDistanceAz( (LATLON *) &P[ia], (LATLON *) &P[ib] );
         azideltC = GetDistanceAz( (LATLON *) &P[ia], (LATLON *) &P[ic] );
/* Reverse the indices if necessary */
         if (azideltC.dAzimuth <= azideltB.dAzimuth)
         {
            i = ib;
            ib = ic;
            ic = i;                    /* Recompute with new indices */
            azideltB = GetDistanceAz ((LATLON *) &P[ia], (LATLON *) &P[ib]);
            azideltC = GetDistanceAz ((LATLON *) &P[ia], (LATLON *) &P[ic]);
         }
/* Compute azimuth of bisectors */
         if ( (azideltC.dAzimuth - azideltB.dAzimuth) > 180. )
         {                             
            azideltAB.dAzimuth = azideltB.dAzimuth + 90.;
            azideltAC.dAzimuth = azideltC.dAzimuth - 90.;
         }
         else
         {
            azideltAB.dAzimuth = azideltB.dAzimuth - 90.;
            azideltAC.dAzimuth = azideltC.dAzimuth + 90.;
         }
         if ( azideltAB.dAzimuth < 0.0 ) azideltAB.dAzimuth += 360.;
         if ( azideltAC.dAzimuth < 0.0 ) azideltAC.dAzimuth += 360.;
/* Compute time difference between stations */
         dTdAB = P[ib].dPTime - P[ia].dPTime;
         dTdAC = P[ic].dPTime - P[ia].dPTime;
         if ( dTdAB == 0.0 ) dTdAB = 0.01;        /* Prevent divide by zero */
         if ( dTdAC == 0.0 ) dTdAC = 0.01;
/* Maximum time difference between stations from iaspei91 tables */
         i = (int) ((azideltB.dDelta)*(1./IASP_DIST_INC));
         dCorrB = azideltB.dDelta*(1./IASP_DIST_INC) - 
                  floor( azideltB.dDelta*(1./IASP_DIST_INC) );
         n = (int) ((azideltC.dDelta)*(1./IASP_DIST_INC)); 
         dCorrC = azideltC.dDelta*(1./IASP_DIST_INC) - 
                  floor( azideltC.dDelta*(1./IASP_DIST_INC) );
/* Use DEPTHKM in table */
         dTdABm = fPP[i+iDLev] + dCorrB*(fPP[(i+1)+iDLev] - fPP[i+iDLev]);
         dTdACm = fPP[n+iDLev] + dCorrC*(fPP[(n+1)+iDLev] - fPP[n+iDLev]);
/* Compute azimuth from center of tripartite to epicenter (trig method) */
         if ( dTdAB/dTdABm < 0.0 ) azideltAC.dAzimuth += 180.;
         if ( dTdAC/dTdACm < 0.0 ) azideltAB.dAzimuth += 180.;
         if ( azideltAB.dAzimuth >= 360. ) azideltAB.dAzimuth -= 360.;
         if ( azideltAC.dAzimuth >= 360. ) azideltAC.dAzimuth -= 360.;
         dx = fabs( dTdAB/dTdABm ) * sin( azideltAC.dAzimuth * RAD ) +
              fabs( dTdAC/dTdACm ) * sin( azideltAB.dAzimuth * RAD );
         dy = fabs( dTdAB/dTdABm ) * cos( azideltAC.dAzimuth * RAD ) +
              fabs( dTdAC/dTdACm ) * cos( azideltAB.dAzimuth * RAD );
         dHyp = sqrt( dx*dx + dy*dy );
         if ( dx > 0.0 )
         {
            if ( dy > 0.0 ) dAz = acos( dy/dHyp ) * DEG;
            else            dAz = acos( dx/dHyp ) * DEG + 90.;
         }
         else
         {
            if ( dy <= 0.0 ) dAz = acos( fabs( dy )/dHyp) * DEG + 180.;
            else             dAz = asin( dy/dHyp ) * DEG + 270.;
         }
/* Compute distance from center of AB and AC to epicenter. Assume surface
   velocity of 6 km/sec. */
         dAzEpiAB = fabs( dAz - azideltB.dAzimuth );
         dAzEpiAC = fabs( dAz - azideltC.dAzimuth );
         if ( dAzEpiAB > 180. ) dAzEpiAB -= 180.;
         if ( dAzEpiAC > 180. ) dAzEpiAC -= 180.;
/* Use the leg which is closest to the azimuth on which the epicenter lies.
   This will give more accurate results than looking at a leg which is
   perpendicular to the azimuth. */
         if ( fabs( cos( dAzEpiAB*RAD ) ) >= fabs( cos( dAzEpiAC*RAD ) ) )
         {
            dApVel = azideltB.dDelta*111./fabs( dTdAB );
            dTemp2 = fabs( cos( dAzEpiAB*RAD ) );
            if ( dTemp2 == 0.0 ) dTemp2 = 0.05; /* Prevent divide by 0 */
            dTemp1 = 6. / (dApVel*dTemp2);
            if ( dTemp1 >= 1.0 ) dTemp1 = 0.99; /* Prevent asin error */
            dAngIncident = asin( dTemp1 ) * DEG;
            llCen2.dLat = (P[ia].dLat+P[ib].dLat) / 2.0;
            llCen2.dLon = (P[ia].dLon+P[ib].dLon) / 2.0;
         }		 
         else
         {
            dApVel = azideltC.dDelta*111./fabs( dTdAC );
            dTemp2 = fabs( cos( dAzEpiAC*RAD ) );
            if ( dTemp2 == 0.0 ) dTemp2 = 0.05; /* Prevent divide by 0 */
            dTemp1 = 6. / (dApVel*dTemp2);
            if ( dTemp1 >= 1.0 ) dTemp1 = 0.99; /* Prevent asin error */
            dAngIncident = asin( dTemp1 ) * DEG;
            llCen2.dLat = (P[ia].dLat+P[ic].dLat) / 2.0;
            llCen2.dLon = (P[ia].dLon+P[ic].dLon) / 2.0;
         }
/* Pull the proper distance from the incident angle vs. distance table */
         for ( i=0; i<100; i++ )
            if ( (iDelt[i]*.1) < dAngIncident )
            {
               dDelta = (double) (i+1);
               break;
            }
/* Get distance and azimuth from center of tripartite leg to center of array
   and adjust computed distance */
         if ( llCen2.dLat != llCen1.dLat || llCen1.dLon != llCen2.dLon )
         {
            azideltTemp = GetDistanceAz( &llCen1, &llCen2 );
            if ( dDelta >= azideltTemp.dDelta )
               dDelta += azideltTemp.dDelta * cos( fabs(
	                 dAz-azideltTemp.dAzimuth ) * RAD );
         }
/* Compute geocentric lat/lon of inital guess */
         azideltTemp.dDelta = dDelta * RAD;
         azideltTemp.dAzimuth = dAz * RAD;
         llEpi = PointToEpi( &llCen1, &azideltTemp );
         if ( pHypo->iNumPs > 3 )  /* Avg. loc over 4 tries */
         {                         /* Take 1/4 of each try and add 'em up */
            pHypo->dLat += llEpi.dLat * 0.25;
            if ( dTempLon < 0.8 && llEpi.dLon > 5.2 )/* Adj. near 0 deg. lon. */
               llEpi.dLon -= TWOPI; 
            else if ( dTempLon > 5.2 && llEpi.dLon < 0.8 ) 
               llEpi.dLon += TWOPI;
            pHypo->dLon += .25 * llEpi.dLon;
            dTempLon = pHypo->dLon * 4.0 / (double) (i3+1);
         }
         else                      /* This is the starting location */
         {
            pHypo->dLat = llEpi.dLat;
            pHypo->dLon = llEpi.dLon;
            break;                 /* Get out of loop and return location */
         }
      }
      while ( pHypo->dLon >= TWOPI ) pHypo->dLon -= TWOPI; /* Just in case */
      while ( pHypo->dLon < 0.0 )    pHypo->dLon += TWOPI;
      while ( pHypo->dLat >= PI )    pHypo->dLat -= PI;		
      while ( pHypo->dLon < 0.0 )    pHypo->dLon += PI;
   }
   else                      /* Should never happen */
   {
      pHypo->dLat = 63.0;    /* Use a spot in south-central Alaska */
      pHypo->dLon = -152.0;
      GeoCent( (LATLON *) pHypo );
   }
   GetLatLonTrig( (LATLON *) pHypo );
}

      /******************************************************************
       *                         IsItGoodSoln()                         *
       *                                                                *
       * This function determines (by the residuals) whether or not a   *
       * solution has converged or not.  It fills a variable in the     *
       * HYPO structure (iGoodSoln). The possible values of             *
       * iGoodSoln are: 0 - poor fit; 1 - ok fit; 2 - good fit; 3 -     *
       * very good fit.  The factors which determine the fit quality are*       
       * arbitrarily chosen.                                            *
       *                                                                *
       *  December, 2004: Added distance/azimuth control checks         *
       *                                                                *
       *  Arguments:                                                    *
       *   iPNum            Number of Ps in this structure              *
       *   P                One of the P Buffer structures              *
       *   pHypo            Computed hypocentral parameters             *
       *   iMinPs           Minimum # of Ps allowed for location        *
       *                                                                *
       ******************************************************************/
       
void IsItGoodSoln( int iPNum, PPICK *P, HYPO *pHypo, int iMinPs )
{
   int     i;
   int     iAnyOut;	    /* Flag indicating if any stations res. > 3 */
   int     iAnyWayOut;  /* Flag indicating if any stations res. > 6 */
   int     iPUsed;      /* Number of Ps used in sol'n */  

/* First, check each residual individually */
   iAnyOut    = 0;
   iAnyWayOut = 0;	
   iPUsed = 0;
   for ( i=0; i<iPNum; i++ )
      if ( P[i].iUseMe > 0 && P[i].dPTime > 0. )
      {
         iPUsed++;
         if ( fabs( P[i].dRes ) > 10. ) iAnyWayOut++;
         if ( fabs( P[i].dRes ) > 5. ) iAnyOut++;
      }

/* Check the residuals, # stations, nearest distance, azimuthal
   control, and define a quality */
   if ( (iPUsed >  iMinPs+1 && pHypo->dAvgRes < 1.5 && iPUsed/10 >= iAnyOut) ||
       ((iPUsed == iMinPs+1 || iPUsed == iMinPs) && pHypo->dAvgRes < 1.5 &&
	     iPUsed/10 >= iAnyOut &&
        (pHypo->dNearestDist < 10. || pHypo->iAzm > 180.)) ) 
      pHypo->iGoodSoln = 3;			/* Very good fit */
   else if ( (iPUsed > iMinPs+1 && pHypo->dAvgRes < 2.5 &&
         iPUsed/10 >= iAnyWayOut) ||
       ((iPUsed == iMinPs+1 || iPUsed == iMinPs) && pHypo->dAvgRes < 2.5 &&
	     iPUsed/10 >= iAnyWayOut &&
        (pHypo->dNearestDist < 10. || pHypo->iAzm > 180.)) ) 
      pHypo->iGoodSoln = 2;			/* Good fit */
   else if ( pHypo->dAvgRes < 4. ) 
      pHypo->iGoodSoln = 1;			/* So-so fit */
   else
      pHypo->iGoodSoln = 0;			/* Poor fit */
}

      /******************************************************************
       *                         IsItSameQuake()                        *
       *                                                                *
       * This function compares two hypocenters and determines if they  *
       * are likely the same quake.                                     *
       *                                                                *
       *  Arguments:                                                    *
       *   pHypo1           One of the hypocenters                      *
       *   pHypo2           The other hypocenter                        *
       *                                                                *
       *  Returns:          1 if a match; 0 if not                      *
       *                                                                *
       ******************************************************************/
       
int IsItSameQuake( HYPO *pHypo1, HYPO *pHypo2 )
{
   AZIDELT azidelt;         /* Distance/azimuth between two hypocenters */

/* Get distance between two hypocenters in degrees */
   azidelt = GetDistanceAz( (LATLON *) pHypo1, (LATLON *) pHypo2 );
                       
/* Check origin time and location match */
   if ( (pHypo1->dOriginTime > pHypo2->dOriginTime-60.  &&
         pHypo1->dOriginTime < pHypo2->dOriginTime+60.) &&
         azidelt.dDelta < 10. ) return 1;
   return 0;
}

      /******************************************************************
       *                       QuakeAzimuthSort()                       *
       *                                                                *
       * This function sorts the quake azimuthal values.  This makes it *
       * easier to compute the azimuthal coverage of stations about the *
       * epicenter.                                                     *
       *                                                                *
       *  Arguments:                                                    *
       *   iPNum            Number of Ps in this structure              *
       *   P                One of the P Buffer structures              *
       *                                                                *
       *  Return:                                                       *
       *   int - azimuthal coverage about epicenter                     *
       *                                                                *
       ******************************************************************/
       
int QuakeAzimuthSort( int iPNum, PPICK *P )
{
   double  dAz[MAX_STATION_DATA];  /* Azimuths of stations used in soln */
   double  dAzGap;
   double  dTemp, dTempAz;
   int     i, ii, j;
   int     iGoodPicks;             /* Number of stations used in loc. */

/* Put good azimuths into array */
   ii = 0;
   for ( i=0; i<iPNum; i++ )
      if ( P[i].iUseMe > 0 && P[i].dPTime > 0. )
      {
         dAz[ii] = P[i].dAz;
         ii++;
      }
   iGoodPicks = ii;

/* Sort the array */
   for ( i=0; i<iGoodPicks-1; i++ )
   {
      ii = i+1;
      for ( j=ii; j<iGoodPicks; j++ )
         if ( dAz[i] > dAz[j] )
         {
            dTemp = dAz[i];
            dAz[i] = dAz[j];
            dAz[j] = dTemp;
         }
   }
   
/* Compute coverage from sorted array */
   dAzGap = 0.0;
   ii = 0;
   for ( i=0; i<iGoodPicks-1; i++ )
   {
      dTempAz = dAz[i+1] - dAz[i];
      if ( dTempAz > dAzGap ) dAzGap = dTempAz;
   }
   
/* Return the coverage */
   dTempAz = 360. - (dAz[iGoodPicks-1] - dAz[0]);
   if (dTempAz > dAzGap) return( (int) (360.0 - dTempAz) );
   else                  return( (int) (360.0 - dAzGap) );
}

      /******************************************************************
       *                           QuakeDeta()                          *
       *                                                                *
       * Solve matrices.                                                *
       *                                                                *
       *  Arguments:                                                    *
       *   iDNum            Matrix order (3 fixed depth, 4 for float)   *
       *   dADet            Matrices                                    * 
       *                                                                *
       ******************************************************************/
       
double QuakeDeta( int iDNum, double dAdet[][4] )
{
   double  dProd, dSave[4];
   int     k, kk, i1, k1, k2, i, j, iCnt;

   dProd = 1.0;
   for ( k=0; k<(iDNum-1); k++ )
   {
      iCnt = 0;
      kk = k+1;
      while ( dAdet[k][k] == 0.0 )
      {
         if ( iCnt-(iDNum-1)+k+1 > 0 ) return 0.0;
         else
         {
            for ( i1=k; i1<iDNum; i1++ ) dSave[i1] = dAdet[i1][k];
            for ( k1=k; k1<iDNum; k1++ )
               for ( k2=k; k2<(iDNum-1); k2++ ) 
                  dAdet[k1][k2] = dAdet[k1][k2+1];
            for ( i1=k; i1<iDNum; i1++ ) dAdet[i1][k] = dSave[i1];
            iCnt++;
            dProd *= ((((iDNum-1)-k) % 2) ? -1.0 : 1.0);
         }
      }
      for ( i=kk; i<iDNum; i++ )
         for ( j=kk; j<iDNum; j++ )
            dAdet[j][i] -= dAdet[j][k] * dAdet[k][i] / dAdet[k][k];
      dProd *= dAdet[k][k];
   }
   dProd *= dAdet[iDNum-1][iDNum-1];
   return dProd;
}

      /******************************************************************
       *                           QuakeDets()                          *
       *                                                                *
       * This function uses determinates to get variable residuals.     *
       *                                                                *
       *  Arguments:                                                    *
       *   iDNum            Matrix order (3 fixed depth, 4 for float)   *
       *   dA, B, XDet      Matrices                                    * 
       *                                                                *
       ******************************************************************/
       
void QuakeDets( int iDNum, double dAdet[][4], double dBdet[], double dXdet[] )
{
   double  dA1det[4][4], dDenom, dAnum;
   int     i, j, k;

   for ( i=0; i<iDNum; i++ )
      for ( j=0; j<iDNum; j++ ) dA1det[i][j] = dAdet[i][j];

   dDenom = QuakeDeta( iDNum, dAdet );
   if ( dDenom < 1.E-13 )
   {
      dXdet[0] = 111.;
      return;
   }
   for ( k=0; k<iDNum; k++ )
   {
      for ( i=0; i<iDNum; i++ )
         for ( j=0; j<iDNum ; j++ ) dAdet[i][j] = dA1det[i][j];
      for ( i=0; i<iDNum; i++ ) dAdet[i][k] = dBdet[i];
      dAnum = QuakeDeta( iDNum, dAdet );
      dXdet[k] = dAnum / dDenom;
   }
}

      /******************************************************************
       *                       QuakeSolveIasp()                         *
       *                                                                *
       * This function determines the hypocenter of an event by         *
       * minimizing the set of time residuals of the observing stations.*
       * Up to QUAKE_ITER iterations are used to adjust the hypocenter  *
       * parameters.  On a first location attempt, the origin time is   *
       * calculated by subtracting the travel time from the earliest    *
       * station's arrival time and the depth is fixed.  The initial    *
       * location is taken from the HYPO array (this should have been   *
       * filled in InitialLocator). On subsequent iteration attempts,   *
       * the user may let the depth vary.  Depth is restricted to 750   *
       * km.  The travel times are taken from the IASPEI91 travel time  *
       * tables.
       *                                                                *
       * March, 2006: Added call to find depth so that depth is fixed at*
       *              average for location when fixed is specified, and *
       *              that max is set by location when floated.         *
       *                                                                *
       *  Arguments:                                                    *
       *   iPNum            Number of Ps in this structure              *
       *   P                One of the P Buffer structures              *
       *   pHypo            Computed hypocentral parameters             *
       *   pEqDep	    Array of depth data structures              *
       *   iFindDepth	    1->look up depth data, 0->don't             *
       *                                                                *
       ******************************************************************/
       
void QuakeSolveIasp( int iPNum, PPICK *P, HYPO *pHypo, EQDEPTHDATA pEqDep[],
                     int iFindDepth )
{
   static double  dAdet[4][4];  /* Matrices, etc., used to solve location */
   static double  dBdet[4];
   static double  dCoef[4*COEFFSIZE+MAX_STATION_DATA+10];
   static double  dDep;		    /* Epi. depth in km */
   double  dMin;
   static double  dXdet[4];
   double  dPTimeMin, dDeltaMin;/* Var. used on 1st pass thru for O-time */
   double  fih, fh, dadd, dadh, ptt1, ptt2, ptt; /* Interpolation values */
   int     i, j, k, l, mm, ih, itab, iIndex;     /* Counters, indices    */
   int     iIterCnt;                             /* Iteration counter    */
   int	   iMaxDepthLevel;      /* Maximum expected IASPEI depth level */
   int     iPCnt;               /* # of P's used in this solution so far */
   LATLON  LLIn, LLOut;	        /* LATLON structures for conversion to
                                   geographic coords. */
   float   *pfIA;               /* Pointer to IASPEI91 travel time table */

   iIterCnt = 0;
   for ( i=0; i<QUAKE_ITER; i++ )
   {
      iPCnt = 0;
            
/* Compute distance, azimuth, etc. for all P stations from previous loc */
      GetEpiAzDelta( iPNum, P, pHypo );
      
/* First time through get origin time from first P-time and delta */
      if ( pHypo->dOriginTime == 0.0 )
      {
         dMin = 1.e20;
         l = 0;
         for ( j=0; j<iPNum; j++ )
            if ( P[j].dPTime > 0. && P[j].iUseMe > 0 && P[j].dPTime < dMin ) 
            {
               l = j;
               dMin = P[j].dPTime;
            }
	    
/* Use first pick included in sol'n */
         dPTimeMin = P[l].dPTime;	/* These are sorted by time */
         dDeltaMin = P[l].dDelta;	/* Filled in GetEpiAzDelta */
	 
/* I have no idea where these formulas come from (PW) */
         if ( dDeltaMin <= 20.0 )
            pHypo->dOriginTime = dPTimeMin - 3.67489 - 14.1561*dDeltaMin -
             0.0189237*dDeltaMin*dDeltaMin + 0.00267753*dDeltaMin*dDeltaMin*
             dDeltaMin;
         else
            pHypo->dOriginTime = dPTimeMin - 61.1089 - 11.4192*dDeltaMin +
             0.0410401*dDeltaMin*dDeltaMin - 0.0000301625*dDeltaMin*dDeltaMin*
             dDeltaMin;
      }
	
/* Figure which depth level of the IASPEI91 travel time tables to use */
      pHypo->dAvgRes = 0.0;
    
/* Search the depth data base if desired */
      if (iFindDepth == 1)
      {
         LLIn.dLat = pHypo->dLat;
         LLIn.dLon = pHypo->dLon;
         GeoGraphic (&LLOut, &LLIn);
         if (LLOut.dLon > 180.) LLOut.dLon -= 360.;
         if (pHypo->iDepthControl == 3)		// Imply fixed depth
         {
            iIndex = FindDepth (LLOut.dLat, LLOut.dLon, pEqDep);
            if (iIndex == -1)
	    {
               pHypo->dDepth = DEFAULT_DEPTH;
	       iMaxDepthLevel = DEFAULT_MAXDEPTH / (int) IASP_DEPTH_INC;
	    }
	    else
            {
	       pHypo->dDepth = (double) pEqDep[iIndex].iAveDepth;
	       iMaxDepthLevel = (int) (pEqDep[iIndex].iMaxDepth /
                                (int) IASP_DEPTH_INC) + 5;
               if (iMaxDepthLevel > DEPTH_LEVELS_IASP)
                   iMaxDepthLevel = DEPTH_LEVELS_IASP;
	       }
	    }
         else if (pHypo->iDepthControl == 4)		// Imply float depth
         {
            iIndex = FindDepth (LLOut.dLat, LLOut.dLon, pEqDep);
	    if (iIndex == -1)
	       iMaxDepthLevel = DEFAULT_MAXDEPTH / (int) IASP_DEPTH_INC;
	    else
	    {
	       iMaxDepthLevel = (int) (pEqDep[iIndex].iMaxDepth /
                                (int) IASP_DEPTH_INC) + 5;
               if (iMaxDepthLevel > DEPTH_LEVELS_IASP)
                   iMaxDepthLevel = DEPTH_LEVELS_IASP;
	    }
	 }
      }
      else
        iMaxDepthLevel = DEPTH_LEVELS_IASP;

      dDep = pHypo->dDepth;
      fih = dDep / IASP_DEPTH_INC;      /* 0, 10km, 20km; depth inc. in table */
      if ( fih <= 1./IASP_DEPTH_INC )   /* Force depth to 1km */
      {
         fih = 1./IASP_DEPTH_INC;					
         pHypo->dDepth = 1.0;
      }
      ih = (int) (fih + 1.0);
      fh = fih - floor( fih );
      if (ih >= iMaxDepthLevel - 1)	/* max depth set above */
      {	  
         ih = iMaxDepthLevel - 1;
         pHypo->dDepth = (double) (iMaxDepthLevel*IASP_DEPTH_INC);
         fh = 0.0;
      }
      
/* Get expected P-times for each station */
      for ( j=0; j<iPNum; j++ )
      {
         if ( P[j].dPTime > 0. )
         {
/* First get index to use in IASPEI91 tables based on delta (table spacing
   is IASP_DIST_INC degree) */
            itab = (int) (P[j].dDelta*(1./IASP_DIST_INC));
            while ( P[j].dFracDelta - IASP_DIST_INC >= 0.0 )  
                    P[j].dFracDelta -= IASP_DIST_INC;

            pfIA = fPP + ih*IASP_NUM_PER_DEP + itab; /* Ptr to IASPEI91 table */
            dadd = ((*(pfIA+1)-*pfIA)*fh + (*(pfIA-IASP_NUM_PER_DEP+1)-
                   *(pfIA-IASP_NUM_PER_DEP))*(1.-fh)) / IASP_DIST_INC;
            dadh = (*(pfIA-IASP_NUM_PER_DEP)-*pfIA) * 
                   (1.-P[j].dFracDelta/IASP_DIST_INC) + 
                   (*(pfIA-IASP_NUM_PER_DEP+1) - 
                   *(pfIA+1))*(P[j].dFracDelta/IASP_DIST_INC);
		   
/* Four point interplolation */
            ptt1 = *pfIA + (*(pfIA+1)-*pfIA)*P[j].dFracDelta/IASP_DIST_INC;
            ptt2 = *(pfIA-IASP_NUM_PER_DEP) + (*(pfIA-IASP_NUM_PER_DEP+1)-
                   *(pfIA-IASP_NUM_PER_DEP))*P[j].dFracDelta/IASP_DIST_INC;
            ptt = ptt1*fh + ptt2*(1.-fh);            /* Depth adjustment */

            P[j].dRes = P[j].dPTime - (pHypo->dOriginTime + ptt) -
                        P[j].dElevation/6371.;       /* Normalize Elevation */
            if ( P[j].iUseMe > 0 )                   /* Adjust matrix */
            {
               dCoef[COEFFSIZE+iPCnt] = dadd * P[j].dCooze;
               dCoef[2*COEFFSIZE+iPCnt] = dadd * P[j].dSnooze *
                                          pHypo->dSinlat * (-1.0);
               dCoef[3*COEFFSIZE+iPCnt] = -dadh * 0.1;  /* ??? */
               dCoef[4*COEFFSIZE+iPCnt] = P[j].dRes;
               dCoef[iPCnt] = 1.0;
               pHypo->dAvgRes += fabs( P[j].dRes );
               if ( pHypo->iDepthControl == 3 )
                  dCoef[3*COEFFSIZE+iPCnt] = dCoef[4*COEFFSIZE+iPCnt];
               iPCnt++;
            } 
         }
         else
            P[j].dRes = 0.;
      }
	 
      if ( iIterCnt >= QUAKE_ITER-1 ||
         pHypo->dAvgRes/(double) iPNum < 0.2 ) goto FunctionEnd;

      for ( j=0; j<pHypo->iDepthControl; j++ )
         for ( l=0; l<pHypo->iDepthControl; l++ )
         {
            dAdet[j][l] = 0.0;
            for ( k=0; k<iPCnt; k++ )
               dAdet[j][l] += dCoef[k+j*COEFFSIZE] * dCoef[k+l*COEFFSIZE];
         }
      mm = pHypo->iDepthControl;
      for ( j=0; j<pHypo->iDepthControl; j++ )
      {
         dBdet[j] = 0.0;
         for ( k=0; k<iPCnt; k++ )
            dBdet[j] += dCoef[k+j*COEFFSIZE] * dCoef[k+mm*COEFFSIZE];
      }
      
/* Solve matrix */
      QuakeDets( pHypo->iDepthControl, dAdet, dBdet, dXdet );
      if ( dXdet[0] == 111. ) goto FunctionEnd;
      if ( pHypo->iDepthControl > 3 ) pHypo->dDepth += dXdet[3];
      pHypo->dOriginTime += dXdet[0];
      pHypo->dLat += dXdet[1]*RAD;
      pHypo->dLon += dXdet[2]*RAD;
      iIterCnt++;
   }
FunctionEnd:

/* Compute the average residual */
   pHypo->dAvgRes = pHypo->dAvgRes/(double) iPNum;

/* Find azimuthal coverage around epicenter and dist to closest station */
   pHypo->iAzm = QuakeAzimuthSort( iPNum, P );
   pHypo->dNearestDist = 200.;
   for ( j=0; j<iPNum; j++ )
      if ( P[j].dPTime > 0. && P[j].iUseMe > 0 )
         if ( P[j].dDelta < pHypo->dNearestDist )
         {
            pHypo->dNearestDist = P[j].dDelta;
            pHypo->dFirstPTime = P[j].dPTime;
         }
	 
/* Adjust lat/lon to real earth values */
   if (pHypo->dLat < 0.)
   {
      pHypo->dLat = fabs( pHypo->dLat );
      pHypo->dLon -= PI;
   }
   while ( pHypo->dLat > TWOPI ) pHypo->dLat -= TWOPI;
   if ( pHypo->dLat > PI && pHypo->dLat <= TWOPI )
   {
      pHypo->dLat = TWOPI - pHypo->dLat;
      pHypo->dLon -= PI;
   }
   while ( pHypo->dLon > TWOPI ) pHypo->dLon -= TWOPI;
   while ( pHypo->dLon < 0.0 ) pHypo->dLon += TWOPI;
}

      /******************************************************************
       *                       Round()                                  *
       *                                                                *
       * Function Round will round double values to the nearest integer.*
       *                                                                *
       *  Arguments:                                                    *
       *   dInput        Input double value                             *
       *                                                                *
       *  Returns:                                                      *
       *   int           Rounded value                                  *
       *                                                                *
       ******************************************************************/
       
int Round( double dInput )
{
   int     iRound;
   double  dDec;
    
   if ( dInput >= 0. )
   {
      if ( modf( dInput, &dDec ) >= 0.5 )
      {
         iRound = (int) ceil( dInput );
      }
      else
         iRound = (int) floor( dInput );
   }
   else
      if ( modf( dInput, &dDec ) <= -0.5 )
      {
         iRound = (int) floor( dInput );
      }
      else
         iRound = (int) ceil( dInput );
	 
   return (iRound);	
}

      /******************************************************************
       *                       SortAllByPTime()                         *
       *                                                                *
       * This function sorts the PPICK array by P-times.  It includes   *
       * all stations in the sort.  This is a sort routine for qsort.   *
       *                                                                *
       *  Arguments:                                                    *
       *   pP1, pP2         PPICK structure                             *
       *                                                                *
       ******************************************************************/
	   
int SortAllByPTime( const void *pP1, const void *pP2 )
{
   PPICK   *pP1T, *pP2T;

   pP1T = (PPICK *) pP1;
   pP2T = (PPICK *) pP2;
   if ( pP1T->dPTime > pP2T->dPTime ) return 1;
   else if ( pP1T->dPTime < pP2T->dPTime ) return -1;
   else return 0;
}

      /******************************************************************
       *                  SortAllByExpectedPTime()                      *
       *                                                                *
       * This function sorts the PPICK array by expected P-times.  It   *
       * includes all stations in the sort.  This is a sort routine for *
       * qsort.                                                         *
       *                                                                *
       *  Arguments:                                                    *
       *   pP1, pP2         PPICK structure                             *
       *                                                                *
       ******************************************************************/
	   
int SortAllByExpectedPTime( const void *pP1, const void *pP2 )
{
   PPICK   *pP1T, *pP2T;

   pP1T = (PPICK *) pP1;
   pP2T = (PPICK *) pP2;
   if ( pP1T->dExpectedPTime > pP2T->dExpectedPTime ) return 1;
   else if ( pP1T->dExpectedPTime < pP2T->dExpectedPTime ) return -1;
   else return 0;
}

      /******************************************************************
       *                          WritePickFile()                       *
       *                                                                *
       * This function writes the P and magnitude data to a file.       *
       *                                                                *
       *  Arguments:                                                    *
       *   iPNum            Number of Ps in this structure              *
       *   P                One of the P Buffer structures              *
       *   pszFile          Output data file name                       *
       *                                                                *
       ******************************************************************/
	   
void WritePickFile( int iNumP, PPICK P[], char *pszFile )
{
   FILE    *hFile;                   /* File handle */
   int     i;

/* Open pick file */        
   if ( (hFile = fopen( pszFile, "w" )) == NULL )
   {
      logit ("t", "Pick file, %s, not opened for write (1).\n", pszFile);
      return;
   }
   
/* Dump the picks and magnitude data to disk */   
   for ( i=0; i<iNumP; i++ )  
      if ( P[i].iUseMe > 0 )
         fprintf( hFile, "%s %s %s %lf %s %lf %lf %lf %lf %lf %lf %lf %lf %lf"
          " %lf %lf %lf %lf %lf %lf %lf %E %lf %lf %lf %lf %lf %lf\n",
          P[i].szStation, P[i].szChannel, P[i].szNetID, P[i].dPTime,
          P[i].szPhase,   /* NOTE: 0. for sens -> amp in GM */
          P[i].dMbAmpGM, (double) P[i].lMbPer/10., 0., P[i].dMbTime, 
          P[i].dMlAmpGM, (double) P[i].lMlPer/10., 0., P[i].dMlTime, 
          P[i].dMSAmpGM, (double) P[i].lMSPer, 0., P[i].dMSTime, 
          0., 0., 0., 0., 
          P[i].dMwpIntDisp, P[i].dMwpTime, 0., 0., 0., 0., 0. );
   fclose( hFile );
}

      /******************************************************************
       *                          WritePickFile2()                      *
       *                                                                *
       * This function writes the P and magnitude data to a file for    *
       * use by hypo_display.                                           *
       *                                                                *
       *  Arguments:                                                    *
       *   iPNum            Number of Ps in this structure              *
       *   P                One of the P Buffer structures              *
       *   pszFile          Output data file name                       *
       *                                                                *
       ******************************************************************/
	   
void WritePickFile2( int iNumP, PPICK P[], char *pszFile )
{
   FILE    *hFile;                   /* File handle */
   int     i;
                                                   
/* Open pick file */        
   if ( (hFile = fopen( pszFile, "w" )) == NULL )
   {
      logit ("t", "Pick file, %s, not opened.\n", pszFile);
      return;
   }
   
/* Dump the picks and magnitude data to disk */   
   for ( i=0; i<iNumP; i++ )  
      fprintf( hFile, "%s %s %s %ld %lf %s %lf %lf %lf %ld %c %lf %lf %lf %lf "
       "%lf %lf %lf %lf %lf %lf %lf %lf %E %lf %lf\n",
       P[i].szStation, P[i].szChannel, P[i].szNetID, P[i].lPickIndex, 
       P[i].dPTime, P[i].szPhase, P[i].dRes, P[i].dDelta, P[i].dAz,
       P[i].iUseMe, P[i].cFirstMotion,
       P[i].dMbAmpGM, (double) P[i].lMbPer/10., P[i].dMbMag, P[i].dMbTime, 
       P[i].dMlAmpGM, (double) P[i].lMlPer/10., P[i].dMlMag, P[i].dMlTime, 
       P[i].dMSAmpGM, (double) P[i].lMSPer, P[i].dMSMag, P[i].dMSTime, 
       P[i].dMwpIntDisp, P[i].dMwpTime, P[i].dMwpMag );
   fclose( hFile );
}
