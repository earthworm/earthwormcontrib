
  /**********************************************************************
   *                             display.c                              *
   *                                                                    *
   * This set of functions draws traces, time lines, stations names,    *
   * etc. to a window.  Windows API calls are used throughout.          *
   *                                                                    *
   **********************************************************************/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>
#include "earlybirdlib.h"

 /********************************************************************
  *                 DisplayChannelID()                               *
  *                                                                  *
  * This function displays the station name next to its trace in the *
  * window.                                                          *
  *                                                                  *
  * September, 2004: Color name green if data ahead of present time. *
  * March, 2002: Color name blue if pager alarms on.                 *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  Sta               Array of all station data structures          *
  *  Trace             Array of screen display information per trace *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  iScreenToDisplay  Screen index (subset of stations) to display  *
  *  lTitleFHt         Title font height                             *
  *  lTitleFWd         Title font width                              *
  *  cxScreen          Horizontal pixel width of window              *
  *  cyScreen          Vertical pixel height of window               *
  *  iNumTracePerScreen Number of traces to put on screen            *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iAhead            0->data ok; 1->data timing off                *
  *                                                                  *
  ********************************************************************/
  
void DisplayChannelID( HDC hdc, STATION Sta[], TRACE Trace[], int iNumStas,
                       int iScreenToDisplay, long lTitleFHt, long lTitleFWd,
                       int cxScreen, int cyScreen, int iNumTracePerScreen,
                       int iVScrollOffset, int iTitleOffset, int iAhead[] )
{
   HFONT   hOFont, hNFont;       /* Old and new font handles */
   int     i, Font_Height, Font_Width;
   int     iCnt;                 /* Channel names counter */
   long    lOffset;              /* Offset to center of trace from top */
   POINT   pt;                   /* Screen location for trace name */

/* Create font */

/*   hNFont = CreateFont( 3*lTitleFHt/4, 3*lTitleFWd/4, 
             0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN,
             "Times New Roman" );
*/


///////////////////////////////////////////////////////////////////////////////
/* Compute font size using a ramp function; this change was necessary for
    the increase in pixels on eb4;  dln 10/11/06 */

Font_Height = 40*lTitleFHt/iNumTracePerScreen;
		if (Font_Height < 6 )
		{
		 Font_Height = 6;
		}
		if (Font_Height > 30 )
		{
		 Font_Height = 30;
		}
Font_Width = lTitleFWd;
		if (Font_Width > 8 )
		{
		 Font_Width = 8;
		}
hNFont = CreateFont( Font_Height, Font_Width, 
             0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS,
             "Arial" );		
hOFont = SelectObject( hdc, hNFont );
///////////////////////////////////////////////////////////////////////////////



/* Loop through all stations and see which should be shown now */   
   pt.x = cxScreen / 500;
   iCnt = 0;
   for ( i=0; i<iNumStas; i++ )
      if ( Trace[i].iStationDisp[iScreenToDisplay] )
      {
         if (iAhead[i] > 0)              SetTextColor( hdc, RGB( 0, 128, 0 ) );
         else if (Sta[i].iAlarmSpeak > 0)SetTextColor( hdc, RGB( 255, 0, 0 ) );
         else                            SetTextColor( hdc, RGB( 0, 0, 128 ) );
         lOffset = iCnt*(cyScreen-iTitleOffset)/iNumTracePerScreen +
                   iVScrollOffset + iTitleOffset;
         pt.y = lOffset - 3*lTitleFHt/8;
         TextOut( hdc, pt.x, pt.y, Sta[i].szStation, strlen( Sta[i].szStation ) );
         iCnt++;
      }
   DeleteObject( SelectObject( hdc, hOFont ) );  /* Reset font */
}

 /********************************************************************
  *                  DisplayChannel()                                *
  *                                                                  *
  * This function displays the channel next to                       *
  *  the station name next to its trace in the window.               *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  Sta               Array of all station data structures          *
  *  Trace             Array of screen display information per trace *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  iScreenToDisplay  Screen index (subset of stations) to display  *
  *  lTitleFHt         Title font height                             *
  *  lTitleFWd         Title font width                              *
  *  cyScreen          Vertical pixel height of window               *
  *  iNumTracePerScreen Number of traces to put on screen            *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *                                                                  *
  ********************************************************************/
  
void DisplayChannel( HDC hdc, STATION Sta[], TRACE Trace[], int iNumStas,
                     int iScreenToDisplay, long lTitleFHt, long lTitleFWd,
                     int cyScreen, int iNumTracePerScreen,
                     int iVScrollOffset, int iTitleOffset, int iChanIDOffset )
{
   HFONT   hOFont, hNFont;       /* Old and new font handles */
   int     i, Font_Height, Font_Width;
   int     iCnt;                 /* Channel names counter */
   long    lOffset;              /* Offset to center of trace from top */
   POINT   pt;                   /* Screen location for trace name */

/* Create font */
/*   hNFont = CreateFont( 3*lTitleFHt/4, 3*lTitleFWd/4, 
             0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN,
             "Times New Roman" );
   hOFont = SelectObject( hdc, hNFont );
*/
   
///////////////////////////////////////////////////////////////////////////////
/* Compute font size using a ramp function; this change was necessary for
    the increase in pixels on eb4;  dln 10/11/06 */

Font_Height = 40*lTitleFHt/iNumTracePerScreen;
		if (Font_Height < 6 )
		{
		 Font_Height = 6;
		}
		if (Font_Height > 30 )
		{
		 Font_Height = 30;
		}
Font_Width = lTitleFWd;
		if (Font_Width > 8 )
		{
		 Font_Width = 8;
		}
hNFont = CreateFont( Font_Height, Font_Width, 
             0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS,
             "Arial" );		
hOFont = SelectObject( hdc, hNFont );
///////////////////////////////////////////////////////////////////////////////
SetTextColor( hdc, RGB( 255, 0, 0 ) );
   
/* Loop through all stations and see which should be shown now */   
   pt.x = 9*Font_Width; //iChanIDOffset;
   iCnt = 0;
   for ( i=0; i<iNumStas; i++ )
      if ( Trace[i].iStationDisp[iScreenToDisplay] )
      {
         lOffset = iCnt*(cyScreen-iTitleOffset)/iNumTracePerScreen +
                   iVScrollOffset + iTitleOffset;
         pt.y = lOffset - 3*lTitleFHt/8;
         TextOut( hdc, pt.x, pt.y, Sta[i].szChannel,
                  strlen( Sta[i].szChannel ) );
         iCnt++;
      }
   DeleteObject( SelectObject( hdc, hOFont ) );  /* Reset font */
}

 /********************************************************************
  *                  DisplayDCOffset()                               *
  *                                                                  *
  * This function displays the average DC offset in counts next to   *
  *  the station name next to its trace in the window.               *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  Sta               Array of all station data structures          *
  *  Trace             Array of screen display information per trace *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  iScreenToDisplay  Screen index (subset of stations) to display  *
  *  lTitleFHt         Title font height                             *
  *  lTitleFWd         Title font width                              *
  *  cyScreen          Vertical pixel height of window               *
  *  iNumTracePerScreen Number of traces to put on screen            *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *                                                                  *
  ********************************************************************/
  
void DisplayDCOffset( HDC hdc, STATION Sta[], TRACE Trace[], int iNumStas,
                      int iScreenToDisplay, long lTitleFHt, long lTitleFWd,
                      int cyScreen, int iNumTracePerScreen,
                      int iVScrollOffset, int iTitleOffset, int iChanIDOffset )
{
   HFONT   hOFont, hNFont;       /* Old and new font handles */
   int     i, Font_Height, Font_Width;
   int     iCnt;                 /* Channel names counter */
   long    lOffset;              /* Offset to center of trace from top */
   POINT   pt;                   /* Screen location for trace name */
   char    szTemp[32];           /* DC Offset in counts */

/* Create font */
/*   hNFont = CreateFont( 2*lTitleFHt/3, 2*lTitleFWd/3, 
             0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN,
             "Times New Roman" );
   hOFont = SelectObject( hdc, hNFont );
*/
   
///////////////////////////////////////////////////////////////////////////////
/* Compute font size using a ramp function; this change was necessary for
    the increase in pixels on eb4;  dln 10/11/06 */

Font_Height = 40*lTitleFHt/iNumTracePerScreen;
		if (Font_Height < 6 )
		{
		 Font_Height = 6;
		}
		if (Font_Height > 30 )
		{
		 Font_Height = 30;
		}
Font_Width = lTitleFWd;
		if (Font_Width > 8 )
		{
		 Font_Width = 8;
		}
hNFont = CreateFont( Font_Height, Font_Width, 
             0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS,
             "Arial" );		
hOFont = SelectObject( hdc, hNFont );
///////////////////////////////////////////////////////////////////////////////
SetTextColor( hdc, RGB( 0, 0, 0 ) );

   
/* Loop through all stations and see which should be shown now */   
   pt.x = 9*Font_Width; // iChanIDOffset;
   iCnt = 0;
   for ( i=0; i<iNumStas; i++ )
      if ( Trace[i].iStationDisp[iScreenToDisplay] )
      {
         lOffset = iCnt*(cyScreen-iTitleOffset)/iNumTracePerScreen +
                   iVScrollOffset + iTitleOffset;
         pt.y = lOffset - 3*lTitleFHt/8;
         itoaX( (int) fabs( Sta[i].dAveLDCRaw ), szTemp );
         TextOut( hdc, pt.x, pt.y, szTemp, strlen( szTemp ) );
         iCnt++;
      }
   DeleteObject( SelectObject( hdc, hOFont ) );  /* Reset font */
}

 /********************************************************************
  *                  DisplayExpectedP()                              *
  *                                                                  *
  * This function displays the expected P arrival time for the quake *
  * in the dummy file.                                               *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  Trace             Array of screen display information per trace *
  *  iNumTrace         Number of traces to show on screen            *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  iScreenToDisplay  Screen index (subset of stations) to display  *
  *  cxScreen          Horizontal pixel width of window              *
  *  cyScreen          Vertical pixel height of window               *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *  dScreenEnd        1/1/70 (seconds) time at end of display       *
  *  dScreenTime       # seconds to show on screen                   *
  *  lTitleFHt         Title font height                             *
  *  pPBuf             P-pick buffer to copy PStruct into            *
  *                                                                  *
  ********************************************************************/
  
void DisplayExpectedP( HDC hdc, TRACE Trace[], int iNumTrace, int iNumStas,
                       int iScreenToDisplay, int cxScreen, int cyScreen,
                       int iVScrollOffset, int iTitleOffset, int iChanIDOffset,
                       double dScreenEnd, double dScreenTime, long lTitleFHt,
                       PPICK pPBuf[] ) 
{
   double  dScreenStart;         /* 1/1/70 (sec) time at left of screen */
   HPEN    hGPen, hOPen;         /* Pen handles */
   int     i, iCnt;              /* Counters */
   long    lOffsetY;             /* Dist to skip on top of screen */
   POINT   pt[2];

/* Compute time at left side of screen */
   dScreenStart = dScreenEnd - dScreenTime;

/* Create font and pens */
   hGPen = CreatePen( PS_SOLID, 2, RGB( 0, 255, 127 ) );   /* Lines Green */
   hOPen = SelectObject( hdc, hGPen );

/* Show expected P times if on screen */   
   iCnt = 0;
   for ( i=0; i<iNumStas; i++ )
      if ( Trace[i].iStationDisp[iScreenToDisplay] )
      {
         if ( pPBuf[i].dExpectedPTime > 0. )
         {
            pt[0].x = iChanIDOffset + (long) (((pPBuf[i].dExpectedPTime-
                      dScreenStart) * (double) (cxScreen-iChanIDOffset)) /
                      dScreenTime);
            pt[1].x = pt[0].x;
            lOffsetY = iCnt*(cyScreen-iTitleOffset)/iNumTrace +
                       iVScrollOffset + iTitleOffset;
			
/* Mark pick on screen, if it really shows up on screen */			
            if ( pt[1].x >= iChanIDOffset && pt[1].x <= cxScreen )
            {
               pt[0].y = lOffsetY - lTitleFHt/4;
               pt[1].y = lOffsetY + lTitleFHt/4;
               MoveToEx ( hdc, pt[0].x, pt[0].y, NULL );
               LineTo ( hdc, pt[1].x, pt[1].y );
            }
         }		
         iCnt++;
      }
   DeleteObject( SelectObject( hdc, hOPen ) );   /* Reset Pen color */
}

 /********************************************************************
  *                     DisplayKOPs()                                *
  *                                                                  *
  * This function displays an X when traces have been removed from   *
  * further locations.                                               *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  Trace             Array of screen display information per trace *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  iScreenToDisplay  Screen index (subset of stations) to display  *
  *  cxScreen          Horizontal pixel width of window              *
  *  cyScreen          Vertical pixel height of window               *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *  pPBuf             P-pick buffer to copy PStruct into            *
  *  lTitleFHt         Title font height                             *
  *  lTitleFWd         Title font width                              *
  *  iNumTracePerScreen Number of traces to put on screen            *
  *                                                                  *
  ********************************************************************/
  
void DisplayKOPs( HDC hdc, TRACE Trace[], int iNumStas,
                  int iScreenToDisplay, int cxScreen, int cyScreen,
                  int iVScrollOffset, int iTitleOffset, int iChanIDOffset,
                  PPICK pPBuf[], long lTitleFHt, long lTitleFWd,
                  int iNumTracePerScreen )
{
   HFONT   hOFont, hNFont;       /* Old and new font handles */
   int     i, iCnt;              /* Counters */
   int     Font_Height, Font_Width; //height and width of X
   POINT   pt;                   /* Screen location for X */

/* Create font */
/*   hNFont = CreateFont( lTitleFHt, lTitleFWd*2, 
             0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN,
             "Times New Roman" );
   hOFont = SelectObject( hdc, hNFont );
*/
   
///////////////////////////////////////////////////////////////////////////////
/* Compute font size using a ramp function; this change was necessary for
    the increase in pixels on eb4;  dln 10/11/06 */

Font_Height = 40*lTitleFHt/iNumTracePerScreen;
		if (Font_Height < 6 )
		{
		 Font_Height = 6;
		}
		if (Font_Height > 30 )
		{
		 Font_Height = 30;
		}
Font_Width = lTitleFWd;
		if (Font_Width > 8 )
		{
		 Font_Width = 8;
		}
hNFont = CreateFont( Font_Height, Font_Width*2, 
             0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN,
             "Times New Roman"  );		
hOFont = SelectObject( hdc, hNFont );
///////////////////////////////////////////////////////////////////////////////
   SetTextColor( hdc, RGB( 0, 0, 128 ) );         /* X's in Blue */
   iCnt = 0; 

/* Show all X's */
   pt.x = 6*Font_Width; //iChanIDOffset / 4;
   for ( i=0; i<iNumStas; i++ )
      if ( Trace[i].iStationDisp[iScreenToDisplay] )
      {
         if ( pPBuf[i].iUseMe == -1 )
         {
            pt.y = iCnt*(cyScreen-iTitleOffset)/iNumTracePerScreen +
                   iVScrollOffset + iTitleOffset - lTitleFHt*1/2;
            TextOut( hdc, pt.x, pt.y, "X", 1 );
         }
         iCnt++;
      }
      
   DeleteObject( SelectObject( hdc, hOFont ) );  /* Reset font */
}

 /********************************************************************
  *                      DisplayMagBox()                             *
  *                                                                  *
  * Box the cycle used to compute Ms.                                *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  iNumTrace         Number of traces shown per screen             *
  *  cxScreen          Horizontal pixel width of window              *
  *  cyScreen          Vertical pixel height of window               *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *  dScreenEnd        1/1/70 (seconds) time at end of display       *
  *  dScreenTime       # seconds to show on screen                   *
  *  lTitleFHt         Title font height                             *
  *  pPBuf             P-pick buffers                                *
  *                                                                  *
  ********************************************************************/
  
void DisplayMagBox( HDC hdc, int iNumStas, int iNumTrace, int cxScreen,
                    int cyScreen, int iVScrollOffset, int iTitleOffset,
                    int iChanIDOffset, double dScreenEnd, double dScreenTime,
                    long lTitleFHt, PPICK pPBuf[] ) 
{
   double  dScreenStart;         /* 1/1/70 (sec) time at left of screen */
   HPEN    hBPen, hOPen;         /* Pen handles */
   int     i;                    /* Counters */
   long    lOffsetY;             /* Dist to skip on top of screen */
   POINT   pt[2];

/* Compute time at left side of screen */
   dScreenStart = dScreenEnd - dScreenTime;

/* Create font and pens */
   hBPen = CreatePen( PS_SOLID, 2, RGB( 0, 0, 127 ) );   /* Lines Blue */
   hOPen = SelectObject( hdc, hBPen );

/* Show which cycle Ms was computed on */   
   for ( i=0; i<iNumStas; i++ )
      if ( pPBuf[i].dMSMag > 0. )
      {
         pt[0].x = iChanIDOffset + (long) (((pPBuf[i].dMSTime-dScreenStart)
                   * (double) (cxScreen-iChanIDOffset)) / dScreenTime);
         pt[1].x = pt[0].x;
         lOffsetY = i*(cyScreen-iTitleOffset)/iNumTrace +
                    iVScrollOffset + iTitleOffset;
			
/* Mark spot on screen, if it really shows up on screen */			
         if ( pt[1].x >= iChanIDOffset && pt[1].x <= cxScreen )
         {
            pt[0].y = lOffsetY - lTitleFHt/2;
            pt[1].y = lOffsetY + lTitleFHt/2;
            MoveToEx ( hdc, pt[0].x, pt[0].y, NULL );
            LineTo ( hdc, pt[1].x, pt[1].y );
 	 
/* Then make cross */	 
            pt[0].x -= lTitleFHt/2; 
            pt[1].x = pt[0].x + lTitleFHt;
            pt[0].y = lOffsetY;
            pt[1].y = lOffsetY;
            MoveToEx ( hdc, pt[0].x, pt[0].y, NULL );
            LineTo ( hdc, pt[1].x, pt[1].y );
         }
      }		
   DeleteObject( SelectObject( hdc, hOPen ) );   /* Reset Pen color */
}

 /********************************************************************
  *                         DisplayMs()                              *
  *                                                                  *
  * This function displays the Ms next to the station name.          *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  pPBuf             P-pick buffers                                *
  *  lTitleFHt         Title font height                             *
  *  lTitleFWd         Title font width                              *
  *  cxScreen          Horizontal pixel width of window              *
  *  cyScreen          Vertical pixel height of window               *
  *  iNumTracePerScreen Number of traces to put on screen            *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *                                                                  *
  ********************************************************************/
  
void DisplayMs( HDC hdc, int iNumStas, PPICK pPBuf[], long lTitleFHt,
                long lTitleFWd, int cxScreen, int cyScreen,
                int iNumTracePerScreen, int iVScrollOffset, int iTitleOffset,
                int iChanIDOffset )
{
   HFONT   hOFont, hNFont;       /* Old and new font handles */
   int     i;
   long    lOffset;              /* Offset to center of trace from top */
   POINT   pt;                   /* Screen location for trace name */
   char    szBuffer[12], szTemp[12];

/* Create font */
   hNFont = CreateFont( 3*lTitleFHt/4, 3*lTitleFWd/4, 
             0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN,
             "Times New Roman" );
   hOFont = SelectObject( hdc, hNFont );
   SetTextColor( hdc, RGB( 255, 0, 0 ) );             /* Color Ms's Blue */

/* Write the Ms's */   
   pt.x = iChanIDOffset;
   for ( i=0; i<iNumStas; i++ )
      if ( pPBuf[i].dMSMag > 0. )
      {
         lOffset = i*(cyScreen-iTitleOffset)/iNumTracePerScreen +
                   iVScrollOffset + iTitleOffset;
         pt.y = lOffset - 3*lTitleFHt/8;
         strcpy (szBuffer, "MS = ");
         _gcvt (pPBuf[i].dMSMag, 2, szTemp);
         if ( pPBuf[i].dMSMag-(int)pPBuf[i].dMSMag <= 0.05 || 
              pPBuf[i].dMSMag-(int)pPBuf[i].dMSMag >= 0.95 ) szTemp[2] = '0';
         strcat( szBuffer, szTemp );
         TextOut( hdc, pt.x, pt.y, szBuffer, strlen( szBuffer ) );
      }
   DeleteObject( SelectObject( hdc, hOFont ) );  /* Reset font */
}

 /********************************************************************
  *                     DisplayMwp()                                 *
  *                                                                  *
  * This function displays a line over the trace where Mwp           *
  * computation took place.                                          * 
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  Trace             Array of screen display information per trace *
  *  iNumTrace         Number of traces to show on screen            *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  iScreenToDisplay  Screen index (subset of stations) to display  *
  *  cxScreen          Horizontal pixel width of window              *
  *  cyScreen          Vertical pixel height of window               *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *  dScreenEnd        1/1/70 (seconds) time at end of display       *
  *  dScreenTime       # seconds to show on screen                   *
  *  pPBuf             P-pick buffer to copy PStruct into            *
  *                                                                  *
  ********************************************************************/
  
void DisplayMwp( HDC hdc, TRACE Trace[], int iNumTrace, int iNumStas,
                 int iScreenToDisplay, int cxScreen, int cyScreen,
                 int iVScrollOffset, int iTitleOffset, int iChanIDOffset,
                 double dScreenEnd, double dScreenTime, PPICK pPBuf[] ) 
{
   double  dScreenStart;         /* 1/1/70 (sec) time at left of screen */
   HPEN    hOPen, hYPen;         /* Pen handles */
   int     i, iCnt;              /* Counters */
   POINT   pt[2];                /* Line start and end */

/* Compute time at left side of screen */
   dScreenStart = dScreenEnd - dScreenTime;

/* Create pens */
   hYPen = CreatePen( PS_SOLID, 2, RGB( 100, 100, 0 ) );/*Create yellow lines */
   hOPen = SelectObject (hdc, hYPen);
   iCnt = 0; 

/* Show all Mwp time on each trace which Mwp was computed */
   for ( i=0; i<iNumStas; i++ )
      if ( Trace[i].iStationDisp[iScreenToDisplay] )
      {
	  
/* Draw Mwp yellow line over integrated part of P */
         if ( pPBuf[i].dMwpIntDisp > 0. )
         {
            pt[0].x = iChanIDOffset + (long) (((pPBuf[i].dPTime - dScreenStart)
                      * (double) (cxScreen-iChanIDOffset)) / dScreenTime);
            pt[1].x = pt[0].x + (long) (pPBuf[i].dMwpTime * 
                      (double) (cxScreen-iChanIDOffset) / dScreenTime);
            pt[0].y = iCnt*(cyScreen-iTitleOffset)/iNumTrace +
                      iVScrollOffset + iTitleOffset;
            pt[1].y = pt[0].y;
            if ( pt[0].x < iChanIDOffset && pt[1].x > iChanIDOffset )
               pt[0].x = iChanIDOffset;
            if ( pt[0].x < cxScreen && pt[1].x > cxScreen )
               pt[1].x = cxScreen;
            if ( pt[0].x >= iChanIDOffset && pt[1].x <= cxScreen )
            {
               MoveToEx( hdc, pt[0].x, pt[0].y, NULL );
               Polyline( hdc, pt, 2 );
            }
         }
         iCnt++;
      }   
DeleteObject( hYPen );
SelectObject( hdc, hOPen );               /* Reset Pen color */
}

 /********************************************************************
  *                  DisplayNetwork()                                *
  *                                                                  *
  * This function displays the Network ID next to                    *
  *  the station name next to its trace in the window.               *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  Sta               Array of all station data structures          *
  *  Trace             Array of screen display information per trace *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  iScreenToDisplay  Screen index (subset of stations) to display  *
  *  lTitleFHt         Title font height                             *
  *  lTitleFWd         Title font width                              *
  *  cyScreen          Vertical pixel height of window               *
  *  iNumTracePerScreen Number of traces to put on screen            *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *                                                                  *
  ********************************************************************/
  
void DisplayNetwork( HDC hdc, STATION Sta[], TRACE Trace[], int iNumStas,
                     int iScreenToDisplay, long lTitleFHt, long lTitleFWd,
                     int cyScreen, int iNumTracePerScreen,
                     int iVScrollOffset, int iTitleOffset, int iChanIDOffset )
{
   HFONT   hOFont, hNFont;       /* Old and new font handles */
   int     i, Font_Height, Font_Width;
   int     iCnt;                 /* Channel names counter */
   long    lOffset;              /* Offset to center of trace from top */
   POINT   pt;                   /* Screen location for trace name */

/* Create font */
/*   hNFont = CreateFont( 3*lTitleFHt/4, 3*lTitleFWd/4, 
             0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN,
             "Times New Roman" );
   hOFont = SelectObject( hdc, hNFont );
*/
   
///////////////////////////////////////////////////////////////////////////////
/* Compute font size using a ramp function; this change was necessary for
    the increase in pixels on eb4;  dln 10/11/06 */

Font_Height = 40*lTitleFHt/iNumTracePerScreen;
		if (Font_Height < 6 )
		{
		 Font_Height = 6;
		}
		if (Font_Height > 30 )
		{
		 Font_Height = 30;
		}
Font_Width = lTitleFWd;
		if (Font_Width > 8 )
		{
		 Font_Width = 8;
		}
hNFont = CreateFont( Font_Height, Font_Width, 
             0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS,
             "Arial" );		
hOFont = SelectObject( hdc, hNFont );
///////////////////////////////////////////////////////////////////////////////
   
   SetTextColor( hdc, RGB( 255, 0, 0 ) );
   
/* Loop through all stations and see which should be shown now */   
   pt.x = 9*Font_Width; //iChanIDOffset;
   iCnt = 0;
   for ( i=0; i<iNumStas; i++ )
      if ( Trace[i].iStationDisp[iScreenToDisplay] )
      {
         lOffset = iCnt*(cyScreen-iTitleOffset)/iNumTracePerScreen +
                   iVScrollOffset + iTitleOffset;
         pt.y = lOffset - 3*lTitleFHt/8;
         TextOut( hdc, pt.x, pt.y, Sta[i].szNetID,
                  strlen( Sta[i].szNetID ) );
         iCnt++;
      }            
   DeleteObject( SelectObject( hdc, hOFont ) );  /* Reset font */
}

 /********************************************************************
  *                     DisplayPPicks()                              *
  *                                                                  *
  * This function displays all P arrivals in the PBuf array.         *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  Trace             Array of screen display information per trace *
  *  iNumTrace         Number of traces to show on screen            *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  iScreenToDisplay  Screen index (subset of stations) to display  *
  *  cxScreen          Horizontal pixel width of window              *
  *  cyScreen          Vertical pixel height of window               *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *  dScreenEnd        1/1/70 (seconds) time at end of display       *
  *  dScreenTime       # seconds to show on screen                   *
  *  lTitleFHt         Title font height                             *
  *  pPBuf             P-pick buffer to copy PStruct into            *
  *                                                                  *
  ********************************************************************/
  
void DisplayPPicks( HDC hdc, TRACE Trace[], int iNumTrace, int iNumStas,
                    int iScreenToDisplay, int cxScreen, int cyScreen,
                    int iVScrollOffset, int iTitleOffset, int iChanIDOffset,
                    double dScreenEnd, double dScreenTime, long lTitleFHt,
                    PPICK pPBuf[] ) 
{
   double  dScreenStart;         /* 1/1/70 (sec) time at left of screen */
   HPEN    hRPen, hOPen;         /* Pen handles */
   int     i, iCnt;              /* Counters */
   long    lOffsetY;             /* Dist to skip on top of screen */
   POINT   pt[2];

/* Compute time at left side of screen */
   dScreenStart = dScreenEnd - dScreenTime;

/* Create font and pens */
   hRPen = CreatePen( PS_SOLID, 2, RGB( 255, 0, 127 ) );   /* Lines Red */
   hOPen = SelectObject( hdc, hRPen );

/* Show picks if on screen */   
   iCnt = 0;
   for ( i=0; i<iNumStas; i++ )
      if ( Trace[i].iStationDisp[iScreenToDisplay] )
      {
         if ( pPBuf[i].dPTime > 0. )
         {
            pt[0].x = iChanIDOffset + (long) (((pPBuf[i].dPTime-dScreenStart)
                      * (double) (cxScreen-iChanIDOffset)) / dScreenTime);
            pt[1].x = pt[0].x;
            lOffsetY = iCnt*(cyScreen-iTitleOffset)/iNumTrace +
                       iVScrollOffset + iTitleOffset;
			
/* Mark pick on screen, if it really shows up on screen */			
            if ( pt[1].x >= iChanIDOffset && pt[1].x <= cxScreen )
            {
               pt[0].y = lOffsetY - lTitleFHt/2;
               pt[1].y = lOffsetY + lTitleFHt/2;
               MoveToEx ( hdc, pt[0].x, pt[0].y, NULL );
               LineTo ( hdc, pt[1].x, pt[1].y );
            }
			
/* If pick is off screen, display arrow showing where to go. */
            if ( pt[1].x > cxScreen )          /* It is right of screen */
            {
               pt[0].y = lOffsetY;
               pt[0].x = 97*cxScreen/100;
               pt[1].y = lOffsetY;
               pt[1].x = 99*cxScreen/100;
               MoveToEx( hdc, pt[0].x, pt[0].y, NULL );
               LineTo( hdc, pt[1].x, pt[1].y );
               pt[0].y = lOffsetY - lTitleFHt/2;
               pt[0].x = 98*cxScreen/100;
               MoveToEx( hdc, pt[0].x, pt[0].y, NULL );
               LineTo( hdc, pt[1].x, pt[1].y );
               pt[0].y = lOffsetY + lTitleFHt/2;
               MoveToEx( hdc, pt[0].x, pt[0].y, NULL );
               LineTo( hdc, pt[1].x, pt[1].y );
            }
            if ( pt[1].x < iChanIDOffset )     /* It is left of screen */
            {
               pt[0].y = lOffsetY;
               pt[0].x = iChanIDOffset + 3*cxScreen/100;
               pt[1].y = lOffsetY;
               pt[1].x = iChanIDOffset + 1*cxScreen/100;
               MoveToEx( hdc, pt[0].x, pt[0].y, NULL );
               LineTo( hdc, pt[1].x, pt[1].y );
               pt[0].y = lOffsetY - lTitleFHt/2;
               pt[0].x = iChanIDOffset + 2*cxScreen/100;
               MoveToEx( hdc, pt[0].x, pt[0].y, NULL );
               LineTo( hdc, pt[1].x, pt[1].y );
               pt[0].y = lOffsetY + lTitleFHt/2;
               MoveToEx( hdc, pt[0].x, pt[0].y, NULL );
               LineTo( hdc, pt[1].x, pt[1].y );
            }
         }		
         iCnt++;
      }
   DeleteObject( SelectObject( hdc, hOPen ) );   /* Reset Pen color */
}

 /********************************************************************
  *                     DisplayRTimes()                              *
  *                                                                  *
  * This function displays Rayleigh wave arrival times.              *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  iNumTrace         Number of traces shown per screen             *
  *  cxScreen          Horizontal pixel width of window              *
  *  cyScreen          Vertical pixel height of window               *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *  dScreenEnd        1/1/70 (seconds) time at end of display       *
  *  dScreenTime       # seconds to show on screen                   *
  *  lTitleFHt         Title font height                             *
  *  pRTimes           Rayleigh wave arrival times                   *
  *                                                                  *
  ********************************************************************/
  
void DisplayRTimes( HDC hdc, int iNumStas, int iNumTrace, int cxScreen,
                    int cyScreen, int iVScrollOffset, int iTitleOffset,
                    int iChanIDOffset, double dScreenEnd, double dScreenTime,
                    long lTitleFHt, RTIMES pRTimes[] ) 
{
   double  dScreenStart;         /* 1/1/70 (sec) time at left of screen */
   HPEN    hGPen, hOPen;         /* Pen handles */
   int     i;                    /* Counters */
   long    lOffsetY;             /* Dist to skip on top of screen */
   POINT   pt[2];

/* Compute time at left side of screen */
   dScreenStart = dScreenEnd - dScreenTime;

/* Create font and pens */
   hGPen = CreatePen( PS_SOLID, 2, RGB( 0, 127, 0 ) );   /* Lines Green */
   hOPen = SelectObject( hdc, hGPen );

/* Show Rayleigh wave start times on screen */   
   for ( i=0; i<iNumStas; i++ )
   {
      pt[0].x = iChanIDOffset + (long) (((pRTimes[i].dRStartTime-dScreenStart)
                * (double) (cxScreen-iChanIDOffset)) / dScreenTime);
      pt[1].x = pt[0].x;
      lOffsetY = i*(cyScreen-iTitleOffset)/iNumTrace +
                 iVScrollOffset + iTitleOffset;
			
/* Mark pick on screen, if it really shows up on screen */			
      if ( pt[1].x >= iChanIDOffset && pt[1].x <= cxScreen )
      {
         pt[0].y = lOffsetY - lTitleFHt/2;
         pt[1].y = lOffsetY + lTitleFHt/2;
         MoveToEx ( hdc, pt[0].x, pt[0].y, NULL );
         LineTo ( hdc, pt[1].x, pt[1].y );
      }
   }		
   DeleteObject( SelectObject( hdc, hOPen ) );   /* Reset Pen color */
}

 /********************************************************************
  *                 DisplayTimeLines()                               *
  *                                                                  *
  * This function displays timing lines and times on the display.    *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  lTitleFHt         Title font height                             *
  *  lTitleFWd         Title font width                              *
  *  cxScreen          Horizontal pixel width of window              *
  *  cyScreen          Vertical pixel height of window               *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *  dScreenEnd        1/1/70 (seconds) time at end of display       *
  *  dScreenTime       # seconds to show on screen                   *
  *  dInc              Increment to display time lines               *
  *                                                                  *
  ********************************************************************/
  
void DisplayTimeLines( HDC hdc, long lTitleFHt, long lTitleFWd,
                       int cxScreen, int cyScreen, int iChanIDOffset, 
                       double dScreenEnd, double dScreenTime, double dInc ) 
{
   double  dScreenStart;         /* 1/1/70 (sec) time at left of screen */
   HFONT   hOFont, hNFont;       /* Old and new font handles */
   HPEN    hGPen, hRPen, hOPen;  /* Pen handles */
   long    lTime;                /* 1/1/70 time in seconds in dInc's */
   POINT   pt, pt2;              /* Screen location for outputs */
   char    szTemp[8], szBuffer[8]; /* Time rounded to minutes */
   static  struct  tm *tm;       /* time structure */

/* Create font and pens */
   hNFont = CreateFont( lTitleFHt, lTitleFWd, 
             0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_MODERN, "Elite" );
   hOFont = SelectObject( hdc, hNFont );
   SetTextColor( hdc, RGB( 0, 0, 128 ) );                /* Color times blue */
   hGPen = CreatePen( PS_SOLID, 1, RGB( 0, 128, 0 ) );   /* Lines Green */
   hRPen = CreatePen( PS_SOLID, 1, RGB( 128, 0, 0 ) );   /* Lines Red */
   hOPen = SelectObject( hdc, hRPen );

/* Compute time at left side of screen */
   dScreenStart = dScreenEnd - dScreenTime;
   
/* Get first time after dScreenStart which is an even increment of dInc */   
   lTime = (long) ((dScreenStart+dInc-0.001) / dInc) * (long) dInc;
   
/* Loop thorugh dScreenTime, and draw time lines and times whereever needed */   
   while ( (double) lTime < dScreenEnd )
   {
      tm = TWCgmtime( lTime );
      if ( tm->tm_sec == 0 )       /* Even minute; list time at top */
      {
         SelectObject( hdc, hRPen );
         strcpy( szBuffer, "\0" );
         itoaX( tm->tm_hour, szTemp );
         PadZeroes( 2, szTemp );
         strcpy( szBuffer, szTemp );                            
         strcat( szBuffer, " " );
         itoaX( tm->tm_min, szTemp );
         PadZeroes( 2, szTemp );
         strcat( szBuffer, szTemp );                            
         pt2.x = iChanIDOffset + (long) ((((double) lTime-dScreenStart)/
                 dScreenTime) * (double) (cxScreen-iChanIDOffset)) -
                 (long) ((double) lTitleFWd*2.5);
         pt2.y = lTitleFHt/10;
         TextOut( hdc, pt2.x, pt2.y, szBuffer, strlen( szBuffer ) );
      }
      else SelectObject( hdc, hGPen );
      pt.x = iChanIDOffset + (long) ((((double) lTime-dScreenStart)/
             dScreenTime) * (double) (cxScreen-iChanIDOffset));
      pt.y = 0;
      MoveToEx( hdc, pt.x, pt.y, NULL );
      pt.y = cyScreen;
      LineTo( hdc, pt.x, pt.y );
      lTime += (long) dInc;
   }
   
   DeleteObject( SelectObject( hdc, hOFont ) );  /* Reset font */
   DeleteObject( hGPen );
   DeleteObject( hRPen );
   SelectObject( hdc, hOPen );
}

 /********************************************************************
  *                     DisplayTraces()                              *
  *                                                                  *
  * This function displays seismic traces to the screen and clips    *
  * them if ClipIt is set.                                           *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  Sta               Array of all station data structures          *
  *  Trace             Array of screen display information per trace *
  *  iNumTrace         Number of traces to show on screen            *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  iScreenToDisplay  Screen index (subset of stations) to display  *
  *  iFiltDisplay      1=display filtered data, 0=display broadband  *
  *  iClipIt           1=amplitude limit display, 0=don't limit      *
  *  cxScreen          Horizontal pixel width of window              *
  *  cyScreen          Vertical pixel height of window               *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *  dScreenEnd        1/1/70 (seconds) time at end of display       *
  *  dScreenTime       # seconds to show on screen                   *
  *  piNumShown        # traces displayed on screen                  *
  *                                                                  *
  ********************************************************************/
  
void DisplayTraces( HDC hdc, STATION Sta[], TRACE Trace[], int iNumTrace,
                    int iNumStas, int iScreenToDisplay, int iFiltDisplay, 
                    int iClipIt, int cxScreen, int cyScreen,
                    int iVScrollOffset, int iTitleOffset, int iChanIDOffset,
                    double dScreenEnd, double dScreenTime, int *piNumShown ) 
{
   double  dOldestTime;              /* Time of oldest data in buffer */
   double  dScreenStart;             /* 1/1/70 (sec) time at left of screen */
   HPEN    hWPen, hBPen, hRPen, hOPen;/* Pen handles */
   static  long    i, ii, j, lTemp;  /* Loop counters */
   long    lNum;                     /* # of data points to draw to screen */
   long    lOffsetX;                 /* Dist to skip from left side of screen */
   long    lOffsetY;                 /* Dist to skip on top of screen */
   long    lStart;                   /* Starting buffer index */
   long    lVertRange;               /* Max signal deflection when iClipIt=1 */
   int     iChanIDOffset2; // used to change trace offset from left of screen
                           // necessary for eb4; dln 10/13/06    
   POINT   ptData[DISPLAY_BUFFER_SIZE];  /* Trace display buffer */

///////////////////////////////////////////////////////////////////////////////
// setting a variable offset to accomodate the greater # pixels on eb4
// dln 10/13/06

iChanIDOffset2 = 18*iChanIDOffset/140;


   	if (iChanIDOffset2 > 8 )
	{
	    iChanIDOffset2 = 8;
	}

iChanIDOffset2 = 11*iChanIDOffset2;
///////////////////////////////////////////////////////////////////////////////

/* Create pen and select into device context */
   hBPen = CreatePen( PS_SOLID, 1, RGB( 0,0,0 ) );         /* Black */
   hWPen = CreatePen( PS_SOLID, 1, RGB( 255,255,255 ) );   /* White */
   hRPen = CreatePen( PS_SOLID, 1, RGB( 255,0,0 ) );       /* Red */
   hOPen = SelectObject( hdc, hBPen );
   *piNumShown = 0;

/* Compute time at left side of screen */
   dScreenStart = dScreenEnd - dScreenTime;
   
/* Loop through all stations */
   for ( j=0; j<iNumStas; j++ )
      if ( Trace[j].iStationDisp[iScreenToDisplay] ) /* On this screen? */
      {
/* Compute time of oldest data in buffer */
         dOldestTime = Sta[j].dEndTime -
          ((double) Sta[j].lRawCircSize/Sta[j].dSampRate) + 1./Sta[j].dSampRate;
		 
/* Does the buffer data fit in this screen's time */
         if ( dOldestTime >= dScreenEnd || Sta[j].dEndTime <= dScreenStart )
            goto EndIf;
			
/* Find where on screen we should start plotting */
         if ( dOldestTime <= dScreenStart ) lOffsetX = iChanIDOffset2;
         else lOffsetX = (long) (((double) (cxScreen-iChanIDOffset2) * 
              (dOldestTime-dScreenStart)) / dScreenTime) + 
               iChanIDOffset2;
				
/* Where in the buffer should we start plotting from? */
         if ( dOldestTime < dScreenStart )
            lStart = Sta[j].lSampIndexR-1 -
             (long) ((Sta[j].dEndTime-dScreenStart)*Sta[j].dSampRate+0.00001);
         else               /* Take index of oldest data */
            lStart = Sta[j].lSampIndexR-1 -
             (long) ((Sta[j].dEndTime-dOldestTime)*Sta[j].dSampRate+0.00001);
         while ( lStart < 0 ) lStart += Sta[j].lRawCircSize;
         while ( lStart >= Sta[j].lRawCircSize ) lStart -= Sta[j].lRawCircSize; 
		 
/* How many points should we plot? */
         lNum = (long) (dScreenTime*Sta[j].dSampRate + 0.0001);
		 
/* Adjust lNum so we don't wrap into older data */
         if ( dScreenEnd > Sta[j].dEndTime ) lNum -= (long) ((dScreenEnd- 
              Sta[j].dEndTime)*Sta[j].dSampRate + 0.0001);
         if ( lNum > Sta[j].lRawCircSize ) lNum = Sta[j].lRawCircSize;
         if ( lNum <= 0 ) goto EndIf;
	
/* If we are trying to get more than will fit in DISPLAY_BUFFER, adjust
   lStart to get the latest data */
         if ( lNum > DISPLAY_BUFFER_SIZE ) 	       
         {					       
            lStart += lNum - DISPLAY_BUFFER_SIZE;	       
            lOffsetX += (long) (((double) (cxScreen-iChanIDOffset2) * (double)
             (lNum-DISPLAY_BUFFER_SIZE) / Sta[j].dSampRate) /
             dScreenTime);
            if ( lStart >= Sta[j].lRawCircSize ) lStart -= Sta[j].lRawCircSize; 
            lNum = DISPLAY_BUFFER_SIZE;			
         }							 
	 
/* Compute Y offset from top to center of trace and max signal range */
         lOffsetY = *piNumShown*(cyScreen-iTitleOffset)/iNumTrace +
                    iVScrollOffset + iTitleOffset;
         lVertRange = (cyScreen-iTitleOffset) / (iNumTrace+1);

/* Fill display buffer with data */		 
         if ( lStart >= 0 && lStart < Sta[j].lRawCircSize )
         {
            for ( i=lStart; i<lStart+lNum; i++ )
            {
               lTemp = i - lStart;           /* lTemp is display buffer ctr */
               if ( lTemp >= DISPLAY_BUFFER_SIZE ) break;
               ii = i;                       /* Use ii to stay in bounds */
               if ( ii >= Sta[j].lRawCircSize ) ii -= Sta[j].lRawCircSize;
	       
/* Check to see if we are about to go into older data and load x array */
               if ( lTemp > 0 && ii == Sta[j].lSampIndexR ) break;
               ptData[lTemp].x = lOffsetX + (long) (((cxScreen-iChanIDOffset2)*
                (lTemp/Sta[j].dSampRate)) / dScreenTime);
		
/* Load y array */
               if ( iFiltDisplay == 0 )        /* Broadband data */
                  ptData[lTemp].y = (long) (((double) Sta[j].plRawCircBuff[ii]
                   -Sta[j].dAveLDCRaw) *
                   Trace[j].dVScale/BROADBAND_SCALE)*(-1) + lOffsetY;
               else                            /* Filtered data */
                  ptData[lTemp].y = (long) (((double) Sta[j].plFiltCircBuff[ii]
                   -Sta[j].dAveLDC) * Trace[j].dVScale)*(-1) + lOffsetY;
		   
/* Limit trace deflection if ClipIt was chosen */
               if ( iClipIt == 1 )
               {
                  if ( ptData[lTemp].y - lOffsetY > lVertRange )
                     ptData[lTemp].y = lOffsetY + lVertRange;
                  if ( ptData[lTemp].y - lOffsetY < lVertRange*(-1) )
                     ptData[lTemp].y = lOffsetY - lVertRange;
               }
            }
	    
/* Should this trace be plotted? Is it turned off? If it is alarmed, draw it
   in red. */
            if ( Trace[j].iDisplayStatus == 1 )
            {
               if ( Sta[j].iAlarmStatus == 3 ) SelectObject( hdc, hRPen );
               else                            SelectObject( hdc, hBPen );
               MoveToEx( hdc, ptData[0].x, ptData[0].y, NULL );
               Polyline( hdc, &ptData[0], lTemp );
            }
            else if ( Trace[j].iDisplayStatus == 0 )
            {
               SelectObject( hdc, hWPen );
               MoveToEx( hdc, ptData[0].x, ptData[0].y, NULL );
               Polyline( hdc, &ptData[0], lTemp );
            }
         }
EndIf:;
         *piNumShown = *piNumShown + 1;
      }
   DeleteObject( hBPen );
   DeleteObject( hWPen );
   DeleteObject( hRPen );
   SelectObject( hdc, hOPen );
}
