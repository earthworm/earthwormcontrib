
#
#                     disk_wcatwc's Configuration File
#
MyModId     MOD_DISK_WCATWC_LP # This instance of disk_wcatwc
StaFile        "pick_wcatwc_lp.sta" # File containing stns to be written to disk
StaDataFile    "station.dat"   # Further data on stations
InRing           WAVE_RING_LP  # Transport ring to find waveform data
HeartbeatInt            20     # Heartbeat interval, in seconds
#
FileLength              20     # Length of file in minutes (should be evenly
                               #  divisible into 60 (i.e., 1,2,3,4,5,6,10,...)
DiskWritePath    "e:\datafile" # Data file directory
FileSuffix            ".L"     # File suffix for differentiating different
                               #  sample rates of the same data
#			       
CircDeleteHours          0     # This parameter triggers a thread
                               #  which will delete files older than this
                               #  many hours. Use 0 to turn this off
Debug                    0     # If 1, print debugging message
