/******************************************************************
 *                         File disk_wcatwc.h                     *
 *                                                                *
 *  Include file for disk writer used at the West Coast/Alaska    *
 *  Tsunami Warning Center.  Made into Earthworm module 01/2001.  *
 ******************************************************************/
#include "\earthworm\atwc\src\libsrc\earlybirdlib.h"

#define    TOO_OLD   24*60*60 /* If data is this many seconds behind present,
                                 ignore it */

/* Station list parameters
   ***********************/
typedef struct {
   double  dAlarmAmp;         /* Signal amplitude to exceedin m/s */
   double  dAlarmDur;         /* Duration (sec) signal must exceed dAlarmDur */
   double  dAlarmMinFreq;     /* In hertz; low frequency limit condition */
   double  dClipLevel;        /* Max Counts which signal can attain */
   double  dElevation;        /* Station elevation in meters */
   double  dGainCalibration;  /* Factor which converts SW cal amp to gain */
   double  dLat;              /* Station geographic latitude (+=N, -=S) */
   double  dLon;              /* Station geographic longitude (+=E, -=W) */
   double  dSampRate;         /* Samp rate (samp/s) expected of incoming data */
   double  dScaleFactor;      /* Empirical factor for screen trace scaling */
   double  dSens;             /* Station sensitivity at 1 Hz in cts/m/s */
   double  dTimeCorrection;   /* Transmission time delay (sec) to subtract
                                 from data */
   int     iAlarmStatus;      /* 1=Initialize digital alarm variables, 2=Process
                                 data in alarm, 3=Alarm declared, 0=No alarms*/
   int     iComputeMwp;       /* 1=use this stn for Mwp, 0=don't */
   int     iFiltStatus;       /* 1=Run data through SP filter, 0=don't */
   int     iPickStatus;       /* 0=don't pick, 1=initialize, 2=pick it
                                 3=it's been picked, get mags */
   int     iSignalToNoise;    /* S:N ratio to exceed for P-pick */
   int     iStationType;      /* Model of seismometer:
                                 1 = STS1     360s
                                 2 = STS2     130s
                                 3 = CMG-3NSN 30s
                                 4 = CMG-3T   100s
                                 5 = KS360i   360s
                                 6 = KS5400   350s
                                 7 = CMG-3    30s
                                 8 = CMG-40T  60s
                                 9 = CMG3TNSN 30s 
                                 10 = KS-10   20s
                                 11 = CMG3ESP_30  30s
                                 12 = CMG3ESP_60  60s
				 13 = Trillium  
				 14 = CMG3ESP_120 120s 
				 15 = CMG40T_20   20s 
				 16 = CMG3T_360  360s 
				 17 = KS2000_120 120s 
				 20 = unknown broadband (no cal) 
				 30 = EpiSensor FBA ES-T 
				 40 = GT_S13 
				 41 = MP_L4 
                                 50 = generic LP (no cal)
                                 51 = ATWC LP Response (Hi gain)
                                 52 = ATWC LP Response (Low gain)
                                 100 = Generic SP (no cal)
                                 101 = ATWC SP Response (Hi gain)
                                 102 = ATWC SP Response (Medium gain)
                                 103 = ATWC SP Response (Low gain) */
   char    szChannel[TRACE_CHAN_LEN]; /* Channel identifier (SEED notation) */
   char    szNetID[TRACE_NET_LEN];    /* Network ID */
   char    szStation[TRACE_STA_LEN];  /* Station name */
} STATIOND;

typedef struct {
   char StaFile[64];              /* Name of file with SCN info */
   char StaDataFile[64];          /* Further data on stations */
   long InKey;                    /* Key to ring where waveforms live */
   int  HeartbeatInt;             /* Heartbeat interval in seconds */
   int  Debug;                    /* If 1, print debug messages */
   unsigned char MyModId;         /* Module id of this program */
   int  FileLength;               /* File size in minutes long */
   int  CircDeleteHours;          /* # hours before data delete (0->no delete)*/
   char DiskWritePath[64];        /* Directory to write disk files */
   char FileSuffix[16];           /* Starting letter of file suffix */
   SHM_INFO InRegion;             /* Info structure for input region */
} GPARM;

typedef struct {
   unsigned char MyInstId;        /* Local installation */
   unsigned char GetThisInstId;   /* Get messages from this inst id */
   unsigned char GetThisModId;    /* Get messages from this module */
   unsigned char TypeHeartBeat;   /* Heartbeat message id */
   unsigned char TypeError;       /* Error message id */
   unsigned char TypeWaveform;    /* Waveform buffer for data input */
} EWH;
