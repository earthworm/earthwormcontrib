/* ATWC, 6/19/97: Paul Whitmore and Alex Bittenbinder
         10/23/97; converted to NT, os/2 no longer supported
         8/26/98; added disk file output of all incoming hypocenter info
         1/25/99; accepts TYPE_H71SUM2K message format instead of earlier format
		 10/18/99; prints flinn-engdahl region name for quakes outside atwc AOR
         10/3/01; removed printer writes; now to a file; updated code (removed
                  private ring).
*/


/*       *** version 2.3 ***
 * hypo_print.c: This is a simple print-out program based on the earthworm
                 pager output module.  It is used at the WC&ATWC for a hard-copy
                 printout of locations made at the regional centers.  It also
                 sends the output to a disk file that is monitored by the GIS.
				 
				 In 2001, the printers were removed and locations go to a log
				 file.

                 The magnitude threshold of prints can be set in the .d file.
*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <math.h>
#define MAXLOGO   10
#define MSG_LEN   82
#define ERR_MISSMSG           0
#define ERR_TOOBIG            1
#define ERR_NOTRACK           2
#define ERR_INTERNAL          3
#include "hypo_print.h"

/* Functions in this source file
   *****************************/
int   eqalarm_status( unsigned char, short, char * ); /* Send heartbeats, etc.*/
int   eqalarm_config( char * ); /* Read configuration file */
int   eqalarm_lookup ( void );  /* Convert symbolic names to numeric values */
char *MakeMessage( char * );          /* Formats message */
int   IsItBigDeal( char *, double );  /* Magnitude > threshold? */
int   PrintMessage( char * );         /* Log message */

/* Global Variables
   ****************/
CITY	  city[NUM_CITIES];   /* City locations used in NearestCities */
CITY	  cityEC[NUM_CITIES_EC];/* Eastern City locs used in NearestCitiesEC */
double    dMagThreshold;      /* Mag threshold above which we send a page */
MSG_LOGO  GetLogo[MAXLOGO];   /* Array for requesting module, type, instid */
int       iLogSwitch;         /* 0 if no logging should be done to disk */
time_t    lHeartBeatInterval = 30; /* Seconds between heartbeats */
long      lPublicKey;         /* Key to public memory region for i/o */
time_t    lTimeNow;           /* Present time (1/1/70 seconds) */
time_t    lTimeLastBeat;      /* Time last heartbeat was sent */
pid_t     myPid;              /* Process id of this process */
SHM_INFO  Region1;            /* Shared memory for receiving hypo messages */
short     sNLogo;             /* Number of logos read from ring */
char      szLogFile[64];      /* File with readable quake info */
char      szMapFile[64];      /* File with quake info for EarthVu */
char      szMyModuleId[20];   /* Module id for this module */
char      szRingName[20];     /* Name of transport ring for i/o */
unsigned char ucTypeError;    /* Earthworm error message id */
unsigned char ucTypeHeartBeat;/* Earthworm heartbeat message id */
unsigned char ucInstId;       /* Local installation id */
unsigned char ucMyModId;      /* Our module id */
unsigned char ucTypeSumm;     /* Earthworm H71SUM2K message id */

int main( int argc, char **argv )
{
   int       iRC;                         /* Error flag from getmsg */
   long      lMsgSize;                    /* size of retrieved message  */
   MSG_LOGO  msgLogo;                     /* logo of retrieved message  */
   char     *pszMsgText;                  /* Re-formatted message */
   char      szH71Sum[128];               /* hypo71 summary message */
   char      szMessage[128];              /* actual retrieved message   */
   char      szText[150];                 /* Error text */
		
/* Check command line arguments
   ****************************/
   if ( argc != 2 )
   {
      fprintf( stderr, "Usage: hypo_print <configfile>\n" );
      return -1;
   }
		
/* Read the configuration file
   ***************************/
   if ( eqalarm_config( argv[1] ) < 0 )
   {
      fprintf( stderr, "eqalarm_config() failed. file %s.\n", argv[1] );
      return -1;
   }

/* Look up important info from earthworm.h tables
   **********************************************/
   if ( eqalarm_lookup() < 0 )
   {
      fprintf( stderr, "eqalarm_lookup() failed.\n" );
      return -1;
   }

/* Initialize name of log-file & open it
   *************************************/
   logit_init( "hypo_print", (short) ucMyModId, 256, iLogSwitch );
   logit( "et" , "hypo_print: Read command file <%s>\n", argv[1] );

/* Read WC/ATWC reference cities
   *****************************/
   if ( LoadCities( city, 1 ) < 0 )
   {
      logit( "et", "Couldn't open cities.dat\n" );
      return -1;
   }

/* Read WC/ATWC Eastern reference cities
   *************************************/
   if ( LoadCitiesEC( cityEC, 1 ) < 0 )
   {
      logit( "et", "Couldn't open cities-ec.dat\n" );
      return -1;
   }

/* Get our own pid for restart purposes
   ************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "hypo_print: Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Attach to public HYPO shared memory ring
   ****************************************/
   tport_attach( &Region1, lPublicKey );
   logit( "", "hypo_print: Attached to public memory region <%s>: %ld.\n",
          szRingName, Region1.key );
		  
/* Send first heartbeat
   ********************/
   time( &lTimeLastBeat );
   if ( eqalarm_status( ucTypeHeartBeat, 0, "" ) < 0 )
   {
      fprintf( stderr, "1st hearbeat send failed.\n" );
      tport_detach( &Region1 );
      return -1;
   }
   
/* Flush the input ring
   ********************/
   while ( tport_getmsg( &Region1, GetLogo, sNLogo, &msgLogo, &lMsgSize,
                          szMessage, sizeof( szMessage )-1) != GET_NONE );

/* Loop to read hypocenter messages and load buffer
   ************************************************/
   while ( tport_getflag( &Region1 ) != TERMINATE )
   {
/* Send hypo_print's heartbeat
   ***************************/
      if  ( time( &lTimeNow )-lTimeLastBeat >= lHeartBeatInterval )
      {
         lTimeLastBeat = lTimeNow;
         if ( eqalarm_status( ucTypeHeartBeat, 0, "" ) < 0 )
         {
            logit( "", "hypo_print: Error sending heartbeat\n");
            break;
         }
      }	  
	  
/* Get and process the next HYPO message from shared memory
   ********************************************************/
      iRC = tport_getmsg( &Region1, GetLogo, sNLogo, &msgLogo, &lMsgSize,
                           szMessage, sizeof( szMessage )-1 );
						   
      if ( iRC == GET_NONE )
      {
         sleep_ew( 500 );
         continue;
      }

      if ( iRC == GET_TOOBIG )
      {
         sprintf( szText, "Retrieved msg[%ld] (i%u m%u t%u) too big[%d]",
                  lMsgSize, msgLogo.instid, msgLogo.mod, msgLogo.type,
				  sizeof( szMessage ) );
         eqalarm_status( ucTypeError, ERR_TOOBIG, szText );
         continue;
      }

      if ( iRC == GET_MISS )
      {
         sprintf( szText, "Missed msg(s) i%u m%u t%u region:%ld.",
                  msgLogo.instid, msgLogo.mod, msgLogo.type, Region1.key);
         eqalarm_status( ucTypeError, ERR_MISSMSG, szText );
      }

      if ( iRC == GET_NOTRACK )
      {
         sprintf( szText, "Msg received (i%u m%u t%u); NTRACK_GET exceeded",
                  msgLogo.instid, msgLogo.mod, msgLogo.type );
         eqalarm_status( ucTypeError, ERR_NOTRACK, szText );
      }

      if ( iRC == GET_OK )
      {
         szMessage[lMsgSize] = '\0';           /* Null terminate the message */
         strcpy( szH71Sum, szMessage );        /* Copy message to hy71sum */

/* See if its a significant quake, and put in proper form if it is
   ***************************************************************/
         if ( IsItBigDeal( szH71Sum, dMagThreshold ) == 1 )
         {
            pszMsgText = MakeMessage( szH71Sum );
            logit( "et", "hypo_print has significant event:\n <<%s>> \n",
                   pszMsgText);
            if ( pszMsgText == NULL )
            {
               sprintf( szText, "hypo_print: MakeMessage failed, args: %s ",
                        szH71Sum);
               eqalarm_status( ucTypeError, ERR_INTERNAL, szText);
            }
            if ( PrintMessage( pszMsgText ) < 0 )
            {
               sprintf( szText, "hypo_print: PrintMessage failed, args: %s ",
                        pszMsgText);
               eqalarm_status( ucTypeError, ERR_INTERNAL, szText);
            }
         }
         else logit( "t", "%s\n", szH71Sum );
      }
   }
   
/* Detach from shared memory regions*/
   tport_detach( &Region1 );
   logit( "t", "hypo_print: Termination requested; exitting.\n" );
   return 0;
}

      /*******************************************************
       *                 eqalarm_config()                    *
       *                                                     *
       * Read configuration file.                            *
       *                                                     *
       *  Arguments:                                         *
       *    pszConfigFile Configuration file name            *
       *                                                     *
       *  Returns:      < 0 ->problem in this function, 0->OK*
       *                                                     *
       *******************************************************/

int eqalarm_config( char *pszConfigFile )
{
   char cInit[7];      /* cInit flags, one byte for each required command */
   int  i;
   int  iNCommand;     /* # of required commands you expect */
   int  iNFiles;       /* Number of files (?) */
   int  iNMiss;        /* number of required commands that were missed   */
   int  iSuccess;      /* File counter */
   char *pszCom;       /* Line of text from config file */
   char *pszStr;       /* String that matches an expected variable */


/* Set to zero one cInit flag for each required command
   ****************************************************/
   iNCommand = 7;
   for( i=0; i<iNCommand; i++ ) cInit[i] = 0;
   sNLogo = 0;

/* Open the main configuration file
   ********************************/
   iNFiles = k_open( pszConfigFile );
   if ( iNFiles == 0 )
   {
      fprintf( stderr, "hypo_print: Error opening command file <%s>\n",
               pszConfigFile );
      return -1;
   }

/* Process all command files
   *************************/
   while ( iNFiles > 0 )        /* While there are command files open */
   {
      while ( k_rd() )          /* Read next line from active file  */
      {
         pszCom = k_str();      /* Get the first token from line */
         if ( !pszCom ) continue;               /* Ignore blank lines */
         if ( pszCom[0] == '#' ) continue;      /* Ignore comments */

/* Open a nested configuration file
   ********************************/
         if ( pszCom[0] == '@' )
         {
            iSuccess = iNFiles + 1;
            iNFiles  = k_open( &pszCom[1] );
            if ( iNFiles != iSuccess )
            {
               fprintf( stderr, "hypo_print: Error opening command file <%s>\n",
                        &pszCom[1] );
               return -1;
            }
            continue;
         }

/* Process anything else as a command
   **********************************/
         if ( k_its( "LogSwitch" ) )
         {
            iLogSwitch = k_int();
            cInit[0] = 1;
         }
 		 
         else if ( k_its( "MyModuleId" ) )
         {
            if ( pszStr = k_str() ) strcpy( szMyModuleId, pszStr );
            cInit[1] = 1;
         }
			
         else if ( k_its( "RingName" ) )
         {
            if ( pszStr = k_str() )
            {
               if( (lPublicKey = GetKey( pszStr )) == -1 )
               {
                  fprintf( stderr, "hypo_print: Invalid RingName <%s>. "
                                   "Exitting.\n", pszStr );
                  return -1;
               }
            }
            cInit[2] = 1;
         }

         else if ( k_its( "GetSumFrom" ) )
         {
            if ( sNLogo >= MAXLOGO )
            {
               fprintf( stderr, "hypo_print: Too many GetSumFrom commands - %s"
                                "; max=%d.\n", pszConfigFile, (int) MAXLOGO );
               return -1;
            }
            if ( ( pszStr=k_str() ) )
            {
               if ( GetInst( pszStr, &GetLogo[sNLogo].instid ) != 0 )
               {
                  fprintf( stderr, "hypo_print: Invalid installation name <%s>",
                                   " in <GetSumFrom> cmd; exitting!\n",pszStr );
                  return -1;
               }
            }
            if ( ( pszStr=k_str() ) )
            {
               if ( GetModId ( pszStr, &GetLogo[sNLogo].mod ) != 0 )
               {
                  fprintf( stderr, "hypo_print: Invalid module name <%s>"
                                   " in <GetSumFrom> cmd; exitting!\n",pszStr );
                  return -1;
               }
            }
            if ( ( pszStr=k_str() ) )
            {
               if ( GetType( pszStr, &GetLogo[sNLogo].type ) != 0 )
               {
                  fprintf( stderr, "hypo_print: Invalid message type <%s>"
                                   " in <GetSumFrom> cmd; exitting!\n",pszStr );
                  return -1;
               }
            }
            sNLogo++;
            cInit[3] = 1;
         }

         else if ( k_its( "MagThreshold" ) )
         {
            dMagThreshold = k_val();
            cInit[4] = 1;
         }
 		 
         else if ( k_its( "MapFile" ) )
         {
            if ( pszStr = k_str() ) strcpy( szMapFile, pszStr );
            cInit[5] = 1;
         }
 		 
         else if ( k_its( "LogFile" ) )
         {
            if ( pszStr = k_str() ) strcpy( szLogFile, pszStr );
            cInit[6] = 1;
         }

/* At this point we give up. Unknown thing.
   ****************************************/
         else
         {
            fprintf( stderr, "hypo_print: <%s> Unknown command in <%s>.\n",
                     pszCom, pszConfigFile );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            fprintf( stderr, "hypo_print: Bad <%s> command  in <%s>\n",
                     pszCom, pszConfigFile );
            return -1;
         }
      }
      iNFiles = k_close();
   }

/* After all files are closed, check cInit flags for missed commands
   *****************************************************************/
   iNMiss = 0;
   for ( i=0; i<iNCommand; i++ ) if ( !cInit[i] ) iNMiss++;
   if ( iNMiss )
   {
      fprintf( stderr, "hypo_print: ERROR, no " );
      if ( !cInit[0] )  fprintf( stderr, "<LogSwitch> " );
      if ( !cInit[1] )  fprintf( stderr, "<MyModuleId> " );
      if ( !cInit[2] )  fprintf( stderr, "<RingName> " );
      if ( !cInit[3] )  fprintf( stderr, "<GetSumFrom> " );
      if ( !cInit[4] )  fprintf( stderr, "<MagThreshold> " );
      if ( !cInit[5] )  fprintf( stderr, "<MapFile> " );
      if ( !cInit[6] )  fprintf( stderr, "<LofFile> " );
      fprintf( stderr, "command(s) in <%s>; exitting!\n", pszConfigFile );
      return -1;
   }
   return 0;
}

      /*******************************************************
       *                 eqalarm_lookup()                    *
       *                                                     *
       *      Get parameters from the earthworm.d file.      *
       *                                                     *
       *  Returns:      < 0 ->problem in this function, 0->OK*
       *                                                     *
       *******************************************************/

int eqalarm_lookup()
{

/* Look up installation Id
   ***********************/
   if ( GetLocalInst( &ucInstId ) != 0 )
   {
      fprintf( stderr, "hypo_print: error getting local installation id.\n" );
      return -1;
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( szMyModuleId, &ucMyModId ) != 0 )
   {
      fprintf( stderr, "hypo_print: Invalid module name <%s>\n", szMyModuleId );
      return -2;
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &ucTypeHeartBeat ) != 0 )
   {
      fprintf( stderr, "hypo_print: Invalid message type <TYPE_HEARTBEAT>\n" );
      return -3;
   }
   if ( GetType( "TYPE_ERROR", &ucTypeError ) != 0 )
   {
      fprintf( stderr, "hypo_print: Invalid message type <TYPE_ERROR>\n" );
      return -4;
   }
   if ( GetType( "TYPE_H71SUM2K", &ucTypeSumm ) != 0 )
   {
      fprintf( stderr, "hypo_print: Invalid message type <TYPE_H71SUM2K>\n" );
      return -5;
   }
   return 0;
}

      /*******************************************************
       *                 eqalarm_status()                    *
       *                                                     *
       * Build heartbeat and error messages and put in shared*
       * memory.                                             *
       *                                                     *
       *  Arguments:                                         *
       *    ucType      Message type (heartbeat or error)    *
       *    iErr        Error number                         *
       *    pszNote     Error text                           *
       *                                                     *
       *  Returns:      < 0 ->problem in this function, 0->OK*
       *                                                     *
       *******************************************************/

int eqalarm_status( unsigned char ucType,  short iErr,  char *pszNote )
{
   MSG_LOGO logo;         /* Earthworm message logo to send */
   long     lSize;        /* Size of earthworm message */
   char     szMsg[256];   /* Error or hearbeat message string */
   time_t   t;            /* Present time (1/1/70 seconds) */

/* Build message logo */
   logo.instid = ucInstId;
   logo.mod    = ucMyModId;
   logo.type   = ucType;

/* Create the message */
   time( &t );
   if ( ucType == ucTypeHeartBeat ) sprintf( szMsg, "%ld %ld\n", t, myPid );
   else if ( ucType == ucTypeError )
   {
      sprintf( szMsg, "%ld %hd %s\n", t, iErr, pszNote );
      logit( "t", "hypo_print:  %s\n", pszNote );
   }

/* Send the message */
   lSize = strlen( szMsg );   /* don't include the null byte in the message */
   if ( tport_putmsg( &Region1, &logo, lSize, szMsg ) != PUT_OK )
   {
      if ( ucType == ucTypeHeartBeat ) 
      {
         logit( "et", "hypo_print:  Error sending heartbeat.\n" );
         return -1;
      }
      else if ( ucType == ucTypeError ) 
         logit( "et", "hypo_print:  Error sending error:%d.\n", iErr );
   }
   return 0;
}

      /*******************************************************
       *                 IsItBigDeal()                       *
       *                                                     *
       * Decide if this quake is big enough to bother with.  *
       *                                                     *
       *  Arguments:                                         *
       *    pszMsg      Message from HYPO_RING               *
       *    dMagThresh  Minimum size of quake to bother with *
       *                                                     *
       *  Returns:      1->Big quake, 0->not big enough      *
       *                                                     *
       *******************************************************/
	   
int IsItBigDeal( char *pszMsg, double dMagThresh )
{
   double  dMagVal;                    /* Magnitude from message */
   char    szMagStr[10];               /* Magnitude from message */

/* Extract magnitude from pszMsg */
   strncpy( szMagStr, &pszMsg[47], (size_t) 5);     /* Magnitude */
   szMagStr[7] = (char) NULL;
   dMagVal = atof( szMagStr );

/* And here, the sophisticated decision is made...*/
   if ( dMagVal >= dMagThresh ) return 1;
   return 0;
}

      /*******************************************************
       *                 MakeMessage()                       *
       *                                                     *
       * Convert message from HYPO_71SUM2K format to one that*
       * is more readable.                                   *
       *                                                     *
       *  Arguments:                                         *
       *    pszMsg      Message from HYPO_RING               *
       *                                                     *
       *  Returns:      Character string in readable format  *
       *                                                     *
       *******************************************************/
/*	   
HYPO_71SUM2K FORMAT

1st Col  Length  Type           Description
_______  ______  __________     ___________
0        4       int    %4d     Origin time year
4        2       int    %2d     Origin time month
6        2       int    %2d     Origin time day
8        1                      unused
9        2       int    %2d     Origin time hours
11       2       int    %2d     Origin time minutes
13       6       float  %6.2f   Origin time seconds
19       3       int    %3d     Latitude (degrees)
22       1       char   %c      S for south, blank for north
23       5       float  %5.2f   Latitude (decimal minutes)
28       4       int    %4d     Longitude (degrees)
32       1       char   %c      E for east, blank for west
33       5       float  %5.2f   Longitude (decimal minutes)
38       7       float  %7.2f   Depth (km)
45       1                      unused
46       1       char   %c      Magnitude type; D=duration, Z=low gain duration
47       5       float  %5.2f   Magnitude
52       3       int    %3d     # P&S times with weights > 0.1
55       4       int    %4d     Maximum azimuthal gap
59       5       float  %5.1f   Distance to nearest station
64       5       float  %5.2f   RMS travel time residual
69       5       float  %5.2f   Horizontal error (km)
74       5       float  %5.1f   Vertical error (km)
79       1       char   %c      Remark: Q if quarry blast
80       1       char   %c      Remark: Quality A-D
81       1       char   %c      Remark: Data source code
82       1                      unused
83       10      long   %10ld   Event ID number
93       1                      unused
94       1       char   %c      Version; 0=prelim, 1=Final w/ MD, 2=1+ML etc.
95       1       char   %c      newline character

Output message
          1111111111222222222233333333334444444444555555555566666666667777777777
01234567890123456789012345678901234567890123456789012345678901234567890123456789
yymmdd hh:mm:ss xx xxN xxx xxE Depth:xxx Mag:x.x picks:xxx gap:xxx RMS:xx.xx EW
*/
char *MakeMessage( char *pszMsg )
{
   int     i;
   static  char    szText[MSG_LEN];   

/* Make up a message - from hypo71 summary format to our own 'easy-to-read' */
   for ( i=0; i<MSG_LEN; i++ ) szText[i]=' ';        /* fill with spaces */

/* Date and Time */
   strncpy( &szText[0], &pszMsg[0], (size_t) 8 );    /* yyyymmdd */
   strncpy( &szText[9], &pszMsg[9], (size_t) 2 );    /* hh */
   if ( szText[9] == ' ' ) szText[9] = '0';          /* leading zero */
   szText[11] = ':';
   strncpy( &szText[12], &pszMsg[11], (size_t) 2 );  /* mm */
   if ( szText[12] == ' ' ) szText[12] = '0';        /* leading zero */
   szText[14] = ':';
   strncpy( &szText[15], &pszMsg[13], (size_t) 2 );  /* ss */
   if ( szText[15] == ' ' ) szText[15] = '0';        /* leading zero */

/* Latitude */
   strncpy( &szText[18], &pszMsg[20], (size_t) 2 );  /* degrees latitude */
   strncpy( &szText[21], &pszMsg[23], (size_t) 2 );  /* minutes latitude */
   if ( szText[21] == ' ' ) szText[21] = '0';        /* leading zero */
   if ( szText[22] == ' ' ) szText[22] = '0';        /* leading zero */
   strncpy( &szText[23], &pszMsg[22], (size_t) 1 );  /* the N or the S */
   if ( szText[23] == ' ' ) szText[23] = 'N';        /* Blank=North, S=South */

/* Longitude */
   strncpy( &szText[25], &pszMsg[29], (size_t) 3 );  /* degrees longitude */
   strncpy( &szText[29], &pszMsg[33], (size_t) 2 );  /* minutes longitude */
   if ( szText[33] == ' ' ) szText[33] = '0';        /* leading zero */
   if ( szText[34] == ' ' ) szText[34] = '0';        /* leading zero */
   strncpy( &szText[31], &pszMsg[32], (size_t) 1 );  /* the E or the W */
   if ( szText[31] == ' ' ) szText[31] = 'W';        /* Blank=West, E=East */

/* Depth */
   strncpy( &szText[33], "Depth:", (size_t) 6 );     /* depth label */
   strncpy( &szText[39], &pszMsg[39], (size_t) 3 );  /* depth */

/* Magnitude */
   strncpy( &szText[43], "Mag:", (size_t) 4 );       /* magnitude label */
   strncpy( &szText[47], &pszMsg[48], (size_t) 3 );  /* magnitude */

/* And so forth */
   strncpy( &szText[51], "Picks:", (size_t) 6 );     /* number of picks label */
   strncpy( &szText[57], &pszMsg[52], (size_t) 3 );  /* # of good picks */
   strncpy( &szText[61], "Gap:", (size_t) 4 );       /* gap label */
   strncpy( &szText[65], &pszMsg[56], (size_t) 3 );  /* gap - in degrees (?) */
   strncpy( &szText[69], "RMS:", (size_t) 4 );       /* rms label */
   strncpy( &szText[73], &pszMsg[64], (size_t) 5 );  /* rms */
   if ( szText[73] == ' ' ) szText[73] = '0';        /* leading zero */
   strcpy( &szText[79], "EW" );                      /* our humble signature */

   return( szText );
}

      /*******************************************************
       *                 PrintMessage()                      *
       *                                                     *
       * Take message created in MakeMessage and write it to *
       * any devices (files, printers, ...) that need it.    *
       *                                                     *
       *  Arguments:                                         *
       *    pszMsg      Message from HYPO_RING               *
       *                                                     *
       *  Returns:      < 0 -> failure                       *
       *                                                     *
       *******************************************************/

int PrintMessage( char *pszMsg )
{
   CITYDIS CityDis;           /* Array of nearest city information */
   double  dLat, dLon, dMag;  /* Location and magnitude data */
   FILE   *hFile;             /* File handle */
   int     i, iMonth, iAzimuth, iCnt;
   int     iFERegion;         /* Flinn-Engdahl region number */
   int     iRegion;           /* Procedural region */
   LATLON  ll;                /* Lat/lon converted to geocentrics */
   LATLON  ll1;               /* Lat/lon in geographics */
   long    lTime;             /* 1/1/70 time in seconds */
   PSZ     psz;               /* Pointer to geographic name string */
   char    szFileMsg[MAX_LINES_IN_FILE][MSG_LEN];   /* File array */
   char    szMonth[12][4] = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN",
                             "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
   char    szYear[5], szMonthTemp[3], szDay[3], szHour[3], szMin[3],
           szSec[3], szLatDeg[3], szLatMin[3], szLonDeg[4], szLonMin[3],
           cLat, cLon, szMag[6], szDepth[8], szGap[5], szRMS[7];

/* Read in old file, add new message, and re-write file.  This file keeps
   a list of the last MAX_QUAKES_HYPO earthquakes. */
   iCnt = 0;
   hFile = fopen( szMapFile, "r" );
   if ( hFile == NULL )
   {
      logit( "et", "hypo_print: can't open latest quakes file.\n" );
      return -1;
   }
   while ( !feof( hFile ) )
   {
      if ( !fgets( szFileMsg[iCnt], MSG_LEN, hFile ) ) break;
      iCnt++;
      if ( iCnt == MAX_QUAKES_HYPO ) break;
   }
   if ( iCnt == MAX_QUAKES_HYPO ) iCnt -= 1;
   fclose( hFile );

/* Now, re-write the file with new msg */
   hFile = fopen( szMapFile, "w" );
   if ( hFile == NULL )
      logit( "et", "hypo_print: can't open latest quakes file (2).\n" );
   else
   {
      fprintf( hFile, "%s\n", pszMsg );
      for ( i=0; i<iCnt; i++ )
         fprintf( hFile, "%.*s", strlen( szFileMsg[i] ), szFileMsg[i] );
      fclose( hFile );
   }

/* Output the message in a more readable form to a data file */
/* Read in old file first and save existing data in an array. */
   iCnt = 0;
   hFile = fopen( szLogFile, "r" );
   if ( hFile == NULL )
   {
      logit( "et", "hypo_print: can't open log file.\n" );
      return -1;
   }
   while ( !feof( hFile ) )
   {
      if ( !fgets( szFileMsg[iCnt], MSG_LEN, hFile ) ) break;
      iCnt++;
      if ( iCnt == MAX_LINES_IN_FILE ) break;
   }
   if ( iCnt == MAX_LINES_IN_FILE ) iCnt -= 1;
   fclose( hFile );

/* Now, re-write the file with new quake */
   hFile = fopen( szLogFile, "w" );
   if ( hFile == NULL )
   {
      logit( "et", "hypo_print: can't open log file (2).\n" );
      return -1;
   }
   else
   {
   
/* Convert message string into specific character strings for output */	
      strncpy( szHour, &pszMsg[9], 2 );
      strncpy( szMin, &pszMsg[12], 2 );
      strncpy( szSec, &pszMsg[15], 2 );
      strncpy( szDay, &pszMsg[6], 2 );
      strncpy( szMonthTemp, &pszMsg[4], 2 );
      strncpy( szYear, &pszMsg[0], 4 );
      iMonth = atoi( szMonthTemp );
      strncpy( szLatDeg, &pszMsg[18], 2 );
      strncpy( szLatMin, &pszMsg[21], 2 );
      strncpy( szLonDeg, &pszMsg[25], 3 );
      strncpy( szLonMin, &pszMsg[29], 2 );
      strcat( szLatDeg, "\0" );
      strcat( szLatMin, "\0" );
      strcat( szLonDeg, "\0" );
      strcat( szLonMin, "\0" );
      cLat = pszMsg[23];
      cLon = pszMsg[31];
      dLat = atof( szLatDeg ) + (atof( szLatMin ) / 60.);
      dLon = atof( szLonDeg ) + (atof( szLonMin ) / 60.);
      if ( cLat == 'S' ) dLat *= -1.;
      if ( cLon == 'W' ) dLon = (dLon*(-1.)) + 360.;
      strncpy( szDepth, &pszMsg[39], 3 );
      strcpy( szMag, "     " );
      strncpy( szMag, &pszMsg[47], 3 );
      dMag = atof( szMag );
      strncpy( szGap, &pszMsg[65], 3 );
      iAzimuth = atoi( szGap );
      iAzimuth = 360 - iAzimuth;
      itoaX( iAzimuth, szGap );
      strncpy( szRMS, &pszMsg[73], 5 );

/* Convert lat/lon to geocentric coords. */
      ll1.dLat = dLat;
      ll1.dLon = dLon;
      ll.dLat = dLat;
      ll.dLon = dLon;
      GeoCent( &ll );
      GetLatLonTrig( &ll );

/* Print hypocenter parameters in manner similar to WC/ATWC system */
      time( &lTime );
      fprintf( hFile, "\n------------------------------------------------------"
                      "--------------------------\n" );
      fprintf( hFile, "                     SOLUTION AT %s",
               asctime( TWCgmtime( lTime ) ) );
      fprintf( hFile, "--------------------------------------------------------"
                      "------------------------\n" );
					  
/* Hawaii locations come in Hawaiian standard time, so note it */					  
      if ( dLat > 15. && dLat < 26. && dLon > -162. && dLon < -150. )
         fprintf( hFile, "H(HST)    = %2.2s:%2.2s:%2.2s   %3.3s %2.2s, %4.4s"
                         "                        AVG. RES. = %5s\n"
                         "LOC...    = %2s:%2s%c     %3s:%2s%c               "
                         "              AZIMUTH   = %3s\n",
                         szHour, szMin, szSec, szMonth[iMonth-1], szDay, szYear, 
                         szRMS, szLatDeg, szLatMin, cLat, szLonDeg, szLonMin, 
                         cLon, szGap );
      else						 
         fprintf( hFile, "H(Z)      = %2.2s:%2.2s:%2.2s   %3.3s %2.2s, %4.4s"
                         "                        AVG. RES. = %5s\n"
                         "LOC...    = %2s:%2s%c     %3s:%2s%c               "
                         "              AZIMUTH   = %3s\n",
                         szHour, szMin, szSec, szMonth[iMonth-1], szDay, szYear, 
                         szRMS, szLatDeg, szLatMin, cLat, szLonDeg, szLonMin, 
                         cLon, szGap );
   
/* Compute distance to nearest cities */   
      if ( ll1.dLon < 0 ) ll1.dLon += 360.;
      iRegion = GetRegion( ll1.dLat, ll1.dLon );
      if ( iRegion >= 10 )                  /* WC&ATWC Eastern AOR */
         NearestCitiesEC( &ll, cityEC, &CityDis ); 
      else
         NearestCities( &ll, city, &CityDis );
   
/* Output nearest city and distance if less than 320km away */
      if ( CityDis.iDis[1] < 320 || CityDis.iDis[0] < 320 ) /* Near-by city */
      {
         fprintf( hFile, "LOC...    = %3d miles %2s of %s\n",
                  CityDis.iDis[0], CityDis.pszDir[0], CityDis.pszLoc[0] );
         if ( strnicmp( CityDis.pszLoc[1], CityDis.pszLoc[0], 27 ) )
            fprintf( hFile, "LOC...    = %3d miles %2s of %s\n", 
                    CityDis.iDis[1], CityDis.pszDir[1], CityDis.pszLoc[1]);
      }
      else                     /* Use the general region */
      {
         if ( dLon > 180.0 && dLon < 360.0 ) dLon -= 360.0;
         psz = namnum( dLat, dLon, &iFERegion );
         fprintf( hFile, "LOC...    = %s", psz );
      }
   
      fprintf( hFile, "MAGNITUDE = %3s                                    "
                      "      DEPTH     = %3.3s\n", szMag, szDepth );
      fprintf( hFile, "------------------------------------------------------"
                      "--------------------------\n" );
				 
/* Write the remaining part of the file */				 
      for ( i=0; i<iCnt; i++ )
         fprintf( hFile, "%.*s", strlen( szFileMsg[i] ), szFileMsg[i] );
      fclose( hFile );
   }
   return 0;
}
