#
#       HYPO_PRINT's configuration file
#
 MyModuleId     MOD_HYPO_PRINT       # module id for this import,
 RingName       WAVE_RING            # transport ring to use for input/output,
 LogSwitch              1            # 0 to turn off logging
#
#       List the messages logos to grab from the transport ring
#
#                       Installation    Module          Message Type
GetSumFrom              INST_WILDCARD   MOD_WILDCARD    TYPE_H71SUM2K_WCATWC
GetSumFrom              INST_WILDCARD   MOD_WILDCARD    TYPE_H71SUM2K
#
MagThreshold    2.0     # Print all quakes over this magnitude
MapFile "\DoNotCopyToEB\latest.dat"  # Hypocenter log
LogFile "\DoNotCopyToEB\quake.dat"   # Hypocenter log

