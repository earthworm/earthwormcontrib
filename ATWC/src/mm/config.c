#include <stdio.h>
#include <string.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>
#include "mm.h"

#define ncommand 12          /* Number of commands in the config file */


 /***********************************************************************
  *                              GetConfig()                            *
  *             Processes command file using kom.c functions.           *
  *               Returns -1 if any errors are encountered.             *
  ***********************************************************************/

int GetConfig( char *config_file, GPARM *Gparm )
{
   char     init[ncommand];     /* Flags, one for each command */
   int      nmiss;              /* Number of commands that were missed */
   int      nfiles;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i=0; i<ncommand; i++ ) init[i] = 0;
   strcpy( Gparm->ATPLineupFileLP, "\0" );

/* Open the main configuration file
   ********************************/
   nfiles = k_open( config_file );
   if ( nfiles == 0 )
   {
      fprintf( stderr, "mm: Error opening configuration file <%s>\n",
               config_file );
      return -1;
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          /* Get the first token from line */

         if ( !com ) continue;             /* Ignore blank lines */
         if ( com[0] == '#' ) continue;    /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               fprintf( stderr, "mm: Error opening command file <%s>."
			            "\n", &com[1] );
               return -1;
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "StaFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->StaFile, str );
            init[0] = 1;
         }
		 
         else if ( k_its( "StaDataFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->StaDataFile, str );
            init[1] = 1;
         }

         else if ( k_its( "InRing" ) )
         {
            if ( str = k_str() )
            {
               if( (Gparm->InKey = GetKey(str)) == -1 )
               {
                  fprintf( stderr, "mm: Invalid InRing name <%s>. "
                                   "Exiting.\n", str );
                  return -1;
               }
            }
            init[2] = 1;
         }

         else if ( k_its( "HypoRing" ) )
         {
            if ( str = k_str() )
            {
               if ( (Gparm->HKey = GetKey(str)) == -1 )
               {
                  fprintf( stderr, "mm: Invalid HypoRing name <%s>. "
                                   "Exiting.\n", str );
                  return -1;
               }
            }
            init[3] = 1;
         }

         else if ( k_its( "HeartbeatInt" ) )
         {
            Gparm->HeartbeatInt = k_int();
            init[4] = 1;
         }

         else if ( k_its( "Debug" ) )
         {
            Gparm->Debug = k_int();
            init[5] = 1;
         }

         else if ( k_its( "MyModId" ) )
         {
            if ( str = k_str() )
            {
               if ( GetModId(str, &Gparm->MyModId) == -1 )
               {
                  fprintf( stderr, "mm: Invalid MyModId <%s>.\n", str);
                  return -1;
               }
            }
            init[6] = 1;
         }

         else if ( k_its( "MinutesInBuff" ) )
         {
            Gparm->MinutesInBuff = k_int();
            init[7] = 1;
         }
		 
         else if ( k_its( "DummyFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->DummyFile, str );
            init[8] = 1;
         }
		 
         else if ( k_its( "ResponseFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->ResponseFile, str );
            init[9] = 1;
         }
		 
         else if ( k_its( "RegionFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->RegionFile, str );
            init[10] = 1;
         }
		 
         else if ( k_its( "MwFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->MwFile, str );
            init[11] = 1;
         }
	 
/* Optional command when run with ATPlayer
  ****************************************/	 
         else if ( k_its( "ATPLineupFileLP" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->ATPLineupFileLP, str );
         }		 

/* An unknown parameter was encountered               
   ************************************/
         else
         {
            fprintf( stderr, "mm: <%s> unknown parameter in <%s>\n",
                     com, config_file );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            fprintf( stderr, "mm: Bad <%s> command in <%s>.\n", com,
                     config_file );
            return -1;
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss > 0 )
   {
      fprintf( stderr, "mm: ERROR, no " );
      if ( !init[0] ) fprintf( stderr, "<StaFile> " );
      if ( !init[1] ) fprintf( stderr, "<StaDataFile> " );
      if ( !init[2] ) fprintf( stderr, "<InRing> " );
      if ( !init[3] ) fprintf( stderr, "<HypoRing> " );
      if ( !init[4] ) fprintf( stderr, "<HeartbeatInt> " );
      if ( !init[5] ) fprintf( stderr, "<Debug> " );
      if ( !init[6] ) fprintf( stderr, "<MyModId> " );
      if ( !init[7] ) fprintf( stderr, "<MinutesInBuff> " );
      if ( !init[8] ) fprintf( stderr, "<DummyFile> " );
      if ( !init[9] ) fprintf( stderr, "<ResponseFile> " );
      if ( !init[10] ) fprintf( stderr, "<RegionFile> " );
      if ( !init[11] ) fprintf( stderr, "<MwFile> " );
      fprintf( stderr, "command(s) in <%s>. Exiting.\n", config_file );
      return -1;
   }
   return 0;
}


 /***********************************************************************
  *                              LogConfig()                            *
  *                                                                     *
  *                   Log the configuration parameters                  *
  ***********************************************************************/

void LogConfig( GPARM *Gparm )
{
   logit( "", "\n" );
   logit( "", "StaFile:          %s\n",    Gparm->StaFile );
   logit( "", "StaDataFile:      %s\n",    Gparm->StaDataFile );
   logit( "", "InKey:            %d\n",    Gparm->InKey );
   logit( "", "HKey:             %d\n",    Gparm->HKey );
   logit( "", "HeartbeatInt:     %d\n",    Gparm->HeartbeatInt );
   logit( "", "Debug:            %d\n",    Gparm->Debug );
   logit( "", "MyModId:          %u\n",    Gparm->MyModId );
   logit( "", "MinutesInBuff     %ld\n",   Gparm->MinutesInBuff );
   logit( "", "DummyFile:        %s\n",    Gparm->DummyFile );
   logit( "", "ReseponseFile:    %s\n",    Gparm->ResponseFile );
   logit( "", "MwFile:           %s\n",    Gparm->MwFile );
    if ( strlen( Gparm->ATPLineupFileLP ) > 2 )
      logit( "", "Mm to be used with Player - File: %s\n",
       Gparm->ATPLineupFileLP);
  logit( "", "RegionFile:       %s\n\n",  Gparm->RegionFile );
   return;
}
