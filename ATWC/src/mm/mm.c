
/******************************************************************************
 *  This function orchestrates computation of Mm.  It calls several routines  *
 *  which perform FFT, response application, and compute corrections necessary*
 *  to produce an Mm.  This code needs regional path corrections supplied     *
 *  from elsewhere.  Also, the trace data should be split out and supplie as  *
 *  a long array.  The code was obtained from Stu Weinstein at PTWC.  Most    *
 *  of it was originally developed by Emile Okal from Northwestern U.  It was *
 *  converted into an earthworm module and re-programmed to use WC/ATWC       *
 *  earthworm structures and formats by Whitmore of the WC/ATWC in 11/2001.   *
 ******************************************************************************/
 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <earthworm.h>
#include <transport.h>
#include "mm.h"

/* External Global Variables
   *************************/
extern double  dGroup[NUM_ATTEN][NUM_PATH_PER]; /* Group velocities */
extern double  dPeriod[NUM_PATH_PER];           /* Periods in path corr. file */
extern double  dQQ[NUM_ATTEN][NUM_PATH_PER];    /* Attenuations */
extern int     iCrust[NUM_LAT_BOUNS][NUM_LON_BOUNS]; /* Crustal sections */

  /******************************************************************
   *                           Mm()                                 *
   *                                                                *
   *  Mm computes moment magnitude given an input time series and   *
   *  associated information.                                       *
   *                                                                *
   *  January, 2005: Use frequencies up to 400s dependent on Mwp.   *
   *                                                                *
   *  July, 2003: Don't use frequencies below 170s.                 *
   *                                                                *
   *  Arguments:                                                    *
   *    lNSamps     Number of samples to evaluate                   *
   *    plData      Data array                                      *
   *    MmStuff     Station response structure (poles, zeroes)      *
   *    dSampRate   Trace sample rate (samp/s)                      *
   *    pszStation  Station name                                    *
   *    dLat        Epicenter geocentric latitude                   *
   *    dLon        Epicenter geocentric longitude                  *
   *    SpectraT    Spectral amplitudes and periods                 *
   *    dMag        Quake's magnitude to this point                 *
   *                                                                *
   *  Returns:                                                      *
   *    double      Mm magnitude                                    *
   *                                                                *
   ******************************************************************/

double Mm( long lNSamps, long *plData, MMSTUFF *MmStuff, double dSampRate,
           char *pszStation, double dLat, double dLon, SPECTRA *SpectraT,
           double dMag )
{
   AZIDELT az;                      /* Distance/azimuth structure */
   double  dAbsVal;                 /* Displacement for each period */
   double  dAtten;                  /* Attenuation for path corr. */
   double  dAvgMm;                  /* Average Mm */
   double  dAx, dBx;                /* Path correction variables */
   double  dCdju, dP1, dP2;         /* Path correction factors */
   double  dDf;                     /* Temp FFT variable */
   double  dFqCnt;                  /* Freq increments for spectra */
   static  double  dFrMax = 1./50., dFrMin;    /* Freq. window (Hz) */
   double  dMm[MAX_FFT_PER];        /* Mm for each per */
   double  dMmMax;                  /* Max Mm over all periods */
   double  dPathCorrT[MAX_FFT_PER]; /* Path correction for each per */
   double  dPathCorr[NUM_ATTEN];    /* Crustal zones travelled by surface
                                       waves from source to station */
   double  dPathInc;                /* Stepping increment for path correction*/
   double  dPer[MAX_FFT_PER];       /* Periods (s) examined */
   double  dResp;                   /* Response factor */
   double  dStdDev;                 /* Std Dev. of Mms */
   double  dSourceCorr[MAX_FFT_PER];/* Source correction for each per */
   static  double  dSPer0 = 1.8209,	dSa0 = 3.7411, dSa1 = 0.42861,
                   dSa2 = -0.8332, dSa3 = 1.6163;/* Shallow source parameters */
   double  dSumMm;                  /* Mm summation */
   double  dSumMmStdDev;            /* Mm summation for std. dev.*/
   static  double  dTBeg = 0.05, dTEnd = 0.1;/* Taper length at start & end */
   double  dTheta;                  /* Source correction variable */
   double  dU;                      /* Velocity for this period */
   float   fData[MAX_FFT];          /* Data array converted to floats */
   int     i, j;
   int     iCrustType;              /* Average crust properties for 10x10
                                       degree sections of earth: 1-4=ocean,
                                       5=shiels, 6=mountains, 7=trench */
   int     iDist;                   /* Epicentral distance rounded up */
   int     iFFTOrder;               /* Order of FFT */
   int     iLat, iLon;              /* Indices relating to REGIONS file */
   int     iMaxPerIndex;            /* Period index of maximum Mm */
   int     iNFMax, iNFMin;          /* FFT bounding indices */
   int     iNk;                     /* Path correction variables */
   int     iNPath;                  /* # steps in path correction */
   int     iNumFFTPts;              /* # pts. in FFT (=2**iFFTOrder) */
   int     iTest;                   /* Error test */
   int     jFqCnt, ju, k;           /* Counters */
   LATLON  ll1, ll2, ll2GG;         /* Lat/lon structures */
   static  fcomplex z[MAX_FFT];     /* Data array converted to complex */
   fcomplex zres;                   /* Spectral amp. with response removed */

/* Set minimum frequency based on Mwp
   **********************************/
   if ( dMag > 7.5 ) dFrMin = 1./410.;
   else              dFrMin = 1./171.;

/* Make a few checks on the data
   *****************************/
   if ( (double) lNSamps/dSampRate < MM_MIN_TIME )
   {
      logit( "", "%s not enough samples - %ld\n", pszStation, lNSamps );
      return 0.0;
   }
   if ( MmStuff->iNPole == 0 && MmStuff->iNZero == 0 )
   {
      logit( "", "%s no poles or zeros\n", pszStation );
      return 0.0;
   }
   if ( MmStuff->azdelt.dDelta < 15. )
   {
      logit( "", "%s too close for Mm - %lf\n",
             pszStation, MmStuff->azdelt.dDelta );
      return 0.0;
   }

/* Compute FFT order from number of points
   ***************************************/
   myfnd( lNSamps, &iNumFFTPts, &iFFTOrder );
   if ( iNumFFTPts == 0 || iFFTOrder == 0 )
   {
      logit( "", "%s FFT=0\n", pszStation );
      return 0.0;
   }
   if ( iNumFFTPts > MAX_FFT )
   {
      logit( "", "%s FFT > MAX_FFT (%ld)\n", pszStation, iNumFFTPts );
      return 0.0;
   }
   
/* Convert data array to real (for compatibility with PTWC)
   ********************************************************/
   for ( j=0; j<lNSamps; j++ )
      fData[j] = (float) plData[j];
   
/* Attempt to remove any DC Offset
   *******************************/
   mydtr( fData, lNSamps );
	
/* Cosine taper the data array
   ***************************/
   mytpr( fData, lNSamps, dTBeg, dTEnd );
	
/* Fill in complex array; first initialize it
   ******************************************/
   for ( j=0; j<iNumFFTPts; j++ )
   {
      z[j].r = (float) 0.;
      z[j].i = (float) 0.;
   }
   for ( j=0; j<lNSamps; j++ )
   {
      z[j].r = fData[j];
      z[j].i = (float) 0.;
   }
	  
/* Perform FFT
   ***********/
   coolb( iFFTOrder, z, -1. );
/*   if ( dMag > 7.5 ) dDf = 1. / ((1./dSampRate)*(double) iNumFFTPts);
   else              dDf = 0.5 / ((1./dSampRate)*(double) iNumFFTPts); */
   dDf = 1. / ((1./dSampRate)*(double) iNumFFTPts);
   if ( dDf <= 0. )
   {
      logit( "", "%s dDf <= 0., sr=%lf, iNumFFTPts=%ld\n", pszStation,
             dSampRate, iNumFFTPts );
      return 0.0;
   }

/* Defining bounding indices in FFT series
   ***************************************/
   iNFMin = (int) (dFrMin/dDf + 1.5);
   iNFMax = (int) (dFrMax/dDf + 1.5);
   
/* Compute the contribution of crustal sections to the path correction
   *******************************************************************/
   ll1.dLat = dLat;
   ll1.dLon = dLon;
   for ( i=0; i<NUM_ATTEN; i++ ) dPathCorr[i] = 0.;
   iDist = (int) (MmStuff->azdelt.dDelta + 1.0);
   dPathInc = MmStuff->azdelt.dDelta / (double) iDist;
   iNPath = iDist + 1;
   for ( j=1; j<iNPath+1; j++ )
   {
      az.dAzimuth = MmStuff->azdelt.dAzimuth * RAD;
      az.dDelta = dPathInc * ((double) j-1) * RAD;
      ll2 = PointToEpi( &ll1, &az );
      GeoGraphic( &ll2GG, &ll2 );
      while (ll2GG.dLon < 0.0) ll2GG.dLon += 360.0;
      iLon = (int) (ll2GG.dLon / 10.0);
      iLat = (int) ((90.0-ll2GG.dLat) / 10.0);
      iCrustType = iCrust[iLat][iLon];
      if ( j == 1 || j == iNPath ) dPathCorr[iCrustType-1] += .5*dPathInc;
      else                         dPathCorr[iCrustType-1] += dPathInc;
   }
	
/* Loop Over All Frequencies & compute Mm
   **************************************/
   dMmMax = 0.;
   ju = 0;
   dSumMm = 0.;
   dSumMmStdDev = 0.;                                                                
   iMaxPerIndex = -1;
   SpectraT->iNumPers = 0;
   for ( jFqCnt=iNFMin; jFqCnt<iNFMax+1; jFqCnt++ )
   {
      dFqCnt = ((float) (jFqCnt-1)) * dDf;
      if ( dFqCnt < dFrMin || dFqCnt > dFrMax ) continue; 
      dPer[ju] = 1./dFqCnt;
      dResp = 1.;
      resgeo( dFqCnt, MmStuff->dAmp0, MmStuff->iNZero, MmStuff->iNPole,
              MmStuff->zZeros, MmStuff->zPoles, &zres );
      dResp = (double) Cabs( zres );
      dResp /= 100.;        /* Convert from counts per meter to counts per cm */
      if ( dResp == 0. ) continue;
      dAbsVal = (double) (Cabs( z[jFqCnt-1] )) / dResp;
      dAbsVal = fabs( dAbsVal*(1./dSampRate) );
      if ( ju < MAX_SPECTRA )
      {
         SpectraT->dAmp[ju] = dAbsVal;
         SpectraT->dPer[ju] = dPer[ju];
         SpectraT->iNumPers += 1;
      }
      dAbsVal *= 10000.;
      if ( dAbsVal <= 0. )
      {
         logit( "", "%s amplitude <= 0. (%lf) \n", pszStation, dAbsVal );
         return 0.0;
      }
	  
/* Compute Source Correction
   *************************/
      if ( dPer[ju] <= 0. )
      {
         logit( "", "%s Period <= 0., ju=%ld\n", pszStation, ju );
         return 0.0;
      }
      dTheta = log10( dPer[ju] ) - dSPer0;
      dSourceCorr[ju] = dSa3*dTheta*dTheta*dTheta + dSa2*dTheta*dTheta;
      dSourceCorr[ju] = dSourceCorr[ju] + dSa1*dTheta + dSa0;
		
/* Compute Distance Correction
   ***************************/
      mysert( dPer[ju], dPeriod, NUM_PATH_PER, &dAx, &dBx, &iNk, &iTest );
      if ( iTest != 1 )
      {
         dAtten = 0.;
         for ( k=0; k<NUM_ATTEN; k++ )
         {
            dU = dGroup[k][iNk-1]*dAx + dGroup[k][iNk]*dBx;
            if ( dU == 0. || dQQ[k][iNk-1] == 0. || dQQ[k][iNk] == 0. )
               continue;
            dAtten += PI * dFqCnt * dPathCorr[k] * DEGTOKM *
                     (dAx/dQQ[k][iNk-1] + dBx/dQQ[k][iNk]) / dU;
         }		 
         dP1 = exp( dAtten );
         dP2 = MmStuff->azdelt.dDelta * PI / 180.0;
         dP2 = (dP2 > 0.0) ? dP2 : -dP2;
         dCdju = sqrt( sin( dP2 ) ) * dP1;
         if ( dCdju <= 0. )
         {
            logit( "", "%s cDju <= 0.\n", pszStation );
            return 0.0;
         }
         dPathCorrT[ju] = log10( dCdju );
		 
/* Compute Mm at period indexed ju
   *******************************/
         dMm[ju] = log10( dAbsVal ) + dPathCorrT[ju] + dSourceCorr[ju] - 0.90;
         dSumMm += dMm[ju];
         dSumMmStdDev = dSumMmStdDev + dMm[ju]*dMm[ju];
         if ( dMmMax < dMm[ju] ) iMaxPerIndex = ju;   /* Save maximums */
         if ( dMmMax < dMm[ju] ) dMmMax = dMm[ju];
         ju++;
         continue;
      }
      ju++;
   }
   
/* Compute average Mm for this station
   ***********************************/   
   if ( ju > 0 )
   {
      dAvgMm = dSumMm / (double) ju;
      dStdDev = sqrt((dSumMmStdDev - ((double) ju*dAvgMm*dAvgMm)) / (double)ju);
   }
   else
   {
      dAvgMm = 0.;
      dStdDev = 0.;
   }
   if ( iMaxPerIndex >= 0 && iMaxPerIndex < MAX_FFT_PER)
      MmStuff->dPerMax = dPer[iMaxPerIndex];
   else MmStuff->dPerMax = 0.;
   logit( "", "%s Average Mm %lf +- %lf Maximum Mm %lf at T= %lf\n",
          MmStuff->szStation, dAvgMm, dStdDev, dMmMax, MmStuff->dPerMax);
	
/* Return the maximum Mm
   *********************/
   return dMmMax; 
}

  /******************************************************************
   *                       RdRegion()                               *
   *                                                                *
   *  Read regionalized period, velocity, and attenuation file.     *
   *                                                                *
   *  Arguments:                                                    *
   *    pszRegionFile File name of regionalized path data           *
   *                                                                *
   *  Returns:                                                      *
   *    int         1=OK, 0=no good                                 *
   *                                                                *
   ******************************************************************/
   
int RdRegion( char *pszRegionFile )
{
   FILE    *hFile;
   int     i, j, iTemp;

/* Open regional path correction data file */
   hFile = fopen( pszRegionFile, "r" );   
   if ( hFile == NULL )
   {
      logit( "", "%s file could not open\n", pszRegionFile );
	  return 0;
   }
   
/* Read in periods with attenuations and group velocity */   
   for ( i=0; i<NUM_PATH_PER; i++ )
   {
      fscanf( hFile, " %ld", &iTemp );
      dPeriod[i] = (double) iTemp;	  
   }

/* Read indexed crustal regions */   
   for ( i=0; i<NUM_LAT_BOUNS; i++ )
   {
      for ( j=0; j<NUM_LON_BOUNS; j++ )
         fscanf( hFile, " %ld", &iCrust[i][j] );
      fscanf( hFile, "\n" );
   }
   
/* Read regionalized group vel. and Q for each period/region */
   for ( i=0; i<NUM_PATH_PER; i++ )
   {
      fscanf( hFile, " %ld", &iTemp );
      for ( j=0; j<NUM_ATTEN; j++ )
      {	  
         fscanf( hFile, " %lf %ld", &dGroup[j][i], &iTemp );
         dQQ[j][i] = (double) iTemp;	  
      }
      fscanf( hFile, "\n" );
   }
   fclose( hFile );
   return 1;
}
