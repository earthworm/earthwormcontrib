#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <earthworm.h>
#include <transport.h>
#include <trace_buf.h>
#include "mm.h"

  /***************************************************************
   *                         GetStaList()                        *
   *                                                             *
   *                     Read the station list                   *
   *                                                             *
   *  Returns -1 if an error is encountered.                     *
   ***************************************************************/

int GetStaList( STATION **Sta, int *Nsta, GPARM *Gparm, MMSTUFF **MmT )
{
   double  dTemp;                 /* Values in file, not needed here */
   char    string[180];
   int     i;
   int     iReturn;
   int     ndecoded;
   int     nsta;
   MMSTUFF *Mmtemp;
   STATION *sta;
   STATION StaTemp;
   FILE    *fp;

/* Open the station list file
   **************************/
   if ( ( fp = fopen( Gparm->StaFile, "r") ) == NULL )
   {
      logit( "et", "mm: Error opening station list file <%s>.\n",
             Gparm->StaFile );
      return -1;
   }

/* Count channels in the station file.
   Ignore comment lines and lines consisting of all whitespace.
   ************************************************************/
   nsta = 0;
   while ( fgets( string, 160, fp ) != NULL )
   {
      if ( IsComment( string ) ) continue;
      ndecoded = sscanf( string,"%s %s %s %d %d %d %d %lf %lf %lf %d %lf %lf\n",
                 StaTemp.szStation, StaTemp.szChannel, StaTemp.szNetID,
                 &StaTemp.iPickStatus, &StaTemp.iFiltStatus, 
                 &StaTemp.iSignalToNoise, &StaTemp.iAlarmStatus, 
                 &StaTemp.dAlarmAmp, &StaTemp.dAlarmDur,
                 &StaTemp.dAlarmMinFreq, &StaTemp.iComputeMwp, &dTemp, &dTemp );
      if ( ndecoded < 13 )
      {
         logit( "et", "mm: Error decoding station file. - 1\n" );
         logit( "e", "ndecoded: %d\n", ndecoded );
         logit( "e", "Offending line:\n" );
         logit( "e", "%s\n", string );
         return -1;
      }
      nsta++;
   }
   rewind( fp );

/* Allocate the station list
   *************************/
   if ( nsta > MAX_STATIONS )
   {
      logit( "et", "Too many stations in Station file\n" );
      return -1;
   }   
   sta = (STATION *) calloc( nsta, sizeof(STATION) );
   if ( sta == NULL )
   {
      logit( "et", "mm: Cannot allocate the station array\n" );
      return -1;
   }
   Mmtemp = (MMSTUFF *) calloc( nsta, sizeof(MMSTUFF) );
   if ( Mmtemp == NULL )
   {
      logit( "et", "mm: Cannot allocate the response array\n" );
      return -1;
   }

/* Read stations from the station list file into the station
   array, including parameters used by the picking algorithm
   *********************************************************/
   i = 0;
   while ( fgets( string, 160, fp ) != NULL )
   {
      if ( IsComment( string ) ) continue;
      ndecoded = sscanf( string,"%s %s %s %d %d %d %d %lf %lf %lf %d %lf %lf\n",
                 sta[i].szStation, sta[i].szChannel, sta[i].szNetID,
                 &sta[i].iPickStatus, &sta[i].iFiltStatus, 
                 &sta[i].iSignalToNoise, &sta[i].iAlarmStatus, 
                 &sta[i].dAlarmAmp, &sta[i].dAlarmDur,
                 &sta[i].dAlarmMinFreq, &sta[i].iComputeMwp, &dTemp,
                 &sta[i].dScaleFactor );
      if ( ndecoded < 13 )
      {
         logit( "et", "mm: Error decoding station file.\n" );
         logit( "e", "ndecoded: %d\n", ndecoded );
         logit( "e", "Offending line:\n" );
         logit( "e", "%s\n", string );
         return -1;
      }
      if ( LoadStationData( &sta[i], Gparm->StaDataFile ) == -1 )
      {
         logit( "et", "mm: No match for scn in station info file.\n" );
         logit( "e", "file: %s\n", Gparm->StaDataFile );
         logit( "e", "scn = %s %s %s\n", sta[i].szStation, sta[i].szChannel,
		        sta[i].szNetID );
         return -1;
      }
      iReturn = LoadResponseData( &sta[i], &Mmtemp[i], Gparm->ResponseFile );
      if ( iReturn == -1 )
      {
         logit( "e", "file: %s\n", Gparm->ResponseFile );
         logit( "e", "scn = %s %s %s\n", sta[i].szStation, sta[i].szChannel,
		        sta[i].szNetID );
         return -1;
      }
      else if ( iReturn == 0 )
         logit( "", "scn = %s %s %s - No RESPONSE info\n", sta[i].szStation,
                sta[i].szChannel, sta[i].szNetID );
      else if ( iReturn == 1 )
         logit( "", "scn = %s %s %s - RESPONSE info read\n", sta[i].szStation,
                sta[i].szChannel, sta[i].szNetID );
      InitVar( &sta[i] );	  
      sta[i].dEndTime = 0.;
      sta[i].iFirst = 1;
      i++;
   }
   fclose( fp );
   *Sta  = sta;
   *Nsta = nsta;
   *MmT = Mmtemp;
   return 0;
}

  /***************************************************************
   *                       LoadResponseData()                    *
   *                                                             *
   *  Read response information in poles/zeroes form.            *
   *                                                             *
   *  Returns -1 if an error is encountered, 0 if no match found.*
   ***************************************************************/

int LoadResponseData( STATION *Sta, MMSTUFF *MmT, char *pszInfoFile )
{
   double  dTemp; 
   FILE    *hFile;
   int     i, iTemp, j;
   int     iUse;                /* Flag indicating if this response is good */
   MMSTUFF Mmtemp;             /* Temp MMSTUFF array */
   char    szLine[128];

/* First, intialize information in the response structure
   ******************************************************/
   strcpy( MmT->szStation, Sta->szStation );
   strcpy( MmT->szChannel, Sta->szChannel );
   strcpy( MmT->szNetID, Sta->szNetID );
   MmT->dAmp0 = 0.;
   MmT->iNZero = 0;
   MmT->iNPole = 0;
   for ( i=0; i<MAX_ZP; i++ )
   {
      MmT->zPoles[i] = Complex( 0., 0. );
      MmT->zZeros[i] = Complex( 0., 0. );   
   }
   MmT->dMmTravTime = 0.;
   MmT->dMmStartTime = 0.;
   MmT->dMmEndTime = 0.;
   MmT->dMmMax = 0.;
   MmT->dPerMax = 0.;
         
/* Open the response file
   **********************/
   if ( (hFile = fopen( pszInfoFile, "r")) == NULL )
   {
      logit( "et", "mm: Error opening response file <%s>.\n",
             pszInfoFile );
      return -1;
   }

/* Read response data from the response file 
   *****************************************/
   iUse = 0;
   while ( !feof( hFile ) )
   {
      if ( fgets( szLine, 126, hFile ) == NULL ) break;
      sscanf( szLine, "%*s %s %s %*s %*s %*lf %*lf %*lf %*s %*s %ld",
              Mmtemp.szStation, Mmtemp.szChannel, &iUse );
      if ( fgets( szLine, 126, hFile ) == NULL ) break;
      sscanf( szLine, "%lf %lf %ld", &dTemp, &dTemp, &iTemp );
      if ( fgets( szLine, 126, hFile ) == NULL ) break;
      sscanf( szLine, "%*s %*d %*c %lf %d %d", &Mmtemp.dAmp0, &Mmtemp.iNPole,
              &Mmtemp.iNZero);			  
      for ( j=0; j<Mmtemp.iNPole; j++ )
      {
         if ( fgets( szLine, 126, hFile ) == NULL ) break;
         sscanf( szLine, "%e %e", &Mmtemp.zPoles[j].r, &Mmtemp.zPoles[j].i );
      }
      for ( j=0; j<Mmtemp.iNZero; j++ )
      {
         if ( fgets( szLine, 126, hFile ) == NULL ) break;
         sscanf( szLine, "%e %e", &Mmtemp.zZeros[j].r, &Mmtemp.zZeros[j].i );
      }
      Mmtemp.dAmp0 *= 1.0e9;

if (!strcmp (Sta->szStation, "COLA"))
    logit( "", "iUse=%ld, stn=%s, chn=%s\n", iUse, Mmtemp.szStation, Mmtemp.szChannel);
      
      
      if ( !strcmp( Mmtemp.szStation, Sta->szStation ) &&
           !strcmp( Mmtemp.szChannel, Sta->szChannel ) && iUse == 1 )
      {
         MmT->dAmp0 = Mmtemp.dAmp0;
         MmT->iNPole = Mmtemp.iNPole;
         MmT->iNZero = Mmtemp.iNZero;
         for ( j=0; j<Mmtemp.iNPole; j++ )
         {
            MmT->zPoles[j].r = Mmtemp.zPoles[j].r;
            MmT->zPoles[j].i = Mmtemp.zPoles[j].i;
         }
         for ( j=0; j<Mmtemp.iNZero; j++ )
         {
            MmT->zZeros[j].r = Mmtemp.zZeros[j].r;
            MmT->zZeros[j].i = Mmtemp.zZeros[j].i;
         }
         fclose( hFile );
         return 1;
      }
      if ( fgets( szLine, 126, hFile ) == NULL ) break;
   }	  
   fclose( hFile );
   return 0;
}

  /***************************************************************
   *                       LoadStationData()                     *
   *                                                             *
   *       Get data on station from information file             *
   *                                                             *
   *  Returns -1 if an error is encountered or no match is found.*
   ***************************************************************/

int LoadStationData( STATION *Sta, char *pszInfoFile )
{
   char    string[180];
   int     ndecoded;
   FILE    *hFile;
   char    szChannel[TRACE_CHAN_LEN], szStation[TRACE_STA_LEN],
           szNetID[TRACE_NET_LEN];

/* Open the station data file
   **************************/
   if ( ( hFile = fopen( pszInfoFile, "r") ) == NULL )
   {
      logit( "et", "mm: Error opening station data file <%s>.\n",
             pszInfoFile );
      return -1;
   }

/* Read station data from the station data file 
   ********************************************/
   while ( fgets( string, 160, hFile ) != NULL )
   {
      ndecoded = sscanf( string, "%s %s %s", szStation, szChannel,
	             szNetID);
      if ( ndecoded < 3 )
      {
         logit( "et", "mm: Error decoding station data file.\n" );
         logit( "e", "ndecoded: %d\n", ndecoded );
         logit( "e", "Offending line:\n" );
         logit( "e", "%s\n", string );
         fclose( hFile );
         return -1;
      }
	  
/* Compare SCNs
   ************/	  
      if ( !strcmp (szStation, Sta->szStation ) &&
	       !strcmp (szChannel, Sta->szChannel ) &&
	       !strcmp (szNetID, Sta->szNetID ) )
      {     /* We have a match */
         ndecoded = sscanf( string, "%s %s %s %lf %lf %lf %lf %lf %lf %lf %d",
                    Sta->szStation, Sta->szChannel, Sta->szNetID,
                    &Sta->dSens, &Sta->dGainCalibration, 
                    &Sta->dLat, &Sta->dLon, &Sta->dElevation,
                    &Sta->dClipLevel, &Sta->dTimeCorrection,					
                    &Sta->iStationType);
         if ( ndecoded < 11 )
         {
            logit( "et", "mm: Error decoding station data file-2.\n" );
            logit( "e", "ndecoded: %d\n", ndecoded );
            logit( "e", "Offending line:\n" );
            logit( "e", "%s\n", string );
            fclose( hFile );
            return -1;
         }
         fclose( hFile );
         return 0;
      }
   }
   fclose( hFile );
   return -1;
}

 /***********************************************************************
  *                             LogStaList()                            *
  *                                                                     *
  *                         Log the station list                        *
  ***********************************************************************/

void LogStaList( STATION *Sta, int Nsta )
{
   int i;

   logit( "", "\nStation List:\n" );
   for ( i = 0; i < Nsta; i++ )
   {
      logit( "", "%4s",     Sta[i].szStation );
      logit( "", " %3s",    Sta[i].szChannel );
      logit( "", " %2s",    Sta[i].szNetID );
      logit( "", " %1d",    Sta[i].iAlarmStatus );
      logit( "", " %lf",    Sta[i].dScaleFactor );
      logit( "", "\n" );
   }
   logit( "", "\n" );
}

    /*************************************************************************
     *                             IsComment()                               *
     *                                                                       *
     *  Accepts: String containing one line from a mm station list           *
     *  Returns: 1 if it's a comment line                                    *
     *           0 if it's not a comment line                                *
     *************************************************************************/

int IsComment( char string[] )
{
   int i;

   for ( i = 0; i < (int)strlen( string ); i++ )
   {
      char test = string[i];

      if ( test!=' ' && test!='\t' && test!='\n' )
      {
         if ( test == '#'  )
            return 1;          /* It's a comment line */
         else
            return 0;          /* It's not a comment line */
      }
   }
   return 1;                   /* It contains only whitespace */
}
