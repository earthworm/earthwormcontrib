#
#                     Mm's Configuration File
#
MyModId         MOD_MM         # This instance of mm
InRing        WAVE_RING_LP     # Input waveform ring (input)
HypoRing         HYPO_RING     # Hypocenter ring (input)
StaFile        "pick_wcatwc_lp.sta" # File containing stations to be processed
StaDataFile    "station.dat"   # File with information about stations
HeartbeatInt            20     # Heartbeat interval, in seconds
#    Add file created in ATPlayer if used in conjunction with player (optional)
#    Comment this line if using mm in real-time mode
#ATPLineupFileLP "\earthworm\run\params\ATP-LP.sta" # Station config file
#
MinutesInBuff          180     # Time (minutes) to allocate for each trace 
#
DummyFile    "d:\seismic\erlybird\dummyX.dat"  # WC/ATWC EarlyBird dummy file
ResponseFile "c:\earthworm\run\params\calibs"  # Broadband response file for Mm
RegionFile   "c:\earthworm\run\params\regions" # Mm path correction file
MwFile       "c:\earthworm\atwc\src\mm\mw.dat" # Mw results file
#
Debug                    1     # If 1, print debugging message
