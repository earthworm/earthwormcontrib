#include <stdio.h>
#include <math.h>
#include <string.h>
#include <earthworm.h>
#include <transport.h>
#include "mm.h"

  /******************************************************************
   *                       routines.c                               *
   *                                                                *
   *  These routines perform FFTs, filtering, and corrections       *
   *  associated with the computations of Mm.  The routines were    *
   *  provided to the WC/ATWC by Weinstein of PTWC, but originally  *
   *  were from Okal and were programmed in FORTRAN.  They were     *
   *  converted to C by Whitmore at WC/ATWC in 11/2001.             * 
   *                                                                *
   ******************************************************************/

/* FFT program */
void coolb( int nn, fcomplex zData[], double dSign )
{
   double  dTheta, dSinTheta, dSinR, dSinI, dWR, dWI;
   int     i, j, mm;       /* Based on 0 */
   int     iStep;
   int     m, n, mMax;     /* Based on 1 */
   fcomplex zTemp;
   
   n = (int) (pow( 2., (double) nn ) + 0.05);
   j = 0;
   for ( i=0; i<n; i++ )
   {
      if ( i-j < 0 )
      {
         zTemp.r = zData[j].r;   
         zTemp.i = zData[j].i;   
         zData[j].r = zData[i].r;
         zData[j].i = zData[i].i;
         zData[i].r = zTemp.r;   
         zData[i].i = zTemp.i;   
      }
      m = n/2;                      
Lp1:  if ( (j+1)-m > 0 )          
      {
         j = j - m;           
         m = m / 2;               
         if ( m-2 >= 0 ) goto Lp1;
      }
      j = j + m;              
   }
   mMax = 2;
TotalCheck:
   if ( (mMax/2 - n) < 0 )
   {
      iStep = 2*mMax;
      dTheta = dSign*TWOPI/(double) mMax;
      dSinTheta = sin( dTheta/2. );
      dSinR = -2.*dSinTheta*dSinTheta;
      dSinI = sin( dTheta );
      dWR = 1.;
      dWI = 0.;
      for ( mm=0; mm<mMax/2; mm++ )
      {
         for ( i=mm; i<n; i+=iStep/2 )
         {
            j = i + mMax/2;
            zTemp.r = (float) (dWR*zData[j].r - dWI*zData[j].i);
            zTemp.i = (float) (dWR*zData[j].i + dWI*zData[j].r);
            zData[j].r = zData[i].r - zTemp.r;
            zData[j].i = zData[i].i - zTemp.i;
            zData[i].r = zData[i].r + zTemp.r;
            zData[i].i = zData[i].i + zTemp.i;
         }
         zTemp.r = (float) dWR;
         dWR = dWR*dSinR - dWI*dSinI + dWR;
         dWI = dWI*dSinR + zTemp.r*dSinI + dWI;
      }
      mMax = iStep;
      goto TotalCheck;
   }
}
	  
/* Detrend array by suppressing average value */
void mydtr( float pfData[], int iNSamps )
{
   double  dSum;              /* Summation variable, later average */
   int     j;
   
   dSum = 0.;
   for ( j=0; j<iNSamps; j++ ) dSum = dSum + (double) pfData[j];
   dSum = dSum / (double) iNSamps;
   for ( j=0; j<iNSamps; j++ ) pfData[j] = pfData[j]-(float) dSum;
}

/* Given number iNSamps, Finds piFFTOrder such that  2**piFFTOrder is next
   smallest power of 2 (piFFTOrder is order of FFT to process iNSamps points).
   Also returns piNumFFTPts=2**piFFTOrder. */
void myfnd( int iNSamps, int *piNumFFTPts, int *piFFTOrder )
{
   double  dPower[MAX_FFT_ORDER], dTemp1, dTemp2;
   int     i, iTemp;
   
   dPower[0] = 1.;                                  
   for ( i=1; i<25; i++ ) dPower[i] = 2.*dPower[i-1];
   mysert( (double) iNSamps, dPower, MAX_FFT_ORDER, &dTemp1, &dTemp2,
            piFFTOrder, &iTemp );
   *piNumFFTPts = (int) (dPower[*piFFTOrder] + 0.5);
}
	  
/* Inserts dX into array dPower(iMax); returns index piIndex, and barycentral
   coeffs. pdAx, pdBx. piError =1 if screw-up.  dPower can be increasing or
   decreasing. Returns pdAx=dX-dPower(j+1); pdBx=dPower(j)-dX, so that  interp.
   goes dX=dA(piIndex)*pdAx+dPower(piIndex+1)*pdBx */
void mysert( double dX, double dPower[], int iMax, double *pdAx, double *pdBx,
             int *piIndex, int *piError )
{
   double  dCx;
   int     j;                         /* Based on 0 */

/* If array is increasing */
   *piError = 0;
   if ( dPower[iMax-1] >= dPower[0] )
   {
      if ( dX < dPower[0] )      
      {
         *piIndex = 0;
         goto Error;
      }
      if ( dX > dPower[iMax-1])  
      {
         *piIndex = iMax-1;
         goto Error;
      }
      for ( j=0; j<iMax; j++ )
         if ( dX >= dPower[j] && dX <= dPower[j+1]) break;
      *piIndex = j+1;
      *pdBx = dPower[j]-dX;
      *pdAx = dX-dPower[j+1];
      dCx = *pdAx+*pdBx;
      *pdAx = *pdAx/dCx;
      *pdBx=*pdBx/dCx;
      return;
   }
   else                   /* Array is decreasing */
   {
      if ( dX > dPower[0] )
      {
         *piIndex = 0;
         goto Error;
      }
      if ( dX < dPower[iMax-1] )
      {
         *piIndex = iMax-1;
         goto Error;
      }
      for ( j=0; j<iMax; j++ )
         if ( dX < dPower[j] && dX >= dPower[j+1]) break;
      *piIndex = j+1;
      *pdBx = dPower[j]-dX;
      *pdAx = dX-dPower[j+1];
      dCx = *pdAx+*pdBx;
      *pdAx = *pdAx/dCx;
      *pdBx = *pdBx/dCx;
      return;
   }
   Error: 
   *piError = 1;
   *pdAx = 0.;
   *pdBx = 0.;
}

/* Cosine taper of array A of N points, by B at beginning and E at end */
void mytpr( float pfData[], int iNSamps, double dBeg, double dEnd )
{
   double  dAng, dCs;
   int     i;
   long    M1;          /* # samps to taper at beginning */
   long    M3;          /* # samps to taper at end */
   long    M4;          /* # starting sample for end taper (based on 0) */

   M1 = (long) ((double) iNSamps*dBeg + 0.5);
   if ( M1 > 0 )
   {
      dAng = PI / (double) M1;
      for ( i=0; i<M1; i++ )
      {
         dCs = (1. - cos( (double) (i+1)*dAng )) / 2.;
         pfData[i] = pfData[i] * (float) dCs;
      }
   }
   M3 = (long) ((double) iNSamps*dEnd + 0.5);
   M4 = iNSamps-M3;
   if ( M3 > 0 )
   {
      dAng = PI / (double) M3;
      for ( i=M4; i<iNSamps; i++ )
      {
         dCs = (1. - cos( (double) (i-iNSamps)*dAng ))/2.;
         pfData[i] = pfData[i] * (float) dCs;
      }
   }
}

/* zres returns T(s)=a0*(s-z(1))*...*(s-z(m))/(s-p(1))*...*(s-p(n)),
   where p, z, and s are complex.  s = cmplx(0.,2*pi*fqc).
   This is the regular IRIS convention */
void resgeo( double dFqc, double dA0, int iNumZero, int iNumPole,
             fcomplex zZero[], fcomplex zPole[], fcomplex *pzres )
{
   int     j;               /* Based on 0 */
   fcomplex zD, zS;

   *pzres = Complex ( 0.0, 0.0 );
   zD = Complex( 1., 0. );
   zS = Complex( 0., (2.*PI*dFqc) );
	
/* COMPUTE THE CONTRIBUTION OF THE ZEROES, IF ANY */
   if ( iNumZero > 0 )
      for ( j=0; j<iNumZero; j++ ) zD = Cmul( zD, Csub( zS, zZero[j] ) );
 
/* COMPUTE THE CONTRIBUTION OF THE POLES */
   if ( iNumPole > 0 )
   {
      for ( j=0; j<iNumPole; j++ ) zD = Cdiv( zD, Csub( zS, zPole[j] ) );
      *pzres = Cmul( Complex( dA0, 0. ), zD );           /* Displacement */
   }
}
