/******************************************************************
 *                          File mm.h                             *
 *                                                                *
 *  Include file for mm module used at the West Coast/            *
 *  Alaska Tsunami Warning Center.                                *
 ******************************************************************/

#include <trace_buf.h>
#include "\earthworm\atwc\src\libsrc\earlybirdlib.h"

/* Definitions
   ***********/
#define MAX_HYPO_SIZE    512   /* Maximum size of TYPE_HYPOTWC */
#define DEGTOKM        111.194927  /* km per degree */
#define MAX_FFT        16184   /* Max samples in FFT */
#define MAX_SPECTRA    512     /* Max # spectral amplitudes */
#define MAX_FFT_PER    100
#define NUM_PATH_PER   28      /* # different periods specified in regions */
#define NUM_ATTEN      7       /* # crustal types */
#define NUM_LAT_BOUNS  18      /* # lat grids for crust definition */
#define NUM_LON_BOUNS  36      /* # lon grids for crust definition */
#define MAX_FFT_ORDER  25      /* Max order of FFT (2**MAX_FFT_ORDER */
#define MMBUFFER_SIZE  16184   /* Max # samples to send to Mm functions */
#define MM_MIN_TIME    60.     /* Minimum # seconds necessary for Mm */

#define IDM_EXIT_MAIN       115
#define IDM_REFRESH         140	  /* Refresh screen */
#define IDM_STOP_LP         170   /* Stop Mm computations */
#define IDM_COMPUTE_MOMENT  180   /* Compute Mm magnitudes */

#define ID_CHILD_MM         200   /* Station/Mm list box */

typedef struct {
   int     iNumPers;                  /* Number of frequencies with amps */
   double  dAmp[MAX_SPECTRA];         /* Spectral amplitude */
   double  dPer[MAX_SPECTRA];         /* Spectral period */
} SPECTRA;

typedef struct {
   char StaFile[64];              /* Name of file with SCN info */
   char StaDataFile[64];          /* Station information file */
   char ATPLineupFileLP[64];      /* Optional command when used with ATPlayer */
   long InKey;                    /* Key to ring where waveforms live */
   long HKey;                     /* Key to ring where hypocenters will live */
   int  HeartbeatInt;             /* Heartbeat interval in seconds */
   int  Debug;                    /* If 1, print debug messages */
   unsigned char MyModId;         /* Module id of this program */
   int MinutesInBuff;             /* Number of minutes data to save per trace */
   char DummyFile[64];            /* Hypocenter parameter disk file */
   char ResponseFile[64];         /* Broadband station response file */
   char RegionFile[64];           /* Mm path correction file */
   char MwFile[64];               /* Mw results file */
   SHM_INFO InRegion;             /* Info structure for input region */
   SHM_INFO HRegion;              /* Info structure for Hypocenter region */
} GPARM;

typedef struct {
   unsigned char MyInstId;        /* Local installation */
   unsigned char GetThisInstId;   /* Get messages from this inst id */
   unsigned char GetThisModId;    /* Get messages from this module */
   unsigned char TypeHeartBeat;   /* Heartbeat message id */
   unsigned char TypeError;       /* Error message id */
   unsigned char TypeWaveform;    /* Earthworm waveform messages */
   unsigned char TypeHypoTWC;     /* Hypocenter message - TWC format*/
} EWH;

/* Function declarations for mm
   ****************************/
thr_ret CheckLocThread( void * );
int     CreateChildren( HWND, int, int );
void    DisplayLittoral( HDC, long, long, int, int, HYPO * );
void    DisplaySpectra( HDC, long, long, int, int, MMSTUFF [], SPECTRA [],
                        int [] );
void    DisplayTitles( HDC, long, long, int, int );
void    FillMmList( HWND, MMSTUFF [], int );
int     GetEwh( EWH * );
void    GetLDC( long, long *, double *, long );
thr_ret HThread( void * );
void    InitMmStructure( MMSTUFF * );
int     MoveChildren( int, int );
void    PadBuffer( long, double, long *, long *, long );
int     PatchDummyWithMw( HYPO *, char * );
void    PutDataInBuffer( TRACE_HEADER *, long *, long *, long *, long );
long WINAPI WndProc( HWND, UINT, UINT, long );
thr_ret WThread( void * );

int     GetConfig( char *, GPARM * );                    /* config.c */
void    LogConfig( GPARM * );

int     GetStaList( STATION **, int *, GPARM *, MMSTUFF ** );/* stalist.c */
int     IsComment( char [] );
int     LoadStationData( STATION *, char * );
int     LoadResponseData( STATION *, MMSTUFF *, char * );
void    LogStaList( STATION *, int );

double  Mm( long, long *, MMSTUFF *, double, char *, double, double,
            SPECTRA *, double );                        /* mm.c*/
int     RdRegion( char * );

void    coolb( int, fcomplex[], double );               /* routines.c */
void    mydtr( float[], int );
void    myfnd( int, int *, int * );
void    mysert( double, double[], int, double *, double *, int *, int * );
void    mytpr( float[], int, double, double );
void    resgeo( double, double, int, int, fcomplex[], fcomplex[],
                fcomplex * );

