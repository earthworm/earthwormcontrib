
      /*****************************************************************
       *                        mm.c                                   *
       *                                                               *
       *  This program determines moment magnitude                     *
       *  from broadband seismic data.                                 *
       *                                                               *
       *  The magnitude scheme (Mm) used at the Polynesian Tsunami     *
       *  Center is used to determine Mm (Mw).  This was developed by  *
       *  Talandier, Okal, and others                                  *
       *  from that center.  The code was provided by Weinstein of     *
       *  PTWC.  Originally I think it came from Okal.  Half the       *
       *  original code was converted to C by Weinstien and the rest by*
       *  Whitmore at the WC/ATWC.  At the WC/ATWC it was coupled with *
       *  this earthworm module.                                       *
       *                                                               *
       *  Input seismic data is taken from an earthworm ring           *
       *  (TYPE_TRACEBUF).  The data is buffered by this module.       *
       *  Processing is triggered by a TYPE_HYPOTWC message obtained   *
       *  from the HYPO_RING.  Data is saved for up to MinutesInBuffer *
       *  time, so processing can be performed for a quake so long as  *
       *  the data is still in the buffer.  A semaphore is set up here *
       *  (similar to LPPROC) that can be set in the WC/ATWC program   *
       *  LOCATE to trigger processing.  LOCATE does not have to be    *
       *  running for this code to work.  So the processing is either  *
       *  triggered automatically through the HYPO_RING, manually      *
       *  in LOCATE, or manually here through a menu option.           *
       *                                                               *
       *  Output from this module is written to a data file and to a   *
       *  Windows window.  The code can only be used with the          *
       *  Windows NT/2K operating system.                              *
       *                                                               *
       *  Original code contributed by Weinstein                       *
       *  Converted to C and ew module by Whitmore (WC/ATWC) - 11/2001 *
       *                                                               *
       *  October, 2005: Added options to run this module in           *
       *                 conjunction with ATPlayer. !!!                *
       *  January, 2005: Added magnitude pass to mm.                   *
       *  May, 2002: Added graphics: listbox with individual mags.,    *
       *             Spectral display                                  *
       *  July, 2003: Upgraded Calibs file with PTWC upgrades (iUse    *
       *              flag)                                            *
       *                                                               *
       ****************************************************************/
	   
#include <windows.h>
#include <wingdi.h>                                                 
#include <winuser.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include <swap.h>
#include "mm.h"

/* Global Variables 
   ****************/
double  dGroup[NUM_ATTEN][NUM_PATH_PER]; /* Group velocities */
double  dLastEndTime;          /* End time of last data */
double  dPeriod[NUM_PATH_PER];           /* Periods in path corr. file */
double  dQQ[NUM_ATTEN][NUM_PATH_PER];    /* Attenuations */
EWH     Ewh;                   /* Parameters from earthworm.h */
MSG_LOGO getlogoW;             /* Logo of requested waveforms */
GPARM   Gparm;                 /* Configuration file parameters */
HANDLE  hEventMom;             /* Event shared externally to trigger moment */
HANDLE  hEventMomDone;         /* Event shared externally to signal proc. done*/
HINSTANCE hInstMain;           /* Copy of main program instance (process id) */
MSG_LOGO hrtlogo;              /* Logo of outgoing heartbeats */
HYPO    HStruct;               /* Hypocenter data structure */
HWND    hwndStaMm;             /* Window handle to Mm listbox */
HWND    hwndWndProc;           /* Client window handle */
int     iCrust[NUM_LAT_BOUNS][NUM_LON_BOUNS]; /* Crustal sections */
int     iFromEW;               /* 1-> processing triggered in Earthworm */
int     iIndices[6];           /* Indices of Spectra to show on screen */
int     iProcMoment;           /* 0->no moment proc going on, 1-> moment going*/
int     iProcMomFromThread;    /* 1-> COMPUTE_moment called from LOCATE */
MSG     msg;                   /* Windows control message variable */
mutex_t mutsem1;               /* Semaphore to protect variable adjustments */
pid_t   myPid;                 /* Process id of this process */
int     Nsta;                  /* Number of stations to display */
MMSTUFF *MmStuff;              /* Broadband Response array, etc. */
SPECTRA Spectra[MAX_STATIONS]; /* Spectral amplitudes over Mm range */
STATION *StaArray;             /* Station data array */
char    szProcessName[] = "Mm-Earthworm";
char    szTitle[] = "Mm-Earthworm";/* String to load in Title bar */
time_t  then;                  /* Previous heartbeat time */
char    *WaveBuf;              /* Pointer to waveform buffer */
TRACE_HEADER *WaveHead;        /* Pointer to waveform header */
long    *WaveLong;             /* Long pointer to waveform data */
short   *WaveShort;            /* Short pointer to waveform data */

      /***********************************************************
       *              The main program starts here.              *
       *                                                         *
       *  Argument:                                              *
       *     argv[1] = Name of configuration file                *
       ***********************************************************/

int WINAPI WinMain (HINSTANCE hInst, HINSTANCE hPreInst, 
                    LPSTR lpszCmdLine, int iCmdShow)
{
   char          configfile[64];  /* Name of config file */
   HDC     hIC;	                  /* Information context to check for info */
   int           i;
   long          InBufl;          /* Maximum message size in bytes */
   char          line[40];        /* Heartbeat message */
   int           lineLen;         /* Length of heartbeat message */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */
   static unsigned tidC;          /* LP semaphore checker Thread */
   static unsigned tidH;          /* Hypocenter getter Thread */
   static unsigned tidW;          /* Waveform getter Thread */
   static WNDCLASS wc;
   
   hInstMain = hInst;
   iProcMoment = 0;
   iProcMomFromThread = 0;
   iFromEW = 0;
   dLastEndTime = 0.;             
   
/* Get config file name (format "mm mm.D")
   ***********************************************/
   if ( strlen( lpszCmdLine ) <= 0 )
   {
      fprintf( stderr, "Need configfile in start line.\n" );
      return -1;
   }
   strcpy( configfile, lpszCmdLine );

/* Get parameters from the configuration files
   *******************************************/
   if ( GetConfig( configfile, &Gparm ) == -1 )
   {
      fprintf( stderr, "GetConfig() failed. file %s.\n", configfile );
      return -1;
   }

/* Look up info in the earthworm.h tables
   **************************************/
   if ( GetEwh( &Ewh ) < 0 )
   {
      fprintf( stderr, "mm: GetEwh() failed. Exiting.\n" );
      return -1;
   }

/* Specify logos of incoming waveforms and outgoing heartbeats
   ***********************************************************/
   getlogoW.instid = Ewh.GetThisInstId;
   getlogoW.mod    = Ewh.GetThisModId;
   getlogoW.type   = Ewh.TypeWaveform;

   hrtlogo.instid = Ewh.MyInstId;
   hrtlogo.mod    = Gparm.MyModId;
   hrtlogo.type   = Ewh.TypeHeartBeat;

/* Initialize name of log-file & open it
   *************************************/
   logit_init( configfile, Gparm.MyModId, 256, 1 );

/* Get our own pid for restart purposes
   ************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "mm: Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Log the configuration parameters
   ********************************/
   LogConfig( &Gparm );

/* Allocate the waveform buffers
   *****************************/
   InBufl = MAX_TRACEBUF_SIZ*2;
   WaveBuf = (char *) malloc( (size_t) InBufl );
   if ( WaveBuf == NULL )
   {
      logit( "et", "mm: Cannot allocate waveform buffer\n" );
      return -1;
   }

/* Point to header and data portions of waveform message
   *****************************************************/
   WaveHead  = (TRACE_HEADER *) WaveBuf;
   WaveLong  = (long *) (WaveBuf + sizeof (TRACE_HEADER));
   WaveShort = (short *) (WaveBuf + sizeof (TRACE_HEADER));

/* Read the station list and return the number of stations found.
   Allocate the station list array.
   **************************************************************/
   if ( GetStaList( &StaArray, &Nsta, &Gparm, &MmStuff ) == -1 )
   {
      logit( "", "mm: GetStaList() failed. Exiting.\n" );
	  free( WaveBuf );
      return -1;
   }
   if ( Nsta == 0 )
   {
      logit( "et", "mm: Empty station list. Exiting." );
      free( WaveBuf );
      free( StaArray );
      free( MmStuff );
      return -1;
   }
   logit( "t", "mm: Displaying %d stations.\n", Nsta );

/* Read Path Correction data
   *************************/
   if ( RdRegion( Gparm.RegionFile ) == -1 )
   {
      logit( "et", "mm: RdRegion error - %s.", Gparm.RegionFile );
      free( WaveBuf );
      free( StaArray );
      free( MmStuff );
      return -1;
   }
   
/* Create event to share with an external (non-earthworm) program; this event
   triggers moment processing in mm.
   **************************************************************************/
   hEventMom = CreateEvent( NULL,      /* Default security */
                            FALSE,     /* Auto-reset event */
                            FALSE,     /* Initially not set */
                            "RTMM" );  /* Share with LOCATE */
   if ( hEventMom == NULL )            /* If event not created */
   {
      logit( "t", "failed to create moment event" );
      free( WaveBuf );
      free( StaArray );
      free( MmStuff );
      return -1;
   }
   
/* Create another event to share with an external (non-earthworm) program;
   this event signals the external program that the moment processing is done.
   ***************************************************************************/
   hEventMomDone = CreateEvent( NULL,        /* Default security */
                                FALSE,       /* Auto-reset event */
                                FALSE,       /* Initially not set */
                                "MomentDone" );/* Share with LOCATE */
   if ( hEventMomDone == NULL )              /* If event not created */
   {
      logit( "t", "failed to create momentdone event" );
      free( WaveBuf );
      free( StaArray );
      free( MmStuff );
      CloseHandle( hEventMom );
      return -1;
   }

/* If this is the first instance of this program, init window stuff and
   register window (it always is)
   ********************************************************************/
   if ( !hPreInst )
   {  /* Force PAINT when sized and give double click notification */
      wc.style         = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
      wc.lpfnWndProc   = WndProc;         /* Control Window process */
      wc.cbClsExtra    = 0;
      wc.cbWndExtra    = 0;
      wc.hInstance     = hInst;           /* Process id */
      wc.hIcon         = LoadIcon( hInst, "mm" );    /* System app icon */
      wc.hCursor       = LoadCursor( NULL, IDC_ARROW );          /* Pointer */
      wc.hbrBackground = (HBRUSH) GetStockObject( WHITE_BRUSH ); /* White */
      wc.lpszMenuName  = "mm_menu";       /* Relates to .RC file */
      wc.lpszClassName = szProcessName;
      if ( !RegisterClass( &wc ) )        /* Window not registered */
      {
         logit( "t", "RegisterClass failed\n" );
         free( WaveBuf );
         free( StaArray );
         free( MmStuff );
         CloseHandle( hEventMom );
         CloseHandle( hEventMomDone );
         return -1;
      }
   }

/* Create the window
   *****************/
   hIC = CreateIC ("DISPLAY", NULL, NULL, NULL);
   if (hIC == NULL)
   {
      logit( "t", "CreateIC error\n" );
      free( WaveBuf );
      free( StaArray );
      free( MmStuff );
      CloseHandle( hEventMom );
      CloseHandle( hEventMomDone );
      return -1;
   }
   hwndWndProc = CreateWindow(
                 szProcessName,          /* Process name */
                 szProcessName,          /* Initial title bar caption */
                 WS_OVERLAPPEDWINDOW | WS_VSCROLL,
//                 CW_USEDEFAULT,          /* top left x starting location */
//                 CW_USEDEFAULT,          /* top left y starting location */
//                 CW_USEDEFAULT,          /* Initial screen width in pixels */
//                 CW_USEDEFAULT,          /* Initial screen height in pixels */
                 3*GetDeviceCaps (hIC, HORZRES)/4, /* top left x starting loc */
                 GetDeviceCaps (hIC, VERTRES)/2, /* top left y starting loc */
                 GetDeviceCaps (hIC, HORZRES)/4, /* Window width in pixels */
                 GetDeviceCaps (hIC, VERTRES)/2, /* Window height in pixels */
                 NULL,                   /* No parent window */
                 NULL,                   /* Use standard system menu */
                 hInst,                  /* Process id */
                 NULL );                 /* No extra data to pass in */
   if ( hwndWndProc == NULL )            /* Window not created */
   {
      logit( "t", "CreateWindow failed\n" );
      free( WaveBuf );
      free( StaArray );
      free( MmStuff );
      CloseHandle( hEventMom );
      CloseHandle( hEventMomDone );
      return 0;
   }
   
   ShowWindow( hwndWndProc, iCmdShow );  /* Show the Window */
   UpdateWindow( hwndWndProc );          /* Force an initial PAINT call */

/* Attach to existing transport rings
   **********************************/
   tport_attach( &Gparm.InRegion,  Gparm.InKey );
   tport_attach( &Gparm.HRegion,   Gparm.HKey );

/* Flush the input waveform ring
   *****************************/
   while ( tport_getmsg( &Gparm.InRegion, &getlogoW, 1, &logo, &MsgLen,
                         WaveBuf, MAX_TRACEBUF_SIZ ) != GET_NONE );

/* Send 1st heartbeat to the transport ring
   ****************************************/
   time( &then );
   sprintf( line, "%d %d\n", then, myPid );
   lineLen = strlen( line );
   if ( tport_putmsg( &Gparm.InRegion, &hrtlogo, lineLen, line ) != PUT_OK )
   {
      logit( "et", "mm: Error sending 1st heartbeat. Exiting." );
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.HRegion );
      free( WaveBuf );
      free( StaArray );
      free( MmStuff );
      CloseHandle( hEventMom );
      CloseHandle( hEventMomDone );
      return 0;
   }
   
/* Create a mutex for protecting adjustments of variables
   ******************************************************/
   CreateSpecificMutex( &mutsem1 );

/* Start the moment compute semaphore checker thread
   *************************************************/
   if ( StartThread( CheckLocThread, 8192, &tidC ) == -1 )
   {
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.HRegion );
      free( WaveBuf );
      free( StaArray );
      free( MmStuff );
      CloseHandle( hEventMom );
      CloseHandle( hEventMomDone );
      logit( "et", "Error starting H thread; exiting!\n" );
      return -1;
   }

/* Start the Hypocenter getter thread
   **********************************/
   if ( StartThread( HThread, 8192, &tidH ) == -1 )
   {
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.HRegion );
      free( WaveBuf );
      free( StaArray );
      free( MmStuff );
      CloseHandle( hEventMom );
      CloseHandle( hEventMomDone );
      logit( "et", "Error starting H thread; exiting!\n" );
      return -1;
   }

/* Start the waveform getter tread
   *******************************/
   if ( StartThread( WThread, 8192, &tidW ) == -1 )
   {
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.HRegion );
      free( WaveBuf );
      free( StaArray );
      free( MmStuff );
      CloseHandle( hEventMom );
      CloseHandle( hEventMomDone );
      logit( "et", "Error starting W thread; exiting!\n" );
      return -1;
   }
   
/* Main windows loop
   *****************/      
   while ( GetMessage( &msg, NULL, 0, 0 ) )
   {
      TranslateMessage( &msg );
      DispatchMessage( &msg );
   }

/* Detach from the ring buffers
   ****************************/
   tport_detach( &Gparm.InRegion );
   tport_detach( &Gparm.HRegion );
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
   free( WaveBuf );
   free( MmStuff );
   CloseHandle( hEventMom );
   CloseHandle( hEventMomDone );
   for ( i=0; i<Nsta; i++ ) 
      free( StaArray[i].plRawCircBuff );
   free( StaArray );
   logit( "t", "Termination requested. Exiting.\n" );
   return (msg.wParam);
}

      /*********************************************************
       *              CheckLocThread()                         *
       *                                                       *
       *  This thread checks for events set in LOCATE to force *
       *  a new moment processing.                             *
       *                                                       *
       *  Its prupose is to let an external program trigger    *
       *  moment processing.  Windows specific semaphore cmds  *
       *  are used throughout as named events are not supported*
       *  in sema_ew.c.                                        *
       *                                                       *
       *********************************************************/
	   
thr_ret CheckLocThread( void *dummy )
{
   sleep_ew( 10000 );      /* Let things get going */

/* Loop forever, waiting to send a message to Process LP */
   for( ;; )
   {
      WaitForSingleObject( hEventMom, INFINITE );
      iProcMomFromThread = 1;
      PostMessage( hwndWndProc, WM_COMMAND, IDM_COMPUTE_MOMENT, 0 );
   }
}

     /**************************************************************
      *                CreateChildren()                            *
      *                                                            *
      * This function creates the child window which lists Stations*
      * and Mm.                                                    *
      *                                                            *
      * Arguments:                                                 *
      *  hwnd        Handle of parent window (WndProc)             *
      *  cxScreenCC  Horizontal pixel width of window              *
      *  cyScreenCC  Vertical pixel height of window               *
      *                                                            *
      * Return - 0 if OK, -1 if problem                            *
      **************************************************************/
      
int CreateChildren( HWND hwnd, int cxScreenCC, int cyScreenCC )
{
/* Create Mm list LISTBOX (leave room for header) */
   hwndStaMm = CreateWindow ("listbox",/* Create a listbox child */
              NULL,                   /* No initial text */
              WS_CHILD | WS_VISIBLE | LBS_NOTIFY | WS_VSCROLL | WS_BORDER,
//              | LBS_MULTIPLESEL,
                                      /* Window style */
              1*cxScreenCC/100,       /* top left x starting location */
              3*cyScreenCC/100,       /* top left y starting location */
              28*cxScreenCC/100,      /* Width */
              97*cyScreenCC/100,      /* Height */
              hwnd,                   /* Client window is the parent */
              (HMENU) ID_CHILD_MM,    /* Child window ID */
              hInstMain,              /* Main Process id */
              NULL);                  /* No extra data to pass in */
   if ( hwndStaMm == NULL )           /* Window not created */
   {
      logit( "t", "Create Mm listbox failed\n" );
      return -1;
   }		
return 0;
}

     /**************************************************************
      *                 DisplayLittoral()                          *
      *                                                            *
      * This function outputs the littoral location and preferred  *
      * magnitude for the Active Hypocenter.                       *
      *                                                            *
      * Arguments:                                                 *
      *  hdc               Device context to use                   *
      *  lTitleFHt         Title font height                       *
      *  lTitleFWd         Title font width                        *
      *  cxScreenT         Horizontal pixel width of window        *
      *  cyScreenT         Vertical pixel height of window         *
      *  pHypo             Hypocenter data                         *
      *                                                            *
      **************************************************************/
      
void DisplayLittoral( HDC hdc, long lTitleFHt, long lTitleFWd,
                      int cxScreenT, int cyScreenT, HYPO *pHypo )
{
   double  dM;                  /* Temp mag. variable */
   HFONT   hOFont, hNFont;      /* Font handles */
   LATLON  ll;                  /* Geographic lat/lon */
   long    lTime;                                   /* 1/1/70 time */
   POINT   pt;
   char    szBuffer[64], szTemp[12];
   struct  tm *tm;                                  /* time structure */

/* Create new font */
   hNFont = CreateFont( lTitleFHt, lTitleFWd, 
             0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN,
             "Times New Roman" );
					   
/* Select font and color */
   hOFont = (HFONT) SelectObject( hdc, hNFont );    /* Select the font */
   SetTextColor( hdc, RGB( 0, 0, 0 ) );             /* Black text */
   
/* Display O-time, location, and magnitude */
   pt.x = 45*cxScreenT/100;
   pt.y = 1*cyScreenT/100;
   TextOut( hdc, pt.x, pt.y, "O-Time:    ", 11 );	 
   pt.x = 45*cxScreenT/100;
   pt.y = 4*cyScreenT/100;
   TextOut( hdc, pt.x, pt.y, "Location:  ", 11 );	 
   pt.x = 45*cxScreenT/100;
   pt.y = 7*cyScreenT/100;
   TextOut( hdc, pt.x, pt.y, "Magnitude: ", 11 );	 
   if ( pHypo->dOriginTime > 0. )
   {
      strcpy( szBuffer, "\0" );
      lTime = (long) (pHypo->dOriginTime+0.5);
      tm = TWCgmtime( lTime );
      itoaX( tm->tm_mon+1, szTemp );
      PadZeroes( 2, szTemp );
      strcpy( szBuffer, szTemp );                            /* Month */
      strcat( szBuffer, "/" );
      itoaX( tm->tm_mday, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Day */
      strcat( szBuffer, "  " );
      itoaX( tm->tm_hour, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Hour */
      strcat( szBuffer, ":" );
      itoaX( tm->tm_min, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Minute */
      strcat( szBuffer, ":" );
      itoaX( tm->tm_sec, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Second */
      pt.x = 60*cxScreenT/100;
      pt.y = 1*cyScreenT/100;
      TextOut( hdc, pt.x, pt.y, szBuffer, strlen( szBuffer ) );
      
      strcpy( szBuffer, "\0" );
      GeoGraphic( &ll, (LATLON *) pHypo );
      _gcvt( fabs( ll.dLat ), 3, szTemp );
      strcat( szBuffer, szTemp );                         /* Latitude */
      if ( ll.dLat > 0. ) strcat( szBuffer, "N" );
      else                strcat( szBuffer, "S" );
      strcat( szBuffer, "  " );
      if ( ll.dLon > 180. ) ll.dLon -= 360.;
      _gcvt( fabs( ll.dLon ), 4, szTemp );
      strcat( szBuffer, szTemp );                         /* Longitude */
      if ( ll.dLon > 0. ) strcat( szBuffer, "E" );
      else                strcat( szBuffer, "W" );
      pt.x = 60*cxScreenT/100;
      pt.y = 4*cyScreenT/100;
      TextOut( hdc, pt.x, pt.y, szBuffer, strlen( szBuffer ) );
      
      strcpy( szBuffer, "\0" );
      if ( pHypo->dMwAvg > 0. )
      {	  
         dM = (double) ((int) ((pHypo->dMwAvg+0.05)*10)) / 10.;/* Round Mw */
         itoaX( (int) dM, szTemp );
         PadZeroes( 1, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Mw */
         strcat( szBuffer, "." );
         itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Mw decimal */
         strcat( szBuffer, "w" );   
      }
      else if ( pHypo->dMwpAvg > 0. )
      {	  
         dM = (double) ((int) ((pHypo->dMwpAvg+0.05)*10)) / 10.;/* Round Mwp */
         itoaX( (int) dM, szTemp );
         PadZeroes( 1, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Mwp */
         strcat( szBuffer, "." );
         itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Mwp decimal */
         strcat( szBuffer, "wp" );   
         if (pHypo->iNumMwp < 3) strcat( szBuffer, "?");
      }
      else if ( pHypo->dMSAvg > 0. )
      {	  
         dM = (double) ((int) ((pHypo->dMSAvg+0.05)*10)) / 10.;/* Round Ms */
         itoaX( (int) dM, szTemp );
         PadZeroes( 1, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Ms */
         strcat( szBuffer, "." );
         itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Ms decimal */
         strcat( szBuffer, "s" );   
      }
      else if ( pHypo->dMbAvg > 0. )
      {	  
         dM = (double) ((int) ((pHypo->dMbAvg+0.05)*10)) / 10.;/* Round Mb */
         itoaX( (int) dM, szTemp );
         PadZeroes( 1, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Mb */
         strcat( szBuffer, "." );
         itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Mb decimal */
         strcat( szBuffer, "b" );   
      }
      else if ( pHypo->dMlAvg > 0. )
      {	  
         dM = (double) ((int) ((pHypo->dMlAvg+0.05)*10)) / 10.;/* Round Ml */
         itoaX( (int) dM, szTemp );
         PadZeroes( 1, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Ml */
         strcat( szBuffer, "." );
         itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Ml decimal */
         strcat( szBuffer, "l" );   
      }
      pt.x = 60*cxScreenT/100;
      pt.y = 7*cyScreenT/100;
      TextOut( hdc, pt.x, pt.y, szBuffer, strlen( szBuffer ) );
   }
   
   DeleteObject (SelectObject (hdc, hOFont));       /* Reset font */
}

     /**************************************************************
      *                 DisplaySpectra()                           *
      *                                                            *
      * This draws the spectra on the Mm child window.             *
      *                                                            *
      * Arguments:                                                 *
      *  hdc               Device context to use                   *
      *  lTitleFHt         Title font height                       *
      *  lTitleFWd         Title font width                        *
      *  cxScreenT         Horizontal pixel width of window        *
      *  cyScreenT         Vertical pixel height of window         *
      *  MmT               MMSTUFF structures                      *
      *  SpectraT          Spectral structures                     *
      *  iIndices          Indices of structures to display        *
      *                                                            *
      **************************************************************/
      
void DisplaySpectra( HDC hdc, long lTitleFHt, long lTitleFWd, int cxScreenT,
                     int cyScreenT, MMSTUFF MmT[], SPECTRA SpectraT[],
                     int iIndicesT[] )
{
   double  dMaxAmp, dMinAmp;     /* Min/Max spectral amplitudes */
   double  dMaxPer, dMinPer;     /* Min/Max spectral periods */
   HFONT   hOFont, hNFont;       /* Font handles */
   HPEN    hRPen, hOPen;         /* Pen handles */
   int     i, j;
   int     iTop, iBot, iLt, iRt; /* Bounds for each plot */
   POINT   pt;
   POINT   ptS[MAX_SPECTRA];     /* Spectral signal */
   char    szTemp[6];

/* Create new font */
   hNFont = CreateFont( lTitleFHt, lTitleFWd, 
             0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN,
             "Times New Roman" );
					   
/* Select font and color */
   hOFont = (HFONT) SelectObject( hdc, hNFont );    /* Select the font */
   SetTextColor( hdc, RGB( 0, 0, 0 ) );             /* Black text */
   hRPen = CreatePen (PS_SOLID, 1, RGB (255,0,0));  /* Create red lines */
   hOPen = SelectObject (hdc, hRPen);
   
/* Loop through total number spectra possible */
   for ( i=0; i<6; i++ )
      if ( iIndicesT[i] >= 0 )
      {
         iTop = (20 + i/2*25)*cyScreenT / 100;
         iBot = (40 + i/2*25)*cyScreenT / 100;
         iLt  = (40 + (i%2)*30)*cxScreenT / 100;
         iRt  = (60 + (i%2)*30)*cxScreenT / 100;
   
/* Display station name */
         pt.x = iLt+2;
         pt.y = iTop-2;
         TextOut( hdc, pt.x, pt.y, MmT[iIndicesT[i]].szStation,
                  strlen( MmT[iIndicesT[i]].szStation ) );	 
	       
/* Plot axes */	       
         pt.x = iLt;
         pt.y = iTop;
         MoveToEx( hdc, pt.x, pt.y, NULL );
         pt.x = iLt;
         pt.y = iBot;
         LineTo( hdc, pt.x, pt.y );
         pt.x = iLt;
         pt.y = iBot;
         MoveToEx( hdc, pt.x, pt.y, NULL );
         pt.x = iRt;
         pt.y = iBot;
         LineTo( hdc, pt.x, pt.y );

/* Get max/min period and amplitude */	       
         dMinPer = 100000000000.;
         dMaxPer = 0.;
         dMinAmp = 100000000000.;
         dMaxAmp = 0.;      
         for ( j=0; j<Spectra[iIndicesT[i]].iNumPers; j++ )
         {
            if ( Spectra[iIndicesT[i]].dPer[j] > dMaxPer )
               dMaxPer = Spectra[iIndicesT[i]].dPer[j];
            if ( Spectra[iIndicesT[i]].dPer[j] < dMinPer )
               dMinPer = Spectra[iIndicesT[i]].dPer[j];
            if ( Spectra[iIndicesT[i]].dAmp[j] > dMaxAmp )
               dMaxAmp = Spectra[iIndicesT[i]].dAmp[j];
            if ( Spectra[iIndicesT[i]].dAmp[j] < dMinAmp )
               dMinAmp = Spectra[iIndicesT[i]].dAmp[j];
         }
         dMaxAmp *= 1.25;
         dMinAmp /= 2.;
   
/* Display Period range */
         pt.x = iLt;
         pt.y = iBot+2;
         _gcvt( dMinPer, 3, szTemp );
         TextOut( hdc, pt.x, pt.y, szTemp, strlen( szTemp ) );	 
         pt.x = iRt;
         pt.y = iBot+2;
         _gcvt( dMaxPer, 3, szTemp );
         TextOut( hdc, pt.x, pt.y, szTemp, strlen( szTemp ) );	 
         pt.x = (iRt+iLt)/2 - 2*lTitleFWd;
         pt.y = iBot+2;
         TextOut( hdc, pt.x, pt.y, "Per (s)", 7 );	 
         pt.x = iLt - 6*lTitleFWd;
         pt.y = (iBot+iTop)/2;
         TextOut( hdc, pt.x, pt.y, "Amp", 3 );	 
      
/* Fill up spectra array, and display */
         for ( j=0; j<Spectra[iIndicesT[i]].iNumPers; j++ )
         {      
            ptS[j].x = iLt + (int) ((double) (iRt-iLt) *
             ((Spectra[iIndicesT[i]].dPer[j]-dMinPer)/(dMaxPer-dMinPer)));
            ptS[j].y = iTop + (int) ((double) (iBot-iTop) *
             ((dMaxAmp-Spectra[iIndicesT[i]].dAmp[j])/(dMaxAmp-dMinAmp)));
         }
         MoveToEx( hdc, ptS[0].x, ptS[0].y, NULL );
         Polyline( hdc, ptS, Spectra[iIndicesT[i]].iNumPers );
      }
      
   DeleteObject( hRPen );
   SelectObject( hdc, hOPen );                        /* Reset Pen color */
   DeleteObject( SelectObject( hdc, hOFont ) );       /* Reset font */
}

     /**************************************************************
      *                 DisplayTitles()                            *
      *                                                            *
      * This function outputs the title to the Mm listbox.         *
      *                                                            *
      * Arguments:                                                 *
      *  hdc               Device context to use                   *
      *  lTitleFHt         Title font height                       *
      *  lTitleFWd         Title font width                        *
      *  cxScreenT         Screen width in pixels                  *
      *  cyScreenT         Screen height in pixels                 *
      *                                                            *
      **************************************************************/
      
void DisplayTitles( HDC hdc, long lTitleFHt, long lTitleFWd, int cxScreenT,
                    int cyScreenT )
{
   HFONT   hOFont, hNFont;      /* Font handles */
   POINT   pt;
   
/* Create font */   
   hNFont = CreateFont( lTitleFHt, lTitleFWd, 
             0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
             DEFAULT_PITCH | FF_MODERN, "Elite" );

/* Select font and color */
   hOFont = (HFONT) SelectObject( hdc, hNFont );    /* Select the font */
   SetTextColor( hdc, RGB( 0, 0, 0 ) );             /* Black text */
   
/* First, write hypocenter listbox title */
   pt.x = 3*cxScreenT/200;
   pt.y = 0*cyScreenT/100;
   TextOut( hdc, pt.x, pt.y, "Stn     Dist.  Az.   Mw", 23 );	 
   DeleteObject (SelectObject (hdc, hOFont));       /* Reset font */
}

     /**************************************************************
      *                    FillMmList()                            *
      *                                                            *
      * This function fills the Mm listbox.                        *
      *                                                            *
      * NOTE: The system button font is used to fill this listbox. *
      * It looks best if a constant width font is used.  Change    *
      * this in the WM_SIZE calls.                                 *
      *                                                            *
      * Arguments:                                                 *
      *  hwnd              Window handle                           *
      *  MmT               Array of Mm data                        *
      *  iNSta             # stations in Mm array                  *
      *                                                            *
      **************************************************************/
      
void FillMmList( HWND hwnd, MMSTUFF MmT[], int iNSta )
{
   double  dTemp;
   int     i, iCnt;
   char    szBuffer[256], szTemp[12];

/* Delete all existing list box Mms */
   SendMessage( hwnd, LB_RESETCONTENT, 0, 0 );
   
/* Fill up list box with Mm data */   
   iCnt = 0;
   for ( i=0; i<iNSta; i++ )
      if ( MmT[i].dMmMax > 0. )
      {   
         strcpy( szBuffer, "\0" );
         strcpy( szBuffer, MmT[i].szStation );               /* Station */
         if ( strlen( MmT[i].szStation ) == 3 ) strcat( szBuffer, "    " );
         else strcat( szBuffer, "   " );
         dTemp = (double) ((int) ((MmT[i].azdelt.dDelta+0.05)*10.))/10.;  /* Round */
         itoaX( (int) dTemp, szTemp );
         if ( strlen( szTemp ) == 1 ) strcat( szBuffer, "  " );
         if ( strlen( szTemp ) == 2 ) strcat( szBuffer, " " );
         strcat( szBuffer, szTemp );                         /* Distance */
         strcat( szBuffer, "." );
         itoaX( (int) ((dTemp-floor( dTemp ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Distance dec. */
         strcat( szBuffer, "  " );
         dTemp = (double) ((int) ((MmT[i].azdelt.dAzimuth+0.05)*10.))/10.;  /* Round */
         itoaX( (int) dTemp, szTemp );
         if ( strlen( szTemp ) == 1 ) strcat( szBuffer, "  " );
         if ( strlen( szTemp ) == 2 ) strcat( szBuffer, " " );
         strcat( szBuffer, szTemp );                         /* Azimuth */
         strcat( szBuffer, "." );
         itoaX( (int) ((dTemp-floor( dTemp ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Azimuth dec. */
         strcat( szBuffer, "  " );
         dTemp = (double) ((int) ((MmT[i].dMmMax+0.05)*10.))/10.;  /* Round */
         itoaX( (int) dTemp, szTemp );
         strcat( szBuffer, szTemp );                         /* Mm */
         strcat( szBuffer, "." );
         itoaX( (int) ((dTemp-floor( dTemp ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Mm dec. */
         strcat( szBuffer, " " );
      
/* Send line to list box */      
         SendMessage (hwnd, LB_INSERTSTRING, iCnt, (LPARAM) szBuffer);
         iCnt++;
   }
   SendMessage (hwnd, LB_SETCURSEL, 0, (LPARAM) szBuffer);
}

      /*******************************************************
       *                      GetEwh()                       *
       *                                                     *
       *      Get parameters from the earthworm.d file.      *
       *******************************************************/

int GetEwh( EWH *Ewh )
{
   if ( GetLocalInst( &Ewh->MyInstId ) != 0 )
   {
      fprintf( stderr, "mm: Error getting MyInstId.\n" );
      return -1;
   }
   if ( GetInst( "INST_WILDCARD", &Ewh->GetThisInstId ) != 0 )
   {
      fprintf( stderr, "mm: Error getting GetThisInstId.\n" );
      return -2;
   }
   if ( GetModId( "MOD_WILDCARD", &Ewh->GetThisModId ) != 0 )
   {
      fprintf( stderr, "mm: Error getting GetThisModId.\n" );
      return -3;
   }
   if ( GetType( "TYPE_HEARTBEAT", &Ewh->TypeHeartBeat ) != 0 )
   {
      fprintf( stderr, "mm: Error getting TypeHeartbeat.\n" );
      return -4;
   }
   if ( GetType( "TYPE_ERROR", &Ewh->TypeError ) != 0 )
   {
      fprintf( stderr, "mm: Error getting TypeError.\n" );
      return -5;
   }
   if ( GetType( "TYPE_TRACEBUF", &Ewh->TypeWaveform ) != 0 )
   {
      fprintf( stderr, "mm: Error getting TYPE_TRACEBUF.\n" );
      return -6;
   }
   if ( GetType( "TYPE_HYPOTWC", &Ewh->TypeHypoTWC ) != 0 )
   {
      fprintf( stderr, "mm: Error getting TYPE_HYPOTWC.\n" );
      return -7;
   }
   return 0;
}

  /******************************************************************
   *                           GetLDC()                             *
   *                                                                *
   *  Determine and update moving averages of signal value          *
   *  (called LDC here).                                            *
   *                                                                *
   *  Arguments:                                                    *
   *    lNSamps     Number of samples in this packet                *
   *    WaveLong    Pointer to data array                           *
   *    pdLDC       Pointer to long term DC average                 *
   *    lFiltSamp   # samples through filter                        *
   *                                                                *
   ******************************************************************/

void GetLDC( long lNSamps, long *WaveLong, double *pdLDC, long lFiltSamp )
{
   double  dSumLDC;     /* Summation of all values in this packet */
   int     i;
   
   dSumLDC = 0.;
   
/* Sum up total for this packet */
   for ( i=0; i<lNSamps; i++ )
   {
      if ( lFiltSamp == 0 ) dSumLDC  += ((double) WaveLong[i]);
      else                  dSumLDC  += ((double) WaveLong[i] - *pdLDC);
   }
	  
/* Compute new LTAs (first time through, just take average.  Adjust average
   from there */
   if ( lFiltSamp == 0 ) *pdLDC =  (dSumLDC/(double) lNSamps);
   else                  *pdLDC += (0.5 * dSumLDC/(double) lNSamps);
}

      /*********************************************************
       *                     HThread()                         *
       *                                                       *
       *  This thread gets messages from the hypocenter ring.  *
       *                                                       *
       *********************************************************/
	   
thr_ret HThread( void *dummy )
{
   MSG_LOGO      getlogoH;        /* Logo of requested hypos */
   char          HIn[MAX_HYPO_SIZE];/* Pointer to hypocenter from ring */
   HYPO          HStructT;        /* Temporary Hypocenter data structure */
   static int    iQuakeID;        /* ID of last quake (processed) */
   static int    iVersion;        /* Version of last quake (processed) */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */

   iVersion = -1;
   iQuakeID = -1;
   
/* Set up logos for Hypo ring 
   **************************/
   getlogoH.instid = Ewh.GetThisInstId;
   getlogoH.mod    = Ewh.GetThisModId;
   getlogoH.type   = Ewh.TypeHypoTWC;

/* Flush the input ring
   ********************/
   while ( tport_getmsg( &Gparm.HRegion, &getlogoH, 1, &logo, &MsgLen,
                         HIn, MAX_HYPO_SIZE) != GET_NONE );
						 
/* Loop to read hypocenter messages and load buffer
   ************************************************/
   while ( tport_getflag( &Gparm.HRegion ) != TERMINATE )
   {
      int     rc;               /* Return code from tport_getmsg() */

/* Get a hypocenter from transport region
   **************************************/
      rc = tport_getmsg( &Gparm.HRegion, &getlogoH, 1, &logo, &MsgLen,
                         HIn, MAX_HYPO_SIZE);

      if ( rc == GET_NONE )
      {
         sleep_ew( 1000 );
         continue;
      }

      if ( rc == GET_NOTRACK )
         logit( "et", "mmT: Tracking error.\n");

      if ( rc == GET_MISS_LAPPED )
         logit( "et", "mmT: Got lapped on the ring.\n");

      if ( rc == GET_MISS_SEQGAP )
         logit( "et", "mmT: Gap in sequence numbers.\n");

      if ( rc == GET_MISS )
         logit( "et", "mmT: Missed messages.\n");

      if ( rc == GET_TOOBIG )
      {
         logit( "et", "mmT: Retrieved message too big (%d) for msg.\n",
                MsgLen );
         continue;
      }
	  
/* Put hypocenter into temp structure (NOTE: No byte-swapping is performed, so 
   there will be trouble getting hypos from an opposite order machine)
   *******************************************************************/   
      if ( HypoStruct( HIn, &HStructT ) < 0 )
      {
         logit( "t", "Problem in HypoStruct function - 1\n" );
         continue;
      }
      	  
/* Process data for moment if this is a new or updated location and of
   sufficient magnitude.
   *******************************************************************/
      if ( (iProcMoment == 1 && HStructT.iQuakeID == iQuakeID && 
            HStructT.iVersion != iVersion && HStructT.iVersion < 6 &&
           (HStructT.dMbAvg > 5.5 || HStructT.dMlAvg > 5.5 ||
            HStructT.dMwpAvg > 5.5)) ||
           (iProcMoment == 0 && HStructT.iQuakeID != iQuakeID &&
           (HStructT.dMbAvg > 5.5 || HStructT.dMlAvg > 5.5 ||
            HStructT.dMwpAvg > 5.5)) )
      {
         logit( "t", "Start moment processing - %ld %ld %ld\n",
                HStructT.iQuakeID, HStructT.iVersion, iProcMoment );
         if ( HypoStruct( HIn, &HStruct ) < 0 )
         {
            logit( "t", "Problem in HypoStruct function - 2\n" );
            continue;
         }
         iQuakeID = HStruct.iQuakeID;	  
         iVersion = HStruct.iVersion;	  
         iFromEW = 1;
         PostMessage( hwndWndProc, WM_COMMAND, IDM_COMPUTE_MOMENT, 0 );
      }
   }
}

  /***************************************************************
   *                       InitMmStructure()                     *
   *                                                             *
   *  Initialize MmStuff structure.                              *
   *                                                             *
   *  Arguments:                                                 *
   *    Mm          MMSTUFF structure to initialize              *
   *                                                             *
   ***************************************************************/

void InitMmStructure( MMSTUFF *MmT )
{
   MmT->dMmTravTime = 0.;
   MmT->dMmStartTime = 0.;
   MmT->dMmEndTime = 0.;
   MmT->dMmMax = 0.;
   MmT->dPerMax = 0.;
}

     /**************************************************************
      *                  MoveChildren()                            *
      *                                                            *
      * This function places the child window which lists Mm       *
      * in its proper position on the screen.                      *
      *                                                            *
      * Arguments:                                                 *
      *  cxScreenT         Horizontal pixel width of window        *
      *  cyScreenT         Vertical pixel height of window         *
      *                                                            *
      * Return - 0 if OK, -1 if problem                            *
      **************************************************************/
	  
int MoveChildren( int cxScreenT, int cyScreenT )
{
/* Move Mm list LISTBOX */
   if ( !MoveWindow( hwndStaMm,             /* Move the hypocenter window */
         1*cxScreenT/100,                   /* top left x starting location */
         3*cyScreenT/100,                   /* top left y starting loc */
         28*cxScreenT/100,                  /* Width */
         97*cyScreenT/100,                  /* Height */
         TRUE ) )                           /* Force a re-paint */
   {
      logit( "t", "Move Mm listbox failed\n" );
      return -1;
   }
return 0;
}

  /******************************************************************
   *                        PadBuffer()                             *
   *                                                                *
   *  Fill in gaps in the data with DC offset.                      *
   *  Update last data value index.                                 *
   *                                                                *
   *  Arguments:                                                    *
   *    lGapSize    Number of values to pad (+1)                    *
   *    dLDC        DC average                                      *
   *    plBuffCtr   Index tracker in main buffer                    *
   *    plBuff      Pointer to main circular buffer                 *
   *    lBuffSize   # samples in main circular buffer               *
   *                                                                *
   ******************************************************************/

void PadBuffer( long lGapSize, double dLDC, long *plBuffCtr, 
                long *plBuff, long lBuffSize )
{
   long    i;
   
/* Pad the data buffer over the gap interval */
   for ( i=0; i<lGapSize-1; i++ )
   {
      plBuff[*plBuffCtr] = (long) dLDC;
      *plBuffCtr += 1;
      if ( *plBuffCtr == lBuffSize ) *plBuffCtr = 0;
   }	  
}

  /******************************************************************
   *                      PatchDummyWithMw()                        *
   *                                                                *
   *  Update the dummy file with the new average Mw.                *
   *                                                                *
   *  Arguments:                                                    *
   *    pHypo       Structure with hypocenter parameters            *
   *    pszDFile    Dummy File name                                 *
   *                                                                *
   ******************************************************************/
   
int PatchDummyWithMw( HYPO *pHypo, char *pszDFile )
{
   HYPO    HypoT;       /* Temporary hypocenter data structure */

/* Read in hypocenter information from dummy file */
   if ( ReadDummyData( &HypoT, pszDFile ) == 0 ) 
   {
      logit ("t", "Failed to open DummyFile in PatchDummyWithMw\n");
      return 0;
   }
    
/* Update Mw data */
   HypoT.dMwAvg = pHypo->dMwAvg;
   HypoT.iNumMw = pHypo->iNumMw;
   
/* Choose (and update) the preferred magnitude */
   GetPreferredMag( &HypoT );
   pHypo->iNumPMags = HypoT.iNumPMags;
   strcpy( pHypo->szPMagType, HypoT.szPMagType );
   pHypo->dPreferredMag = HypoT.dPreferredMag;

/* Update dummy file */
   if ( WriteDummyData( &HypoT, NULL, pszDFile, 0, 0 ) == 0 )
   {
      logit ("t", "Failed to open DummyFile in PatchDummyWithMw (2)\n");
      return 0;
   }
   return 1;
}

  /******************************************************************
   *                         PutDataInBuffer()                      *
   *                                                                *
   *  Load the data from the input buffer to the large circular     *
   *  buffer.  Update last data value index.                        *
   *                                                                *
   *  Arguments:                                                    *
   *    WaveHead    Pointer to data buffer header                   *
   *    WaveLong    Pointer to data array                           *
   *    plBuff      Pointer to main circular buffer                 *
   *    plBuffCtr   Index tracker in main buffer                    *
   *    lBuffSize   # samples in main circular buffer               *
   *                                                                *
   ******************************************************************/

void PutDataInBuffer( TRACE_HEADER *WaveHead, long *WaveLong, 
                      long *plBuff, long *plBuffCtr, long lBuffSize )
{
   int     i;
   
/* Copy the data buffer */
   for ( i=0; i<WaveHead->nsamp; i++ )
   {
      plBuff[*plBuffCtr] = WaveLong[i];
      *plBuffCtr += 1;
      if ( *plBuffCtr == lBuffSize ) *plBuffCtr = 0;
   }	  
}

      /***********************************************************
       *                      WndProc()                          *
       *                                                         *
       *  This dialog procedure processes messages from the      *
       *  windows screen and messages passed to it from          *
       *  elsewhere.  All                                        *
       *  display is performed in PAINT.  Menu options control   *
       *  the display.                                           *
       *                                                         *
       ***********************************************************/
       
long WINAPI WndProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   static  int    cxScreen, cyScreen;        /* Window size in pixels */
   double  dMmAvg;                           /* Average Mm for Title */
   double  dOldestTime;       /* 1/1/70 time of oldest data in buffer */
   HCURSOR hCursor;           /* Present cursor handle (hourglass or arrow) */
   static  HDC hdc;           /* Device context of screen */
   FILE    *hFile;            /* File pointer */
   static  HANDLE  hMenu;     /* Handle to the menu */
   static  HFONT hPTimeFont;  /* List box font handle (same as titles) */
   HYPO    HypoD;             /* Hypocenter structure from dummy file */
   int     i, j, iCnt;        /* Loop Counters */
   static  int iFirst;        /* Flag CreateChildren the first SIZE message */
   int     iIndex;            /* Index chosen from list */
   int     iMmAvgCnt;         /* Counter for Mm averages */
   LATLON  ll1, ll2;          /* LATLON structures for epicentral dist. */
   long    lData[MMBUFFER_SIZE];      /* Surface wave section of trace */
   long    lNum;              /* Number of samples to transfer to temp buffer */
   long    lStart;            /* Starting index for R-wave from buffer */
   long    lTIndex;           /* Temporary index */
   static  long lTitleFHt, lTitleFWd; /* Font height and width */
   PAINTSTRUCT ps;            /* Paint structure used in WM_PAINT command */
   RECT    rct;               /* RECT (rectangle structure) */
   char    szMmAvg[8], szNumMm[4], szQuake[8]; /* Used in Title */

/* Respond to user input (menu choices, etc.) and system messages */
   switch ( msg )
   {
      case WM_CREATE:         /* Do this the first time through */
         hCursor = LoadCursor( NULL, IDC_ARROW );
         SetCursor( hCursor );
         hMenu = GetMenu( hwnd );
         SetWindowText( hwnd, szTitle );       /* Display the title */
         iFirst = 1;
         for (i=0; i<6; i++) iIndices[i] = -1;  /* Reset indices */
         break;
		 
/* Get screen size in pixels, re-paint, and re-proportion screen */
      case WM_SIZE:
         cyScreen = HIWORD (lParam);
         cxScreen = LOWORD (lParam);
		 
         if ( iFirst )     /* Do this on first WM_SIZE only */
         {
            iFirst = 0;
/* Create the child windows (shown on main screen) */
            if ( CreateChildren( hwnd, cxScreen, cyScreen ) < 0 )
            {
               logit( "t", "Child windows not created\n" );
               PostMessage( hwnd, WM_DESTROY, 0, 0 );
            }
         }
		 
/* Put children in proper place on screen */		 
         if ( MoveChildren( cxScreen, cyScreen ) < 0 )
            logit( "t", "Child windows not moved successfully\n" );
			
/* Compute font size */
         lTitleFHt = cyScreen / 33;
         lTitleFWd = cxScreen / 100;
		 
/* Set the font in the station list box (must be proportional) */
         hPTimeFont = CreateFont( lTitleFHt, lTitleFWd, 
                       0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,
                       OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
                       DEFAULT_PITCH | FF_MODERN, "Elite" );
					   
         SendMessage( hwndStaMm, WM_SETFONT, (unsigned int) hPTimeFont, TRUE );
         FillMmList( hwndStaMm, MmStuff, Nsta );
         InvalidateRect( hwnd, NULL, TRUE );    /* Force a re-PAINT */		 
         break;

/* Respond to menu selections */
      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {			   
            case IDM_STOP_LP:
               RequestSpecificMutex( &mutsem1 );   /* Sem protect buff writes */
               for ( i=0; i<Nsta; i++ ) StaArray[i].iPickStatus = 0;
               strcpy( szTitle, szProcessName );
               SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
               iProcMoment = 0;
               ReleaseSpecificMutex( &mutsem1 ); /* Let someone else have sem */
               logit( "t", "LP processing manually stopped\n" );
               break;
			   
    	    case ID_CHILD_MM:
               if (HIWORD (wParam) == LBN_DBLCLK)	// Double-click 
               {
                  iIndex = SendMessage( hwndStaMm, LB_GETCURSEL, 0, 0 );
                  iCnt = 0;
                  for ( i=0; i<Nsta; i++ )
                     if ( MmStuff[i].dMmMax > 0. )
                        {
                        if ( iIndex == iCnt ) { iIndex = i; goto Next; }
                        iCnt++;
                        }
Next:             iIndices[5] = iIndices[4];
                  iIndices[4] = iIndices[3];
                  iIndices[3] = iIndices[2];
                  iIndices[2] = iIndices[1];
                  iIndices[1] = iIndices[0];
                  iIndices[0] = iIndex;
                  InvalidateRect (hwnd, NULL, TRUE);
               }
               break;
			
            case IDM_COMPUTE_MOMENT:
               RequestSpecificMutex( &mutsem1 );   /* Sem protect buff writes */
               if ( iProcMoment == 1 )
               {
                  for ( i=0; i<Nsta; i++ ) StaArray[i].iPickStatus = 0;
                  strcpy( szTitle, szProcessName );
                  SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
                  iProcMoment = 0;                  
                  logit( "t", "processing stopped in COMPUTE_MOMENT-1\n" );
               }
               for (i=0; i<6; i++) iIndices[i] = -1;  /* Reset indices */
			   
/* Get hypo parameters from dummy file if not triggered through earthworm */			   
               if ( iFromEW == 0 )
               {
                  ReadDummyData( &HStruct, Gparm.DummyFile );
                  HStruct.iQuakeID = 0;
                  HStruct.iVersion = 0;
               }
               iFromEW = 0;
               ll1.dLat = HStruct.dLat;
               ll1.dLon = HStruct.dLon;
			   
/* Initialize Mw results file with hypocenter parameter information */
               if ( (hFile = fopen( Gparm.MwFile, "w" )) != NULL )
               {
                  	// Sanity Check
                  if (HStruct.dOriginTime < 1.E5 || HStruct.dLat < -2.*PI ||
                      HStruct.dLat > 2.*PI || HStruct.dLon < -2.*PI ||
                      HStruct.dLon > 2.*PI )
                  {
                     logit ("t", "Bad quake location in write mw.dat\n");
                     fclose (hFile);
                     goto CantProc;
                  }
                  fprintf( hFile, "%lf\n", HStruct.dLat );
                  fprintf( hFile, "%lf\n", HStruct.dLon );
                  fprintf( hFile, "%lf\n", HStruct.dOriginTime );
                  fclose( hFile );
               }			   
               else
               {
                  logit( "t", "Mw file (%s) not opened\n", Gparm.MwFile );
                  goto CantProc;
               }			   
			   
               strcpy( szTitle, szProcessName );
               strcat( szTitle, " - PROCESSING Mm" );
               SetWindowText( hwndWndProc, szTitle );  /* Display the title */
               logit( "t", "processing started in COMPUTE_MOMENT\n" );
               iProcMoment = 1;
               for ( i=0; i<Nsta; i++ )
               {   
                  InitMmStructure( &MmStuff[i] );
                  if ( !strcmp( StaArray[i].szChannel, "BHZ" ) ||
                       !strcmp( StaArray[i].szChannel, "BHZ10" ) ||
                       !strcmp( StaArray[i].szChannel, "BHZ00" ) ||
                       !strcmp( StaArray[i].szChannel, "BHZXX" ) ) 
                  {
                     StaArray[i].iPickStatus = 1;
				  
/* Compute epicentral distance in degrees and epicenter to station az */
                     ll2.dLat = StaArray[i].dLat;
                     ll2.dLon = StaArray[i].dLon;
                     GeoCent( &ll2 );
                     MmStuff[i].azdelt = GetDistanceAz( &ll1, &ll2 );
   
/* Compute R-wave travel time. Use fastest travelling surface waves
   for start; add 11 minutes for end */
                     MmStuff[i].dMmTravTime = MmStuff[i].azdelt.dDelta*DEGTOKM/
                                              4.3;
                     MmStuff[i].dMmStartTime = HStruct.dOriginTime +
                                               MmStuff[i].dMmTravTime;
                     MmStuff[i].dMmEndTime = MmStuff[i].dMmStartTime + 800.;
   
/* Compute time of oldest data in buffer */
                     if ( StaArray[i].dSampRate > 0. )
                        dOldestTime = StaArray[i].dEndTime - ((double)
                         StaArray[i].lRawCircSize/StaArray[i].dSampRate) +
                         1./StaArray[i].dSampRate;
                     else
                        dOldestTime = 0.;
				   
/* Skip this one if the data is already passed */				   
                     if ( MmStuff[i].dMmStartTime < dOldestTime ) 
                     {		 	  		
                        if ( Gparm.Debug ) logit( "", "%s R-wave start prior to"
                           " bufferstart=%lf, dEndTime=%lf\n",
                           StaArray[i].szStation, dOldestTime,
                           StaArray[i].dEndTime );
                        StaArray[i].iPickStatus = 2;
                        continue;
                     }
					 
/* Or, skip this one if there is no data */				   
                     if ( dOldestTime <= 0. ) 
                     {		 	  		
                        if ( Gparm.Debug ) logit( "", "%s no data\n",
                           StaArray[i].szStation );
                        StaArray[i].iPickStatus = 2;
                        continue;
                     }
	     
/* Is there data in this buffer covering the Mm window? */
                     if ( MmStuff[i].dMmStartTime > dOldestTime &&
   				          MmStuff[i].dMmEndTime   < StaArray[i].dEndTime )
                     {		 	  		
/* First, fill up the buffer */
                        lStart = StaArray[i].lSampIndexR -
                         (long) ((StaArray[i].dEndTime-MmStuff[i].dMmStartTime)
                         *StaArray[i].dSampRate);
                        while ( lStart < 0 ) lStart += StaArray[i].lRawCircSize;
                        lNum = (long) ((MmStuff[i].dMmEndTime-
                         MmStuff[i].dMmStartTime)*StaArray[i].dSampRate);
                        if ( lNum > MMBUFFER_SIZE ) lNum = MMBUFFER_SIZE;
                        if ( lNum > StaArray[i].lRawCircSize )
                             lNum = StaArray[i].lRawCircSize;
                        for ( j=0; j<lNum; j++ )
                        {
                           lTIndex = lStart + j;
                           if ( lTIndex >= StaArray[i].lRawCircSize )
                              lTIndex -= StaArray[i].lRawCircSize;
                           lData[j] = StaArray[i].plRawCircBuff[lTIndex];
                        }
	
/* Compute the Mm */
                        MmStuff[i].dMmMax = Mm( lNum, lData,
                         &MmStuff[i], StaArray[i].dSampRate,
                          StaArray[i].szStation, HStruct.dLat, HStruct.dLon,
                         &Spectra[i], HStruct.dPreferredMag );
/* Convert Mm to Mw */						 
                        if ( MmStuff[i].dMmMax > 0. )
                        {
                           MmStuff[i].dMmMax = MmStuff[i].dMmMax/1.5 + 2.6;
			   
/* Update Mw results file with Mw for this station */
                           if ( (hFile = fopen( Gparm.MwFile, "a" )) != NULL )
                           {
                              fprintf( hFile, "%s %s %s %lf\n",
                               StaArray[i].szStation, StaArray[i].szChannel, 
                               StaArray[i].szNetID, MmStuff[i].dMmMax );
                              fclose( hFile );
                           }			   
                           iIndices[5] = iIndices[4];
                           iIndices[4] = iIndices[3];
                           iIndices[3] = iIndices[2];
                           iIndices[2] = iIndices[1];
                           iIndices[1] = iIndices[0];
                           iIndices[0] = i;
                        }						   
                        StaArray[i].iPickStatus = 2;
                     }
                  }
                  else
                     StaArray[i].iPickStatus = 2;
               }
			   
/* Compute average Mm */
               dMmAvg = ComputeAvgMm( Nsta, MmStuff, &iMmAvgCnt );
               HStruct.dMwAvg = dMmAvg; 
               HStruct.iNumMw = iMmAvgCnt; 
						
/* If this is the same hypo as in the dummy file, update the magnitudes */				  
               if ( dMmAvg > 0. ) 
               {
                  ReadDummyData( &HypoD, Gparm.DummyFile ); 
                  if ( IsItSameQuake( &HStruct, &HypoD ) == 1 )
                     if ( PatchDummyWithMw( &HStruct, Gparm.DummyFile ) == 0 )
                        logit( "t", "Write dummy file error in mm\n");
                  _gcvt( dMmAvg, 2, szMmAvg );
                  if ( dMmAvg - (int) dMmAvg <= 0.05 || 
                       dMmAvg - (int) dMmAvg >= 0.95 ) szMmAvg[2] = '0';
                  strcpy( szTitle, szProcessName );
                  strcat( szTitle, " - PROCESSING Moment - " );
                  strcat( szTitle, szMmAvg );
                  strcat( szTitle, "-" );
                  itoaX( iMmAvgCnt, szNumMm );
                  strcat( szTitle, szNumMm );
                  strcat( szTitle, " - " );
                  itoaX( HStruct.iQuakeID, szQuake );
                  PadZeroes( 4, szQuake );
                  strcat( szTitle, szQuake );
                  SetWindowText( hwndWndProc, szTitle );   /* Display title */
                  FillMmList( hwndStaMm, MmStuff, Nsta );  /* Fill listbox */
               }
               if ( Gparm.Debug == 1 ) logit( "t", "avg=%lf\n", dMmAvg );			
										 
               for ( i=0; i<Nsta; i++ )
                  if ( StaArray[i].iPickStatus != 2 ) goto LoopEnd;
               strcpy( szTitle, szProcessName );
               SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
               iProcMoment = 0;
               logit( "t", "processing stopped in COMPUTE_MOMENT-2\n" );
LoopEnd:
               if ( iProcMomFromThread == 1 ) SetEvent( hEventMomDone );
               iProcMomFromThread = 0;
CantProc:      FillMmList( hwndStaMm, MmStuff, Nsta );
               ReleaseSpecificMutex( &mutsem1 ); /* Let someone else have sem */
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_REFRESH:       // redraw the screen
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_EXIT_MAIN:     /* Send DESTROY message to itself */
               PostMessage( hwnd, WM_DESTROY, 0, 0 );
               break;          
         }
         break ;

/* Fill in screen display */
      case WM_PAINT:
         hdc = BeginPaint( hwnd, &ps );         /* Get device context */
         GetClientRect( hwnd, &rct );
         FillRect( hdc, &rct, (HBRUSH) GetStockObject( WHITE_BRUSH ) );
         DisplayTitles( hdc, lTitleFHt, lTitleFWd, cxScreen, cyScreen );
         DisplayLittoral( hdc, lTitleFHt, lTitleFWd, cxScreen, cyScreen,
                          &HStruct );
         if ( iIndices[0] >= 0 )
            DisplaySpectra( hdc, lTitleFHt, lTitleFWd,
                            cxScreen, cyScreen, MmStuff, Spectra, iIndices );
         EndPaint( hwnd, &ps );
         break;

/* Close up shop and return */
      case WM_DESTROY:
         logit( "", "WM_DESTROY posted\n" );
         PostQuitMessage( 0 );
         break;

      default:
         return ( DefWindowProc( hwnd, msg, wParam, lParam ) );
   }
return 0;
}

      /*********************************************************
       *                     WThread()                         *
       *                                                       *
       *  This thread gets earthworm waveform messages.        *
       *                                                       *
       *********************************************************/
	   
thr_ret WThread( void *dummy )
{
   double        dMmAvg;          /* Average Mm for Title */
   double        dOldestTime;     /* 1/1/70 time of oldest data in buffer */
   FILE          *hFile;          /* File pointer */
   HYPO          HypoD;           /* Hypocenter structure from dummy file */
   int           i, j;
   int           iIndex;          /* Array index of this SCN */
   int           iMmAvgCnt;       /* Number stns used in average Mm (Title) */
   int           iReturn;         /* Return from LoadResponse */
   long          lData[MMBUFFER_SIZE]; /* Surface wave section of trace */
   char          line[40];        /* Heartbeat message */
   int           lineLen;         /* Length of heartbeat message */
   long          lNum;            /* # of samples to transfer to temp buffer */
   long          lStart;          /* Starting index for R-wave from buffer */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          lTIndex;         /* Temporary index */
   long          MsgLen;          /* Size of retrieved message */
   long          RawBufl;         /* Raw data buffer size */
   char          szAvgMm[8], szNumMm[4], szQuake[8]; /* Used in Title */

/* Loop to read waveform messages
   ******************************/
   while ( tport_getflag( &Gparm.InRegion ) != TERMINATE )
   {
      long    lGapSize;         /* Number of missing samples (integer) */
      static  MMSTUFF *MmT;     /* Pointer to the station being processed (Mm)*/
      static  time_t  now;      /* Current time */
      int     rc;               /* Return code from tport_getmsg() */
      static  STATION *Sta;     /* Pointer to the station being processed */
      char    szType[3];        /* Incoming data format type */
	  
/* Send a heartbeat to the transport ring
   **************************************/
      time( &now );
      if ( (now - then) >= Gparm.HeartbeatInt )
      {
         then = now;
         sprintf( line, "%d %d\n", now, myPid );
         lineLen = strlen( line );
         if ( tport_putmsg( &Gparm.InRegion, &hrtlogo, lineLen, line ) !=
              PUT_OK )
         {
            logit( "et", "mm: Error sending heartbeat." );
            break;
         }
      }
      
/* Get a waveform from transport region
   ************************************/
      rc = tport_getmsg( &Gparm.InRegion, &getlogoW, 1, &logo, &MsgLen,
                         WaveBuf, MAX_TRACEBUF_SIZ);

      if ( rc == GET_NONE )
      {
         sleep_ew( 100 );
         continue;
      }

      if ( rc == GET_NOTRACK )
         logit( "et", "mm: Tracking error.\n");

      if ( rc == GET_MISS_LAPPED )
         logit( "et", "mm: Got lapped on the ring.\n");

      if ( rc == GET_MISS_SEQGAP )
         logit( "et", "mm: Gap in sequence numbers.\n");

      if ( rc == GET_MISS )
         logit( "et", "mm: Missed messages.\n");

      if ( rc == GET_TOOBIG )
      {
         logit( "et", "mm: Retrieved message too big (%d) for msg.\n",
                MsgLen );
         continue;
      }

/* If necessary, swap bytes in the message
   ***************************************/
      if ( WaveMsgMakeLocal( WaveHead ) < 0 )
      {    
         logit( "et", "mm: Unknown waveform type.\n" );
         continue;
      }
	  
/* If sample rate is 0, get out of here before it kills program
   ************************************************************/
      if ( WaveHead->samprate == 0. )
      {
         logit( "", "Sample rate=0., %s %s\n", WaveHead->sta, WaveHead->chan );
         continue;
      }

/* If we are in the ATPlayer version of mm, see if we should re-init
   *****************************************************************/
      if ( strlen( Gparm.ATPLineupFileLP ) > 2 )     /* Then we are */
         if ( WaveHead->starttime-(int) dLastEndTime > 3600) /* Big gap */
         {
            RequestSpecificMutex( &mutsem1 );   /* Sem protect buff writes */
            Nsta = ReadLineupFile( Gparm.ATPLineupFileLP, StaArray );
            if ( Nsta < 2 )
            {
               logit( "", "Bad Lineup File read %s\n", Gparm.ATPLineupFileLP );
               continue;
            }	    
            for ( i=0; i<Nsta; i++ )
            {
               free( StaArray[i].plRawCircBuff );
               iReturn = LoadResponseData( &StaArray[i], &MmStuff[i],
                         Gparm.ResponseFile );
               if ( iReturn == -1 )
               {
                  logit( "e", "file: %s\n", Gparm.ResponseFile );
                  logit( "e", "scn = %s %s %s\n", StaArray[i].szStation,
                   StaArray[i].szChannel, StaArray[i].szNetID );
               }
               else if ( iReturn == 0 )
                  logit( "", "scn = %s %s %s - No RESPONSE info\n",
                   StaArray[i].szStation, StaArray[i].szChannel, StaArray[i].szNetID );
               else if ( iReturn == 1 )
                  logit( "", "scn = %s %s %s - RESPONSE info read\n",
                   StaArray[i].szStation, StaArray[i].szChannel, StaArray[i].szNetID );
               InitVar( &StaArray[i] );	  
               StaArray[i].iFirst = 1;
            }
            ReleaseSpecificMutex( &mutsem1 ); /* Let someone else have sem */
         } 	 
 
/* Look up SCN number in the station list
   **************************************/
      Sta = NULL;								  
      for ( i=0; i<Nsta; i++ )
         if ( !strcmp( WaveHead->sta,  StaArray[i].szStation ) &&
              !strcmp( WaveHead->chan, StaArray[i].szChannel ) &&
              !strcmp( WaveHead->net,  StaArray[i].szNetID ) )
         {
            Sta = (STATION *) &StaArray[i];
            MmT = (MMSTUFF *) &MmStuff[i];
            iIndex = i;
            break;
         }

      if ( Sta == NULL )      /* SCN not found */
         continue;
		 
/* Check if the time stamp is reasonable.  If it is ahead of the present
   1/1/70 time, it is not reasonable. (+1. to account for int).
   *********************************************************************/
      if ( WaveHead->endtime > (double) now+1. )
      {
         logit( "t", "%s %s endtime (%lf) ahead of present (%ld)\n",
                Sta->szStation, Sta->szChannel, WaveHead->endtime, now );
         continue;  
      }

/* Do this the first time we get a message with this SCN
   *****************************************************/
      if ( Sta->iFirst == 1 )
      {
         logit( "", "Init %s %s\n", Sta->szStation, Sta->szChannel );	  
         Sta->iFirst = 0;
         Sta->iPickStatus = 0;
         Sta->dEndTime = WaveHead->starttime - 1./WaveHead->samprate;
         Sta->dSampRate = WaveHead->samprate;
         ResetFilter( Sta );
		 		 
/* Allocate memory for raw circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples)
   ************************************************************************/
         Sta->lRawCircSize =
          (long) (WaveHead->samprate*(double)Gparm.MinutesInBuff*60.+0.1);
         RawBufl = sizeof (long) * Sta->lRawCircSize;
         Sta->plRawCircBuff = (long *) malloc( (size_t) RawBufl );
         if ( Sta->plRawCircBuff == NULL )
         {
            logit( "et", "mm: Can't allocate raw circ buffer for %s\n",
			       Sta->szStation );
            PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
            return;
         }
      }      
      
/* If data is not in order, throw it out
   *************************************/
      if ( Sta->dEndTime >= WaveHead->starttime )
      {
         if ( Gparm.Debug ) logit( "e", "%s out of order\n", Sta->szStation );
         continue;
      }

/* If the samples are shorts, make them longs
   ******************************************/
      strcpy( szType, WaveHead->datatype );
      if ( (strcmp( szType, "i2" ) == 0) || (strcmp( szType, "s2") == 0) )
         for ( i=WaveHead->nsamp-1; i>-1; i-- )
            WaveLong[i] = (long) WaveShort[i];
			
/* Compute the number of samples since the end of the previous message.
   If (lGapSize == 1), no data has been lost between messages.
   If (1 < lGapSize <= 2), go ahead anyway.
   If (lGapSize > 2), re-initialize filter variables.
   *******************************************************************/
      lGapSize = (long) (WaveHead->samprate *
                        (WaveHead->starttime-Sta->dEndTime) + 0.5);

/* Announce gaps
   *************/
      if ( lGapSize > 2 )
      {
         int      lineLen;
         time_t   errTime;
         char     errmsg[80];
         MSG_LOGO logo;

         time( &errTime );
         sprintf( errmsg,
               "%d 1 Found %4d sample gap. Restarting station %-5s%-2s%-3s\n",
               errTime, lGapSize, Sta->szStation, Sta->szNetID,
               Sta->szChannel );
         lineLen = strlen( errmsg );
         logo.type   = Ewh.TypeError;
         logo.mod    = Gparm.MyModId;
         logo.instid = Ewh.MyInstId;
         tport_putmsg( &Gparm.InRegion, &logo, lineLen, errmsg );
         if ( Gparm.Debug )
            logit( "t", "mm: Restarting %-5s%-2s %-3s. lGapSize = %d\n",
                     Sta->szStation, Sta->szNetID, Sta->szChannel, lGapSize ); 

/* For big gaps reset filter 
   *************************/
         ResetFilter( Sta );
         Sta->dSumLDCRaw = 0.;
         Sta->dAveLDCRaw = 0.;
         Sta->dSumLDC = 0.;
      }

/* For gaps less than the size of the buffer, pad buffers with DC
   **************************************************************/
      if ( lGapSize > 1 && lGapSize <= 2 )
         PadBuffer( lGapSize, Sta->dAveLDCRaw, &Sta->lSampIndexR,
                    Sta->plRawCircBuff, Sta->lRawCircSize );
	  
/* For gaps greater than the size of the buffer, re-init
   *****************************************************/
      if ( lGapSize >= Sta->lRawCircSize )
      {
         Sta->dSumLDCRaw = 0.;
         Sta->dAveLDCRaw = 0.;
         Sta->dSumLDC = 0.;
         Sta->lSampIndexR = 0;
         Sta->iPickStatus = 0;
      }
      
/* In this module, lEndData is the first buffer index in this packet
   *****************************************************************/      
      Sta->lEndData = Sta->lSampIndexR;

/* Compute DC offset for raw data
   ******************************/
      GetLDC( WaveHead->nsamp, WaveLong, &Sta->dAveLDCRaw, Sta->lFiltSamps );
			
/* Tuck raw data into proper location in buffer
   ********************************************/			
      PutDataInBuffer( WaveHead, WaveLong, Sta->plRawCircBuff,
                       &Sta->lSampIndexR, Sta->lRawCircSize );
		       
/* Keep track of last data (for use with Player)
   *********************************************/
      if ( WaveHead->endtime > dLastEndTime+5.0 )
         dLastEndTime = WaveHead->endtime;
	   
/* Save time of the end of the current message
   *******************************************/
      Sta->dEndTime = WaveHead->endtime;

/* Process data if this station is still waiting on Rayleigh wave data
   *******************************************************************/
      if ( iProcMoment == 1 )
      {
         RequestSpecificMutex( &mutsem1 );   /* Sem protect buffer writes */
         if ( Sta->iPickStatus == 1 )
         {
/* Compute time of oldest data in buffer */
            dOldestTime = Sta->dEndTime - ((double) Sta->lRawCircSize/
             Sta->dSampRate) + 1./Sta->dSampRate;
	     
/* Is there data in this buffer covering the Mm window? */
            if ( MmT->dMmStartTime > dOldestTime &&
   				 MmT->dMmEndTime   < Sta->dEndTime )
            {		 	  		
			
/* First, fill up the buffer */
               lStart = Sta->lSampIndexR -
                (long) ((Sta->dEndTime-MmT->dMmStartTime)*Sta->dSampRate);
               while ( lStart < 0 ) lStart += Sta->lRawCircSize;
               lNum = (long) ((MmT->dMmEndTime-MmT->dMmStartTime)*
                Sta->dSampRate);
               if ( lNum > MMBUFFER_SIZE ) lNum = MMBUFFER_SIZE;
               if ( lNum > Sta->lRawCircSize ) lNum = Sta->lRawCircSize;
               for ( j=0; j<lNum; j++ )
               {
                  lTIndex = lStart + j;
                  if ( lTIndex >= Sta->lRawCircSize )
                     lTIndex -= Sta->lRawCircSize;
                  lData[j] = Sta->plRawCircBuff[lTIndex];
               }
	
/* Compute the Mm */
               MmT->dMmMax = Mm( lNum, lData, MmT, Sta->dSampRate,
			                     Sta->szStation, HStruct.dLat, HStruct.dLon,
                                 &Spectra[i], HStruct.dPreferredMag );
               Sta->iPickStatus = 2;
               if ( MmT->dMmMax > 0. )                  /* Convert Mm to Mw */
               {
                  MmT->dMmMax = MmT->dMmMax/1.5 + 2.6;
			   
/* Update Mw results file with Mw for this station */
                  if ( (hFile = fopen( Gparm.MwFile, "a" )) != NULL )
                  {
                     fprintf( hFile, "%s %s %s %lf\n", Sta->szStation,
                              Sta->szChannel, Sta->szNetID, MmT->dMmMax );
                     fclose( hFile );
                  }			   
			   
/* Compute average Mm */
                  dMmAvg = ComputeAvgMm( Nsta, MmStuff, &iMmAvgCnt );
                  HStruct.dMwAvg = dMmAvg; 
                  HStruct.iNumMw = iMmAvgCnt; 
						
/* If this is the same hypo as in the dummy file, update the magnitudes */				  
                  if ( dMmAvg > 0. ) 
                  {
                     ReadDummyData( &HypoD, Gparm.DummyFile ); 
                     if ( IsItSameQuake( &HStruct, &HypoD ) == 1 )
                        if ( PatchDummyWithMw( &HStruct,Gparm.DummyFile ) == 0 )
                           logit( "t", "Write dummy file error in mm\n");
                     _gcvt( dMmAvg, 2, szAvgMm );
                     if ( dMmAvg - (int) dMmAvg <= 0.05 || 
                          dMmAvg - (int) dMmAvg >= 0.95 ) szAvgMm[2] = '0';
                     strcpy( szTitle, szProcessName );
                     strcat( szTitle, " - PROCESSING Moment - " );
                     strcat( szTitle, szAvgMm );
                     strcat( szTitle, "-" );
                     itoaX( iMmAvgCnt, szNumMm );
                     strcat( szTitle, szNumMm );
                     strcat( szTitle, " - " );
                     itoaX( HStruct.iQuakeID, szQuake );
                     PadZeroes( 4, szQuake );
                     strcat( szTitle, szQuake );
                     SetWindowText( hwndWndProc, szTitle );  /* Display title */
                     FillMmList( hwndStaMm, MmStuff, Nsta );  /* Fill listbox */
                     iIndices[5] = iIndices[4];
                     iIndices[4] = iIndices[3];
                     iIndices[3] = iIndices[2];
                     iIndices[2] = iIndices[1];
                     iIndices[1] = iIndices[0];
                     iIndices[0] = iIndex;
                     InvalidateRect( hwndWndProc, NULL, TRUE );
                  }
                  if ( Gparm.Debug == 1 ) logit( "t", "%s - Mw=%lf, avg=%lf\n",
                                          Sta->szStation, MmT->dMmMax, dMmAvg );			
               }
            }
         }
		 
/* Is it time to reset title bar */		 
         for ( i=0; i<Nsta; i++ )
            if ( (double) now < MmStuff[i].dMmEndTime ) goto LoopEnd;
         for ( i=0; i<Nsta; i++ ) StaArray[i].iPickStatus = 0;
         strcpy( szTitle, szProcessName );
         SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
         iProcMoment = 0;
         logit( "t", "Moment processing stopped in WThread\n" );
LoopEnd:
         ReleaseSpecificMutex( &mutsem1 ); /* Let someone else have sem */
      }
   }   
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
}
