
       /****************************************************************
       *                                                               *
       *                        hypo_display.c                         *
       *                                                               *
       *   This program takes hypocenter information dropped in InRing *
       *  and displays it on a Windows window.  An RC file goes along  *
       *  with this to describe the window menu, set-up, etc.          *
       *                                                               *
       *  This program is strictly for use under Windows and was       *
       *  designed to take locations produced by loc_wcatwc.  It uses  *
       *  many options from EarlyBird created at the West Coast/Alaska *
       *  Tsunami Warning Center.  It has also borrowed some ideas from*
       *  module GLASS as far as the display goes.                     *
       *                                                               *
       *  The program just displays                                    *
       *  hypocenter information and waveforms on an easy to           *
       *  read and interact with display.  The display is broken into  *
       *  four sections.  The top section lists the last MAX_QUAKES    *
       *  locations written to the InRing.  Only the latest version    *
       *  of each is shown.  Information is listed about each quake,   *
       *  such as: ID, version, O-time, date, lat, lon, depth,         *
       *  residual, azimuth, # stns., and magnitude data.  When a quake*
       *  is clicked on with the left button, the quake's associated   *
       *  P-picks and waveforms are shown in the lower boxes.          *
       *  The box in the lower-left corner gives each P-pick           *
       *  used to calculate the hypocenter, its residual, distance,    *
       *  azimuth, and any magnitudes computed from this trace.  The   *
       *  lower-right box is a display of all P-waveforms used in the  *
       *  location.  P's can be adjusted here and the quake re-        *
       *  located elsewhere.  The Ps are also sent back to loc_wcatwc  *
       *  via PickRing. Just above this box is a littoral location and *
       *  preferred magnitude for the earthquake.                      *
       *                                                               *
       *  Written by Paul Whitmore, (WC/ATWC) March, 2001              *
       *                                                               *
       *  January, 2006: Removed P-time list box and expanded trace    *
       *                 window.                                       *
       *  October, 2005: Added options to run this module in           *
       *                 conjunction with ATPlayer. !!!                *
       *  Sept., 2004: Added menu option to force loc_wcatwc to        * 
       *               relocate present quake immediately.             *
       *                                                               *
       ****************************************************************/
	   
#include <windows.h>
#include <wingdi.h>
#include <winuser.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include "hypo_display.h"

/* Global Variables (those needed in threads)
   ******************************************/
CITY   city[NUM_CITIES];       /* Array of reference city locations */
CITY   cityEC[NUM_CITIES_EC];  /* Array of eastern reference city locations */
int    cxScreen, cyScreen;     /* Window size in pixels */
double dMwpSigNoise;           /* Mwp signal-to-noise ratio */
EWH    Ewh;                    /* Parameters from earthworm.h */
GPARM  Gparm;                  /* Configuration file parameters */
HINSTANCE hInstMain;           /* Copy of main program instance (process id) */
MSG_LOGO hrtlogo;              /* Logo of outgoing heartbeats */
HYPO   HStruct;                /* Hypocenter data structure */
HWND   hwndChildA;             /* Child window A handle */
HWND   hwndChildB;             /* Child window B handle */
HWND   hwndWndProc;            /* Client window handle */
HWND   hwndHypo;               /* Window handle to hypocenter window */
HWND   hwndPTime;              /* Window handle to P-time window */
HYPO   Hypo[MAX_QUAKES];       /* Array of Hypocenter structures */
int    iActiveHypo;            /* Index of hypocenter to display */
int    iChildA;                /* 1 when waveform window is on */
int    iChildB;                /* 1 when Mwp display window is on */
int    iFromMainWnd;           /* 1 if RECOMPUTE_MWP called from menu */
int    iMwpSeconds;            /* Mwp integration window */
int    iNumPAuto;              /* # P-picks from auto-location */
int    iTimerGoing;            /* 1->Timer has been started */
int    iTimerRedoMwp;          /* 1->Update Mwp every X seconds */
long   lPickCounter;           /* Pick Counter (20000-30000) assigned here */
MSG    msg;                    /* Windows control message variable */
mutex_t mutsem1;               /* Semaphore to protect variable adjustments */
pid_t  myPid;                  /* Process id of this process */
int    Nsta;                   /* Number of stations to display */
ONSCREEN *OnScreen;            /* SCNs drawn on child window */
PPICK  *PBufG;                 /* Pointer to P-pick buffer (all stn) */
PPICK  *PBufL;                 /* Pointer to P-pick buffer (just auto-ps) */
STATION *StaArray;             /* Station data array */
char   szChildAName[] = "Waveforms"; /* Class name created for ChildA */
char   szChildBName[] = "Integrated Waveform Display"; /* Class name ChildB */
time_t then;                   /* Previous heartbeat time */

      /***********************************************************
       *              The main program starts here.              *
       *                                                         *
       *  Argument:                                              *
       *     argv[1] = Name of configuration file                *
       ***********************************************************/

int WINAPI WinMain (HINSTANCE hInst, HINSTANCE hPreInst, 
                    LPSTR lpszCmdLine, int iCmdShow)
{
   char          szProcessName[] = "Hypocenter Display";
   static WNDCLASS wc;
   HDC     hIC;	                  /* Information context to check for info */
   int           i;
   long          InBufl;          /* Buffer size in bytes */
   int           lineLen;         /* Length of heartbeat message */
   char          line[40];        /* Heartbeat message */
   char          configfile[64];  /* Name of config file */
   long          RawBufl;         /* Raw data buffer size (for Mwp) */
   static unsigned tidH;          /* Hypocenter getter Thread */
   
   hInstMain = hInst;
   iChildA = 0;
   iChildB = 0;
   lPickCounter = 20000;
   
/* Get config file name (format "HYPO_DISPLAY HYPO_DISPLAY.D")
   ***********************************************************/
   if ( strlen( lpszCmdLine ) <= 0 )
   {
      fprintf( stderr, "Need configfile in start line.\n" );
      return -1;
   }
   strcpy( configfile, lpszCmdLine );

/* Get parameters from the configuration files
   *******************************************/
   if ( GetConfig( configfile, &Gparm ) == -1 )
   {
      fprintf( stderr, "GetConfig() failed. file %s.\n", configfile );
      return -1;
   }

/* Look up info in the earthworm.h tables
   **************************************/
   if ( GetEwh( &Ewh ) < 0 )
   {
      fprintf( stderr, "hypo_display: GetEwh() failed. Exiting.\n" );
      return -1;
   }

/* Specify heartbeat logos
   ***********************/
   hrtlogo.instid = Ewh.MyInstId;
   hrtlogo.mod    = Gparm.MyModId;
   hrtlogo.type   = Ewh.TypeHeartBeat;

/* Initialize name of log-file & open it
   *************************************/
   logit_init( configfile, Gparm.MyModId, 256, 1 );

/* Get our own pid for restart purposes
   ************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "hypo_display: Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Log the configuration parameters
   ********************************/
   LogConfig( &Gparm );

/* Load reference city coordinates used in littoral locations
  ***********************************************************/
   if ( LoadCities( city, 1 ) < 0)
   {
      logit( "t", "LoadCities failed, exiting\n" );
      return -1;
   }

/* Load eastern reference city coordinates used in littoral locations
  *******************************************************************/
   if ( LoadCitiesEC( cityEC, 1 ) < 0)
   {
      logit( "t", "LoadCitiesEC failed, exiting\n" );
      return -1;
   }

/* Read the station list and return the number of stations found.
   Allocate the station list array.
   *************************************************************/
   if ( GetStaList( &StaArray, &Nsta, &Gparm ) == -1 )
   {
      fprintf( stderr, "hypo_display: GetStaList() failed. Exiting.\n" );
      return -1;
   }
   if ( Nsta == 0 )
   {
      logit( "et", "hypo_display: Empty station list. Exiting." );
      free( StaArray );
      return -1;
   }
   logit( "t", "hypo_display: Displaying %d stations.\n", Nsta );

/* Log the station list
   ********************/
   LogStaList( StaArray, Nsta );
   
/* Allocate the waveform buffers
   *****************************/
   for ( i=0; i<Nsta; i++ )
   {
      ResetFilter( &StaArray[i] );
		 
/* Allocate memory for Mwp displacement data buffer (*2 for old data) 
   ******************************************************************/
      RawBufl = sizeof (double) * (long) (StaArray[i].dSampRate*2. + 0.001) *
                (Gparm.MwpSeconds+20);
      if ( RawBufl/sizeof (double) > MAXMWPARRAY )
      {
         logit( "et", "Mwp array too large for %s\n", StaArray[i].szStation );
         return -1;
      }
      StaArray[i].pdRawDispData = (double *) malloc( (size_t) RawBufl );
      if ( StaArray[i].pdRawDispData == NULL )
      {
         logit( "et", "hypo_display: Can't allocate disp. data buff for %s\n",
                StaArray[i].szStation );
         return -1;
      }
		 
/* Allocate memory for Mwp integrated displacement data buffer 
   ***********************************************************/
      StaArray[i].pdRawIDispData = (double *) malloc( (size_t) RawBufl );
      if ( StaArray[i].pdRawIDispData == NULL )
      {
         logit( "et", "hypo_display: Cannot allocate int. disp. data buffer for %s\n",
                StaArray[i].szStation );
         return -1;
      }
		 		                  
/* Allocate memory for raw circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples) (*2 to account for old data which may have
   larger sample rate).
   ********************************/
      StaArray[i].lRawCircSize =
       (long) (StaArray[i].dSampRate*2.*(double)Gparm.MinutesInBuff*60.+0.1);
      RawBufl = sizeof (long) * StaArray[i].lRawCircSize;
      StaArray[i].plRawCircBuff = (long *) malloc( (size_t) RawBufl );
      if ( StaArray[i].plRawCircBuff == NULL )
      {
         logit( "et", "hypo_display: Can't allocate raw circ buffer for %s\n",
                StaArray[i].szStation );
         return -1;
      }
		 		 
/* Allocate memory for filt. circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples)
   ********************************/
      StaArray[i].plFiltCircBuff = (long *) malloc( (size_t) RawBufl );
      if ( StaArray[i].plFiltCircBuff == NULL )
      {
         logit( "et", "hypo_display: Can't allocate filt circ buffer for %s\n",
                StaArray[i].szStation );
         return -1;
      }
   }      

/* Allocate and init the P-pick and OnScreen buffers
   *************************************************/
   InBufl = sizeof( PPICK ) * Nsta; 
   PBufG = (PPICK *) malloc( (size_t) InBufl );
   if ( PBufG == NULL )
   {
      for ( i=0; i<Nsta; i++ ) 
      {
         free( StaArray[i].pdRawDispData );
         free( StaArray[i].pdRawIDispData );
         free( StaArray[i].plRawCircBuff );
         free( StaArray[i].plFiltCircBuff );
      }
      free( StaArray );
      logit( "et", "hypo_display: Cannot allocate ppick buffer\n");
      return -1;
   }
   for ( i=0; i<Nsta; i++ ) InitP( &PBufG[i] );
   for ( i=0; i<Nsta; i++ ) PBufG[i].iUseMe = 0;
   
   PBufL = (PPICK *) malloc( (size_t) InBufl );
   if ( PBufL == NULL )
   {
      free( PBufG );
      for ( i=0; i<Nsta; i++ ) 
      {
         free( StaArray[i].pdRawDispData );
         free( StaArray[i].pdRawIDispData );
         free( StaArray[i].plRawCircBuff );
         free( StaArray[i].plFiltCircBuff );
      }
      free( StaArray );
      logit( "et", "hypo_display: Cannot allocate ppick buffer -2\n");
      return -1;
   }
   for ( i=0; i<Nsta; i++ ) InitP( &PBufL[i] );
   for ( i=0; i<Nsta; i++ ) PBufL[i].iUseMe = 0;
   
   InBufl = sizeof( ONSCREEN ) * Nsta; 
   OnScreen = (ONSCREEN *) malloc( (size_t) InBufl );
   if ( OnScreen == NULL )
   {
      free( PBufG );
      free( PBufL );
      for ( i=0; i<Nsta; i++ ) 
      {
         free( StaArray[i].pdRawDispData );
         free( StaArray[i].pdRawIDispData );
         free( StaArray[i].plRawCircBuff );
         free( StaArray[i].plFiltCircBuff );
      }
      free( StaArray );
      logit( "et", "hypo_display: Cannot allocate OnScreen\n");
      return -1;
   }
   
/* Create mutexes for protecting adjustments of variables
   ******************************************************/
   CreateSpecificMutex( &mutsem1 );
   
/* If this is the first instance of this program, init window stuff and
   register window (it always is)
   ********************************************************************/
   if ( !hPreInst )
   {  /* Force PAINT when sized and give double click notification */
      wc.style         = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
      wc.lpfnWndProc   = WndProc;         /* Control Window process */
      wc.cbClsExtra    = 0;
      wc.cbWndExtra    = 0;
      wc.hInstance     = hInst;           /* Process id */
      wc.hIcon         = LoadIcon( hInst, "hypo_display" );/* System app icon */
      wc.hCursor       = LoadCursor( NULL, IDC_ARROW );    /* Pointer */
      wc.hbrBackground = (HBRUSH) GetStockObject( WHITE_BRUSH ); /* White */
      wc.lpszMenuName  = "Hypo_menu";     /* Relates to .RC file */
      wc.lpszClassName = szProcessName;
      if ( !RegisterClass( &wc ) )        /* Window not registered */
      {
         logit( "t", "RegisterClass failed\n" );
         free( PBufG );
         free( PBufL );
         free( OnScreen );
         for ( i=0; i<Nsta; i++ ) 
         {
            free( StaArray[i].pdRawDispData );
            free( StaArray[i].pdRawIDispData );
            free( StaArray[i].plRawCircBuff );
            free( StaArray[i].plFiltCircBuff );
         }
         free( StaArray );
         return -1;
      }
	  
/* And register child window A (trace display)
   *******************************************/
      wc.style           = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;  
      wc.lpfnWndProc     = ChildAWndProc;            /* Window process */
      wc.cbClsExtra      = 0;
      wc.cbWndExtra      = 0;
      wc.hInstance       = hInst;                    /* Process id */
      wc.hIcon           = NULL;
      wc.hCursor         = NULL;
      wc.hbrBackground   = GetStockObject( WHITE_BRUSH ); /* White background */
      wc.lpszMenuName    = NULL;                     /* Relates to .RC file */
      wc.lpszClassName   = szChildAName;
      if ( !RegisterClass( &wc ) )                   /* Window not registered */
      {
         logit( "t", "RegisterClass 2 failed\n" );
         free( PBufG );
         free( PBufL );
         free( OnScreen );
         for ( i=0; i<Nsta; i++ ) 
         {
            free( StaArray[i].pdRawDispData );
            free( StaArray[i].pdRawIDispData );
            free( StaArray[i].plRawCircBuff );
            free( StaArray[i].plFiltCircBuff );
         }
         free( StaArray );
         return -1;
      }
	  
/* And child window B (Mwp display)
   ********************************/
      wc.style           = CS_HREDRAW | CS_VREDRAW;  
      wc.lpfnWndProc     = ChildBWndProc;            /* Window process */
      wc.cbClsExtra      = 0;
      wc.cbWndExtra      = 0;
      wc.hInstance       = hInst;                    /* Process id */
      wc.hIcon           = NULL;
      wc.hCursor         = NULL;
      wc.hbrBackground   = GetStockObject( WHITE_BRUSH ); /* White background */
      wc.lpszMenuName    = NULL;                     /* Relates to .RC file */
      wc.lpszClassName   = szChildBName;
      if ( !RegisterClass( &wc ) )                   /* Window not registered */
      {
         logit( "t", "RegisterClass 2 failed\n" );
         free( PBufG );
         free( PBufL );
         free( OnScreen );
         for ( i=0; i<Nsta; i++ ) 
         {
            free( StaArray[i].pdRawDispData );
            free( StaArray[i].pdRawIDispData );
            free( StaArray[i].plRawCircBuff );
            free( StaArray[i].plFiltCircBuff );
         }
         free( StaArray );
         return -1;
      }
   }

/* Create the window
   *****************/
   hIC = CreateIC ("DISPLAY", NULL, NULL, NULL);
   if (hIC == NULL)
   {
      logit( "t", "CreateIC error\n" );
      free( PBufG );
      free( PBufL );
      free( OnScreen );
      for ( i=0; i<Nsta; i++ ) 
      {
         free( StaArray[i].pdRawDispData );
         free( StaArray[i].pdRawIDispData );
         free( StaArray[i].plRawCircBuff );
         free( StaArray[i].plFiltCircBuff );
      }
      free( StaArray );
      return -1;
   }
   hwndWndProc = CreateWindow(
                 szProcessName,          /* Process name */
                 szProcessName,          /* Initial title bar caption */
                 WS_OVERLAPPEDWINDOW,    /* Standard window style */
//                 CW_USEDEFAULT,          /* top left x starting location */
//                 CW_USEDEFAULT,          /* top left y starting location */
//                 CW_USEDEFAULT,          /* Initial screen width in pixels */
//                 CW_USEDEFAULT,          /* Initial screen height in pixels */
                 0,                      /* top left x starting location */
                 GetDeviceCaps (hIC, VERTRES)/2, /* top left starting loc. */
                 GetDeviceCaps (hIC, HORZRES)/4, /* Window width in pixels */
                 GetDeviceCaps (hIC, VERTRES)/2, /* Window height in pixels */
                 NULL,                   /* No parent window */
                 NULL,                   /* Use standard system menu */
                 hInst,                  /* Process id */
                 NULL );                 /* No extra data to pass in */
   if ( hwndWndProc == NULL )            /* Window not created */
   {
      logit( "t", "CreateWindow failed\n" );
      free( PBufG );
      free( PBufL );
      free( OnScreen );
      for ( i=0; i<Nsta; i++ ) 
      {
         free( StaArray[i].pdRawDispData );
         free( StaArray[i].pdRawIDispData );
         free( StaArray[i].plRawCircBuff );
         free( StaArray[i].plFiltCircBuff );
      }
      free( StaArray );
      return 0;
   }
   
   ShowWindow( hwndWndProc, iCmdShow );  /* Show the Window */
   UpdateWindow( hwndWndProc );          /* Force an initial PAINT call */

/* Attach to existing transport rings
   **********************************/
   tport_attach( &Gparm.InRegion, Gparm.InKey );
   tport_attach( &Gparm.PRegion, Gparm.PKey );

/* Send 1st heartbeat to the transport ring
   ****************************************/
   time( &then );
   sprintf( line, "%d %d\n", then, myPid );
   lineLen = strlen( line );
   if ( tport_putmsg( &Gparm.InRegion, &hrtlogo, lineLen, line ) != PUT_OK )
   {
      logit( "et", "hypo_display: Error sending 1st heartbeat. Exiting." );
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.PRegion );
      free( PBufG );
      free( PBufL );
      free( OnScreen );
      for ( i=0; i<Nsta; i++ ) 
      {
         free( StaArray[i].pdRawDispData );
         free( StaArray[i].pdRawIDispData );
         free( StaArray[i].plRawCircBuff );
         free( StaArray[i].plFiltCircBuff );
      }
      free( StaArray );
      PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
      return 0;
   }

/* Start the Hypocenter getter thread
   **********************************/
   if ( StartThread( HThread, 8192, &tidH ) == -1 )
   {
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.PRegion );
      free( PBufG );
      free( PBufL );
      free( OnScreen );
      for ( i=0; i<Nsta; i++ ) 
      {
         free( StaArray[i].pdRawDispData );
         free( StaArray[i].pdRawIDispData );
         free( StaArray[i].plRawCircBuff );
         free( StaArray[i].plFiltCircBuff );
      }
      free( StaArray );
      PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
      logit( "et", "Error starting H thread; exiting!\n" );
      return -1;
   }
   
/* Main windows loop
   *****************/      
   while ( GetMessage( &msg, NULL, 0, 0 ) )
   {
      TranslateMessage( &msg );
      DispatchMessage( &msg );
   }

/* Detach from the ring buffers
   ****************************/
   tport_detach( &Gparm.InRegion );
   tport_detach( &Gparm.PRegion );
   free( PBufG );
   free( PBufL );
   free( OnScreen );
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
   if( iChildA == 1 )
   {
      DestroyWindow( hwndChildA );
      iChildA = 0;
   } 
   if( iChildB == 1 )
   {
      DestroyWindow( hwndChildB );
      iChildB = 0;
   } 
   for ( i=0; i<Nsta; i++ ) 
   {
      free( StaArray[i].pdRawDispData );
      free( StaArray[i].pdRawIDispData );
      free( StaArray[i].plRawCircBuff );
      free( StaArray[i].plFiltCircBuff );
   }
   free( StaArray );
   logit( "t", "Termination requested. Exiting.\n" );
   return (msg.wParam);
}

      /***********************************************************
       *                  ChildAWndProc()                        *
       *                                                         *
       *  This dialog procedure processes messages from the child*
       *  waveform window and messages passed to it from         *
       *  elsewhere.  All input and output to/from this window   *
       *  is take care of here.                                  *
       *                                                         *
       *  January, 2002: Set iUseMe to 2 when P-pick made or     *
       *                 adjusted here.                          *
       *                                                         *
       ***********************************************************/
	   
long WINAPI ChildAWndProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   static  int  cxScreenA, cyScreenA; /* Display area in pixels */
   double  dTemp;
   static  HDC  hdc;                  /* Handle to Device context */
   int     i, j, k, iTemp;
   static  int  iChanIDOffset;        /* Trace offset at left of screen */
   static  int  iDeltaPerLine, iAccumDelta;  /* Mousewheel variables */
   static  int  iHScrollOffset;       /* Horizontal scoll bar setting */
   static  int  iNumOnScreen;         /* Number of traces displayed */
   int     iNumShown;         /* Number of traces displayed in DisplayTraces */
   static  int  iScrollHorzBuff;      /* Small horizontal scroll amount (pxl) */
   static  int  iScrollHorzPage;      /* Large horizontal scroll amount (pxl) */				
   static  int  iScrollVertBuff;      /* Small vertical scroll amount (pxl) */
   static  int  iScrollVertPage;      /* Large vertical scroll amount (pxl) */				
   static  int  iTitleOffset;         /* Trace offset at top of screen */
   static  int  iVScrollOffset;       /* Vertical scoll bar setting */
   long    lTime;                     /* 1/1/70 time */
   static  long lTitleFHt, lTitleFWd; /* Font height and width */
   POINT   pt;                        /* Screen location for trace name */
   static  PAINTSTRUCT ps;
   static  RECT    rct;
   unsigned long ulScrollLines;  /* Mousewheel variables */
   TRACE_HEADER  WaveBuf;  /* TraceBuf header for compatibility with filters */

   switch ( msg )
   {	
      case WM_CREATE:
      case WM_SETTINGCHANGE:
         SystemParametersInfo( SPI_GETWHEELSCROLLLINES, 0, &ulScrollLines, 0 );
/* ulScrolllines usually equals 3 or 0 (for no scrolling)
   WHEEL_DELTA equals 120, so iDeltaPerLine will be 40 */
         iAccumDelta = 0;
         if ( ulScrollLines )
            iDeltaPerLine = WHEEL_DELTA / ulScrollLines;	
         else
            iDeltaPerLine = 0;	
         break;
	  
      case WM_SIZE:               /* Get size and force a redraw */
         cyScreenA = HIWORD( lParam );       /* (in child windows, this is */
         cxScreenA = LOWORD( lParam );       /*  the client display size)  */
		 
/* Compute vertical scrolling amounts */		 
         iScrollVertBuff = cyScreenA / 16;
         iScrollVertPage = cyScreenA / 2;
         iVScrollOffset = 0;       /* Start with no vertical scrolling */
	 
/* Compute horizontal scrolling amounts */		 
         iScrollHorzBuff = cxScreenA / 10;
         iScrollHorzPage = cxScreenA / 3;
         iHScrollOffset = 0;       /* Start with no horizontal scrolling */
	 
/* Set scroll thumb positions */
         SetScrollRange( hwnd, SB_VERT, 0, 100, FALSE );  /* 100/50 arbitrary */
         SetScrollPos( hwnd, SB_VERT, 0, TRUE );
         SetScrollRange( hwnd, SB_HORZ, 0, 100, FALSE );  /* 100/50 arbitrary */
         SetScrollPos( hwnd, SB_HORZ, 50, TRUE );
		 
         InvalidateRect( hwnd, NULL, TRUE );
         break;
		 
      case WM_DRAW_WAVEFORM:      /* Signalled to show waveforms */
         iVScrollOffset = 0;      /* Start with no vertical scrolling */
         iHScrollOffset = 0;      /* Start with no horizontal scrolling */
         RequestSpecificMutex( &mutsem1 );   /* Sem protect buff writes */
		 
/* First, initialize PBufG with data from StaArray */
         InitPBufWithSta( Nsta, PBufG, StaArray );	  
	 
/* Next, get the expected P-time at each site for the present quake */
         GetPTimes( Nsta, PBufG, &Hypo[iActiveHypo] );
	 
/* Update the PBufG array with data from the automatic P-picks */
         UpdatePPickArray( iNumPAuto, Nsta, PBufL, PBufG );
		 
/* Read the data files and patch appropriate data to Station array */

         for ( i=0; i<Nsta; i++ )
            {
//	      char eMessage[256];	      
// ------------------------------------------------------------         	    
// JMC 12.14.2005 - Start
            memset( StaArray[i].plRawCircBuff, 0, 
             sizeof (long) * StaArray[i].lRawCircSize );
            memset( StaArray[i].plFiltCircBuff, 0, 
             sizeof (long) * StaArray[i].lRawCircSize );
            memset( StaArray[i].pdRawDispData, 0, 
             sizeof (double) * (long) (StaArray[i].dSampRate+0.001) *
             (Gparm.MwpSeconds+20));
	     
/*            if ( StaArray[i].plRawCircBuff != NULL)
	      free (StaArray[i].plRawCircBuff);
            if ( StaArray[i].plFiltCircBuff != NULL )
	      free (StaArray[i].plFiltCircBuff);
            if ( StaArray[i].pdRawDispData != NULL)
	      free (StaArray[i].pdRawDispData);
	      
            StaArray[i].plRawCircBuff =
	       calloc(1, (sizeof (long) * StaArray[i].lRawCircSize) );
            StaArray[i].plFiltCircBuff =
               calloc(1, (sizeof (long) * StaArray[i].lRawCircSize) );
            StaArray[i].pdRawDispData =
               calloc(1, (sizeof (double) * (long) (StaArray[i].dSampRate+0.001) *
             (Gparm.MwpSeconds+20)));

//
// Put out a message for the user - so that they know why we have an issue
// could exit at this point - but it is easier if we let the program error and
// have the debugger kick in
//
            if ( StaArray[i].plRawCircBuff == NULL)
	      {
	       sprintf(eMessage,
	           "Error: Could not calloc %d bytes for: plRawCircBuff\n%s (%d) - %s %s",
		    ( sizeof (long) * StaArray[i].lRawCircSize ),
		   __FILE__, __LINE__, __DATE__, __TIME__ );
   	       MessageBox(NULL, eMessage, "hypo_display", MB_ICONEXCLAMATION);
	      }
	      
            if ( StaArray[i].plFiltCircBuff == NULL) 	    
	      {
	       sprintf(eMessage,
	           "Error: Could not calloc %d bytes for: plFiltCircBuff\n%s (%d) - %s %s",
		   ( sizeof (long) * StaArray[i].lRawCircSize ),
		   __FILE__, __LINE__, __DATE__, __TIME__);
   	       MessageBox(NULL, eMessage, "hypo_display", MB_ICONEXCLAMATION);
	      }
	    
            if ( StaArray[i].pdRawDispData == NULL) 	    
	      {
	       sprintf(eMessage,
	           "Error: Could not calloc %d bytes for: pdRawDispData\n%s (%d) - %s %s",
                   (sizeof (double) * (long) (StaArray[i].dSampRate+0.001) *
                   (Gparm.MwpSeconds+20)),		   
		   __FILE__, __LINE__, __DATE__, __TIME__);
   	       MessageBox(NULL, eMessage, "hypo_display", MB_ICONEXCLAMATION);
	      }
	     
// JMC 12.14.2005 - End
// ------------------------------------------------------------   */
         }                          
         ReadDiskDataForHypo( Gparm.FileLength, Gparm.MinutesInBuff, 
          Gparm.DiskWritePath, Gparm.FileSuffix, Gparm.PreEventTime, Nsta,
          PBufG, StaArray );
		  
/* Short-period bandpass filter the data if specified in station file 
   (This is usually necessary for broadband data, not so for SP data.
   Taper the signal at start.) Also, create a TRACE_BUF structure so
   filter routines may be called and adjust variables so that filtering
   stops at end of data (not at end of file). */
         for ( i=0; i<Nsta; i++ )
         {
            FindDataEnd( &StaArray[i] );
            for ( j=0; j<StaArray[i].lRawCircCtr; j++ )
               StaArray[i].plFiltCircBuff[j] = StaArray[i].plRawCircBuff[j]; 

            WaveBuf.nsamp = StaArray[i].lRawCircCtr;
            WaveBuf.samprate = StaArray[i].dSampRate;
            if ( StaArray[i].iFiltStatus == 1 )
               FilterPacket ( &StaArray[i].plFiltCircBuff[0], &StaArray[i],
                              &WaveBuf, Gparm.LowCutFilter,
                              Gparm.HighCutFilter, 3.*(1./Gparm.LowCutFilter) );

/* Determine background levels */							  
            if ( StaArray[i].dEndTime > 0. )
            {
               GetLDC( (long) (StaArray[i].dSampRate*10.),
                (long) (StaArray[i].dSampRate*20.),
                StaArray[i].plRawCircBuff, &StaArray[i].dAveLDCRaw );
               GetLDC( (long) (StaArray[i].dSampRate*10.),
                (long) (StaArray[i].dSampRate*20.),
                StaArray[i].plFiltCircBuff, &StaArray[i].dAveLDC );
            }
         }	  
	 
/* Order Ps within PPICK array from first to last by expected P-time */	  
         qsort( (void *) PBufG, Nsta, sizeof( PPICK ), SortAllByExpectedPTime );
         WritePickFile( Nsta, PBufG, Gparm.RTPFile );
         ReleaseSpecificMutex( &mutsem1 ); /* Let someone else have sem */		 
         InvalidateRect( hwnd, NULL, TRUE );
         break;
		 
/* Recompute Mwp with different parameters */
      case WM_MWP_RECOMPUTE:		 
         RequestSpecificMutex( &mutsem1 );   /* Protect buf write */
         for ( i=0; i<Nsta; i++ )            /* Loop through all stations */
            if ( PBufG[i].dPTime > 0. )      /* Can we do an Mwp here */
               if ( PBufG[i].dSens > 0. )
               {                  
                  for( k=0; k<Nsta; k++ )    /* Which index in StaArray? */
                     if ( !strcmp( PBufG[i].szStation, StaArray[k].szStation )&&
                          !strcmp( PBufG[i].szChannel, StaArray[k].szChannel )&&
                          !strcmp( PBufG[i].szNetID, StaArray[k].szNetID ) )
                        break;
                  if ( k >= Nsta ) goto EndLoop;
/* If there is enough data, compute Mwp parameters */						
                  time( &lTime );
                  if ( (StaArray[k].dEndTime-PBufG[i].dPTime >= 50. &&
                        StaArray[k].iComputeMwp == 1 && iFromMainWnd == 1) ||
                       (StaArray[k].dEndTime-PBufG[i].dPTime >= 50. &&
                       (lTime - (long) (Hypo[iActiveHypo].dOriginTime+0.5)) <=
                        REDRAW_TOTAL &&
                        StaArray[k].dEndTime-PBufG[i].dPTime < iMwpSeconds &&
                        StaArray[k].iComputeMwp == 1 && iFromMainWnd == 0) )
                  {
                     iFromMainWnd = 0;
                     iTemp = (int) (StaArray[k].dEndTime-PBufG[i].dPTime);
                     if ( iTemp > iMwpSeconds ) iTemp = iMwpSeconds;
                     dTemp = PBufG[i].dMwpIntDisp;
                     AutoMwp( &StaArray[k], &PBufG[i], dMwpSigNoise,
                               iTemp, 1 );
/* Update pick if Mwp has changed */                     
                     if ( dTemp+0.00001 < PBufG[i].dMwpIntDisp || 
                          dTemp-0.00001 > PBufG[i].dMwpIntDisp ) 
                     {
/* Save the QuakeID so that loc_... is forced to put it in the proper buffer */	
                        PBufG[i].iHypoID = Hypo[iActiveHypo].iQuakeID;
                        ReportPick( &PBufG[i], &Gparm, &Ewh, StaArray, Nsta );                     
                     }
                  }
EndLoop:;      }				  
         WritePickFile( Nsta, PBufG, Gparm.RTPFile );
         ReleaseSpecificMutex( &mutsem1 );   /* Release sem */
         InvalidateRect( hwnd, NULL, TRUE );
         break;

/* Remove/Include P from locations or make a pick */
      case WM_LBUTTONDOWN:
/* If timer going, reset so that it doesn't drive user crazy */
   
         if ( iTimerGoing == 1 )	  
         {
            KillTimer( hwndWndProc, ID_TIMER ); /* Stop the redraw timer */
            SetTimer( hwndWndProc, ID_TIMER, REDRAW_INTERVAL, NULL );   
         }
         GetCursorPos( &pt );            /* Get the cursor location */
         ScreenToClient( hwnd, &pt );    /* Put in client window coords */
		 
/* Which screen index is this y value (NOTE: j is the OnScreen index,
   i is the PBufG index, and k is the StaArray index) */
         j = ((pt.y-iTitleOffset-iVScrollOffset+(cyScreenA-iTitleOffset)/
              (Gparm.NumTracePerScreen*2)) * Gparm.NumTracePerScreen) /
              (cyScreenA-iTitleOffset);
         if ( j < 0 || j >= iNumOnScreen ) break;
         for( i=0; i<Nsta; i++ )         /* Which index in PBufG array? */
            if ( !strcmp( OnScreen[j].szStation, PBufG[i].szStation ) &&
                 !strcmp( OnScreen[j].szChannel, PBufG[i].szChannel ) &&
                 !strcmp( OnScreen[j].szNetID, PBufG[i].szNetID ) ) break;
         if ( i >= Nsta ) break;
         for( k=0; k<Nsta; k++ )         /* Which index in StaArray array? */
            if ( !strcmp( OnScreen[j].szStation, StaArray[k].szStation ) &&
                 !strcmp( OnScreen[j].szChannel, StaArray[k].szChannel ) &&
                 !strcmp( OnScreen[j].szNetID, StaArray[k].szNetID ) ) break;
         if ( k >= Nsta ) break;
			
/* If click was within station name part of screen, eliminate if from solution;
   otherwise it is a new P-pick */			
         RequestSpecificMutex( &mutsem1 );   /* Protect buf write */
         if ( pt.x > iChanIDOffset )     /* P-pick made on this station */
         {
            PBufG[i].dPTime = (PBufG[i].dExpectedPTime-Gparm.PreEventTime) +
             Gparm.TimePerScreen*((double) (pt.x-iChanIDOffset-iHScrollOffset)/
             (double) (cxScreenA-iChanIDOffset)) - StaArray[k].dTimeCorrection;
            PBufG[i].iUseMe = 2;
            strcpy( PBufG[i].szPhase, "eP" );
/* Save the QuakeID so that loc_... is forced to put it in the proper buffer */	
            PBufG[i].iHypoID = Hypo[iActiveHypo].iQuakeID;
            if ( PBufG[i].lPickIndex == 0 )
            {
               PBufG[i].lPickIndex = lPickCounter;
               lPickCounter++;
               if ( lPickCounter == 30000 ) lPickCounter = 20000;
            }
			
/* Get magnitudes for this pick */
            if ( PBufG[i].dSens > 0. )					 
            {
/* Compute Mb/Ml parameters */					 
               GetMag( &StaArray[k], &PBufG[i], &Gparm);
			 
/* If there is enough data, compute Mwp parameters */						
               if ( StaArray[k].dEndTime-PBufG[i].dPTime >= 
                   50. && StaArray[k].iComputeMwp == 1 ) 
               {
                  iTemp = (int) (StaArray[k].dEndTime-PBufG[i].dPTime+0.5);
                  if ( iTemp > iMwpSeconds ) iTemp = iMwpSeconds;
                  AutoMwp( &StaArray[k], &PBufG[i], dMwpSigNoise,
                            iTemp, 1 );
               }
            }
         }
         else                         /* X out from solution */
         {
/* Set display menu item check */
            if ( PBufG[i].dPTime > 0. )
            {
               PBufG[i].iHypoID = Hypo[iActiveHypo].iQuakeID;
               if      ( PBufG[i].iUseMe > 0 ) PBufG[i].iUseMe = -1;
               else                            PBufG[i].iUseMe = 2;
            }
         }
   
/* Dump P/mag data to file used by LOCATE */
         WritePickFile( Nsta, PBufG, Gparm.RTPFile );
         ReleaseSpecificMutex( &mutsem1 );   /* Release sem */
         ReportPick( &PBufG[i], &Gparm, &Ewh, StaArray, Nsta );                     
         InvalidateRect( hwnd, NULL, TRUE );
         break;
	 
/* Respond to mouse roller */                          
      case WM_MOUSEWHEEL:
         if ( iDeltaPerLine == 0 ) break;
         iAccumDelta += (short) HIWORD ( wParam );
  	 while ( iAccumDelta >= iDeltaPerLine )
	 {
            SendMessage( hwnd, WM_VSCROLL, SB_LINEUP, 0 );
	    iAccumDelta -= iDeltaPerLine;
	 }
	 while ( iAccumDelta <= -iDeltaPerLine )
	 {
            SendMessage( hwnd, WM_VSCROLL, SB_LINEDOWN, 0 );
	    iAccumDelta += iDeltaPerLine;
	 }
	 break;

      case WM_PAINT:
/* Compute font size */
         lTitleFHt = cyScreenA / 25;
         lTitleFWd = cxScreenA / 140;
		 
/* Compute offsets from top and left sides of screen */		 
         iTitleOffset = cyScreenA / 30;
         iChanIDOffset = cxScreenA / 32;
		 
         hdc = BeginPaint( hwnd, &ps );
         GetClientRect( hwnd, &rct );
         FillRect( hdc, &rct, (HBRUSH) GetStockObject( WHITE_BRUSH ) );		 
         DisplayChannelIDChild( hdc, StaArray, PBufG, OnScreen, Nsta, lTitleFHt,
          lTitleFWd, cxScreenA, cyScreenA, Gparm.NumTracePerScreen,
          iVScrollOffset, iTitleOffset, &iNumOnScreen );
         DisplayTracesChild( hdc, StaArray, PBufG, Gparm.NumTracePerScreen,
          Nsta, cxScreenA, cyScreenA, iVScrollOffset, iTitleOffset,
          iChanIDOffset, Gparm.TimePerScreen, Gparm.PreEventTime, 
          iHScrollOffset, &iNumShown );
         DisplayMwpH( hdc, Gparm.NumTracePerScreen, Nsta, cxScreenA, cyScreenA,
          iVScrollOffset, iTitleOffset, iChanIDOffset, Gparm.TimePerScreen,
          Gparm.PreEventTime, StaArray, PBufG, iHScrollOffset );
         DisplayPPicksH( hdc, Gparm.NumTracePerScreen, Nsta, cxScreenA,
          cyScreenA, iVScrollOffset, iTitleOffset, iChanIDOffset,
          Gparm.TimePerScreen, Gparm.PreEventTime, lTitleFHt, StaArray, PBufG,
          iHScrollOffset );
         DisplayKOPsH( hdc, Nsta, cxScreenA, cyScreenA, iVScrollOffset,
          iTitleOffset, iChanIDOffset, lTitleFHt, lTitleFWd,
          Gparm.NumTracePerScreen, StaArray, PBufG );
         SetScrollPos( hwnd, SB_VERT, (int) (((double) iVScrollOffset*-1.) /
                       ((cyScreenA-iTitleOffset) /
                       (double) Gparm.NumTracePerScreen *
                       (double) iNumShown) * 100.), TRUE );
         EndPaint( hwnd, &ps );
         if ( iTimerRedoMwp == 1 )
         {
            iTimerRedoMwp = 0;
            PostMessage( hwnd, WM_MWP_RECOMPUTE, 0, 0 );   
         }
         break;                                                          
	
      case WM_VSCROLL:   /* Vertical scroll message */	
         if ( LOWORD (wParam) == SB_LINEUP )
            iVScrollOffset += iScrollVertBuff;
         else if ( LOWORD (wParam) == SB_PAGEUP )
            iVScrollOffset += iScrollVertPage;
         else if ( LOWORD (wParam) == SB_LINEDOWN )
            iVScrollOffset -= iScrollVertBuff;
         else if ( LOWORD (wParam) == SB_PAGEDOWN )
            iVScrollOffset -= iScrollVertPage;
         InvalidateRect( hwnd, NULL, TRUE );
         break;
	
      case WM_HSCROLL:   /* Horizontal scroll message */	
         if ( LOWORD (wParam) == SB_LINERIGHT )
            iHScrollOffset -= iScrollHorzBuff;
         else if ( LOWORD (wParam) == SB_PAGERIGHT )
            iHScrollOffset -= iScrollHorzPage;
         else if ( LOWORD (wParam) == SB_LINELEFT )
            iHScrollOffset += iScrollHorzBuff;
         else if ( LOWORD (wParam) == SB_PAGELEFT )
            iHScrollOffset += iScrollHorzPage;
         InvalidateRect( hwnd, NULL, TRUE );
         break;

    default:
        return( DefWindowProc( hwnd, msg, wParam, lParam ) );
    }

return 0;
}

      /***********************************************************
       *                  ChildBWndProc()                        *
       *                                                         *
       *  This dialog procedure processes messages from the child*
       *  windows screen and messages passed to it from          *
       *  elsewhere.  All display is performed in PAINT. The     *
       *  velocity, displacement, and integrated displacement    *
       *  trace for each signal with an Mwp computed is shown    *
       *                                                         *
       ***********************************************************/
	   
long WINAPI ChildBWndProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   static  int     cxWidthAx, cyHeightAx;   /* Display area in pixels */
   static  double  dMaxV[MAX_STATIONS], dMaxD[MAX_STATIONS], 
                   dMaxID[MAX_STATIONS];    /* Max value of each trace */
   double  dTotalDisp;                      /* Running total of integrations */
   static  HDC     hdc;                     /* Handle to Device context */
   static  HPEN    hRPen, hBPen;            /* Font/color handles */
   static  long    i, j, k, l, lTemp, jCnt; /* Counters */
   static  int     iNumToDisplay;           /* Number of stations to show */
   static  long    lNum;                    /* Number of samples to evaluate */
   static  long    lPIndex;                 /* Buffer index of P-time */
   long    lTemp2;                          /* Temporary counters */
   static  PAINTSTRUCT ps;
   static  POINT   ptArray[MAXMWPARRAY], pt;/* Display point structures */
   static  RECT    rct;
   static  char    szTemp2[12], szTemp3[6];

   switch ( msg )
   {
      case WM_CREATE:
         SetWindowPos( hwnd, HWND_TOPMOST, 0, 0, 0, 0,
                       SWP_NOMOVE | SWP_NOSIZE );
         SetFocus( hwnd );
         break;	  
   
      case WM_SIZE:               /* Get size and force a redraw */
         cyHeightAx = HIWORD( lParam );       /* (in child windows, this is */
         cxWidthAx = LOWORD( lParam );        /*  the client display size)  */
         InvalidateRect( hwnd, NULL, TRUE );
         break;
		 
      case WM_PAINT:   /* First, loop through each array to determine the max */
         hdc = BeginPaint( hwnd, &ps );
         hBPen = CreatePen( PS_SOLID, 1, RGB( 0,0,0 ) );    /* black */
         hRPen = CreatePen( PS_SOLID, 1, RGB( 255,0,0 ) );  /* red */
         GetClientRect( hwnd, &rct );
         FillRect( hdc, &rct, (HBRUSH) GetStockObject( WHITE_BRUSH ) );
		 
/* How many stations have Mwp */		 
         iNumToDisplay = 0;
         for ( j=0; j<Nsta; j++ )
            if ( PBufG[j].dMwpIntDisp > 0. ) iNumToDisplay++;
         jCnt = 0;
         for ( j=0; j<Nsta; j++ )
            if ( PBufG[j].dMwpIntDisp > 0. )
            {
/* Find index in StaArray */			
               for ( l=0; l<Nsta; l++ )
                  if ( !strcmp( StaArray[l].szStation, PBufG[j].szStation ) &&
                       !strcmp( StaArray[l].szChannel, PBufG[j].szChannel ) &&
                       !strcmp( StaArray[l].szNetID, PBufG[j].szNetID ) ) break;
/* What is buffer index of P-time? */   
               lPIndex = StaArray[l].lSampIndexF - (long) ((StaArray[l].dEndTime
                         -PBufG[j].dPTime) * StaArray[l].dSampRate) - 1;
               while ( lPIndex <  0 ) lPIndex += StaArray[l].lRawCircSize;
               while ( lPIndex >= StaArray[l].lRawCircSize )
                  lPIndex -= StaArray[l].lRawCircSize; 
   
/* How many points to display? */   
               lNum = (long) (StaArray[l].dSampRate*
                      ((double) PBufG[j].dMwpTime));
   
/* See if this integration was done here */   
               for ( i=0; i<10; i++ )
                  if ( fabs( StaArray[l].pdRawDispData[i] ) > 1.e-10 )
                     goto PastInt;   
   
/* Integrate velocity signal to get displacement. Convert counts to m/s before
   conversion. */
               dTotalDisp = 0.;
               for ( i=0; i<lNum-1; i++ )
               {
                  lTemp = i + lPIndex;
                  if ( lTemp >= StaArray[l].lRawCircSize )
                     lTemp -= StaArray[l].lRawCircSize;
                  lTemp2 = lTemp + 1;
                  if ( lTemp2 >= StaArray[l].lRawCircSize )
	                 lTemp2 -= StaArray[l].lRawCircSize;
                  StaArray[l].pdRawDispData[i] = dTotalDisp + 1./
                   StaArray[l].dSampRate*0.5*((double)
                   (StaArray[l].plRawCircBuff[lTemp]-StaArray[l].dAveLDCRaw)/
                   StaArray[l].dSens + (double)
                   (StaArray[l].plRawCircBuff[lTemp2]-
                   StaArray[l].dAveLDCRaw)/StaArray[l].dSens);
                  dTotalDisp = StaArray[l].pdRawDispData[i];
               }
   
/* Integrate displacement to get integrated displacement signal */
               dTotalDisp = 0.;
               for ( i=0; i<lNum-1; i++ )
               {
                  StaArray[l].pdRawIDispData[i] = dTotalDisp + 1./
                   StaArray[l].dSampRate*0.5*(StaArray[l].pdRawDispData[i]+
                   StaArray[l].pdRawDispData[i+1]);
                  dTotalDisp = StaArray[l].pdRawIDispData[i];
               } 

PastInt:
/* Find max value of each three traces */			   
               dMaxV[j] = 0.;
               dMaxD[j] = 0.;
               dMaxID[j] = 0.;
               for ( i=1; i<lNum-1; i++ )
               {
                  lTemp = i + lPIndex;
                  if ( lTemp >= StaArray[l].lRawCircSize )
                     lTemp -= StaArray[l].lRawCircSize;
                  if ( fabs( (double) (StaArray[l].plRawCircBuff[lTemp]-
                       StaArray[l].dAveLDCRaw)/StaArray[l].dSens ) > dMaxV[j] )
                     dMaxV[j] = fabs( (double) (StaArray[l].plRawCircBuff[lTemp]
                      -StaArray[l].dAveLDCRaw)/StaArray[l].dSens );
                  if ( fabs( StaArray[l].pdRawDispData[i] ) > dMaxD[j] ) 
                     dMaxD[j] = fabs( StaArray[l].pdRawDispData[i] );
                  if ( fabs( StaArray[l].pdRawIDispData[i] ) > dMaxID[j] )
                     dMaxID[j] = fabs ( StaArray[l].pdRawIDispData[i] );
               }
			   
/* Prevent divide by zero */
               if ( dMaxV[j] == 0. )  dMaxV[j]  = 1.;
               if ( dMaxD[j] == 0. )  dMaxD[j]  = 1.;
               if ( dMaxID[j] == 0. ) dMaxID[j] = 1.;
			   
/* Erase the screen and initialize values */
               for ( k=0; k<3; k++ )   /* 3 displays for each station */
               {
/* Start the displays at Ptime */
                  for ( i=0; i<lNum-1; i++ )
                  {       /* Fill .x array first (same for each of 3) */
                     ptArray[i].x = (long) (((double) i / (double) lNum) *
                      (cxWidthAx/(iNumToDisplay+1)) + jCnt*cxWidthAx/
                       iNumToDisplay + 5.);
                     lTemp = i + lPIndex;
                     if ( lTemp >= StaArray[l].lRawCircSize )
                        lTemp -= StaArray[l].lRawCircSize;
                     if ( k == 0 )          /* Velocity */
                        ptArray[i].y = cyHeightAx - (k*cyHeightAx/3 +
                         cyHeightAx/6 + (long) ((double)
                         ((StaArray[l].plRawCircBuff[lTemp]-
                         StaArray[l].dAveLDCRaw)/StaArray[l].dSens) / dMaxV[j] *
                         (cyHeightAx/6)));
                     if ( k == 1 )          /* Displacement */
                        ptArray[i].y = cyHeightAx - (k*cyHeightAx/3 +
                         cyHeightAx/6 + (long) (StaArray[l].pdRawDispData[i] / 
                         dMaxD[j] * (cyHeightAx/6)));
                     if ( k == 2 )          /* Integrated displacement */
                        ptArray[i].y = cyHeightAx - (k*cyHeightAx/3 +
                         cyHeightAx/6 + (long) (StaArray[l].pdRawIDispData[i] / 
                         dMaxID[j] * (cyHeightAx/6)));
                  }
/* Draw trace */
                  SelectObject( hdc, hBPen );
                  MoveToEx( hdc, ptArray[0].x, ptArray[0].y, NULL );
                  Polyline( hdc, ptArray, lNum-1 );
/* Put up zero lines */
                  pt.x = jCnt*cxWidthAx/iNumToDisplay + 5;
                  pt.y = cyHeightAx - (k*cyHeightAx/3 + cyHeightAx/6);
                  SelectObject( hdc, hRPen );
                  MoveToEx( hdc, pt.x, pt.y, NULL );
                  pt.x = cxWidthAx / (iNumToDisplay+1) +
                         jCnt*cxWidthAx/iNumToDisplay + 5;
                  LineTo( hdc, pt.x, pt.y );
/* Put up labels */
                  if ( jCnt == 0 )
                  {
                     pt.x = jCnt*cxWidthAx/iNumToDisplay + 5;
                     pt.y = 19*cyHeightAx/20 - (k*cyHeightAx/3);
                     SetTextColor( hdc, RGB( 0,0,255 ) );  /* blue */
                     if ( k == 0 ) strcpy( szTemp2, "Vel." );
                     if ( k == 1 ) strcpy( szTemp2, "Disp." );
                     if ( k == 2 ) strcpy( szTemp2, "Int. Disp." );
                     TextOut( hdc, pt.x, pt.y, szTemp2, strlen( szTemp2 ) );
                  }
/* Display station name */
                  pt.x = jCnt*cxWidthAx/iNumToDisplay + 5;
                  pt.y = 1;
                  SetTextColor( hdc, RGB( 0,0,255 ) );     /* blue */
                  TextOut( hdc, pt.x, pt.y, StaArray[l].szStation,
                           strlen( StaArray[l].szStation ) );
/* Display integration time at bottom */
                  pt.x = jCnt*cxWidthAx/iNumToDisplay + 5 +
                         cxWidthAx/iNumToDisplay/2;
                  pt.y = 9*cyHeightAx/10;
                  lTemp = (long) (PBufG[j].dMwpTime + 0.5);
                  itoaX( lTemp, szTemp3 );
                  SetTextColor( hdc, RGB( 0,0,255 ) );     /* blue */
                  TextOut( hdc, pt.x, pt.y, szTemp3, strlen( szTemp3 ) );
               }
               jCnt++;
            }
						
/* Write Title bar message */
         DeleteObject( hBPen );
         DeleteObject( hRPen );
         SetWindowText( hwnd, "Integrated Waveform Display" );
         EndPaint( hwnd, &ps );
         break;                                                          

      case WM_DESTROY:		 
/* Remove Mwp window and resize waveform window */		 
         if ( iChildA )  /* If this child is already running, stop it */
         {
            DestroyWindow( hwndChildA );
            iChildA = 0;
         }
         hwndChildA = CreateWindow( szChildAName, /* Child Name */
          szChildAName,           /* Window Class name */
          WS_BORDER | WS_CHILDWINDOW | WS_HSCROLL | WS_VSCROLL |
          WS_CLIPSIBLINGS,        /* Style */
          1*cxScreen/100,        /* top left x starting location */
          38*cyScreen/100,        /* top left y starting location */
          98*cxScreen/100,        /* initial screen width in pixels */
          61*cyScreen/100,        /* initial screen height in pixels */
          hwndWndProc,            /* Parent window */
          (HMENU) ID_CHILD_WAVEFORM,/* Child window ID */
                                  /* Let Windows supply the Instance */
          (HANDLE) GetWindowLong( hwndWndProc, GWL_HINSTANCE ),
          NULL );                 /* No extra data to pass in */
         if ( hwndChildA == NULL )/* window not created */
         {
            logit( "t", "waveform window not created - 3\n" );
            break;
         }
         ShowWindow( hwndChildA, SW_SHOW );   /* Show the window */
         UpdateWindow( hwndChildA );    /* Force an initial PAINT call */
         SetActiveWindow( hwndChildA ); /* Give it the input focus */
         iChildA = 1;   
         break;

    default:
        return( DefWindowProc( hwnd, msg, wParam, lParam ) );
    }
return 0;
}

     /**************************************************************
      *                CreateChildren()                            *
      *                                                            *
      * This function creates the child windows which list P-times *
      * and hypocenter parameters.                                 *
      *                                                            *
      * Arguments:                                                 *
      *  hwnd        Handle of parent window (WndProc)             *
      *  cxScreenCC  Horizontal pixel width of window              *
      *  cyScreenCC  Vertical pixel height of window               *
      *                                                            *
      * Return - 0 if OK, -1 if problem                            *
      **************************************************************/
      
int CreateChildren( HWND hwnd, int cxScreenCC, int cyScreenCC )
{
/* Create hypocenter list LISTBOX (leave room for header) */
   hwndHypo = CreateWindow ("listbox",/* Create a listbox child */
              NULL,                   /* No initial text */
              WS_CHILD | WS_VISIBLE | LBS_NOTIFY | WS_VSCROLL | WS_BORDER,
                                      /* Window style */
              1*cxScreenCC/100,       /* top left x starting location */
              3*cyScreenCC/100,       /* top left y starting location */
              98*cxScreenCC/100,      /* Width */
              32*cyScreenCC/100,      /* Height */
              hwnd,                   /* Client window is the parent */
              (HMENU) ID_CHILD_HYPO,  /* Child window ID */
              hInstMain,              /* Main Process id */
              NULL);                  /* No extra data to pass in */
   if ( hwndHypo == NULL )            /* Window not created */
   {
      logit( "t", "Create hypocenter window failed\n" );
      return -1;
   }		
return 0;
}

 /********************************************************************
  *                 DisplayChannelIDChild()                          *
  *                                                                  *
  * This function displays the station name next to its trace in the *
  * child window.                                                    *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  Sta               Array of all station data structures          *
  *  PBuf              Ppick array of structures                     *
  *  Screen            ONSCREEN array of station ids                 *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  lTitleFHt         Title font height                             *
  *  lTitleFWd         Title font width                              *
  *  cxScreenT         Horizontal pixel width of window              *
  *  cyScreenT         Vertical pixel height of window               *
  *  iNumTracePerScreen Number of traces to put on screen            *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  piNumOnScreen     Number of traces actually written to screen   *
  *                                                                  *
  ********************************************************************/
  
void DisplayChannelIDChild( HDC hdc, STATION Sta[], PPICK PBuf [],
      ONSCREEN Screen[], int iNumStas, long lTitleFHt, long lTitleFWd,
      int cxScreenT, int cyScreenT, int iNumTracePerScreen, int iVScrollOffset,
      int iTitleOffset, int *piNumOnScreen )
{
   HFONT   hOFont, hNFont;       /* Old and new font handles */
   int     i, j;
   int     iCnt;                 /* Channel names counter */
   long    lOffset;              /* Offset to center of trace from top */
   POINT   pt;                   /* Screen location for trace name */

/* Create font */
   hNFont = CreateFont( 3*lTitleFHt/4, 3*lTitleFWd/4, 
             0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN,
             "Times New Roman" );
   hOFont = SelectObject( hdc, hNFont );
   SetTextColor( hdc, RGB( 255, 0, 0 ) );   /* Color trace names red */
   *piNumOnScreen = 0;

/* Loop through all stations and see which should be shown now */   
   pt.x = cxScreenT / 500;
   iCnt = 0;
   for ( i=0; i<iNumStas; i++ )
      for (j=0; j<iNumStas; j++ )
         if ( !strcmp( Sta[j].szStation, PBuf[i].szStation ) &&
              !strcmp( Sta[j].szChannel, PBuf[i].szChannel ) &&
              !strcmp( Sta[j].szNetID, PBuf[i].szNetID ) ) 		 
         {
            if ( Sta[j].dEndTime > 0. && strcmp( Sta[j].szChannel, "BLZ" ) && 
                 strcmp( Sta[j].szChannel, "SLZ" ) &&
                 strcmp( Sta[j].szChannel, "SMZ" ) )
            {
/* Here is where Screen array is updated */
               strcpy( Screen[iCnt].szStation, Sta[j].szStation );
               strcpy( Screen[iCnt].szChannel, Sta[j].szChannel );
               strcpy( Screen[iCnt].szNetID, Sta[j].szNetID );
			   
               lOffset = iCnt*(cyScreenT-iTitleOffset)/iNumTracePerScreen +
                         iVScrollOffset + iTitleOffset;
               pt.y = lOffset - 3*lTitleFHt/8;
               TextOut( hdc, pt.x, pt.y, PBuf[i].szStation,
                        strlen( PBuf[i].szStation ) );
               iCnt++;
            }
            break;
         }
   *piNumOnScreen = iCnt;
   DeleteObject( SelectObject( hdc, hOFont ) );  /* Reset font */
}

 /********************************************************************
  *                     DisplayKOPsH()                               *
  *                                                                  *
  * This function displays an X when traces have been removed from   *
  * the location.                                                    *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  cxScreenT         Horizontal pixel width of window              *
  *  cyScreenT         Vertical pixel height of window               *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *  lTitleFHt         Title font height                             *
  *  lTitleFWd         Title font width                              *
  *  iNumTracePerScreen Number of traces to put on screen            *
  *  Sta               Station array of structures                   *
  *  pPBuf             P-pick buffer to copy PStruct into            *
  *                                                                  *
  ********************************************************************/
  
void DisplayKOPsH( HDC hdc, int iNumStas, int cxScreenT, int cyScreenT,
                   int iVScrollOffset, int iTitleOffset, int iChanIDOffset,
                   long lTitleFHt, long lTitleFWd,
                   int iNumTracePerScreen, STATION Sta[], PPICK pPBuf[] )
{
   HFONT   hOFont, hNFont;       /* Old and new font handles */
   int     i, j, iCnt;           /* Counters */
   POINT   pt;                   /* Screen location for X */

/* Create font */
   hNFont = CreateFont( lTitleFHt, 3*lTitleFWd/2, 
             0, 0, 0, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN,
             "Times New Roman" );
   hOFont = SelectObject( hdc, hNFont );
   SetTextColor( hdc, RGB( 0, 0, 128 ) );         /* X's in Blue */

/* Show all X's */
   pt.x = 3*iChanIDOffset/4;
   iCnt = 0;
   for ( i=0; i<iNumStas; i++ )                 /* PBuf Loop */
      for (j=0; j<iNumStas; j++ )               /* Sta Loop */
         if ( !strcmp( Sta[j].szStation, pPBuf[i].szStation ) &&
              !strcmp( Sta[j].szChannel, pPBuf[i].szChannel ) &&
              !strcmp( Sta[j].szNetID, pPBuf[i].szNetID ) ) 		 
         {
            if ( Sta[j].dEndTime > 0. && strcmp( Sta[j].szChannel, "BLZ" ) && 
                 strcmp( Sta[j].szChannel, "SLZ" ) &&
                 strcmp( Sta[j].szChannel, "SMZ" ) )
            {
               if ( pPBuf[i].iUseMe < 1 )
               {
                  pt.y = iCnt*(cyScreenT-iTitleOffset)/iNumTracePerScreen +
                         iVScrollOffset + iTitleOffset - lTitleFHt*9/16;
                  TextOut( hdc, pt.x, pt.y, "X", 1 );
               }
               iCnt++;
            }
            break;
         }
      
   DeleteObject( SelectObject( hdc, hOFont ) );  /* Reset font */
}

     /**************************************************************
      *                 DisplayLittoral()                          *
      *                                                            *
      * This function outputs the littoral location and preferred  *
      * magnitude for the Active Hypocenter.                       *
      *                                                            *
      * January, 2006: Moved text to where P-time box was.         *
      *                                                            *
      *                                                            *
      * Arguments:                                                 *
      *  hdc               Device context to use                   *
      *  pHypo             Hypocenter data                         *
      *  lTitleFHt         Title font height                       *
      *  lTitleFWd         Title font width                        *
      *  cxScreenT         Horizontal pixel width of window        *
      *  cyScreenT         Vertical pixel height of window         *
      *  pcity             Array of reference cities               *
      *  pcityEC           Array of eastern reference cities       *
      *                                                            *
      **************************************************************/
      
void DisplayLittoral( HDC hdc, HYPO *pHypo, long lTitleFHt, long lTitleFWd,
                      int cxScreenT, int cyScreenT, CITY *pcity, CITY *pcityEC )
{
   CITYDIS CityDis;             /* Distance, direction from nearest cities */
   HFONT   hOFont, hNFont;      /* Font handles */
   int     iFERegion;           /* Flinn-Engdahl Region number */
   int     iRegion;             /* Procedural region */
   int     iTemp;
   LATLON  ll;                  /* Geographic epicenter coordinates */
   char    *psz;                /* Epicentral region */
   POINT   pt;
   char    szBuffer[64];        /* Line to output on screen */

   if ( pHypo->iGoodSoln == 0 ) return;

/* Create new font */
   hNFont = CreateFont( lTitleFHt, lTitleFWd, 
             0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
             DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN,
             "Times New Roman" );
					   
/* Select font and color */
   hOFont = (HFONT) SelectObject( hdc, hNFont );    /* Select the font */
   SetTextColor( hdc, RGB( 0, 0, 0 ) );             /* Black text */
   
/* Compute distance to nearest cities */   
   GeoGraphic( &ll, (LATLON *) pHypo );
   if ( ll.dLon < 0 ) ll.dLon += 360.;
   iRegion = GetRegion( ll.dLat, ll.dLon );
   if ( iRegion >= 10 )                  /* WC&ATWC Eastern AOR */
      NearestCitiesEC( (LATLON *) pHypo, pcityEC, &CityDis ); 
   else
      NearestCities( (LATLON *) pHypo, pcity, &CityDis );

   if ( CityDis.iDis[1] < 200 || CityDis.iDis[0] < 200 )   
/* Then, show nearest cities */
   {
      iTemp = 0;
      if ( CityDis.iDis[1] < CityDis.iDis[0] ) iTemp = 1;
      sprintf( szBuffer, "%3d miles %2s of %s",
       CityDis.iDis[iTemp], CityDis.pszDir[iTemp], CityDis.pszLoc[iTemp] );
      pt.x = 25*cxScreenT/100;
      pt.y = 34*cyScreenT/100;
      TextOut( hdc, pt.x, pt.y, szBuffer, strlen( szBuffer ) );
   }
   else             /* Otherwise, use General Area (Flinn-Engdahl) */
   {
/* Convert epicenter coords from geocentric to geographic */
      GeoGraphic( &ll, (LATLON *) pHypo );
      if ( ll.dLon > 180.0 && ll.dLon < 360.0 ) ll.dLon -= 360.0;
      psz = namnum( ll.dLat, ll.dLon, &iFERegion );
      strncpy( psz, psz, strlen( psz )-1 );
      psz[strlen (psz)-1] = '\0';                      
      sprintf( szBuffer, "%s", psz );
      pt.x = 25*cxScreenT/100;
      pt.y = 34*cyScreenT/100;
      TextOut( hdc, pt.x, pt.y, szBuffer, strlen( szBuffer ) );
   }
   
/* Next output preferred magnitude */
   sprintf( szBuffer, "M%s = %3.1lf", pHypo->szPMagType, pHypo->dPreferredMag );
   pt.x = 10*cxScreenT/100;
   pt.y = 34*cyScreenT/100;
   TextOut( hdc, pt.x, pt.y, szBuffer, strlen( szBuffer ) );
   
   DeleteObject (SelectObject (hdc, hOFont));       /* Reset font */
}

 /********************************************************************
  *                     DisplayMwpH()                                *
  *                                                                  *
  * This function displays a line over the trace where Mwp           *
  * computation took place.                                          * 
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  iNumTrace         Number of traces to show on screen            *
  *  iNumStas          Number of stations in Sta and Trace arrays    *
  *  cxScreenT         Horizontal pixel width of window              *
  *  cyScreenT         Vertical pixel height of window               *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *  dScreenTime       # seconds to show on screen                   *
  *  dPreEventTime     Trace time to display prior to expected P     *
  *  Sta               Station array of structures                   *
  *  pPBuf             P-pick buffer to copy PStruct into            *
  *  iHScrollOffset    Horizontal scroll setting                     *
  *                                                                  *
  ********************************************************************/
  
void DisplayMwpH( HDC hdc, int iNumTrace, int iNumStas,
                  int cxScreenT, int cyScreenT, int iVScrollOffset,
                  int iTitleOffset, int iChanIDOffset, double dScreenTime,
                  double dPreEventTime, STATION Sta[], PPICK pPBuf[],
                  int iHScrollOffset ) 
{
   HPEN    hOPen, hYPen;         /* Pen handles */
   int     i, j, iCnt;           /* Counters */
   long    lOffsetY;             /* Dist to skip on top of screen */
   POINT   pt[2];                /* Line start and end */

/* Create pens */
   hYPen = CreatePen( PS_SOLID, 2, RGB( 100, 100, 0 ) );/*Create yellow lines */
   hOPen = SelectObject (hdc, hYPen);
   iCnt = 0; 
   
/* Show Yellow line over section of trace integrated for Mwp */   
   iCnt = 0;
   for ( i=0; i<iNumStas; i++ )                 /* PBuf Loop */
      for (j=0; j<iNumStas; j++ )               /* Sta Loop */
         if ( !strcmp( Sta[j].szStation, pPBuf[i].szStation ) &&
              !strcmp( Sta[j].szChannel, pPBuf[i].szChannel ) &&
              !strcmp( Sta[j].szNetID, pPBuf[i].szNetID ) ) 		 
         {
            if ( Sta[j].dEndTime > 0. && strcmp( Sta[j].szChannel, "BLZ" ) && 
                 strcmp( Sta[j].szChannel, "SLZ" ) &&
                 strcmp( Sta[j].szChannel, "SMZ" ) )
            {
               if ( pPBuf[i].dMwpTime > 0. && pPBuf[i].dMwpIntDisp > 0. )
               {
                  pt[0].x = iChanIDOffset + iHScrollOffset +
                            (long) (((dPreEventTime+
                            (pPBuf[i].dPTime-pPBuf[i].dExpectedPTime))
                            * (double) (cxScreenT-iChanIDOffset)) / dScreenTime);
                  pt[1].x = pt[0].x + (long) ((double) (cxScreenT-iChanIDOffset)*
                           (pPBuf[i].dMwpTime/dScreenTime));
                  lOffsetY = iCnt*(cyScreenT-iTitleOffset)/iNumTrace +
                             iVScrollOffset + iTitleOffset;
                  pt[0].y = lOffsetY;
                  pt[1].y = lOffsetY;
                  MoveToEx ( hdc, pt[0].x, pt[0].y, NULL );
                  LineTo ( hdc, pt[1].x, pt[1].y );
               }            
               iCnt++;
            }
            break;
         }
   DeleteObject( SelectObject( hdc, hOPen ) );   /* Reset Pen color */
}

 /********************************************************************
  *                     DisplayPPicksH()                             *
  *                                                                  *
  * This function displays all P arrivals in the PBuf array.         *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  iNumTrace         Number of traces to show on screen            *
  *  iNumStas          Number of stations in Sta and pPBuf arrays    *
  *  cxScreenT         Horizontal pixel width of window              *
  *  cyScreenT         Vertical pixel height of window               *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *  dScreenTime       # seconds to show on screen                   *
  *  dPreEventTime     Trace time to display prior to expected P     *
  *  lTitleFHt         Title font height                             *
  *  Sta               Station array of structures                   *
  *  pPBuf             P-pick buffer to copy PStruct into            *
  *  iHScrollOffset    Horizontal scroll setting                     *
  *                                                                  *
  ********************************************************************/
  
void DisplayPPicksH( HDC hdc, int iNumTrace, int iNumStas, int cxScreenT,
                     int cyScreenT, int iVScrollOffset, int iTitleOffset,
                     int iChanIDOffset, double dScreenTime, double dPreEventTime,
                     long lTitleFHt, STATION Sta[], PPICK pPBuf[],
                     int iHScrollOffset ) 
{
   HPEN    hRPen, hOPen;         /* Pen handles */
   int     i, j, iCnt;           /* Counters */
   long    lOffsetY;             /* Dist to skip on top of screen */
   POINT   pt[2];

/* Create font and pens */
   hRPen = CreatePen( PS_SOLID, 2, RGB( 255, 0, 127 ) );   /* Lines Red */
   hOPen = SelectObject( hdc, hRPen );

/* Show picks if on screen */   
   iCnt = 0;
   for ( i=0; i<iNumStas; i++ )                 /* PBuf Loop */
      for (j=0; j<iNumStas; j++ )               /* Sta Loop */
         if ( !strcmp( Sta[j].szStation, pPBuf[i].szStation ) &&
              !strcmp( Sta[j].szChannel, pPBuf[i].szChannel ) &&
              !strcmp( Sta[j].szNetID, pPBuf[i].szNetID ) ) 		 
         {
            if ( Sta[j].dEndTime > 0. && strcmp( Sta[j].szChannel, "BLZ" ) && 
                 strcmp( Sta[j].szChannel, "SLZ" ) &&
                 strcmp( Sta[j].szChannel, "SMZ" ) )
            {
               if ( pPBuf[i].dPTime > 0. )
               {
                  pt[0].x = iChanIDOffset + iHScrollOffset +
                            (long) (((dPreEventTime+
                            (pPBuf[i].dPTime-pPBuf[i].dExpectedPTime))
                            * (double) (cxScreenT-iChanIDOffset)) / dScreenTime);
                  pt[1].x = pt[0].x;
                  lOffsetY = iCnt*(cyScreenT-iTitleOffset)/iNumTrace +
                             iVScrollOffset + iTitleOffset;
			
/* Mark pick on screen, if it really shows up on screen */			
                  if ( pt[1].x >= iChanIDOffset && pt[1].x <= cxScreenT )
                  {
                     pt[0].y = lOffsetY - lTitleFHt/2;
                     pt[1].y = lOffsetY + lTitleFHt/2;
                     MoveToEx ( hdc, pt[0].x, pt[0].y, NULL );
                     LineTo ( hdc, pt[1].x, pt[1].y );
                  }
               }            
               iCnt++;
            }
            break;
         }
   DeleteObject( SelectObject( hdc, hOPen ) );   /* Reset Pen color */
}

     /**************************************************************
      *                 DisplayTitles()                            *
      *                                                            *
      * This function outputs the title to the P-time and          *
      * hypocenter list boxes.                                     *
      *                                                            *
      * January, 2006: Removed P-time window header.               *
      *                                                            *
      * Arguments:                                                 *
      *  hdc               Device context to use                   *
      *  lTitleFHt         Title font height                       *
      *  lTitleFWd         Title font width                        *
      *  cxScreenT         Screen width in pixels                  *
      *  cyScreenT         Screen height in pixels                 *
      *                                                            *
      **************************************************************/
      
void DisplayTitles( HDC hdc, long lTitleFHt, long lTitleFWd, int cxScreenT,
                    int cyScreenT )
{
   HFONT   hOFont, hNFont;      /* Font handles */
   POINT   pt;
   
/* Create font */   
   hNFont = CreateFont( lTitleFHt, lTitleFWd, 
             0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
             DEFAULT_PITCH | FF_MODERN, "Elite" );

/* Select font and color */
   hOFont = (HFONT) SelectObject( hdc, hNFont );    /* Select the font */
   SetTextColor( hdc, RGB( 0, 0, 0 ) );             /* Black text */
   
/* First, write hypocenter listbox title */
   pt.x = 3*cxScreenT/200;
   pt.y = 0*cyScreenT/100;
   TextOut( hdc, pt.x, pt.y,
    "Date   O-time    Lat.    Lon.    Dep   Res   Azm   #Stn    ID       Ms     Mw     Mwp    Mb     Ml",
    98);
	 
/* Next, write P-time listbox title */
/*   pt.x = 3*cxScreenT/200;
   pt.y = 34*cyScreenT/100;
   TextOut (hdc, pt.x, pt.y,
    "Stn   P-time    Res.   Dist.  Ms  Mwp Mb  Ml", 44); */
   DeleteObject (SelectObject (hdc, hOFont));       /* Reset font */
}

 /********************************************************************
  *                    DisplayTracesChild()                          *
  *                                                                  *
  * This function draws seismic traces to the child window in order  *
  * of the expected P-time.  Traces are scaled by pre-event          *
  * background amplitudes.                                           *
  *                                                                  *
  * September, 2005: Added catch so that data from future times will *
  *                  not be displayed (for ATPLayer)                 *
  *                                                                  *
  * Arguments:                                                       *
  *  hdc               Device context to use                         *
  *  Sta               Array of all station data structures          *
  *  PBuf              Array of all PPick data structures            *
  *  iNumTrace         Number of traces to show on screen            *
  *  iNumStas          Number of stations in Sta and PBuf arrays     *
  *  cxScreenT         Horizontal pixel width of window              *
  *  cyScreenT         Vertical pixel height of window               *
  *  iVScrollOffset    Vertical scroll setting                       *
  *  iTitleOffset      Vertical offset of traces to give room at top *
  *  iChanIDOffset     Horizontal offset of traces to give room at lt*
  *  dScreenTime       # seconds to show on screen                   *
  *  dPreEventTime     Trace time to display prior to expected P     *
  *  iHScrollOffset    Horizontal scroll setting                     *
  *  piNumShown        Number of traces written to screen            *
  *                                                                  *
  ********************************************************************/
  
void DisplayTracesChild( HDC hdc, STATION Sta[], PPICK PBuf[], int iNumTrace,
                    int iNumStas, int cxScreenT, int cyScreenT,
                    int iVScrollOffset, int iTitleOffset, int iChanIDOffset,
                    double dScreenTime, double dPreEventTime,
                    int iHScrollOffset, int *piNumShown ) 
{
   double  dOldestTime;              /* Time of oldest data in buffer */
   HPEN    hBPen, hBlPen, hOPen;     /* Pen handles */
   long    i, j, k, lTemp;           /* Loop counters */
   long    lHi, lLo;                 /* Highest and lowest data values */
   long    lNum;                     /* # of data points to draw to screen */
   long    lNum2;                    /* # of data points to use for background*/
   long    lOffsetX;                 /* Dist to skip from left side of screen */
   long    lOffsetY;                 /* Dist to skip on top of screen */
   long    lStart;                   /* Starting buffer index */
   long    lStart2;                  /* Starting buffer index for scaling */
   long    lTime;                    /* present time */
   long    lVertRange;               /* Max signal deflection when iClipIt=1 */
   POINT   ptData[DISPLAY_BUFFER_SIZE_H];  /* Trace display buffer */

/* Create pen and select into device context */
   hBlPen = CreatePen( PS_SOLID, 1, RGB( 0,0,192 ) );       /* Blue  */
   hBPen  = CreatePen( PS_SOLID, 1, RGB( 0,0,0 ) );         /* Black */
   hOPen = SelectObject( hdc, hBPen );
   *piNumShown = 0;
   
/* Loop through all stations */
   for ( i=0; i<iNumStas; i++ )                 /* PBuf Loop */
      for (j=0; j<iNumStas; j++ )               /* Sta Loop */
         if ( !strcmp( Sta[j].szStation, PBuf[i].szStation ) &&
              !strcmp( Sta[j].szChannel, PBuf[i].szChannel ) &&
              !strcmp( Sta[j].szNetID, PBuf[i].szNetID ) ) 		 
         {
            if ( Sta[j].dEndTime > 0. && strcmp( Sta[j].szChannel, "BLZ" ) && 
                 strcmp( Sta[j].szChannel, "SLZ" ) &&
                 strcmp( Sta[j].szChannel, "SMZ" ) )
            {
/* Compute time of oldest data in buffer */
               dOldestTime = Sta[j].dEndTime -
                ((double) Sta[j].lRawCircCtr/Sta[j].dSampRate) +
                1./Sta[j].dSampRate;
		 
/* Find where on screen we should start plotting */
               lOffsetX = iChanIDOffset;
				
/* Where in the buffer should we start plotting from? */
               lStart = (long) (((PBuf[i].dExpectedPTime-dPreEventTime-
                      (((double) iHScrollOffset/(double)
                        (cxScreenT-iChanIDOffset))*dScreenTime))-
                         dOldestTime)*Sta[j].dSampRate + 0.00001);
               if ( lStart < 0 )
               {
                  lOffsetX += (long)((((double) labs( lStart )/Sta[j].dSampRate) 
                      / dScreenTime) * (double) (cxScreenT-iChanIDOffset));
                  lStart = 0;
               }
               if ( lStart > Sta[j].lRawCircCtr ) goto EndIf;
                  		 
/* How many points should we plot? */
               lNum = (long) (dScreenTime*Sta[j].dSampRate + 0.0001);
               if ( lNum+lStart > Sta[j].lRawCircCtr )
                  lNum = Sta[j].lRawCircCtr - lStart;
               if ( lNum > DISPLAY_BUFFER_SIZE_H ) lNum = DISPLAY_BUFFER_SIZE_H;
/* Compare to present time for ATPlayer (so that we don't show data ahead of
   now */                                                        
               time( &lTime );
               lTemp = (long) (((double)lTime-dOldestTime) * Sta[j].dSampRate);
               if ( lNum+lStart > lTemp ) lNum = lTemp-lStart-1;
	                                 
/* Compute Y offset from top to center of trace and max signal range */
               lOffsetY = *piNumShown*(cyScreenT-iTitleOffset)/iNumTrace +
                          iVScrollOffset + iTitleOffset;
               lVertRange = (cyScreenT-iTitleOffset) / (3*iNumTrace/2);
			   
/* Compute Scale Factor (based on pre-event noise levels) */			   
/*               Sta[j].dScaleFactor = (double) (3*lVertRange/2) /
                                     (Sta[j].dAveMDF*20.); */
               if ( iHScrollOffset >= 0 )
               {
                  lHi = -100000000;
                  lLo =  100000000;
                  lStart2 = (long) (((PBuf[i].dExpectedPTime-(dPreEventTime-2.)-
                          (((double) iHScrollOffset/(double)
                            (cxScreenT-iChanIDOffset))*dScreenTime))-
                             dOldestTime)*Sta[j].dSampRate + 0.00001);
                  if ( lStart2 < 0 ) lStart2 = 0;
                  if ( lStart2 > Sta[j].lRawCircCtr ) goto EndIf;
		  
                  lNum2 = (long) (dScreenTime/8.*Sta[j].dSampRate + 0.0001);
                  if ( lNum2+lStart2 > Sta[j].lRawCircCtr )
                     lNum2 = Sta[j].lRawCircCtr - lStart2;
                  if ( lNum2 > DISPLAY_BUFFER_SIZE_H ) lNum2 = DISPLAY_BUFFER_SIZE_H;
                  for ( k=lStart2; k<lStart2+lNum2; k++ )
                  {
                     if (Sta[j].plFiltCircBuff[k] > lHi )
                        lHi = Sta[j].plFiltCircBuff[k];
                     if (Sta[j].plFiltCircBuff[k] < lLo )
                        lLo = Sta[j].plFiltCircBuff[k];
                  }
                  Sta[j].dScaleFactor = (double) (lVertRange/2) /
                                        (double) (lHi-lLo);
               }

/* Fill display buffer with data */		 
               for ( k=lStart; k<lStart+lNum; k++ )
               {
                  lTemp = k - lStart;          /* lTemp is display buffer ctr */
                  if ( lTemp >= DISPLAY_BUFFER_SIZE_H ) break;
                  if ( k >= Sta[j].lRawCircCtr ) break;
                  ptData[lTemp].x = lOffsetX + (long) (((cxScreenT-iChanIDOffset)
                   *(double) lTemp/Sta[j].dSampRate) / dScreenTime);
		
/* Load y array */
                  ptData[lTemp].y = (long) (((double) Sta[j].plFiltCircBuff[k]
                   -Sta[j].dAveLDC) * Sta[j].dScaleFactor)*(-1) + lOffsetY;
		   
/* Limit trace deflection */
                  if ( ptData[lTemp].y - lOffsetY > lVertRange )
                     ptData[lTemp].y = lOffsetY + lVertRange;
                  if ( ptData[lTemp].y - lOffsetY < lVertRange*(-1) )
                     ptData[lTemp].y = lOffsetY - lVertRange;
               }
	    
/* Plot the trace */
               if ( Sta[j].iComputeMwp == 1 ) SelectObject( hdc, hBlPen );
               else                           SelectObject( hdc, hBPen );
               MoveToEx( hdc, ptData[0].x, ptData[0].y, NULL );
               Polyline( hdc, &ptData[0], lTemp-1 );
EndIf:;        *piNumShown = *piNumShown + 1;
            }
            break;
         }               
   DeleteObject( hBPen );
   DeleteObject( hBlPen );
   SelectObject( hdc, hOPen );
} 

     /**************************************************************
      *                  FillHypoList()                            *
      *                                                            *
      * This function fills the hypocenter listbox.                *
      *                                                            *
      * NOTE: The system button font is used to fill this listbox. *
      * It looks best if a constant width font is used.  Change    *
      * this in the WM_SIZE calls.                                 *
      *                                                            *
      * Arguments:                                                 *
      *  hwnd              Window handle                           *
      *  Hypo              Array of hypocenter data                *
      *                                                            *
      **************************************************************/
      
void FillHypoList( HWND hwnd, HYPO Hypo[] )
{
   char    cEW, cNS;                                /* Lat/lon indicator */
   double  dM;                                      /* Magnitude (to tenth) */
   double  dRes;                                    /* Residual (to tenth) */
   int     i;
   LATLON  ll;                                      /* Geographic location */
   long    lTime;                                   /* 1/1/70 time */
   char    szBuffer[256], szTemp[12];
   struct  tm *tm;                                  /* time structure */

/* Delete all existing list box hypocenters */
   SendMessage( hwnd, LB_RESETCONTENT, 0, 0 );
   
/* Fill up list box with hypocenters */   
   i = 0;
   while ( Hypo[i].dLat > 0. && Hypo[i].dLon > 0. && i < MAX_QUAKES )
   {
      strcpy( szBuffer, "\0" );
      lTime = (long) (Hypo[i].dOriginTime+0.5);
      tm = TWCgmtime( lTime );
      itoaX( tm->tm_mon+1, szTemp );
      PadZeroes( 2, szTemp );
      strcpy( szBuffer, szTemp );                            /* Month */
      strcat( szBuffer, "/" );
      itoaX( tm->tm_mday, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Day */
      strcat( szBuffer, "  " );
      itoaX( tm->tm_hour, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Hour */
      strcat( szBuffer, ":" );
      itoaX( tm->tm_min, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Minute */
      strcat( szBuffer, ":" );
      itoaX( tm->tm_sec, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Second */
      strcat( szBuffer, "  " );
      
      cEW = 'E';                             /*Put lat/lon in form for output */
      cNS = 'N';
      GeoGraphic( &ll, (LATLON *) &Hypo[i] );
      while ( ll.dLon > 180. ) ll.dLon -= 360.;
      if (ll.dLon < 0.0 )
      {
         ll.dLon = fabs (ll.dLon);
         cEW = 'W';
      }
      if (ll.dLat < 0.0)
      {
         ll.dLat = fabs (ll.dLat);
         cNS = 'S';
      }
      ll.dLat = (double) ((int) ((ll.dLat+0.05)*10)) / 10.;  /* Round-off */
      ll.dLon = (double) ((int) ((ll.dLon+0.05)*10)) / 10.;  /* Round-off */
      itoaX( (int) ll.dLat, szTemp );
      if ( strlen( szTemp ) == 1 ) strcat( szBuffer, " " );
      strcat( szBuffer, szTemp );                            /* Lat. degrees */
      strcat( szBuffer, "." );
      itoaX( (int) ((ll.dLat-floor( ll.dLat ))*10.+0.01), szTemp );
      PadZeroes( 1, szTemp );
      strcat( szBuffer, szTemp );                            /* Lat. dec. deg */
      strncat( szBuffer, &cNS, 1 );
      strcat( szBuffer, "  " );
      itoaX( (int) ll.dLon, szTemp );
      if ( strlen( szTemp ) == 1 ) strcat( szBuffer, "  " );
      if ( strlen( szTemp ) == 2 ) strcat( szBuffer, " " );
      strcat( szBuffer, szTemp );                            /* Lon. degrees */
      strcat( szBuffer, "." );
      itoaX( (int) ((ll.dLon-floor( ll.dLon ))*10.+0.01), szTemp );
      PadZeroes( 1, szTemp );
      strcat( szBuffer, szTemp );                            /* Lon. dec. deg */
      strncat( szBuffer, &cEW, 1 );
      strcat( szBuffer, "   " );
      itoaX( (int) (Hypo[i].dDepth+0.5), szTemp );
      if ( strlen( szTemp ) == 1 ) strcat( szBuffer, "  " );
      if ( strlen( szTemp ) == 2 ) strcat( szBuffer, " " );
      strcat( szBuffer, szTemp );                            /* Depth */
      strcat( szBuffer, "   " );
      if ( Hypo[i].dAvgRes < 9.9 )
      {
         dRes = (double) ((int) ((Hypo[i].dAvgRes+0.05)*10)) / 10.;/* Round */
         itoaX( (int) dRes, szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Residual */
         strcat( szBuffer, "." );
         itoaX( (int) ((dRes-floor( dRes ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Residual dec. */
         strcat( szBuffer, "   " );
      }
      else                                                   /* Very Hi Res. */
      {
         strcat( szBuffer, "***" );                          /* Residual */
         strcat( szBuffer, "   " );
      }
      itoaX( Hypo[i].iAzm, szTemp );
      if ( strlen( szTemp ) == 1 ) strcat( szBuffer, "  " );
      if ( strlen( szTemp ) == 2 ) strcat( szBuffer, " " );
      strcat( szBuffer, szTemp );                            /* Azimuth cov. */
      strcat( szBuffer, "    " );
      itoaX( Hypo[i].iNumPs, szTemp );
      if ( strlen( szTemp ) == 1 ) strcat( szBuffer, " " );
      strcat( szBuffer, szTemp );                            /* # Ps used */
      strcat( szBuffer, "   " );
      itoaX( Hypo[i].iQuakeID, szTemp );
      PadZeroes( 4, szTemp );
      strcat( szBuffer, szTemp );                            /* Quake ID */
      strcat( szBuffer, "-" );
      itoaX( Hypo[i].iVersion, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Version */
      strcat( szBuffer, "  " );
      if ( Hypo[i].dMSAvg > 0. )
      {	  
         dM = (double) ((int) ((Hypo[i].dMSAvg+0.05)*10)) / 10.;/* Round Ms */
         itoaX( (int) dM, szTemp );
         PadZeroes( 1, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Ms */
         strcat( szBuffer, "." );
         itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Ms decimal */
         strcat( szBuffer, "-" );
         itoaX( Hypo[i].iNumMS, szTemp );
         PadZeroes( 2, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Num Ms */
      }
      else strcat( szBuffer, "      " );
      strcat( szBuffer, " " );
      if ( Hypo[i].dMwAvg > 0. )
      {	  
         dM = (double) ((int) ((Hypo[i].dMwAvg+0.05)*10)) / 10.;/* Round Mw */
         itoaX( (int) dM, szTemp );
         PadZeroes( 1, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Mw */
         strcat( szBuffer, "." );
         itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Mw decimal */
         strcat( szBuffer, "-" );
         itoaX( Hypo[i].iNumMw, szTemp );
         PadZeroes( 2, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Num Mw */
      }
      else strcat( szBuffer, "      " );
      strcat( szBuffer, " " );
      if ( Hypo[i].dMwpAvg > 0. && Hypo[i].iNumMwp > 2 )
      {	  
         dM = (double) ((int) ((Hypo[i].dMwpAvg+0.05)*10)) / 10.;/* Round Mwp */
         itoaX( (int) dM, szTemp );
         PadZeroes( 1, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Mwp */
         strcat( szBuffer, "." );
         itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Mwp decimal */
         strcat( szBuffer, "-" );
         itoaX( Hypo[i].iNumMwp, szTemp );
         PadZeroes( 2, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Num Mwp */
         strcat( szBuffer, " " );                         
      }
      else if ( Hypo[i].dMwpAvg > 0. && Hypo[i].iNumMwp <= 2 )
      {	  
         dM = (double) ((int) ((Hypo[i].dMwpAvg+0.05)*10)) / 10.;/* Round Mwp */
         itoaX( (int) dM, szTemp );
         PadZeroes( 1, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Mwp */
         strcat( szBuffer, "." );
         itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Mwp decimal */
         strcat( szBuffer, "-" );
         itoaX( Hypo[i].iNumMwp, szTemp );
         PadZeroes( 2, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Num Mwp */
         strcat( szBuffer, "?" );                         
      }
      else strcat( szBuffer, "       " );
      if ( Hypo[i].dMbAvg > 0. )
      {	  
         dM = (double) ((int) ((Hypo[i].dMbAvg+0.05)*10)) / 10.;/* Round Mb */
         itoaX( (int) dM, szTemp );
         PadZeroes( 1, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Mb */
         strcat( szBuffer, "." );
         itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                         /* Mb decimal */
         strcat( szBuffer, "-" );
         itoaX( Hypo[i].iNumMb, szTemp );
         PadZeroes( 2, szTemp );                 
         strcat( szBuffer, szTemp );                         /* Num Mb */
      }
      else strcat( szBuffer, "      " );
      strcat( szBuffer, " " );
      if ( Hypo[i].dMlAvg > 0. )
      {	  
         dM = (double) ((int) ((Hypo[i].dMlAvg+0.05)*10)) / 10.;/* Round Ml */
         itoaX( (int) dM, szTemp );
         PadZeroes( 1, szTemp );                 
         strcat( szBuffer, szTemp );                            /* Ml */
         strcat( szBuffer, "." );
         itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
		 PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                            /* Ml decimal */
         strcat( szBuffer, "-" );
         itoaX( Hypo[i].iNumMl, szTemp );
         PadZeroes( 2, szTemp );
         strcat( szBuffer, szTemp );                         /* Num Ml */
      }		 
      else strcat( szBuffer, "      " );
      strcat( szBuffer, " " );

/* Send line to list box */      
      SendMessage (hwnd, LB_INSERTSTRING, i, (LPARAM) szBuffer);
      i++;
   }
   SendMessage (hwnd, LB_SETCURSEL, iActiveHypo, (LPARAM) szBuffer);
}
 
     /**************************************************************
      *                 FillPTimeList()                            *
      *                                                            *
      * This function fills the P-time listbox.                    *
      *                                                            *
      * NOTE: The system button font is used to fill this listbox. *
      * It looks best if a constant width font is used.  Change    *
      * this in the WM_SIZE calls.                                 *
      *                                                            *
      * Arguments:                                                 *
      *  hwnd              Window handle                           *
      *  pszFile           File in which to look for P data        *
      *                                                            *
      **************************************************************/
      
void FillPTimeList( HWND hwnd, char *pszFile )
{
   double  dM;                                      /* Magnitude (to tenth) */
   double  dRes;                                    /* Residual (to tenth) */
   int     i, iCnt, iTemp;
   long    lTime;                                   /* 1/1/70 time */
   char    szBuffer[128], szTemp[12];
   struct  tm *tm;                                  /* time structure */

/* Delete all existing list box hypocenters */
   SendMessage( hwnd, LB_RESETCONTENT, 0, 0 );
   
/* Fill up P buffer with data */   
   RequestSpecificMutex( &mutsem1 );   /* Sem protect buff writes */
   for ( i=0; i<Nsta; i++ ) InitP( &PBufL[i] );
   for ( i=0; i<Nsta; i++ ) PBufL[i].iUseMe = 0;
   ReadPickFile2( &iNumPAuto, PBufL, pszFile, Nsta );
   ReleaseSpecificMutex( &mutsem1 );   /* Release sem */
   
/* Fill up list box with P-times and magnitudes */   
   iCnt = 0;
   for ( i=0; i<iNumPAuto; i++ )
      if ( PBufL[i].dPTime > 0. )
      {
         strcpy( szBuffer, PBufL[i].szStation );                /* Station */
         if ( strlen( PBufL[i].szStation ) == 3 ) strcat( szBuffer, " " );
         strcat( szBuffer, "  " );
         lTime = (long) (PBufL[i].dPTime+0.05);
         tm = TWCgmtime( lTime );
         itoaX( tm->tm_hour, szTemp );
         PadZeroes( 2, szTemp );
         strcat( szBuffer, szTemp );                            /* Hour */
         itoaX( tm->tm_min, szTemp );
         PadZeroes( 2, szTemp );
         strcat( szBuffer, szTemp );                            /* Minute */
         itoaX( tm->tm_sec, szTemp );
         PadZeroes( 2, szTemp );
         strcat( szBuffer, szTemp );                            /* Second */
         strcat( szBuffer, "." );
         iTemp = (int) ((PBufL[i].dPTime-floor( PBufL[i].dPTime ))*10.+0.501);
         while ( iTemp >= 10 ) iTemp -= 10;
         itoaX( iTemp, szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                            /* Tenths */
         if ( PBufL[i].iUseMe > 0 ) strcat( szBuffer, " " );    /* KOd ? */
         else                       strcat( szBuffer, "X" );  
         strcat( szBuffer, " " );
        
         if ( fabs( PBufL[i].dRes ) < 9.9 )
         {
            if ( PBufL[i].dRes < 0. )                           /* Negate */         
               strcat( szBuffer, "-" );
            else
               strcat( szBuffer, " " );
            if ( PBufL[i].dRes > 0. )                           /* Round */
               dRes = (double) ((int) ((PBufL[i].dRes+0.05)*10.))/10.;
            else
               dRes = (double) ((int) ((PBufL[i].dRes-0.05)*10.))/10.;
            itoaX( abs( (int) dRes ), szTemp );
            PadZeroes( 1, szTemp );
            strcat( szBuffer, szTemp );                         /* Residual */
            strcat( szBuffer, "." );
            if ( PBufL[i].dRes < 0. )
               itoaX( abs( (int) ((dRes-(double) ((int) dRes))*10.-0.01) ), szTemp );
            else
               itoaX( abs( (int) ((dRes-(double) ((int) dRes))*10.+0.01) ), szTemp );
            PadZeroes( 1, szTemp );
            strcat( szBuffer, szTemp );                         /* Residual dec. */
            strcat( szBuffer, "   " );
         }
         else                                                   /* Very Hi Res. */
         {
            strcat( szBuffer, "****" );                         /* Residual */
            strcat( szBuffer, "   " );
         }
       
         dRes = (double) ((int) ((PBufL[i].dDelta+0.05)*10.))/10.;  /* Round */
         itoaX( (int) dRes, szTemp );
         if ( strlen( szTemp ) == 1 ) strcat( szBuffer, "  " );
         if ( strlen( szTemp ) == 2 ) strcat( szBuffer, " " );
         strcat( szBuffer, szTemp );                            /* Distance */
         strcat( szBuffer, "." );
         itoaX( (int) ((dRes-floor( dRes ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                            /* Distance dec. */
         strcat( szBuffer, "  " );
      
         if ( PBufL[i].dMSMag > 0. )
         {	  
            dM = (double) ((int) ((PBufL[i].dMSMag+0.05)*10)) / 10.;/* Round Ms */
            itoaX( (int) dM, szTemp );
            PadZeroes( 1, szTemp );                 
            strcat( szBuffer, szTemp );                         /* Ms */
            strcat( szBuffer, "." );
            itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
            PadZeroes( 1, szTemp );
            strcat( szBuffer, szTemp );                         /* Ms decimal */
         }
         else strcat( szBuffer, "   " );
         strcat( szBuffer, " " );
         if ( PBufL[i].dMwpMag > 0. )
         {	  
            dM = (double) ((int) ((PBufL[i].dMwpMag+0.05)*10)) / 10.;/* Round Mwp */
            itoaX( (int) dM, szTemp );
            PadZeroes( 1, szTemp );                 
            strcat( szBuffer, szTemp );                         /* Mwp */
            strcat( szBuffer, "." );
            itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
            PadZeroes( 1, szTemp );
            strcat( szBuffer, szTemp );                         /* Mwp decimal */
         }
         else strcat( szBuffer, "   " );
         strcat( szBuffer, " " );
         if ( PBufL[i].dMbMag > 0. )
         {	  
            dM = (double) ((int) ((PBufL[i].dMbMag+0.05)*10)) / 10.;/* Round Mb */
            itoaX( (int) dM, szTemp );
            PadZeroes( 1, szTemp );                 
            strcat( szBuffer, szTemp );                         /* Mb */
            strcat( szBuffer, "." );
            itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
            PadZeroes( 1, szTemp );
            strcat( szBuffer, szTemp );                         /* Mb decimal */
         }
         else strcat( szBuffer, "   " );
         strcat( szBuffer, " " );
         if ( PBufL[i].dMlMag > 0. )
         {	  
            dM = (double) ((int) ((PBufL[i].dMlMag+0.05)*10)) / 10.;/* Round Ml */
            itoaX( (int) dM, szTemp );
            PadZeroes( 1, szTemp );                 
            strcat( szBuffer, szTemp );                         /* Ml */
            strcat( szBuffer, "." );
            itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
            PadZeroes( 1, szTemp );
            strcat( szBuffer, szTemp );                         /* Ml decimal */
         }		 
         else strcat( szBuffer, "   " );
         strcat( szBuffer, " " );
         
/* Send line to list box */      
         SendMessage (hwnd, LB_INSERTSTRING, iCnt, (LPARAM) szBuffer);
         iCnt++;
      }
   
/* Show the rest of data (probably MS) */   
   for ( i=0; i<iNumPAuto; i++ )
      if ( PBufL[i].dPTime < 0.0001 )
      {
         strcpy( szBuffer, PBufL[i].szStation );                /* Station */
         if ( strlen( PBufL[i].szStation ) == 3 ) strcat( szBuffer, " " );
         strcat( szBuffer, "          " );
         if ( PBufL[i].iUseMe > 0 ) strcat( szBuffer, " " );    /* KOd ? */
         else                       strcat( szBuffer, "X" );  
         strcat( szBuffer, "        " );
        
         dRes = (double) ((int) ((PBufL[i].dDelta+0.05)*10.))/10.;  /* Round */
         itoaX( (int) dRes, szTemp );
         if ( strlen( szTemp ) == 1 ) strcat( szBuffer, "  " );
         if ( strlen( szTemp ) == 2 ) strcat( szBuffer, " " );
         strcat( szBuffer, szTemp );                            /* Distance */
         strcat( szBuffer, "." );
         itoaX( (int) ((dRes-floor( dRes ))*10.+0.01), szTemp );
         PadZeroes( 1, szTemp );
         strcat( szBuffer, szTemp );                            /* Distance dec. */
         strcat( szBuffer, "  " );
      
         if ( PBufL[i].dMSMag > 0. )
         {	  
            dM = (double) ((int) ((PBufL[i].dMSMag+0.05)*10)) / 10.;/* Round Ms */
            itoaX( (int) dM, szTemp );
            PadZeroes( 1, szTemp );                 
            strcat( szBuffer, szTemp );                         /* Ms */
            strcat( szBuffer, "." );
            itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
            PadZeroes( 1, szTemp );
            strcat( szBuffer, szTemp );                         /* Ms decimal */
         }
         else strcat( szBuffer, "   " );
         strcat( szBuffer, " " );
         if ( PBufL[i].dMwpMag > 0. )
         {	  
            dM = (double) ((int) ((PBufL[i].dMwpMag+0.05)*10)) / 10.;/* Round Mwp */
            itoaX( (int) dM, szTemp );
            PadZeroes( 1, szTemp );                 
            strcat( szBuffer, szTemp );                         /* Mwp */
            strcat( szBuffer, "." );
            itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
            PadZeroes( 1, szTemp );
            strcat( szBuffer, szTemp );                         /* Mwp decimal */
         }
         else strcat( szBuffer, "   " );
         strcat( szBuffer, " " );
         if ( PBufL[i].dMbMag > 0. )
         {	  
            dM = (double) ((int) ((PBufL[i].dMbMag+0.05)*10)) / 10.;/* Round Mb */
            itoaX( (int) dM, szTemp );
            PadZeroes( 1, szTemp );                 
            strcat( szBuffer, szTemp );                         /* Mb */
            strcat( szBuffer, "." );
            itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
            PadZeroes( 1, szTemp );
            strcat( szBuffer, szTemp );                         /* Mb decimal */
         }
         else strcat( szBuffer, "   " );
         strcat( szBuffer, " " );
         if ( PBufL[i].dMlMag > 0. )
         {	  
            dM = (double) ((int) ((PBufL[i].dMlMag+0.05)*10)) / 10.;/* Round Ml */
            itoaX( (int) dM, szTemp );
            PadZeroes( 1, szTemp );                 
            strcat( szBuffer, szTemp );                         /* Ml */
            strcat( szBuffer, "." );
            itoaX( (int) ((dM-floor( dM ))*10.+0.01), szTemp );
            PadZeroes( 1, szTemp );
            strcat( szBuffer, szTemp );                         /* Ml decimal */
         }		 
         else strcat( szBuffer, "   " );
         strcat( szBuffer, " " );
         
/* Send line to list box */      
         SendMessage (hwnd, LB_INSERTSTRING, iCnt, (LPARAM) szBuffer);
         iCnt++;
      }
}

     /**************************************************************
      *                   FindDataEnd()                            *
      *                                                            *
      * This function finds where data ends within the buffer. This*
      * is necessary since data is read before an entire data file *
      * is filled.  So the last part of the data buffer will be    *
      * zeroes as data hasn't arrived yet. The shift from data to  *
      * zeroes can cause a "kick" by the filter if there is a DC   *
      * offset.                                                    *
      *                                                            *
      * Arguments:                                                 *
      *  pSta              Station information structure           *
      *                                                            *
      **************************************************************/
void FindDataEnd( STATION *pSta )
{
   long    lLastNonZero;     /* Latest index with data != 0 */
   long    i;                /* Counters */

/* See if any data was on this trace */
   lLastNonZero = 0;
   if ( pSta->dEndTime > 0.1 )   /* If yes, at what time does it end? */
   {
      for ( i=0; i<=pSta->lRawCircCtr; i++ )
         if ( pSta->plRawCircBuff[i] != 0 )  /* Possibly stops here */
            lLastNonZero = i;
/* If we're close to end, assume we're at end, otherwise, update variables
   to reflect actual data end. */
      if ( lLastNonZero < pSta->lRawCircCtr-NUM_AT_ZERO )
      {
         pSta->dEndTime -= ((double) (pSta->lRawCircCtr-lLastNonZero)/
                                      pSta->dSampRate);
         pSta->lSampIndexF = lLastNonZero;
         pSta->lRawCircCtr = lLastNonZero;
      }			       
   }
}

      /*******************************************************
       *                      GetEwh()                       *
       *                                                     *
       *      Get parameters from the earthworm.d file.      *
       *******************************************************/

int GetEwh( EWH *Ewh )
{
   if ( GetLocalInst( &Ewh->MyInstId ) != 0 )
   {
      fprintf( stderr, "hypo_display: Error getting MyInstId.\n" );
      return -1;
   }

   if ( GetInst( "INST_WILDCARD", &Ewh->GetThisInstId ) != 0 )
   {
      fprintf( stderr, "hypo_display: Error getting GetThisInstId.\n" );
      return -2;
   }
   if ( GetModId( "MOD_WILDCARD", &Ewh->GetThisModId ) != 0 )
   {
      fprintf( stderr, "hypo_display: Error getting GetThisModId.\n" );
      return -3;
   }
   if ( GetType( "TYPE_HEARTBEAT", &Ewh->TypeHeartBeat ) != 0 )
   {
      fprintf( stderr, "hypo_display: Error getting TypeHeartbeat.\n" );
      return -4;
   }
   if ( GetType( "TYPE_ERROR", &Ewh->TypeError ) != 0 )
   {
      fprintf( stderr, "hypo_display: Error getting TypeError.\n" );
      return -5;
   }
   if ( GetType( "TYPE_HYPOTWC", &Ewh->TypeHypoTWC ) != 0 )
   {
      fprintf( stderr, "hypo_display: Error getting TYPE_HYPOTWC.\n" );
      return -8;
   }
   if ( GetType( "TYPE_PICKTWC", &Ewh->TypePickTWC ) != 0 )
   {
      fprintf( stderr, "hypo_display: Error getting TYPE_PICKTWC.\n" );
      return -9;
   }
   return 0;
}

  /******************************************************************
   *                           GetLDC()                             *
   *                                                                *
   *  Determine and update moving averages of signal value          *
   *  (called LDC here).                                            *
   *                                                                *
   *  Arguments:                                                    *
   *    lStart      Starting index to use for LDC                   *
   *    lNSamps     Number of samples in this packet                *
   *    WaveLong    Pointer to data array                           *
   *    pdLDC       Pointer to long term DC average                 *
   *                                                                *
   ******************************************************************/

void GetLDC( long lStart, long lNSamps, long *WaveLong, double *pdLDC )
{
   double  dSumLDC;     /* Summation of all values in this packet */
   int     i;
   
   dSumLDC = 0.;
   
/* Sum up total for this packet */
   for ( i=0; i<lNSamps; i++ )
      dSumLDC  += ((double) WaveLong[i+lStart]);
	  
/* Compute new LTAs */
   *pdLDC =  (dSumLDC/(double) lNSamps);
}

 /***********************************************************************
  *                             GetMag()                                *
  *      Loop through data to get magnitude parameters for the latest   *
  *      interactive pick.                                              *
  *                                                                     *
  *  Arguments:                                                         *
  *     Sta              Pointer to station being processed             *
  *     pPBuf            P-pick buffer                                  *
  *     Gparm            Configuration buffer                           *
  *                                                                     *
  ***********************************************************************/
  
void GetMag( STATION *Sta, PPICK *pPBuf, GPARM *Gparm )
{
   double  dOldestTime;   /* Oldest time (1/1/70 seconds) in buffer */
   long    i, lIndex;
   int     iReversal;     /* 1 when signal difference changes sign */
   long    lNum;          /* Number of samples to evaluate */
   long    lNumInBuff;    /* # samples ahead of P in buffer */
   long    lPIndex;       /* Buffer index of P-time */
   
/* Is P-time within buffer? */
   dOldestTime = Sta->dEndTime -
    ((double) Sta->lRawCircCtr/Sta->dSampRate) + 1./Sta->dSampRate;
   if ( pPBuf->dPTime < dOldestTime || pPBuf->dPTime > Sta->dEndTime )
   {
      logit( "", "%s - P-time out of buffer, P=%lf, End=%lf, Old=%lf\n",
             Sta->szStation, pPBuf->dPTime, Sta->dEndTime, dOldestTime );
      return;
   }
   
/* What is buffer index of P-time? */   
   lPIndex = (long) ((pPBuf->dPTime-dOldestTime)*Sta->dSampRate + 0.0001) - 1;
   
/* How many points to evaluate? */   
   lNumInBuff = (long) ((Sta->dEndTime-pPBuf->dPTime) * Sta->dSampRate);
   lNum = (long) (Sta->dSampRate * (double) Gparm->LGSeconds);
   if ( lNum > lNumInBuff )       /* There is more data needed to compute Ml */
      lNum = lNumInBuff;

/* Initialize some things */
   if ( lPIndex > 0 ) Sta->lSampOld = Sta->plFiltCircBuff[lPIndex-1];
   else               Sta->lSampOld = Sta->plFiltCircBuff[Sta->lRawCircSize-1];
   Sta->lMDFOld = 0;
   Sta->lSampsPerCyc = 0;
   Sta->lMDFRunning = 0;
   Sta->lCycCnt = 0;
   Sta->dMaxPk = 0.;
   iReversal = 0;

/* Loop through data to get magnitude */
   for ( i=0; i<lNum; i++ )
   {
      lIndex = i + lPIndex;
      if ( lIndex >= Sta->lRawCircSize ) lIndex -= Sta->lRawCircSize; 
      Sta->lSampNew = Sta->plFiltCircBuff[lIndex];
      Sta->lMDFNew = Sta->lSampNew - Sta->lSampOld;
	  
/* Check for cycle changes */
      if ( i > 0 )
      {
         if ( (Sta->lMDFOld <  0 && Sta->lMDFNew <  0) ||
              (Sta->lMDFOld >= 0 && Sta->lMDFNew >= 0) )
         {     /* No changes, continuing adding up MDF */
            Sta->lSampsPerCyc++;
            Sta->lMDFRunning += Sta->lMDFNew;
         }
         else  /* Cycle has changed sign, get mags and start anew */
         {
            if ( i > 1 ) iReversal = 1;         /* For 1st motion determination */
            ComputeMbMl ( Sta, i, pPBuf, Gparm->MbCycles );
            Sta->lMDFRunning = Sta->lMDFNew;
            Sta->lSampsPerCyc = 0;
         }
	  
/* Get first motion */
         if ( i == 1 )	  
            if (Sta->lMDFRunning > 0) pPBuf->cFirstMotion = 'U';
            else                      pPBuf->cFirstMotion = 'D';
         if ( i > 1 && i <= FIRST_MOTION_SAMPS && iReversal == 1 )
            pPBuf->cFirstMotion = '?';
      }                                                      	  
      Sta->lSampOld = Sta->lSampNew;
      Sta->lMDFOld = Sta->lMDFNew;
   }   
}

      /*********************************************************
       *                     HThread()                         *
       *                                                       *
       *  This thread gets messages from the hypocenter ring.  *
       *                                                       *
       *********************************************************/
	   
thr_ret HThread( void *dummy )
{
   MSG_LOGO      getlogoH;        /* Logo of requested picks */
   char          HIn[MAX_HYPO_SIZE];/* Pointer to hypocenter from ring */
   int           i;
   int           lineLen;         /* Length of heartbeat message */
   char          line[40];        /* Heartbeat message */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */
   long          RawBufl;         /* Raw data buffer size (for Mwp) */

/* Set up logos for Hypo ring 
   **************************/
   getlogoH.instid = Ewh.GetThisInstId;
   getlogoH.mod    = Ewh.GetThisModId;
   getlogoH.type   = Ewh.TypeHypoTWC;


/* Flush the input ring
   ********************/
   while ( tport_getmsg( &Gparm.InRegion, &getlogoH, 1, &logo, &MsgLen,
                         HIn, MAX_HYPO_SIZE) != GET_NONE );
						 
/* Loop to read hypocenter messages
   ********************************/
   while ( tport_getflag( &Gparm.InRegion ) != TERMINATE )
   {
      int     rc;               /* Return code from tport_getmsg() */
      time_t  now;              /* Current time */
      time_t  lastupdate;       /* Last update time */

/* Send a heartbeat to the transport ring
   **************************************/
      time( &now );
      if ( (now - then) >= Gparm.HeartbeatInt )
      {
         then = now;
         sprintf( line, "%d %d\n", now, myPid );
         lineLen = strlen( line );
         if ( tport_putmsg( &Gparm.InRegion, &hrtlogo, lineLen, line ) !=
              PUT_OK )
         {
            logit( "et", "hypo_display: Error sending heartbeat." );
            break;
         }
      }
	  
/* Get a hypocenter from transport region
   **************************************/
      rc = tport_getmsg( &Gparm.InRegion, &getlogoH, 1, &logo, &MsgLen,
                         HIn, MAX_HYPO_SIZE);

      if ( rc == GET_NONE )
      {
         sleep_ew( 1000 );
         continue;
      }

      if ( rc == GET_NOTRACK )
         logit( "et", "hypo_display: Tracking error.\n");

      if ( rc == GET_MISS_LAPPED )
         logit( "et", "hypo_display: Got lapped on the ring.\n");

      if ( rc == GET_MISS_SEQGAP )
         logit( "et", "hypo_display: Gap in sequence numbers.\n");

      if ( rc == GET_MISS )
         logit( "et", "hypo_display: Missed messages.\n");

      if ( rc == GET_TOOBIG )
      {
         logit( "et", "hypo_display: Retrieved message too big (%d) for msg.\n",
                MsgLen );
         continue;
      }
	  
/* Put hypocenter into structure (NOTE: No byte-swapping is performed, so 
   there will be trouble getting hypos from an opposite order machine)
   *******************************************************************/
      if ( HypoStruct( HIn, &HStruct ) < 0 )
      {
         logit( "t", "Problem in HypoStruct function\n" );
         continue;
      }
      
/* If we are in the ATPlayer version of hypo_display, see if we should re-init
   ***************************************************************************/
      if ( strlen( Gparm.ATPLineupFileBB ) > 2 )    
         if ( now-lastupdate > 3600 )   /* Re-init */
         {
            Nsta = ReadLineupFile( Gparm.ATPLineupFileBB, StaArray );
            logit ("", "Nsta=%ld\n", Nsta);
            if ( Nsta < 2 )
            {
               logit( "", "Bad Lineup File read %s\n", Gparm.ATPLineupFileBB );
               continue;
            }	    
            for ( i=0; i<Nsta; i++ )
            {
               InitVar( &StaArray[i] );	  
               free( StaArray[i].pdRawDispData );
               free( StaArray[i].pdRawIDispData );
               free( StaArray[i].plRawCircBuff );
               free( StaArray[i].plFiltCircBuff );
               ResetFilter( &StaArray[i] );		 
/* Allocate memory for Mwp displacement data buffer 
   ************************************************/
               RawBufl = sizeof (double) * (long) (StaArray[i].dSampRate+0.001)*
                (Gparm.MwpSeconds+20);
               if ( RawBufl/sizeof (double) > MAXMWPARRAY )
               {
                  logit( "et", "Mwp array too large for %s - %lf, i=%ld, buf=%ld\n",
                         StaArray[i].szStation, StaArray[i].dSampRate, i, RawBufl );
                  RawBufl = (MAXMWPARRAY-1) * sizeof (double);
               }
               StaArray[i].pdRawDispData = (double *) malloc( (size_t) RawBufl);
               if ( StaArray[i].pdRawDispData == NULL )
               {
                  logit( "et", "hypo_display: Can't allocate disp. data buff for %s\n",
                   StaArray[i].szStation );
               }
		 
/* Allocate memory for Mwp integrated displacement data buffer 
   ***********************************************************/
               StaArray[i].pdRawIDispData = (double *) malloc( (size_t) RawBufl );
               if ( StaArray[i].pdRawIDispData == NULL )
               {
                  logit( "et", "hypo_display: Cannot allocate int. disp. data buffer for %s\n",
                   StaArray[i].szStation );
               }
		 		                  
/* Allocate memory for raw circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples)
   ********************************/
               StaArray[i].lRawCircSize =
                (long) (StaArray[i].dSampRate*(double)Gparm.MinutesInBuff*60.+0.1);
               RawBufl = sizeof (long) * StaArray[i].lRawCircSize;
               StaArray[i].plRawCircBuff = (long *) malloc( (size_t) RawBufl );
               if ( StaArray[i].plRawCircBuff == NULL )
               {
                  logit( "et", "hypo_display: Can't allocate raw circ buffer for %s\n",
                   StaArray[i].szStation );
               }
		 		 
/* Allocate memory for filt. circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples)
   ********************************/
               StaArray[i].plFiltCircBuff = (long *) malloc( (size_t) RawBufl );
               if ( StaArray[i].plFiltCircBuff == NULL )
               {
                  logit( "et", "hypo_display: Can't allocate filt circ buffer for %s\n",
                   StaArray[i].szStation );
               }
            }      
/* Init the P-pick buffer
   **********************/
            for ( i=0; i<Nsta; i++ ) InitP( &PBufG[i] );
            for ( i=0; i<Nsta; i++ ) PBufG[i].iUseMe = 0;
            for ( i=0; i<Nsta; i++ ) InitP( &PBufL[i] );
            for ( i=0; i<Nsta; i++ ) PBufL[i].iUseMe = 0;
         } 	 
	  
/* Send Message to WndProc
   ***********************/
      lastupdate = now;
      logit( "", "Post message %s to NEW_LOC\n", HIn );
      PostMessage( hwndWndProc, WM_NEW_LOC, 0, 0 );   
   }
}

     /**************************************************************
      *                  InitPBufWithSta()                         *
      *                                                            *
      * Initialize the global PPICK structure with data about the  *
      * station from the STATION array.  What can't be filled in   *
      * is initialized with zeroes.                                *
      *                                                            *
      * Arguments:                                                 *
      *  iNumSta     Number of stations in StaArray                *
      *  P           PPICK data structures                         *
      *  Sta         STATION data structures                       *
      *                                                            *
      **************************************************************/

void InitPBufWithSta( int iNumSta, PPICK P[], STATION Sta[] )	  
{
   int     i;

   for ( i=0; i<iNumSta; i++ )
   {
      P[i].dLat = Sta[i].dLat;
      P[i].dLon = Sta[i].dLon;
      GeoCent( (LATLON *) &P[i] );
      GetLatLonTrig( (LATLON *) &P[i] );      
      P[i].dPTime = 0.;
      P[i].dExpectedPTime = 0.;
      P[i].iUseMe = 0;
      P[i].dMbAmpGM = 0.;
      P[i].dMbMag   = 0.;
      P[i].lMbPer   = 0;
      P[i].dMbPer   = 0.;
      P[i].dMbTime  = 0.;
      P[i].iMbClip  = 0;
      P[i].dMlAmpGM = 0.;
      P[i].dMlMag   = 0.;
      P[i].lMlPer   = 0;
      P[i].dMlPer   = 0.;
      P[i].dMlTime  = 0.;
      P[i].iMlClip  = 0;
      P[i].dMSAmpGM = 0.;
      P[i].dMSMag   = 0.;
      P[i].lMSPer   = 0;
      P[i].dMSPer   = 0.;
      P[i].dMSTime  = 0.;
      P[i].iMSClip  = 0;
      P[i].dMwpIntDisp = 0.;
      P[i].dMwpMag     = 0.;
      P[i].dMwpTime    = 0.;
      P[i].lPickIndex  = 0;
      P[i].dClipLevel  = Sta[i].dClipLevel;
      P[i].dElevation  = Sta[i].dElevation;
      P[i].dGainCalibration  = Sta[i].dGainCalibration;
      P[i].dSens       = Sta[i].dSens;
      P[i].iStationType= Sta[i].iStationType;
      P[i].dRes        = 0.;
      P[i].dDelta      = 0.;
      P[i].dAz         = 0.;
      P[i].dFracDelta  = 0.;
      P[i].dSnooze     = 0.;
      P[i].dCooze      = 0.;
      P[i].cFirstMotion= '?';
      strcpy( P[i].szStation, Sta[i].szStation );
      strcpy( P[i].szChannel, Sta[i].szChannel );
      strcpy( P[i].szNetID, Sta[i].szNetID );
      strcpy( P[i].szPhase, "     " );
   }
}

     /**************************************************************
      *                  MoveChildren()                            *
      *                                                            *
      * This function places the child windows which list P-times  *
      * and hypocenter parameters in their proper position on the  *
      * screen.                                                    *
      *                                                            *
      * Arguments:                                                 *
      *  cxScreenT         Horizontal pixel width of window        *
      *  cyScreenT         Vertical pixel height of window         *
      *                                                            *
      * Return - 0 if OK, -1 if problem                            *
      **************************************************************/
	  
int MoveChildren( int cxScreenT, int cyScreenT )
{
/* Move hypocenter list LISTBOX */
   if ( !MoveWindow( hwndHypo,              /* Move the hypocenter window */
         1*cxScreenT/100,                   /* top left x starting location */
         3*cyScreenT/100,                   /* top left y starting loc */
         98*cxScreenT/100,                  /* Width */
         32*cyScreenT/100,                  /* Height */
         TRUE ) )                           /* Force a re-paint */
   {
      logit( "t", "Move Hypo window failed\n" );
      return -1;
   }
   
/* Move the P-time window */
/*   if ( !MoveWindow( hwndPTime, 1*cxScreenT/100, 37*cyScreenT/100,
                     46*cxScreenT/100, 63*cyScreenT/100, TRUE ) )			
   {
      logit( "t", "Move P-pick window failed\n" );
      return -1;
   } */
return 0;
}

      /***********************************************************
       *                     MwpTimeDlgProc()                    *
       *                                                         *
       * This procedure is used to change the integration window *
       * length and signal:noise for Mwp computations.           *
       *                                                         *
       ***********************************************************/
	   
long WINAPI MwpTimeDlgProc (HWND hwnd, UINT msg, UINT wParam, long lParam)
{
   double  dTemp2;
   int     iTemp;
   char    szTemp[12];

   switch ( msg )
   {
      case WM_INITDIALOG:         /* Set defaults */
         SetDlgItemText( hwnd, EF_MWPTIME,
                         itoaX( iMwpSeconds, szTemp ) );
         SetDlgItemText( hwnd, EF_MWPSN,
                         gcvt( dMwpSigNoise, 3, szTemp ) );
         SetFocus( hwnd );
         break;

      case WM_COMMAND:            
         switch (LOWORD (wParam))
            {
               case IDOK:                /* OK was chosen */
/* Read integration time in seconds */
                  GetDlgItemText( hwnd, EF_MWPTIME, szTemp, sizeof (szTemp) );
                  iTemp = atoi( szTemp );
                  if ( iTemp <= 0 )      /* Was it too small? */
                  {  MessageBox( hwnd, "Invalid Time",
                                 NULL, MB_OK | MB_ICONEXCLAMATION ); break; }
                  else if ( iTemp > Gparm.MwpSeconds+20 )   /* Too big? */
                  {  MessageBox( hwnd, "Max time is listed in .d file (+20)", 
                                 NULL, MB_OK | MB_ICONEXCLAMATION ); break; }
/* Read Signal-to-noise ratio which must be exceeded
   for Mwp computations to take place */
                  GetDlgItemText( hwnd, EF_MWPSN, szTemp, sizeof (szTemp) );
                  dTemp2 = atof( szTemp );
                  if ( dTemp2 <= 0. )     /* Is it reasonable? */
                  {  MessageBox( hwnd, "Invalid S:N",
                                 NULL, MB_OK | MB_ICONEXCLAMATION ); break; }
                  dMwpSigNoise = dTemp2;
                  iMwpSeconds = iTemp;
                  EndDialog( hwnd, IDOK );
                  break;

               case IDCANCEL:           /* Escape was chosen */
                  EndDialog( hwnd, IDCANCEL );
                  break;
            }
            break;
   }
   return 0;
}

      /*************************************************************
       *                  MwpWarningDlgProc()                      *
       *                                                           *
       * This procedure gives a warning when Mwp window is adjusted*
       * during a large earthquake.                                *
       *                                                           *
       *************************************************************/
	   
long WINAPI MwpWarningDlgProc (HWND hwnd, UINT msg, UINT wParam, long lParam)
{
   switch ( msg )
   {
      case WM_INITDIALOG:         /* Set defaults */
         SetFocus( hwnd );
         break;

      case WM_COMMAND:            
         switch (LOWORD (wParam))
            {
               case IDOK:                /* Continue was chosen */
                  EndDialog( hwnd, IDOK );
                  break;

               case IDCANCEL:           /* Escape was chosen */
                  EndDialog( hwnd, IDCANCEL );
                  break;
            }
            break;
   }
   return 0;
}

     /**************************************************************
      *                  UpdatePPickArray()                        *
      *                                                            *
      * Grab certain data from one PPICK array and update another  *
      * with it.                                                   *
      *                                                            *
      * Arguments:                                                 *
      *  iNumPIn     Number of stations in auto-loc P-array        *
      *  iNumSta     Number of stations in PPICK arrays            *
      *  PIn         Array to take data from                       *
      *  POut        Array to put data in                          *
      *                                                            *
      **************************************************************/

void UpdatePPickArray( int iNumPIn, int iNumSta, PPICK PIn[], PPICK POut[] )
{
   int     i, j;

   for ( i=0; i<iNumPIn; i++ )
      for ( j=0; j<iNumSta; j++ )
         if ( !strcmp( PIn[i].szStation, POut[j].szStation ) &&
              !strcmp( PIn[i].szChannel, POut[j].szChannel ) &&
              !strcmp( PIn[i].szNetID, POut[j].szNetID ) )
         {
            POut[j].dPTime = PIn[i].dPTime;
            POut[j].dRes   = PIn[i].dRes;
            POut[j].iUseMe = PIn[i].iUseMe;
            POut[j].lPickIndex = PIn[i].lPickIndex;
            POut[j].cFirstMotion = PIn[i].cFirstMotion;
            POut[j].dMbAmpGM = PIn[i].dMbAmpGM;
            POut[j].dMbPer   = PIn[i].dMbPer;
            POut[j].lMbPer   = (long) (PIn[i].dMbPer*10. + 0.5);
            POut[j].dMbMag   = PIn[i].dMbMag;
            POut[j].dMbTime  = PIn[i].dMbTime;
            POut[j].dMlAmpGM = PIn[i].dMlAmpGM;
            POut[j].dMlPer   = PIn[i].dMlPer;
            POut[j].lMlPer   = (long) (PIn[i].dMlPer*10. + 0.5);
            POut[j].dMlMag   = PIn[i].dMlMag;
            POut[j].dMlTime  = PIn[i].dMlTime;
            POut[j].dMSAmpGM = PIn[i].dMSAmpGM;
            POut[j].dMSPer   = PIn[i].dMSPer;
            POut[j].lMSPer   = (long) (PIn[i].dMSPer + 0.5);
            POut[j].dMSMag   = PIn[i].dMSMag;
            POut[j].dMSTime  = PIn[i].dMSTime;
            POut[j].dMwpIntDisp = PIn[i].dMwpIntDisp;
            POut[j].dMwpTime    = PIn[i].dMwpTime;
            POut[j].dMwpMag     = PIn[i].dMwpMag;
            strcpy( POut[j].szPhase, PIn[i].szPhase );
            break;
         }
}

      /*********************************************************
       *                      WndProc()                        *
       *                                                       *
       *  This dialog procedure processes messages from the    *
       *  windows screen and messages past to it from elsewhere*
       *  (e.g., the message WM_NEW_LOC comes from thread when *
       *  a hypocenter has been picked out of the ring).       *
       *  Based on the LOCATE program from                     *
       *  the West Coast/Alaska Tsunami Warning Center.        *
       *                                                       *
       *********************************************************/
       
long WINAPI WndProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   HCURSOR hCursor;         /* Present cursor handle (hourglass or arrow) */
   static  HDC hdc;         /* Device context of screen */
   HANDLE  hMenu;           /* Handle to the menu */
   static  HFONT hPTimeFont;/* List box font handle (same as titles) */
   static  int iUpdateWaveForm; /* 1->Redraw; 2->Force redraw; 0->Don't */
   static  int iFirst;      /* Flag CreateChildren the first SIZE message */
   int     iIndex;          /* Hypocenter index clicked on */
   static  int     iLastQuakeID;  /* Previous quake ID */
   long    lTime;                 /* Present 1/1/70 time */
   static  time_t  lTimeNow, lTimeLast; /* Times of disk reads */
   static  long lTitleFHt, lTitleFWd;   /* Font height and width */
   PPICK   PForce;          /* PPick structure which forces auto-relocate */
   PAINTSTRUCT ps;          /* Paint structure used in WM_PAINT command */
   RECT    rct;             /* RECT (rectangle structure) */
   static  char    *pszFileName;  /* File with P data */

/* Respond to user input (menu choices, etc.) and system messages */
   switch ( msg )
   {
      case WM_CREATE:         /* Do this the first time through */
         iActiveHypo = 0;
         lTimeLast = 0;
         iFirst = 1;
         iUpdateWaveForm = 0;
         iMwpSeconds = Gparm.MwpSeconds;
         dMwpSigNoise = Gparm.MwpSigNoise;
         hCursor = LoadCursor( NULL, IDC_ARROW );
         SetCursor( hCursor );
         hMenu = GetMenu( hwnd );
         iLastQuakeID = 0;
         iTimerGoing = 0;
         iTimerRedoMwp = 0;
         iFromMainWnd = 0;
		 
/* Initially load up hypocenter array with previously located quakes */
         LoadHypo( Gparm.QuakeFile, Hypo );
         break;

      case WM_TIMER:		
         if ( wParam == ID_TIMER )           /* Time to redraw wfs */
         {
            time( &lTime );
            if ( (lTime - (long) (Hypo[iActiveHypo].dOriginTime+0.5)) <=
                  REDRAW_TOTAL )
            {
               iUpdateWaveForm = 2;
               iTimerGoing = 1;
               iTimerRedoMwp = 1;
               InvalidateRect( hwnd, NULL, TRUE ); /* Force a re-PAINT */
            }
            else
            {
               iTimerGoing = 0;
               KillTimer (hwnd, ID_TIMER);  /* Stop the redraw timer */
            }
         }
         break;

/* Get screen size in pixels, re-paint, and re-proportion screen */
      case WM_SIZE:
         cyScreen = HIWORD (lParam);
         cxScreen = LOWORD (lParam);
         iUpdateWaveForm = 1;
         if ( iFirst )     /* Do this on first WM_SIZE only */
         {
            iFirst = 0;
/* Create the child windows (shown on main screen) */
            if ( CreateChildren( hwnd, cxScreen, cyScreen ) < 0 )
            {
               logit( "t", "Child windows not created\n" );
               PostMessage( hwnd, WM_DESTROY, 0, 0 );
            }
         }
		 
/* Put children in proper place on screen */		 
         if ( MoveChildren( cxScreen, cyScreen ) < 0 )
            logit( "t", "Child windows not moved successfully\n" );
			
/* Compute font size */
         lTitleFHt = cyScreen / 33;
         lTitleFWd = cxScreen / 100;
		 
/* Set the font in the Hypo and P-time list boxes (must be proportional) */
         hPTimeFont = CreateFont( lTitleFHt, lTitleFWd, 
                       0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,
                       OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
                       DEFAULT_PITCH | FF_MODERN, "Elite" );
					   
         SendMessage( hwndHypo, WM_SETFONT, (unsigned int) hPTimeFont, TRUE );
		 
/* Create the Child window (A) which shows waveforms for this quake */		 
         if ( iChildA )  /* If this child is already running, stop it */
         {
            DestroyWindow( hwndChildA );
            iChildA = 0;
         }
         hwndChildA = CreateWindow( szChildAName, /* Child Name */
          szChildAName,           /* Window Class name */
          WS_BORDER | WS_CHILDWINDOW | WS_HSCROLL | WS_VSCROLL |
          WS_CLIPSIBLINGS,        /* Style */
          1*cxScreen/100,        /* top left x starting location */
          38*cyScreen/100,        /* top left y starting location */
          98*cxScreen/100,        /* initial screen width in pixels */
          61*cyScreen/100,        /* initial screen height in pixels */
          hwnd,                   /* Parent window */
          (HMENU) ID_CHILD_WAVEFORM,/* Child window ID */
                                  /* Let Windows supply the Instance */
          (HANDLE) GetWindowLong( hwnd, GWL_HINSTANCE ),
          NULL );                 /* No extra data to pass in */
         if ( hwndChildA == NULL )/* window not created */
         {
            logit( "t", "waveform window not created - 2\n" );
            break;
         }
         ShowWindow( hwndChildA, SW_SHOW );   /* Show the window */
         UpdateWindow( hwndChildA );    /* Force an initial PAINT call */
         SetActiveWindow( hwndChildA ); /* Give it the input focus */
         iChildA = 1;   
         FillHypoList( hwndHypo, Hypo );
         pszFileName = GetPFile( &Hypo[iActiveHypo], Gparm.szLocFilePath );
         FillPTimeList( hwndPTime, pszFileName );
         InvalidateRect( hwnd, NULL, TRUE );    /* Force a re-PAINT */
         break;

/* Respond to menu seclections */	
      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {	
            case IDM_FORCE_LOC:		    /* Force location for active quake */
/* Check time - don't do it if more than 60 minutes since quake */
               time( &lTime );
               if ( (lTime - (long) (Hypo[iActiveHypo].dOriginTime+0.5)) >
                    (60*60) )
               {
                  logit( "", "Can't force location - too late\n" );
                  MessageBox( hwnd, "Can't re-locate; past 1 hour",
                              NULL, MB_OK | MB_ICONEXCLAMATION ); 
               }
               else    /* Fill up P Struct with location trigger */
               {
                  strcpy( PForce.szStation, "LOC" );
                  strcpy( PForce.szChannel, "ATE" );
                  strcpy( PForce.szNetID,   "XX" );
                  PForce.lPickIndex   = 0;
                  PForce.iUseMe       = 0;
                  PForce.dPTime       = 0.;
                  PForce.cFirstMotion = '?';
                  strcpy( PForce.szPhase, "eX" );
                  PForce.dMbAmpGM     = 0.;
                  PForce.lMbPer       = 0;
                  PForce.dMbTime      = 0.;
                  PForce.dMlAmpGM     = 0.;
                  PForce.lMlPer       = 0;
                  PForce.dMlTime      = 0.;
                  PForce.dMSAmpGM     = 0.;
                  PForce.lMSPer       = 0;
                  PForce.dMSTime      = 0.;
                  PForce.dMwpIntDisp  = 0.;
                  PForce.dMwpTime     = 0.;
                  PForce.iHypoID      = Hypo[iActiveHypo].iQuakeID;
                  logit( "", "Force location id %ld\n", PForce.iHypoID );
                  ReportPick( &PForce, &Gparm, &Ewh, StaArray, Nsta );                     
               }
               break;
			   
            case IDM_MWP_DISPLAY:       /* Display Mwp integrations */
               if ( iChildB )  /* If this child is already running, stop it */
               {
                  DestroyWindow( hwndChildB );
                  iChildB = 0;
               }
			   
/* Create child window in which Mwp amps. are shown */
               hwndChildB = CreateWindow(
                szChildBName,           /* Child Name */
                szChildBName,           /* Initial title bar caption */
                WS_OVERLAPPEDWINDOW | WS_CHILD,   /* Std style */
//                48*cxScreen/100,        /* top left x starting location */
                1*cxScreen/100,        /* top left x starting location */
//                35*cyScreen/100,        /* top left y starting location */
                34*cyScreen/100,        /* top left y starting location */
//                51*cxScreen/100,        /* initial screen width in pixels */
                98*cxScreen/100,        /* initial screen width in pixels */
                30*cyScreen/100,        /* initial screen height in pixels */
                hwnd,                   /* Parent window */
                (HMENU) ID_CHILD_MWP_DISP,  /* Child window ID */
                                        /* Let Windows supply the Instance */
                (HANDLE) GetWindowLong( hwnd, GWL_HINSTANCE ),
                NULL );                 /* No extra data to pass in */
               if ( hwndChildB == NULL )/* window not created */
               {
                  logit( "t", "Integration window not created\n" );
                  break;
               }
               ShowWindow( hwndChildB, SW_SHOW );   /* Show the window */
               UpdateWindow( hwndChildB );    /* Force an initial PAINT call */
               SetActiveWindow( hwndChildB ); /* Give it the input focus */
               iChildB = 1;
			   	 
/* Re-size waveform window */			   
               if ( iChildA )  /* If this child is already running, stop it */
               {
                  DestroyWindow( hwndChildA );
                  iChildA = 0;
               }
               hwndChildA = CreateWindow( szChildAName, /* Child Name */
                szChildAName,           /* Window Class name */
                WS_BORDER | WS_CHILDWINDOW | WS_HSCROLL | WS_VSCROLL |
                WS_CLIPSIBLINGS,        /* Style */
//                48*cxScreen/100,        /* top left x starting location */
                1*cxScreen/100,        /* top left x starting location */
                66*cyScreen/100,        /* top left y starting location */
//                51*cxScreen/100,        /* initial screen width in pixels */
                98*cxScreen/100,        /* initial screen width in pixels */
                33*cyScreen/100,        /* initial screen height in pixels */
                hwnd,                   /* Parent window */
                (HMENU) ID_CHILD_WAVEFORM,/* Child window ID */
                                        /* Let Windows supply the Instance */
                (HANDLE) GetWindowLong( hwnd, GWL_HINSTANCE ),
                NULL );                 /* No extra data to pass in */
               if ( hwndChildA == NULL )/* window not created */
               {
                  logit( "t", "waveform window not created - 2\n" );
                  break;
               }
               ShowWindow( hwndChildA, SW_SHOW );   /* Show the window */
               UpdateWindow( hwndChildA );    /* Force an initial PAINT call */
               SetActiveWindow( hwndChildA ); /* Give it the input focus */
               iChildA = 1;   
               break;

/* Enter new values for integration window length and S:N */
            case IDM_MWP_RECOMPUTE: 
               if ( Hypo[iActiveHypo].dPreferredMag > 6.75 )
                   if ( DialogBox( hInstMain, "MwpWarning", hwndWndProc, 
                       (DLGPROC) MwpWarningDlgProc ) == IDCANCEL ) break;
               if ( DialogBox( hInstMain, "MwpTime", hwndWndProc, 
                   (DLGPROC) MwpTimeDlgProc ) == IDCANCEL ) break;
               PostMessage( hwndChildA, WM_MWP_RECOMPUTE, 0, 0 );   
               iFromMainWnd = 1;
               break;

            case IDM_REFRESH:                   /* Refresh the screen */
               iUpdateWaveForm = 2;
               InvalidateRect( hwnd, NULL, TRUE );
               break;

/* Put stations used in this location in PTime listbox */
            case ID_CHILD_HYPO:
               if ( HIWORD( wParam ) == LBN_DBLCLK ) /* Double-click on quake */
               {
                  iIndex = SendMessage( hwndHypo, LB_GETCURSEL, 0, 0 );
                  if ( iIndex >= 0 && iIndex < MAX_QUAKES )
                  {                            /* Create file name for P data */
                     iActiveHypo = iIndex;
                     iUpdateWaveForm = 2;
                     pszFileName = GetPFile( &Hypo[iActiveHypo], Gparm.szLocFilePath );
                     FillPTimeList( hwndPTime, pszFileName );
                     iMwpSeconds = Gparm.MwpSeconds;
                     dMwpSigNoise = Gparm.MwpSigNoise;
                     iLastQuakeID = Hypo[iActiveHypo].iQuakeID;
                     InvalidateRect( hwnd, NULL, TRUE );
                  }
               } 
               break;
         }
         break;
	
/* Have new location, do something with it */	
      case WM_NEW_LOC:		 
         iActiveHypo = CopyHypo( &HStruct, Hypo );
         FillHypoList( hwndHypo, Hypo );
         pszFileName = GetPFile( &Hypo[iActiveHypo], Gparm.szLocFilePath );
         FillPTimeList( hwndPTime, pszFileName );
/* Force an update of the waveform window if this is not just a magnitude
   update */
         if ( HStruct.iMagOnly == 0 ) iUpdateWaveForm = 1;
/* Force an update if this is not just a magnitude change and a different quake
   is being displayed */
         if ( HStruct.iQuakeID != iLastQuakeID )
         {
            iUpdateWaveForm = 2;
/* Start a timer so that waveform display are redrawn and Mwp updated */
            if ( iTimerGoing == 1 )	KillTimer( hwndWndProc, ID_TIMER );
            SetTimer( hwnd, ID_TIMER, REDRAW_INTERVAL, NULL );   
         }
         iMwpSeconds = Gparm.MwpSeconds;
         dMwpSigNoise = Gparm.MwpSigNoise;
         iLastQuakeID = HStruct.iQuakeID;
         InvalidateRect( hwnd, NULL, TRUE );
         break;

/* Fill in screen display */
      case WM_PAINT:
         time( &lTimeNow );
         hdc = BeginPaint( hwnd, &ps );         /* Get device context */
         GetClientRect( hwnd, &rct );
         FillRect( hdc, &rct, (HBRUSH) GetStockObject( WHITE_BRUSH ) );
         DisplayLittoral( hdc, &Hypo[iActiveHypo], lTitleFHt, lTitleFWd,
                          cxScreen, cyScreen, city, cityEC );
         DisplayTitles( hdc, lTitleFHt, lTitleFWd, cxScreen, cyScreen );
/* Don't update waveforms if they have been updated within the last 20s
   (since it takes a while for the reads and to give geophysicist a
    chance to add picks without screen being redrawn) */		 
         if ( (((lTimeNow > lTimeLast+1 && Hypo[iActiveHypo].iNumPs < 13) ||
                (lTimeNow > lTimeLast+20 && Hypo[iActiveHypo].iNumPs >= 13)) &&
                 iUpdateWaveForm >= 1 ) || iUpdateWaveForm == 2 )
         {
            PostMessage( hwndChildA, WM_DRAW_WAVEFORM, 0, 0 );   
            lTimeLast = lTimeNow;
         }  
         iUpdateWaveForm = 0;
         EndPaint( hwnd, &ps );
         break;

/* Close up shop and return */
      case WM_DESTROY:
         DeleteObject( hPTimeFont );
         if( iChildA == 1 )
         {
            DestroyWindow( hwndChildA );
            iChildA = 0;
         } 
         if( iChildB == 1 )
         {
            DestroyWindow( hwndChildB );
            iChildB = 0;
         } 
         logit( "", "WM_DESTROY posted\n" );
         PostQuitMessage( 0 );
         break;

      default:
         return ( DefWindowProc( hwnd, msg, wParam, lParam ) );
   }
return 0;
}
