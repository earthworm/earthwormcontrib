  /**********************************************************************
   *                              report.c                              *
   *                                                                    *
   **********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include <transport.h>
#include "hypo_display.h"

     /**************************************************************
      *                         ReportPick()                       *
      *                                                            *
      *                 Fill in TYPE_PICKTWC format                *
      *                                                            *
      * March, 2006: Prevent picks from going back to PRing if     *
      *              station not originally picked in main Ring    *
      *                                                            *
      **************************************************************/

void ReportPick( PPICK *P, GPARM *Gparm, EWH *Ewh, STATION Sta[], int iNumStas )
{
   int          i;
   MSG_LOGO     logo;      /* Logo of message to send to the output ring */
   int          lineLen;
   char         line[512]; /* TYPE_PICKTWC format message */
   long         lTemp;

/* First, see if this station is in the pick list
   (if not, do not send back to PICK_RING)
   **********************************************/
   for ( i=0; i<iNumStas; i++ )
      if ( !strcmp( Sta[i].szStation, P->szStation ) )
         if ( Sta[i].iPickStatus == 0 )/* This station should not be sent back*/
         {
            logit( "t", "Pick not reported for %s\n", P->szStation );
            return;
         }

/* Create TYPE_PICKTWC message
   ***************************/
   sprintf( line,    "%ld %ld %ld %s %s %s %ld %ld %lf %c %s %lf %ld %lf %lf "
                     "%ld %lf %lf %ld %lf %lE %lf %ld\0",
            (int) Ewh->TypePickTWC, (int) Gparm->MyModId, (int) Ewh->MyInstId,
            P->szStation, P->szChannel, P->szNetID, P->lPickIndex, P->iUseMe, 
            P->dPTime, P->cFirstMotion, P->szPhase, P->dMbAmpGM, P->lMbPer,
            P->dMbTime, P->dMlAmpGM, P->lMlPer, P->dMlTime,
            P->dMSAmpGM, P->lMSPer, P->dMSTime, P->dMwpIntDisp, P->dMwpTime,
            P->iHypoID );
   lineLen = strlen( line ) + 1;

/* log pick
   ********/
   lTemp = (long) (P->dPTime);
   logit( "e", "%s, P-time=%s %c P=%lf, Mbamp=%lf, MbPer=%ld, MbTime=%lf,"
    " Mlamp=%lf, MlPer=%ld, MlTime=%lf, Mwptime=%lf, MwpID=%le\n", P->szStation,
    asctime( TWCgmtime( lTemp ) ), P->cFirstMotion, P->dPTime, P->dMbAmpGM,
    P->lMbPer, P->dMbTime, P->dMlAmpGM, P->lMlPer, P->dMlTime, P->dMwpTime,
    P->dMwpIntDisp );

/* Send the pick to the output ring
   ********************************/
   logo.type   = Ewh->TypePickTWC;
   logo.mod    = Gparm->MyModId;
   logo.instid = Ewh->MyInstId;

   if ( tport_putmsg( &Gparm->PRegion, &logo, lineLen, line ) != PUT_OK )
      logit( "t", "hypo_display: Error sending pick to output ring.\n" );
}
