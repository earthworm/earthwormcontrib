
#
#                     Hypo_display's Configuration File
#
MyModId        MOD_HYPO_DISPLAY  # This instance of hypo_display
StaFile        "pick_wcatwc.sta" # File containing stns written to disk
StaDataFile    "station.dat"   # Further data on stations
InRing           HYPO_RING     # Input hypocenter transport ring
PRing            PICK_RING     # Output pick transport ring
HeartbeatInt            15     # Heartbeat interval, in seconds
#    Add file created in ATPlayer if used in conjunction with player (optional)
#    Comment this line if using hypo_display in real-time mode
#ATPLineupFileBB "\earthworm\run\params\ATP-BB.sta" # Station config file
#
#
#    Hypo_dispaly bandpass filters data prior to display. 
#    The following values are used as filter coefficients.
#    The default values (0.5, 5.) relate to the
#    response coefficients given within magcomp.  If other values are
#    used, the filter response for those values must be computed and used 
#    instead of the standard response.
#
LowCutFilter           0.5     # In hz, low frequency bandpass filter cutoff
HighCutFilter          5.0     # In hz, high frequency bandpass filter cutoff
#
#    After P-pick has been specified, the following value speicifes the number
#    of half-cycles to examine to determine mb.  The maximum amplitude
#    (and corresponding half-period) within these cycles is used to compute mb.
#
MbCycles                20     # Number 1/2 cycles of P to examine for mb
#
LGSeconds              160     # Number seconds after P to examine Lg for Ml
                               #  (first MbCycles of this time is excluded for
                               #   Ml period and amplitude)
#    Mwp computations are made as described by Tsuboi, Whitmore, and Sokolowski,
#    BSSA, v89, 1345-1351.  The technique has been completely automated here.
#    Results should be examined before issuance to ensure correctness.
#
MwpSeconds             200     # Max # seconds to evaluate after P start for Mwp
MwpSigNoise            2.5     # Signal-to-noise ratio necessary for Mwp comps.
#
FileLength               2     # Length of data files in minutes (same as
                               #  in the disk_wcatwc which writes the files)
DiskWritePath    "f:\datafile" # Data file directory
FileSuffix            ".S"     # File suffix for differentiating different
                               #  sample rates of the same data
PreEventTime            30.    # Time (seconds) to display before P							   
#
QuakeFile "\DoNotCopyToEB\oldquakeX.dat"  # List of located quakes
#MinutesInBuff            6     # Time (minutes) to allocate for each trace 
MinutesInBuff            8     # Time (minutes) to allocate for each trace 
                               # (Should be multiple of FileLength)
#NumTracePerScreen       25     # Number of traces to show on visible screen
NumTracePerScreen       30     # Number of traces to show on visible screen
                               # (the rest will scroll)
#TimePerScreen           60.    # Seconds of data to display on screen							   
TimePerScreen           120.    # Seconds of data to display on screen							   
RTPFile    "\seismic\locate\prtimeX.dat"   # WC/ATWC LOCATE real-time P File
LocFilePath "\DoNotCopyToEB\LocFiles\"     # Path for disk logs
#
Debug                    1     # If 1, print debugging message
