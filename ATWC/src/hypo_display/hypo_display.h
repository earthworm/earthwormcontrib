/******************************************************************
 *                          File Hypo_display.h                   *
 *                                                                *
 *  Include file for display module used at the West Coast/Alaska *
 *  Tsunami Warning Center.                                       *
 ******************************************************************/

#include <trace_buf.h>
#include "\earthworm\atwc\src\libsrc\earlybirdlib.h"

/* Definitions
   ***********/
#define DISPLAY_BUFFER_SIZE_H 30000 /* # samples allowed in trace buffer */
#define FIRST_MOTION_SAMPS 3 /* Number of samples after trigger which must be in
                                same direction for a 1st motion to be declared*/
#define TIMER_INT	3600000  /* P update timeout (after this, traces re-drawn)*/
#define NUM_AT_ZERO	6        /* # samps at 0 before assuming dead trace */
#define REDRAW_INTERVAL 30000/* Refresh screen every X mseconds after new loc */
#define REDRAW_TOTAL      900/* Stop redrawing after this amount of time (s) */

/* User defined window messages
   ****************************/
#define WM_NEW_LOC       (WM_USER + 10)
#define WM_DRAW_WAVEFORM (WM_USER + 11)
#define WM_MWP_RECOMPUTE (WM_USER + 12)

/* Child window IDs
   ****************/
#define ID_TIMER             1   /* Waveform redraw timer */   
#define ID_CHILD_HYPO      200   /* Hypocenter list box */
#define ID_CHILD_PTIME     201   /* P-time list box */
#define ID_CHILD_MAP       202   /* Map group box */
#define ID_CHILD_MAPBUTTON 203   /* Map button group box */
#define ID_CHILD_BUTTON    210   /* Map push Button (MAX_MAPS must not be
                                    defined below this) */
#define ID_CHILD_MWP_DISP  220   /* Child window which shows int. disp. */
									
#define ID_CHILD_WAVEFORM  300   /* Child window which waveforms */
#define ID_NULL            -1    /* Resource file declaration */

/* Menu IDs
   ********/
#define IDM_REFRESH        100   /* Re-draw the screen */   
#define IDM_MWP_DISPLAY    101   /* Display Mwp integrations */   
#define IDM_MWP_RECOMPUTE  102   /* Re-compute Mwp integrations */   
#define IDM_FORCE_LOC      103   /* Force re-location in loc_wcatwc */   

/* Entry Fields
   ************/
#define EF_MWPTIME          307       /* MwpTime dialog */
#define EF_MWPSN            308

/* This structure keeps track of which stations were drawn on screen */
typedef struct {
   char    szChannel[TRACE_CHAN_LEN]; /* Channel identifier (SEED notation) */
   char    szNetID[TRACE_NET_LEN];    /* Network ID */
   char    szStation[TRACE_STA_LEN];  /* Station name */
} ONSCREEN;

typedef struct {
   char           StaFile[64];    /* Name of file with SCN info */
   char           StaDataFile[64];/* Further data on stations */
   char ATPLineupFileBB[64];      /* Optional command when used with ATPlayer */
   long           InKey;          /* Key to ring where hypocenters live */
   long           PKey;           /* Key to ring where pickslive */
   int            HeartbeatInt;   /* Heartbeat interval in seconds */
   int            Debug;          /* If 1, print debug messages */
   unsigned char  MyModId;        /* Module id of this program */
   char           QuakeFile[128]; /* File with last MAX_QUAKES quake data */
   char           szLocFilePath[128]; /* Path to log the LOC files */
   int            MinutesInBuff;  /* Number of minutes data to save per trace */
   int            NumTracePerScreen;/* # traces to show on screen (rest scroll) */
   double         TimePerScreen;  /* # seconds data to show on screen */
   int            FileLength;     /* File size in minutes long */
   char           DiskWritePath[64];/* Directory to write disk files */
   char           FileSuffix[16]; /* Starting letter of file suffix */
   char           RTPFile[128];   /* File to send P/mag data to LOCATE */
   double         PreEventTime;   /* Time (seconds) to display before P */
   double         HighCutFilter;  /* Bandpass high cut in hz */
   double         LowCutFilter;   /* Bandpass low cut in hz */
   int            MbCycles;       /* # 1/2 cycles after P Mb can be computed */
   int            LGSeconds;      /* # seconds after P in which max LG can be 
                                     computed for Ml (excluding 1st MbCycles) */
   int            MwpSeconds;     /* Max # seconds to evaluate P for Mwp */
   double         MwpSigNoise;    /* Auto-Mwp necessary signal-to-noise ratio */
   SHM_INFO       InRegion;       /* Info structure for input region */
   SHM_INFO       PRegion;        /* Info structure for output P region */
} GPARM;

typedef struct {
   unsigned char MyInstId;        /* Local installation */
   unsigned char GetThisInstId;   /* Get messages from this inst id */
   unsigned char GetThisModId;    /* Get messages from this module */
   unsigned char TypeHeartBeat;   /* Heartbeat message id */
   unsigned char TypeError;       /* Error message id */
   unsigned char TypeHypoTWC;     /* Hypocenter message - TWC format */
   unsigned char TypePickTWC;     /* P-pick message - TWC format */
} EWH;

/* Function declarations for Hypo_display
   ************************************/
long WINAPI ChildAWndProc( HWND, UINT, UINT, long );
long WINAPI ChildBWndProc( HWND, UINT, UINT, long );
int     CreateChildren( HWND, int, int );
void    DisplayChannelIDChild( HDC, STATION [], PPICK [], ONSCREEN [], int,
                               long, long, int, int, int, int, int, int * );
void    DisplayKOPsH( HDC, int, int, int, int, int, int, long, long,
                      int, STATION [], PPICK [] );
void    DisplayLittoral( HDC, HYPO *, long, long, int, int, CITY *, CITY * );
void    DisplayMwpH( HDC, int, int, int, int, int, int, int, double,
                     double, STATION [], PPICK [], int );
void    DisplayPPicksH( HDC, int, int, int, int, int, int, int,
                        double, double, long, STATION [], PPICK [], int );
void    DisplayTitles( HDC, long, long, int, int );
void    DisplayTracesChild( HDC, STATION [], PPICK [], int, int, int, int,
                            int, int, int, double, double, int, int * );
void    FindDataEnd( STATION * );
int     GetEwh( EWH * );
void    GetLDC( long, long, long *, double * );
void    GetMag( STATION *, PPICK *, GPARM * );
void    FillHypoList( HWND, HYPO [] );
void    FillPTimeList( HWND, char * );
thr_ret HThread( void * );
void    InitPBufWithSta( int, PPICK [], STATION [] );	  
int     MoveChildren( int, int );
long WINAPI MwpTimeScaleDlgProc( HWND, UINT, UINT, long );
long WINAPI MwpWarningDlgProc( HWND, UINT, UINT, long );
void    UpdatePPickArray( int, int, PPICK [], PPICK [] );
long WINAPI WndProc( HWND, UINT, UINT, long );

int     GetConfig( char *, GPARM * );                    /* config.c */
void    LogConfig( GPARM * );

int     GetStaList( STATION **, int *, GPARM * );        /* stalist.c */
int     IsComment( char [] );
int     LoadStationData( STATION *, char * );
void    LogStaList( STATION *, int );

void    ReportPick( PPICK *, GPARM *, EWH *, STATION [], int );/* report.c */
