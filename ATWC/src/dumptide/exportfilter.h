/*   exportfilter.h   981112:ldd
 *   These are the prototypes for a generic filter for export.
 */
 
#define MAX_SCN_DUMP   256   /* Total max of stations allowed         */
#define INCREMENT_SCN   10   /* increment the limit of # scn's        */
#define STATION_LEN      6   /* max string-length of station code     */
#define CHAN_LEN         8   /* max string-length of component code   */
#define NETWORK_LEN      8   /* max string-length of network code     */
#define ABBREV_LEN       8   /* max string-length of stn abbreviation */

typedef struct {             
   char   sta[STATION_LEN+1];
   char   chan[CHAN_LEN+1];
   char   net[NETWORK_LEN+1];
   char   stnAbbrev[ABBREV_LEN+1];
   double dGain;
} SCNstruct;


/*********************************************************
 * exportfilter_com() processes all config-file commands *
 *                    related to the filter code.        *
 * Returns  1 if the command was recognized an processed *
 *          0 if the command was not recognized          *
 * Note: this function may exit the process if it finds  *
 *       serious errors in any commands                  *
 *********************************************************/
int exportfilter_com( void );


/*********************************************************
 * exportfilter_init()   Make sure all the required      *
 *  commands were found in the config file, do any other *
 *  startup things necessary for filter to work properly *
 * Returns: 0 if all went well                           *
 *          non-zero if an error was detected            *
 *********************************************************/
int exportfilter_init( void );


/**********************************************************
 * exportfilter() looks at the candidate message.         *
 *                Analyzes it and possibly reformats it.  *
 * Returns: 1 if the resulting message is to be exported  *
 *          0 otherwise                                   *
 **********************************************************/
int exportfilter( char *, long,  unsigned char,     /* input message  */
                  char**, long*, unsigned char * ); /* output message */


/**********************************************************
 * exportfilter_shutdown()  frees allocated memory and    *
 *         does any other cleanup stuff                   *
 **********************************************************/
void exportfilter_shutdown( void );
