/*
 *  scnfilter.c  contains a function to filter messages based
 *               on content of site-component-network fields.
 *               Works on TYPE_TRACEBUF, TYPE_PICK2KK, TYPE_CODA2K 
 *               messages.
 *              
 *               Returns: 1 if this site-compenent-network
 *                          is on the list of requested scn's.
 *                        0 if it isn't.
 *                       -1 if it's an unknown message type.
 *
 *   981112 Lynn Dietz
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <trace_buf.h>
#include <kom.h>
#include "\earthworm\v6.3\src\data_sources\naqs2ew\naqsser.h"
#include "exportfilter.h"

/* Globals for site-component-net filter
 ***************************************/
int FilterInit = 0;          /* initialization flag                   */
int Max_SCN    = 0;          /* working limit on # scn's to ship      */
extern int nSCN;             /* # of scn's we're configured to ship   */
extern char TideGageFile[64];/* File with sea level stn control info */
extern TGFILE_HEADER *TGH;   /* Output file data header info          */

extern SCNstruct *Send; /* list of s-c-n's to accept                  */
char Wild = '*';        /* wildcard value for station,channel,network */

/* Message types this filter can handle
 **************************************/
unsigned char TypeTraceBuf;   
unsigned char TypePick2K;
unsigned char TypeCoda2K;

/* Local function prototypes
 ***************************/
char *copytrim( char *, char *, int );
int  FillHeaderStructure( char *, char *, TGFILE_HEADER * );

/*********************************************************
 * exportfilter_com() processes config-file commands to  *
 *               set up the filter & send-list.          *
 * Returns  1 if the command was recognized & processed  *
 *          0 if the command was not recognized          *
 * Note: this function may exit the process if it finds  *
 *       serious errors in any commands                  *
 *********************************************************/
int exportfilter_com( void )
{
   char   *str;
   size_t  size;

/* Add an entry to the send-list
 ********************************/
   if ( k_its( "Send_scn" ) ) 
   {
      if ( nSCN >= MAX_SCN_DUMP )
      {
         fprintf( stderr, "scnfilter_com: Too many stations, increase"
                          " MAX_SCN_DUMP\n" );
         exit( -1 );
      }
      if ( nSCN >= Max_SCN )
      {
         Max_SCN += INCREMENT_SCN;
         size     = Max_SCN * sizeof( SCNstruct );
         Send     = (SCNstruct *) realloc( Send, size );
         if ( Send == NULL )
         {
            fprintf( stderr,
                     "scnfilter_com: Error allocating %d bytes"
                     " for SCN list; exiting!\n", size );
            exit( -1 );
         }
      }
      str = k_str( );
      if ( str && strlen( str ) <= (size_t) STATION_LEN ) 
      {
         strcpy( Send[nSCN].sta, str );
         str = k_str( );
         if ( str && strlen( str ) <= (size_t) CHAN_LEN ) 
         {
            strcpy( Send[nSCN].chan, str );
            str = k_str( );
            if ( str && strlen( str ) <= (size_t) NETWORK_LEN ) 
            {
               strcpy( Send[nSCN].net, str );
               str = k_str( );
               if ( str && strlen( str ) <= (size_t) ABBREV_LEN ) 
               {
                  strcpy( Send[nSCN].stnAbbrev, str );
                  Send[nSCN].dGain = k_val();
                  if ( (FillHeaderStructure( TideGageFile,
                        Send[nSCN].stnAbbrev, &TGH[nSCN] )) == 0 )
                  {			      
                     fprintf( stderr, "Stn abbrev %s not in file\n",
                              Send[nSCN].stnAbbrev );
                     exit( -1 );
                  }
                  nSCN++;
                  return( 1 );        
               }
            }
         }         
      }
      fprintf( stderr, "scnfilter_com: Error in <Send_scn> command "
                       "(argument either too long or missing): \"%s\"\n",
                        k_com() );
      fprintf( stderr, "scnfilter_com: exiting!\n" );
      exit( -1 );
   }
   return( 0 );
}


/***********************************************************
 * exportfilter_init()   Make sure all the required        *
 *    commands were found in the config file, do any other *
 *    startup things necessary for filter to work properly *
 ***********************************************************/
int exportfilter_init(void)
{
   int i;

/* Check to see if required config-file commands were processed
 **************************************************************/
   if( nSCN == 0 ) {
      logit( "e", "scnfilter_init: No <Send_scn> commands in config file;"
                  " no data will be exported\n" );
      return( -1 );
   }

/* Look up message types of we can deal with
   *****************************************/
   if ( GetType( "TYPE_TRACEBUF", &TypeTraceBuf ) != 0 ) {
      logit( "e",
             "scnfilter_init: Invalid message type <TYPE_TRACEBUF>\n" );
      return( -1 );
   }
   if ( GetType( "TYPE_PICK2K", &TypePick2K ) != 0 ) {
      logit( "e",
             "scnfilter_init: Invalid message type <TYPE_PICK2K>\n" );
      return( -1 );
   }
   if ( GetType( "TYPE_CODA2K", &TypeCoda2K ) != 0 ) {
      logit( "e",
             "scnfilter_init: Invalid message type <TYPE_CODA2K>\n" );
      return( -1 );
   }
                    
/* Log list of SCN's that we're handling
 ***************************************/
   logit("o", "scnfilter_init: configured to ship %d channels:\n", nSCN );
   for( i=0; i<nSCN; i++ ) {
      logit("o","scnfilter_init: channel[%d]: %s %s %s\n",
             i, Send[i].sta, Send[i].chan, Send[i].net );
   }

   FilterInit = 1;
   return( 0 );
}


/**********************************************************
 * exportfilter() looks at the candidate message.         *
 *          returns: 1 if s-c-n is in "send" list         *
 *                   0 otherwise                          *
 **********************************************************/
int exportfilter( char  *inmsg,  long inlen,   unsigned char  intype, 
                  char **outmsg, long *outlen, unsigned char *outtype )
{
   TRACE_HEADER *thd;
   int i;
   char sta[STATION_LEN+1];
   char chan[CHAN_LEN+1];
   char net[NETWORK_LEN+1];

   if( !FilterInit ) exportfilter_init();

/* Read S-C-N of a TYPE_TRACEBUF message
 ***************************************/
   if( intype == TypeTraceBuf )
   {
       thd = (TRACE_HEADER *)inmsg;
       copytrim( sta,  thd->sta,  STATION_LEN );
       copytrim( chan, thd->chan, CHAN_LEN    );
       copytrim( net,  thd->net,  NETWORK_LEN );
   }

/* Read S-C-N of a TYPE_PICK2K, TYPE_CODA2K message
 **************************************************/
   else if( intype == TypePick2K  ||
            intype == TypeCoda2K )
   {
       copytrim( sta,  &inmsg[15], 5 );
       copytrim( net,  &inmsg[20], 2 );
       copytrim( chan, &inmsg[22], 3 );
   }

/* Or else we don't know how to read this type of message
 ********************************************************/
   else 
   {
      return( 0 );
   }

/* Look for the message's S-C-N in the Send-list
 ***********************************************/
   for( i=0; i<nSCN; i++ )
   {
      if( Send[i].sta[0] != Wild   &&  
          strcmp(sta,  Send[i].sta)  != 0 ) continue;

      if( Send[i].chan[0] != Wild  &&  
          strcmp(chan, Send[i].chan) != 0 ) continue;

      if( Send[i].net[0] != Wild   &&  
          strcmp(net,  Send[i].net)  != 0 ) continue;

     *outmsg  = inmsg;
     *outlen  = inlen;
     *outtype = intype;

      /*printf("scnfilter: accepting msgtype:%d from %s %s %s\n",
              intype, sta, chan, net);*/ /*DEBUG*/

      return( 1 );
   }
   return( 0 );
}

/**********************************************************
 * exportfilter_shutdown()  frees allocated memory and    *
 *         does any other cleanup stuff                   *
 **********************************************************/
void exportfilter_shutdown(void)
{
   free( Send );
   return;
}

/**********************************************************
 * copytrim()  copies n bytes from one string to another, *
 *   trimming off any leading and trailing blank spaces   *
 **********************************************************/
char *copytrim( char *str2, char *str1, int n )
{
   int i, len;

   /*printf( "copy %3d bytes of str1: \"%s\"\n", n, str1 );*/ /*DEBUG*/

/* find first non-blank char in str1 (trim off leading blanks) 
 *************************************************************/
   for( i=0; i<n; i++ ) if( str1[i] != ' ' ) break;

/* copy the remaining number of bytes to str2
 ********************************************/
   len = n-i;
   strncpy( str2, str1+i, len );
   str2[len] = '\0';
   /*printf( "  leading-trimmed str2: \"%s\"\n", str2 );*/ /*DEBUG*/

/* find last non-blank char in str2 (trim off trailing blanks) 
 *************************************************************/
   for( i=len-1; i>=0; i-- ) if( str2[i] != ' ' ) break;
   str2[i+1] = '\0';
   /*printf( " trailing-trimmed str2: \"%s\"\n\n", str2 );*/ /*DEBUG*/

   return( str2 );
}
