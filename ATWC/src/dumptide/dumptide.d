#  Dumptide configuration file
#
#  Writes to disk messages from a given list of station/channel/network codes
#  Knows how to decipher TYPE_TRACEBUF
#
 MyModuleId     MOD_DUMPTIDE        # module id for this program
 RingName       WAVE_RING           # transport ring to use for input/output
 HeartBeatInt   30                  # EW internal heartbeat interval (sec)
 LogFile        1                   # If 0, don't write logfile
#
# Logos of messages to write to disk
#              Installation       Module       Message Type
 GetMsgLogo    INST_WILDCARD      MOD_WILDCARD     TYPE_TRACEBUF 
# Maximum Message Size and RingSize
 MaxMsgSize     4096
 RingSize         50 
# Path to send data
 DataPath       u:\tidegagedata\
# File with control information for sea level stations 
 TideGageFile   r:\nostide\TideStation.dat
# Interval between data points (average over this many points)
 OutputInterval   15
#
# List of station/channel/network codes to write to disk
#
Send_scn  KPA W PT kalaC 0.334942
Send_scn  HIL W PT hiloC 0.334942
Send_scn  KPO W PT kapoC 0.334942
Send_scn  HPO W PT honuC 0.251207
Send_scn  HKH W PT honkC 0.334942
Send_scn  MHA W PT mahuC 0.167471
Send_scn  LPH W PT laupC 0.334942
