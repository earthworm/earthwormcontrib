/****************************************************************************
     dumptide.c - 3/15/00 - Whitmore at WC/ATWC
	 
 This is mostly copied from export.  It is used to grab certain scn data off
 of a ring and dump them to disk in a simple format (ht / time) for use
 by WC/ATWC'c tide processing programs.

 12/2004: Added second data file output in new "approved" format.
 
 2/2003: Changed output from selecting 1 point every OutputInterval points
         to averaging over OutputInterval points.
****************************************************************************/	 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>
#include <errno.h>
#include <signal.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <trace_buf.h>
#include <swap.h>
#include <imp_exp_gen.h>
#include <mem_circ_queue.h> 
#include <socket_ew.h>
#include "\earthworm\atwc\src\libsrc\earlybirdlib.h"
#include "\earthworm\v6.3\src\data_sources\naqs2ew\naqsser.h"
#include "exportfilter.h"

/* Functions in this source file 
 *******************************/
void  dumptide_config  ( char * );
void  dumptide_lookup  ( void );
void  dumptide_status  ( unsigned char, short, char * );
void  dumptide_free    ( void );
int   WriteTGNewFormat ( char *, TGFILE_HEADER *, long, double, int );
int   FillHeaderStructure( char *, char *, TGFILE_HEADER * );

/* Thread things
 ***************/
#define THREAD_STACK 8192
static unsigned tidWriter;           /* WriteThread thread id */
static unsigned tidStacker;          /* Thread moving messages from transport */
                                     /*   to queue */

#define MSGSTK_OFF    0              /* MessageStacker has not been started   */
#define MSGSTK_ALIVE  1              /* MessageStacker alive and well         */
#define MSGSTK_ERR   -1              /* MessageStacker encountered error quit */
volatile int MessageStackerStatus = MSGSTK_OFF;

QUEUE OutQueue; 		     /* from queue.h, queue.c; sets up linked    */
                                     /*    list via malloc and free           */
thr_ret WriteThread( void * );
thr_ret MessageStacker( void * );    /* used to pass messages between main thread */
                                     /*   and WriteThread thread */

/* Message Buffers to be allocated
 *********************************/
static char *Rawmsg = NULL;          /* "raw" retrieved msg for main thread   */ 
static char *SSmsg = NULL;           /* WriteThread's incoming message buffer */
static MSG_LOGO BinLogo;             /* logo of message to re-send */
static char *MSrawmsg = NULL;        /* MessageStacker's "raw" retrieved message */
static char *MSfilteredmsg = NULL;   /* MessageStacker's "filtered" message to*/
                                     /*    be sent to client                  */

/* Timers
   ******/
time_t now;		    /* current time, used for timing heartbeats */
time_t MyLastBeat;  /* time of last local (into Earthworm) hearbeat */

static  SHM_INFO  Region;       /* shared memory region to use for i/o    */

#define   MAXLOGO   10
MSG_LOGO  GetLogo[MAXLOGO];     /* array for requesting module,type,instid */
short 	  nLogo;

char *Argv0;        /* pointer to executable name */ 
char *Argv1;        /* pointer to executable name */ 
pid_t MyPid;        /* Our own pid, sent with heartbeat for restart purposes */
   
/* Things to read or derive from configuration file
 **************************************************/
static char    RingName[20];        /* name of transport ring for i/o    */
static char    MyModName[20];       /* speak as this module name/id      */
static int     LogSwitch;           /* 0 if no logfile should be written */
static int     HeartBeatInt;        /* seconds between heartbeats        */
static long    MaxMsgSize;          /* max size for input/output msgs    */
static int     RingSize;	    /* max messages in output circular buffer */
static int     OutputInterval;      /* number samples to avg. between writes */
static char    szPath[64];      /* Path to send data files to */
char    TideGageFile[64];/* File with sea level stn control info */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o      */
static unsigned char InstId;        /* local installation id              */
static unsigned char MyModId;       /* Module Id for this program         */
static unsigned char TypeHeartBeat; 
static unsigned char TypeError;

/* Error messages used by dumptide
 *********************************/
#define  ERR_MISSMSG       0   /* message missed in transport ring        */
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer      */
#define  ERR_NOTRACK       2   /* msg retreived; tracking limit exceeded  */
#define  ERR_QUEUE         3   /* error queueing message for sending      */
static char  errText[256];     /* string for log/error messages           */

/* Also declared elsewhere
 *************************/
int nSCN        = 0;           /* # of scn's we're configured to ship     */
SCNstruct *Send = NULL;        /* list of s-c-n's to accept               */
TGFILE_HEADER *TGH  = NULL;    /* Output file data header info            */

main( int argc, char **argv )
{
   int           res;
   long          recsize;	/* size of retrieved message             */
   MSG_LOGO      reclogo;	/* logo of retrieved message             */
   size_t        size;
   
   /* Check command line arguments 
   ******************************/
   Argv0 = argv[0];
   Argv1 = argv[1];
   if ( argc != 2 )
   {
      fprintf( stderr, "Usage: %s <configfile>\n", Argv0 );
      return( 0 );
   }
   
   /* Create memory for header information */   
   size = MAX_SCN_DUMP * sizeof( TGFILE_HEADER );
   TGH  = (TGFILE_HEADER *) malloc( size );
   if ( TGH == NULL )
   {
      logit( "e", "Error allocating %d bytes"
             " for requested file header; exiting!\n", size );
      dumptide_free();
      return( -1 );
   }
	   
   /* Read the configuration file(s)
   ********************************/
   dumptide_config( Argv1 );

   /* Look up important info from earthworm.h tables
   *************************************************/
   dumptide_lookup();

   /* Initialize name of log-file & open it 
   ****************************************/
   logit_init( Argv1, (short) MyModId, 512, LogSwitch );

   logit( "et" , "%s(%s): Read command file <%s>\n", 
           Argv0, MyModName, Argv1 );

   /* Get our own Pid for restart purposes
   ***************************************/
   MyPid = getpid();
   if(MyPid == -1)
   {
      logit("e", "%s(%s): Cannot get pid; exiting!\n", Argv0, MyModName);
      return(0);
   }

   /* Initialize export filter
   ***************************/
   if( exportfilter_init() != 0 )
   {
      logit("e", "%s(%s): Error in exportfilter_init(); exiting!\n",
            Argv0, MyModName );    
      return(-1);
   }      
  
   /* Allocate space for input/output messages for all threads
   ***********************************************************/
   /* Buffer for main thread: */
   if ( (Rawmsg = (char *) malloc(MaxMsgSize) ) ==  NULL ) 
   {
      logit( "e", "%s(%s): error allocating Rawmsg; exiting!\n", 
             Argv0, MyModName );
      dumptide_free();
      return( -1 );
   }

   /* Buffer for WriteThread thread: */
   if ( ( SSmsg = (char *) malloc(MaxMsgSize) ) ==  NULL ) 
   {
      logit( "e", "%s(%s): error allocating SSmsg; exiting!\n",
              Argv0, MyModName );
      dumptide_free();
      return( -1 );
   }

   /* Buffers for the MessageStacker thread: */
   if ( ( MSrawmsg = (char *) malloc(MaxMsgSize) ) ==  NULL ) 
   {
      logit( "e", "%s(%s): error allocating MSrawmsg; exiting!\n", 
             Argv0, MyModName );
      dumptide_free();
      return( -1 );
   }
   if ( ( MSfilteredmsg = (char *) malloc(MaxMsgSize) ) ==  NULL ) 
   {
      logit( "e", "%s(%s): error allocating MSfilteredmsg; exiting!\n", 
              Argv0, MyModName );
      dumptide_free();
      return( -1 );
   }

   /* Create a Mutex to control access to queue
   ********************************************/
   CreateMutex_ew();

   /* Initialize the message queue
   *******************************/
   initqueue( &OutQueue, (unsigned long)RingSize,(unsigned long)MaxMsgSize );
	   	
   /* Attach to Input/Output shared memory ring 
   ********************************************/
   tport_attach( &Region, RingKey );

   /* step over all messages from transport ring
   *********************************************/
   /* As Lynn pointed out: if we're restarted by startstop after hanging, 
      we should throw away any of our messages in the transport ring. 
      Else we could end up re-writing previously written data */
   do
   {
     res = tport_getmsg( &Region, GetLogo, nLogo, 
                         &reclogo, &recsize, Rawmsg, MaxMsgSize-1 );
   } while (res !=GET_NONE);

   /* One heartbeat to announce ourselves to statmgr
   ************************************************/
   dumptide_status( TypeHeartBeat, 0, "" );
   time(&MyLastBeat);

   /* Start the message stacking thread if it isn't already running.
    ****************************************************************/
   if (MessageStackerStatus != MSGSTK_ALIVE )
   {
     if ( StartThread(  MessageStacker, (unsigned)THREAD_STACK, &tidStacker ) == -1 )
     {
       logit( "e", 
              "%s(%s): Error starting  MessageStacker thread; exiting!\n",
	      Argv0, MyModName );
       tport_detach( &Region );
       return( -1 );
     }
     MessageStackerStatus = MSGSTK_ALIVE;
   }

   /* Start the disk writing thread
   ********************************/
   if ( StartThread(  WriteThread, (unsigned)THREAD_STACK, &tidWriter ) == -1 )
   {
      logit( "e", "%s(%s): Error starting WriteThread thread; exiting!\n",
              Argv0, MyModName );
      tport_detach( &Region );
      return( -1 );
   }

   /* Start main service loop 
   **************************/
   while( tport_getflag( &Region ) != TERMINATE )
   {
     /* Beat the heart into the transport ring
      ****************************************/
      time(&now);
      if (difftime(now,MyLastBeat) > (double)HeartBeatInt ) 
      {
         dumptide_status( TypeHeartBeat, 0, "" );
	     time(&MyLastBeat);
      }

      /* take a brief nap; added 970624:ldd 
       ************************************/
      sleep_ew(500);
   } /*end while of monitoring loop */

   /* Shut it down
   ***************/
   tport_detach( &Region );
   exportfilter_shutdown();
   dumptide_free();
   logit("t", "%s(%s): termination requested; exiting!\n", 
          Argv0, MyModName );
   return( 0 );	
}
/* *******************   end of main   *******************************/

/**************************Disk Writing Thread ***********************
*          Pull a messsage from the queue, and write to the disk     *
*          file. 
**********************************************************************/
thr_ret WriteThread( void *dummy )
{
   double   dIncrement;     /* Time (seconds) between samples */
   double   dStartTime;     /* Message start time in Modified Julian Seconds */
   FILE     *hFile;         /* Data File handle */
   int iCnt[MAX_SCN_DUMP];  /* Summation counter for averaging */
   int      iIndex;         /* Index of this SCN */
   int      iOutput;        /* Output sample rate */
   long     lData;          /* Raw data value to write to disk */
   long     lStartTime;     /* Integer part of message start time (MJS) */
   long lSum[MAX_SCN_DUMP]; /* Summation variable for averaging height */
   struct tm* packetTime;   /* unix integer form time structure */
   char     szFileName[64]; /* Path/file to output data to */
   char     szTemp[10];     /* temporary character string */
   long     msgSize;  
   int      ret;
   int      datLen=0;	/* bytes per data point */
   int      i;		/* index over data points in message */
   char*    next;	/* pointer to next data value (either 2 or 4 bytes long) */

   for ( i=0; i<MAX_SCN_DUMP; i++ );
   {
      lSum[i] = 0;
      iCnt[i] = 0;
   }
   
   while (1)   /* main loop */
   {
     /* Get message from queue
      *************************/
      RequestMutex();
      ret=dequeue( &OutQueue, SSmsg, &msgSize, &BinLogo);
      ReleaseMutex_ew();
      if(ret < 0 )
      { /* -1 means empty queue */
         sleep_ew(500); /* wait a bit (changed from 1000 to 500 on 970624:ldd) */
         continue;
      }

     /* Get index in Send array of this trace
      ***************************************/	 
      for ( i=0; i<nSCN; i++ )
      {
         if ( !strcmp( ((TRACE_HEADER*)SSmsg)->sta,  Send[i].sta ) &&
              !strcmp( ((TRACE_HEADER*)SSmsg)->chan, Send[i].chan ) &&
              !strcmp( ((TRACE_HEADER*)SSmsg)->net,  Send[i].net ) )
         {
            iIndex = i;
            break;
         }
      }
      if ( iIndex == nSCN )
      {
         logit( "", "No match in array for %s %s %s\n",
               ((TRACE_HEADER*)SSmsg)->sta, ((TRACE_HEADER*)SSmsg)->chan,
               ((TRACE_HEADER*)SSmsg)->net );
         continue;
      }
     
     /* Adjust start time of message to Modified Julian seconds
      *********************************************************/
      dStartTime = ((TRACE_HEADER*)SSmsg)->starttime + 3506630400.;
      dIncrement = 1. / ((TRACE_HEADER*)SSmsg)->samprate;
	 
     /* Set the output sample rate
      ****************************/
      iOutput = OutputInterval;
	 
     /* Open the data file
      ********************/
      strcpy (szFileName, szPath);  /* First, create file name */
      strcat (szFileName, "T");
      strcat (szFileName, ((TRACE_HEADER*)SSmsg)->sta);
      lStartTime = (long) (floor (((TRACE_HEADER*)SSmsg)->starttime));
      packetTime = TWCgmtime (lStartTime);
      itoaX ((int) packetTime->tm_year+1900, szTemp);
      strcat (szFileName, szTemp);
      strcat (szFileName, ".");
      itoaX ((int) packetTime->tm_yday+1, szTemp);
      PadZeroes (3, szTemp);
      strcat (szFileName, szTemp);
      next = SSmsg + sizeof (TRACE_HEADER);      /* point to start of data */
      if( strcmp( ((TRACE_HEADER*)SSmsg)->datatype, "s4")==0 ) datLen=4;
      if( strcmp( ((TRACE_HEADER*)SSmsg)->datatype, "s2")==0 ) datLen=2;
      if( strcmp( ((TRACE_HEADER*)SSmsg)->datatype, "i4")==0 ) datLen=4;
      if( strcmp( ((TRACE_HEADER*)SSmsg)->datatype, "i2")==0 ) datLen=2;	 

      if ((hFile = fopen (szFileName, "a")) != NULL) /* Dump data to file */
      {
         for (i=0; i<((TRACE_HEADER*)SSmsg)->nsamp; i++)
         {
            iCnt[iIndex] += 1;
            if (datLen == 2) lData = (long) *((short*)next);
            if (datLen == 4) lData = (long) *(( long*)next);
            lSum[iIndex] += lData;
            if ( (iCnt[iIndex]%iOutput) == 0 )
            {
               iCnt[iIndex] = 0;
               fprintf (hFile, "%ld %lf\n", (long) ((double) lSum[iIndex]/
                       (double) iOutput + 0.5),
                        dStartTime+(dIncrement*(double) (i)));
               WriteTGNewFormat( szPath, &TGH[iIndex], (long)
                (lStartTime+(dIncrement*(double) i)),
                ((double) lSum[iIndex]/(double) iOutput), iIndex );
               lSum[iIndex] = 0;
            }
            next = next + datLen;
         }
         fclose (hFile);
      }		 
   }   /* End of main loop */   
} 

/********************** Message Stacking Thread *******************
 *           Move messages from transport to memory queue         *
 ******************************************************************/
thr_ret MessageStacker( void *dummy )
{
   long          recsize;	/* size of retrieved message             */
   MSG_LOGO      reclogo;       /* logo of retrieved message             */
   long 	 filteredSize;  /* size of message after user-filtering  */
   unsigned char filteredType;  /* type of message after filtering       */
   int		 res;
   int 		 ret;
   int           NumOfTimesQueueLapped= 0; /* number of messages lost due to 
                                             queue lap */

   /* Tell the main thread we're ok
   ********************************/
   MessageStackerStatus = MSGSTK_ALIVE;

   /* Start main service loop 
   **************************/
   while( 1 )
   {
      /* Get a message from transport ring
      ************************************/
      res = tport_getmsg( &Region, GetLogo, nLogo, 
                          &reclogo, &recsize, MSrawmsg, MaxMsgSize-1 );

      /* Wait if no messages for us 
       ****************************/
      if( res == GET_NONE ) {sleep_ew(100); continue;} 

      /* Check return code; report errors 
      ***********************************/
      if( res != GET_OK )
      {
         if( res==GET_TOOBIG ) 
         {
            sprintf( errText, "msg[%ld] i%d m%d t%d too long for target",
                            recsize, (int) reclogo.instid,
			    (int) reclogo.mod, (int)reclogo.type );
            dumptide_status( TypeError, ERR_TOOBIG, errText );
            continue;
         }
         else if( res==GET_MISS ) 
         {
            sprintf( errText, "missed msg(s) i%d m%d t%d in %s",(int) reclogo.instid,
                    (int) reclogo.mod, (int)reclogo.type, RingName );
            dumptide_status( TypeError, ERR_MISSMSG, errText );
         }
         else if( res==GET_NOTRACK ) 
         {
            sprintf( errText, "no tracking for logo i%d m%d t%d in %s",
                     (int) reclogo.instid, (int) reclogo.mod, (int)reclogo.type,
                     RingName );
            dumptide_status( TypeError, ERR_NOTRACK, errText );
         }
      }
                     
      /* Process retrieved msg (res==GET_OK,GET_MISS,GET_NOTRACK) 
      ***********************************************************/
      MSrawmsg[recsize] = '\0';
      /* pass it through the filter routine: this may reformat, 
         or reject as it chooses. */
      if ( exportfilter( MSrawmsg, recsize, reclogo.type, &MSfilteredmsg, 
                        &filteredSize, &filteredType ) == 0 )  continue;
      reclogo.type = filteredType;  /* note the new message type */
	  
      WaveMsgMakeLocal( (TRACE_HEADER*)MSfilteredmsg ); /* convert the whole message to local byte order */

      /* put it into the 'to be shipped' queue */
      /* the thread WriteThread is in the biz of de-queueng and sending */
      RequestMutex();
      ret=enqueue( &OutQueue, MSfilteredmsg, filteredSize, reclogo ); 
      ReleaseMutex_ew();

      if ( ret!= 0 )
      {       
         if (ret==-2)  /* Serious: quit */
         {    /* Currently, eneueue() in mem_circ_queue.c never returns this error. */
	    sprintf(errText,"internal queue error. Terminating.");
            dumptide_status( TypeError, ERR_QUEUE, errText );
	    goto error;
         }
         if (ret==-1) 
         {
            sprintf(errText,"queue cannot allocate memory. Lost message.");
            dumptide_status( TypeError, ERR_QUEUE, errText );
            continue;
         }
         if (ret==-3)  /* Log only while client's connected */
         {
         /* Queue is lapped too often to be logged to screen.  
          * Log circular queue laps to logfile.  
          * Maybe queue laps should not be logged at all.
          */
            NumOfTimesQueueLapped++;
            if (!(NumOfTimesQueueLapped % 5))
            {
               logit("t", 
                     "%s(%s): Circular queue lapped 5 times. Messages lost.\n",
                      Argv0, MyModName);
               if (!(NumOfTimesQueueLapped % 100))
               {
                  logit( "et", 
                        "%s(%s): Circular queue lapped 100 times. Messages lost.\n",
                         Argv0, MyModName);
               }
            }
            continue; 
         }
      }
   } /* end of while */

   /* we're quitting 
   *****************/
error:
   MessageStackerStatus = MSGSTK_ERR; /* file a complaint to the main thread */
   KillSelfThread(); /* main thread will restart us */
}


/*****************************************************************************
 *  dumptide_config() processes command file(s) using kom.c functions;         *
 *                    exits if any errors are encountered.	             *
 *****************************************************************************/
void dumptide_config( char *configfile )
{
   int      ncommand;     /* # of required commands you expect to process   */ 
   char     init[10];     /* init flags, one byte for each required command */
   int      nmiss;        /* number of required commands that were missed   */
   char    *com;
   char     processor[15];
   int      nfiles;
   int      success;
   int      i;	
   char*    str;

/* Set to zero one init flag for each required command 
 *****************************************************/   
   ncommand = 10;
   for( i=0; i<ncommand; i++ )  init[i] = 0;
   nLogo = 0;

/* Open the main configuration file 
 **********************************/
   nfiles = k_open( configfile ); 
   if ( nfiles == 0 ) {
	fprintf( stderr,
                "%s: Error opening command file <%s>; exiting!\n", 
                Argv0, configfile );
	exit( -1 );
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {  
	    com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr, 
                          "%s: Error opening command file <%s>; exiting!\n",
                           Argv0, &com[1] );
                  exit( -1 );
               }
               continue;
            }
            strcpy( processor, "dumptide_config" );

        /* Process anything else as a command 
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("HeartBeatInt") ) {
                HeartBeatInt = k_int();
                init[3] = 1;
            }


         /* Enter installation & module & message types to get
          ****************************************************/
  /*4*/     else if( k_its("GetMsgLogo") ) {
                if ( nLogo >= MAXLOGO ) {
                    fprintf( stderr, 
                            "%s: Too many <GetMsgLogo> commands in <%s>", 
                             Argv0, configfile );
                    fprintf( stderr, "; max=%d; exiting!\n", (int) MAXLOGO );
                    exit( -1 );
                }
                if( ( str=k_str() ) ) {
                   if( GetInst( str, &GetLogo[nLogo].instid ) != 0 ) {
                       fprintf( stderr, 
                               "%s: Invalid installation name <%s>", Argv0, str ); 
                       fprintf( stderr, " in <GetMsgLogo> cmd; exiting!\n" );
                       exit( -1 );
                   }
                }
                if( ( str=k_str() ) ) {
                   if( GetModId( str, &GetLogo[nLogo].mod ) != 0 ) {
                       fprintf( stderr, 
                               "%s: Invalid module name <%s>", Argv0, str ); 
                       fprintf( stderr, " in <GetMsgLogo> cmd; exiting!\n" );
                       exit( -1 );
                   }
                }
                if( ( str=k_str() ) ) {
                   if( GetType( str, &GetLogo[nLogo].type ) != 0 ) {
                       fprintf( stderr, 
                               "%s: Invalid msgtype <%s>", Argv0, str ); 
                       fprintf( stderr, " in <GetMsgLogo> cmd; exiting!\n" );
                       exit( -1 );
                   }
                }
                nLogo++;
                init[4] = 1;
            /*    printf("GetLogo[%d] inst:%d module:%d type:%d\n",
		        nLogo, (int) GetLogo[nLogo].instid,
                               (int) GetLogo[nLogo].mod,
                               (int) GetLogo[nLogo].type ); */  /*DEBUG*/
            }

         /* Maximum message size
          **********************/ 
  /*5*/     else if( k_its("MaxMsgSize") ) {
                MaxMsgSize = k_long();
                init[5] = 1;
            }

         /* Ring Size
          ***********/ 
  /*6*/     else if( k_its("RingSize") ) {
                RingSize = k_int();
                init[6] = 1;
            }
			
         /* Path to load data files
		  *************************/
  /*7*/     else if( k_its("DataPath") ) {
                str = k_str();
                if(str) strcpy( szPath, str );
                init[7] = 1;
            }

         /* Output Interval
          *****************/ 
  /*8*/     else if( k_its("OutputInterval") ) {
                OutputInterval = k_int();
                init[8] = 1;
            }

         /* Tide gage control data file name
          **********************************/			
  /*9*/     else if( k_its("TideGageFile") ) {
                str = k_str();
                if(str) strcpy( TideGageFile, str );
                init[9] = 1;
            }

         /* Pass it off to the filter's config processor
          **********************************************/		
            else if( exportfilter_com() ) strcpy( processor, "exportfilter_com" );

         /* Unknown command
          *****************/ 
	    else {
                fprintf( stderr, "%s: <%s> Unknown command in <%s>.\n", 
                         Argv0, com, configfile );
                continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr, 
                       "%s: Bad <%s> command for %s() in <%s>; exiting!\n",
                        Argv0, com, processor, configfile );
               exit( -1 );
            }
	}
	nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "%s: ERROR, no ", Argv0 );
       if ( !init[0] )  fprintf( stderr, "<LogFile> "      );
       if ( !init[1] )  fprintf( stderr, "<MyModuleId> "   );
       if ( !init[2] )  fprintf( stderr, "<RingName> "     );
       if ( !init[3] )  fprintf( stderr, "<HeartBeatInt> " );
       if ( !init[4] )  fprintf( stderr, "<GetMsgLogo> "   );
       if ( !init[5] )  fprintf( stderr, "<MaxMsgSize> "   );
       if ( !init[6] )  fprintf( stderr, "<RingSize> "   );
       if ( !init[7] )  fprintf( stderr, "<DataPath> "   );
       if ( !init[8] )  fprintf( stderr, "<OutputInterval> "   );
       if ( !init[9] )  fprintf( stderr, "<TideGageFile> "   );
       fprintf( stderr, "command(s) in <%s>; exiting!\n", configfile );
       exit( -1 );
   }
   return;
}

/****************************************************************************
 *  dumptide_lookup( )   Look up important info from earthworm.h tables       *
 ****************************************************************************/
void dumptide_lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 )
   {
      fprintf( stderr, "%s:  Invalid ring name <%s>; exiting!\n", 
               Argv0, RingName);
      exit( -1 );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
      fprintf( stderr, 
              "%s: error getting local installation id; exiting!\n",
               Argv0 );
      exit( -1 );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid module name <%s>; exiting!\n", 
               Argv0, MyModName );
      exit( -1 );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_HEARTBEAT>; exiting!\n", Argv0 );
      exit( -1 );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
      fprintf( stderr, 
              "%s: Invalid message type <TYPE_ERROR>; exiting!\n", Argv0 );
      exit( -1 );
   }
   return;
} 

/***************************************************************************
 * dumptide_status() builds a heartbeat or error message & puts it into      *
 *                 shared memory.  Writes errors to log file & screen.     *
 ***************************************************************************/
void dumptide_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char	       msg[256];
   long	       size;
   long        t;
 
/* Build the message
 *******************/ 
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
	sprintf( msg, "%ld %ld\n\0", t, MyPid);
   else if( type == TypeError )
   {
	sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);

	logit( "et", "%s(%s): %s\n", Argv0, MyModName, note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */ 	

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","%s(%s):  Error sending heartbeat.\n", 
                  Argv0, MyModName );
	}
	else if( type == TypeError ) {
           logit("et", "%s(%s):  Error sending error:%d.\n", 
                  Argv0, MyModName, ierr );
	}
   }

   return;
}

/***************************************************************************
 * dumptide_free()  free all previously allocated memory                     *
 ***************************************************************************/
void dumptide_free( void )
{
   free (TGH);                  /* Header information                      */ 
   free (Rawmsg);               /* "raw" retrieved msg for main thread     */ 
   free (SSmsg);                /* WriteThread's incoming message buffer   */
   free (MSrawmsg);             /* MessageStacker's "raw" retrieved message*/   
   free (MSfilteredmsg);        /* MessageStacker's "filtered" message to  */
                                /*    be sent to client                    */
   return;
}

 /******************************************************************
  *                   WriteTGNewFormat()                           *
  *                                                                *
  *  This function writes data to a tide gage data file in the new *
  *  (10/2004) format.  If necessary, the file is created first.   *
  *                                                                *
  *  Arguments:                                                    *
  *   pszPath          Path to output data files                   *
  *   pTGH             Structure with TG header information        *
  *   lTime            Sample time (1/1/70 seconds)                *
  *   dSamp            Sea level height in cm                      *
  *   iSendIndex       Index for this sample in Send array         *
  *                                                                *
  *  Returns:                                                      *
  *   not used                                                     *
  *                                                                *
  ******************************************************************/
int WriteTGNewFormat( char *pszPath, TGFILE_HEADER *pTGH, long lTime,
                      double dSamp, int iSendIndex )
{
   FILE *hFile;              /* File pointer */
   struct tm* packetTime;    /* Time structure */
   char  szFileName[64];     /* Output file name with path */
   char  szTime[32];         /* Time character string */
   char  szYear[5], szMon[3], szDay[3], szHour[3], szMin[3], szSec[3],szYDay[4];
	  
/* Create file name and open the data file (new format) */
   packetTime = TWCgmtime( lTime );
   itoa( (int) packetTime->tm_year+1900, szYear, 10 );
   PadZeroes( 4, szYear );
   itoa( (int) packetTime->tm_yday+1, szYDay, 10 );
   PadZeroes( 3, szYDay );
   itoa( (int) packetTime->tm_mon+1, szMon, 10 );
   PadZeroes( 2, szMon );
   itoa( (int) packetTime->tm_mday, szDay, 10 );
   PadZeroes( 2, szDay );
   itoa( (int) packetTime->tm_hour, szHour, 10 );
   PadZeroes( 2, szHour );
   itoa( (int) packetTime->tm_min, szMin, 10 );
   PadZeroes( 2, szMin );
   itoa( (int) packetTime->tm_sec, szSec, 10 );
   PadZeroes( 2, szSec );
   strcpy( szFileName, pszPath );        
   strcat( szFileName, pTGH->szSiteAbbrev );
   strcat( szFileName, szYear );
   strcat( szFileName, "." );
   strcat( szFileName, szYDay );
   if ( (hFile = fopen( szFileName, "r" )) != NULL )
   {             /* File exists so just append data */
      fclose( hFile );
      if ( (hFile = fopen( szFileName, "a" )) != NULL )
      {  /* Put data in proper time format */
         strcpy( szTime, szYear );
         strcat( szTime, szMon );
         strcat( szTime, szDay );
         strcat( szTime, szHour );
         strcat( szTime, szMin );
         strcat( szTime, szSec );
/* Output sample to file */         
         fprintf( hFile, "%ld %.1lf %s\n", lTime, dSamp*Send[iSendIndex].dGain,
                  szTime );
         fclose( hFile );
      }
      else        /* File open failed seconds time */
      {
         logit( "et", "Append to file (%s) failed\n", szFileName );
      }
   }
   else          /* File doesn't exist so create with header */
   {
      logit( "t", "Create new file - %s\n", szFileName );
/* Put data in proper time format and create file name */
      strcpy( szTime, szYear );
      strcat( szTime, szMon );
      strcat( szTime, szDay );
      if ( (hFile = fopen( szFileName, "w" )) != NULL )
      {  /* Log the header information first */
         logit( "t", "file %s created\n", szFileName );
         fprintf( hFile, "%s %s %s %s %s %s %s\n", pTGH->szSiteName, 
          pTGH->szPlatformID, pTGH->szSiteOperator, pTGH->szTransmissionType, 
          pTGH->szSiteLat, pTGH->szSiteLon, szTime );
         fprintf( hFile, "%s %s %s %s %s %s %s %s\n", pTGH->szSensorType, 
          pTGH->szWaterLevelUnits, pTGH->szTimeZone, pTGH->szSampleRate, 
          pTGH->szSampleRateUnits, pTGH->szReferenceDatum,
          pTGH->szRecordingAgency, pTGH->szProcessingInfo );
         fprintf( hFile, "%s\n", pTGH->szComments ); 
         fprintf( hFile, "%s\n", pTGH->szDataHeader ); 
         strcat( szTime, szHour );       /* Finish Time */
         strcat( szTime, szMin );
         strcat( szTime, szSec );
/* Output sample to file */         
         fprintf( hFile, "%ld %.1lf %s\n", lTime, dSamp*Send[iSendIndex].dGain,
                  szTime );
         fclose( hFile );
      }
      else          /* File could not be opened for write */
      {
         logit( "et", "Create new file (%s) failed\n", szFileName );
      }
   }     
   return( 0 );
}

      /******************************************************************
       *                 FillHeaderStructure()                          *
       *                                                                *
       *  This function fills in header information for each station    *
       *  given data from a tide station data file.                     *
       *                                                                *
       *  Arguments:                                                    *
       *   pszFileName      Name of file containing station information *
       *   pszStnAbbrev     Station for which to get information        *
       *   pTGH             Structure to hold header information        *
       *                                                                *
       *  Returns:                                                      *
       *   int              1 -> Data filled OK                         *
       *                    0 -> Problem                                *
       *                                                                *
       ******************************************************************/
	   
int FillHeaderStructure( char *pszFileName, char *pszStnAbbrev,
                         TGFILE_HEADER *pTGH )
{
   FILE    *hFile;
   TGFILE_HEADER TG;

   if ( (hFile = fopen( pszFileName, "r" )) == NULL )
   {
      fprintf( stderr, "Tide station file (%s) not opened\n", pszFileName );
      return( 0 );
   }
   else
   {
      while( !feof( hFile ) )
      {
         fscanf( hFile, "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s\n",
          TG.szSiteName, TG.szSiteAbbrev, TG.szPlatformID, TG.szWMOCode,
          TG.szGOESID, TG.szSiteOperator, TG.szSiteOAbbrev, TG.szSiteLat,
          TG.szSiteLon, TG.szWaterLevelUnits, TG.szTransmissionType,
          TG.szSampleRate, TG.szSampleRateUnits, TG.szReferenceDatum,
          TG.szRecordingAgency, TG.szSensorType, TG.szOcean, TG.szComments );
         if ( !strcmp( pszStnAbbrev, TG.szSiteAbbrev ) )
         {
            fprintf( stderr, "Found %s in data file\n", TG.szSiteAbbrev );
            strcpy( pTGH->szSiteAbbrev, TG.szSiteAbbrev );
            strcpy( pTGH->szSiteName, TG.szSiteName );
            strcpy( pTGH->szPlatformID, TG.szPlatformID );
            strcpy( pTGH->szWMOCode, TG.szWMOCode );
            strcpy( pTGH->szGOESID, TG.szGOESID );
            strcpy( pTGH->szSiteOperator, TG.szSiteOperator );
            strcpy( pTGH->szSiteOAbbrev, TG.szSiteOAbbrev );
            strcpy( pTGH->szSiteLat, TG.szSiteLat );
            strcpy( pTGH->szSiteLon, TG.szSiteLon );
            strcpy( pTGH->szSensorType, TG.szSensorType );
            strcpy( pTGH->szWaterLevelUnits, TG.szWaterLevelUnits );
            strcpy( pTGH->szSampleRate, TG.szSampleRate );
            strcpy( pTGH->szSampleRateUnits, TG.szSampleRateUnits );
            strcpy( pTGH->szTransmissionType, TG.szTransmissionType );
            strcpy( pTGH->szReferenceDatum, TG.szReferenceDatum );
            strcpy( pTGH->szRecordingAgency, TG.szRecordingAgency );
            strcpy( pTGH->szOcean, TG.szOcean );
            strcpy( pTGH->szComments, TG.szComments );
            strcpy( pTGH->szTimeZone, "UTC" );
            strcpy( pTGH->szProcessingInfo, "Unfiltered" );
            strcpy( pTGH->szDataHeader, "Data Format: SampleTime(epochal 1/1/1970)  WaterLevel  SampleTime(yyymmddhhmmss)" );
            fclose( hFile );
            return( 1 );
         }
      }   
   }
   fprintf( stderr, "%s not found in %s\n", pszStnAbbrev, pszFileName );
   fclose( hFile );
   return( 0 );
}
