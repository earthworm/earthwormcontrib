/******************************************************************
 *                          File develo.h                         *
 *                                                                *
 *  Include file for develocorder module used at the West Coast/  *
 *  Alaska Tsunami Warning Center.                                *
 ******************************************************************/

#include <trace_buf.h>
#include "\earthworm\atwc\src\libsrc\earlybirdlib.h"

/* Definitions
   ***********/
#define FIRST_MOTION_SAMPS 3 /* Number of samples after trigger which must be in
                                same direction for a 1st motion to be declared*/
#define RESET_INTERVAL 1800000 /* Reset options interval in milliseconds */
#define DUMMY_READ_INT 20000 /* Time increment (millisec) to read dummy file */
#define RECENT_ALARM_TIME 900 /* # seconds late data can be and still run thru
                                 alarm */							   
#define TOO_OLD_DEV   1*60*60 /* If data is this many seconds behind present,
                                 ignore it */
								

/* Menu options
   ************/
#define IDM_HSCALE          110     /* Control panel options */
#define IDM_VSCALE          111
#define IDM_TRACEPERSCREEN  112		
#define IDM_FILTDISPLAY     113
#define IDM_BBDISPLAY       114 
#define IDM_EXIT_MAIN       115
#define IDM_DCOFFSET        116
#define IDM_SHOWCHAN        117
#define IDM_SHOWNET         118
#define IDM_SCALE_8X   	    120     /* Individual station menu options */
#define IDM_SCALE_4X        121
#define IDM_SCALE_2X        122          
#define IDM_SCALE_HALFX     123
#define IDM_SCALE_QUARTERX  124
#define IDM_SCALE_EIGHTHX   125
#define IDM_SCALE_DEFAULT   126
#define IDM_STADISP_ON      127
#define IDM_STADISP_OFF     128
#define IDM_STA_DESC        129
#define IDM_REFRESH         140	    /* Refresh screen */
#define IDM_CLIPIT          150	    /* Clip-it flag */
#define IDM_DISP_HOLD       160     /* Prevent auto-scrolling */
#define IDM_DISPLAYMWP      170		/* Magnitude determination options */
#define IDM_MWPWINDOW       171
#define IDM_ALARM_PAGE      190     /* Alarms sent to pager */
#define IDM_ALARM_SPEAK     191     /* Alarms sent to speaker */
#define IDM_SCREEN        	200     /* Next MAX_SCREENS must be undefined */

/* Entry fields
   ************/
#define EF_NUMSTATODISP     3000    /* For TracesPerScreen dialog */
#define IDD_SCALE4X         3100    /* For Global Scale dialog */
#define IDD_SCALE2X         3102
#define IDD_SCALEHALF       3103
#define IDD_SCALEQUARTER    3104
#define IDD_SCALEEIGHTH     3105
#define IDD_SCALE_DEFAULT   3106
#define EF_MWPTIME          3107    /* MwpTime dialog */
#define EF_MWPSN            3108
#define IDC_STATION         3109    /* Station Description items */
#define IDC_CHANNEL         3110    
#define IDC_NET_ID          3111
#define IDC_OPERATING_AGENCY 3112    
#define IDC_LAT_DESC        3113
#define IDC_LON_DESC        3114    
#define IDC_EL_DESC         3115    
#define IDC_GAIN_DESC       3116
#define IDC_SEIS_TYPE       3117
#define IDC_CORNER_FREQ     3118

#define ID_CHILD_MWP_DISP    200    /* Child window which shows int. disp. */

#define ID_NULL            -1       /* Resource file declaration */

/* User defined window messages
   ****************************/
#define WM_NEW_DATA (WM_USER + 10)
#define WM_NEW_PPICKS (WM_USER + 11)

#define ID_TIMER2            2    /* Reset options timer */   
#define ID_TIMER3            3    /* Dummy file reader */   

typedef struct {
   char StaFile[64];              /* Name of file with SCN info */
   char StaDataFile[64];          /* Station information file */
   char ATPLineupFileBB[64];      /* Optional command when used with ATPlayer */
   long InKey;                    /* Key to ring where waveforms live */
   long PKey;                     /* Key to ring where picks will live */
   long AlarmKey;                 /* Key to ring where alarms will live */
   int  HeartbeatInt;             /* Heartbeat interval in seconds */
   int  Debug;                    /* If 1, print debug messages */
   double HighCutFilter;          /* Bandpass high cut in hz */
   double LowCutFilter;           /* Bandpass low cut in hz */
   unsigned char MyModId;         /* Module id of this program */
   int MbCycles;                  /* # 1/2 cycles after P Mb can be computed */
   int LGSeconds;                 /* # seconds after P in which max LG can be 
                                     computed for Ml (excluding 1st MbCycles) */
   int MwpSeconds;                /* Max # seconds to evaluate P for Mwp */
   double MwpSigNoise;            /* Auto-Mwp necessary signal-to-noise ratio */
   int MinutesInBuff;             /* Number of minutes data to save per trace */
   int NumTracePerScreen;         /* # traces to show on screen (rest scroll) */
   double TimePerScreen;          /* # seconds data to show on screen */
   double PTimeDelta;             /* Time (sec).  If new pick within this many
                                     seconds from previous pick, it is same P */
   char ScreenFile[64];           /* Name of file with screen display info */
   char DummyFile[64];            /* Hypocenter data file */
   int      AlarmOn;              /* 1->Alarm function enabled, 0->Disabled */
   double   AlarmTimeout;         /* Time (sec) to re-start alarm after trig */
   SHM_INFO InRegion;             /* Info structure for input region */
   SHM_INFO PRegion;              /* Info structure for P region */
   SHM_INFO AlarmRegion;          /* Info structure for alarm output region */
} GPARM;

typedef struct {
   unsigned char MyInstId;        /* Local installation */
   unsigned char GetThisInstId;   /* Get messages from this inst id */
   unsigned char GetThisModId;    /* Get messages from this module */
   unsigned char TypeHeartBeat;   /* Heartbeat message id */
   unsigned char TypeError;       /* Error message id */
   unsigned char TypeAlarm;       /* Tsunami Ctr alarm message id */
   unsigned char TypePickTWC;     /* P-pick message - TWC format */
   unsigned char TypeWaveform;    /* Earthworm waveform messages */
} EWH;

/* Function declarations for develo
   ********************************/
long WINAPI ChildAWndProc( HWND, UINT, UINT, long );
void    FillP( PPICK *, STATION *, double, long );
int     GetEwh( EWH * );
void    GetLDC( long, long *, double *, long );
void    GetMag( STATION *, PPICK *, GPARM * );
long WINAPI GlobalScaleDlgProc( HWND, UINT, UINT, long );
void    HorizontalScroll( WORD, double *, double, double );
long WINAPI MwpTimeScaleDlgProc( HWND, UINT, UINT, long );
void    PadBuffer( long, double, long *, long *, long );
thr_ret PThread( void * );
void    PutDataInBuffer( TRACE_HEADER *, long *, long *, long *, long );
int     ReadScreenIni( int *, int, TRACE [], char [][32], STATION [], char * );
void    ResetOptions( HWND, int, TRACE [], int *, int *, int *, HMENU );
long WINAPI StationInformationDlgProc (HWND, UINT, UINT, long);
long WINAPI TracePerScreenDlgProc( HWND, UINT, UINT, long );
long WINAPI WndProc( HWND, UINT, UINT, long );
thr_ret WThread( void * );

int     GetConfig( char *, GPARM * );                    /* config.c */
void    LogConfig( GPARM * );
                                                         /* alarm.c */   
void    SeismicAlarm( STATION *, GPARM *, EWH *, TRACE_HEADER *, long * );

int     GetStaList( STATION **, int *, GPARM * );        /* stalist.c */
int     IsComment( char [] );
int     LoadStationData( STATION *, char * );
void    LogStaList( STATION *, int );

void    ReportPick( PPICK *, GPARM *, EWH * );           /* report.c */
void    ReportAlarm( STATION *, GPARM *, EWH * );   

