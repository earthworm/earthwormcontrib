
       /*****************************************************************
       *                        develo.c                               *
       *                                                               *
       *  This program takes waveforms dropped in InRing and           *
       *  displays them on a Windows window.  The display is similar   *
       *  to an old develocorder (with no film delay).  Every X        *
       *  seconds the screen scrolls to the left.  The display can be  *
       *  "held" and manually scrolled through previous times (the time*
       *  limit of saved data is specified in the .d file). An RC      *
       *  file goes along with this to describe the window menu,       *
       *  set-up, etc.                                                 *
       *                                                               *
       *  This module is strictly for use under Windows.               *
       *  It is based on the Windows NT real-time seismic processing   *
       *  system known as EarlyBird which was designed at the West     *
       *  Coast/Alaska Tsunami Warning Center.                         *
       *                                                               *
       *  P-picks entered to a PICK_RING are obtained by this module.  *
       *  The picks are displayed on the scrolling traces.  P-picks    *
       *  can also be entered to a pick ring interactively through this*
       *  module.  P-picks are made by left-clicking on a trace.       *
       *  Magnitude information is also computed as the pick is made.  *
       *  Other display options are trace scaling, time scaling,       *
       *  number of traces per screen, filtered/broadband data display,*
       *  and seismic subset specification.  The incoming signal is    *
       *  specified in the pick....sta file used by pick_wcatwc.       *
       *  SCREENDISP.INI, which is also used by the WC/ATWC interactive*
       *  analysis program, ANALYZE, can be used by this module.  This *
       *  INI file specifies subsets of stations which can be shown on *
       *  the screen.                                                  *
       *                                                               *
       *  Written by Paul Whitmore, (WC/ATWC) April, 2001              *
       *                                                               *
       *  October, 2005: Added options to run this module in           *
       *                 conjunction with ATPlayer. !!!                *
       *                                                               *
       ****************************************************************/
	   
#include <windows.h>
#include <wingdi.h>
#include <winuser.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include <swap.h>
#include "develo.h"

/* Global Variables 
   ****************/
double  dLastEndTime;          /* Screen end time of last display */
EWH     Ewh;                   /* Parameters from earthworm.h */
MSG_LOGO getlogoW;             /* Logo of requested waveforms */
GPARM   Gparm;                 /* Configuration file parameters */
HINSTANCE hInstMain;           /* Copy of main program instance (process id) */
MSG_LOGO hrtlogo;              /* Logo of outgoing heartbeats */
HWND  	hwndChildA;            /* Child window A handle */
HWND    hwndWndProc;           /* Client window handle */
int     iAhead[MAX_STATIONS];  /* Flag indicating if data ahead of present */
int     iChildA;               /* 1 when integration window is on */
int     iHold;                 /* 0->let screen auto-scroll, 1->fix screen */
int     iIndex;                /* Index of station right clicked upon */   
int     iMwpSecOrig;           /* MwpSeconds from .d file */
int     iNewPlayerData;        /* New set of data started from player !!! */
int     iScale;                /* Global scaling factor from dialogs */
long    lPickCounter;          /* Interactively added pick counter */
MSG     msg;                   /* Windows control message variable */
mutex_t mutsem1;               /* Semaphore to protect PBuf adjustments */
mutex_t mutsem2;               /* Semaphore to protect display */
pid_t   myPid;                 /* Process id of this process */
int     Nsta;                  /* Number of stations to display */
PPICK   *PBuf;                 /* Pointer to P-pick buffer */
STATION *StaArray;             /* Station data array */
char    szChildAName[]  = "Integrated waveforms";
char    szProcessName[] = "Develocorder";
time_t  then;                  /* Previous heartbeat time */
char    *WaveBuf;              /* Pointer to waveform buffer */
char    *WaveBuf2;             /* Pointer to spare waveform buffer */
TRACE_HEADER *WaveHead;        /* Pointer to waveform header */
long    *WaveLong;             /* Long pointer to waveform data */
long    *WaveLongF;            /* Waveform data with DC removed */
short   *WaveShort;            /* Short pointer to waveform data */

      /***********************************************************
       *              The main program starts here.              *
       *                                                         *
       *  Argument:                                              *
       *     argv[1] = Name of configuration file                *
       ***********************************************************/

int WINAPI WinMain (HINSTANCE hInst, HINSTANCE hPreInst, 
                    LPSTR lpszCmdLine, int iCmdShow)
{
   char          configfile[64];  /* Name of config file */
   HDC     hIC;	                  /* Information context to check for info */
   int           i;
   long          InBufl;          /* Maximum message size in bytes */
   char          line[40];        /* Heartbeat message */
   int           lineLen;         /* Length of heartbeat message */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */
   static unsigned tidP;          /* P-pick getter Thread */
   static unsigned tidW;          /* Waveform getter Thread */
   static WNDCLASS wc;

   iNewPlayerData = 0;   //!!!
   hInstMain = hInst;
   dLastEndTime = 0.;             
   lPickCounter = 10000;
   iChildA = 0;
   iHold = 0;                     /* Let screen auto scroll with new data */
   for ( i=0; i<MAX_STATIONS; i++ ) iAhead[i] = 0;
   
/* Get config file name (format "develo develo.D")
   ***********************************************************/
   if ( strlen( lpszCmdLine ) <= 0 )
   {
      fprintf( stderr, "Need configfile in start line.\n" );
      return -1;
   }
   strcpy( configfile, lpszCmdLine );

/* Get parameters from the configuration files
   *******************************************/
   if ( GetConfig( configfile, &Gparm ) == -1 )
   {
      fprintf( stderr, "GetConfig() failed. file %s.\n", configfile );
      return -1;
   }
   iMwpSecOrig = Gparm.MwpSeconds;

/* Look up info in the earthworm.h tables
   **************************************/
   if ( GetEwh( &Ewh ) < 0 )
   {
      fprintf( stderr, "develo: GetEwh() failed. Exiting.\n" );
      return -1;
   }

/* Specify logos of incoming waveforms and outgoing heartbeats
   ***********************************************************/
   getlogoW.instid = Ewh.GetThisInstId;
   getlogoW.mod    = Ewh.GetThisModId;
   getlogoW.type   = Ewh.TypeWaveform;

   hrtlogo.instid = Ewh.MyInstId;
   hrtlogo.mod    = Gparm.MyModId;
   hrtlogo.type   = Ewh.TypeHeartBeat;

/* Initialize name of log-file & open it
   *************************************/
   logit_init( configfile, Gparm.MyModId, 256, 1 );

/* Get our own pid for restart purposes
   ************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "develo: Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Log the configuration parameters
   ********************************/
   LogConfig( &Gparm );

/* Allocate the waveform buffers
   *****************************/
   InBufl = MAX_TRACEBUF_SIZ*2;
   WaveBuf = (char *) malloc( (size_t) InBufl );
   if ( WaveBuf == NULL )
   {
      logit( "et", "develo: Cannot allocate waveform buffer\n" );
      return -1;
   }
   WaveBuf2 = (char *) malloc( (size_t) InBufl );
   if ( WaveBuf2 == NULL )
   {
      logit( "et", "develo: Cannot allocate waveform buffer2\n" );
      return -1;
   }

/* Point to header and data portions of waveform message
   *****************************************************/
   WaveHead  = (TRACE_HEADER *) WaveBuf;
   WaveLong  = (long *) (WaveBuf + sizeof (TRACE_HEADER));
   WaveLongF  = (long *) (WaveBuf2 + sizeof (TRACE_HEADER));
   WaveShort = (short *) (WaveBuf + sizeof (TRACE_HEADER));

/* Read the station list and return the number of stations found.
   Allocate the station list array.
   *************************************************************/
   if ( GetStaList( &StaArray, &Nsta, &Gparm ) == -1 )
   {
      fprintf( stderr, "develo: GetStaList() failed. Exiting.\n" );
	  free( WaveBuf );
      free( WaveBuf2 );
      return -1;
   }
   if ( Nsta == 0 )
   {
      logit( "et", "develo: Empty station list. Exiting." );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      return -1;
   }
   logit( "t", "develo: Displaying %d stations.\n", Nsta );

/* Log the station list
   ********************/
   LogStaList( StaArray, Nsta );

/* If this is the first instance of this program, init window stuff and
   register window (it always is)
   ********************************************************************/
   if ( !hPreInst )
   {  /* Force PAINT when sized and give double click notification */
      wc.style         = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
      wc.lpfnWndProc   = WndProc;         /* Control Window process */
      wc.cbClsExtra    = 0;
      wc.cbWndExtra    = 0;
      wc.hInstance     = hInst;           /* Process id */
      wc.hIcon         = LoadIcon( hInst, "develo" );/* System app icon */
      wc.hCursor       = LoadCursor( NULL, IDC_ARROW );    /* Pointer */
      wc.hbrBackground = (HBRUSH) GetStockObject( WHITE_BRUSH ); /* White */
      wc.lpszMenuName  = "develo_menu";   /* Relates to .RC file */
      wc.lpszClassName = szProcessName;
      if ( !RegisterClass( &wc ) )        /* Window not registered */
      {
         logit( "t", "RegisterClass failed\n" );
         free( WaveBuf );
         free( WaveBuf2 );
         free( StaArray );
         return -1;
      }
   
/* And register child window A
   ***************************/
      wc.style           = CS_HREDRAW | CS_VREDRAW;  
      wc.lpfnWndProc     = ChildAWndProc;            /* Window process */
      wc.cbClsExtra      = 0;
      wc.cbWndExtra      = 0;
      wc.hInstance       = hInst;                    /* Process id */
      wc.hIcon           = NULL;
      wc.hCursor         = NULL;
      wc.hbrBackground   = GetStockObject( WHITE_BRUSH ); /* White background */
      wc.lpszMenuName    = "DeveloA_menu";           /* Relates to .RC file */
      wc.lpszClassName   = szChildAName;
      if ( !RegisterClass( &wc ) )                   /* Window not registered */
      {
         logit( "t", "RegisterClass 2 failed\n" );
         free( WaveBuf );
         free( WaveBuf2 );
         free( StaArray );
         return -1;
      }
   }

/* Create the window
   *****************/
   hIC = CreateIC ("DISPLAY", NULL, NULL, NULL);
   if (hIC == NULL)
   {
      logit( "t", "CreateIC error\n" );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      return -1;
   }
   hwndWndProc = CreateWindow(
                 szProcessName,          /* Process name */
                 szProcessName,          /* Initial title bar caption */
                 WS_OVERLAPPEDWINDOW | WS_HSCROLL | WS_VSCROLL,
//                 CW_USEDEFAULT,          /* top left x starting location */
//                 CW_USEDEFAULT,          /* top left y starting location */
//                 CW_USEDEFAULT,          /* Initial screen width in pixels */
//                 CW_USEDEFAULT,          /* Initial screen height in pixels */
                 0,                      /* top left x starting location */
                 0,                      /* top left y starting location */
                 GetDeviceCaps (hIC, HORZRES)/4, /* Window width in pixels */
                 GetDeviceCaps (hIC, VERTRES)/2, /* Window height in pixels */
                 NULL,                   /* No parent window */
                 NULL,                   /* Use standard system menu */
                 hInst,                  /* Process id */
                 NULL );                 /* No extra data to pass in */
   if ( hwndWndProc == NULL )            /* Window not created */
   {
      logit( "t", "CreateWindow failed\n" );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      return 0;
   }
   
   ShowWindow( hwndWndProc, iCmdShow );  /* Show the Window */
   UpdateWindow( hwndWndProc );          /* Force an initial PAINT call */

/* Attach to existing transport rings
   **********************************/
   if ( Gparm.PKey != Gparm.InKey )
   {
      tport_attach( &Gparm.InRegion,  Gparm.InKey );
      tport_attach( &Gparm.PRegion,   Gparm.PKey );
      tport_attach( &Gparm.AlarmRegion, Gparm.AlarmKey );
   }
   else
   {
      tport_attach( &Gparm.InRegion, Gparm.InKey );
      Gparm.PRegion = Gparm.InRegion;
   }

/* Flush the input waveform ring
   *****************************/
   while ( tport_getmsg( &Gparm.InRegion, &getlogoW, 1, &logo, &MsgLen,
                         WaveBuf, MAX_TRACEBUF_SIZ ) != GET_NONE );

/* Send 1st heartbeat to the transport ring
   ****************************************/
   time( &then );
   sprintf( line, "%d %d\n", then, myPid );
   lineLen = strlen( line );
   if ( tport_putmsg( &Gparm.InRegion, &hrtlogo, lineLen, line ) != PUT_OK )
   {
      logit( "et", "develo: Error sending 1st heartbeat. Exiting." );
      if ( Gparm.PKey != Gparm.InKey )
      {
         tport_detach( &Gparm.InRegion );
         tport_detach( &Gparm.PRegion );
         tport_detach( &Gparm.AlarmRegion );
      }
      else
         tport_detach( &Gparm.InRegion );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      return 0;
   }

/* Allocate and init the P-pick buffer
   ***********************************/
   InBufl = sizeof( PPICK ) * Nsta; 
   PBuf = (PPICK *) malloc( (size_t) InBufl );
   if ( PBuf == NULL )
   {
      if ( Gparm.PKey != Gparm.InKey )
      {
         tport_detach( &Gparm.InRegion );
         tport_detach( &Gparm.PRegion );
         tport_detach( &Gparm.AlarmRegion );
      }
      else
         tport_detach( &Gparm.InRegion );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      logit( "et", "develo: Cannot allocate ppick buffer\n");
      return -1;
   }
   for ( i=0; i<Nsta; i++ ) InitP( &PBuf[i] );
   for ( i=0; i<Nsta; i++ )       /* Fill lat/lon part of structure */
   {
      PBuf[i].dLat = StaArray[i].dLat;
      PBuf[i].dLon = StaArray[i].dLon;
      GeoCent( (LATLON *) &PBuf[i] );
      GetLatLonTrig( (LATLON *) &PBuf[i] );
   }
   
/* Create a mutex for protecting adjustments of PBuf
   *************************************************/
   CreateSpecificMutex( &mutsem1 );
   
/* Create a mutex for protecting screen display
   ********************************************/
   CreateSpecificMutex( &mutsem2 );

/* Start the P-pick getter thread
   ******************************/
   if ( StartThread( PThread, 8192, &tidP ) == -1 )
   {
      if ( Gparm.PKey != Gparm.InKey )
      {
         tport_detach( &Gparm.InRegion );
         tport_detach( &Gparm.PRegion );
         tport_detach( &Gparm.AlarmRegion );
      }
      else
         tport_detach( &Gparm.InRegion );
      free( PBuf );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      logit( "et", "Error starting P thread; exiting!\n" );
      return -1;
   }

/* Start the waveform getter tread
   *******************************/
   if ( StartThread( WThread, 8192, &tidW ) == -1 )
   {
      if ( Gparm.PKey != Gparm.InKey )
      {
         tport_detach( &Gparm.InRegion );
         tport_detach( &Gparm.PRegion );
         tport_detach( &Gparm.AlarmRegion );
      }
      else
         tport_detach( &Gparm.InRegion );
      free( PBuf );
      free( WaveBuf );
      free( WaveBuf2 );
      free( StaArray );
      logit( "et", "Error starting W thread; exiting!\n" );
      return -1;
   }
	 
/* Start a timer so that display options are automatically reset 
   *************************************************************/
   SetTimer( hwndWndProc, ID_TIMER2, RESET_INTERVAL, NULL );
   
/* Start a timer for dummy file reads (so that expected Ps are shown)
   ******************************************************************/
   SetTimer( hwndWndProc, ID_TIMER3, DUMMY_READ_INT, NULL );
   
/* Main windows thread
   *******************/      
   while ( GetMessage( &msg, NULL, 0, 0 ) )
   {
      TranslateMessage( &msg );
      DispatchMessage( &msg );
   }

/* Detach from the ring buffers
   ****************************/
   if ( Gparm.PKey != Gparm.InKey )
   {
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.PRegion );
      tport_detach( &Gparm.AlarmRegion );
   }
   else
      tport_detach( &Gparm.InRegion );
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
   free( WaveBuf );
   free( WaveBuf2 );
   free( PBuf );
   for ( i=0; i<Nsta; i++ ) 
   {
      free( StaArray[i].pdRawDispData );
      free( StaArray[i].pdRawIDispData );
      free( StaArray[i].plRawCircBuff );
      free( StaArray[i].plFiltCircBuff );
   }
   free( StaArray );
   logit( "t", "Termination requested. Exiting.\n" );
   return (msg.wParam);
}

      /***********************************************************
       *                  ChildAWndProc()                        *
       *                                                         *
       *  This dialog procedure processes messages from the child*
       *  windows screen and messages passed to it from          *
       *  elsewhere.  All display is performed in PAINT. The     *
       *  velocity, displacement, and integrated displacement    *
       *  trace for each signal with an Mwp computed is shown    *
       *                                                         *
       ***********************************************************/
	   
long WINAPI ChildAWndProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   static  int     cxWidthAx, cyHeightAx;   /* Display area in pixels */
   static  double  dMaxV[MAX_STATIONS], dMaxD[MAX_STATIONS], 
                   dMaxID[MAX_STATIONS];    /* Max value of each trace */
   double  dTotalDisp;                      /* Running total of integrations */
   static  HDC     hdc;                     /* Handle to Device context */
   static  HPEN    hRPen, hBPen;            /* Font/color handles */
   static  long    i, j, k, lTemp, jCnt;    /* Counters */
   static  int     iNumToDisplay;           /* Number of stations to show */
   static  long    lNum;                    /* Number of samples to evaluate */
   static  long    lPIndex;                 /* Buffer index of P-time */
   long    lTemp2;                          /* Temporary counters */
   static  PAINTSTRUCT ps;
   static  POINT   ptArray[MAXMWPARRAY], pt;/* Display point structures */
   static  RECT    rct;
   static  char    szTemp2[12], szTemp3[6];

   switch ( msg )
   {
      case WM_SIZE:               /* Get size and force a redraw */
         cyHeightAx = HIWORD( lParam );       /* (in child windows, this is */
         cxWidthAx = LOWORD( lParam );        /*  the client display size)  */
         InvalidateRect( hwnd, NULL, TRUE );
         break;

      case WM_PAINT:   /* First, loop through each array to determine the max */
         hdc = BeginPaint( hwnd, &ps );
         hBPen = CreatePen( PS_SOLID, 1, RGB( 0,0,0 ) );    /* black */
         hRPen = CreatePen( PS_SOLID, 1, RGB( 255,0,0 ) );  /* red */
         GetClientRect( hwnd, &rct );
         FillRect( hdc, &rct, (HBRUSH) GetStockObject( WHITE_BRUSH ) );
		 
/* How many stations have Mwp */		 
         iNumToDisplay = 0;
         for ( j=0; j<Nsta; j++ )
            if ( PBuf[j].dMwpIntDisp > 1.e-12 &&
                 StaArray[j].dEndTime-PBuf[j].dPTime < Gparm.MinutesInBuff*60. )
                    iNumToDisplay++;
         jCnt = 0;
         for ( j=0; j<Nsta; j++ )
            if ( PBuf[j].dMwpIntDisp > 1.e-12 &&
                 StaArray[j].dEndTime-PBuf[j].dPTime < Gparm.MinutesInBuff*60. )
            {
/* What is buffer index of P-time? */   
               lPIndex = StaArray[j].lSampIndexF - (long) ((StaArray[j].dEndTime
                         -PBuf[j].dPTime) * StaArray[j].dSampRate) - 1;
               while ( lPIndex <  0 ) lPIndex += StaArray[j].lRawCircSize;
               while ( lPIndex >= StaArray[j].lRawCircSize )
                  lPIndex -= StaArray[j].lRawCircSize; 
   
/* How many points to display? */   
               lNum = (long) (StaArray[j].dSampRate*
                            ((double) PBuf[j].dMwpTime));
//                            ((double) PBuf[j].dMwpTime+10.));
   
/* See if this integration was done here */   
               for ( i=0; i<10; i++ )
                  if ( fabs( StaArray[j].pdRawDispData[i] ) > 1.e-10 )
                     goto PastInt;   
   
/* Integrate velocity signal to get displacement. Convert counts to m/s before
   conversion. */
               dTotalDisp = 0.;
               for ( i=0; i<lNum-1; i++ )
               {
                  lTemp = i + lPIndex;
                  if ( lTemp >= StaArray[j].lRawCircSize )
                     lTemp -= StaArray[j].lRawCircSize;
                  lTemp2 = lTemp + 1;
                  if ( lTemp2 >= StaArray[j].lRawCircSize )
	                 lTemp2 -= StaArray[j].lRawCircSize;
                  StaArray[j].pdRawDispData[i] = dTotalDisp + 1./
                   StaArray[j].dSampRate*0.5*((double)
                   (StaArray[j].plRawCircBuff[lTemp]-StaArray[j].dAveLDCRaw)/
                   StaArray[j].dSens + (double)
                   (StaArray[j].plRawCircBuff[lTemp2]-
                   StaArray[j].dAveLDCRaw)/StaArray[j].dSens);
                  dTotalDisp = StaArray[j].pdRawDispData[i];
               }
   
/* Integrate displacement to get integrated displacement signal */
               dTotalDisp = 0.;
               for ( i=0; i<lNum-1; i++ )
               {
                  StaArray[j].pdRawIDispData[i] = dTotalDisp + 1./
                   StaArray[j].dSampRate*0.5*(StaArray[j].pdRawDispData[i]+
                   StaArray[j].pdRawDispData[i+1]);;
                  dTotalDisp = StaArray[j].pdRawIDispData[i];
               }
PastInt:
/* Find max value of each three traces */			   
               dMaxV[j] = 0.;
               dMaxD[j] = 0.;
               dMaxID[j] = 0.;
               for ( i=1; i<lNum-1; i++ )
               {
                  lTemp = i + lPIndex;
                  if ( lTemp >= StaArray[j].lRawCircSize )
                     lTemp -= StaArray[j].lRawCircSize;
                  if ( fabs( (double) (StaArray[j].plRawCircBuff[lTemp]-
                       StaArray[j].dAveLDCRaw)/StaArray[j].dSens ) > dMaxV[j] )
                     dMaxV[j] = fabs( (double) (StaArray[j].plRawCircBuff[lTemp]
                      -StaArray[j].dAveLDCRaw)/StaArray[j].dSens );
                  if ( fabs( StaArray[j].pdRawDispData[i] ) > dMaxD[j] ) 
                     dMaxD[j] = fabs( StaArray[j].pdRawDispData[i] );
                  if ( fabs( StaArray[j].pdRawIDispData[i] ) > dMaxID[j] )
                     dMaxID[j] = fabs ( StaArray[j].pdRawIDispData[i] );
               }
			   
/* Prevent divide by zero */
               if ( dMaxV[j] == 0. )  dMaxV[j]  = 1.;
               if ( dMaxD[j] == 0. )  dMaxD[j]  = 1.;
               if ( dMaxID[j] == 0. ) dMaxID[j] = 1.;
			   
/* Erase the screen and initialize values */
               for ( k=0; k<3; k++ )   /* 3 displays for each station */
               {
/* Start the displays at Ptime */
                  for ( i=0; i<lNum-1; i++ )
                  {       /* Fill .x array first (same for each of 3) */
                     ptArray[i].x = (long) (((double) i / (double) lNum) *
                      (cxWidthAx/(iNumToDisplay+1)) + jCnt*cxWidthAx/
                       iNumToDisplay + 5.);
                     lTemp = i + lPIndex;
                     if ( lTemp >= StaArray[j].lRawCircSize )
                        lTemp -= StaArray[j].lRawCircSize;
                     if ( k == 0 )          /* Velocity */
                        ptArray[i].y = cyHeightAx - (k*cyHeightAx/3 +
                         cyHeightAx/6 + (long) ((double)
                         ((StaArray[j].plRawCircBuff[lTemp]-
                         StaArray[j].dAveLDCRaw)/StaArray[j].dSens) / dMaxV[j] *
                         (cyHeightAx/6)));
                     if ( k == 1 )          /* Displacement */
                        ptArray[i].y = cyHeightAx - (k*cyHeightAx/3 +
                         cyHeightAx/6 + (long) (StaArray[j].pdRawDispData[i] / 
                         dMaxD[j] * (cyHeightAx/6)));
                     if ( k == 2 )          /* Integrated displacement */
                        ptArray[i].y = cyHeightAx - (k*cyHeightAx/3 +
                         cyHeightAx/6 + (long) (StaArray[j].pdRawIDispData[i] / 
                         dMaxID[j] * (cyHeightAx/6)));
                  }
/* Draw trace */
                  SelectObject( hdc, hBPen );
                  MoveToEx( hdc, ptArray[0].x, ptArray[0].y, NULL );
                  Polyline( hdc, ptArray, lNum-1 );
/* Put up zero lines */
                  pt.x = jCnt*cxWidthAx/iNumToDisplay + 5;
                  pt.y = cyHeightAx - (k*cyHeightAx/3 + cyHeightAx/6);
                  SelectObject( hdc, hRPen );
                  MoveToEx( hdc, pt.x, pt.y, NULL );
                  pt.x = cxWidthAx / (iNumToDisplay+1) +
                         jCnt*cxWidthAx/iNumToDisplay + 5;
                  LineTo( hdc, pt.x, pt.y );
/* Put up labels */
                  if ( jCnt == 0 )
                  {
                     pt.x = jCnt*cxWidthAx/iNumToDisplay + 5;
                     pt.y = 19*cyHeightAx/20 - (k*cyHeightAx/3);
                     SetTextColor( hdc, RGB( 0,0,255 ) );  /* blue */
                     if ( k == 0 ) strcpy( szTemp2, "Vel." );
                     if ( k == 1 ) strcpy( szTemp2, "Disp." );
                     if ( k == 2 ) strcpy( szTemp2, "Int. Disp." );
                     TextOut( hdc, pt.x, pt.y, szTemp2, strlen( szTemp2 ) );
                  }
/* Display station name */
                  pt.x = jCnt*cxWidthAx/iNumToDisplay + 5;
                  pt.y = 1;
                  SetTextColor( hdc, RGB( 0,0,255 ) );     /* blue */
                  TextOut( hdc, pt.x, pt.y, StaArray[j].szStation,
                           strlen( StaArray[j].szStation ) );
/* Display integration time at bottom */
                  pt.x = jCnt*cxWidthAx/iNumToDisplay + 5 +
                         cxWidthAx/iNumToDisplay/2;
                  pt.y = 9*cyHeightAx/10;
                  lTemp = (long) (PBuf[j].dMwpTime + 0.5);
                  itoaX( lTemp, szTemp3 );
                  SetTextColor( hdc, RGB( 0,0,255 ) );     /* blue */
                  TextOut( hdc, pt.x, pt.y, szTemp3, strlen( szTemp3 ) );
               }
               jCnt++;
            }
						
/* Write Title bar message */
         DeleteObject( hBPen );
         DeleteObject( hRPen );
         SetWindowText( hwnd, "Integrated Waveform Display" );
         EndPaint( hwnd, &ps );
         break;                                                          

    default:
        return( DefWindowProc( hwnd, msg, wParam, lParam ) );
    }
return 0;
}

      /******************************************************************
       *                            FillP()                             *
       *                                                                *
       *  This function loads a PPICK structure.                        *
       *                                                                *
       *  Arguments:                                                    *
       *   pP               PPICK structure                             *
       *   pSta             STATION structure                           *
       *   dPTime           New P-time (1/1/70 seconds)                 *
       *   lPIndex          Pick Index (number offset from pick_wcatwc) *
       *                                                                *
       ******************************************************************/
	   
void FillP( PPICK *pP, STATION *pSta, double dPTime, long lPIndex )
{
   strcpy( pP->szStation, pSta->szStation );
   strcpy( pP->szChannel, pSta->szChannel );
   strcpy( pP->szNetID, pSta->szNetID );
   strcpy( pP->szPhase, "eP");
   pP->cFirstMotion = '?';
   pP->dLat = pSta->dLat;
   pP->dLon = pSta->dLon;
   GeoCent( (LATLON *) pP );
   GetLatLonTrig( (LATLON *) pP );
   pP->dPTime           = dPTime;
   pP->iUseMe           = 1;
   pP->iStationType     = pSta->iStationType;
   pP->dSens            = pSta->dSens;
   pP->dGainCalibration = pSta->dGainCalibration;
   pP->dClipLevel       = pSta->dClipLevel;
   pP->dElevation       = pSta->dElevation;
   pP->lPickIndex       = lPIndex;
}

      /*******************************************************
       *                      GetEwh()                       *
       *                                                     *
       *      Get parameters from the earthworm.d file.      *
       *******************************************************/

int GetEwh( EWH *Ewh )
{
   if ( GetLocalInst( &Ewh->MyInstId ) != 0 )
   {
      fprintf( stderr, "develo: Error getting MyInstId.\n" );
      return -1;
   }
   if ( GetInst( "INST_WILDCARD", &Ewh->GetThisInstId ) != 0 )
   {
      fprintf( stderr, "develo: Error getting GetThisInstId.\n" );
      return -2;
   }
   if ( GetModId( "MOD_WILDCARD", &Ewh->GetThisModId ) != 0 )
   {
      fprintf( stderr, "develo: Error getting GetThisModId.\n" );
      return -3;
   }
   if ( GetType( "TYPE_HEARTBEAT", &Ewh->TypeHeartBeat ) != 0 )
   {
      fprintf( stderr, "develo: Error getting TypeHeartbeat.\n" );
      return -4;
   }
   if ( GetType( "TYPE_ERROR", &Ewh->TypeError ) != 0 )
   {
      fprintf( stderr, "develo: Error getting TypeError.\n" );
      return -5;
   }
   if ( GetType( "TYPE_ALARM", &Ewh->TypeAlarm ) != 0 )
   {
      fprintf( stderr, "develo: Error getting TypeAlarm.\n" );
      return -6;
   }
   if ( GetType( "TYPE_PICKTWC", &Ewh->TypePickTWC ) != 0 )
   {
      fprintf( stderr, "develo: Error getting TYPE_PICKTWC.\n" );
      return -7;
   }
   if ( GetType( "TYPE_TRACEBUF", &Ewh->TypeWaveform ) != 0 )
   {
      fprintf( stderr, "develo: Error getting TYPE_TRACEBUF.\n" );
      return -8;
   }
   return 0;
}

  /******************************************************************
   *                           GetLDC()                             *
   *                                                                *
   *  Determine and update moving averages of signal value          *
   *  (called LDC here).                                            *
   *                                                                *
   *  Arguments:                                                    *
   *    lNSamps     Number of samples in this packet                *
   *    WaveLong    Pointer to data array                           *
   *    pdLDC       Pointer to long term DC average                 *
   *    lFiltSamp   # samples through filter                        *
   *                                                                *
   ******************************************************************/

void GetLDC( long lNSamps, long *WaveLong, double *pdLDC, long lFiltSamp )
{
   double  dSumLDC;     /* Summation of all values in this packet */
   int     i;
   
   dSumLDC = 0.;
   
/* Sum up total for this packet */
   for ( i=0; i<lNSamps; i++ )
   {
      if ( lFiltSamp == 0 ) dSumLDC  += ((double) WaveLong[i]);
      else                  dSumLDC  += ((double) WaveLong[i] - *pdLDC);
   }
	  
/* Compute new LTAs (first time through, just take average.  Adjust average
   from there */
   if ( lFiltSamp == 0 ) *pdLDC =  (dSumLDC/(double) lNSamps);
   else                  *pdLDC += (0.5 * dSumLDC/(double) lNSamps);
}

 /***********************************************************************
  *                             GetMag()                                *
  *      Loop through data to get magnitude parameters for the latest   *
  *      interactive pick.                                              *
  *                                                                     *
  *  Arguments:                                                         *
  *     Sta              Pointer to station being processed             *
  *     pPBuf            P-pick buffer                                  *
  *     Gparm            Configuration buffer                           *
  *                                                                     *
  ***********************************************************************/
  
void GetMag( STATION *Sta, PPICK *pPBuf, GPARM *Gparm )
{
   double  dOldestTime;   /* Oldest time (1/1/70 seconds) in buffer */
   long    i, lIndex;
   int     iReversal;     /* 1 when signal difference changes sign */
   long    lNum;          /* Number of samples to evaluate */
   long    lNumInBuff;    /* # samples ahead of P in buffer */
   long    lPIndex;       /* Buffer index of P-time */

/* Is P-time within buffer? */
   dOldestTime = Sta->dEndTime -
    ((double) Sta->lRawCircSize/Sta->dSampRate) + 1./Sta->dSampRate;
   if ( pPBuf->dPTime < dOldestTime || pPBuf->dPTime > Sta->dEndTime )
   {
      logit( "", "%s - P-time out of buffer, P=%lf, End=%lf, Old=%lf\n",
             Sta->szStation, pPBuf->dPTime, Sta->dEndTime, dOldestTime );
      return;
   }
   
/* What is buffer index of P-time? */   
   lPIndex = Sta->lSampIndexF - (long) ((Sta->dEndTime-pPBuf->dPTime) * 
             Sta->dSampRate) - 1;
   while ( lPIndex <  0 )                 lPIndex += Sta->lRawCircSize;
   while ( lPIndex >= Sta->lRawCircSize ) lPIndex -= Sta->lRawCircSize; 
   
/* How many points to evaluate? */   
   lNumInBuff = (long) ((Sta->dEndTime-pPBuf->dPTime) * Sta->dSampRate);
   lNum = (long) (Sta->dSampRate * (double) Gparm->LGSeconds);
   if ( lNum > lNumInBuff )       /* There is more data needed to compute Ml */
      lNum = lNumInBuff;

/* Initialize some things */
   if ( lPIndex > 0 ) Sta->lSampOld = Sta->plFiltCircBuff[lPIndex-1];
   else               Sta->lSampOld = Sta->plFiltCircBuff[Sta->lRawCircSize-1];
   Sta->lMDFOld = 0;
   Sta->lSampsPerCyc = 0;
   Sta->lMDFRunning = 0;
   Sta->lCycCnt = 0;
   Sta->dMaxPk = 0.;
   iReversal = 0;

/* Loop through data to get magnitude */
   for ( i=0; i<lNum; i++ )
   {
      lIndex = i + lPIndex;
      if ( lIndex >= Sta->lRawCircSize ) lIndex -= Sta->lRawCircSize; 
      Sta->lSampNew = Sta->plFiltCircBuff[lIndex];
      Sta->lMDFNew = Sta->lSampNew - Sta->lSampOld;
	  
/* Check for cycle changes */
      if ( i > 0 )
      {
         if ( (Sta->lMDFOld <  0 && Sta->lMDFNew <  0) ||
              (Sta->lMDFOld >= 0 && Sta->lMDFNew >= 0) )
         {     /* No changes, continuing adding up MDF */
            Sta->lSampsPerCyc++;
            Sta->lMDFRunning += Sta->lMDFNew;
         }
         else  /* Cycle has changed sign, get mags and start anew */
         {
            if ( i > 1 ) iReversal = 1;         /* For 1st motion determnination */
            ComputeMbMl ( Sta, i, pPBuf, Gparm->MbCycles );
            Sta->lMDFRunning = Sta->lMDFNew;
            Sta->lSampsPerCyc = 0;
         }
	  
/* Get first motion */
         if ( i == 1 )	  
            if (Sta->lMDFRunning > 0) pPBuf->cFirstMotion = 'U';
            else                      pPBuf->cFirstMotion = 'D';
         if ( i > 1 && i <= FIRST_MOTION_SAMPS && iReversal == 1 )
            pPBuf->cFirstMotion = '?';
      }
      Sta->lSampOld = Sta->lSampNew;
      Sta->lMDFOld = Sta->lMDFNew;
   }
}
		 
      /***********************************************************
       *                   GlobalScaleDlgProc()                  *
       *                                                         *
       * This procedure is used to change the overall scale in   *
       * both the vertical and horizontal directions.            *
       *                                                         *
       ***********************************************************/
       
long WINAPI GlobalScaleDlgProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   switch ( msg )
   {
      case WM_INITDIALOG:
         SetFocus( hwnd );
         break;

      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {
            case IDD_SCALE4X:        /* The following scales end the */
               iScale = 4;           /*  dialog when chosen          */
               EndDialog( hwnd, IDOK );
               break;
	       
            case IDD_SCALE2X:
               iScale = 2;
               EndDialog( hwnd, IDOK );
               break;
	       
            case IDD_SCALEHALF:
               iScale = -2;
               EndDialog( hwnd, IDOK );
               break;
	       
            case IDD_SCALEQUARTER:
               iScale = -4;
               EndDialog( hwnd, IDOK );
               break;
		
            case IDD_SCALE_DEFAULT:	// Re-set to default
               iScale = 0;
               EndDialog( hwnd, IDOK );
               break;
        
            case IDCANCEL:        /* Escape and make no changes */
               EndDialog( hwnd, IDCANCEL );
               break;
         }
   }
   return 0;
}

   /*******************************************************************
    *                      HorizontalScroll()                         *
    *                                                                 *
    * This function handles scrolling messages.  How much the screen  *
    * scrolls depends on the amount message.                          *
    *                                                                 *
    *  Arguments:                                                     *
    *    wScroll       Amount to scroll screen                        *
    *    pdLastEndTime New Time at right side of screen (1/1/70 sec)  *
    *    dScrollHBit   Small scrolling amount                         *
    *    dScrollHPage  Large scrolling amount                         *
    *                                                                 *
    *******************************************************************/
	
void HorizontalScroll( WORD wScroll, double *pdLastEndTime,
                       double dScrollHBit, double dScrollHPage )
{
   switch ( wScroll )         /* Respond to scrolling message */
   {
      case SB_LINELEFT:
         *pdLastEndTime += -dScrollHBit;
         break;
		 
      case SB_LINERIGHT:
         *pdLastEndTime += dScrollHBit;
         break;
		
      case SB_PAGELEFT:
         *pdLastEndTime += -dScrollHPage;
         break;
		
      case SB_PAGERIGHT:
         *pdLastEndTime += dScrollHPage;
         break;
		
      default:
         *pdLastEndTime += 0.;
         break;
    }
}

     /**************************************************************
      *                         LoadP()                            *
      *                                                            *
      * Load latest P-pick in the buffer.  If the pick is a        *
      * repeat of a previous pick, replace the previous one (this  *
      * should have new magnitude information).                    *
      *                                                            *
      * Arguments:                                                 *
      *  pPStruct    Structure containing input P-pick information *
      *  pPBuf       P-pick buffer to copy PStruct into            *
      *  iNumSta     Numberof stations in Sta                      *
      *  Sta         Array of station information                  *
      *                                                            *
      **************************************************************/
	  
void LoadP( PPICK *pPStruct, PPICK *pPBuf, STATION Sta[], int iNumSta )
{
   int     i;                      /* Index counters */
   
/* First, see if this pick is from a station in our array */
   for ( i=0; i<iNumSta; i++ )
      if ( !strcmp( pPStruct->szStation, Sta[i].szStation ) &&
           !strcmp( pPStruct->szChannel, Sta[i].szChannel ) &&
           !strcmp( pPStruct->szNetID,   Sta[i].szNetID ) )
      {
/* If pick is an update, don't adjust P-time, just mags.  This is
   done so that any interactively changed P-times will not be re-changed */
         if ( pPStruct->lPickIndex == PBuf[i].lPickIndex )
            ShortCopyPBuf( pPStruct, &pPBuf[i] );
         else CopyPBuf( pPStruct, &pPBuf[i] );
         return;
      }
   logit( "", "Incoming P has no match - %s %s %s\n", pPStruct->szStation, 
          pPStruct->szChannel, pPStruct->szNetID ); 
}				  

      /***********************************************************
       *                     MwpTimeDlgProc()                    *
       *                                                         *
       * This procedure is used to change the integration window *
       * length and signal:noise for Mwp computations.           *
       *                                                         *
       ***********************************************************/
	   
long WINAPI MwpTimeDlgProc (HWND hwnd, UINT msg, UINT wParam, long lParam)
{
   double  dTemp2;
   int     iTemp;
   char    szTemp[12];

   switch ( msg )
   {
      case WM_INITDIALOG:         /* Set defaults */
         SetDlgItemText( hwnd, EF_MWPTIME,
                         itoaX( Gparm.MwpSeconds, szTemp ) );
         SetDlgItemText( hwnd, EF_MWPSN,
                         gcvt( Gparm.MwpSigNoise, 3, szTemp ) );
         SetFocus( hwnd );
         break;

      case WM_COMMAND:            
         switch (LOWORD (wParam))
            {
               case IDOK:                /* OK was chosen */
/* Read integration time in seconds */
                  GetDlgItemText( hwnd, EF_MWPTIME, szTemp, sizeof (szTemp) );
                  iTemp = atoi( szTemp );
                  if ( iTemp <= 0 )      /* Was it too small? */
                  {  MessageBox( hwnd, "Invalid Time",
                                 NULL, MB_OK | MB_ICONEXCLAMATION ); break; }
                  else if ( iTemp > iMwpSecOrig+20 )   /* Too big? */
                  {  MessageBox( hwnd, "Max time is listed in .d file (+20)", 
                                 NULL, MB_OK | MB_ICONEXCLAMATION ); break; }
/* Read Signal-to-noise ratio which must be exceeded
   for Mwp computations to take place */
                  GetDlgItemText( hwnd, EF_MWPSN, szTemp, sizeof (szTemp) );
                  dTemp2 = atof( szTemp );
                  if ( dTemp2 <= 0. )     /* Is it reasonable? */
                  {  MessageBox( hwnd, "Invalid S:N",
                                 NULL, MB_OK | MB_ICONEXCLAMATION ); break; }
                  Gparm.MwpSigNoise = dTemp2;
                  Gparm.MwpSeconds = iTemp;
                  EndDialog( hwnd, IDOK );
                  break;

               case IDCANCEL:           /* Escape was chosen */
                  EndDialog( hwnd, IDCANCEL );
                  break;
            }
            break;
   }
   return 0;
}

  /******************************************************************
   *                        PadBuffer()                             *
   *                                                                *
   *  Fill in gaps in the data with DC offset.                      *
   *  Update last data value index.                                 *
   *                                                                *
   *  Arguments:                                                    *
   *    lGapSize    Number of values to pad (+1)                    *
   *    dLDC        DC average                                      *
   *    plBuffCtr   Index tracker in main buffer                    *
   *    plBuff      Pointer to main circular buffer                 *
   *    lBuffSize   # samples in main circular buffer               *
   *                                                                *
   ******************************************************************/

void PadBuffer( long lGapSize, double dLDC, long *plBuffCtr, 
                long *plBuff, long lBuffSize )
{
   long    i;
   
/* Pad the data buffer over the gap interval */
   for ( i=0; i<lGapSize-1; i++ )
   {
      plBuff[*plBuffCtr] = (long) dLDC;
      *plBuffCtr += 1;
      if ( *plBuffCtr == lBuffSize ) *plBuffCtr = 0;
   }	  
}

      /*********************************************************
       *                     PThread()                         *
       *                                                       *
       *  This thread gets messages from the P-ring.  It takes *
       *  the messages, loads up the P buffer, and alerts      *
       *  WndProc that a P is here to show.                    *
       *                                                       *
       *********************************************************/
	   
thr_ret PThread( void *dummy )
{
   MSG_LOGO      getlogoP;        /* Logo of requested picks */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */
   char          PIn[MAX_PICKTWC_SIZE];/* Pointer to P-pick from ring */

/* Set up logos for P ring 
   ***********************/
   getlogoP.instid = Ewh.GetThisInstId;
   getlogoP.mod    = Ewh.GetThisModId;
   getlogoP.type   = Ewh.TypePickTWC;

/* Flush the input ring
   ********************/
   while ( tport_getmsg( &Gparm.PRegion, &getlogoP, 1, &logo, &MsgLen,
                         PIn, MAX_PICKTWC_SIZE) != GET_NONE );
						 
/* Loop to read picker messages, load buffer, and notify WndProc
   *************************************************************/
   while ( tport_getflag( &Gparm.PRegion ) != TERMINATE )
   {
      int     rc;               /* Return code from tport_getmsg() */
      PPICK   PStruct;          /* P-pick data structure */

/* Get a P-pick buffer from transport region
   *****************************************/
      rc = tport_getmsg( &Gparm.PRegion, &getlogoP, 1, &logo, &MsgLen,
                         PIn, MAX_PICKTWC_SIZE);

      if ( rc == GET_NONE )
      {
         sleep_ew( 1000 );
         continue;
      }

      if ( rc == GET_NOTRACK )
         logit( "et", "develoT: Tracking error.\n");

      if ( rc == GET_MISS_LAPPED )
         logit( "et", "develoT: Got lapped on the ring.\n");

      if ( rc == GET_MISS_SEQGAP )
         logit( "et", "develoT: Gap in sequence numbers.\n");

      if ( rc == GET_MISS )
         logit( "et", "develoT: Missed messages.\n");

      if ( rc == GET_TOOBIG )
      {
         logit( "et", "develoT: Retrieved message too big (%d) for msg.\n",
                MsgLen );
         continue;
      }
	  
/* Put P-pick into structure (NOTE: No byte-swapping is performed, so 
   there will be trouble getting picks from an opposite order machine)
   ******************************************************************/
      if ( PPickStruct( PIn, &PStruct, Ewh.TypePickTWC ) < 0 ) continue;

/* Add other known data about station to PPick structure
   *****************************************************/
      if ( PPickMatch( &PStruct, StaArray, Nsta ) < 0 )
      {
         int      lineLen;
         time_t   errTime;
         char     errmsg[80];
         MSG_LOGO logo;
		 
         time( &errTime );
         sprintf( errmsg, "%d 2 %s %s %s not found in StaDataFile\n", errTime,
                  PStruct.szStation, PStruct.szNetID, PStruct.szChannel );
         lineLen = strlen( errmsg );
         logo.type   = Ewh.TypeError;
         logo.mod    = Gparm.MyModId;
         logo.instid = Ewh.MyInstId;
         tport_putmsg( &Gparm.InRegion, &logo, lineLen, errmsg );
         logit( "et", "Error message (PPickMatch) posted - %s %s\n",
                 PStruct.szStation, PStruct.szChannel );
         continue;
      }
	  
/* Load P-pick into buffer
   ***********************/
      if ( PStruct.dPTime > 0. )          /* It is not just an Ms */
      {
         RequestSpecificMutex( &mutsem1 );/* Semaphore protect buffer writes */
         LoadP( &PStruct, PBuf, StaArray, Nsta );
         ReleaseSpecificMutex( &mutsem1 );/* Let someone else have sem */
      }
   }
   
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
}

  /******************************************************************
   *                         PutDataInBuffer()                      *
   *                                                                *
   *  Load the data from the input buffer to the large circular     *
   *  buffer.  Update last data value index.                        *
   *                                                                *
   *  Arguments:                                                    *
   *    WaveHead    Pointer to data buffer header                   *
   *    WaveLong    Pointer to data array                           *
   *    plBuff      Pointer to main circular buffer                 *
   *    plBuffCtr   Index tracker in main buffer                    *
   *    lBuffSize   # samples in main circular buffer               *
   *                                                                *
   ******************************************************************/

void PutDataInBuffer( TRACE_HEADER *WaveHead, long *WaveLong, 
                      long *plBuff, long *plBuffCtr, long lBuffSize )
{
   int     i;
   
/* Copy the data buffer */
   for ( i=0; i<WaveHead->nsamp; i++ )
   {
      plBuff[*plBuffCtr] = WaveLong[i];
      *plBuffCtr += 1;
      if ( *plBuffCtr == lBuffSize ) *plBuffCtr = 0;
   }	  
}

      /******************************************************************
       *                      ReadScreenIni()                           *
       *                                                                *
       * This function reads the screen display initialization file.    *
       * This file allows screen display which show just a sub-set of   *
       * stations.                                                      *
       *                                                                *
       *  Arguments:                                                    *
       *   piNumScreens     Number station screen subsets               *
       *   iNumSta          Numberof stations in Sta                    *
       *   Trace            Array of trace control information          *
       *   szScreenName     Name of screen sub-sets                     *
       *   Sta              Array of station information                *
       *   pszFile          Screen display file name                    *
       *                                                                *
       *  Return:           1 if read ok, 0 if problem                  *
       *                                                                *
       ******************************************************************/
	   
int ReadScreenIni( int *piNumScreens, int iNumSta, TRACE Trace[],
                   char szScreenName[][32], STATION Sta[], char *pszFile )
{
   int     i, j;
   int     iScreen[MAX_SCREENS];   /* Temp flags for screen display */
   FILE    *hFile;                 /* File handle */
   char    szSta[6], szChn[6], szNet[4];   /* Input SCN */

/* Try to open the ini file */
   if ( (hFile = (FILE *) fopen( pszFile, "r" )) == NULL )
   {
      logit ("t", "Couldn't open screen ini file: %s\n", pszFile);
      return 0;
   }
   
/* Read screen initialization values */
   fscanf( hFile, "Number of Station Screens: %ld\n", piNumScreens );
   if ( *piNumScreens > MAX_SCREENS || *piNumScreens < 1 )
   {
      logit( "", "Incorrect number of screen subsets - %ld\n", *piNumScreens );
      fclose( hFile );
      return 0;
   }
   for ( i=0; i<*piNumScreens; i++ )
   {
      fscanf( hFile, "Screen Name: " );
      fgets( szScreenName[i], sizeof (szScreenName[i]), hFile );
      fscanf( hFile, "\n" );
      szScreenName[i][strlen( szScreenName[i] )-1] = '\0';  /* End string */
   }
   
/* Assign screen display flags to known stations */
   while ( fscanf( hFile, "%s %s %s ", szSta, szChn, szNet ) != EOF )
   {
      for ( i=0; i<*piNumScreens; i++ )
         fscanf( hFile, "%ld ", &iScreen[i] );
      fscanf( hFile, "\n" );
      for ( i=0; i<iNumSta; i++ )
         if ( !strcmp( szSta, Sta[i].szStation ) &&
              !strcmp( szChn, Sta[i].szChannel ) &&
              !strcmp( szNet, Sta[i].szNetID ) )
         {
            for ( j=0; j<*piNumScreens; j++ )
               Trace[i].iStationDisp[j] = iScreen[j];
            break;
         }
      if ( i == iNumSta ) logit( "", "No match for %s %s\n", szSta, szChn );
   }
   fclose( hFile ); 
   
/* Flag unmatched stations in log file */
   for ( i=0; i<iNumSta; i++ )
   {
      for ( j=0; j<*piNumScreens; j++ )
         if ( Trace[i].iStationDisp[j] == 1 ) break;
      if ( j == *piNumScreens )
      {
         logit( "", "%s %s not in ini file\n", szSta, szChn );
         Trace[i].iStationDisp[0] = 1;            /* Force on in screen 1 */
      }
   }	    
   return 1;
}

      /******************************************************************
       *                       ResetOptions()                           *
       *                                                                *
       * Every X minutes, this functions is called to set some variables*
       * to defaults.                                                   *
       *                                                                *
       *  Arguments:                                                    *
       *   hwnd             Window handle of calling program            *
       *   iNumSta          Numberof stations in Sta                    *
       *   Trace            Array of trace control information          *
       *   piFiltDisplay    1->display filtered signal, 0->broadband    *
       *   piClipIt         1->Amplitude limit signal, 0->let it go     *
       *   piVScrollOffset  Vertical scrolling amount                   *
       *   hMenu            Menu handle                                 *
       *                                                                *
       ******************************************************************/
	   
void ResetOptions( HWND hwnd, int iNumSta, TRACE Trace[], int *piFiltDisplay,
                   int *piClipIt, int *piVScrollOffset, HMENU hMenu )
{
   int     i;
		
/* Turn trace toggle to on (Rt. mouse click) */		
   for ( i=0; i<iNumSta; i++ )
      if ( Trace[i].iDisplayStatus == 0 ) Trace[i].iDisplayStatus = 1;
		
/* Set trace clipping to on */
   *piClipIt = 1;
   HiliteMenuItem( hwnd, hMenu, IDM_CLIPIT, MF_BYCOMMAND | MFS_HILITE );
		
/* Set data display to filtered data */
   *piFiltDisplay = 1;
   CheckMenuItem( hMenu, IDM_FILTDISPLAY, MF_BYCOMMAND | MFS_CHECKED );
   CheckMenuItem( hMenu, IDM_BBDISPLAY, MF_BYCOMMAND | MFS_UNCHECKED );
   
/* Turn of screen hold option */   
   iHold = 0;
   HiliteMenuItem( hwnd, hMenu, IDM_DISP_HOLD, MF_BYCOMMAND | MFS_UNHILITE );

/* Turn integration window off if on */
   if( iChildA == 1 )
   {
      DestroyWindow( hwndChildA );
      iChildA = 0;
   } 
   
/* Reset vertical offset to 0 position */
   *piVScrollOffset = 0;
}

      /***********************************************************
       *             StationInformationDlgProc()                 *
       *                                                         *
       * This dialog procedure gives the user lat/lon/elevation  *
       * for a specified station along with all other known      *
       * information about that station such as full name,       *
       * station type, etc.  The station is specified by a popup *
       * window from a mouse click.                              *
       *                                                         *
       ***********************************************************/
	   
long WINAPI StationInformationDlgProc (HWND hwnd, UINT msg, UINT wParam, 
                                       long lParam)
{
   double  dFlatResp;           /* Response length for seis */
   char    szBuffer[64];	// Station description

   switch ( msg )
   {
      case WM_INITDIALOG:  /* Fill up Channel, Net, etc. */
         SetDlgItemText( hwnd, IDC_STATION, StaArray[iIndex].szStation );
         SetDlgItemText( hwnd, IDC_CHANNEL, StaArray[iIndex].szChannel );
         SetDlgItemText( hwnd, IDC_NET_ID, StaArray[iIndex].szNetID );
       	 SetDlgItemText( hwnd, IDC_OPERATING_AGENCY,
	                 GetOpAgency( StaArray[iIndex].szNetID ) );	 
		  
/* Fill up lat/lon/el/gain */
         gcvt( StaArray[iIndex].dLat, 6, szBuffer );
         strcat( szBuffer, "\0" );
         SetDlgItemText( hwnd, IDC_LAT_DESC, szBuffer );
         gcvt( StaArray[iIndex].dLon, 7, szBuffer );
         strcat( szBuffer, "\0" );
         SetDlgItemText( hwnd, IDC_LON_DESC, szBuffer );
	     gcvt( StaArray[iIndex].dElevation, 4, szBuffer );
         SetDlgItemText( hwnd, IDC_EL_DESC, szBuffer );
         gcvt( StaArray[iIndex].dSens, 12, szBuffer );
         SetDlgItemText( hwnd, IDC_GAIN_DESC, szBuffer );
		
/* Fill in seismometer type */
         SetDlgItemText (hwnd, IDC_SEIS_TYPE, GetSeisInfo(
                         StaArray[iIndex].iStationType, &dFlatResp ));
         if ( dFlatResp > 1.5 )
	     gcvt (dFlatResp, 4, szBuffer);
	   else
	     strcpy (szBuffer, "");	    
         SetDlgItemText (hwnd, IDC_CORNER_FREQ, szBuffer);
         SetFocus( hwnd );
         break;

      case WM_COMMAND:
         switch (LOWORD (wParam))
         {
            case IDOK:               /* Return to main screen */
                EndDialog (hwnd, IDOK);                  
                break;

            default:
                break;
         }
   }
return (0);
}

      /***********************************************************
       *                 TracePerScreenDlgProc()                 *
       *                                                         *
       * This dialog procedure lets the user set the number of   *
       * traces to be shown on the visible part of the screen    *
       * display.                                                *
       *                                                         *
       ***********************************************************/
	   
long WINAPI TracePerScreenDlgProc( HWND hwnd, UINT msg, UINT wParam,
                                   long lParam )
{
   int     iTemp;

   switch ( msg ) 
   {
      case WM_INITDIALOG:  /* Pre-set entry field to present number */
         SetDlgItemInt( hwnd, EF_NUMSTATODISP, Gparm.NumTracePerScreen, TRUE );
         SetFocus( hwnd );
         break;

      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {
            case IDOK:     /* Accept user input */
               Gparm.NumTracePerScreen =
                GetDlgItemInt( hwnd, EF_NUMSTATODISP, &iTemp, TRUE );
               if ( Gparm.NumTracePerScreen > Nsta )
                  Gparm.NumTracePerScreen = Nsta;
               if ( Gparm.NumTracePerScreen < 2 )
                  Gparm.NumTracePerScreen = 2;
               EndDialog (hwnd, IDOK);
               break;

            case IDCANCEL: /* Escape - don't accept input */
               EndDialog (hwnd, IDCANCEL);
               break;
		
            default:
               break;
         }
   }
   return 0;
}

      /***********************************************************
       *                      WndProc()                          *
       *                                                         *
       *  This dialog procedure processes messages from the      *
       *  windows screen and messages passed to it from elsewhere*
       *  (e.g., the message WM_NEW_DATA comes from WinMain when *
       *  a waveform has been picked out of the ring).  All      *
       *  display is performed in PAINT.  Menu options (on main  *
       *  menu and through right button clicks) control the      *
       *  display.  P-picks can be set with the left button.     *
       *  Traces can be erased with the right button on the      *
       *  trace.                                                 *
       *                                                         *
       ***********************************************************/
       
long WINAPI WndProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   static  int    cxScreen, cyScreen;        /* Window size in pixels */
   static  double dInc;                      /* Timing line increments */
   double  dPTime;          /* Temporary pick time */
   static  double dScreenTime;               /* # secs. of data on screen */
   static  double dScrollHPage, dScrollHBit; /* Hor. scroll amounts (sec) */
   HCURSOR hCursor;         /* Present cursor handle (hourglass or arrow) */
   static  HDC hdc;         /* Device context of screen */
   static  HANDLE  hMenu;   /* Handle to the menu */
   static  HMENU   hMenu2;  /* Handle to the station popup menu */
   static  HMENU   hMenu3;  /* Handle to the station popup menu */
   HYPO    Hypo;            /* Last located hypocenter from dummy file */
   int     i, j, iCnt;      /* Loop Counters */
   static  int  iChanIDOffset;/* Trace offset at left of screen */
   static  int  iClipIt;      /* 0->no trace clipping, 1->trace clipping */
   static  int  iDCOffset;    /* 0->DC offset not shown, 1->Show DC Offset */
   static  int  iDeltaPerLine, iAccumDelta;  /* Mousewheel variables */
   static  int  iFiltDisplay; /* 0->broadband data display; 1->filt disp (def)*/
   static  int  iNumScreens;          /* # of screen subsets (1 default) */
   int     iNumShown;         /* Number of traces displayed in DisplayTraces */
   static  int  iScreenToDisplay;     /* Set of stations to display */
   static  int  iScrollVertBuff;      /* Small vertical scroll amount (pxl) */
   static  int  iScrollVertPage;      /* Large vertical scroll amount (pxl) */				
   static  int  iShowChan;    /* 0->Channel not shown, 1->Show Channel */
   static  int  iShowNet;     /* 0->Network ID not shown, 1->Show Network ID */
   static  int  iVScrollOffset;       /* Vertical scoll bar setting */
   static  long lTitleFHt, lTitleFWd; /* Font height and width */
   static  int  iTitleOffset;         /* Trace offset at top of screen */
   PAINTSTRUCT ps;          /* Paint structure used in WM_PAINT command */
   POINT   pt, pt2;         /* Screen locations */
   RECT    rct;             /* RECT (rectangle structure) */
   char    szMessage[128];  /* Window caption message string */
   static  char szScreenName[MAX_SCREENS][32]; /* Array of screen subset names*/
   static  TRACE *Trace;    /* Array of structures which describe screen */   
   unsigned long ulScrollLines;  /* Mousewheel variables */

/* Respond to user input (menu choices, etc.) and system messages */
   switch ( msg )
   {
      case WM_CREATE:         /* Do this the first time through */
         hCursor = LoadCursor( NULL, IDC_ARROW );
         SetCursor( hCursor );
         hMenu = GetMenu( hwnd );
         hMenu2 = LoadMenu( hInstMain, "Station_Menu" );     /* Popup menu */
         hMenu3 = GetSubMenu( hMenu2, 0 );      /* Load station POPUP menu */
         dScreenTime = Gparm.TimePerScreen;
         iVScrollOffset = 0;       /* Start with no vertical scrolling */
         iDCOffset = 0;            /* DC Offset not shown */
         iShowChan = 0;            /* Channel not shown */
         iShowNet = 0;             /* Network ID not shown */
         CheckMenuItem( hMenu, IDM_DCOFFSET, MF_BYCOMMAND | MFS_UNCHECKED );
         CheckMenuItem( hMenu, IDM_SHOWCHAN, MF_BYCOMMAND | MFS_UNCHECKED );
         CheckMenuItem( hMenu, IDM_SHOWNET, MF_BYCOMMAND | MFS_UNCHECKED );
         HiliteMenuItem( hwnd, hMenu, IDM_DISP_HOLD,     /* Let screen auto- */
                         MF_BYCOMMAND | MFS_UNHILITE );  /* scroll           */
         iClipIt = 1;              /* Clip traces */
         HiliteMenuItem( hwnd, hMenu, IDM_CLIPIT, MF_BYCOMMAND | MFS_HILITE );
         iFiltDisplay = 1;         /* Start with filtered data on display */
         CheckMenuItem( hMenu, IDM_FILTDISPLAY, MF_BYCOMMAND | MFS_CHECKED );
         CheckMenuItem( hMenu, IDM_BBDISPLAY, MF_BYCOMMAND | MFS_UNCHECKED );
         dInc = 10.;

/* Allocate the trace array */
         Trace = (TRACE *) calloc( Nsta, sizeof (TRACE) );
         if ( Trace == NULL )
         {
            logit( "et", "develo: Cannot allocate the trace array\n" );
            PostMessage( hwnd, WM_DESTROY, 0, 0 );
            break;
         }
		 
/* Fill in Trace array */
         for ( i=0; i<Nsta; i++ )
         {
            Trace[i].iDisplayStatus = 1;
            Trace[i].dVScale = StaArray[i].dScaleFactor;
         }
		 
/* Read in station display parameters */		 
         if ( strlen( Gparm.ATPLineupFileBB ) < 2 )   /* Normal mode */
         {
            if ( (ReadScreenIni( &iNumScreens, Nsta, Trace, szScreenName, 
                                  StaArray, Gparm.ScreenFile )) == 0 )
            {
               iNumScreens = 1;       /* Default # of screen subsets to 1 */
               strcpy( szScreenName[0], "All Data" );
               for ( i=0; i<MAX_STATIONS; i++ ) Trace[i].iStationDisp[0] = 1;
            }
            iScreenToDisplay = 0;     /* Default is show p-picked stations */
         }
         else                         /* Player Mode */
         {
            iNumScreens = 1;       /* Default # of screen subsets to 1 */
            strcpy( szScreenName[0], "All Data" );
            for ( i=0; i<Nsta; i++ ) Trace[i].iStationDisp[0] = 1;
            iScreenToDisplay = 0;     /* Default is show p-picked stations */
         }
		 
/* Add sub-screens to menu */
         for ( i=0; i<iNumScreens; i++ )
         {
            InsertMenu( hMenu, IDM_FILTDISPLAY, MF_BYCOMMAND | MFT_STRING,
                        IDM_SCREEN+i, szScreenName[i] );
            CheckMenuItem( hMenu, IDM_SCREEN+i,
                           MF_BYCOMMAND | MFS_UNCHECKED );
         } 
         CheckMenuItem( hMenu, IDM_SCREEN+iScreenToDisplay,
                        MF_BYCOMMAND | MFS_CHECKED );
         InsertMenu( hMenu, IDM_FILTDISPLAY, MF_BYCOMMAND | MFT_SEPARATOR,
                     IDM_SCREEN+i+1, NULL );
         sprintf( szMessage, "%s - %s", szProcessName,
                  szScreenName[iScreenToDisplay] );
         SetWindowText( hwnd, szMessage );          /* Display the title */

/* Set horizontal scrolling amounts */		 
         dScrollHBit  = Gparm.TimePerScreen / 10.;
         dScrollHPage = Gparm.TimePerScreen / 2.;
	 
      case WM_SETTINGCHANGE:
         SystemParametersInfo( SPI_GETWHEELSCROLLLINES, 0, &ulScrollLines, 0 );
/* ulScrolllines usually equals 3 or 0 (for no scrolling)
   WHEEL_DELTA equals 120, so iDeltaPerLine will be 40 */
          iAccumDelta = 0;
          if ( ulScrollLines )
             iDeltaPerLine = WHEEL_DELTA / ulScrollLines;	
          else
             iDeltaPerLine = 0;	
          break;
		 
/* Get screen size in pixels, re-paint, and re-proportion screen */
      case WM_SIZE:
         cyScreen = HIWORD (lParam);
         cxScreen = LOWORD (lParam);
		 
/* Compute font size */
         lTitleFHt = cyScreen / 40;
         lTitleFWd = cxScreen / 140;
		 
/* Compute vertical scrolling amounts */		 
         iScrollVertBuff = cyScreen / Gparm.NumTracePerScreen;
         iScrollVertPage = cyScreen / 8;
		 
/* Compute offsets from top and left sides of screen */		 
         iTitleOffset = cyScreen / 30;
         iChanIDOffset = cxScreen / 18;
	 
/* Set scroll thumb positions */
         SetScrollRange( hwnd, SB_HORZ, 0, 100, FALSE );  /* 100/50 arbitrary */
         SetScrollPos( hwnd, SB_HORZ, 50, TRUE );
         SetScrollRange( hwnd, SB_VERT, 0, 100, FALSE );  /* 100/50 arbitrary */
         SetScrollPos( hwnd, SB_VERT, 0, TRUE );
		 
         InvalidateRect( hwnd, NULL, TRUE );    /* Force a re-PAINT */
         break;

      case WM_TIMER:		
         if ( wParam == ID_TIMER2 )           /* Time to reset options */
            ResetOptions( hwnd, Nsta, Trace, &iFiltDisplay, &iClipIt,
                          &iVScrollOffset, hMenu );   
         if ( wParam == ID_TIMER3 )           /* Time to read dummy file */
         {
            ReadDummyData( &Hypo, Gparm.DummyFile );
            GetPTimes( Nsta, PBuf, &Hypo );
         }
         break;

/* Respond to menu selections */
      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {
            case IDM_SCALE_8X:      /* 8 times existing trace scale */
               Trace[iIndex].dVScale *= 8.;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
	        
            case IDM_SCALE_4X:      /* 4 times existing trace scale */
               Trace[iIndex].dVScale *= 4.;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
	       
            case IDM_SCALE_2X:      /* 2 times existing trace scale */
               Trace[iIndex].dVScale *= 2.;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
	        
            case IDM_SCALE_HALFX:   /* 0.5 times existing trace scale */
               Trace[iIndex].dVScale *= 0.5;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
	        
            case IDM_SCALE_QUARTERX:/* 0.25 times existing trace scale */
               Trace[iIndex].dVScale *= 0.25;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
	       
            case IDM_SCALE_EIGHTHX: /* 0.125 times existing trace scale */
               Trace[iIndex].dVScale *= 0.125;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
	       
            case IDM_SCALE_DEFAULT: /* Force back to original */
               Trace[iIndex].dVScale = StaArray[iIndex].dScaleFactor;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
		
            case IDM_STA_DESC:     /* Dialog box with information on stn */
               DialogBox (hInstMain, "StationInformation", hwndWndProc,
                (DLGPROC) StationInformationDlgProc);
               break;
		
            case IDM_STADISP_ON:    /* Toggle display to on */
               Trace[iIndex].iDisplayStatus = 1;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
		
            case IDM_STADISP_OFF:   /* Toggle display to always off */
               Trace[iIndex].iDisplayStatus = 2;
               InvalidateRect( hwnd, NULL, TRUE );
               break;
		
            case IDM_ALARM_PAGE:    /* Toggle pager alarm on/off */
               if ( StaArray[iIndex].iAlarmPage == 1 )
                  StaArray[iIndex].iAlarmPage = 0;
               else if ( StaArray[iIndex].iAlarmPage == 0 )
                  StaArray[iIndex].iAlarmPage = 1;
               break;
		
            case IDM_ALARM_SPEAK:   /* Toggle speaker alarm on/off */
               if ( StaArray[iIndex].iAlarmSpeak == 1 )
                  StaArray[iIndex].iAlarmSpeak = 0;
               else if ( StaArray[iIndex].iAlarmSpeak == 0 )
                  StaArray[iIndex].iAlarmSpeak = 1;
               break;

            case IDM_HSCALE:        /* Change the horizontal scale */
               iScale = 1;          /*  on all traces              */
               if ( DialogBox( hInstMain, "HorizontalScale", hwndWndProc,
                   (DLGPROC) GlobalScaleDlgProc ) == IDOK )
               {
                  if ( iScale == 0 )           /* Set scale to original */
                     dScreenTime = Gparm.TimePerScreen;
                  else if ( iScale < 0 )       /* Change scale - increase time*/
                     dScreenTime *= (double) abs( iScale );
                  else if ( iScale > 0 )       /* Change scale - decrease time*/
                     dScreenTime /= (double) iScale; 
                  dScrollHBit = dScreenTime / 10.;
                  dScrollHPage = dScreenTime / 2.;
                  InvalidateRect( hwnd, NULL, TRUE );
               }
               break;

            case IDM_VSCALE:        /* Change the vertical scale */
               iScale = 1;          /*  on all traces            */
               if ( DialogBox( hInstMain, "GlobalVerticalScale", hwndWndProc,
                   (DLGPROC) GlobalScaleDlgProc ) == IDOK )
               {
                  if ( iScale == 0 )           /* Reset to original */
                     for ( i=0; i<Nsta; i++ )
                        Trace[i].dVScale = StaArray[i].dScaleFactor;
                  else if ( iScale > 0 )       /* Change scale on all-increase*/
                     for ( i=0; i<Nsta; i++ )  
                        Trace[i].dVScale *= (double) iScale;
                  else if ( iScale < 0 )       /* Change scale on all-decrease*/
                     for ( i=0; i<Nsta; i++ )  
                        Trace[i].dVScale /= (double) abs( iScale );
                  InvalidateRect( hwnd, NULL, TRUE );
               }
               break;
		
/* This menu option lets the user change the number of traces
   which are squished into the visible (vertically) part of the
   screen display. */
            case IDM_TRACEPERSCREEN:	
               if ( DialogBox( hInstMain, "TracePerScreen", hwndWndProc, 
                   (DLGPROC) TracePerScreenDlgProc ) == IDOK )
               {
                  iScrollVertBuff = cyScreen / Gparm.NumTracePerScreen;
                  iScrollVertPage = cyScreen / 8;
                  InvalidateRect( hwnd, NULL, TRUE );
               }
               break;
			   
            case IDM_DCOFFSET:         /* DC Offset display on/off */
               if ( iDCOffset == 1 )
                   {
                   iDCOffset = 0;
                   CheckMenuItem( hMenu, IDM_DCOFFSET, 
                                  MF_BYCOMMAND | MFS_UNCHECKED );
                   }
                 else if ( iDCOffset == 0 )
                   {
                   iDCOffset = 1;
                   CheckMenuItem( hMenu, IDM_DCOFFSET, 
                                  MF_BYCOMMAND | MFS_CHECKED );
                   }
               InvalidateRect( hwnd, NULL, TRUE );
               break;
			   
            case IDM_SHOWCHAN:         /* Channel display on/off */
               if ( iShowChan == 1 )
                   {
                   iShowChan = 0;
                   CheckMenuItem( hMenu, IDM_SHOWCHAN, 
                                  MF_BYCOMMAND | MFS_UNCHECKED );
                   }
                 else if ( iShowChan == 0 )
                   {
                   iShowChan = 1;
                   CheckMenuItem( hMenu, IDM_SHOWCHAN, 
                                  MF_BYCOMMAND | MFS_CHECKED );
                   }
               InvalidateRect( hwnd, NULL, TRUE );
               break;
			   
            case IDM_SHOWNET:          /* Network ID display on/off */
               if ( iShowNet == 1 )
                   {
                   iShowNet = 0;
                   CheckMenuItem( hMenu, IDM_SHOWNET, 
                                  MF_BYCOMMAND | MFS_UNCHECKED );
                   }
                 else if ( iShowNet == 0 )
                   {
                   iShowNet = 1;
                   CheckMenuItem( hMenu, IDM_SHOWNET, 
                                  MF_BYCOMMAND | MFS_CHECKED );
                   }
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_FILTDISPLAY:      /* Let trace display filtered data */
               iFiltDisplay = 1;
               CheckMenuItem( hMenu, IDM_FILTDISPLAY,    /* Menu checkmarks */
                              MF_BYCOMMAND | MFS_CHECKED );
               CheckMenuItem( hMenu, IDM_BBDISPLAY, 
                              MF_BYCOMMAND | MFS_UNCHECKED );
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_BBDISPLAY:        /* Let trace display broadband data */
               iFiltDisplay = 0;
               CheckMenuItem( hMenu, IDM_FILTDISPLAY,    /* Menu checkmarks */
                              MF_BYCOMMAND | MFS_UNCHECKED );
               CheckMenuItem( hMenu, IDM_BBDISPLAY, 
                              MF_BYCOMMAND | MFS_CHECKED );
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_SCREEN:      /* Change screen subset that is on display */
            case IDM_SCREEN+1:
            case IDM_SCREEN+2:
            case IDM_SCREEN+3:
            case IDM_SCREEN+4:
            case IDM_SCREEN+5:
            case IDM_SCREEN+6:
            case IDM_SCREEN+7:
            case IDM_SCREEN+8:
            case IDM_SCREEN+9:
            case IDM_SCREEN+10:
            case IDM_SCREEN+11:
            case IDM_SCREEN+12:
            case IDM_SCREEN+13:
            case IDM_SCREEN+14:
            case IDM_SCREEN+15:
            case IDM_SCREEN+16:
            case IDM_SCREEN+17:
            case IDM_SCREEN+18:
            case IDM_SCREEN+19:
            case IDM_SCREEN+20:
               iScreenToDisplay = LOWORD (wParam) - IDM_SCREEN;
               for ( i=0; i<iNumScreens; i++ )      /* Set checkmarks */
                  CheckMenuItem( hMenu, IDM_SCREEN+i,
                                 MF_BYCOMMAND | MFS_UNCHECKED );
               CheckMenuItem( hMenu, LOWORD (wParam),
                              MF_BYCOMMAND | MFS_CHECKED );
               sprintf( szMessage, "%s - %s", szProcessName,
                        szScreenName[LOWORD (wParam)-IDM_SCREEN] );
               SetWindowText( hwnd, szMessage );    /* Display the title */
               iVScrollOffset = 0;                  /* Show top again */
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_CLIPIT:     /* Turn iClipIt flag on or off (this */
               if ( iClipIt )    /*  amplitude limits the display)    */
               {
                  iClipIt = 0;
                  HiliteMenuItem( hwnd, hMenu, IDM_CLIPIT, 
                                  MF_BYCOMMAND | MFS_UNHILITE );
               }
               else 
               {
                  iClipIt = 1;
                  HiliteMenuItem( hwnd, hMenu, IDM_CLIPIT, 
                                  MF_BYCOMMAND | MFS_HILITE );
               }
               break;

            case IDM_DISP_HOLD:   /* Stop screen from updating */
               if ( iHold )
               {
                  iHold = 0;
                  HiliteMenuItem( hwnd, hMenu, IDM_DISP_HOLD, 
                                  MF_BYCOMMAND | MFS_UNHILITE );
               }
               else          
               {
                  iHold = 1;
                  HiliteMenuItem( hwnd, hMenu, IDM_DISP_HOLD, 
                                  MF_BYCOMMAND | MFS_HILITE );
               }
               break;

            case IDM_REFRESH:       // redraw the screen
               InvalidateRect( hwnd, NULL, TRUE );
               break;

/* Show Mwp calculations (velocity, disp., and integrated disp.) in a child
   window */
            case IDM_DISPLAYMWP:
               if ( iChildA )  /* If this child is already running, stop it */
               {
                  DestroyWindow( hwndChildA );
                  iChildA = 0;
               }
			   
/* Create child window in which Mwp amps. are shown */
               hwndChildA = CreateWindow( szChildAName, /* Child Name */
                szChildAName,           /* Initial title bar caption */
                WS_OVERLAPPEDWINDOW | WS_CHILDWINDOW,   /* Std style */
                cxScreen/5,             /* top left x starting location */
                cyScreen/40,            /* top left y starting location */
                cxScreen/2,             /* initial screen width in pixels */
                2*cyScreen/5,           /* initial screen height in pixels */
                hwnd,                   /* Parent window */
                (HMENU) ID_CHILD_MWP_DISP,  /* Child window ID */
                                        /* Let Windows supply the Instance */
                (HANDLE) GetWindowLong( hwnd, GWL_HINSTANCE ),
                NULL );                 /* No extra data to pass in */
               if ( hwndChildA == NULL )/* window not created */
               {
                  logit( "t", "Integration window not created\n" );
                  break;
               }
               ShowWindow( hwndChildA, SW_SHOW );   /* Show the window */
               UpdateWindow( hwndChildA );    /* Force an initial PAINT call */
               SetActiveWindow( hwndChildA ); /* Give it the input focus */
               iChildA = 1;
               break;

/* Set the integration window length and re-compute Mwp (Max window length
   is that which was set in .d file) */
            case IDM_MWPWINDOW:
               if ( DialogBox( hInstMain, "MwpTime", hwndWndProc, 
                   (DLGPROC) MwpTimeDlgProc ) == IDCANCEL ) break;
               for ( i=0; i<Nsta; i++ )
                  if ( PBuf[i].dPTime > 0. && StaArray[i].iComputeMwp == 1 )
                  {
                     RequestSpecificMutex( &mutsem1 );   /* Protect buf write */
                     StaArray[i].iPickStatus = 0;
                     if ( StaArray[i].dEndTime-PBuf[i].dPTime >=
                         (double) Gparm.MwpSeconds )
                        AutoMwp( &StaArray[i], &PBuf[i], Gparm.MwpSigNoise,
                                  Gparm.MwpSeconds, 1 );
                     else StaArray[i].iPickStatus = 1;
                     ReleaseSpecificMutex( &mutsem1 );   /* Release sem */
                     ReportPick( &PBuf[i], &Gparm, &Ewh );                     
                  }
                  InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_EXIT_MAIN:     /* Send DESTROY message to itself */
               PostMessage( hwnd, WM_DESTROY, 0, 0 );
               break;          
         }
         break ;
       	
/* Have update traces, show them on screen */	
      case WM_NEW_DATA:	
         if ( strlen( Gparm.ATPLineupFileBB ) > 2 )   /* Player mode!!! */
            if ( iNewPlayerData == 1 )	    
            {
               iNewPlayerData = 0;
               for ( i=0; i<Nsta; i++ )
               {
                  Trace[i].iDisplayStatus = 1;
                  Trace[i].dVScale = StaArray[i].dScaleFactor;
               } 
            }
         InvalidateRect( hwnd, NULL, TRUE );
         break;
       	
/* Have new P-picks, show them on screen */	
      case WM_NEW_PPICKS:	
         InvalidateRect( hwnd, NULL, TRUE );
         break;

/* Remove/Include P from locations or make a pick */
      case WM_LBUTTONDOWN:
         GetCursorPos( &pt );            /* Get the cursor location */
         ScreenToClient( hwnd, &pt );    /* Put in client window coords */
		 
/* Which screen index is this y value */
         j = ((pt.y-iTitleOffset-iVScrollOffset+(cyScreen-iTitleOffset)/
              (Gparm.NumTracePerScreen*2)) * Gparm.NumTracePerScreen) /
              (cyScreen-iTitleOffset);
         iCnt = 0;
         for( i=0; i<Nsta; i++ )            /* Which station was clicked? */
            if( Trace[i].iStationDisp[iScreenToDisplay] )
            {
               if ( iCnt == j ) break;
               iCnt++;
            }
			
/* If click was within station name part of screen, eliminate if from solution;
   otherwise it is a new P-pick */			
         if ( i >= 0 && i < Nsta && fabs( StaArray[i].dAveLDCRaw ) > 0. )
            if ( pt.x > iChanIDOffset )     /* P-pick made on this station */
            {
               dPTime = (dLastEndTime-dScreenTime) + dScreenTime*
                        ((double) (pt.x-iChanIDOffset)/
                         (double) (cxScreen-iChanIDOffset));
			
/* Either create a new P-pick entry, or adjust the existing one */
               if ( fabs( (dPTime-PBuf[i].dPTime) ) < Gparm.PTimeDelta )
               {
                  RequestSpecificMutex( &mutsem1 );   /* Protect buf write */
                  PBuf[i].dPTime = dPTime - StaArray[i].dTimeCorrection;
                  if ( StaArray[i].dSens > 0. )					 
                  {
/* Compute Mb/Ml parameters */					 
                     GetMag( &StaArray[i], &PBuf[i], &Gparm);
/* If there is enough data, compute Mwp parameters */						
                     if ( StaArray[i].dEndTime-PBuf[i].dPTime >=
                         (double) Gparm.MwpSeconds &&
                          StaArray[i].iComputeMwp == 1 ) 
                        AutoMwp( &StaArray[i], &PBuf[i], Gparm.MwpSigNoise,
                                  Gparm.MwpSeconds, 1 );
                     else if ( StaArray[i].iComputeMwp == 1 )
                        StaArray[i].iPickStatus = 1;
                  }
                  ReleaseSpecificMutex( &mutsem1 );   /* Release sem */
                  ReportPick( &PBuf[i], &Gparm, &Ewh );                     
               }
               else          /* Re-init and assign new pick ID */
               {
                  RequestSpecificMutex( &mutsem1 );   /* Protect buf write */
                  InitP( &PBuf[i] );
                  FillP( &PBuf[i], &StaArray[i],
                          dPTime-StaArray[i].dTimeCorrection, lPickCounter );
                  lPickCounter++;
                  if ( lPickCounter == 20000 ) lPickCounter = 10000;
                  if ( StaArray[i].dSens > 0. )
                  {
/* Compute Mb/Ml parameters */					 
                     GetMag( &StaArray[i], &PBuf[i], &Gparm);
/* If there is enough data, compute Mwp parameters */						
                     if ( StaArray[i].dEndTime-PBuf[i].dPTime >=
                         (double) Gparm.MwpSeconds &&
                          StaArray[i].iComputeMwp == 1 )
                        AutoMwp( &StaArray[i], &PBuf[i], Gparm.MwpSigNoise,
                                  Gparm.MwpSeconds, 1 );
                     else if ( StaArray[i].iComputeMwp == 1 )
                        StaArray[i].iPickStatus = 1;
                  }
                  ReleaseSpecificMutex( &mutsem1 );   /* Release sem */
                  ReportPick( &PBuf[i], &Gparm, &Ewh );                     
               }
               InvalidateRect( hwnd, NULL, TRUE );
            }
            else                         /* X out from solution */
            {
/* Set display menu item check */
               if ( PBuf[i].dPTime > 0. )
               {
                  RequestSpecificMutex( &mutsem1 );   /* Protect buf write */
                  if      ( PBuf[i].iUseMe > 0 ) PBuf[i].iUseMe = -1;
                  else                           PBuf[i].iUseMe = 2;
                  ReleaseSpecificMutex( &mutsem1 );   /* Release sem */
                  ReportPick( &PBuf[i], &Gparm, &Ewh );
                  InvalidateRect( hwnd, NULL, TRUE );
               }
            }
         break;

/* Toggle on/off for trace display, or bring up popup menu */
      case WM_RBUTTONDOWN:
         GetCursorPos( &pt );            /* Get the cursor location */
         pt2.x = pt.x;
         pt2.y = pt.y;
         ScreenToClient( hwnd, &pt );    /* Put in client window coords */
		 
/* Which screen index is this y value */
         j = ((pt.y-iTitleOffset-iVScrollOffset+(cyScreen-iTitleOffset)/
              (Gparm.NumTracePerScreen*2)) * Gparm.NumTracePerScreen) /
              (cyScreen-iTitleOffset);
         iCnt = 0;
         for( i=0; i<Nsta; i++ )            /* Which station was clicked? */
            if( Trace[i].iStationDisp[iScreenToDisplay] )
            {
               if ( iCnt == j ) break;
               iCnt++;
            }
			
/* If click was within station name part of screen, bring up menu; otherwise
   toggle trace on or off */			
//         if( i >= 0 && i < Nsta && fabs( StaArray[i].dAveLDCRaw ) > 0. )
         if ( i >= 0 && i < Nsta )
            if ( pt.x > iChanIDOffset )     /* Toggle trace on/off */
               {
                  if ( Trace[i].iDisplayStatus == 0 )
                     Trace[i].iDisplayStatus = 1;
                  else if ( Trace[i].iDisplayStatus == 1 )
                     Trace[i].iDisplayStatus = 0;
                  InvalidateRect( hwnd, NULL, TRUE );
               }
               else                         /* Bring up station popup menu */
               {
                  iIndex = i;               /* Save for future scaling */
	    
/* Set display menu item check */
                  if( Trace[i].iDisplayStatus == 0 ||
                      Trace[i].iDisplayStatus == 1 ) 
                  {                                       /* On */
                     CheckMenuItem( hMenu3, IDM_STADISP_ON,
                                    MF_BYCOMMAND | MFS_CHECKED );
                     CheckMenuItem( hMenu3, IDM_STADISP_OFF,
                                    MF_BYCOMMAND | MFS_UNCHECKED );
                  }
                  else                                    /* Off */
                  {
                     CheckMenuItem( hMenu3, IDM_STADISP_ON,
                                    MF_BYCOMMAND | MFS_UNCHECKED );
                     CheckMenuItem( hMenu3, IDM_STADISP_OFF,
                                    MF_BYCOMMAND | MFS_CHECKED );
                  }
	    
/* Set alarm menu items check */
                  if ( StaArray[i].iAlarmPage == 1 )      /* On */
                     CheckMenuItem( hMenu3, IDM_ALARM_PAGE,
                                    MF_BYCOMMAND | MFS_CHECKED );
                  else                                    /* Off */
                     CheckMenuItem( hMenu3, IDM_ALARM_PAGE,
                                    MF_BYCOMMAND | MFS_UNCHECKED );
                  if ( StaArray[i].iAlarmSpeak == 1 )     /* On */
                     CheckMenuItem( hMenu3, IDM_ALARM_SPEAK,
                                    MF_BYCOMMAND | MFS_CHECKED );
                  else                                    /* Off */
                     CheckMenuItem( hMenu3, IDM_ALARM_SPEAK,
                                    MF_BYCOMMAND | MFS_UNCHECKED );
                  TrackPopupMenu( hMenu3, 0, pt2.x, pt2.y, 0, hwnd, NULL );
               }
         break;
	 
/* Respond to mouse roller */                          
      case WM_MOUSEWHEEL:
         if ( iDeltaPerLine == 0 ) break;
         iAccumDelta += (short) HIWORD ( wParam );
  	 while ( iAccumDelta >= iDeltaPerLine )
	 {
            SendMessage( hwnd, WM_VSCROLL, SB_LINEUP, 0 );
	    iAccumDelta -= iDeltaPerLine;
	 }
	 while ( iAccumDelta <= -iDeltaPerLine )
	 {
            SendMessage( hwnd, WM_VSCROLL, SB_LINEDOWN, 0 );
	    iAccumDelta += iDeltaPerLine;
	 }
	 break;

/* Fill in screen display with traces, Ps, mag info, and stn names */
      case WM_PAINT:
         hdc = BeginPaint( hwnd, &ps );         /* Get device context */
         GetClientRect( hwnd, &rct );
         FillRect( hdc, &rct, (HBRUSH) GetStockObject( WHITE_BRUSH ) );
         if ( dLastEndTime > 0. )
         {
            RequestSpecificMutex( &mutsem2 );/* Semaphore protect display */
            DisplayTraces( hdc, StaArray, Trace, Gparm.NumTracePerScreen, Nsta,
                           iScreenToDisplay, iFiltDisplay, iClipIt, cxScreen,
                           cyScreen, iVScrollOffset, iTitleOffset,
                           iChanIDOffset, dLastEndTime, dScreenTime,
                           &iNumShown );
            DisplayExpectedP( hdc, Trace, Gparm.NumTracePerScreen, Nsta,
                           iScreenToDisplay, cxScreen, cyScreen, iVScrollOffset,
                           iTitleOffset, iChanIDOffset, dLastEndTime,
                           dScreenTime, lTitleFHt, PBuf );
            DisplayPPicks( hdc, Trace, Gparm.NumTracePerScreen, Nsta,
                           iScreenToDisplay, cxScreen, cyScreen, iVScrollOffset,
                           iTitleOffset, iChanIDOffset, dLastEndTime,
                           dScreenTime, lTitleFHt, PBuf );
            DisplayChannelID (hdc, StaArray, Trace, Nsta, iScreenToDisplay,
                              lTitleFHt, lTitleFWd, cxScreen, cyScreen,
                              Gparm.NumTracePerScreen, iVScrollOffset,
                              iTitleOffset, iAhead );
            DisplayTimeLines( hdc, lTitleFHt, lTitleFWd, cxScreen, cyScreen,
                              iChanIDOffset, dLastEndTime, dScreenTime, dInc );
            DisplayMwp( hdc, Trace, Gparm.NumTracePerScreen, Nsta,
                        iScreenToDisplay, cxScreen, cyScreen, iVScrollOffset,
                        iTitleOffset, iChanIDOffset, dLastEndTime, dScreenTime,
                        PBuf );          
            DisplayKOPs( hdc, Trace, Nsta, iScreenToDisplay, cxScreen,
                         cyScreen, iVScrollOffset, iTitleOffset, iChanIDOffset,
                         PBuf, lTitleFHt, lTitleFWd, Gparm.NumTracePerScreen );
            if ( iDCOffset == 1 ) DisplayDCOffset (hdc, StaArray, Trace, Nsta,
                                   iScreenToDisplay, lTitleFHt, lTitleFWd,
                                   cyScreen, Gparm.NumTracePerScreen,
                                   iVScrollOffset, iTitleOffset, iChanIDOffset );
            if ( iShowChan == 1 ) DisplayChannel (hdc, StaArray, Trace, Nsta,
                                   iScreenToDisplay, lTitleFHt, lTitleFWd,
                                   cyScreen, Gparm.NumTracePerScreen,
                                   iVScrollOffset, iTitleOffset, iChanIDOffset );
            if ( iShowNet == 1 ) DisplayNetwork (hdc, StaArray, Trace, Nsta,
                                  iScreenToDisplay, lTitleFHt, lTitleFWd,
                                  cyScreen, Gparm.NumTracePerScreen,
                                  iVScrollOffset, iTitleOffset, iChanIDOffset );
            ReleaseSpecificMutex( &mutsem2 );/* Let WThread have sem */
            SetScrollPos( hwnd, SB_VERT, (int) (((double) iVScrollOffset*-1.) /
                          ((cyScreen-iTitleOffset) /
                          (double) Gparm.NumTracePerScreen *
                          (double) iNumShown) * 100.), TRUE );
         }
         EndPaint( hwnd, &ps );
         break;

/* Horizontal scroll message */
      case WM_HSCROLL:
         if ( LOWORD (wParam) != SB_ENDSCROLL )
         {
            HorizontalScroll( LOWORD (wParam), &dLastEndTime,
                              dScrollHBit, dScrollHPage );
            InvalidateRect( hwnd, NULL, TRUE );
         }
         break;
	
/* Vertical scroll message */	
      case WM_VSCROLL:
         if ( LOWORD (wParam) == SB_LINEUP )
            iVScrollOffset += iScrollVertBuff;
         else if ( LOWORD (wParam) == SB_PAGEUP )
            iVScrollOffset += iScrollVertPage;
         else if ( LOWORD (wParam) == SB_LINEDOWN )
            iVScrollOffset -= iScrollVertBuff;
         else if ( LOWORD (wParam) == SB_PAGEDOWN )
            iVScrollOffset -= iScrollVertPage;
         InvalidateRect( hwnd, NULL, TRUE );
         break;

/* Close up shop and return */
      case WM_DESTROY:
         logit( "", "WM_DESTROY posted\n" );
         KillTimer( hwndWndProc, ID_TIMER2 );
         KillTimer( hwndWndProc, ID_TIMER3 );
         PostQuitMessage( 0 );
         break;

      default:
         return ( DefWindowProc( hwnd, msg, wParam, lParam ) );
   }
return 0;
}
                              
      /*********************************************************
       *                     WThread()                         *
       *                                                       *
       *  This thread gets earthworm waveform messages.        *
       *                                                       *
       *********************************************************/
	   
thr_ret WThread( void *dummy )
{
   int           i;
   long          InBufl;          /* Memory size!!! */
   char          line[40];        /* Heartbeat message */
   int           lineLen;         /* Length of heartbeat message */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */
   long          RawBufl;         /* Raw data buffer size (for Mwp) */

/* Loop to read waveform messages
   ******************************/
   while ( tport_getflag( &Gparm.InRegion ) != TERMINATE )
   {
      long    lGapSize;         /* Number of missing samples (integer) */
      static  time_t  now;      /* Current time */
      int     rc;               /* Return code from tport_getmsg() */
      static  STATION *Sta;     /* Pointer to the station being processed */
      static  PPICK   *pPBuf;   /* Pointer to the PPick being processed */
      char    szType[3];        /* Incoming data format type */
	  
/* Send a heartbeat to the transport ring
   **************************************/
      time( &now );
      if ( (now - then) >= Gparm.HeartbeatInt )
      {
         then = now;
         sprintf( line, "%d %d\n", now, myPid );
         lineLen = strlen( line );
         if ( tport_putmsg( &Gparm.InRegion, &hrtlogo, lineLen, line ) !=
              PUT_OK )
         {
            logit( "et", "develo: Error sending heartbeat." );
            break;
         }
		 
/* Also, this is a good place to clear PBuf of old P data
   ******************************************************/
         for ( i=0; i<Nsta; i++ )
            if ( (now-(int) PBuf[i].dPTime)/60 > Gparm.MinutesInBuff &&
                  PBuf[i].dPTime > 0. )
            {
               RequestSpecificMutex( &mutsem1 );   /* Sem protect buffer writes */
               InitP( &PBuf[i] );
               StaArray[i].iPickStatus = 0;
               PBuf[i].dLat = StaArray[i].dLat;    /* Reset lat/lon */
               PBuf[i].dLon = StaArray[i].dLon;
               GeoCent( (LATLON *) &PBuf[i] );
               GetLatLonTrig( (LATLON *) &PBuf[i] );
               ReleaseSpecificMutex( &mutsem1 );   /* Let someone else have sem */
            }
      }
      
/* Get a waveform from transport region
   ************************************/
      rc = tport_getmsg( &Gparm.InRegion, &getlogoW, 1, &logo, &MsgLen,
                         WaveBuf, MAX_TRACEBUF_SIZ);

      if ( rc == GET_NONE )
      {
         sleep_ew( 100 );
         continue;
      }

      if ( rc == GET_NOTRACK )
         logit( "et", "develo: Tracking error.\n");

      if ( rc == GET_MISS_LAPPED )
         logit( "et", "develo: Got lapped on the ring.\n");

      if ( rc == GET_MISS_SEQGAP )
         logit( "et", "develo: Gap in sequence numbers.\n");

      if ( rc == GET_MISS )
         logit( "et", "develo: Missed messages.\n");

      if ( rc == GET_TOOBIG )
      {
         logit( "et", "develo: Retrieved message too big (%d) for msg.\n",
                MsgLen );
         continue;
      }

/* If necessary, swap bytes in the message
   ***************************************/
      if ( WaveMsgMakeLocal( WaveHead ) < 0 )
      {
         logit( "et", "develo: Unknown waveform type.\n" );
         continue;
      }
	  
/* If sample rate is 0, get out of here before it kills program
   ************************************************************/
      if ( WaveHead->samprate == 0. )
      {
         logit( "", "Sample rate=0., %s %s\n", WaveHead->sta, WaveHead->chan );
         continue;
      }
      
      RequestSpecificMutex( &mutsem2 );   /* Protect display */

/* If we are in the ATPlayer version of develo, see if we should re-init!!!
   *********************************************************************/
      if ( strlen( Gparm.ATPLineupFileBB ) > 2 )     /* Then we are */
         if ( WaveHead->starttime-(int) dLastEndTime > 3600 ) /* Big gap */
         {
            iNewPlayerData = 1;
            Nsta = ReadLineupFile( Gparm.ATPLineupFileBB, StaArray );
            if ( Nsta < 2 )
            {
               logit( "", "Bad Lineup File read %s\n", Gparm.ATPLineupFileBB );
               ReleaseSpecificMutex( &mutsem2 );   /* Release sem */
               continue;
            }	    
            for ( i=0; i<Nsta; i++ )
	    {
               StaArray[i].iFirst = 1; 
               InitVar( &StaArray[i] );	  
               free( StaArray[i].pdRawDispData );
               free( StaArray[i].pdRawIDispData );
               free( StaArray[i].plRawCircBuff );
               free( StaArray[i].plFiltCircBuff );
            }
/* Re-Allocate and init the P-pick buffer
   **************************************/
            if ( PBuf != NULL ) free( PBuf );
            InBufl = sizeof( PPICK ) * Nsta; 
            PBuf = (PPICK *) malloc( (size_t) InBufl );
            if ( PBuf == NULL )
            {
               logit( "et", "develo: Cannot re-allocate ppick buffer\n");
               ReleaseSpecificMutex( &mutsem2 );   /* Release sem */
               continue;
	    }
            for ( i=0; i<Nsta; i++ ) InitP( &PBuf[i] );
            for ( i=0; i<Nsta; i++ )       /* Fill lat/lon part of structure */
            {
               PBuf[i].dLat = StaArray[i].dLat;
               PBuf[i].dLon = StaArray[i].dLon;
               GeoCent( (LATLON *) &PBuf[i] );
               GetLatLonTrig( (LATLON *) &PBuf[i] );
            }		 
         } 	 

/* Look up SCN number in the station list
   **************************************/
      Sta = NULL;								  
      for ( i=0; i<Nsta; i++ )
         if ( !strcmp( WaveHead->sta,  StaArray[i].szStation ) &&
              !strcmp( WaveHead->chan, StaArray[i].szChannel ) &&
              !strcmp( WaveHead->net,  StaArray[i].szNetID ) )
         {
            Sta = (STATION *) &StaArray[i];
            pPBuf = (PPICK *) &PBuf[i];
            break;
         }

      if ( Sta == NULL )      /* SCN not found */
      {
         ReleaseSpecificMutex( &mutsem2 );   /* Release sem */
         continue;
      }
		 
/* Check if the time stamp is reasonable.  If it is ahead of the present
   1/1/70 time, it is not reasonable. (+1. to account for int).  If it is
   more than TOO_OLD seconds behind present, ignore it.
   **********************************************************************/
      if ( WaveHead->endtime > (double) now+1. ||
           WaveHead->endtime < (double) (now-TOO_OLD_DEV) ) 
      {
         if ( WaveHead->endtime > (double) now+1. ) iAhead[i] = 1;
         logit( "t", "%s %s bad time: endtime (%lf) present (%ld)\n",
                Sta->szStation, Sta->szChannel, WaveHead->endtime, now );
         ReleaseSpecificMutex( &mutsem2 );   /* Release sem */
         continue;
      }
      iAhead[i] = 0;

/* Do this the first time we get a message with this SCN
   *****************************************************/
      if ( Sta->iFirst == 1 )
      {
         logit( "", "Init %s %s\n", Sta->szStation, Sta->szChannel );	  
         Sta->iFirst = 0;
         Sta->iPickStatus = 0;
         Sta->dEndTime = WaveHead->starttime - 1./WaveHead->samprate;
         Sta->dSampRate = WaveHead->samprate;
         ResetFilter( Sta );
		 
/* Allocate memory for Mwp displacement data buffer 
   ************************************************/
         RawBufl = sizeof (double) * (long) (WaveHead->samprate+0.001) *
		           (Gparm.MwpSeconds+20);
         if ( RawBufl/sizeof (double) > MAXMWPARRAY )
         {
            logit( "et", "Mwp array too large for %s\n", Sta->szStation );
            PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
            return;
         }
         Sta->pdRawDispData = (double *) malloc( (size_t) RawBufl );
         if ( Sta->pdRawDispData == NULL )
         {
            logit( "et", "develo: Can't allocate disp. data buff for %s\n",
			       Sta->szStation );
            PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
            return;
         }
		 
/* Allocate memory for Mwp integrated displacement data buffer 
   ***********************************************************/
         Sta->pdRawIDispData = (double *) malloc( (size_t) RawBufl );
         if ( Sta->pdRawIDispData == NULL )
         {
            logit( "et", "develo: Cannot allocate int. disp. data buffer for %s\n",
			       Sta->szStation );
            PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
            return;
         }
		 		 
/* Allocate memory for raw circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples)
   ********************************/
         Sta->lRawCircSize =
          (long) (WaveHead->samprate*(double)Gparm.MinutesInBuff*60.+0.1);
         RawBufl = sizeof (long) * Sta->lRawCircSize;
         Sta->plRawCircBuff = (long *) malloc( (size_t) RawBufl );
         if ( Sta->plRawCircBuff == NULL )
         {
            logit( "et", "develo: Can't allocate raw circ buffer for %s\n",
			       Sta->szStation );
            PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
            return;
         }
		 		 
/* Allocate memory for filt. circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples)
   ********************************/
         Sta->plFiltCircBuff = (long *) malloc( (size_t) RawBufl );
         if ( Sta->plFiltCircBuff == NULL )
         {
            logit( "et", "develo: Can't allocate filt circ buffer for %s\n",
			       Sta->szStation );
            PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
            return;
         }
      }      
      
/* If data is not in order, throw it out
   *************************************/
      if ( Sta->dEndTime >= WaveHead->starttime )
      {
         if ( Gparm.Debug ) logit( "e", "%s out of order\n", Sta->szStation );
         Sta->iPickStatus = 0;
         ReleaseSpecificMutex( &mutsem2 );   /* Release sem */
         continue;
      }

/* If the samples are shorts, make them longs
   ******************************************/
      strcpy( szType, WaveHead->datatype );
      if ( (strcmp( szType, "i2" ) == 0) || (strcmp( szType, "s2") == 0) )
         for ( i=WaveHead->nsamp-1; i>-1; i-- )
            WaveLong[i] = (long) WaveShort[i];
			
/* Compute the number of samples since the end of the previous message.
   If (lGapSize == 1), no data has been lost between messages.
   If (1 < lGapSize <= 2), go ahead anyway.
   If (lGapSize > 2), re-initialize filter variables.
   *******************************************************************/
      lGapSize = (long) (WaveHead->samprate *
                        (WaveHead->starttime-Sta->dEndTime) + 0.5);

/* Announce gaps
   *************/
      if ( lGapSize > 2 )
      {
         int      lineLen;
         time_t   errTime;
         char     errmsg[80];
         MSG_LOGO logo;

         time( &errTime );
         sprintf( errmsg,
               "%d 1 Found %4d sample gap. Restarting station %-5s%-2s%-3s\n",
               errTime, lGapSize, Sta->szStation, Sta->szNetID,
               Sta->szChannel );
         lineLen = strlen( errmsg );
         logo.type   = Ewh.TypeError;
         logo.mod    = Gparm.MyModId;
         logo.instid = Ewh.MyInstId;
         tport_putmsg( &Gparm.InRegion, &logo, lineLen, errmsg );
         if ( Gparm.Debug )
            logit( "t", "develo: Restarting %-5s%-2s %-3s. lGapSize = %d\n",
                     Sta->szStation, Sta->szNetID, Sta->szChannel, lGapSize ); 

/* For big gaps reset filter and 0-out gapped region in buffer
   ***********************************************************/
         ResetFilter( Sta );
         Sta->dSumLDCRaw = 0.;
         Sta->dAveLDCRaw = 0.;
         Sta->dSumLDC = 0.;
         Sta->dAveLDC = 0.;
         if ( Sta->iAlarmStatus >= 1 ) Sta->iAlarmStatus = 1;
         if ( lGapSize < Sta->lRawCircSize )
         {
            PadBuffer( lGapSize, Sta->dAveLDCRaw, &Sta->lSampIndexR,
                       Sta->plRawCircBuff, Sta->lRawCircSize );
            PadBuffer( lGapSize, Sta->dAveLDC, &Sta->lSampIndexF,
                       Sta->plFiltCircBuff, Sta->lRawCircSize );
         }
      }

/* For gaps less than the size of the buffer, pad buffers with DC
   **************************************************************/
      if ( lGapSize > 1 && lGapSize <= 2 )
      {
         PadBuffer( lGapSize, Sta->dAveLDCRaw, &Sta->lSampIndexR,
                    Sta->plRawCircBuff, Sta->lRawCircSize );
         PadBuffer( lGapSize, Sta->dAveLDC, &Sta->lSampIndexF,
                    Sta->plFiltCircBuff, Sta->lRawCircSize );
         Sta->iPickStatus = 0;
         if ( Gparm.Debug )
            logit( "t", "Small gap - %s\n", Sta->szStation ); 
      }
	  
/* For gaps greater than the size of the buffer, re-init
   *****************************************************/
      if ( lGapSize >= Sta->lRawCircSize )
      {
         Sta->lSampIndexR = 0;
         Sta->lSampIndexF = 0;
         Sta->iPickStatus = 0;
      }

/* Compute DC offset for raw data
   ******************************/
      GetLDC( WaveHead->nsamp, WaveLong, &Sta->dAveLDCRaw, Sta->lFiltSamps );
			
/* Tuck raw data into proper location in buffer
   ********************************************/			
      PutDataInBuffer( WaveHead, WaveLong, Sta->plRawCircBuff,
                       &Sta->lSampIndexR, Sta->lRawCircSize );
	  
/* Short-period bandpass filter the data if specified in station file 
   (This is usually necessary for broadband data, not so for SP data.
   Taper the signal for restarts, but not for continuous signal.).
   ******************************************************************/
      for ( i=0; i<WaveHead->nsamp; i++ )
         WaveLongF[i] = WaveLong[i];             
		 		 
      if ( Sta->iFiltStatus == 1 )
         FilterPacket ( WaveLongF, Sta, WaveHead, Gparm.LowCutFilter,
                        Gparm.HighCutFilter, 3.*(1./Gparm.LowCutFilter) );
   
/* Process data through alarm function if set in .sta file
   *******************************************************/
      if ( now-WaveHead->endtime < RECENT_ALARM_TIME )
         if ( Gparm.AlarmOn )
            if ( Sta->iAlarmStatus >= 1 )
               SeismicAlarm( Sta, &Gparm, &Ewh, WaveHead, WaveLongF );
	  
/* Compute DC offset for filtered data
   ***********************************/
      GetLDC( WaveHead->nsamp, WaveLongF, &Sta->dAveLDC, Sta->lFiltSamps );
			
/* Tuck filtered data into proper location in buffer
   *************************************************/			
      PutDataInBuffer( WaveHead, WaveLongF, Sta->plFiltCircBuff,
                       &Sta->lSampIndexF, Sta->lRawCircSize );

/* Compute Mwp if necessary for this station
   *****************************************/
      if ( Sta->iPickStatus == 1 )
         if ( Sta->dEndTime-pPBuf->dPTime >= (double) Gparm.MwpSeconds ) 
         {
            RequestSpecificMutex( &mutsem1 );   /* Protect buf write */
            AutoMwp( Sta, pPBuf, Gparm.MwpSigNoise, Gparm.MwpSeconds, 1 );
            Sta->iPickStatus = 0;
            ReleaseSpecificMutex( &mutsem1 );   /* Release sem */
            ReportPick( pPBuf, &Gparm, &Ewh );                     
         }
	  
/* Send Message to WndProc (update screen every 5 seconds)
   *******************************************************/
      if ( WaveHead->endtime > dLastEndTime+5.0 )
         if ( iHold == 0 ) 
         {
            dLastEndTime = WaveHead->endtime;
            PostMessage( hwndWndProc, WM_NEW_DATA, 0, 0 );   
         }     

/* Save time of the end of the current message
   *******************************************/
      Sta->dEndTime = WaveHead->endtime;
      ReleaseSpecificMutex( &mutsem2 );   /* Release sem */
   }
   
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
}
