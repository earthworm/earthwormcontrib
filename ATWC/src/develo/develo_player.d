
#
#                     Develo's Configuration File
#
MyModId         MOD_DEVELO     # This instance of develo
InRing           WAVE_RING     # Input waveform ring
PRing            PICK_RING     # P-pick ring (for both input and output)
AlarmRing       ALARM_RING     # Transport ring to write alarm messages to
StaFile        "pick_wcatwc.sta" # File containing stations to be displayed
StaDataFile    "station.dat"   # File with information about stations
HeartbeatInt            30     # Heartbeat interval, in seconds
#    Add file created in ATPlayer if used in conjunction with player (optional)
#    Comment this line if using develo in real-time mode
ATPLineupFileBB "\earthworm\run\params\ATP-BB.sta" # Station config file
#
#    Develo can bandpass filter data prior to display. 
#    The following values are used as filter coefficients.
#    The default values (0.5, 5.) relate to the
#    response coefficients given within magcomp.  If other values are
#    used, the filter response for those values must be computed and used 
#    instead of the standard response.
#
LowCutFilter           0.5     # In hz, low frequency bandpass filter cutoff
HighCutFilter          5.0     # In hz, high frequency bandpass filter cutoff
#
#    After P-pick has been specified, the following value speicifes the number
#    of half-cycles to examine to determine mb.  The maximum amplitude
#    (and corresponding half-period) within these cycles is used to compute mb.
#
MbCycles                20     # Number 1/2 cycles of P to examine for mb
#
LGSeconds              160     # Number seconds after P to examine Lg for Ml
                               #  (first MbCycles of this time is excluded for
                               #   Ml period and amplitude)
#    Mwp computations are made as described by Tsuboi, Whitmore, and Sokolowski,
#    BSSA, v89, 1345-1351.  The technique has been completely automated here.
#    Results should be examined before issuance to ensure correctness.
#
MwpSeconds             120     # Max # seconds to evaluate after P start for Mwp
MwpSigNoise            2.5     # Signal-to-noise ratio necessary for Mwp comps.
#
ScreenFile  "screendisp.ini"   # File with screen display information
DummyFile  "d:\seismic\erlybird\dummyX_test.dat"  # WC/ATWC EarlyBird dummy file
MinutesInBuff           20     # Time (minutes) to allocate for each trace 
NumTracePerScreen       60     # Number of traces to show on visible screen
                               # (the rest will scroll)
TimePerScreen           90.    # Seconds of data to display on screen							   
PTimeDelta              60.    # Time difference between an input P-pick and the
                               #  existing pick.  If time (seconds) is greater
                               #  than this time, a new pick is called.
                               #  Otherwise, the existing pick is altered.
#
AlarmOn                  1     # 1->Alarm function enabled, 0->disabled
AlarmTimeout           180.    # Time (seconds) to reset alarm after triggering
#
Debug                    1     # If 1, print debugging message
