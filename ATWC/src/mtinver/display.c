 /***********************************************************************
  *                             Display()                               *
  *                                                                     *
  *  Routines which display information to the window.                  *
  *                                                                     *
  ***********************************************************************/
  
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>
#include "mtinver.h"

extern  double  dFiltHiSpec;         /* Short period filter cutoff (s) */
extern  double  dFiltLoSpec;         /* Long period filter cutoff (s) */
extern  double  dTLagSpec;           /* Max time adjustment (s) */
extern  HINSTANCE hInstMain;         /* Copy of main program instance (process id) */
extern  int     iDepthSpec;          /* Fixed depth for inversion (km) */
extern  int     iGF2Display[3];      /* Indices of Green's Functions to display */
extern  int     iWindowTotal;        /* Comparison window length (s) */
extern  PPICK   *PBuf;               /* Pointer to P-pick buffer */

     /**************************************************************
      *                    DisplayGFs()                            *
      *                                                            *
      * This function outputs Green's Functions traces.            *
      *                                                            *
      * Arguments:                                                 *
      *  hdc               Device context to use                   *
      *  pGF               Pointer to Green's function structures  *
      *  lTitleFHt         Title font height                       *
      *  lTitleFWd         Title font width                        *
      *  cxScreenT         Screen width in pixels                  *
      *  cyScreenT         Screen height in pixels                 *
      *  iVScoll           Vertical scroll amount                  *
      *                                                            *
      **************************************************************/
      
void DisplayGFs( HDC hdc, GF *pGF, long lTitleFHt, long lTitleFWd,
                 int cxScreenT, int cyScreenT, int iVScroll )
{
   static  float   fMaxAmp;     /* Largest amplitude (+ or -) of function */
   HFONT   hOFont, hNFont;      /* Font handles */
   HPEN    hRPen, hBPen;        /* Pen handles */
   int     i, j;
   int     iMaxTime;            /* Total time to display in seconds */
   int     iyTotal;
   static  POINT   pt, pt2, pt3;
   char    szMaxTime[6];        /* Total time to display in secs (string) */
   char    szTemp[8];
   
/* Create font */   
   hNFont = CreateFont( lTitleFHt, lTitleFWd, 
             0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,   
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
             DEFAULT_PITCH | FF_MODERN, "Elite" );

/* Select font and color */
   hOFont = (HFONT) SelectObject( hdc, hNFont );    /* Select the font */
   SetTextColor( hdc, RGB( 0, 0, 0 ) );             /* Black text */
   
/* Set pens and brushes up */
   hRPen = CreatePen( PS_SOLID, 1, RGB( 255, 0, 0 ) );   /* Red */
   hBPen = CreatePen( PS_SOLID, 2, RGB( 0, 0, 0 ) );     /* Black */
   
/* Get maximum time of output */
   iMaxTime = (int) GF_TRACELENGTH;
   itoaX( iMaxTime, szMaxTime );  /* Convert max to character string */
   strcat( szMaxTime, "s" );

/* Get max amplitude of output */
   fMaxAmp = 0.;
   for ( i=0; i<3; i++ )
      for ( j=0; j<iMaxTime; j++ )
         if (fabs( pGF[iGF2Display[i]].fGF[j] ) > fMaxAmp )
            fMaxAmp = (float) fabs( (double) pGF[iGF2Display[i]].fGF[j] );
	 
/* Loop through components, write distance/depth/comp and trace */
   for ( i=0; i<3; i++ )
   {
      _gcvt( pGF[iGF2Display[i]].dDistance, 5, szTemp );
      pt.x = 42*cxScreenT/100;
      pt.y = 5 + (i*5) + i*12*cyScreenT/100 - iVScroll;
      TextOut( hdc, pt.x, pt.y, szTemp, strlen( szTemp ) ); /* Distance */
      itoaX( pGF[iGF2Display[i]].iDepth, szTemp );
      pt.x = 50*cxScreenT/100;
      TextOut( hdc, pt.x, pt.y, szTemp, strlen( szTemp ) ); /* Depth */
      pt.x = 58*cxScreenT/100;
      TextOut( hdc, pt.x, pt.y, pGF[iGF2Display[i]].szComp, /* Component */
               strlen( pGF[iGF2Display[i]].szComp ) );	  
/* Display axes */
      SelectObject( hdc, hBPen );
      pt.x = 40*cxScreenT/100;                  /* Time axis */
      pt3.y = 5 + (i*5) + i*12*cyScreenT/100 + 12*cyScreenT/200 - iVScroll;
      MoveToEx( hdc, pt.x, pt3.y, NULL );
      pt2.x = 65*cxScreenT/100;
      LineTo( hdc, pt2.x, pt3.y );
      pt.x = 64*cxScreenT/100;                  /* Time label */
      TextOut( hdc, pt.x, pt3.y+2, szMaxTime, strlen( szMaxTime ) ); 
      pt.x = 40*cxScreenT/100;                  /* Amplitude axis */
      pt.y = 5 + (i*5) + i*12*cyScreenT/100 - iVScroll;
      MoveToEx( hdc, pt.x, pt.y, NULL );
      pt2.y = 5 + (i*5) + (i+1)*12*cyScreenT/100 - iVScroll;
      LineTo( hdc, pt.x, pt2.y );
      iyTotal = pt.y - pt2.y + 1;
/* Display Green's Function */
      SelectObject( hdc, hRPen );
      MoveToEx( hdc, pt.x, pt3.y, NULL );
      for ( j=0; j<iMaxTime; j++ )
      {
         pt3.x = (int) ((((double) j)/(double) (iMaxTime-1))
                *((double) (pt2.x-pt.x+1))) + pt.x;
         pt.y = pt3.y + (int) ((pGF[iGF2Display[i]].fGF[j]/fMaxAmp)
                * ((double) iyTotal/2.));
         LineTo( hdc, pt3.x, pt.y );
      }			       
   }           
   DeleteObject( SelectObject( hdc, hOFont ) );     /* Reset font */
   DeleteObject( hBPen );
   DeleteObject( hRPen );
}

     /**************************************************************
      *                      DisplaySoln()                         *
      *                                                            *
      * This function outputs the solution summary and beachball.  *
      *                                                            *
      * Arguments:                                                 *
      *  hdc               Device context to use                   *
      *  pMtS              Pointer to moment tensor structure      *
      *  pszFile           File with data to display               *
      *  lTitleFHt         Title font height                       *
      *  lTitleFWd         Title font width                        *
      *  cxScreenT         Screen width in pixels                  *
      *  cyScreenT         Screen height in pixels                 *
      *  iVScoll           Vertical scroll amount                  *
      *                                                            *
      **************************************************************/
      
void DisplaySoln( HDC hdc, MTSTUFF *pMtS, char *pszFile, long lTitleFHt,
                  long lTitleFWd, int cxScreenT, int cyScreenT, int iVScroll )
{
   FILE    *hFile;
   HFONT   hOFont, hNFont;      /* Font handles */
   int     iCnt;
   static  POINT   pt;
   char    szTemp[81];

   iCnt = 0;   
/* Create font */   
   hNFont = CreateFont( lTitleFHt, lTitleFWd, 
             0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,   
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
             DEFAULT_PITCH | FF_MODERN, "Elite" );

/* Select font and color */
   hOFont = (HFONT) SelectObject( hdc, hNFont );    /* Select the font */
   SetTextColor( hdc, RGB( 0, 0, 0 ) );             /* Black text */
   
/* Open data file, read, and output to screen */
   iCnt = 1;
   hFile = fopen( pszFile, "r" );
   while ( !feof( hFile ) )
   {
      fgets( szTemp, 80, hFile );
      pt.x = 2*cxScreenT/100;
      pt.y = 5 + iCnt*cyScreenT/50 - iVScroll;
      TextOut( hdc, pt.x, pt.y, szTemp, strlen( szTemp ) );	  
      iCnt++;
   }
   DeleteObject( SelectObject( hdc, hOFont ) );     /* Reset font */
}

     /**************************************************************
      *                    DisplaySynths()                         *
      *                                                            *
      * This function outputs synthetic and data traces.           *
      *                                                            *
      * Arguments:                                                 *
      *  hdc               Device context to use                   *
      *  pMtS              Pointer to moment tensor structure      *
      *  lTitleFHt         Title font height                       *
      *  lTitleFWd         Title font width                        *
      *  cxScreenT         Screen width in pixels                  *
      *  cyScreenT         Screen height in pixels                 *
      *  iNumSta           Total Number of stations                *
      *  iVScoll           Vertical scroll amount                  *
      *  iWinLen           Trace length in samples (seconds also)  *
      *                                                            *
      **************************************************************/
      
void DisplaySynths( HDC hdc, MTSTUFF *pMtS, long lTitleFHt, long lTitleFWd,
                    int cxScreenT, int cyScreenT, int iNumSta, int iVScroll,
                    int iWinLen )
{
   static  float   fMaxAmp;     /* Largest amplitude (+ or -) of function */
   HFONT   hOFont, hNFont;      /* Font handles */
   HPEN    hRPen, hBPen, hBlPen;/* Pen handles */
   int     i, j, iCnt, iCnt2;
   int     iMaxTime;            /* Total time to display in seconds */
   int     ixAdd;               /* x offset for 2nd row */
   int     iyOffset;            /* y offset for 2nd row */
   int     iyTotal;
   static  POINT   pt, pt2, pt3;
   char    szMaxTime[6];        /* Total time to display in secs (string) */

   iCnt = 0;   
   iCnt2 = 0;
/* Create font */   
   hNFont = CreateFont( lTitleFHt, lTitleFWd, 
             0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET,   
             OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
             DEFAULT_PITCH | FF_MODERN, "Elite" );

/* Select font and color */
   hOFont = (HFONT) SelectObject( hdc, hNFont );    /* Select the font */
   SetTextColor( hdc, RGB( 0, 0, 0 ) );             /* Black text */
   
/* Set pens and brushes up */
   hRPen = CreatePen( PS_SOLID, 1, RGB( 255, 0, 0 ) );   /* Red */
   hBlPen = CreatePen( PS_SOLID, 1, RGB( 0, 0, 255 ) );  /* Blue */
   hBPen = CreatePen( PS_SOLID, 2, RGB( 0, 0, 0 ) );     /* Black */
   
/* Loop through stations - find out how many to display */
   for ( i=0; i<iNumSta; i++ )
      if ( pMtS[i].iUse == 1 ) iCnt++;

/* Display signals */	 
   for ( i=0; i<iNumSta; i++ )
      if ( pMtS[i].iUse == 1 )
      {   
         if ( iCnt2 < 12 ) { ixAdd = 0;  iyOffset = 0;  } 
         else              { ixAdd = 32; iyOffset = 12; }	  
	  
/* Get maximum time of output */
         iMaxTime = iWinLen;
         itoaX( iMaxTime, szMaxTime ); /* Convert max to character string */
         strcat( szMaxTime, "s" );

/* Get max amplitude of output */
         fMaxAmp = 0.;
         for ( j=0; j<iWinLen; j++ )
            if (fabs( pMtS[i].dSynth[j] ) > fMaxAmp )
               fMaxAmp = (float) fabs( (double) pMtS[i].dSynth[j] );
         for ( j=0; j<iWinLen; j++ )
            if (fabs( pMtS[i].dDataFilt[j] ) > fMaxAmp )
               fMaxAmp = (float) fabs( (double) pMtS[i].dDataFilt[j] );
	  
         pt.x = (58+ixAdd) * cxScreenT/100;
         pt.y = 5 + ((iCnt2-iyOffset)*5) + (iCnt2-iyOffset)*8*cyScreenT/105
                - iVScroll;
         TextOut( hdc, pt.x, pt.y, pMtS[i].szStation,           /* Station */
                  strlen( pMtS[i].szStation ) );	  
				  
/* Display axes */
         SelectObject( hdc, hBPen );
         pt.x = (38+ixAdd)*cxScreenT/100;          /* Time axis */
         pt3.y = 5 + ((iCnt2-iyOffset)*5) + (iCnt2-iyOffset)*8*cyScreenT/105 +
                 8*cyScreenT/200 - iVScroll;
         MoveToEx( hdc, pt.x, pt3.y, NULL );
         pt2.x = (63+ixAdd)*cxScreenT/100;
         LineTo( hdc, pt2.x, pt3.y );
         pt.x = (62+ixAdd)*cxScreenT/100;          /* Time label */
         TextOut( hdc, pt.x, pt3.y+2, szMaxTime, strlen( szMaxTime ) ); 
         pt.x = (38+ixAdd)*cxScreenT/100;          /* Amplitude axis */
         pt.y = 5 + ((iCnt2-iyOffset)*5) + (iCnt2-iyOffset)*8*cyScreenT/105
                - iVScroll;
         MoveToEx( hdc, pt.x, pt.y, NULL );
         pt2.y = 5 + ((iCnt2-iyOffset)*5) +
                     ((iCnt2-iyOffset)+1)*8*cyScreenT/105 - iVScroll;
         LineTo( hdc, pt.x, pt2.y );
         iyTotal = pt.y - pt2.y + 1;
		 
/* Display data */
         SelectObject( hdc, hRPen );
         MoveToEx( hdc, pt.x, pt3.y, NULL );
         for ( j=0; j<iWinLen; j++ )
         {
            pt3.x = (int) ((((double) j)/(double) (iMaxTime-1))
                    *((double) (pt2.x-pt.x+1))) + pt.x;
            pt.y = pt3.y + (int) ((pMtS[i].dDataFilt[j]/(double) fMaxAmp)
                   * ((double) iyTotal/2.));
            LineTo( hdc, pt3.x, pt.y );
         }
		 
/* Display synthetic */
         SelectObject( hdc, hBlPen );
         MoveToEx( hdc, pt.x, pt3.y, NULL );
         for ( j=0; j<iWinLen; j++ )
         {
            pt3.x = (int) ((((double) j)/(double) (iMaxTime-1))
                    * ((double) (pt2.x-pt.x+1))) + pt.x;
            pt.y = pt3.y + (int) ((pMtS[i].dSynth[j]/(double) fMaxAmp)
                   * ((double) iyTotal/2.));
            LineTo( hdc, pt3.x, pt.y );
         }			       
         iCnt2++;
      }           
   DeleteObject( SelectObject( hdc, hOFont ) );     /* Reset font */
   DeleteObject( hBPen );
   DeleteObject( hBlPen );
   DeleteObject( hRPen );
}

  /******************************************************************
   *                    InvParamsDlgProc()                          *
   *                                                                *
   *  Input inversion parameters.  Check and adjust as necessary.   *
   *                                                                *
   *  Arguments:                                                    *
   *    Standard dialog procedure arguments and returns             *
   *                                                                *
   ******************************************************************/
   
long WINAPI InvParamsDlgProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   char    szTemp[8];
   
   switch (msg)
   {
      case WM_INITDIALOG:         /* Set defaults */
         SetDlgItemText( hwnd, EF_TWIN, "200" );
         SetDlgItemText( hwnd, EF_DEPTH, "-1" );
         SetDlgItemText( hwnd, EF_TLAG, "-1." );
         SetDlgItemText( hwnd, EF_FILTLO, "-1." );
         SetDlgItemText( hwnd, EF_FILTHI, "-1." );
         SetFocus( hwnd );
         break;

      case WM_COMMAND:            
         switch (LOWORD (wParam))
         {
            case IDOK:          /* OK was chosen */
               GetDlgItemText( hwnd, EF_TWIN, szTemp, sizeof (szTemp) );
               iWindowTotal = atoi( szTemp );   /* Comparison time window */
               if ( iWindowTotal > GF_TRACELENGTH )
                  {
                  logit( "", "Max length = %lds (%ld -> %ld)\n", GF_TRACELENGTH,
                   iWindowTotal, GF_TRACELENGTH );
                  iWindowTotal = GF_TRACELENGTH;			   
                  }
               if ( iWindowTotal < 20 && iWindowTotal > 0 )
                  {
                  logit( "", "Min length = 20s (%ld -> 20)\n", iWindowTotal );
                  iWindowTotal = 20;			   
                  }
               GetDlgItemText( hwnd, EF_DEPTH, szTemp, sizeof (szTemp) );
               iDepthSpec = atoi( szTemp );     /* Fixed depth */
               if ( iDepthSpec > 660 )
                  {
                  logit( "", "Max depth = 660km (%ld -> 660)\n", iDepthSpec );
                  iDepthSpec = 660;			   
                  }
               GetDlgItemText( hwnd, EF_TLAG, szTemp, sizeof (szTemp) );
               dTLagSpec = atof( szTemp );      /* Max Correlation Lag */
               if ( dTLagSpec > (double) iWindowTotal  )
                  {
                  logit( "", "Max lag = WinLen (%lf -> %lf)\n", dTLagSpec,
                         (double) iWindowTotal );
                  dTLagSpec = (double) iWindowTotal;			   
                  }
               GetDlgItemText( hwnd, EF_FILTLO, szTemp, sizeof (szTemp) );
               dFiltLoSpec = atof( szTemp );    /* Low freq. filter cutoff */
               if ( dFiltLoSpec >= 0. && dFiltLoSpec < 10. )
                  {
                  logit( "", "Min Long period = 10. (%lf -> 10.)\n", dFiltLoSpec );
                  dFiltLoSpec = 10.;			   
                  }
               if ( dFiltLoSpec > 500. )
                  {
                  logit( "", "Max Long period = 500. (%lf -> 500.)\n", dFiltLoSpec );
                  dFiltLoSpec = 500.;			   
                  }
               GetDlgItemText( hwnd, EF_FILTHI, szTemp, sizeof (szTemp) );
               dFiltHiSpec = atof( szTemp );    /* High freq. filter cutoff */
               if ( dFiltHiSpec >= 0. && dFiltHiSpec < 2. )
                  {
                  logit( "", "Min Short period = 2. (%lf -> 2.)\n", dFiltHiSpec );
                  dFiltHiSpec = 2.;			   
                  }
               if ( dFiltHiSpec > dFiltLoSpec/2. && dFiltLoSpec > 0. )
                  {
                  logit( "", "Max Short period = Long/2 (%lf -> %lf)\n",
                   dFiltHiSpec, dFiltLoSpec/2. );
                  dFiltHiSpec = dFiltLoSpec/2.;			   
                  }
               EndDialog( hwnd, IDOK );
               break;

            case IDCANCEL:      // Escape was chosen
               EndDialog( hwnd, IDCANCEL );
               break;
         }
      break;
   }
   return 0;
}

