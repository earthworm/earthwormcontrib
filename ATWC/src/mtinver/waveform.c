#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include "mtinver.h"

  /******************************************************************
   *                     AlignWf2()                                 *
   *                                                                *
   *  Align waveform.  This function was written to simplify the    *
   * previous method used in the auto-CMT solver.  This one just    *
   * correlates the observed with the synthetic, computes the       *
   * optimal shift, and then shifts the Green's functions as before.*
   *                                                                *
   *  Whitmore: 7/12/04.                                            *
   *                                                                *
   *  Arguments:                                                    *
   *   dTWindow    Window length in seconds                         *
   *   dMaxTLagT   Maximum time shift allowed (s)                   *
   *   iNSta       Number of traces in array                        *
   *   MtS         Moment tensor inversion structure                *
   *   iDebug      Debug level                                      *
   *   iWinLen     Window length in seconds                         *
   *                                                                *
   *  Return:       0 = OK; -1 = Too many sample points             *
   *                                                                *
   ******************************************************************/

int AlignWf2( double dTWindow, double dMaxTLagT, 
              int iNSta, MTSTUFF MtS[], int iDebug, int iWinLen )
{
   double  dObsT[GF_TRACELENGTH], dSyn[GF_TRACELENGTH];  /* Observed and synthetic waveforms */
   double  dTime;
   double  dTLag;                /* Computed time shift between Obs and Syn */
   double  dVss[GF_TRACELENGTH], dVds[GF_TRACELENGTH], dFfds[GF_TRACELENGTH];    /* Gf files data */
   double  dVssT[GF_TRACELENGTH], dVdsT[GF_TRACELENGTH], dFfdsT[GF_TRACELENGTH]; /* Gf files tmp data */
   static  int     i, ii, j, jj;         /* Based on 0 */
   int     iCnt, k;              /* Based on 1 */
   int     iNPts1;               /* # samples in data */
   int     iNPts2;               /* # samples expected in synthetic seis. */
   int     iPz, iPr, iSH;        /* Waveform types */

   iPz = 1;            /* Waveform type declarations */
   iPr = 2;
   iSH = 3;
                        
/* Loop through all waveforms */
   for ( ii=0; ii<iNSta; ii++ )   
      if ( MtS[ii].iUse == 1 )
      {   
         iNPts1 = iWinLen;
         iNPts2 = (int) (dTWindow/MtS[ii].dDt) + 1;
         if ( iNPts2 > iNPts1 ) iNPts2 = iNPts1;
/* Copy data and synthetic to temporary arrays */		 
         for ( jj=0; jj<iNPts1; jj++ ) dObsT[jj] = MtS[ii].dDataFilt[jj];
         for ( jj=0; jj<iNPts1; jj++ ) dSyn[jj]  = MtS[ii].dSynth[jj];

/* Compute time shift (by correlating) between Obs and Syn */	  
         TimeShift( dObsT, dSyn, iNPts2, MtS[ii].dDt, &dTLag, dMaxTLagT );
         if ( iDebug > 0 )
            logit( "", "%s dTLag=%lf, dMaxTLagT=%lf\n", MtS[ii].szStation,
                   dTLag, dMaxTLagT );
         if ( fabs( dTLag ) > dMaxTLagT )
         {
            logit( "", "Time shift too great.\n" );
            goto EndLoop;
         }

/* Now shift Green's functions accordingly */
         if ( MtS[ii].iWvType == iPr || MtS[ii].iWvType == iPz) 
         {
            for ( jj=0; jj<iNPts1; jj++ ) dVss[jj] = MtS[ii].dGFFiltVss[jj];
            for ( jj=0; jj<iNPts1; jj++ ) dVds[jj] = MtS[ii].dGFFiltVds[jj];
            for ( jj=0; jj<iNPts1; jj++ ) dFfds[jj] = MtS[ii].dGFFiltFfds[jj];
         }
         else if ( MtS[ii].iWvType == iSH )
         {
            for ( jj=0; jj<iNPts1; jj++ ) dVss[jj] = MtS[ii].dGFFiltVss[jj];
            for ( jj=0; jj<iNPts1; jj++ ) dVds[jj] = MtS[ii].dGFFiltVds[jj];
         }
	  
         if ( MtS[ii].iWvType == iPr || MtS[ii].iWvType == iPz )
            for ( i=0; i<iNPts1; i++ )   
            {
               dVssT[i] = 0.0;
               dVdsT[i] = 0.0;
               dFfdsT[i] = 0.0;
            }
         else if ( MtS[ii].iWvType == iSH )
            for ( i=0; i<iNPts1; i++ )   
            {
               dVssT[i] = 0.0;
               dVdsT[i] = 0.0;
            }
         k = 0;
         iCnt = 0;
         if ( dTLag < 0.0 ) 
         {
InIf1:      if ( iCnt >= iNPts1 ) goto PastIf1;
            iCnt++;
            dTime = (1-iCnt) * MtS[ii].dDt;
            if ( (dTime-dTLag) <= 0.0001 ) 
            {
               k++;
               if ( MtS[ii].iWvType == iPr || MtS[ii].iWvType == iPz )
               {
                  dVssT[iCnt-1] = dVss[k-1];
                  dVdsT[iCnt-1] = dVds[k-1];
                  dFfdsT[iCnt-1] = dFfds[k-1];
               }
               else if ( MtS[ii].iWvType == iSH )
               {
                  dVssT[iCnt-1] = dVss[k-1];
                  dVdsT[iCnt-1] = dVds[k-1];
               }
            }
            goto InIf1;
PastIf1:;
         }
         else
         {
InIf2:      if ( iCnt >= iNPts1 ) goto PastIf2;
            iCnt++;
            dTime = (iCnt-1) * MtS[ii].dDt;
            if ( (dTime-dTLag) >= -0.0001 ) 
            {
               k++;
               if ( MtS[ii].iWvType == iPr || MtS[ii].iWvType == iPz )
               {
                  dVssT[k-1] = dVss[iCnt-1];
                  dVdsT[k-1] = dVds[iCnt-1];
                  dFfdsT[k-1] = dFfds[iCnt-1];
               }
               else if ( MtS[ii].iWvType == iSH )
               {
                  dVssT[k-1] = dVss[iCnt-1];
                  dVdsT[k-1] = dVds[iCnt-1];
               }
            }
            goto InIf2;
PastIf2:;
         }

         for ( i=0; i<iNPts1; i++ )   
         {
            if ( MtS[ii].iWvType == iPr || MtS[ii].iWvType == iPz )
            {
               MtS[ii].dGFFiltVss[i]  = dVssT[i];
               MtS[ii].dGFFiltVds[i]  = dVdsT[i];
               MtS[ii].dGFFiltFfds[i] = dFfdsT[i];
            }
            else if ( MtS[ii].iWvType == iSH )
            {
               MtS[ii].dGFFiltVss[i]  = dVssT[i];
               MtS[ii].dGFFiltVds[i]  = dVdsT[i];
            }
         }   	  
EndLoop: ;
      }
   return 0;
}

  /******************************************************************
   *                     TimeShift()                                *
   *                                                                *
   *  Determine the optimal shift to data based on correlation      *
   * between two functions.                                         *
   *                                                                *
   *  Whitmore: 7/12/04.                                            *
   *                                                                *
   *  Arguments:                                                    *
   *   dData1, 2   Time series to compare                           *
   *   iNPts       Number of points to correlate in time series     *
   *   dDt         Sample interval                                  *
   *   dTLagT      Computed time shift to minimize difference       *
   *   dMaxTLagT   Maximum possible time shift                      *
   *                                                                *
   ******************************************************************/
   
void TimeShift( double dData1[], double dData2[], int iNPts, double dDt,
                double *dTLagT, double dMaxTLagT )
{
   double  dError;      /* Difference total between time series */
   double  dErrorMin;   /* Minimum of dErrors */
   int     i, j;
   int     iNumTries;   /* Number of possible data shifts */
   
/* Compute number of times to shift data series in time */   
   iNumTries = (int) ((dMaxTLagT/dDt)*2. + 1.);
   
   dErrorMin = 1.e25;
   *dTLagT = 0.;
   for ( i=0; i<iNumTries; i++ )
   {
      dError = 0.;
      for ( j=0; j<iNPts; j++ )
         if ( j+i-iNumTries/2 >= 0 )
         {
            dError += ((dData1[j]-dData2[j+i-iNumTries/2]) *
                       (dData1[j]-dData2[j+i-iNumTries/2]));
         }
      if ( dError < dErrorMin )
      {
         dErrorMin = dError;
//         *dTLagT = (iNumTries/2-i) * dDt;
         *dTLagT = (double) (i-iNumTries/2) * dDt;
      }
   }      
   return;
}

