
      /*****************************************************************
       *                       mtsolo.c                               *
       *                                                               *
       *  This program determines moment tensor solutions              *
       *  from broadband seismic data.                                 *
       *                                                               *
       *  The original FORTRAN code was donated by Harley Benz of NEIC.*
       *  This was converted to C and made into an earthworm module by *
       *  Whitmore at the WC/ATWC.                                     *
       *                                                               *
       *  Input seismic data is read from disk files (WCATWC format).  *
       *  Processing is triggered by a menu selection.                 *
       *                                                               *
       *  Input seismic data must be at the same sample rate as the    *
       *  Green's functions (1sps).                                    *
       *                                                               *
       *  Output from this module is written to a data file and to a   *
       *  Windows window.                                              *
       *                                                               *
       *  Original code contributed by Benz                            *
       *  Converted to C and ew module by Whitmore (WC/ATWC) - 2002    *
       *                                                               *
       *  Date (converted to Earthworm/EarlyBird system): 2/2003       *
       *  Compiler: MicroSoft Visual C/C** v6.0                        *
       *  Link Info: see mtsolo.mak                                    *
       *                                                               *
       ****************************************************************/
	   
#include <windows.h>
#include <wingdi.h>
#include <winuser.h>
#include <process.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <memory.h>
#include <signal.h>
#include <earthworm.h>
#include <transport.h>
#include "\earthworm\atwc\src\mtinver\mtinver.h"

/* Function protypes */
int    ReadMtSoloIni( GPARM * );

/* WINDOW PROCEDURES */
long WINAPI WndProc( HWND, UINT, UINT, long );

/* Global variable definitions */
double  dFiltHiSpec;           /* Short period filter cutoff (s) */
double  dFiltLoSpec;           /* Long period filter cutoff (s) */
double  dTLagSpec;             /* Max time adjustment (s) */
GF      GreensFunctions[NUM_GFS]; /* Green's function computed signal */
GPARM   Gparm;                 /* Configuration file parameters */
HINSTANCE hInstMain;           /* Copy of main program instance (process id) */
HWND    hwndWndProc;           /* Client window handle */
int     iDepthSpec;            /* Fixed depth for inversion (km) */
int     iGF2Display[3];        /* Indices of Green's Functions to display */
int     iGFDepths[NUM_GF_DEPTHS] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 
                     100, 120, 140, 160, 180, 200, 220, 240, 260, 280,
                     300, 320, 340, 360, 380, 400, 420, 440, 460, 480,
                     500, 520, 540, 560, 580, 600, 620, 640, 660};
int     iWindowTotal;          /* Comparison window length (s) */
MSG     msg;                   /* Windows control message variable */
int     Nsta;                  /* Number of stations to display */
MTRESULTS MtResults;           /* Inversion results */
MTSTUFF MtStuff[MAXWVS];       /* Broadband Response array, etc. */
PPICK   *PBuf;                 /* Pointer to P-pick buffer */
STATION StaArray[MAXWVS];      /* Station data array */
char    szLogDir[64];          /* Path to load log files */
char    szProcessName[] = "CMT From Disk Data";
char    szTitle[] = "CMT From Disk Data"; /* String to load in Title bar */

/****************************************************************************
Main program for MTSOLO.C. 

        Function:           WinMain
        Author:             Whitmore
        Written:            February, 2003
        OS:                 Windows 2000
        Compiler:           Microsoft Visual C++ v6.0
---------------------------------------------------------------------------*/
int WINAPI WinMain (HINSTANCE hInst, HINSTANCE hPreInst, 
                    LPSTR lpszCmdLine, int iCmdShow)
{
   int              i;
   long             InBufl;          /* Maximum PBuf size in bytes */
   static  MSG      msg;
   long             RawBufl;         /* Raw data buffer size */
   static  WNDCLASS wc;

   hInstMain = hInst;
   Nsta = 0;

/* Read the INI file */
   if ( ReadMtSoloIni( &Gparm ) == 0 )
   {
      printf( "ReadMtSoloIni failed\n" );
      exit( 0 );
   }
 
/* Open and initialize error file */
   logit_init( "mtsolo.ini", 0, 256, 1 );
   
/* Read in Green's Functions. Functions are 1024s in length, 1 sps,
   vertical component only.  Functions are created based on depth and
   distance. Distances range from 1100km to 10700km and depths from 10km
   to 660km. The .clv, .vss, and .vds component for each are computed.
   *********************************************************************/
   ReadGreensFunctions( GreensFunctions, Gparm.GFDir );
   
/* Allocate the waveform buffers
   *****************************/
   for ( i=0; i<MAXWVS; i++ )
   {		 		                  
/* Allocate memory for raw circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples)
   ********************************/
      StaArray[i].lRawCircSize = (long) (Gparm.GFSampRate*(double)Gparm.ReadTime*60.+0.1);
      RawBufl = sizeof (long) * StaArray[i].lRawCircSize;
      StaArray[i].plRawCircBuff = (long *) malloc( (size_t) RawBufl );
      if ( StaArray[i].plRawCircBuff == NULL )
      {
         logit( "et", "mtsolo: Can't allocate raw circ buffer for %ld\n", i );
         exit( 0 );
      }
   }

/* If this is the first instance of this program, init window
   stuff and register window. */
   if (!hPreInst)
   {      /* Force PAINT when sized and give double click notification */
      wc.style           = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
      wc.lpfnWndProc     = WndProc;            /* Window process */
      wc.cbClsExtra      = 0;
      wc.cbWndExtra      = 0;
      wc.hInstance       = hInst;              /* Process id */
      wc.hIcon           = LoadIcon( hInst, "MtSolo" );   /* System app icon */
      wc.hCursor         = LoadCursor( NULL, IDC_ARROW );/* Pointer cursor */
      wc.hbrBackground   = GetStockObject( WHITE_BRUSH );/* White background */
      wc.lpszMenuName    = "MtSolo_menu";      /* Relates to .RC file */
      wc.lpszClassName   = szProcessName;
      if ( !RegisterClass( &wc ) )             /* Window not registered */
      {
         logit( "et" , "RegisterClass failed\n" );
         for ( i=0; i<MAXWVS; i++ ) free( StaArray[i].plRawCircBuff );
         exit( 0 );
      }
   }

/* Create the window */
   hwndWndProc = CreateWindow(
                  szProcessName,            /* Process name */
                  szProcessName,            /* Initial title bar caption */	      	
                  WS_OVERLAPPEDWINDOW | WS_VSCROLL,  /* Standard window style */
                  CW_USEDEFAULT,            /* top left x starting location */
                  CW_USEDEFAULT,            /* top left y starting location */
                  CW_USEDEFAULT,            /* Initial screen width in pixels */
                  CW_USEDEFAULT,            /* Initial screen height in pixels */
                  NULL,                     /* No parent window */
                  NULL,                     /* Use standard system menu */
                  hInst,                    /* Process id */
                  NULL );                   /* No extra data to pass in */
   if ( hwndWndProc == NULL )               /* Window not created */
   {
      logit ( "et" , "CreateWindow failed\n" );
      for ( i=0; i<MAXWVS; i++ ) free( StaArray[i].plRawCircBuff );
      exit ( 0 );
   }
   
   ShowWindow( hwndWndProc, iCmdShow );     /* Show the Window */
   UpdateWindow( hwndWndProc );             /* Force an initial PAINT call */

/* Allocate and init the P-pick buffer
   ***********************************/
   InBufl = sizeof( PPICK ) * MAXWVS; 
   PBuf = (PPICK *) malloc( (size_t) InBufl );
   if ( PBuf == NULL )
   {
      logit( "et", "mtsolo: Can't allocate P-pick buffer." );
      return 0;
   }
   for ( i=0; i<MAXWVS; i++ ) InitP( &PBuf[i] );

/* Set up message handling loop */
   while ( GetMessage( &msg, NULL, 0, 0 ) )
   {
      TranslateMessage( &msg );
      DispatchMessage( &msg );
   }
   
/* Close up shop */   
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
   free( PBuf );
   for ( i=0; i<MAXWVS; i++ ) free( StaArray[i].plRawCircBuff );
   return ( msg.wParam );
}

      /***********************************************************
       *                      WndProc()                          *
       *                                                         *
       *  This dialog procedure processes messages from the      *
       *  windows screen and messages passed to it from          *
       *  elsewhere.  All                                        *
       *  display is performed in PAINT.  Menu options control   *
       *  the display.                                           *
       *                                                         *
       ***********************************************************/
       
long WINAPI WndProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   static  int    cxScreen, cyScreen;        /* Window size in pixels */
   double  dData[MTBUFFER_SIZE]; /* Trace */
   double  dDataOut[DECON_BUFFER];/* Deconvolved trace (w/ inst resp removed */ 
   double  dDataStartTime;    /* 1/1/70 time of data start for all traces */
   double  dFileTime;         /* time (1/1/70) at start of file */   
   double  dMag;              /* Magnitude of best depth fit */
   double  dMaxDepth;         /* Maximum depth expected for this region */
   static  double dMaxTLag;   /* Maximum trace shift (s) */
   double  dMinErr;           /* Smallest error for all inversions */
   double  dSN;               /* Signal-to-Noise ratio */
   HCURSOR hCursor;           /* Present cursor handle (hourglass or arrow) */
   static  HDC hdc;           /* Device context of screen */
   FILE    *hFile;            /* File pointer */
   static  HANDLE  hMenu;     /* Handle to the menu */
   static  HFONT hPTimeFont;  /* List box font handle (same as titles) */
   HYPO    HStruct;           /* Hypocenter data structure */
   int     i, ii, j, k, iCnt; /* Loop Counters */
   int     iArch;             /* 0->Data on disk; 1->Data from archive */
   static  int iDepthStart, iDepthEnd; /* GF depth indices to loop through */
   int     iDepthIndex;       /* Index of best-fitting depth */
   int     iDum;
   int     iIndex;            /* PBuf array index in StaArray */
   int     iMin, iMinIndex, iTemp; /* Indices for depth index computations */
   static  int  iNumPAuto;    /* Number Ps read from file */
   int     iNWaves;           /* # stations in inversion */
   static  int iProcMoment;   /* 0->no moment proc going on, 1-> moment going*/
   static  int  iScrollVertBuff;      /* Small vertical scroll amount (pxl) */
   static  int  iScrollVertPage;      /* Large vertical scroll amount (pxl) */				
   time_t  iTime;             /* time (1/1/70) at start of file */   
   static  int  iVScrollOffset;       /* Vertical scoll bar setting */
   static  int iWaveFormDisplay; /* 0->no waveforms shown;
                                    1->Show Green's Functions; 
                                    2->Show synthetics and data */
   LATLON  llIn, llOut;       /* Temp lat/lon structure */
   long    lNum;              /* Number of samples to transfer to temp buffer */
   static  long lTitleFHt, lTitleFWd;   /* Font height and width */
   static  long lTitleFHt2, lTitleFWd2; /* Font2 height and width */
   static ResponseStruct *pFinalResponse; /* Output response */
   static ResponseStruct *pResponse; /* Response poles/zeroes when known */
   PAINTSTRUCT ps;            /* Paint structure used in WM_PAINT command */
   char    *pszFileToOpen;    /* File Name which should contain disk data */
   int     rc;                /* Return code from convertWave */
   RECT    rct;               /* RECT (rectangle structure) */
   char    szDir[64];         /* Path to read seismic data */
   double  taperFreqs[4];     /* Taper variables for deconvolution */
   struct tm *tm;           /* time structure for the file name time */
   char    szTemp[16];

/* Respond to user input (menu choices, etc.) and system messages */
   switch ( msg )
   {
      case WM_CREATE:         /* Do this the first time through */
         iVScrollOffset = 0;                  
         iWaveFormDisplay = 0;
         iProcMoment = 0;
         hCursor = LoadCursor( NULL, IDC_ARROW );
         SetCursor( hCursor );
         hMenu = GetMenu( hwnd );
         SetWindowText( hwnd, szTitle );       /* Display the title */
         if ((pResponse = (ResponseStruct *) malloc 
                          (sizeof (ResponseStruct))) == NULL )
         {
            logit( "", "Could not malloc the Response structure.\n" );
            PostMessage( hwnd, WM_DESTROY, 0, 0 );
         }
         if ((pFinalResponse = (ResponseStruct *) malloc 
                               (sizeof (ResponseStruct))) == NULL )
         {
            logit( "", "Could not malloc the FinalResponse structure.\n" );
            PostMessage( hwnd, WM_DESTROY, 0, 0 );
         }
         pFinalResponse->dGain = 1.;
         pFinalResponse->iNumPoles = 0;
         pFinalResponse->iNumZeros = 0;
         break;
		 
/* Get screen size in pixels, re-paint, and re-proportion screen */
      case WM_SIZE:
         cyScreen = HIWORD (lParam);
         cxScreen = LOWORD (lParam);
		 
/* Compute font size */
         lTitleFHt  = cyScreen / 33;
         lTitleFWd  = cxScreen / 100;
         lTitleFHt2 = cyScreen / 50;
         lTitleFWd2 = cxScreen / 115;
	 
/* Set scroll thumb positions */
         iScrollVertBuff = cyScreen / 10;
         iScrollVertPage = cyScreen / 2;
         SetScrollRange( hwnd, SB_VERT, 0, cyScreen*3, FALSE );  
         SetScrollPos( hwnd, SB_VERT, 0, TRUE );
		 
         InvalidateRect( hwnd, NULL, TRUE );    /* Force a re-PAINT */
         break;

/* Respond to menu selections */
      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {			   
            case IDM_STOP_LP:
               strcpy( szTitle, szProcessName );
               SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
               iProcMoment = 0;
               logit( "t", "MT processing manually stopped\n" );
               break;
			
            case IDM_COMPUTE_MOMENT:
               if ( iProcMoment == 1 )
               {
                  strcpy( szTitle, szProcessName );
                  SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
                  iProcMoment = 0;                  
                  logit( "t", "processing stopped in COMPUTE_MOMENT-1\n" );
               }
               iDepthStart = 0;
               iDepthEnd = NUM_GF_DEPTHS;
			                  			   
/* Get inversion parameters */			   
               if ( DialogBox( hInstMain, "InvParams", hwndWndProc, 
                   (DLGPROC) InvParamsDlgProc ) == IDCANCEL ) break;
			   
/* Initialize P-time array each time and reset iUse arrays */			   
               for ( i=0; i<MAXWVS; i++ ) InitP( &PBuf[i] );
               for ( i=0; i<MAXWVS; i++ ) MtStuff[i].iUse = 0;
               for ( i=0; i<MAXWVS; i++ ) MtStuff[i].iUseOrig = 0;
			   
/* Get hypo parameters from dummy file and P-times from P-times file
   Only use Ps that were automatically or manually picked.  (Depth could
   throw off expected P). */			   
               ReadDummyData( &HStruct, Gparm.DummyFile );
               if ( ReadEBPTimeFile( &iNumPAuto, PBuf,
                                     Gparm.EBPTimeFile, MAXWVS ) == 0 )
               {
                  logit( "", "P-time file read problem: %s\n", Gparm.EBPTimeFile );
                  break;
               }
               HStruct.iQuakeID = 0;
               HStruct.iVersion = 0;			   
               if ( iWindowTotal < 0 )             /* Base time on mag. */
                   if ( HStruct.dMwpAvg > 6.2 ) iWindowTotal = GF_TRACELENGTH;
                   else                         iWindowTotal = 100;
			   
/* Initialize MT results file with hypocenter parameter information */
               if ( (hFile = fopen( Gparm.MTFile, "w" )) != NULL )
               {
                  fprintf( hFile, "%lf\n", HStruct.dLat );
                  fprintf( hFile, "%lf\n", HStruct.dLon );
                  fprintf( hFile, "%lf\n", HStruct.dOriginTime );
                  fclose( hFile );
               }			   
               else
                  logit( "t", "MT file (%s) not opened\n", Gparm.MTFile );

/* Compute proper depth indices */
               if ( iDepthSpec < 0 ) /* Loop through depths */
               {                     /* Get max expected depth */
                  llIn.dLat = HStruct.dLat;
                  llIn.dLon = HStruct.dLon;
                  GeoGraphic( &llOut, &llIn );
                  if ( llOut.dLon >= 180. ) llOut.dLon -= 360.;
                  dMaxDepth = DepthCheck( llOut.dLat, llOut.dLon,
                                           Gparm.DepthFile ); 
                  if ( dMaxDepth > 0. )
                  {
                     iMin = 1000000;
                     iMinIndex = 0;
                     for ( i=0; i<NUM_GF_DEPTHS; i++ )
                     {
                        iTemp = abs( iGFDepths[i] - (int) dMaxDepth );
                        if ( iTemp < iMin )
                        {
                           iMin = iTemp;
                           iMinIndex = i;
                        }
                     }
                     iDepthStart = 0;
                     iDepthEnd = iMinIndex + 1;
                  }
               }
               else                  /* Use only specified depth */
               {
                  iMin = 1000000;
                  iMinIndex = 0;
                  for ( i=0; i<NUM_GF_DEPTHS; i++ )
                  {
                     iTemp = abs( iGFDepths[i] - iDepthSpec );
                     if ( iTemp < iMin )
                     {
                        iMin = iTemp;
                        iMinIndex = i;
                     }
                  }
                  iDepthStart = iMinIndex;
                  iDepthEnd = iDepthStart + 1;
               }
               if ( Gparm.Debug > 0 ) logit( "", "Check depth indices %ld to %ld\n",
                  iDepthStart, iDepthEnd );
				  
/* Empty final results file */				  
               if ( (hFile = fopen( Gparm.EmailFile, "w" )) != NULL )
                  fclose( hFile );
               else
                  logit( "t", "Email file (%s) not opened\n", Gparm.EmailFile );

               strcpy( szTitle, szProcessName );
               strcat( szTitle, " - PROCESSING MT" );
               SetWindowText( hwndWndProc, szTitle );  /* Display the title */
               logit( "t", "processing started in COMPUTE_MOMENT\n" );
               iProcMoment = 1;
		
/* Load up STATION array with station info for MT; get this from disk
   file header (use o-time disk file). */
               iTime = (time_t) (floor( HStruct.dOriginTime ));
               tm = TWCgmtime( iTime );
               dFileTime = floor( HStruct.dOriginTime - ((double) (tm->tm_min %
                Gparm.LPFileSize)*60.) - (double) tm->tm_sec );
               iArch = 0;
               pszFileToOpen = CreateFileName( dFileTime, Gparm.LPFileSize, 
                                               Gparm.RootDirectory, Gparm.FileSuffix );
               if ( (Nsta = ReadDiskHeader( pszFileToOpen, StaArray, MAXWVS )) <
                     0 )
               {                               /* Bad data read */
/* Try the archive directory if the data can't be found on disk */	       
                  pszFileToOpen = CreateFileName( dFileTime, Gparm.LPFileSize, 
                                                  Gparm.ArchDirectory, Gparm.FileSuffix );
                  if ( (Nsta = ReadDiskHeader( pszFileToOpen, StaArray,
                                               MAXWVS )) < 0 )
                  {                            /* Bad data read */
                     strcpy( szTitle, szProcessName );
                     SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
                     iProcMoment = 0;
                     logit ("t", "File header not read: %s\n", pszFileToOpen);
                     break;
                  }
                  iArch = 1;
               }
               logit( "", "%ld Stations read from data file header\n", Nsta );

/* Then add response information */
               for ( i=0; i<Nsta; i++ ) 
               {
                  rc = LoadResponseData( &StaArray[i], &MtStuff[i],
                                          Gparm.ResponseFile );
                  if ( rc == -1 )
                  {
                     if ( Gparm.Debug > 0 )
                        logit( "e", "file: %s\n", Gparm.ResponseFile );
                     strcpy( szTitle, szProcessName );
                     SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
                     iProcMoment = 0;
                     break;
                  }
                  else if ( rc == 0 )
                     if ( Gparm.Debug > 0 )
                        logit( "", "scn = %s %s %s - No RESPONSE info\n",
                         StaArray[i].szStation, StaArray[i].szChannel,
                         StaArray[i].szNetID );
                  else if ( rc == 1 )
                     if ( Gparm.Debug > 0 )
                        logit( "", "scn = %s %s %s - RESPONSE info read\n",
                         StaArray[i].szStation, StaArray[i].szChannel,
                         StaArray[i].szNetID );
                  InitVar( &StaArray[i] );	  
                  StaArray[i].dEndTime = 0.;
                  StaArray[i].iFirst = 1;
               }
               logit( "", "Response information assigned\n" );

/* Read in all data from O-time to Total time */	       
               for ( i=0; i<Nsta; i++ )
                  memset( StaArray[i].plRawCircBuff, 0, 
			              sizeof (long) * StaArray[i].lRawCircSize );
               if ( iArch == 0 ) strcpy( szDir, Gparm.RootDirectory );
               else              strcpy( szDir, Gparm.ArchDirectory );
               if ( Gparm.Debug > 0 ) logit( "", "Read from %s\n", szDir );
               rc = ReadDiskDataForMTSolo( Gparm.LPFileSize, Gparm.ReadTime, szDir,
                     Gparm.FileSuffix, HStruct.dOriginTime-(double)DATA_PREEVENT-
                     (double)GF_PREEVENT, Nsta, StaArray, &iDum );
               if ( rc != 0 )
               {
                  strcpy( szTitle, szProcessName );
                  SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
                  iProcMoment = 0;
                  logit( "t", "Data file read problem\n" );
                  break;
               }
               dDataStartTime = HStruct.dOriginTime - (double)DATA_PREEVENT -
                                                      (double)GF_PREEVENT;
               if ( Gparm.Debug > 0 ) logit( "", "Disk data read in\n" );              

/* Loop through all picks and set up data */   	       
               iNWaves = 0;
               for ( i=0; i<iNumPAuto; i++ )
               {   
/* Get this station's index in main arrays */
                  for ( j=0; j<Nsta; j++ )
                     if ( !strcmp( StaArray[j].szStation, PBuf[i].szStation ) &&
                          !strcmp( StaArray[j].szChannel, PBuf[i].szChannel ) &&
                          !strcmp( StaArray[j].szNetID, PBuf[i].szNetID ) )
                     {
                        iIndex = j;
                        break;
                     }
                  if ( j == Nsta )
                  {
                     if ( Gparm.Debug > 0 )
                        logit( "", "Station %s not found in array\n",
                               PBuf[i].szStation );
                     goto NextStation;
                  }
                  if ( Gparm.Debug > 0 )
                     logit( "", "%s %s Pick; Index=%ld, Start=%lf, End=%lf\n",
                      PBuf[i].szStation, PBuf[i].szChannel, iIndex,
                      dDataStartTime, StaArray[iIndex].dEndTime );

/* Verify that this type of seismometer is OK for inversions */
                  if ( StaArray[iIndex].iStationType != 1 && /* STS-1 */
                       StaArray[iIndex].iStationType != 2 && /* STS-2 */
                       StaArray[iIndex].iStationType != 3 && /* CMG-3NSN */
                       StaArray[iIndex].iStationType != 4 && /* CMG-3T */
                       StaArray[iIndex].iStationType != 5 && /* KS360i */
                       StaArray[iIndex].iStationType != 6 && /* KS54000 */
                       StaArray[iIndex].iStationType != 9 && /* CMG-3TNSN */
                       StaArray[iIndex].iStationType != 12 && /* CMG3ESP_60 */
                       StaArray[iIndex].iStationType != 14 && /* CMG3ESP_120 */
                       StaArray[iIndex].iStationType != 16 && /* CMG3T_360 */
                       StaArray[iIndex].iStationType != 13 ) /* Trillium */
                  {
                     if ( Gparm.Debug > 0 )
                        logit( "", "Station %s wrong instrument type (%ld)\n",
                             PBuf[i].szStation, StaArray[iIndex].iStationType );
                     goto NextStation;
                  }			  
				  
/* Continue if there is response data for this station and the distance
   is appropriate */
                  if ( MtStuff[iIndex].dAmp0 > 0. && PBuf[i].dDelta >= 20. &&
                       PBuf[i].dDelta < 94. )
                  {
                     if ( Gparm.Debug > 0 )
                        logit( "", "%s %s has resp. and dist.\n",
                         PBuf[i].szStation, PBuf[i].szChannel );
                     MtStuff[iIndex].dDt = 1. / StaArray[iIndex].dSampRate;
                     MtStuff[iIndex].dMtStartTime = PBuf[i].dPTime -
                      (double) GF_PREEVENT;  /* Must assume 1sps */
                     MtStuff[iIndex].dMtEndTime =
                      MtStuff[iIndex].dMtStartTime + (double) iWindowTotal;
                     InitMtStructure( &MtStuff[iIndex] );
				  
/* Set epicentral distance in degrees and epicenter to station az */
                     MtStuff[iIndex].azdelt.dDelta = PBuf[i].dDelta;
                     MtStuff[iIndex].azdelt.dAzimuth = PBuf[i].dAz;
				   
/* Is there data in this buffer covering the TimeWindow? */
                     if ( MtStuff[iIndex].dMtStartTime-DATA_PREEVENT >
                          dDataStartTime && MtStuff[iIndex].dMtEndTime+
                          DATA_POSTEVENT < StaArray[iIndex].dEndTime )
                     {		 	  		
/* First, fill up the buffer */
                        rc = FillMtDataBuffSolo( &StaArray[iIndex],
                         &MtStuff[iIndex], &lNum, dData, dDataStartTime );
                        if ( rc < 0 ) goto NextStation;
                        MtStuff[iIndex].iNPts = lNum;
						
/* Make some checks on data: are there gaps?, is S:N OK?, is DC OK? */
/* First, check for gaps, and a DC level near the "stops" */
                        rc = CheckDataForDCandGaps( lNum, dData,
                                                   &StaArray[iIndex] );
                        if ( rc < 0 ) goto NextStation;
                						 
/* Now check the signal-to-noise ratio */
                        rc = CheckDataForSN( lNum, dData, &StaArray[iIndex],
                                             &MtStuff[iIndex], &dSN );
                        if ( rc < 0 ) goto NextStation;  
                        if ( Gparm.Debug ) logit( "", "%s SN=%lf\n",
                           StaArray[iIndex].szStation, dSN );
						
/* Set up response structure for deconvolution */
                        rc = FillResponse( pResponse, &MtStuff[iIndex] );
                        if ( rc < 0 ) goto NextStation;
						
/* Set up taper frequencies (filter 10-100s) */						
                        if ( dFiltLoSpec < 0. )
                        {
                           taperFreqs[0] = 0.008;
                           taperFreqs[1] = 0.01;
                        }
                        else
                        {
                           taperFreqs[0] = 1. / (dFiltLoSpec+dFiltLoSpec/4.);
                           taperFreqs[1] = 1. / dFiltLoSpec;
                        }
                        if ( dFiltHiSpec < 0. )
                        {
                           taperFreqs[3] = 0.2;
                           taperFreqs[2] = 0.1;
                        }
                        else
                        {
                           taperFreqs[3] = 1. / (dFiltHiSpec-dFiltHiSpec/2.);
                           taperFreqs[2] = 1. / dFiltHiSpec;
                        }
                        if ( Gparm.Debug > 1 ) logit( "", "tF0=%lf, tF1=%lf, tF2=%lf tF3=%lf\n",
                           taperFreqs[0], taperFreqs[1], taperFreqs[2], taperFreqs[3] );
						
/* Passed all tests, remove instrument reponse and filter 10s-100s*/	
                        if ( (DeconvolveWF( dData, lNum,
                         1./StaArray[iIndex].dSampRate, pResponse,
                         pFinalResponse, taperFreqs, dDataOut,
                         DECON_BUFFER )) == -1 ) goto NextStation;
						 
/* Save original data and filtered (just over P) in arrays. */
                        for ( j=0; j<lNum; j++ )
                           MtStuff[iIndex].dData[j] = dData[j];
                        for ( j=0; j<iWindowTotal; j++ )
                           MtStuff[iIndex].dDataFilt[j] = dDataOut[j+DATA_PREEVENT];
                						 
/* Get Sum-square of amps */
                        CheckDataForSSAmp( dDataOut, &StaArray[iIndex],
                                           &MtStuff[iIndex], iWindowTotal );
						
/* Sample rates must be same in data and GF */
                        if ( fabs( GreensFunctions[0].dSR - 
                             StaArray[iIndex].dSampRate ) > 0.0001 )
                        {
                           logit( "", "GF sample rate different than data"
                            "data=%lf, GF=%lf\n", StaArray[iIndex].dSampRate,
                            GreensFunctions[0].dSR );
                           goto NextStation;
                        }
                        if ( Gparm.Debug > 0 )
                           logit( "", "%s %s looks good\n", PBuf[i].szStation,
                            PBuf[i].szChannel );
                        MtStuff[iIndex].iUseOrig = 1;
                        free( pResponse->Poles );
                        free( pResponse->Zeros );
                        iNWaves++;
                     }
                  }
NextStation:   ;
               }
			   
/* Then call MtInv (if > enough stations have data) */ 
               dMag = 0.;
               dMinErr = 1.E6;
               if ( Gparm.Debug > 0 )
                  logit( "t", "iNWaves=%ld, MIN_WAVEFORMS=%ld\n", iNWaves,
                         MIN_WAVEFORMS );
               if ( iNWaves >= MIN_WAVEFORMS )
               {
/* Loop through different Green's Function depths */			   
                  for ( i=iDepthStart; i<iDepthEnd; i++ )
                  {
                     if ( Gparm.Debug > 0 )
                        logit( "", "Invert at %ldkm Depth\n", iGFDepths[i] );
                     for ( ii=0; ii<Nsta; ii++ )
                        MtStuff[ii].iUse = MtStuff[ii].iUseOrig;
                     InitMtR( &MtResults, &Gparm );
                     for ( ii=0; ii<Nsta; ii++ )
                        if ( MtStuff[ii].iUse == 1 )
                        {  
/* Choose proper Green's Function and filter same as data */
                           FindGFIndices( GreensFunctions,
                            MtStuff[ii].azdelt.dDelta*111.1, iGFDepths[i],
                            iGF2Display );
                           if ( Gparm.Debug > 1 )
                              logit( "", "%ld %s %lf degrees GF=%ld %ld %ld\n", ii,
                               MtStuff[ii].szStation, MtStuff[ii].azdelt.dDelta,
	                           iGF2Display[0], iGF2Display[1], iGF2Display[2] );
                           for ( k=0; k<iWindowTotal; k++ )
                           {
                              MtStuff[ii].dGFFiltFfds[k] =
                               GreensFunctions[iGF2Display[0]].fGF[k];
                              MtStuff[ii].dGFFiltVss[k]  =
                               GreensFunctions[iGF2Display[1]].fGF[k];
                              MtStuff[ii].dGFFiltVds[k]  =
                               GreensFunctions[iGF2Display[2]].fGF[k];
                           }
						
/* Filter GFs as data */	
                           if ( Gparm.Debug > 1 ) logit( "", "Filter GFs\n");
                           DeconvolveWF( MtStuff[ii].dGFFiltFfds,
                            iWindowTotal, 1./GreensFunctions[iGF2Display[0]].dSR,
                            pFinalResponse, pFinalResponse, taperFreqs,
                            dDataOut, DECON_BUFFER );
                           for ( j=0; j<iWindowTotal; j++ )
                              MtStuff[ii].dGFFiltFfds[j] = dDataOut[j];
                           DeconvolveWF( MtStuff[ii].dGFFiltVss,
                            iWindowTotal, 1./GreensFunctions[iGF2Display[1]].dSR,
                            pFinalResponse, pFinalResponse, taperFreqs,
                            dDataOut, DECON_BUFFER );
                           for ( j=0; j<iWindowTotal; j++ )
                              MtStuff[ii].dGFFiltVss[j] = dDataOut[j];
                           DeconvolveWF( MtStuff[ii].dGFFiltVds,
                            iWindowTotal, 1./GreensFunctions[iGF2Display[2]].dSR,
                            pFinalResponse, pFinalResponse, taperFreqs,
                            dDataOut, DECON_BUFFER );
                           for ( j=0; j<iWindowTotal; j++ )
                              MtStuff[ii].dGFFiltVds[j] = dDataOut[j];
                           if ( Gparm.Debug > 1 ) logit( "", "GFs Filtered\n");
                        }
/* Perform Inversion for this depth */
                     if ( Gparm.Debug > 0 ) logit( "", "call mtinv\n");
                     if ( MtInv( &MtResults, MtStuff, Nsta, iWindowTotal,
                          Gparm.Debug ) < 0 )
                        logit( "t", "MtInv problem at %ldkm\n", iGFDepths[i] );
                     else
                     {
                        if ( Gparm.Debug > 1 )
                        {
                           logit( "t", "MtInv complete at %ldkm\n", iGFDepths[i] );
                           EmailOutput( Gparm.EmailFile, &HStruct, MtResults,
                            iNWaves, Nsta, MtStuff, iGFDepths[i], Gparm.Debug );
                        }
/* Align waveform with Green's function and recompute inversion */
                        if ( Gparm.Align == 1 )
                        { /* Set MaxTLag */
                           if ( dTLagSpec < 0. ) dMaxTLag = Gparm.MaxTLag;
                           else                  dMaxTLag = dTLagSpec;
                           if ( Gparm.TimeWindow > iWindowTotal )
                           {
                              Gparm.TimeWindow = iWindowTotal/2;
                              logit( "", "TimeWindow too long, reset\n" );
                           }
                           MtResults.iPass = 2;
                           if ( AlignWf2( Gparm.TimeWindow, dMaxTLag, Nsta,
                                MtStuff, Gparm.Debug, iWindowTotal ) < 0 )
                              logit( "", "Problem in align\n" );
                           else
                           {
                              if ( MtInv( &MtResults, MtStuff, Nsta,
                                   iWindowTotal, Gparm.Debug ) < 0 )
                                 logit( "t", "MtInv problem at %ldkm-2\n",
                                        iGFDepths[i] );
                              else
                              {
                                 if ( Gparm.Debug > 1 )
                                 {
                                    logit( "t", "MtInv complete at %ldkm-2\n",
                                           iGFDepths[i] );
                                    EmailOutput( Gparm.EmailFile, &HStruct,
                                     MtResults, iNWaves, Nsta, MtStuff,
                                     iGFDepths[i], Gparm.Debug );
                                 }
                              }
                           }
                        }		   
/* Now check to see if # of useable stations has changed (if it has, redo) */
                        iCnt = 0;
                        for ( ii=0; ii<Nsta; ii++ )
                           if ( MtStuff[ii].iUse == 1 ) iCnt++;
                        if ( iCnt != iNWaves && iCnt >= MIN_WAVEFORMS ) 
                        {
                           MtResults.iPass = 3;
                           if ( MtInv( &MtResults, MtStuff, Nsta,
                                iWindowTotal, Gparm.Debug ) < 0 )
                              logit( "t", "MtInv problem at %ldkm-3\n",
                                     iGFDepths[i] );
                           else
                           {
                              iCnt = 0;
                              for ( ii=0; ii<Nsta; ii++ )
                                 if ( MtStuff[ii].iUse == 1 ) iCnt++;
                              if ( Gparm.Debug > 1 )
                              {
                                 logit( "t", "MtInv complete at %ldkm-3\n",
                                        iGFDepths[i] );
                                 EmailOutput( Gparm.EmailFile, &HStruct,
                                  MtResults, iCnt, Nsta, MtStuff, iGFDepths[i],
                                  Gparm.Debug );
                              }
                           }
                        }
                     }
                     if ( MtResults.dMisFitAvg < dMinErr &&
                          iCnt >= MIN_WAVEFORMS)
                     {
                        dMinErr = MtResults.dMisFitAvg;
                        iDepthIndex = i;
                        dMag = MtResults.dMag;
                     }
                  }
               }
               else
                  if ( Gparm.Debug > 0 )
                     logit( "t", "Not enough waveforms (%ld)\n", iNWaves );
				  
/* Now, take minimum error depth, and recompute - changing filters and
   MaxLag if necessary. Re-filter data if necessary. */				  
               if ( iNWaves >= MIN_WAVEFORMS && dMag > 0.)
               {
                  for ( ii=0; ii<Nsta; ii++ )
                     MtStuff[ii].iUse = MtStuff[ii].iUseOrig;
                  InitMtR( &MtResults, &Gparm );
				  
/* See if magnitude requires re-filtering */
                  if ( dMag > 6.5 && dMag <= 7.0 && dFiltLoSpec < 0. )
                  {
                     taperFreqs[0] = 0.005;
                     taperFreqs[1] = 0.00666;
                  }
                  else if ( dMag > 7.0 && dFiltLoSpec < 0. )
                  {
                     taperFreqs[0] = 0.004;
                     taperFreqs[1] = 0.005;
                  }
				  
                  if ( dMag > 6.5 && dMag <= 7.0 && dFiltHiSpec < 0. )
                  {
                     taperFreqs[3] = 0.1;
                     taperFreqs[2] = 0.05;
                  }
                  else if ( dMag > 7.0 && dFiltHiSpec < 0. )
                  {
                     taperFreqs[3] = 0.05;
                     taperFreqs[2] = 0.025;
                  }
				  
/* Reset MaxTLag based on magnitude */
                  if ( dMag > 6.5 && dMag <= 7.0 && dTLagSpec < 0. )
                     dMaxTLag = 12.;
                  else if ( dMag > 7.0 && dTLagSpec < 0. )
                     dMaxTLag = 18.;

                  for ( ii=0; ii<Nsta; ii++ )
                     if ( MtStuff[ii].iUse == 1 )
                     {  						
/* Re-filter and deconvolve data */	
                        FillResponse( pResponse, &MtStuff[ii] );
                        DeconvolveWF( MtStuff[ii].dData,
                         MtStuff[ii].iNPts,
                         1./StaArray[ii].dSampRate, pResponse,
                         pFinalResponse, taperFreqs, dDataOut,
                         DECON_BUFFER );
                        free( pResponse->Poles );
                        free( pResponse->Zeros );
                        for ( j=0; j<iWindowTotal; j++ )
                           MtStuff[ii].dDataFilt[j] = dDataOut[j+DATA_PREEVENT];
                						 
/* Get Sum-square of amps */
                        CheckDataForSSAmp( dDataOut, &StaArray[ii],
                                           &MtStuff[ii], iWindowTotal );
						   
/* Choose proper Green's Function and filter same as data */
                        FindGFIndices( GreensFunctions,
                         MtStuff[ii].azdelt.dDelta*111.1, iGFDepths[iDepthIndex],
                         iGF2Display );
                        for ( k=0; k<iWindowTotal; k++ )
                        {
                           MtStuff[ii].dGFFiltFfds[k] =
                            GreensFunctions[iGF2Display[0]].fGF[k];
                           MtStuff[ii].dGFFiltVss[k]  =
                            GreensFunctions[iGF2Display[1]].fGF[k];
                           MtStuff[ii].dGFFiltVds[k]  =
                            GreensFunctions[iGF2Display[2]].fGF[k];
                        }
			
/* Filter GFs as data */	
                        DeconvolveWF( MtStuff[ii].dGFFiltFfds,
                         iWindowTotal, 1./GreensFunctions[iGF2Display[0]].dSR,
                         pFinalResponse, pFinalResponse, taperFreqs,
                         dDataOut, DECON_BUFFER );
                        for ( j=0; j<iWindowTotal; j++ )
                           MtStuff[ii].dGFFiltFfds[j] = dDataOut[j];
                        DeconvolveWF( MtStuff[ii].dGFFiltVss,
                         iWindowTotal, 1./GreensFunctions[iGF2Display[1]].dSR,
                         pFinalResponse, pFinalResponse, taperFreqs,
                         dDataOut, DECON_BUFFER );
                        for ( j=0; j<iWindowTotal; j++ )
                           MtStuff[ii].dGFFiltVss[j] = dDataOut[j];
                        DeconvolveWF( MtStuff[ii].dGFFiltVds,
                         iWindowTotal, 1./GreensFunctions[iGF2Display[2]].dSR,
                         pFinalResponse, pFinalResponse, taperFreqs,
                         dDataOut, DECON_BUFFER );
                        for ( j=0; j<iWindowTotal; j++ )
                           MtStuff[ii].dGFFiltVds[j] = dDataOut[j];
                     } 							  
/* Perform Inversion for this depth */
                  if ( MtInv( &MtResults, MtStuff, Nsta, iWindowTotal,
                       Gparm.Debug ) < 0 )
                     logit( "t", "MtInv problem at %ldkm-a\n",
                      iGFDepths[iDepthIndex] );
                  else
                  {
                     if ( Gparm.Debug > 0 )
                        logit( "t", "MtInv complete at %ldkm-a\n",
                         iGFDepths[iDepthIndex] );
                     EmailOutput( Gparm.EmailFile, &HStruct, MtResults,
                      iNWaves, Nsta, MtStuff, iGFDepths[iDepthIndex],
                      Gparm.Debug );
/* Align waveform with Green's function and recompute inversion */
                     if ( Gparm.Align == 1 )
                     {
                        if ( Gparm.TimeWindow > iWindowTotal )
                        {
                           Gparm.TimeWindow = iWindowTotal/2;
                           logit( "", "TimeWindow too long, reset\n" );
                        }
                        MtResults.iPass = 2;
                        if ( AlignWf2( Gparm.TimeWindow, dMaxTLag, Nsta,
                             MtStuff, Gparm.Debug, iWindowTotal ) < 0 )
                           logit( "", "Problem in align\n" );
                        else
                        {
                           if ( MtInv( &MtResults, MtStuff, Nsta,
                                iWindowTotal, Gparm.Debug ) < 0 )
                              logit( "t", "MtInv problem at %ldkm-2a\n",
                                     iGFDepths[iDepthIndex] );
                           else
                           {
                              if ( Gparm.Debug > 0 )
                                 logit( "t", "MtInv complete at %ldkm-2a\n",
                                        iGFDepths[iDepthIndex] );
                              EmailOutput( Gparm.EmailFile, &HStruct,
                               MtResults, iNWaves, Nsta, MtStuff,
                               iGFDepths[iDepthIndex], Gparm.Debug );
                           }
                        }
                     }		   
/* Now check to see if # of useable stations has changed (if it has, redo) */
                     iCnt = 0;
                     for ( ii=0; ii<Nsta; ii++ )
                        if ( MtStuff[ii].iUse == 1 ) iCnt++;
                     if ( iCnt != iNWaves && iCnt >= MIN_WAVEFORMS ) 
                     {
                        MtResults.iPass = 3;
                        if ( MtInv( &MtResults, MtStuff, Nsta,
                             iWindowTotal, Gparm.Debug ) < 0 )
                           logit( "t", "MtInv problem at %ldkm-3a\n",
                                  iGFDepths[iDepthIndex] );
                        else
                        {
                           if ( Gparm.Debug > 0 )
                              logit( "t", "MtInv complete at %ldkm-3a\n",
                                     iGFDepths[iDepthIndex] );
                           iCnt = 0;
                           for ( ii=0; ii<Nsta; ii++ )
                              if ( MtStuff[ii].iUse == 1 ) iCnt++;
                           if ( iCnt >= MIN_WAVEFORMS ) 
                              EmailOutput( Gparm.EmailFile, &HStruct,
                               MtResults, iCnt, Nsta, MtStuff,
                               iGFDepths[iDepthIndex], Gparm.Debug );
                           else
                              logit( "t", "Not enough waveforms (%ld) after "
                                     "pass 3\n", iNWaves );
                        }
                     }
                  }
               }
               else
                  logit( "t", "Still not enough waveforms (%ld)\n", iNWaves );
					 	
/* Update MT results file */
               if ( MtResults.dMag > 0. )
                  if ( (hFile = fopen( Gparm.MTFile, "a" )) != NULL )
                  {
                     fprintf( hFile, "%Mw = %lf, Moment = %lf\n",
                      MtResults.dMag, MtResults.dScalarMom );
                     fclose( hFile );
                  }						   
               iCnt = 0;
               for ( ii=0; ii<Nsta; ii++ )
                  if ( MtStuff[ii].iUse == 1 ) iCnt++;
               if ( iCnt >= MIN_WAVEFORMS ) iWaveFormDisplay = 2;
						
/* Update the title bar */				  
               strcpy( szTitle, szProcessName );
               SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
               iProcMoment = 0;
               logit( "t", "processing stopped in COMPUTE_MOMENT-2\n" );
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_SHOW_GFS:      /* Draw Green's Functions on screen */
/* Enter depth and distance of function you with to display */			   
               if ( DialogBox( hInstMain, "GFDisplay", hwndWndProc, 
                   (DLGPROC) GFDisplayDlgProc ) == IDCANCEL ) break;
               iWaveFormDisplay = 1;
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_SHOW_SYNTHS:   /* Draw latest synthetics and data */
               iWaveFormDisplay = 2;
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_REFRESH:       /* redraw the screen */
               iWaveFormDisplay = 0;
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_EXIT_MAIN:     /* Send DESTROY message to itself */
               PostMessage( hwnd, WM_DESTROY, 0, 0 );
               break;          
         }
         break ;

/* Fill in screen display */
      case WM_PAINT:
         hdc = BeginPaint( hwnd, &ps );         /* Get device context */
         GetClientRect( hwnd, &rct );
         FillRect( hdc, &rct, (HBRUSH) GetStockObject( WHITE_BRUSH ) );
         if ( iWaveFormDisplay == 1 )
            DisplayGFs( hdc, GreensFunctions, lTitleFHt, lTitleFWd, cxScreen,
                        cyScreen, iVScrollOffset );
         else if ( iWaveFormDisplay == 2 )
         {
            DisplaySoln( hdc, MtStuff, Gparm.EmailFile, lTitleFHt2, lTitleFWd2,
                         cxScreen, cyScreen, iVScrollOffset );
            DisplaySynths( hdc, MtStuff, lTitleFHt, lTitleFWd, cxScreen,
                           cyScreen, Nsta, iVScrollOffset, iWindowTotal );
         }
         SetScrollPos( hwnd, SB_VERT, iVScrollOffset, TRUE );
         EndPaint( hwnd, &ps );
         break;
	
/* Vertical scroll message */	
      case WM_VSCROLL:
         if ( LOWORD (wParam) == SB_LINEUP )
            { if ( iVScrollOffset >= iScrollVertBuff )
               iVScrollOffset -= iScrollVertBuff; }
         else if ( LOWORD (wParam) == SB_PAGEUP )
            { if ( iVScrollOffset >= iScrollVertPage )
               iVScrollOffset -= iScrollVertPage; }
         else if ( LOWORD (wParam) == SB_LINEDOWN )
            { if ( iVScrollOffset <= 3*cyScreen - iScrollVertBuff )
               iVScrollOffset += iScrollVertBuff; }
         else if ( LOWORD (wParam) == SB_PAGEDOWN )
            { if ( iVScrollOffset <= 3*cyScreen - iScrollVertPage )
               iVScrollOffset += iScrollVertPage; }
         InvalidateRect( hwnd, NULL, TRUE );
         break;

/* Close up shop and return */
      case WM_DESTROY:
         logit( "", "WM_DESTROY posted\n" );
         PostQuitMessage( 0 );
         break;

      default:
         return ( DefWindowProc( hwnd, msg, wParam, lParam ) );
   }
return 0;
}

/****************************************************************************
This function reads the .ini file containing information pertinent to the
running of this program.

        Function:           ReadMtSoloIni
        Function List:      GP         // Control parameter structure
        Author:             Whitmore
        Written:            February, 2003
        OS:                 Windows 2000
        Compiler:           Microsoft Visual C++ v6.0
        Returns:            int        1 if read went OK; 0 otherwise
---------------------------------------------------------------------------*/
int ReadMtSoloIni( GPARM *GP )
{
   FILE    *hFile;             /* Ini file handle */

/* Try to open the ini file */
   if ( (hFile = (FILE *) fopen( "mtsolo.ini", "r" )) == NULL )
   {
      logit( "t", "Couldn't open mtsolo.ini\n" );
      return 0;
   }
   
/* Read initialization values */
   fscanf( hFile, "Log file directory: %s\n", szLogDir );
   fscanf( hFile, "Station data file: %s\n", &GP->StaDataFile );
   fscanf( hFile, "Station directory: %s\n", &GP->RootDirectory );
   fscanf( hFile, "Archive directory: %s\n", &GP->ArchDirectory );
   fscanf( hFile, "Green's functions directory: %s\n", &GP->GFDir );
   fscanf( hFile, "Output P Data File: %s\n", &GP->EBPTimeFile );
   fscanf( hFile, "Dummy File: %s\n", &GP->DummyFile );
   fscanf( hFile, "LP File Size (in minutes): %ld\n", &GP->LPFileSize );
   fscanf( hFile, "LP File Suffix: %s\n", &GP->FileSuffix );
   fscanf( hFile, "Mw Results File: %s\n", &GP->MTFile );
   fscanf( hFile, "Mw EMail File: %s\n", &GP->EmailFile );
   fscanf( hFile, "Max Depth File: %s\n", &GP->DepthFile );
   fscanf( hFile, "Broadband Response File: %s\n", &GP->ResponseFile );
   fscanf( hFile, "Align synthetics and waveforms: %ld\n", &GP->Align );
   fscanf( hFile, "Window length of seismograms: %lf\n", &GP->TimeWindow );
   fscanf( hFile, "Max time shift in wave alignment: %lf\n", &GP->MaxTLag );
   fscanf( hFile, "Amount of time to read from disk (minutes): %ld\n", &GP->ReadTime );
   fscanf( hFile, "Green's Function Sample Rate (samples/sec): %lf\n", &GP->GFSampRate );
   fscanf( hFile, "Debug (0=little, 1=some, 2=all): %ld\n", &GP->Debug );
   fclose( hFile );     /* Close file; OK for this part */
   
return 1;
}
