
      /*****************************************************************
       *                       mtinver.c                               *
       *                                                               *
       *  This program determines moment tensor solutions              *
       *  from broadband seismic data.                                 *
       *                                                               *
       *  The original FORTRAN code was donated by Harley Benz of NEIC.*
       *  This was converted to C and made into an earthworm module by *
       *  Whitmore at the WC/ATWC.                                     *
       *                                                               *
       *  Input seismic data is taken from an earthworm ring           *
       *  (TYPE_TRACEBUF), a disk file, or an archive file.  The data  *
       *  is buffered by this module.                                  *
       *  Processing is triggered by a TYPE_HYPOTWC message obtained   *
       *  from the HYPO_RING or by a menu option.  Parameters are      *
       *  computed for the last located earthquake.                    *
       *                                                               *
       *  Two sets of Station and MT arrays are maintained so that     *
       *  the real time data buffers can continue to fill while older  *
       *  data is being processed.                                     *
       *                                                               *
       *  Input seismic data must be at the same sample rate as the    *
       *  Green's functions (1sps).  If data is not still in memory,   *
       *  it is read from disk files or the archives. Disk and archive *
       *  data must be in WC/ATWC format.                              *
       *                                                               *
       *  Output from this module is written to a data file and to a   *
       *  Windows window.                                              *
       *                                                               *
       *  Original code contributed by Benz                            *
       *  Converted to C and ew module by Whitmore (WC/ATWC) - 2002-4  *
       *  Compiler: Microsoft Visual C/C++ v6.0                        *
       *                                                               *
       *  October, 2005: Added options to run this module in           *
       *                 conjunction with ATPlayer.                    *
       *                                                               *
       ****************************************************************/
	   
#include <windows.h>
#include <wingdi.h>                                                 
#include <winuser.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <math.h>
#include <earthworm.h>
#include <transport.h>
#include <sachead.h>
#include <swap.h>
#include "mtinver.h"

/* Global Variables 
   ****************/
double  dFiltHiSpec;           /* Short period filter cutoff (s) */
double  dFiltLoSpec;           /* Long period filter cutoff (s) */
double  dLastEndTime;          /* End time of last data */
double  dTLagSpec;             /* Max time adjustment (s) */
EWH     Ewh;                   /* Parameters from earthworm.h */
MSG_LOGO getlogoW;             /* Logo of requested waveforms */
GF      GreensFunctions[NUM_GFS]; /* Green's function computed signal */
GPARM   Gparm;                 /* Configuration file parameters */
HINSTANCE hInstMain;           /* Copy of main program instance (process id) */
MSG_LOGO hrtlogo;              /* Logo of outgoing heartbeats */
HYPO    HStruct;               /* Hypocenter data structure */
HWND    hwndWndProc;           /* Client window handle */
int     iDepthSpec;            /* Fixed depth for inversion (km) */
int     iGF2Display[3];        /* Indices of Green's Functions to display */
int     iGFDepths[NUM_GF_DEPTHS] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 
                     100, 120, 140, 160, 180, 200, 220, 240, 260, 280,
                     300, 320, 340, 360, 380, 400, 420, 440, 460, 480,
                     500, 520, 540, 560, 580, 600, 620, 640, 660};
int     iProcFlag;             /* 1->Start the Auto Compute timer */
int     iWindowTotal;          /* Comparison window length (s) */
MSG     msg;                   /* Windows control message variable */
mutex_t mutsem1;               /* Semaphore to protect variable adjustments */
pid_t   myPid;                 /* Process id of this process */
int     Nsta;                  /* Number of stations to display */
MTRESULTS MtResults;           /* Inversion results */
MTSTUFF *MtStuff;              /* Broadband Response array, etc. */
MTSTUFF MtStuffD[MAXWVS];      /* Broadband Response array, etc. (disk data) */
PPICK   *PBuf;                 /* Pointer to P-pick buffer */
STATION *StaArray;             /* Station data array */
STATION StaArrayD[MAXWVS];     /* Station data array (disk data)*/
char    szProcessName[] = "CMT Inversion";
char    szTitle[] = "CMT Inversion"; /* String to load in Title bar */
time_t  then;                  /* Previous heartbeat time */
char    *WaveBuf;              /* Pointer to waveform buffer */
TRACE_HEADER *WaveHead;        /* Pointer to waveform header */
long    *WaveLong;             /* Long pointer to waveform data */
short   *WaveShort;            /* Short pointer to waveform data */

      /***********************************************************
       *              The main program starts here.              *
       *                                                         *
       *  Argument:                                              *
       *     argv[1] = Name of configuration file                *
       ***********************************************************/

int WINAPI WinMain (HINSTANCE hInst, HINSTANCE hPreInst, 
                    LPSTR lpszCmdLine, int iCmdShow)
{
   char          configfile[64];  /* Name of config file */
   HDC     hIC;	                  /* Information context to check for info */
   int           i;
   long          InBufl;          /* Maximum message size in bytes */
   char          line[40];        /* Heartbeat message */
   int           lineLen;         /* Length of heartbeat message */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */
   long          RawBufl;         /* Raw data buffer size */
   static unsigned tidH;          /* Hypocenter getter Thread */
   static unsigned tidW;          /* Waveform getter Thread */
   static WNDCLASS wc;
   
   hInstMain = hInst;
   iProcFlag = 0;
   dLastEndTime = 0.;             
   
/* Get config file name (format "mtinver mtinver.D")
   *************************************************/
   if ( strlen( lpszCmdLine ) <= 0 )
   {
      fprintf( stderr, "Need configfile in start line.\n" );
      return -1;
   }
   strcpy( configfile, lpszCmdLine );

/* Get parameters from the configuration files
   *******************************************/
   if ( GetConfig( configfile, &Gparm ) == -1 )
   {
      fprintf( stderr, "GetConfig() failed. file %s.\n", configfile );
      return -1;
   }

/* Initialize name of log-file & open it
   *************************************/
   logit_init( configfile, Gparm.MyModId, 256, 1 );

/* Log the configuration parameters
   ********************************/
   LogConfig( &Gparm );

/* Look up info in the earthworm.h tables
   **************************************/
   if ( GetEwh( &Ewh ) < 0 )
   {
      logit( "", "mtinver: GetEwh() failed. Exiting.\n" );
      return -1;
   }

/* Specify logos of incoming waveforms and outgoing heartbeats
   ***********************************************************/
   getlogoW.instid = Ewh.GetThisInstId;
   getlogoW.mod    = Ewh.GetThisModId;
   getlogoW.type   = Ewh.TypeWaveform;

   hrtlogo.instid = Ewh.MyInstId;
   hrtlogo.mod    = Gparm.MyModId;
   hrtlogo.type   = Ewh.TypeHeartBeat;

/* Get our own pid for restart purposes
   ************************************/
   myPid = getpid();
   if ( myPid == -1 )
   {
      logit( "e", "mtinver: Can't get my pid. Exiting.\n" );
      return -1;
   }

/* Allocate the waveform buffers
   *****************************/
   InBufl = MAX_TRACEBUF_SIZ*2;
   WaveBuf = (char *) malloc( (size_t) InBufl );
   if ( WaveBuf == NULL )
   {
      logit( "et", "mtinver: Cannot allocate waveform buffer\n" );
      return -1;
   }

/* Point to header and data portions of waveform message
   *****************************************************/
   WaveHead  = (TRACE_HEADER *) WaveBuf;
   WaveLong  = (long *) (WaveBuf + sizeof (TRACE_HEADER));
   WaveShort = (short *) (WaveBuf + sizeof (TRACE_HEADER));

/* Read the station list and return the number of stations found.
   Allocate the station list array for the real time processing.
   **************************************************************/
   if ( GetStaList( &StaArray, &Nsta, &Gparm, &MtStuff ) == -1 )
   {
      logit( "", "mtinver: GetStaList() failed. Exiting.\n" );
	  free( WaveBuf );
      return -1;
   }
   if ( Nsta == 0 )
   {
      logit( "et", "mtinver: Empty station list. Exiting." );
      free( WaveBuf );
      free( StaArray );
      free( MtStuff );
      return -1;
   }
   logit( "t", "mtinver: Analyzing %d real-time stations.\n", Nsta );
   
/* Allocate the waveform buffers for disk data
   *******************************************/
   for ( i=0; i<MAXWVS; i++ )
   {		 		                  
/* Allocate memory for raw circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples)
   ********************************/
      StaArrayD[i].lRawCircSize = (long) (Gparm.GFSampRate *
                                  (double)Gparm.ReadTime*60.+0.1);
      RawBufl = sizeof (long) * StaArrayD[i].lRawCircSize;
      StaArrayD[i].plRawCircBuff = (long *) malloc( (size_t) RawBufl );
      if ( StaArrayD[i].plRawCircBuff == NULL )
      {
         logit( "et", "mtinver: Can't allocate raw circ buffer for %ld\n", i );
         free( WaveBuf );
         free( StaArray );
         free( MtStuff );
         return -1;
      }
   }
   
/* Read in Green's Functions. Functions are 1024s in length, 1 sps,
   vertical component only.  Functions are created based on depth and
   distance. Distances range from 1100km to 10700km and depths from 10km
   to 660km. The .clv, .vss, and .vds component for each are computed.
   *********************************************************************/
   ReadGreensFunctions( GreensFunctions, Gparm.GFDir );

/* If this is the first instance of this program, init window stuff and
   register window (it always is).
   ********************************************************************/
   if ( !hPreInst )
   {  /* Force PAINT when sized and give double click notification */
      wc.style         = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
      wc.lpfnWndProc   = WndProc;         /* Control Window process */
      wc.cbClsExtra    = 0;
      wc.cbWndExtra    = 0;
      wc.hInstance     = hInst;           /* Process id */
      wc.hIcon         = LoadIcon( hInst, "mtinver" );    /* System app icon */
      wc.hCursor       = LoadCursor( NULL, IDC_ARROW );          /* Pointer */
      wc.hbrBackground = (HBRUSH) GetStockObject( WHITE_BRUSH ); /* White */
      wc.lpszMenuName  = "mtinver_menu";   /* Relates to .RC file */
      wc.lpszClassName = szProcessName;
      if ( !RegisterClass( &wc ) )        /* Window not registered */
      {
         logit( "t", "RegisterClass failed\n" );
         free( WaveBuf );
         free( StaArray );
         free( MtStuff );
         for ( i=0; i<MAXWVS; i++ ) free( StaArrayD[i].plRawCircBuff );
         return -1;
      }
   }

/* Create the window
   *****************/
   hIC = CreateIC ("DISPLAY", NULL, NULL, NULL);
   if (hIC == NULL)
   {
      logit( "t", "CreateIC error\n" );
      free( WaveBuf );
      free( StaArray );
      free( MtStuff );
      for ( i=0; i<MAXWVS; i++ ) free( StaArrayD[i].plRawCircBuff );
      return -1;
   }
   hwndWndProc = CreateWindow(
                 szProcessName,          /* Process name */
                 szProcessName,          /* Initial title bar caption */
                 WS_OVERLAPPEDWINDOW | WS_VSCROLL,
//                 CW_USEDEFAULT,          /* top left x starting location */
//                 CW_USEDEFAULT,          /* top left y starting location */
//                 CW_USEDEFAULT,          /* Initial screen width in pixels */
//                 CW_USEDEFAULT,          /* Initial screen height in pixels */
                 3*GetDeviceCaps (hIC, HORZRES)/4, /* top left x starting loc */
                 0,                      /* top left y starting location */
                 GetDeviceCaps (hIC, HORZRES)/4, /* Window width in pixels */
                 GetDeviceCaps (hIC, VERTRES)/2, /* Window height in pixels */
                 NULL,                   /* No parent window */
                 NULL,                   /* Use standard system menu */
                 hInst,                  /* Process id */
                 NULL );                 /* No extra data to pass in */
   if ( hwndWndProc == NULL )            /* Window not created */
   {
      logit( "t", "CreateWindow failed\n" );
      free( WaveBuf );
      free( StaArray );
      free( MtStuff );
      for ( i=0; i<MAXWVS; i++ ) free( StaArrayD[i].plRawCircBuff );
      return 0;
   }
   
   ShowWindow( hwndWndProc, iCmdShow );  /* Show the Window */
   UpdateWindow( hwndWndProc );          /* Force an initial PAINT call */

/* Attach to existing transport rings
   **********************************/
   tport_attach( &Gparm.InRegion,  Gparm.InKey );
   tport_attach( &Gparm.HRegion,   Gparm.HKey );

/* Flush the input waveform ring
   *****************************/
   while ( tport_getmsg( &Gparm.InRegion, &getlogoW, 1, &logo, &MsgLen,
                         WaveBuf, MAX_TRACEBUF_SIZ ) != GET_NONE );

/* Send 1st heartbeat to the transport ring
   ****************************************/
   time( &then );
   sprintf( line, "%d %d\n", then, myPid );
   lineLen = strlen( line );
   if ( tport_putmsg( &Gparm.InRegion, &hrtlogo, lineLen, line ) != PUT_OK )
   {
      logit( "et", "mtinver: Error sending 1st heartbeat. Exiting." );
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.HRegion );
      free( WaveBuf );
      free( StaArray );
      free( MtStuff );
      for ( i=0; i<MAXWVS; i++ ) free( StaArrayD[i].plRawCircBuff );
      return 0;
   }

/* Allocate and init the P-pick buffer
   ***********************************/
   InBufl = sizeof( PPICK ) * MAXWVS; 
   PBuf = (PPICK *) malloc( (size_t) InBufl );
   if ( PBuf == NULL )
   {
      logit( "et", "mtinver: Can't allocate P-pick buffer." );
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.HRegion );
      free( WaveBuf );
      free( StaArray );
      free( MtStuff );
      for ( i=0; i<MAXWVS; i++ ) free( StaArrayD[i].plRawCircBuff );
      return 0;
   }
   for ( i=0; i<MAXWVS; i++ ) InitP( &PBuf[i] );
   
/* Create a mutex for protecting adjustments of variables
   ******************************************************/
   CreateSpecificMutex( &mutsem1 );

/* Start the Hypocenter getter thread
   **********************************/
   if ( StartThread( HThread, 8192, &tidH ) == -1 )
   {
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.HRegion );
      free( WaveBuf );
      free( StaArray );
      free( MtStuff );
      for ( i=0; i<MAXWVS; i++ ) free( StaArrayD[i].plRawCircBuff );
      free( PBuf );
      logit( "et", "Error starting H thread; exiting!\n" );
      return -1;
   }

/* Start the waveform getter tread
   *******************************/
   if ( StartThread( WThread, 8192, &tidW ) == -1 )
   {
      tport_detach( &Gparm.InRegion );
      tport_detach( &Gparm.HRegion );
      free( WaveBuf );
      free( MtStuff );
      free( PBuf );
      for ( i=0; i<MAXWVS; i++ ) free( StaArrayD[i].plRawCircBuff );
      free( StaArray );
      logit( "et", "Error starting W thread; exiting!\n" );
      return -1;
   }
	 
/* Start a timer to monitor auto-location times and CMT comps.
   ***********************************************************/
   SetTimer( hwndWndProc, ID_TIMER, CHECK_INTERVAL, NULL );
   
/* Main windows loop
   *****************/      
   while ( GetMessage( &msg, NULL, 0, 0 ) )
   {
      TranslateMessage( &msg );
      DispatchMessage( &msg );
   }

/* Detach from the ring buffers
   ****************************/
   tport_detach( &Gparm.InRegion );
   tport_detach( &Gparm.HRegion );
   logit( "", "Post WM_DESTROY at end of Main\n");
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
   free( WaveBuf );
   free( MtStuff );
   free( PBuf );
   for ( i=0; i<Nsta; i++ ) free( StaArray[i].plRawCircBuff );
   for ( i=0; i<MAXWVS; i++ ) free( StaArrayD[i].plRawCircBuff );
   free( StaArray );
   logit( "t", "Termination requested. Exiting.\n" );
   return (msg.wParam);
}

      /*******************************************************
       *                      GetEwh()                       *
       *                                                     *
       *      Get parameters from the earthworm.d file.      *
       *******************************************************/

int GetEwh( EWH *Ewh )
{
   if ( GetLocalInst( &Ewh->MyInstId ) != 0 )
   {
      fprintf( stderr, "mtinver: Error getting MyInstId.\n" );
      return -1;
   }
   if ( GetInst( "INST_WILDCARD", &Ewh->GetThisInstId ) != 0 )
   {
      fprintf( stderr, "mtinver: Error getting GetThisInstId.\n" );
      return -2;
   }
   if ( GetModId( "MOD_WILDCARD", &Ewh->GetThisModId ) != 0 )
   {
      fprintf( stderr, "mtinver: Error getting GetThisModId.\n" );
      return -3;
   }
   if ( GetType( "TYPE_HEARTBEAT", &Ewh->TypeHeartBeat ) != 0 )
   {
      fprintf( stderr, "mtinver: Error getting TypeHeartbeat.\n" );
      return -4;
   }
   if ( GetType( "TYPE_ERROR", &Ewh->TypeError ) != 0 )
   {
      fprintf( stderr, "mtinver: Error getting TypeError.\n" );
      return -5;
   }
   if ( GetType( "TYPE_TRACEBUF", &Ewh->TypeWaveform ) != 0 )
   {
      fprintf( stderr, "mtinver: Error getting TYPE_TRACEBUF.\n" );
      return -6;
   }
   if ( GetType( "TYPE_HYPOTWC", &Ewh->TypeHypoTWC ) != 0 )
   {
      fprintf( stderr, "mtinver: Error getting TYPE_HYPOTWC.\n" );
      return -7;
   }
   return 0;
}

  /******************************************************************
   *                           GetLDC()                             *
   *                                                                *
   *  Determine and update moving averages of signal value          *
   *  (called LDC here).                                            *
   *                                                                *
   *  Arguments:                                                    *
   *    lNSamps     Number of samples in this packet                *
   *    WaveLong    Pointer to data array                           *
   *    pdLDC       Pointer to long term DC average                 *
   *    lFiltSamp   # samples through filter                        *
   *                                                                *
   ******************************************************************/

void GetLDC( long lNSamps, long *WaveLong, double *pdLDC, long lFiltSamp )
{
   double  dSumLDC;     /* Summation of all values in this packet */
   int     i;
   
   dSumLDC = 0.;
   
/* Sum up total for this packet */
   for ( i=0; i<lNSamps; i++ )
   {
      if ( lFiltSamp == 0 ) dSumLDC  += ((double) WaveLong[i]);
      else                  dSumLDC  += ((double) WaveLong[i] - *pdLDC);
   }
	  
/* Compute new LTAs (first time through, just take average.  Adjust average
   from there */
   if ( lFiltSamp == 0 ) *pdLDC =  (dSumLDC/(double) lNSamps);
   else                  *pdLDC += (0.5 * dSumLDC/(double) lNSamps);
}

      /*********************************************************
       *                     HThread()                         *
       *                                                       *
       *  This thread gets messages from the hypocenter ring.  *
       *                                                       *
       *********************************************************/
	   
thr_ret HThread( void *dummy )
{
   MSG_LOGO      getlogoH;        /* Logo of requested hypos */
   char          HIn[MAX_HYPO_SIZE];/* Pointer to hypocenter from ring */
   HYPO          HStructT;        /* Temporary Hypocenter data structure */
   static int    iQuakeProc;      /* Quake id being processed now */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */

/* Set up logos for Hypo ring 
   **************************/
   getlogoH.instid = Ewh.GetThisInstId;
   getlogoH.mod    = Ewh.GetThisModId;
   getlogoH.type   = Ewh.TypeHypoTWC;

/* Flush the input ring
   ********************/
   while ( tport_getmsg( &Gparm.HRegion, &getlogoH, 1, &logo, &MsgLen,
                         HIn, MAX_HYPO_SIZE) != GET_NONE );
						 
/* Loop to read hypocenter messages and load buffer
   ************************************************/
   while ( tport_getflag( &Gparm.HRegion ) != TERMINATE )
   {
      int     rc;               /* Return code from tport_getmsg() */

/* Get a hypocenter from transport region
   **************************************/
      rc = tport_getmsg( &Gparm.HRegion, &getlogoH, 1, &logo, &MsgLen,
                         HIn, MAX_HYPO_SIZE);

      if ( rc == GET_NONE )
      {
         sleep_ew( 1000 );
         continue;
      }

      if ( rc == GET_NOTRACK )
         logit( "et", "mtinverT: Tracking error.\n");

      if ( rc == GET_MISS_LAPPED )
         logit( "et", "mtinverT: Got lapped on the ring.\n");

      if ( rc == GET_MISS_SEQGAP )
         logit( "et", "mtinverT: Gap in sequence numbers.\n");

      if ( rc == GET_MISS )
         logit( "et", "mtinverT: Missed messages.\n");

      if ( rc == GET_TOOBIG )
      {
         logit( "et", "mtinverT: Retrieved message too big (%d) for msg.\n",
                MsgLen );
         continue;
      }
	  
/* Put hypocenter into temp structure (NOTE: No byte-swapping is performed, so 
   there will be trouble getting hypos from an opposite order machine)
   *******************************************************************/   
      if ( HypoStruct( HIn, &HStructT ) < 0 )
      {
         logit( "t", "Problem in HypoStruct function - 1\n" );
         continue;
      }
      	  
/* Process data for moment if this quake has the proper parameters.
   ****************************************************************/
      if ( iProcFlag == 0 && HStructT.iNumPs > 12 && 
          (HStructT.dMbAvg > 5.5 || HStructT.dMlAvg > 5.5 ||
           HStructT.dMwpAvg > 5.5) )      /* Start processing */
      {
         logit( "t", "Start moment inv. processing - %ld %ld\n",
                HStructT.iQuakeID, HStructT.iVersion );
         if ( HypoStruct( HIn, &HStruct ) < 0 )
         {
            logit( "t", "Problem in HypoStruct function - 2\n" );
            continue;
         }
         iProcFlag = 1;
         iQuakeProc = HStructT.iQuakeID;
      }
      if ( iProcFlag == 1 && HStructT.iNumPs > 12 &&
           HStructT.iQuakeID == iQuakeProc ) /* Update Version */
      {
         if ( Gparm.Debug > 0 )
            logit( "t", "Update version - %ld\n", HStructT.iVersion );
         HStruct.iVersion = HStructT.iVersion;
      }
   }
}

  /******************************************************************
   *                        PadBuffer()                             *
   *                                                                *
   *  Fill in gaps in the data with DC offset.                      *
   *  Update last data value index.                                 *
   *                                                                *
   *  Arguments:                                                    *
   *    lGapSize    Number of values to pad (+1)                    *
   *    dLDC        DC average                                      *
   *    plBuffCtr   Index tracker in main buffer                    *
   *    plBuff      Pointer to main circular buffer                 *
   *    lBuffSize   # samples in main circular buffer               *
   *                                                                *
   ******************************************************************/

void PadBuffer( long lGapSize, double dLDC, long *plBuffCtr, 
                long *plBuff, long lBuffSize )
{
   long    i;
   
/* Pad the data buffer over the gap interval */
   for ( i=0; i<lGapSize-1; i++ )
   {
      plBuff[*plBuffCtr] = (long) dLDC;
      *plBuffCtr += 1;
      if ( *plBuffCtr == lBuffSize ) *plBuffCtr = 0;
   }	  
}

  /******************************************************************
   *                         PutDataInBuffer()                      *
   *                                                                *
   *  Load the data from the input buffer to the large circular     *
   *  buffer.  Update last data value index.                        *
   *                                                                *
   *  Arguments:                                                    *
   *    WaveHead    Pointer to data buffer header                   *
   *    WaveLong    Pointer to data array                           *
   *    plBuff      Pointer to main circular buffer                 *
   *    plBuffCtr   Index tracker in main buffer                    *
   *    lBuffSize   # samples in main circular buffer               *
   *                                                                *
   ******************************************************************/

void PutDataInBuffer( TRACE_HEADER *WaveHead, long *WaveLong, 
                      long *plBuff, long *plBuffCtr, long lBuffSize )
{
   int     i;
   
/* Copy the data buffer */
   for ( i=0; i<WaveHead->nsamp; i++ )
   {
      plBuff[*plBuffCtr] = WaveLong[i];
      *plBuffCtr += 1;
      if ( *plBuffCtr == lBuffSize ) *plBuffCtr = 0;
   }	  
}

      /***********************************************************
       *                      WndProc()                          *
       *                                                         *
       *  This dialog procedure processes messages from the      *
       *  windows screen and messages passed to it from          *
       *  elsewhere.  All                                        *
       *  display is performed in PAINT.  Menu options control   *
       *  the display.                                           *
       *                                                         *
       ***********************************************************/

long WINAPI WndProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   static  int    cxScreen, cyScreen;        /* Window size in pixels */
   double  dData[MTBUFFER_SIZE]; /* Trace */
   double  dDataOut[DECON_BUFFER];/* Deconvolved trace (w/ inst resp removed */ 
   double  dFileTime;         /* time (1/1/70) at start of file */   
   double  dMag;              /* Magnitude of best depth fit */
   double  dMaxDepth;         /* Maximum depth expected for this region */
   static  double dMaxTLag;   /* Maximum trace shift (s) */
   double  dMinErr;           /* Smallest error for all inversions */
   double  dOldestTime;       /* 1/1/70 time of oldest data in buffer */
   double  dSN;               /* Signal-to-Noise ratio */
   HCURSOR hCursor;           /* Present cursor handle (hourglass or arrow) */
   static  HDC hdc;           /* Device context of screen */
   FILE    *hFile;            /* File pointer */
   static  HANDLE  hMenu;     /* Handle to the menu */
   static  HFONT hPTimeFont;  /* List box font handle (same as titles) */
   int     i, j, k, ii, iCnt; /* Loop Counters */
   int     iArch;             /* 0->Data on disk; 1->Data from archive */
   static  int iCalledFromTimer; /* 1-> processing triggered in Earthworm */
   static  int iDepthStart, iDepthEnd; /* GF depth indices to loop through */
   int     iDepthIndex;       /* Index of best-fitting depth */
   static  int iDisk;         /* 1->data from disk, 0->from RAM */
   int     iDum;
   int     iMin, iMinIndex, iTemp; /* Indices for depth index computations */
   time_t  iTime;             /* time (1/1/70) at start of file */   
   static  int iTimer2Act;    /* 1-> Timer2 has been started */
   int     iNumPAuto;            /* Number Ps read from file */
   static  int iNumSta;          /* Either Nsta or Nsta2 based on caller */
   int     iNWaves;              /* # stations in inversion */
   static  int iScrollVertBuff;  /* Small vertical scroll amount (pxl) */
   static  int iScrollVertPage;  /* Large vertical scroll amount (pxl) */				
   static  int iVScrollOffset;   /* Vertical scoll bar setting */
   static  int iWaveFormDisplay; /* 0->no waveforms shown;
                                    1->Show Green's Functions; */
   LATLON  llIn, llOut;          /* Temp lat/lon structure */
   static MTSTUFF *MtS;       /* Temp MTSTUFF pointer */
   long    lNum;              /* Number of samples to transfer to temp buffer */
   static  long lTitleFHt, lTitleFWd; /* Font height and width */
   static  long lTitleFHt2, lTitleFWd2; /* Font2 height and width */
   int     Nsta2;             /* # stations read from disk */
   static ResponseStruct *pFinalResponse; /* Output response */
   static ResponseStruct *pResponse; /* Response poles/zeroes when known */
   PAINTSTRUCT ps;            /* Paint structure used in WM_PAINT command */
   char    *pszFileToOpen;    /* File Name which should contain disk data */
   static  char    *pszPFileName;  /* File with P data */
   int     rc;                /* Return code from convertWave */
   RECT    rct;               /* RECT (rectangle structure) */
   static STATION *Sta;       /* Temp station pointer */
   char    szDir[64];         /* Path to read seismic data */
   double  taperFreqs[4];     /* Taper variables for deconvolution */
   struct tm *tm;             /* time structure for the file name time */
   time_t  tPresentTime;      /* Present 1/1/70 time in seconds */

/* Respond to user input (menu choices, etc.) and system messages */
   switch ( msg )
   {
      case WM_CREATE:         /* Do this the first time through */
         iNumSta = 0;
         iDisk = 0;
         iTimer2Act = 0;
         iCalledFromTimer = 0;
         iVScrollOffset = 0;                  
         iWaveFormDisplay = 0;
         hCursor = LoadCursor( NULL, IDC_ARROW );
         SetCursor( hCursor );
         hMenu = GetMenu( hwnd );
         SetWindowText( hwnd, szTitle );       /* Display the title */
         if ((pResponse = (ResponseStruct *) malloc 
                          (sizeof (ResponseStruct))) == NULL )
         {
            logit( "", "Could not malloc the Response structure.\n" );
            PostMessage( hwnd, WM_DESTROY, 0, 0 );
         }
         if ((pFinalResponse = (ResponseStruct *) malloc 
                               (sizeof (ResponseStruct))) == NULL )
         {
            logit( "", "Could not malloc the FinalResponse structure.\n" );
            PostMessage( hwnd, WM_DESTROY, 0, 0 );
         }
         pFinalResponse->dGain = 1.;
         pFinalResponse->iNumPoles = 0;
         pFinalResponse->iNumZeros = 0;
         break;
 		 
/* Get screen size in pixels, re-paint, and re-proportion screen */
      case WM_SIZE:
         cyScreen = HIWORD (lParam);
         cxScreen = LOWORD (lParam);
		 
/* Compute font size */
         lTitleFHt = cyScreen / 33;
         lTitleFWd = cxScreen / 100;
         lTitleFHt2 = cyScreen / 50;
         lTitleFWd2 = cxScreen / 115;
	 
/* Set scroll thumb positions */
         iScrollVertBuff = cyScreen / 10;
         iScrollVertPage = cyScreen / 2;
         SetScrollRange( hwnd, SB_VERT, 0, cyScreen*3, FALSE );  
         SetScrollPos( hwnd, SB_VERT, 0, TRUE );

         InvalidateRect( hwnd, NULL, TRUE );    /* Force a re-PAINT */
         break;

      case WM_TIMER:		
         if ( wParam == ID_TIMER ) /* Time to turn on Auto-compute timer? */
         {
            if ( iProcFlag == 1 )  /* Turn on/off Timer2 */
            {
               time( &tPresentTime );
               logit( "t", "In Timer - Ptime=%ld, OTime=%lf\n", tPresentTime,
                      HStruct.dOriginTime );
               if ( tPresentTime-(int) HStruct.dOriginTime > 1320 ) 
               {
                  logit( "t", "Stop Timer2\n" );
                  iProcFlag = 0;				 
                  KillTimer( hwndWndProc, ID_TIMER2 );
                  iTimer2Act = 0;
               }
               else if ( tPresentTime-(int) HStruct.dOriginTime > 540 &&
                         iTimer2Act == 0 ) 
               {
                  logit( "t", "Set Timer2\n" );
                  iTimer2Act = 1;
                  SetTimer( hwndWndProc, ID_TIMER2, COMPUTE_INTERVAL, NULL );
               }
            }
         }		 
         else if ( wParam == ID_TIMER2 ) /* Time to Auto-compute */
         {
            logit( "t", "Timer2 now activated\n" );
            iCalledFromTimer = 1;
            PostMessage( hwnd, WM_COMMAND, IDM_COMPUTE_MOMENT, 0 );
         }
         break;

/* Respond to menu selections */
      case WM_COMMAND:
         switch ( LOWORD (wParam) )
         {			   
            case IDM_STOP_LP:
               RequestSpecificMutex( &mutsem1 );   /* Sem protect buff writes */
               strcpy( szTitle, szProcessName );
               SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
               ReleaseSpecificMutex( &mutsem1 ); /* Let someone else have sem */
               iProcFlag = 0;				 
               KillTimer( hwndWndProc, ID_TIMER2 );
               iTimer2Act = 0;
               logit( "t", "MT processing manually stopped\n" );
               break;
			
            case IDM_COMPUTE_MOMENT:
               iDepthStart = 0;
               iDepthEnd = NUM_GF_DEPTHS;
			                  
/* Get inversion parameters if not called from earthworm */
               if ( iCalledFromTimer == 0 )
               {
                  if ( DialogBox( hInstMain, "InvParams", hwndWndProc, 
                      (DLGPROC) InvParamsDlgProc ) == IDCANCEL ) break;
               }
               else
               {
                  dFiltLoSpec = -1.;
                  dFiltHiSpec = -1.;
                  iDepthSpec = -1;
                  dTLagSpec = -1.;
	              iWindowTotal = GF_TRACELENGTH;
               }
					  
               if ( iWindowTotal < 0 )             /* Base time on mag. */
                   if ( HStruct.dMwpAvg > 6.2 ) iWindowTotal = GF_TRACELENGTH;
                   else                         iWindowTotal = 100;

               RequestSpecificMutex( &mutsem1 );   /* Sem protect buff writes */
			   
/* Initialize P-time array each time and reset iUse arrays */			   
               for ( i=0; i<MAXWVS; i++ ) InitP( &PBuf[i] );
               if ( iCalledFromTimer == 1 )
               {
                  for ( i=0; i<Nsta; i++ ) MtStuff[i].iUse = 0;
                  for ( i=0; i<Nsta; i++ ) MtStuff[i].iUseOrig = 0;
               }
               else
               {
                  for ( i=0; i<MAXWVS; i++ ) MtStuffD[i].iUse = 0;
                  for ( i=0; i<MAXWVS; i++ ) MtStuffD[i].iUseOrig = 0;
               }
			   
/* Get hypo parameters from dummy file and P-times from P-times file
   if not triggered through earthworm.  Only use Ps that were automatically
   or manually picked.  (Depth could throw off expected P). */			   
               if ( iCalledFromTimer == 1 )   /* Get Ps from LOC... file */
               {
                  pszPFileName = GetPFile( &HStruct, Gparm.szLocFilePath );
                  ReadPickFile2( &iNumPAuto, PBuf, pszPFileName, MAXWVS );
               }
               else                           /* Read dummy file and P file */
               {
                  ReadDummyData( &HStruct, Gparm.DummyFile );
                  if ( ReadEBPTimeFile( &iNumPAuto, PBuf,
                                        Gparm.EBPTimeFile, MAXWVS ) == 0 )
                  {
                     ReleaseSpecificMutex( &mutsem1 ); 
                     break;
                  }
                  HStruct.iQuakeID = 0;
                  HStruct.iVersion = 0;
               }
			   
/* Initialize MT results file with hypocenter parameter information */
               if ( (hFile = fopen( Gparm.MTFile, "w" )) != NULL )
               {
                  fprintf( hFile, "%lf\n", HStruct.dLat );
                  fprintf( hFile, "%lf\n", HStruct.dLon );
                  fprintf( hFile, "%lf\n", HStruct.dOriginTime );
                  fclose( hFile );
               }			   
               else
                  logit( "t", "MT file (%s) not opened\n", Gparm.MTFile );

/* Compute proper depth indices */
               if ( iDepthSpec < 0 ) /* Loop through depths */
               {                     /* Get max expected depth */
                  llIn.dLat = HStruct.dLat;
                  llIn.dLon = HStruct.dLon;
                  GeoGraphic( &llOut, &llIn );
                  if ( llOut.dLon >= 180. ) llOut.dLon -= 360.;
                  dMaxDepth = DepthCheck( llOut.dLat, llOut.dLon,
                                          Gparm.DepthFile ); 
                  if ( dMaxDepth > 0. )
                  {
                     iMin = 1000000;
                     iMinIndex = 0;
                     for ( i=0; i<NUM_GF_DEPTHS; i++ )
                     {
                        iTemp = abs( iGFDepths[i] - (int) dMaxDepth );
                        if ( iTemp < iMin )
                        {
                           iMin = iTemp;
                           iMinIndex = i;
                        }
                     }
                     iDepthStart = 0;
                     iDepthEnd = iMinIndex + 1;
                  }
               }
               else                  /* Use only specified depth */
               {
                  iMin = 1000000;
                  iMinIndex = 0;
                  for ( i=0; i<NUM_GF_DEPTHS; i++ )
                  {
                     iTemp = abs( iGFDepths[i] - iDepthSpec );
                     if ( iTemp < iMin )
                     {
                        iMin = iTemp;
                        iMinIndex = i;
                     }
                  }
                  iDepthStart = iMinIndex;
                  iDepthEnd = iDepthStart + 1;
               }
               if ( Gparm.Debug > 0 ) logit( "", "Check depth indices %ld to %ld\n",
                  iDepthStart, iDepthEnd );
				  
/* Empty final results file */				  
               if ( (hFile = fopen( Gparm.EmailFile, "w" )) != NULL )
                  fclose( hFile );
               else
                  logit( "t", "Email file (%s) not opened\n", Gparm.EmailFile );
			   
/* Set title bar text */
               strcpy( szTitle, szProcessName );
               strcat( szTitle, " - PROCESSING MT" );
               SetWindowText( hwndWndProc, szTitle );  /* Display the title */
               logit( "t", "processing started in COMPUTE_MOMENT\n" );
		
/* Load up STATION array with station info for MT; get this from disk
   file header (use o-time disk file). (If not real-time processing) */
               if ( iCalledFromTimer == 0 )
               {
                  Nsta2 = 0;
                  iTime = (time_t) (floor( HStruct.dOriginTime ));
                  tm = TWCgmtime( iTime );
                  dFileTime = floor( HStruct.dOriginTime - ((double) (tm->tm_min
                   % Gparm.LPFileSize)*60.) - (double) tm->tm_sec );
                  iArch = 0;
                  pszFileToOpen = CreateFileName( dFileTime, Gparm.LPFileSize, 
                   Gparm.RootDirectory, Gparm.FileSuffix );
                  if ( (Nsta2 = ReadDiskHeader( pszFileToOpen, StaArrayD,
                                                MAXWVS )) < 0 )
                  {                               /* Bad data read */
/* Try the archive directory if the data can't be found on disk */	       
                     pszFileToOpen = CreateFileName( dFileTime,
                      Gparm.LPFileSize, Gparm.ArchDirectory, Gparm.FileSuffix );
                     if ( (Nsta2 = ReadDiskHeader( pszFileToOpen, StaArrayD,
                                                   MAXWVS )) < 0 )
                     {                            /* Bad data read */
                        strcpy( szTitle, szProcessName );
                        SetWindowText( hwndWndProc, szTitle );/* Re-set title */         
                        logit ("t", "File header not read:%s\n", pszFileToOpen);
                        break;
                     }
                     iArch = 1;
                  }
                  logit( "", "%ld Stns read from data file header\n", Nsta2 );
            
/* Then add response information */
                  for ( i=0; i<Nsta2; i++ ) 
                  {
                     rc = LoadResponseData( &StaArrayD[i], &MtStuffD[i],
                                             Gparm.ResponseFile );
                     if ( rc == -1 )
                     {
                        if ( Gparm.Debug > 0 )
                           logit( "e", "file: %s\n", Gparm.ResponseFile );
                        strcpy( szTitle, szProcessName );
                        SetWindowText( hwndWndProc, szTitle );/* Re-set title */         
                        break;
                     }
                     else if ( rc == 0 )
                     {
                        if ( Gparm.Debug > 0 )
                           logit( "", "scn = %s %s %s - No RESPONSE info\n",
                            StaArrayD[i].szStation, StaArrayD[i].szChannel,
                            StaArrayD[i].szNetID );
                     }
                     else if ( rc == 1 )
                     {
                        if ( Gparm.Debug > 0 )
                           logit( "", "scn = %s %s %s - RESPONSE info read\n",
                            StaArrayD[i].szStation, StaArrayD[i].szChannel,
                            StaArrayD[i].szNetID );
                     }
                     InitVar( &StaArrayD[i] );	  
                     StaArrayD[i].dEndTime = 0.;
                     StaArrayD[i].iFirst = 1;
                  }
                  logit( "", "Response information assigned\n" );

/* Read in all data from O-time to Total time */	       
                  for ( i=0; i<Nsta2; i++ )
                     memset( StaArrayD[i].plRawCircBuff, 0, 
                             sizeof (long) * StaArrayD[i].lRawCircSize );
                  if ( iArch == 0 ) strcpy( szDir, Gparm.RootDirectory );
                  else              strcpy( szDir, Gparm.ArchDirectory );
                  if ( Gparm.Debug > 0 ) logit( "", "Read from %s\n", szDir );
                  rc = ReadDiskDataForMTSolo( Gparm.LPFileSize, Gparm.ReadTime,
                        szDir, Gparm.FileSuffix, HStruct.dOriginTime-(double)
                        DATA_PREEVENT-(double)GF_PREEVENT, Nsta2, StaArrayD,
                        &iDum );
                  if ( rc != 0 )
                  {
                     strcpy( szTitle, szProcessName );
                     SetWindowText( hwndWndProc, szTitle );  /* Re-set title */         
                     logit( "t", "Data file read problem\n" );
                     break;
                  }
                  dOldestTime = HStruct.dOriginTime - (double)DATA_PREEVENT -
                                                      (double)GF_PREEVENT;
                  if ( Gparm.Debug > 0 ) logit( "", "Disk data read in\n" );              
               }

               if ( iCalledFromTimer == 1 ) iNumSta = Nsta;
               else                         iNumSta = Nsta2;
			   
/* Loop through all picks and set up data */   	       
               iNWaves = 0;
               for ( i=0; i<iNumPAuto; i++ )
               {   /* Get this station's index in main arrays */
                  for ( j=0; j<iNumSta; j++ )
                  {
                     if ( iCalledFromTimer == 1 )
                     {
                        Sta = (STATION *) &StaArray[j];
                        MtS = (MTSTUFF *) &MtStuff[j];
                     }
                     else if ( iCalledFromTimer == 0 )
                     {
                        Sta = (STATION *) &StaArrayD[j];
                        MtS = (MTSTUFF *) &MtStuffD[j];
                     }
                     if ( !strcmp( Sta->szStation, PBuf[i].szStation ) &&
                          !strcmp( Sta->szChannel, PBuf[i].szChannel ) &&
                          !strcmp( Sta->szNetID, PBuf[i].szNetID ) ) break;
                  }
                  if ( j == iNumSta )
                  {
                     logit( "", "Station %s not found in array\n",
                            PBuf[i].szStation );
                     goto NextStation;
                  }
                  if ( Gparm.Debug > 0 )
                     logit( "", "%s %s Pick; Index=%ld\n",
                      PBuf[i].szStation, PBuf[i].szChannel, j );

/* Verify that this type of seismometer is OK for inversions */
                  if ( Sta->iStationType != 1 && /* STS-1 */
                       Sta->iStationType != 2 && /* STS-2 */
                       Sta->iStationType != 3 && /* CMG-3NSN */
                       Sta->iStationType != 4 && /* CMG-3T */
                       Sta->iStationType != 5 && /* KS360i */
                       Sta->iStationType != 6 && /* KS54000 */
                       Sta->iStationType != 9 && /* CMG-3TNSN */
                       Sta->iStationType != 12 && /* CMG3ESP_60 */
                       Sta->iStationType != 14 && /* CMG3ESP_120 */
                       Sta->iStationType != 16 && /* CMG3T_360 */
                       Sta->iStationType != 13 ) /* Trillium */
                  {
                     if ( Gparm.Debug > 0 )
                        logit( "", "Station %s wrong instrument type (%ld)\n",
                             PBuf[i].szStation, Sta->iStationType );
                     goto NextStation;
                  }			  
				  
/* Continue if there is response data for this station and the distance
   is appropriate */
                  if ( MtS->dAmp0 > 0. && PBuf[i].dDelta >= 20. &&
                       PBuf[i].dDelta < 94. )
                  {
                     if ( Gparm.Debug > 0 )
                        logit( "", "%s %s has resp. and dist.\n",
                         PBuf[i].szStation, PBuf[i].szChannel );
                     MtS->dDt = 1. / Sta->dSampRate;
                     MtS->dMtStartTime = PBuf[i].dPTime -
                      (double) GF_PREEVENT;  /* Must assume 1sps */
                     MtS->dMtEndTime =
                      MtS->dMtStartTime + (double) iWindowTotal;
                     InitMtStructure( MtS );
				  
/* Set epicentral distance in degrees and epicenter to station az */
                     MtS->azdelt.dDelta = PBuf[i].dDelta;
                     MtS->azdelt.dAzimuth = PBuf[i].dAz;
   
/* Compute time of oldest data in buffer */
                     if ( iCalledFromTimer == 1 ) 
                     {
                        if ( Sta->dSampRate > 0. )
                           dOldestTime = Sta->dEndTime - ((double)
                            Sta->lRawCircSize/Sta->dSampRate)+1./Sta->dSampRate;
                        else
                           dOldestTime = 0.;
				   
/* Skip this one if the data is already passed */				   
                        if ( MtS->dMtStartTime-DATA_PREEVENT < dOldestTime ) 
                        {		 	  		
                           if ( Gparm.Debug ) logit( "", "%s P-wave start prior"
                              " to bufferstart=%lf, dEndTime=%lf\n",
                              Sta->szStation, dOldestTime, Sta->dEndTime );
                           continue;
                        }
					 
/* Or, skip this one if there is no data */				   
                        if ( dOldestTime <= 0. ) 
                        {		 	  		
                           if ( Gparm.Debug ) logit( "", "%s no data\n",
                              Sta->szStation );
                           continue;
                        }
                     }
	     
/* Is there data in this buffer covering the TimeWindow? */
                     if ( MtS->dMtStartTime-DATA_PREEVENT > dOldestTime &&
                          MtS->dMtEndTime+DATA_POSTEVENT  < Sta->dEndTime )
                     {		 	  		
/* First, fill up the buffer */
                        if ( iCalledFromTimer == 1 )
                           FillMtDataBuff( Sta, MtS, &lNum, dData );
                        else if ( iCalledFromTimer == 0 )
                           {
                              if ( FillMtDataBuffSolo( Sta, MtS, &lNum, dData,
                                 dOldestTime ) < 0 )
                              {
                                 if ( Gparm.Debug ) logit( "", "%s FillMT problem\n",
                                    Sta->szStation );
                                 goto NextStation;
                              }
                           }
                        MtS->iNPts = lNum;
						
/* Make some checks on data: are there gaps?, is S:N OK?, is DC OK? */
/* First, check for gaps, and a DC level near the "stops" */
                        rc = CheckDataForDCandGaps( lNum, dData, Sta );
                        if ( rc < 0 )
                        {
                           if ( Gparm.Debug ) logit( "", "%s DC problem\n",
                              Sta->szStation );
                           goto NextStation;
                        }
                						 
/* Now check the signal-to-noise ratio */
                        rc = CheckDataForSN( lNum, dData, Sta, MtS, &dSN );
                        if ( rc < 0 )
                        {
                           if ( Gparm.Debug ) logit( "", "%s SN problem\n",
                              Sta->szStation );
                           goto NextStation;
                        }
						
/* Set up response structure for deconvolution */
                        if ( FillResponse( pResponse, MtS ) < 0 )
                        {
                           if ( Gparm.Debug ) logit( "", "%s FillResp problem\n",
                              Sta->szStation );
                           goto NextStation;
                        }
						
/* Set up taper frequencies (filter 10-100s) */						
                        if ( dFiltLoSpec < 0. )
                        {
                           taperFreqs[0] = 0.008;
                           taperFreqs[1] = 0.01;
                        }
                        else
                        {
                           taperFreqs[0] = 1. / (dFiltLoSpec+dFiltLoSpec/4.);
                           taperFreqs[1] = 1. / dFiltLoSpec;
                        }
                        if ( dFiltHiSpec < 0. )
                        {
                           taperFreqs[3] = 0.2;
                           taperFreqs[2] = 0.1;
                        }
                        else
                        {
                           taperFreqs[3] = 1. / (dFiltHiSpec-dFiltHiSpec/2.);
                           taperFreqs[2] = 1. / dFiltHiSpec;
                        }
                        if ( Gparm.Debug > 1 ) logit( "", "tF0=%lf, tF1=%lf, tF2=%lf tF3=%lf\n",
                           taperFreqs[0], taperFreqs[1], taperFreqs[2], taperFreqs[3] );
						
/* Passed all tests, remove instrument reponse and filter 10s-100s*/	
                        if ( (DeconvolveWF( dData, lNum, 1./Sta->dSampRate,
                         pResponse, pFinalResponse, taperFreqs, dDataOut,
                         DECON_BUFFER )) == -1 )
                        {
                           if ( Gparm.Debug ) logit( "", "%s Deconvolve problem\n",
                              Sta->szStation );
                           goto NextStation;
                        }
						 
/* Save original data and filtered (just over P) in arrays. */
                        for ( j=0; j<lNum; j++ ) MtS->dData[j] = dData[j];
                        for ( j=0; j<iWindowTotal; j++ )
                           MtS->dDataFilt[j] = dDataOut[j+DATA_PREEVENT];
                						 
/* Get Sum-square of amps */
                        CheckDataForSSAmp( dDataOut, Sta, MtS, iWindowTotal );
						   						
/* Sample rates must be same in data and GF */
                        if ( fabs( GreensFunctions[0].dSR - 
                             Sta->dSampRate ) > 0.0001 )
                        {
                           logit( "", "GF sample rate different than "
                            "data=%lf, GF=%lf\n", Sta->dSampRate,
                            GreensFunctions[0].dSR );
                           goto NextStation;
                        }
						
                        if ( Gparm.Debug > 0 )
                           logit( "", "%s %s looks good\n", PBuf[i].szStation,
                            PBuf[i].szChannel );						
                        MtS->iUseOrig = 1;
                        free( pResponse->Poles );
                        free( pResponse->Zeros );
                        iNWaves++;
                     }
                  }
NextStation:;
               }
			   
/* Then call mtinv (if > enough have enough data) */ 
               dMag = 0.;
               dMinErr = 1.E6;
               if ( Gparm.Debug > 0 )
                  logit( "t", "iNWaves=%ld, MIN_WAVEFORMS=%ld\n", iNWaves,
                         MIN_WAVEFORMS );
               if ( iCalledFromTimer == 1 )
               {
                  Sta = StaArray;
                  MtS = MtStuff;
                  iDisk = 0;
               }
               else if ( iCalledFromTimer == 0 )
               {
                  Sta = StaArrayD;
                  MtS = MtStuffD;
                  iDisk = 1;
               }
               if ( iNWaves >= MIN_WAVEFORMS )
               {
/* Loop through different Green's Function depths */			   
                  for ( i=iDepthStart; i<iDepthEnd; i++ )
                  {
                     if ( Gparm.Debug > 0 )
                        logit( "", "Invert at %ldkm Depth\n", iGFDepths[i] );
                     for ( ii=0; ii<iNumSta; ii++ )
                        MtS[ii].iUse = MtS[ii].iUseOrig;
                     InitMtR( &MtResults, &Gparm );
                     for ( ii=0; ii<iNumSta; ii++ )
                        if ( MtS[ii].iUse == 1 )
                        {  
/* Choose proper Green's Function and filter same as data */
                           FindGFIndices( GreensFunctions,
                            MtS[ii].azdelt.dDelta*111.1, iGFDepths[i],
                            iGF2Display );
                           if ( Gparm.Debug > 1 )
                              logit( "", "%ld %s %lf degrees GF=%ld %ld %ld\n", 
                               ii, MtS[ii].szStation, MtS[ii].azdelt.dDelta,
	                           iGF2Display[0], iGF2Display[1], iGF2Display[2] );
                           for ( k=0; k<iWindowTotal; k++ )
                           {
                              MtS[ii].dGFFiltFfds[k] =
                               GreensFunctions[iGF2Display[0]].fGF[k];
                              MtS[ii].dGFFiltVss[k]  =
                               GreensFunctions[iGF2Display[1]].fGF[k];
                              MtS[ii].dGFFiltVds[k]  =
                               GreensFunctions[iGF2Display[2]].fGF[k];
                           }
						
/* Filter GFs as data */	
                           if ( Gparm.Debug > 1 ) logit( "", "Filter GFs\n");
                           DeconvolveWF( MtS[ii].dGFFiltFfds, iWindowTotal,
                            1./GreensFunctions[iGF2Display[0]].dSR,
                            pFinalResponse, pFinalResponse, taperFreqs,
                            dDataOut, DECON_BUFFER );
                           for ( j=0; j<iWindowTotal; j++ )
                              MtS[ii].dGFFiltFfds[j] = dDataOut[j];
                           DeconvolveWF( MtS[ii].dGFFiltVss, iWindowTotal,
                            1./GreensFunctions[iGF2Display[1]].dSR,
                            pFinalResponse, pFinalResponse, taperFreqs,
                            dDataOut, DECON_BUFFER );
                           for ( j=0; j<iWindowTotal; j++ )
                              MtS[ii].dGFFiltVss[j] = dDataOut[j];
                           DeconvolveWF( MtS[ii].dGFFiltVds, iWindowTotal,
                            1./GreensFunctions[iGF2Display[2]].dSR,
                            pFinalResponse, pFinalResponse, taperFreqs,
                            dDataOut, DECON_BUFFER );
                           for ( j=0; j<iWindowTotal; j++ )
                              MtS[ii].dGFFiltVds[j] = dDataOut[j];
                           if ( Gparm.Debug > 1 ) logit( "", "GFs Filtered\n");
                        }
/* Perform Inversion for this depth */
                     if ( Gparm.Debug > 0 ) logit( "", "call mtinv\n");
                     if ( MtInv( &MtResults, MtS, iNumSta, iWindowTotal,
                          Gparm.Debug ) < 0 )
                        logit( "t", "MtInv problem at %ldkm\n", iGFDepths[i] );
                     else
                     {
                        if ( Gparm.Debug > 1 )
                        {
                           logit( "t", "MtInv complete at %ldkm\n", iGFDepths[i] );
                           EmailOutput( Gparm.EmailFile, &HStruct, MtResults,
                            iNWaves, iNumSta, MtS, iGFDepths[i], Gparm.Debug );
                        }
/* Align waveform with Green's function and recompute inversion */
                        if ( Gparm.Align == 1 )
                        { /* Set MaxTLag */
                           if ( dTLagSpec < 0. ) dMaxTLag = Gparm.MaxTLag;
                           else                  dMaxTLag = dTLagSpec;
                           if ( Gparm.TimeWindow > iWindowTotal )
                           {
                              Gparm.TimeWindow = iWindowTotal/2;
                              logit( "", "TimeWindow too long, reset\n" );
                           }
                           MtResults.iPass = 2;
                           if ( AlignWf2( Gparm.TimeWindow, dMaxTLag, iNumSta,
                                MtS, Gparm.Debug, iWindowTotal ) < 0 )
                              logit( "", "Problem in align\n" );
                           else
                           {
                              if ( MtInv( &MtResults, MtS, iNumSta,
                                   iWindowTotal, Gparm.Debug ) < 0 )
                                 logit( "t", "MtInv problem at %ldkm-2\n",
                                        iGFDepths[i] );
                              else
                              {
                                 if ( Gparm.Debug > 1 )
                                 {
                                    logit( "t", "MtInv complete at %ldkm-2\n",
                                           iGFDepths[i] );
                                    EmailOutput( Gparm.EmailFile, &HStruct,
                                     MtResults, iNWaves, iNumSta, MtS,
                                     iGFDepths[i], Gparm.Debug );
                                 }
                              }
                           }
                        }		   
/* Now check to see if # of useable stations has changed (if it has, redo) */
                        iCnt = 0;
                        for ( ii=0; ii<iNumSta; ii++ )
                           if ( MtS[ii].iUse == 1 ) iCnt++;
                        if ( iCnt != iNWaves && iCnt >= MIN_WAVEFORMS ) 
                        {
                           MtResults.iPass = 3;
                           if ( MtInv( &MtResults, MtS, iNumSta,
                                iWindowTotal, Gparm.Debug ) < 0 )
                              logit( "t", "MtInv problem at %ldkm-3\n",
                                     iGFDepths[i] );
                           else
                           {
                              iCnt = 0;
                              for ( ii=0; ii<iNumSta; ii++ )
                                 if ( MtS[ii].iUse == 1 ) iCnt++;
                              if ( Gparm.Debug > 1 )
                              {
                                 logit( "t", "MtInv complete at %ldkm-3\n",
                                        iGFDepths[i] );
                                 EmailOutput( Gparm.EmailFile, &HStruct,
                                  MtResults, iCnt, iNumSta, MtS, iGFDepths[i],
                                  Gparm.Debug );
                              }
                           }
                        }
                     }
                     if ( MtResults.dMisFitAvg < dMinErr &&
                          iCnt >= MIN_WAVEFORMS )
                     {
                        dMinErr = MtResults.dMisFitAvg;
                        iDepthIndex = i;
                        dMag = MtResults.dMag;
                     }
                  }
               }
               else
                  if ( Gparm.Debug > 0 )
                     logit( "t", "Not enough waveforms (%ld)\n", iNWaves );
				  
/* Now, take minimum error depth, and recompute - changing filters and
   MaxLag if necessary. Re-filter data if necessary. */				  
               if ( iNWaves >= MIN_WAVEFORMS && dMag > 0.)
               {
                  for ( ii=0; ii<iNumSta; ii++ )
                     MtS[ii].iUse = MtS[ii].iUseOrig;
                  InitMtR( &MtResults, &Gparm );
				  
/* See if magnitude requires re-filtering */
                  if ( dMag > 6.4 && dMag <= 7.0 && dFiltLoSpec < 0. )
                  {
                     taperFreqs[0] = 0.005;
                     taperFreqs[1] = 0.00666;
                  }
                  else if ( dMag > 7.0 && dFiltLoSpec < 0. )
                  {
                     taperFreqs[0] = 0.004;
                     taperFreqs[1] = 0.005;
                  }
				  
                  if ( dMag > 6.4 && dMag <= 7.0 && dFiltHiSpec < 0. )
                  {
                     taperFreqs[3] = 0.1;
                     taperFreqs[2] = 0.05;
                  }
                  else if ( dMag > 7.0 && dFiltHiSpec < 0. )
                  {
                     taperFreqs[3] = 0.05;
                     taperFreqs[2] = 0.025;
                  }
				  
/* Reset MaxTLag based on magnitude */
                  if ( dMag > 6.4 && dMag <= 7.0 && dTLagSpec < 0. )
                     dMaxTLag = 12.;
                  else if ( dMag > 7.0 && dTLagSpec < 0. )
                     dMaxTLag = 18.;

                  for ( ii=0; ii<iNumSta; ii++ )
                     if ( MtS[ii].iUse == 1 )
                     {  						
/* Re-filter and deconvolve data */	
                        FillResponse( pResponse, &MtS[ii] );
                        DeconvolveWF( MtS[ii].dData, MtS[ii].iNPts,
                         1./Sta[ii].dSampRate, pResponse,
                         pFinalResponse, taperFreqs, dDataOut, DECON_BUFFER );
                        free( pResponse->Poles );
                        free( pResponse->Zeros );
                        for ( j=0; j<iWindowTotal; j++ )
                           MtS[ii].dDataFilt[j] = dDataOut[j+DATA_PREEVENT];
                						 
/* Get Sum-square of amps */
                        CheckDataForSSAmp( dDataOut, &Sta[ii], &MtS[ii],
                                           iWindowTotal );
						   
/* Choose proper Green's Function and filter same as data */
                        FindGFIndices( GreensFunctions,
                         MtS[ii].azdelt.dDelta*111.1, iGFDepths[iDepthIndex],
                         iGF2Display );
                        for ( k=0; k<iWindowTotal; k++ )
                        {
                           MtS[ii].dGFFiltFfds[k] =
                            GreensFunctions[iGF2Display[0]].fGF[k];
                           MtS[ii].dGFFiltVss[k]  =
                            GreensFunctions[iGF2Display[1]].fGF[k];
                           MtS[ii].dGFFiltVds[k]  =
                            GreensFunctions[iGF2Display[2]].fGF[k];
                        }
			
/* Filter GFs as data */	
                        DeconvolveWF( MtS[ii].dGFFiltFfds,
                         iWindowTotal, 1./GreensFunctions[iGF2Display[0]].dSR,
                         pFinalResponse, pFinalResponse, taperFreqs,
                         dDataOut, DECON_BUFFER );
                        for ( j=0; j<iWindowTotal; j++ )
                           MtS[ii].dGFFiltFfds[j] = dDataOut[j];
                        DeconvolveWF( MtS[ii].dGFFiltVss,
                         iWindowTotal, 1./GreensFunctions[iGF2Display[1]].dSR,
                         pFinalResponse, pFinalResponse, taperFreqs,
                         dDataOut, DECON_BUFFER );
                        for ( j=0; j<iWindowTotal; j++ )
                           MtS[ii].dGFFiltVss[j] = dDataOut[j];
                        DeconvolveWF( MtS[ii].dGFFiltVds,
                         iWindowTotal, 1./GreensFunctions[iGF2Display[2]].dSR,
                         pFinalResponse, pFinalResponse, taperFreqs,
                         dDataOut, DECON_BUFFER );
                        for ( j=0; j<iWindowTotal; j++ )
                           MtS[ii].dGFFiltVds[j] = dDataOut[j];
                     } 							  
/* Perform Inversion for this depth */
                  if ( MtInv( &MtResults, MtS, iNumSta, iWindowTotal,
                       Gparm.Debug ) < 0 )
                     logit( "t", "MtInv problem at %ldkm-a\n",
                      iGFDepths[iDepthIndex] );
                  else
                  {
                     if ( Gparm.Debug > 0 )
                        logit( "t", "MtInv complete at %ldkm-a\n",
                         iGFDepths[iDepthIndex] );
                     EmailOutput( Gparm.EmailFile, &HStruct, MtResults,
                      iNWaves, iNumSta, MtS, iGFDepths[iDepthIndex],
                      Gparm.Debug );
/* Align waveform with Green's function and recompute inversion */
                     if ( Gparm.Align == 1 )
                     {
                        if ( Gparm.TimeWindow > iWindowTotal )
                        {
                           Gparm.TimeWindow = iWindowTotal/2;
                           logit( "", "TimeWindow too long, reset\n" );
                        }
                        MtResults.iPass = 2;
                        if ( AlignWf2( Gparm.TimeWindow, dMaxTLag, iNumSta,
                             MtS, Gparm.Debug, iWindowTotal ) < 0 )
                           logit( "", "Problem in align\n" );
                        else
                        {
                           if ( MtInv( &MtResults, MtS, iNumSta,
                                iWindowTotal, Gparm.Debug ) < 0 )
                              logit( "t", "MtInv problem at %ldkm-2a\n",
                                     iGFDepths[iDepthIndex] );
                           else
                           {
                              if ( Gparm.Debug > 0 )
                                 logit( "t", "MtInv complete at %ldkm-2a\n",
                                        iGFDepths[iDepthIndex] );
                              EmailOutput( Gparm.EmailFile, &HStruct,
                               MtResults, iNWaves, iNumSta, MtS,
                               iGFDepths[iDepthIndex], Gparm.Debug );
                           }
                        }
                     }		   
/* Now check to see if # of useable stations has changed (if it has, redo) */
                     iCnt = 0;
                     for ( ii=0; ii<iNumSta; ii++ )
                        if ( MtS[ii].iUse == 1 ) iCnt++;
                     if ( iCnt != iNWaves && iCnt >= MIN_WAVEFORMS ) 
                     {
                        MtResults.iPass = 3;
                        if ( MtInv( &MtResults, MtS, iNumSta,
                             iWindowTotal, Gparm.Debug ) < 0 )
                           logit( "t", "MtInv problem at %ldkm-3a\n",
                                  iGFDepths[iDepthIndex] );
                        else
                        {
                           if ( Gparm.Debug > 0 )
                              logit( "t", "MtInv complete at %ldkm-3a\n",
                                     iGFDepths[iDepthIndex] );
                           iCnt = 0;
                           for ( ii=0; ii<iNumSta; ii++ )
                              if ( MtS[ii].iUse == 1 ) iCnt++;
                           if ( iCnt >= MIN_WAVEFORMS ) 
                           {
                              EmailOutput( Gparm.EmailFile, &HStruct,
                               MtResults, iCnt, iNumSta, MtS,
                               iGFDepths[iDepthIndex], Gparm.Debug );
                              if ( iDisk == 0 )
                                 PlaySound( "\\earthworm\\atwc\\src\\mtinver\\tada.wav",
                                            NULL, SND_SYNC );	 
                           }
                           else
                              logit( "t", "Not enough waveforms (%ld) after "
                                     "pass 3\n", iNWaves );
                        }
                     }
                  }
               }
               else
                  logit( "t", "Still not enough waveforms (%ld)\n", iNWaves );
	
/* Update MT results file with MT for this station */
               if ( MtResults.dMag > 0. )
                  if ( (hFile = fopen( Gparm.MTFile, "a" )) != NULL )
                  {
                     fprintf( hFile, "%Mw = %lf, Moment = %lf\n",
                      MtResults.dMag, MtResults.dScalarMom );
                     fclose( hFile );
                  }						   
               iCnt = 0;
               for ( ii=0; ii<iNumSta; ii++ )
                  if ( MtS[ii].iUse == 1 ) iCnt++;
               if ( iCnt >= MIN_WAVEFORMS ) iWaveFormDisplay = 2;
						
/* Update the title bar */				  
               strcpy( szTitle, szProcessName );
               SetWindowText( hwndWndProc, szTitle );  /* Re-set the title */         
               logit( "t", "processing stopped in COMPUTE_MOMENT-2\n" );
               ReleaseSpecificMutex( &mutsem1 ); /* Let someone else have sem */
               iCalledFromTimer = 0;
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_SHOW_GFS:      /* Draw Green's Functions on screen */
/* Enter depth and distance of function you with to display */			   
               if ( DialogBox( hInstMain, "GFDisplay", hwndWndProc, 
                   (DLGPROC) GFDisplayDlgProc ) == IDCANCEL ) break;
               iWaveFormDisplay = 1;
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_SHOW_SYNTHS:   /* Draw latest synthetics and data */
               iWaveFormDisplay = 2;
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_REFRESH:       /* redraw the screen */
               iWaveFormDisplay = 0;
               InvalidateRect( hwnd, NULL, TRUE );
               break;

            case IDM_EXIT_MAIN:     /* Send DESTROY message to itself */
               logit( "", "EXIT_MAIN\n");
               PostMessage( hwnd, WM_DESTROY, 0, 0 );
               break;          
         }
         break ;

/* Fill in screen display */
      case WM_PAINT:
         hdc = BeginPaint( hwnd, &ps );         /* Get device context */
         GetClientRect( hwnd, &rct );
         FillRect( hdc, &rct, (HBRUSH) GetStockObject( WHITE_BRUSH ) );
         if ( iWaveFormDisplay == 1 )
            DisplayGFs( hdc, GreensFunctions, lTitleFHt, lTitleFWd, cxScreen,
                        cyScreen, iVScrollOffset );
         else if ( iWaveFormDisplay == 2 && iDisk == 1 )
         {
            DisplaySoln( hdc, MtStuffD, Gparm.EmailFile, lTitleFHt2, lTitleFWd2,
                         cxScreen, cyScreen, iVScrollOffset );
            DisplaySynths( hdc, MtStuffD, lTitleFHt, lTitleFWd, cxScreen,
                           cyScreen, iNumSta, iVScrollOffset, iWindowTotal );
         }
         else if ( iWaveFormDisplay == 2 && iDisk == 0 )
         {
            DisplaySoln( hdc, MtStuff, Gparm.EmailFile, lTitleFHt2, lTitleFWd2,
                         cxScreen, cyScreen, iVScrollOffset );
            DisplaySynths( hdc, MtStuff, lTitleFHt, lTitleFWd, cxScreen,
                           cyScreen, iNumSta, iVScrollOffset, iWindowTotal );
         }
         SetScrollPos( hwnd, SB_VERT, iVScrollOffset, TRUE );
         EndPaint( hwnd, &ps );
         break;
	
/* Vertical scroll message */	
      case WM_VSCROLL:
         if ( LOWORD (wParam) == SB_LINEUP )
            { if ( iVScrollOffset >= iScrollVertBuff )
               iVScrollOffset -= iScrollVertBuff; }
         else if ( LOWORD (wParam) == SB_PAGEUP )
            { if ( iVScrollOffset >= iScrollVertPage )
               iVScrollOffset -= iScrollVertPage; }
         else if ( LOWORD (wParam) == SB_LINEDOWN )
            { if ( iVScrollOffset <= 3*cyScreen - iScrollVertBuff )
               iVScrollOffset += iScrollVertBuff; }
         else if ( LOWORD (wParam) == SB_PAGEDOWN )
            { if ( iVScrollOffset <= 3*cyScreen - iScrollVertPage )
               iVScrollOffset += iScrollVertPage; }
         InvalidateRect( hwnd, NULL, TRUE );
         break;

/* Close up shop and return */
      case WM_DESTROY:
         logit( "", "WM_DESTROY posted\n" );
         KillTimer( hwndWndProc, ID_TIMER );
         PostQuitMessage( 0 );
         break;

      default:
         return ( DefWindowProc( hwnd, msg, wParam, lParam ) );
   }
return 0;
}

      /*********************************************************
       *                     WThread()                         *
       *                                                       *
       *  This thread gets earthworm waveform messages.        *
       *                                                       *
       *********************************************************/
	   
thr_ret WThread( void *dummy )
{
   int           i;
   long          InBufl;          /* Memory size */
   int           iReturn;         /* Return from LoadResponse */
   char          line[40];        /* Heartbeat message */
   int           lineLen;         /* Length of heartbeat message */
   MSG_LOGO      logo;            /* Logo of retrieved msg */
   long          MsgLen;          /* Size of retrieved message */
   long          RawBufl;         /* Raw data buffer size */

/* Loop to read waveform messages
   ******************************/
   while ( tport_getflag( &Gparm.InRegion ) != TERMINATE )
   {
      long    lGapSize;         /* Number of missing samples (integer) */
      static  MTSTUFF *MtS;     /* Pointer to the station being processed (Mm)*/
      static  time_t  now;      /* Current time */
      int     rc;               /* Return code from tport_getmsg() */
      static  STATION *Sta;     /* Pointer to the station being processed */
      char    szType[3];        /* Incoming data format type */
	  
/* Send a heartbeat to the transport ring
   **************************************/
      time( &now );
      if ( (now - then) >= Gparm.HeartbeatInt )
      {
         then = now;
         sprintf( line, "%d %d\n", now, myPid );
         lineLen = strlen( line );
         if ( tport_putmsg( &Gparm.InRegion, &hrtlogo, lineLen, line ) !=
              PUT_OK )
         {
            logit( "et", "mtinver: Error sending heartbeat." );
            break;
         }
      }
      
/* Get a waveform from transport region
   ************************************/
      rc = tport_getmsg( &Gparm.InRegion, &getlogoW, 1, &logo, &MsgLen,
                         WaveBuf, MAX_TRACEBUF_SIZ);

      if ( rc == GET_NONE )
      {
         sleep_ew( 100 );
         continue;
      }

      if ( rc == GET_NOTRACK )
         logit( "et", "mtinver: Tracking error.\n");

      if ( rc == GET_MISS_LAPPED )
         logit( "et", "mtinver: Got lapped on the ring.\n");

      if ( rc == GET_MISS_SEQGAP )
         logit( "et", "mtinver: Gap in sequence numbers.\n");

      if ( rc == GET_MISS )
         logit( "et", "mtinver: Missed messages.\n");

      if ( rc == GET_TOOBIG )
      {
         logit( "et", "mtinver: Retrieved message too big (%d) for msg.\n",
                MsgLen );
         continue;
      }

/* If necessary, swap bytes in the message
   ***************************************/
      if ( WaveMsgMakeLocal( WaveHead ) < 0 )
      {
         logit( "et", "mtinver: Unknown waveform type.\n" );
         continue;
      }
	  
/* If sample rate is 0, get out of here before it kills program
   ************************************************************/
      if ( WaveHead->samprate == 0. )
      {
         logit( "", "Sample rate=0., %s %s\n", WaveHead->sta, WaveHead->chan );
         continue;
      }

/* If we are in the ATPlayer version of mtinver, see if we should re-init
   **********************************************************************/
      if ( strlen( Gparm.ATPLineupFileLP ) > 2 )     /* Then we are */
         if ( WaveHead->starttime-(int) dLastEndTime > 3600) /* Big gap */
         {
            RequestSpecificMutex( &mutsem1 );   /* Sem protect buff writes */
            Nsta = ReadLineupFile( Gparm.ATPLineupFileLP, StaArray );
            if ( Nsta < 2 )
            {
               logit( "", "Bad Lineup File read %s\n", Gparm.ATPLineupFileLP );
               continue;
            }	    
            for ( i=0; i<Nsta; i++ )
            {
               free( StaArray[i].plRawCircBuff );
               iReturn = LoadResponseData( &StaArray[i], &MtStuff[i],
                         Gparm.ResponseFile );
               if ( iReturn == -1 )
               {
                  logit( "e", "file: %s\n", Gparm.ResponseFile );
                  logit( "e", "scn = %s %s %s\n", StaArray[i].szStation,
                   StaArray[i].szChannel, StaArray[i].szNetID );
               }
               else if ( iReturn == 0 )
                  logit( "", "scn = %s %s %s - No RESPONSE info\n",
                   StaArray[i].szStation, StaArray[i].szChannel, StaArray[i].szNetID );
               else if ( iReturn == 1 )
                  logit( "", "scn = %s %s %s - RESPONSE info read\n",
                   StaArray[i].szStation, StaArray[i].szChannel, StaArray[i].szNetID );
               InitVar( &StaArray[i] );	  
               StaArray[i].iFirst = 1;
            }
            ReleaseSpecificMutex( &mutsem1 ); /* Let someone else have sem */
         } 	 
                                     
/* Look up SCN number in the station list
   **************************************/
      Sta = NULL;								  
      for ( i=0; i<Nsta; i++ )
         if ( !strcmp( WaveHead->sta,  StaArray[i].szStation ) &&
              !strcmp( WaveHead->chan, StaArray[i].szChannel ) &&
              !strcmp( WaveHead->net,  StaArray[i].szNetID ) )
         {
            Sta = (STATION *) &StaArray[i];
            MtS = (MTSTUFF *) &MtStuff[i];
            break;
         }

      if ( Sta == NULL )      /* SCN not found */
         continue;
		 
/* Check if the time stamp is reasonable.  If it is ahead of the present
   1/1/70 time, it is not reasonable. (+1. to account for int).
   *********************************************************************/
      if ( WaveHead->endtime > (double) now+1. )
      {
         logit( "t", "%s %s endtime (%lf) ahead of present (%ld)\n",
                Sta->szStation, Sta->szChannel, WaveHead->endtime, now );
         continue;
      }

/* Do this the first time we get a message with this SCN
   *****************************************************/
      if ( Sta->iFirst == 1 )
      {
         logit( "", "Init %s %s\n", Sta->szStation, Sta->szChannel );	  
         Sta->iFirst = 0;
         Sta->dEndTime = WaveHead->starttime - 1./WaveHead->samprate;
         Sta->dSampRate = WaveHead->samprate;
         ResetFilter( Sta );
		 		 
/* Allocate memory for raw circular buffer (let the buffer be big enough to
   hold MinutesInBuff data samples)
   ************************************************************************/
         Sta->lRawCircSize =
          (long) (WaveHead->samprate*(double)Gparm.MinutesInBuff*60.+0.1);
         RawBufl = sizeof (long) * Sta->lRawCircSize;
         Sta->plRawCircBuff = (long *) malloc( (size_t) RawBufl );
         if ( Sta->plRawCircBuff == NULL )
         {
            logit( "et", "mtinver: Can't allocate raw circ buffer for %s\n",
			       Sta->szStation );
            PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
            return;
         }
      }      
      
/* If data is not in order, throw it out
   *************************************/
      if ( Sta->dEndTime >= WaveHead->starttime )
      {
         if ( Gparm.Debug ) logit( "e", "%s out of order\n", Sta->szStation );
         continue;
      }

/* If the samples are shorts, make them longs
   ******************************************/
      strcpy( szType, WaveHead->datatype );
      if ( (strcmp( szType, "i2" ) == 0) || (strcmp( szType, "s2") == 0) )
         for ( i=WaveHead->nsamp-1; i>-1; i-- )
            WaveLong[i] = (long) WaveShort[i];
			
/* Compute the number of samples since the end of the previous message.
   If (lGapSize == 1), no data has been lost between messages.
   If (1 < lGapSize <= 2), go ahead anyway.
   If (lGapSize > 2), re-initialize filter variables.
   *******************************************************************/
      lGapSize = (long) (WaveHead->samprate *
                        (WaveHead->starttime-Sta->dEndTime) + 0.5);

/* Announce gaps
   *************/
      if ( lGapSize > 2 )
      {
         int      lineLen;
         time_t   errTime;
         char     errmsg[80];
         MSG_LOGO logo;

         time( &errTime );
         sprintf( errmsg,
               "%d 1 Found %4d sample gap. Restarting station %-5s%-2s%-3s\n",
               errTime, lGapSize, Sta->szStation, Sta->szNetID,
               Sta->szChannel );
         lineLen = strlen( errmsg );
         logo.type   = Ewh.TypeError;
         logo.mod    = Gparm.MyModId;
         logo.instid = Ewh.MyInstId;
         tport_putmsg( &Gparm.InRegion, &logo, lineLen, errmsg );
         if ( Gparm.Debug )
            logit( "t", "mtinver: Restarting %-5s%-2s %-3s. lGapSize = %d\n",
                     Sta->szStation, Sta->szNetID, Sta->szChannel, lGapSize ); 

/* For big gaps reset filter 
   *************************/
         ResetFilter( Sta );
         Sta->dSumLDCRaw = 0.;
         Sta->dAveLDCRaw = 0.;
         Sta->dSumLDC = 0.;
      }

/* For gaps less than the size of the buffer, pad buffers with DC
   **************************************************************/
      if ( lGapSize > 1 && lGapSize <= 2 )
         PadBuffer( lGapSize, Sta->dAveLDCRaw, &Sta->lSampIndexR,
                    Sta->plRawCircBuff, Sta->lRawCircSize );
	  
/* For gaps greater than the size of the buffer, re-init
   *****************************************************/
      if ( lGapSize >= Sta->lRawCircSize )
      {
         Sta->dSumLDCRaw = 0.;
         Sta->dAveLDCRaw = 0.;
         Sta->dSumLDC = 0.;
         Sta->lSampIndexR = 0;
      }

/* Compute DC offset for raw data
   ******************************/
      GetLDC( WaveHead->nsamp, WaveLong, &Sta->dAveLDCRaw, Sta->lFiltSamps );
			
/* Tuck raw data into proper location in buffer
   (SampIndexR = index of last sample)
   ********************************************/			
      PutDataInBuffer( WaveHead, WaveLong, Sta->plRawCircBuff,
                       &Sta->lSampIndexR, Sta->lRawCircSize );
	  
/* Keep track of last data (for use with Player)
   *********************************************/
      if ( WaveHead->endtime > dLastEndTime+5.0 )
         dLastEndTime = WaveHead->endtime;
	  
/* Save time of the end of the current message
   *******************************************/
      Sta->dEndTime = WaveHead->endtime;
   }   
   logit( "", "Post WM_DESTROY at end of WThread\n");
   PostMessage( hwndWndProc, WM_DESTROY, 0, 0 );   
}
