#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include "mtinver.h"

  /******************************************************************
   *                     Eigens()                                   *
   *                                                                *
   *  purpose:                                                      *
   *   compute eigenvalues and eigenvectors of a real symmetric     *
   *   matrix                                                       *
   *                                                                *
   *  usage:                                                        *
   *   call eigen(a,r,n,mv)                                         *
   *                                                                *
   *  description of parameters:                                    *
   *   a - original matrix (symmetric), destroyed in computation    *
   *       resultant eigenvalues are developed in dagonal of        *
   *       matrix a in descending order.                            *
   *   r - resultant matrix of eigenvectors (stored comunmwixe,     *
   *       in same sequence as eigenvalues                          *
   *   n - order of matrix a and r                                  *
   *   mv- input code                                               *
   *    0 compute eigen values and eigenvectors                     *
   *    1 compute eigenvalues only (r need not be dimensioned       *
   *      but must still appeat in calling sequence)                *
   *                                                                *
   *   remarks::                                                    *
   *    original matrix a must be real symmetric (storage mode 1)   *
   *    matrix r cannot be in same place as a                       *
   *                                                                *
   *   subroutine and functions required:                           *
   *    none                                                        *
   *                                                                *
   *   method:                                                      *
   *    diagonalization method originated by jacobi and adapted     *
   *    by von neumann for lage computers as found in 'mathematical *
   *    methods for digital computers', edited by a. ralston and    *
   *    h.s.wilf, john riley and sons, new york, 1962, chapter 7.   *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/21/02.                          *
   *                                                                *
   ******************************************************************/

void Eigens( double a[], double r[], int n, int mv )
{
   double  range, anorm, anrmx, thr, x, y, argq, sinx, sinx2, cosx, cosx2,
           sincs;
   int     i, j, k, ij, ia, lm, ll, mm, im, il, ilr, imr; /* Based on 0 */
   int     ind, iq, l, m, mq, lq, jq, ilq, imq;           /* Based on 1 */

/* Generate identity matrix */
   range = 1.0e-6;
   if ( mv == 0 )
   {
      iq = n*(-1);
      for ( j=0; j<n; j++ )
      {
         iq = iq + n;
         for ( i=0; i<n; i++ )
         {
            ij = iq + i;
            r[ij] = 0.;
            if ( i == j ) r[ij] = 1.;
         }
      }
   }
   
/* Compute initial and final norms */
   anorm = 0.;
   for ( i=0; i<n; i++ )
      for ( j=i; j<n; j++ )
      {
         if ( i != j )
         {
            ia = i + ((j+1)*(j+1) - (j+1)) / 2;  /* j+1 in C since based on 0 */
            anorm += (a[ia]*a[ia]);
         }
      }
	  
   if ( anorm <= 0 ) goto SortEig;
   anorm = 1.414 * sqrt( anorm );
   anrmx = anorm*range/(double) n;
   
/* Initialize indicators and compute threshold, thr */
   ind = 0;
   thr = anorm;
CompThr:
   thr = thr / (double) n;
SetL:
   l = 1;
IncM:
   m = l + 1;

/* Compute sin and cos */
SinComp:
   mq = (m*m - m) / 2;
   lq = (l*l - l) / 2;
   lm = l + (mq-1);
   if ( fabs( a[lm] )-thr < 0. ) goto Test;
   ind = 1;
   ll = l + (lq-1);
   mm = m + (mq-1);
   x = 0.5 * (a[ll]-a[mm]);
   y = -a[lm] / sqrt( a[lm]*a[lm] + x*x );
   if ( x < 0. ) y *= -1.;
   argq = 1.0 - y*y;
   if ( argq < 0. ) argq = 0.0;
   sinx = y / sqrt( 2.0*(1.0+(sqrt( argq ))) );
   sinx2 = sinx * sinx;
   argq = 1.0 - sinx2;
   if ( argq < 0.0 ) argq = 0.0;
   cosx = sqrt( argq );
   cosx2 = cosx * cosx;
   sincs = sinx * cosx;
   
/* Rotate l and m columns */
   ilq = n * (l-1);
   imq = n * (m-1);
   for ( i=0; i<n; i++ )
   {
      iq = ((i+1)*(i+1) - (i+1)) / 2;
      if ( i-(l-1) == 0 ) goto Mvtest;
      if ( i-(m-1) == 0 ) goto Mvtest;
      if ( i-(m-1) < 0 ) im = i + mq;
      else               im = (m-1) + iq;
      if ( i-(l-1) < 0 ) il = i + lq;
      else               il = (l-1) + iq;
      x = a[il]*cosx - a[im]*sinx;
      a[im] = a[il]*sinx + a[im]*cosx;
      a[il] = x;
Mvtest:
      if ( mv == 0 ) 
      {
         ilr = ilq + i;
         imr = imq + i;
         x = r[ilr]*cosx - r[imr]*sinx;
         r[imr] = r[ilr]*sinx + r[imr]*cosx;
         r[ilr] = x;
      }
   }
   
   x = 2.0 * a[lm] * sincs;
   y = a[ll]*cosx2 + a[mm]*sinx2 - x;
   x = a[ll]*sinx2 + a[mm]*cosx2 + x;
   a[lm] = (a[ll]-a[mm])*sincs + a[lm]*(cosx2-sinx2);
   a[ll] = y;
   a[mm] = x;
   
/* Test for completion; m=last column */
Test:
   if ( m-n != 0 ) 
   {
      m = m + 1;
      goto SinComp;
   }
   
/* Test for l = second from last column */
   if ( l-(n-1) != 0 ) 
   {
      l = l+1;
      goto IncM;
   }
   if ( ind-1 == 0 ) 
   {
      ind = 0;
      goto SetL;
   }
   
/* Cmpare threshold with final norm */
   if ( thr-anrmx > 0. ) goto CompThr;
   
/* Sort eigenvalues and eigenvectors */
SortEig:
   iq = n*(-1);
   for ( i=0; i<n; i++ )
   {
      iq = iq + n;
      ll = (i+1) + ((i+1)*(i+1) - (i+1)) / 2 - 1;
      jq = n * (i-1);
      for ( j=i; j<n; j++ )
      {
         jq = jq + n;
         mm = (j+1) + ((j+1)*(j+1) - (j+1)) / 2 - 1;
         if ( a[ll]-a[mm] < 0. ) 
         {
            x = a[ll];
            a[ll] = a[mm];
            a[mm] = x;
            if ( mv == 0 ) 
            {
               for ( k=0; k<n; k++ )
               {
                  ilr = iq + k;
                  imr = jq + k;
                  x = r[ilr];
                  r[ilr] = r[imr];
                  r[imr] = x;
               }
            }
         }
      }
   }
}

  /******************************************************************
   *                       AHat()                                   *
   *                                                                *
   *  routine to compute the operator matrix defined                *
   *   below eq. 3.6 on p.355 of hext(1963)                         *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/21/02.                          *
   *                                                                *
   ******************************************************************/
   
void AHat( double v1[][3], double v2[][3], double ah[][1], int i1, int i2 )
{
   ah[0][0] = v1[0][i1]*v2[0][i2];
   ah[1][0] = v1[1][i1]*v2[1][i2];
   ah[2][0] = v1[2][i1]*v2[2][i2];
   ah[3][0] = v1[1][i1]*v2[2][i2] + v1[2][i1]*v2[1][i2];
   ah[4][0] = v1[0][i1]*v2[2][i2] + v1[2][i1]*v2[0][i2];
   ah[5][0] = v1[0][i1]*v2[1][i2] + v1[1][i1]*v2[0][i2];
}

  /******************************************************************
   *                       Hext()                                   *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/21/02.                          *
   *                                                                *
   ******************************************************************/

void Hext( double smt[], double ev[][3], double mcov[][6], double delm[],
           double wroot[][3], double wegnv[][2][3], double windx[][3] )
{
   double  ah[MAXMAT][1], temp[1][MAXMAT], se[1][1], wvec[4];
   double  ah1[MAXMAT][1], ah2[MAXMAT][1], ah2d[MAXMAT][2], temp2[2][MAXMAT];
   double  scale1, scale2;
   double  w[2][2], wev[4];                             /* ev[2][2]; */
   int     indx[3];
   int     i, i1, i2, in0, in1, in2, j;                 /* Based on 0 */
   
   indx[0] = 0;
   indx[1] = 2;
   indx[2] = 5;
   
/* Computation of error on eigenvalue */
   for ( i=0; i<3; i++ )
   {

/* Form AHat matrix, p.356 of hext */
      AHat( ev, ev, ah, i, i );

/* Form AHat(transpose)* at*a inv */
      Mult1( ah, -1, mcov, 1, temp, 1, 6, 6, 0 );

/* Form ahat(t) * a(t)*a inv * ahat */
      Mult2( temp, 1, ah, 1, se, 1, 6, 1, 0 );
      delm[i] = sqrt( se[0][0] );
   }

/* Computation of error ellipse on eigenvector */
   for ( i=0; i<3; i++ )
   {
      i1 = i + 1;
      i2 = i + 2;
      if ( i1 >= 3) i1 = i1 - 3;
      if ( i2 >= 3) i2 = i2 - 3;

/* Calculate wihat using eq. 5.6 in hext(1963). First compute AHats */
      AHat( ev, ev, ah1, i1, i );
      AHat( ev, ev, ah2, i2, i );

/* scale the AHats */
      in0 = indx[i];
      in1 = indx[i1];
      in2 = indx[i2];
      scale1 = 1.0 / (smt[in0]-smt[in1]);
      scale2 = 1.0 / (smt[in0]-smt[in2]);
      for ( j=0; j<6; j++ )
      {
         ah1[j][0] = scale1 * ah1[j][0];
         ah2[j][0] = scale2 * ah2[j][0];
      }
	  
/* Use this loop to replace FORTRAN EQUIVALENCE statement */
      for ( j=0; j<6; j++ )
      {
         ah2d[j][0] = ah1[j][0];
         ah2d[j][1] = ah2[j][0];
      }

/* Form ah2d(t) * a(t)*a inv */
      Mult3( ah2d, -1, mcov, 1, temp2, 2, 6, 6, 0 ); 

/* Form ah2d(t) * a(t)*a inv * ah2d */
      Mult4( temp2, 1, ah2d, 1, w, 2, 6, 2, 0 );

/* Form matrix to be diagonalized */
      wvec[0] = w[0][0]; 
      wvec[1] = w[0][1];
      wvec[2] = w[1][1];
      Eigens( wvec, wev, 2, 0 );
      wroot[0][i] = wvec[0];
      wroot[1][i] = wvec[2];
      wegnv[0][0][i] = wev[0];  /* wev[0][0];      Indices altered in C */
      wegnv[1][0][i] = wev[1];  /* wev[0][1];      was [1][0] */
      wegnv[0][1][i] = wev[2];  /* wev[1][0];      was [0][1] */
      wegnv[1][1][i] = wev[3];  /* wev[1][1];                 */
      windx[0][i] = in1;
      windx[1][i] = in2;
   }
}
