#
#                     Mtinver's Configuration File
#
MyModId         MOD_MTINVER    # This instance of mtinver
InRing        WAVE_RING_LP     # Input waveform ring 
HypoRing         HYPO_RING     # Hypocenter ring (input)
StaFile        "pick_wcatwc_lp.sta" # File containing stations to be processed
StaDataFile    "station.dat"   # File with information about stations
HeartbeatInt            20     # Heartbeat interval, in seconds
#    Add file created in ATPlayer if used in conjunction with player (optional)
#    Comment this line if using mtinver in real-time mode
#ATPLineupFileLP "\earthworm\run\params\ATP-LP.sta" # Station config file
#
MinutesInBuff          180     # Time (minutes) to allocate for each real-time trace 
#
LPFileSize              20     # Number of minutes per data file (see disk_wcatwc)
FileSuffix             ".L"    # Data file suffix start (see disk_wcatwc)
GFSampRate             1.0     # Sample rate of saved Green's Functions
ReadTime                40     # Number minutes to read from disk files
#
Align                    1     # 1->Align synthetics and waveforms
TimeWindow              30.    # Window length of seismograms (seconds)
MaxTLag                  6.    # Max time shift in wave alignment
#
RootDirectory   "f:\datafile"  # Root directory for disk file reads
ArchDirectory           "d:"   # Root directory for archive data reads
DummyFile    "\DoNotCopyToEB\dummyX.dat"  # WC/ATWC EarlyBird dummy file
ResponseFile "\earthworm\run\params\calibs"  # Broadband response file
MTFile       "\earthworm\atwc\src\mtinver\mtinv.dat" # MT inv. results file
EmailFile    "\earthworm\atwc\src\mtinver\email.dat" # Email inversion file
DepthFile    "\earthworm\run\params\data_in.dat" # Areas with max quake depth
GFDir        "\earthworm\run\params\gfs_lp"  # Directory containing Green's
                                             # functions
EBPTimeFile  "\seismic\messages\ptimemsg.dat"# P-time file from LOCATE
LocFilePath "\DoNotCopyToEB\LocFiles\"       # Path for disk logs
#
Debug                    1     # 0=few msgs, 1=some, 2=lots
