
# C Source Make file patterned after Microsoft example

!include <ntwin32.mak>
APP = mtsolo
O = $(APP).obj eigens.obj matrix.obj mtinv.obj stalist.obj output.obj waveform.obj prepp.obj readgf.obj display.obj
B = $(EW_HOME)\$(EW_VERSION)\bin
L = $(EW_HOME)\$(EW_VERSION)\lib
N = $(EW_HOME)\atwc\src\libsrc

compflags= /c /W3 /MT /nologo

linkflags= /release /nologo

libs= libcmt.lib

$B\$(APP).exe: $O $(APP).res
   link $(linkflags) $(libs) $O \
    $L\logit.obj $N\filters.obj $N\diskrw.obj $N\fereg.obj $N\mjstime.obj \
    $N\geotools.obj $N\locquake.obj $L\swap.obj $L\time_ew.obj $L\sleep_ew.obj \
    $N\complex.obj $N\dummy.obj $N\get_hypo.obj $N\get_pick.obj \
    $L\sacputaway.obj $L\chron3.obj $L\dirops_ew.obj $L\geo_to_km.obj \
    $L\fft99.obj $L\transfer.obj $L\fft_prep.obj \
    -out:$B\$(APP).exe $(APP).res $(guilibs) 

$(APP).res: $(APP).rc
    rc -r -fo $(APP).tmp $(APP).rc
    cvtres -$(CPU) $(APP).tmp -o $(APP).res
    del $(APP).tmp

.c.obj:
   cl /nologo $(compflags) $(tflags) $<

# Clean-up directives
clean:
	del a.out core *.o *.obj *% *~

clean_bin:
	del $B\mtsolo*	
