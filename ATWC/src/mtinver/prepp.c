 /***********************************************************************
  *                               PrepP()                               *
  *                                                                     *
  *  Routines which initialize structures and perform other tasks on the*
  * waveform data prior to inversion.                                   *
  *                                                                     *
  ***********************************************************************/
  
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>
#include "mtinver.h"

 /***********************************************************************
  *                      CheckDataForDCandGaps()                        *
  *                                                                     *
  *  Run through some checks on the data. First look for gaps then see  *
  * if data is near the "stops" (i.e. is DC level huge?).               *
  *                                                                     *
  *  Function Arguments:                                                *
  *   lNum    Number of data points in dData.                           *
  *   dData   Data array.                                               *
  *   Sta     Station data structure.                                   *
  *                                                                     *
  *  Returns:                                                           *
  *   1       OK                                                        *
  *   -1      Gap in data                                               *
  *   -2      Data near "stops"                                         *
  *                                                                     *
  ***********************************************************************/
  
int CheckDataForDCandGaps( long lNum, double *dData, STATION *Sta )
{
   double  dAvg;              /* Average signal level */
   double  dTotal;            /* Summation variable */
   int     iGapSize;          /* Max allowable gap in data (in samples) */
   int     j, iCnt;

   iCnt = 0;
   dTotal = 0.;
   iGapSize = (int) ((double) GAP_SIZE*Sta->dSampRate + 0.001);
   
   for ( j=0; j<lNum; j++ )   /* Is there a gap? */
   {
      dTotal += dData[j];
      if ( dData[j] == 0. ) iCnt++;
      if ( iCnt >= iGapSize )
      {
         logit( "", "%s gap in data\n", Sta->szStation );
         Sta->iPickStatus = 2;
         return -1;
      }
      if ( iCnt > 0 && dData[j] != 0. ) iCnt = 0;
   }
   dAvg = dTotal / ((double) lNum);
   if ( fabs( dAvg ) > Sta->dClipLevel )
   {
      logit( "", "%s data near stops; %lf %lf\n",
       Sta->szStation, dTotal, Sta->dClipLevel );
      Sta->iPickStatus = 2;
      return -2;
   }
   return 1;
}

 /***********************************************************************
  *                          CheckDataForSN()                           *
  *                                                                     *
  *  Run through some more checks on the data. Look for a reasonable    *
  * signal-to-noise ratio and removes DC offset.                        *
  *                                                                     *
  *  Function Arguments:                                                *
  *   lNum    Number of data points in dData.                           *
  *   dData   Data array.                                               *
  *   Sta     Station data structure.                                   *
  *   MtS     MtStuff data structure.                                   *
  *   pdSN    Computed S:N                                              *
  *                                                                     *
  *  Returns:                                                           *
  *   1       OK S:N                                                    *
  *   -1      Low S:N                                                   *
  *                                                                     *
  ***********************************************************************/
  
int CheckDataForSN( long lNum, double *dData, STATION *Sta, MTSTUFF *MtS,
                    double *pdSN )
{
   double  dDC;               /* DC Offset */
   double  dSNNoise, dSNSignal;  /* Signal and noise levels */
   double  dTotal1, dTotal2;  /* Summation variables */
   int     iPreEvent;         /* # samples expected prior to P */
   int     iPSamps;           /* # samples to use for P signal level */
   int     j;
   
   *pdSN = 1.;
   dTotal1 = 0.;
   dTotal2 = 0.;
   iPSamps =   (int) ((double) (P_LENGTH+GF_PREEVENT)*
                               Sta->dSampRate + 0.001);
   iPreEvent = (int) ((double) DATA_PREEVENT*Sta->dSampRate + 0.001);

/* Compute DC */
   for ( j=0; j<iPreEvent; j++ )  
      dTotal1 += dData[j];
   dDC = dTotal1 / ((double) iPreEvent);
logit( "", "%s DC=%lf\n", Sta->szStation, dDC );   

/* Remove DC */
   for ( j=0; j<lNum; j++ )
//{
   dData[j] = dData[j] - dDC;
//logit( "", "%ld %lf\n", j, dData[j] );
//}   
   
/* Compute S:N */
   dTotal1 = 0.;
   for ( j=0; j<iPreEvent+iPSamps; j++ )  
   {
      if ( j < iPreEvent )
         dTotal1 += (dData[j]*dData[j]);
      else
         dTotal2 += (dData[j]*dData[j]);
   }
   dSNNoise  = dTotal1 / ((double) iPreEvent);
   dSNSignal = dTotal2 / ((double) iPSamps);
   *pdSN = dSNSignal / dSNNoise;
	  
   if ( dSNSignal < dSNNoise*SIGNOISE )
   {
      logit( "", "%s S:N too low; %lf %lf %lf\n", Sta->szStation, dSNNoise,
                                                  dSNSignal, *pdSN );
      Sta->iPickStatus = 2;
      return -1;
   }
   else
   {
      logit( "", "%s S:N OK; %lf %lf %lf\n", Sta->szStation, dSNNoise,
                                             dSNSignal, *pdSN );
      return 1;
   }
}

 /***********************************************************************
  *                         CheckDataForSSAmp()                         *
  *                                                                     *
  *  Compute sum square amplitude of signal.                            *
  *                                                                     *
  *  Function Arguments:                                                *
  *   dData   Data array.                                               *
  *   Sta     Station data structure.                                   *
  *   MtS     MtStuff data structure.                                   *
  *   iWinLen Window length in samples.                                 *
  *                                                                     *
  *  Returns:                                                           *
  *                                                                     *
  ***********************************************************************/
  
void CheckDataForSSAmp( double *dData, STATION *Sta, MTSTUFF *MtS, int iWinLen )
{
   int     iSignalStart;      /* # samples expected prior to signal for corr. */
   int     iNumSigSamps;      /* # samples in correlation signal itself */
   int     j;
   
   iSignalStart = (int) ((double) (DATA_PREEVENT)*Sta->dSampRate + 0.001);
   iNumSigSamps = (int) ((double) iWinLen*Sta->dSampRate + 0.001);
   
   MtS->dSumSq = 0.;
   for ( j=iSignalStart; j<iSignalStart+iNumSigSamps; j++ )  
      MtS->dSumSq += (dData[j]*dData[j]);	  
}

 /***********************************************************************
  *                          DeconvolveWF()                             *
  *                                                                     *
  *  Remove instrument response and filter signal using earthworm       *
  * library functions.                                                  *
  *                                                                     *
  *  Function Arguments:                                                *
  *   dData   Input data array                                          *
  *   lNum    Number of data points in dData                            *
  *   dInt    Sample interval (s)                                       *
  *   pR      Response of input data                                    *
  *   pFR     Response of output data                                   *
  *   dTFqs   Taper array                                               *
  *   dDataOut Output data array                                        *
  *   lDNum   Number of data points in dDataOut                         *
  *                                                                     *
  *  Returns:                                                           *
  *   1       OK                                                        *
  *   -1      Problem                                                   *
  *                                                                     *
  ***********************************************************************/
  
int DeconvolveWF( double *dData, long lNum, double dInt, ResponseStruct *pR,
                  ResponseStruct *pFR, double *dTFqs, double *dDataOut,
                  long lDNum )
{
   double  dDataFFT[DECON_BUFFER+1];/* Work buffer for convertWave */ 
   double  dDataWork[DECON_BUFFER+2];/* Work buffer for convertWave */ 
   long    lNnft;             /* Size of fft chosen by gma */
   long    lPadLen;           /* # samples padded during deconvolution */
   int     rc;                /* Return code from convertWave */
   
//   lPadLen = -1;
   lPadLen = 0;
   lNnft = 0;
   rc = convertWave( dData, lNum, dInt, pR, pFR, dTFqs, 0, &lPadLen, &lNnft,
                     dDataOut, lDNum, dDataWork, dDataFFT );
   if ( rc < 0 )              /* Error in reponse removal */
   {
      switch ( rc )
      {
         case -1:
            logit( "", "convertWave failed: out of memory\n" );
            return -1;
         case -3:
            logit( "", "convertWave failed: invalid arguments\n" );
            return -1;
         case -4:
            logit( "", "convertWave failed: FFT error; nfft: %ld\n", lNnft );
            return -1;
         default:
            logit( "", "convertWave failed: unknown error %d\n", rc );
            return -1;
      }
   }
   
/* Was data removed at end of input data? */
   if ( lNnft-lPadLen < lNum )
      logit( "", "Data chopped in convertWave: lNnft=%ld,"
                 " lPadLen=%ld, lNum=%ld\n", lNnft, lPadLen, lNum );
   return 1;
}

 /***********************************************************************
  *                           FillMtDataBuff()                          *
  *                                                                     *
  *  Fill up data buffer for Moment Tensor inversions.                  *
  *                                                                     *
  *  Function Arguments:                                                *
  *   Sta     Station data structure.                                   *
  *   MtS     MtStuff structure (start, end times, etc.).               *
  *   lNum    Number of data points in dData.                           *
  *   dData   Data array.                                               *
  *                                                                     *
  ***********************************************************************/

void FillMtDataBuff( STATION *Sta, MTSTUFF *MtS, long *lNum, double *dData )
{
   long    j;
   long    lStart;            /* Starting index for R-wave from buffer */
   long    lTIndex;           /* Temporary index */

   lStart = (Sta->lSampIndexR-1) - (long) ((Sta->dEndTime-
            MtS->dMtStartTime)*Sta->dSampRate) - DATA_PREEVENT;
   while ( lStart < 0 ) lStart += Sta->lRawCircSize;
   *lNum = (long) ((MtS->dMtEndTime- MtS->dMtStartTime)*
                    Sta->dSampRate) + DATA_PREEVENT + DATA_POSTEVENT;
   if ( *lNum > MTBUFFER_SIZE ) *lNum = MTBUFFER_SIZE;
   if ( *lNum > Sta->lRawCircSize ) *lNum = Sta->lRawCircSize;
   for ( j=0; j<*lNum; j++ )
   {
       lTIndex = lStart + j;
       if ( lTIndex >= Sta->lRawCircSize ) lTIndex -= Sta->lRawCircSize;
       dData[j] = (double) Sta->plRawCircBuff[lTIndex];
   }
}

 /***********************************************************************
  *                       FillMtDataBuffSolo()                          *
  *                                                                     *
  *  Fill up data buffer for Moment Tensor inversions (used in mtsolo). *
  *                                                                     *
  *  Function Arguments:                                                *
  *   Sta     Station data structure.                                   *
  *   MtS     MtStuff structure (start, end times, etc.).               *
  *   lNum    Number of data points in dData.                           *
  *   dData   Data array.                                               *
  *   dDataST 1/1/70 seconds time at beginning of data in StaArray.     *
  *                                                                     *
  ***********************************************************************/

int FillMtDataBuffSolo( STATION *Sta, MTSTUFF *MtS, long *lNum, double *dData,
                        double dDataST )
{
   long    j;
   long    lStart;            /* Starting index for R-wave from buffer */
   long    lTIndex;           /* Temporary index */

   lStart = (long) ((MtS->dMtStartTime-dDataST) * Sta->dSampRate) - DATA_PREEVENT;
   if ( lStart < 0 )
   {
      logit( "", "lStart < 0 in FillMtDataBuffSolo; %ld\n", lStart );
      return -1;
   }
   *lNum = (long) ((MtS->dMtEndTime- MtS->dMtStartTime)*
                    Sta->dSampRate) + DATA_PREEVENT + DATA_POSTEVENT;
   if ( *lNum > MTBUFFER_SIZE ) *lNum = MTBUFFER_SIZE;
   if ( *lNum+lStart > Sta->lRawCircSize ) *lNum = Sta->lRawCircSize-lStart;
   for ( j=0; j<*lNum; j++ )
   {
      lTIndex = lStart + j;
      if ( lTIndex >= Sta->lRawCircSize ) 
      {
         logit( "", "lTIndex < RawCircSize in FillMtDataBuffSolo; %ld\n", lTIndex );
         return -1;
      }
      dData[j] = (double) Sta->plRawCircBuff[lTIndex];
   }
   return 1;
}

 /***********************************************************************
  *                            FillResponse()                           *
  *                                                                     *
  *  Fill up Response Structure.                                        *
  *                                                                     *
  *  Function Arguments:                                                *
  *   pR      Response structure.                                       *
  *   MtS     MtStuff structure (start, end times, etc.).               *
  *                                                                     *
  *  Returns:                                                           *
  *   1       OK allocation                                             *
  *   -1      No allocation                                             *
  *                                                                     *
  ***********************************************************************/

int FillResponse( ResponseStruct *pR, MTSTUFF *MtS )
{
   int     k;

   pR->dGain = MtS->dAmp0;
   pR->iNumPoles = MtS->iNPole;
   pR->iNumZeros = MtS->iNZero;
   if ( (pR->Poles = (PZNum *) malloc( pR->iNumPoles*sizeof(PZNum) )) == NULL )
   {
      logit ("", "Couldn't malloc the Response structure in FillResponse 1\n");
      return -1;
   }   
   for ( k=0; k<pR->iNumPoles; k++ )
   {
      pR->Poles[k].dReal = MtS->zPoles[k].r;
      pR->Poles[k].dImag = MtS->zPoles[k].i;
   }
   if ( (pR->Zeros = (PZNum *) malloc( pR->iNumZeros*sizeof(PZNum) )) == NULL )
   {
      logit ("", "Couldn't malloc the Response structure in FillResponse 2\n");
      return -1;
   }
   for ( k=0; k<pR->iNumZeros; k++ )
   {
      pR->Zeros[k].dReal = MtS->zZeros[k].r;
      pR->Zeros[k].dImag = MtS->zZeros[k].i;
   }
   return 1;
}

 /***********************************************************************
  *                           InitMtResults()                           *
  *                                                                     *
  *  Initialize values in the MTRESULTS structure.                      *
  *                                                                     *
  *  Function Arguments:                                                *
  *   MtR     Moment Tensor results structure.                          *
  *   GP      Input data from d file.                                   *
  *                                                                     *
  ***********************************************************************/

void InitMtR( MTRESULTS *MtR, GPARM *GP )
{
   MtR->dM0 = 1.E26;
   MtR->dScalarMom = 0.;
   MtR->dMag = 0.;
   MtR->iPass = 1;
   MtR->iAlign = GP->Align;
   MtR->dTWindow = GP->TimeWindow;
   MtR->dMaxTLag = GP->MaxTLag;
}

  /***************************************************************
   *                       InitMtStructure()                     *
   *                                                             *
   *  Initialize MtStuff structure.                              *
   *                                                             *
   *  Arguments:                                                 *
   *    Mt          MTSTUFF structure to initialize              *
   *                                                             *
   ***************************************************************/

void InitMtStructure( MTSTUFF *MtT )
{
   MtT->dWt = -1.;
   MtT->iUse = 0;
   MtT->iWvType = 1;
}

