#include <stdio.h>
#include <string.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>
#include "mtinver.h"

#define ncommand 25          /* Number of commands in the config file */


 /***********************************************************************
  *                              GetConfig()                            *
  *             Processes command file using kom.c functions.           *
  *               Returns -1 if any errors are encountered.             *
  ***********************************************************************/

int GetConfig( char *config_file, GPARM *Gparm )
{
   char     init[ncommand];     /* Flags, one for each command */
   int      nmiss;              /* Number of commands that were missed */
   int      nfiles;
   int      i;

/* Set to zero one init flag for each required command
   ***************************************************/
   for ( i=0; i<ncommand; i++ ) init[i] = 0;
   strcpy( Gparm->ATPLineupFileLP, "\0" );

/* Open the main configuration file
   ********************************/
   nfiles = k_open( config_file );
   if ( nfiles == 0 )
   {
      fprintf( stderr, "mtinver: Error opening configuration file <%s>\n",
               config_file );
      return -1;
   }

/* Process all nested configuration files
   **************************************/
   while ( nfiles > 0 )          /* While there are config files open */
   {
      while ( k_rd() )           /* Read next line from active file  */
      {
         int  success;
         char *com;
         char *str;

         com = k_str();          /* Get the first token from line */

         if ( !com ) continue;             /* Ignore blank lines */
         if ( com[0] == '#' ) continue;    /* Ignore comments */

/* Open another configuration file
   *******************************/
         if ( com[0] == '@' )
         {
            success = nfiles + 1;
            nfiles  = k_open( &com[1] );
            if ( nfiles != success )
            {
               fprintf( stderr, "mtinver: Error opening command file <%s>."
			            "\n", &com[1] );
               return -1;
            }
            continue;
         }

/* Read configuration parameters
   *****************************/
         else if ( k_its( "StaFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->StaFile, str );
            init[0] = 1;
         }
		 
         else if ( k_its( "StaDataFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->StaDataFile, str );
            init[1] = 1;
         }

         else if ( k_its( "InRing" ) )
         {
            if ( str = k_str() )
            {
               if( (Gparm->InKey = GetKey(str)) == -1 )
               {
                  fprintf( stderr, "mtinver: Invalid InRing name <%s>. "
                                   "Exiting.\n", str );
                  return -1;
               }
            }
            init[2] = 1;
         }

         else if ( k_its( "HypoRing" ) )
         {
            if ( str = k_str() )
            {
               if ( (Gparm->HKey = GetKey(str)) == -1 )
               {
                  fprintf( stderr, "mtinver: Invalid HypoRing name <%s>. "
                                   "Exiting.\n", str );
                  return -1;
               }
            }
            init[3] = 1;
         }

         else if ( k_its( "HeartbeatInt" ) )
         {
            Gparm->HeartbeatInt = k_int();
            init[4] = 1;
         }

         else if ( k_its( "Debug" ) )
         {
            Gparm->Debug = k_int();
            init[5] = 1;
         }

         else if ( k_its( "MyModId" ) )
         {
            if ( str = k_str() )
            {
               if ( GetModId(str, &Gparm->MyModId) == -1 )
               {
                  fprintf( stderr, "mtinver: Invalid MyModId <%s>.\n", str);
                  return -1;
               }
            }
            init[6] = 1;
         }

         else if ( k_its( "MinutesInBuff" ) )
         {
            Gparm->MinutesInBuff = k_int();
            init[7] = 1;
         }
		 
         else if ( k_its( "DummyFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->DummyFile, str );
            init[8] = 1;
         }
		 
         else if ( k_its( "ResponseFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->ResponseFile, str );
            init[9] = 1;
         }
		 
         else if ( k_its( "MTFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->MTFile, str );
            init[10] = 1;
         }
		 
         else if ( k_its( "EmailFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->EmailFile, str );
            init[11] = 1;
         }

         else if ( k_its( "Align" ) )
         {
            Gparm->Align = k_int();
            init[12] = 1;
         }

         else if ( k_its( "ReadTime" ) )
         {
            Gparm->ReadTime = k_int();
            init[13] = 1;
         }

         else if ( k_its( "TimeWindow" ) )
         {
            Gparm->TimeWindow = k_val();
            init[14] = 1;
         }

         else if ( k_its( "MaxTLag" ) )
         {
            Gparm->MaxTLag = k_val();
            init[15] = 1;
         }
		 
         else if ( k_its( "GFDir" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->GFDir, str );
            init[16] = 1;
         }
		 
         else if ( k_its( "EBPTimeFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->EBPTimeFile, str );
            init[17] = 1;
         }

         else if ( k_its( "LPFileSize" ) )
         {
            Gparm->LPFileSize = k_int();
            init[18] = 1;
         }

         else if ( k_its( "GFSampRate" ) )
         {
            Gparm->GFSampRate = k_val();
            init[19] = 1;
         }
		 
         else if ( k_its( "RootDirectory" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->RootDirectory, str );
            init[20] = 1;
         }
		 
         else if ( k_its( "ArchDirectory" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->ArchDirectory, str );
            init[21] = 1;
         }
		 
         else if ( k_its( "FileSuffix" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->FileSuffix, str );
            init[22] = 1;
         }
		 
         else if ( k_its( "DepthFile" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->DepthFile, str );
            init[23] = 1;
         }
		 
         else if ( k_its( "LocFilePath" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->szLocFilePath, str );
            init[24] = 1;
         }
	 
/* Optional command when run with ATPlayer
  ****************************************/	 
         else if ( k_its( "ATPLineupFileLP" ) )
         {
            if ( str = k_str() )
               strcpy( Gparm->ATPLineupFileLP, str );
         }

/* An unknown parameter was encountered               
   ************************************/
         else
         {
            fprintf( stderr, "mtinver: <%s> unknown parameter in <%s>\n",
                     com, config_file );
            continue;
         }

/* See if there were any errors processing the command
   ***************************************************/
         if ( k_err() )
         {
            fprintf( stderr, "mtinver: Bad <%s> command in <%s>.\n", com,
                     config_file );
            return -1;
         }
      }
      nfiles = k_close();
   }

/* After all files are closed, check flags for missed commands
   ***********************************************************/
   nmiss = 0;
   for ( i = 0; i < ncommand; i++ )
      if ( !init[i] )
         nmiss++;

   if ( nmiss > 0 )
   {
      fprintf( stderr, "mtinver: ERROR, no " );
      if ( !init[0] ) fprintf( stderr, "<StaFile> " );
      if ( !init[1] ) fprintf( stderr, "<StaDataFile> " );
      if ( !init[2] ) fprintf( stderr, "<InRing> " );
      if ( !init[3] ) fprintf( stderr, "<HypoRing> " );
      if ( !init[4] ) fprintf( stderr, "<HeartbeatInt> " );
      if ( !init[5] ) fprintf( stderr, "<Debug> " );
      if ( !init[6] ) fprintf( stderr, "<MyModId> " );
      if ( !init[7] ) fprintf( stderr, "<MinutesInBuff> " );
      if ( !init[8] ) fprintf( stderr, "<DummyFile> " );
      if ( !init[9] ) fprintf( stderr, "<ResponseFile> " );
      if ( !init[10] ) fprintf( stderr, "<MTFile> " );
      if ( !init[11] ) fprintf( stderr, "<EmailFile> " );
      if ( !init[12] ) fprintf( stderr, "<Align> " );
      if ( !init[13] ) fprintf( stderr, "<ReadTime> " );
      if ( !init[14] ) fprintf( stderr, "<TimeWindow> " );
      if ( !init[15] ) fprintf( stderr, "<MaxTLag> " );
      if ( !init[16] ) fprintf( stderr, "<GFDir> " );
      if ( !init[17] ) fprintf( stderr, "<EBPTimeFile> " );
      if ( !init[18] ) fprintf( stderr, "<LPFileSize> " );
      if ( !init[19] ) fprintf( stderr, "<GFSampRate> " );
      if ( !init[20] ) fprintf( stderr, "<RootDirectory> " );
      if ( !init[21] ) fprintf( stderr, "<ArchDirectory> " );
      if ( !init[22] ) fprintf( stderr, "<FileSuffix> " );
      if ( !init[23] ) fprintf( stderr, "<DepthFile> " );
      if ( !init[24] ) fprintf( stderr, "<LocFilePath> " );
      fprintf( stderr, "command(s) in <%s>. Exiting.\n", config_file );
      return -1;
   }
   return 0;
}


 /***********************************************************************
  *                              LogConfig()                            *
  *                                                                     *
  *                   Log the configuration parameters                  *
  ***********************************************************************/

void LogConfig( GPARM *Gparm )
{
   logit( "", "\n" );
   logit( "", "StaFile:          %s\n",    Gparm->StaFile );
   logit( "", "StaDataFile:      %s\n",    Gparm->StaDataFile );
   logit( "", "InKey:            %d\n",    Gparm->InKey );
   logit( "", "HKey:             %d\n",    Gparm->HKey );
   logit( "", "HeartbeatInt:     %d\n",    Gparm->HeartbeatInt );
   logit( "", "Debug:            %d\n",    Gparm->Debug );
   logit( "", "MyModId:          %u\n",    Gparm->MyModId );
   logit( "", "MinutesInBuff:    %ld\n",   Gparm->MinutesInBuff );
   logit( "", "Align:            %ld\n",   Gparm->Align );
   logit( "", "ReadTime:         %ld\n",   Gparm->ReadTime );
   logit( "", "LPFileSize:       %ld\n",   Gparm->LPFileSize );
   logit( "", "TimeWindow:       %lf\n",   Gparm->TimeWindow );
   logit( "", "MaxTLag:          %lf\n",   Gparm->MaxTLag );
   logit( "", "GFSampRate:       %lf\n",   Gparm->GFSampRate );
   logit( "", "DummyFile:        %s\n",    Gparm->DummyFile );
   logit( "", "ReseponseFile:    %s\n",    Gparm->ResponseFile );
   logit( "", "EmailFile:        %s\n",    Gparm->EmailFile );
   logit( "", "DepthFile:        %s\n",    Gparm->DepthFile );
   logit( "", "GFDir:            %s\n",    Gparm->GFDir );
   logit( "", "EBPTimeFile:      %s\n",    Gparm->EBPTimeFile );
   logit( "", "MTFile:           %s\n",    Gparm->MTFile );
   logit( "", "RootDirectory:    %s\n",    Gparm->RootDirectory );
   logit( "", "ArchDirectory:    %s\n",    Gparm->ArchDirectory );
   logit( "", "LocFilePath:      %s\n",    Gparm->szLocFilePath );
   if ( strlen( Gparm->ATPLineupFileLP ) > 2 )
      logit( "", "Mtinver to be used with Player - File: %s\n",
       Gparm->ATPLineupFileLP);
   logit( "", "FileSuffix:       %s\n\n",  Gparm->FileSuffix );
   return;
}
