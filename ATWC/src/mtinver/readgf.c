 /***********************************************************************
  *                              ReadGF()                               *
  *                                                                     *
  *  Routines to read in Green's Functions from SAC files.              *
  *                                                                     *
  ***********************************************************************/
  
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <kom.h>
#include <transport.h>
#include <earthworm.h>
#include <sachead.h>
#include <swap.h>
#include "mtinver.h"

/* Earthworm defines */
void swapsac (struct SAChead *);

extern GF      GreensFunctions[NUM_GFS]; /* Green's function computed signal */
extern int     iGF2Display[3];    /* Indices of Green's Functions to display */


  /******************************************************************
   *                       FindGFIndices()                          *
   *                                                                *
   *  Choose the VLC, VSS, and VDS indices of the closest Green's   *
   *  function in distance and depth.                               *
   *                                                                *
   *  Arguments:                                                    *
   *    pGF         Pointer to Green's function structure array     *
   *    dEpiDist    Epicentral distance in km                       *
   *    iHypoDep    Hypocentral depth in km                         *
   *    iDisplay    Must be defined in calling program as int[3].   *
   *                The returned indices of the appropriate GF.     *
   *                                                                *
   ******************************************************************/

void FindGFIndices( GF *pGF, double dEpiDist, int iHypoDep, int *iDisplay )
{
   double  dDist;   /* GF Array distance (closest to input) */
   double  dMin;    /* Minimum distance (epicentral-GF) */
   int     i;
   double  iDep;    /* GF Array depth (closest to input) */
   int     iMin;    /* Minimum depth (hypocentral-GF) */

   iMin = 10000;
   dMin = 1000000000.;
   for ( i=0; i<NUM_GFS; i++ )
   {
      if ( fabs( pGF[i].dDistance-dEpiDist ) < dMin )
      {
         dDist = pGF[i].dDistance;
         dMin = fabs( pGF[i].dDistance-dEpiDist );
      }
      if ( abs( pGF[i].iDepth-iHypoDep ) < iMin )
      {
         iDep = pGF[i].iDepth;
         iMin = abs( pGF[i].iDepth-iHypoDep );
      }
   }   			   
   for ( i=0; i<NUM_GFS; i++ )
   {
      if ( pGF[i].iDepth == iDep && pGF[i].dDistance == dDist &&
          !strcmp( pGF[i].szComp, "CLV" ) ) iDisplay[0] = i;
      if ( pGF[i].iDepth == iDep && pGF[i].dDistance == dDist &&
          !strcmp( pGF[i].szComp, "VSS" ) ) iDisplay[1] = i;
      if ( pGF[i].iDepth == iDep && pGF[i].dDistance == dDist &&
          !strcmp( pGF[i].szComp, "VDS" ) ) iDisplay[2] = i;
   }
}

  /******************************************************************
   *                    GFDisplayDlgProc()                          *
   *                                                                *
   *  Dialog procedure which allows input of distance (degrees) and *
   *  depth for Green's Function specification.                     *
   *                                                                *
   *  Arguments:                                                    *
   *    Standard dialog procedure arguments and returns             *
   *                                                                *
   ******************************************************************/
   
long WINAPI GFDisplayDlgProc( HWND hwnd, UINT msg, UINT wParam, long lParam )
{
   double  dGFDist;           /* Input distance (deg converted to km) */
   int     iGFDep;            /* Input depth (km) */
   char    szTemp[12];

   switch (msg)
   {
      case WM_INITDIALOG:         /* Set defaults */
         SetDlgItemText( hwnd, EF_GFDIST, "45" );
         SetDlgItemText( hwnd, EF_GFDEP, "50" );
         SetFocus( hwnd );
         break;

      case WM_COMMAND:            
         switch (LOWORD (wParam))
         {
            case IDOK:          /* OK was chosen */
/* Read distance in km */
               GetDlgItemText( hwnd, EF_GFDIST, szTemp, sizeof (szTemp) );
               dGFDist = atof( szTemp );
               dGFDist *= 111.1;    /* Convert to km */
/* Read depth in km */
               GetDlgItemText( hwnd, EF_GFDEP, szTemp, sizeof (szTemp) );
               iGFDep = atoi( szTemp );
/* Find indices of closest Green's Functions (all 3 comps.) */
               FindGFIndices( GreensFunctions, dGFDist, iGFDep, iGF2Display );
               logit( "", "GF to show - %ld %ld %ld\n", iGF2Display[0],
                      iGF2Display[1], iGF2Display[2] );
               EndDialog( hwnd, IDOK );
               break;

            case IDCANCEL:      // Escape was chosen
               EndDialog( hwnd, IDCANCEL );
               break;
         }
      break;
   }
   return 0;
}

  /******************************************************************
   *                     ReadGreensFunctions()                      *
   *                                                                *
   *  Load the Green's function computed signal into an array from  *
   *  disk files.  Files must match the expected sample             *
   *  rate.  The original functions are cut down to GF_TRACELENGTH  *
   *  number of samples.  The cutting goes from Ptime-GF_PREEVENT to*
   *  Ptime+GF_TRACELENGTH-GF_PREEVENT (assuming 1sps).             *
   *                                                                *
   *  Each function is loaded into an array created by a declaration*
   *  statement in the Global variable list.  Earthworm library SAC *
   *  formatting routines are used to read the functions.           *
   *                                                                *
   *  Arguments:                                                    *
   *    pGF         Pointer to Green's function structure array     *
   *    pszGFDir    Pointer to Green's function directory           *
   *                                                                *
   ******************************************************************/

int ReadGreensFunctions( GF *pGF, char *pszGFDir )
{
   WIN32_FIND_DATA  fd;     /* Structure with information about sub-dir */
   WIN32_FIND_DATA  fd2;    /* Structure with information about file */
   float   fSacData[GF_ORIG_TRACELENGTH]; /* SAC GF data */
   HANDLE  hDir;            /* Handle to directory to search for files */
   HANDLE  hDir2;           /* Handle of file to check */
   FILE    *hFile;          /* File Pointer */
   int     i, iCnt;
   int     iRet;            /* Return from FindNextFile (subdir check) */
   int     iRet2;           /* Return from FindNextFile (file check) */
   int     iStartIndex;     /* Starting idx to use when parsing from original */
   char    *psz;            /* String to extract file suffix */
   char    SH[SACHEADERSIZE]; /* SAC header (one long string) */
   struct SAChead *pSH;     /* SAC header structure */
   char	   szFileName[64];  /* GF files names (with path) */
   char	   szFileName2[64]; /* Directory name to search in */
   char	   szFileName3[64]; /* Copy of szFileName2 for later name append */
   char	   szFileName4[64]; /* Full file name to check */
   char	   szFileName5[64]; /* Copy of szFileName2 for later delete */
   
   iCnt = 0;
   logit("et", "Reading Green's Functions\n" );   
   
/* Set up directory name */
   strcpy( szFileName, pszGFDir );
   strcat( szFileName, "\\*.*" );
      
/* Get handle to first directory */      
   hDir = FindFirstFile( szFileName, &fd );
   iRet = 1;
    
/* Loop through all Green's Functions directories */    
   while( hDir != INVALID_HANDLE_VALUE && iRet )
   {
      strcpy( szFileName2, pszGFDir );
      strcat( szFileName2, "\\" );
      strcat( szFileName2, fd.cFileName );
      strcpy( szFileName5, szFileName2 );
      strcat( szFileName2, "\\GF\\" );
      strcpy( szFileName3, szFileName2 );
      strcat( szFileName2, "*.*" );
      iRet2 = 1;
      hDir2 = FindFirstFile (szFileName2, &fd2);

/* Loop while there are still files within this directory */	 
      while ( hDir2 != INVALID_HANDLE_VALUE && iRet2 )
      {	 
/* Set up file name from which to read Green's Functions */	    
         strcpy( szFileName4, szFileName3 );
         strcat( szFileName4, fd2.cFileName );
		 
/* Open and read SAC GF file */		 
         if ( (hFile = fopen( szFileName4, "rb" )) != NULL )
         {
            if ( iCnt >= NUM_GFS )  /* Too many files read from */
            {
               logit( "", "More Files read in (%ld) than can fit - %s\n",
                      iCnt, szFileName4 );
               fclose( hFile );
               FindClose( hDir );
               FindClose( hDir2 );
               goto GetOut;
            }
			
/* Read SAC header */			
            if ( fread( SH, sizeof (char), (size_t) SACHEADERSIZE, hFile ) ==
                (size_t) SACHEADERSIZE )
            {
               pSH = (struct SAChead *) &SH;

/* The SAC header is in Solaris order. If NT, swap its byte order. */   
#ifdef _WINNT
               swapsac( pSH );
#endif _WINNT

/* Log SAC header if desired */
/*             logit( "", "%s: %lf %lf %lf %lf %lf\n", szFileName4, pSH->delta, pSH->depmin, pSH->depmax, pSH->scale, pSH->odelta );
               logit( "", "%lf %lf %lf %lf %lf\n", pSH->b, pSH->e, pSH->o, pSH->a, pSH->internal1 );
               logit( "", "%lf %lf %lf %lf %lf\n", pSH->t0, pSH->t1, pSH->t2, pSH->t3, pSH->t4 );
               logit( "", "%lf %lf %lf %lf %lf\n", pSH->t5, pSH->t6, pSH->t7, pSH->t8, pSH->t9 );
               logit( "", "%lf %lf %lf %lf %lf\n", pSH->f, pSH->resp0, pSH->resp1, pSH->resp2, pSH->resp3 );
               logit( "", "%lf %lf %lf %lf %lf\n", pSH->resp4, pSH->resp5, pSH->resp6, pSH->resp7, pSH->resp8 );
               logit( "", "%lf %lf %lf %lf %lf\n", pSH->resp9, pSH->stla, pSH->stlo, pSH->stel, pSH->stdp );
               logit( "", "%lf %lf %lf %lf %lf\n", pSH->evla, pSH->evlo, pSH->evel, pSH->evdp, pSH->blank1 );
               logit( "", "%lf %lf %lf %lf %lf\n", pSH->user0, pSH->user1, pSH->user2, pSH->user3, pSH->user4 );
               logit( "", "%lf %lf %lf %lf %lf\n", pSH->user5, pSH->user6, pSH->user7, pSH->user8, pSH->user9 );
               logit( "", "%lf %lf %lf %lf %lf\n", pSH->dist, pSH->az, pSH->baz, pSH->gcarc, pSH->internal2 );
               logit( "", "%lf %lf %lf %lf %lf\n", pSH->internal3, pSH->depmen, pSH->cmpaz, pSH->cmpinc, pSH->blank4[0] );
               logit( "", "%lf %lf %lf %lf %lf\n", pSH->blank4[1], pSH->blank4[2], pSH->blank4[3], pSH->blank4[4], pSH->blank4[5] );
               logit( "", "%lf %lf %lf %lf %lf\n", pSH->blank4[6], pSH->blank4[7], pSH->blank4[8], pSH->blank4[9], pSH->blank4[10] );
               logit( "", "%ld %ld %ld %ld %ld\n", pSH->nzyear, pSH->nzjday, pSH->nzhour, pSH->nzmin, pSH->nzsec );
               logit( "", "%ld %ld %ld %ld %ld\n", pSH->nzmsec, pSH->internal4, pSH->internal5, pSH->internal6, pSH->npts );
               logit( "", "%ld %ld %ld %ld %ld\n", pSH->internal7, pSH->internal8, pSH->blank6[0], pSH->blank6[1], pSH->blank6[2] );
               logit( "", "%ld %ld %ld %ld %ld\n", pSH->iftype, pSH->idep, pSH->iztype, pSH->iblank6a, pSH->iinst );
               logit( "", "%ld %ld %ld %ld %ld\n", pSH->istreg, pSH->ievreg, pSH->ievtyp, pSH->iqual, pSH->isynth );
               logit( "", "%ld %ld %ld %ld %ld\n", pSH->blank7[0], pSH->blank7[1], pSH->blank7[2], pSH->blank7[3], pSH->blank7[4] );
               logit( "", "%ld %ld %ld %ld %ld\n", pSH->blank7[5], pSH->blank7[6], pSH->blank7[7], pSH->blank7[8], pSH->blank7[9] );
               logit( "", "%ld %ld %ld %ld %ld\n", pSH->leven, pSH->lpspol, pSH->lovrok, pSH->lcalda, pSH->lblank1 );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kstnm[i] );
			   logit( "", " " );
               for ( i=0; i<KEVNMLEN; i++ ) logit( "", "%c", pSH->kevnm[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->khole[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->ko[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->ka[i] );
			   logit( "", " \n" );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kt0[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kt1[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kt2[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kt3[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kt4[i] );
			   logit( "", " \n" );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kt5[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kt6[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kt7[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kt8[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kt9[i] );
			   logit( "", " \n" );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kf[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kuser0[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kuser1[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kuser2[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kcmpnm[i] );
			   logit( "", " \n" );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->knetwk[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kdatrd[i] );
			   logit( "", " " );
               for ( i=0; i<K_LEN; i++ ) logit( "", "%c", pSH->kinst[i] );
			   logit( "", " \n" );
*/

/* Read SAC format Green's function */
               if ( pSH->npts <= GF_ORIG_TRACELENGTH )
               {
                  if ( fread( &fSacData, sizeof (float), 
                      (size_t) pSH->npts, hFile ) != (size_t) pSH->npts )
                     logit( "", "Error reading %d bytes data from file: %s!\n",
                            pSH->npts, szFileName4 );
							
/* Swap data values if NT  */
#ifdef _WINNT
                  else
                     for ( i=0; i<pSH->npts; i++) SwapFloat( &fSacData[i] );
#endif _WINNT

/* Log Green's Function if desired - (lots of space) */
/*                logit( "", "%s\n", szFileName4 );
                  for ( i=0; i<pSH->npts; i++ )
                  {
                     logit( "", "%lf ", fSacData[i] );
					 if ( ((i+1)%8) == 0 ) logit( "", "\n" ); 
                  }	*/
				  
/* Parse function so that a total of GF_TRACELENGTH samples are saved to
   the Green's function structure (GF_PREEVENT before P) */
                  iStartIndex = (int) (pSH->t0-pSH->b) - 1 - GF_PREEVENT;
                  if ( iStartIndex < 0 ) iStartIndex = 0;
                  for ( i=0; i<GF_TRACELENGTH; i++ )
                     pGF[iCnt].fGF[i] = fSacData[iStartIndex+i];
					 
/* Save salient parts of SAC header in GF structure */
                  pGF[iCnt].iPrePTime = GF_PREEVENT;					 
                  pGF[iCnt].iDepth = (int) pSH->evdp;					 
                  pGF[iCnt].dDistance = (double) pSH->dist;	 
                  pGF[iCnt].dSR = 1. / (double) pSH->delta;					 
                  pGF[iCnt].dMax = (double) pSH->depmax;					 
                  pGF[iCnt].dMin = (double) pSH->depmin;	 
                  pGF[iCnt].iFini = (int) (pSH->f-pSH->t0) + 1 + GF_PREEVENT;	 
                  psz = strrchr( fd2.cFileName, '.' );  /* Get loc of . */
                  strcpy( pGF[iCnt].szComp, (psz+1) );  /* Extract the suffix */
               }
               else   /* Couldn't read data (too much) */
                  logit( "", "More data in file (%d): %s than allowed.\n",
                         pSH->npts, szFileName4 );
               fclose( hFile );
            }
            else      /* Error reading header */
            {
               logit( "", "Error reading header: %s\n", szFileName4 );
               fclose( hFile );
            }
            iCnt++;
         }			
         else         /* SAC file not opened */
            logit( "", "Can't open SAC GF File %s\n", szFileName4 );
		 
         iRet2 = FindNextFile( hDir2, &fd2 );
      }
      FindClose( hDir2 );
	 
/* Go to next directory */
      iRet = FindNextFile( hDir, &fd );
   }
   FindClose( hDir );   
GetOut:;   
   logit( "", "%ld files read\n", iCnt );
   return ( 0 );
}

