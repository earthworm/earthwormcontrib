#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include "mtinver.h"

  /******************************************************************
   *                      HseHld()                                  *
   *                                                                *
   *  Routine from Chuck Langston - momvert                         *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/21/02.                          *
   *                                                                *
   *  Arguments:                                                    *
   *   nn         size of matrix                                    *
   *   a          matrix (only lower triangle used, it is destroyed)*
   *   eig        returned eigenvalues in algebraic descending order*
   *   vec        returned eigenvectors in columns                  *
   *                                                                *
   *  Return:       0 = OK; -1 = Problem with matrix size           *
   *                                                                *
   ******************************************************************/

int HseHld( int nn, double a[][NSIZE], double vec[][NSIZE], double eig[][3] )
{
   double  s, sgn, b, trace, enorm, sqrts, d, temp, wtaw, sum, qj, wj, shift,
           cosa, g, pp, ppbs, ppbr, cosap, sina, sina2, dia, u, a2, r2,
           r1, r12, dif, esum, essq;
   int     neig[100];                     /* Sorted indices (on 0) */
   int     n, nn1, n1, n2, m, npas, nt, nv, np, lv, n1k; /* Based on 1 */
   int     i, ii, i1, j, k, l, nr, nrr;   /* Based on 0 */

   if ( nn > 100 )
   {
      logit( "", "Array exceeds 100 elements in HseHld - %ld\n", nn );
      return -1;
   }
   
   n = abs( nn );
   nn1 = n - 1;
   for ( i=0; i<nn1; i++ )
   {
      ii = i + 1;
      for ( j=ii; j<n; j++ ) a[j][i] = a[i][j];
   }
   
   if ( n == 0 ) return -1;
   n1 = n - 1;
   n2 = n - 2;
   
/* Compute the trace and euclidian norm of the input matrix
   later check against sum and sum of squares of eigenvalues */
   enorm = 0.;
   trace = 0.;
   for ( j=0; j<n; j++ )
   {
      for ( i=j; i<n; i++ ) enorm += (a[i][j]*a[i][j]);
      trace += a[j][j];
      enorm = enorm - (.5*(a[j][j]*a[j][j]));
   }
   enorm = enorm + enorm;
   eig[2][0] = a[0][0];
   
   if ( n2 > 0 ) 
   {
      for ( nr=0; nr<n2; nr++ )
      {
         b = a[nr+1][nr];
         s = 0.;
         for ( i=nr; i<n2; i++ ) s = s + (a[i+2][nr]*a[i+2][nr]);
		 
/* Prepare for possible bypass of transformation */
         a[nr+1][nr] = 0.;
         if ( s > 0. )
         {
            s += (b*b);
            sgn = 1.;
            if ( b < 0 ) sgn = -1.;
            sqrts = sqrt( s );
            d = sgn / (sqrts+sqrts);
            temp = sqrt( .5 + b*d );
            eig[4][nr] = temp;
            a[nr+1][nr] = temp;
            b = (sgn*-1.) * sqrts;
			
/* d is factor of proportionality. now compute and save w vector. */
            d /= temp;
			
/* Extra singly subscripted w vector used for speed. */
            for ( i=nr; i<n2; i++ ) 
            {
               temp = d * a[i+2][nr];
               eig[4][i+1] = temp;
               a[i+2][nr] = temp;
            }
			
/* Premultiply vector w by matrix a to obtain p vector.
   Simultaneously accumulate dot product wp (the scalar k) */
            wtaw = 0.;
            for ( i=nr; i<n1; i++ ) 
            {
               sum = 0.;
               for ( j=nr; j<=i; j++ ) sum += (a[i+1][j+1]*eig[4][j]);
               i1 = i + 1;
               if ( n1-(i1+1) >= 0 ) 
                  for ( j=i1; j<n1; j++ ) sum += (a[j+1][i+1]*eig[4][j]);
               eig[1][i] = sum;
               wtaw += (sum*eig[4][i]);
            }
			
/* p vector and scalar k now stored. next compute q vector */
            for ( i=nr; i<n1; i++ ) eig[1][i] -= (wtaw * eig[4][i]);
			
/* Now form pap matrix, required part */
            for ( j=nr; j<n1; j++ ) 
            {
               qj = eig[1][j];
               wj = eig[4][j];
               for ( i=j; i<n1; i++ ) 
                  a[i+1][j+1] -= (2.*(eig[4][i]*qj + wj*eig[1][i]));
            }
         }
         eig[1][nr] = b;
         eig[3][nr] = b * b;
         eig[2][nr+1] = a[nr+1][nr+1];
      }
   }
   
   if ( n2 >= 0 )
   {
      b = a[n-1][n-2];
      eig[1][n-2] = b;
      eig[3][n-2] = b * b;
      eig[2][n-1] = a[n-1][n-1];
   }
   eig[3][n-1] = 0.;
   
/* Adjoin an identity matrix to be postmultiplied by rotations. */
   for ( i=0; i<n; i++ ) 
   {
      for ( j=0; j<n; j++ ) vec[i][j] = 0.;
      vec[i][i] = 1.;
   }
   m = n;
   sum = 0.;
   npas = 1;
   goto JumpHere;
   
JumpBack:
   sum += shift;
   cosa = 1.;
   g = eig[2][0] - shift;
   pp = g;
   ppbs = pp*pp + eig[3][0];
   ppbr = sqrt( ppbs );
   for ( j=0; j<m; j++ ) 
   {
      cosap = cosa;
      if ( ppbs == 0. ) 
      {
         sina = 0.;
         sina2 = 0.;
         cosa = 1.;
      }
      else
      {
         sina = eig[1][j] / ppbr;
         if ( fabs( sina ) < 1.0e-25) sina = 0.0;
         sina2 = eig[3][j] / ppbs;
         cosa = pp / ppbr;
		 
/* Postmultiply identity by p-transpose matrix */
         nt = (j+1) + npas;
         if ( nt >= n ) nt = n;
         for ( i=0; i<nt; i++ ) 
         {
            temp = cosa*vec[i][j] + sina*vec[i][j+1];
            if ( fabs( temp ) < 1.0e-38) temp = 0.0;
            vec[i][j+1] = (-1.*sina*vec[i][j]) + cosa*vec[i][j+1];
            vec[i][j] = temp;
         }
      }
      dia = eig[2][j+1] - shift;
      u = sina2 * (g+dia);
      eig[2][j] = g + u;
      g = dia - u;
      pp = dia*cosa - sina*cosap*eig[1][j];
      if ( (j+1) == m ) 
      {
         eig[1][j] = sina * pp;
         eig[3][j] = sina2 * pp * pp;
      }
      else
      {
         ppbs = pp*pp + eig[3][j+1];
         ppbr = sqrt( ppbs );
         eig[1][j] = sina * ppbr;
         eig[3][j] = sina2 * ppbs;
      }
   }
   eig[2][m] = g;
   
/* Test for convergence of last diagonal element */
   npas = npas + 1;
   if ( eig[3][m-1] <= 1.e-21 )
   {
IfTop:   
      eig[0][m] = eig[2][m] + sum;
JumpHere:
      eig[1][m-1] = 0.;
      eig[3][m-1] = 0.;
      m = m - 1;
      if ( m == 0) goto JumpAhead;
      if ( eig[3][m-1] <= 1.e-21) goto IfTop;
   }
	  
/* Take root of corner 2 by 2 nearest to lower diagonal in value
   as estimate of eigenvalue to use for shift */
   a2 = eig[2][m];
   r2 = .5 * a2;
   r1 = .5 * eig[2][m-1];
   r12 = r1 + r2;
   dif = r1 - r2;
   temp = sqrt( dif*dif + eig[3][m-1] );
   r1 = r12 + temp;
   r2 = r12 - temp;
   dif = fabs( a2-r1 ) - fabs( a2-r2 );
   if ( dif >= 0. ) shift = r2;
   else             shift = r1;
   goto JumpBack;
JumpAhead:	  
   eig[0][0] = eig[2][0] + sum;
	  
/* Use SortD2 to sort the eigenvalues */
   SortD2( &eig[0][0], n, neig );
   for ( i=0; i<n; i++ ) eig[4][i] = neig[i];
   if ( nn < 0 ) return 0;
   if ( n1 != 0 ) 
   {
	  
/* Initialize auxiliary tables required for rearranging the vectors */
      for ( k=0; k<n; k++ ) 
      {
         eig[3][k] = k+1;
         eig[2][k] = k+1;
      }
      for ( l=0; l<n1; l++ ) 
      {
         nv = (int) eig[4][l];
         np = (int) eig[3][nv-1];
         if ( (np-1) != l) 
         {
            lv = (int) eig[2][l];
            eig[2][np-1] = lv;
            eig[3][lv-1] = np;
            for ( i=0; i<n; i++ ) 
            {
               temp = vec[i][l];
               vec[i][l] = vec[i][np-1];
               vec[i][np-1] = temp;
            }
         }
      }
   }
   
   esum = 0.;
   essq = 0.;
   
/* Back transform the vectors of the triple diagonal matrix */
   for ( nrr=0; nrr<n; nrr++ ) 
   {
      n1k = n1;
      n1k -= 1;
      while (n1k > 0 )
      {
         sum = 0.;
         for ( i=n1k-1; i<n1; i++ ) sum += (vec[i+1][nrr]*a[i+1][n1k-1]);
         sum += sum;
         for ( i=n1k-1; i<n1; i++ ) vec[i+1][nrr] -= (sum*a[i+1][n1k-1]);
         n1k -= 1;
      }
      esum += eig[0][nrr];
      essq += (eig[0][nrr]*eig[0][nrr]);
   }
   return 0;
}

void SortD2( double *a, int n, int idx[] )
{
   double  t, c;
   int     i;                                                    /* Based on 0 */
   int     ii, ik, jk, n1, n2, ntemp, n21, ict, np2, ic, it;     /* Based on 1 */

   if ( n == 1 )
   {
      idx[0] = 1;
      return;
   }
   if ( n <= 0 )
   {
      logit( "", "n <= 0 in SortD2 (%ld)\n", n );
      return;
   }
   for ( i=0; i<n; i++ ) idx[i] = i+1;
   n2 = n / 2;
   n21 = n2 + 2;
   ict = 1;
   ii = 2;
ComeBack:
   n1 = n21 - ii;
   ntemp = n;
   ik = n1;
ComeBack2:
   c = a[ik-1];
   ic = idx[ik-1];
ComeBack3:
   jk = 2*ik;
   if ( jk > ntemp ) goto JumpAhead;
   if ( jk != ntemp && a[jk] < a[jk-1] ) jk++;
   if ( a[jk-1] < c )
   {
      a[ik-1] = a[jk-1];
      idx[ik-1] = idx[jk-1];
      ik = jk;
      goto ComeBack3;
   }
JumpAhead:
   a[ik-1] = c;
   idx[ik-1] = ic;
   if ( ict == 1 ) 
   {
      if ( ii < n2 )
      {
         ii++;
         goto ComeBack;
      }
      ict = 2;
      np2 = n + 2;
      ii = 2;
ComeBack4:
      n1 = np2 - ii;
      ntemp = n1;
      ik = 1;
      goto ComeBack2;
   }
   t = a[0];
   a[0] = a[n1-1];
   a[n1-1] = t;
   it = idx[0];
   idx[0] = idx[n1-1];
   idx[n1-1] = it;
   if ( ii < n ) 
   {
      ii++;
      goto ComeBack4;
   }
}

  /******************************************************************
   *                       Mult1()                                  *
   *                                                                *
   *  matrix multiplication.                          a.shakal 8/74 *
   *   form the matrix product c(m by l) = a(m by n) * b(n by l).   *
   *   if atrnsp =-1, form c = a(transpose)*b;                      *
   *   if btrnsp =-1, form c = a*b(transpose);                      *
   *   if both =-1, form c=a(transpose)*b(transpose).               *
   *    (note the matrices must be conformable - the 1st matrix must*
   *     be mxn, and the 2nd nxl, in the form (normal or transpose) *
   *     in which multiplication is to be performed.)               *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/12/02.                          *
   *                                                                *
   *  Arguments:                                                    *
   *   All integer arguments based on 1                             *
   *                                                                *
   ******************************************************************/

void Mult1( double a[][1], int iATrnsp, double b[][MAXMAT], int iBTrnsp,
            double c[][MAXMAT], int m, int n, int l, int iSymm )
{
   double  aa, bb;
   int     iSym;
   int     i, j, k, i2, j2;                   /* Based on 0 */
   int     iATrns, iBTrns;
	  
/* Switch for transpose of 1st matrix. */
   iATrns = 0;
   if ( iATrnsp == -1 ) iATrns = 1;
	  
/* Switch for transpose of 2nd matrix. */
   iBTrns = 0;
   if ( iBTrnsp == -1 ) iBTrns = 1;

/* Switch for symmetric product - if symmetric, just calculate
   below and on diagonal, then fold. */
   iSym = 0;
   if ( iSymm == 1 ) iSym = 1;
   j2 = l-1;
   for ( i=0; i<m; i++ )
   {
      if ( iSym == 1 ) j2=i;
      for ( j=0; j<=j2; j++ )
      {
         c[i][j] = 0.;
         for ( k=0; k<n; k++ )
         {
            aa = a[i][k];
            if ( iATrns == 1 ) aa = a[k][i];
            bb = b[k][j];
            if ( iBTrns == 1 ) bb = b[j][k];
            c[i][j] = aa*bb + c[i][j];
         }
      }
   }
   if ( iSym == 0 ) return;
   for ( j=1; j<l; j++ )
   {
      i2 = j - 1;
      for ( i=0; i<=i2; i++ ) c[i][j] = c[j][i];
   }
}

  /******************************************************************
   *                       Mult2()                                  *
   *                                                                *
   *  matrix multiplication.                          a.shakal 8/74 *
   *   form the matrix product c(m by l) = a(m by n) * b(n by l).   *
   *   if atrnsp =-1, form c = a(transpose)*b;                      *
   *   if btrnsp =-1, form c = a*b(transpose);                      *
   *   if both =-1, form c=a(transpose)*b(transpose).               *
   *    (note the matrices must be conformable - the 1st matrix must*
   *     be mxn, and the 2nd nxl, in the form (normal or transpose) *
   *     in which multiplication is to be performed.)               *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/12/02.                          *
   *                                                                *
   *  Arguments:                                                    *
   *   All integer arguments based on 1                             *
   *                                                                *
   ******************************************************************/

void Mult2( double a[][MAXMAT], int iATrnsp, double b[][1], int iBTrnsp,
            double c[][1], int m, int n, int l, int iSymm )
{
   double  aa, bb;
   int     iSym;
   int     i, j, k, i2, j2;                   /* Based on 0 */
   int     iATrns, iBTrns;
	  
/* Switch for transpose of 1st matrix. */
   iATrns = 0;
   if ( iATrnsp == -1 ) iATrns = 1;
	  
/* Switch for transpose of 2nd matrix. */
   iBTrns = 0;
   if ( iBTrnsp == -1 ) iBTrns = 1;

/* Switch for symmetric product - if symmetric, just calculate
   below and on diagonal, then fold. */
   iSym = 0;
   if ( iSymm == 1 ) iSym = 1;
   j2 = l-1;
   for ( i=0; i<m; i++ )
   {
      if ( iSym == 1 ) j2=i;
      for ( j=0; j<=j2; j++ )
      {
         c[i][j] = 0.;
         for ( k=0; k<n; k++ )
         {
            aa = a[i][k];
            if ( iATrns == 1 ) aa = a[k][i];
            bb = b[k][j];
            if ( iBTrns == 1 ) bb = b[j][k];
            c[i][j] = aa*bb + c[i][j];
         }
      }
   }
   if ( iSym == 0 ) return;
   for ( j=1; j<l; j++ )
   {
      i2 = j - 1;
      for ( i=0; i<=i2; i++ ) c[i][j] = c[j][i];
   }
}	  

  /******************************************************************
   *                       Mult3()                                  *
   *                                                                *
   *  matrix multiplication.                          a.shakal 8/74 *
   *   form the matrix product c(m by l) = a(m by n) * b(n by l).   *
   *   if atrnsp =-1, form c = a(transpose)*b;                      *
   *   if btrnsp =-1, form c = a*b(transpose);                      *
   *   if both =-1, form c=a(transpose)*b(transpose).               *
   *    (note the matrices must be conformable - the 1st matrix must*
   *     be mxn, and the 2nd nxl, in the form (normal or transpose) *
   *     in which multiplication is to be performed.)               *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/12/02.                          *
   *                                                                *
   *  Arguments:                                                    *
   *   All integer arguments based on 1                             *
   *                                                                *
   ******************************************************************/

void Mult3( double a[][2], int iATrnsp, double b[][MAXMAT], int iBTrnsp,
            double c[][MAXMAT], int m, int n, int l, int iSymm )
{
   double  aa, bb;
   int     iSym;
   int     i, j, k, i2, j2;                   /* Based on 0 */
   int     iATrns, iBTrns;
	  
/* Switch for transpose of 1st matrix. */
   iATrns = 0;
   if ( iATrnsp == -1 ) iATrns = 1;
	  
/* Switch for transpose of 2nd matrix. */
   iBTrns = 0;
   if ( iBTrnsp == -1 ) iBTrns = 1;

/* Switch for symmetric product - if symmetric, just calculate
   below and on diagonal, then fold. */
   iSym = 0;
   if ( iSymm == 1 ) iSym = 1;
   j2 = l-1;
   for ( i=0; i<m; i++ )
   {
      if ( iSym == 1 ) j2=i;
      for ( j=0; j<=j2; j++ )
      {
         c[i][j] = 0.;
         for ( k=0; k<n; k++ )
         {
            aa = a[i][k];
            if ( iATrns == 1 ) aa = a[k][i];
            bb = b[k][j];
            if ( iBTrns == 1 ) bb = b[j][k];
            c[i][j] = aa*bb + c[i][j];
         }
      }
   }
   if ( iSym == 0 ) return;
   for ( j=1; j<l; j++ )
   {
      i2 = j - 1;
      for ( i=0; i<=i2; i++ ) c[i][j] = c[j][i];
   }
}	  

  /******************************************************************
   *                       Mult4()                                  *
   *                                                                *
   *  matrix multiplication.                          a.shakal 8/74 *
   *   form the matrix product c(m by l) = a(m by n) * b(n by l).   *
   *   if atrnsp =-1, form c = a(transpose)*b;                      *
   *   if btrnsp =-1, form c = a*b(transpose);                      *
   *   if both =-1, form c=a(transpose)*b(transpose).               *
   *    (note the matrices must be conformable - the 1st matrix must*
   *     be mxn, and the 2nd nxl, in the form (normal or transpose) *
   *     in which multiplication is to be performed.)               *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/12/02.                          *
   *                                                                *
   *  Arguments:                                                    *
   *   All integer arguments based on 1                             *
   *                                                                *
   ******************************************************************/

void Mult4( double a[][MAXMAT], int iATrnsp, double b[][2], int iBTrnsp,
            double c[][2], int m, int n, int l, int iSymm )
{
   double  aa, bb;
   int     iSym;
   int     i, j, k, i2, j2;                   /* Based on 0 */
   int     iATrns, iBTrns;
	  
/* Switch for transpose of 1st matrix. */
   iATrns = 0;
   if ( iATrnsp == -1 ) iATrns = 1;
	  
/* Switch for transpose of 2nd matrix. */
   iBTrns = 0;
   if ( iBTrnsp == -1 ) iBTrns = 1;

/* Switch for symmetric product - if symmetric, just calculate
   below and on diagonal, then fold. */
   iSym = 0;
   if ( iSymm == 1 ) iSym = 1;
   j2 = l-1;
   for ( i=0; i<m; i++ )
   {
      if ( iSym == 1 ) j2=i;
      for ( j=0; j<=j2; j++ )
      {
         c[i][j] = 0.;
         for ( k=0; k<n; k++ )
         {
            aa = a[i][k];
            if ( iATrns == 1 ) aa = a[k][i];
            bb = b[k][j];
            if ( iBTrns == 1 ) bb = b[j][k];
            c[i][j] = aa*bb + c[i][j];
         }
      }
   }
   if ( iSym == 0 ) return;
   for ( j=1; j<l; j++ )
   {
      i2 = j - 1;
      for ( i=0; i<=i2; i++ ) c[i][j] = c[j][i];
   }
}	  
	  
  /******************************************************************
   *                           SvdCmp()                             *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/8/02.                           *
   *                                                                *
   ******************************************************************/
   
void SvdCmp( double a[][NPRM], int m, int n, double w[], double v[][NPRM] )
{
   double  dAnorm;
   double  dG, dS, dF, dH, dC, dX, dY, dZ;
   double  dRv1[NMAX];
   double  dScale;
   int     i, j, k, l, ii, nm, nmx;
	  
   dG = 0.0;
   dScale = 0.0;
   dAnorm = 0.0;
   for ( i=0; i<n; i++ )
   {
      l = i+1;
      dRv1[i] = dScale*dG;
      dG = 0.0;
      dS = 0.0;
      dScale = 0.0;
      if ( i+1 <= m )
      {
         for ( k=i; k<m; k++ ) dScale += fabs( a[k][i] );
         if ( dScale != 0. ) 
         {
            for ( k=i; k<m; k++ )
            {
               a[k][i] /= dScale;
               dS += (a[k][i]*a[k][i]);
            }
            dF = a[i][i];
            dG = (-1.) * sqrt( dS ) * (fabs( dF )/dF); /* Simulate FORTR SIGN */
            dH = dF*dG - dS;
            a[i][i] = dF-dG;
            if ( i+1 != n ) 
               for ( j=l; j<n; j++ )
               {
                  dS = 0.0;
                  for ( k=i; k<m; k++ ) dS += (a[k][i]*a[k][j]);
                  dF = dS/dH;
                  for ( k=i; k<m; k++ ) a[k][j] += (dF*a[k][i]);
               }
            for ( k=i; k<m; k++ ) a[k][i] *= dScale;
         }
      }
      w[i] = dScale * dG;
      dG = 0.0;
      dS = 0.0;
      dScale = 0.0;
      if ( i <= (m-1) && i != (n-1) )
      {
         for ( k=l; k<n; k++ ) dScale += fabs( a[i][k] );
         if ( dScale != 0. )
         {
            for ( k=l; k<n; k++ )
            {
               a[i][k] /= dScale;
               dS += (a[i][k]*a[i][k]);
            }
            dF = a[i][l];
            dG = (-1.) * sqrt( dS ) * (fabs( dF )/dF); /* Simulate FORTR SIGN */
            dH = dF*dG - dS;
            a[i][l] = dF-dG;
            for ( k=l; k<n; k++ ) dRv1[k] = a[i][k]/dH;
            if ( i != (m-1) )
			   for ( j=l; j<m; j++ ) 
               {
                  dS = 0.0;
                  for ( k=l; k<n; k++ ) dS += (a[j][k]*a[i][k]);
                  for ( k=l; k<n; k++ ) a[j][k] += (dS*dRv1[k]);
               }
            for ( k=l; k<n; k++ ) a[i][k] *= dScale;
         }
      }
      dAnorm = max( dAnorm, (fabs( w[i] )+ fabs( dRv1[i] )) );
   }
   for ( i=n-1; i>=0; i-- )
   {
      if ( i < (n-1) )
      {
         if ( dG != 0.0 )
         {
            for ( j=l; j<n; j++ ) v[j][i] = (a[i][j]/a[i][l]) / dG;
            for ( j=l; j<n; j++ ) 
            {
               dS = 0.0;
               for ( k=l; k<n; k++ ) dS += (a[i][k]*v[k][j]);
               for ( k=l; k<n; k++ ) v[k][j] += (dS*v[k][i]);
            }
         }
         for ( j=l; j<n; j++ ) 
         {
            v[i][j] = 0.;
            v[j][i] = 0.;
         }
      }
      v[i][i] = 1.;
      dG = dRv1[i];
      l = i;
   }
   for ( i=n-1; i>=0; i-- )
   {
      l = i+1;
      dG = w[i];
      if ( i < (n-1) )
         for ( j=l; j<n; j++ ) a[i][j] = 0.0;
      if ( dG != 0. )
      {
         dG = 1.0 / dG;
         if ( i != (n-1) )
            for ( j=l; j<n; j++ )
            {
               dS = 0.0;
               for ( k=l; k<m; k++ ) dS += (a[k][i]*a[k][j]);
               dF = (dS/a[i][i])*dG;
               for ( k=i; k<m; k++ ) a[k][j] += (dF*a[k][i]);
            }
         for ( j=i; j<m; j++ ) a[j][i] *= dG;
      }
      else
         for ( j=i; j<m; j++ ) a[j][i] = 0.;
      a[i][i] += 1.; 
   }
   for ( k=n-1; k>=0; k-- )
   {
      for ( ii=0; ii<50; ii++ )
      {
         for ( l=k; l>=0; l-- )
         {
            nm=l-1;
            if ( (fabs( dRv1[l] )+dAnorm) == dAnorm ) goto NextLoop;
            if ( nm >= 0 && (fabs( w[nm] )+dAnorm) == dAnorm ) break;
         }
         dC = 0.0;
         dS = 1.0;
         for ( i=l; i<=k; i++ )
         {
            dF = dS*dRv1[i];
            if ( (fabs( dF )+dAnorm) != dAnorm )
            {
               dG = w[i];
               dH = sqrt( dF*dF + dG*dG );
               w[i] = dH;
               dH = 1.0/dH;
               dC = dG*dH;
               dS = -1.*(dF*dH);
               for ( j=0; j<m; j++ )
               {
                  if ( nm >= 0 ) dY = a[j][nm];
                  dZ = a[j][i];
                  if ( nm >= 0 ) a[j][nm] = (dY*dC) + (dZ*dS);
                  a[j][i]  = -1.*(dY*dS) + (dZ*dC);
               }
            }
         }
NextLoop: dZ = w[k];
         if ( l == k )
         {
            if ( dZ < 0.0 )
            {
               w[k] = -dZ;
               for ( j=0; j<n; j++ ) v[j][k] = -1.*v[j][k];
            }
            goto EndLoop;
         }
         dX = w[l];
         nm = k-1;
         if ( nm >= 0 ) dY = w[nm];
         if ( nm >= 0 ) dG = dRv1[nm];
         dH = dRv1[k];
         dF = ((dY-dZ)*(dY+dZ) + (dG-dH)*(dG+dH))/(2.0*dH*dY);
         dG = sqrt( dF*dF + 1.0 );
         dF = ((dX-dZ)*(dX+dZ) + dH*((dY/(dF+(dG*fabs( dF )/dF)))-dH))/dX;
         dC = 1.0;
         dS = 1.0;
         for ( j=l; j<=nm; j++ )
         {
            i = j+1;
            dG = dRv1[i];
            dY = w[i];
            dH = dS*dG;
            dG = dC*dG;
            dZ = sqrt( dF*dF + dH*dH );
            dRv1[j] = dZ;
            dC = dF/dZ;
            dS = dH/dZ;
            dF =     (dX*dC) + (dG*dS);
            dG = -1.*(dX*dS) + (dG*dC);
            dH = dY*dS;
            dY = dY*dC;
            for ( nmx=0; nmx<n; nmx++ )
            {
               dX = v[nmx][j];
               dZ = v[nmx][i];
               v[nmx][j] =     (dX*dC) + (dZ*dS);
               v[nmx][i] = -1.*(dX*dS) + (dZ*dC);
            }
            dZ = sqrt( dF*dF + dH*dH );
            w[j] = dZ;
            if ( dZ != 0.0 ) 
            {
               dZ = 1.0/dZ;
               dC = dF*dZ;
               dS = dH*dZ;
            }
            dF =     (dC*dG) + (dS*dY);
            dX = -1.*(dS*dG) + (dC*dY);
            for ( nmx=0; nmx<m; nmx++ )
            {
               dY = a[nmx][j];
               dZ = a[nmx][i];
               a[nmx][j] =     (dY*dC) + (dZ*dS);
               a[nmx][i] = -1.*(dY*dS) + (dZ*dC);
            }
         }
         dRv1[l] = 0.0;
         dRv1[k] = dF;
         w[k] = dX;
      }
EndLoop:;
   }
}

  /******************************************************************
   *                           SvdVar()                             *
   *                                                                *
   *  Compute co-variant matrix.                                    *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/8/02.                           *
   *                                                                *
   ******************************************************************/
   
void SvdVar( double v[][NPRM], int ma, double w[], double cvm[][NPRM] )
{			 
   double  dSum;                      /* Summation variabe */
   double  wti[MMAX];
   int     i, j, k;                      /* Counters */

   for ( i=0; i<ma; i++ )
   {
      wti[i] = 0.;
      if ( w[i] != 0. ) wti[i] = 1./(w[i]*w[i]);
   }
   for ( i=0; i<ma; i++ )
      for ( j=0; j<=i; j++ )
      {
         dSum = 0.;
         for ( k=0; k<ma; k++ )
            dSum += (v[i][k]*v[j][k]*wti[k]);
         cvm[i][j] = dSum;
         cvm[j][i] = dSum;
      }
}

  /******************************************************************
   *                           SvbKsb()                             *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/12/02.                          *
   *                                                                *
   ******************************************************************/
   
void SvbKsb( double u[][NPRM], double w[], double v[][NPRM], int m, int n,
             double b[], double x[] )
{
   double  dS;
   double  dTemp[NMAX];
   int     i, j, jj;

   for (j=0; j<n; j++ )
   {
      dS = 0.;
      if ( w[j] != 0. )
      {
         for ( i=0; i<m; i++ ) dS += (u[i][j]*b[i]);
         dS = dS/w[j];
      }
      dTemp[j] = dS;
   }
   for (j=0; j<n; j++ )
   {
      dS = 0.;
      for ( jj=0; jj<n; jj++ ) dS += (v[j][jj]*dTemp[jj]);
      x[j] = dS;
   }
} 
