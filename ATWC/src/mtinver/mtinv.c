#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include "mtinver.h"

  /******************************************************************
   *                         MtInv()                                *
   *                                                                *
   *                                                                *
   *      deviatoric moment tensor inversion                        *
   *                                                                *
   *      Time function is fixed, so problem is linear              *
   *                                                                *
   *      Author:  Chuck Ammon, UC Santa Cruz, Saint Louis U.       *
   *               parts by George Randall, U South Carolina        *
   *                                                                *
   *      Version: 2.1 September, 1994                              *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/8/02.                           *
   *                                                                *
   *      Notation from Langston (1981)                             *
   *        x is north, y is east, z is down                        *
   *        all signs are opposite Aki and Richards due to a        *
   *        different choice of the direction for the fault normal  *
   *                                                                *
   ******************************************************************/

double  a[NPRM][MAXWVS*GF_TRACELENGTH];
double  asave[NPRM][MAXWVS*GF_TRACELENGTH];
int MtInv( MTRESULTS *MtR, MTSTUFF MtS[], int iNSta, int iNPts, int iDebug )
{
   double  ata[NPRM][NPRM];
   double  data[MAXWVS*GF_TRACELENGTH], datasv[MAXWVS*GF_TRACELENGTH],atb[NPRM];
   double  mtensor[6],  sval[NPRM], v[NPRM][NPRM], tsval[NPRM];
   double  covmatrix[NPRM][NPRM];
   double  mf[MAXWVS], tmpsig;
   double  misfit_tm, misfit_cb;
   double  err;
   double  factor, total_err, tmp1;
   double  dResponse[GF_TRACELENGTH];
   int     n, j, k;
   int     i, iCnt, nr, nc, ibegin, m, iTemp;

   ibegin = 1;
   
   if ( MtR->iAlign == 1 )
   {
      if ( iDebug > 0 )
         logit( "", "Seismograms are aligned with synthetics.\n"
                    "Length of window used in alignment = %lf\n",
                     MtR->dTWindow );
   }
   else
      if ( iDebug > 0 )
         logit( "", "Seismograms are not aligned with synthetics\n" );
   
/* Now set up the matrix equations. */
   for ( i=0; i<iNSta; i++ )
      if ( MtS[i].iUse == 1 )
      {
         if ( iDebug > 1 )
            logit( "", " Sum of Square Amplitudes: %s %le\n", MtS[i].szStation,
                                                              MtS[i].dSumSq );

/* SET-UP THE INVERSION
*
*  Compute the response for individual mt element and store in system matrix a
*/
         if ( ibegin > MAXWVS*iNPts )
         {
            logit( "", "ERROR: Overflowing the a matrix\n" );
            logit( "", "ibegin = %ld\n", ibegin );
            logit( "", "max allowed = %ld\n", MAXWVS*iNPts );
            return -1;
         }

/* A copy of the "a matrix" is saved 
*  for later computation of errors and predicted fits
*  a quick alternative is to just read in the seismograms
*  and build the A matrix again later
*/
         for ( m=1; m<=5; m++ )
         {
            GetMTResp( m, MtS[i].iWvType, MtS[i].azdelt.dAzimuth*DEG2RAD,
                       iNPts, MtS[i].dGFFiltVss, MtS[i].dGFFiltVds,
                       MtS[i].dGFFiltFfds, dResponse );
            AppendVector( &a[m-1][0], ibegin, dResponse, iNPts );
            AppendVector( &asave[m-1][0], ibegin, dResponse, iNPts );
         }

/* Next set up the data vector (save a copy here as well) */
         AppendVector( data, ibegin, &MtS[i].dDataFilt[0], iNPts );
         AppendVector( datasv, ibegin, &MtS[i].dDataFilt[0], iNPts );

/* Keep track of the number of rows in a matrix */
         ibegin += iNPts;        /* ibegin pts to the next row of matrix */
      }
	  
   if ( iDebug > 0 )
      logit( "", "\nUsing a total of %ld points in inversion.\n\n", ibegin-1 );
   
/*     END of SETUP *****************************************************
*     FORM THE NORMAL EQUATIONS
*     set up the number of rows in the a matrix
*/
   nr = ibegin - 1;

/*     for now, only 5 elements of moment tensor are estimated
*/
   nc = 5;

/*     set up the normal equations
*/  
   ATransA( a, nr, nc, ata );
   ATransB( a, nr, nc, data, atb );

/*     solve the normal equations using an SVD
*/
   SvdCmp( ata, NPRM, NPRM, sval, v );

   if ( iDebug > 1 )
   {
      logit( "", "Singular Values of A**t A:" );
      for (i=0; i<5; i++ ) logit( "", "%16.12lf ", sval[i] );
   }
      
   if ( iDebug > 1 )
      logit( "", "\n\nSingular Values of A:" );
   for (i=0; i<5; i++ )
   {
      tsval[i] = sqrt( sval[i] );
      if ( iDebug > 1 )
         logit( "", "%16.12lf ", tsval[i] );
   }

/* Solve the AtAx = Atb system */
   SvbKsb( ata, sval, v, NPRM, NPRM, atb, mtensor );
   mtensor[5] = (mtensor[0]+mtensor[1]) * -1.;

   if ( GMoment( mtensor, MtR, iDebug ) < 0 ) return -1;

/* Compute the Covariance Matrix */
   SvdVar( v, NPRM, tsval, covmatrix );
   if ( iDebug > 1 ) PrintCm( covmatrix, NPRM );

/* COMPUTE THE PREDICTED WAVEFORMS */
   total_err = 0.0;
   ibegin = 1;
   n = 0;

   if ( iDebug > 1 )
      logit( "", "\nfile,weight,sum_square_error,fractional_error,ls_scale_factor\n" );
   if ( iDebug > 1 )
      logit( "", "Station           norm      error     fit\n" );

   for ( i=0; i<iNSta; i++ ) 
      if ( MtS[i].iUse == 1 )
      {
         for ( j=0; j<iNPts; j++ ) MtS[i].dSynth[j] = 0;
         for ( m=1; m<=5; m++ )
            for ( j=0; j<iNPts; j++ ) 
            {
               k = ibegin + j - 1;
               MtS[i].dSynth[j] += (mtensor[m-1]*asave[m-1][k]);
            }

/* COMPUTE THE ERRORS (scale factor = 1.0, l2 norm) */
         err = 0.0;
         SumErr( &datasv[ibegin-1], MtS[i].dSynth, 1.0, iNPts, 2, &err );

/* WARNING!!!!!  There are several ways to compute the error or misfit between
*  the observed and synthetic seismograms.  This part of the program has been
*  changes from the original code of Ammon, so please beware.  I (HB?) have modified
*  the definition used to compute the misfit.  The formulation that I use is
*  misfit = ( data - syn )**2 / data**2, so if data = syn, misfit = 0;
*  if data = -syn, misfit = 4; if syn = 0, misfit = 1.  This is similar to
*  the error function of Pasyanos et al. (1996).  This is the error that is
*  passed through to the subroutine the prints the ASCII solution.
*  Ammon's error analysis is maintained and passed on to the subroutine 
*  HjpHext().
*/
         MtR->dMisFit[i] = (err * (double) iNPts / MtS[i].dSumSq);
         err *= (double) iNPts;
         total_err += err;
         n += iNPts;
 
/* If misfit > 1.0, do the inversion again, but don't use data from that
   station */
         if ( MtR->dMisFit[i] >= 1.0 )
         {
            if ( MtR->iAlign == 1 && MtR->iPass == 2 ) MtS[i].iUse = 0;
            if ( MtR->iAlign != 1 && MtR->iPass == 1 ) MtS[i].iUse = 0;
            if ( iDebug > 0 )
               logit( "", "%s MisFit > 1.0; Pass=%ld, Align=%ld, Use=%ld\n",
                      MtS[i].szStation, MtR->iPass, MtR->iAlign, MtS[i].iUse );
         }

/* Compute a scale factor for the seismogram fit; not used, just a statistic */
         ScaleFit( &datasv[ibegin-1], MtS[i].dSynth, iNPts, &factor );
         if ( iDebug > 1 )
            logit( "", "%ld %8.2e  %8.2e  %16.9e\n", i,
                   MtS[i].dSumSq, err, MtR->dMisFit[i] );

/* The value of sum_sq_amp(i) != 0, this is checked when first computed */
         if ( iDebug > 1 )
            logit( "", "(%s %16.9e %16.9e %16.9e) \n", MtS[i].szStation, err,
                   err/MtS[i].dSumSq, factor ); 
         ibegin += iNPts;
      }
   
/*      OUTPUT the error for all waveforms
*
*      output the sum of the square errors divided by the number of traces
*        minus the number of unknowns (free parameters)
*/
   tmpsig = 1.0 / (double) (n-6);
   if ( iDebug > 1 ) logit( "", "Unweighted SSE,Unweighted RMS\n" );
   if ( iDebug > 1 )
      logit( "", "%16.12lf %16.12lf\n", total_err*tmpsig, sqrt( total_err*tmpsig ) );

/*      Output the simple version of the error based on modified version of
*      Pasyanos et al. (1996)
*      Sort the misfit from low to high
*/
   iCnt = 0;
   for ( i=0; i<iNSta; i++ )
      if ( MtS[i].iUse == 1 )
      {
         mf[iCnt] = MtR->dMisFit[i];
         iCnt++;
      }
   for ( i=0; i<iCnt; i++ )
      for ( j=i+1; j<iCnt; j++ )
         if ( mf[j] < mf[i] )
         {
            tmp1 = mf[i];
            mf[i] = mf[j];
            mf[j] = tmp1;
         }

/* Compute the Median of the misfit */
   iTemp = (iCnt % 2);
   if ( iTemp != 0 ) 
   {
      n = iCnt/2 + 1;
      MtR->dMisFitMd = mf[n-1];
   }
   else
   {
      n = iCnt / 2;
      MtR->dMisFitMd = (mf[n-1] + mf[n]) / 2.0;
   }

/* Compute the average of the misfit */
   MtR->dMisFitAvg = 0.0;
   for ( i=0; i<iCnt; i++ ) MtR->dMisFitAvg += mf[i];
   MtR->dMisFitAvg = MtR->dMisFitAvg / (double) iCnt;

/* Compute the trimmed mean of the misfit */
   misfit_tm = 0.0;
   for ( i=1; i<iCnt-1; i++ )
      misfit_tm += mf[i];
   misfit_tm = misfit_tm / (double) (iCnt-2);

/* Compute the combined (median and mean) */
   misfit_cb = (MtR->dMisFitMd + MtR->dMisFitAvg) / 2.0;
   if ( iDebug > 0 )
   {
      logit( "", "\nSummary of the misfit of all seismograms\n" );
      logit( "", "Misfit: median, mean, trimmed mean, combined\n" );
      logit( "", "Total Error: %16.9e %16.9e %16.9e %16.9e %16.9e %5ld\n",
             MtR->dMisFitMd, MtR->dMisFitAvg, misfit_tm, misfit_cb,
             MtR->dError, iCnt );
   }

/* Perform the Hext analysis of HJP in preparation for plotting the
   error ellipses on focal sphere about the P/T/B axes */

/* Write out the waveform fits */
   HjpHext( mtensor, covmatrix, NPRM, total_err*tmpsig, iDebug );

/* Fill in azimuth gap */
   MtR->iAzGap = GetAzGap( iNSta, MtS );

   return 0;
}

/*******************************************************************
*
*  Find a simple LS scale factor for two vectors
*
*******************************************************************/
void ScaleFit( double obs[], double pred[], int npts, double *scale )
{     
   double  numerator, denominator;
   int     i;

   numerator = 0.0;
   denominator = 0.0;
   for ( i=0; i<npts; i++ )
   {              
      numerator += (obs[i]*pred[i]);
      denominator += (pred[i]*pred[i]);
   }   
   *scale = numerator / denominator;
}       
       
/*******************************************************************       
*
*      oz,or,ot - three-components observed
*      cz, cr,ct - three-components model
*      s = scale factor for the model waveforms
*      npts = number of points
*      etype = 1 L2 norm; etype = 2 L1 norm
*      err = output errors
*/
void SumErr( double obs[], double pred[], double s, int npts, int etype,
             double *err )
{
   int     ismp_l2, ismp_l1, i;
       
   ismp_l2 = 2;
   ismp_l1 = 1;       
   *err = 0.;
       
/* Simple L2 norm */
   if ( etype == ismp_l2 )
   {
      for ( i=0; i<npts; i++ )
          *err = *err + ((obs[i]-s*pred[i]) * (obs[i]-s*pred[i]));
      *err = *err / (double) npts;
   }      

/* Simple L1 norm */
   if ( etype == ismp_l1 )
   {       
      for ( i=0; i<npts; i++ )
         *err = *err + fabs( obs[i] - s*pred[i] );
      *err = *err / (double) npts;
   }      
}
       
/******************************************************************************
*
*     routine that combines the fundamental fault responses of Langston and
*         Helmberger into moment tensor responses
*
******************************************************************************/

void GetMTResp( int i, int iiWvType, double az, int npts, double dVssT[],
                double dVdsT[], double dFfdsT[], double dResponseT[] )
{       
   int     ixx, iyy, ixy, ixz, iyz, iPz, iPr, iSH, j;
   double  c2a, s2a, ca, sa;
              
   ixx = 1;
   iyy = 2;
   ixy = 3;
   ixz = 4;
   iyz = 5;      
   iPz = 1;
   iPr = 2;
   iSH = 3;  
   c2a = cos( 2.*az );
   s2a = sin( 2.*az );
   ca = cos( az );
   sa = sin( az );
      
   if ( iiWvType == iPz || iiWvType == iPr )
   {      
      if (i == ixx ) 
         for ( j=0; j<npts; j++ ) dResponseT[j] = 0.5 * (dFfdsT[j] - c2a*dVssT[j]);
      else if ( i == iyy )
         for ( j=0; j<npts; j++ ) dResponseT[j] = 0.5 * (c2a*dVssT[j] + dFfdsT[j]);
      else if ( i == ixy )
         for ( j=0; j<npts; j++ ) dResponseT[j] = -s2a * dVssT[j];
      else if ( i == ixz )
         for ( j=0; j<npts; j++ ) dResponseT[j] = ca * dVdsT[j];
      else if( i  == iyz )
         for ( j=0; j<npts; j++ ) dResponseT[j] = sa * dVdsT[j];
   }
   else if ( iiWvType == iSH )
   {      
      if ( i == ixx )
         for ( j=0; j<npts; j++ ) dResponseT[j] = 0.5*s2a*dVssT[j];
      else if ( i == iyy )
         for ( j=0; j<npts; j++ ) dResponseT[j] = -0.5*s2a*dVssT[j];
      else if ( i == ixy )
         for ( j=0; j<npts; j++ ) dResponseT[j] = -c2a*dVssT[j];
      else if ( i == ixz )
         for ( j=0; j<npts; j++ ) dResponseT[j] = -sa * dVdsT[j];
      else if ( i == iyz )
         for ( j=0; j<npts; j++ ) dResponseT[j] = ca * dVdsT[j];
   }
}
       
/*******************************************************************/      
void AppendVector( double outv[], int ibegin, double inv[], int npts )
{   
   int     i, k;

   for ( i=0; i<npts; i++ )
   {      
      k = i + ibegin - 1;
      outv[k] = inv[i];
   }
}
      
/*******************************************************************/
void ATransA( double a[][MAXWVS*GF_TRACELENGTH], int nr, int nc,
              double ata[][NPRM] )
{
   int     r, i, j;
   
   for ( i=0; i<nc; i++ )
      for ( j=0; j<nc; j++ )
      {
         ata[i][j] = 0.;
         for ( r=0; r<nr; r++ )
            ata[i][j] += (a[i][r]*a[j][r]);
      }
}
      
/*******************************************************************/  
void ATransB( double a[][MAXWVS*GF_TRACELENGTH], int nr, int nc, double b[],
              double atb[] )
{
   int     r, i;

   for ( i=0; i<nc; i++ )
   {      
      atb[i] = 0.;
      for ( r=0; r<nr; r++ )
         atb[i] += (a[i][r]*b[r]);
   }
}

/*******************************************************************/
void PrintCm( double cm[][NPRM], int n )
{
   int     i, j;

   logit( "", "Covariance Matrix\n" );      
   logit( "", "   xx          yy          xy         xz         yz\n" );      

   for ( i=0; i<n; i++ )            
   {
      for ( j=0; j<=i; j++ ) logit( "", "%10.3e ", cm[i][j] );
      logit( "", "\n" );
   }
}

/*******************************************************************
*
*     take the Mij and Cov(Mij,Mkl) tensors and perform the Hext (1963)
*     computations, being careful to reorder the input tensors into the
*     order used by Howard Patton's implementation of the Hext 
*
*******************************************************************/
void HjpHext( double mij[], double cm[][NPRM], int n, double sigsq, int iDebug )
{
   int     i, j, iCnt;
   int     ixx, iyy, ixy, ixz, iyz;
   int     hjpxx, hjpyy, hjpxy, hjpxz, hjpyz, hjpzz;
   int     hextxx, hextyy, hextxy, hextxz, hextyz, hextzz;
   double  dyncm, arg, az, dip;
   double  smt[6], e[3][3], delm[3], ev[9], esave[3][3];
   double  wroot[2][3], windx[2][3], wegnv[2][2][3];
   double  amajor[3], bminor[3], thknot[3], mcov[6][6];
              
/* Using 1-N 2-E 3-D */
   ixx = 0;
   iyy = 1;
   ixy = 2;
   ixz = 3;
   iyz = 4;
      
/*    Using 1-E 2-N 3-U
*     Re-arrange as
*     Mxx <-> Myy ;  -Mxz -> Myz ; -Myz -> Mxz ;  for the transformation
*/
   hjpxx = 0;
   hjpxy = 1;
   hjpyy = 2;
   hjpxz = 3;
   hjpyz = 4;
   hjpzz = 5;
      
   hextxx = 0;
   hextyy = 1;
   hextzz = 2;
   hextyz = 3;
   hextxz = 4;
   hextxy = 5;
   dyncm = 1.0;

/* Scale the unit covariance by the noise (misfit) variance) */
   for ( i=0; i<n; i++ )
      for ( j=0; j<n; j++ )
         cm[i][j] = cm[i][j] * sigsq;
      
   if ( iDebug > 1 )
   {
      logit( "", "\nCovariance Matrix\n" );
      logit( "", "   xx          yy          xy         xz         yz\n" );            
      for ( i=0; i<n; i++ )
      {
         for ( j=0; j<=i; j++ ) logit( "", "%10.3e ", cm[i][j] );
         logit( "", "\n" );		 
      }
   }

/* Copy the Mij into Howard's order, adding Mzz = -(Mxx+Myy) */
   smt[hjpxx] = mij[iyy];
   smt[hjpxy] = mij[ixy];
   smt[hjpyy] = mij[ixx];
   smt[hjpxz] = -mij[iyz];
   smt[hjpyz] = -mij[ixz];
   smt[hjpzz] = -(mij[ixx] + mij[iyy]);

/*     Copy the scaled Coavriance matrix into upper diagonal in Howard's order
*     and calculate Cov(Mzz,Mij) = -( Cov(Mxx,Mij) + Cov(Myy,Mij) )
*     with Cov(Mzz,Mzz) = Cov(-(Mxx+Myy),-(Mxx+Myy) ) 
*     Also need to account for the different Mij coordinate systems as above.
*/
   mcov[hextxx][hextxx] = cm[iyy][iyy];
   mcov[hextxx][hextyy] = cm[iyy][ixx];
   mcov[hextxx][hextzz] = -(cm[iyy][iyy] + cm[iyy][ixx]);
   mcov[hextxx][hextyz] = -cm[iyy][ixz];
   mcov[hextxx][hextxz] = -cm[iyy][iyz];
   mcov[hextxx][hextxy] = cm[iyy][ixy];
       
   mcov[hextyy][hextyy] = cm[ixx][ixx];
   mcov[hextyy][hextzz] = -(cm[ixx][ixx] + cm[ixx][iyy]);
   mcov[hextyy][hextyz] = -cm[ixx][ixz];
   mcov[hextyy][hextxz] = -cm[ixx][iyz];
   mcov[hextyy][hextxy] = cm[ixx][ixy];
      
   mcov[hextzz][hextzz] = cm[ixx][ixx] + cm[iyy][iyy] + 2.*cm[ixx][iyy];
   mcov[hextzz][hextyz] = -(-cm[ixx][ixz] - cm[iyy][ixz]);
   mcov[hextzz][hextxz] = -(-cm[ixx][iyz] - cm[iyy][iyz]);
   mcov[hextzz][hextxy] = -(cm[ixx][ixy] + cm[iyy][ixy]);

   mcov[hextyz][hextyz] = cm[ixz][ixz];
   mcov[hextyz][hextxz] = cm[ixz][iyz];
   mcov[hextyz][hextxy] = -cm[ixz][ixy];

   mcov[hextxz][hextxz] = cm[iyz][iyz];
   mcov[hextxz][hextxy] = -cm[iyz][ixy];
   mcov[hextxy][hextxy] = cm[ixy][ixy];

/* Fill in the mcov by symmetry */
   for ( i=0; i<5; i++ )
      for ( j=i+1; j<6; j++ )
         mcov[j][i] = mcov[i][j];

   Eigens( smt, ev, 3, 0 );
   iCnt = 0;
   for ( i=0; i<3; i++ )      /* Account for FORTRAN EQUIVALENCE statement */
      for ( j=0; j<3; j++ )   /* Fill as a C array - reversals accounted elsewhere */
      {
         e[i][j] = ev[iCnt];
         iCnt++;
      }

   Hext( smt, e, mcov, delm, wroot, wegnv, windx );

   smt[0] = smt[0]*dyncm;
   smt[1] = smt[2]*dyncm;
   smt[2] = smt[5]*dyncm;

   for ( i=0; i<3; i++ ) delm[i] *= dyncm;

   for ( i=0; i<3; i++ ) 
   {
      amajor[i] = 2.79*sqrt( wroot[0][i] );
      bminor[i] = 2.79*sqrt( wroot[1][i] );
      arg = wegnv[1][0][i] / wegnv[0][0][i];
      thknot[i] = atan( arg )/DEG2RAD;
      for ( j=0; j<3; j++ ) esave[i][j] = e[i][j];
   }

   for ( i=0; i<3; i++ ) 
   {
      AzDip( &e[i][0], &az, &dip );     /* e indices reversed for C */
      e[i][0] = az;
      e[i][1] = dip;
   }

   if ( iDebug > 1 )
   {
      logit( "", "\n          deviatoric tensor in diagonalized form:\n" );
      for ( i=0; i<3; i++ ) 
         logit( "", "\n          lambda %1ld: %10.2e +/- %8.2e dyn-cm az=%7.1lf deg e. of north dip=%5.1lf deg.\n",
                i, smt[i], delm[i], e[i][0], e[i][1] );

/* Write out eigenvector orientations to file */
      logit( "", "t: az=%7.1lf, dip=%5.1lf\n"
                 "b: az=%7.1lf, dip=%5.1lf\n"
                 "p: az=%7.1lf, dip=%5.1lf\n", e[0][0], e[0][1], e[1][0], e[1][1],
                                               e[2][0], e[2][1] );
      for ( i=0; i<3; i++ ) 
         logit( "", " %9.3e %9.3e %9.3e %9.3e %9.3e %9.3e\n",
                esave[i][0], esave[i][1], esave[i][2], amajor[i], bminor[i],
                thknot[i] );
   }
}

  /******************************************************************
   *                         GMoment()                              *
   *                                                                *
   *  gmoment - routine to rotate the moment tensor into its        *
   *            principle axes, find the greatest double couple     *
   *            component, and resulting orientation parameters     *
   *            for the two nodal planes.                           *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/8/02.                           *
   *                                                                *
   *  The following coordinate system is assumed:                   *
   *   1 -> east                                                    *
   *   2 -> north                                                   *
   *   3 -> up                                                      *
   *                                                                *
   *  rad(i) =>  i=1(m11);=2(m22);=3(m12);=4(m13);=5(m23);=6(m33)   *
   *                                                                *
   ******************************************************************/

int GMoment( double rad[], MTRESULTS *MtR, int iDebug )
{
   double  g[6], ev[5][3], v[3][3], p1[3], p2[3];
   double  m[3][3], md1, md3, merr, tv[3], tmin, tmax, plng, pl1;
   double  t1, t2, s2, az, plunge, temp, epsilon, c1, c2, az1, az2, pl2;
   double  dMomentMag, dScalarMoment, dm11, dm22, dm12, dm13, dm23;
   double  pazim, pplng, dIsotropic, tazim, tplng, rake, cd2sd1, rakt, err;
   double  cd1sd2, t3;
   int     nsiz, j;
   int     i;

/* Initialize some variables */
   t1 = 1.0e-05;
   t2 = 1.0e-04;
   nsiz = 3;
   s2 = 1.414213562;
   if ( iDebug > 1 )
      logit( "", "\n--------- From Subroutine Moment ---------------\n" );

/* Store the moment tensor in 2d array */
   m[0][0] = rad[0];
   m[1][1] = rad[1];
   m[2][2] = rad[5];
   m[0][1] = rad[2];
   m[1][0] = m[0][1];
   m[0][2] = rad[3];
   m[2][0] = m[0][2];
   m[1][2] = rad[4];
   m[2][1] = m[1][2];

   g[0] = -rad[5];
   g[1] = -rad[3];
   g[2] = -rad[0];
   g[3] = -rad[4];
   g[4] = -rad[2];
   g[5] = -rad[1];

/* Output the Input moment tensor */
   if ( iDebug > 1 )
   {
      logit( "", "Input moment tensor:\n" );
      for ( i=0; i<3; i++ )
         logit( "", " %10.3e     %10.3e     %10.3e     \n",
                  m[i][0], m[i][1], m[i][2] );
      logit( "", "m0 = %lE\n", MtR->dM0 );
   }

/* Remove the isotropic component */
   dIsotropic = 0.333333 * (rad[0]+rad[1]+rad[5]);
   if ( iDebug > 1 )
   {
      logit( "", "rad0: %le\n", rad[0] );
      logit( "", "rad1: %le\n", rad[1] );
      logit( "", "rad5: %le\n", rad[5] );
      logit( "", "\nIsotropic Component: %le\n", dIsotropic );
   }
   m[0][0] = rad[0] - dIsotropic;
   m[1][1] = rad[1] - dIsotropic;
   m[2][2] = rad[5] - dIsotropic;
   if ( iDebug > 1 ) logit( "", "Subtracted out the isotropic component.\n" );

/* Output the moment tensor */
   if ( iDebug > 1 )
   {
      logit( "", "\nThe Deviatoric Moment Tensor:\n" );
      for ( i=0; i<3; i++ )
         logit( "", " %10.3e     %10.3e     %10.3e     \n",
                m[i][0], m[i][1], m[i][2] );
   }

/* Compute the eigenvalues and eigenvectors of m */
   if ( HseHld( nsiz, m, v, ev ) < 0 )
   {
      logit( "", "Error in HseHld\n" );
      return -1;
   }
     
/* Make sure that the eigenvectors form a right handed
   coordinate system v3 = v1 x v2. */
   v[0][2] = v[1][0]*v[2][1] - v[2][0]*v[1][1];
   v[1][2] = v[2][0]*v[0][1] - v[0][0]*v[2][1];
   v[2][2] = v[0][0]*v[1][1] - v[1][0]*v[0][1];

/* Output the eigenvalues and eigenvectors of m */
   if ( iDebug > 1 )
   {
      logit( "", "\neigenvalues of the moment tensor" );
      logit( "", " %10.3e     %10.3e     %10.3e     \n",
               ev[0][0], ev[0][1], ev[0][2] );
      logit( "", "\nand associated eigenvectors (in the columns)\n" );
      for ( i=0; i<3; i++ )
         logit( "", " %10.3e     %10.3e     %10.3e     \n",
                  v[i][0], v[i][1], v[i][2] );
   }

/* Compute the azimuth and plunge */
   if ( iDebug > 1 ) logit( "", "\nEigenvector 1:\n" );
   tv[0] = v[0][0];
   tv[1] = v[1][0];
   tv[2] = v[2][0];
   GetAzPlunge( tv, &az, &plunge );
   if ( iDebug > 1 ) logit( "", "Az = %lf, Plunge = %lf\n", az, plunge );

   if ( iDebug > 1 ) logit( "", "\nEigenvector 2:\n" );
   tv[0] = v[0][1];
   tv[1] = v[1][1];
   tv[2] = v[2][1];
   GetAzPlunge( tv, &az, &plunge );
   if ( iDebug > 1 ) logit( "", "Az = %lf, Plunge = %lf\n", az, plunge );

   if ( iDebug > 1 ) logit( "", "\nEigenvector 3:\n" );
   tv[0] = v[0][2];
   tv[1] = v[1][2];
   tv[2] = v[2][2];
   GetAzPlunge( tv, &az, &plunge );
   if ( iDebug > 1 ) logit( "", "Az = %lf, Plunge = %lf\n", az, plunge );

/* Output the Scalar moment estimate of silver and jordan */
   temp = (dIsotropic+ev[0][0]) * (dIsotropic+ev[0][0]);
   temp = temp + (dIsotropic+ev[0][1]) * (dIsotropic+ev[0][1]);
   temp = temp + (dIsotropic+ev[0][2]) * (dIsotropic+ev[0][2]);
   dScalarMoment = sqrt( temp/2.0 )*MtR->dM0;
   dMomentMag = 0.6667*log10( dScalarMoment ) - 10.7;
   MtR->dScalarMom = dScalarMoment;
   MtR->dMag = dMomentMag;
   if ( iDebug > 1 )
   {
      logit( "", "\n\nThe Total Scalar Moment (Silver&Jordan,1982):\n" );
      logit( "", "Total Scalar Moment: %10.3e\n", dScalarMoment );
      logit( "", "\nThe Total Moment Magnitude (Kanamori(1979)):\n" );
      logit( "", "Total Moment Magnitude: %.1lf\n", dMomentMag );
   }

   temp = ev[0][0] * ev[0][0];
   temp = temp + ev[0][1]*ev[0][1];
   temp = temp + ev[0][2]*ev[0][2];
   dScalarMoment = sqrt( temp/2.0) * MtR->dM0;
   dMomentMag = 0.6667*log10( dScalarMoment ) - 10.7;
     
   if ( iDebug > 1 )
   {
      logit( "", "\nThe Deviatoric Scalar Moment (Silver&Jordan,1982):\n" );
      logit( "", "Deviatoric Scalar Moment: %10.3e\n", dScalarMoment );
      logit( "", "\nThe Deviatoric Moment Magnitude (Kanamori(1979)):\n" );
      logit( "", "Deviatoric Moment Magnitude: %.1lf\n", dMomentMag );
   }

/* Compute the percent clvd (relative to double couple) */
   md1 = (ev[0][0]/fabs( ev[0][0] )) * (fabs( ev[0][0] )+fabs( ev[0][2] ))/2.;
   md3 = (ev[0][2]/fabs( ev[0][2] )) * fabs( md1 );
   merr = fabs( ev[0][1]/md1 )*100.0;
      
   tmin = fabs( ev[0][0] );
   if ( fabs( ev[0][1] ) < tmin ) tmin = fabs( ev[0][1] );
   if ( fabs( ev[0][2] ) < tmin ) tmin = fabs( ev[0][2] );
      
   tmax = fabs( ev[0][0] );
   if ( fabs( ev[0][1] ) > tmax ) tmax = fabs( ev[0][1] );
   if ( fabs( ev[0][2] ) > tmax ) tmax = fabs( ev[0][2] );      
   epsilon = tmin / tmax;
      
   merr = 200 * epsilon;
   MtR->dError = merr;
      
   if ( iDebug > 1 )
   {
      logit( "", "\ndouble couple eigenvalues=%10.3e     %10.3e\n",
             md1, md3 );
      logit( "", "error (clvd/dc)*100=%10.3lf\n", merr );
   }

/* Compute the major double couple associated with m.
   Calculate moment tensor elements for dislocation assuming
   eigenvalues are +1, -1. - DM = V E V^T */
   dm11 = v[0][0]*v[0][0] - v[0][2]*v[0][2];
   dm22 = v[1][0]*v[1][0] - v[1][2]*v[1][2];
   dm12 = v[0][0]*v[1][0] - v[1][2]*v[0][2];
   dm13 = v[2][0]*v[0][0] - v[2][2]*v[0][2];
   dm23 = v[2][0]*v[1][0] - v[2][2]*v[1][2];
   if ( iDebug > 1 )
      logit( "", "\n Moment tensor for the major dislocation\n"
            " %10.4lf   %10.4lf   %10.4lf   %10.4lf   %10.4lf   \n", dm11,
            dm22, dm12, dm13, dm23 );

/* Rotate the pressure and tension axes to find poles of p nodal planes */
   p2[0] = (v[0][0]+v[0][2]) / s2;
   p2[1] = (v[1][0]+v[1][2]) / s2;
   p2[2] = (v[2][0]+v[2][2]) / s2;      
   p1[0] = (-v[0][0]+v[0][2]) / s2;
   p1[1] = (-v[1][0]+v[1][2]) / s2;
   p1[2] = (-v[2][0]+v[2][2]) / s2;
      
   c1 = 1.0;
   c2 = 1.0;
   if ( p1[2] < 0. ) c1 = -1.0;
   if ( p2[2] < 0. ) c2 = -1.0;
      
   for ( i=0; i<3; i++ )
   {
      p1[i] = p1[i] * c1;
      p2[i] = p2[i] * c2;
   }

   if ( iDebug > 1 )
   {
      logit( "", "Pole vectors p1 and p2\n" );
      logit( "", " %10.3e     %10.3e     %10.3e     \n",
               p1[0], p1[1], p1[2] );
      logit( "", " %10.3e     %10.3e     %10.3e     \n",
               p2[0], p2[1], p2[2] );
   }
      
/* Azimuth and plunge of pole 1 */
   if ( fabs( p1[0] ) > t1 ) az1 = atan2( p1[1], p1[0] );
   else      
   {
      if ( p1[1] >= 0. ) az1 = 1.570796;
      else               az1 = 4.712389;
   }
   
   if ( az1 < 0. ) az1 += (2.*PI);
   plng = sqrt( p1[0]*p1[0] + p1[1]*p1[1] );
   if ( plng <= t1 ) pl1 = PI/2.;
   else              pl1 = atan2( p1[2], plng );

   if ( iDebug > 1 )
      logit( "", "\n Azimuth and plunge of pole #1;    az1=%7.2lf"
                 "     pl1=%7.2lf\n", az1*RAD2DEG, pl1*RAD2DEG );
   pazim = az1 * RAD2DEG;
   pplng = pl1 * RAD2DEG;
      
/* Azimuth and plunge of pole #2 */
   if ( fabs( p2[0] ) <= t1 ) 
   {
      if ( p2[1] >= 0. ) az2 = PI/2.;
      if ( p2[1] <  0. ) az2 = 3.*PI/2.;      
   }
   else
      az2 = atan2( p2[1], p2[0] );
      
   if ( az2 < 0. ) az2 += 2.*PI;
   plng = sqrt( p2[0]*p2[0] + p2[1]*p2[1] );
   
   if ( plng <= t1 ) pl2 = PI/2.;
   else              pl2 = atan2( p2[2], plng );

   if ( iDebug > 1 )
      logit( "", "\n Azimuth and plunge of pole #2;    az2=%7.2lf"
                 "     pl2=%7.2lf\n", az2*RAD2DEG, pl2*RAD2DEG );
   tazim = az2 * RAD2DEG;
   tplng = pl2 * RAD2DEG;
      
/* Find the nodal plane angle parameters */
   MtR->dStrike1 = az1 - 3.*PI/2.;
   if ( MtR->dStrike1 < 0. ) MtR->dStrike1 += (2.*PI);
   MtR->dStrike2 = az2 - 3.*PI/2.;
   if ( MtR->dStrike2 < 0. ) MtR->dStrike2 += (2.*PI);
   MtR->dDip1 = PI/2. - pl1;
   MtR->dDip2 = PI/2. - pl2;

/* Find the rake of plane #1 */
   if ( MtR->dDip1 < t1 ) 
   {
      MtR->dRake1 = 0.;             /* Degenerate case when MtR->dDip1=0 */
      rake = v[2][0] + v[2][2];
      if ( rake > 0. ) MtR->dRake2 = -PI/2.;
      if ( rake < 0. ) MtR->dRake2 =  PI/2.;  
   }
   else
   {
      cd2sd1 = cos( MtR->dDip2 ) / sin( MtR->dDip1 );
      if ( fabs( cd2sd1 ) > 1.0 ) cd2sd1 = 1.0;
      rake = asin( cd2sd1 );
      merr = 1000.;
      for ( j=1; j<=4; j++ )
      {
         if ( j == 1 ) rakt = rake;
         if ( j == 2 ) rakt = -rake + PI;
         if ( j == 3 ) rakt =  rake + PI;
         if ( j == 4 ) rakt = -rake;
         MTest( MtR->dStrike1, MtR->dDip1, rakt, dm11, dm22, dm12, dm13, dm23,
                &err );
         if( err <= merr ) 
         {
            MtR->dRake1 = rakt;
            merr = err;
         }
      }

/* Find the rake of plane #2 */
      if ( MtR->dDip2 < t1 ) MtR->dRake2 = 0.;
      else
      {
         cd1sd2 = cos( MtR->dDip1 ) / sin( MtR->dDip2 );
         if ( fabs( cd1sd2 ) > 1.0 ) cd1sd2 = 1.0;
         rake = asin( cd1sd2 );
         merr = 1000.;
         for ( j=1; j<=4; j++ )
         {
            if ( j == 1 ) rakt = rake;
            if ( j == 2 ) rakt = -rake + PI;
            if ( j == 3 ) rakt = +rake + PI;
            if ( j == 4 ) rakt = -rake;
            MTest( MtR->dStrike2, MtR->dDip2, rakt, dm11, dm22, dm12, dm13,
                   dm23, &err );
            if ( err <= merr ) 
            {
               MtR->dRake2 = rakt;
               merr = err;
            }
         }
      }
   }
   
   if ( iDebug > 1 )
      logit( "", "\nSolution:  fault angle parameters"
                 "\nSolution:  strike      dip     rake\n" );
   t3 = 360.0/(2.*PI);
   MtR->dDip1 = MtR->dDip1 * t3;
   MtR->dRake1 = MtR->dRake1 * t3;
   MtR->dStrike1 = MtR->dStrike1 * t3;
   MtR->dDip2 = MtR->dDip2 * t3;
   MtR->dRake2 = MtR->dRake2 * t3;
   MtR->dStrike2 = MtR->dStrike2 * t3;
   if ( iDebug > 1 )
   {
      logit( "", "Solution: %7.2lf   %7.2lf   %7.2lf   \n",
          MtR->dStrike1, MtR->dDip1, MtR->dRake1 );
      logit( "", "Solution: %7.2lf   %7.2lf   %7.2lf   \n",
          MtR->dStrike2, MtR->dDip2, MtR->dRake2 );
      logit( "", "\n --------------------------------------------------\n" );
   }
   return 0;
}

/* ------------------------ MTest ---------------------------------

       this routine calculates the theoretical moment tensor expected
       from values of strike,dip,rake, and compares it with the
       observed.  rake is changed to rake + pi if the comparison
       fails.
*/
void MTest( double s, double d, double r, double dm11, double dm22, double dm12,
            double dm13, double dm23, double *err )
{
   double  tm11, tm22, tm12, tm13, tm23;

   tm11 = sin( s )*sin( s )*sin( r )*sin( 2.*d ) + sin( 2.*s )*cos( r )*sin( d );
   tm22 = cos( s )*cos( s )*sin( r )*sin( 2.*d ) - sin( 2.*s )*cos( r )*sin( d );
   tm12 = -0.5*sin( 2.*s )*sin( r )*sin( 2.*d ) - cos( 2.*s )*cos( r )*sin( d );
   tm13 = cos( s )*cos( r )*cos( d ) + sin( s )*sin( r )*cos( 2.*d );
   tm23 = -cos( s )*sin( r )*cos( 2.*d ) + sin( s )*cos( r )*cos( d );
  *err = (tm11-dm11)*(tm11-dm11) + (tm22-dm22)*(tm22-dm22) +
         (tm12-dm12)*(tm12-dm12) + (tm13-dm13)*(tm13-dm13) +
         (tm23-dm23)*(tm23-dm23);
  *err = sqrt( *err );
}

/* ------------------------ GetAzPlunge  ------------------------------

     v(0) = east    v(1) = north   v(2) = down
*/

void GetAzPlunge( double v[], double *az, double *plunge )
{
   double    north, south, east, west;
   double    vsmall, az1, r, pl1, temp;

   north = 0.;
   south = PI;
   east = PI/2.;
   west = 3*PI/2.;
   vsmall = 1.0e-06;

   if ( fabs( v[1] ) <= vsmall )
   {
      if ( fabs( v[0] ) > vsmall )
      {
          if ( v[0] >= 0.0 ) az1 = east;
          if ( v[0] <  0.0 ) az1 = west;
      }
      else
      {
          az1 = 0.0;
      }
   }
   else 
   {
      if ( fabs( v[0] ) <= vsmall )
      {
	     if ( v[1] >= 0.0 ) az1 = north;
	     if ( v[1] <  0.0 ) az1 = south;
      }
      else
         az1 = PI/2. - atan2( v[0], v[1] );
   }
   if ( az1 < 0. ) az1 += (PI*2.);

/* Compute the plunge */
   r = sqrt( v[0]*v[0] + v[1]*v[1] );

   if ( fabs( v[2] ) <= vsmall ) pl1 = 0.0;
   else
   {
      if ( r < vsmall )
      {
         if ( v[2] > 0 ) pl1 = -PI/2.;
         if ( v[2] < 0 ) pl1 = PI/2.;
      }
      else
      {
          temp = v[2];
          pl1 = atan2( temp, r );
      }
   }

/* Convert to degrees */
   *az = az1 * RAD2DEG;
   if ( *az > 360.0 ) *az -= 360.0;
   *plunge = pl1 * RAD2DEG;

   if ( *plunge < 0.0 )
   {
      *plunge *= -1.;
      *az += 180.;
      if ( *az > 360. ) *az -= 360.;
   }
   if ( fabs( *plunge ) > 90.0 )
   {
      if ( *plunge > 90.0 )
      {
         *plunge -= 90.0;
         *az += 180.0;
         if ( *az > 360.0 ) *az -= 360.0;
      }
      else if ( *plunge < -90.0 )
      {
         *plunge += 90.0;
         *az += 180.0;
         if ( *az > 360.0 ) *az -= 360.0;
      }
   }
}

      /******************************************************************
       *                       GetAzGap()                               *
       *                                                                *
       * This function sorts the quake azimuthal values.  This makes it *
       * easier to compute the azimuthal coverage of stations about the *
       * epicenter.                                                     *
       *                                                                *
       *  Arguments:                                                    *
       *   iNSta            Number of stations in this structure        *
       *   MtS              MtStuff structure                           *
       *                                                                *
       *  Return:                                                       *
       *   int - azimuthal gap in coverage about epicenter              *
       *                                                                *
       ******************************************************************/
       
int GetAzGap( int iNSta, MTSTUFF MtS[] )
{
   double  dAz[MAX_STATION_DATA];  /* Azimuths of stations used in soln */
   double  dAzGap;
   double  dTemp, dTempAz;
   int     i, ii, j;
   int     iGoodPicks;             /* Number of stations used in loc. */

/* Put good azimuths into array */
   ii = 0;
   for ( i=0; i<iNSta; i++ )
      if ( MtS[i].iUse == 1 )
      {
         dAz[ii] = MtS[i].azdelt.dAzimuth;
		 ii++;
      }
   iGoodPicks = ii;
	  
/* Sort the array */
   for ( i=0; i<iGoodPicks-1; i++ )
   {
      ii = i+1;
      for ( j=ii; j<iGoodPicks; j++ )
         if ( dAz[i] > dAz[j] )
         {
            dTemp = dAz[i];
            dAz[i] = dAz[j];
            dAz[j] = dTemp;
         }
   }
   
/* Compute coverage from sorted array */
   dAzGap = 0.0;
   ii = 0;
   for ( i=0; i<iGoodPicks-1; i++ )
   {
      dTempAz = dAz[i+1] - dAz[i];
      if ( dTempAz > dAzGap ) dAzGap = dTempAz;
   }
   
/* Return the coverage */
   dTempAz = 360. - (dAz[iGoodPicks-1] - dAz[0]);
   if (dTempAz > dAzGap) return( (int) (dTempAz) );
   else                  return( (int) (dAzGap) );
}

