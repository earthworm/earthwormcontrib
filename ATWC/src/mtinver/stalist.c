#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <earthworm.h>
#include <transport.h>
#include <trace_buf.h>
#include "mtinver.h"

  /***************************************************************
   *                         GetStaList()                        *
   *                                                             *
   *                     Read the station list                   *
   *                                                             *
   *  Returns -1 if an error is encountered.                     *
   ***************************************************************/

int GetStaList( STATION **Sta, int *Nsta, GPARM *Gparm, MTSTUFF **MtT )
{
   double  dTemp;                 /* Values in file, not needed here */
   char    string[180];
   int     i;
   int     iReturn;
   int     ndecoded;
   int     nsta;
   MTSTUFF *Mttemp;
   STATION *sta;
   STATION StaTemp;
   FILE    *fp;

/* Open the station list file
   **************************/
   if ( ( fp = fopen( Gparm->StaFile, "r") ) == NULL )
   {
      logit( "et", "mtinver: Error opening station list file <%s>.\n",
             Gparm->StaFile );
      return -1;
   }

/* Count channels in the station file.
   Ignore comment lines and lines consisting of all whitespace.
   ************************************************************/
   nsta = 0;
   while ( fgets( string, 160, fp ) != NULL )
   {
      if ( IsComment( string ) ) continue;
      ndecoded = sscanf( string,"%s %s %s %d %d %d %d %lf %lf %lf %d %lf %lf\n",
                 StaTemp.szStation, StaTemp.szChannel, StaTemp.szNetID,
                 &StaTemp.iPickStatus, &StaTemp.iFiltStatus, 
                 &StaTemp.iSignalToNoise, &StaTemp.iAlarmStatus, 
                 &StaTemp.dAlarmAmp, &StaTemp.dAlarmDur,
                 &StaTemp.dAlarmMinFreq, &StaTemp.iComputeMwp, &dTemp, &dTemp );
      if ( ndecoded < 13 )
      {
         logit( "et", "mtinver: Error decoding station file. - 1\n" );
         logit( "e", "ndecoded: %d\n", ndecoded );
         logit( "e", "Offending line:\n" );
         logit( "e", "%s\n", string );
         return -1;
      }
      nsta++;
   }
   rewind( fp );

/* Allocate the station list and moment tensor station array
   *********************************************************/
   if ( nsta > MAX_STATIONS )
   {
      logit( "et", "Too many stations in Station file\n" );
      return -1;
   }   
   sta = (STATION *) calloc( nsta, sizeof(STATION) );
   if ( sta == NULL )
   {
      logit( "et", "mtinver: Cannot allocate the station array\n" );
      return -1;
   }
   Mttemp = (MTSTUFF *) calloc( nsta, sizeof(MTSTUFF) );
   if ( Mttemp == NULL )
   {
      logit( "et", "mtinver: Cannot allocate the response array\n" );
      return -1;
   }

/* Read stations from the station list file into the station array.
   ****************************************************************/
   i = 0;
   while ( fgets( string, 160, fp ) != NULL )
   {
      if ( IsComment( string ) ) continue;
      ndecoded = sscanf( string,"%s %s %s %d %d %d %d %lf %lf %lf %d %lf %lf\n",
                 sta[i].szStation, sta[i].szChannel, sta[i].szNetID,
                 &sta[i].iPickStatus, &sta[i].iFiltStatus, 
                 &sta[i].iSignalToNoise, &sta[i].iAlarmStatus, 
                 &sta[i].dAlarmAmp, &sta[i].dAlarmDur,
                 &sta[i].dAlarmMinFreq, &sta[i].iComputeMwp, &dTemp,
                 &sta[i].dScaleFactor );
      if ( ndecoded < 13 )
      {
         logit( "et", "mtinver: Error decoding station file.\n" );
         logit( "e", "ndecoded: %d\n", ndecoded );
         logit( "e", "Offending line:\n" );
         logit( "e", "%s\n", string );
         return -1;
      }
      if ( LoadStationData( &sta[i], Gparm->StaDataFile ) == -1 )
      {
         logit( "et", "mtinver: No match for scn in station info file.\n" );
         logit( "e", "file: %s\n", Gparm->StaDataFile );
         logit( "e", "scn = %s %s %s\n", sta[i].szStation, sta[i].szChannel,
		        sta[i].szNetID );
         return -1;
      }
      iReturn = LoadResponseData( &sta[i], &Mttemp[i], Gparm->ResponseFile );
      if ( iReturn == -1 )
      {
         logit( "e", "file: %s\n", Gparm->ResponseFile );
         logit( "e", "scn = %s %s %s\n", sta[i].szStation, sta[i].szChannel,
		        sta[i].szNetID );
         return -1;
      }
      else if ( iReturn == 0 )
         logit( "", "scn = %s %s %s - No RESPONSE info\n", sta[i].szStation,
                sta[i].szChannel, sta[i].szNetID );
      else if ( iReturn == 1 )
         logit( "", "scn = %s %s %s - RESPONSE info read\n", sta[i].szStation,
                sta[i].szChannel, sta[i].szNetID );
      InitVar( &sta[i] );	  
      sta[i].dEndTime = 0.;
      sta[i].iFirst = 1;
      i++;
   }
   fclose( fp );
   *Sta  = sta;
   *Nsta = nsta;
   *MtT = Mttemp;
   return 0;
}

  /***************************************************************
   *                       LoadResponseData()                    *
   *                                                             *
   *  Read response information in poles/zeroes form.            *
   *                                                             *
   *  Returns -1 if an error is encountered, 0 if no match found.*
   ***************************************************************/

int LoadResponseData( STATION *Sta, MTSTUFF *MtT, char *pszInfoFile )
{
   double  dTemp; 
   FILE    *hFile;
   int     i, iTemp, j;
   int     iUse;               /* Flag indicating whether or not to use */
   MTSTUFF Mttemp;             /* Temp MTSTUFF array */
   char    szLine[128];

/* First, intialize information in the response structure
   ******************************************************/
   strcpy( MtT->szStation, Sta->szStation );
   strcpy( MtT->szChannel, Sta->szChannel );
   strcpy( MtT->szNetID, Sta->szNetID );
   MtT->dAmp0 = 0.;
   MtT->iNZero = 0;
   MtT->iNPole = 0;
   for ( i=0; i<MAX_ZP; i++ )
   {
      MtT->zPoles[i] = Complex( 0., 0. );
      MtT->zZeros[i] = Complex( 0., 0. );   
   }
         
/* Open the response file
   **********************/
   if ( (hFile = fopen( pszInfoFile, "r")) == NULL )
   {
      logit( "et", "mtinver: Error opening response file <%s>.\n",
             pszInfoFile );
      return -1;
   }

/* Read response data from the response file 
   *****************************************/
   while ( !feof( hFile ) )
   {
      if ( fgets( szLine, 126, hFile ) == NULL ) break;
      sscanf( szLine, "%*s %s %s %*s %*s %*lf %*lf %*lf %*s %*s %ld",
              Mttemp.szStation, Mttemp.szChannel, &iUse );	  
      if ( fgets( szLine, 126, hFile ) == NULL ) break;
      sscanf( szLine, "%lf %lf %ld", &dTemp, &dTemp, &iTemp );
      if ( fgets( szLine, 126, hFile ) == NULL ) break;
      sscanf( szLine, "%*s %*d %*c %lf %d %d", &Mttemp.dAmp0, &Mttemp.iNPole,
              &Mttemp.iNZero);			  
      for ( j=0; j<Mttemp.iNPole; j++ )
      {
         if ( fgets( szLine, 126, hFile ) == NULL ) break;
         sscanf( szLine, "%e %e", &Mttemp.zPoles[j].r, &Mttemp.zPoles[j].i );
      }
      for ( j=0; j<Mttemp.iNZero; j++ )
      {
         if ( fgets( szLine, 126, hFile ) == NULL ) break;
         sscanf( szLine, "%e %e", &Mttemp.zZeros[j].r, &Mttemp.zZeros[j].i );
      }
      Mttemp.dAmp0 *= 1.0e9;       /* To match Benz's cal file */
	  if ( !strcmp( Mttemp.szStation, Sta->szStation ) &&
	       !strcmp( Mttemp.szChannel, Sta->szChannel ) && iUse == 1 )
      {
         MtT->dAmp0 = Mttemp.dAmp0;
         MtT->iNPole = Mttemp.iNPole;
         MtT->iNZero = Mttemp.iNZero;
         for ( j=0; j<Mttemp.iNPole; j++ )
         {
            MtT->zPoles[j].r = Mttemp.zPoles[j].r;
            MtT->zPoles[j].i = Mttemp.zPoles[j].i;
         }
         for ( j=0; j<Mttemp.iNZero; j++ )
         {
            MtT->zZeros[j].r = Mttemp.zZeros[j].r;
            MtT->zZeros[j].i = Mttemp.zZeros[j].i;
         }
         fclose( hFile );
         return 1;
      }
      if ( fgets( szLine, 126, hFile ) == NULL ) break;
   }	  
   fclose( hFile );
   return 0;
}

  /***************************************************************
   *                       LoadStationData()                     *
   *                                                             *
   *       Get data on station from information file             *
   *                                                             *
   *  Returns -1 if an error is encountered or no match is found.*
   ***************************************************************/

int LoadStationData( STATION *Sta, char *pszInfoFile )
{
   char    string[180];
   int     ndecoded;
   FILE    *hFile;
   char    szChannel[TRACE_CHAN_LEN], szStation[TRACE_STA_LEN],
           szNetID[TRACE_NET_LEN];

/* Open the station data file
   **************************/
   if ( ( hFile = fopen( pszInfoFile, "r") ) == NULL )
   {
      logit( "et", "mtinver: Error opening station data file <%s>.\n",
             pszInfoFile );
      return -1;
   }

/* Read station data from the station data file 
   ********************************************/
   while ( fgets( string, 160, hFile ) != NULL )
   {
      ndecoded = sscanf( string, "%s %s %s", szStation, szChannel,
	             szNetID);
      if ( ndecoded < 3 )
      {
         logit( "et", "mtinver: Error decoding station data file.\n" );
         logit( "e", "ndecoded: %d\n", ndecoded );
         logit( "e", "Offending line:\n" );
         logit( "e", "%s\n", string );
         fclose( hFile );
         return -1;
      }
	  
/* Compare SCNs
   ************/	  
	  if ( !strcmp (szStation, Sta->szStation ) &&
	       !strcmp (szChannel, Sta->szChannel ) &&
	       !strcmp (szNetID, Sta->szNetID ) )
      {     /* We have a match */
         ndecoded = sscanf( string, "%s %s %s %lf %lf %lf %lf %lf %lf %lf %d",
                    Sta->szStation, Sta->szChannel, Sta->szNetID,
                    &Sta->dSens, &Sta->dGainCalibration, 
                    &Sta->dLat, &Sta->dLon, &Sta->dElevation,
                    &Sta->dClipLevel, &Sta->dTimeCorrection,					
                    &Sta->iStationType);
         if ( ndecoded < 11 )
         {
            logit( "et", "mtinver: Error decoding station data file-2.\n" );
            logit( "e", "ndecoded: %d\n", ndecoded );
            logit( "e", "Offending line:\n" );
            logit( "e", "%s\n", string );
            fclose( hFile );
            return -1;
         }
         fclose( hFile );
         return 0;
      }
   }
   fclose( hFile );
   return -1;
}

 /***********************************************************************
  *                             LogStaList()                            *
  *                                                                     *
  *                         Log the station list                        *
  ***********************************************************************/

void LogStaList( STATION *Sta, int Nsta )
{
   int i;

   logit( "", "\nStation List:\n" );
   for ( i = 0; i < Nsta; i++ )
   {
      logit( "", "%4s",     Sta[i].szStation );
      logit( "", " %3s",    Sta[i].szChannel );
      logit( "", " %2s\n",    Sta[i].szNetID );
   }
   logit( "", "\n" );
}

    /*************************************************************************
     *                             IsComment()                               *
     *                                                                       *
     *  Accepts: String containing one line from a mtinver station list      *
     *  Returns: 1 if it's a comment line                                    *
     *           0 if it's not a comment line                                *
     *************************************************************************/

int IsComment( char string[] )
{
   int i;

   for ( i=0; i<(int)strlen( string ); i++ )
   {
      char test = string[i];

      if ( test!=' ' && test!='\t' && test!='\n' )
      {
         if ( test == '#'  )
            return 1;          /* It's a comment line */
         else
            return 0;          /* It's not a comment line */
      }
   }
   return 1;                   /* It contains only whitespace */
}
