/******************************************************************
 *                         File mtinver.h                         *
 *                                                                *
 *  Include file for mtinver module used at the West Coast/       *
 *  Alaska Tsunami Warning Center.                                *
 ******************************************************************/
 
#include <trace_buf.h>
#include <transfer.h>
#include "\earthworm\atwc\src\libsrc\earlybirdlib.h"

/* Definitions
   ***********/
#define DEG2RAD      PI/180.
#define RAD2DEG      180./PI
#define NMAX         514          /* Must be greater than NPRM */
#define MMAX         20           /* Must be greater than NPRM */
#define NPRM         5
#define MAXMAT       6            /* Max matrix size */
#define NSIZE        3            /* Matrix size for HseHld */
#define NSP          5            /* # of spectral periods to eval in gma */
#define MIN_WAVEFORMS 5           /* Minimum # of waveforms before inversion */
#define NUM_GF_DEPTHS        38   /* # depth levels for Green's functions */
#define NUM_GF_DISTS         88   /* # distance levels for Green's functions */
#define NUM_GFS NUM_GF_DEPTHS*NUM_GF_DISTS*3 /* Total number of Green's funcs */
#define DECON_BUFFER MTBUFFER_SIZE*2 /* Buffer length of deconvolution output */								  
#define GF_ORIG_TRACELENGTH 1024  /* # samples in original GF files */
#define GAP_SIZE              3   /* Max allowable data gap in seconds */
#define SIGNOISE             5.0  /* Signal-to-noise level to exceed */
#define P_LENGTH             20   /* P length (seconds) for S:N comparison */
#define CHECK_INTERVAL    10000   /* Check for auto-compute interval in ms */
#define COMPUTE_INTERVAL  60000   /* Auto-compute time interval in mseconds */

#define IDM_EXIT_MAIN       115
#define IDM_REFRESH         140	  /* Refresh screen */
#define IDM_SHOW_GFS        151	  /* Show Green's Functions */
#define IDM_SHOW_SYNTHS     152	  /* Show Synthetics and data*/
#define IDM_STOP_LP         170   /* Stop moment tensor computations */
#define IDM_COMPUTE_MOMENT  180   /* Compute moment tensors */
#define ID_TIMER              1   /* Check Auto-Compute timer */   
#define ID_TIMER2             2   /* Auto-Compute timer */   

/* Dialog box definitions */
#define EF_GFDIST           301   /* Epicentral Distance in degrees */
#define EF_GFDEP            302   /* Hypocentral depth in km */

#define EF_TWIN             310   /* Comparison time window (s) */
#define EF_DEPTH            311   /* Quake depth specification (KM) */
#define EF_TLAG             312   /* Maximum time adjustment(s) */
#define EF_FILTLO           313   /* Long period bandpass cutoff (s) */
#define EF_FILTHI           314   /* Short period bandpass cutoff (s) */

#define ID_NULL            -1     /* Resource file declaration */

typedef struct {
   int    iPrePTime;       /* Start time of function (before expected P time) */
   int    iDepth;          /* Depth (km) for this Green's function */
   double dDistance;       /* Distance (km) for this Green's function */
   double dSR;             /* Sample rate (samp/sec) for this function */
   double dMax;            /* GF maximum value */
   double dMin;            /* GF minimum value */
   int    iFini;           /* SAC header Fini index (end of P (?)) */
   char   szComp[8];       /* Component (clv, vss, vds) for this function */
   float  fGF[GF_TRACELENGTH];     /* Green's function computed signal */
} GF;

typedef struct {
   char StaFile[64];              /* Name of file with SCN info */
   char StaDataFile[64];          /* Station information file */
   char ATPLineupFileLP[64];      /* Optional command when used with ATPlayer */
   long InKey;                    /* Key to ring where waveforms live */
   long HKey;                     /* Key to ring where hypocenters will live */
   int  HeartbeatInt;             /* Heartbeat interval in seconds */
   int  Debug;                    /* If 1, print debug messages */
   unsigned char MyModId;         /* Module id of this program */
   int  MinutesInBuff;            /* Number of minutes data to save per trace */
   int  Align;                    /* 1->Alig synthetics and waveforms */
   int  LPFileSize;               /* # of minutes LP seismic data per file */
   int  ReadTime;                 /* Minutes data to read from disk */
   double GFSampRate;             /* Green's function sample rates (samples/sec) */
   double TimeWindow;             /* Window length of seismograms (seconds) */
   double MaxTLag;                /* Max time shift in wave alignment */
   char FileSuffix[8];            /* LP File suffix (disk/archive data) */
   char DummyFile[64];            /* Hypocenter parameter disk file */
   char ResponseFile[64];         /* Broadband station response file */
   char MTFile[64];               /* Moment tensor results file */
   char EmailFile[64];            /* Moment tensor results file */
   char DepthFile[64];            /* Max depth expected for regions */
   char GFDir[64];                /* Green's function directory */
   char EBPTimeFile[64];          /* P-time file from LOCATE */
   char RootDirectory[64];        /* Disk directory in which to find data */
   char ArchDirectory[64];        /* Archive directory in which to find data */
   char szLocFilePath[128];       /* Path to log the LOC files */
   SHM_INFO InRegion;             /* Info structure for input region */
   SHM_INFO HRegion;              /* Info structure for Hypocenter region */
} GPARM;

typedef struct {
   unsigned char MyInstId;        /* Local installation */
   unsigned char GetThisInstId;   /* Get messages from this inst id */
   unsigned char GetThisModId;    /* Get messages from this module */
   unsigned char TypeHeartBeat;   /* Heartbeat message id */
   unsigned char TypeError;       /* Error message id */
   unsigned char TypeWaveform;    /* Earthworm waveform messages */
   unsigned char TypeHypoTWC;     /* Hypocenter message - TWC format*/
} EWH;

/* Function declarations for mtinver
   *********************************/
thr_ret CheckLocThread( void * );
int     GetEwh( EWH * );
void    GetLDC( long, long *, double *, long );
thr_ret HThread( void * );
void    PadBuffer( long, double, long *, long *, long );
int     PatchDummyWithMw( HYPO *, char * );
void    PutDataInBuffer( TRACE_HEADER *, long *, long *, long *, long );
long WINAPI WndProc( HWND, UINT, UINT, long );
thr_ret WThread( void * );
   
void    AHat( double [], double [], double [][1] );
int     AlignWf( double, int, double, int, MTSTUFF [], int );
int     AlignWf2( double, double, int, MTSTUFF [], int, int );
void    AppendVector( double [], int, double [], int );
void    ATransA( double [][MAXWVS*GF_TRACELENGTH], int, int, double [][NPRM] );
void    ATransB( double [][MAXWVS*GF_TRACELENGTH], int, int, double [],
                 double [] );
void    AzDip( double [], double *, double * );
void    CosTaper( float *, long, double, int );   
void    CrsCor( double [], double [], int, int, int, char [], double [],
                int *, char [] );
void    Eigens( double [], double [], int, int );
void    EmailOutput( char *, HYPO *, MTRESULTS, int, int, MTSTUFF [], int, int );
void    Fft( double [], double [], int, int );
void    FirTrn( char *, double [], int, double [], double [] );
int     GetAzGap( int, MTSTUFF [] );
void    GetAzPlunge( double [], double *, double * );
void    GetMTResp( int, int, double, int, double [], double [], double [],
                   double [] );
int     GMoment( double [], MTRESULTS *, int );
void    Hext( double [], double [][3], double [][6], double [],
              double [][3], double [][2][3], double [][3] );
void    HjpHext( double [], double [][NPRM], int, double, int );
int     HseHld( int, double [][NSIZE], double [][NSIZE], double [][3] );
void    MTest( double, double, double, double, double, double, double,
               double, double * );
int     MtInv( MTRESULTS *, MTSTUFF [], int, int, int );
void    Mult1( double [][1], int, double [][MAXMAT], int, double [][MAXMAT],
               int, int, int, int );
void    Mult2( double [][MAXMAT], int, double [][1], int, double [][1],
               int, int, int, int );
void    Mult3( double [][2], int, double [][MAXMAT], int, double [][MAXMAT],
               int, int, int, int );
void    Mult4( double [][MAXMAT], int, double [][2], int, double [][2],
               int, int, int, int );
void    OverLp( double [], int, double [], double [], int, int, double [],
                double [] );
void    PrintCm( double [][NPRM], int );
void    ReadInput( void );
double  Rms( double [], int );
void    ScaleFit( double [], double [], int, double * );
int     Shift_wf( double [], double [], int, double, double * );
void    SortD2( double *, int, int [] );
void    SumErr( double [], double [], double, int, int, double * );
void    SvdCmp( double [][NPRM], int, int, double [], double [][NPRM] );
void    SvbKsb( double [][NPRM], double [], double [][NPRM], int, int,
                double [], double [] );                       
void    SvdVar( double [][NPRM], int, double [], double [][NPRM] );
void    TextBeach( double, double, double, int, int, FILE *, int ); 
void    TimeShift( double [], double [], int, double, double *, double );
void    Window( double [], int, char *, int, int, double [], char * );
void    ZShft( double [], int, int );

int     GetConfig( char *, GPARM * );                        /* config.c */
void    LogConfig( GPARM * );

int     GetStaList( STATION **, int *, GPARM *, MTSTUFF ** );/* stalist.c */
int     IsComment( char [] );
int     LoadStationData( STATION *, char * );
int     LoadResponseData( STATION *, MTSTUFF *, char * );
void    LogStaList( STATION *, int );

int     CheckDataForDCandGaps( long, double *, STATION * );  /* prepp.c */
int     CheckDataForSN( long, double *, STATION *, MTSTUFF *, double * );
void    CheckDataForSSAmp( double *, STATION *, MTSTUFF *, int );
int     DeconvolveWF( double *, long, double, ResponseStruct *,
                      ResponseStruct *, double *, double *, long );
void    FillMtDataBuff( STATION *, MTSTUFF *, long *, double * );
int     FillMtDataBuffSolo( STATION *, MTSTUFF *, long *, double *, double );
int     FillResponse( ResponseStruct *, MTSTUFF * );
void    InitMtR( MTRESULTS *, GPARM * );
void    InitMtStructure( MTSTUFF * );

int     ReadGreensFunctions( GF *, char * );                 /* readGF.c */
long    WINAPI GFDisplayDlgProc( HWND, UINT, UINT, long );
void    FindGFIndices( GF [], double, int, int [] );

void    DisplayGFs( HDC, GF *, long, long, int, int, int );  /* display.c */
void    DisplaySoln( HDC, MTSTUFF *, char *, long, long, int, int, int );
void    DisplaySynths( HDC, MTSTUFF *, long, long, int, int, int, int, int );
long    WINAPI InvParamsDlgProc( HWND, UINT, UINT, long );

