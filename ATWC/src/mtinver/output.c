#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <transport.h>
#include "mtinver.h"

  /******************************************************************
   *                     EmailOutput()                              *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/26/02.                          *
   *                                                                *
   ******************************************************************/

void EmailOutput( char *szOutFile, HYPO *HypoT, MTRESULTS MtR,
                  int iNWave, int iNSta, MTSTUFF MtS[], int iDepth, int iDebug )
{
   int     iProj, iNChar, i;
   int     iFERegion;                    /* Flinn-Engdahl Region number */
   LATLON  ll;
   char    *psz;                         /* Epicentral region */
   char    cNS, cEW, szTemp[12], szBuffer[256];
   FILE    *hFile;
   long    lTime;                        /* 1/1/70 time */
   struct  tm *tm;                       /* time structure */
        
   iNChar = 40;
   iProj = 1;
   
   cEW = 'E';                             /*Put lat/lon in form for output */
   cNS = 'N';
   GeoGraphic( &ll, (LATLON *) HypoT );
   while ( ll.dLon > 180. ) ll.dLon -= 360.;
   psz = namnum( ll.dLat, ll.dLon, &iFERegion );
   if (ll.dLon < 0.0 )
   {
      ll.dLon = fabs (ll.dLon);
      cEW = 'W';
   }
   if (ll.dLat < 0.0)
   {
      ll.dLat = fabs (ll.dLat);
      cNS = 'S';
   }
   
   if ( (hFile = fopen( szOutFile, "w" )) != NULL )
   {
      fprintf( hFile, "Mw=%.1lf  %s\n", MtR.dMag, psz );
      strcpy( szBuffer, "\0" );
      lTime = (long) (HypoT->dOriginTime+0.5);
      tm = TWCgmtime( lTime );
      itoaX( tm->tm_year+1900, szTemp );
      PadZeroes( 4, szTemp );
      strcpy( szBuffer, szTemp );                            /* Year */
      strcat( szBuffer, "/" );
      itoaX( tm->tm_mon+1, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Month */
      strcat( szBuffer, "/" );
      itoaX( tm->tm_mday, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Day */
      strcat( szBuffer, "  " );
      itoaX( tm->tm_hour, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Hour */
      strcat( szBuffer, ":" );
      itoaX( tm->tm_min, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Minute */
      strcat( szBuffer, ":" );
      itoaX( tm->tm_sec, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Second */
      strcat( szBuffer, "  EVID: " );
      itoaX( HypoT->iQuakeID, szTemp );
      PadZeroes( 4, szTemp );
      strcat( szBuffer, szTemp );                            /* Quake ID */
      strcat( szBuffer, "-" );
      itoaX( HypoT->iVersion, szTemp );
      PadZeroes( 2, szTemp );
      strcat( szBuffer, szTemp );                            /* Version */
      fprintf( hFile, "%s\n", szBuffer );
      fprintf( hFile, "LAT: %5.2lf%c, %6.2lf%c, DEPTH: %3ld\n",
               ll.dLat, cEW, ll.dLon, cNS, iDepth );
      fprintf( hFile, "Mw: %.1lf, Mwp: %.1lf, mb: %.1lf, Ms: %.1lf\n",
               MtR.dMag, HypoT->dMwpAvg, HypoT->dMbAvg, HypoT->dMSAvg );
      fprintf( hFile, "Mo=%11.3e (dyne-cm)\n\n", MtR.dScalarMom );
      fprintf( hFile, "No. of Stations: %3ld, GAP: %3ld\n", iNWave,
               MtR.iAzGap );
      fprintf( hFile, "Error (clvd/dc)*100= %le\n\n", MtR.dError );

/* Write out the beachball in an ASCII format */
      fprintf( hFile, "Nodal plane parameters\n       strike     dip    rake\n" );
      fprintf( hFile, "NP1:    %4ld    %4ld    %4ld    \n",
               (int) (MtR.dStrike1+0.5), (int) (MtR.dDip1+0.5),
               (int) (MtR.dRake1+0.5) );
      fprintf( hFile, "NP2:    %4ld    %4ld    %4ld    \n\n",
               (int) (MtR.dStrike2+0.5), (int) (MtR.dDip2+0.5),
               (int) (MtR.dRake2+0.5) );			
      TextBeach( MtR.dStrike1, MtR.dDip1, MtR.dRake1, iNChar, iProj, hFile,
                 iDebug );

/* Write out the waveform fits */
      fprintf( hFile, "\nStat Dist Az Misfit\n" );
      for ( i=0; i<iNSta; i++ )
         if ( MtS[i].iUse == 1 )
            fprintf( hFile, "%4s %3ld %3ld %9.3e\n",
                     MtS[i].szStation, (int) (MtS[i].azdelt.dDelta+0.5),
                     (int)(MtS[i].azdelt.dAzimuth), MtR.dMisFit[i] );
      fprintf( hFile, "\n" );
     
      fprintf( hFile, "Median of misfit: %9.3e\n", MtR.dMisFitMd );
      fprintf( hFile, "Average misfit: %9.3e\n", MtR.dMisFitAvg );
      fclose( hFile );
   }
   else
      logit( "", "EmailFile not opened = %s\n", szOutFile );
}

  /******************************************************************
   *                        TextBeach()                             *
   *                                                                *
   *  Generates a text "beach-ball" based on strike, dip, and slip. *
   *                                                                *
   *  Original Fortran code by Antonio Villasenor: 9/2/98.          *
   *  Converted to C by Whitmore: 4/8/02.                           *
   *                                                                *
   *  Arguments:                                                    *
   *    dStrike     Fault plane strike in degrees (0-360)           *
   *    dDip        Fault plane dip in degrees (0-90)               *
   *    dRake       Slip vector orientation (-180 - 180)            *
   *    iNumChar    Diameter of beach ball in text characters       *
   *    iProj       0 - stereographic (Wulff net)                   *
   *                1 - equal-area (Schmidt net - default)          *
   *    hOutput     Handle to output device                         *
   *    iDebug      Debug level                                     *
   *                                                                *
   ******************************************************************/
   
void TextBeach( double dStrike, double dDip, double dRake, int iNumChar,
                int iProj, FILE *hOutput, int iDebug )
{
   double  dAspectRatio = 18./33.;  /* Aspect ratio */
   double  dAz;                     /* Polar coordinate - azimuth */
   double  dFp;                     /* Polarity */
   double  dInc;                    /* Take off angle */
   double  dRad;                    /* Polar coordinate - R */
   double  dStrikeR, dDipR, dRakeR; /* Strike, dip, slip converted to radians */
   double  dX, dY;                  /* Cartesian coordinates */
   int     i, j, k;                 /* Counters */
   int     iNumLines;               /* Number of lines in beach-ball */
   char    szLine[256];             /* Beach-ball line */

/* Check output size and projection, set if they make no sense */
   if ( iNumChar <= 30 ) iNumChar = 30;
   if ( iNumChar > 80 ) iNumChar = 80;
   if ( iProj != 0 ) iProj = 1;

/* Convert strike, dip, slip to radians */
   dStrikeR = dStrike*PI/180.;
   dDipR    = dDip*PI/180.;
   dRakeR   = dRake*PI/180.;

/* Given iNumChar and aspect ratio, calculate number of lines of beach ball */
   iNumLines = (int) ((double) iNumChar*dAspectRatio + 0.5);

/* Calculate plunge and azimuth of P and T axis from strike, dip, and rake.
   Calculate position of P and T axis in character matrix: ip,jp,it,jt. */

/* Loop over all lines */
   for ( i=0; i<iNumLines; i++ )
   {
      dY = (((iNumLines+1)/2.0)-(i+1))/dAspectRatio;
	  
/* Loop over each charcter in line */	  
      for ( j=0; j<iNumChar; j++ )
      {
         dX = (j+1) - (iNumChar+1)/2.0;

/* Convert from cartesian coords. to polar coords. */
         dRad = sqrt( dX*dX + dY*dY );
         if ( dX == 0.)
         {
            if ( dY >= 0.0 ) dAz = 0.;
              else           dAz = PI;
         }
         else if ( dX > 0.)  dAz = PI/2.0 - atan( dY/dX );
         else                dAz = 3.0*PI/2.0 - atan( dY/dX );

         if ( dRad > iNumChar/2.0 )
         {
            szLine[j] = ' ';
            goto EndInner;
         }

/* Go back from polar coordinates (dRad, dAz) to (dInc, dAz)
   where dInc is the incidence or take-off angle.  Azimuth
   is identical, but take off angle depends on the projection: */
         if ( iProj == 0 ) dInc = 2.0 * atan( 2.0*dRad/(double) iNumChar );
          else dInc = 2.0 * asin( sqrt( 2.0 )*dRad/(double) iNumChar );

/* Calculate polarity => write # or - */
         dFp = cos( dRakeR )*sin( dDipR )*sin( dInc )*sin( dInc )*
               sin( 2*(dAz-dStrikeR) ) - cos( dRakeR )*cos( dDipR )*
               sin( 2*dInc )*cos( dAz-dStrikeR ) + sin( dRakeR )*sin( 2*dDipR )*
              (cos( dInc )*cos( dInc ) - sin( dInc )*sin( dInc )*
               sin( dAz-dStrikeR )*sin( dAz-dStrikeR )) +
               sin( dRakeR )*cos( 2*dDipR )*sin( 2*dInc )*sin( dAz-dStrikeR );
         if ( dFp < 0.0 ) szLine[j] = '-';
         else             szLine[j] = '#';
EndInner:;
      }
	  
      if ( iDebug > 0 )
      {
         for ( k=0; k<iNumChar; k++ )
            logit( "", "%c", szLine[k] );
         logit( "", "\n" );
      }
      for ( k=0; k<iNumChar; k++ )
         fprintf( hOutput, "%c", szLine[k] );
      fprintf( hOutput, "\n" );
   }
}

  /******************************************************************
   *                       AzDip()                                  *
   *                                                                *
   *  Convert eigenvectors to angles.                               *
   *                                                                *
   *  From Harley Benz (NEIC).                                      *
   *  Converted to C by Whitmore: 4/21/02.                          *
   *                                                                *
   ******************************************************************/

void AzDip( double vec[], double *az, double *dip )
{
   static double  a1, a2, a3, a4, aztemp, diptemp;
   
   a1 = vec[0];
   a2 = vec[1];
   if ( a1 == 0.0 && a2 == 0.0 )
   {
      aztemp = 0.0;
      if ( vec[2] > 0.0) aztemp = 180.;
   }
   else
   {
      aztemp = atan2( a1, a2 );
      aztemp /= DEG2RAD;
   }
   
   a3 = sqrt( a1*a1 + a2*a2 );
   a4 = fabs( vec[2] );
   diptemp = atan2( a4, a3 );
   diptemp /= DEG2RAD;
   if ( vec[2] > 0. ) aztemp = aztemp - 180.;
   if      ( *az >  180. ) aztemp = aztemp - 360.;
   else if ( *az < -180. ) aztemp = aztemp + 360.;
   *az = aztemp;
   *dip = diptemp;
}
