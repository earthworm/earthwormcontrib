    /******************************************************************
     *                            pick_v.c                            *
     *                                                                *
     *              Contains PickV(), a function to pick              *
     *                    one demultiplexed message                   *
     *          and other functions called by the P-picker.           *
     *                                                                *
     *   By:   Whitmore - Jan., 2001                                  *
     ******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include <trace_buf.h>
#include <transport.h>
#include "pick_wcatwc.h"

 /***********************************************************************
  *                              PickV()                                *
  *      Evaluate one demultiplexed message with the Veith P-picker     *
  *                                                                     *
  *  Arguments:                                                         *
  *     Sta              Pointer to station being processed             *
  *     WaveBuf          Pointer to the message buffer                  *
  *     GParm            Pointer to configuration parameters            *
  *     Ewh              Pointer to Earthworm logo structure            *
  *     WaveRaw          Array of unfiltered signal                     *
  *     WaveLong         Pointer to array of filtered data              *
  *     pAS              ALARMSTRUCT array with regional alarm data     *
  *     WaveLong         Number of regions in ALARMSTRUCT               *
  *                                                                     *
  *  December, 2004: Added multi-station alarm and signal strength      *
  *                  determination.                                     *
  *                                                                     *
  ***********************************************************************/

void PickV( STATION *Sta, char *WaveBuf, GPARM *Gparm, EWH *Ewh,
            long WaveRaw [], long *WaveLong, ALARMSTRUCT pAS[], int iNumReg )
{
   int     i, ii;
   long    lCnt;                /* Index counter for transfer of raw data 
                                   circular buffer to Mwp buffer */
   static  long lLTASamps;      /* # samps / moving avg block */
   long    lNumConsec;          /* Max. # samps. to evaluate in 1/2 cycle */
   static  long lPickCounter=0; /* Pick Counter */
   time_t  now;                 /* Present 1/1/1970 time */
   PPICK   PBuf;                /* Temp buffer for compatibility with AutoMwp */
   TRACE_HEADER  *WaveHead;     /* Pointer to waveform header */

/* Find pointer to TRACE_BUF header */
   WaveHead  = (TRACE_HEADER *) WaveBuf;
   
/* Process data through alarm function if set in .sta file (and recent data) */
   time( &now );
   if ( now-WaveHead->endtime < RECENT_ALARM_TIME )
      if ( Gparm->AlarmOn )
         if ( Sta->iAlarmStatus >= 1 )
            SeismicAlarm( Sta, Gparm, Ewh, WaveHead, WaveLong );

/* Compute Maximum number of samples to allow per cycle */
   lNumConsec = (long) (WaveHead->samprate / (2.*Gparm->MinFreq) + 0.0001);

/* Compute number of samples in the moving average block */
   lLTASamps = (long) (WaveHead->samprate * Gparm->LTASeconds + 0.0001);

/* Loop over all samples in packet */
   for ( i=0; i<WaveHead->nsamp; i++ )
   {       
/* First, update averages */
      MovingAvg ( WaveLong[i], Sta, lLTASamps, WaveRaw[i], lNumConsec );

/* If Station past first initialization or just had pick, proceed with P-picker */
      if ( Sta->iPickStatus >= 2 )
      {
/* Fill raw data circular buffer (if Mwps are to be computed) */
         if ( Sta->dSens > 0.0 && Sta->iComputeMwp ) RawDataBuff( Sta );

/* Add to Mwp array if Phase 1 passed */
         if ( Sta->lMwpCtr > 0 && Sta->lMwpCtr <
		    (long) (WaveHead->samprate*Gparm->MwpSeconds+0.001 ))
         {
            Sta->plRawData[Sta->lMwpCtr] = (long) ((double) Sta->lSampRaw -
                                           Sta->dAveLDCRawOrig);
            Sta->lMwpCtr++;

/* Compute Mwp when array is full or every 50 seconds */
            if (Sta->lMwpCtr==(long)(WaveHead->samprate*Gparm->MwpSeconds+0.1)||
               (Sta->lMwpCtr % (long)(WaveHead->samprate*50.)) == 0 )
            {
               AutoMwp( Sta, &PBuf, Gparm->MwpSigNoise, Gparm->MwpSeconds, 0 ); 

/* If S:N was great enough to compute an Mwp, report the pick to PICK_RING */
               if ( PBuf.dMwpIntDisp > 0. )
               {	
                  Sta->dMwpIntDisp = PBuf.dMwpIntDisp;
                  Sta->dMwpTime = PBuf.dMwpTime;
                  ReportPick( Sta, Gparm, Ewh );
               }
            }
         }

/* If we are done with Lg and Mwp processing, restart computations with new
   averages */
         if ( Sta->iPickStatus == 3 && 
              WaveHead->starttime+(double)i/WaveHead->samprate-Sta->dTrigTime >
               (double) Gparm->MwpSeconds &&
              WaveHead->starttime+(double)i/WaveHead->samprate-Sta->dTrigTime >
               (double) Gparm->LGSeconds ) 
         {
            Sta->iPickStatus = 1;
            InitVar ( Sta );
            goto EndOfLoop;
         }

/* Back to the picker, first check for cycle changes */
         if ( (Sta->lMDFOld <  0 && Sta->lMDFNew <  0) ||
              (Sta->lMDFOld >= 0 && Sta->lMDFNew >= 0))
         {     /* No changes, continuing adding up MDF */
            Sta->lSampsPerCyc++;
            if ( Sta->lSampsPerCyc < lNumConsec )
               Sta->lMDFRunning += Sta->lMDFNew;
            else
            {
               Sta->lMDFRunning = Sta->lMDFNew;
               Sta->lSampsPerCyc = 0;            
            }
         }
         else  /* Cycle has changed sign, get mags and start anew */
         {
            if ( Sta->lPhase1 == 1 )
            {
               Sta->lMDFTotal += labs( Sta->lMDFRunning );
               Sta->lMDFCnt++;
               if ( GetMbMl ( Sta, WaveHead, i, Gparm, Ewh ) < 0 )
                  goto EndOfLoop;     /* Sine-wave cal must be over */
            }
            Sta->lMDFRunning = Sta->lMDFNew;
            Sta->lSampsPerCyc = 0;
         }
		 
/* If this is a wc/atwc sine wave cal, skip further processing */		 
         if ( Sta->iCal ) goto EndOfLoop;
		 
/* Check first motion if we are in first few samples of pick */
         if ( Sta->lFirstMotionCtr >= 1 &&
              Sta->lFirstMotionCtr < FIRST_MOTION_SAMPS )
         {
            if ( (Sta->lMDFRunning < 0 && Sta->lMDFOld < 0) ||
                 (Sta->lMDFRunning >= 0 && Sta->lMDFOld >= 0) )
               Sta->lFirstMotionCtr++;
            else   /* There was a reveral so 1st motion is questionable */
            {
               Sta->cFirstMotion = '?';
               Sta->lFirstMotionCtr = 0;
            }
         }         /* If we've checked enough samples, assume 1st mo. is good */
         if ( Sta->lFirstMotionCtr == FIRST_MOTION_SAMPS )
            Sta->lFirstMotionCtr = 0;

/* If the station is picked, no need to do anything more */
         if ( Sta->iPickStatus == 3 ) goto EndOfLoop;

/* If Phase3 has been passed, wait 3s (for sine cal discrimination) before
   declaring pick */
         if ( Sta->lPhase3 == 1 )
         {   
            Sta->l3sCnt++;
            if ( Sta->l3sCnt < (long) (WaveHead->samprate * 3. + 0.01))
                  goto EndOfLoop;
            else  goto PickCounter;
         }

/* Phase 1: Has MDF trigger threshold been surpassed? */
         if ( ( !Sta->lPhase1 && (double) (labs (Sta->lMDFRunning)) >=
		         Sta->dMDFThresh) ||
              (  Sta->lPhase1 && (double) (labs (Sta->lMDFRunning)) >=
			     Sta->dMDFThreshOrig) )
         {
            Sta->lTrigFlag = 1;
            if (Sta->lPhase1 == 0)
            {   /* Set phase1 passage here */  
               Sta->lPhase1 = 1;
			   
/* Save existing moving averages and thresholds */
               Sta->dMDFThreshOrig = Sta->dMDFThresh;
               Sta->dLTAThreshOrig = Sta->dLTAThresh;
               Sta->dAveLDCRawOrig = Sta->dAveLDCRaw;
               Sta->dAveRawNoiseOrig = Sta->dAveRawNoise;
               Sta->dAveMDFOrig    = Sta->dAveMDF;
               Sta->lRawNoiseOrig  = Sta->lRawNoise;
			   
/* Look for first motion */
               Sta->lFirstMotionCtr = 1;
               if (Sta->lMDFRunning > 0) Sta->cFirstMotion = 'U';
               else                      Sta->cFirstMotion = 'D';

/* If this station is used for Mwp calculations, start Counter and fill buffer*/
               if ( Sta->dSens > 0.0 && Sta->iComputeMwp )
               {
                  lCnt = Sta->lRawCircCtr - Sta->lSampsPerCyc - 1;
				  if ( lCnt < 0 ) lCnt += Sta->lRawCircSize;
                  for ( ii=0; ii<Sta->lSampsPerCyc; ii++ )
                  {
                     Sta->plRawData[ii] = (long) ((double)
					  Sta->plRawCircBuff[lCnt] - Sta->dAveLDCRawOrig);
                     lCnt++;
                     if ( lCnt >= Sta->lRawCircSize ) lCnt -= Sta->lRawCircSize;
                  }
                  Sta->lMwpCtr = Sta->lSampsPerCyc;
               }

/* Save P-time (# seconds since 1/1/1970) */
               Sta->dTrigTime = WaveHead->starttime +
                (double) (i-Sta->lSampsPerCyc)/WaveHead->samprate;
            }
         }
         if (Sta->lPhase2 == 1) goto Phase3;
         if (Sta->lPhase1 == 0) goto Phase4;

/* Phase 2: P-Phase processing */
         Sta->lPhase2Cnt++;
         if ( Sta->lPhase2Cnt > 3*lNumConsec ) goto Reset;

/* Count trigger passes versus misses */
         if ( Sta->lTrigFlag == 1 )
         {
            Sta->lHit++;
            if ( Sta->lHit == lNumConsec ) Sta->lTest1 = 1;
         }
         else
         {
            Sta->lMis++;

/* NOTE: By Veith's paper, this test should be performed after the passing 
   of tests 1 and 2.  The picks are much better, though, if it is done 
   sample-by-sample. */
            if ( Sta->lMis > Sta->lHit ) goto Reset; // Fail test 3
         }

/* Test2 in Phase2 - Must exceed following amp. sometime in phase2 */
         if ( fabs ((double) Sta->lSampNew-Sta->dAveLDC) > Sta->dLTAThreshOrig )
            Sta->lTest2 = 1;

/* See if Phase 2 has passed */
         if ( Sta->lTest1+Sta->lTest2 != 2 ) goto Phase4;

/* Otherwise, Phase 2 has passed. So, get ready for Phase 3 */
         Sta->lPhase2 = 1;
         Sta->lNumOsc = 0;
         Sta->lHit = 0;
         Sta->lMis = 0;
         Sta->lLastSign = 0;
         if ( Sta->lMDFRunning < 0 ) Sta->lLastSign = 1;

/* Phase 3: Look for oscillatory motion */
Phase3:  Sta->lPhase3Cnt++;

/* Below is time limit for passing Phase 3 (test 2) */
         if ( Sta->lPhase3Cnt > 12*lNumConsec ) goto Reset;

/* Check to see if trigger MDF was exceeded */
         if ( Sta->lTrigFlag == 1 ) Sta->lHit++;
         else                       Sta->lMis++;
         if ( Sta->lTrigFlag == 0 ) goto Phase4;

/* Next, check for oscillations */
         Sta->lCurSign = 0;
         if ( Sta->lMDFRunning < 0 ) Sta->lCurSign = 1;
         if ( Sta->lLastSign != Sta->lCurSign ) Sta->lNumOsc++;

/* Phase3, test3 is passed when 6 reverses are noted */
         if ( Sta->lNumOsc < 6 )
         {
            Sta->lLastSign = Sta->lCurSign;
            goto Phase4;
         }

/* If MDF < trigger value more than not and for more than 4s, fail test 3 */
         if (Sta->lMis > Sta->lHit && Sta->lMis > (long)(4.*WaveHead->samprate)) 
            goto Reset;

/* Otherwise, phase 3 was passed; don't declare pick yet, wait 3s for in case
   its cal */
         Sta->lPhase3 = 1;
         goto EndOfLoop;

/* Declare and report the pick */
PickCounter: 
         Sta->iPickStatus = 3;
         lPickCounter++;
         if ( lPickCounter >= 9999 ) lPickCounter = 1;
         Sta->lPickIndex = lPickCounter;
         ReportPick( Sta, Gparm, Ewh );
         ReportPickGlobal( Sta, Gparm, Ewh );
         Sta->dPStrength = ((double) Sta->lMDFTotal/(double) Sta->lMDFCnt) /
                                     Sta->dMDFThreshOrig;
         time( &now );	               /* See that its not old data */
         if ( now-Sta->dTrigTime < RECENT_ALARM_TIME )
             if ( Gparm->TwoStnAlarmOn == 1 )
                 CheckForAlarm( Sta, pAS, iNumReg, 
                                Gparm, Ewh );  /* Check for alarm */
         goto Phase4;

/* Reset some picker variables, one of the tests failed */
Reset:   Reset (Sta);
                        
/* Start of Phase 4 (Phase 4 is mainly skipped here.  Events are terminated 
   when magnitude information has been computed). */
Phase4:  Sta->lTrigFlag = 0;
EndOfLoop:;
      }
      Sta->lMDFOld = Sta->lMDFNew;
   }
}

  /******************************************************************
   *                           MovingAvg()                          *
   *                                                                *
   *  Determine and update moving averages of absolute signal value *
   *  (called LTA here) and differential function (called MDF). Peak*
   *  noise (unfiltered) is also noted here for each LTASamps.      *
   *  NOTE: Incoming data must be short-period or filtered.         *
   *                                                                *
   *  March, 2004: Changed background noise computation to RMS for  *
   *               Mwp computations                                 *
   *                                                                *
   *  Arguments:                                                    *
   *    LongSample  One waveform data sample                        *
   *    Sta         Station data array                              *
   *    lLTASamps   # of samples per moving avg block               *
   *    RawSample   Un-filtered waveform data sample                *
   *    lNumConsec  Maximum samples to add up MDF (based on MinFreq)*
   *                                                                *
   ******************************************************************/

void MovingAvg( long LongSample, STATION *Sta, long lLTASamps,  
                long RawSample, long lNumConsec )
{
   static long    lHigh, lLow; /* Peak/trough signal values for each interval */

/* Copy new sample to structure and compute DF */
   Sta->lSampNew = LongSample;
   Sta->lSampRaw = RawSample;
   Sta->lMDFNew = Sta->lSampNew - Sta->lSampOld;
   
/* Add last sample and MDF to running totals and noise levels */
   if ( Sta->lLTACtr < lLTASamps )
   {
      if ( Sta->iPickStatus == 1 )
      {
         Sta->dSumLDC    += (double) Sta->lSampOld;
         Sta->dSumLDCRaw += (double) Sta->lSampRaw;
         Sta->dSumLTA    += (double) (labs( Sta->lSampOld ));
         Sta->dSumRawNoise += (((double)Sta->lSampRaw/Sta->dSens) *
                               ((double)Sta->lSampRaw/Sta->dSens));
      }
      else
      {
         Sta->dSumLDC    += ((double) (Sta->lSampOld) - Sta->dAveLDC);
         Sta->dSumLDCRaw += ((double) (Sta->lSampRaw) - Sta->dAveLDCRaw);
         Sta->dSumLTA    += (fabs( (double) Sta->lSampOld-Sta->dAveLDC ) -
                             Sta->dAveLTA);
         Sta->dSumRawNoise += (((double) Sta->lSampRaw-Sta->dAveLDCRaw)/Sta->dSens *
                               ((double) Sta->lSampRaw-Sta->dAveLDCRaw)/Sta->dSens);
      }
      if ( Sta->lSampRaw > lHigh ) lHigh = Sta->lSampRaw;
      if ( Sta->lSampRaw < lLow )  lLow  = Sta->lSampRaw;
   }
   else               /* Compute new LTAs and noise level */
   {
      if ( Sta->iPickStatus == 1 )
      {
         Sta->dAveMDF    = Sta->dSumMDF / (double) Sta->lCycCntLTA;
         Sta->dAveLDC    = Sta->dSumLDC / (double) (lLTASamps-1);
         Sta->dAveLDCRaw = Sta->dSumLDCRaw / (double) (lLTASamps-1);
         Sta->dAveLTA    = Sta->dSumLTA / (double) (lLTASamps-1);
         Sta->dAveRawNoise = sqrt( Sta->dSumRawNoise /
                                  (double) (lLTASamps-1) );
         Sta->iPickStatus = 2;
      }
      else
      {
         Sta->dAveMDF    += (0.5*Sta->dSumMDF / (double) Sta->lCycCntLTA);
         Sta->dAveLDC    += (0.5*Sta->dSumLDC / (double) (lLTASamps));
         Sta->dAveLDCRaw += (0.5*Sta->dSumLDCRaw / (double) (lLTASamps));
         Sta->dAveLTA    += (0.5*Sta->dSumLTA / (double) (lLTASamps));
         Sta->dAveRawNoise = 0.9*Sta->dAveRawNoise +
                             0.1*sqrt( Sta->dSumRawNoise /
                                       (double) (lLTASamps) );
      }
      Sta->lRawNoise = lHigh - lLow;
      if ( Sta->lRawNoise == 0 ) Sta->lRawNoise = 1;
	  
/* Reset summation variables and compute thresholds */	  
      lHigh = -10000000;
      lLow  =  10000000;
      Sta->dSumMDF = 0.;
      Sta->dSumLDC = 0.;
      Sta->dSumLDCRaw = 0.;
      Sta->dSumRawNoise = 0.;
      Sta->dSumLTA = 0.;
      Sta->lLTACtr = 0;
      Sta->lCycCntLTA = 0;
      Sta->dMDFThresh = 0.5 * (double) Sta->iSignalToNoise * Sta->dAveMDF;
      Sta->dLTAThresh = 1.57 * (double) Sta->iSignalToNoise * Sta->dAveLTA;
   }   
   
/* Check for cycle changes (convert DF to MDF) */
   if ( (Sta->lMDFOld < 0 && Sta->lMDFNew < 0) ||
        (Sta->lMDFOld >= 0 && Sta->lMDFNew >= 0) )
   {
         /* No changes, continuing adding up MDF */
      Sta->lSampsPerCycLTA++;
      if ( Sta->lSampsPerCycLTA < lNumConsec )
         Sta->lMDFRunningLTA += Sta->lMDFNew;
      else
      {
         if ( Sta->iPickStatus == 1 )
            Sta->dSumMDF += (double) (labs( Sta->lMDFRunningLTA ));
         else
            Sta->dSumMDF += ((double) (labs( Sta->lMDFRunningLTA )) -
                             Sta->dAveMDF);
         Sta->lMDFRunningLTA = Sta->lMDFNew;
         Sta->lCycCntLTA++;
         Sta->lSampsPerCycLTA = 0;
      }
   }
   else  /* Cycle has changed sign, start anew */
   {
      if ( Sta->iPickStatus == 1 )
         Sta->dSumMDF += (double) (labs( Sta->lMDFRunningLTA ));
      else
         Sta->dSumMDF += ((double) (labs( Sta->lMDFRunningLTA )) -
                          Sta->dAveMDF);
      Sta->lMDFRunningLTA = Sta->lMDFNew;
      Sta->lCycCntLTA++;
      Sta->lSampsPerCycLTA = 0;
   }   
   
/* Update old with new value and increment averages counter (MDF updated
   elsewhere) */   
   Sta->lSampOld = Sta->lSampNew;
   Sta->lLTACtr++;           // LTA counter
}

 /***********************************************************************
  *                             RawDataBuff()                           *
  *                                                                     *
  *  Fill raw data circular buffer.  This buffer is necessary so that   *
  *  old data is available to fill Mwp raw data buffer after phase 1    *
  *  is passed.  It only needs to be big enough to hold samples that    *
  *  accumulate while Phase 1 has not yet passed.                       *
  *                                                                     *
  *  Arguments:                                                         *
  *     Sta              Pointer to station being processed             *
  *                                                                     *
  ***********************************************************************/

void RawDataBuff( STATION *Sta )
{
   Sta->plRawCircBuff[Sta->lRawCircCtr] = Sta->lSampRaw;
   Sta->lRawCircCtr++;
   if ( Sta->lRawCircCtr == Sta->lRawCircSize ) Sta->lRawCircCtr = 0;
}

 /***********************************************************************
  *                              Reset()                                *
  *      Reset some picker variables when a phase/test has failed       *
  *                                                                     *
  *  Arguments:                                                         *
  *     Sta              Pointer to station being processed             *
  *                                                                     *
  ***********************************************************************/

void Reset( STATION *Sta )
{
   Sta->lRawCircCtr = 0;
   Sta->lPhase1 = 0;
   Sta->lPhase2 = 0;
   Sta->lPhase3 = 0;
   Sta->lTest1 = 0;
   Sta->lTest2 = 0;
   Sta->lHit = 0;
   Sta->lMis = 0;
   Sta->dMaxPk = 0.;
   Sta->lCycCnt = 0;
   Sta->lPer = 0;
   Sta->lMlPer = 0;
   Sta->dMlTime = 0.;
   Sta->lMbPer = 0;
   Sta->dMbTime = 0.;
   Sta->dMlAmpGM = 0.;
   Sta->dMbAmpGM = 0.;
   Sta->lMwpCtr = 0;
   Sta->iCal = 0;
   Sta->cFirstMotion = '?';
   Sta->lFirstMotionCtr = 0;
   Sta->lMagAmp = 0;
   Sta->dAvAmp = 0.;
   Sta->lSWSim = 0;
   Sta->lPhase2Cnt = 0;
   Sta->lPhase3Cnt = 0;
   Sta->dTrigTime = 0.;
   Sta->dMDFThreshOrig = 0.;
   Sta->dLTAThreshOrig = 0.;
   Sta->dAveLDCRawOrig = 0.;
   Sta->dAveRawNoiseOrig = 0.;
   Sta->dAveMDFOrig = 0.;
   Sta->lRawNoiseOrig = 0;
   Sta->lMDFTotal = 0;
   Sta->lMDFCnt = 0;
   Sta->dPStrength = 0.;
}


 /***********************************************************************
  *                             CheckForAlarm()                         *
  *                                                                     *
  *  Check for multi station regional alarm.  This logic was patterned  *
  *  after a PTWC program which basically does the same thing. Send     *
  *  alarm if necessary.                                                *
  *                                                                     *
  *  May, 2005: Add timeout feature to alarm (i.e., if pick is over x   *
  *             minutes old, ignore it.                                 *
  *                                                                     *
  *  Arguments:                                                         *
  *     Sta              Station data structure                         *
  *     pAS              ALARMSTRUCT structure                          *
  *     iNumReg          Number of Alarm regions                        *
  *     Gparm            Configuration parameters                       *
  *     Ewh              Earthworm message values                       *
  *                                                                     *
  ***********************************************************************/
void CheckForAlarm( STATION *Sta, ALARMSTRUCT pAS[], int iNumReg, 
                    GPARM *Gparm, EWH *Ewh )
{
   int   i, j, k;
   
   for ( i=0; i<iNumReg; i++ )
      for ( j=0; j<pAS[i].iNumStnInReg; j++ ) /* Is it an alarm station */
	     if ( !strcmp( pAS[i].szStation[j], Sta->szStation ) )
         {                                    /* Yes, it is */
            logit( "", "Alarm Stn found-%s in %s, SN=%lf\n", Sta->szStation, 
                   pAS[i].szRegionName, Sta->dPStrength );
            if ( Sta->dPStrength > pAS[i].dThresh )/* Is it strong enough? */
               if ( (Sta->dTrigTime-Sta->dTimeCorrection)-pAS[i].dLastTime <
                  pAS[i].dMaxTime )                   /* Is it within time? */
               {  /* First, see if this station is already alarmed */
                  for ( k=0; k<pAS[i].iNumPicksCnt; k++ )
                     if ( !strcmp( Sta->szStation, pAS[i].szStnAlarm[k] ) )
                     {
                        logit( "", "%s already alarmed\n", Sta->szStation );
	                    goto LoopEnd;
                     }
                  strcpy( pAS[i].szStnAlarm[pAS[i].iNumPicksCnt], Sta->szStation );
                  pAS[i].iNumPicksCnt++;
                  logit( "", "%s has %ld alarm stations\n", pAS[i].szRegionName,
                         pAS[i].iNumPicksCnt );
                  if ( pAS[i].iNumPicksCnt >= pAS[i].iAlarmThresh )
                  {
                     ReportAlarm( Sta, Gparm, Ewh, 2, pAS[i].szRegionName );
                     pAS[i].iNumPicksCnt = 0;
                     pAS[i].dLastTime = 0.;
                  }
               }
               else
               {
                  logit( "", "Reset alarm vars. in %s, %s\n", pAS[i].szRegionName,
                         Sta->szStation );
                  strcpy( pAS[i].szStnAlarm[0], Sta->szStation );
                  pAS[i].iNumPicksCnt = 1;
                  pAS[i].dLastTime = Sta->dTrigTime-Sta->dTimeCorrection;
               }
         }
   LoopEnd:;
   return;
}

