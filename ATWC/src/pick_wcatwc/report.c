  /**********************************************************************
   *                              report.c                              *
   *                                                                    *
   *                   Pick, alarm buffering functions                  *
   *                                                                    *
   *  This file contains function ReportPick() and ReportAlarm().       *
   **********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include <transport.h>
#include "pick_wcatwc.h"

     /**************************************************************
      *                         ReportPick()                       *
      *                                                            *
      *                 Fill in TYPE_PICKTWC format                *
      **************************************************************/

void ReportPick( STATION *Sta, GPARM *Gparm, EWH *Ewh )
{
   MSG_LOGO     logo;      /* Logo of message to send to the output ring */
   int          lineLen;
   char         line[512]; /* TYPE_PICKTWC format message */
   long         lTemp;

/* Create TYPE_PICKTWC message
   ***************************/
   sprintf( line,    "%ld %ld %ld %s %s %s %ld %ld %lf %c %s %lf %ld %lf %lf "
                     "%ld %lf %lf %ld %lf %lE %lf %ld\0",
            (int) Ewh->TypePickTWC, (int) Gparm->MyModId, (int) Ewh->MyInstId,
            Sta->szStation, Sta->szChannel, Sta->szNetID, Sta->lPickIndex, 1,
            Sta->dTrigTime-Sta->dTimeCorrection, Sta->cFirstMotion, Sta->szPhase,
            Sta->dMbAmpGM, Sta->lMbPer, Sta->dMbTime,
            Sta->dMlAmpGM, Sta->lMlPer, Sta->dMlTime, 0., 0, 0.,
            Sta->dMwpIntDisp, Sta->dMwpTime, -1 );
   lineLen = strlen( line ) + 1;

/* log pick
   ********/
   lTemp = (long) (Sta->dTrigTime-Sta->dTimeCorrection);
   logit( "e", "%s, %c, P-time=%s", Sta->szStation, Sta->cFirstMotion,
          asctime( TWCgmtime( lTemp ) ) );

/* Send the pick to the output ring
   ********************************/
   logo.type   = Ewh->TypePickTWC;
   logo.mod    = Gparm->MyModId;
   logo.instid = Ewh->MyInstId;

   if ( tport_putmsg( &Gparm->OutRegion, &logo, lineLen, line ) != PUT_OK )
      logit( "t", "pick_wcatwc: Error sending pick to output ring.\n" );
}

     /**************************************************************
      *                    ReportPickGlobal()                      *
      *                                                            *
      *                 Fill in TYPE_PICK_GLOBAL format            *
      **************************************************************/

void ReportPickGlobal( STATION *Sta, GPARM *Gparm, EWH *Ewh )
{
   MSG_LOGO     logo;      /* Logo of message to send to the output ring */
   int          lineLen;
   char         line[512]; /* TYPE_PICK_GLOBAL format message */
   long         lTemp;
   struct tm   *tm;        /* pick time structure */
   char         szType[4], szModId[4], szInstId[4], szTime[19], szTemp[5];

/* Create TYPE_PICK_GLOBAL message
   *******************************/
   itoaX( (int) Ewh->TypePickTWC, szType );
   PadZeroes( 3, szType );
   itoaX( (int) Gparm->MyModId, szModId );
   PadZeroes( 3, szModId );
   itoaX( (int) Ewh->MyInstId, szInstId );
   PadZeroes( 3, szInstId );   
   lTemp = (long) (Sta->dTrigTime-Sta->dTimeCorrection);
   tm = TWCgmtime( lTemp );
   itoaX( (int) (tm->tm_year+1900), szTemp );
   PadZeroes( 4, szTemp );   
   strcpy( szTime, szTemp );
   itoaX( (int) (tm->tm_mon+1), szTemp );
   PadZeroes( 2, szTemp );   
   strcat( szTime, szTemp );
   itoaX( (int) tm->tm_mday, szTemp );
   PadZeroes( 2, szTemp );   
   strcat( szTime, szTemp );
   itoaX( (int) tm->tm_hour, szTemp );
   PadZeroes( 2, szTemp );   
   strcat( szTime, szTemp );
   itoaX( (int) tm->tm_min, szTemp );
   PadZeroes( 2, szTemp );   
   strcat( szTime, szTemp );
   itoaX( (int) tm->tm_sec, szTemp );
   PadZeroes( 2, szTemp );   
   strcat( szTime, szTemp );
   strcat( szTime, "." );
   lTemp = (long) (((Sta->dTrigTime-Sta->dTimeCorrection) - 
                  (long) (Sta->dTrigTime-Sta->dTimeCorrection)) * 1000);
   itoaX( (int) lTemp, szTemp );
   PadZeroes( 3, szTemp );   
   strcat( szTime, szTemp );
   
   sprintf( line, "%3s%3s%3s %ld %d %s %s %s %s %s %s %lf %c\0",
            szType, szModId, szInstId,
            Sta->lPickIndex, (short) GLOBAL_PICK_VERSION, 
            Sta->szStation, Sta->szChannel, Sta->szNetID, "00", szTime,
			Sta->szPhase, 0.0, Sta->cFirstMotion );
   lineLen = strlen( line ) + 1;

/* log pick
   ********/
   logit( "", "%s, %c, P-timeG=%s\n", Sta->szStation, Sta->cFirstMotion,
                                    szTime );

/* Send the pick to the output ring
   ********************************/
   logo.type   = Ewh->TypePickGlobal;
   logo.mod    = Gparm->MyModId;
   logo.instid = Ewh->MyInstId;

   if ( tport_putmsg( &Gparm->OutRegion, &logo, lineLen, line ) != PUT_OK )
      logit( "t", "pick_wcatwc: Error sending pick to output ring-2.\n" );
}

     /**************************************************************
      *                         ReportAlarm()                      *
      *                                                            *
      *                 Fill in TYPE_ALARM format                  *
      **************************************************************/

void ReportAlarm( STATION *Sta, GPARM *Gparm, EWH *Ewh, int iType,
                  char *pszAlarmString )
{
   MSG_LOGO     logo;      /* Logo of message to send to the output ring */
   int          lineLen;
   char         line[128]; /* TYPE_PICKALARM format message */

/* Create TYPE_ALARM message (1->activate Respond Thread)
   ******************************************************/
   if ( iType == 1 )		/* Strong motion single stn alarm */
   {
      sprintf( line,    "%5ld %5ld %5ld %5ld %5ld     0 %s %s DIGITAL SP ALARM\0",
           (int) Ewh->TypeAlarm, (int) Gparm->MyModId, (int) Ewh->MyInstId,
           Sta->iAlarmSpeak, Sta->iAlarmPage, Sta->szStation, Sta->szChannel );
      lineLen = strlen( line ) + 1;
   }
   else if ( iType == 2 )   /* Multi-station regional alarm */
   {
      sprintf( line,    "%5ld %5ld %5ld     1     1     0 %s REGIONAL ALARM\0",
           (int) Ewh->TypeAlarm, (int) Gparm->MyModId, (int) Ewh->MyInstId,
           pszAlarmString );
      lineLen = strlen( line ) + 1;
   }

/* log alarm
   *********/
   logit( "t", "%s\n", line );

/* Send the alarm to the output ring
   *********************************/
   logo.type   = Ewh->TypeAlarm;
   logo.mod    = Gparm->MyModId;
   logo.instid = Ewh->MyInstId;

   if ( tport_putmsg( &Gparm->AlarmRegion, &logo, lineLen, line ) != PUT_OK )
      logit( "t", "pick_wcatwc: Error sending alarm to output ring.\n" );
}

     /**************************************************************
      *                  ReportNoDataAlarm()                       *
      *                                                            *
      *                 Fill in TYPE_ALARM format                  *
      **************************************************************/

void ReportNoDataAlarm( GPARM *Gparm, EWH *Ewh )
{
   MSG_LOGO     logo;      /* Logo of message to send to the output ring */
   int          lineLen;
   char         line[128]; /* TYPE_PICKALARM format message */

/* Create TYPE_ALARM message (1->activate Respond Thread)
   ******************************************************/
   sprintf( line,    "%5ld %5ld %5ld     0     1     1 Earthworm Data Outage\0",
           (int) Ewh->TypeAlarm, (int) Gparm->MyModId, (int) Ewh->MyInstId );
   lineLen = strlen( line ) + 1;

/* log alarm
   *********/
   logit( "t", "%s\n", line );

/* Send the alarm to the output ring
   *********************************/
   logo.type   = Ewh->TypeAlarm;
   logo.mod    = Gparm->MyModId;
   logo.instid = Ewh->MyInstId;

   if ( tport_putmsg( &Gparm->AlarmRegion, &logo, lineLen, line ) != PUT_OK )
      logit( "t", "pick_wcatwc: Error sending alarm to output ring - 2.\n" );
}
