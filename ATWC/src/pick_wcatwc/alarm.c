  /**********************************************************************
   *                              alarm.c                               *
   *                                                                    *
   *        Determine if the station has an alarm event recorded in     *
   *                            this packet.                            *
   *                                                                    *
   *  This file contains function SeismicAlarm().                       *
   **********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <earthworm.h>
#include <transport.h>
#include "pick_wcatwc.h"

 /***********************************************************************
  *                          SeismicAlarm()                             *
  *                                                                     *
  *                 Process data through alarm algorithm                *
  *  Arguments:                                                         *
  *     Sta              Pointer to station being processed             *
  *     GParm            Pointer to configuration parameters            *
  *     Ewh              Pointer to Earthworm logo structure            *
  *     WaveHead         Pointer to the message buffer header           *
  *     WaveLong         Pointer to array of filtered data              *
  ***********************************************************************/

void SeismicAlarm( STATION *Sta, GPARM *Gparm, EWH *Ewh, TRACE_HEADER *WaveHead,
                   long *WaveLong )
{
   int  i;
   long lNumConsecA;       /* Max # samples which can pass without an alarm
                              being declared. Reset variables when no alarm. */

/* Compute lNumConsecA */
   lNumConsecA = (long) (WaveHead->samprate / (2.*Sta->dAlarmMinFreq) + 0.0001);
   
/* Initialize SeismicAlarm variables if necessary */
   if ( Sta->iAlarmStatus == 1 )
   {
      Sta->lAlarmP1 = 0;
      Sta->lAlarmCycs = 0;
      Sta->lAlarmSamps = 0;		     
      Sta->iAlarmStatus = 2;
   }

/* Loop through each sample in data buffer */   
   if ( Sta->iAlarmStatus >= 2 )
      for ( i=0; i<WaveHead->nsamp; i++ )
      {       
	  
/* Reset Alarm status and variables if timeout has passed */
         if ( Sta->iAlarmStatus == 3 )
         {
            if ( (WaveHead->starttime + (double) i/WaveHead->samprate) >
			     Sta->dAlarmLastTriggerTime+Gparm->AlarmTimeout )
            {
               Sta->iAlarmStatus = 2;
               goto ResetA;
            }
            goto EndOfLoop;
         }
	  
/* Convert SP (filtered) data to approximate value in m/s */	
         Sta->dAlarmSamp = (double) WaveLong[i] / Sta->dSens;
		 
/* Does this value exceed the alarm amplitude threshold? */
         if ( (fabs (Sta->dAlarmSamp) > Sta->dAlarmAmp) && !Sta->lAlarmP1 )
         {
            Sta->lAlarmP1 = 1;
            Sta->dAlarmLastSamp = Sta->dAlarmSamp;
            Sta->lAlarmCycs = 0;
            Sta->lAlarmSamps = 0;
         }
		 
/* If Phase 1 is passed, look for strong, cyclical motion */		 
         if ( Sta->lAlarmP1 )
         {
            Sta->lAlarmSamps++;
            Sta->lAlarmCycs++;
			
/* Signal has lNumConsecA samples to reverse and exceed the threshold, or
   processing will start over */			
            if ( Sta->lAlarmCycs > lNumConsecA ) goto ResetA;
			
/* Has the sample's sign changed and is it over the threshold */
            if ( Sta->dAlarmSamp*Sta->dAlarmLastSamp < 0. &&
                 fabs (Sta->dAlarmSamp) > Sta->dAlarmAmp )
            {
               Sta->lAlarmCycs = 0;
               Sta->dAlarmLastSamp = Sta->dAlarmSamp;
            }
			
/* Has the signal stayed in the threshold range for long enough
   without having a timeout to declare an alarm? */
            if ( (double) Sta->lAlarmSamps > WaveHead->samprate*Sta->dAlarmDur )
            {     /* If yes, report the alarm to ring */
               Sta->iAlarmStatus = 3;
               Sta->dAlarmLastTriggerTime = WaveHead->starttime +
                                           (double) i/WaveHead->samprate;
               ReportAlarm( Sta, Gparm, Ewh, 1, Sta->szStation );
            }
         }
      goto EndOfLoop;
	  
/* Reset variables when alarm threshold can not be sustained for duration */	  
ResetA:		      
      Sta->lAlarmP1 = 0;
      Sta->lAlarmCycs = 0;
      Sta->lAlarmSamps = 0;		     
EndOfLoop:;
      }
}  
