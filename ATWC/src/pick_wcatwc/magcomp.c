    /******************************************************************
     *                           magcomp.c                            *
     *                                                                *
     * Contains magnitude determination functions for use in          *
     * pick_wcatwc routines.                                          *
     *                                                                *
     *   By:   Whitmore - Jan., 2001                                  *
     ******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <earthworm.h>
#include <trace_buf.h>
#include <transport.h>
#include "pick_wcatwc.h"
#define PI           4.*atan (1.0)

 /***********************************************************************
  *                            GetMbMl()                                *
  *      Compute magnitude parameters (period and amplitude) for mb     *
  *      and Ml computations.  Also, discriminate sine wave cals from   *
  *      P-picks. (Gains are no longer automatically updated.  This must*
  *      be done interactively.)                                        *
  *                                                                     *
  *  Arguments:                                                         *
  *     Sta              Pointer to station being processed             *
  *     WaveHead         Trace buffer header information                *
  *     iIndex           Present sample index                           *
  *     Gparm            Configuration parameters structure             *
  *     Ewh              Earthworm logo structure                       *
  *                                                                     *
  *  Return:             -1 if cal over, 0 otherwise                    *
  *                                                                     *
  ***********************************************************************/
int GetMbMl( STATION *Sta, TRACE_HEADER *WaveHead, int iIndex, GPARM *Gparm,
             EWH *Ewh )
{

/* Process period and amplitude if sensitivity is known */
   if ( Sta->dSens > 0. )
   {     /* Increment MbCycles counter */
      Sta->lCycCnt++;
      if ( Sta->lCycCnt >= 3 )
      {     /* Ignore first 2 cycles in sine-wave cal discrimation */
         Sta->lMagAmp += labs (Sta->lMDFRunning);  
         Sta->dAvAmp = (double) Sta->lMagAmp / (double) (Sta->lCycCnt - 2);

/* Check to see if amplitude constant (it will be for SW cal) */
         if ( (double) labs (Sta->lMDFRunning) < (Sta->dAvAmp+Sta->dAvAmp/5.) &&
              (double) labs (Sta->lMDFRunning) > (Sta->dAvAmp-Sta->dAvAmp/5.) &&
              Sta->dAvAmp > (2.*Sta->dAveMDF) &&
              Sta->dAvAmp < Sta->dClipLevel ) Sta->lSWSim++;
      }

/* Next, check to see if period is near 1s (as it will be for SW cal) */
      if ( (2*(Sta->lSampsPerCyc+1) <= (long) (WaveHead->samprate + 
           WaveHead->samprate/5.)) &&
           (2*(Sta->lSampsPerCyc+1) >= (long) (WaveHead->samprate - 
           WaveHead->samprate/5.)) ) Sta->lSWSim++;

/* If first 3.5 cyc + 3s look like cal, it probably is */
      if ( Sta->lCycCnt <= 13 && Sta->lSWSim >= 18 )
      {
         Sta->iCal = 1;
         logit( "t", "cal on %s\n", Sta->szStation );
      }

      if ( !Sta->iCal )
      {
/* Compute full period in seconds*10 */
         Sta->lPer = (long) ((double) (2 * (Sta->lSampsPerCyc + 1)) / 
                     WaveHead->samprate * 10. + 0.0001);
					 
/* Max period is 3s (due to response curves) */
         if ( Sta->lPer > 30 ) Sta->lPer = 30;
			   
/* Min period is 0.3s. (This is done for compatibility with previous
   magnitude computations performed at WC/ATWC. It doesn't make much sense,
   but magnitudes are more accurate and consistent with this edict.) */
         if ( Sta->lPer < 3 ) Sta->lPer = 3;
			   
/* Reset cycle counter for Ml period/amplitudes if Mb has passed */
         if ( Sta->lCycCnt == Gparm->MbCycles ) Sta->dMaxPk = 0.;   
		 
/* Compare present ground motion amplitude to maximum, update max and mag
   params if needed */
         if ( MbMlGroundMotion( Sta, Sta->lPer, labs( Sta->lMDFRunning ) ) >=
              Sta->dMaxPk )
         {
            Sta->dMaxPk = MbMlGroundMotion( Sta, Sta->lPer,
                          labs( Sta->lMDFRunning ) );

/* If we are within the first MbCycles 1/2 cycles, get mb; else get Ml */
            if ( Sta->lCycCnt < Gparm->MbCycles )
            {
               Sta->lMbPer = Sta->lPer;
               Sta->dMbAmpGM = Sta->dMaxPk;
               Sta->dMbTime = WaveHead->starttime +
                              (double) iIndex/WaveHead->samprate;			   
/* Log pick */
               if ( Sta->iPickStatus == 3 ) ReportPick ( Sta, Gparm, Ewh );
            }

/* Fill Ml variables if past MbCycles */            
            else 
            {
               Sta->lMlPer = Sta->lPer;
               Sta->dMlAmpGM = Sta->dMaxPk;
               Sta->dMlTime = WaveHead->starttime +
                              (double) iIndex/WaveHead->samprate;			   
/* Log pick */
               if ( Sta->iPickStatus == 3 ) ReportPick( Sta, Gparm, Ewh );


			}       
         } 
      }
      else
      {
/* Reset calibration variables after 80 seconds */
         if ( WaveHead->starttime >= Sta->dTrigTime + 80. )
            {
               Sta->iPickStatus = 1;
               InitVar( Sta );
               return (-1);
            }
      }
   }
return (0);
}

