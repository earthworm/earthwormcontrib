B = $(EW_HOME)/$(EW_VERSION)/bin
L = $(EW_HOME)/$(EW_VERSION)/lib
N = $(EW_HOME)/atwc/src/libsrc


O = pick_wcatwc.o pick_v.o config.o stalist.o \
    report.o alarm.o magcomp.o \
    $L/kom.o $L/getutil.o $L/time_ew.o $L/chron3.o $L/logit.o \
    $L/transport.o $L/sleep_ew.o $L/swap.o $N/filters.o $N/mags.o $N/geotools.o \
    $N/complex.o

pick_wcatwc: $O
	cc -o $B/pick_wcatwc $O -lm -lposix4

lint:
	lint pick_wcatwc.c pick_v.c config.c stalist.c magcomp.c\
			report.c alarm.c \
			$(GLOBALFLAGS)



# Clean-up rules
clean:
	rm -f a.out core *.o *.obj *% *~

clean_bin:
	rm -f $B/pick_wcatwc*
