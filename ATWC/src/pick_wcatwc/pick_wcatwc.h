/******************************************************************
 *                     File pick_wcatwc.h                         *
 *                                                                *
 *  Include file for P-picker used at the West Coast/Alaska       *
 *  Tsunami Warning Center.  Made into Earthworm module 12/2000.  *
 ******************************************************************/

#include <trace_buf.h>
#include "\earthworm\atwc\src\libsrc\earlybirdlib.h"

/* Definitions
 *************/
#define PK_RESTART 1        /* Set when time series broken, picker restarted */
#define FIRST_MOTION_SAMPS 3/* Number of samples after trigger which must be in
                               same direction for a 1st motion to be declared */
#define RECENT_ALARM_TIME 900 /* # seconds late data can be and still run thru
                                 alarm */							   
#define GLOBAL_PICK_VERSION 1 /* NEIC global pick structure (message type) */
#define MAX_ALARM_STN      16 /* Max number of stations per region */

typedef struct {
   char     szRegionName[32];     /* Name this alarm region */
   int      iAlarmThresh;         /* Number of picks needed to call alarm */
   int      iNumPicksCnt;         /* Running total of picks */
   double   dMaxTime;             /* Max time in s allowed between all picks */
   double   dLastTime;            /* Time (1/1/70 s) of pick #1 */
   double   dThresh;              /* PStrength Threshold to exceed */
   int      iNumStnInReg;         /* Number of stations to use in region */
   char     szStation[MAX_ALARM_STN][TRACE_STA_LEN]; /* Station Name */
   char     szStnAlarm[MAX_ALARM_STN][TRACE_STA_LEN]; /* Stations alarmed */
} ALARMSTRUCT;

typedef struct {
   char     StaFile[64];          /* Name of file with SCN info */
   char     StaDataFile[64];      /* Station information file */
   char ATPLineupFileBB[64];      /* Optional command when used with ATPlayer */
   long     InKey;                /* Key to ring where waveforms live */
   long     OutKey;               /* Key to ring where picks will live */
   long     AlarmKey;             /* Key to ring where alarms will live */
   int      HeartbeatInt;         /* Heartbeat interval in seconds */
   int      MaxGap;               /* Maximum gap to interpolate */
   int      Debug;                /* If 1, print debug messages */
   double   HighCutFilter;        /* Bandpass high cut in hz */
   double   LowCutFilter;         /* Bandpass low cut in hz */
   unsigned char MyModId;         /* Module id of this program */
   double   LTASeconds;           /* Moving average length of time (seconds) */
   double   MinFreq;              /* Minimum P frequency (hz) of interest */
   int      MbCycles;             /* # 1/2 cycles after P Mb can be computed */
   int      LGSeconds;            /* # seconds after P in which max LG can be 
                                     computed for Ml (excluding 1st MbCycles) */
   int      MwpSeconds;           /* Max # seconds to evaluate P for Mwp */
   double   MwpSigNoise;          /* Auto-Mwp necessary signal-to-noise ratio */
   int      AlarmOn;              /* 1->Alarm function enabled, 0->Disabled */
   double   AlarmTimeout;         /* Time (sec) to re-start alarm after trig */
   int      AlarmTime;            /* If no data in this many secs, send alarm */
   int      TwoStnAlarmOn;        /* 1->multo station alarm on, 0->off */
   char     TwoStnAlarmFile[64];  /* File name with two station params */
   SHM_INFO InRegion;             /* Info structure for input region */
   SHM_INFO OutRegion;            /* Info structure for output region */
   SHM_INFO AlarmRegion;          /* Info structure for alarm output region */
} GPARM;

typedef struct {
   unsigned char MyInstId;        /* Local installation */
   unsigned char GetThisInstId;   /* Get messages from this inst id */
   unsigned char GetThisModId;    /* Get messages from this module */
   unsigned char TypeHeartBeat;   /* Heartbeat message id */
   unsigned char TypeError;       /* Error message id */
   unsigned char TypeAlarm;       /* Tsunami Ctr alarm message id */
   unsigned char TypePickTWC;     /* Tsunami Ctr P-picker message id */
   unsigned char TypePickGlobal;  /* NEIC format P-picker message id */
   unsigned char TypeWaveform;    /* Waveform buffer for data input */
} EWH;

/* Function declarations 
   *********************/
int     GetEwh( EWH * );                                 /* pick_wcatwc.c */
void    Interpolate( STATION *, char *, int );
int     ReadAlarmParams( ALARMSTRUCT **, int *, char * ); 
                                                         /* alarm.c */   
void    SeismicAlarm( STATION *, GPARM *, EWH *, TRACE_HEADER *, long * );

int     GetConfig( char *, GPARM * );                    /* config.c */
void    LogConfig( GPARM * );  
                                                         /* magcomp.c */ 
int     GetMbMl( STATION *, TRACE_HEADER *, int, GPARM *, EWH * );

void    CheckForAlarm( STATION *, ALARMSTRUCT [], int, GPARM *, EWH * );
void    MovingAvg( long, STATION *, long, long, long );  /* pick_v.c */
void    PickV( STATION *, char *, GPARM *, EWH *, long [], long *,
               ALARMSTRUCT [], int );
void    RawDataBuff( STATION * );
void    Reset( STATION * );

void    ReportPick( STATION *, GPARM *, EWH * );         /* report.c */
void    ReportPickGlobal( STATION *, GPARM *, EWH * );
void    ReportAlarm( STATION *, GPARM *, EWH *, int, char * );   
void    ReportNoDataAlarm( GPARM *, EWH * );
   
int     GetStaList( STATION **, int *, GPARM * );        /* stalist.c */
int     IsComment( char [] );                    
int     LoadStationData( STATION *, char * );
void    LogStaList( STATION *, int );

   
