#include "wfdisc.h"

typedef struct
{
	WFDISC30	wf;
	char		units[21];
} BigWfdisc;

typedef struct
{
	char		sta[7];
	char		chan[9];
	char		datatype[3];
	int		chanid;
	char		wfdisc_file[MAXPATHLEN+1];
	int		nsamp;
	float		samprate;
	double		time;
	int		count;
	int		deci_nsamp;
	int		deci;
	double		end_hour;
	BigWfdisc	w;
	FILE		*fp_dotw;
	FILE		*fp_wfdisc;
} Channel;
