/*
**	$Id: css_general.h 239 2006-10-03 15:53:57Z paulf $
**	$Source$
*/

#ifndef _CSS_GENERAL_INCLUDE
#define _CSS_GENERAL_INCLUDE

/*
 * general definitions, used by everyone
 */
#define EOS		'\0'		/* end of string */
#define ERR		-1		/* general error; often system error*/
#define ERR2            -2              /* application error, perror useless */
#define FALSE		0		/* general no */
#define NO		0		/* general no */
#define OK		0		/* okay exit */
#define TRUE		1		/* general yes */
#define YES		1		/* general yes */

/* lower-case string */
#define TOLCASE(S) { \
	register char	*xx_S; \
	for (xx_S = S;*xx_S;++xx_S) \
		if (isupper(*xx_S)) \
			*xx_S = tolower(*xx_S); \
}

/* upper-case string */
#define TOUCASE(S) { \
	register char	*xx_S; \
	for (xx_S = S;*xx_S;++xx_S) \
		if (islower(*xx_S)) \
			*xx_S = toupper(*xx_S); \
}

#endif !_CSS_GENERAL_INCLUDE
/* all additions should be above the preceding line */
