/*
**	$Header$
**	$Source$
*/

#define ISLEAP(yr)	(!(yr % 4) && yr % 100 || !(yr % 400))

struct date_time{
	double epoch;
	long date;
	int year;
	int month;
	char mname[4];
	int day;
	int doy;
	int hour;
	int minute;
	float second;
};
