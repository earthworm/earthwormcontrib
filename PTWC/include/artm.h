typedef struct
{
	char	name[100];
	time_t	mtime;
	off_t	length;
} FileList;


/* SUDS_STRUCTTAG:  Structure to identify structures when archived together  */

#define ST_MAGIC   'S'       /* magic character for sync in structtag        */

typedef struct {
   char  sync;              /* The letter S. If not present, error exists.
                                Use to unscramble damaged files or tapes.    */
   char  machine;           /* code for machine writing binary file for use
                                in identifying byte order and encoding.      */
   short id_struct;         /* structure identifier: numbers defined above  */
   int	 len_struct;        /* structure length in bytes for fast reading
                                and to identify new versions of the structure */
   int	 len_data;          /* length of data following structure in bytes  */
} SUDS_STRUCTTAG;

/* SUDS_DETECTOR:  Information on detector program being used                */

typedef struct {
   char    dalgorithm;       /* triggering algorithm: x=xdetect, m=mdetect
                                e=eqdetect                                   */
   char    event_type;       /* c=calibration, e=earthquake, E=explosion,
                                f=free run, n=noise, etc.                    */
   char    net_node_id[10];  /* network node identification                  */
   float   versionnum;       /* software version number                      */
   int     event_number;     /* unique event number assigned locally.        */
   int     spareL;           /* spare                                        */
} SUDS_DETECTOR;


/* SUDS_EQUIPMENT:  Equipment making up a station/component. Primarily used for
                    maintenance but may be referenced by researcher. One or more
                    structures exist for each piece of equipment making up a
                    station/component.                                       */

/* SUDS_MUXDATA:  Header for multiplexed data                                */

typedef struct {
   char	  netname[4];       /* network name                                 */
   double begintime;        /* time of first data sample                    */
   short  loctime;          /* minutes to add to GMT to get local time      */
   short  numchans;         /* number of channels: if !=1 then multiplexed  */
   float  dig_rate;         /* samples per second                           */
   char   typedata;         /* s=short(16 bit), r=12 bit data, 4 lsb time,
                                l=long(32 bit), f=float,
                                d=double, c=complex, v=vector, t=tensor      */
   char   descript;         /* g=good, t=telemetry noise, c=calibration, etc*/
   short  spareG;           /* spare                                        */
   int    numsamps;         /* number of sample sweeps. Typically not known
                                when header is written, but can be added later*/
   int    blocksize;        /* number of demultiplexed samples per channel if
                                data is partially demultiplexed, otherwise=0 */
} SUDS_MUXDATA;


/* SUDS_STATIDENT:  Station identification.                                  */

typedef struct {           /* station component identifier                 */
   char  network[4];       /* network name                                 */
   char  st_name[5];       /* name of station where equipment is located   */
   char  component;        /* component v,n,e                              */
   short inst_type;        /* instrument type                              */
} SUDS_STATIDENT;


/* SUDS_STATIONCOMP:  Generic station component information                  */

typedef struct {
   SUDS_STATIDENT sc_name;   /* station component identification             */
   short   azim;             /* component azimuth clockwise from north,
                                     0 for vertical                          */
   short   incid;            /* component angle of incidence from vertical
                                     0 is vertical, 90 is horizontal         */
   double  st_lat;           /* latitude, north is plus                      */
   double  st_long;          /* longitude, east is plus                      */
   float   elev;             /* elevation in meters                          */
   char    enclosure;        /* d=dam, n=nuclear power plant, v=underground
                                     vault, b=buried, s=on surface, etc.     */
   char    annotation;       /* annotated comment code                       */
   char    recorder;         /* type device data recorded on                 */
   char    rockclass;        /* i=igneous, m=metamorphic, s=sedimentary      */
   short   rocktype;         /* code for type of rock                        */
   char    sitecondition;    /* p=permafrost, etc.                           */
   char    sensor_type;      /* sensor type: d=displacement, v=velocity,
                                a=acceleration, t=time code                  */
   char    data_type;        /* s=short(16 bit), r=12 bit data, 4 lsb time,
                                l=long(32 bit), f=float,
                                d=double, c=complex, v=vector, t=tensor      */
   char    data_units;       /* data units: d=digital counts, v=millivolts,
                                n=nanometers (/sec or /sec/sec)              */
   char    polarity;         /* n=normal, r=reversed                         */
   char    st_status;        /* d=dead, g=good                               */
   float   max_gain;         /* maximum gain of the amplifier                */
   float   clip_value;       /* +-value of data where clipping begins        */
   float   con_mvolts;       /* conversion factor to millivolts: mv per counts
                                     0 means not defined or not appropriate
                                max_ground_motion=digital_sample*con_mvolts*
                                max_gain                                     */
   short   channel;          /* a2d channel number                           */
   short   atod_gain;        /* gain of analog to digital converter          */
   short   effective;        /* date/time these values became effective      */
   float   clock_correct;    /* clock correction in seconds.                 */
   float   station_delay;    /* seismological station delay.                 */
} SUDS_STATIONCOMP;
