/*
 ************   ew_ptwc.h   ********************
Include file declaring the routines below:
*************************************************
*/

/* routine prototypes for use by main line calling program
**********************************************************/
int   start_ew_data (			/* called at startup */
		int ncomponents, 	/* number of entries in following three arrays */
		char* sta[], 		/* array of station names to get */
		char* chan[], 		/* ditto component names */
		char* net[], 		/* ditto network names */
		char* configFile,	/* to reach our config file */
		int*  maxSamples);	/* largest number of samples per message, ever. */
					
int   get_ew_data (			/* called to supply data */
		   TRACE_HEADER* pev,	/* pointer to EVENTINFO structure (rcv.h) */
	 	   long* pewData,
		   long maxData);	/* limit of # of points to stuff */

int   stop_ew_data (VOID);	/* called to gracefully shut the whole thing off */

void  err_ew_ptwc( 
		  short ierr,		/* external error routine for use by our */ 
		  char *note );		/* main program */

/* Error codes used by ew_ptwc.c and it's main line program
***********************************************************/
#define  ERR_MISSMSG       0   /* message missed in transport ring       */
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer     */
#define  ERR_NOTRACK       2   /* msg retreived; tracking limit exceeded */
#define  ERR_QUEUE         3   /* error queueing message for sending     */
#define  ERR_BAD_PACKET    4   /* unrecognized data format               */
#define  ERR_PACKET_SIZE   5   /* Packet larger than supported in ptwc code */
#define  ERR_QUEUE_BROKEN  6   /* memory fifo seriously broken           */

/* Return codes used by ew_ptwc.c and it's main line program
***********************************************************/
#define  RET_OK         1    /* normal completion        */
#define  RET_QUIT       -1   /* shutdown requested       */
#define  RET_TOO_LARGE  -2   /* trace has too many bytes */
#define  RET_BAD_TYPE   -3   /* unknown data format      */


