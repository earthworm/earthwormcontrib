#ifndef _CHANNELS_H
#define _CHANNELS_H


#define CHANNELS_DEFAULT_PORT 5050

typedef int (*ReceiveData)();

extern int channels_init();
extern int channels_connect();
extern int channels_init_and_connect();
extern int channels_accept();
extern int channels_send();
extern int channels_available();
extern int channels_close();

extern int channels_socket;
extern int channels_port;
extern char channels_host[];

typedef struct
{
	char	sta[10];
	char	chan[10];
} StaChan;

typedef struct
{
	char	addr[64];
	char	name[32];
	int	pid;
	int	n_insc;
	int	n_outsc;
	StaChan	*insc;
	StaChan	*outsc;
} ClientList;

typedef struct
{
	char	sta[10];
	char	chan[10];
	int	npts;
	int	*data;
	double	*time;
} DataStruct;

#endif
