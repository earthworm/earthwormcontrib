/*
This is the coupler module to go from Earthworm trace data to PTWC
decimate().  The purpose of this program is to take specified trace
data from an Earthworm ring, reformat it, and present it to decimate(),
the PTWC intake for trace data.

Uses the Earthworm coupler routines in ew_ptwc.c.

Briefly, it reads parameters from its command line (including the names
of the configuration files), connects to the PTWC end of things,
connects to the Earthworm ring, and starts shovelling data.

It reads  the PTWC side (which includes the names of the stations to be
moved, and what to call them), calls init_com(). It then reads the
Earthworm configuration file, and calls the earthworm initializer
start_ew_data(). If all goes well, it picks specified trace messages
from the Earthworm ring, and places them into the maws of decimate().


NOTES:

On the Earthworm side the station naming convention is via three ascii
string, Station, Component, and Network (SCN), ala IRIS.

The Earthworm-side routines are multi-threaded. The program must be linked
with multi-thread libraries. It is fervently believed that no unsafe
routines are called.

Both files (this one, and ew_ptwc.c) contain a compile-time DEBUG variable
for controlling some debug writes and logs.

The Earthworm side uses the Earthworm logging feature, which creates daily
log files in the Earthworm /run/log directory. Beware, these files must
be managed.

Alex Bittenbinder, Nov 98

May 12 99:
Installing the OVERLAP TRAP to see if we're sending ovelapping trace data
to decimate(). All additions for this are  tagged with the characters
"OVERLAP TRAP". Should be removed after the experiment is over. Alex

*/

#define DEBUG 1	/* this is a switch ( 0 or 1) which turns some handy debug 
		   writes on and off (1=>on) */

#include <stdio.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <math.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <trace_buf.h>
#include "util.h"
#include "cssio.h"
#include "artm.h"
#include "channels.h"
#include "ew_ptwc.h"

#define False	0
#define True	1

#define MAX_CHANNELS 1000
#define STALEN 6
#define CHANLEN 8
#define NETLEN 8
int	hours_per_dotw, archive;

extern	int	init_com_timeout;
extern	int	port;
extern	char	channels_host[1024];
extern	int	init_com_timeout;
extern	int	port;
extern	char	channels_host[1024];

	char*	dataDirEnvVarName = "DATA_DIR";	/* the name of the environment variable of where to put the data */
	char*	data_dir;
static	char	ertm_setup[1024], archive_str[20];
	char	ew_config[1024];

static	char*	sta[MAX_CHANNELS];
static	char*	chan[MAX_CHANNELS];
static	char*	net[MAX_CHANNELS];
	char	line[1024];
      	char    *program = "ertm";
	char	dsta[STALEN+1], dchan[CHANLEN+1], dnet[NETLEN+1];
static	char*	datatype[MAX_CHANNELS],s2[]="s2", s3[]="s3", s4[]="s4";
FILE*		fp;
static	int     nsamp[MAX_CHANNELS];
static  double  samprate[MAX_CHANNELS], begin_time[MAX_CHANNELS];
static  double  begin_time_last[MAX_CHANNELS];	/*used for OVERLAP TRAP only */
static  double  slop;				/*used for OVERLAP TRAP only */
static  int	trap_dump_count; 		/*used for OVERLAP TRAP only */
static  double  prime_time;
static  char*	pdata[MAX_CHANNELS];

void main(argc, argv)
	int	argc;
	char**	argv;
{
	int	iret;
static	int	nsta  = 0;
        int 	keepup = 1;
        int 	retry = 1;
	int	status, count;
	int	i, j, k, n;
static  int gotdata;
	char*	sc;
	char	filename[MAXPATHLEN+1];
	char	datafile[MAXPATHLEN+1];
static	char	*c;
	char*	getenv();
	TRACE_HEADER	ewPacketHeader;  /* from trace_buf.h */
	struct	rlimit rlp;

	long*	ewData; 	/* will point to malloc'd buffer which 
				 * will hold the samples of one
				 * trace data message */

	int	maxSamples;	/* The size of the above area in bytes */
        struct timeval  tp;     /* time since epoch */
        void    *ptr; 

/* Introduce yourself
*********************/
printf("%s: Today's Date is Jan 14, 1999\n",program);

/*
 *checking environment variables
 */

/* get Channels port
 */


/*
 * Get command line parameters
 */

  setarg(argc, argv);
  if(!getarg("port", "%d", &port))
  {
     if((c = getenv("CHANNELS_PORT")) != NULL && strlen(c) > 0)
     {
	sscanf(c, "%d", &port);
     }
     else
     {
	fprintf(stderr, "%s: \n",program);
        fprintf(stderr, "     port not declared on command line and \n"); 
        fprintf(stderr, "     environment variable CHANNELS_PORT not set\n");
	exit(1);
     }
  }
  if(!getarg("host", "%s", channels_host))
  {
	if((c = getenv("CHANNELS_HOST")) != NULL && strlen(c) > 0)
	{
		strcpy(channels_host, c);
	}
	else
	{
		gethostname(channels_host, 1024);
	}
  }

  if(!getarg("setup", "%s", ertm_setup) || !getarg("ew_setup", "%s", ew_config) )
  {
	fprintf(stderr, "ertm: missing arguments.\n");
	fprintf(stderr,
	    "usage: ertm setup= ew_setup= [port=] [host=] [archive=(true,false)]\n");
	fprintf(stderr,
	    "      setup is the complete path for the setup file\n");
	fprintf(stderr,
	    "      ew_setup is the complete path for the EW config file\n");
	fprintf(stderr,
	    "      port is the port number for the target DataServer\n");
	fprintf(stderr,
	    "      host is the host name for port\n");
	fprintf(stderr,
	    "      archive controls archiving to local database\n");
        fflush(stdout);
	exit(1);
  }

/* ascii file that maps Earthworm station/channel names to PTWC names */
  printf("%s: Using setup file  %s\n", program, ertm_setup);

/* ascii file that contains Earthworm-specific configuration information */
  printf("%s: Using ew_config file  %s\n",program, ew_config);

  strcpy(archive_str, "false");
  getarg("archive", "%s", archive_str);
  if(!strcasecmp(archive_str, "true")) 
    {
	archive = 1;
    }
  else 
    {
	archive = 0;
    }
  fprintf(stderr, "%s: Archiving set to %s\n",program, archive_str);
		/* if true on-line archiving turned on, defaults to off*/
  fflush(stdout);


/* increase number of files which can be open to the system maximum 
*******************************************************************/
getrlimit(RLIMIT_NOFILE, &rlp);
rlp.rlim_cur = rlp.rlim_max;
setrlimit(RLIMIT_NOFILE, &rlp);

/* set the default data type = s4 (4-byte integers) 
***************************************************/
for(i=0; i<MAX_CHANNELS; i++) datatype[i]=s4;


/* Open and read the PTWC setup file
************************************/
if((fp = fopen(ertm_setup, "r")) == NULL)
	{
	fprintf(stderr, "%s: cannot open %s\n", program, ertm_setup);
	fflush(stderr);
	exit(1);
	}

nsta=0;
k=0;
while(fgets(line, 1023, fp) != NULL)
	{
	if(line[0] != '#' && sscanf(line, "%s %s %*s %*s %*s %*s %*s %*s %s",dsta, dchan,dnet ) == 3)  
		{
		sta[k]=(char*)malloc(sizeof(char)*(STALEN+1));
		if (sta[k] == NULL)
			{ 
			printf("Can't malloc sta. Exiting\n");
			exit(1);
			}
		strncpy(sta[k],dsta,STALEN+1);

		chan[k]=(char*)malloc(sizeof(char)*(CHANLEN+1));
		if (chan[k] == NULL)
			{ 
			printf("Can't malloc chan. Exiting\n");
			exit(1);
			}
		strncpy(chan[k],dchan,CHANLEN+1);

		net[k]=(char*)malloc(sizeof(char)*(NETLEN+1));
		if (net[k] == NULL)
			{ 
			printf("Can't malloc net. Exiting\n");
			exit(1);
			}
		strncpy(net[k],dnet,NETLEN+1);

		k++;
		nsta=k;
		}
	}

    fclose(fp);
/* Dump out stuff from ptwc initialization file
***********************************************/
if(DEBUG)
	{
	printf("From file %s:  \nnsta=%d\n",ertm_setup,nsta);
	for (i=0;i<nsta;i++)
		{
		printf(".%s. .%s. .%s.\n",sta[i],chan[i],net[i]);
		}
	}

/* Call the PTWC-side initializer
********************************/
init_com();

/* Call the Earthworm-side initializer 
 *
 * Read our EW configuration file, initialize: attach to the message ring, 
 * start logging, set up the fifo, start the getter-thread, start data reception.
 * NOTE:
 *    This routine will set the value for maxSamples, which is used by the 
 *    PTWC-side stuff. This will be obtained from the MaxMsgSize parameter 
 *    in the Earthworm-side configuration file: EW delivers 4-byte integers, 
 *    converting 16-byte data to 4-byte data before delivering to 'decimate' code.
 */
iret= start_ew_data(nsta, sta, chan, net, ew_config, &maxSamples); 
/* sta, and chan are the SCN names obtained from EW. */
if (iret != RET_OK)
	{
	printf("Earthworm initializer start_ew_data() complained: %d. Exiting.\n",iret);
	fflush(stderr);
	exit(1);
	}

/*  Set the directory where data will be written 
************************************************/
  data_dir = getenv(dataDirEnvVarName);
  if(data_dir == NULL)
  {
        logit("e", "ertm: environment variable DATA_DIR not set. Exiting\n");
        exit(1);
  }
/*  the number of hours that a .w will be in length */
  if((c = getenv("HOURS_PER_DOTW")) == NULL)
  {
        fprintf(stderr, "%s: environment variable HOURS_PER_DOTW not set.\n",program);
	fflush(stderr);
        exit(1);
  }
  else if(sscanf(c, "%d", &hours_per_dotw) != 1 || hours_per_dotw <= 0
	|| hours_per_dotw > 24)
  {
	fprintf(stderr, "%s: invalid hours_per_dotw: %s\n", program,c);
	fflush(stderr);
	exit(1);
  }




/* Allocate transfer buffer
***************************/
ewData = (long*)malloc(sizeof(long)*maxSamples);
if (ewData == NULL)
	{ 
	logit("e","Can't malloc ewData. Exiting\n");
	stop_ew_data(); /* detach from the Earthworm ring, etc */
	exit(1);
	}

/*
 * nsta {int} = number of station/channel pairs total
 * sta {pointer to char array} = station names from index 0 to nsta
 * chan {pointer to char array} = channel names from index 0 to nsta
 * nsamp {pointer to int array} = number of samples per sta/chan pair in array
 * samprate {pointer to double array} = samples per second for each 
 *                                   sta/chan pair in array
 * time {pointer to a double array} = epochal time of first sample
 * pdata {pointer to an array of char pointers containing the data} =
 *          pdata[j] where j is the index of the station/channel pair
 *          and pdata points to the memory location of the data array.
 * datatype {char **datatype} = pointer to array of char pointers,
 *          valid entries are "s2", "s3", "s4", and "t4".
 * data_dir {char *data_dir} = path pointed to by environment variable
 *          "DATA_DIR".
 * ertm_setup {char *ertm_setup} = ertm setup file, declared on command line
 */ 

/* Tell decimate about the stations it's about to get
 *
 * To do this, we're going to send it, a 1 hour old
 * one-sample packet for each station/channel pair. 
 */

for (i=0;i<nsta;i++) nsamp[i]=0;

        if (gettimeofday(&tp,ptr))
        {
                perror("gettimeofday failed");
                exit(1);
        }
        else
        {
               prime_time=(double)(tp.tv_sec-3600);
        }

/* initialize with a single datum for each channel */
for (i=0;i<nsta;i++) 
	{
	int ret;
	nsamp[i]=1; if(i>0)nsamp[i-1]=0; /* look at this one */
	samprate[i]=20.;		 /* arbitrary for priming the pump */
	begin_time[i]=prime_time; 	 /* set to current time minus 3600 sec.*/
	begin_time_last[i]=prime_time; 	 /* for OVERLAP TRAP only */
	pdata[i]=(char*)ewData;		 /*  it exists */
	*ewData=1;			 /* arbitrary */
	
	if(DEBUG) logit("","calling decimate init. call %d\n",i);
/*
	ret=decimate(nsta, sta, chan, nsamp, samprate, begin_time, 
                    (char**)pdata, datatype, data_dir, ertm_setup,archive);
 */
	}

/* announce that we're doing the OVERLAP TRAP
*********************************************/
logit("et","This is the version which does overlap trapping\n");
trap_dump_count=0;

/* Top of working loop of drinking trace data from the Earthworm ring
*********************************************************************/
while (1) 	/* loop until get_ew_data() complains */
	{
	
	status = get_ew_data( &ewPacketHeader, ewData, maxSamples );
	if (status == RET_OK)
		{
		/* update only the element in the sta/chan arrays for the current 
	 	   packet received, all others set nsamp equal to 0. */
		for (i = 0; i < nsta; i++)
			{
			if( !strcmp(sta[i],  ewPacketHeader.sta ) && 
			    !strcmp(chan[i], ewPacketHeader.chan) &&
			    !strcmp(net[i],  ewPacketHeader.net )   )
				{
				nsamp[i]=ewPacketHeader.nsamp;
				samprate[i]=ewPacketHeader.samprate;
				begin_time[i]=ewPacketHeader.starttime;
				pdata[i]=(char *)ewData;
                                if(nsamp[i] > 0) 
                                   {
                                   gotdata=1;
                                   }
				}
			else
				{
				nsamp[i]=0;
				} 
			}
		if(init_com_timeout) init_com(); /* if we still haven't connected */
                   if(gotdata)
                    {
			/* OVERLAP TRAP! Alex, 5/12/99
			   To log any overlaps in the time series being fed to decimate. */
		       for (i = 0; i < nsta; i++)
			  {
                            if (nsamp[i]>0) break;
                          }
 
			slop = samprate[i]/2; /* How much jitter do we allow before we scream */
			if( begin_time[i] < begin_time_last[i] + (double)nsamp[i]/samprate[i] - slop)
			{
				logit("et","overlap detected: %s %s %s %lf %d %lf\n", sta[i],chan[i],net[i], begin_time_last[i], nsamp[i], begin_time[i]);
				trap_dump_count++;
			} else {
				if (trap_dump_count < 100) logit("et","overlap NOT detected: %s %s %s %lf %d %lf\n", sta[i],chan[i],net[i], begin_time_last[i], nsamp[i], begin_time[i]);
				trap_dump_count++;
                        }
				begin_time_last[i]=begin_time[i];
	    	        if(decimate(nsta, sta, chan, nsamp, samprate, begin_time, 
                          (char**)pdata, datatype, data_dir, ertm_setup, archive) <= -2)
			{
			   stop_ew_data(); /* detach from the Earthworm ring, etc */
			   exit(1);
			}
                    }
		continue; 
	       	}
	if (status == RET_QUIT)
		{ 
		printf("%s shutting down on request from get_ew_data(): %d\n",program, status);
		stop_ew_data(); /* detach from the Earthworm ring, etc */
		exit(1);
		}
	if (status == RET_TOO_LARGE)
		{ 
		printf("%s packet length from get_ew_data too big (max=%d)\n",program, maxSamples);
		printf("Exiting.");
		stop_ew_data(); /* detach from the Earthworm ring, etc */
		exit(1);
		}
	else
		{
		printf("%s: WARNING! bad packet received (%d). See ew log file\n",program,status);
		}
		
	}

}

