/*
This is the coupler module to go from Earthworm trace data to PTWC
decimate().  The purpose of this program is to take specified trace
data from an Earthworm ring, reformat it, and present it to decimate(),
the PTWC intake for trace data.

Based on irtm, as supplied by Bob Cessaro. Uses the Earthworm coupler
routines in ew_ptwc.c.

Briefly, it reads parameters from its command line (including the names
of the configuration files), connects to the PTWC end of things,
connects to the Earthworm ring, and starts shovelling data.

It reads  the PTWC side (which includes the names of the stations to be
moved, and what to call them), calls init_com(). It then reads the
Earthworm configuration file, and calls the earthworm initializer
start_ew_data(). If all goes well, it picks specified trace messages
from the Earthworm ring, and places them into the maws of decimate().


NOTES:

On the Earthworm side the station naming convention is via three ascii
string, Station, Component, and Network (SCN), ala IRIS.

The Earthworm-side routines are multi-threaded. The program must be linked
with multi-thread libraries. It is fervently believed that no unsafe
routines are called.

Both files (this one, and ew_ptwc.c) contain a compile-time DEBUG variable
for controlling some debug writes and logs.

The Earthworm side uses the Earthworm logging feature, which creates daily
log files in the Earthworm /run/log directory. Beware, these files must
be managed.

Alex Bittenbinder, Nov 98

PS: Bob: I'm shipping this to you with a bunch of stuff stubbed off so I
could run it here. Besides the usual nightmare with the make file, I've
tweaked several things in this file. These are marked with the search tag
" DANGER: REMOVE BEFORE FLIGHT ".
		Alex 12/14/98

*/

#define DEBUG 1	/* this is a switch ( 0 or 1) which turns some handy debug 
		   writes on and off (1=>on) */

#include <stdio.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <math.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <trace_buf.h>
#include "util.h"
#include "cssio.h"
#include "artm.h"
#include "channels.h"
#include "ew_ptwc.h"

#define False	0
#define True	1

#define MAX_CHANNELS 1000
#define STALEN 6
#define CHANLEN 8
#define NETLEN 8
int	hours_per_dotw, archive;

extern	int	init_com_timeout;
extern	int	port;
extern	char	channels_host[1024];
extern	int	init_com_timeout;
extern	int	port;
extern	char	channels_host[1024];

	char*	dataDirEnvVarName = "DATA_DIR";	/* the name of the environment variable of where to put the data */
	char*	data_dir;
static	char	ew2rtm_setup[1024], archive_str[20];
	char	ew_config[1024];

static	char*	sta[MAX_CHANNELS];
static	char*	chan[MAX_CHANNELS];
static	char*	net[MAX_CHANNELS];
	char	line[1024];
	char	dsta[STALEN+1], dchan[CHANLEN+1], dnet[NETLEN+1];
	char*	program = "ew2rtm";
static	char*	datatype[MAX_CHANNELS],s2[]="s2", s3[]="s3", s4[]="s4";
FILE*		fp;
static	int     nsamp[MAX_CHANNELS];
static  double  samprate[MAX_CHANNELS], begin_time[MAX_CHANNELS];
static  double  prime_time;
static  char*	pdata[MAX_CHANNELS];

void main(argc, argv)
	int	argc;
	char**	argv;
{
	int	iret;
static	int	nsta  = 0;
        int 	keepup = 1;
        int 	retry = 1;
	int	status, count;
	int	i, j, k, n;
	char*	sc;
	char	filename[MAXPATHLEN+1];
	char	datafile[MAXPATHLEN+1];
	char*	c;
	char*	getenv();
	TRACE_HEADER	ewPacketHeader;  /* from trace_buf.h */
	struct	rlimit rlp;

	long*	ewData; 	/* will point to malloc'd buffer which 
				 * will hold the samples of one
				 * trace data message */

	int	maxSamples;	/* The size of the above area in bytes */
        struct timeval  tp;     /* time since epoch */
        void    *ptr; 

/* Introduce yourself
*********************/
printf("ew2rtm: Version 1.0 Nov 98\n");

/* Get command line parameters
******************************/
for(i = 1; i < argc; i++)
	{
	if(!strncmp(argv[i], "port=", 5))
		{
		sscanf(argv[i]+5, "%d", &port);
		}
	else if(!strncmp(argv[i], "host=", 5))
		{
		sscanf(argv[i]+5, "%s", channels_host);
		}
	else if(!strncmp(argv[i], "setup=", 6))
		{
		sscanf(argv[i]+6, "%s", ew2rtm_setup);
	        fprintf(stderr, "%s: Using setup file  %s\n",program, ew2rtm_setup);
		/* ascii file that maps Earthworm station/channel names to PTWC names */
		}
	else if(!strncmp(argv[i], "archive=", 8))
		{
		sscanf(argv[i]+8, "%s", archive_str);
	        fprintf(stderr, "%s: Archiving set to %s\n",program, archive_str);
		/* if true on line archiving turned on, defaults to off*/
                if(!strcasecmp(archive_str, "true")) 
                  {
                      printf("archive on\n");
                      archive = 1;
                  }
                  else 
                  {
                      printf("archive off\n");
                      archive = 0;
                  }

		}
	else if(!strncmp(argv[i], "ew_setup=", 9))
		{
		sscanf(argv[i]+9, "%s", ew_config);
	        fprintf(stderr, "%s: Using ew_config file  %s\n",program, ew_config);
		/* ascii file that contains Earthworm-specific stuff */
		}
	}


if(channels_host[0] == '\0')
	{
	if((c = getenv("CHANNELS_HOST")) != NULL && strlen(c) > 0)
		{ strcpy(channels_host, c);}
	else
		{gethostname(channels_host, 1024);}
  	}

/* increase number of files which can be open to the system maximum 
*******************************************************************/
getrlimit(RLIMIT_NOFILE, &rlp);
rlp.rlim_cur = rlp.rlim_max;
setrlimit(RLIMIT_NOFILE, &rlp);

/* set the default data type = s4 (4-byte integers) 
***************************************************/
for(i=0; i<MAX_CHANNELS; i++) datatype[i]=s4;


/* Open and read the PTWC setup file
************************************/
if((fp = fopen(ew2rtm_setup, "r")) == NULL)
	{
	fprintf(stderr, "%s: cannot open %s\n", program, ew2rtm_setup);
	fflush(stderr);
	exit(1);
	}

nsta=0;
k=0;
while(fgets(line, 1023, fp) != NULL)
	{
	if(line[0] != '#' && sscanf(line, "%s %s %*s %*s %*s %*s %*s %*s %s",dsta, dchan,dnet ) == 3)  
		{
		sta[k]=(char*)malloc(sizeof(char)*(STALEN+1));
		if (sta[k] == NULL)
			{ 
			printf("Can't malloc sta. Exiting\n");
			exit(1);
			}
		strncpy(sta[k],dsta,STALEN+1);

		chan[k]=(char*)malloc(sizeof(char)*(CHANLEN+1));
		if (chan[k] == NULL)
			{ 
			printf("Can't malloc chan. Exiting\n");
			exit(1);
			}
		strncpy(chan[k],dchan,CHANLEN+1);

		net[k]=(char*)malloc(sizeof(char)*(NETLEN+1));
		if (net[k] == NULL)
			{ 
			printf("Can't malloc net. Exiting\n");
			exit(1);
			}
		strncpy(net[k],dnet,NETLEN+1);

		k++;
		nsta=k;
		}
	}

    fclose(fp);
/* Dump out stuff from ptwc initialization file
***********************************************/
if(DEBUG)
	{
	printf("From file %s:  \nnsta=%d\n",ew2rtm_setup,nsta);
	for (i=0;i<nsta;i++)
		{
		printf(".%s. .%s. .%s.\n",sta[i],chan[i],net[i]);
		}
	}

/* Call the PTWC-side initializer
********************************/
init_com();

/* Call the Earthworm-side initializer 
**************************************/
/* read our configuration file, set up shop: attach to the message ring, start logging, set up the fifo,
   start the getter-thread, start drinking. And put out the cat. 
	NOTE:
	This routine will set the value for maxSamples, which is used by the PTWC-side stuff. 	This will
	be derived from the MaxMsgSize parameter in the Earthworm-side configuration file: we're to deliver
	4-byte integers, but we may be getting messages with 16-byte data.
*/
iret= start_ew_data(nsta, sta, chan, net, ew_config, &maxSamples); /* sta, and chan are the SCN names we're to get. */
if (iret != RET_OK)
	{
	printf("Earthworm initializer start_ew_data() complained: %d. Exiting.\n",iret);
	exit(1);
	}

/*  Set the directory where data will be written 
************************************************/
  data_dir = getenv(dataDirEnvVarName);
  if(DEBUG)logit(""," environment variable DATA_DIR len:%d string: .%s.\n",strlen(data_dir),data_dir);
  if(data_dir == NULL)
  {
        logit("e", "ew2rtm: environment variable DATA_DIR not set. Exitting\n");
        exit(1);
  }
/*  the number of hours that a .w will be in length */
  if((c = getenv("HOURS_PER_DOTW")) == NULL)
  {
        fprintf(stderr, "      ertm: environment variable HOURS_PER_DOTW not set.\n");
        exit(1);
  }
  else if(sscanf(c, "%d", &hours_per_dotw) != 1 || hours_per_dotw <= 0
	|| hours_per_dotw > 24)
  {
	fprintf(stderr, "      ertm: invalid hours_per_dotw: %s\n", c);
	exit(1);
  }




/* Allocate transfer buffer
***************************/
ewData = (long*)malloc(sizeof(long)*maxSamples);
if (ewData == NULL)
	{ 
	logit("e","Can't malloc ewData. Exiting\n");
	stop_ew_data(); /* detach from the Earthworm ring, etc */
	exit(1);
	}

/*
 * nsta {int} = number of station/channel pairs total
 * sta {pointer to char array} = station names from index 0 to nsta
 * chan {pointer to char array} = channel names from index 0 to nsta
 * nsamp {pointer to int array} = number of samples per sta/chan pair in array
 * samprate {pointer to double array} = samples per second for each 
 *                                   sta/chan pair in array
 * time {pointer to a double array} = epochal time of first sample
 * pdata {pointer to an array of char pointers containing the data} =
 *          pdata[j] where j is the index of the station/channel pair
 *          and pdata points to the memory location of the data array.
 * datatype {char **datatype} = pointer to array of char pointers,
 *          valid entries are "s2", "s3", "s4", and "t4".
 * data_dir {char *data_dir} = path pointed to by environment variable
 *          "DATA_DIR".
 * ew2rtm_setup {char *ew2rtm_setup} = path pointed to by environment variable
 *          "IRTM_SETUP".
 */ 

/* Tell decimate about the stations its about to get
****************************************************/
/* To do this, we're going to send it, for each channel,
   an ancient one-sample packet for each component. */

for (i=0;i<nsta;i++) nsamp[i]=0; /* careful: decimate is aroused by the sight of a non-zero nsamp */

        if (gettimeofday(&tp,ptr))
        {
                perror("gettimeofday failed");
                exit(1);
        }
        else
        {
               prime_time=(double)(tp.tv_sec-3600);
        }
   if (DEBUG) logit("e","we2rtm made it past here 2 prime_time: %f\n",prime_time);

/* initialize with a single datum for each channel */
for (i=0;i<nsta;i++) 
	{
	int ret;
	nsamp[i]=1; if(i>0)nsamp[i-1]=0; /* look at this one */
	samprate[i]=20.;		/* anything, really. */
	begin_time[i]=prime_time; 	/* use current time minus 3600 sec.*/
	pdata[i]=(char*)ewData;		/*  it exists */
	*ewData=13;			/* as an eye-catcher */
	if(DEBUG) logit("","calling decimate init. call %d\n",i);
	/*ret=decimate(nsta, sta, chan, nsamp, samprate, begin_time, (char**)pdata, datatype, data_dir, ew2rtm_setup,archive);*/
	if(DEBUG) logit("","return from dummy decimate: %d\n",ret);	
	}

/* Top of working loop of drinking trace data from the Earthworm ring
*********************************************************************/
while (1) 	/* loop until get_ew_data() complains */
	{
	status = get_ew_data( &ewPacketHeader, ewData, maxSamples );
	if (status == RET_OK)
		{
		if(DEBUG) logit("","from get_ew_data: %s %s %s\n",ewPacketHeader.sta, ewPacketHeader.chan, ewPacketHeader.net);
		/* update only the element in the sta/chan arrays for the current 
	 	   packet received, all others set to nsamp equal to 0. */
		for (i = 0; i < nsta; i++)
			{
			if( !strcmp(sta[i],  ewPacketHeader.sta ) && 
			    !strcmp(chan[i], ewPacketHeader.chan) &&
			    !strcmp(net[i],  ewPacketHeader.net )   )
				{
				nsamp[i]=ewPacketHeader.nsamp;
				samprate[i]=ewPacketHeader.samprate;
				begin_time[i]=ewPacketHeader.starttime;
				pdata[i]=(char *)ewData;
				}
			else
				{
				nsamp[i]=0;
				} 
			}
		if(init_com_timeout) init_com(); /* if we still haven't connected */

	    	if(decimate(nsta, sta, chan, nsamp, samprate, begin_time, (char**)pdata, datatype, data_dir, ew2rtm_setup, archive) <= -2)
			{
			stop_ew_data(); /* detach from the Earthworm ring, etc */
			exit(1);
			}
		continue; 
	       	}
	if (status == RET_QUIT)
		{ 
		printf("%s shutting down on request from get_ew_data(): %d\n",program, status);
		stop_ew_data(); /* detach from the Earthworm ring, etc */
		exit(1);
		}
	if (status == RET_TOO_LARGE)
		{ 
		printf("%s packet length from get_ew_data too big (max=%d)\n",program, maxSamples);
		printf("Exiting.");
		stop_ew_data(); /* detach from the Earthworm ring, etc */
		exit(1);
		}
	else
		{
		printf("%s: WARNING! bad packet received (%d). See ew log file\n",program,status);
		}
		
	}

}

