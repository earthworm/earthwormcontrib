#include <stdio.h>
#include <sys/time.h>
struct timeval  tp;     /* time since epoch */
struct timezone tzp;    /* time zone */

void main(argc, argv)
        int     argc;
        char**  argv;
{
        void    *ptr;
        if (gettimeofday(&tp,ptr))
        {
                perror("gettimeofday failed");
                exit(1);
        }
        else
        {
               printf("Time of day %lf \n",(double)(tp.tv_sec));
               printf("Time of day %ld \n",tp.tv_sec);
        }
}

