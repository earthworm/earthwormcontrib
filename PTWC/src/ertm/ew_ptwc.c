/*
 ************   ew_ptwc.c   ********************
By Alex Bittenbinder, 11/23/98

	These are three support routines to take specified trace data,
from an Earthworm ring, buffer it, and supply a chunk of it whenever a
working routine is called. Includes three routines:

		start_ew_data(), get_ew_data(), and stop_ew_data().

	Much code and inspiration taken from the Earthworm-VDL project and work
	done at ATWC.

Warning: MULTI-THREAD DANGER: gmtime is used; it's not multi-thread safe. 
 	 It's ok as long as no other thread uses it. There should be a 
	 mutex to protect it. (Solaris has a multi-thread-safe version...)

Warning: THIS MODULE IS SYSTEM DEPENDENT. Intended as an interface to PTWC Solaris
	 based software. Nothing inherently system specific in these routines,
	 but never tested on other os's.



start_ew_data():	
***************
	Must be called at startup time. It's given the name of the
Earthworm parameter file, which contains all the Earthwormy things,
like module id, ring to attach to, station to send, and what to call
it. Its also given (via the calling arguments) arrays full of names of
stations to be passed along.
	It reads its parameter file (traditionally a .d), attaches to
	the specified Earthworm message ring, creates an in-memory,
circular fifo, and starts a thread which moves desired Earthworm trace
messages from the Earthworm ring to this fifo.


get_ew_data():
**************
	Called by the main program whenever it wishes a piece of trace data. This routine
will pull the oldest message out of the in-memory fifo, and deliver it. The piece
of trace data supplied is promised to be one of the desired s-c-n's, but in no predicable
order. Data and header information is stufffed into a pre-allocated structure in the
calling program. Pointers to such structures are supplied as arguments. It's promised that
no more than a specified number of (long) data points will be crammed to the caller. If that's
not enough to hold the guts of a particular Earthworm message from the fifo, and error is 
declared. 
				NOTE
The routine blocks until data is available.

				NOTE
If the routine is not called often enough, the fifo (internal to these
routines) will wrap (configuration file has the maximum permissible
size for the fifo), and will overwrite in a circular manner - thus, it
will always offer the N most recent messages.


stop_ew_data():
**************
	Detaches from the Earthworm shared memory ring, and kills the message gathering thread.
Does not affect the rest of any Earthworm pieces (we hope).

Earthworm shutdown:
	If the Earthworm is ordered to shut down, the following happens: The
Earthworm shutdown procedure includes releasing 'kill yourself'
messages into all message rings in the Earthworm. A decent module is
expected to clean up and fold up. (Harsher measures are taken against
those who try to escape this).
	When the message-getting thread gets a termination message from the transport ring, it'll
	set a status variable to -1, and exit. get_ew_data() will see the negative status the next
	time it's called, and exit with a -1. ew2rtm hopefully gets the hint, and does whatever it will. 
	For safety's sake, this should include calling stop_ew_data(), so we can do a formal detach from
	the soon non-exiting Earthworm shared memory regions.

	
 */
/* #include <types.h> 
   #include <nerrno.h>
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <fcntl.h>
#include <math.h>
#include <time.h>

#ifdef _SOLARIS             
#include <sys/types.h>
#include <sys/shm.h>
#include <wait.h>
#include <thread.h>
#endif

#include <errno.h>
#include <signal.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <trace_buf.h>
#include <swap.h>
#include "mem_circ_queue.h" 
#include "ew_ptwc.h"

/* Functions in this source file 
 *******************************/
int   ew_ptwc_config  ( char * ); 			/* read the configuration file */
int  ew_ptwc_lookup  ( void ); 			/* lookup numeric values for EW names */
void  ew_ptwc_status  ( unsigned char, short, char * );	/* Issue status message into transport ring */ 
int   ew_ptwc_filter  ( TRACE_HEADER* );		/* Decide if a message is for us */

/* Thready things
*****************/
#define THREAD_STACK 8192
static unsigned tidGetMsg;            /* message getter (transport to mem fifo) thread id */
int GetMsgStatus=0;	  	      /* 0=> GetMsg thread ok. <0 => dead: */
int GetMsgErrNum=0;		  /* associated error code (see ew_ptwc.h). Set when the
				    GetMsg thread declares problems via GetMsgStatus */
thr_ret GetMsg( void * );

QUEUE OutQueue; 		      /* from queue.h, queue.c; sets up linked list via malloc and free */
#ifdef _OS2                          /* OS2 ONLY */
void  GetMsg( void * );
#endif
#ifdef _WINNT                          /* NT ONLY */
void  GetMsg( void * );
#endif
#ifdef _SOLARIS                      /* SOLARIS ONLY */
void *GetMsg( void * );
#endif
                                      /* used to pass messages between main thread and SocketSender thread */

/* turns debugging logging to log file on and off (1 or 0) */
#define DEBUG 0

extern int  errno;

static  SHM_INFO  Region;       /* shared memory region to use for i/o    */

#define   MAXLOGO   100
MSG_LOGO  GetLogo[MAXLOGO];     /* array for requesting module,type,instid */
short 	  nLogo;

     
/* Things to read or derive from configuration file
 **************************************************/
static char    RingName[20];        /* name of transport ring for i/o    */
static char    MyModName[20];       /* speak as this module name/id      */
static int     LogSwitch;           /* 0 if no logfile should be written */
static int     HeartBeatInt;        /* seconds between heartbeats        */
static long    MaxMsgSize;          /* max size for input/output msgs    */
static int     MaxMessages;	    /* max messages in output circular buffer       */

/* Names of things to send
**************************/
#define MAX_NAM_LEN 10  /* characters per any name */
#define MAX_NAMES 500   /* number of names  */
int Ncomponents;	/* actual number of names supplied */
char Stat[MAX_NAMES][MAX_NAM_LEN];
char Comp[MAX_NAMES][MAX_NAM_LEN];
char Net[MAX_NAMES][MAX_NAM_LEN];

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o     */
static unsigned char InstId;        /* local installation id             */
static unsigned char MyModId;       /* Module Id for this program        */
static unsigned char TypeHeartBeat; 
static unsigned char TypeError;

/* Error messages used by ew_ptwc are in ew_ptwc.h 
 *************************************************/
static char  Text[150];        /* string for log/error messages          */

/* Other variables: 
*******************/
   time_t MyLastBeat;  		/* time of last heartbeat into the local Earthworm ring */
   char         *inMsg;		/* message stuffed by thread             */    
   char         *outMsg;	/* Pulled out by get_ew_data                  */    
   long          recsize;	/* size of retrieved message             */
   MSG_LOGO      reclogo;	/* logo of retrieved message             */
   int           res;
   int itmp;

/****************** stop_ew_data ***************************************
	Called by when main has decided to quit operations.
	This could be because get_ew_data() has sent it a quit signal.
***************************************************************************/
int stop_ew_data()
{
   tport_detach( &Region ); /* let go of transport ring */
   (void) KillThread(tidGetMsg); /* try to shut down the message getting thread */
   logit("et","ew_ptwc: Shutdown - detaching from Earthworm (stop_ew_data)\n");
   return(RET_OK);
}

/************************* start_ew_data *************************************
	Called at start of operations. We read our config
	file, connect to transport, start the memory queue, and
	start the message gathering thread.
****************************************************************************/
int start_ew_data ( int ncomponents,  char* station[], char* component[], char* network[],
		    char* param_file, int* maxSamples )
{
    int i;
    int	 ret;
    int		c;


   /* Set initial GetMsg thread status to running
    *********************************************/
   GetMsgStatus= 0;

   /* Initialize heartbeat timer
    ****************************/
   MyLastBeat=0; /* assure a heartbeat on first execution of get_ew_data() */
      
   /* Read the configuration file(s)
   ********************************/
   if(DEBUG) printf("about to read config file %s\n",param_file);
   ret=ew_ptwc_config( param_file );
   if(ret<0) return(RET_QUIT); /* something went wrong. Prepare to abort */

   /* Set the PTWC-side max-data-size variable
    ******************************************/
   /* the worst imaginable case is a message of two-byte data */
   *maxSamples = ( MaxMsgSize-sizeof(TRACE_HEADER) )/2;

   /* Look up important info from earthworm.h tables
   ************************************************/
   ret=ew_ptwc_lookup();   
   if (ret != RET_OK)	return(ret);
 
   /* Initialize name of log-file & open it 
   ***************************************/
   logit_init( "ew_ptwc", (short) MyModId, 256, LogSwitch ); 
   if (DEBUG) logit( "e" , "ew_ptwc: These are the Earthworm to PTWC routines. Read command file <%s>\n", param_file);

   /* Write reassuring  things to log file
    **************************************/
   if (DEBUG) logit("","Established fifo of %ld messages, max %ld bytes per message\n",MaxMessages,MaxMsgSize);
   if (DEBUG) logit("e", "RingName: %s, MyModName: %s, nLogo: %u\n",RingName,MyModName,nLogo);

   
   /* Copy name arrays to our memory
   ********************************/
   Ncomponents = ncomponents;  /* copy to our global memory */
   if(Ncomponents > MAX_NAMES)
	{
   	logit("e"," FATAL ERROR. Too many station names: %d given, %d max\n", Ncomponents, MAX_NAMES );
	return(RET_QUIT);
	}
   for (i=0; i<Ncomponents; i++) /* copy to our own arrays. Who knows what may happen to the originals */
	{
	strncpy( Stat[i], station[i],   MAX_NAM_LEN);
	strncpy( Comp[i], component[i], MAX_NAM_LEN);
	strncpy( Net[i],  network[i],   MAX_NAM_LEN);
	}
   if(DEBUG)
	{
	logit("e","start_ew_data: Sending the following stations:\n");
	for(i=0;i<Ncomponents;i++)
	    {
	    logit("e","    %s  %s  %s\n",Stat[i], Comp[i], Net[i]);
	    }
	}

   /* Allocate space for  messages
   *********************************/
   if ( ( inMsg = (char *) malloc(MaxMsgSize) ) == (char *) NULL ) 
      {
      logit( "e", "ew_ptwc: error allocating inMsg; exitting!\n" );
      return(RET_QUIT);
      }

   if ( ( outMsg = (char *) malloc(MaxMsgSize) ) == (char *) NULL ) 
      {
      logit( "e", "ew_ptwc: error allocating outMsg; exitting!\n" );
      return(RET_QUIT);
      }

   /* Create a Mutex to control access to queue
   ********************************************/
   CreateMutex_ew();

   /* Initialize the message queue
   *******************************/
   ret = initqueue ( &OutQueue, (unsigned long)MaxMessages,(unsigned long)MaxMsgSize);
   if ( ret!= 0 )
         {       
	 if (ret==-2)  
	    {
	    sprintf(Text,"initqueue error: requested memory overflows long. Terminating\n");
            ew_ptwc_status( TypeError, ERR_QUEUE_BROKEN, Text );
	    GetMsgErrNum=ERR_QUEUE_BROKEN;
	    return(RET_QUIT);
	    }
 	 if (ret==-1) 
	    {
	    sprintf(Text,"initqueue cant allocate memory. Terminating\n");
            ew_ptwc_status( TypeError, ERR_QUEUE_BROKEN, Text );
	    GetMsgErrNum=ERR_QUEUE_BROKEN;
	    return(RET_QUIT);
	    }
	sprintf(Text,"Unknown error from initqueue. Terminating\n");
	ew_ptwc_status( TypeError, ERR_QUEUE_BROKEN, Text );
	GetMsgErrNum=ERR_QUEUE_BROKEN;
	return(RET_QUIT);
         }

   /* Attach to Input/Output shared memory ring 
   *******************************************/
   tport_attach( &Region, RingKey );

   /* Start the GetMsg thread
   **************************/
   /* moves messages from transport to memory queue */
   if ( StartThread( GetMsg, THREAD_STACK, &tidGetMsg ) == -1 ) 
      {
      GetMsgStatus= -1; /* => not running */
      logit( "e", "ew_ptwc: Error starting GetMsg thread. Exitting.\n" );
      tport_detach( &Region );
      return(RET_QUIT);
      }
    if (DEBUG) logit("","Message getting started. start_ew_data() completed ok.\n");

   /* End of start_ew_data
    ********************/
   return(RET_OK);
}
/****************************** end of start_ew_data ****************************************************/

/***************************** get_ew_data() *************************************
	Called to supply a chunck of trace data.
	Takes trace data messages from the memory queue.
	Messages are placed on this queue by the GetMsg
	tread.
	We also sense if that thread has quit, and send 
	a signal to our main program to quit.
***************************************************************************/

int get_ew_data(
		TRACE_HEADER* pewHeader,/* pointer to TRACE_HEADER structure (trace_buf.h) */
		long* pewData,		/* pointer to data buffer */
		long maxData)		/* limit of # of points to stuff */
{
   int ret;
	int ms;
   time_t now;
   static long msgSize;
      long lHdrSec;
      struct tm* hdr; /* unix integer form time structure */
    
   /* Top of Working loop 
    *********************/
   if (DEBUG) logit("e","entering queue-wait loop\n");
   topOfLoop:

   /* Beat the heart into the transport ring
    ****************************************/
   time(&now);
   if (difftime(now,MyLastBeat) > (double)HeartBeatInt)
	{	
        ew_ptwc_status( TypeHeartBeat, 0, "" );
		time(&MyLastBeat);
    }

   /* Has Earthworm requested a shutdown?
   **************************************/
   if (GetMsgStatus <0 ) /* then the message getter has quit, and so should we */
	{
	logit("et","get_ew_data: requesting shutdown\n");
	return(RET_QUIT);
	}

   /* Pull a message from the queue
    *******************************/
   RequestMutex();
   ret=dequeue( &OutQueue, outMsg, &msgSize, &reclogo);
   ReleaseMutex_ew();
   if(ret < 0 )
      { /* -1 means empty queue */
      sleep_ew(100); 		/*wait a bit DCK was 1000 */
      goto topOfLoop;
      }

   /* It's not too big, is it? 
   ***************************/
   if (DEBUG) logit("e","got message from queue\n");
   if ( ((TRACE_HEADER*)outMsg)->nsamp > maxData ) 
	{
	logit("e"," FATAL ERROR. Earthworm trace message too large: %ld limit is: %ld\n",
			((TRACE_HEADER*)outMsg)->nsamp, maxData);
	return(RET_TOO_LARGE);
	}

   /* copy message header to user's structure
   ******************************************/
   memcpy( (char*)pewHeader, (char*)outMsg, sizeof(TRACE_HEADER) );
  
   /* Hand it the data 
   ******************/
      {
      /* story: here we unpack the data in the EW message into 4-byte integers */
      int datLen=0;	/* bytes per data point */
      int j;		/* index over data points in message */
      char* next;	/* pointer to next data value (either 2 or 4 bytes long) */

      next = outMsg+sizeof(TRACE_HEADER);      /* point to start of data */
      if( strcmp( ((TRACE_HEADER*)outMsg)->datatype, "s4")==0 ) datLen=4;
      if( strcmp( ((TRACE_HEADER*)outMsg)->datatype, "s2")==0 ) datLen=2;
      if(datLen ==0)
	{
	logit("","FATAL ERROR get_ew_data dequeued illegal datatype: %s. \n",((TRACE_HEADER*)outMsg)->datatype);
	return(RET_BAD_TYPE);
	}
      for(j=0; j<(int)(((TRACE_HEADER*)outMsg)->nsamp); j++)
         {
         if (datLen == 2) pewData[j] = (long) *((short*)next);
         if (datLen == 4) pewData[j] = (long) *(( long*)next);
         next = next + datLen;
         }
      }

    if(GetMsgErrNum != 0)
	{
	int i;
	i=GetMsgErrNum;
	GetMsgErrNum=0; /* clear it for next call */
	return(i);
	}
   else
   	return(RET_OK);  
}



/***************************** GetMsg Thread *******************
	Pick up messages of our kind from the transport ring
	and push them into the memory fifo. get_ew_data() will pull
	them out and hand them to our caller
 ******************************************************************/
thr_ret GetMsg( void *dummy )
{
   int ret;

   /* Added 8/10/98; Set the priority to real-time
    *********************************************
   SetThreadPriority (GetCurrentThread (), THREAD_PRIORITY_TIME_CRITICAL);
   */

   /* declare ourselves in operation 
    ********************************/
   GetMsgStatus =0; /* show optimism initially */
   if (DEBUG) logit("e","GetMsg thread starting up. nLogo: %d, \n", nLogo);
   if (DEBUG) logit("e","GetLogo[%d] inst:%d module:%d type:%d \n",
		        0, (int) GetLogo[0].instid,
                               (int) GetLogo[0].mod,
                               (int) GetLogo[0].type );  /*DEBUG*/
 
   while( tport_getflag( &Region ) != TERMINATE )
      {
      /* Get a message from transport ring
      ***********************************/
      res = tport_getmsg( &Region, GetLogo, nLogo, &reclogo, &recsize, inMsg, MaxMsgSize-1 );

      if( res == GET_NONE ) {sleep_ew(40); continue;} /*DCK was 100 - wait if no messages for us */
      /* logit("et","Got message from transport of %ld bytes, res=%d\n",recsize,res); */

      /* Check return code; report errors 
      **********************************/
      if( res != GET_OK )
         {
         if( res==GET_TOOBIG ) 
            {
            sprintf( Text, "msg[%ld] i%d m%d t%d too long for target",
                            recsize, (int) reclogo.instid,
			    (int) reclogo.mod, (int)reclogo.type );
            ew_ptwc_status( TypeError, ERR_TOOBIG, Text );
            continue;
            }
         else if( res==GET_MISS ) 
            {
            sprintf( Text, "missed msg(s) i%d m%d t%d in %s",(int) reclogo.instid,
			    (int) reclogo.mod, (int)reclogo.type, RingName );
            ew_ptwc_status( TypeError, ERR_MISSMSG, Text );
            }
         else if( res==GET_NOTRACK ) 
           {
            sprintf( Text, "no tracking for logo i%d m%d t%d in %s",
                          (int) reclogo.instid, (int) reclogo.mod,
                          (int)reclogo.type, RingName );
            ew_ptwc_status( TypeError, ERR_NOTRACK, Text );
            }
         }

     /* Got a message. Pass it through the filter. If it passes, swap and place on queue. 
      **********************************************************************************/
     /* See if we've supposed to pass on this s-c-n */
     if ( ew_ptwc_filter( (TRACE_HEADER*)inMsg ) == -1 ) 
         {
         continue; /* recall, we're in a while loop over messages */
	 }
     WaveMsgMakeLocal( (TRACE_HEADER*)inMsg ); /* convert the whole message to local byte order */
     RequestMutex();
     ret=enqueue( &OutQueue, inMsg, recsize, reclogo ); /* put it into the 'to be shipped' queue */
			/* get_ew_data() is in the biz of de-queueng and handing to  */
    if (DEBUG) logit("","Stuffed message; ret: %d\n",ret); 
    ReleaseMutex_ew();
    if ( ret!= 0 )
         {       
 	 if (ret==-1) 
	    {
	    sprintf(Text,"enqueue: message too big. Lost message\n");
            ew_ptwc_status( TypeError, ERR_QUEUE, Text );
	    GetMsgErrNum=ERR_QUEUE;
	    continue;
	    }
	 if (ret==-3) 
	    {
	    sprintf(Text,"Circular queue lapped. Message lost\n");
            ew_ptwc_status( TypeError, ERR_QUEUE, Text );
	    GetMsgErrNum=ERR_QUEUE;
            continue;
	    }
	 sprintf(Text,"Unknown error from enqueue: %d\n",ret);
	 ew_ptwc_status( TypeError, ERR_QUEUE, Text );
	 GetMsgErrNum=ERR_QUEUE;
	 continue;
         }
      } /*end of message getting loop */

   /* Shut it down
   **************/
   /* we do  this by setting our status variable to negative, which
      causes get_ew_data() to send a signal to caller to shut down
   */
   GetMsgStatus= -1;  /* so that get_ew_data() will signal to quit */
   logit("t", "GetMsg thread: termination requested; exitting!\n" );
   (void)KillSelfThread( );  /* this terminates us (the thread), without upsetting anyone else */
}               



/*****************************************************************************
 *  ew_ptwc_config() processes command file(s) using kom.c functions;         *
 *                    exits if any errors are encountered.	             *
 *****************************************************************************/
int ew_ptwc_config( char *configfile )
{
   int      ncommand;     /* # of required commands you expect to process   */ 
   char     init[20];     /* init flags, one byte for each required command */
   int      nmiss;        /* number of required commands that were missed   */
   char    *com;
   int      nfiles;
   int      success;
   int      i;	
   char*    str;

/* Set to zero one init flag for each required command 
 *****************************************************/   
   ncommand = 7;
   for( i=0; i<ncommand; i++ )  init[i] = 0;
   nLogo = 0;

/* Open the main configuration file 
 **********************************/
   nfiles = k_open( configfile ); 
   if ( nfiles == 0 ) {
	fprintf( stderr,
                "ew_ptwc: Error opening command file <%s>; exitting!\n", 
                 configfile );
	return(RET_QUIT);
   }

/* Process all command files
 ***************************/
   while(nfiles > 0)   /* While there are command files open */
   {
        while(k_rd())        /* Read next line from active file  */
        {  
	    com = k_str();         /* Get the first token from line */

        /* Ignore blank lines & comments
         *******************************/
            if( !com )           continue;
            if( com[0] == '#' )  continue;

        /* Open a nested configuration file 
         **********************************/
            if( com[0] == '@' ) {
               success = nfiles+1;
               nfiles  = k_open(&com[1]);
               if ( nfiles != success ) {
                  fprintf( stderr, 
                          "ew_ptwc: Error opening command file <%s>; exitting!\n",
                           &com[1] );
                  return(-1);
               }
               continue;
            }

        /* Process anything else as a command 
         ************************************/
  /*0*/     if( k_its("LogFile") ) {
                LogSwitch = k_int();
                init[0] = 1;
            }
  /*1*/     else if( k_its("MyModuleId") ) {
                str = k_str();
                if(str) strcpy( MyModName, str );
                init[1] = 1;
            }
  /*2*/     else if( k_its("RingName") ) {
                str = k_str();
                if(str) strcpy( RingName, str );
                init[2] = 1;
            }
  /*3*/     else if( k_its("HeartBeatInt") ) {
                HeartBeatInt = k_int();
                init[3] = 1;
            }

  /*4*/     else if( k_its("MaxMsgSize") ) {
                MaxMsgSize = k_int();
                init[4] = 1;
            }

  /*5*/     else if( k_its("MaxMessages") ) {
                MaxMessages = k_int();
                init[5] = 1;
            }


         /* Enter installation & module & message types to get
          ****************************************************/
  /*6*/     else if( k_its("GetMsgLogo") ) {
                if ( nLogo >= MAXLOGO ) {
                    fprintf( stderr, 
                            "ew_ptwc: Too many <GetMsgLogo> commands in <%s>", 
                             configfile );
                    fprintf( stderr, "; max=%d; exitting!\n", (int) MAXLOGO );
                    return( -1 );
                }
                if( ( str=k_str() ) ) {
                   if( GetInst( str, &GetLogo[nLogo].instid ) != 0 ) {
                       fprintf( stderr, 
                               "ew_ptwc: Invalid installation name <%s>", str ); 
                       fprintf( stderr, " in <GetMsgLogo> cmd; exitting!\n" );
                       return( -1 );
                   }
                }
                if( ( str=k_str() ) ) {
                   if( GetModId( str, &GetLogo[nLogo].mod ) != 0 ) {
                       fprintf( stderr, 
                               "ew_ptwc: Invalid module name <%s>", str ); 
                       fprintf( stderr, " in <GetMsgLogo> cmd; exitting!\n" );
                       return( -1 );
                   }
                }
                if( ( str=k_str() ) ) {
                   if( GetType( str, &GetLogo[nLogo].type ) != 0 ) {
                       fprintf( stderr, 
                               "ew_ptwc: Invalid msgtype <%s>", str ); 
                       fprintf( stderr, " in <GetMsgLogo> cmd; exitting!\n" );
                       return( -1 );
                   }
                }
                nLogo++;
                init[6] = 1;
            }


         /* Unknown command
          *****************/ 
	    else {
                fprintf( stderr, "ew_ptwc: <%s> Unknown command in <%s>.\n", 
                         com, configfile );
                continue;
            }

        /* See if there were any errors processing the command 
         *****************************************************/
            if( k_err() ) {
               fprintf( stderr, 
                       "ew_ptwc: Bad <%s> command in <%s>; exitting!\n",
                        com, configfile );
               return( -1 );
            }
	}
	nfiles = k_close();
   }

/* After all files are closed, check init flags for missed commands
 ******************************************************************/
   nmiss = 0;
   for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
   if ( nmiss ) {
       fprintf( stderr, "ew_ptwc: ERROR, no " );
       if ( !init[0] )  fprintf( stderr, "<LogFile> "      );
       if ( !init[1] )  fprintf( stderr, "<MyModuleId> "   );
       if ( !init[2] )  fprintf( stderr, "<RingName> "     );
       if ( !init[3] )  fprintf( stderr, "<HeartBeatInt> " );
       if ( !init[4] )  fprintf( stderr, "<MaxMsgSize> " );
       if ( !init[5] )  fprintf( stderr, "<MaxMessages> " );
       if ( !init[6] )  fprintf( stderr, "<GetMsgLogo> "   );
       fprintf( stderr, "command(s) in <%s>; exitting!\n", configfile );
       return( -1 );
   }

   return(0);
}

/****************************************************************************
 *  ew_ptwc_lookup( )   Look up important info from earthworm.h tables       *
 ****************************************************************************/
int ew_ptwc_lookup( void )
{
/* Look up keys to shared memory regions
   *************************************/
   if( ( RingKey = GetKey(RingName) ) == -1 ) {
 	fprintf( stderr,
 	        "ew_ptwc:  Invalid ring name <%s>; exitting!\n", 
                 RingName);
	return( RET_QUIT );
   }

/* Look up installations of interest
   *********************************/
   if ( GetLocalInst( &InstId ) != 0 ) {
        fprintf( stderr, 
              "ew_ptwc: error getting local installation id; exitting!\n" );
        return( RET_QUIT );
   }

/* Look up modules of interest
   ***************************/
   if ( GetModId( MyModName, &MyModId ) != 0 ) {
        fprintf( stderr, 
              "ew_ptwc: Invalid module name <%s>; exitting!\n", 
               MyModName );
        return( RET_QUIT );
   }

/* Look up message types of interest
   *********************************/
   if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
        fprintf( stderr, 
              "ew_ptwc: Invalid message type <TYPE_HEARTBEAT>; exitting!\n" );
        return( RET_QUIT );
   }
   if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
        fprintf( stderr, 
              "ew_ptwc: Invalid message type <TYPE_ERROR>; exitting!\n" );
        return( RET_QUIT );
   }
   return(RET_OK);
} 

/*******************************************************************************
 * ew_ptwc_status() builds a heartbeat or error message & puts it into          *
 *                 shared memory.  Writes errors to log file & screen.         *
 *******************************************************************************/
void ew_ptwc_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char	       msg[256];
   long	       size;
   long        t;
 
/* Build the message
 *******************/ 
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
	sprintf( msg, "%ld\n\0", t);
   }
   else if( type == TypeError )
   {
	sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
	logit( "et", "ew_ptwc: %s\n", note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */ 	

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","ew_ptwc:  Error sending heartbeat.\n" );
	}
	else if( type == TypeError ) {
           logit("et","ew_ptwc:  Error sending error:%d.\n", ierr );
	}
   }

   return;
}



/************************** ew_ptwc_filter *************************
 *           Decide if this message should be exported.           *
 *     Return    0 if the message should  be exported             *
 *              -1 if message should not be exported              *
 ******************************************************************/

int ew_ptwc_filter( TRACE_HEADER* inMsg )
{
   int i;
   for(i=0;i<Ncomponents;i++)
	{
	if( strcmp( ((TRACE_HEADER*)inMsg)->sta,  Stat[i] ) != 0) continue; /* not this one; keep trying */
	if( strcmp( ((TRACE_HEADER*)inMsg)->net,  Net[i]  ) != 0) continue; /* not this one; keep trying */
	if( strcmp( ((TRACE_HEADER*)inMsg)->chan, Comp[i] ) != 0) continue; /* not this one; keep trying */
	return(0);  /* if we're here, all three fields matched: it's one of ours! */
	}
   return( -1 ); /* None matched. Don't ship it */
}

/********************** BigOrLittle ******************************
	to discover in which order this machine stores the
	bytes of a number
******************************************************************/

int BigOrLittle()
{
   char b[4];
   long l;
   union u {long l;char b[4];};
   union u overlay;

   l=1;
   logit("","byte order: %d %d %d %d\n",
		overlay.b[0],overlay.b[1],overlay.b[2],overlay.b[3]);
   return(1);
}


/******************************************************************************
 * err_ew_ptwc() builds a error message & puts it into                        *
 *                   shared memory.  Writes errors to log file & screen.      *
 ******************************************************************************/
void err_ew_ptwc( short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size;
   long        t;
 
/* Build the message
 *******************/ 
   logo.instid = InstId;
   logo.mod    = MyModId;
   logo.type   = TypeError;

   time( &t );

        sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
        logit( "et", "ew_ptwc: %s\n", note );

   size = strlen( msg );   /* don't include the null byte in the message */     

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
   	{
	logit("et","ew_ptwc:  Error sending error.\n" );
   	}

   return;
}



