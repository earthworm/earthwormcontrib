#
# This is the Earthworm-side configuration file for ew2rtm. 
# ew2rtm is the Earthworm->PTWC coupler module. That module calls  the 
# Earthworm-side startup routine start_ew_data(), which reads this file. 
# The s-c-n names of the channels to be passed are specified in the PTWC-side 
# configuration file. (The names of both files are passed via command line argument).

# ew2rtm is started by actions external to Earthworm. 
# Earthworm must be running when ew2rtm is started, as the Earthworm side of ew2rtm
# will attempt to attache to the Earthworm ring (specified in this file) at startup.
#
 MyModuleId     MOD_EW2RTM     # module id to be used by this module,
 RingName       WAVE_RING      # transport ring to attach to and drink from.
 HeartBeatInt   10             # Heartbeat interval in seconds (Earthworm internal)
		# ew2rtm will issue a heartbeat into the Earthworm ring. It will 
		# consider beating the heart each time get_ew_data() is called, but will
		# not beat faster than the interval above. That is, if the PTWC-side
		# quits drinking data, the Earthworm-side heartbeat will cease.
 LogFile        1              # If 0, don't write logfile at all,
 MaxMsgSize	500	       # length of largest message we'll ever handle - in bytes
 MaxMessages	1000	       # limit of number of message to buffer:
		# The Earthworm-side maintains an in-memory circular buffer of messages
		# which have been picked up but not delivered. This is the number of
		# messages in this buffer.

# List the message logos to grab from Earthworm transport ring 
#              Installation       Module       Message Type
 GetMsgLogo    INST_WILDCARD      MOD_WILDCARD     TYPE_TRACEBUF

