/* @(#)protos.h	1.1 06/21/96 */
/*======================================================================
 *
 *  protos.h
 *
 *  Prototypes for revision specific functions.
 *
 *====================================================================*/
#ifndef reftek_protos_included
#define reftek_protos_included
#include "reftek.h"

#ifdef __STDC__

void reftek_common1(
    unsigned char *,
    int *,
    int *,
    int *,
    double *
);

int reftek_dt1(
    struct reftek_dt *,
    char *
);

int reftek_eh1(
    struct reftek_eh *,
    char *
);

int reftek_et1(
    struct reftek_et *,
    char *
);

#endif /* __STDC__ */

#endif /* reftek_protos_included */
