#
# This is ref2ew's parameter file, one for each station/tty port
#
RingName    WAVE_RING       # put waveforms into this ring
MyModuleId  MOD_REF2EW_1    # module id to label logfile  with.
LogFile       1             # 0=no log; 1=log errors
HeartBeatInt  15            # seconds between heartbeats


# Specify TtyName,Speed or TcpHost,Port for data source, but not both pairs.
# TtyName     /dev/sts/ttyd63 # TTY device from which to read reftek data
# Speed       38400           # Baud rate for reading data
TcpHost     acola24         # Hostname or IP address
Port        14012           # TCP port number
Station     SEA             # Station name
Network     UW              # Network name
Channel    1  ELZ           # Channel(s) to use
Channel    2  ELN
Channel    3  ELE
Channel    4  HHZ
Channel    5  HHN
Channel    6  HHE
TimeShift   0.29            # Time in seconds to add to packet time
SampleRate    100.0         # Samples per second
RefVar	Terra               # optional Reftek variant: "Reftek" or "Terra"
StationFile pick_ew_uw.sta  # Station file, the same as used by pick_ew
nFileBufs   720              # How many packets to a file; 0 for no file
DirPath    /data5/earthworm/SMO/SEA    # Where to put the files


# Debug                     # uncomment to gets lots of debugging output
