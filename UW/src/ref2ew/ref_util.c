/************************************************************************/
/* FILE:   ref_util.c                                                   */
/* AUTHOR:	Bruce Crawford		UNR Seismology			*/
/* modified by Pete Lombard, UW Seismology
/************************************************************************/

#include <ctype.h>
#include <memory.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <errno.h>
#include "bcd2int.h"
#include "protos.h"
#include "reftek.h"
#include "util.h"

/* Offsets to the various pieces */

#define EVTN_OFF  16  /* event number           */
#define STRM_OFF  18  /* stream id              */
#define CHAN_OFF  19  /* channel id             */
#define NSMP_OFF  20  /* number of samples      */
#define FRMT_OFF  23  /* data format descriptor */
#define UDAT_OFF  24  /* uncompressed data      */
#define CDAT_OFF  24  /* compressed data        */
#define SINT_OFF  88  /* sample rate            */
#define TTYP_OFF  92  /* trigger type           */
#define TRON_OFF  96  /* trigger time           */
#define TOFS_OFF 112  /* time of first sample   */
#define TOFF_OFF  96  /* detrigger time         */
#define TOLS_OFF 112  /* time of last sample    */


/*----------------------------------------------------------------------*/
int bcd2int( unsigned char *input, int numDigits, int nibble)
{
	int digit, decimal, sum;

	sum = 0;

	for (digit = 0 ; digit < numDigits; digit++)
	{
		if (nibble)
			decimal = *input & 0xF;
		else
			decimal = (*input & 0xF0) >> 4;

		sum = sum * 10 + decimal;

		if (nibble)
			input++;

		nibble = ! nibble;
	}

	return (sum);
}


/* from: com1.c	1.1 06/21/96 */
/*======================================================================
 *
 *  com1.c
 *
 *  Decode the common header of an arbitrary rev 1 RefTek packet.
 *
 *====================================================================*/

/* Offsets to the various pieces */

#define EXPN_OFF     2  /* experiment number */
#define YEAR_OFF     3  /* year              */
#define UNIT_OFF     4  /* unit id           */
#define TIME_OFF     6  /* time stamp        */
#define SEQN_OFF    14  /* sequence number   */

void reftek_common1(unsigned char *src, int *exp, int *unit, int *seqno, 
		    double *tstamp)
{
int yr, da, hr, mn, sc, ms;

    yr = bcd2int(src + YEAR_OFF,   2, 0); 
    yr += (yr < 99) ? 1900 : 2000;  /* WARNING: fix this before 2099! */

    da = bcd2int(src + TIME_OFF,   3, 0);
    hr = bcd2int(src + TIME_OFF+1, 2, 1);
    mn = bcd2int(src + TIME_OFF+2, 2, 1);
    sc = bcd2int(src + TIME_OFF+3, 2, 1);
    ms = bcd2int(src + TIME_OFF+4, 3, 1);

    *exp    = bcd2int(src + EXPN_OFF,   2, 0); 
    *unit   = bcd2int(src + UNIT_OFF,   4, 0);
    *seqno  = bcd2int(src + SEQN_OFF,   4, 0);
    *tstamp = util_ydhmsmtod(yr, da, hr, mn, sc, ms);
}


/* from: dcomp.c	1.2 10/10/96 */
/*======================================================================
 *
 *  dcomp.c
 *
 *  Decompress a C0 compressed data record.  If the output buffer is
 *  not NULL, then the decompressed data are stored there and the data
 *  pointer in the reftek_dt structure is set to point to this
 *  uncompressed buffer.  If the output buffer is NULL, then an internal
 *  buffer is used and the data pointer is set to point to it, however
 *  the data will be overwritten on the next call to here.
 *
 *  Requires sizeof(int) to be 4.
 *
 *  Data are uncompressed into the host native byte order.
 *
 *  Returns:
 *  0 if data were decompressed successfully
 * <0 if they were not, with reftek_errno set to indicate the reason.
 *
 *====================================================================*/

#define NFRM 15           /* number of frames              */
#define FLEN 64           /* number of bytes per frame     */
#define NSEQ 16           /* number of sequences per frame */
#define WLEN FLEN / NSEQ  /* number of bytes per sequence  */

int reftek_dcomp(dt, user_buffer, order)
struct reftek_dt *dt;
int *user_buffer;
u_long order;
{
int i, j, k, beg, end, code[NSEQ], key, val, nsamp, itmp;
short stmp;
char *frm;
int *output;
static int temp_buffer[REFTEK_MAXC0];
union {
    char  *c;
    short *s;
    int   *i;
} ptr;

    if (sizeof(int) != WLEN) {
        reftek_errno = REFTEK_EINVAL;
        return -1;
    }

    if (dt->format != REFTEK_FC0) {
        reftek_errno = REFTEK_EINVAL;
        return -2;
    }

    output = (user_buffer == (int *) 0) ? temp_buffer : user_buffer;

/* Get the block start/stop values */

    ptr.c = dt->raw;

    memcpy(&beg, ptr.c + 4, 4);
    if (order != dt->order) util_lswap((long *)&beg, 1);

    memcpy(&end, ptr.c + 8, 4);
    if (order != dt->order) util_lswap((long *)&end, 1);

/* Loop over each frame */
/* We do not verify that the 0x00 codes are where they should be */

    val   = output[0] = beg;
    nsamp = 1;

    for (i = 0; i < NFRM; i++) {

        frm = dt->raw + (i * FLEN);  /* point to start of current frame */
        key = *((int *) frm);        /* codes are in first 4 bytes      */
        if (order != dt->order) util_lswap((long *)&key, 1);
        for (j = NSEQ - 1; j >= 0; j--) {
            code[j] = key & 0x03;
            key >>= 2;
        }

        for (j = 1; j < NSEQ; j++) {

            if (nsamp >= REFTEK_MAXC0) {
                reftek_errno = REFTEK_EDCMP;
                return -3;
            }

            ptr.c = frm + (j * 4);  /* point to current 4 byte sequence */

            switch (code[j]) {
              case 0:
                break;

              case 1:
                for (k = (nsamp == 1) ? 1 : 0; k < 4; k++) {
                    output[nsamp++] = (val += (int) ptr.c[k]);
                }
                break;

              case 2:
                for (k = (nsamp == 1) ? 1 : 0; k < 2; k++) {
                    stmp = ptr.s[k];
                    if (order != dt->order) util_sswap(&stmp, 1);
                    output[nsamp++] = (val += (int) stmp);
                }
                break;

              case 3:
                if (nsamp > 1) {
                    itmp = ptr.i[0];
                    if (order != dt->order) util_lswap((long *)&itmp, 1);
                    output[nsamp++] = (val += itmp);
                }
                break;

              default:
                reftek_errno = REFTEK_EDCMP;
                return -4;
            }
        }
    }

/* Update the dt header to point to uncompressed data */

    dt->wrdsiz = sizeof(int);
    dt->order  = order;
    dt->data   = (char *) output;

/* Sanity checks */

    if (dt->nsamp != nsamp) {
        reftek_errno = REFTEK_EDCMP;
        return -5;
    }

    if (output[nsamp-1] != end) {
        reftek_errno = REFTEK_EDCMP;
        return -6;
    }

    return 0;

}

/* from: dt.c	1.1 06/21/96 */
/*======================================================================
 *
 *  Decode a DT record.
 *
 *====================================================================*/

int reftek_dt(dest, src, rev)
struct reftek_dt *dest;
char *src;
int  rev;
{
int i, status;
short *sdata;
long  *ldata;

    dest->rev = rev;

    if (rev == 1) {
        status = reftek_dt1(dest, src);
    } else {
        reftek_errno = REFTEK_EREV;
        status = -1;
    }

    return status;
}

/*======================================================================
 *
 *  dt1_terra.c
 *
 *  Load a rev 1 data record from a Terra Technology IDS-24 box.
 *  The Terra box speaks `reftek' with an accent: some of the header
 *  information is not in standard format, so we have to handle it 
 *  with care. But we try to keep this close to standard.
 *====================================================================*/


int reftek_dt1(dest, src)
struct reftek_dt *dest;
char *src;
{
char *dptr;
int  ndat;

/* Load the meta-knowledge */

    dest->order = BIG_ENDIAN_ORDER;

/* Load the rev 1 common header */

    reftek_common1((unsigned char *)src,&dest->exp,&dest->unit,
		   &dest->seqno,&dest->tstamp);

/* Load the record specific parts */

    dest->evtno  = bcd2int((unsigned char *)src + EVTN_OFF, 4, 0); 
    dest->stream = bcd2int((unsigned char *)src + STRM_OFF, 2, 0);
    dest->chan   = bcd2int((unsigned char *)src + CHAN_OFF, 2, 0);
    dest->nsamp  = bcd2int((unsigned char *)src + NSMP_OFF, 4, 0);

    switch (bcd2int((unsigned char *)src + FRMT_OFF, 2, 0)) {
      case 16:
        dest->format = REFTEK_F16;
        dest->wrdsiz = 2;
        dest->data   = (char *) dest->raw;
        dptr = src + UDAT_OFF;
        ndat = 1000;
        break;

      case 32:
        dest->format = REFTEK_F32;
        dest->wrdsiz = 4;
        dest->data   = (char *) dest->raw;
        dptr = src + UDAT_OFF;
        ndat = 1000;
        break;

      case 120:
        dest->format = REFTEK_FC0;
        dest->wrdsiz = -1;
        dest->data   = (char *) NULL;
        dptr = src + CDAT_OFF;
        ndat = 960;
        break;

      default:
        reftek_errno = REFTEK_EINVAL;
        return -1;
    }

    memcpy(dest->raw, dptr, ndat);

    return 0;

}


/* from: errstr.c	1.1 06/21/96 */
/*======================================================================
 *
 * errmsg.c
 *
 * Generate an string describing the current contents of reftek_errno.
 *
 *====================================================================*/

char *reftek_errstr()
{
static char *defmsg = "unknown error 0x00000000 plus some extra";

    if (reftek_errno == REFTEK_EREV) {
        return "unsupported data revision";
    } else if (reftek_errno == REFTEK_EPAK) {
        return "unsupported packet type";
    } else if (reftek_errno == REFTEK_EDCMP) {
        return "decompression error";
    } else if (reftek_errno == REFTEK_EINVAL) {
        return "illegal argument or data";
    }

    sprintf(defmsg, "unknown error 0x%x", reftek_errno);
    return defmsg;
}

/* from: swap.c	2.3 01/28/97 */
/*======================================================================
 *
 *  Byte-swapping utilities.
 *
 *  util_lswap:  byte swap an array of longs
 *  util_sswap:  byte swap an array of shorts
 *  util_iftovf: convert IEEE floats into VAX floats.
 *  util_vftoif: convert VAX floats into IEEE floats.
 *  util_order:  determine native bute order
 *
 *====================================================================*/

#define MANTISSA_MASK ((unsigned long)(0x00FFFFFF))
#define MANTISSA_SIZE (24)

/**********************************************************************/

void util_lswap(input, number)
long *input;
long number;
{
short s_temp[2];
short temp;
long i;

    util_sswap((short *) input, number*2);
    for (i = 0; i < number; i++) {
        memcpy(s_temp, input + i, 4);
        temp      = s_temp[0];
        s_temp[0] = s_temp[1];
        s_temp[1] = temp;
        memcpy(input + i, s_temp, 4);
    }
}

/**********************************************************************/

void util_sswap(input, number)
short *input;
long number;
{
char byte[2];
char temp;
long i;

    for (i = 0; i < number; i++) {
        memcpy(byte, input + i, 2);
        temp = byte[0];
        byte[0] = byte[1];
        byte[1] = temp;
        memcpy(input + i, byte, 2);
    }
}

/**********************************************************************/

void util_iftovf(input, number)
unsigned long *input;     /*  pointer to data to be converted    */
long number;              /*  number of samples to be converted  */
{
static int native_order = -1;
unsigned long  mantissa, exponent;
long i;

    if (native_order == -1) native_order = util_order();
    if (native_order == LTL_ENDIAN_ORDER) util_lswap((long *) input, number);
    for (i = 0; i < number; i++) {
        mantissa = input[i] & MANTISSA_MASK;
        exponent = (((input[i] >> MANTISSA_SIZE) + 1) << MANTISSA_SIZE);
        input[i] = mantissa | exponent;
    }
    util_sswap((short *)input, number*2);
    if (native_order == LTL_ENDIAN_ORDER) util_lswap((long *)input, number);
}

/**********************************************************************/

void util_vftoif(input, number)
unsigned long *input;     /*  pointer to data to be converted    */
long number;              /*  number of samples to be converted  */
{
static int native_order = -1;
unsigned long  mantissa, exponent;
long i;

    if (native_order == -1) native_order = util_order();
    if (native_order == LTL_ENDIAN_ORDER) util_lswap((long *)input, number);
    util_sswap((short *)input, number*2);
    for (i = 0; i < number; i++) {
        if (input[i] != 0) {
            mantissa = input[i] & MANTISSA_MASK;
            exponent = (((input[i]>>MANTISSA_SIZE)-1)<<MANTISSA_SIZE);
            input[i] = mantissa | exponent;
        }
    }
    if (native_order == LTL_ENDIAN_ORDER) util_lswap((long *)input, number);
}

/**********************************************************************/

unsigned long util_order()
{
union {
    unsigned char character[4];
    unsigned long int integer;
} test4;
char wordorder[4+1];

/* Construct a 4-byte word of known contents - 0x76543210 */
/* Result will be 0x10325476 for little endian machines (eg Vax, PC) */
/*                0x76543210 for big    endian machines (eg Sun)     */
/*                0x54761032 for PDP-11's                            */
/* The include file "util.h" defines the constants BIG_ENDIAN_ORDER  */
/* and LTL_ENDIAN_ORDER to correspond to output of this routine.     */

    test4.character[0] = 0x76;
    test4.character[1] = 0x54;
    test4.character[2] = 0x32;
    test4.character[3] = 0x10;

    return test4.integer;
}

/* from: timefunc.c	2.7 02/21/97 */
/*======================================================================
 *
 *  Misc. time related functions.
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *
 *  util_dttostr:
 *  Given a double time value and format code, make a string of one of
 *  the following formats:
 *
 *  Format code   Output string
 *       0        yyyy:ddd-hh:mm:ss.msc
 *       1        Mon dd, year hh:mm:ss:msc
 *       2        yy:ddd-hh:mm:ss.msc, where input time is an interval
 *       3        yyyydddhhmmssmsc
 *       4        yyyyddd
 *       5        Day Mon dd, year
 *       6        yyyymmddhhmmss
 *       7        yyyy mm dd hh mm ss
 *       8        ddd-hh:mm:ss.msc, where input time is an interval
 *
 *  No newline is appended.
 *
 *  Returns a pointer to static space, therefore a call of the form:
 *
 *  printf("%s %s\n",
 *      util_dttostr(dtime1, form1), util_dttostr(dtime2, form1)
 *  );
 *
 *  will result in two duplicate time strings, even if dtime1 != dtime2
 *  or form1 != form2.  In such a case you should make distinct calls to
 *  printf.  For example:
 *
 *  printf("%s ",   util_dttostr(dtime1, form1));
 *  printf(" %s\n", util_dttostr(dtime2, form1));
 *
 *----------------------------------------------------------------------
 *
 *  util_lttostr:
 *  Given a long time value and format code, make a string of one of
 *  the following formats:
 *
 *  Format code   Output string
 *       0        yyyy:ddd-hh:mm:ss
 *       1        Mon dd, year hh:mm:ss
 *       2        yy:ddd-hh:mm:ss, where input time is an interval
 *       3        yydddhhmmss
 *       4        yyyyddd
 *       5        Day Mon dd, year
 *       6        yyyymmddhhmmss
 *       7        yyyy mm dd hh mm ss
 *       8        ddd-hh:mm:ss, where input time is an interval
 *
 *  No newline is appended.
 *
 *  Returns a pointer to static space, therefore the same words of
 *  caution from util_dttostr() above apply here as well.
 *
 *----------------------------------------------------------------------
 *
 *  util_tsplit:
 *  Split a double time to yyyy, ddd, hh, mm, ss, msc.
 *
 *----------------------------------------------------------------------
 *
 *  util_ydhmsmtod:
 *  Given year, day, hour, minutes, seconds, and milliseconds, return
 *  a double containing the seconds since 00:00:00.000 Jan 1, 1970.
 *
 *  Only works for times after Jan 1, 1970!
 *
 *----------------------------------------------------------------------
 *
 *  util_jdtomd:
 *  Given year and day of year, determine month and day of month.
 *
 *----------------------------------------------------------------------
 *
 *  util_ymdtojd:
 *  Given year, month, and day determine day of year.
 *
 *----------------------------------------------------------------------
 *
 *  util_today:
 *  Returns today's date in YYYYDDD form.
 *
 *====================================================================*/

#ifndef leap_year
#define leap_year(i) ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0)
#endif

static char _util_daytab[2][13] = {
    {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
    {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
};

static char *_util_month[] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", 
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

static char *_util_day[] = {
    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
};

static char *_util_bad_call = "Bad util_dttostr input!";

#define _UTIL_MIN_CODE 0
#define _UTIL_MAX_CODE 8

static char *_util_bad_str[_UTIL_MAX_CODE-_UTIL_MIN_CODE+1] = {
    "                     ",
    "                         ",
    "                   ",
    "                ",
    "       ",
    "                ",
    "              ",
    "                   ",
    "                "
};

static char _util_out_str[] = "xxxxxxxxxxxxxxxxxxxxxxxxx";

static char copy[1024];

#define SPM (       60L)
#define SPH (SPM *  60L)
#define SPD (SPH *  24L)
#define SPY (SPD * 365L)

/***********************************************************************/

char *util_dttostr(dtime, code)
double dtime;
int code;
{
struct tm *tm;
long  ltime;
float ffrac;
int   ifrac, yr, da, hr, mn, sc, ms;

    if (code < _UTIL_MIN_CODE || code > _UTIL_MAX_CODE) return _util_bad_call;

    ltime = (long) dtime;
    ffrac = (float) (dtime - (double) ltime) * 1000.0;
    ifrac = (int) ffrac;
    if (ifrac < 999 && (ffrac - (float) ifrac >= 0.5)) ifrac++;

/* Deal with the intervals */

    if (code == 2 || code == 8) {
        yr = ltime / SPY; ltime -= yr * SPY;
        da = ltime / SPD; ltime -= da * SPD;
        hr = ltime / SPH; ltime -= hr * SPH;
        mn = ltime / SPM; ltime -= mn * SPM;
        sc = ltime;
        ms = ifrac;
        if (code == 2) {
            sprintf(_util_out_str,"%2.2d:%3.3d-%2.2d:%2.2d:%2.2d.%3.3d",
                yr, da, hr, mn, sc, ms
            );
        } else {
            sprintf(_util_out_str,"%3.3d-%2.2d:%2.2d:%2.2d.%3.3d",
                da, hr, mn, sc, ms
            );
        }
        return _util_out_str;
    }

    if ((tm = gmtime(&ltime)) == NULL) return _util_bad_str[code];
    tm->tm_year += 1900;
    tm->tm_yday += 1;
    tm->tm_mon  += 1;

    switch (code) {
        case 0:
            sprintf(_util_out_str,"%4.4d:%3.3d-%2.2d:%2.2d:%2.2d.%3.3d",
                   tm->tm_year, tm->tm_yday, tm->tm_hour,
                   tm->tm_min, tm->tm_sec, ifrac);
            break;
        case 1:
            sprintf(_util_out_str, "%s %2.2d, %4.4d %2.2d:%2.2d:%2.2d.%3.3d",
              _util_month[tm->tm_mon], tm->tm_mday,tm->tm_year + 1900,
              tm->tm_hour, tm->tm_min, tm->tm_sec, ifrac);
            break;
        case 3:
            sprintf(_util_out_str,"%4.4d%3.3d%2.2d%2.2d%2.2d%3.3d",
                   tm->tm_year, tm->tm_yday, tm->tm_hour,
                   tm->tm_min, tm->tm_sec, ifrac);
            break;
        case 4:
            sprintf(_util_out_str,"%4.4d%3.3d",
                   tm->tm_year, tm->tm_yday);
            break;
        case 5:
            sprintf(_util_out_str, "%s %2.2d/%2.2d/%4.4d",
              _util_day[tm->tm_wday], tm->tm_mon, tm->tm_mday, tm->tm_year);
            break;
        case 6:
            sprintf(_util_out_str,"%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d",
                   tm->tm_year, tm->tm_mon, tm->tm_mday,
                   tm->tm_hour, tm->tm_min, tm->tm_sec);
            break;
        case 7:
            sprintf(_util_out_str,"%4.4d %2.2d %2.2d %2.2d %2.2d %2.2d",
                   tm->tm_year, tm->tm_mon, tm->tm_mday,
                   tm->tm_hour, tm->tm_min, tm->tm_sec);
            break;
        default: 
            strcpy(_util_out_str, _util_bad_call);
            break;
    }

    return _util_out_str;
}

/***********************************************************************/

char *util_lttostr(ltime, code)
long ltime;
int code;
{
char *buffer;

    buffer = util_dttostr((double) ltime, code);
    if (code >= 4 && code <= 7) return buffer;

    if (code != 3) {
        buffer[strlen(buffer)-strlen(".msc")] = 0;
    } else {
        buffer[strlen(buffer)-strlen("msc")] = 0;
    }

    return buffer;
}

/***********************************************************************/

void util_tsplit(dtime, yr, da, hr, mn, sc, ms)
double dtime;
int *yr, *da, *hr, *mn, *sc, *ms;
{
long  ltime;
int   imsc;
double dmsc;
struct tm *tiempo;

    ltime = (long) dtime;
    dmsc = ((dtime - (double) ltime)) * (double) 1000.0;
    imsc = (int) dmsc;
    if (dmsc - (double) imsc >= (double) 0.5) imsc++;
    if (imsc == 1000) {
        ++ltime;
        imsc = 0;
    }

    tiempo = gmtime(&ltime);
    *yr = 1900 + tiempo->tm_year;
    *da = ++tiempo->tm_yday;
    *hr = tiempo->tm_hour;
    *mn = tiempo->tm_min;
    *sc = tiempo->tm_sec;
    *ms = imsc;

}

/***********************************************************************/

double util_ydhmsmtod(yr, da, hr, mn, sc, ms)
int yr, da, hr, mn, sc, ms;
{
int i, days_in_year_part;
long   secs;

    days_in_year_part = 0;
    for (i = 1970; i < yr; i++) days_in_year_part += daysize(i);
    secs = (long) days_in_year_part * SPD;

    secs += (long)(da-1)*SPD + (long)hr*SPH + (long)mn*SPM + (long)sc;

    return (double) secs + ((double) (ms)/1000.0);

}

/***********************************************************************/

int util_jdtomd(year, day, m_no, d_no)
int year, day, *m_no, *d_no;
{
int i, leap;

    leap = leap_year(year);
    
    for (i = 1; day > _util_daytab[leap][i]; i++) day -= _util_daytab[leap][i];

    *m_no = i;
    *d_no = day;
}

/***********************************************************************/

int util_ymdtojd(year, mo, da)
int year, mo, da;
{
int jd, m, leap;

    leap = leap_year(year);
    for (jd = 0, m = 1; m < mo; m++) jd += _util_daytab[leap][m];
    jd += da;

    return jd;
}

/***********************************************************************/

long util_today()
{
time_t now;
struct tm *current;

    now = time(NULL);
    current = localtime(&now);
    current->tm_year += 1900;
    ++current->tm_yday;

    return (1000 * current->tm_year) + current->tm_yday;

}

/* from: type.c	1.1 06/21/96 */
/*======================================================================
 *
 *  dtrec.c
 *
 *  Decode a data record.
 *
 *====================================================================*/

int reftek_type(raw, rev)
char *raw;
int rev;
{
    switch (rev) {
      case 1:
        if (memcmp(raw, "AD", 2) == 0) return REFTEK_AD;
        if (memcmp(raw, "CD", 2) == 0) return REFTEK_CD;
        if (memcmp(raw, "DS", 2) == 0) return REFTEK_DS;
        if (memcmp(raw, "DT", 2) == 0) return REFTEK_DT;
        if (memcmp(raw, "EH", 2) == 0) return REFTEK_EH;
        if (memcmp(raw, "ET", 2) == 0) return REFTEK_ET;
        if (memcmp(raw, "FD", 2) == 0) return REFTEK_FD;
        if (memcmp(raw, "OM", 2) == 0) return REFTEK_OM;
        if (memcmp(raw, "SH", 2) == 0) return REFTEK_SH;
        if (memcmp(raw, "SC", 2) == 0) return REFTEK_SC;
	reftek_errno = REFTEK_EPAK;
	return -1;

      default:
        reftek_errno = REFTEK_EREV;
        return -1;
    }
}

static char buffer[1024];

char *reftek_dtstr(dt)
struct reftek_dt *dt;
{

    sprintf(buffer, "DT ");
    sprintf(buffer + strlen(buffer), "%03d ", dt->exp);
    sprintf(buffer + strlen(buffer), "%05d ", dt->unit);
    sprintf(buffer + strlen(buffer), "%s ",   util_dttostr(dt->tstamp, 0));
    sprintf(buffer + strlen(buffer), "%05d ", dt->seqno);
    sprintf(buffer + strlen(buffer), "%05d ", dt->evtno);
    sprintf(buffer + strlen(buffer), "%02d ", dt->stream);
    sprintf(buffer + strlen(buffer), "%02d ", dt->chan);
    sprintf(buffer + strlen(buffer), "%05d ", dt->nsamp);
    sprintf(buffer + strlen(buffer), "%d ",   dt->format);
    sprintf(buffer + strlen(buffer), "%2d ",  dt->wrdsiz);
    sprintf(buffer + strlen(buffer), "0x%x",  dt->data);

    return buffer;
}


int util_parse(
	char *input,     /* input string                */
	char **argv,     /* output array to hold tokens */
	char *delimiters,/* token delimiters            */
	int  max_tokens, /* max number of tokens (number of elements in argv) */
	char quote       /* quote character for strings */
){
char *ptr;
int i = 0, nquote = 0;

    if (max_tokens < 1) {
        errno = EINVAL;
        return -1;
    }

/* Save embedded blanks inside quoted strings */

    if (quote != 0) {
        for (ptr = input; *ptr != (char) 0; ptr++) {
            if (*ptr == quote) {
                if (++nquote == 2) nquote = 0;
            } else {
                if (nquote == 1 && *ptr == ' ') *ptr = (char) -1;
            }
        }
    }

/* Parse the string, restoring blanks if required */

    if ((argv[0] = strtok(input, delimiters)) == NULL) return 0;
    
    i = 1;
    do {
        if ((argv[i] = strtok(NULL, delimiters)) != NULL && quote != 0) {
            for (ptr = argv[i]; *ptr != (char) 0; ptr++) {
                if (*ptr == (char) -1) *ptr = ' ';
            }
        }
    } while (argv[i] != NULL && ++i < max_tokens);

/* Return the number of tokens */

    return i;
}

#define MAX_INPUT (1024)

int parse(fp, argv, delimiters, max_tokens)
FILE *fp;
char *argv[];
char *delimiters;
int  max_tokens;
{
static char input[MAX_INPUT];
int i = 0;

    if (max_tokens < 1) {
        fprintf(stderr,"parse: illegal 'max_tokens'\n");
        return -1;
    }

    if (fgets(input, MAX_INPUT-1, fp) == NULL) {
        if (feof(fp)) return -1;
        perror("lib_src/util/parse");
        return -1;
    }

    i = 0;
    if ((argv[i] = strtok(input, delimiters)) == NULL) return 0;
    for (i = 1; i < max_tokens; i++) {
        if ((argv[i] = strtok(NULL, delimiters)) == NULL) return i;
    }

    return i;
}


/*  from eh.c	1.1 06/21/96 */
/*======================================================================
 *
 *  Decode an EH record.
 *
 *====================================================================*/

int reftek_eh(dest, src, rev)
struct reftek_eh *dest;
char *src;
int  rev;
{
int i, status;
short *sdata;
long  *ldata;

    dest->rev = rev;

    if (rev == 1) {
        status = reftek_eh1(dest, src);
    } else {
        reftek_errno = REFTEK_EREV;
        status = -1;
    }

    return status;
}
/*  from eh1.c	1.1 06/21/96 */
/*======================================================================
 *
 *  eh1.c
 *
 *  Load a rev 1 event header record
 *
 *====================================================================*/

int reftek_eh1(dest, src)
struct reftek_eh *dest;
char *src;
{
int srate, yr, da, hr, mn, sc, ms;

/* Load the rev 1 common header */

    reftek_common1((unsigned char *)src,&dest->exp,&dest->unit,
		   &dest->seqno,&dest->tstamp);

/* Load the record specific parts */

    dest->evtno  = bcd2int((unsigned char *)src + EVTN_OFF, 4, 0); 
    dest->stream = bcd2int((unsigned char *)src + STRM_OFF, 2, 0);

    switch (bcd2int((unsigned char *)src + FRMT_OFF, 2, 0)) {
      case 16:
        dest->format = REFTEK_F16;
        break;

      case 32:
        dest->format = REFTEK_F32;
        break;

      case 120:
        dest->format = REFTEK_FC0;
        break;

      default:
        reftek_errno = REFTEK_EINVAL;
        return -1;
    }

    sscanf(src + SINT_OFF, "%d", &srate);
    dest->sint = 1.0 / srate;

    if (memcmp(src + TTYP_OFF, "CON", 3) == 0) {
        dest->trgtype = REFTEK_TRGCON;
    } else if (memcmp(src + TTYP_OFF, "CRS", 3) == 0) {
        dest->trgtype = REFTEK_TRGCRS;
    } else if (memcmp(src + TTYP_OFF, "EVT", 3) == 0) {
        dest->trgtype = REFTEK_TRGEVT;
    } else if (memcmp(src + TTYP_OFF, "EXT", 3) == 0) {
        dest->trgtype = REFTEK_TRGEXT;
    } else if (memcmp(src + TTYP_OFF, "LVL", 3) == 0) {
        dest->trgtype = REFTEK_TRGLVL;
    } else if (memcmp(src + TTYP_OFF, "RAD", 3) == 0) {
        dest->trgtype = REFTEK_TRGRAD;
    } else if (memcmp(src + TTYP_OFF, "TIM", 3) == 0) {
        dest->trgtype = REFTEK_TRGTIM;
    } else {
        reftek_errno = REFTEK_EINVAL;
        return -1;
    }

    sscanf(src + TRON_OFF, "%4d%3d%2d%2d%2d%3d",&yr,&da,&hr,&mn,&sc,&ms);
    dest->on = util_ydhmsmtod(yr, da, hr, mn, sc, ms);

    sscanf(src + TOFS_OFF, "%4d%3d%2d%2d%2d%3d",&yr,&da,&hr,&mn,&sc,&ms);
    dest->tofs = util_ydhmsmtod(yr, da, hr, mn, sc, ms);

    return 0;

}

/* from et.c	1.1 06/21/96 */
/*======================================================================
 *
 *  Decode an ET record.
 *
 *====================================================================*/
int reftek_et(dest, src, rev)
struct reftek_et *dest;
char *src;
int  rev;
{
int i, status;
short *sdata;
long  *ldata;

    dest->rev = rev;

    if (rev == 1) {
        status = reftek_et1(dest, src);
    } else {
        reftek_errno = REFTEK_EREV;
        status = -1;
    }

    return status;
}

/* from et1.c	1.1 06/21/96 */
/*======================================================================
 *
 *  et1.c
 *
 *  Load a rev 1 event trailer record
 *
 *====================================================================*/

int reftek_et1(dest, src)
struct reftek_et *dest;
char *src;
{
int srate, yr, da, hr, mn, sc, ms;

/* Load the rev 1 common header */

    reftek_common1((unsigned char *)src,&dest->exp,&dest->unit,
		   &dest->seqno,&dest->tstamp);

/* Load the record specific parts */

    dest->evtno  = bcd2int((unsigned char *)src + EVTN_OFF, 4, 0); 
    dest->stream = bcd2int((unsigned char *)src + STRM_OFF, 2, 0);

    switch (bcd2int((unsigned char *)src + FRMT_OFF, 2, 0)) {
      case 16:
        dest->format = REFTEK_F16;
        break;

      case 32:
        dest->format = REFTEK_F32;
        break;

      case 120:
        dest->format = REFTEK_FC0;
        break;

      default:
        reftek_errno = REFTEK_EINVAL;
        return -1;
    }

    sscanf(src + SINT_OFF, "%d", &srate);
    dest->sint = 1.0 / srate;

    if (memcmp(src + TTYP_OFF, "CON", 3) == 0) {
        dest->trgtype = REFTEK_TRGCON;
    } else if (memcmp(src + TTYP_OFF, "CRS", 3) == 0) {
        dest->trgtype = REFTEK_TRGCRS;
    } else if (memcmp(src + TTYP_OFF, "EVT", 3) == 0) {
        dest->trgtype = REFTEK_TRGEVT;
    } else if (memcmp(src + TTYP_OFF, "EXT", 3) == 0) {
        dest->trgtype = REFTEK_TRGEXT;
    } else if (memcmp(src + TTYP_OFF, "LVL", 3) == 0) {
        dest->trgtype = REFTEK_TRGLVL;
    } else if (memcmp(src + TTYP_OFF, "RAD", 3) == 0) {
        dest->trgtype = REFTEK_TRGRAD;
    } else if (memcmp(src + TTYP_OFF, "TIM", 3) == 0) {
        dest->trgtype = REFTEK_TRGTIM;
    } else {
        reftek_errno = REFTEK_EINVAL;
        return -1;
    }

    sscanf(src + TRON_OFF, "%4d%3d%2d%2d%2d%3d",&yr,&da,&hr,&mn,&sc,&ms);
    dest->on = util_ydhmsmtod(yr, da, hr, mn, sc, ms);

    sscanf(src + TOFS_OFF, "%4d%3d%2d%2d%2d%3d",&yr,&da,&hr,&mn,&sc,&ms);
    dest->tofs = util_ydhmsmtod(yr, da, hr, mn, sc, ms);

    sscanf(src + TOFF_OFF, "%4d%3d%2d%2d%2d%3d",&yr,&da,&hr,&mn,&sc,&ms);
    dest->off = util_ydhmsmtod(yr, da, hr, mn, sc, ms);

    sscanf(src + TOLS_OFF, "%4d%3d%2d%2d%2d%3d",&yr,&da,&hr,&mn,&sc,&ms);
    dest->tols = util_ydhmsmtod(yr, da, hr, mn, sc, ms);

    return 0;

}
