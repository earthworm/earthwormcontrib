# B = ../../bin
L = ${EW_HOME}${EW_VERSION}lib

CFLAGS = -g ${GLOBALFLAGS} -DSYSV_TERM

BINARIES = ref2ew.o r2e_config.o ref_util.o reformat.o \
          $L/chron3.o $L/getutil.o $L/kom.o $L/logit.o $L/time_ew.o \
          $L/pipe.o $L/sleep_ew.o $L/transport.o 

ref2ew: ${BINARIES}
	cc -o ref2ew ${CFLAGS} ${BINARIES} -lm -lposix4 -lnsl -lsocket


.c.o:
	cc -c $(CFLAGS) $<


clean:
	rm -f *.o ref2ew


parse.o: util.h

r2e_config.o: ref2ew.h

ref2ew.o: ref2ew.h

ref_util.o: bcd2int.h protos.h reftek.h util.h

reformat.o: ref2ew.h reftek.h util.h

refline: refline.o
	cc -o refline ${CFLAGS} refline.o

reftcp: reftcp.o
	cc -o reftcp ${CFLAGS} reftcp.o -lnsl -lsocket
