/*
 *
 *
 *  usage:  refline  line baud duration sta_name
 *
 *  where   
 *          line=tty line, ie /dev/sts/ttyd6?
 *          baud=baud rate, ie 38400
 *	    duration=number of buffers to save per file (0 for no save)
 *	    Three letter station code
 */
 
/*
 *  Modification History:
 *  Originally linelog.c from Lamont and/or NEIC but changed by SDM
 *  to do reftek serial line reading and saving for EARTHWORM
 *
 */
 
#include <stdio.h>          /* Standard UNIX definitions */
 

#include <sgtty.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/ttydev.h>
 
#define TANDEM      0       /* define it to be nothing since it's unsupported */
 
/* Global Variables */
 
int	ttyfd,              /* File descriptor of tty for I/O, 0 if remote */
	fd,		    /* file descriptor of file to transmitt */
	duration;	    /* number of buffers per file,
				duration = 0 if no saved file */
char	staname[8];	    /* Station code for file name */
 
char    filename[20];            /* Current file name */
 
/*
 *  m a i n
 *
 *  Main routine - parse command and options, set up the
 *  tty lines, and dispatch to reading routines
 */
 
main(argc,argv)
int argc;                           /* Character pointers to and count of */
char **argv;                            /* command line arguments */
{
    char *ttyname,                      /* tty name for LINE argument */
        *cp;                            /* char pointer */
    int speed,                          /* speed of assigned tty, */
        cflg, rflg, sflg;               /* flags for CONNECT, RECEIVE, SEND */
 
    struct sgttyb
        rawmode,                        /* Controlling tty raw mode */
        cookedmode,                     /* Controlling tty cooked mode */
        ttymode;                        /* mode of tty line in LINE option */
    if (argc != 5) usage();              /* Make sure there's a command line */
 
    ttyname = *++argv;
/*    if(! strncmp(ttyname,"/dev/",5)) usage(); */
    speed = atoi(*++argv);
    duration = atoi(*++argv);
    strcpy(staname, *++argv);
 
printf("tty: %s, speed: %d, duration: %d, staname: %s\n",ttyname, speed, duration, staname);
/* Done parsing */
 
 
 
    if (ttyname) {                        /* If LINE was specified, we */
        ttyfd = open(ttyname,2);        /* Open the tty line */
        if (ttyfd < 0) {
            printmsg("Cannot open %s",ttyname);
            exit(1);
        }
    }
 
/* Put the proper tty into the correct mode */
                                   /* Local, use assigned line */
        gtty(ttyfd,&ttymode);
        ttymode.sg_flags |= (RAW|TANDEM);
        ttymode.sg_flags &= ~(ECHO|CRMOD);
 
        switch(speed)               /* Get internal system code */
        {
            case 110: speed = B110; break;
            case 150: speed = B150; break;
            case 300: speed = B300; break;
            case 1200: speed = B1200; break;
            case 2400: speed = B2400; break;
            case 4800: speed = B4800; break;
            case 9600: speed = B9600; break;
            case 19200: speed = B19200; break;
            case 38400: speed = B38400; break;
 
            default:
                printmsg("Bad line speed.");
                exit(1);
        }
        ttymode.sg_ispeed = speed;
        ttymode.sg_ospeed = speed;
 
        stty(ttyfd,&ttymode);           /* Put asg'd tty in raw mode */
 
/* All set up */
 
    connect();                /* Connect command */
}
 
/*
 *  c o n n e c t
 *
 *  Establish a connection over an
 *  assigned tty line.
 */

/* Refpacket header structure   */
    struct ch_hdr_exp {         /* packet header expanded to characters */
        char            type[2];/* packet type */
        char            exp_num[2];     /* experiment # */
        char            yr[2];  /* year */
        char            u_id[4];/* unit id # */
        char            day[3]; /* day */
        char            hour[2];/* hour */
        char            min[2]; /* minute */
        char            sec[2]; /* second */
        char            mil[3]; /* millisecond */
        char            bytes[4];       /* bytes in packet */
        char            seq_n[4];       /* sequence number */
    } cheader;

 
connect()
{
    int	n;				/* Number of characters received */ 
    int nbufs;
    int nall, nr;
    char inbuf[1025];
    char outbuf[1025];
    int year, mon, day;
    char *chrptr;
    int retry;

    chrptr= (char)&cheader;
    nbufs=duration;
    while(1)                        /* Do this forever */
    {
	retry=20;
	nall=1024;
	n=0;
	while(nall) {
		if((nr = read(ttyfd,inbuf+n,nall)) == -1 ) {
			fprintf(stderr,"REFLINE ERROR on reading\n");
			exit(1);
		}
		if(n== 0  && (inbuf[0] != 'D' || inbuf[1] != 'T')) {
		   fprintf(stdout,"REFLINE: Lost sync. retry # %d\n", retry);
		   fflush(stdout);
		   retry--;
		   if(retry == 0) {
			fprintf(stdout,"Max retrys. Quitting\n");
			exit(1);
		   }
		   continue;
		}
		n += nr;
		nall -= nr;
	}

	if( nbufs == duration && duration > 0) {
 
	   nbufs = 0;
	   if(fd > 0) close(fd);
		/* New file name */
	   unbcd(&cheader.exp_num, inbuf+2, 14);  /* convert BCD to ascii */
	   sscanf(chrptr+4,"%2d",&year);
	   sscanf(chrptr+10,"%3d",&day);
	   mthday_(&year,&day,&mon,&day);
	   sprintf(filename,"%02d%02d%02d%.2s%.2s.%.3s",year, mon, day,
		cheader.hour, cheader.min, staname);
	   printf("Opening file, %s\n",filename);
	   fflush(stdout);
	   fd = open(filename,O_CREAT|O_RDWR|O_APPEND,0666);
	   if ( fd < 0 ){
		printmsg("can't open file %s \r",filename);
		exit(0);
	   }
	}
	write(fd,inbuf,n);
	nbufs++;
    }
}
 
 
 
/*
 *  refline printing routines:
 *
 *  usage - print command line options showing proper syntax
 *  printmsg -  like printf with "Refline: " prepended
 */
 
/*
 *  u s a g e
 *
 *  Print summary of usage info and quit
 */
 
usage()
{
    printf("Usage: refline line baud duration staname\n");
    exit(1);
}
 
/*
 *  p r i n t m s g
 *
 *  Print message on standard output if not remote.
 */
 
/*VARARGS1*/
printmsg(fmt, a1, a2, a3, a4, a5)
char *fmt;
{
        printf("Refline: ");
        printf(fmt,a1,a2,a3,a4,a5);
        printf("\n");
        fflush(stdout);                 /* force output (UTS needs it) */
}
 
 
/*************************************************************************/
/*                     unbcd()                                           */
/*************************************************************************/
/*	convert from bcd to ascii
*/


unbcd(dest, source, sbytes)
    unsigned char   dest[];
    unsigned char   source[];
    unsigned short  sbytes;
{
    unsigned short  i;

    for (i = 0; i < sbytes; i++) {
	dest[i * 2] = (source[i] >> 4) + 0x30;
	dest[i * 2 + 1] = (source[i] & 0x0F) + 0x30;
    }
    dest[i * 2] = '\0';
}

/*********************  end of unbcd()  ***********************/

/***  conversion from julian day *********/

static long daytab[2][13] = {
	{0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
	{0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};

mthday_(yr,yday,mon,mday)
long *yr,*yday,*mon,*mday;
{
	int i,leap;
	leap= *yr%4 ==0 && *yr%100 != 0 || *yr%400 == 0;
	for(i = 1; *yday> daytab[leap][i]; i++)
		*yday -= daytab[leap][i];
	*mon= i;
	*mday= *yday;
}
