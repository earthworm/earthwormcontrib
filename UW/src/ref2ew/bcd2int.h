/************************************************************************/
/* FILE:	bcd2int.h						*/
/* AUTHOR:	Bruce Crawford		UNR Seismology			*/
/************************************************************************/

#ifndef	BINARY_CODES_H
#define	BINARY_CODES_H

int bcd2int (unsigned char *input, int numDigits, int nibble);

#endif	/* BINARY_CODES_H */
