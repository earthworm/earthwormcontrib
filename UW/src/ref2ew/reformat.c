/*======================================================================
 *
 * Reformat reftek data packet into tracebuf message
 * Based on similar code from Dave Chavez (UNR)
 * Modified by Pete Lombard, UW Seismology 10/27/97
 * Includes treatment for Terra Technology GPS special DT header fields
 *
 *====================================================================*/
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <time.h>
#include <trace_buf.h>
#include "ref2ew.h"
#include "reftek.h"
#include "util.h"

extern struct params Par;
static unsigned char type_tracebuf;

/* function prototypes
**********************/
char *datatype(int, unsigned long, int);


char *reformat(dt, ibuf, obuf, nbyte, var)
struct reftek_dt *dt;     /* reftek DT header */
char             *ibuf;   /* input (reftek) data */
char             *obuf;   /* output (tracebuf) data */
int              *nbyte;  /* number of bytes to transfer */
int              var;     /* reftek variant flag */
{
  int pin, status;
  char *sname, *cname, *nname;
  static TracePacket *tpak;
  static unsigned long native_order = 0;
  static double last_time[8] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
  long first_samp, last_samp, *message_data;
  int  gps_stat;
  time_t timenow;

  tpak = (TracePacket *) obuf;
  message_data = (long *) (obuf + sizeof(TRACE_HEADER));

  if (native_order == 0) native_order = util_order();

  /* if the packet is compressed, decompress it now */
  if (dt->format == REFTEK_FC0) {
    if (Par.debug) logit( "t", "compressed packet\n");
    status = reftek_dcomp(dt, (int *) message_data, native_order);
    if (status != 0) {

      logit("et", "WARNING: reftek_dcomp(%d): %s packet dropped)\n",
	    status, reftek_errstr()
	    );
      logit("et", "DROPPED %s\n", reftek_dtstr(dt));
      *nbyte = 0;
      return ibuf;
    }
  } else {
    /* not compressed; just copy the data */
    memcpy( message_data, dt->raw, dt->nsamp * dt->wrdsiz );
  }

  tpak->trh.pinno      = Par.channel[dt->chan].pin;
  strcpy(tpak->trh.sta,  Par.station);
  strcpy(tpak->trh.chan, Par.channel[dt->chan].name);
  if (strlen(tpak->trh.chan) == 0) {
    logit("et", "packet from unexpected channel: %d; rejected\n", dt->chan);
    *nbyte = 0;
    return (char *)0;
  }
  strcpy(tpak->trh.net,  Par.network);
  tpak->trh.starttime  = dt->tstamp + Par.timeshift;
 
  timenow = time((time_t)0);
  if (tpak->trh.starttime < (double) (timenow - 604800)) {
    logit("et", "old packet start time: %f; rejected\n", tpak->trh.starttime);
    *nbyte = 0;
    return (char *)0;
  }
  if (tpak->trh.starttime > (double) timenow + 1.0 ) {
    logit("et", "future packet start time: %f; rejected\n", tpak->trh.starttime);
    *nbyte = 0;
    return (char *)0;
  }
  if (tpak->trh.starttime < last_time[dt->chan]) {
    logit("et", "channel %d time going backwards; this: %f; last %f; rejected\n", 
	  dt->chan, tpak->trh.starttime, last_time[dt->chan]);
    *nbyte = 0;
    return (char *)0;
  }
  tpak->trh.endtime    = tpak->trh.starttime + 
    ((double)(dt->nsamp - 1) / Par.samprate);
  tpak->trh.nsamp      = dt->nsamp;
  tpak->trh.samprate   = Par.samprate;
  strcpy( tpak->trh.datatype, datatype(dt->wrdsiz, dt->order, 1) );
  if (strcmp(tpak->trh.datatype, "-") == 0) {
    logit("et", "unknown data format; packet rejected\n");
    *nbyte = 0;
    return (char *)0;
  }
  *nbyte = dt->nsamp * dt->wrdsiz + sizeof(TRACE_HEADER);
  if (*nbyte > 1064 ) {   /* wavetrace packet size for 250 4-byte samples */
    logit("et", "packet too long: %d bytes; rejected\n", *nbyte);
    *nbyte = 0;
    return (char *)0;
  }
  
  if (var == REF_VAR_TERRA) {
    /* Terra Technology IDS-24 boxes use two of the reftek DT fields for
     * non-standard data. EXPN contains GPS status (bits 4-7) and message
     * sequence number (bits 0-3). UNIT contains Terra's Firmware Rev Number;
     * must be that it changes frequently!
     * We pull the GPS status back from dt->exp which has been through bcd2int().
     * dt->exp has the sequence number in the one's digit in decimal.
     * The ten's digit in dt->exp contains the GPS status shifted 4 to the right
     * from the Terra DT packet. Thus dt->exp / 10 contains:
     *        bit 3 = no time lock
     *        bit 2 = No 1PPS
     *        bit 1 = no RxD (???)
     *        bit 0 = GPS/UTC no sync
     * We declare time questionable if any of these are set.
     */
    gps_stat = dt->exp / 10;
    if (gps_stat & 0xF) tpak->trh.quality[0] = (char)TIME_TAG_QUESTIONABLE;
  }

  if (Par.debug) {
    fprintf(stderr, "%3s:%s:%s %3d %s ",
	    tpak->trh.sta, tpak->trh.chan, tpak->trh.net, tpak->trh.pinno,
	    util_dttostr(tpak->trh.starttime, 0)
            );
    
    first_samp = message_data[0];
    last_samp  = message_data[tpak->trh.nsamp-1];
    fprintf(stderr, "%s %3d %5.1f %s %4d %6ld %6ld\n",
	    util_dttostr(tpak->trh.endtime, 0), tpak->trh.nsamp,
	    tpak->trh.samprate, tpak->trh.datatype, *nbyte,
	   first_samp, last_samp
            );
  }

  last_time[dt->chan] = tpak->trh.starttime;
  return (char *) obuf;
}

/* datatype: returns a text string describing the data type based on
 * the word size, byte order, and integer flag.
 */
char *datatype(int wrdsiz, unsigned long order, int integer)
{
static char *s2 = "s2";
static char *s4 = "s4";
static char *t4 = "t4";
static char *i2 = "i2";
static char *i4 = "i4";
static char *f4 = "f4";
static char *undefined = "-";

    if ( integer && order == BIG_ENDIAN_ORDER && wrdsiz == 2) return s2;
    if ( integer && order == BIG_ENDIAN_ORDER && wrdsiz == 4) return s4;
    if (!integer && order == BIG_ENDIAN_ORDER && wrdsiz == 4) return t4;

    if ( integer && order == LTL_ENDIAN_ORDER && wrdsiz == 2) return i2;
    if ( integer && order == LTL_ENDIAN_ORDER && wrdsiz == 4) return i4;
    if (!integer && order == LTL_ENDIAN_ORDER && wrdsiz == 4) return f4;

    return undefined;
}
