/*
 *
 *
 *  usage:  reftcp  host port duration sta_name
 *
 *  where   
 *          host=hostname, ie geops
 *          port=port number, ie 14011
 *	    duration=number of buffers to save per file (0 for no save)
 *	    Three letter station code
 */
 
/*
 *  Modification History:
 *  Originally linelog.c from Lamont and/or NEIC but changed by SDM
 *  to do reftek serial line reading and saving for EARTHWORM
 *  Changed by PNL to connect to tcp port instead of serial line.
 *
 */
 
#include <stdio.h>          /* Standard UNIX definitions */
 

#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/ttydev.h>
 
 
/* Global Variables */
 
int	sockfd,              /* File descriptor of tty for I/O, 0 if remote */
  fd,		    /* file descriptor of file to save */
  duration;	    /* number of buffers per file,
		       duration = 0 if no saved file */
char	staname[8];	    /* Station code for file name */
char    filename[20];            /* Current file name */

/* signal handler for all interrupts */
void onintr( int );
void doit( void );
 
/*
 *  m a i n
 *
 *  Main routine - parse command and options, set up the
 *  tty lines, and dispatch to reading routines
 */
 
main(argc,argv)
     int argc;                           /* Character pointers to and count of */
     char **argv;                            /* command line arguments */
{
  char *hostname;
  int port;
  struct sockaddr_in sin;
  struct hostent *hp;
  
  if (argc != 5) usage();              /* Make sure there's a command line */
 
  /* Signal catching for cleaning up before exit */
  signal(SIGHUP, onintr);
  signal(SIGINT, onintr);
  signal(SIGILL, onintr);
  signal(SIGSEGV, onintr);
  signal(SIGBUS, onintr);
  signal(SIGPIPE, onintr);

  hostname = *++argv;
  port = atoi(*++argv);
  duration = atoi(*++argv);
  strcpy(staname, *++argv);
 
  printf("host: %s, port: %d, duration: %d, staname: %s\n",hostname, port, duration, staname);
  /* Done parsing */
 
 
  
  if ( (hp = gethostbyname(hostname)) == 0)
    {
      printf("reftcp: Unable to get host name, %s",hostname);
      onintr(1);
    }
  
  memset(&sin,0,sizeof(sin));
  memcpy(&sin.sin_addr,hp->h_addr,hp->h_length);
  sin.sin_family = AF_INET;
  sin.sin_port = port;
  
  if( (sockfd = socket(AF_INET,SOCK_STREAM,0)) == -1) {
    printf("reftcp: Socket call failed\n");
    onintr(1);
  }
  if(connect(sockfd,(struct sockaddr *) &sin,(int) sizeof(sin)) == -1) {
    printf("reftcp: Connect call failed\n");
    onintr(1);
  }
  printf("ref2cp: using port , %d from %s: %d\n",port,hostname);


 
  /* All set up */
 
  doit();                /* Connect command */
}
 
/*
 */

/* Refpacket header structure   */
struct ch_hdr_exp {         /* packet header expanded to characters */
  char            type[2];/* packet type */
  char            exp_num[2];     /* experiment # */
  char            yr[2];  /* year */
  char            u_id[4];/* unit id # */
  char            day[3]; /* day */
  char            hour[2];/* hour */
  char            min[2]; /* minute */
  char            sec[2]; /* second */
  char            mil[3]; /* millisecond */
  char            bytes[4];       /* bytes in packet */
  char            seq_n[4];       /* sequence number */
} cheader;

 
void doit()
{
  int	n;				/* Number of characters received */ 
  int nbufs;
  int nall, nr;
  char inbuf[1025];
  char outbuf[1025];
  int year, mon, day;
  char *chrptr;
  int retry;

  chrptr= (char*)&cheader;
  nbufs=duration;
  while(1)                        /* Do this forever */
    {
      retry=20;
      nall=1024;
      n=0;
      while(nall) {
	if((nr = read(sockfd,inbuf+n,nall)) == -1 ) {
	  fprintf(stderr,"REFLINE ERROR on reading\n");
	  exit(1);
	}
	if(n== 0  && (inbuf[0] != 'D' || inbuf[1] != 'T')) {
	  fprintf(stdout,"REFLINE: Lost sync. retry # %d\n", retry);
	  fflush(stdout);
	  retry--;
	  if(retry == 0) {
	    fprintf(stdout,"Max retrys. Quitting\n");
	    onintr(1);
	  }
	  continue;
	}
	n += nr;
	nall -= nr;
      }

      if( nbufs == duration && duration > 0) {
 
	nbufs = 0;
	if(fd > 0) close(fd);
	/* New file name */
	unbcd(&cheader.exp_num, inbuf+2, 14);  /* convert BCD to ascii */
	sscanf(chrptr+4,"%2d",&year);
	sscanf(chrptr+10,"%3d",&day);
	mthday_(&year,&day,&mon,&day);
	sprintf(filename,"%02d%02d%02d%.2s%.2s.%.3s",year, mon, day,
		cheader.hour, cheader.min, staname);
	printf("Opening file, %s\n",filename);
	fflush(stdout);
	fd = open(filename,O_CREAT|O_RDWR|O_APPEND,0666);
	if ( fd < 0 ){
	  printmsg("can't open file %s \r",filename);
	  exit(0);
	}
      }
      write(fd,inbuf,n);
      nbufs++;
    }
}
 
 
 
/*
 *  reftcp printing routines:
 *
 *  usage - print command line options showing proper syntax
 *  printmsg -  like printf with "Reftcp: " prepended
 */
 
/*
 *  u s a g e
 *
 *  Print summary of usage info and quit
 */
 
usage()
{
  printf("Usage: reftcp host port duration staname\n");
  exit(1);
}
 
/*
 *  p r i n t m s g
 *
 *  Print message on standard output if not remote.
 */
 
/*VARARGS1*/
printmsg(fmt, a1, a2, a3, a4, a5)
     char *fmt;
{
  printf("reftcp: ");
  printf(fmt,a1,a2,a3,a4,a5);
  printf("\n");
  fflush(stdout);                 /* force output (UTS needs it) */
}
 
 
/*************************************************************************/
/*                     unbcd()                                           */
/*************************************************************************/
/*	convert from bcd to ascii
 */


unbcd(dest, source, sbytes)
     unsigned char   dest[];
     unsigned char   source[];
     unsigned short  sbytes;
{
  unsigned short  i;

  for (i = 0; i < sbytes; i++) {
    dest[i * 2] = (source[i] >> 4) + 0x30;
    dest[i * 2 + 1] = (source[i] & 0x0F) + 0x30;
  }
  dest[i * 2] = '\0';
}

/*********************  end of unbcd()  ***********************/

/***  conversion from julian day *********/

static long daytab[2][13] = {
  {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
  {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};

mthday_(yr,yday,mon,mday)
     long *yr,*yday,*mon,*mday;
{
  int i,leap;
  leap= *yr%4 ==0 && *yr%100 != 0 || *yr%400 == 0;
  for(i = 1; *yday> daytab[leap][i]; i++)
    *yday -= daytab[leap][i];
  *mon= i;
  *mday= *yday;
}

void onintr(sig)     /* interupt service routine */
     int sig;
{
  printf("reftcp: termination with signal, %d\n",sig);
  close(sockfd);
  close(fd);
  sleep(2);
  exit(1);
}
