/* @(#)reftek.h	1.6 8/6/96 */
/*======================================================================
 *
 * reftek.h
 *
 * Various defines in support of the reftek utility library.
 *
 *====================================================================*/
#ifndef reftek_h_included
#define reftek_h_included

#include <sys/types.h>

/* Misc constants */

#define REFTEK_MAXPAKLEN 1024 /* largest raw packet size              */
#define REFTEK_MAXDAT    1000 /* largest sample data bytes            */
#define REFTEK_MAXC0      892 /* max number of C0 compressed samples  */
#define REFTEK_MAXNAMLEN   64 /* max ascii name length                */
#define REFTEK_MAXNSTRM     4 /* max number of streams in a DS record */

/* packet types */

#define REFTEK_AD 0x0001
#define REFTEK_CD 0x0002
#define REFTEK_DS 0x0004
#define REFTEK_DT 0x0008
#define REFTEK_EH 0x0010
#define REFTEK_ET 0x0020
#define REFTEK_FD 0x0040
#define REFTEK_OM 0x0080
#define REFTEK_SH 0x0100
#define REFTEK_SC 0x0200

/* error conditions */

extern int reftek_errno;

#define REFTEK_EREV   1  /* unknown or unsupported format revision */
#define REFTEK_EPAK   2  /* unknown or unsupported packet type     */
#define REFTEK_EDCMP  3  /* decompression error                    */
#define REFTEK_EINVAL 4  /* invalid input                          */

/* Data format codes */

#define REFTEK_F16  1  /* 16-bit uncompressed */
#define REFTEK_F32  2  /* 32-bit uncompressed */
#define REFTEK_FC0  3  /* Steim 1 compressed  */

/* Auxlliary Data (AD) packet */

struct reftek_ad {
    int    rev;                  /* original data rev      */
    int    exp;                  /* experiment number      */
    int    unit;                 /* unit id                */
    double tstamp;               /* timestamp              */
    int    seqno;                /* sequence number        */
    float  sint;                 /* sample interval        */
    int    format;               /* data format flag       */
};

/* Calibration Definition (CD) packet */

struct reftek_cd {
    int dummy1;
    int dummy2;
};

/* Data Stream (DS) packet */

#define REFTEK_TRGCON 1
#define REFTEK_TRGCRS 2
#define REFTEK_TRGEVT 3
#define REFTEK_TRGEXT 4
#define REFTEK_TRGLVL 5
#define REFTEK_TRGRAD 6
#define REFTEK_TRGTIM 7

struct reftek_contrg {
    float dur;       /* event duration (sec) */
};

struct reftek_crstrg {
    int key;         /* trigger stream number   */
    float pretrig;   /* pretrigger length (sec) */
    float dur;       /* event duration (sec)    */
};

struct reftek_evttrg {
    int dummy1;
    int dummy2;
};

struct reftek_exttrg {
    int dummy1;
    int dummy2;
};

struct reftek_lvltrg {
    int dummy1;
    int dummy2;
};

struct reftek_radtrg {
    int dummy1;
    int dummy2;
};

struct reftek_timtrg {
    int dummy1;
    int dummy2;
};

struct reftek_stream {
    int   ident;                    /* stream number   */
    char  name[REFTEK_MAXNAMLEN+1]; /* stream name     */
    float sint;                     /* sample interval */
    int   trgtype;                  /* trigger type    */
    union {
        struct reftek_contrg con;
        struct reftek_contrg crs;
        struct reftek_contrg evt;
        struct reftek_contrg ext;
        struct reftek_contrg lvl;
        struct reftek_contrg rad;
        struct reftek_contrg tim;
    } trg;                          /* trigger info     */
};

struct reftek_ds {
    int    rev;                  /* original data rev                   */
    int    exp;                  /* experiment number                   */
    int    unit;                 /* unit id                             */
    double tstamp;               /* timestamp                           */
    int    seqno;                /* sequence number                     */
    int    nstream;              /* no. stream descriptors to follow    */
    struct reftek_stream stream[REFTEK_MAXNSTRM]; /* stream descriptors */
};

/* Data (DT) packet */

struct reftek_dt {
    int    rev;                  /* original data rev      */
    int    exp;                  /* experiment number      */
    int    unit;                 /* unit id                */
    double tstamp;               /* timestamp              */
    int    seqno;                /* sequence number        */
    int    evtno;                /* event number           */
    int    stream;               /* stream id              */
    int    chan;                 /* channel id             */
    int    nsamp;                /* number of samples      */
    int    format;               /* data format flag       */
    int    wrdsiz;               /* uncompressed wordsize  */
    u_long order;                /* data byte order        */
    char   raw[REFTEK_MAXDAT];   /* raw data (compressed?) */
    char  *data;                 /* decompressed data      */
};

/* Event Header (EH) packet */

struct reftek_eh {
    int    rev;                  /* original data rev      */
    int    exp;                  /* experiment number      */
    int    unit;                 /* unit id                */
    double tstamp;               /* timestamp              */
    int    seqno;                /* sequence number        */
    int    evtno;                /* event number           */
    int    stream;               /* stream id              */
    int    format;               /* data format flag       */
    char   name[REFTEK_MAXNAMLEN+1]; /* stream name        */
    float  sint;                 /* sample interval        */
    int    trgtype;              /* trigger type           */
    double on;                   /* trigger time           */
    double tofs;                 /* time of first sample   */
};

/* Event Trailer (ET) packet */

struct reftek_et {
    int    rev;                  /* original data rev      */
    int    exp;                  /* experiment number      */
    int    unit;                 /* unit id                */
    double tstamp;               /* timestamp              */
    int    seqno;                /* sequence number        */
    int    evtno;                /* event number           */
    int    stream;               /* stream id              */
    int    format;               /* data format flag       */
    char   name[REFTEK_MAXNAMLEN+1]; /* stream name        */
    float  sint;                 /* sample interval        */
    int    trgtype;              /* trigger type           */
    double on;                   /* trigger on  time       */
    double off;                  /* trigger off time       */
    double tofs;                 /* time of first sample   */
    double tols;                 /* time of last  sample   */
};

/* Operating Mode (OM) packet */

struct reftek_om {
    int    rev;                  /* original data rev      */
    int    exp;                  /* experiment number      */
    int    unit;                 /* unit id                */
    double tstamp;               /* timestamp              */
    int    seqno;                /* sequence number        */
};

/* Station/Channel (SC) packet */

struct reftek_sc {
    int    rev;                  /* original data rev      */
    int    exp;                  /* experiment number      */
    int    unit;                 /* unit id                */
    double tstamp;               /* timestamp              */
    int    seqno;                /* sequence number        */
};

/* State of Health (SH) packet */

struct reftek_sh {
    int    rev;                  /* original data rev      */
    int    exp;                  /* experiment number      */
    int    unit;                 /* unit id                */
    double tstamp;               /* timestamp              */
    int    seqno;                /* sequence number        */
};

/* Function prototypes */

#ifdef __STDC__

int reftek_dcomp(
    struct reftek_dt *,
    int *,
    u_long
);

int reftek_dt(
    struct reftek_dt *,
    char *,
    int
);

int reftek_eh(
    struct reftek_eh *,
    char *,
    int
);

int reftek_et(
    struct reftek_et *,
    char *,
    int
);

char *reftek_errstr(
    void
);

void reftek_init(
    void
);

int reftek_type(
    char *,
    int
);

char *reftek_ehstr(
    struct reftek_eh *
);

char *reftek_etstr(
    struct reftek_et *
);

char *reftek_dtstr(
    struct reftek_dt *
);

#endif /* __STDC__ */

#endif /* reftek_h_included */
