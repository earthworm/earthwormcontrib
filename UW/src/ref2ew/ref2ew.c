/*
 *  ref2ew: Earthworm module to get reftek data packets from Terra boxes
 *  via tty or tcp, make them into TraceBuf messages and put them on a ring.
 */
 
/*
 *  Modification History:
 *  Hacked together by Pete Lombard (UW Seismology) using bits and pieces
 *  from Iceworm, UNR Seismology, Passcal and others.
 *
 *  Command line argument:
 *       arg[1] = Name of configuration file
 */
 
#define Debug 1

#include <stdio.h>          /* Standard UNIX definitions */
#include <string.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <stropts.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <poll.h>
#include <errno.h>

#include <earthworm.h>
#include <trace_buf.h>
#include "ref2ew.h"
#include "reftek.h"
#include "util.h"

/* Error conditions from ref_get_packet() */
#define R2E_TIMEOUT -3
#define R2E_EOF -2
#define R2E_ERR -1
 
/* Function prototypes
*********************/
int    GetConfig( char * );
void   LogConfig( void );
int    GetPins( void );
void   ref2ew_lookup( void );
int    init_io( void );
void   ref2ew_status( unsigned char, short, char * );
int    ref_get_packet( int, char **, size_t *);
void   do_dtfile( struct reftek_dt *, char * );
char   *reformat( struct reftek_dt *, char *, char *, size_t *, int );
void   ref2ew_die( int );
void   od( char *, int );

/* Look up from earthworm.d
 **************************/
static unsigned char TypeHeartBeat;
static unsigned char TypeError;
static unsigned char TypeTraceBuf;
 
struct params Par;
int reftek_errno;
static long npto = 0;  /* counter for poll() timeouts */
static char errmsg[80];
 
/*
 *  m a i n
 *
 */
 
int main(int argc, char *argv[])
{
  MSG_LOGO     logo;          /* Logo of outgoing tracebuf message */
  time_t       timeLastBeat;  /* Previous heartbeat time */
  time_t       timeNow;

  size_t       nbyte, maxdat = REFTEK_MAXPAKLEN;
  char         inbuf[2 * REFTEK_MAXPAKLEN];/* raw reftek input buffer */
  char         *ib_ptr;
  char         outbuf[MAX_TRACEBUF_SIZ]; /* wavetrace output buffer */

  int          in_fd;         /* input file descriptor */
  int          retry = 0;     /* how many times to try getting in sync */
                              /* zero means try forever */
  int          retval;
  int          ref_rev = 1;   /* Reftek revision */
  int          type;
  struct       reftek_dt dt;  /* decoded reftek data packet */
  char         *message;      /* pointer to outgoing message */

  /* Check arguments
  *****************/
  if ( argc != 2 ) {
    fprintf( stderr, "Usage: ref2ew <configfile>\n" );
    exit( 1 );
  }
  /* Read configuration file
  *************************/
  if( GetConfig( argv[1] ) == -1) {
    printf("ref2ew: Error getting configuration file, %s. Exitting\n",
	   argv[1] );
    exit(1);
  }
  /* Look up important information from earthworm.d tables */
  ref2ew_lookup();

  /* Initialize name of log-file & open it
  ***************************************/
  logit_init( "ref2ew", (short) Par.MyModId, 256, 1 );

  /* Look up pin numbers from pick_ew's station file */
  if( GetPins() == -1) {
    printf("ref2ew: Error getting pin numbers; exitting\n");
    exit(1);
  }

  /* Log parameters from the configuration file
  ******************************************/
  LogConfig();
 
  /* Attach to existing transport ring
  **********************************/
  tport_attach( &Par.region, Par.ringkey );

  /* Set up outgoing message logo
  *******************************/
  logo.instid = Par.MyInstId;
  logo.mod    = Par.MyModId;
  logo.type   = TypeTraceBuf;

  /* Initialize the input device */
  retval= init_io( );
  if (retval != 0)  /* something went wrong; give up now! */
    {
      logit( "et", "ref2ew: TTY setup failed; exitting!\n" );
      ref2ew_die(-1);
    }

  /* force a heartbeat to be issued in first pass thru loop */
  timeLastBeat = time(&timeNow) - Par.hbeatint - 1;

  /* Main loop to read messages, decode and write to ring 
  ******************************************************/
  while(1)                        /* Do this forever */
    {
      /* send ref2ew heartbeat
      *****************************/
      if ( time(&timeNow) - timeLastBeat  >=  Par.hbeatint ) {
	timeLastBeat = timeNow;
	ref2ew_status( TypeHeartBeat, 0, "" );
      }
 
      /* See if it's time to stop
      **************************/
      if( tport_getflag( &Par.region ) == TERMINATE ) {
	logit( "t", "ref2ew: Termination requested; exitting!\n" );
	tport_detach( &Par.region );
	exit( 0 );
      }
 
      /* Assemble an incoming data block 
      *********************************/
      ib_ptr = inbuf;
      nbyte = maxdat;
      type = ref_get_packet(Par.in_fd, &ib_ptr, &nbyte );
      if (Par.debug) logit("t", "ref_get_packet returns %d\n", type);

      /* Deal with different types or error returns */
      switch ( type ) {
      case R2E_ERR:   /* ref_get_packet already blabbed so we won't here */
      case R2E_EOF:
	ref2ew_die(-1);
	break;

      case R2E_TIMEOUT:
	if (npto == 1) { /* report the first failure */
	  sprintf(errmsg, "ref2ew stopped reading from %s", Par.station);
	  ref2ew_status( TypeError, 0, errmsg);
	}
	/* We'll keep trying. If the sender comes back on line, we'll resume
	 * reading from it. Or maybe we'll get an error or EOF and quit
	 */
	continue;
	break;

      case REFTEK_DT:
	/* decode the DT packet header */
	if (( retval = reftek_dt( &dt, ib_ptr, 1)) != 0 ) {
	  logit( "et", "ref2ew: can't decode DT packet from %s: error %d\n",
		 Par.station, retval );
	  continue;
	}
	/* Optionally write the raw DT packet to file */
	if( Par.nfilebufs > 0) do_dtfile( &dt, ib_ptr );

	/* Reformat packet into tracebuf message */
	message = reformat( &dt, ib_ptr, outbuf, &nbyte, Par.refvar );

	if ((nbyte == 0) || (message == (char *)NULL)) continue;

	/* Write the new message to the output ring */
	retval = tport_putmsg( &Par.region, &logo, nbyte, message);
	if ( retval == PUT_NOTRACK )
	  logit( "et", "ref2ew: Tracking error from tport_putmsg\n" );
	if ( retval == PUT_TOOBIG )
	  logit( "et", "ref2ew: Error. Outgoing message too big.\n" );

	break;
	
      default:
	  /* Case for non-data packet */
	  logit( "t", "unexpexted non-data packet from %s\n", Par.station );
	  if (retry == 1000) {
	    logit( "et", "ref2ew: too many bad packets; giving up!\n" );
	    ref2ew_die(-1);
	    retry++;
	  }
	  break;

      }

    }
}
 
/***************************************************************************
 *  ref2ew_lookup( )   Look up important info from earthworm.d tables  *
 ***************************************************************************/
void ref2ew_lookup( void )
{
  if ( GetLocalInst( &Par.MyInstId ) != 0 ) {
    fprintf( stderr,
	     "ref2ew: error getting local installation id; exitting!\n" );
    exit( -1 );
  }
  if( GetType( "TYPE_TRACEBUF", &TypeTraceBuf ) != 0 ) {
    fprintf( stderr,
	     "ref2ew: Invalid message type <TYPE_TRACEBUF>; exitting!\n" );
    exit( -1 );
  }
  if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
    fprintf( stderr, 
	     "ref2ew: Invalid message type <TYPE_HEARTBEAT>; exitting!\n" );
    exit( -1 );
  }
  if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
    fprintf( stderr, 
	     "ref2ew: Invalid message type <TYPE_ERROR>; exitting!\n" );
    exit( -1 );
  }
  return;
} 

/******************************************************************************
 * send_heart_beat() builds a heartbeat (or error)
 *                     shared memory.  Writes errors to log file & screen.    *
 ******************************************************************************/
void send_heart_beat( unsigned char type, short ierr, char *note )
{
  MSG_LOGO    logo;
  char        msg[256];
  long        size;
  long        t;
 
  /* Build the message
  *******************/ 
  logo.instid = Par.MyInstId;
  logo.mod   = Par.MyModId;
  logo.type  = type;

  time( &t );

  if( type == TypeHeartBeat )
    {
      sprintf( msg, "%ld\n\0", t);
    }
  else if( type == TypeError )
    {
      sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
      logit( "et", "ref2ew: %s\n", note );
    }

  size = strlen( msg );   /* don't include the null byte in the message */     

  /* Write the message to shared memory
  ************************************/
  if( tport_putmsg( &Par.region, &logo, size, msg ) != PUT_OK )
    {
      if( type == TypeHeartBeat ) {
	logit("et","ref2ew:  Error sending heartbeat.\n" );
      }
      else if( type == TypeError ) {
	logit("et","ref2ew:  Error sending error:%d.\n", ierr );
      }
    }

  return;
}


/* init_io opens the input device, TTY or tcp port, and configures it for our
 * needs returns 0 on success, -1 on failure.
 */

int init_io()
{
  if ( Par.src_flg == TTY_SRC ) {
    struct termios modes, savemodes;
    int baud, val;

    /* open the TTY device;
     * set nonblocking flag so we don't wait for the modem */
    Par.in_fd = open( Par.ttyname, O_RDONLY );
    if ( Par.in_fd < 0 ) {
      logit( "et", "ref2ew: can't open `%s': ", Par.ttyname, strerror(errno) );
      return -1;
    }
    
    /* Get current settings so we can mess with them */
    if (tcgetattr(Par.in_fd, &modes) < 0) {
      logit("et", "tcgetattr failed: %s\n", strerror(errno));
      ref2ew_die(-1);
    }
    
    /* save a copy of them for later */
    savemodes = modes;
    Par.savemodes = &savemodes;
    
    modes.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    modes.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    modes.c_cflag &= ~(CSIZE | PARENB);
    modes.c_cflag |= CS8;
    modes.c_oflag &= ~(OPOST);
    modes.c_cc[VMIN] = 1;
    modes.c_cc[VTIME] = 0;
    
    switch(Par.speed)
      {
      case 110: baud = B110; break;
      case 150: baud = B150; break;
      case 300: baud = B300; break;
      case 1200: baud = B1200; break;
      case 2400: baud = B2400; break;
      case 4800: baud = B4800; break;
      case 9600: baud = B9600; break;
      case 19200: baud = B19200; break;
      case 38400: baud = B38400; break;
	
      default:
	logit("et","ref2uw: Bad line speed <%d> for %s\n", 
	      Par.speed, Par.ttyname);
	return -1;
      }
    cfsetospeed(&modes, baud);
    cfsetispeed(&modes, baud);
    
    if (tcsetattr(Par.in_fd, TCSAFLUSH, &modes) < 0) {
      logit("et", "tcsetattr failed: %s\n", strerror(errno));
      ref2ew_die(-1);
    }
  } else if (Par.src_flg == TCP_SRC) {   /* open TCP port */
    struct hostent  *hp;
    struct in_addr addr_s;
    char *addr_p;
    struct sockaddr_in sin;

    /* Assume address is a hostname. If we can look it up, we'll get an IP 
     * address. But if we can't than we'll assume it alreay is an IP address.
     */
    memset(&sin,0,sizeof(sin));
    if ( (hp = gethostbyname(Par.tcphost)) != NULL) {
      memcpy(&sin.sin_addr,hp->h_addr,hp->h_length);
    } else {
      memcpy(&sin.sin_addr, Par.tcphost, strlen(Par.tcphost));
    }
    sin.sin_family = AF_INET;
    sin.sin_port = Par.port;

    if( (Par.in_fd = socket(AF_INET,SOCK_STREAM,0)) == -1) {
      logit("e", "ref2ew: Socket call failed: %s\n", strerror(errno));
      return -1;
    }
    if (connect( Par.in_fd,(struct sockaddr *) &sin,(int) sizeof(sin)) == -1) {
      logit("e", "ref2ew: Connect call failed: %s\n", strerror(errno));
      return -1;
    }
  } else {
    logit("e", "ref2ew: unknown source flag %d\n", Par.src_flg);
    return -1;
  }

  /* set non-blocking IO */
  if (fcntl(Par.in_fd, F_SETFL, O_NDELAY) == -1) {
    logit("e", "ref2ew: fcntl error: %s\n", strerror(errno));
    return -1;
  }

  return 0;
}


/******************************************************************************
 * ref2ew_status() builds a heartbeat or error message & puts it into     *
 *                     shared memory.  Writes errors to log file & screen.    *
 ******************************************************************************/
void ref2ew_status( unsigned char type, short ierr, char *note )
{
   MSG_LOGO    logo;
   char        msg[256];
   long        size;
   long        t;
 
/* Build the message
 *******************/ 
   logo.instid = Par.MyInstId;
   logo.mod   = Par.MyModId;
   logo.type  = type;

   time( &t );

   if( type == TypeHeartBeat )
   {
        sprintf( msg, "%ld\n\0", t);
   }
   else if( type == TypeError )
   {
        sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
        logit( "et", "ref2ew: %s\n", note );
   }

   size = strlen( msg );   /* don't include the null byte in the message */     

/* Write the message to shared memory
 ************************************/
   if( tport_putmsg( &Par.region, &logo, size, msg ) != PUT_OK )
   {
        if( type == TypeHeartBeat ) {
           logit("et","ref2ew:  Error sending heartbeat.\n" );
        }
        else if( type == TypeError ) {
           logit("et","ref2ew:  Error sending error:%d.\n", ierr );
        }
   }

   return;
}

/* write raw reftek buffers to file
 * arguments:
 *      dt:     pointer to DT header structure, needed to create file name 
 *      buf:    pointer to raw reftek buffer
 */
void do_dtfile( struct reftek_dt *dt, char *buf )
{
  static int n;
  char   filename[100]; 
  char   *dtstr;
  int    i, len;

  if (n == 0 || Par.fd <= 0) {  /* need a new file */
    n = Par.nfilebufs;
    if(Par.fd > 0) {
      close(Par.fd);
      Par.fd = 0;
    }
    strcpy(filename, Par.dirpath);
    len = strlen(Par.dirpath);
    /* New file name 
     * format #6 from util_dttostr is yyyymmddhhmmss
     * output filename format is yymmddhhmm.sta
     */
    dtstr = util_dttostr(dt->tstamp, 6); 
    for (i=2; i<12; i++) filename[len++] = dtstr[i];
    filename[len++] = '.';
    strcpy(filename+len, Par.station);

    Par.fd = open(filename,O_CREAT|O_RDWR|O_APPEND,0666);
    if ( Par.fd < 0 ){
      logit("et", "ref2ew: can't open file %s \n",filename);
      logit("e", "\tGiving up creating files\n");
      Par.nfilebufs = 0;
    }
  }
  
  /*  Buffer read now write to file
  *****************************/
  write(Par.fd, buf, REFTEK_MAXPAKLEN);
  n--;  /* count down one for a buffer to file */
}


/* read a reftek packet into a buffer:
 * inputs:   fd: file descriptor to read from
 *         *buf: buffer to put the packet
 *           *n: bytes to read
 * output: *buf: where the start of the packet is
 *           *n: bytes actually read
 * returns:      reftek packet type
 */
int ref_get_packet(int fd, char **buf, size_t *n)
{
  int nread1, nread2, nall, nr;
  int retry = 0;
  int type, save_errno;
  int poll_int;
  long nfail;        /* counter for undecipherable packets */
  char *b_ptr;
  char *start, *last;
  char chr;
  struct pollfd fds;

  fds.fd = fd;
  fds.events = POLLIN;
  poll_int = Par.hbeatint * 1000 / 2; /* try not to miss a heartbeat */
  nfail = 0;

again:
  b_ptr = *buf;
  nall = *n;
  nread1 = 0;

  /* read in a buffer's worth */
  while (nall) {
    if (Par.debug) logit("t", "poll interval %d for fd %d\n", poll_int, fd);
    if (poll(&fds, 1, poll_int) == 0 ) { 
      *n = nread1;
      npto++;
      return R2E_TIMEOUT;
    }
    if ((fds.revents & POLLERR) || (fds.revents & POLLHUP)) return R2E_ERR;

    /* non-blocking IO is set in init_io() */
    nr = read( fd, b_ptr+nread1, nall);
    if ( nr < 0 ) {
      logit( "et" "read error: %s\n", strerror(errno));
      if (errno == EWOULDBLOCK) continue;
      *n = nread1;
      return R2E_ERR;
    }
    if (nr == 0) {
      logit("et", "nothing read\n");
      *n = nread1;
      return R2E_EOF;
    }
    nread1 += nr;
    nall -= nr;
  }  /* end of while(nall) */
  if (Par.debug) logit( "t", "read %d bytes\n", nread1 );
  
  /* See if there's a known packet marker in there */
  last = b_ptr + *n - 1;  /* last place the type string could start */
  start = b_ptr;

  if (Par.debug) 
    logit("t", "searching: %d - %d = %d\n", start, last, last - start);

  while (start < last) {
    if (memcmp(start, "AD", 2) == 0) {type = REFTEK_AD; break;}
    if (memcmp(start, "CD", 2) == 0) {type = REFTEK_CD; break;}
    if (memcmp(start, "DS", 2) == 0) {type = REFTEK_DS; break;}
    if (memcmp(start, "DT", 2) == 0) {type = REFTEK_DT; break;}
    if (memcmp(start, "EH", 2) == 0) {type = REFTEK_EH; break;}
    if (memcmp(start, "ET", 2) == 0) {type = REFTEK_ET; break;}
    if (memcmp(start, "FD", 2) == 0) {type = REFTEK_FD; break;}
    if (memcmp(start, "OM", 2) == 0) {type = REFTEK_OM; break;}
    if (memcmp(start, "SH", 2) == 0) {type = REFTEK_SH; break;}
    if (memcmp(start, "SC", 2) == 0) {type = REFTEK_SC; break;}
    start++;
  }
  if (start == last ) { /* didn't find it packet start */
    nfail++;
    if (nfail == 1 ) logit("et", "can't find packet type from %s\n",
			   Par.station);
    if (nfail == 100 ) {
      logit("et", "read 100 bad packets from %s\n", Par.station);
      return R2E_ERR;
    }
    goto again; 
  }

  /* if we reported timeouts before, tell'em we've recovered */
  if (npto) {
    sprintf(errmsg, "ref2ew resumed reading from %s", Par.station);
    ref2ew_status( TypeError, 1, errmsg );
    npto = 0;
  }
  /* Found the start, now read the rest of packet */
  nread1 = 1 + last - start;
  nall = *n - nread1;
  nread2 = 0;
  while (nall) {
    if (poll(&fds, 1, poll_int) == 0 ) {
      *n = nread1 + nread2;
      npto++;
      return R2E_TIMEOUT;
    }
    if ((fds.revents & POLLERR) || (fds.revents & POLLHUP)) return R2E_ERR;

    /* non-blocking IO is set in init_io() */    
    nr = read( fd, start + nread2, nall);
    if ( nr < 0 ) {
      logit( "et" "read error: %s\n", strerror(errno));
      if (errno == EWOULDBLOCK) continue;
      *n = nread1;
      return R2E_ERR;
    }
    if (nr == 0) {
      logit("et", "nothing read\n");
      *n = nread1;
      return R2E_EOF;
    }
    nread2 += nr;
    nall -= nr;
    
  }
  *buf = start;
  if (nread1 + nread2 != *n) 
    logit("et", "bytes requested: %d; bytes read: %d\n", *n, nread1 + nread2);
  *n = nread1 + nread2;
  return type;
}

void ref2ew_die(int status)
{
  int i;
  
  /* Close input */
  
  if (Par.in_fd > 0) {
    /* Try to restore the tty modes before dying */
    if (Par.savemodes)
      tcsetattr(Par.in_fd, TCSAFLUSH, Par.savemodes);
    close( Par.in_fd );
  }
  
  /* Detach from the transport ring */
  
  logit("t", "detaching from transport ring\n");
  tport_detach(&Par.region);
  
/* Neg status means violation occurred... abort() for core  */
  
  if (status < 0) {
    logit("et", "abort()\n");
    abort();
  }
  
  /* Otherwise, just log the exit */
  
  logit("t", "exit %d\n", status);
  
  exit(status);
}

/* od: log the contents of a buffer in format similar to `od' (octal dump) */
/* Used for debugging only */ 
void od(char *ptr, int n)
{
  int cntr, col, chr, fill;

    for ( cntr = 0, col = 0; cntr < n; ) {
      for ( col = 0; (col + cntr < n) && (col < 16); col++ ) {
	chr = ptr[cntr + col] & 0x7F;
	logit("", "%02x ",chr);
      }
      for ( fill = col; fill < 16; fill++ ) {
	logit("","   ");
      }
      logit(""," ");
      for ( col = 0; (col + cntr < n) && (col < 16); col++ ) {
	chr = ptr[cntr + col] & 0x7F;
	if ( (chr >= ' ' && chr <= '~') || (chr == 0x20) ) {
	  logit("","%c",chr);
	} else {
	  logit("",".");
	}
      }
      cntr += col;
      if ( col == 16 ) {
	logit("","\n");
	col = 0;
      }
    }   /* for ( cntr = 0, col = 0; cntr < sz; ) */
    if ( col ) logit("","\n");
    
}
