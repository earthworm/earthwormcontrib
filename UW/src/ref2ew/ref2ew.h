/*  ref2ew.h - fixed things for ref2ew */

#include <termios.h>        /* for termios tty control */
#include <transport.h>


#define MAX_CHAN   8  /* Reftech has up to 8 channels */
#define TTY_LEN 20    /* length of TtyName */
#define ADDR_LEN 80   /* length of host name or address */
#define STA_LEN 5     /* station name length for UW format; EW length is 7 */
#define CHA_LEN 4     /* channel name length for UW format; EW length is 9 */
#define NET_LEN 4     /* network name length for UW format; EW length is 9 */
#define DIR_LEN 80    /* directory name for saving files */
#define TTY_SRC 1
#define TCP_SRC 2

/* Some variants of RefTek formats; REFTEK is the standard */
#define REF_VAR_REFTEK 0
#define REF_VAR_TERRA 1

typedef struct {
  char name[CHA_LEN];
  int  pin;
} CHAN;

struct params {
  unsigned char MyInstId;
  unsigned char MyModId;
  SHM_INFO region;
  long ringkey;
  int logswitch;
  int hbeatint;
  int rev;
  int refvar;        /* Reftek variant, e.g. Terra Tech is variant 1 */
  int src_flg;
  char ttyname[TTY_LEN];
  int in_fd;
  struct termios *savemodes;
  int speed;
  char tcphost[ADDR_LEN];
  int port;
  char station[STA_LEN];
  char network[NET_LEN];
  CHAN channel[MAX_CHAN];
  double samprate;
  double timeshift;
  int nfilebufs;
  char dirpath[DIR_LEN];
  int fd;
  int debug;
};
