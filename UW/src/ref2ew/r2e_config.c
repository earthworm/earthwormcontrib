/*  Configuration routines for ref2ew */
/*  contains: 
 *       GetConfig( char *configfile )
 *       LogConfig()
 */

#include <stdio.h>
#include <string.h>
#include <kom.h>
#include <earthworm.h>
#include "ref2ew.h"

#define FNAME_LEN 80
#define ncommand 13              /* Number of commands in the config file */

extern struct params Par;
static char stafile[FNAME_LEN];  /* Station file to look up pin numbers */

/***************************************************************
      *                          GetConfig()                        *
      *         Processes command file using kom.c functions.       *
      *           Returns -1 if any errors are encountered.         *
      ***************************************************************/

int GetConfig( char *configfile )
{
  char     init[ncommand];     /* Flags, one for each command */
  int      nmiss;              /* Number of commands that were missed */
  int      nfiles;
  int      i;
  int      channo, chan_count = 0;

  /* make all the channel names empty */
  for (i = 0; i < MAX_CHAN; i++) {
    Par.channel[i].name[0] = (char) 0;
    Par.channel[i].pin = 9900 + i ; /* some unlikely pin number, not zero! */
  }
  
  /* Set defaults for optional settings */
  Par.nfilebufs = 0;
  Par.debug = 0;
  Par.timeshift = 0.0;
  Par.src_flg = 0;
  Par.refvar = REF_VAR_REFTEK;

  /* Set to zero one init flag for each required command
  ***************************************************/
  for ( i = 0; i < ncommand; i++ )
    init[i] = 0;

  /* Open the main configuration file
  ********************************/
  nfiles = k_open( configfile );
  if ( nfiles == 0 )
    {
      fprintf( stderr, "ref2ew: Error opening configuration file <%s>\n",
               configfile );
      return -1;
    }

  /* Process all nested configuration files
  **************************************/
  while ( nfiles > 0 )          /* While there are config files open */
    {
      while ( k_rd() )           /* Read next line from active file  */
	{
	  int  success;
	  char *com;
	  char *str;

	  com = k_str();          /* Get the first token from line */

	  if ( !com ) continue;             /* Ignore blank lines */
	  if ( com[0] == '#' ) continue;    /* Ignore comments */

	  /* Open nested configuration files
	  *******************************/
	  if ( com[0] == '@' )
	    {
	      success = nfiles + 1;
	      nfiles  = k_open( &com[1] );
	      if ( nfiles != success )
		{
		  fprintf( stderr, "ref2ew: Error opening command file <%s>.\n",
			   &com[1] );
		  return -1;
		}
	      continue;
	    }
	  
	  /* Read configuration parameters
	  *****************************/
	  /* 0: Read the transport ring name */
	  else if ( k_its( "RingName" ) )
	    {
	      if ( str = k_str() )
		{
		  if ( (Par.ringkey = GetKey(str)) == -1 )
		    {
		      fprintf( stderr, "ref2ew: Invalid RingName <%s>. Exitting.\n", str );
		      return -1;
		    }
		}
	      init[0] = 1;
	    }
	  
	  /* 1: Read ID of this module */
	  else if ( k_its( "MyModuleId" ) )
	    {
	      if ( str = k_str() )
		{
		  if ( GetModId(str, &Par.MyModId) == -1 )
		    {
		      fprintf( stderr, "ref2ew: Invalid MyModId <%s>. Exitting.\n", str );
		      return -1;
		    }
		}
	      init[1] = 1;
	    }
	  
	  /* 2: Read the log file switch */
	  else if( k_its( "LogFile" ) )
            {
	      Par.logswitch = k_int();
	      init[2] = 1;
            }
	  
          /* 3: Read heartbeat interval (seconds) */
	  else if ( k_its("HeartBeatInt") ) 
            {
	      Par.hbeatint = k_long();
	      init[3] = 1;
            }
	  
	  /* 4: Read TTY name from which to get data */
	  else if( k_its("TtyName") )
	    {
	      if( ( str=k_str() ) ) {
		if (Par.src_flg) {
		  fprintf( stderr, "ref2ew: more than one source found: %s\n",
			   str);
		  exit( -1 );
		}
		if( strlen(str) > (size_t)TTY_LEN-1 ) {
		  fprintf( stderr, 
			   "ref2ew: tty name: <%s> too long in config file", 
			   str ); 
		  fprintf( stderr, " cmd; max=%d; exitting!\n", TTY_LEN-1 );
		  exit( -1 );
		}
		strcpy( Par.ttyname, str );
	      }
	      init[4] = 1;
	      Par.src_flg = TTY_SRC;
	      init[6] = 1;  /* port not needed with tty */
	    }
	  
	  /* 4: Read TCP address from which to get data */
	  else if( k_its("TcpHost") )
	    {
	      if( ( str=k_str() ) ) {
		if (Par.src_flg) {
		  fprintf( stderr, "ref2ew: more than one source found: %s\n",
			   str);
		  exit( -1 );
		}
		if( strlen(str) > (size_t)ADDR_LEN-1 ) {
		  fprintf( stderr, 
			   "ref2ew: TCP address: <%s> too long in config file", 
			   str ); 
		  fprintf( stderr, " cmd; max=%d; exitting!\n", ADDR_LEN-1 );
		  exit( -1 );
		}
		strcpy( Par.tcphost, str );
	      }
	      init[4] = 1;
	      Par.src_flg = TCP_SRC;
	      init[5] = 1;  /* baud rate not needed with TCP */
	    }
	  
          /* 5: Read TTY baud rate */
	  else if( k_its("Speed") ) 
            {
	      Par.speed = k_int();
	      init[5] = 1;
            }
	  
          /* 6: Read TCP port */
	  else if( k_its("Port") ) 
            {
	      Par.port = k_int();
	      init[6] = 1;
            }
	  
	  /* 7: Read station name */
	  else if( k_its("Station") )
	    {
	      if( ( str=k_str() ) ) {
		if( strlen(str) > (size_t)STA_LEN-1 ) {
		  fprintf( stderr, 
			   "ref2ew: station name: <%s> too long in config file", 
			   str ); 
		  fprintf( stderr, " cmd; max=%d; exitting!\n", STA_LEN-1 );
		  exit( -1 );
		}
		strcpy( Par.station, str );
	      }
	      init[7] = 1;
	    }
	  
	  /* 8: Read network name */
	  else if( k_its("Network") )
	    {
	      if( ( str=k_str() ) ) {
		if( strlen(str) > (size_t)NET_LEN-1 ) {
		  fprintf( stderr, 
			   "ref2ew: network name: <%s> too long in <Network>",
			   str ); 
		  fprintf( stderr, " cmd; max=%d; exitting!\n", NET_LEN-1 );
		  exit( -1 );
		}
		strcpy( Par.network, str );
	      }
	      init[8] = 1;
	    }
	  
	  /* 9: Read the channels that we want */
	  else if( k_its("Channel") )
	    {
	      channo = k_int();
	      if (channo < 0 || channo >= MAX_CHAN) {
		fprintf( stderr,
			 "ref2ew: channel number <%d> no good in <Channel>", channo);
		fprintf( stderr, " cmd; exitting!\n");
		exit( -1 );
	      }
	      if( ( str=k_str() ) ) {
		if(  strlen(str) > (size_t)CHA_LEN-1) {
		  fprintf( stderr, 
			   "ref2ew: channel name: <%s> too long in <Channel>",
			   str ); 
		  fprintf( stderr, " cmd; max=%d; exitting!\n", CHA_LEN-1 );
		  exit( -1 );
		}
		strcpy(Par.channel[channo].name, str);
		chan_count++;
		}
	      init[9] = 1;
	    }
	  
	  /* 10: Read sample rate */
	  else if( k_its("SampleRate") )
	    {
	      Par.samprate = k_val();
	      init[10] = 1;
	    }
	  
	  /* 11: Read station file */
	  else if ( k_its("StationFile") )
	    {
	      if ( str = k_str() )
		if( strlen(str) > (size_t)FNAME_LEN-1 ) {
		  fprintf( stderr, 
			   "ref2ew: name <%s> too long in config file <StationFile>", str ); 
		  fprintf( stderr, " cmd; max=%d; exitting!\n", FNAME_LEN-1 );
		  exit( -1 );
		}
	      strcpy( stafile, str );
	      init[11] = 1;
	    }    
	  /* optional: Read time shift: seconds to add to packet time */
	  else if ( k_its("TimeShift") )
	    {
	      Par.timeshift = k_val();
	    }
	  
	  /* optional: Read how many packets to save in one file */
	  else if ( k_its("nFileBufs") )
	    {
	      Par.nfilebufs = k_int();
	    }
	  
	  /* Read directory name for storing files */
	  else if ( k_its("DirPath") )
	    {
	      if ( str = k_str() )
		if( strlen(str) > (size_t)DIR_LEN-1 ) {
		  fprintf( stderr, 
			   "ref2ew: name <%s> too long in config file <DirPath>", str ); 
		  fprintf( stderr, " cmd; max=%d; exitting!\n", DIR_LEN-1 );
		  exit( -1 );
		}
	      strcpy( Par.dirpath, str );
	      if (Par.dirpath[strlen(Par.dirpath) - 1] != '/' )
		strcat(Par.dirpath, "/");
	      init[12] = 1;
	    }

	  /* optional: debug flag */
	  else if ( k_its("Debug") )
	    {
	      Par.debug = 1;
	    }
	  
	  /* optional: RefTek variant */
	  else if ( k_its("RefVar") )
	    {
	      if (str = k_str() ) {
		if (strcmp(str, "Terra") == 0) {
		  Par.refvar = REF_VAR_TERRA;
		} else if (strcmp(str, "Reftek") == 0) {
		  Par.refvar = REF_VAR_REFTEK;
		} else {
		  fprintf( stderr, "Unknown RefVar <%s>; exitting!\n", str);
		  exit( -1 );
		}
              }
	    }

	  /* An unknown parameter was encountered
	  ************************************/
	  else
	    {
	      fprintf( stderr, "ref2ew: <%s> unknown parameter in <%s>\n",
		       com, configfile );
	      continue;
	    }
	  
	  /* See if there were any errors processing the command
	  ***************************************************/
	  if ( k_err() )
	    {
	      fprintf( stderr, "ref2ew: Bad <%s> command in <%s>.\n", com,
		       configfile );
	      return -1;
	    }
	}
      nfiles = k_close();
    }

  /* if nFileBufs is missing (it's optional) we don't save and
   * thus don't need DirPath.
   */
  if (Par.nfilebufs == 0) init[12] = 1;

  /* After all files are closed, check flags for missed commands
  ***********************************************************/
  nmiss = 0;
  for ( i = 0; i < ncommand; i++ )
    if ( !init[i] )
      nmiss++;

  if ( nmiss > 0 )
    {
      fprintf( stderr, "ref2ew: ERROR, no " );
      if ( !init[0] ) fprintf( stderr, "<RingName> " );
      if ( !init[1] ) fprintf( stderr, "<MyModId> " );
      if ( !init[2] ) fprintf( stderr, "<LogFile> " );
      if ( !init[3] ) fprintf( stderr, "<HeartBeatInt> " );
      if ( !init[4] ) fprintf( stderr, "<TtyName or TcpHost> " );
      if ( !init[5] ) fprintf( stderr, "<Speed> " );
      if ( !init[6] ) fprintf( stderr, "<Port> " );
      if ( !init[7] ) fprintf( stderr, "<Station> " );
      if ( !init[8] ) fprintf( stderr, "<Network> " );
      if ( !init[9] ) fprintf( stderr, "<Channel> " );
      if ( !init[10] ) fprintf( stderr, "<SampleRate> " );
      if ( !init[11]) fprintf( stderr, "<StationFile> " );
      if ( !init[12]) fprintf( stderr, "<DirPath> " );
      fprintf( stderr, "command(s) in <%s>. Exitting.\n", configfile );
      return -1;
    }
  return 0;
}


/***********************************************************************
  *                              LogConfig()                            *
  *                                                                     *
  *                   Log the configuration parameters                  *
  ***********************************************************************/

void LogConfig( void )
{
  logit( "", "\n" );
  logit( "", "RingKey:         %6d\n",   Par.ringkey );
  logit( "", "MyModId:         %6d\n",   Par.MyModId );
  logit( "", "LogFile:         %6d\n",   Par.logswitch );
  logit( "", "HeartBeatInt:    %6d\n",   Par.hbeatint );
  if (Par.src_flg == TTY_SRC) {
    logit( "", "TtyName:         %s\n",    Par.ttyname );
    logit( "", "Speed:           %6d\n",   Par.speed );
  } else if (Par.src_flg == TCP_SRC) {
    logit( "", "Address:         %s\n",    Par.tcphost );
    logit( "", "Port:            %6d\n",   Par.port);
  }
  logit( "", "Station:         %s\n",    Par.station );
  logit( "", "Network:         %s\n",    Par.network );
  logit( "", "SampleRate:      %f\n",    Par.samprate );
  logit( "", "StationFile:     %s\n",    stafile );
  logit( "", "TimeShift:       %f\n",    Par.timeshift );
  logit( "", "nFileBufs:       %6d\n",   Par.nfilebufs );
  if (Par.nfilebufs)
    logit( "", "DirPath:         %s\n",    Par.dirpath );
    
}


int GetPins( void )
{
  FILE *fp;
  char string[130];
  char sname[STA_LEN], cname[CHA_LEN], nname[NET_LEN];
  int pickit, pin, i;

  /* Open the station list file
  *****************************/
   if ( ( fp = fopen( stafile, "r") ) == NULL )
   {
      logit( "et", "ref2ew:: Error opening station list file <%s>.\n",
             stafile );
      return -1;
   }

  /* Read the station file, looking for my station */

  while ( fgets( string, 130, fp ) != NULL )
    {
      if ( strncmp( string, "//", 2 ) == 0 ) continue;
      if ( sscanf( string, "%d%d%s%s%s", &pickit, &pin, 
		   sname, nname, cname ) < 5 ) continue;

      if ( !strcmp( sname, Par.station ) && !strcmp( nname, Par.network ) ) {
	for ( i = 0; i < MAX_CHAN; i++ ) {
	  if ( !strcmp( Par.channel[i].name, cname ) )
	    Par.channel[i].pin = pin;
	}
      }
    }
  fclose(fp);
  return 0;
}
