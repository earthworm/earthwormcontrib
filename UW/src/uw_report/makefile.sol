# B = ../../bin
L = ../../v2.9/lib

CFLAGS = ${GLOBALFLAGS}

REPORT = uw_report.o uw_pick.o $L/logit.o $L/time_ew.o $L/kom.o  \
        $L/getutil.o $L/sleep_ew.o $L/transport.o $L/copyfile.o $L/dirops_ew.o

uw_report: $(REPORT)
	cc -o uw_report $(REPORT)  -lm -lposix4

arcfile2ring: arcfile2ring.o $L/getutil.o $L/transport.o $L/sleep_ew.o
	cc -o arcfile2ring arcfile2ring.o $L/getutil.o $L/transport.o \
	$L/sleep_ew.o $L/kom.o -lposix4	

arc2uwpk: arc2uwpk.o uw_pick.o
	cc -o arc2uwpk arc2uwpk.o uw_pick.o  -lm -lposix4

uw_pick.o: uw_pick.c uw_pick.h
	cc -c uw_pick.c


clean:
	-rm *.o uw_report arcfile2ring arc2uwpk
