/*
 * uw_report.c : Grabs final event messages and writes to files.
 * Can write your choice of hypoinverse archive file, h71 summary file and/or
 * UW2-format pick file.
 * Runs the specified script after a pickfile is generated.
 */


#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>

/* Function prototypes
 *******************************/
void  uw_report_hinvarc ( char * );
void  uw_report_h71sum  ( char * );
void  uw_report_uwpick  ( char * );
void  uw_report_dosum   ( char * );
void  uw_report_dopick  ( char * );
int   uw_report_write   ( FILE * );
void  uw_report_config  ( char * );
void  uw_report_lookup  ( void );
void  uw_report_status  ( unsigned char, short, char * );

static  SHM_INFO  Region;      /* shared memory region to use for i/o    */

#define   MAXLOGO   2
MSG_LOGO  GetLogo[MAXLOGO];    /* array for requesting module,type,instid */
short 	  nLogo;

#define BUF_SIZE 50000	       /* define maximum size for event msg  */
        
/* Things to read from configuration file
 ****************************************/
static char    RingName[20];        /* name of transport ring for i/o     */
static char    MyModName[20];       /* speak as this module name/id       */
static int     LogSwitch;           /* 0 if no logfile should be written  */
static long    HeartBeatInterval;   /* seconds between heartbeats         */
static char    ArcDir[50];          /* directory to write arc files       */
static char    ArcSuffix[6];        /* suffix to use for archive files    */
static char    SumDir[50];          /* directory to write sum files       */
static char    SumSuffix[6];        /* suffix to use for summary files    */
static char    UWDir[50];           /* directory to write UW pick files   */
       char    UWSuffix[6];         /* suffix to use for UW pick files    */
static char    UWCommand[100];      /* command to run with UW pick files  */
static int     SendArc   = 1;       /* =0 if ArcSuffix="none" or "NONE"   */
static int     SendSum   = 1;       /* =0 if SumSuffix="none" or "NONE"   */
static int     SendUW    = 1;       /* =0 if UWSuffix="none" or "NONE"    */
static int     DoCommand = 1;       /* =0 if UWCommand="none" or "NONE"   */
int     WriteNet = 0;               /* =1 to put network code in pick file */

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
static long          RingKey;       /* key of transport ring for i/o      */
static unsigned char InstId;        /* local installation id              */
static unsigned char MyModId;       /* Module Id for this program         */
static unsigned char TypeHeartBeat; 
static unsigned char TypeError;
static unsigned char TypeHinvArc;
static unsigned char TypeH71Sum;

/* Error messages used by uw_report 
 *************************************/
#define  ERR_MISSMSG       0   /* message missed in transport ring       */
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer     */
#define  ERR_NOTRACK       2   /* msg retreived; tracking limit exceeded */
#define  ERR_ARCFILE       3   /* error creating/writing arc file        */
#define  ERR_SUMFILE       4   /* error creating/writing sum file        */
#define  ERR_UWFILE        5   /* error creating/writing UW pick file    */
static char  Text[150];        /* string for log/error messages          */

main( int argc, char **argv )
{
  static char  eqmsg[BUF_SIZE];  /* array to hold event message     */
  long         timeNow;          /* current time                    */       
  long         timeLastBeat;     /* time last heartbeat was sent    */
  long         recsize;	  /* size of retrieved message       */
  MSG_LOGO     reclogo;	  /* logo of retrieved message       */
  int          result;

  /* Check command line arguments 
  ******************************/
  if ( argc != 2 )
    {
      fprintf( stderr, "Usage: uw_report <configfile>\n" );
      exit( 0 );
    }
	   
  /* Read the configuration file(s)
  ********************************/
  uw_report_config( argv[1] );

  /* Look up important info from earthworm.h tables
  ************************************************/
  uw_report_lookup();
 
  /* Initialize name of log-file & open it 
  ***************************************/
  logit_init( "uw_report", (short) MyModId, 256, LogSwitch );
  logit( "" , "uw_report: Read command file <%s>\n", argv[1] );

  /* Attach to Input/Output shared memory ring 
  *******************************************/
  tport_attach( &Region, RingKey );
  logit( "", "uw_report: Attached to public memory region %s: %d\n", 
	 RingName, RingKey );

  /* Force a heartbeat to be issued in first pass thru main loop
  *************************************************************/
  timeLastBeat = time(&timeNow) - HeartBeatInterval - 1;

  /*--------------------- setup done; start main loop -----------------------*/

  /*  logit( "", "uw_report: started loop\n" );*/ /*debug*/
  while( tport_getflag(&Region) != TERMINATE )
    {
      /* send uw_report's heartbeat
      *******************************/
      if  ( time(&timeNow) - timeLastBeat  >=  HeartBeatInterval ) 
        {
	  timeLastBeat = timeNow;
	  uw_report_status( TypeHeartBeat, 0, "" ); 
	  /* logit( "t", "uw_report: heartbeat\n" );*/ /*debug*/
	}

      /* Process all new hypoinverse archive msgs and hypo71 summary msgs   
      ******************************************************************/
      do
        {
	  /* Get the next message from shared memory
	  *****************************************/
	  result = tport_getmsg( &Region, GetLogo, nLogo,
				 &reclogo, &recsize, eqmsg, sizeof(eqmsg)-1 );

	  /* Check return code; report errors if necessary
	  ***********************************************/

	  if( result != GET_OK )
	    {
              if( result == GET_NONE ) 
		{
		  break;
		}
              else if( result == GET_TOOBIG ) 
		{
		  sprintf( Text, 
			   "Retrieved msg[%ld] (i%u m%u t%u) too big for eqmsg[%d]",
			   recsize, reclogo.instid, reclogo.mod, reclogo.type, 
			   sizeof(eqmsg)-1 );
		  uw_report_status( TypeError, ERR_TOOBIG, Text );
		  continue;
		}
              else if( result == GET_MISS ) 
		{
		  sprintf( Text,
			   "Missed msg(s)  i%u m%u t%u  %s.",
			   reclogo.instid, reclogo.mod, reclogo.type, RingName );
		  uw_report_status( TypeError, ERR_MISSMSG, Text );
		}
              else if( result == GET_NOTRACK ) 
		{
		  sprintf( Text,
			   "Msg received (i%u m%u t%u); transport.h NTRACK_GET exceeded",
			   reclogo.instid, reclogo.mod, reclogo.type );
		  uw_report_status( TypeError, ERR_NOTRACK, Text );
		}
	    }

	  /* Process new message (result==GET_OK,GET_MISS,GET_NOTRACK)
	  ********************************************************/
	  eqmsg[recsize] = '\0';   /*null terminate the message*/
	  
	  if( reclogo.type == TypeHinvArc ) 
	    {
              if( SendArc ) uw_report_hinvarc( eqmsg );
	      if( SendUW )  uw_report_uwpick( eqmsg );
	    }
	  else if( reclogo.type == TypeH71Sum ) 
	    {
	      if( SendSum ) uw_report_h71sum( eqmsg );
	    }

	} while( result != GET_NONE );  /*end of message-processing-loop */
        
      sleep_ew( 1000 );  /* no more messages; wait for new ones to arrive */
    }  
  /*---------------------------end of main loop------------------------------*/

  /* Termination has been requested 
  ********************************/
  /* detach from shared memory */
  tport_detach( &Region ); 
  /* write a termination msg to log file */
  logit( "t", "uw_report: Termination requested; exitting!\n" );
  exit( 0 );
}

/******************************************************************************
 *  uw_report_uwpick( )  process Hypoinverse archive message into UW2 format *
 *****************************************************************************/
void uw_report_uwpick( char *arcmsg )
{
  FILE *fp;
  char *msg;         /* pointer to current location in the arc message */
  char  card[200];
  char  c4[5];
  char  fname[25];
  char  pathname[75];
  char  commandline[200];
  long  length;
  int   i, result;

  msg = arcmsg;

  /* Create name for local file using the event origin 
   time and SUFFIX read from the configuration file
   command UWSuffix.
   Example:  96012113245y
             yymmddhhmmsSUFFIX
 *********************************************/
  strncpy( fname,    msg,     11 );
  strcpy ( fname+11, UWSuffix      );
  for ( i=0; i<(int) strlen(fname); i++ )  
    {
      if( fname[i] == ' ' )  fname[i] = '0';
    }   

  /* Read in the first (summary) line of the message 
     Don't use spot 0 in arrays; it is easier to read &
     matches column numbers in documentation.
     *************************************************/
  sscanf( msg, "%[^\n]", card+1 );
  msg += (strlen(card+1) + 1);  /* move past the line and the newline */

   /* Process the summary card */
  uw_report_dosum( card );

  /* loop to read the remaining lines from the message */
  while ( sscanf( msg, "%[^\n]", card+1 ) ) {
    msg += (strlen(card+1) + 1);
    
    if ( card[1] == '$' )    /* shadow card, skip it */
      continue;
    
    strncpy( c4, card+1, 4 );
    c4[4] = '\0';
    if ( !strcmp( c4, "    " ) )  /* terminator card, all done */
      break;
    
    /* Process the pick line */
    uw_report_dopick( card );
    
  }  /* end of message loop */
  
  
  /* Write file */
  strcpy( pathname, UWDir );
  strcat( pathname, fname );
  if( (fp = fopen( pathname, "w" )) == (FILE *) NULL )
    {
      sprintf( Text, "uw_report: error opening file <%s>\n",
	       pathname );
      uw_report_status( TypeError, ERR_UWFILE, Text );
      return;
    }
  
  if ( uw_report_write( fp ) != 0 )
    {
      sprintf( Text, "uw_report: error writing to file <%s>\n",
	       pathname );
      uw_report_status( TypeError, ERR_UWFILE, Text );
      fclose( fp );
      return;
    } 
  fclose( fp );
  
  /* Run the selected command on the pick file */
  if (DoCommand) {
    sprintf(commandline, "%s %s &", UWCommand, pathname);
    logit("t", "uw_report: run command:\n");
    logit("", "\t %s\n", commandline);
    system(commandline);
  }

  return;
}

/******************************************************************************
 *  uw_report_hinvarc( )  process a Hypoinverse archive message            *
 ******************************************************************************/
void uw_report_hinvarc( char *arcmsg )
{
   FILE *fparc;
   char  fname[25];
   char  pathname[75];
   long  length;
   int   i, res;

/* Create name for local file using the event 
   origin time, last 2 digits of the event id,
   and SUFFIX read from the configuration file
   command ArcSuffix.
   Example:  9601211324_98.arc
             yymmddhhmm_xx.SUFFIX
 *********************************************/
   strncpy( fname,    arcmsg,     10 );
   fname[10] = '_';
   strncpy( fname+11, arcmsg+136,  2 );
   fname[13] = '.';
   strcpy ( fname+14, ArcSuffix      );
   for ( i=0; i<(int) strlen(fname); i++ )  
   {
       if( fname[i] == ' ' )  fname[i] = '0';
   }   

/* Write file
 ******************/
   strcpy( pathname, ArcDir );
   strcat( pathname, fname );
   if( (fparc = fopen( pathname, "w" )) == (FILE *) NULL )
   {
       sprintf( Text, "uw_report: error opening file <%s>\n",
                pathname );
       uw_report_status( TypeError, ERR_ARCFILE, Text );
       return;
   }
   length = strlen( arcmsg );
   if ( fwrite( arcmsg, (size_t)1, (size_t)length, fparc ) == 0 ) 
   {
       sprintf( Text, "uw_report: error writing to file <%s>\n",
                fname );
       uw_report_status( TypeError, ERR_ARCFILE, Text );
       fclose( fparc );
       return;
   } 
   fclose( fparc );

   return;
}

/*****************************************************************************
 *  uw_report_h71sum( )  process a Hypo71 summary message                 *
 *****************************************************************************/
void uw_report_h71sum( char *summsg )
{
   FILE *fpsum;
   char  fname[25];
   char  pathname[75];
   long  length;
   int   i, res;

/* Create name for local file using the event 
   origin time, last 2 digits of the event id,
   and SUFFIX read from the configuration file
   command SumSuffix.
   Example:  9601211324_98.sum
             yymmddhhmm_xx.SUFFIX
 *********************************************/
   strncpy( fname,    summsg,    6 );
   strncpy( fname+6,  summsg+7,  4 );
   fname[10] = '_';
   strncpy( fname+11, summsg+89, 2 ); 
   fname[13] = '.';
   strcpy ( fname+14, SumSuffix    ); 
   for ( i=0; i<(int) strlen(fname); i++ )  
   {
       if( fname[i] == ' ' )  fname[i] = '0';
   }
   printf("Summary file: %s\n", fname ); /*DEBUG*/   

/* Write local file
 ******************/
   strcpy( pathname, SumDir );
   strcat( pathname, fname );
   if( (fpsum = fopen( pathname, "w" )) == (FILE *) NULL )
   {
       sprintf( Text, "uw_report: error opening file <%s>\n",
                pathname );
       uw_report_status( TypeError, ERR_SUMFILE, Text );
       return;
   }
   length = strlen( summsg );
   if ( fwrite( summsg, (size_t)1, (size_t)length, fpsum ) == 0 ) 
   {
       sprintf( Text, "uw_report: error writing to file <%s>\n",
                pathname );
       uw_report_status( TypeError, ERR_SUMFILE, Text );
       fclose( fpsum );
       return;
   } 
   fclose( fpsum );

   return;
}

/******************************************************************************
 *  uw_report_config() processes command file(s) using kom.c functions;       *
 *                       exits if any errors are encountered.	              *
 ******************************************************************************/
void uw_report_config( char *configfile )
{
  int      ncommand;     /* # of required commands you expect to process   */ 
  char     init[12];     /* init flags, one byte for each required command */
  int      nmiss;        /* number of required commands that were missed   */
  char    *com;
  char    *str;
  int      nfiles;
  int      success;
  int      i;

  /* Set to zero one init flag for each required command 
  *****************************************************/   
  ncommand = 10;
  for( i=0; i<ncommand; i++ )  init[i] = 0;
  nLogo = 0;

  /* Open the main configuration file 
  **********************************/
  nfiles = k_open( configfile ); 
  if ( nfiles == 0 ) {
    fprintf( stderr,
	     "uw_report: Error opening command file <%s>; exitting!\n", 
	     configfile );
    exit( -1 );
  }

  /* Process all command files
  ***************************/
  while(nfiles > 0)   /* While there are command files open */
    {
      while(k_rd())        /* Read next line from active file  */
        {  
	  com = k_str();         /* Get the first token from line */

	  /* Ignore blank lines & comments
	  *******************************/
	  if( !com )           continue;
	  if( com[0] == '#' )  continue;

	  /* Open a nested configuration file 
	  **********************************/
	  if( com[0] == '@' ) {
	    success = nfiles+1;
	    nfiles  = k_open(&com[1]);
	    if ( nfiles != success ) {
	      fprintf( stderr, 
		       "uw_report: Error opening command file <%s>; exitting!\n",
		       &com[1] );
	      exit( -1 );
	    }
	    continue;
	  }

	  /* Process anything else as a command 
	  ************************************/
	  /*0*/     if( k_its("LogFile") ) {
	    LogSwitch = k_int();
	    init[0] = 1;
	  }
	  /*1*/     else if( k_its("MyModuleId") ) {
	    str = k_str();
	    if(str) strcpy( MyModName, str );
	    init[1] = 1;
	  }
	  /*2*/     else if( k_its("RingName") ) {
	    str = k_str();
	    if(str) strcpy( RingName, str );
	    init[2] = 1;
	  }
	  /*3*/     else if( k_its("HeartBeatInterval") ) {
	    HeartBeatInterval = k_long();
	    init[3] = 1;
	  }


	  /* Enter installation & module to get event messages from
          ********************************************************/
	  /*4*/     else if( k_its("GetEventsFrom") ) {
	    if ( nLogo+1 >= MAXLOGO ) {
	      fprintf( stderr, 
		       "uw_report: Too many <GetEventsFrom> commands in <%s>", 
		       configfile );
	      fprintf( stderr, "; max=%d; exitting!\n", (int) MAXLOGO/2 );
	      exit( -1 );
	    }
	    if( ( str=k_str() ) ) {
	      if( GetInst( str, &GetLogo[nLogo].instid ) != 0 ) {
		fprintf( stderr, 
			 "uw_report: Invalid installation name <%s>", str ); 
		fprintf( stderr, " in <GetEventsFrom> cmd; exitting!\n" );
		exit( -1 );
	      }
	    }
	    if( ( str=k_str() ) ) {
	      if( GetModId( str, &GetLogo[nLogo].mod ) != 0 ) {
		fprintf( stderr, 
			 "uw_report: Invalid module name <%s>", str ); 
		fprintf( stderr, " in <GetEventsFrom> cmd; exitting!\n" );
		exit( -1 );
	      }
	      GetLogo[nLogo+1].mod = GetLogo[nLogo].mod;
	    }
	    if( GetType( "TYPE_HINVARC", &GetLogo[nLogo].type ) != 0 ) {
	      fprintf( stderr, 
		       "uw_report: Invalid message type <TYPE_HINVARC>" ); 
	      fprintf( stderr, "; exitting!\n" );
	      exit( -1 );
	    }
	    nLogo  += 1;
	    init[4] = 1;
            /*    printf("GetLogo[%d] inst:%d module:%d type:%d\n",
		  nLogo, (int) GetLogo[nLogo].instid,
		  (int) GetLogo[nLogo].mod,
		  (int) GetLogo[nLogo].type ); */  /*DEBUG*/
	  }

	  /* Suffix to use for output archive files 
          ****************************************/
	  /*5*/    else if( k_its("ArcSuffix") ) {
	    str = k_str();
	    if(str) {
	      if( strlen(str) >= sizeof(ArcSuffix) ) {
		fprintf( stderr,
			 "uw_report: ArcSuffix <%s> too long; ", str );
		fprintf( stderr, "max length:%d; exitting!\n",
			 (int)(sizeof(ArcSuffix)-1) );
		exit(-1);
	      }
	      /* ArcSuffix shouldn't include the period */
	      if( str[0] == '.' ) strcpy( ArcSuffix, &str[1] );
	      else                strcpy( ArcSuffix, str     );
	      /* Set SendArc flag based on ArcSuffix value */
	      if     (strncmp(ArcSuffix, "none", 4) == 0) SendArc=0;
	      else if(strncmp(ArcSuffix, "NONE", 4) == 0) SendArc=0;
	      else if(strncmp(ArcSuffix, "None", 4) == 0) SendArc=0;
	      else                                        SendArc=1;

	      init[5] = 1;
	      if ( SendArc == 0 ) init[6] = 1; /* don't need directory */
            }
	  }

	  /* Enter name of local directory for arc files
          *****************************************************/
	  /*6*/     else if( k_its("ArcDir") ) {
	    str = k_str();
	    if (str) strcpy( ArcDir, str );
	    /* make sure ArcDir ends in "/" */
	    if ( ArcDir[strlen(ArcDir)-1] != '/' )
	      strcat( ArcDir, "/" );
	    init[6] = 1;
	  }

	  /* Suffix to use for output sum files 
          ****************************************/
	  /*7*/    else if( k_its("SumSuffix") ) {
	    str = k_str();
	    if(str) {
	      if( strlen(str) >= sizeof(SumSuffix) ) {
		fprintf( stderr,
			 "uw_report: SumSuffix <%s> too long; ", str );
		fprintf( stderr, "max length:%d; exitting!\n",
			 (int)(sizeof(SumSuffix)-1) );
		exit(-1);
	      }
	      /* SumSuffix shouldn't include the period */
	      if( str[0] == '.' ) strcpy( SumSuffix, &str[1] );
	      else                strcpy( SumSuffix, str     );
	      /* Set SendSum flag based on SumSuffix value */
	      if     (strncmp(SumSuffix, "none", 4) == 0) SendSum=0;
	      else if(strncmp(SumSuffix, "NONE", 4) == 0) SendSum=0;
	      else if(strncmp(SumSuffix, "None", 4) == 0) SendSum=0;
	      else                                        SendSum=1;

	      init[7] = 1;
	      if ( SendSum == 0 ) init[8] = 1;
            }
	  }
	  
	  /* Enter name of local directory for sum files
          *****************************************************/
	  /*8*/     else if( k_its("SumDir") ) {
	    str = k_str();
	    if(str) strcpy( SumDir, str );
	    /* make sure SumDir ends in "/" */
	    if ( SumDir[strlen(SumDir)-1] != '/' )
	      strcat( SumDir, "/" );
	    init[8] = 1;
	  }

	  /* Suffix to use for UW pick files
          ****************************************/
	  /*9*/    else if( k_its("UWSuffix") ) {
	    str = k_str();
	    if(str) {
	      if( strlen(str) >= sizeof(UWSuffix) ) {
		fprintf( stderr,
			 "uw_report: UWSuffix <%s> too long; ", str );
		fprintf( stderr, "max length:%d; exitting!\n",
			 (int)(sizeof(UWSuffix)-1) );
		exit(-1);
	      }
	      /* UWSuffix shouldn't include the period */
	      if( str[0] == '.' ) strcpy( UWSuffix, &str[1] );
	      else                strcpy( UWSuffix, str     );
	      /* Set SendUW flag based on UWSuffix value */
	      if     (strncmp(UWSuffix, "none", 4) == 0) SendUW=0;
	      else if(strncmp(UWSuffix, "NONE", 4) == 0) SendUW=0;
	      else if(strncmp(UWSuffix, "None", 4) == 0) SendUW=0;
	      else                                       SendUW=1;

	      init[9] = 1;
	      if ( SendUW == 0 ) {
		init[10] = 1;
		init[11] = 1;
	      }
	    }
	  }

	  /* Enter name of local directory for UW pick files
          *****************************************************/
	  /*10*/     else if( k_its("UWDir") ) {
	    str = k_str();
	    if(str) strcpy( UWDir, str );
	    /* make sure UWDir ends in "/" */
	    if ( UWDir[strlen(UWDir)-1] != '/' )
	      strcat( UWDir, "/" );
	    init[10] = 1;
	  }

	  /* Enter command to run on UW Pick files
          *****************************************************/
	  /*11*/     else if( k_its("UWCommand") ) {
	    str = k_str();
	    if (str) strcpy( UWCommand, str );
	    /* Set SendSum flag based on SumSuffix value */
	    if     (strncmp(UWCommand, "none", 4) == 0) DoCommand=0;
	    else if(strncmp(UWCommand, "NONE", 4) == 0) DoCommand=0;
	    else if(strncmp(UWCommand, "None", 4) == 0) DoCommand=0;
	    else                                        DoCommand=1;
	    init[11] = 1;
	  }

	  /* optional */
	  else if ( k_its("WriteNet") ) {
	    WriteNet = 1;
	  }
	  
	  /* Unknown command
	  *****************/ 
	  else {
	    fprintf( stderr, "uw_report: <%s> Unknown command in <%s>.\n", 
		     com, configfile );
	    continue;
	  }
	  
	  /* See if there were any errors processing the command 
	  *****************************************************/
	  if( k_err() ) {
	    fprintf( stderr, 
		     "uw_report: Bad <%s> command in <%s>; exitting!\n",
		     com, configfile );
	    exit( -1 );
	  }
	}
      nfiles = k_close();
    }
  
  /* After all files are closed, check init flags for missed commands
  ******************************************************************/
  nmiss = 0;
  for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
  if ( nmiss ) {
    fprintf( stderr, "uw_report: ERROR, no " );
    if ( !init[0] )  fprintf( stderr, "<LogFile> "           );
    if ( !init[1] )  fprintf( stderr, "<MyModuleId> "        );
    if ( !init[2] )  fprintf( stderr, "<RingName> "          );
    if ( !init[3] )  fprintf( stderr, "<HeartBeatInterval> " );
    if ( !init[4] )  fprintf( stderr, "<GetEventsFrom> "     );
    if ( !init[5] )  fprintf( stderr, "<ArcSuffix> "         );
    if ( !init[6] )  fprintf( stderr, "<ArcDir> "            );
    if ( !init[7] )  fprintf( stderr, "<SumSuffix> "         );
    if ( !init[8] )  fprintf( stderr, "<SumDir> "            );
    if ( !init[9] )  fprintf( stderr, "<UWSuffix> "          );
    if ( !init[10])  fprintf( stderr, "<UWDir> "             );
    fprintf( stderr, "command(s) in <%s>; exitting!\n", configfile );
    exit( -1 );
  }
  
  return;
}

/*****************************************************************************
 *  uw_report_lookup( )   Look up important info from earthworm.h tables    *
 *****************************************************************************/
void uw_report_lookup( void )
{
  /* Look up keys to shared memory regions
  *************************************/
  if( ( RingKey = GetKey(RingName) ) == -1 ) {
    fprintf( stderr,
	     "uw_report:  Invalid ring name <%s>; exitting!\n", RingName);
    exit( -1 );
  }

  /* Look up installations of interest
   *********************************/
  if ( GetLocalInst( &InstId ) != 0 ) {
    fprintf( stderr, 
	     "uw_report: error getting local installation id; exitting!\n" );
    exit( -1 );
  }

  /* Look up modules of interest
   ***************************/
  if ( GetModId( MyModName, &MyModId ) != 0 ) {
    fprintf( stderr, 
	     "uw_report: Invalid module name <%s>; exitting!\n", MyModName );
    exit( -1 );
  }

  /* Look up message types of interest
   *********************************/
  if ( GetType( "TYPE_HEARTBEAT", &TypeHeartBeat ) != 0 ) {
    fprintf( stderr, 
	     "uw_report: Invalid message type <TYPE_HEARTBEAT>; exitting!\n" );
    exit( -1 );
  }
  if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
    fprintf( stderr, 
	     "uw_report: Invalid message type <TYPE_ERROR>; exitting!\n" );
    exit( -1 );
  }
  if ( GetType( "TYPE_HINVARC", &TypeHinvArc ) != 0 ) {
    fprintf( stderr, 
	     "uw_report: Invalid message type <TYPE_HINVARC>; exitting!\n" );
    exit( -1 );
  }
  if ( GetType( "TYPE_H71SUM", &TypeH71Sum ) != 0 ) {
    fprintf( stderr, 
	     "uw_report: Invalid message type <TYPE_H71SUM>; exitting!\n" );
    exit( -1 );
  }
  return;
} 

/*******************************************************************************
 * uw_report_status() builds a heartbeat or error message & puts it into    *
 *                       shared memory.  Writes errors to log file & screen.   *
 *******************************************************************************/
void uw_report_status( unsigned char type, short ierr, char *note )
{
  MSG_LOGO    logo;
  char	       msg[256];
  long	       size;
  long        t;
 
  /* Build the message
  *******************/ 
  logo.instid = InstId;
  logo.mod    = MyModId;
  logo.type   = type;

  time( &t );

  if( type == TypeHeartBeat )
    {
      sprintf( msg, "%ld\n\0", t);
    }
  else if( type == TypeError )
    {
      sprintf( msg, "%ld %hd %s\n\0", t, ierr, note);
      logit( "et", "uw_report: %s\n", note );
    }

  size = strlen( msg );   /* don't include the null byte in the message */ 	

  /* Write the message to shared memory
  ************************************/
  if( tport_putmsg( &Region, &logo, size, msg ) != PUT_OK )
    {
      if( type == TypeHeartBeat ) {
	logit("et","uw_report:  Error sending heartbeat.\n" );
      }
      else if( type == TypeError ) {
	logit("et","uw_report:  Error sending error:%d.\n", ierr );
      }
    }

  return;
}

