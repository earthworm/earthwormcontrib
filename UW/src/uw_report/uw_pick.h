/*    uw_report.h */

typedef struct {
  char     sta_name[5];     /* station name is 4 chars plus a null char */
  char     comp[4];         /* station component is 3 chars plus null char */
  int      p;               /* P phase arrival time, hundreths of secs */
  char     pfm;             /* U or D first motion */
  int      pwt;             /* P pick weight */
  int      pres;            /* P pick residual */
  float    punc;            /* P pick uncertainty */
  int      dur;             /* Coda duration */
} PICK;
