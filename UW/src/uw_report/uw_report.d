#
# This is uw_report's parameter file

#  Basic Earthworm setup:
#
MyModuleId         MOD_REPORT     # module id for this instance of report 
RingName           HYPO_RING      # ring to get input from
LogFile            1              # 0 to completely turn off disk log file
HeartBeatInterval  15             # seconds between heartbeats

# List the message logos to grab from transport ring
#              Installation       Module          Message Types
GetEventsFrom  INST_WILDCARD    MOD_EQPROC        # hinvarc & h71sum

# Set suffixes for output filenames
# NOTE: If a suffix is "none", no file will   
#       be reported for that type of message
ArcSuffix     ".arc"        # suffix for archive (hinvarc message) files
SumSuffix     "none"        # suffix for summary (h71sum message) files
UWSuffix      "y"           # suffix for UW pickfile from earthworm

# Directories to write files, end with a `/'
# NOTE: If a suffix is "none", no corresponding directory is needed
# Don't leave trailing spaces!
ArcDir     /u1/barbara/EW_ARC/
# SumDir   not needed
UWDir      /sunworm/EARTHWORM/v2.8plusUW/src/uw_report/Pick/

# Command to run on UW pick file.
# Command will be run in background, so its output should be collected by
# the command itself.
# Command may be ommitted here if UWSuffix is "none"
UWCommand  /sunworm/EARTHWORM/UW-src/test-script

# Optional command to put network ID in the pickfile
# WriteNet
