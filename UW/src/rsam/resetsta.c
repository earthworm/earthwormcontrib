/*
 * resetsta.c: Reset a station structure due to an extended gap.
 *              1) Reset the members of the STATION structure.
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: ResetStation						*/
/*									*/
/*	Inputs:		Pointer to a Rsam station structure		*/
/*									*/
/*	Outputs:	Updated station structure(above)		*/
/*									*/
/*	Returns:	0 on success					*/
/*			Error code as defined in rsam.h on 		*/
/*                      failure						*/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>
#include <stdlib.h>	/* malloc					*/
#include <string.h>	/* memset					*/

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>

/*******							*********/
/*	Rsam Includes							*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: ResetStation						*/
int ResetStation( STATION* station )
{
  int		retVal = 0;	/* Return value for this function.	*/

  /*	Zero out the trace buffer					*/
  memset( station->traceBuf, 0, station->bufSize );

  /*	Zero some of the numerical members				*/
  station->LTA = 0.0;
  station->LTAR = 0.0;
  station->sampleRate = 0.0;
  station->dataSize = 0;
  station->bufSampsEnd = 0;
  station->bufSampsStart = 0;
  station->bufSecsEnd = 0;
  station->bufSecsStart = 0;
  station->calcSamps = 0;
  station->calcSecs = 0;
  station->numSamps = 0;
  station->numSSR = 0;

  /*	Set the data type member					*/
  station->dataType = UNKNOWN;

  return ( retVal );
}
