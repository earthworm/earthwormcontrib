/*
 * cmprscn.c: CompareSCNs
 *              1) Compare alphabetical order of SCN's
 *		2) Passed to qsort() to sort the station list
 *		3) Passed to bsearch() to look up an SCN in the list
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: compareSCNs						*/
/*									*/
/*	Inputs:		Pointers to two station structures		*/
/*									*/
/*	Outputs:	Relative order of SCNs				*/
/*									*/
/*	Returns:	0 if SCN's are identical			*/
/*		       +1 if first SCN comes after second SCN		*/
/*		       -1 if second SCN comes after first SCN		*/


#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <string.h>	/* strcmp					*/

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>	/* logit					*/

/*******							*********/
/*	Rsam Includes						*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/


int CompareSCNs( const void *s1, const void *s2 )
{
   int rc;
   STATION *t1 = (STATION *) s1;
   STATION *t2 = (STATION *) s2;

   rc = strcmp( t1->staCode, t2->staCode );
   if ( rc != 0 ) return rc;
   rc = strcmp( t1->compCode, t2->compCode );
   if ( rc != 0 ) return rc;
   rc = strcmp( t1->netCode,  t2->netCode );
   return rc;
}
