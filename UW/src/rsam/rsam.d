#
# Rsam's Parameter File
#

#
#  Basic Earthworm Setup
#
MyModuleId	MOD_RSAM	# Module id for this instance of Rsam
				# 
Debug		1		# Write out debug messages? (0 = No,
				#   1 = Minimal, 3 = Chatterbox )
RingNameIn	WAVE_RING	# Name of ring from which trace data will be
				#   read - REQUIRED.
RingNameOut	PICK_RING	# Name of ring to which BS will be
				#   written - REQUIRED.
HeartBeatInterval	30	# Heartbeat Interval (seconds).

#
# Rsam Specific Setup
#
nSecs          30               # Number of seconds to average
MaxGap		1		# Maximum gap between trace data points that
				#   can be interpolated (otherwise restart the
				#   station).
LogDir         /cloudhome/earthworm/run/log/  # directory for logging data
LogBasename    Rsam             # base name of data file; year, month and day
                                # are appending to the basename.
Command     /bin/true           # a command to run on the datafile; not
                                #  currently implemented.
#
# Station list
#"Station"  sta   comp    net
Station   SEP EHZ     UW
Station   EDM EHZ     UW
Station   HSR EHZ     UW
Station   SHW EHZ     UW
Station   SOS EHZ     UW
Station   FL2 EHZ     UW
Station   RCM EHZ     UW
Station   RCS EHZ     UW
Station   FMW EHZ     UW
Station   LO2 EHZ     UW
