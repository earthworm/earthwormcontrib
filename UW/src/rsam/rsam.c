/*
 * rsam.c: 	Realtime Seismic Amplitude Monitor (ala Murry and Endo)
 *		Log RSAM values and alarm on big events
 *              1) reads a configuration file using kom.c routines 
 *                 (ReadConfig).
 *              2) looks up shared memory keys, installation ids, 
 *                 module ids, message types from earthworm.h tables 
 *                 using getutil.c functions (ReadEWH).
 *              3) attaches to a public shared memory regions for
 *                 input using transport.c functions.
 *              4) processes hard-wired message types from configuration-
 *                 file-given installations & module ids (This source
 *                 code expects to process TYPE_TRACEBUF messages).
 *              5) produces running ascii log files and executes alarm script
 *              6) sends heartbeats and error messages back to the
 *                 shared memory region (StatusReport).
 *              7) errors to a log file using logit.c functions.
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: main							*/
/*									*/
/*	Inputs:		Configuration File Name				*/
/*									*/
/*	Outputs:	Log Messages					*/
/*									*/
/*	Returns:	0 on success					*/
/*			error code as defined in rsam.h on failure	*/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>
#include <stdlib.h>	/* exit, free, malloc, realloc			*/
#include <sys/types.h>	/* time						*/
#include <time.h>	/* time						*/

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>	/* logit					*/

/*******							*********/
/*	Rsam Includes							*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: main							*/
main( int argc, char* argv[] )
{
  char		msg[MAXMESSAGELEN];	/* Status or error text.	*/
  char*		inBuf;		/* Pointer to the input message buffer.	*/
  RSAMEWH	rsamEwh;	/* Earthworm.[hd] configuration params.	*/
  int		result;		/* Result from function calls.		*/
  long		inBufLen;	/* Maximum message size in bytes.	*/
  long		sizeMsg;	/* Size of retrieved message.		*/
  MSG_LOGO	logoMsg;	/* Logo of retrieved message.		*/
  MSG_LOGO	logoWave;	/* Logo of requested waveforms.		*/
  WORLD	rsamWorld;		/* The entire network's information.	*/
  SHM_INFO	regionIn;	/* Input shared memory region info.	*/
  time_t	timeNow;	/* Current time.			*/
  time_t	timeLastBeat;	/* Time last heartbeat was sent.	*/

  /*	Check command line arguments					*/
  if ( 2 != argc )
  {
    fprintf( stderr, "Usage: %s <configfile>\n", argv[0] );
    exit( ERR_USAGE );
  }
           
  /*	Initialize parameter structures to defaults			*/
  result = InitializeParameters( &rsamEwh, &rsamWorld );
  if ( CT_FAILED( result ) )
  {
    fprintf( stderr, "%s module initialization failed.\n", argv[0] );
    exit( ERR_INIT );
  }

  /*	Initialize pointer variables					*/
  inBuf = NULL;

  /*	Read the configuration file					*/
  result = ReadConfig( argv[1], &rsamWorld );
  if ( CT_FAILED( result ) )
  {
    fprintf( stderr, "%s error in ReadConfig: %d!\n", argv[0], result );
    exit( result );
  }

  /*	Look up important info from earthworm.h tables			*/
  result = ReadEWH( &(rsamWorld.rsamParam), &rsamEwh );
  if ( CT_FAILED( result ) )
  {
    fprintf( stderr, "%s error in ReadEWH: %d!\n", argv[0], result );
    exit( result );
  }
 
  /*	Initialize message logging 					*/
  logit_init( argv[0], (short) rsamEwh.myModId, MAXMESSAGELEN, 1 );

  /*	Log a startup message						*/
  if ( rsamWorld.rsamParam.debug )
    logit( "t", "%s initialized with configuration file '%s'\n", argv[0],
	argv[1] );

  /*	Read the station file						*/
  result = ReadStations( &rsamWorld );
  if ( CT_FAILED( result ) )
    exit( result );

  /*	Allocate the message input buffer - use a default size to start	*/
  inBufLen = ( sizeof( long ) * ( 100 + rsamWorld.maxGap - 1 ) ) + 64;
	/*sizeof( TRACE_HEADER );*/
  if ( ! ( inBuf = (char *) malloc( (size_t) inBufLen ) ) )
  {
    logit( "et", "%s: Memory allocation failed - initial message buffer!\n",
	argv[0] );
    exit( ERR_MALLOC );
  }

  /*	Attach to Input and Output shared memory rings			*/
  tport_attach( &regionIn, rsamWorld.rsamParam.ringInKey );
  if ( rsamWorld.rsamParam.debug )
    logit( "t", "%s: Attached to public memory region %s:%d for input.\n", 
  	argv[0], rsamWorld.rsamParam.ringIn, rsamWorld.rsamParam.ringInKey );

  tport_attach( &(rsamWorld.regionOut), rsamWorld.rsamParam.ringOutKey );
  if ( rsamWorld.rsamParam.debug )
    logit( "t", "%s: Attached to public memory region %s:%d for output.\n", 
  	argv[0], rsamWorld.rsamParam.ringOut, rsamWorld.rsamParam.ringOutKey );

  /*	Specify logos of incoming waveforms				*/
  logoWave.instid = rsamEwh.readInstId;
  logoWave.mod    = rsamEwh.readModId;
  logoWave.type   = rsamEwh.typeWaveform;

  /*	Specify logos of outgoing messages				*/
  rsamWorld.outLogo.instid = rsamEwh.myInstId;
  rsamWorld.outLogo.mod    = rsamEwh.myModId;

  if ( rsamWorld.rsamParam.debug > 1 )
    {
      logit( "t", "%s: Reading messages (i%u m%u t%u)\n", argv[0],
	     logoWave.instid, logoWave.mod, logoWave.type );
      logit( "", "heartbeatint: %d\n", rsamWorld.rsamParam.heartbeatInt );
    }
  
  /*	Flush the input ring						*/
  if ( rsamWorld.rsamParam.debug > 2 )
    logit( "t", "%s: Flushing input buffer...", argv[0] );
  while ( tport_getmsg( &regionIn, &logoWave, 1, &logoMsg, &sizeMsg,
  	inBuf, inBufLen ) != GET_NONE );
  if ( rsamWorld.rsamParam.debug > 2 )
    logit( "t", "Done\n", argv[0] );

  /*	Force a heartbeat to be issued in first pass thru main loop	*/
  timeLastBeat = time( &timeNow ) - rsamWorld.rsamParam.heartbeatInt - 1;

  /*	Main message processing loop					*/
  while ( tport_getflag( &regionIn ) != TERMINATE )
  {
    /*	Check for need to send heartbeat message			*/
    if ( time( &timeNow ) - timeLastBeat >= rsamWorld.rsamParam.heartbeatInt )
    {
      timeLastBeat = timeNow;
      StatusReport( &rsamWorld, rsamEwh.typeHeartbeat, 0, "" ); 
      if ( rsamWorld.rsamParam.debug > 2 )
        logit( "t", "%s: Sent Heartbeat\n", argv[0] );
    }

    /*	Get next message						*/
    result = tport_getmsg( &regionIn, &logoWave, 1, &logoMsg, &sizeMsg, inBuf,
	inBufLen - 1 );

    /*	Arsam on the return code from the transport function		*/
    switch ( result )
    {
      case GET_OK:	/* got a message, no errors or warnings		*/
	if ( rsamWorld.rsamParam.debug > 2 )
	  logit( "t", "%s: Message received(i%u m%u t%u).\n", argv[0],
		logoMsg.instid, logoMsg.mod, logoMsg.type );
	break;
      case GET_NONE:	/* no messages of interest, check again later	*/
	/* if ( rsamWorld.rsamParam.debug > 2 )
	 *          logit( "t", "%s: No messages, sleeping.\n", argv[0] );
	 */
        sleep_ew( 500 );	/* milliseconds				*/
        continue;
      case GET_NOTRACK:	/* got a msg, but can't tell if any were missed	*/
        sprintf( msg, "%s: Message received(i%u m%u t%u); NTRACK_GET exceeded",
		argv[0], logoMsg.instid, logoMsg.mod, logoMsg.type );
        StatusReport( &rsamWorld, rsamEwh.typeError, ERR_NOTRACK, msg );
	break;
      case GET_MISS_LAPPED:	/* got a msg, but also missed lots	*/
        sprintf( msg, "%s: Message received(i%u m%u t%u); missed lots",
		argv[0], logoMsg.instid, logoMsg.mod, logoMsg.type );
        StatusReport( &rsamWorld, rsamEwh.typeError, ERR_MISSLAPMSG,
		msg );
        break;
      case GET_MISS_SEQGAP:	/* got a msg, but seq gap		*/
        sprintf( msg, "%s: Message received(i%u m%u t%u); sequence number gap",
		argv[0], logoMsg.instid, logoMsg.mod, logoMsg.type );
        StatusReport( &rsamWorld, rsamEwh.typeError, ERR_MISSGAPMSG,
		msg );
        break;
      case GET_MISS:	/* got a msg, but missed some			*/
        sprintf( msg, "%s: Message received(i%u m%u t%u); missed some",
		argv[0], logoMsg.instid, logoMsg.mod, logoMsg.type );
        StatusReport( &rsamWorld, rsamEwh.typeError, ERR_MISSMSG, msg );
        break;
      case GET_TOOBIG:	/* next message was too big, resize buffer	*/
	if ( rsamWorld.rsamParam.debug > 1 )
          logit( "t",
		"%s: Resizing input buffer to hold message(i%u m%u t%u)\n",
		argv[0], logoMsg.instid, logoMsg.mod, logoMsg.type );
	if ( ! ( inBuf = (char *) realloc ( inBuf, sizeMsg + 1 ) ) )
	{
          sprintf( msg, "%s: Memory allocation failed - resize message buffer",
		argv[0] );
          StatusReport( &rsamWorld, rsamEwh.typeError, ERR_MALLOC, msg );
          exit( ERR_MALLOC );
	}
	/*	Successful reallocation of memory			*/
	inBufLen = sizeMsg + 1;
	continue;
      default:	/* Unknown result					*/
        sprintf( msg, "%s: Unknown tport_getmsg result", argv[0] );
        StatusReport( &rsamWorld, rsamEwh.typeError, ERR_UNKNOWN, msg );
	continue;
    }

    /*	NULL terminate the message					*/
    inBuf[sizeMsg] = '\0';

    if ( rsamEwh.typeWaveform == logoMsg.type ) 
    {
      /*	Process the message					*/
      result = ProcessTraceMsg( &rsamWorld, inBuf );
      if ( CT_FAILED( result ) )
	exit( result );
    }
    else
      logit( "et", "%s: Unknown or unrequested message received!\n", argv[0] );

  /*	End of main processing loop					*/
  }  

  /*	Termination has been requested					*/
  if ( rsamWorld.rsamParam.debug )
    logit( "t", "%s: Termination requested; cleaning up...", argv[0] );

  /*	Detach from shared memory regions				*/
  tport_detach( &regionIn ); 
  tport_detach( &(rsamWorld.regionOut) ); 

  /*	Free memory used						*/
  free( rsamWorld.stations );
  free( inBuf );

  /*	Flush the output stream						*/
  fflush( stdout );

  if ( rsamWorld.rsamParam.debug )
  {
    logit( "o", "rsam done.\n" );
    logit( "t", "%s: Exit.\n", argv[0] );
  }
/*	End of function main						*/

  return ( 0 );
}
