/*
 * readstas.c: Read the network stations from a file.
 *              1) Create a list of stations based on the input file.
 *		2) Sort the station array for calls to bsearch()
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: ReadStations						*/
/*									*/
/*	Inputs:		Pointer to the Rsam World structure		*/
/*									*/
/*	Outputs:	Updated station list(above)			*/
/*									*/
/*	Returns:	0 on success					*/
/*			Error code as defined in rsam.h on 		*/
/*                      failure						*/
/*									*/
/*      Auxiliary function: IsComment					*/
/*									*/
/*	Inputs:		Pointer to a string containing a line from	*/
/*			Rsam station list				*/
/*									*/
/*	Outputs:	Comment determination				*/
/*									*/
/*	Returns:	1 if it's a comment line                        */
/*			0 if it's not a comment line			*/


#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>
#include <stdlib.h>	/* malloc, qsort				*/
#include <string.h>	/* strcpy					*/

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>	/* logit					*/

/*******							*********/
/*	Rsam Includes							*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: ReadStations						*/
int ReadStations( WORLD* rsamWorld )
{
  char          keyword[MAXLINELEN];
  char		inBuf[MAXLINELEN];	/* Input buffer.		*/
  FILE*		staFile;	/* Pointer to the station file stream.	*/
  STATION       *sta;
  int		retVal = 0;	/* Return value for this function.	*/
  int           i, nsta;
  
  /*	Validate the input parameter					*/
  if ( rsamWorld->rsamParam.staFile )
  {
    /*	Open the stations file						*/
    if ( staFile = fopen( rsamWorld->rsamParam.staFile, "r" ) )
    {
      /* Count channels in the station file.
	 Ignore comment lines and lines consisting of all whitespace.
	 ***********************************************************/
      nsta = 0;
      while ( fgets( inBuf, MAXLINELEN - 1, staFile ) != NULL )
	if ( IsStation( inBuf ) ) nsta++;

      rewind( staFile );

      /* 	Allocate the station list				*/
      if ( sta = (STATION *) calloc( (size_t) nsta, sizeof(STATION) ) )
      {
	/*	Properly initialize all the station structures		*/
	for ( i = 0; i < nsta; i++ )
	{
	  retVal = InitializeStation( &(sta[i]) );
	  if ( CT_FAILED( retVal ) )
	    break;
	}

	/* Read stations from the station list file into station array */
	i = 0;
	while ( fgets( inBuf, MAXLINELEN, staFile ) != NULL )
	{
	  int ttl; /* read ttl don't save it here */
	    
	  /* 	Check for comments 					*/
	  if ( ! IsStation( inBuf ) ) continue;
	    
	  /*	Process the line					*/
	  if ( 4 != sscanf( inBuf, "%s %s %s %s %d", keyword, sta[i].staCode, 
			    sta[i].compCode, sta[i].netCode ) )
	  {
	    logit( "et",
		   "Rsam: Unable to find station parameters in '%s'.\n",
		   inBuf );
	    retVal = ERR_STA_READ;
	    break;
	  }
	  sta[i].index = i;

	  /* ok, get ready for the next station */
	  i++;
	}
      }
      
      else 
      {
	logit( "et", "Rsam: Cannot allocate the station array\n" );
	retVal = ERR_MALLOC;
      }
      
      /*	Close the input file					*/
      fclose( staFile );
      
      rsamWorld->staIndex = malloc( sizeof(int) * nsta );
      
      /*	Sort the station array so bsearch() will work		*/
      qsort( sta, (size_t) nsta, sizeof(STATION), CompareSCNs );

      logit("", "Sorted station list:\n");
      for ( i = 0; i < nsta; i++ )
      {
	  logit( "", "\t%s.%s.%s\n", 
		 sta[i].staCode, sta[i].compCode, sta[i].netCode);
	  rsamWorld->staIndex[ sta[i].index ] = i;
      }
      
      rsamWorld->stations = sta;
      rsamWorld->nSta = nsta;
      
    }
    else
    {
      logit( "et", "Rsam: Error attempting to open '%s'.\n", 
	     rsamWorld->rsamParam.staFile );
      retVal = ERR_STA_OPEN;
    }
  }
  else
  {
    logit( "et", "Rsam: No stations file to open.\n" );
    retVal = ERR_STA_OPEN;
  }

  return ( retVal );
}


/***************************************************************************/

int IsStation( char string[] )
{
  int i;
  char Station[] = "Station";
  char space = ' ';
  char tab = '\t';
  
  for ( i = 0; i < (int)strlen( Station ); i++ )
    if ( string[i] != Station[i] ) return 0;

  /* Make sure it's not StationContrib or some such...			*/
  if ( string[i] == space || string[i] == tab )
    return 1;

  return 0;
}
