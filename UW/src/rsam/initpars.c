/*
 * initpars.c: Initialize parameter structures used by Rsam.
 *              1) Initialize members of the RSAMEWH structure.
 *              2) Initialize members of the WORLD structure.
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: InitializeParameters					*/
/*									*/
/*	Inputs:		Pointer to an Earthworm parameter structure	*/
/*			Pointer to a Rsam network parameter		*/
/*			  structure					*/
/*									*/
/*	Outputs:	Updated inputs(above)				*/
/*									*/
/*	Returns:	0 on success					*/
/*			Error code as defined in rsam.h on 	*/
/*			failure						*/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>
#include <stdlib.h>	/* malloc					*/

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>	/* logit					*/

/*******							*********/
/*	Rsam Includes						*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: InitializeParameters					*/
int InitializeParameters( RSAMEWH* rsamEwh, WORLD* rsamWorld )
{
  int	retVal = 0;	/* Return value for this function		*/

  /*	Initialize members of the RSAMEWH structure			*/
  rsamEwh->myInstId = 0;
  rsamEwh->myModId = 0;
  rsamEwh->readInstId = 0;
  rsamEwh->readModId = 0;
  rsamEwh->typeError = 0;
  rsamEwh->typeHeartbeat = 0;
  rsamEwh->typeWaveform = 0;

  /*	Initialize members of the WORLD structure			*/
  rsamWorld->rsamEWH = rsamEwh;
  rsamWorld->maxGap = 0;
  rsamWorld->nSecs = 60;
  rsamWorld->stations = NULL;
  rsamWorld->logDir[0] = '\0';
  strcpy( rsamWorld->logBase, "rsam" );
  rsamWorld->Command[0] = '\0';
  
  /*	Initialize members of the RSAMPARAM structure			*/
  rsamWorld->rsamParam.debug = 0;
  sprintf( rsamWorld->rsamParam.readInstName, "INST_WILDCARD" );
  sprintf( rsamWorld->rsamParam.readModName, "MOD_WILDCARD" );
  rsamWorld->rsamParam.ringIn[0] = '\0';
  rsamWorld->rsamParam.ringOut[0] = '\0';
  rsamWorld->rsamParam.staFile[0] = '\0';
  rsamWorld->rsamParam.heartbeatInt = 15;
  rsamWorld->rsamParam.ringInKey = 0;
  rsamWorld->rsamParam.ringOutKey = 0;

  return ( retVal );
}
