/*
 * addtrace.c: Append trace data to a station.
 *              1) Validate input trace data.
 *		2) Check for gaps and act accordingly based on gap size(s).
 *		3) Update the station's averages and critical times.
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: AppendTraceData					*/
/*									*/
/*	Inputs:		Pointer to a trace packet structure		*/
/*			Pointer to a station structure			*/
/*			Pointer to a network structure			*/
/*									*/
/*	Outputs:	Updated station structure(above)		*/
/*									*/
/*	Returns:	0 on success					*/
/*			Error code as defined in rsam.h on 	*/
/*                      failure						*/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>
#include <stdlib.h>	/* atoi						*/
#include <string.h>	/* memcpy					*/

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>

/*******							*********/
/*	Rsam Includes							*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: AppendTraceData					*/
int AppendTraceData( TracePacket* data, STATION* station, WORLD* rsamWorld )
{
  char*		bufferEnd;	/* Pointer to the end of the station's	*/
  /*   buffer.				*/
  char*		traceData;	/* Pointer to the beginning of the	*/
  /*   trace data.			*/
  DATATYPE	dataType;	/* Byte size of one data value.		*/
  int		dataSize;	/* Byte size of one data value.		*/
  int		retVal = 0;	/* Return value for this function.	*/
  long		gapSize;	/* Number of data points between the	*/
  /*   end of the buffer and the		*/
  /*   beginning of the new data.		*/
  long		samps;		/* Number of samples that do not make a	*/
  /*   full second's worth of data.	*/
  long		secs;		/* Number of full seconds of data.	*/

  /*	Determine the size and type of each data value			*/
  dataSize = atoi( &(data->trh.datatype[1]) );
  switch ( dataSize )
  {
  case 2:
    dataType = CT_SHORT;
    break;
  case 4:
#ifdef _SPARC
    if ( 's' == data->trh.datatype[0] )
#endif
#ifdef _INTEL
    if ( 'i' == data->trh.datatype[0] )
#endif
      dataType = CT_LONG;
    else
      dataType = CT_FLOAT;
    break;
  case 8:
    dataType = CT_DOUBLE;
    break;
  default:
    logit( "et", "rsam: Unknown data size of %d in AppendTraceData.\n",
	   dataSize );
    /*	Unknown size, quit now before major errors happen	*/
    return( ERR_UNKNOWN );
  }

  /* 	Check for existing data in the buffer			*/
  if ( station->numSamps > 0 )
  {
    /* 	Check for equal data types				*/
    if ( dataType == station->dataType )
    {
      /* Check for equal sampling rate				*/
      if ( data->trh.samprate == station->sampleRate )
      {
	/* Calculate the gap (difference between the current		*/
	/* buffer end time and new data start time multiplied by	*/
	/* sampling rate, then subtract 1 sample)			*/
	secs = (long) data->trh.starttime;
	samps = (long) ( ( data->trh.starttime - secs ) * station->sampleRate 
			 + 0.5 );
	if ( secs == station->bufSecsEnd )
	  /* Gap size is determined by the difference in samples	*/
	  gapSize = samps - station->bufSampsEnd - 1;
	else if ( secs < station->bufSecsEnd )
	  /* New data start time is definitely older than end time */
	  gapSize = (long) ( ( secs - station->bufSecsEnd ) *
			     station->sampleRate - 0.5 ) + samps - 
	    station->bufSampsEnd - 1;
	else
	  /* New data start time is possibly newer than end time	*/
	  gapSize = (long) ( ( secs - station->bufSecsEnd ) *
			     station->sampleRate + 0.5 ) + samps - 
	    station->bufSampsEnd - 1;

	/* Check gap threshold					*/
	if ( gapSize > rsamWorld->maxGap )
	{
	  /* Reset the station					*/
	  retVal = ResetStation( station );
	  if ( ! CT_FAILED( retVal ) )
	  {
	    if ( rsamWorld->rsamParam.debug )
	    {
	      logit( "t", "Rsam: Station (%s.%s.%s)\n", 
		     station->staCode, station->compCode, 
		     station->netCode );
	      logit( "", "\tReset due to prolonged gap.\n" );
	    }

	    /* Redo this function to start the station with the */
	    /* new data and parameters			*/
	    retVal = AppendTraceData( data, station, rsamWorld );
	  }
	}
	else  /* gapSize <= maxGap */
	{
	  /*	Check for misaligned data			    */
	  if ( 0 != gapSize )
	  {
	    if ( gapSize > 0 )
	    {
	      /*	Interpolate the data points in the gap	    */
	      retVal = InterpolateGap( gapSize, (char*) data + 
				       sizeof( TRACE_HEADER ), station );
	    }
	    else
	    {
	      /*	Data overlaps				  */
	      if ( (long)( data->trh.starttime * station->sampleRate + 0.5 ) > 
		   (long)( station->calcSecs * station->sampleRate + 0.5 ) + 
		   station->calcSamps )
	      {
		/* For now, just cut off the data that overlaps */
		station->bufSecsEnd = (long) data->trh.starttime;
		station->bufSampsEnd = (long) ( ( data->trh.starttime - 
						  station->bufSecsEnd ) * 
						station->sampleRate + 0.5 ) - 1;
		while ( station->bufSampsEnd < 0 )
		{
		  station->bufSecsEnd--;
		  station->bufSampsEnd += (long) ( station->sampleRate + 0.5 );
		}
		station->numSamps += gapSize;
	      }
	      else  /* large everlap */
	      {
		logit( "et", "Rsam: NEED TO CORRECT DATA OVERLAP (%d)!\n",
		       gapSize );
		return ( 0 );
	      }
	    }
	  }
	  /* Gap resolved, now insert new trace data */
	  if ( ! CT_FAILED( retVal ) )
	  {
	    /* Check for buffer overflow			*/
	    if ( ( data->trh.nsamp + station->numSamps ) * dataSize < station->bufSize )
	    {
	      /* Copy in the new data			*/
	      bufferEnd = station->traceBuf + ( station->numSamps * dataSize );
	      traceData = (char*) data + sizeof( TRACE_HEADER );
	      memcpy( bufferEnd, traceData, data->trh.nsamp * dataSize );

	      /* Set the buffer information parameters	    */
	      station->bufSecsEnd = (long) data->trh.endtime;
	      station->bufSampsEnd = (long) ( ( data->trh.endtime -
						station->bufSecsEnd ) *
					      station->sampleRate + 0.5 );
	      station->numSamps += data->trh.nsamp;

	      if ( rsamWorld->rsamParam.debug > 2 )
		logit( "t", "Rsam: Received another set of data for "
		       "station (%s.%s.%s).\n", station->staCode,
		       station->compCode, station->netCode );
	
	      /* Update the station's averages and triggers	*/
	      retVal = UpdateStation( station, rsamWorld );
	    }
	    else  /* nsamp + numsamps >= station->bufSize */
	    {
	      /* Flush out the buffer			*/
	      retVal = FlushBuffer( station );
	      if ( ! CT_FAILED( retVal ) )
	      {
		/* Redo this function to add the new data */
		/* to the	flushed buffer			*/
		retVal = AppendTraceData( data, station, rsamWorld );
	      }
	    }
	  }
	}
      }
      else  /* sample rates don't match */
      {
	logit( "t", "Rsam: Change in data sampling rate from %f to %f "
	       "for station (%s/%s).\n", station->sampleRate,
	       data->trh.samprate, station->staCode, station->compCode );

	/* Reset the station					*/
	retVal = ResetStation( station );
	if ( ! CT_FAILED( retVal ) )
	{
	  /* Redo this function to start the station with the new   */
	  /* data and parameters				    */
	  retVal = AppendTraceData( data, station, rsamWorld );
	}
      }
    }
    else  /* data types don't match */
    {
      size_t newBufSize;
      
      logit( "t", "Rsam: Change in data type to %s for station "
	     "(%s.%s.%s).\n", data->trh.datatype, station->staCode,
	     station->compCode, station->netCode );
      
      newBufSize = dataSize * ( rsamWorld->nSecs * data->trh.samprate +
				data->trh.nsamp );
      if ( newBufSize != station->bufSize )
      {
	station->traceBuf = realloc( station->traceBuf, newBufSize );
	if ( station->traceBuf == NULL )
	{
	  logit( "et", "Rsam: unable to re-allocate %d bytes for (%s.%s.%s)\n",
		 station->bufSize, station->staCode, station->compCode,
		 station->netCode );
	  return ( ERR_MALLOC );
	}
      }
      
      /* Reset the station					*/
      retVal = ResetStation( station );

      if ( ! CT_FAILED( retVal ) )
      {
	/* Redo this function to start the station with the new	*/
	/* data and parameters					*/
	retVal = AppendTraceData( data, station, rsamWorld );
      }
    }
  }
  else  /* no existing data in station trace buffer */
  {
    /* Do we need to allocate buffer space?			     	*/
    if ( station->bufSize == 0 )
    {
      station->bufSize = dataSize * ( rsamWorld->nSecs * data->trh.samprate +
				      data->trh.nsamp );
      if ( ( station->traceBuf = malloc( station->bufSize ) ) == NULL )
      {
	logit( "et", "Rsam: unable to allocate %d bytes for (%s.%s.%s)\n",
	       station->bufSize, station->staCode, station->compCode,
	       station->netCode );
	return ( ERR_MALLOC );
      }
    }
    
    /* Check for buffer overflow					*/
    if ( data->trh.nsamp * dataSize < station->bufSize )
    {
      /* Copy in the new data					*/
      traceData = (char*) data + sizeof( TRACE_HEADER );
      memcpy( station->traceBuf, traceData, data->trh.nsamp * dataSize );

      /* Set the buffer information parameters			*/
      station->dataType = dataType;
      station->sampleRate = data->trh.samprate;
      station->dataSize = dataSize;
      station->bufSecsStart = (long) data->trh.starttime;
      station->bufSampsStart = 
	(long) ( ( data->trh.starttime - station->bufSecsStart ) * 
		 station->sampleRate + 0.5 );
      station->bufSecsEnd = (long) data->trh.endtime;
      station->bufSampsEnd = 
	(long) ( ( data->trh.endtime - station->bufSecsEnd ) * 
		 station->sampleRate + 0.5 );
      station->numSamps = data->trh.nsamp;
      station->calcSecs = station->bufSecsStart;
      station->calcSamps = station->bufSampsStart;
	  
      if ( rsamWorld->rsamParam.debug > 2 )
	logit( "t", "Rsam: Received first set of data for station "
	       "(%s.%s.%s).\n", station->staCode, station->compCode,
	       station->netCode );
	
      /* Update the station's averages and triggers		*/
      retVal = UpdateStation( station, rsamWorld );
    }
    else
    {
      logit( "et",
	     "Rsam: Error appending trace data - buffer overflow.\n" );
    }
  }

  return ( retVal );
}
