/*
 * readcnfg.c: Read the Earthworm parameters.
 *              1) Set members of the RSAMEWH structure.
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: ReadEWH						*/
/*									*/
/*	Inputs:		Pointer to a Rsam Earthworm parameter	*/
/*			  structure					*/
/*									*/
/*	Outputs:	Updated input structure(above)			*/
/*			Error messages to stderr			*/
/*									*/
/*	Returns:	0 on success					*/
/*			Error code as defined in rsam.h on 	*/
/*                      failure 					*/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>	/* GetInst, GetLocalInst, GetModId, GetType	*/

/*******							*********/
/*	Rsam Includes						*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: ReadEWH						*/
int ReadEWH( RSAMPARAM* rsamParam, RSAMEWH* rsamEwh )
{
  int	retVal = 0;	/* Return value for this function		*/

  if ( CT_FAILED( GetLocalInst( &(rsamEwh->myInstId) ) ) )
  {
    fprintf( stderr, "Rsam: Error getting myInstId.\n" );
    retVal = ERR_EWH_READ;
  }
  else if ( CT_FAILED( GetModId( rsamParam->myModName, &(rsamEwh->myModId) ) ) )
  {
    fprintf( stderr, "Rsam: Error getting myModId.\n" );
    retVal = ERR_EWH_READ;
  }
  else if ( CT_FAILED( GetInst( rsamParam->readInstName,
	&(rsamEwh->readInstId) ) ) )
  {
    fprintf( stderr, "Rsam: Error getting readInstId.\n" );
    retVal = ERR_EWH_READ;
  }
  else if ( CT_FAILED( GetModId( rsamParam->readModName,
	&(rsamEwh->readModId) ) ) )
  {
    fprintf( stderr, "Rsam: Error getting readModId.\n" );
    retVal = ERR_EWH_READ;
  }
  else if ( CT_FAILED( GetType( "TYPE_ERROR", &(rsamEwh->typeError) ) ) )
  {
    fprintf( stderr, "Rsam: Error getting typeError.\n" );
    retVal = ERR_EWH_READ;
  }
  else if ( CT_FAILED( GetType( "TYPE_HEARTBEAT", &(rsamEwh->typeHeartbeat) ) ) )
  {
    fprintf( stderr, "Rsam: Error getting typeHeartbeat.\n" );
    retVal = ERR_EWH_READ;
  }
  else if ( CT_FAILED( GetType( "TYPE_TRACEBUF", &(rsamEwh->typeWaveform) ) ) )
  {
    fprintf( stderr, "Rsam: Error getting typeWaveform.\n" );
    retVal = ERR_EWH_READ;
  }

  return ( retVal );
}
