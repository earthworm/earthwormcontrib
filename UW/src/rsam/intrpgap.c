/*
 * intrpgap.c: Interpolate over a gap in a station's trace data.
 *              1) Interpolate trace values in the gap.
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: InterpolateGap					*/
/*									*/
/*	Inputs:		The size of the gap				*/
/*			Pointer to the new trace data			*/
/*			Pointer to a Rsam station structure	*/
/*									*/
/*	Outputs:	Updated station structure(above)		*/
/*									*/
/*	Returns:	0 on success					*/
/*			Error code as defined in rsam.h on 	*/
/*			failure						*/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>	/* logit					*/

/*******							*********/
/*	Rsam Includes							*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: InterpolateGap					*/
int InterpolateGap( long gapSize, char* traceData, STATION* station )
{
  char*		bufPtr;		/* Pointer to trace data in a buffer.	*/
  double	delta;		/* Change in value at each gap point.	*/
  double	valBeg;		/* Value at the beginning of the gap.	*/
  double	valEnd;		/* Value at the end of the gap.		*/
  int		retVal = 0;	/* Return value for this function.	*/
  long		count;		/* Loop counter.			*/

  /*	Retrieve the beginning gap value				*/
  bufPtr = station->traceBuf + ( (long) ( ( ( station->bufSecsEnd -
	station->bufSecsStart ) * station->sampleRate + 0.5 ) +
	station->bufSampsEnd - station->bufSampsStart ) *
	station->dataSize );
  switch ( station->dataType )
  {
    case CT_SHORT:
      valBeg = *( (short*) bufPtr );
      break;
    case CT_LONG:
      valBeg = *( (long*) bufPtr );
      break;
    case CT_FLOAT:
      valBeg = *( (float*) bufPtr );
      break;
    case CT_DOUBLE:
      valBeg = *( (double*) bufPtr );
      break;
    default:
      logit( "et", "Rsam: Unknown data type in InterpolateGap.\n" );
      retVal = ERR_UNKNOWN;
  }

  if ( ! CT_FAILED( retVal ) )
  {
    /*	Retrieve the ending gap value					*/
    switch ( station->dataType )
    {
      case CT_SHORT:
	valEnd = *( (short*) traceData );
	break;
      case CT_LONG:
        valEnd = *( (long*) traceData );
        break;
      case CT_FLOAT:
        valEnd = *( (float*) traceData );
        break;
      case CT_DOUBLE:
        valEnd = *( (double*) traceData );
        break;
      default:
	/*	This should have already been discovered in the first	*/
	/*	  switch statement					*/
        logit( "et", "Rsam: Unknown data type in InterpolateGap.\n" );
        retVal = ERR_UNKNOWN;
    }

    if ( ! CT_FAILED( retVal ) )
    {
      /*	Determine the increment for each value in the gap	*/
      delta = ( valEnd - valBeg ) / ( gapSize + 1.0 );

      /*	Create interpolated trace data values in the station's	*/
      /*	  buffer						*/
      for ( count = 0, bufPtr += station->dataSize; count < gapSize; count++,
		bufPtr += station->dataSize )
      {
	switch ( station->dataType )
	{
          case CT_SHORT:
	    if ( delta * count + valBeg < 0.0 )
	      *( (short*) bufPtr ) = (short) ( delta * count + valBeg - 0.5 );
	    else
	      *( (short*) bufPtr ) = (short) ( delta * count + valBeg + 0.5 );
	    break;
          case CT_LONG:
	    if ( delta * count + valBeg < 0.0 )
              *( (long*) bufPtr ) = (long) ( delta * count + valBeg + 0.5 );
            else
	      *( (long*) bufPtr ) = (long) ( delta * count + valBeg + 0.5 );
            break;
          case CT_FLOAT:
            *( (float*) bufPtr ) = (float) ( delta * count + valBeg );
            break;
          case CT_DOUBLE:
            *( (double*) bufPtr ) = delta * count + valBeg;
            break;
          default:
	    /*	This should have already been discovered in the first	*/
	    /*	  switch statement					*/
            logit( "et", "Rsam: Unknown data type in InterpolateGap.\n" );
	    /*	Quit now so only one message is printed			*/
            return ( ERR_UNKNOWN );
	}
      }

      /*	Adjust the station buffer's end time			*/
      station->bufSampsEnd += gapSize;
      while ( station->bufSampsEnd >= (long) ( station->sampleRate + 0.5 ) )
      {
	station->bufSecsEnd++;
	station->bufSampsEnd -= (long) ( station->sampleRate + 0.5 );
      }

      /*	Adjust the station's sample count			*/
      station->numSamps += gapSize;
    }
  }
  
  return ( retVal );
}
