/*
 * rsam.h: Definitions for the Rsam Earthworm Module.
 */

/*******							*********/
/*	Redefinition Exclusion						*/
/*******							*********/
#ifndef __RSAM_H__
#define __RSAM_H__

#include <time.h>	/* time_t					*/
#include <transport.h>  /* Shared memory                                */
#include <trace_buf.h>	/* TracePacket					*/

/*******							*********/
/*	Constant Definitions						*/
/*******							*********/

#define DOUBLE_EQUAL	0.0001	/* If the difference in value of	*/
				/*   variables of type double is less	*/
				/*   than this value, the variables are	*/
				/*   considered equal.			*/

  /*	Error Codes - Must coincide with rsam.desc			*/
#define	ERR_USAGE	1	/* Module started with improper params.	*/
#define	ERR_INIT	2	/* Module initialization failed.	*/
#define	ERR_CONFIG_OPEN	3	/* Error opening configuration file.	*/
#define	ERR_CONFIG_READ	4	/* Error reading configuration file.	*/
#define	ERR_EWH_READ	5	/* Error reading earthworm.h info.	*/
#define	ERR_STA_OPEN	6	/* Error opening station file.		*/
#define	ERR_STA_READ	7	/* Error reading station file.		*/
#define	ERR_MALLOC	10	/* Memory allocation failed.		*/

#define	ERR_MISSMSG	11	/* Message missed in transport ring.	*/
#define	ERR_NOTRACK	12	/* Message retreived, but tracking	*/
				/*   limit exceeded.			*/
#define	ERR_TOOBIG	13	/* Retreived message too large for	*/
				/*   buffer.				*/
#define	ERR_MISSLAPMSG	14	/* Some messages were overwritten.	*/
#define	ERR_MISSGAPMSG	15	/* There was a gap in message seq #'s.	*/
#define	ERR_UNKNOWN	16	/* Unknown function return code.	*/
#define	ERR_PROC_MSG	17	/* Error processing a trace message.	*/
#define	ERR_LOG	        18	/* Error writing data file.		*/


  /*	Buffer Lengths							*/
#define	MAXCODELEN	8	/* Maximum length of a code.		*/
#define	MAXFILENAMELEN	80	/* Maximum length of a file name.	*/
#define	MAXLINELEN	240	/* Maximum length of a line read from	*/
				/*   or written to a file.		*/
#define	MAXMESSAGELEN	160	/* Maximum length of a status or error	*/
				/*   message.				*/
#define	MAXRINGNAMELEN	20	/* Maximum length of a ring name.	*/


/*******							*********/
/*	Macro Definitions						*/
/*******							*********/
#define	CT_FAILED( a ) ( 0 != a )	/* Generic function failure	*/
					/*   test.			*/


/*******							*********/
/*	Enumeration Definitions						*/
/*******							*********/

  /*	Data type of trace data						*/
typedef enum _DATATYPE
{
  UNKNOWN = 0,	/* Unknown data type.					*/
  CT_SHORT = 1,	/* Trace data is of type short.				*/
  CT_LONG = 2,	/* Trace data is of type long.				*/
  CT_FLOAT = 3,	/* Trace data is of type float.				*/
  CT_DOUBLE = 4	/* Trace data is of type double.			*/
} DATATYPE;

/*******							*********/
/*	Structure Definitions						*/
/*******							*********/

  /*	Rsam Parameter Structure (read from *.d files)			*/
typedef struct _RSAMPARAM
{
  char	debug;				/* Write out debug messages?	*/
					/*   ( 0 = No, 1 = Yes )	*/
  char	myModName[MAXFILENAMELEN];	/* Name of this instance of the	*/
					/*   Rsam module -		*/
					/*   REQUIRED.			*/
  char	readInstName[MAXFILENAMELEN];	/* Name of installation that	*/
					/*   is producing trace data	*/
					/*   messages.			*/
  char	readModName[MAXFILENAMELEN];	/* Name of module at the above	*/
					/*   installation that is	*/
					/*   producing the trace data	*/
					/*   messages.			*/
  char	ringIn[MAXRINGNAMELEN];		/* Name of ring from which	*/
					/*   trace data will be read -	*/
					/* REQUIRED.			*/
  char	ringOut[MAXRINGNAMELEN];	/* Name of ring to which	*/
					/*   triggers will be written -	*/
					/* REQUIRED.			*/
  char	staFile[MAXFILENAMELEN];	/* Name of file containing	*/
					/*   station information -	*/
					/* REQUIRED.			*/
  int	heartbeatInt;			/* Heartbeat Interval(seconds).	*/
  long	ringInKey;			/* Key to input shared memory	*/
					/*   region.			*/
  long	ringOutKey;			/* Key to output shared memory	*/
					/*   region.			*/
} RSAMPARAM;

  /*	Information Retrieved from Earthworm.h				*/
typedef struct _RSAMEWH
{
  unsigned char	myInstId;	/* Installation running this module.	*/
  unsigned char	myModId;	/* ID of this module.			*/
  unsigned char	readInstId;	/* Retrieve trace messages from		*/
				/*   specified installation.		*/
  unsigned char	readModId;	/* Retrieve trace messages from		*/
				/*   specified module.			*/
  unsigned char	typeRsam;	/* Rsam message type.			*/
  unsigned char	typeError;	/* Error message type.			*/
  unsigned char typeHeartbeat;	/* Heartbeat message type.		*/
  unsigned char	typeWaveform;	/* Waveform message type.		*/
} RSAMEWH;

typedef struct _STATION
{
  char		staCode[MAXCODELEN];	/* Station code (name).		*/
  char		compCode[MAXCODELEN];	/* Component code.		*/
  char		netCode[MAXCODELEN];	/* Network code.		*/
  int           index;          /* Index number from station list	*/
  char		*traceBuf;	/* Buffered trace data.			*/
  size_t        bufSize;	/* size of traceBuf			*/
  double	LTA;		/* Long-term average at calcTime.	*/
  double	LTAR;		/* Long-term average at calcTime	*/
				/*   (rectified).			*/
  double	sampleRate;	/* Sample Rate (hz)			*/
  int		channelNum;	/* Unique channel number (pin number).	*/
  int		dataSize;	/* Byte size of one data value.		*/
  long		bufSampsEnd;	/* Used to determine the actual end	*/
				/*   time of data in the buffer.	*/
  long		bufSampsStart;	/* Used to determine the actual start	*/
				/*   time of data in the buffer.	*/
  long		bufSecsEnd;	/* End time of data in the buffer.	*/
				/*   Actual end time is bufSecsEnd +	*/
				/*   (bufSampsEnd * 1.0) / sampleRate.	*/
  long		bufSecsStart;	/* Start time of data in the buffer.	*/
				/*   Actual start time is bufSecsStart	*/
				/*   + ( bufSampsStart * 1.0 ) /	*/
				/*   sampleRate.			*/
  long		calcSamps;	/* Used to determine the actual time	*/
				/*   for which an STA/LTA calculation	*/
				/*   has been performed.		*/
  long		calcSecs;	/* Latest time for which an STA/LTA	*/
				/*   calculation has been performed.	*/
				/*   Actual calc time is calcSecs +	*/
				/*   ( calcSamps * 1.0 ) / sampleRate.	*/
  long		numSamps;	/* Number of samples in traceBuf.	*/
  long		numSSR;		/* Number of Samples Since Restart at	*/
  				/*   calcTime.				*/
  DATATYPE	dataType;	/* Trace data type (short, float, etc.)	*/
} STATION;

  /*	Rsam World structure						*/
typedef struct _WORLD
{
  RSAMEWH*	rsamEWH;	/* Pointer to the Earthworm parameters.	*/
  RSAMPARAM	rsamParam;	/* Network parameters.			*/
  SHM_INFO      regionOut;      /* Output shared memory region info     */
  MSG_LOGO      outLogo;	/* Logo of outgoing message.		*/
  STATION*	stations;	/* Array of Stations in the network.	*/
  int          *staIndex;       
  int           nSta;           /* Number of stations in the array      */
  long		maxGap;		/* Maximum gap between trace data 	*/
				/*   points that can be	interpolated	*/
				/*   (otherwise	restart the station).	*/
  long		nSecs;   	/* number of seconds in averaging	*/
				/*   interval				*/
  time_t        lastLog;        /* Epoch time when we last logged data  */
  char          logDir[50];
  char          logBase[20];
  char          Command[100];
} WORLD;

/*	All the Rsam function prototypes				*/
int AppendTraceData( TracePacket *data, STATION *station, WORLD *rsamWorld );
int CompareSCNs( const void *s1, const void *s2 );
STATION * FindStation( char *staCode, char *compCode, char *netCode, 
		       WORLD *rsamWorld );
int FlushBuffer( STATION* station );
int InitializeParameters( RSAMEWH *rsamEwh, WORLD *rsamWorld );
int InitializeStation( STATION *station );
int InterpolateGap( long gapSize, char *traceData, STATION *station );
int ProcessTraceMsg( WORLD *rsamWorld, char *msg );
int ReadConfig( char *filename, WORLD *rsamWorld );
int ReadEWH( RSAMPARAM *rsamParam, RSAMEWH *rsamEwh );
int ReadStations( WORLD* rsamWorld );
int ResetStation( STATION *station );
void StatusReport( WORLD *rsamWorld, unsigned char type, short code, 
		   char *message );
int UpdateStation( STATION *station, WORLD *rsamWorld );
int SamIAm( WORLD *rsamWorld );


#endif /*	__RSAM_H__					*/

