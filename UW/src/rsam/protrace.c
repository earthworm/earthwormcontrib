/*
 * protrace.c: Process a trace data message that came in from the input ring.
 *              1) Read the header information in the message.
 *              2) Append the trace data to the appropriate station.
 *		3) Update the subnet's that contain the station.
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: ProcessTraceMsg					*/
/*									*/
/*	Inputs:		Pointer to a World information structure	*/
/*			Pointer to a message buffer			*/
/*									*/
/*	Outputs:	Updated station triggers			*/
/*									*/
/*	Returns:	0 on success					*/
/*			Error code as defined in rsam.h on 	*/
/*                      failure						*/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>
#include <string.h>	/* strcmp					*/

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>
#include <swap.h>	/* WaveMsgMakeLocal				*/
#include <trace_buf.h>	/* TRACE_HEADER, TracePacket			*/
#include <transport.h>	/* SHM_INFO					*/

/*******							*********/
/*	Rsam Includes							*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: ProcessTraceMsg					*/
int ProcessTraceMsg( WORLD* rsamWorld, char* msg )
{
  int		retVal = 0;	/* Return value for this function.	*/
  STATION*	station;	/* Pointer to a station.		*/
  TracePacket*	packet;		/* Incoming trace data packet.		*/

  /*	Convert the message to the local byte-ordering system		*/
  packet = (TracePacket*) msg;
  if ( WaveMsgMakeLocal( (TRACE_HEADER*) packet ) < 0 )
  {
    logit( "et", "Rsam: Error localizing trace data packet.\n" );
    retVal = ERR_PROC_MSG;
  }
  else
  {
    if ( rsamWorld->rsamParam.debug > 3 )
    {
      logit( "t", "Rsam: Processing trace data packet.\n" );
      logit( "", "\tHeader Info:\n" );
      logit( "", "\t\tPin Number: %d\n", packet->trh.pinno );
      logit( "", "\t\tNumber of Samples: %d\n", packet->trh.nsamp );
      logit( "", "\t\tStart Time: %lf\n", packet->trh.starttime );
      logit( "", "\t\tEnd Time: %lf\n", packet->trh.endtime );
      logit( "", "\t\tSample Rate: %lf\n", packet->trh.samprate );
      logit( "", "\t\tStation Code: %s\n", packet->trh.sta );
      logit( "", "\t\tNetwork Code: %s\n", packet->trh.net );
      logit( "", "\t\tComponent Code: %s\n", packet->trh.chan );
      logit( "", "\t\tData Format: %s\n", packet->trh.datatype );
      logit( "", "\t\tData Quality: %c\n", packet->trh.quality );
    }

    /*	Locate the appropriate station in the master list		*/
    station = FindStation( packet->trh.sta, packet->trh.chan, packet->trh.net,
			   rsamWorld );
    if ( station )
    {
      /*	Found station, append the new trace data		*/
      retVal = AppendTraceData( packet, station, rsamWorld );
    }
    else
    {
      if ( rsamWorld->rsamParam.debug > 1 )
	logit( "et", "Rsam: Unable to find station (%s/%s) for trace data "
	       "append.\n", packet->trh.sta, packet->trh.chan );
    }
  }
  
  return ( retVal );
}
