/*
 * statrpt.c: Produce an error or heartbeat message on the output ring.
 *              1) Construct the correct type of message.
 *              2) Send the message to the output ring.
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: StatusReport						*/
/*									*/
/*	Inputs:		Pointer to the World structure			*/
/*			Message type					*/
/*			Message id(code)				*/
/*			Pointer to a string(message)			*/
/*									*/
/*	Outputs:	Message structure sent to output ring		*/
/*									*/
/*	Returns:	Nothing						*/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>
#include <sys/types.h>	/* time						*/
#include <time.h>	/* time						*/
#include <strings.h>    /* strlen					*/

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>	/* logit					*/
#include <transport.h>	/* MSG_LOGO, SHM_INFO, tport_putmsg		*/

/*******							*********/
/*	Rsam Includes						*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: StatusReport						*/
void StatusReport( WORLD* rsamWorld, unsigned char type, short code, 
		   char* message )
{
  char		outMsg[MAXMESSAGELEN];	/* The outgoing message.	*/
  time_t	msgTime;	/* Time of the message.			*/

  /*	Validate the input parameters					*/
  if ( rsamWorld )
  {
    /*	Get the time of the message					*/
    time( &msgTime );

    /*	Build the message based on the type				*/
    if ( rsamWorld->rsamEWH->typeHeartbeat == type )
    {	
      rsamWorld->outLogo.type   = rsamWorld->rsamEWH->typeHeartbeat;
      sprintf( outMsg, "%ld\n\0", msgTime );
    }
    else
    {
      rsamWorld->outLogo.type   = rsamWorld->rsamEWH->typeError;
      if ( message )
	sprintf( outMsg, "%ld %hd %s\n\0", msgTime, code, message );
      else
	sprintf( outMsg, "%ld %hd (No description)\n\0", msgTime, code );
    }

    /*	Write the message to the output region				*/
    if ( tport_putmsg( &(rsamWorld->regionOut), &(rsamWorld->outLogo), 
		       (long) strlen( outMsg ), outMsg ) != PUT_OK )
    {
      /*	Log an error message					*/
      logit( "et", "Rsam: Failed to send a status message (%d).\n",
		code );
    }
  }
}
