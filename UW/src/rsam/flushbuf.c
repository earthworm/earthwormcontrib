/*
 * flushbuf.c: Flush a station's trace data buffer.
 *              1) Empty some of the trace data from the buffer.
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: FlushBuffer						*/
/*									*/
/*	Inputs:		Pointer to a Rsam station structure		*/
/*									*/
/*	Outputs:	Updated station structure(above)		*/
/*									*/
/*	Returns:	0 on success					*/
/*			Error code as defined in rsam.h on 		*/
/*                      failure						*/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>
#include <string.h>	/* memmove					*/

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>

/*******							*********/
/*	Rsam Includes							*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: FlushBuffer						*/
int FlushBuffer( STATION* station )
{
  char*	bufPtr;		/* Pointer to the beginning of data to keep.	*/
  int	retVal = 0;	/* Return value for this function.		*/
  long	sampsToFlush;	/* Number of data points to remove.		*/
  long	sampsToKeep;	/* Number of data points to keep.		*/
  long	secs;		/* Number of full seconds to flush.		*/

  /*	Determine if the buffer has data to flush			*/
  secs = station->calcSecs - station->bufSecsStart;
  if ( ( secs > 0 ) || ( ( secs == 0 ) && ( station->calcSamps -
	station->bufSampsStart > 0 ) ) )
  {
    /*	Calculate the amount of data to flush				*/
    sampsToFlush = (long) ( ( station->calcSecs - station->bufSecsStart ) *
	station->sampleRate + 0.5 ) + station->calcSamps -
	station->bufSampsStart;
    sampsToKeep = (long) ( ( station->bufSecsEnd - station->calcSecs ) *
	station->sampleRate + 0.5 ) + station->bufSampsEnd -
	station->calcSamps + 1;

    /*	Determine the beginning position in the buffer for data to keep	*/
    bufPtr = station->traceBuf + ( sampsToFlush * station->dataSize );

    /*	Flush the buffer						*/
    memmove( station->traceBuf, bufPtr, sampsToKeep * station->dataSize );

    /*	Update the buffer variables					*/
    station->bufSecsStart = station->calcSecs;
    station->bufSampsStart = station->calcSamps;
    station->numSamps -= sampsToFlush;
  }
  else
  {
    /*	Calculations not up to date					*/
    logit( "et", "Rsam: Unable to flush station <%s.%s.%s> trace buffer.\n\tReason: "
	"Trigger calculations not yet performed on existing data.\n",
	   station->staCode, station->compCode, station->netCode );
  }

  return ( retVal );
}
