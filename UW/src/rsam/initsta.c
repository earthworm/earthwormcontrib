/*
 * initsta.c: Initialize a station structure.
 *              1) Initialize members of the STATION structure.
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: InitializeStation					*/
/*									*/
/*	Inputs:		Pointer to a Rsam station structure	*/
/*									*/
/*	Outputs:	Updated station structure(above)		*/
/*									*/
/*	Returns:	0 on success					*/
/*			Error code as defined in rsam.h on	*/
/*			failure						*/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>
#include <string.h>	/* malloc, memset				*/

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>

/*******							*********/
/*	Rsam Includes							*/
/*******							*********/
#include "rsam.h"	

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: InitializeStation					*/
int InitializeStation( STATION* station )
{
  int	retVal = 0;	/* Return value for this function.		*/

  /*	Initialize the members of station				*/
  station->compCode[0] = '\0';
  station->netCode[0] = '\0';
  station->staCode[0] = '\0';
  station->dataType = UNKNOWN;
  station->bufSize = 0;
  
  /*	Zero numerical members						*/
  station->LTA = 0.0;
  station->LTAR = -1.0;   /* NULL value;  LTAR is normally non-negative */
  station->sampleRate = 0.0;
  station->channelNum = 0;
  station->dataSize = 0;
  station->bufSampsEnd = 0;
  station->bufSampsStart = 0;
  station->bufSecsEnd = 0;
  station->bufSecsStart = 0;
  station->calcSamps = 0;
  station->calcSecs = 0;
  station->numSamps = 0;
  station->numSSR = 0;
  
  return ( retVal );
}
