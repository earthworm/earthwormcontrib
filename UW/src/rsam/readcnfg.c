/*
 * readcnfg.c: Read the Rsam parameters from a file.
 *              1) Set members of the RSAMPARAM and WORLD structures.
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: ReadConfig						*/
/*									*/
/*	Inputs:		Pointer to a string(input filename)		*/
/*			Pointer to the Rsam World structure	*/
/*									*/
/*	Outputs:	Updated Rsam parameter structures(above)	*/
/*			Error messages to stderr			*/
/*									*/
/*	Returns:	0 on success					*/
/*			Error code as defined in rsam.h on 	*/
/*                      failure						*/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>
#include <string.h>	/* strcpy					*/

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>	/* GetKey					*/
#include <kom.h>	/* k_close, k_err, k_int, k_its, k_open, k_rd,	*/
			/*   k_str, k_val				*/

#define	MAXBUF	4	/* found in kom.c ( should move to kom.h ??? )	*/
#define NUMCTPARAMS	11	/* Number of parameters that can be set */
				/*   from the config file.		*/
#define	NUMCTREQ	9	/* Number of parameters that MUST be	*/
				/*   set from the config file.		*/

/*******							*********/
/*	Rsam Includes						*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: ReadConfig						*/
int ReadConfig( char* filename, WORLD* rsamWorld)
{
  char	filesOpen[MAXBUF][MAXFILENAMELEN];	/* Names of open files.	*/
  char	params[NUMCTPARAMS];	/* Flag for each parameter that is set.	*/
  char*	paramNames[NUMCTPARAMS];	/* Name of each parameter.	*/
  int	index;			/* Loop control variable.		*/
  int	numOpen;		/* Number of open files.		*/
  int	retVal = 0;		/* Return value for this function.	*/
  char* str;
  
  /*	Initialize the parameter names					*/
  paramNames[0] = "MyModuleId";
  paramNames[1] = "RingNameIn";
  paramNames[2] = "RingNameOut";
  paramNames[3] = "HeartBeatInterval";
  paramNames[4] = "MaxGap";
  paramNames[5] = "nSecs";
  paramNames[6] = "LogDir";
  paramNames[7] = "LogBasename";
  paramNames[8] = "Command";
  paramNames[9] = "Debug";
  paramNames[10] = "Station";
  
  /*	Initialize the parameter set flags				*/
  for ( index = 0; index < NUMCTPARAMS; index++ )
  {
    params[index] = 0;
  }

  /* The station commands are in the main config file			*/
  strcpy( rsamWorld->rsamParam.staFile, filename );

  /*	Open the main configuration file				*/
  if ( 0 == ( numOpen = k_open( filename ) ) )
  {
    fprintf( stderr, "Rsam: Error opening configuration file '%s'.\n",
	filename );
    retVal = ERR_CONFIG_OPEN;
  }
  else
  {
    /*	Remember the name of the current file				*/
    strcpy( filesOpen[0], filename );

    /*	Continue processing as long as there are open files		*/
    while ( numOpen > 0 )
    {
      /*	Continue reading from the current open file		*/
      while ( k_rd( ) )
      {
	char*	token;	/* Next token read from the configuration file.	*/
	char*	value;	/* Value associated with the token.		*/
	int	result;	/* Return code from function calls.		*/

	/*	Retrieve the first token from the current line		*/
	token = k_str( );

	if ( ! token )
	  /*	Ignore blank lines					*/
	  continue;

	if ( '#' == token[0] )
	  /*	Ignore comment lines					*/
	  continue;
	
	if ( '@' == token[0] )
	{
	  /*	Open a nested configuration file			*/
	  result = k_open( &(token[1]) );
	  if ( result != numOpen + 1 )
	  {
	    fprintf( stderr,
		"Rsam: Error opening nested configuration file '%s'.\n",
		&(token[1]) );
	    numOpen = 0;
	    retVal = ERR_CONFIG_OPEN;
	    break;
	  }
	  else
	  {
	    /*	Remember the name of the current file			*/
	    strcpy( filesOpen[numOpen], &(token[1]) );

	    numOpen++;
	    continue;
	  }
	}

	/*	Look for a configuration parameter - required first	*/
	if ( k_its( paramNames[0] ) )
	{
	  /*	Read the MyModuleId value				*/
	  if ( value = k_str( ) )
	  {
	    strcpy( rsamWorld->rsamParam.myModName, value );
	    params[0] = 1;
	  }
	}
	else if ( k_its( paramNames[1] ) )
	{
	  /*	Read the RingNameIn value				*/
	  if ( value = k_str( ) )
	  {
	    strcpy( rsamWorld->rsamParam.ringIn, value );
	    if ( -1 == ( rsamWorld->rsamParam.ringInKey = 
			 GetKey( rsamWorld->rsamParam.ringIn ) ) )
	    {
	      fprintf( stderr, "Rsam: Error finding key for ring '%s'.\n",
			rsamWorld->rsamParam.ringIn );
	      numOpen = 0;
	      retVal = ERR_CONFIG_READ;
	      break;
	    }
	    else
	      params[1] = 1;
	  }
	}
	else if ( k_its( paramNames[2] ) )
	{
	  /*	Read the RingNameOut value				*/
	  if ( value = k_str( ) )
	  {
	    strcpy( rsamWorld->rsamParam.ringOut, value );
	    if ( -1 == ( rsamWorld->rsamParam.ringOutKey = 
			 GetKey( rsamWorld->rsamParam.ringOut ) ) )
	    {
	      fprintf( stderr, "Rsam: Error finding key for ring '%s'.\n",
			rsamWorld->rsamParam.ringOut );
	      numOpen = 0;
	      retVal = ERR_CONFIG_READ;
	      break;
	    }
	    else
	      params[2] = 1;
	  }
	}
	else if ( k_its( paramNames[3] ) )
	{
	  /*	Read the HeartBeatInterval value			*/
	  rsamWorld->rsamParam.heartbeatInt = k_int( );
	  params[3] = 1;
	}
	else if ( k_its( paramNames[4] ) )
	{
	  /*	Read the MaxGap value					*/
	  rsamWorld->maxGap = k_long( );
	  params[4] = 1;
	}
	else if ( k_its( paramNames[5] ) )
	{
	  /*    Read the nSecs command 					*/
	  rsamWorld->nSecs = k_int( );
	  params[5] = 1;
	}
	else if ( k_its( paramNames[6] ) )
	{
	  /*	Read the Log Directory name				*/
	  str = k_str();
	  if ( str )
	  {
	    if ( strlen( str ) >= sizeof( rsamWorld->logDir ) -1 )
	    {
	      fprintf( stderr, "LogDir <%s> too long; ", str );
	      fprintf( stderr, "max length: %d\n", sizeof( rsamWorld->logDir) 
		       - 1 );
	      exit(-1);
	    }
	    if ( str[ strlen( str ) - 1] != '/' ) strcat( str, "/" );
	    strcpy( rsamWorld->logDir, str );
	    params[6] = 1;
	  } 
	  else 
	  {
	    fprintf( stderr, "nothing found after LogDir command\n");
	    exit(-1);
	  }
	}
	else if ( k_its( paramNames[7] ) )
	{
	  /*	Read the Log basename					*/
	  str = k_str();
	  if ( str )
	  {
	    if ( strlen( str ) >= sizeof( rsamWorld->logBase ) )
	    {
	      fprintf( stderr, "LogBasename <%s> too long; ", str );
	      fprintf( stderr, "max length: %d\n", sizeof( rsamWorld->logBase) 
		       - 1 );
	      exit(-1);
	    }
	    strcpy( rsamWorld->logBase, str );
	    params[7] = 1;
	  } 
	  else 
	  {
	    fprintf( stderr, "nothing found after LogBasename command\n");
	    exit(-1);
	  }
	}
	else if ( k_its( paramNames[8] ) )
	{
	  /*	Read the Command name					*/
	  str = k_str();
	  if ( str )
	  {
	    if ( strlen( str ) >= sizeof( rsamWorld->Command ) )
	    {
	      fprintf( stderr, "Command <%s> too long; ", str );
	      fprintf( stderr, "max length: %d\n", sizeof( rsamWorld->Command) 
		       - 1 );
	      exit(-1);
	    }
	    strcpy( rsamWorld->Command, str );
	    params[8] = 1;
	  } 
	  else 
	  {
	    fprintf( stderr, "nothing found after Command\n");
	    exit(-1);
	  }
	}
	else if ( k_its( paramNames[9] ) )
	{
	  /*	Read the Debug value					*/
	  rsamWorld->rsamParam.debug = k_int( );
	  params[9] = 1;
	}
	else if ( k_its( paramNames[10] ) )
	  /* Ignore Station commands here; see readsta.c		*/
	  continue;

 	/*	Unknown parameter found					*/
	else
	{
	  fprintf( stderr, "Rsam: Unknown parameter '%s' in '%s'.\n",
		token, filesOpen[numOpen - 1] );
	}

	/*	Check for processing errors				*/
	if ( k_err( ) )
	{
	  fprintf( stderr, "Rsam: Error processing '%s' in '%s'.\n", 
		   token, filesOpen[numOpen - 1] );
	  numOpen = 0;
	  retVal = ERR_CONFIG_READ;
	  break;
	}

	/*	End of processing for current line			*/
      }

      /*	Check for read errors					*/
      if ( k_err( ) )
      {
	fprintf( stderr, "Rsam: Error reading from '%s'.\n",
		filesOpen[numOpen - 1] );
	numOpen = 0;
	retVal = ERR_CONFIG_READ;
      }
      else
      {
	/*	File read ok, close and finish reading other files	*/
	numOpen = k_close( );
      }

      /*	End of processing for current file			*/
    }

    if ( ! CT_FAILED( retVal ) )
    {
      /*	Check for required parameters that were not set		*/
      for ( index = 0; index < NUMCTREQ; index++ )
      {
	if ( ! params[index] )
	{
	  fprintf( stderr, "Rsam: Required parameter '%s' not found.\n",
		paramNames[index] );
	  retVal = ERR_CONFIG_READ;
	}
      }
    }
  }

  return ( retVal );
}
