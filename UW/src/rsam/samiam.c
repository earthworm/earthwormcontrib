/*
 * samiam.c: Output rsam results
 *              1) Log results
 *		2) Maybe run some output script
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: SamIAm						*/
/*									*/
/*	Inputs:		Pointer to the World structure			*/
/*									*/
/*	Outputs:	Log file					*/
/*									*/
/*	Returns:	0 on success					*/
/*			Error code as defined in rsam.h on       */
/*                      failure	                                        */

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>
#include <string.h>	/* strcat, strcmp, strlen			*/
#include <stdlib.h>     /* getenv					*/
#include <sys/types.h>

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>	/* logit					*/
#include <transport.h>	/* MSG_LOGO, SHM_INFO				*/

/*******							*********/
/*	Rsam Includes							*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

static FILE  *logP = ( FILE* ) NULL;
static char  date_prev[7] = "";
static char  logName[200];

/*	Function: SamIAm						*/
int SamIAm( WORLD* rsamWorld )
{
  char         date[7];
  char         *logpath;
  char         staLine[MAXLINELEN];	/* Station output line.		*/
  int          retVal = 0;	/* Return value for this function.	*/
  struct tm    res;
  int          i;
  

  /* Write a new log file for a new day 				*/
  gmtime_ew( &(rsamWorld->lastLog), &res );
  res.tm_year %= 100;
  sprintf( date, "%02d%02d%02d", res.tm_year, res.tm_mon+1,
	   res.tm_mday );
  if (strcmp( date, date_prev ) != 0 )
  {
    if ( logP != (FILE*) NULL )
    {
      fclose( logP );
    }
    
    sprintf( logName, "%s%s.%s", rsamWorld->logDir, rsamWorld->logBase, date );
    logP = fopen( logName, "a" );
    if ( logP == NULL )
    {
      logit( "et", "Rsam: Error opening data file <rsam%s>.\n", date );
      return ( ERR_LOG );
    }
    strcpy( date_prev, date );
    
    /* Write some headers */
    fprintf( logP, "       " );
    for ( i = 0; i < rsamWorld->nSta; i++ )
      fprintf( logP, "%8s", 
	       rsamWorld->stations[rsamWorld->staIndex[i]].staCode );
    fprintf( logP, "\nHHMMSS ");
    for ( i = 0; i < rsamWorld->nSta; i++ )
      fprintf( logP, "%8s", 
	       rsamWorld->stations[rsamWorld->staIndex[i]].compCode );
    fprintf( logP, "\n" );
  }
  
  fprintf( logP, "%02d%02d%02d ", res.tm_hour, res.tm_min, res.tm_sec );
  for ( i = 0; i < rsamWorld->nSta; i++ )
  {
    fprintf( logP, "%8d", (long) 
	     rsamWorld->stations[rsamWorld->staIndex[i]].LTAR );
    rsamWorld->stations[rsamWorld->staIndex[i]].LTAR = -1.0;
  }
  fprintf( logP, "\n" );
  fflush( logP );

  return ( retVal );
}
