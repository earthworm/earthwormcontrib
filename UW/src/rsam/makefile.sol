GLOBALFLAGS=-D_SPARC -D_SOLARIS  -I../../include
CFLAGS=${GLOBALFLAGS} -g

B = ../../bin
L = ../../lib

CTOBJS = addtrace.o \
	 rsam.o \
         cmprscn.o \
	 findsta.o \
	 flushbuf.o \
	 initpars.o \
	 initsta.o \
	 intrpgap.o \
	 protrace.o \
	 readcnfg.o \
	 readewh.o \
	 readstas.o \
	 resetsta.o \
         samiam.o \
	 statrpt.o \
	 updtsta.o \
	 $L/chron3.o \
	 $L/getutil.o \
	 $L/kom.o \
	 $L/logit.o \
	 $L/sleep_ew.o \
	 $L/swap.o \
	 $L/time_ew.o \
	 $L/transport.o

LINTS = addtrace.ln \
	 rsam.ln \
         cmprscn.ln \
	 findsta.ln \
	 flushbuf.ln \
	 initpars.ln \
	 initsta.ln \
	 intrpgap.ln \
	 protrace.ln \
	 readcnfg.ln \
	 readewh.ln \
	 readstas.ln \
	 resetsta.ln \
         samiam.ln \
	 statrpt.ln \
	 updtsta.ln

rsam: $(CTOBJS)
	cc -g -o rsam $(CTOBJS) -lm -lposix4

.c.o:
	$(CC) $(CFLAGS) $(CPPFLAGS) -c  $(OUTPUT_OPTION) $<

clean:
	-rm *.o rsam

.c.ln:
	lint -Nlevel=4 -Ncheck=%all -errchk=%all $(CFLAGS) $(CPPFLAGS) -c $(OUTPUT_OPTION) $<

lint: $(LINTS)
	lint -Nlevel=4 -Ncheck=%all -errchk=%all *.ln
