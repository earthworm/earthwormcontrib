/*
 * findsta.c: Find a station in a station list.
 *              1) Search the sorted station list using bsearch 
 *                   for a station with the given station, component, 
 *                   and network codes.
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: FindStation						*/
/*									*/
/*	Inputs:		Pointer to a string(station code)		*/
/*			Pointer to a string(component code)		*/
/*			Pointer to a string(network code)		*/
/*			Pointer to the World structure			*/
/*									*/
/*	Outputs:	Pointer to the station of interest or not found	*/
/*									*/
/*	Returns:	Valid pointer to a station structure on success	*/
/*			NULL pointer on failure				*/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>
#include <string.h>	/* strcmp					*/
#include <stdlib.h>	/* bsearch					*/

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/

/*******							*********/
/*	Rsam Includes							*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: FindStation						*/
STATION* FindStation( char* staCode, char* compCode, char* netCode,
			WORLD* rsamWorld )
{
  STATION       key;            /* Key for searching			*/
  STATION*	retSta = NULL;	/* Return value for this function.	*/

  /* Point the key items toward that for which we are searching		*/
  strcpy( key.staCode, staCode );
  strcpy( key.compCode, compCode );
  strcpy( key.netCode, netCode );
  
  /* Find it */
  retSta = (STATION *) bsearch(&key, rsamWorld->stations, 
			       rsamWorld->nSta,
			       sizeof(STATION), CompareSCNs );
  
  return ( retSta );
}
