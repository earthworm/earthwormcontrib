/*
 * updtsta.c: Update a station's STA/LTA and trigger information
 *              1) Check the trigger status of the station through the
 *		     station's current end time.
 *
 */

/*******							*********/
/*	Functions defined in this source file				*/
/*******							*********/

/*	Function: UpdateStation						*/
/*									*/
/*	Inputs:		Pointer to a Rsam station structure		*/
/*			Pointer to a Rsam network structure		*/
/*									*/
/*	Outputs:	Updated station structure			*/
/*			Updated subnet structures(via the station)	*/
/*									*/
/*	Returns:	0 on success					*/
/*			Error code as defined in rsam.h on 		*/
/*                      failure						*/

#ifdef _OS2
#define INCL_DOSMEMMGR
#define INCL_DOSSEMAPHORES
#include <os2.h>
#endif

/*******							*********/
/*	System Includes							*/
/*******							*********/
#include <stdio.h>
#include <stdlib.h>	/* free, malloc					*/
#include <math.h>	/* fabs						*/

/*******							*********/
/*	Earthworm Includes						*/
/*******							*********/
#include <earthworm.h>	/* logit					*/

/*******							*********/
/*	Rsam Includes							*/
/*******							*********/
#include "rsam.h"

/*******                                                        *********/
/*      Function definitions                                            */
/*******                                                        *********/

/*	Function: UpdateStation						*/
int UpdateStation( STATION* station, WORLD* rsamWorld )
{
  char*		bufPtr;		/* Pointer to a trace data point in the	*/
				/*    buffer.				*/
  short*	shortPtr;
  long*		longPtr;
  float*	floatPtr;
  double*	doublePtr;
  double	sumSamps;	/* Sum of the samples in nSecs period.	*/
  double	sumSampsR;	/* Sum of the samples in nSecs period	*/
  /*   				     (rectified).			*/
  int		retVal = 0;	/* Return value for this function.	*/
  long		addends;	/* Number of samples used in the STA.	*/
  long		counter;	/* Loop counter.			*/
  long		aveSamps;	/* Number of tracedata points for 	*/
				/*   average 				*/
  time_t        timeNow;
  /*   needed to do one STA calculation.	*/
  long		newSamps;	/* Number of new trace data points.	*/
  

  /*	Determine the amount of "new" data that has not been used	*/
  newSamps = (long) ( ( station->bufSecsEnd - station->calcSecs ) *
		      station->sampleRate + 0.5 ) + station->bufSampsEnd -
    station->calcSamps + 1;

  /*	We need nSecs seconds of new samples to do one calculation	*/
  aveSamps = (long) ( station->sampleRate * rsamWorld->nSecs + 0.5 );
  if ( aveSamps )
  {
    while ( newSamps >= aveSamps )
    {
      /*	Calculate this station's average over a	delta-t period	*/
      bufPtr = station->traceBuf + 
	( ( (long) ( ( station->calcSecs - station->bufSecsStart ) * 
		     station->sampleRate + 0.5 ) +
	    station->calcSamps - station->bufSampsStart ) * station->dataSize );
      /* Straight average */
      sumSamps = 0.0;
      switch ( station->dataType )
      {
      case CT_SHORT:
	shortPtr = ( short *) bufPtr;
	for ( counter = 0, addends = 0; counter < aveSamps;
	      counter ++, addends++, shortPtr++ )
	  sumSamps += *shortPtr;
	break;
      case CT_LONG:
	longPtr = ( long *) bufPtr;
	for ( counter = 0, addends = 0; counter < aveSamps;
	      counter++, addends++, longPtr++ )
	  sumSamps += *longPtr;
	break;
      case CT_FLOAT:
	floatPtr = ( float *) bufPtr;
	for ( counter = 0, addends = 0; counter < aveSamps;
	      counter++, addends++, floatPtr++ )
	  sumSamps += *floatPtr;
	break;
      case CT_DOUBLE:
	doublePtr = ( double *) bufPtr;
	for ( counter = 0, addends = 0; counter < aveSamps;
	      counter++, addends++, doublePtr++ )
	  sumSamps += *doublePtr;
	break;
	default:
	  logit( "et", "Rsam: Unknown data type in UpdateStation.\n" );
	  /*	Quit now so only one message is printed			*/
	  return( ERR_UNKNOWN );
      }
      newSamps -= counter;
      
      station->LTA = sumSamps / addends;

      /*	Calculate the rectified averages over the same period	*/
      sumSampsR = 0.0;
      switch ( station->dataType )
      {
      case CT_SHORT:
	shortPtr = ( short *) bufPtr;
	for ( counter = 0; counter < aveSamps; counter++,
		shortPtr++ )
	  sumSampsR += fabs( *shortPtr - station->LTA );
	break;
      case CT_LONG:
	longPtr = ( long *) bufPtr;
	for ( counter = 0; counter < aveSamps; counter++,
		longPtr++ )
	  sumSampsR += fabs( *longPtr - station->LTA );
	break;
      case CT_FLOAT:
	floatPtr = ( float *) bufPtr;
	for ( counter = 0; counter < aveSamps; counter++,
		floatPtr++ )
	  sumSampsR += fabs( *floatPtr - station->LTA );
	break;
      case CT_DOUBLE:
	doublePtr = ( double *) bufPtr;
	for ( counter = 0; counter < aveSamps; counter++,
		doublePtr++ )
	  sumSampsR += fabs( *doublePtr - station->LTA );
	break;
      default:
	logit( "et", "Rsam: Unknown data type in UpdateStation.\n" );
	/*	Quit now so only one message is printed			*/
	return( ERR_UNKNOWN );
      }

      /*	Determine the rectified average for this delta-t	*/
      station->LTAR = sumSampsR / addends;

      /*	Update the station parameters				*/
      station->calcSamps += counter;
      while ( station->calcSamps >= (long) ( station->sampleRate + 0.5 ) )
      {
	station->calcSecs++;
	station->calcSamps -= (long) ( station->sampleRate + 0.5 );
      }
      station->numSSR += counter;

      if ( rsamWorld->rsamParam.debug > 2 )
      {
        logit( "t", "Station: (%s.%s.%s)\n", station->staCode,
	       station->compCode, station->netCode );
        logit( "", "\tSample Sum: %lf  Num: %d\n", sumSamps, addends );
        logit( "", "\tLTA: %lf\n", station->LTA );
        logit( "", "\tSample SumR: %lf\n", sumSampsR );
        logit( "", "\tLTAR: %lf\n", station->LTAR );
        logit( "", "\tCalcSecs: %ld\n", station->calcSecs );
        logit( "", "\tCalcSamps: %ld\n", station->calcSamps );
        logit( "", "\tSSR: %ld\n", station->numSSR );
      }

      /* See if it's time to log some results */
      if ( strcmp( station->staCode, rsamWorld->stations[0].staCode ) == 0 )
      { 
 	timeNow = time( NULL );
	rsamWorld->lastLog = timeNow;
	retVal = SamIAm( rsamWorld );
      }
    }
  }

  return ( retVal );
}
