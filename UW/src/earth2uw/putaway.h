/*
The prototypes for the Put Away routines
called by xxtrace_save to process trace
snippets.
*/

int PA_init();
int PA_next_ev( TRACE_REQ* , int, int );
int PA_next( TRACE_REQ* , int );
int PA_end_ev();
int PA_close();

/* Simple Definitions
 ********************/
#define RETURN_SUCCESS  0
#define RETURN_FAILURE -1 

