/* earth2uw.c : 
 * Cloned from dbtrace_save (which was written by Alex Bittenbinder). 
 * reads event files and writes specified trace snippets to UW trace files

 * Changed 9 Jan 1998 to use ws_clientII routines; PNL

 * 4/3/98 Fixed hostname lookup and output directory problems: PNL

 * 5/5/98 Corrected reading of end-time in T-line: PNL

This is the command-line driven front end for earth2uw, in contrast to the
earthworm module db_tracesave. Event files such as UW2-format pick files or
command arguments can be given to generate a simplified version of the
TYPE_TRIG message in memory. Several options may also be set from the command
line.

It uses SnippetMaker() from Alex Bittenbinder to handle the event message in 
the same way as earthworm module. Because of the different message format, 
the parsing routine is modified as uw_parse_trig.c

For output as UW2-format data files, SnippetMaker() is connected to the 
PutAway routines in uwputaway.c. Other routines could be developed for
different output such as SAC, etc.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <earthworm.h>
#include <kom.h>
#include <trace_buf.h>
#include <ws_clientII.h>
#include "parse_trig.h"
#include "putaway.h"

#ifndef INADDR_NONE
#define INADDR_NONE  0xffffffff  /* should be in <netinet/in.h> */
#endif

/* Defines 
 *********/
#define MAX_STR		 255
#define MAXLINE		1000
#define MAX_NAME_LEN	  50
#define MAXCOLS		 100
#define MAXTXT           150
#define MAX_WAVESERVERS   10
#define MAX_ADRLEN        20
#define SERVERFILE      "/wormdata/run/params/servers"

/* Functions in this source file 
 *******************************/
char *MakeTrigMsg( char *, char *, char * );
char *ReadTrigFile( FILE *, char * );

void Add2ServerList( char * );
int MakeServerListFromFile( char * );
int SnippetMaker( char *, int * );
void usage( void );
        
/* Globals to set from configuration file
 ****************************************/
long	       wsTimeout = 30;	    /* seconds to wait for reply from ws */
long	       MaxTraces = 512;	    /* max traces per message we'll ever deal with  */
long	       TravelTimeout = 30;  /* seconds to wait for wave propogation */
int	       numserv;		    /* number of wave servers we know about */
char	       wsIp[MAX_WAVESERVERS][MAX_ADRLEN];
char	       wsPort[MAX_WAVESERVERS][MAX_ADRLEN];
int            Debug;
double         GapThresh = 20.0;    /* number of sample periods to declare a gap */
int            Urgent = 0;          /* =1: we're in a hurry, don't dawdle */
int            WriteNet = 0;        /* =1 to put network code in chid field */

/* Things to be malloc'd
 ***********************/
/* story: Some of these things don't have to be global; they are only
so that we can malloc them from main at startup time. Having them 
malloc'd by the routine which uses them presents the danger that the 
module would start ok, but die (if unable to malloc) only when the 
first event hits. Which could be much later, at a bad time...*/

char* TraceBuffer;		/* where we store the trace snippet  */
char* OutBuffer;                /* storage before writing output file */
long TraceBufferLen = 16000;	/* kilobytes of largest snippet we're up for - 
				 * about 1 hour of Terra data */
long OutBufferLen = 15000;      /* kilobytes of largest trace we'll save */

TRACE_REQ*	TraceReq; 	/* request forms for wave server. 
				 * Malloc'd at startup. From ws_client.h */
extern char OutFile[MAXTXT] = ""; /* Start will a null string */
extern char OutDir[MAXTXT] = "";
int have_outfile = 0;

main( int argc, char **argv )
{
  extern char *optarg;
  extern int optind, opterr, optopt;
  int c, ret, i, args_remaining;
  int default_servers = 1;
  int have_input = 0;
  char *trigfilename, *addr, *port, *temp;
  char filename[MAX_NAME_LEN];
  FILE *trigfile = NULL;
  char *Msg;
  int count;

  /* Zero the wave server arrays 
  ******************************/
  for (i=0; i< MAX_WAVESERVERS; i++)
    {
      memset( wsIp[i], 0, MAX_ADRLEN);
      memset( wsPort[i], 0, MAX_ADRLEN);
    }
  /* Initialize the logit system; turn off file logging */
  logit_init(argv[0], 0, MAXLINE, 0);

  /* Allocate the trace snippet buffer
  ***********************************/
  TraceBufferLen *= 1000;   /* from kilobytes to bytes */
  if ( (TraceBuffer = malloc( (size_t)TraceBufferLen)) == NULL )
    {
      logit("e","EARTH2UW: Can't allocate snippet buffer of %ld bytes. Exitting\n",
	    TraceBufferLen);
      exit(-1);
    }

  /* Allocate the output buffer
  ***********************************/
  OutBufferLen *= 1000;   /* from kilobytes to bytes */
  if ( (OutBuffer = malloc( (size_t)OutBufferLen)) == NULL )
    {
      logit("e","EARTH2UW: Can't allocate output buffer of %ld bytes. Exitting\n",
	    OutBufferLen);
      exit(-1);
    }

  /* Allocate the trace request structures 
  ***************************************/
  if ( (TraceReq = calloc( (size_t)MaxTraces, (size_t)sizeof(TRACE_REQ) ) )  == NULL )
    {
      logit("e","EARTH2UW: Can't allocate %ld trace request structures. Exitting\n",
	    MaxTraces);
      exit(-1);
    }

  /* process command line arguments 
  *********************************/
  while (( c = getopt(argc, argv, "d:e:g:h?o:s:S:t:T:v")) != EOF) 
    {
      switch(c) 
	{
	case 'd':             /* output directory path */
	  if (strlen(optarg) < MAXTXT )
	    {
	      strcpy(OutDir, optarg);
	    } 
	  else 
	    {
	      logit("e", "EARTH2UW: directory name too long; max is %d\n", 
		    MAXTXT - 1 );
	      exit( 1 );
	    }
	  break;
	case 'e':          /* event file */
	  trigfilename = optarg;
	  trigfile = fopen( trigfilename, "r" );
	  if (trigfile == NULL) 
	    {
	      logit("e", "EARTH2UW: error opening trigger file <%s>\n", 
		    trigfilename );
	      logit("e", "\t%s\n", strerror(errno));
	      exit( 1 );
	    }
	  have_input = 1;
	  break;
	case 'g':           /* Gap threshhold */
	  sscanf(optarg, "%f", &GapThresh);
	  break;
	case 'n':           /* put network code in chid field */
	  WriteNet = 1;
	  break;
	case 'o':           /* output file name */
	  if (strlen(optarg) > MAX_NAME_LEN - 1) 
	    {
	      logit("e", "EARTH2UW: output file name too long; max is $d\n",
		    MAX_NAME_LEN - 1);
	      exit( 1 );
	    }
	  strcpy(filename, optarg);
	  have_outfile = 1;
	  break;
	case 's':          /* server,port */
	  Add2ServerList( optarg );
	  default_servers = 0;
	  break;
	case 'S':          /* server file */
	  MakeServerListFromFile( optarg );
	  default_servers = 0;
	  break;
	case 't':          /* wave_server timeout */
	  sscanf(optarg, "%d", &wsTimeout);
	  break;
	case 'T':          /* wave travel timeout */
	  sscanf(optarg, "%d", &TravelTimeout);
	  break;
	case 'u':          /* urgent request; don't dawdle */
	  Urgent = 1;
	  break;
	case 'v':          /* verbose output */
	  Debug = 1;
	  break;
	case 'h':          /* help */
	case '?':
	  usage();         /* never returns */
	  break;
	}
    }
  args_remaining = argc - optind;

  if (default_servers) MakeServerListFromFile(SERVERFILE);
  if (numserv == 0) 
    {
      logit("e", "EARTH2UW: no servers given; exitting!\n");
      exit( 1 );
    }

  if (have_input && !have_outfile) 
    { /* if we have a trigger filename but no output filename, use
       * the trigger filename as the output filename */
      
      /* Look for '/' in trigfilename; only put the filename in OutFile */
      if ( (temp = strrchr(trigfilename, '/')) != NULL ) 
	{
	  strcpy(OutFile, temp+1);
	} 
      else
	{
	  strcpy(OutFile, trigfilename);
	}
      OutFile[MAX_NAME_LEN -1] = '\0';  /* make sure it's terminated */
      OutFile[strlen(OutFile) - 1] = 'W';
      have_outfile = 1;
    }

  if (args_remaining == 0 && have_input == 0) trigfile = stdin;
  
  /* Read the trigger information from file or stdin */
  /* If we don't have an output filename yet, it will be generated here */
  if (trigfile != NULL) 
      Msg = ReadTrigFile( trigfile, OutFile);
  else if (args_remaining == 2) 
      Msg = MakeTrigMsg( argv[optind], argv[optind + 1], OutFile );
  else 
      usage();

  if (Msg == NULL) 
    {
      logit("e", "EARTH2UW: no trigger message found; exitting\n");
      exit( 1 );
    }
  
  /*  setWsClient_ewDebug( 1 ); *//* DEBUG */
  /*  setSocket_ewDebug( 1 ); *//* DEBUG */
  
  /* ws_clientII and socket wrappers want timeouts in milliseconds */
  wsTimeout *= 1000;
  
  /* Initialize our putaway system */
  ret = PA_init();
  if ( ret < 0 )
    {
      printf("EARTH2UW: unable to initialize\n");
      exit( 1 );
    }
  
  /* Now go do the real work */
  ret = SnippetMaker( Msg, &count );
  
  if (ret >= 0 ) 
    {
      printf("EARTH2UW: %s made with %d traces, %d errors.\n", OutFile, count,
	     ret );
      ret = PA_close();
      exit( 0 );
    } 
  else 
    {
      printf("EARTH2UW: errors encountered; unable to make trace file\n");
      ret = PA_close();
      exit( 1 );
    }
}
/*--------------------------- end of main() ----------------------------*/

/*****************************************************
 * Add server addresses and port numbers to the list *
 * Servers can be domain names or IP addresses.      *
 *****************************************************/
int MakeServerListFromFile( char *filename )
{
  int nfiles;
  char address[MAXTXT];
  char *str;
  struct hostent  *host;
  struct in_addr addr_s;

  nfiles = k_open( filename );
  if (nfiles == 0) 
    {
      logit("e", "EARTH2UW: unable to open server file <%s>.\n", filename );
      return( numserv );
    }
  
  while( k_rd())  /* Read next line from file */
    {
      if ( numserv >= MAX_WAVESERVERS )
	{
	  logit("e",
		"EARTH2UW: too many serv in server list;");
	  logit("e",
		" using %d; ignoring others\n", MAX_WAVESERVERS );
	  nfiles = k_close();
	  return ( numserv );
	}
      str = k_str();   /* get first token from line */
      
      /* Ignore blank lines and comments */
      if ( !str) continue;
      if ( str[0] == '#' ) continue;
      
      /* Assume we have an IP address and try to convert to network format.
       * If that fails, assume a domain name and look up its IP address.
       * Can't trust reverse name lookup, since some place may not have their
       * name server properly configured. 
       */
      if ( inet_addr(str) == INADDR_NONE )
	{       /* it's not a dotted quad IP address */
	  if ( (host = gethostbyname(str)) == NULL) 
	    {
	      logit("e", "EARTH2UW: bad server address <%s>\n", str );
	      continue;
	    }
	  bcopy(host->h_addr, (caddr_t) &addr_s, host->h_length);
	  str = inet_ntoa(addr_s);
	}
      strcpy( address, str );
      
      /* now the port number */
      str = k_str();
      if ( isdigit(str[0]) ) 
	{    /* perhaps a port number */
	  if (strlen(address) > 15 || strlen(str) > 5) 
	    {
	      logit("e", "EARTH2UW: ignoring dubious server <%s:%s>\n", 
		    address, str);
	      continue;
	    }
	  strcpy( wsIp[numserv], address );
	  strcpy( wsPort[numserv], str );
	  numserv++;
	} 
      else 
	{                   /* ooops! */
	  logit("e", "EARTH2UW: bad server:port numbers <%s:%s>.\n", address, 
		str );
	  continue;
	}
    }
  
  nfiles = k_close();
  return ( numserv );  /* number of server - port combinations we read */
}

/******************************************************************
 * Parse server address and port number in format "address:port"; *
 * add them to the server lists.                                  *
 ******************************************************************/
void Add2ServerList( char * addr_port )
{
  char c;
  char addr[MAXTXT], port[MAX_ADRLEN];
  char *addr_p;
  int is_port = 0;   /* flag to say we are looking at the port number */
  int i = 0;
  int a = 0, p = 0;
  struct hostent  *host;
  struct in_addr addr_s;

  /* scan the input string for format address:port */
  while ((c = addr_port[i++]) != (char) 0 ) 
    {
      if (a == MAXTXT) 
	{
	  logit("e", "EARTH2UW: hostname <%s> too long; max is %d\n", addr, 
		MAXTXT - 1);
	  return;
	}
      if ( is_port == 0 ) 
	{
	  if ( c == ':') 
	    {   /* end of address, start of port# */
	      addr[a++] = '\0';
	      is_port = 1;
	      continue;
	    } 
	  else 
	    {    /* a hostname or IP address */
	      addr[a++] = c;
	      continue;
	    }
	} 
      else 
	{     /* looking at port number */
	  if ( isdigit(c) ) 
	    {   /* looks ok for port number */
	      port[p++] = c;
	      continue;
	    } 
	  else 
	    {              /* oops! Something's wrong! */
	      logit("e", "EARTH2UW: bad server address:port <%s>.\n", 
		    addr_port );
	      return;
	    }
	}   /* if is_port  */
    }     /* while(1)  */
  port[p++] = '\0';

  /* Assume we have an IP address and try to convert to network format.
   * If that fails, assume a domain name and look up its IP address.
   * Can't trust reverse name lookup, since some place may not have their
   * name server properly configured. 
   */
  addr_p = addr;

  if ( inet_addr(addr) == INADDR_NONE )
    {        /* it's not a dotted quad address */
      if ( (host = gethostbyname(addr)) == NULL)
	{
	  logit("e", "EARTH2UW: bad server address <%s>\n", addr );
	  return;
	}
      bcopy(host->h_addr, (caddr_t) &addr_s, host->h_length);
      addr_p = inet_ntoa(addr_s);
    }
  
  if (strlen(addr_p) > 15 || strlen(port) > 5) 
    {
      logit("e", "EARTH2UW: ignoring dubious server <%s:%s>\n", addr_p,
	    port);
      return;
    }
  strcpy( wsIp[numserv], addr_p );
  strcpy( wsPort[numserv], port );
  numserv++;

  return;
}

/********************************************************************** *
 * Make a trigger message for UW stations given start time and duration *
 ************************************************************************/
char *MakeTrigMsg( char *st_p, char *duration, char *outfile)
{
  char starttime[20];
  static char msg[MAXCOLS];
  int i, len;
  
  len = strlen(st_p);
  for (i = 0; i < len; i++) 
    {
      if ( !(isdigit(st_p[i])) ) 
	{
	  logit("e", "EARTH2UW: starttime <%s> not in YYMMDDHHMMSS format\n", 
		st_p);
	  return NULL;
	}
    }
  if (len > 12) 
    {
      logit("e", "EARTH2UW: starttime <%s> not in YYMMDDHHMMSS format\n", 
	    st_p);
      return NULL;
    }
  strcpy(starttime, st_p);
  while (len < 11) starttime[len++] = '0';
  starttime[len] = '\0';
  
  for (i = 0; i < strlen(duration); i++) 
    {
      if ( !(isdigit(duration[i])) ) 
	{
	  logit("e", "EARTH2UW: duration <%s> not understood\n", duration);
	  return NULL;
	}
    }
  
  sprintf( msg, "*  *  * %s %s\n", starttime, duration );
  if (have_outfile == 0) 
    {
      strcat(outfile, starttime);
      strcat(outfile, "W");
      have_outfile = 1;
    }
  return msg;
}

/*********************
 * advertise ourself *
 *********************/
void usage( void )
{
  logit("e", "usage: earth2uw [options] [starttime duration]\n");
  logit("e", "options: -e eventfile         pickfile or triggerfile\n");
  logit("e", "         -d directory         directory for output file\n");
  logit("e", "         -g gap               gap threshhold\n");
  logit("e", "         -n                   put network ID in output file\n");
  logit("e", "         -o filename          output data file name\n");
  logit("e", "         -s server:port       server name, port number\n");
  logit("e", "         -S serverfile        list of server names, ports\n");
  logit("e", "         -t timeout           server timeout in seconds\n");
  logit("e", "         -T delaytime         wave travel timeout, seconds\n");
  logit("e", "         -u                   urgent; don't retry connections\n");
  logit("e", "         -v                   verbose output\n");
  logit("e", "         -h or -?             print this message\n");
  logit("e", "earth2uw will read an eventfile from standard input\n");
  logit("e", "if <starttime duration> pair or -e eventfile are not specified.\n");
  exit( 1 );
}

/**************************************************************************
 * Read a file and make a trigger message in memory.                      *
 * The file may be a UW2 pick file, which starts with 'A', the A card.    *
 * Otherwise, the file must be a trigger file with multiple lines of      *
 * format:  Station Component Network starttime duration                  *
 * Any of the first three may be '*', trreated as a wildcard.             *
 * The starttime is in format YYMMDDHHMMSS.SS of which digits on the      *
 * right may be missing and will be assumed to be zero.                   *
 * The duration is the duration in seconds.                               *
 **************************************************************************/
char *ReadTrigFile( FILE *infile, char *outfile )
{
  static char line[MAX_STR];
  char *msg;
  int msglen = 0;

  char temp[20];
  char *nxttok;
  char terminators[] = " \t";
  char *starttime;
  double time, orig;
  double dur = 120.0;      /* 2-minute duration if no T line found */
  int i, ret, delta = 0;
  int len;

  ret = (int) fgets( line, MAX_STR, infile);
  if (ret == NULL) 
    {
      if (errno == EOF) 
	{
	  logit("e", "EARTH2UW: unexpected EOF encountered reading event file.\n");
	  return NULL;
	} 
      else 
	{
	  logit("e", "EARTH2UW: error reading event file: %s\n",
		strerror(errno));
	  return NULL;
	}
    }
  if (line[0] == 'A') 
    {   /* We probably have a UW pick file */
      /* Read the reference time */
      strncpy(temp, line+2, 10);
      temp[10] = '\0';
      ret = strtimetodbl( temp, &time );
      if (ret < 0 ) 
	{
	  logit("e", "EARTH2UW: preposterous reference time in pickfile\n");
	  return NULL;
	}
      /* Is there an origin time in the A-card (relative to reference time)? 
       * If there is, we'll use it unless we find a T line */
      errno = 0;
      orig = atof(line+13);
      /* save 10 seconds before origin */
      if (errno == 0) delta = (int) orig - 10; 
      
      /* Look for a T line with time window */
      while ( fgets( line, MAX_STR, infile) != NULL ) 
	{
	  if (line[0] == 'T') 
	    {   /* found the T line */
	      nxttok = strtok(line+2, terminators);
	      if ( (nxttok == NULL) || (nxttok[0] == '_')) 
		{
		  logit("e", "EARTH2UW: no window found in pickfile.\n");
		  return NULL;
		}
	      sscanf(nxttok, "%d", &delta);
	      nxttok = strtok( (char *)NULL, terminators);
	      if ( (nxttok == NULL) || (nxttok[0] == '_')) 
		{
		  logit("e", "EARTH2UW: no window found in pickfile.\n");
		  return NULL;
		}
	      sscanf(nxttok, "%d", &ret);
	      dur = (double) ret - delta;
	    }
	}
      fclose(infile);
      
      time += (double) delta;
      sec15epoch(time, &starttime);
      
      /* make a simple trig message for all our local stations */
      /* Needed: add stations from other nets */
      sprintf(line, "*  *  *  %s,  %f\n", starttime, dur);
      
      /* Generate output file name if needed */
      if (have_outfile == 0) 
	{
	  len = strlen(outfile);
	  for (i=0; i<11; i++) outfile[len++] = starttime[i];
	  outfile[len++] = 'W';
	  outfile[len] = '\0';
	  have_outfile = 1;
	}
      return line;
      
    } 
  else 
    {    /* maybe a trigger file; take it verbatim */
      /* already have a line from input file, from above. */
      while (1) 
	{
	  len = strlen(line);
	  /* if (line[len-1] == '\n') line[--len] = '\0'; */
	  msg = realloc(msg, msglen + len);
	  if (msg == NULL) 
	    {
	      logit("e", "EARTH2UW: can't alloc for memory\n");
	      return NULL;
	    }
	  
	  if (msglen) 
	      strcat(msg, line);
	  else 
	      strcpy(msg, line);

	  msglen += len;
	  /* generate output file name if needed */
	  if (have_outfile == 0) 
	    {
	      for (i=0; i<4;i++) nxttok=strtok(line, terminators);
	      strcat(outfile, nxttok);
	      if (strlen(nxttok) < 11) 
		for (i = strlen(nxttok); i <= 11; i++) strcat(outfile, "0");
	      strcat(outfile, "W");
	      have_outfile = 1;
	    }
	  /* get another line */
	  ret = (int) fgets( line, MAX_STR, infile);
	  if (ret == NULL) 
	    {
	      if (errno == 0) 
		{  /* end of message */
		  fclose(infile);
		  return msg;
		} 
	      else 
		{
		  logit("e", "EARTH2UW: error reading event file: %s\n",
			strerror(errno));
		  return NULL;
		}
	    }
	  
	}  /* while(1) */
    }  /* if (pickfile) or (trigfile) */
}
