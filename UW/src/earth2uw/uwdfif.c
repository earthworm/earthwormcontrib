/*
Interface for UW 1/2 style seismic data files Conventions used in
this interface:  Names starting with UWDFdh.. are designated to
pass data coming from the intial header block (e.g., old style UW
format) Names starting with UWDFch.. are designated to pass data
that is channel dependent; e.g., channel length, bias, starting
offset, etc.
 */

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <errno.h>

#include "uwdfif.h"

/* DEFS from SNAPS makefiles: */
#define DEFAULT_STA_MAP "/stor/snaps/Tables/wash.sta.mapping"
#define DEFAULT_STA_REV_TABLE "/stor/snaps/Tables/rev.sta"
#define MAXSTAS 512

#ifdef sun
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
#endif

#define REV_SIGN(x) -(x)
#define LAST_CHAR(string) string[strlen(string)-1]

#ifndef MAX
#define MAX(a,b) ((a) >= (b) ? (a) : (b))
#endif /* MAX */

static FILE *readfs=NULL; /* stream pointer for read */
static FILE *newfile; /* new trace file stream (for writing) */
static char save_new_filename[50],temp_new_filename[20];
static struct masthead *mh = NULL;
static struct chhead1 *ch1 = NULL;
static struct chhead2 *ch2 = NULL, *new_chead = NULL;
static struct masthead new_mhead;
static struct chhead2 new_chead_proto;
static struct expanindex exps1;
static struct expanindex exps;
static int is_rev[MAXSTAS]; /* truth table for station reversal */

/* trace time corrections, u-sec */
static int chtime_corr[MAXSTAS], new_chtime_corr[MAXSTAS];
static int new_chtime_corr_proto;

/* Note: following table is also non-permanent hack to load the full
    station reversal table;  This should also be replaced by server */
static struct {char line[120];} complete_rev_table[200];
static int n_rev_table = 0;

/* Global variables */
static int swab_on_read;    /* swab binary data flg; set TRUE if swab */
static int swab_on_write = FALSE;
static int uw_fmt_flg; /* if 2, uw-2 format is used for reading */
static int nchannels; /* number of channels */
static int no_chans_written; /* number of new channels written */
static int no_expan_structs; /* number of expansion structures; currently 1 */
static struct {
	char old_name[8], /* UW-1 style chan name */
	new_name[8], /* UW-2 style chan name; point on ground */
	comp_flg[8], /* UW-2 style comp flag; SEED appendix */
	id[8], /* UW-2 style user selected id field */
	src[8]; /* Source of data; e.g., HWK for HAWK online acq */
    } sta_map[64];
static int nmap, def_map = -1;
static char name_for_open[128];

/*
 * Prototypes for useful local utilities
 */
#include "static_proto.h"

/* EXTERNAL UWDF FUNCTIONS BELOW HERE */

/*
Opens UW-1 or UW-2 style data files for reading.  Only one data
file at a time may be opened with the present version.  "name" is
character string that is either the name of the big "D" file in
the case of UW-1 style data, or and arbitrary data file name in
the case of UW-2 style data.  In the case of UW-1 style data, the
little "d" file name is derived from the string given.  The
environment variable USER_DATA_PATH can be used to specify a colon
separated list of directories to search for data files.  If the
file "name" cannot be opened, this routine will attempt to find
the file in the directories indicated by the search path.  Returns
TRUE (1) if successful, otherwise FALSE (0).
*/
BOOL UWDFopen_file(char *name)
{
    int i, j;
    /* char name_for_open[128]; */
    struct {int chno; int corr;} tc_struct;
    char tmpstr[512];
    const char *tmpptr;
    char *rawname;
    
    if (strcmp(name, "") == 0)
        return FALSE;

    strcpy(name_for_open, name);
    readfs = fopen(name_for_open,"r");
    if (readfs == NULL) {
	/* Look for the file in directories specified by USER_DATA_PATH */
	if ((tmpptr = getenv("USER_DATA_PATH")) != NULL) {
	    if ((rawname = strrchr(name, '/')) != NULL)
		rawname++;
	    else
		rawname = name;
	    strcpy(tmpstr, tmpptr);
	    tmpptr = strtok(tmpstr, ":");
	    while (tmpptr != NULL) {
		strcpy(name_for_open, tmpptr);
		strcat(name_for_open, "/");
		strcat(name_for_open, rawname);
		if ((readfs = fopen(name_for_open,"r")) != NULL)
		    break;
		tmpptr = strtok(NULL, ":");
	    }
	}
    }
    if (readfs == NULL) {
	/* fprintf(stderr,"can't open old file: %s\n", name); */
	return FALSE;
    }
    
    /* Create space for master header structure */
    if (mh == NULL)
        mh = (struct masthead *)malloc(sizeof(struct masthead)+10);
    if (ch1 == NULL)
        ch1 = (struct chhead1 *)malloc(sizeof(struct chhead1)*MAXSTAS);
    if (ch2 == NULL)
        ch2 = (struct chhead2 *)malloc(sizeof(struct chhead2)*MAXSTAS);
    
    /* Initialize all time corrections to 0 */
    for (i = 0; i < MAXSTAS; i++)
        chtime_corr[i] = 0.0;

    /* Read in first part of header file (muxhead structure);
    Note: Multiple reads of header structure accommodate
    possible word boundary alignment problems; e.g., Ridge does
    not allow alignment of longs across word (4 byte) boundaries */
    
    fread((char *)&mh->nchan, 2, 1, readfs);
    fread((char *)&mh->lrate, 1, 50, readfs);
    fread((char *)mh->comment, 1, 80, readfs);
    mh->comment[79] = '\0';

    /* Look at extra[2] to see if this is UW-1 or UW-2 format */
    if (mh->extra[2] == ' ' || mh->extra[2] == '1') {
        uw_fmt_flg = 1;
    } else if (mh->extra[2] == '2') { /* explicitly set to UW-2 */
        uw_fmt_flg = 2;
    } else {
        uw_fmt_flg = 1; /* default fallback */
    }

    set_swab_on_read();

    /* Swabbing is done in blocks within the masthead structure */
    if (swab_on_read) {
	lswab2((char*)&mh->nchan, 2);
	lswab4((char*)&mh->lrate, 16);
	lswab2((char*)&mh->tapenum, 24);
    }
    /* Determine the number of channels; from master header if UW-1 */
    if (uw_fmt_flg == 1) {
	nchannels = mh->nchan;
    } else { /* from end of file if UW-2 */
	fseek(readfs, -4, SEEK_END);
	fread(&no_expan_structs, 4, 1, readfs);
	if (swab_on_read)
	    lswab4((char *)&no_expan_structs, 4);
	if (no_expan_structs <= 0) {
	    fprintf(stderr,"Structure index count not >= 0\n");
	    exit(1);
	}
	fseek(readfs, -12*no_expan_structs - 4, SEEK_END);
	for (i = 0; i < no_expan_structs; ++i) {
	    fread(&exps, sizeof(exps), 1, readfs);
	    if (swab_on_read) lswab4((char *)&exps.numstructs, 8);
	    if (strncmp(exps.structag,"CH2",3) == 0) {
		exps1 = exps;
		nchannels = exps1.numstructs;
	    }
	    else if (strncmp(exps.structag,"TC2",3) == 0) {
		/* Process the time correction structures */
		fseek(readfs,exps.offset, SEEK_SET);
		for (j = 0; j < exps.numstructs; j++) {
		    fread(&tc_struct, sizeof(tc_struct), 1, readfs);
		    if (swab_on_read)
			lswab4((char *)&tc_struct, sizeof(tc_struct));
		    chtime_corr[tc_struct.chno] = tc_struct.corr;
		}
	    }
	}
    }
    
    /* Time to read channel headers */
    if (uw_fmt_flg == 1) { /* UW-1 format; read channel headers following
                              master header; stuff UW-2 channel headers */
	for (i = 0; i < nchannels; ++i) {
	    fread((ch1+i), sizeof(struct chhead1), 1, readfs);
	    strib((ch1+i)->name);
	    if (swab_on_read) lswab2((char *)&((ch1+i)->lta),6);
	    /* Stuff ch2 style channel headers for compatibility */
	    if ((j = channel_map((ch1+i)->name)) >= 0) {
		strcpy((ch2+i)->name, sta_map[j].new_name);
		strcpy((ch2+i)->compflg, sta_map[j].comp_flg);
		strcpy((ch2+i)->chid, sta_map[j].id);
		strcpy((ch2+i)->src, sta_map[j].src);
	    } else if (def_map >= 0) {
		strcpy((ch2+i)->name, sta_map[def_map].new_name);
		strcpy((ch2+i)->compflg, sta_map[def_map].comp_flg);
		strcpy((ch2+i)->chid, sta_map[def_map].id);
		strcpy((ch2+i)->src, sta_map[def_map].src);
	    } else {
		strcpy((ch2+i)->name, (ch1+i)->name);
		strcpy((ch2+i)->compflg, "");
		strcpy((ch2+i)->chid, "");
		strcpy((ch2+i)->src,"");
	    }
	    if (strcmp((ch2+i)->name, "same") == 0) {
		strcpy((ch2+i)->name, (ch1+i)->name);
	    }
	    strcpy((ch2+i)->fmt,"S");
	    (ch2+i)->chlen = mh->length;
	    (ch2+i)->lrate = mh->lrate;
	    (ch2+i)->start_lmin = mh->lmin;
	    (ch2+i)->start_lsec = mh->lsec;
	    (ch2+i)->offset = i*2*mh->length;
	    (ch2+i)->lta = (ch1+i)->lta;
	    (ch2+i)->trig = (ch1+i)->trig;
	    (ch2+i)->bias = (ch1+i)->bias;
            (ch2+i)->expan1 = 0;
            (ch2+i)->fill = 0;
	}
    } else { /* Assume UW-2 format; read chan headers rel to end of file */
	fseek(readfs,exps1.offset, SEEK_SET);
	fread(ch2, sizeof(struct chhead2), nchannels, readfs);
	for (i = 0; i < nchannels; ++i) {
	    (ch2+i)->name[7] = '\0'; /* Make sure name is terminated */
	    strib((ch2+i)->name);
	    if (swab_on_read) {
		lswab4((char *)&((ch2+i)->chlen), 24);
		lswab2((char *)&((ch2+i)->lta), 8);
	    }
	    (ch2+i)->fmt[1] = '\0';
	    (ch2+i)->compflg[3] = '\0';
	    (ch2+i)->chid[3] = '\0';
	    (ch2+i)->src[3] = '\0';
	    strib((ch2+i)->compflg); /* Strip trailing blanks for strings */
	    strib((ch2+i)->chid);
	    strib((ch2+i)->src);
	}
    }
    /* Read station table and stuff values for channels */
    get_revs(); /* get reversals; only done if reversal table loaded */
    /* If UW-1 format, close header file and open trace file */
    if (uw_fmt_flg == 1) {
	fclose(readfs);
	LAST_CHAR(name_for_open) = 'd';
	if ((readfs = fopen(name_for_open, "r")) == NULL) {
	    fprintf(stderr,"can't open new file: %s\n",name_for_open);
	    return FALSE;
	}
    /* If UW-2 format, set master header time to earliest start */
    } else if (uw_fmt_flg == 2) {
	set_earliest_starttime();
    }
    return TRUE;
}

/*
Returns the complete path name of the last data file that was
actually opened.  If no file was opened, then the returned string is null.
*/
char *UWDFfile_opened(void)
{
    return name_for_open;
}

/*
Itialize the station reversal table using the file name provided
as an argument.  If this function is NOT called, no reversal
information is used.  If the reversal table is invoked, then affected
channels are automatically reversed as they are returned via the
call to UWDFchret_trace().  May be called at any time to initialize
to a new reversal table (file).  Provision is made for a default
station reversal file.  If the function is called with a null string ("")
as an argument, then the default mode is invoked.  If the function
is called with in the default mode, then either (a) the reversal
table path is taken from the environmental variable USER_STA_REV_TABLE
if it is set, or (b) the default defined by DEFAULT_STA_REV_TABLE
is used for the path.
*/
BOOL UWDFinit_rev_table(char *name)
{
    int i;
    char line[100];
    char tmpstr[64], *tmptr;
    FILE *inp;

    if (strcmp(name, "") == 0 ) { /* default mode selected */
	if ((tmptr = getenv("USER_STA_REV_TABLE")) != NULL) {
	    strcpy(tmpstr, tmptr);
	} else if (strcmp(DEFAULT_STA_REV_TABLE, "") != 0) {
	    strcpy(tmpstr, DEFAULT_STA_REV_TABLE);
	} else {
	    fprintf(stderr, "Error in uwdfif - no default home - exit\n");
	    exit(1);
	}
    } else {
	strcpy(tmpstr, name);
    }
    if (strcmp(tmpstr, "") == 0) {
	fprintf(stderr,"ERROR - null reversal file name\n");
        return FALSE;
    }
    if((inp = fopen(tmpstr,"r")) == NULL) {
	fprintf(stderr,"ERROR - can't open reversal file: %s\n", name);
	return FALSE;
    }
    i = 0;
    while(fgets(line, 200, inp) != NULL) {
	strcpy(complete_rev_table[i].line, line);
	++i;
    }
    n_rev_table = i;
    fclose(inp);

    return TRUE;
}

/*
Initializes new file for writing.  Only one new data file at a time
may be written with the present version. "newfilename" is the
character string that provides a name to the new file.
*/
BOOL UWDFinit_for_new_write(char *newfilename)
/* The newfilename is squirreled away until file is closed */
{
    char *mktemp();
    /* Save new file name and make temporary file name for writing */
    strcpy(save_new_filename, newfilename);
    strcpy(temp_new_filename,"tempWXXXXXX");
    mktemp(temp_new_filename);
    if ((newfile = fopen(temp_new_filename,"w")) == NULL) {
	fprintf(stderr,"can't open new file %s: %s\n",temp_new_filename,
		strerror(errno));
	return FALSE;
    }
    /* Make space to save new channel headers */
    if (new_chead == NULL)
        new_chead = (struct chhead2 *)malloc(sizeof(struct chhead2)*MAXSTAS);
    /* Initialize anything in new masthead here */
    /* Initialize the channel head proto here */
    strcpy(new_chead_proto.name,"");
    new_chead_proto.lta = 0;
    new_chead_proto.trig = 0;
    new_chead_proto.bias = 0;
    new_chead_proto.chlen = 0;
    new_chead_proto.offset = 0;
    new_chead_proto.start_lmin = 0;
    new_chead_proto.start_lsec = 0;
    new_chead_proto.lrate = 0;
    strcpy(new_chead_proto.fmt,"");
    strcpy(new_chead_proto.compflg,"");
    strcpy(new_chead_proto.chid,"");
    strcpy(new_chead_proto.src,"");
    new_chead_proto.expan1 = 0;
    new_chead_proto.fill = 0;
    new_chtime_corr_proto = 0.0;
    return TRUE;
}

/*
Closes data file that was previously opened by UWDFopen_file.
Called before new data file is opened for reading.  OK to call this
if no file is open - in this case, a FALSE value is returned.
*/
BOOL UWDFclose_file(void)
{
    if (readfs != NULL) {
	fclose(readfs);
	return TRUE;
    } else {
	return FALSE; /* OK to call with no file open */
    }
}

/*
Closes new data file after writing is complete. This function takes
care of cleanup operations, and must be performed before a new file
is opened for writing.
*/
BOOL UWDFclose_new_file(void)
{
    int ntc;
    int i;
    struct {int chno; int corr;} tc_struct;

    /* Write channel headers to new file */
    exps1.offset = ftell(newfile); /* record offset for channel headers */
    fwrite(new_chead,sizeof(struct chhead2), no_chans_written,newfile);

    /* Write time corrections */
    ntc = 0;
    exps.offset = ftell(newfile); /* record offset for time corrections */
    for (i = 0; i < no_chans_written; i++)
        if (new_chtime_corr[i] != 0.0) {
            tc_struct.chno = i;
            tc_struct.corr = new_chtime_corr[i];
            fwrite(&tc_struct,sizeof(tc_struct), 1, newfile);
            ntc++;
        }

    /* Write expansion structure following the channel headers */
    strcpy(exps1.structag,"CH2");
    exps1.numstructs = no_chans_written;
    no_expan_structs = 1;
    fwrite(&exps1, sizeof(exps1), 1, newfile); /* write expansion struct */

    if (ntc > 0) {
        strcpy(exps.structag, "TC2");
        exps.numstructs = ntc;
        no_expan_structs++;
        fwrite(&exps, sizeof(exps), 1, newfile); /* write expansion struct */
    }

    /* Write number of expan structs as last integer in file */
    fwrite(&no_expan_structs, sizeof(no_expan_structs), 1, newfile);
    /* Close file, and move to correct file name */
    fclose(newfile);
    if(rename(temp_new_filename, save_new_filename)!= 0) {
	fprintf(stderr,"Error renaming new file\n");
	return FALSE;
    }
    no_chans_written = 0;
    return TRUE;
}

/* UWDF HEADER FUNCTIONS (RETURN) */

/*
Return pointer to header "comment" string in master header; maximum
length is 80 characters
*/
char *UWDFdhcomment(void)
{
    return (mh->comment);
}

/*
Return event number as integer; this is a short int in the header struct.
*/
int UWDFdhevno(void)
{
    return ((int)mh->eventnum);
}

/*
Return tape (or run) number as integer; this is a short int in the
header struct.
*/
int UWDFdhtapeno(void)
{
    return ((int)mh->tapenum);
}

/*
Return character pointer to "extra" character string in master
header; maximum length is 10 characters.
*/
char *UWDFdhext(void)
{
    return (mh->extra);
}

/*
Return pointer to array of short int "flags"; array is length 10.
*/
short int *UWDFdhflgs(void)
{
    return (mh->flg);
}

/*
Return integer value with number of channels.  This value is picked
up from the master header block for UW-1 style format, and from
the structure descriptor block for UW-2 style format.
*/
int UWDFdhnchan(void)
{
    return (nchannels);
}
/*
Return the master reference time in the structure defined by "struct
Time" (which is declared in the "uwdfif.h" header).  The structure
contains the information: yr, mon, day, hr, min, sec.  This time
is defined as the start of all traces for UW-1 style data (all
channels have the same starting time); for UW-2, it will be the
earliest start time of all traces as determined at the time the
file is read (min of the start time of all traces).
*/
BOOL UWDFdhref_stime(struct Time *time)
{
    int *tim;
    int tmpmin;
    tim = &time->yr;
    tmpmin = mh->lmin;
    time->sec = mh->lsec * 1.0e-06;
    if (time->sec >= 60.0)
        while(time->sec >= 60.0) {
            time->sec -= 60.0;
            tmpmin++;
        }
    else if (time->sec < 0.0)
        while(time->sec < 0.0) {
            time->sec += 60.0;
            tmpmin--;
        }
    mingrg(tim, &tmpmin);
    return TRUE;
}

/*
Return integer giving format type for current data file open for read.
1 is returned if UW-1 style format, 2 is returned if UW-2 style format.
*/
int UWDFdhfmt_type_for_read(void)
{
    return (uw_fmt_flg);
}

/* UWDF CHANNEL FUNCTIONS (RETURN) */

/*
This is the basic routine for returning channel data in several
formats.  "chno" is an input integer that selects the data channel
to return.  This number should be between 0 and (N-1) where N is
the total number of channels (available with another function call).
"fmt" is a character variable that indicates the format that you
want the data returned in.  "fmt" can be be 'S' for short int, 'F'
for float, and 'L' for long integer.  There can be no default, so
this character must be set with one of those values.  "seis" is a
non-specified (void) pointer to a seismogram buffer that must be
big enough to hold one trace at full (4 byte) precision.  Even if
you want to return data as short (2 byte) integers, you MUST declare
the buffer length to be long enough for the same number of sample
points as long (4 byte) integers.  It is the users responsibility
to pass a buffer of sufficient size (to hold one entire trace).
It is also the users responsibility to ensure that the routine does
not convert data from 'F' (float) or 'L' (long int) formats to 'S'
(short int), unless it is known to be safe to do so.  This conversion
(either to or from a data file) CAN RESULT IN SIGNIFICANT LOSS OF
DATA.  No partial trace option is available at present.  The
seismogram trace length in number of samples may be checked for
each trace with the function UWDFchlen(i) for the i'th channel,
and the native format of the data in the data file may be checked
channel-by-channel with the function UWDFchfmt().
*/
BOOL UWDFchret_trace(int chno, char fmt, void *seis)
{
    int i,position,len;
    short int *wkbuf;
    /* Do some integrity checking */
    if (chno < 0 || chno > nchannels-1) return FALSE;
    if ((fmt != 'S') && (fmt != 'L') && (fmt != 'F')) return FALSE;
    if (uw_fmt_flg == 1) { /* UW-1 format */
	len = mh->length;
	/* set wkbuf ptr to 2nd half of seis; works OK even if len is odd */
	wkbuf = (short int *)(((int *)seis)+len/2);
	position = ch2[chno].offset;
	fseek(readfs,position,0);
	fread(wkbuf,2,len,readfs);
	if (swab_on_read == 1) {
	    lswab2((char *)wkbuf,len*2);
	}
	if (fmt == 'L') { /* Unpack into long integers */
	    for (i = 0; i < len; ++i) ((int *)seis)[i] = (int)wkbuf[i];
	} else if (fmt == 'F') { /* Unpack into floats */
	    for (i = 0; i < len; ++i) ((float *)seis)[i] = (float)wkbuf[i];
	} else if (fmt == 'S') { /* Repack into short integers */
	    for (i = 0; i < len; ++i) {
	        ((short int *)seis)[i] = (short int)wkbuf[i];
	    }
	}
    } else { /* Assume UW-2 format */
	len = (ch2+chno)->chlen;
	position = (ch2+chno)->offset;
	fseek(readfs,position,0);
	if (*((ch2+chno)->fmt) == 'S') { /* Short int fmt */
	    wkbuf = (short int *)(((int *)seis)+(len)/2);
	    fread(wkbuf,2,len,readfs);
	    if (swab_on_read) {
		lswab2((char *)wkbuf,len*2);
	    }
	    if (fmt == 'L') { /* Unpack into long integers */
		for (i = 0; i < len; ++i) ((int *)seis)[i] = (int)wkbuf[i];
	    } else if (fmt == 'F') { /* Unpack into floats */
		for (i = 0; i < len; ++i) ((float *)seis)[i] = (float)wkbuf[i];
	    } else if (fmt == 'S') { /* Repack from start as short integers */
		for (i = 0; i < len; ++i) {
		   ((short int *)seis)[i] = (short int)wkbuf[i];
	        }
	    }
	} else if (((ch2+chno)->fmt)[0] == 'L') { /* Long int fmt */
	    fread(seis,4,len,readfs);
	    if (swab_on_read) {
		lswab4((char *)seis,len*4);
	    }
	    if (fmt == 'L') {
		;
	    } else if (fmt == 'F') { /* Convert output to float */
		for(i=0;i<len;++i) ((float *)seis)[i] = ((int *)seis)[i];
	    } else if (fmt == 'S') { /* Convert output to short */
		for(i=0;i<len;++i) ((short int *)seis)[i] = ((int *)seis)[i];
	    }
	} else if (((ch2+chno)->fmt)[0] == 'F') { /* Float fmt */
	    fread(seis,4,len,readfs);
	    if (fmt == 'F') {
		;
	    } else if (fmt == 'L') { /* Convert output to long int */
		for(i=0;i<len;++i) ((int *)seis)[i] = ((float *)seis)[i];
	    } else if (fmt == 'S') { /* Convert output to short int */
		for(i=0;i<len;++i) ((short int *)seis)[i] = ((float *)seis)[i];
	    }
	} else {
	    return FALSE;
	}
    }
    /* Do final sign reversal if needed */
    if (is_rev[chno]) {
	if (fmt == 'F') {
	    for (i = 0; i < len; ++i) ((float *)seis)[i] =
		REV_SIGN(((float *)seis)[i]);
	} else if (fmt == 'L') {
	    for (i = 0; i < len; ++i) ((int *)seis)[i] =
		REV_SIGN(((int *)seis)[i]);
	} else if (fmt == 'S') {
	    for (i = 0; i < len; ++i) ((short *)seis)[i] =
		REV_SIGN(((short *)seis)[i]);
	}
	
    }
    return TRUE;
}

/*
Return the length (number of samples or points) of the seismogram
for the channel pointed to by index "chno".  The length may vary
with each seismogram in UW-2 style format, but will be the same
for all seismograms for UW-1.  The calling software should be
written to always check the size of each trace prior to returning
the trace, and to ensure that buffer space is allocated, of the
correct type, in each case.
*/
int UWDFchlen(int chno)
{
    return ((ch2+chno)->chlen);
}

/*
Return the format type of the seismogram pointed to by index "chano".
This will be a single character with a value 'S', 'L', or 'F' for
short integer, long integer, or float type respectively.  This
function allows the application program to ascertain the native
data type for any data channel, in case, for example, the application
wants to retain the original data type.  This could be the case if
the application was merging or splitting data files.  Note that
the conversion of float or long int data types to short int format
is highly unadvisable, although the interface presently permits
this type of conversion.  Such conversion could result in loss of
significance or data distortion.
*/
char UWDFchfmt(int chno)
{
    return (((ch2+chno)->fmt)[0]);
}

/*
Return a character string pointer to the name field for specified
channel.  "chno" is the channel number.  This name is the station
name for the site (point on the ground).  There may be several
channels with the same station name, for multicomponent stations
(in UW-2 format).  Component flags and id flags should be used for
discriminating components, etc., at stations.
*/
char *UWDFchname(int chno)
{
    return ((ch2+chno)->name);
}

/*
Return the LTA (long term average) for channel specified by "chno".  The
LTA is a measure of the average RMS value of the signal over a running
window, and is used in the acquisition system triggering algorithm.
*/
int UWDFchlta(int chno)
{
    return ((int)((ch2+chno)->lta));
}

/*
Return the trigger value for the specified data channel as an int.  The
trigger value is a measure of the departure of the short term average
from the long term average, and is used in the acquisition system triggering
algorithm.
*/
int UWDFchtrig(int chno)
{
    return ((int)((ch2+chno)->trig));
}

/*
Return the bias value for the specified channel as an int.  The bias
is a running average of the DC level of the channel as it is digitized,
and is a good measure of the channel state of health.  Normally bias
values should be less than 20 or 30 counts.
*/
int UWDFchbias(int chno) 
{
    return ((int)((ch2+chno)->bias));
}

/*
Returns the reference time for the first point of channel specified
by "chno".  "time" in this case is stored in the form of the
structure "Time" defined in "uwdfif.h".  "Time" contains: yr, mon,
day, hr, min, sec.  Remember to pass the POINTER to the time
structure, since data must be returned to the calling program.
*/
BOOL UWDFchref_stime(int chno, struct Time *time)
{
    int *t;
    int tmpmin;
    t = &time->yr;
    tmpmin = (ch2+chno)->start_lmin;
    time->sec = ((ch2+chno)->start_lsec + chtime_corr[chno]) * 1.0e-06;
    if (time->sec >= 60.0)
        while(time->sec >= 60.0) {
            time->sec -= 60.0;
            tmpmin++;
        }
    else if (time->sec < 0.0)
        while(time->sec < 0.0) {
            time->sec += 60.0;
            tmpmin--;
        }
    mingrg(t, &tmpmin);
    return TRUE;
}

/*
Returns floating point value of sample rate in samples-per-second
for the specified channel number "chno".  The sample rate is assumed to
be constant over the duration of the trace.
*/
float UWDFchsrate(int chno)
{
    return(((float)(ch2+chno)->lrate)/1000.);
}

/*
Return a character pointer to the 4 length string that specifies
the component for channel "chno".  The possible strings are given
in the SEED definition manual, Appendix A.  Typically the string
is a three letter code specifying the Band Code, Source Code, and
the Orientation Code.  An example is "EHV" for extremely short-period,
high-gain, vertical component.  A null terminator exists within
the field width.
*/
char *UWDFchcompflg(int chno)
{
    return ((ch2+chno)->compflg);
}

/*
Return a character pointer to the 4 length string available to
specify channel id in more detail.  Exact use of this field is not
defined by the format, but it is available as a user defined
field to specify things such as high or low gain version of a
particular component, alternate telemetry paths, etc.  A null
terminator exists within the field width.
*/
char *UWDFchid(int chno)
{
    return ((ch2+chno)->chid);
}

/*
Return a character pointer to the 4 length string to specify
the "src" field of the channel header.  Exact use of this field
is not defined by the format, but it is intended to specify
the source of the data in a three-character code.  For example,
data from the Hawk system would use the designator: HWK for
this field.  A null terminator exists within the field width.
*/
char *UWDFchsrc(int chno)
{
    return ((ch2+chno)->src);
}

/*
Return the integer channel index (ordinal number) for the channel
characterized by the three strings: "chname", "compflg", and "chid".
The boolean switch "force_on" forces full exact comparison with
all three strings.  If no such channel exists, or a match cannot
be found, then return -1 (an illegal channel index number).  This
is the way that we can pull out a channel number by its string
specifiers.  Note: If force_on is set FALSE, then if either "compflg"
or "chid" are null strings, the search just ignores that string
comparison for the null case.  If however force_on is TRUE, then
null strings must match null strings for all three strings.  In
every case, "chname" MUST be non-null.  In the present version,
the first encounter of the correctly matching channel will be
returned, even if later matches can be made.
*/
int UWDFchno(char *chname, char *compflg, char *chid, BOOL force_on)
{
    int i;
    for (i = 0; i < nchannels; ++i) {
        if (strcmp(chname,(ch2+i)->name) == 0){
	    if ((strcmp(compflg, "") != 0) || force_on)
		if ((strcmp(compflg, (ch2+i)->compflg) != 0)) continue;
	    if ((strcmp(chid, "") != 0) || force_on)
		if ((strcmp(chid, (ch2+i)->chid) != 0)) continue;
	    return (i);
	}
    }
    return (-1); /* return -1 (illegal value) if channel not found */
}

/* UWDF NEW FILE WRITING FUNCTIONS - HEADERS */

/*
Write new master header.  This function must be the first called
after a new file has been opened (UWDFinit_for_new_write()), and
the master header has been initialized (e.g., with UWDFdup_masthead()).
Note that the new master header structure may be filled several
ways.
*/
BOOL UWDFwrite_new_head(void) 
{
    /* Determine if we are big (I for IEEE) or little (D for DEC) endian */
    int i = 1;
    char *c = (char *) &i;
    char MACHINE = (*c == 1) ? 'D': 'I';

    no_chans_written = 0;
    /* new_mhead.nchan = 65535; */
    /* new_mhead.lrate = 0; */
    /* new_mhead.lmin = 0; */
    /* new_mhead.lsec = 0; */
    new_mhead.extra[1] = MACHINE; /* Ensure that machine is correct */
    new_mhead.extra[2] = '2'; /* Set to UW-2; only style allowed for write */
    
    /* write out muxhead structure */
    fwrite((char*)&new_mhead.nchan, 2, 1, newfile);
    fwrite((char*)&new_mhead.lrate, 1, 50, newfile);
    fwrite((char*)new_mhead.comment, 1, 80, newfile);
    return TRUE;
}

/*
This function duplicates the current master header structure into
the master header structure available for writing new data file.
It may be used to simplify the process of correctly initializing
the master header.  For example, we may first duplicate the master
header from a file that has been read (this function), then change
any fields that require change by individual calls to the appropriate
functions.
that
*/ 
BOOL UWDFdup_masthead(void) 
{
    new_mhead = *mh;
    return TRUE;
}

/*
Set the tape number field in the master header.
*/
BOOL UWDFset_dhtapeno(short int new_tapenum)
{
    new_mhead.tapenum = new_tapenum;
    return TRUE;
}

/*
Set the event number in the master header
*/
BOOL UWDFset_dhevno(short int new_evno)
{
    new_mhead.eventnum = new_evno;
    return TRUE;
}

/*
Set the integer flags array in the master header.  There are 10
of elements in the flags array.  See design document for definitions
of defined elements.
*/
BOOL UWDFset_dhflgs(short int new_flags[])
{
    int i;
    for (i = 0; i < 10; ++i) new_mhead.flg[i] = new_flags[i];
    return TRUE;
}

/*
Set the character array "extra" in the master header.  "extra" is
a character array of length 10.  See design document for definitions
of defined elements.
*/
BOOL UWDFset_dhextra(char extra[])
{
    int i;
    for (i = 0; i < 10; ++i) new_mhead.extra[i] = extra[i];
    return TRUE;
}
/*
Set the comment character field in the master header.  This field is
a string of length 80 (79 + terminator) or less.
*/
BOOL UWDFset_dhcomment(char *comment)
{
    char tmpstr[80];
    strncpy(tmpstr,comment,79);
    tmpstr[79] = '\0';
    strib(tmpstr);
    strcpy(new_mhead.comment,tmpstr);
    return TRUE;
}

/*
Set the master header reference time from the time structure "time".
The structure "Time" is defined in the uwdfif.h include file.  Note
that the argument is not a pointer; the structure is passed by
value.  Note also that this value is used in UW-1 data files as the
master time reference, but it essentially ignored in UW-2 where the
reference time for each channel is carried in the channel headers.
*/
BOOL UWDFset_dhref_stime(struct Time time)
{
    int tmpmin;
    int *tim;
    tim = &time.yr;
    grgmin(tim, &tmpmin);
    if (time.sec >= 60.0)
        while(time.sec >= 60.0) {
            time.sec -= 60.0;
            tmpmin++;
        }
    else if (time.sec < 0.0)
        while(time.sec < 0.0) {
            time.sec += 60.0;
            tmpmin--;
        }
    new_mhead.lmin = tmpmin;
    new_mhead.lsec = time.sec*1.0e+06;
    return TRUE;    
}

/* UWDF NEW FILE WRITING FUNCTIONS - CHANNELS */

/*
Write next seismogram channel in sequence.  This is the basic
seismogram writing function.  "wr_fmt" is a character variable
specifying how you want the file written.  The choices are 'S' (short
int), 'L' (long int), and 'F' (float).  "len" is the length of the data
buffer in sample points, "seis" is the actual seismogram data buffer
passed to function, and "seis_fmt" is actual format of the data
that you passed to the routine (again, 'S', 'L', or 'F').  The
routine can accept any of the three formats, and will do internal
conversion as needed prior to actually writing data.  Appropriate
fields in the channel header such as the length and format type
are filled prior to writing the data.
*/
BOOL UWDFwrite_new_chan(char wr_fmt, int len, void *seis, char seis_fmt)
{
    int i;
    short int *wkbuf1;
    
    i = no_chans_written;
    new_chead_proto.fmt[0] = wr_fmt; /* Set format character */
    new_chead_proto.chlen = len; /* Set length of channel */
    new_chead_proto.offset = ftell(newfile); /* Set pointer to channel */
    /* Copy chan header proto to structure array */
    *(new_chead+i) = new_chead_proto;
    new_chtime_corr[i] = new_chtime_corr_proto;
    /* Write data to new file */
    if (wr_fmt == 'S') { /* Write data as short integers */
	if (seis_fmt == 'S' ) { /* Write straight out */
	    fwrite(seis,2,len,newfile);
	} else if (seis_fmt == 'L') { /* Need to repack into short ints */
	    /* reverse packing operation */
	    wkbuf1 = (short int *)seis; /* Set wkbuf1 to start of seis */
	    for (i = 0; i < len; ++i) { /* repack seis into 2 byte integers */
		wkbuf1[i] = ((int *)seis)[i];
	    }
	    fwrite(wkbuf1,2,len,newfile);
	} else if (seis_fmt == 'F') { /* Convert float to short int */
	    /* reverse packing operation */
	    wkbuf1 = (short int *)seis; /* Set wkbuf1 to start of seis */
	    for (i = 0; i < len; ++i) { /* repack seis into 2 byte integers */
		wkbuf1[i] = ((float *)seis)[i];
	    }
	    fwrite(wkbuf1,2,len,newfile);
	}
    } else if (wr_fmt == 'L') { /* Write data as long integers */
	if (seis_fmt == 'L') { /* Write straight out */
	    ;
	} else if (seis_fmt == 'S') { /* Need to stretch data before write */
	    for (i = len-1; i >= 0; --i) {
	        ((int *)seis)[i] = ((short int *)seis)[i];
	    }
	} else if (seis_fmt == 'F') { /* Convert float to int */
	    for (i = 0; i < len; ++i) {
		((int *)seis)[i] = ((float *)seis)[i];
	    }
	}
	fwrite(seis,sizeof(int),len,newfile);
    } else if (wr_fmt == 'F') { /* Write data as float */
	if (seis_fmt == 'F') { /* Write straight out */
	    ;
	} else if (seis_fmt == 'S') { /* Need to stretch data before write */
	    for (i = len-1; i >= 0; --i) {
	        ((float *)seis)[i] = ((short int *)seis)[i];
	    }
	} else if (seis_fmt == 'L') { /* Convert int to float */
	    for (i = 0; i < len; ++i) {
		((float *)seis)[i] = ((int *)seis)[i];
	    }
	}
	fwrite(seis,sizeof(float),len,newfile);
    } else {
	return FALSE;
    }
    ++no_chans_written;
    return TRUE;
}

/*
This function sets the channel header prototype for writing the
channel to be written next by duplicating it from the channel "chno"
of the file presently open for reading.  If you are writing a data
file from scratch, then you would not normally use this function
since there would be no currently valid data to duplicate from.
To use this function, you must have a currently valid data file
(either UW-1 or UW-2) active for reading.  Following this call,
you may reset any individual fields prior to writing.
*/
BOOL UWDFdup_chead_into_proto(int chno)
{
    new_chead_proto = *(ch2+chno);
    new_chtime_corr_proto = chtime_corr[chno];
    return TRUE;
}

/*
Set the station name for the channel to be written next.  This
string is normally 4 or fewer characters long, although the UW-2
format will accommodate up to 7 characters plus a null terminator.
It is the string specifying the "point on the ground" for the
station.  The string should be correctly null terminated.
*/
BOOL UWDFset_chname(char *chaname)
{
    char tmpname[8];
    /* Ensure proper termination of name */
    strncpy(tmpname,chaname,7);
    tmpname[7] = '\0';
    strib (tmpname);
    strcpy(new_chead_proto.name,tmpname);
    return TRUE;
}

/*
Set the component flag for the channel to be written next.  This
string is normally three characters in length, following the
convention of the SEED defining document, Appendix A.  The field
can be at most 3 characters since the format allows only 4 characters
including the null terminator.
*/
BOOL UWDFset_chcompflg(char *compflg)
{
    char tmpstr[4];
    strncpy(tmpstr,compflg,3);
    tmpstr[3] = '\0';
    strib(tmpstr);
    strcpy(new_chead_proto.compflg,tmpstr);
    return TRUE;
}

/*
Set the channel sample rate for the channel to be written next.
"new_samprate" is a float variable, with units of samples/sec.
*/
BOOL UWDFset_chsrate(float new_samprate)
{
    new_chead_proto.lrate = new_samprate*1000.;
    return TRUE;
}

/*
Set the channel reference time for the channel to be written next.
The reference time is the time of the first sample in the channel.
"time" is a "struct Time" containing: yr, mon, day, hr, min, sec.
See the Time structure declaration in "uwdfif.h".  Note that the
"time" structure is passed by value.
*/
BOOL UWDFset_chref_stime(struct Time time)
{
    int tmpmin;
    int *tim;
    tim = &time.yr;
    grgmin(tim, &tmpmin);
    if (time.sec >= 60.0)
        while(time.sec >= 60.0)
        {
            time.sec -= 60.0;
            tmpmin++;
        }
    else if (time.sec < 0.0)
        while(time.sec < 0.0)
        {
            time.sec += 60.0;
            tmpmin--;
        }
    new_chead_proto.start_lmin = tmpmin;
    new_chead_proto.start_lsec = (time.sec*1.0e+06)+0.5; /* round here */
    return TRUE;    
}

/*
Set the separate channel ID string.  This field can be of the users
definition.  It could be used for example to specify different
flavors of the same component from the same station (e.g., different
telemetry routes).  The field can be at most 3 characters long.
*/
BOOL UWDFset_chid(char *chanid)
{
    char tmpstr[4];
    strncpy(tmpstr,chanid,3);
    tmpstr[3] = '\0';
    strib(tmpstr);
    strcpy(new_chead_proto.chid,tmpstr);
    return TRUE;
}

/*
Set the separate channel source string.  This field can be of the users
definition.  It is intended to specify the source of the data; e.g.,
it would be set to HWK for data form the Hawk system.
*/
BOOL UWDFset_chsrc(char *chansrc)
{
    char tmpstr[4];
    strncpy(tmpstr,chansrc,3);
    tmpstr[3] = '\0';
    strib(tmpstr);
    strcpy(new_chead_proto.src,tmpstr);
    return TRUE;
}

/*
Set the channel "lta" variable.  This is the "long term average"
normally generated by the UW HAWK data acquisition system.  This
would not normally be reset by the user.  Since it is only a 2 byte
integer, it cannot be set to greater than about +/- 32000.  If the
application tries to set it larger, the interface sets it to zero.
*/
BOOL UWDFset_chlta(int lta)
{
    if (abs(lta) < 32100) {
        new_chead_proto.lta = lta;
    } else {
        new_chead_proto.lta = 0;
    }
    return TRUE;
}

/*
Set the channel "trig" variable.  This is a trigger parameter
normally generated by the UW HAWK data acquisition system.  This
would not normally be reset by the user.  Since it is only a 2 byte
integer, it cannot be set to greater than about +/- 32000.  If the
application tries to set it larger, the interface sets it to zero.
*/
BOOL UWDFset_chtrig(int trig)
{
    if (abs(trig) < 32100) {
        new_chead_proto.trig = trig;
    } else {
        new_chead_proto.trig = 0;
    }
    return TRUE;
}

/*
Set the channel "bias" variable.  This is a moving average parameter
normally generated by the UW HAWK data acquisition system.  This
would not normally be reset by the user.  Since it is only a 2 byte
integer, it cannot be set to greater than about +/- 32000.  If the
application tries to set it larger, the interface sets it to zero.
*/
BOOL UWDFset_chbias(int bias)
{
    if (abs(bias) < 32100) {
        new_chead_proto.bias = bias;
    } else {
        new_chead_proto.bias = 0;
    }
    return TRUE;
}

/* UWDF MISCELLANEOUS GENERAL UTILITIES */

/*
Turn on station file name mapping for reading (and writing) UW-1
style data or converting UW-1 data to UW-2 data.  This allows the
user to initialize automatic station name mapping using the specified
station mapping file.  If this function is not called, station name
mapping is not done for UW-1 type data, and original station names
are retained by default.  If station name mapping is not on when
writing new UW-2 data, then original UW-1 station names are retained
in the UW-2 style channel headers.  A default station name mapping
file is defined.  If UWDFsta_mapping_on() is called with a non-null
file name, then that name overrides any defaults and is always
used.  If UWDFsta_mapping_on() is called with a null string as the
file name, then either (a) the path defined by the environment
variable USER_STA_MAP is used for the station name mapping file if
it is defined, or (b) the default path defined by DEFAULT_STA_MAP
is used.
*/
BOOL UWDFsta_mapping_on(char *map_filename)
{
    char tmpstr[128], *tmptr;
    if (strcmp(map_filename, "") == 0) { /* null string given - use default */
	if ((tmptr = getenv("USER_STA_MAP")) != NULL) {
	    strcpy(tmpstr, tmptr);
	} else if (strcmp(DEFAULT_STA_MAP, "") != 0) {
	    strcpy(tmpstr, DEFAULT_STA_MAP);
	} else {
	    fprintf(stderr, "Error in uwdfif - default home environmental"
	    "variable not set - exit\n");
	    exit(1);
	}
    } else { /* use actual file name given as argument */
	strcpy(tmpstr, map_filename);
    }
    init_channel_map(tmpstr);
    return TRUE;
}

/*
Return the time difference in seconds between the time structure
defined by time1 and time2 (time1 - time2).  These times must be
defined by the structure "Time", which is defined in the uwdfif.h
header file.  Always use #include "uwdfif.h" to obtain the proper
structure declarations.  This is just a handy utility routine for
applications to avoid having to unfold the date-time structure.
Note that the structure arguments are passed by value.
*/
float UWDFtime_diff(struct Time time1, struct Time time2)
{
    int *tim1, *tim2;
    int lmin1, lmin2;
    tim1 = &time1.yr;
    grgmin(tim1, &lmin1);
    tim2 = &time2.yr;
    grgmin(tim2, &lmin2);
    return( (lmin1-lmin2)*60. + (time1.sec-time2.sec));
}

/*
Return the value TRUE (1) if the data format for reading (current
file) is UW-2; otherwise return FALSE (0) - currently this implies
UW-1.
*/
BOOL UWDFis_uw2(void)
{
    if (uw_fmt_flg == 2) {
	return TRUE;
    } else {
	return FALSE;
    }
}

/*
Return the maximum trace length (in samples) for current data file.
This utility is useful for allocating space for the seismogram
buffer(s).
*/
int UWDFmax_trace_len(void)
{
    int i, maxlen;
    maxlen = 0;
    for (i = 0; i < nchannels; ++i) {
	maxlen = MAX(maxlen, (ch2+i)->chlen);
    }
    return (maxlen);
}

/*
Returns pointer to the structure for the master header used for
reading.
*/
struct masthead *UWDFret_mast_struct(void)
{
    return (mh);
}

/*
Returns pointer to the channel header (UW-2 style) for the i'th
channel.
*/
struct chhead2 *UWDFret_chead_struct(int i)
{
    return ((ch2+i));
}

/*
Returns pointer to the prototype channel header structure used
for writing.
*/
struct chhead2 *UWDFret_chead_proto_struct(void)
{
    return (&new_chead_proto);
}

/*
Returns pointer to the master header used for writing.
*/
struct masthead *UWDFret_new_mhead_struct(void)
{
    return (&new_mhead);
}

/*
Time conversion utility to convert from month & day time to
day-of-year.  Input variables are year (yr), month (mon), and
day-of-month (dom);  output is day-of-year (doy).
*/
BOOL UWDFconv_to_doy(int yr, int mon, int dom, int *doy)
{
    int day_of_year, firstday;
    int igreg[5];

    /* convert from month-day to day-of-year */
    igreg[0] = yr;
    igreg[1] = 1;
    igreg[2] = 1;
    julian(igreg, &firstday);
    igreg[1] = mon;
    igreg[2] = dom;
    julian(igreg, &day_of_year);
    *doy = day_of_year - firstday + 1;
    return TRUE;
}

/*
Time conversion utiltity to convert from day-of-year to month &
day.  Input variables are year (yr), day-of-year (doy); output is
month (mon), and day-of-month (dom).
*/
BOOL UWDFconv_to_mon_day(int yr, int doy, int *mon, int *dom)
{
    int day_of_year, firstday;
    int igreg[5];

    igreg[0] = yr;
    igreg[1] = 1;
    igreg[2] = 1;
    julian(igreg, &firstday);
    day_of_year = doy + firstday -1;
    gregor(igreg, &day_of_year);
    *mon = igreg[1];
    *dom = igreg[2];
    return TRUE;
}

/* LOCAL (STATIC) UTILITITES BELOW HERE */

static BOOL lswab2(char buf[], int nbuf)        /* swab bytes in place */
{
    register int i;
    register char tmp;
    for (i = 0; i < nbuf; i += 2) {
        tmp = buf[i];
        buf[i] = buf[i+1];
        buf[i+1] = tmp;
    }
    return TRUE;
}

static BOOL lswab4(char buf[], int nbuf)        /* swab bytes in place */
{
    register int i;
    register char tmp;
    for (i = 0; i < nbuf; i += 4) {
        tmp = buf[i];
        buf[i] = buf[i+3];
        buf[i+3] = tmp;
        tmp = buf[i+1];
        buf[i+1] = buf[i+2];
        buf[i+2] = tmp;
    }
    return TRUE;
}

static BOOL strib(char *string) /* strip trailing blanks or newline */
{
     int i,length;
     length = strlen(string);
     if (length == 0) {
         return TRUE;
     } else {
         for(i = length-1; i >= 0; i--) {
             if (string[i] == ' ' || string[i] == '\n') {
                 string[i] = '\0';
             } else {
                 return TRUE;
             }
         }
         return TRUE;
    }
}

static int mo[24] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273,
    304, 334, 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335 };

static BOOL julian(int *igreg, int *ljul)
{
    /* Initialized data */
    static int l100 = 36524;
    static int l400 = 146097;
    static int l4 = 1461;
    int leap, n4, n100, n400, ida, imo, iyr, jyr;
    /* FUNCTION: CONVERT GREGORIAN DATE TO JULIAN DATE */
    /* 4 YEAR, 100 YEAR AND 400 YEAR RULES OBSERVED */
    /* BLAME: CARL JOHNSON */
    /* Parameter adjustments */
    --igreg;
    /* Function Body */
    iyr = igreg[1];
    imo = igreg[2];
    ida = igreg[3];
    *ljul = 0;
    jyr = iyr;
    if (jyr < 1) goto L110;
    --jyr;
    *ljul = 365;
    /* ......FOUR HUNDRED YEAR RULE */
    n400 = jyr / 400;
    *ljul += n400 * l400;
    jyr -= n400 * 400;
    /* ......100 YEAR RULE */
    n100 = jyr / 100;
    *ljul += n100 * l100;
    jyr -= n100 * 100;
    /* ......4 YEAR RULE */
    n4 = jyr / 4;
    *ljul += n4 * l4;
    jyr -= n4 << 2;
    /* ......ONE YEAR RULE */
    *ljul += jyr * 365;
    L110:
    leap = 0;
    if (iyr % 4 == 0) leap = 12;
    if (iyr % 100 == 0)	leap = 0;
    if (iyr % 400 == 0) leap = 12;
    *ljul = *ljul + mo[imo + leap - 1] + ida + 1721060;
    return TRUE;
}

static BOOL gregor(int *igreg, int *ljul)
{
    /* System generated locals */
    int i_1;
    /* Local variables */
    int leap, left, i, ltest, ig[5];
    /* FUNCTION: CALCULATE GREGORIAN DATE FROM JULIAN DATE */
    /* BLAME: CARL JOHNSON (11 JULY 1979) */
    /* Parameter adjustments */
    --igreg;
    /* Function Body */
    ig[0] = (*ljul - 1721061) / 365;
    ig[1] = 1;
    ig[2] = 1;
    julian(ig, &ltest);
    if (ltest <= *ljul) goto L110;
    L20:
    --ig[0];
    julian(ig, &ltest);
    if (ltest > *ljul) goto L20;
    goto L210;
    L105:
    ++ig[0];
    julian(ig, &ltest);
    L110:
    if ((i_1 = ltest - *ljul - 366) < 0) {
        goto L210;
    } else if (i_1 == 0) {
        goto L120;
    } else {
        goto L105;
    }
    L120:
    if (ig[0] % 400 == 0) goto L210;
    if (ig[0] % 100 == 0) goto L105;
    if (ig[0] % 4 == 0) goto L210;
    goto L105;
    L210:
    left = *ljul - ltest;
    igreg[1] = ig[0];
    leap = 0;
    if (ig[0] % 4 == 0) leap = 12;
    if (ig[0] % 100 == 0) leap = 0;
    if (ig[0] % 400 == 0) leap = 12;
    for (i = 2; i <= 12; ++i) {
	if (mo[i + leap - 1] <= left) goto L250;
	igreg[2] = i - 1;
	igreg[3] = left - mo[i + leap - 2] + 1;
	return 0;
	L250: ;
    }
    igreg[2] = 12;
    igreg[3] = left - mo[leap + 11] + 1;
    return TRUE;
}

static BOOL grgmin(int *igreg, int *lmin)
{
    int ljul;
    /* CALCULATE NUMBER OF MINUTES FROM MIDNITE, DEC 31, 1599 */
    /* Parameter adjustments */
    --igreg;
    /* Function Body */
    julian(&igreg[1], &ljul);
    ljul += -2305448;
    *lmin = ljul * 1440 + igreg[4] * 60 + igreg[5];
    return TRUE;
}

static BOOL mingrg(int *igreg, int *lmin)
{
    int imin, ljul;
    /* CALCULATE GREGORIAN DATE FROM MINUTES PAST MIDNITE, 12,31,1599 */
    /* Parameter adjustments */
    --igreg;
    /* Function Body */
    ljul = *lmin / 1440;
    imin = *lmin - ljul * 1440;
    ljul += 2305448;
    gregor(&igreg[1], &ljul);
    igreg[4] = imin / 60;
    igreg[5] = imin - igreg[4] * 60;
    return TRUE;
}

static BOOL set_swab_on_read(void)
{
    /* Determine if we are big (I for IEEE) or little (D for DEC) endian */
    int i = 1;
    char *c = (char *) &i;
    char MACHINE = (*c == 1) ? 'D': 'I';

    char *ctest;
    /* If UW-1 format, use first two bytes to determine byte order */
    if (uw_fmt_flg == 1) {
        /* Look at first byte to see if ANSI or non-ANSI byte order */
	ctest = (char *)&mh->nchan;
	if (MACHINE == 'I') { /* assume non DEC machine */ 
	    if (*ctest != '\0') {
		swab_on_read = TRUE;
	    } else {
		swab_on_read = FALSE;
	    }
	} else { /* assume DEC machine */
	    if (*ctest != '\0') {
		swab_on_read = FALSE;
	    } else {
		swab_on_read = TRUE;
	    }
	}
    }else { /* UW-2 format; interpret mh->extra[1] */
	if (MACHINE == 'I') { /* assume non DEC machine */
	    if (mh->extra[1] == ' ' || mh->extra[1] == 'I') {
		swab_on_read = FALSE;
	    } else {
		swab_on_read = TRUE;
	    }
	} else { /* assume DEC machine */
	    if (mh->extra[1] == ' ' || mh->extra[1] == 'I') {
		swab_on_read = TRUE;
	    } else {
		swab_on_read = FALSE;
	    }
	}
    }
    return TRUE;
}

/*
 * Initialize channel map from external file
 */
static BOOL init_channel_map(char *map_filename)
{
    /* Read channel mapping info from default file */
    int i;
    char line[100];
    FILE *ftmp;

    /* Read the mapping file; if file not available, return FALSE */
    i = 0;
    if (strcmp(map_filename, "") == 0) {
        return FALSE;
    }
    if ((ftmp = fopen(map_filename, "r")) != NULL) { /* found mapping file */
        while (fgets(line, 100, ftmp) != NULL) {
            if (line[0] == '#') continue;
            sscanf(line,"%s %s %s %s %s", sta_map[i].old_name,
		sta_map[i].new_name, sta_map[i].comp_flg,
		sta_map[i].id, sta_map[i].src);
            if (strcmp(sta_map[i].comp_flg, "\"\"") == 0)
                sta_map[i].comp_flg[0] = '\0';
	    if (strcmp(sta_map[i].comp_flg, "none") == 0)
		sta_map[i].comp_flg[0] = '\0';
            if (strcmp(sta_map[i].id, "\"\"") == 0)
                sta_map[i].id[0] = '\0';
            if (strcmp(sta_map[i].id, "none") == 0)
                sta_map[i].id[0] = '\0';
	    if (strcmp(sta_map[i].src, "\"\"") == 0)
		sta_map[i].src[0] = '\0';
	    if (strcmp(sta_map[i].src, "none") == 0)
		sta_map[i].src[0] = '\0';
            if (strcmp(sta_map[i].old_name, "any") == 0) def_map = i;
            ++i;
        }
        fclose(ftmp);
    } else { /* no station mapping file */
        return FALSE;
    }
    nmap = i;
    return TRUE;
}

/*
 * return index to channel mapping for UW-1 to UW-2
 */
static int channel_map(char *old_name)
{
    int i;
    for (i = 0; i < nmap; ++i) {
        if (strcmp(sta_map[i].old_name, old_name) == 0) {
            return i;
        }
    }
    return -1;
}

/*
 * get_revs assigns polarity reversals to each channel from
 * the complete station reversal table
 */

static BOOL get_revs(void) /* determine what stas have reversed first motion at this time, and fill table is_rev[] */
{
    char *tp, reversed_sta_name[10];
    int now, i, j;
    long int early, late;
    struct Time time;
    int *t;

    /* set reversal flags all false */
    for (i = 0; i < nchannels; ++i) is_rev[i] = FALSE;
    if (n_rev_table == 0) return TRUE;
    t = &time.yr;
    mingrg(t, &((ch2)->start_lmin));
    now = 10000 * (time.yr - 1900) + 100 * time.mon + time.day;
    for (i = 0; i < n_rev_table; ++i) {
	strcpy(reversed_sta_name, "");
	tp = strtok(complete_rev_table[i].line, " ");
	while ((tp = strtok(NULL, " ")) != NULL) {
	    sscanf(tp,"%6ld-%6ld",&early,&late);
	    if(now >= early && now <= late) {
		sscanf(complete_rev_table[i].line, "%s", reversed_sta_name);
		break;
	    }
	}
	if (strcmp(reversed_sta_name, "") != 0) {
	    for (j = 0; j < nchannels; ++j) {
		if (strcmp(reversed_sta_name, (ch2+j)->name) == 0) {
		    is_rev[j] = TRUE;
		    break;
		}
	    }
	}
    }
    return TRUE;
}

/*
 * Find earliest start time among all channels, and set this into the
 * master header time field;  This makes it possible to always use the
 * function UWDFdhref_stime(void) to return the minimum channel start time
 */
static BOOL set_earliest_starttime(void)
{
    int i, nchan, temp;
    int minimum_lmin, minimum_lsec, tmp_lmin, tmp_lsec;
    nchan = UWDFdhnchan();
    temp = (ch2+0)->start_lsec + chtime_corr[0];
    minimum_lmin = (ch2+0)->start_lmin;
    while (temp < 0) {
	temp += 60000000;
	minimum_lmin--;
    }
    minimum_lmin += temp/60000000;
    minimum_lsec = temp%60000000;
    for (i = 1; i < nchan; ++i) {
	temp = (ch2+i)->start_lsec + chtime_corr[i];
	tmp_lmin = (ch2+i)->start_lmin;
	while (temp < 0) {
	    temp += 60000000;
	    tmp_lmin--;
	}
	tmp_lmin += temp/60000000;
	tmp_lsec = temp%60000000;
	if (tmp_lmin < minimum_lmin) { /* clearly earlier time */
	    minimum_lmin = tmp_lmin;
	    minimum_lsec = tmp_lsec;
	} else if (tmp_lmin == minimum_lmin) { /* may be earlier */
	    if (tmp_lsec < minimum_lsec) { /* Yes, earlier */
		minimum_lsec = tmp_lsec;
	    }
	}
    }
    mh->lmin = minimum_lmin;
    mh->lsec = minimum_lsec;
    return TRUE;
}

/*
** TIME CORRECTION ROUTINES
**
** Channel time corrections are specified in the data file by a block of
** time correction structures.  One time correction structure is included
** for each channel to which a correction should be applied.  The times in
** the channel headers are not changed so that the original uncorrected
** time is retained.  Note that the data file interface will apply the
** time corrections automatically when returning the start time for a
** channel.  Time correction blocks are indicated in the structure index at
** the end of the file by the string "TC2".
**
**
**                         UW-2 TIME CORRECTION
** ========================================================================
** NAME    BYTES   TYPE    DESCRIPTION
** ========================================================================
** CHNO    4       int     Channel number (zero-based index in channel
**                         header list)
**
** CORR    4       int     Time correction (in micro-seconds) to add to
**                         start time in channel header
** ========================================================================
**
*/

/*
** Returns double precision value of the time correction in seconds
** for the specified channel number "chno".
*/
double UWDFchtime_corr(int chno)
{
    return((double) (chtime_corr[chno] * 1.0e-06));
}

/*
** Set the channel time correction for the channel to be written next.
** "new_chtime_corr" is a double variable, with units of seconds.
*/
BOOL UWDFset_chtime_corr(double new_chtime_corr)
{
    new_chtime_corr_proto = new_chtime_corr * 1.0e+06;
    return TRUE;
}

/*
** Returns double precision value of the time correction in seconds
** of the prototype channel used for writing.
*/
double UWDFret_chtime_corr_proto(void)
{
    return((double) (new_chtime_corr_proto * 1.0e-06));
}
