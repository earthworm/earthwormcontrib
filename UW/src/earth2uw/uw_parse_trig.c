/* UW modified version version of earthworm's parse_trig routines.
 * simplified to assume that trig message contains only trigger lines of
 * format:
 *	Station Comp Network starttime duration
 * starttime is in the format "YYMMDDHHMMSS.SS", maybe missing characters
 * on the right side, which will be treated as zero.
 * Trigger messages are assumed to be for a single event
 *
 * Modified by Pete Lombard, UW seismology; 11/4/97
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "parse_trig.h"

#define LINE_LEN         500

/***********************************************************************
 *  parseSnippet() parses snippet parameters from next line of trigger *
 *                 message. If one line is bad, it keeps trying the    *
 *                 next until it finds a good line.                    *
 *                 Returns RETURN_FAILURE when nothing left.           *
 *		   Does it's own error logging via logit.              *
 ***********************************************************************/
int parseSnippet( char* msg, SNIPPET* pSnp, char** nxtLine)
{
  static char terminators[] = " \t"; /* we accept space and tab as terminators */
  char* nxttok;
  char line[LINE_LEN];
  int len;
  
  /* Read the next station line
  *****************************/
GetNextStation:
  if( getNextLine(nxtLine, line) <= 0 ) /* this should be a station trigger line */
    {
      return(RETURN_FAILURE); 	/* no more lines */
    }
  if ( ( nxttok=strtok(line, terminators) ) == NULL ) /* first token should be station name */
    {
      logit("e","earth2uw: Bad syntax in trigger message:"
	    "Strange station line - no tokens in: \n.&s.\n",line);
      goto GetNextStation;
    }

  /* Find SCN names
  ****************/
  if (nxttok ==NULL) /* oops - should have been the station name */
    {
      logit("e", "earth2uw: Bad syntax in trigger message."
	    " Cant find statio name in:\n.%s.\n", line);
      goto GetNextStation;
    }
  strncpy( pSnp->sta, nxttok, 6); 	/* put away the station name */

  nxttok = strtok( (char*)NULL, terminators); /* should be the component */
  if (nxttok ==NULL) /* oops - there was nothing after station name */
    {
      logit("e", "earth2uw: Bad syntax in trigger message."
	    " Cant find comp name in:\n.%s.\n", line);
      goto GetNextStation;
    }
  strncpy( pSnp->chan, nxttok, 6 );/* put away the component */

  nxttok = strtok( (char*)NULL, terminators); /* should be the net */
  if (nxttok ==NULL) /* oops - there was nothing after component name */
    {
      logit("e", "earth2uw: Bad syntax in trigger message."
	    " Cant find net name in:\n.%s.\n", line);
      goto GetNextStation;
    }
  strncpy( pSnp->net, nxttok, 3 );/* put away the net */

  /* now find time strings
  ***********************/
  nxttok = strtok( (char*)NULL, terminators); /* should be start date-time */
  if (nxttok == NULL) /* oops - there was nothing after save: */
    {
      logit("e", "earth2uw: Bad syntax in trigger message."
	    " Cant find save date in:\n.%s.\n", line);
      goto GetNextStation;
    }
  strcpy( pSnp->startstr, nxttok ); 	/* put away the date string */
  len = strlen(pSnp->startstr);
  while (len < 11 ) pSnp->startstr[len++] = '0';
  pSnp->startstr[len] = '\0';

  /* Convert start time to double 
  ******************************/
  if( strtimetodbl(pSnp->startstr, &(pSnp->starttime) ) < 0)
    {
      logit("e", "earth2ew: Bad syntax in trigger message."
	    " Dont understand start-time in:\n.%s.\n", line);
      goto GetNextStation;
    }
  if ( pSnp->starttime < 500000000 ) /* unreasonable time value */
    {
      logit("e", "earth2ew: Bad syntax in trigger message."
	    " Bad start time value in:\n.%s.\n", line);
      goto GetNextStation;
    }

  /* find duration to save
  ***********************/
  nxttok = strtok( (char*)NULL, terminators); /* should be the duration */
  if (nxttok ==NULL) /* oops - there was nothing after save: */
    {
      logit("e", "earth2ew: Bad syntax in trigger message."
	    " Cant find duration in:\n.%s.\n", line);
      goto GetNextStation;
    }
  pSnp->duration = atol( nxttok );
  if ( pSnp->duration <= 0 )
    {
      logit("e", "earth2ew: Bad syntax in trigger message."
	    " Bad duration value in:\n.%s.\n", line);
      goto GetNextStation;
    }

  return(RETURN_SUCCESS);
}   
/* ---------------------- end of parseSnippet() ------------------------ */

/**************************************************************************
 *    getNextLine(msg, line) moves the next line from 'msg' into 'line    *
 *                           returns the number of characters in the line *
 *			     Returns negative if error.			  *
 **************************************************************************/
int getNextLine ( char** pNxtLine, char* line)
{
  int i;
  char* nxtLine;

  nxtLine=*pNxtLine;

  for (i =0; i< LINE_LEN; i++)
    {
      line[i] = *nxtLine++;
      if ( (int)line[i] == 0 )
	{
	  /* logit("e","earth2uw: ran into end of string\n"); */ /* DEBUG */
	  return(-1); /*  Not good (???) */
	}
      if (line[i] == '\n') goto normal;
    }
  logit("e","earth2uw: line too long \n");
  return(-1);

normal:
  line[i]=0;  /* remove the newline */
  *pNxtLine = nxtLine;
  return(i);
   
}
/* --------------------- end of getNextLine() -------------------------- */


/**************************************************************************
 *    strtimetodbl() takes character date-time string, and converts to    *
 *      double seconds since 1970 (the UNIX epoch).                       *
 *      date-time strings is up to 15 characters of YYMMDDHHMMSS.SS       *
 *      Missing digits on the right are added on internally.              *
 *	    Returns negative if error.			                  *
 **************************************************************************/
int strtimetodbl(char* string, double* time) 
{
  char timestr[20];
  char zero[] = "000000000000.00";
  int ret, i;

  /* we want a string of  the form yymmddhhmmss.ss
   *                               012345678901234
   *  we have yymmddhhmms  sooo... 
   *	      01234567890                */

  strcpy(timestr,string);
  for (i = strlen(string); i < 16; i++) timestr[i] = zero[i];

  /* convert to double seconds */
  ret = epochsec15(time, timestr);
  if (ret < 0)
    {
      logit("e","earth2uw: bad return from epochsec15\n");
      return(-1);
    } 
  return(1);
}


/* convert from duoble seconds since 1970 to 15 character date string */
int sec15epoch( double time, char** string )
{
  static char str15[16];
  double jsec;
  double sec1970 = 11676096000.00; /* seconds between Carl Johnson's
				    * time 0 (Jan 1, 1600) and Jan 1, 1970. */

  date15( time + sec1970, str15);
  *string = str15;
}
