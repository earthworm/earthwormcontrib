/* uwputaway.c 
These are the four routines which plug into xxtrace_save, and cause the trace
data snippets to be put away into a UW2-format trace file.  These are the
model for other put away routines, such as flat files of various formats.


   Written by: Pete Lombard, 10/12/97
   
   */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <earthworm.h>
#include <trace_buf.h>
#include <swap.h>
#include <ws_clientII.h>
#include "putaway.h"
#include "uwdfif.h"

#define MAX_CHANS     512
#define MAXTXT        150
#define BIAS_COUNT   1000
#define GETTING_MEAN    1
#define GOT_MEAN        0
#define BACKFILL_MEAN  -1
#define MEAN_MIN     1000     /* minimum number of samples to use for mean */

void SwapShort( short * );
void SwapLong( long * );
void SwapDouble( double * );
void NCMapChan( TRACE_HEADER* );
void NCFixPin( TRACE_HEADER* );

static struct tm *gtime;       /* for curent date-time */
static char home_dir[MAXTXT];  /* earth2uw's home directory */

static int    logskip = 1;
char OutFile[MAXTXT];     /* Name of the output file */
char OutDir[MAXTXT];
extern int WriteNet;

extern int Debug;

/* allocated in uwtracesave.c */
extern char   *OutBuffer;   /* buffer for storing output before writing */
extern long   OutBufferLen;      /* size of the output buffer in bytes */
extern double GapThresh;    /* number of sample periods to declare a gap */

/* fill number from config file */
short  fill_short = 0;
long   fill_long = 0l;
float  fill_float = 0.0;

/* Initialization function, 
*       This is the Put Away startup intializer. This is called when    *
*       the system first comes up. Here is a chance to look around      *
*       and see if it's possible to do business, and to complain        *
*       if not ,BEFORE an event has to be processed.                    *
 * We (UW) choose to do nothing here
 */
int PA_init()
{
  /* Remember our current directory, so we can come back here later */
  if ( getcwd(home_dir, MAXTXT) == (char *)NULL )
    logit("e", "EARTH2SAC: error getting current directory: %s\n", 
	  strerror(errno));
  
  if ( strlen(OutDir) > 0 )
    if ( chdir(OutDir) != 0 )
      {
	logit("e", "EARTH2SAC: unable to change directory to %s\n", OutDir);
	return( RETURN_FAILURE );
      }
  
  return( RETURN_SUCCESS );
}

/****************************************************************************
*       This is the Put Away event initializer. It's called when a snippet  *
*       has been received, and is about to be processed.                    *
*       It gets to see the pointer to the TraceRequest array,               *
*       and the number of loaded trace structures.                          *
*****************************************************************************/
int PA_next_ev( TRACE_REQ *ptrReq, int nReq, int debug )
{
  long     tssec, usec;
  struct Time	uwtime;
  short int flags[10] = {0,0,0,0,0,0,0,0,0,0};

  /* open the output file, set up the headers */
  if (UWDFinit_for_new_write(OutFile) == FALSE) 
    {
      logit("e", "earth2uw: can't open file %s\n", OutFile );
      return( RETURN_FAILURE );
    }

  /* Set some master header values, mostly not used by UW2 trace format */
  UWDFset_dhtapeno(0);
  UWDFset_dhevno(0);
  filetime(&uwtime, tssec, usec);
  UWDFset_dhref_stime(uwtime);
  UWDFset_dhflgs(flags);
  UWDFset_dhcomment("earth2uw");

  /* Write the file header */
  if (UWDFwrite_new_head() == FALSE) {
    logit("e", "earth2uw: can't write to file %s\n", OutFile );
    return( RETURN_FAILURE );
  }
  return( RETURN_SUCCESS);
}

void od( char *, int );
/*****************************************************************************
*     This is the working entry point into the disposal system. This routine *
*     gets called for each trace snippet which has been recovered. It gets   *
*     to see the corresponding SNIPPET structure, and the event id           *
******************************************************************************
/* Process one channel of data */
int PA_next( TRACE_REQ *getThis, int eventid )
     /*
 * input:  getThis   pointer to buffer for the trace data request
           eventId   not used here 
 */
{
  TRACE_HEADER *wf;
  char *msg_p;
  union {
    short s;
    long  l;
    float f;
  } data;             /* little buffer for swapping bytes */
  int          byte_per_sample;
  int          i, j;
  int          started = 0;       /* 1 if we have put away the first packet */
  int          gap_count = 0;
  int          mean_counter = 0;
  int          get_mean_flag = GETTING_MEAN;
  long         pinno, nsamp, nfill;
  long         nfill_max = 0l;
  long         nsamp_this_scn = 0l;
  long         this_size;
  long         mean_sum = 0l;
  long         bias;
  long         *long_p;
  long         bogus = (long) 0xFFFFFFFF;
  float        *float_p;
  short        *short_p;
  double       starttime, endtime; /* times for current scn         */
  double       m_starttime, m_endtime; /* times for current message */
  double       req_endtime;        /* endtime advertised in the req */
  double       samprate;
  long         tssec, usec;
  struct Time  uwtime;
  char         station[5];         /* UW2 names for station...      */
  char         channel[4];         /* ... channel ...               */
  char         net[4];             /* and network.                  */
  char         idnum[5];           /* UW pinno, ignored             */
  char         otype;              /* UW2 data type: S, L, or F     */
  char         *fill_p;

  req_endtime = getThis->actEndtime;
  msg_p = getThis->pBuf;   /* pointer to first message */
  wf = (TRACE_HEADER *) msg_p;

  /* map Northern California channel names to SEED convention */
  if (strcmp(wf->net, "NC") == 0){
    NCMapChan( wf );
    NCFixPin( wf );
  }
    
  /* UW2 format has small places for names; 
   * may need to do mapping instead of truncating. */
  strncpy(station, wf->sta, (size_t) 4);
  strncpy(channel, wf->chan, (size_t) 3);
  strncpy(net, wf->net, (size_t) 3 );
  station[4] = channel[3] = net[3] = (char) 0;

  pinno = wf->pinno;
  starttime = wf->starttime;
  samprate = wf->samprate;
  byte_per_sample  = atoi(&wf->datatype[1]);

#ifdef _SPARC
  if( wf->datatype[0]=='i' || wf->datatype[0]=='f' )
    {
      SwapLong( &pinno );
      SwapLong( &nsamp );
      SwapDouble( &starttime );
      SwapDouble( &samprate );
      SwapDouble( &req_endtime );
    }
#endif
#ifdef _INTEL
  if( wf->datatype[0]=='s' || wf->datatype[0]=='t' )
    {
      SwapLong( &pinno );
      SwapLong( &nsamp );
      SwapDouble( &starttime );
      SwapDouble( &samprate );
      SwapDouble( &req_endtime );
    }
#endif

  /* Set the output data type */
  if (wf->datatype[0]=='i' || wf->datatype[0]=='s') { /* integer types */
    if (byte_per_sample==2) {
      otype = 'S';
    } else if (byte_per_sample==4) {
      otype = 'L';
    }
  } else if (wf->datatype[0]=='f' || wf->datatype[0]=='t') { /* float types */
    if (byte_per_sample==4) otype = 'F';
  }  /* Some day there may be a double type */

  /* loop through all the messages for this s-c-n */
  while (1) {
    /* read the header inforamtion for each TRACE_BUF message */
    wf = (TRACE_HEADER *) msg_p;
    
    nsamp = wf->nsamp;
    m_starttime = wf->starttime;
    m_endtime = wf->endtime;

#ifdef _SPARC
    if( wf->datatype[0]=='i' || wf->datatype[0]=='f' ) {
      SwapLong( &nsamp );
      SwapDouble( &m_starttime );
      SwapDouble( &m_endtime );
    }
#endif
#ifdef _INTEL
    if( wf->datatype[0]=='s' || wf->datatype[0]=='t' ) {
      SwapLong( &nsamp );
      SwapDouble( &m_starttime );
      SwapDouble( &m_endtime );
    }
#endif

    /* If we have the mean value, we can go back and remove it from the first
       * section of data in the output buffer. But if the data is the `bogus'
       * value, then we replace it with the `fill' value.
       */
    if (get_mean_flag == BACKFILL_MEAN ) {
      bias = mean_sum / (long) mean_counter;
      switch(otype) {
      case 'S':
	short_p = (short*) OutBuffer;
	for (j = 0; j < mean_counter; j++) {
	  if (memcmp(short_p, &bogus, byte_per_sample) == 0) {
	    memcpy(short_p, &fill_short, byte_per_sample);
	  } else {
	    *short_p -= (short) bias;
	  }
	  short_p++;
	}
	break;
      case 'L':
	long_p = (long*) OutBuffer;
	for (j = 0; j < mean_counter; j++) {
	  if (memcmp(long_p, &bogus, byte_per_sample) == 0) {
	    memcpy(long_p, &fill_long, byte_per_sample);
	  } else {
	    *long_p -= (long) bias;
	  }
	  long_p++;
	}
	break;
      case 'F':
	float_p = (float*) OutBuffer;
	for (j = 0; j < mean_counter; j++) {
	  if (memcmp(float_p, &bogus, byte_per_sample) == 0) {
	    memcpy(float_p, &fill_float, byte_per_sample);
	  } else {
	    *float_p -= (float) bias;
	  }
	  float_p++;
	}
	break;
      }
      get_mean_flag = GOT_MEAN;
    }

    /* Check for gaps between messages, which by definition don't occur at
       * the start of data. */
    if (started) {
      if ( endtime + ( 1.0/samprate ) * GapThresh < m_starttime ) {
	/* there's a gap, so fill it */
	logit("e", "gap in %s.%s.%s: %lf - %lf\n", station, 
			 channel, net, m_starttime, endtime);
	nfill = (long) ( samprate * ( m_starttime - endtime ) - 1 );
	if ( (nsamp_this_scn + nfill) * byte_per_sample > OutBufferLen ) {
	  logit("e", "bogus gap; skipping this SCN\n");
	  return(RETURN_FAILURE);
	}
	
	if (get_mean_flag == GETTING_MEAN) {
	  /* We haven't computed the mean yet; so we use a temporary special value
	   * which can later be replaced by the correct fill value */
	  fill_p = (char *) &bogus;
	} else {   /* already have mean, so we can use real fill value */
	  switch(otype) {
	  case 'S':
	    fill_p = (char *) &fill_short;
	    break;
	  case 'L':
	    fill_p = (char *) &fill_long;
	    break;
	  case 'F':
	    fill_p = (char *) &fill_float;
	    break;
	  }
	}
	/* do the filling */
	for ( j = 0; j < nfill; j++ ) {
	  memcpy( (void *) (OutBuffer + byte_per_sample * nsamp_this_scn), 
		    fill_p, byte_per_sample);
	    nsamp_this_scn ++;
	}
	/* keep track of how many gaps and the largest one */
	gap_count++;
	if (nfill_max < nfill) nfill_max = nfill;
      }
    }   /* end of gap-filling section */
    
    /* advance message pointer to the data */
    msg_p += sizeof(TRACE_HEADER);

    /* check for sufficient memory */
    this_size = (nsamp_this_scn + nsamp ) * byte_per_sample;
    if ( OutBufferLen < this_size ) {
      logit( "e", "earth2uw: PA_next out of space for S<%s> C<%s> N<%s>; saving short trace.\n",
	     station, channel, net);
      goto done;
    }

    /* loop through first part of data, compute mean while transferring to OutBuffer */
    if (get_mean_flag == GETTING_MEAN) {
      for ( j = 0; j < nsamp; j++ )
	{
	  memset( (void *) &data, 0, sizeof(data) );
	  memcpy( (void *) &data, msg_p, byte_per_sample );
#ifdef _SPARC
	  if( wf->datatype[0]=='i' || wf->datatype[0]=='f' )
	    {
	      if(byte_per_sample==4) SwapLong ( &data.l );
	      if(byte_per_sample==2) SwapShort( &data.s );
	    }
#endif
#ifdef _INTEL
	  if( wf->datatype[0]=='s' || wf->datatype[0]=='t' )
	    {
	      if(byte_per_sample==4) SwapLong ( &data.l );
	      if(byte_per_sample==2) SwapShort( &data.s );
	    }
#endif
	  memcpy( (void *) (OutBuffer + byte_per_sample * nsamp_this_scn), 
		  &data.l, byte_per_sample );
	  nsamp_this_scn ++;
	  msg_p += byte_per_sample;
	  mean_counter++;
	  switch (otype) {
	  case 'S':
	    mean_sum += (long) data.s;
	    break;
	  case 'L':
	    mean_sum += data.l;
	    break;
	  case 'F':
	    mean_sum += (long) data.f;
	    break;
	  } /* switch */
	}  /* j loop */
      if (mean_counter >= MEAN_MIN) get_mean_flag = BACKFILL_MEAN;
    } else {  /* if (get_mean_flag... */
      /* We already have the mean value, so we can take it out as bias 
       * while we transfer data */
      for ( j = 0; j < nsamp; j++ )
	{
	  memset( (void *) &data, 0, sizeof(data) );
	  memcpy( (void *) &data, msg_p, byte_per_sample );
#ifdef _SPARC
	  if( wf->datatype[0]=='i' || wf->datatype[0]=='f' )
	    {
	      if(byte_per_sample==4) SwapLong ( &data.l );
	      if(byte_per_sample==2) SwapShort( &data.s );
	    }
#endif
#ifdef _INTEL
	  if( wf->datatype[0]=='s' || wf->datatype[0]=='t' )
	    {
	      if(byte_per_sample==4) SwapLong ( &data.l );
	      if(byte_per_sample==2) SwapShort( &data.s );
	    }
#endif
	  switch (otype) {
	  case 'S':
	    data.s -= (int) bias;
	    break;
	  case 'L':
	    data.l -= (long) bias;
	    break;
	  case 'F':
	    data.f -= (float) bias;
	    mean_counter++;
	    break;
	  } /* switch */
	  memcpy( (void *) (OutBuffer + byte_per_sample * nsamp_this_scn), 
		  &data.l, byte_per_sample );
	  msg_p += byte_per_sample;
	  nsamp_this_scn ++;
	}  /* j loop */
    } /* if (get_mean_flag...) */

    endtime = m_endtime;
    /* Are we done with messages? */
    /* End-check based on endtime in snippet request and last trace message */
    /* if ( endtime + ( 1.0/samprate ) * 1.5 => req_endtime ) break; */
  
    /* End-check based on length of snippet buffer */
    if ((long) msg_p >= (long) getThis->actLen + (long) getThis->pBuf) {
      /* if (Debug) logit( "e", "earth2uw: end time of snippet: %d\n", */
      /*      req_endtime); */
      /* if (Debug) logit( "e", "\t end time of last message: %d\n", endtime); */
      break;
    }
    started = 1;
  }


  /*  All data read.  Now setup and write it
  *****************************************/

done:
  tssec=(long)starttime;
  usec=(starttime - (double)tssec)*1000000;
  filetime(&uwtime, tssec, usec);
  UWDFset_chname(station);
  UWDFset_chsrate((float) samprate);
  UWDFset_chref_stime(uwtime);
  UWDFset_chcompflg(channel);
  if (WriteNet) {
    UWDFset_chid(net);
  } else {
    if (strcmp("WWVB", station) == 0) {
      /* Tell xped to put WWVB at the top of its display */
      UWDFset_chid("0");
    } else {
      UWDFset_chid("");
    }
  }
    
  if (pinno < 200) {
    UWDFset_chsrc("S2E");       /* Analog data from sunworm */
  } else if (pinno < 400) {
    UWDFset_chsrc("REF");       /* RefTek instrument */
  } else if (pinno < 600) {
    UWDFset_chsrc("I24");       /* Terra IDS24 instrument */
  } else if (pinno < 800) {
    UWDFset_chsrc("I20");       /* Terra IDS20 instrument */
  } else if (pinno < 1000) {
    UWDFset_chsrc("IMP");       /* Import from other earthworms */
  }
  UWDFset_chlta(0);
  UWDFset_chtrig(0);
  UWDFset_chbias( (int) bias );
  if (UWDFwrite_new_chan(otype, nsamp_this_scn, OutBuffer, otype) == FALSE) {
    logit("e", "earth2uw: PA_event: error writing <%s><%s><%s> to file: %s\n",
	  station, channel, net, strerror(errno));
    return(RETURN_FAILURE);
  }

  if (gap_count) 
    logit("e", "earth2uw: PA_event: %d gaps; largest %ld for <%s><%s><%s>\n",
	  gap_count, nfill_max, station, channel, net);
}

/************************************************************************
*       This is the Put Away end event routine. It's called after we've     *
*       finished processing one event.                                  *
*************************************************************************/
int PA_end_ev()
{
  UWDFclose_new_file();
  return( RETURN_SUCCESS );
}


/************************************************************************
*       This is the Put Away close routine. It's called after when      *
*       we're being shut down.                                          *
*************************************************************************/
int PA_close()
{
  if ( strlen(OutDir) > 0 )
    chdir( home_dir );
  return( RETURN_SUCCESS );
}


/* ******************************************************
 * load up a time structure for UW2 from UNIX type time *
 ********************************************************/

struct tm *gmtime();    /* system function */


filetime(struct Time *time, long tssec, long usec)
{

  gtime=gmtime(&tssec);
  time->yr = gtime->tm_year + 1900;
  time->mon = gtime->tm_mon + 1;
  time->day = gtime->tm_mday;
  time->hr = gtime->tm_hour;
  time->min = gtime->tm_min;
  time->sec = gtime->tm_sec;
  time->sec += (float)usec/1000000.;
}


/* od: log the contents of a buffer in format similar to `od' (octal dump) */
void od(char *ptr, int n)
{
  int cntr, col, chr, fill;

    for ( cntr = 0, col = 0; cntr < n; ) {
      
      for ( col = 0; (col + cntr < n) && (col < 16); col++ ) {
	chr = ptr[cntr + col] & 0x7F;
	logit("o", "%02x ",chr);
      }
      
      for ( fill = col; fill < 16; fill++ ) {
	logit("o","   ");
      }
      
      logit("o"," ");
      
      for ( col = 0; (col + cntr < n) && (col < 16); col++ ) {
	chr = ptr[cntr + col] & 0x7F;
	
	if ( (chr >= ' ' && chr <= '~') || (chr == 0x20) ) {
	  logit("o","%c",chr);
	}
	else {
	  logit("o",".");
	}
      }
      
      cntr += col;
      
      if ( col == 16 ) {
	logit("o","\n");
	col = 0;
      }
    }   /* for ( cntr = 0, col = 0; cntr < sz; ) */
    
    if ( col ) {
      logit("o","\n");
    }
}


void NCMapChan( TRACE_HEADER* wf )
{
#define NMAP 21
  char NCchan[NMAP][9] =   {"VHZ", "VHN", "VHE", "VLZ", "VLN", "VLE", 
			    "VFZ", "VFN", "VFE", "VDZ", "VDN", "VDE",
			    "ADZ", "ADN", "ADE", "ASZ", "ASN", "ASE",
			    "AFZ", "AFN", "AFE"};
  char SEEDchan[NMAP][9] = {"EHZ", "EHN", "EHE", "ELZ", "ELN", "ELE",
			    "ENZ", "ENN", "ENE", "EDZ", "EDN", "EDE",
			    "HGZ", "HGN", "HGE", "HLZ", "HLN", "HLE",
			    "HOZ", "HON", "HOE"};
  int i;
  
  for (i = 0; i < NMAP; i++)
    if (strcmp(NCchan[i], wf->chan) == 0) strcpy( wf->chan, SEEDchan[i]);
    
  return;
}

void NCFixPin( TRACE_HEADER* wf )
{
#define NCSTA 4
  char NCsta[NCSTA][9] = {"KSX", "KTR", "KSC", "KEB"};
  int  NCpins[NCSTA]   = {803,   802,   801,   800};
  int i;
  for (i = 0; i < NCSTA; i++)
    if (strcmp( wf->sta, NCsta[i]) == 0) wf->pinno = NCpins[i];
  
  return;
}

  
  
