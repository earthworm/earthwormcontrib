/*
 * Include file for use with uwdfif data file interface package
 * All applications using this package should include this header
 * file to ensure correct prototype declarations and structure
 * definitions
 */
 
#ifndef __UWDFIF_H_INCLUDED__
#define __UWDFIF_H_INCLUDED__

#ifndef BOOL
#define BOOL int
#endif

#ifndef TRUE
#define TRUE (1)
#endif /* TRUE */

#ifndef FALSE
#define FALSE (0)
#endif /* FALSE */

#include "masthead.h"

#include "chhead.h"

/* Specification of structure for indexing expansion structures */

struct expanindex {
    char structag[4]; /* tag to indicate specific structure;  currently
	"CH2" is the tag for the UW-2 channel structures */
    int numstructs; /* number of structures to read; for the "CH2"
	   structures, this would be the number of channels */
    int offset; /* file offset for beginning of structure; for "CH2"
	   this would be the beginning of the sequence of channel header
	   structures */
}; /* length of expanstruct = 12 bytes */

/* Alternate time structure for UWDFdhref_stime(), UWDFchref_stime(), and
   UWDFset_ref_stime() */

#ifndef TIME_DEFINED
#define TIME_DEFINED
struct Time {
    int yr, mon, day, hr, min;
    double sec;
}; /* lenth of Time = 24 bytes */
#endif

#include "uwdfif_proto.h"

#endif /* _UWDFIF_H_INCLUDED */
