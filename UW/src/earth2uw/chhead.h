/*  Channel headers for UW-1 (chhead1) and UW-2 (chhead2)  */

struct chhead1 {  /* UW-1 station headers; one per channel */
    char name[6]; /* Station name (4 characters and a null) */
    short int lta;    /* long term average */
    short int trig;   /* Trigger  (positive for trigger on) */
    short int bias;   /* DC offset */
}; /* length of chhead1 = 12 bytes */

struct chhead2 { /* UW-2 channel headers; one per channel */
    int chlen;    /* channel length in samples */
    int offset;   /* start offset of channel; bytes rel. to start of file */
    int start_lmin;    /* start time in min; same def. as lmin in masthead */
    int start_lsec;    /* start time offset relative to lmin; u-sec */
    int lrate;    /* sample rate in samples per 1000 secs */
    int expan1;	/* expansion field for long integers */
    short int lta;    /* long term average; same as chhead1 */
    short int trig;   /* Trigger (positive for trigger on); same as chhead1 */
    short int bias;   /* DC offset; same as chhead1 */
    short int fill;   /* Fill short int so short int block 8 bytes long */
    char name[8]; /* station name (4 characters and null) */
    char fmt[4];  /* first char designates data fmt (S, L, or F) */
    char compflg[4]; /* component id as per Seed Appen I + seq no. */
    char chid[4]; /* unique channel id; user defined */
    char src[4]; /* "source" of data field */
}; /* length of chhead2 = 56 bytes */
