# These need to be adjusted to find the current lib an bin directories.
B = ../../bin
L = ../../lib

GLOBALFLAGS=-D_SPARC -D_SOLARIS  -I../../include
# SNAPS utils library requires compiling with -Xc (strict ANSI)
CFLAGS=${GLOBALFLAGS} -g -Xc

BINARIES = earth2uw.o uwputaway.o snippet.o uw_parse_trig.o ../../libutils.a \
           $L/chron3.o $L/kom.o $L/sleep_ew.o\
           $L/logit.o $L/socket_ew.o $L/time_ew.o  $L/swap.o $L/transport.o\
           $L/threads_ew.o $L/queue_max_size.o $L/getutil.o $L/sema_ew.o\
           $L/socket_ew.o $L/ws_clientII.o $L/socket_ew_common.o 

earth2uw: $(BINARIES)
	cc -o earth2uw ${CFLAGS} $(BINARIES) -lnsl -lsocket -lposix4 -lthread 


.c.o:
	cc -c $(CFLAGS) $<


clean:
	-rm *.o earth2uw

uw_parse_trig.o: parse_trig.h

snippet.o: parse_trig.h putaway.h snippet.h

uwputaway.o: uwdfif.h putaway.h

earth2uw.o: parse_trig.h putaway.h
