/*
 * uw_parse_trig.h : Include file for anyone using parse_trig.c; 
 *                defines the SNIPPET structure. This is filled
 *		  by the parse_snippet.c routines, one line at a
 *		  time.
 */

/* Simple Definitions
 ********************/
#define RETURN_SUCCESS  0
#define RETURN_FAILURE -1 


typedef struct
{
long 	eventId;
char 	sta[6];
char	chan[6];
char	net[3];
char	startstr[16];	/* as parsed from trigger file */
double 	starttime;
int	duration;
}
SNIPPET;

/* Function prototypes
 *********************/
int parseSnippet( char* , SNIPPET* , char** );
int strtimetodbl( char*, double * );
int sec15epoch( double, char** );
