#
# This is eqtee's parameter file
#
MyModuleId  MOD_EQPROC  # module id to label logfile  with.
                        # Note: eqtee is part of a mega-module which is
                        # ultimately started by the program eqproc.  All
                        # child processes of this mega-module need to use the
                        # same module id (thus use eqproc's module id).
LogFile       1         # 0=no log; 1=log errors

# send output to the following command:
# PipeTo "eqverify eqverify.d"
PipeTo "/bin/cat"

# Set suffixes for output filenames
# NOTE: If a suffix is "none", no file will   
#       be reported for that type of message
ArcSuffix     "none"        # suffix for archive (hinvarc message) files
UWSuffix      "y"           # suffix for UW pickfile from earthworm

# Directories to write files, end with a `/'
# NOTE: If a suffix is "none", no corresponding directory is needed
# Don't leave trailing spaces!
UWDir      /earthworm/UW-src/eq_tee

# Command to run on UW pick file.
# Command will be run in background, so its output should be collected by
# the command itself.
# Command may be ommitted here if UWSuffix is "none"
UWCommand  /earthworm/bin/earth_proc

# Minimum coda duration. Coda's of duration less than this value
# will be deleted from the pick file.
MinCoda 12

# Optional command to put network ID in pickfile
# WriteNet
