# B = ../../bin
L = ../../v2.9/lib
CFLAGS = -g ${GLOBALFLAGS}

eqtee: eqtee.o uw_pick2.o $L/chron3.o $L/getutil.o $L/kom.o $L/logit.o \
	$L/time_ew.o $L/pipe.o $L/sleep_ew.o
	cc ${CFLAGS} -o eqtee eqtee.o uw_pick2.o $L/chron3.o $L/getutil.o \
	$L/kom.o $L/logit.o $L/time_ew.o $L/pipe.o $L/sleep_ew.o -lm -lposix4 


eqtest: eqtest.o uw_pick2.o $L/chron3.o $L/getutil.o $L/kom.o $L/logit.o \
	$L/time_ew.o $L/pipe.o $L/sleep_ew.o
	cc ${CFLAGS} -o eqtest eqtest.o uw_pick2.o $L/chron3.o $L/getutil.o \
	$L/kom.o $L/logit.o $L/time_ew.o $L/pipe.o $L/sleep_ew.o -lm -lposix4 


arcfeeder: arcfeeder.o $L/getutil.o $L/kom.o $L/pipe.o $L/sleep_ew.o
	cc ${CFLAGS} -o arcfeeder arcfeeder.o $L/getutil.o $L/kom.o $L/pipe.o \
	$L/sleep_ew.o -lposix4	

arcwrite: arcwrite.o $L/getutil.o $L/kom.o $L/pipe.o $L/sleep_ew.o
	cc ${CFLAGS} -o arcwrite arcwrite.o $L/getutil.o $L/kom.o $L/pipe.o \
	$L/sleep_ew.o -lposix4	



clean:
	-rm -f *.o eqtee arcfeeder


.c.o:
	cc -c ${CFLAGS} $<
