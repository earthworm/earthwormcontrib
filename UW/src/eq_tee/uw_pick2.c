/* uw_pick2.c: routines for reading hypoinverse archive files and translating
 * them to UW2-format pick files. Includes some santiy checks for coda
 * durations to eliminate some bogus codas.
 */

/* Change 12/26/97: add ".UW" network designation to channel identifier in
 * pick lines of pickfile.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "uw_pick.h"

/* Function prototypes
 *******************************/
void  uw_report_dosum   ( char * );
void  uw_report_dopick  ( char * );
int   uw_report_write   ( FILE * );

/* things to get from Hypoinverse arc message */
#define MAXPICKS 200          /* maximum number of pick lines in msg */
PICK     pick[MAXPICKS];
time_t   reftime;
int      n_sta;                /* index for stations lines read from arc msg */
int      numphase;             /* Count of P/S phase picks */
char     Acard[100];
int      MinCoda = 0;          /* Minimum coda duration, from config */
int      numcoda;
int      meancoda;
int      WriteNet = 0;         /* =1 to write network code in pickfile */

/**************************************************************************
 * uw_report_dosum() transform Hypoinverse summary card into UW2 A-card *
 ***************************************************************************/
void uw_report_dosum( char *card )
{
  struct tm time_str;
  float depth, dmin, rms, erh, erz;
  int numsta, gap, i;
  char qs, qd;

  /* start with a blank card */
  for(i=0;i<=80;++i) Acard[i] = ' ';
  n_sta = 0;
  numphase = 0;
  numcoda = 0;
  meancoda = 0;

  /* Start the card; could add an event type here */
  strcpy( Acard+1, "A " );

  /* Put reference time in A card and in time structure */
  for(i=3;i<=12;++i)  Acard[i] = card[i-2];   /* date (yymmddhhmm) */
  sscanf( Acard+3, "%2d%2d%2d%2d%2d", &(time_str.tm_year), &(time_str.tm_mon),
	  &(time_str.tm_mday), &(time_str.tm_hour), &(time_str.tm_min) );
  time_str.tm_mon--;   /* Unix months are [0-11], not [1-12] */
  time_str.tm_sec = 0;
  time_str.tm_isdst = 0;

  /* Convert reference time into Standard UNIX time */
  reftime = mktime( &time_str );

  /* Event time, seconds after reference time */
  for(i=14;i<=15;++i) Acard[i] = card[i-3];
  Acard[16] = '.';
  for(i=17;i<=18;++i) Acard[i] = card[i-4];

  /* Latitude of hypocenter */
  for(i=20;i<=26;++i) Acard[i] = card[i-5];
  Acard[22] = 'N';
  
  /* Longitude of hypocenter */
  for(i=28;i<=35;++i) Acard[i] = card[i-6];
  Acard[31] = 'W';

  /* Depth, but no `fix' information */
  for(i=37;i<=38;++i) Acard[i] = card[i-6];
  if ( Acard[37] == '0' ) Acard[37] = ' ';
  Acard[39] = '.';
  for(i=40;i<=41;++i) Acard[i] = card[i-7];
  sscanf( Acard+37, "%5f", &depth );

  /*
   * Magnitude: use "primary coda duration magnitude"
   * There won't be a magnitude if this message hasn't been through
   * hypoinverse
   */
  Acard[44] = card[68];
  Acard[45] = '.';
  Acard[46] = card[69];
  if ( Acard[44] == ' ' ) Acard[44] = '0';
  if ( Acard[46] == ' ' ) Acard[46] = '0';

  /* Numsta; numphase must wait until pick lines are read */
  for(i=47;i<=49;++i) Acard[i] = card[i-10];
  sscanf( Acard+47, "%3d", &numsta );
  Acard[50] = '/';

  /* Azimuthal gap */
  for(i=55;i<=57;++i) Acard[i] = card[i-15];
  sscanf( Acard+55, "%3d", &gap );
  
  /* Distance to nearest station */
  for(i=58;i<=60;++i) Acard[i] = card[i-15];
  if ( Acard[58] == '0' ) Acard[58] = ' ';
  sscanf( Acard+58, "%5f", &dmin );

  /* RMS error */
  for(i=61;i<=62;++i) Acard[i] = card[i-15];
  Acard[63] = '.';
  for(i=64;i<=65;++i) Acard[i] = card[i-16];
  sscanf( Acard+61, "%f", &rms );
  for(i=62;i<=65;i++) {
    if ( Acard[i] == ' ' ) Acard[i] = '0';
  }

  /* horizontal error and vertical errors
   * We put both into Acard so we can convert them to floats
   * which are used for quality determination.
   * Then we rewrite the Acard with the larger of the two errors */  

  /* Put erz in first, since it is usually larger than erh */
  for(i=67;i<=68;++i) Acard[i] = card[i+18];
  Acard[69] = '.';
  for(i=70;i<=71;++i) Acard[i] = card[i+17];
  sscanf( Acard+67, "%5f", &erz );
  
  for(i=73;i<=74;++i) Acard[i] = card[i+8];
  Acard[75] = '.';
  for(i=76;i<=77;++i) Acard[i] = card[i+7];
  sscanf( Acard+73, "%5f", &erh );

  /* Round the larger error into the Acard */
  if ( erz < erh )    
    sprintf( card+67, "%f4.1", erh + 0.05 );
   else
    sprintf( card+67, "%f4.1", erz + 0.05 );
  for(i=68;i<=70;i++) {
    if ( Acard[i] == ' ' ) Acard[i] = '0';
  }
   
  /* err is reported to tenths of km, so clean the rest out */
  for(i=71;i<=77;++i) Acard[i] = ' ';
  
  /* Compute qs, qd: quality codes */
  /* Note the UW numsta limits are different from Menlo limits */
  if     (rms <0.15 && erh<=1.0 && erz <= 2.0) qs = 'A';
  else if(rms <0.30 && erh<=2.5 && erz <= 5.0) qs = 'B';
  else if(rms <0.50 && erh<=5.0)               qs = 'C';
  else                                         qs = 'D';

  if     (numsta >= 8 && gap <=  90 && (dmin<=depth    || dmin<= 5)) qd = 'A';
  else if(numsta >= 7 && gap <= 135 && (dmin<=2.*depth || dmin<=10)) qd = 'B';
  else if(numsta >= 6 && gap <= 180 &&  dmin<=50)                    qd = 'C'; 
  else                                                           qd = 'D';

  Acard[71] = qs;
  Acard[72] = qd;

  /* Velocity model */
  for(i=74;i<=75;++i) Acard[i] = card[i+29];
  
  /* That's it, except for numphase which needs data from the pick lines */
  Acard[76] = '\0';

}

/*****************************************************************************
 *     uw_report_dopick() processes a hypoinverse archive pick line
 *               in preparation for writing a UW2 pick file.
 ****************************************************************************/

void uw_report_dopick( char *card )
{
  int i = 0;
  char field[6];
  struct tm time_str;
  time_t picktime;
  int picksec;
  
  /* 
   * We assume that all phase picks are P-wave picks.
   * The logic of this routine and uw_report_write would need to be
   * modified if S-wave picks are generated by earthworm
   */

  /* station and component */
  strncpy( pick[n_sta].sta_name, card+1, 4 );
  if ( pick[n_sta].sta_name[3] == ' ' ) pick[n_sta].sta_name[3] = (char)0;
  strncpy( pick[n_sta].comp, card+96, 3 );

  /*
   * P-wave pick data 
   */

  /* first motion */
  pick[n_sta].pfm = card[7];

  /* pick weight */
  field[0] = card[8];
  field[1] = '\0';
  if ( field[0] == ' ' ) field[0] = '0';
  pick[n_sta].pwt = atoi( field );

  for(i=10;i<=24;i++) {
    if ( card[i] == ' ' ) card[i] = '0';
  }
  /* 
   * picktime is year, month, day, hour and minute of this pick line.
   * picktime gets converted to seconds after reference time
   * P and S picks each have seconds and hundreths on the pick line.
   */
  sscanf( card+10, "%2d%2d%2d%2d%2d", &(time_str.tm_year), &(time_str.tm_mon),
	  &(time_str.tm_mday), &(time_str.tm_hour), &(time_str.tm_min) );
  time_str.tm_mon--;   /* Unix months are [0-11], not [1-12] */
  time_str.tm_sec = 0;
  time_str.tm_isdst = 0;

  /* Convert pick time into Standard UNIX time */
  picktime = mktime( &time_str );

  /*
   * hundreths of seconds between reference and pick time
   * There is a problem here:
   * Hypoinverse does not put a decimal point, while the Event2 msg
   * does have a decimal point. Reported as possible bug to Barbara B.
   * by PNL 8/5/97
   */
  if ( card[22] == '.' ) { /* Event2 msg has a decimal point */
    field[0] = '0';
    field[1] = card[20];
    field[2] = card[21];
    field[3] = card[23];
    field[4] = card[24];
    field[5] = '\0';
  } else {
    for(i=20;i<=24;i++) field[i-20] = card[i];
  }
  for(i=0;i<5;i++) if (field[i] == ' ') field[i] = '0';
  pick[n_sta].p = atoi( field );
  pick[n_sta].p += ( 100.0 * ( picktime - reftime ));

  /* pick residual, hundreths of seconds */
  for(i=25;i<=28;i++) field[i-25] = card[i];
  field[4] = '\0';
  pick[n_sta].pres = atoi( field );

  /*
   * pick uncertainty
   */
  if      ( pick[n_sta].pwt == 0 ) pick[n_sta].punc = 0.03;
  else if ( pick[n_sta].pwt == 1 ) pick[n_sta].punc = 0.06;
  else if ( pick[n_sta].pwt == 2 ) pick[n_sta].punc = 0.15;
  else if ( pick[n_sta].pwt == 3 ) pick[n_sta].punc = 0.30;
  else                           pick[n_sta].punc = 5.00;
  
  /* check for large residual */
  if ( pick[n_sta].pres > 50 ) {
    pick[n_sta].pwt = 8;      /* pick is weighted out */
  } else {
  /* Is this pick used in this event? */
    if ( (pick[n_sta].pwt <= 3 ) && (time_str.tm_year > 0)) numphase++;
  }


  /*
   * Coda duration
   */

  for(i=72;i<=75;i++) field[i-72] = card[i];
  field[4] = '\0';
  pick[n_sta].dur = atoi( field );
  /*  logit("", "raw coda: %d\n", pick[n_sta].dur); */ /* DEBUG */
  if (pick[n_sta].dur > MinCoda) {
    meancoda += pick[n_sta].dur;
    numcoda++;
    /* logit("", "filtered coda: %d, num: %d\n", pick[n_sta].dur, numcoda); */ /* DEBUG */
  }

  /* Done reading this pick line */
  n_sta++;

}

/****************************************************************************
 *   uw_report_write() write a reasonable facsimile of a uw2-format pick file
 ****************************************************************************/
int uw_report_write( FILE *fp )
{
  int sta, i;
  char card[80];
  int      min_coda_this_event;
  
  /* Put the number of phases into the A card and print it */
  sprintf( Acard+51, "%3.3d%s\n", numphase, Acard+54 );
  fprintf( fp, "%s", Acard+1 );

  /* make sure there are lots of codas if their mean is large */
  if (numcoda > 0) meancoda = meancoda / numcoda;
  /*  logit("t", "numcoda: %d; meancoda: %d\n", numcoda, meancoda); */ /* DEBUG */

  min_coda_this_event = MinCoda;
  if ((meancoda > 50) && (numcoda < 5)) {  /* must be some bogosity here */
    /* so we'll eliminate all the codas */
    min_coda_this_event = meancoda * numcoda + 5;  
  }
  /*  logit("", "min coda: %d\n", min_coda_this_event); */ /* DEBUG */

  /* Write out each of the picks */
  for (sta = 0; sta< n_sta; sta++) {
    
    for (i=0;i<80;i++) card[i] = (char)0;

    /* Station name and component */
    card[0] = '.';
    strcat( card, pick[sta].sta_name );
    strcat( card, "." );
    strcat( card, pick[sta].comp );
    if (WriteNet) strcat( card, ".UW" );

    /* 
     * P pick 
     */
    strcat( card, " (P P " );

    /* First motion */
    if ( pick[sta].pfm == ' ' ) {
      strcat( card, "_" );
    } else {
      card[strlen( card )] = pick[sta].pfm;
    }

    /* arrival time, relative to reference time in A card */
    sprintf( card+strlen(card), " %-.3f", 0.01 * (float) pick[sta].p );

    /* Weight */
    sprintf( card+strlen( card ), " %1d", pick[sta].pwt );

    /* Uncertainty */
    sprintf( card+strlen( card ), " %-.3f", pick[sta].punc );

    /* Residual */
    if ( pick[sta].pres < 0 ) {
      sprintf( card+strlen(card), " %-.3f", 0.01 * (float) pick[sta].pres );
    } else {
      sprintf( card+strlen(card), "  %-.3f", 0.01 * (float) pick[sta].pres );
    }

    /* end of P packet */
    strcat( card, ")" );

    /*
     * Add a duration packet if available
     */

    if ( pick[sta].dur > min_coda_this_event ) {
      sprintf( card+strlen( card ), " (D %-.1f)", (float) pick[sta].dur );
    }
    
    /* put the card in the file */
    fprintf( fp, "%s\n", card );

  }
      
  /* A final comment for posterity */
  fprintf( fp, "C LOCATED BY earthworm\n" );

  return(0);
}
