
/********************************************************************
 *                             eqtee                                *
 *                                                                  *
 *  Read an Event2 msg; tee off a file; pass the msg down the pipe  *
 ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <earthworm.h>
#include <chron3.h>
#include <decode.h>
#include <kom.h>

#define BUF_SIZE 50000
static const int InBufl = BUF_SIZE;

/* Functions in this source file:
 ********************************/
void  eqtee_hinvarc ( char * );
void  eqtee_h71sum  ( char * );
void  eqtee_uwpick  ( char * );
void  eqtee_dosum   ( char * );
void  eqtee_dopick  ( char * );
int   eqtee_write   ( FILE * );
void  eqtee_config   ( char * );

/* Things to look up in the earthworm.h tables with getutil.c functions
 **********************************************************************/
unsigned char TypeEvent2;
unsigned char TypeKill;
unsigned char TypeError;

/* Things read from the configuration file
 *****************************************/
unsigned char    MyModId;            /* eqtee's module id (same as eqproc's) */
static   char    NextProcess[100];   /* command to send output to            */
static   int     LogSwitch   = 1;    /* 0 if no logging to disk              */
static   char    ArcDir[50];         /* directory to write arc files         */
static   char    ArcSuffix[6];       /* suffix to use for archive files      */
static   char    SumDir[50];         /* directory to write sum files         */
static   char    SumSuffix[6];       /* suffix to use for summary files      */
static   char    UWDir[50];          /* directory to write UW pick files     */
char     UWSuffix[6];                /* suffix to use for UW pick files      */
static   char    UWCommand[100];     /* command to run with UW pick files    */
static int     SendArc   = 1;        /* =0 if ArcSuffix="none" or "NONE"     */
static int     SendSum   = 1;        /* =0 if SumSuffix="none" or "NONE"     */
static int     SendUW    = 1;        /* =0 if UWSuffix="none" or "NONE"      */
static int     DoCommand = 1;        /* =0 if UWCommand="none" or "NONE"     */
extern int     MinCoda;              /* minimum coda duration                */

main( int argc, char *argv[] )
{
  static char msg[BUF_SIZE];     /* message from pipe          */
  char        line[200];         /* to store lines from msg    */
  char       *in;                /* working pointer to message */
  int         msglen;            /* length of input message    */
  int         type;             
  int         exit_status = 0;
  int         rc;

  /* Check command line arguments
  ****************************/
  if ( argc != 2 )
    {
      fprintf( stderr, "Usage: eqtee <configfile>\n" );
      exit( 0 );
    }

  /*  sleep(300); */

  /* Read the configuration file(s)
  ********************************/
  eqtee_config( argv[1] );

  /* Look up message types in earthworm.h tables
  *******************************************/
  if ( GetType( "TYPE_EVENT2", &TypeEvent2 ) != 0 ) {
    fprintf( stderr,
	     "eqtee: Invalid message type <TYPE_EVENT2>; exitting!\n" );
    exit( -1 );
  }
  if ( GetType( "TYPE_KILL", &TypeKill ) != 0 ) {
    fprintf( stderr,
	     "eqtee: Invalid message type <TYPE_KILL>; exitting!\n" );
    exit( -1 );
  }
  if ( GetType( "TYPE_ERROR", &TypeError ) != 0 ) {
    fprintf( stderr,
	     "eqtee: Invalid message type <TYPE_ERROR>; exitting!\n" );
    exit( -1 );
  }

  /* Initialize name of log-file & open it
  ***************************************/
  logit_init( "eqtest", (short) MyModId, 256, LogSwitch );
  logit( "" , "eqtest: Read command file <%s>\n", argv[1] );

  /* Spawn the next process
  ************************/
  sleep_ew(100);
  if ( pipe_init( NextProcess, (unsigned long)0 ) == -1 )
    {
      logit( "e", "eqtee: Error starting next process <%s>; exitting!\n",
	     NextProcess );
      exit( -1 );
    }
  logit( "e", "eqtee: piping output to <%s>\n", NextProcess );

  /*-------------------------Main Program Loop----------------------------*/

  do
    {
      /* Get a message from the pipe
      *****************************/
      rc = pipe_get( msg, InBufl, &type );

      if ( rc < 0 )
	{
	  if ( rc == -1 )
            logit( "et", "eqtee: Message in pipe too big for buffer\n" );
	  if ( rc == -2 )
            logit( "et", "eqtee: Trouble reading msg type from pipe\n" );
	  exit_status = rc;
	  break;
	}

      /* Process the event messages
      ****************************/
      if ( type == (int) TypeEvent2 )
	{
	  /* Make pick files out of the message */
	  if( SendArc ) eqtee_hinvarc( msg );
	  if( SendUW )  eqtee_uwpick( msg );
	}

      /* Pass all other types of messages along
      ****************************************/
      if ( type != (int) TypeKill )
	{
	  if ( pipe_put( msg, type ) != 0 )
            logit( "et", "eqtee: Error writing msg to pipe.\n");
	}

      /* Terminate when a kill message is received
      *******************************************/
    } while ( type != (int) TypeKill );

  /*-----------------------------End Main Loop----------------------------*/

  /* Send a kill message to the child.
     Wait for the child to die.
     ***********************************/
  pipe_put( "", (int) TypeKill );
  pipe_close();

  logit( "t", "eqtee: Termination requested. Exitting.\n" );
  exit( exit_status );
}

/******************************************************************************
 *  eqtee_uwpick( )  process Hypoinverse archive message into UW2 format *
 *****************************************************************************/
void eqtee_uwpick( char *arcmsg )
{
  FILE *fp;
  char *msg;         /* pointer to current location in the arc message */
  char  card[200];
  char  c4[5];
  char  fname[25];
  char  pathname[75];
  char  commandline[200];
  long  length;
  int   i, result;

  msg = arcmsg;

  /* Create name for local file using the event origin 
   time and SUFFIX read from the configuration file
   command UWSuffix.
   Example:  96012113245y
             yymmddhhmmsSUFFIX
 *********************************************/
  strncpy( fname,    msg,     11 );
  strcpy ( fname+11, UWSuffix      );
  for ( i=0; i<(int) strlen(fname); i++ )  
    {
      if( fname[i] == ' ' )  fname[i] = '0';
    }   

  /* Read in the first (summary) line of the message 
     Don't use spot 0 in arrays; it is easier to read &
     matches column numbers in documentation.
     *************************************************/
  sscanf( msg, "%[^\n]", card+1 );
  msg += (strlen(card+1) + 1);  /* move past the line and the newline */

   /* Process the summary card */
  uw_report_dosum( card );

  /* loop to read the remaining lines from the message */
  while ( sscanf( msg, "%[^\n]", card+1 ) ) {
    msg += (strlen(card+1) + 1);
    
    if ( card[1] == '$' )    /* shadow card, skip it */
      continue;
    
    strncpy( c4, card+1, 4 );
    c4[4] = '\0';
    if ( !strcmp( c4, "    " ) )  /* terminator card, all done */
      break;
    
    /* Process the pick line */
    uw_report_dopick( card );
    
  }  /* end of message loop */
  
  
  /* Write file */
  strcpy( pathname, UWDir );
  strcat( pathname, fname );
  if( (fp = fopen( pathname, "w" )) == (FILE *) NULL )
    {
      printf( "eqtee: error opening file <%s>\n", pathname );
      exit(1);
    }
  
  if ( uw_report_write( fp ) != 0 )
    {
      printf( "eqtee: error writing to file <%s>\n",
	       pathname );
    } 
  fclose( fp );
  
  /* Run the selected command on the pick file */
  if (DoCommand) {
    sprintf(commandline, "%s %s &", UWCommand, pathname);
    logit("t", "eqtee: run command:\n");
    logit("", "\t %s", commandline);
    system(commandline);
  }

  return;
}

/******************************************************************************
 *  eqtee_hinvarc( )  process a Hypoinverse archive message            *
 ******************************************************************************/
void eqtee_hinvarc( char *arcmsg )
{
   FILE *fparc;
   char  fname[25];
   char  pathname[75];
   long  length;
   int   i, res;

/* Create name for local file using the event 
   origin time, last 2 digits of the event id,
   and SUFFIX read from the configuration file
   command ArcSuffix.
   Example:  9601211324_98.arc
             yymmddhhmm_xx.SUFFIX
 *********************************************/
   strncpy( fname,    arcmsg,     10 );
   fname[10] = '_';
   strncpy( fname+11, arcmsg+136,  2 );
   fname[13] = '.';
   strcpy ( fname+14, ArcSuffix      );
   for ( i=0; i<(int) strlen(fname); i++ )  
   {
       if( fname[i] == ' ' )  fname[i] = '0';
   }   

/* Write file
 ******************/
   strcpy( pathname, ArcDir );
   strcat( pathname, fname );
   if( (fparc = fopen( pathname, "w" )) == (FILE *) NULL )
   {
     logit( "e", "eqtee: error opening file <%s>\n",
                pathname );
     return;
   }
   length = strlen( arcmsg );
   if ( fwrite( arcmsg, (size_t)1, (size_t)length, fparc ) == 0 ) 
   {
     logit( "e", "eqtee: error writing to file <%s>\n",
                fname );
     fclose( fparc );
     return;
   } 
   fclose( fparc );

   return;
}

/****************************************************************************
 * eqtee_config() processes the configuration file using kom.c           *
 *                   functions exits if any errors are encountered          *
 ****************************************************************************/
void eqtee_config( char *configfile )
{
  int      ncommand;     /* # of required commands you expect to process   */
  char     init[10];     /* init flags, one byte for each required command */
  int      nmiss;        /* number of required commands that were missed   */
  char    *com;
  char    *str;
  int      nfiles;
  int      success;
  int      i;

  /* Set to zero one init flag for each required command
  *****************************************************/
  ncommand = 9;
  for( i=0; i<ncommand; i++ )  init[i] = 0;

  /* Open the main configuration file
  **********************************/
  nfiles = k_open( configfile );
  if ( nfiles == 0 ) {
    fprintf( stderr,
	     "eqtee: Error opening command file <%s>; exitting!\n",
	     configfile );
    exit( -1 );
  }

  /* Process all command files
  ***************************/
  while(nfiles > 0)   /* While there are command files open */
    {
      while(k_rd())        /* Read next line from active file  */
        {
	  com = k_str();         /* Get the first token from line */

	  /* Ignore blank lines & comments
	  *******************************/
	  if( !com )           continue;
	  if( com[0] == '#' )  continue;

	  /* Open a nested configuration file
	  **********************************/
	  if( com[0] == '@' ) {
	    success = nfiles+1;
	    nfiles  = k_open(&com[1]);
	    if ( nfiles != success ) {
	      fprintf( stderr,
		       "eqtee: Error opening command file <%s>; exitting!\n",
		       &com[1] );
	      exit( -1 );
	    }
	    continue;
	  }

	  /* Process anything else as a command.
	     Numbered commands are required; 
	     Un-numbered commands change default values.
	     *********************************************/
	  /*0*/      if( k_its( "LogFile" ) )
            {
	      LogSwitch = k_int();
	      init[0] = 1;
            }
	  /*1*/      else if( k_its( "MyModuleId" ) )
            {
	      if ( ( str=k_str() ) ) {
		if ( GetModId( str, &MyModId ) != 0 ) {
		  fprintf( stderr,
			   "eqtee: Invalid module name <%s>; exitting!\n",
			   str );
		  exit( -1 );
		}
	      }
	      init[1] = 1;
            }

	  /* Read in the next command to pipe messages to
          **********************************************/
	  /*2*/     else if( k_its("PipeTo") )
            {
	      str = k_str();
	      if(str) strcpy( NextProcess, str );
	      init[2] = 1;
            }

	  /* Suffix to use for output archive files 
          ****************************************/
	  /*3*/    else if( k_its("ArcSuffix") ) {
	    str = k_str();
	    if(str) {
	      if( strlen(str) >= sizeof(ArcSuffix) ) {
		fprintf( stderr,
			 "eqtee: ArcSuffix <%s> too long; ", str );
		fprintf( stderr, "max length:%d; exitting!\n",
			 (int)(sizeof(ArcSuffix)-1) );
		exit(-1);
	      }
	      /* ArcSuffix shouldn't include the period */
	      if( str[0] == '.' ) strcpy( ArcSuffix, &str[1] );
	      else                strcpy( ArcSuffix, str     );
	      /* Set SendArc flag based on ArcSuffix value */
	      if     (strncmp(ArcSuffix, "none", 4) == 0) SendArc=0;
	      else if(strncmp(ArcSuffix, "NONE", 4) == 0) SendArc=0;
	      else if(strncmp(ArcSuffix, "None", 4) == 0) SendArc=0;
	      else                                        SendArc=1;

	      init[3] = 1;
	      if ( SendArc == 0 ) init[4] = 1; /* don't need directory */
            }
	  }

	  /* Enter name of local directory for arc files
          *****************************************************/
	  /*4*/     else if( k_its("ArcDir") ) {
	    str = k_str();
	    if (str) strcpy( ArcDir, str );
	    /* make sure ArcDir ends in "/" */
	    if ( ArcDir[strlen(ArcDir)-1] != '/' )
	      strcat( ArcDir, "/" );
	    init[4] = 1;
	  }

	  /* Suffix to use for UW pick files
          ****************************************/
	  /*5*/    else if( k_its("UWSuffix") ) {
	    str = k_str();
	    if(str) {
	      if( strlen(str) >= sizeof(UWSuffix) ) {
		fprintf( stderr,
			 "eqtee: UWSuffix <%s> too long; ", str );
		fprintf( stderr, "max length:%d; exitting!\n",
			 (int)(sizeof(UWSuffix)-1) );
		exit(-1);
	      }
	      /* UWSuffix shouldn't include the period */
	      if( str[0] == '.' ) strcpy( UWSuffix, &str[1] );
	      else                strcpy( UWSuffix, str     );
	      /* Set SendUW flag based on UWSuffix value */
	      if     (strncmp(UWSuffix, "none", 4) == 0) SendUW=0;
	      else if(strncmp(UWSuffix, "NONE", 4) == 0) SendUW=0;
	      else if(strncmp(UWSuffix, "None", 4) == 0) SendUW=0;
	      else                                       SendUW=1;

	      init[5] = 1;
	      if ( SendUW == 0 ) {
		init[6] = 1;
		init[7] = 1;
	      }
	    }
	  }

	  /* Enter name of local directory for UW pick files
          *****************************************************/
	  /*6*/     else if( k_its("UWDir") ) {
	    str = k_str();
	    if(str) strcpy( UWDir, str );
	    /* make sure UWDir ends in "/" */
	    if ( UWDir[strlen(UWDir)-1] != '/' )
	      strcat( UWDir, "/" );
	    init[6] = 1;
	  }

	  /* Enter command to run on UW Pick files
          *****************************************************/
	  /*7*/     else if( k_its("UWCommand") ) {
	    str = k_str();
	    if (str) strcpy( UWCommand, str );
	    /* Set SendSum flag based on SumSuffix value */
	    if     (strncmp(UWCommand, "none", 4) == 0) DoCommand=0;
	    else if(strncmp(UWCommand, "NONE", 4) == 0) DoCommand=0;
	    else if(strncmp(UWCommand, "None", 4) == 0) DoCommand=0;
	    else                                        DoCommand=1;
	    init[7] = 1;
	  }
	  
	  /*8*/     else if( k_its( "MinCoda" ) ) {
	    MinCoda = k_int();
	    init[8] = 1;
	  }
	  /* Unknown command 
          *****************/
	  else {
	    fprintf(stderr, "eqtee: <%s> Unknown command in <%s>.\n",
		    com, configfile );
	    continue;
	  }

	  /* See if there were any errors processing the command
	  *****************************************************/
	  if( k_err() ) {
	    fprintf( stderr,
		     "eqtee: Bad <%s> command in <%s>; exitting!\n",
		     com, configfile );
	    exit( -1 );
	  }
        }
      nfiles = k_close();
    }

  /* After all files are closed, check init flags for missed commands
  ******************************************************************/
  nmiss = 0;
  for ( i=0; i<ncommand; i++ )  if( !init[i] ) nmiss++;
  if ( nmiss ) {
    fprintf( stderr, "eqtee: ERROR, no " ); 
    if ( !init[0] )  fprintf( stderr, "<LogFile> "   );
    if ( !init[1] )  fprintf( stderr, "<MyModuleId> ");
    if ( !init[2] )  fprintf( stderr, "<PipeTo> "    );
    if ( !init[3] )  fprintf( stderr, "<ArcSuffix> " );
    if ( !init[4] )  fprintf( stderr, "<ArcDir> "    );
    if ( !init[5] )  fprintf( stderr, "<UWSuffix> "  );
    if ( !init[6] )  fprintf( stderr, "<UWDir> "     );
    if ( !init[7] )  fprintf( stderr, "<UWCommand> " );
    if ( !init[8] )  fprintf( stderr, "<MinCoda> "   );
    fprintf( stderr, "command(s) in <%s>; exitting!\n", configfile );
    exit( -1 );
  }

  return;
}

